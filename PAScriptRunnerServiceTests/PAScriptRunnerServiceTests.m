//
//  PAScriptRunnerServiceTests.m
//  PAScriptRunnerServiceTests
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "PAScriptRunnerService.h"

@interface PAScriptRunnerServiceTests : XCTestCase

@end

@implementation PAScriptRunnerServiceTests

- (void)setUp {
  [super setUp];
  // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
  // Put teardown code here. This method is called after the invocation of each test method in the class.
  [super tearDown];
}

- (void)testExecutablePermissionIsSet {
  NSString *path = @"/Users/admin/Library/Application Support/ProactiveAssist/TestScript.sh";
  PAScript *script = [[PAScript alloc] init];
  BOOL result = [script setExecutablePermissionToScriptAtPath:path];
  XCTAssert(result, @"Executable Permission to Script is successfully set");
}

- (void)testPerformanceExample {
  // This is an example of a performance test case.
  [self measureBlock:^{
    // Put the code you want to measure the time of here.
  }];
}

@end
