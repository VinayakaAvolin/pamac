//
//  PAInterProcessManager.m
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PAInterProcessManager.h"
#import "PAPrivilegedToolProtocol.h"
#import "PALoginAgentProtocol.h"
#import "PAMonitoringAgentProtocol.h"
#import "PAServiceResponse.h"
#import "PASelfHealTriggerInfo.h"
#import <Security/Authorization.h>
#import <ServiceManagement/ServiceManagement.h>
#import "EventDeliveryProtocol.h"




@interface PAInterProcessManager ()
// private stuff


//@property (atomic, strong, readwrite) NSXPCConnection * privilegedToolConnection;
@property (atomic, strong, readwrite) id<PAPrivilegedToolProtocol> privilegedToolConnection;
//@property (atomic, strong, readwrite) NSXPCConnection * loginAgentConnection;
@property (atomic, strong, readwrite) id<PALoginAgentProtocol> loginAgentConnection;
@property (atomic, strong, readwrite) id<PAMonitoringAgentProtocol>monitoringAgentConnection;
//@property (atomic, strong, readwrite) NSXPCConnection *rawLoginAgentConnection;
//@property (atomic, strong, readwrite)NSXPCConnection *rawPrivilegedToolConnection;


@end

@implementation PAInterProcessManager

+ (id)sharedManager {
  static PAInterProcessManager *sharedManager = nil;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    sharedManager = [[self alloc] init];
  });
  return sharedManager;
}
- (void)executeScript:(PAScriptInfo *)script withReply:(PAIPScriptResult)reply {
  switch (script.scriptMode) {
    case PAScriptModeNormal:
      [self executeScriptInUserMode:script withReply:reply];
      break;
    case PAScriptModeElevated:
      [self executeScriptInElevatedMode:script withReply:reply];
      break;
    default:
      reply(false, nil, nil);
      break;
  }
}

- (void)executeService:(PAServiceInfo *)service withReply:(PAIPServiceResult)reply {
  switch (service.serviceMode) {
    case PAServiceModeNormal:
      [self executeServiceInUserMode:service withReply:reply];
      break;
    case PAServiceModeElevated:
      [self executeServiceInElevatedMode:service withReply:reply];
      break;
    default:
      reply(false, nil, nil);
      break;
  }
}

- (void)executePlistService:(PAPlistServiceInfo *)service withReply:(PAIPPlistServiceResult)reply {
  switch (service.serviceMode) {
    case PAServiceModeNormal:
      [self executePlistServiceInUserMode:service withReply:reply];
      break;
    case PAServiceModeElevated:
      [self executePlistServiceInElevatedMode:service withReply:reply];
      break;
    default:
      reply(false);
      break;
  }
}

- (void)executeMonitorService:(PASelfHealTriggerInfo *)triggerInfo withReply: (PAIPMonitorServiceResult)reply {
    switch (triggerInfo.userMode) {
        case admin:
            [self executeMonitorServiceInElevatedMode:triggerInfo withReply:nil];
            break;
        case user:
            [self executeMonitorServiceInUserMode:triggerInfo withReply:nil];
            break;
        default:
            break;
    }
}

-(void) stopMonitoringForEvent:(NSString *)eventGuid withReply:(PAIPMonitorServiceResult)reply
{
    [self connenctToMonitoringAgent];
    [self connectToPrivilegedTool];
    __weak PAInterProcessManager *_weakSelf = self;
    [self.monitoringAgentConnection stopMonitoringForEvent:eventGuid withReply:^(BOOL success){
        if(success) {
            if (reply)
                reply(success);
        } else {
            [_weakSelf.privilegedToolConnection stopMonitoringForEvent:eventGuid withReply:^(BOOL success) {
                if (reply)
                    reply(success);
            }];
        }
    }];
}

- (void)updateWithCallback:(PAIPSyncResult)reply {
  [self connectToLoginAgent];
  [self.loginAgentConnection updateWithCallback:^(BOOL success) {
    reply(success);
    // [self disconnectLoginAgent];
  }];
}

- (void)startOptimizerWithCallback:(PAIPOptimizationResult)reply {
  [self connectToLoginAgent];
  [self.loginAgentConnection startOptimizerWithReply:^(BOOL success) {
    reply(success);
    //  [self disconnectLoginAgent];
  }];
}
- (void)stopOptimizerWithCallback:(PAIPOptimizationResult)reply {
  [self connectToLoginAgent];
  [self.loginAgentConnection stopOptimizerWithReply:^(BOOL success) {
    reply(success);
    //   [self disconnectLoginAgent];
  }];
}


-(void)scheduleOptimization:(NSDictionary *)scheduleInfo {
  [self connectToLoginAgent];
  [self.loginAgentConnection scheduleOptimization:scheduleInfo];
  //[self disconnectLoginAgent];
}

#pragma mark - Private methods

- (void)connectToPrivilegedTool
//{
//    // Ensures that we're connected to our helper tool.
//    //assert([NSThread isMainThread]);
//    if (self.privilegedToolConnection == nil) {
////        self.privilegedToolConnection = [[NSXPCConnection alloc] initWithListenerEndpoint:endpoint];
//        self.privilegedToolConnection = [[NSXPCConnection alloc] initWithMachServiceName:kPAPrivilegedToolMachServiceName options:NSXPCConnectionPrivileged];
//
//        NSXPCInterface *interface= [NSXPCInterface interfaceWithProtocol:@protocol(PAPrivilegedToolProtocol)];
//        NSSet *expected = [NSSet setWithObjects:[PAServiceResponse class], [NSError class], [NSArray class], [NSDictionary class], [NSString class], [NSNumber class], nil];
//        [interface setClasses:expected
//                  forSelector:@selector(executeScript:withReply:)
//                argumentIndex:0
//                      ofReply:YES];
//        [interface setClasses:expected
//                  forSelector:@selector(executeService:withReply:)
//                argumentIndex:0
//                      ofReply:YES];
//        [interface setClasses:expected
//                  forSelector:@selector(executePlistService:withReply:)
//                argumentIndex:0
//                      ofReply:YES];
//
//        self.privilegedToolConnection.remoteObjectInterface = interface;
//#pragma clang diagnostic push
//#pragma clang diagnostic ignored "-Warc-retain-cycles"
//        self.privilegedToolConnection.invalidationHandler = ^{
//            // If the connection gets invalidated then, on the main thread, nil out our
//            // reference to it.  This ensures that we attempt to rebuild it the next time around.
//            //
//            // We can ignore the retain cycle warning for the reasons discussed in -connectToXPCService.
//            self.privilegedToolConnection.invalidationHandler = nil;
//            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
//                self.privilegedToolConnection = nil;
//                dispatch_async(dispatch_get_main_queue(), ^{
//                });
//            }];
//        };
//#pragma clang diagnostic pop
//        [self.privilegedToolConnection resume];
//    }
//}
{
  // assert([NSThread isMainThread]);
  if (self.privilegedToolConnection == nil) {
    // Setup our connection to the launch item's service.
    // This will start the launch item if it isn't already running.
    NSXPCConnection *privilegedToolConnection = [[NSXPCConnection alloc] initWithMachServiceName:kPAPrivilegedToolMachServiceName options:NSXPCConnectionPrivileged];
    if (privilegedToolConnection == nil) {
      //            [_delegate logHistory:@"Failed to connect to login item: %@\n", [error description]];
      return;
    }
    
    NSXPCInterface *interface= [NSXPCInterface interfaceWithProtocol:@protocol(PAPrivilegedToolProtocol)];
    NSSet *expected = [NSSet setWithObjects:[PAServiceResponse class], [NSError class], [NSArray class], [NSDictionary class], [NSString class], [NSNumber class], [PASelfHealEvent class], [PASelfHealTriggerInfo class], nil];
      
    [interface setClasses:expected
              forSelector:@selector(executeScript:withReply:)
            argumentIndex:0
                  ofReply:YES];
    [interface setClasses:expected
              forSelector:@selector(executeService:withReply:)
            argumentIndex:0
                  ofReply:YES];
    [interface setClasses:expected
              forSelector:@selector(executePlistService:withReply:)
            argumentIndex:0
                  ofReply:YES];
    
    [interface setClasses:expected
              forSelector:@selector(executeMonitorService:withReply:)
            argumentIndex:0
                  ofReply:YES];
    
    privilegedToolConnection.remoteObjectInterface = interface;
    [privilegedToolConnection resume];
    // self.rawPrivilegedToolConnection = privilegedToolConnection;
    
    // Get a proxy DecisionAgent object for the connection.
    self.privilegedToolConnection = [privilegedToolConnection remoteObjectProxyWithErrorHandler:^(NSError *err) {
      
      if (err != nil) {
        self.privilegedToolConnection = nil;
      }
      [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"WARNING: IPC with Privilege tool [%@]",err];
      
      // This block is run when an error occurs communicating with
      // the launch item service.  This will not be invoked on the
      // main queue, so re-schedule a block on the main queue to
      // update the U.I.
      dispatch_async(dispatch_get_main_queue(), ^{
        //                [_delegate logHistory:@"Failed to query oracle: %@\n\n", [err description]];
      });
    }];
      
      NSXPCListener *listener = [NSXPCListener anonymousListener];
      [listener setDelegate: self];
      [listener resume];
      NSXPCListenerEndpoint * endpoint = listener.endpoint;
      [self.privilegedToolConnection registerListenerEndpoint: endpoint];
  }
}

- (void)disconnectPrivilegedTool {
  //[self.rawPrivilegedToolConnection suspend];
  //self.privilegedToolConnection = nil;
}

- (void)disconnectLoginAgent {
  //[self.rawLoginAgentConnection suspend];
  //self.loginAgentConnection = nil;
}

-(void)connenctToMonitoringAgent {
    
    if (self.monitoringAgentConnection == nil) {
        NSXPCConnection *xpcConn = [[NSXPCConnection alloc] initWithMachServiceName:kPAMonitoringAgentServiceName options:0];
        if (xpcConn == nil) {
            return;
        }
        NSXPCInterface *interface = [NSXPCInterface interfaceWithProtocol:@protocol(PAMonitoringAgentProtocol)];
        NSSet *executeMonitorServiceClassList = [NSSet setWithObjects:[PASelfHealTriggerInfo class], [NSString class], [NSArray class],[NSDictionary class], [PASelfHealEvent class],[NSXPCListenerEndpoint class], nil];
        [interface setClasses:executeMonitorServiceClassList
                  forSelector:@selector(executeMonitorService:withReply:)
                argumentIndex:0
                      ofReply:YES];
        xpcConn.remoteObjectInterface = interface;
        [xpcConn resume];
        
        self.monitoringAgentConnection = [xpcConn remoteObjectProxyWithErrorHandler:^(NSError *err) {
            if (err != nil) {
                self.monitoringAgentConnection = nil;
            }
            [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"WARNING: IPC with Monitoring agent [%@]",err];
        }];
         NSXPCListener *listener = [NSXPCListener anonymousListener];
        [listener setDelegate: self];
        [listener resume];
        NSXPCListenerEndpoint * endpoint = listener.endpoint;
        [self.monitoringAgentConnection registerListenerEndpoint: endpoint];
    }
}

- (void)connectToLoginAgent {
  // assert([NSThread isMainThread]);
  if (self.loginAgentConnection == nil) {
    // Setup our connection to the launch item's service.
    // This will start the launch item if it isn't already running.
    NSXPCConnection *loginAgentConnection = [[NSXPCConnection alloc] initWithMachServiceName:kPALoginAgentServiceName options:0];
    if (loginAgentConnection == nil) {
      //            [_delegate logHistory:@"Failed to connect to login item: %@\n", [error description]];
      return;
    }
    
    NSXPCInterface *interface= [NSXPCInterface interfaceWithProtocol:@protocol(PALoginAgentProtocol)];
    NSSet *expected = [NSSet setWithObjects:[PAServiceResponse class], [NSError class], [NSArray class], [NSDictionary class], [NSString class], [NSNumber class], nil];
    [interface setClasses:expected
              forSelector:@selector(executeScript:withReply:)
            argumentIndex:0
                  ofReply:YES];
    [interface setClasses:expected
              forSelector:@selector(executeService:withReply:)
            argumentIndex:0
                  ofReply:YES];
    [interface setClasses:expected
              forSelector:@selector(executePlistService:withReply:)
            argumentIndex:0
                  ofReply:YES];
    loginAgentConnection.remoteObjectInterface = interface;
    [loginAgentConnection resume];
    // self.rawLoginAgentConnection = loginAgentConnection;
    // Get a proxy DecisionAgent object for the connection.
    self.loginAgentConnection = [loginAgentConnection remoteObjectProxyWithErrorHandler:^(NSError *err) {
      
      if (err != nil) {
        self.loginAgentConnection = nil;
      }
      [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"WARNING: IPC with Resident agent [%@]",err];
      
      // This block is run when an error occurs communicating with
      // the launch item service.  This will not be invoked on the
      // main queue, so re-schedule a block on the main queue to
      // update the U.I.
      dispatch_async(dispatch_get_main_queue(), ^{
        //                [_delegate logHistory:@"Failed to query oracle: %@\n\n", [err description]];
      });
    }];
  }
}


- (void)executeScriptInElevatedMode:(PAScriptInfo *)script withReply:(PAIPScriptResult)reply {
  [self connectToPrivilegedTool];
  [self.privilegedToolConnection executeScript:script withReply:^(PAServiceResponse *response) {
    reply(response.success, response.error, [response getResult ]);
    //  [self disconnectPrivilegedTool];
  }];
}

- (void)executeScriptInUserMode:(PAScriptInfo *)script withReply:(PAIPScriptResult)reply {
  [self connectToLoginAgent];
  [self.loginAgentConnection executeScript:script withReply:^(PAServiceResponse *response) {
    reply(response.success, response.error, [response getResult ]);
    // [self disconnectLoginAgent];
  }];
}

- (void)executeServiceInElevatedMode:(PAServiceInfo *)service withReply:(PAIPServiceResult)reply {
  [self connectToPrivilegedTool];
  [self.privilegedToolConnection executeService:service withReply:^(PAServiceResponse *response) {
    reply(response.success, response.error, [response getResult ]);
    //  [self disconnectPrivilegedTool];
  }];
}

- (void)executeServiceInUserMode:(PAServiceInfo *)service withReply:(PAIPServiceResult)reply {
  [self connectToLoginAgent];
  [self.loginAgentConnection executeService:service withReply:^(PAServiceResponse *response) {
    reply(response.success, response.error, [response getResult ]);
    //  [self disconnectLoginAgent];
  }];
}
- (void)executePlistServiceInElevatedMode:(PAPlistServiceInfo *)service withReply:(PAIPPlistServiceResult)reply {
  [self connectToPrivilegedTool];
  [self.privilegedToolConnection executePlistService:service withReply:^(BOOL success) {
    reply(success);
    //  [self disconnectPrivilegedTool];
  }];
}

- (void)executePlistServiceInUserMode:(PAPlistServiceInfo *)service withReply:(PAIPPlistServiceResult)reply {
  [self connectToLoginAgent];
  [self.loginAgentConnection executePlistService:service withReply:^(BOOL success) {
    reply(success);
    // [self disconnectLoginAgent];
  }];
}

- (void)executeMonitorServiceInUserMode:(PASelfHealTriggerInfo *)triggerInfo withReply: (PAIPMonitorServiceResult)reply {
    [self connenctToMonitoringAgent];
    [self.monitoringAgentConnection executeMonitorService: triggerInfo withReply:^(PASelfHealEvent *event) {
        NSLog(@"%@", event.eventGUID);
        [[PAEventBus sharedInstance] postEvent:event];
    }];
}

- (void)executeMonitorServiceInElevatedMode:(PASelfHealTriggerInfo *)triggerInfo withReply: (PAIPMonitorServiceResult)reply {
    // TODO: Remove in production.
    static BOOL debugBlessed = YES;
    if(!debugBlessed) {
        debugBlessed = YES;
        [self blessHelperWithLabel:@"PAPrivilegedTool" error:nil];
    }
    [self connectToPrivilegedTool];
    [self.privilegedToolConnection executeMonitorService: triggerInfo withReply:^(PASelfHealEvent *event) {
        
        NSLog(@"%@", event.eventGUID);
        [[PAEventBus sharedInstance] postEvent:event];

    }];
    
}

- (BOOL)blessHelperWithLabel:(NSString *)label
                       error:(NSError **)error {
    
    BOOL result = NO;
    
    AuthorizationItem authItem        = { kSMRightBlessPrivilegedHelper, 0, NULL, 0 };
    AuthorizationRights authRights    = { 1, &authItem };
    AuthorizationFlags flags        =    kAuthorizationFlagDefaults                |
    kAuthorizationFlagInteractionAllowed    |
    kAuthorizationFlagPreAuthorize            |
    kAuthorizationFlagExtendRights;
    
    AuthorizationRef authRef = NULL;
    
    /* Obtain the right to install privileged helper tools (kSMRightBlessPrivilegedHelper). */
    OSStatus status = AuthorizationCreate(&authRights, kAuthorizationEmptyEnvironment, flags, &authRef);
    if (status != errAuthorizationSuccess) {

        
    } else {
        /* This does all the work of verifying the helper tool against the application
         * and vice-versa. Once verification has passed, the embedded launchd.plist
         * is extracted and placed in /Library/LaunchDaemons and then loaded. The
         * executable is placed in /Library/PrivilegedHelperTools.
         */
        result = SMJobBless(kSMDomainSystemLaunchd, (__bridge CFStringRef)label, authRef, nil);
    }
    
    return result;
}

-(void) publishEvent:(PASelfHealEvent *)event {
    NSLog(@"%@", event.eventGUID);
    [[PALogger sharedLoggerForClient:PALogClientSelfHeal] debug:@"----Publishing event for guid %@ with event data = %@---", event.eventGUID, event.eventData];
    [[PAEventBus sharedInstance] postEvent:event];
}

- (BOOL)listener:(NSXPCListener *)listener shouldAcceptNewConnection:(NSXPCConnection *)newConnection {
    NSXPCInterface *interface = [NSXPCInterface interfaceWithProtocol:@protocol(EventDeliveryProtocol)];
    NSSet *monitorServiceClassList = [NSSet setWithObjects:[PASelfHealTriggerInfo class], [NSString class], [NSArray class],[NSDictionary class], [PASelfHealEvent class], nil];
    [interface setClasses:monitorServiceClassList
              forSelector:@selector(publishEvent:) argumentIndex:0 ofReply:NO];
    newConnection.exportedInterface = interface;
    [newConnection setExportedObject:self];
    [newConnection resume];
    return YES;
}

@end


