//
//  PAInterProcessManager.h
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <PACoreServices/PACoreServices.h>
#import <PASupportService/PASupportService.h>
#import <PAEventBus/PAEventBus.h>
@protocol EventDeliveryProtocol;

typedef void(^PAIPScriptResult)(BOOL success, NSError *error, id result);
typedef void(^PAIPServiceResult)(BOOL success, NSError *error, id result);
typedef void(^PAIPSyncResult)(BOOL finish);
typedef void(^PAIPPlistServiceResult)(BOOL finish);
typedef void(^PAIPOptimizationResult)(BOOL finish);
typedef void(^PAIPMonitorServiceResult)(BOOL finish);

@interface PAInterProcessManager : NSObject<NSXPCListenerDelegate, EventDeliveryProtocol>

+ (id)sharedManager;
/// Method to execute solution script
- (void)executeScript:(PAScriptInfo *)script withReply:(PAIPScriptResult)reply;
/// Method to execute sprocket services like download offers, protect
- (void)executeService:(PAServiceInfo *)service withReply:(PAIPServiceResult)reply;

/// Method to execute plist write using plist service framework
- (void)executePlistService:(PAPlistServiceInfo *)service withReply:(PAIPPlistServiceResult)reply;

/// Method to execute monitor service, which will use plugins to monitor self heal events
- (void)executeMonitorService:(PASelfHealTriggerInfo *)triggerInfo withReply: (PAIPMonitorServiceResult)reply;

/// Method to stop monitoring for an event. Will be delegated to appropriate plugin for stopping monitoring
-(void) stopMonitoringForEvent:(NSString *)eventGuid withReply:(PAIPMonitorServiceResult)reply;

// Method to update or initiate content sync
- (void)updateWithCallback:(PAIPSyncResult)reply;


// Method to initiate optimization
- (void)startOptimizerWithCallback:(PAIPOptimizationResult)reply;
- (void)stopOptimizerWithCallback:(PAIPOptimizationResult)reply;
-(void)scheduleOptimization:(NSDictionary *)scheduleInfo;

@end
