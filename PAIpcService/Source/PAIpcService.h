//
//  PAIpcService.h
//  PAIpcService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Cocoa/Cocoa.h>

//! Project version number for PAIpcService.
FOUNDATION_EXPORT double PAIpcServiceVersionNumber;

//! Project version string for PAIpcService.
FOUNDATION_EXPORT const unsigned char PAIpcServiceVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <PAIpcService/PublicHeader.h>

#import <PAIpcService/PAInterProcessManager.h>
