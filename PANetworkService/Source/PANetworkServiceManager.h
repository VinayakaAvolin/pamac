//
//  PANetworkServiceManager.h
//  PANetworkService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PABaseRequest.h"
#import "PABaseResponse.h"

typedef void(^PANetworkServiceManagerCallback)(NSError *error, PABaseResponse *response);

@interface PANetworkServiceManager : NSObject

- (NSURLSessionDataTask *)executeRequest:(id<PABaseRequestProtocol>)request
                              completion:(PANetworkServiceManagerCallback)callback;

- (void)addDataTask:(NSURLSessionDataTask *)task;
- (void)removeDataTask:(NSURLSessionDataTask *)task;
- (void)cancelDataTask:(NSURLSessionDataTask *)task;
- (void)cancelAllRequests;

@end
