//
//  PABaseResponse.h
//  PANetworkService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSInteger const kPAResponseSuccess;

@interface PABaseResponse : NSObject

@property(nonatomic, readonly) id jsonResponse;
@property(nonatomic, readonly) NSDictionary *allHeaderFields;
@property(nonatomic, readonly) NSInteger statusCode;
@property(nonatomic, readonly) NSData *responseData;

- (instancetype)initWithHeaders:(NSDictionary *)headers
                     statusCode:(NSInteger)code
                     outputData:(NSData *)data;

@end
