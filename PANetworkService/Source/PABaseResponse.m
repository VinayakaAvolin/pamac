//
//  PABaseResponse.m
//  PANetworkService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PABaseResponse.h"

NSInteger const kPAResponseSuccess = 200;

@implementation PABaseResponse

- (instancetype)initWithHeaders:(NSDictionary *)headers
                     statusCode:(NSInteger)code
                     outputData:(NSData *)data {
  self = [super init];
  if (self) {
    if (data) {
      _allHeaderFields = [headers copy];
      _statusCode = code;
      _responseData = data;
      if (data) {
        _jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
        // could be string
        if (!_jsonResponse) {
          _jsonResponse = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        }
      }
    } else {
      self = nil;
    }
  }
  return self;
}

@end
