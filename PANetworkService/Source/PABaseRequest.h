//
//  PABaseRequest.h
//  PANetworkService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *const kPABRMethodNamePost;
extern NSString *const kPABRMethodNameHead;
extern NSString *const kPABRMethodNameGet;
extern NSString *const kPABRContentType;
extern NSString *const kPABRContentTypeFormUrlEncoded;

@protocol PABaseRequestProtocol

- (NSString *)baseUrl;
- (NSString *)path;
- (NSString *)encoding;
- (NSString *)method;
- (NSDictionary *)headers;
- (NSDictionary *)parameters;

@end

@interface PABaseRequest : NSObject <PABaseRequestProtocol>

@end
