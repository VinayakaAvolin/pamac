//
//  PANetworkServiceManager.m
//  PANetworkService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <PALoggerService/PALoggerService.h>
#import "PANetworkServiceManager.h"

@interface PANetworkServiceManager ()

@property (nonatomic,strong) NSMutableArray *taskList;

@end

@implementation PANetworkServiceManager

- (instancetype)init
{
  self = [super init];
  if (self) {
    _taskList = [[NSMutableArray alloc]init];
  }
  return self;
}

- (NSURLSessionDataTask *)executeRequest:(id<PABaseRequestProtocol>)request
                              completion:(PANetworkServiceManagerCallback)callback {
  
  
  NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
  NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
  /// get base url
  NSString *urlString = request.baseUrl;
  // append path to base url
  if (request.path) {
    urlString = [urlString stringByAppendingString:request.path];
  }
  urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
  //[[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"[Network] The request url [%@]",urlString];
  
  NSURL *url = [NSURL URLWithString:urlString];
  // create request
  NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url
                                                            cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                        timeoutInterval:60.0];
  // set headers
  for (NSString *key in request.headers.allKeys) {
    NSString *value = request.headers[key];
    [urlRequest addValue:value forHTTPHeaderField:key];
    // [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"[Network] Header [%@] - [%@]",key, value];
  }
  // set http method
  [urlRequest setHTTPMethod:request.method];
  //[[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"[Network] Method [%@]", request.method];
  // set request body
  NSDictionary *bodyDict = request.parameters;
  NSString *reqParams = nil;
  if (bodyDict.count) {
    reqParams = @"";
    for (NSString *key in bodyDict.allKeys) {
      id value = bodyDict[key];
      if ([reqParams isEqualToString:@""]) {
        reqParams = [reqParams stringByAppendingFormat:@"%@=%@",key,value];
      } else {
        reqParams = [reqParams stringByAppendingFormat:@"&%@=%@",key,value];
      }
    }
    //        NSData *postData = [NSJSONSerialization dataWithJSONObject:bodyDict options:0 error:&error];
    //        [urlRequest setHTTPBody:postData];
  }
  
  if (reqParams) {
    //[[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"[Network] Request parameters [%@]", reqParams];
    NSData *reqData=[NSData dataWithBytes:[reqParams UTF8String] length:[reqParams length]];
    [urlRequest setHTTPBody:reqData];
  }
  
  /// send request
  NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
    NSInteger status = httpResponse.statusCode;
    //[[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"[Network] Response [%@]", httpResponse];
    
    NSDictionary *allHeaderFields = httpResponse.allHeaderFields;
    PABaseResponse *apiResponse = [[PABaseResponse alloc]initWithHeaders:allHeaderFields statusCode:status outputData:data];
    callback(error, apiResponse);
  }];
  
  [postDataTask resume];
  return postDataTask;
}

- (void)addDataTask:(NSURLSessionDataTask *)task {
  if (task) {
    [_taskList addObject:task];
  }
}

- (void)removeDataTask:(NSURLSessionDataTask *)task {
  if (task) {
    [_taskList removeObject:task];
  }
}

- (void)cancelDataTask:(NSURLSessionDataTask *)task {
  [task cancel];
}

- (void)cancelAllRequests {
  for (NSURLSessionDataTask *task in _taskList) {
    [task cancel];
  }
}

@end
