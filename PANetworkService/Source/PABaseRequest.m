//
//  PABaseRequest.m
//  PANetworkService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <PAFileSystemService/PAFileSystemService.h>
#import "PABaseRequest.h"

NSString *const kPABRMethodNamePost = @"POST";
NSString *const kPABRMethodNameHead = @"HEAD";
NSString *const kPABRMethodNameGet = @"GET";
NSString *const kPABRContentType = @"Content-Type";
NSString *const kPABRContentTypeFormUrlEncoded = @"application/x-www-form-urlencoded";

@implementation PABaseRequest

- (NSString *)baseUrl {
  PAFileSystemManager *manager = [[PAFileSystemManager alloc]init];
  return [manager serverBasePath];
}

- (NSString *)path {
  return nil;
}

- (NSString *)encoding {
  return nil;
}

- (NSString *)method {
  return kPABRMethodNamePost;
}

- (NSDictionary *)headers {
  NSMutableDictionary *headerDict = [[NSMutableDictionary alloc]init];
  [headerDict setValue:@"application/json" forKey:kPABRContentType];
  [headerDict setValue:@"application/json" forKey:@"Accept"];
  return [NSDictionary dictionaryWithDictionary:headerDict];
}

- (NSDictionary *)parameters {
  return nil;
}

- (NSDictionary *)body {
  return nil;
}


@end
