//
//  PACertificate.h
//  Common
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PACertificate : NSObject

@property (nonatomic, readonly) NSString *certificateDataString;
@property (nonatomic, readonly) NSString *commonName;
@property (nonatomic, readonly) NSString *signature;
@property (nonatomic, readonly) NSString *issuer;
@property (nonatomic, readonly) NSString *issuerCommonName;
@property (nonatomic, readonly) BOOL isExpired;

- (instancetype)initWithPath:(NSString *)certPath;
- (instancetype)initWithCertificateData:(NSString *)certDataStr;
- (instancetype)initWithCertificateRef:(SecCertificateRef)certificateRef;

@end
