//
//  PACertificateUtility.m
//  Common
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <PAFileSystemService/PAFileSystemService.h>
#import "PACertificateUtility.h"
#import "NSString+Additions.h"
#import "PACertificate.h"

@implementation PACertificateUtility

+ (BOOL)doesSignatureMatchWithSyncCertificate:(NSString *)signatureToCompare {
  PAFileSystemManager *fsManager = [[PAFileSystemManager alloc] init];
  PACertificate *certificate = [[PACertificate alloc]initWithPath:[fsManager syncCertificatePath]];
  
  if (![signatureToCompare isEmptyOrHasOnlyWhiteSpaces] &&
      ![certificate.signature isEmptyOrHasOnlyWhiteSpaces]) {
    return ([signatureToCompare isEqualToString:certificate.signature]);
  }
  return false;
}

+ (BOOL)doesCertificateMatchesWithSyncCertificate:(NSString *)certifcateDataStr {
  PAFileSystemManager *fsManager = [[PAFileSystemManager alloc] init];
  
  PACertificate *syncCertificate = [[PACertificate alloc]initWithPath:[fsManager syncCertificatePath]];
  
  PACertificate *manifestCertificate = [[PACertificate alloc]initWithCertificateData:certifcateDataStr];
  if (manifestCertificate && syncCertificate) {
    if (manifestCertificate.commonName && syncCertificate.commonName) {
      return [manifestCertificate.commonName isEqualToString:syncCertificate.commonName];
    }
  }
  
  return false;
}
@end
