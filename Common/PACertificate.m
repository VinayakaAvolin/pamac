//
//  PACertificate.m
//  Common
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Security/Security.h>

#import "PACertificate.h"
#import "NSDate+Additions.h"

@interface PACertificate () {
  NSString *_certificatePath;
  NSDate *_expiryDate;
  SecCertificateRef _certificateRef;
}

@end

@implementation PACertificate

- (instancetype)initWithPath:(NSString *)certPath {
  self = [super init];
  if (self) {
    _certificatePath = certPath;
    [self getCertificateData];
  }
  return self;
}

- (instancetype)initWithCertificateData:(NSString *)certDataStr {
  self = [super init];
  if (self) {
    _certificateDataString = certDataStr;
    [self getCertificateData];
  }
  return self;
}
- (instancetype)initWithCertificateRef:(SecCertificateRef)certificateRef {
  self = [super init];
  if (self) {
    _certificateRef =  (SecCertificateRef)CFRetain(certificateRef);
    [self getCertificateData];
  }
  return self;
}

- (void)getCertificateData {
  SecCertificateRef certificateRef = NULL;
  if (nil != _certificatePath) {
    certificateRef = [self getCertificateFromPath];
  } else  if (nil != _certificateDataString) {
    certificateRef = [self getCertificateFromData];
  } else if (_certificateRef != NULL) {
    certificateRef = _certificateRef;
  }
  [self getDetailsFromCertificate:certificateRef];
}

- (void)getDetailsFromCertificate:(SecCertificateRef)certificateRef {
  if (NULL != certificateRef) {
    CFErrorRef error;
    // Read signature
    const void *keys[] = { kSecOIDX509V1Signature, kSecOIDX509V1IssuerName, kSecOIDInvalidityDate };
    CFArrayRef keySelection = CFArrayCreate(kCFAllocatorDefault, keys , sizeof(keys)/sizeof(keys[0]), &kCFTypeArrayCallBacks);
    CFDictionaryRef vals = SecCertificateCopyValues(certificateRef,  keySelection, &error);
    /// read certificate common name
    CFStringRef commonNameRef;
    OSStatus status;
    if ((status=SecCertificateCopyCommonName(certificateRef, &commonNameRef)) == errSecSuccess) {
      _commonName = (__bridge NSString *)commonNameRef;
    };
    if (NULL != commonNameRef) {
      CFRelease(commonNameRef);
    }
    
    if (NULL != vals) {
      for(int index = 0; index < sizeof(keys)/sizeof(keys[0]); index++) {
        CFDictionaryRef dict = CFDictionaryGetValue(vals, keys[index]);
        if (dict == NULL) {
          continue;
        }
        CFTypeRef values = CFDictionaryGetValue(dict, kSecPropertyKeyValue);
        if (values == NULL) {
          continue;
        }
        // Signature is of type CFData
        if (CFGetTypeID(values) == CFDataGetTypeID()) {
          // Get base 64 encoded signature
          _signature = [(__bridge NSData *)values base64EncodedStringWithOptions:0];
        } else if (CFGetTypeID(values) == CFArrayGetTypeID()) { // issuer is of type array
          _issuer = [NSString stringWithFormat:@"%@\n\n", [self stringFromDNwithSubjectName:values]];
        }  else if (CFGetTypeID(values) == CFDateGetTypeID()) { // validity date is of type CFDate Eg: UTC date 2027-02-01 22:12:15 +0000
          _expiryDate = (__bridge NSDate *)values;
        }
        
      }
      CFRelease(vals);
    }
    CFRelease(certificateRef);
  }
  
  return ;
}

- (SecCertificateRef)getCertificateFromPath {
  if (nil != _certificatePath) {
    NSData *certData = [NSData dataWithContentsOfFile:_certificatePath];
    if (nil != certData) {
      _certificateDataString = [certData base64EncodedStringWithOptions:0];
      
      SecCertificateRef certificateRef = SecCertificateCreateWithData(kCFAllocatorDefault, (__bridge CFDataRef)certData);
      return certificateRef;
    }
  }
  
  return NULL;
}
- (SecCertificateRef)getCertificateFromData {
  if (nil != _certificateDataString) {
    NSData *certData = [[NSData alloc] initWithBase64EncodedString:_certificateDataString options:0];
    if (nil != certData) {
      SecCertificateRef certificateRef = SecCertificateCreateWithData(kCFAllocatorDefault, (__bridge CFDataRef)certData);
      return certificateRef;
    }
  }
  
  return NULL;
}

- (NSString *)stringFromDNwithSubjectName:(CFArrayRef)array {
  NSMutableString * out = [[NSMutableString alloc] init];
  const void *keys[] = { kSecOIDCommonName, kSecOIDEmailAddress, kSecOIDOrganizationalUnitName, kSecOIDOrganizationName, kSecOIDLocalityName, kSecOIDStateProvinceName, kSecOIDCountryName };
  const void *labels[] = { "CN", "E", "OU", "O", "L", "S", "C", "E" };
  
  for(int i = 0; i < sizeof(keys)/sizeof(keys[0]);  i++) {
    for (CFIndex n = 0 ; n < CFArrayGetCount(array); n++) {
      CFDictionaryRef dict = CFArrayGetValueAtIndex(array, n);
      if (CFGetTypeID(dict) != CFDictionaryGetTypeID())
        continue;
      CFTypeRef dictkey = CFDictionaryGetValue(dict, kSecPropertyKeyLabel);
      if (!CFEqual(dictkey, keys[i]))
        continue;
      CFStringRef str = (CFStringRef) CFDictionaryGetValue(dict, kSecPropertyKeyValue);
      [out appendFormat:@"%s=%@ ", labels[i], (__bridge NSString*)str];
      if (CFEqual(dictkey, kSecOIDCommonName)) {
        _issuerCommonName = (__bridge NSString*)str;
      }
    }
  }
  return [NSString stringWithString:out];
}

- (BOOL)isExpired {
  if (!_expiryDate) {
    return true;
  }
  NSDate *currentDate = [NSDate systemDate];
  // is past date; then certificate is expired
  return ([currentDate compare:_expiryDate] == NSOrderedDescending);
}
@end
