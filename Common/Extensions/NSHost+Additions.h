//
//  NSHost+Additions.h
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSHost (Additions)

+ (NSString*)computerName;
+ (NSString *)ipAddress;
+ (NSString *)userName;
+ (NSString*)macId ;

@end
