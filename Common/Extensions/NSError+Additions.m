//
//  NSError+Additions.m
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "NSError+Additions.h"

@implementation NSError (Additions)

+ (NSError *)errorWithMessage:(NSString *)message code:(NSInteger)code {
  return [NSError errorWithMessage:message code:code info:nil];
}
+ (NSError *)errorWithMessage:(NSString *)message code:(NSInteger)code info:(NSDictionary *)info {
  NSString *errMsg = message;
  if (!message) {
    errMsg = @"Unknown error";
  }
  NSMutableDictionary *dict = [NSMutableDictionary dictionary];
  if (info) {
    [dict addEntriesFromDictionary:info];
  }
  [dict setValue:errMsg forKey:@"PAMessage"];
  return [NSError errorWithDomain:NSCocoaErrorDomain code:code userInfo:dict];
}
@end
