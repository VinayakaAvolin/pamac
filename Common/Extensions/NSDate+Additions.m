//
//  NSDate+Additions.m
//  Common
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "NSDate+Additions.h"

NSString* const kPADateFormat1 = @"dd MMM yyyy";
NSString* const kPADateFormat2 = @"dd/MM/yyyy";
NSString* const kPADateFormat3 = @"dd MMM yyyy hh:mm a";
NSString* const kPADateFormat4 = @"hh:mm a";
NSString* const kPADateFormat5 = @"HH:mm:ss";
NSString* const kPADateFormat6 = @"MMM d HH:mm:ss";
NSString* const kPADateFormat7 = @"dd-MMM-yyyy_HH_mm_ss";
NSString* const kPADateFormat8 = @"dd-MMM-yyyy, hh:mm:ss a";
NSString* const kPADateFormat9 = @"MM/dd/yyyy hh:mm:ss a"; //10/28/2017 12:01:01 AM; real time alert date format
NSString* const kPADateFormat10 = @"yyyy-MM-dd'T'hh:mm:ss.SSSSSSSZZZZ"; // optimization result this format is used
NSString* const kPADateFormat11 = @"yyyy-MM-dd_HH-mm-ss"; // "optimizationsinfo-2017-10-28_16-05-30.xml" file name requires this format

@implementation NSDate (Additions)

+ (NSDate *)systemDate {
  return [NSDate date];
}

- (NSString *)dateStringWithFormat:(PADateFormat)format {
  NSDateFormatter *dateFormatter = [NSDate dateFormatterFor:format];
  return [dateFormatter stringFromDate:self];
}

+ (NSDate *)dateFromString:(NSString*)dateString
                    format:(PADateFormat)format
{
  NSDateFormatter *dateFormatter = [NSDate dateFormatterFor:format];
  NSDate *dateFromString = [dateFormatter dateFromString:dateString];
  return dateFromString;
}

+ (NSDateFormatter *)dateFormatterFor:(PADateFormat)format {
  NSString *datePattern;
  
  switch (format) {
    case Format1:
      datePattern = kPADateFormat1;
      break;
    case Format2:
      datePattern = kPADateFormat2;
      break;
    case Format3:
      datePattern = kPADateFormat3;
      break;
    case Format4:
      datePattern = kPADateFormat4;
      break;
    case Format5:
      datePattern = kPADateFormat5;
      break;
    case Format6:
      datePattern = kPADateFormat6;
      break;
    case Format7:
      datePattern = kPADateFormat7;
      break;
    case Format8:
      datePattern = kPADateFormat8;
      break;
    case Format9:
      datePattern = kPADateFormat9;
      break;
    case Format10:
      datePattern = kPADateFormat10;
      break;
    case Format11:
      datePattern = kPADateFormat11;
      break;
    default:
      break;
  }
  if (nil != datePattern) {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = datePattern;
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    return formatter;
  }
  
  return nil;
}
+ (NSDate *)dateAtBeginningOfDayForDate:(NSDate *)inputDate {
  // Use the user's current calendar and time zone
  NSCalendar *calendar = [NSCalendar currentCalendar];
  NSTimeZone *timeZone = [NSTimeZone systemTimeZone];
  [calendar setTimeZone:timeZone];
  
  // Selectively convert the date components (year, month, day) of the input date
  NSDateComponents *dateComps = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:inputDate];
  
  // Set the time components manually
  [dateComps setHour:0];
  [dateComps setMinute:0];
  [dateComps setSecond:0];
  
  // Convert back
  NSDate *beginningOfDay = [calendar dateFromComponents:dateComps];
  return beginningOfDay;
}
+ (NSDate*)dateByAddingDateComponentsHour:(NSInteger)hour
                                   minute:(NSInteger)minute
                                   toDate:(NSDate*)toDate {
  return [NSDate dateByAddingDateComponentsHour:hour minute:minute day:-1 month:-1 toDate:toDate];
}
+ (NSDate*)dateByAddingDateComponentsHour:(NSInteger)hour
                                   minute:(NSInteger)minute
                                      day:(NSInteger)day
                                   toDate:(NSDate*)toDate {
  return [NSDate dateByAddingDateComponentsHour:hour minute:minute day:day month:-1 toDate:toDate];
}
+ (NSDate*)dateByAddingDateComponentsHour:(NSInteger)hour
                                   minute:(NSInteger)minute
                                      day:(NSInteger)day
                                    month:(NSInteger)month
                                   toDate:(NSDate*)toDate {
  NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
  if (hour > 0) {
    [dateComponents setHour:hour];
  }
  if (minute > 0) {
    [dateComponents setMinute:minute];
  }
  if (day >= 0) {
    [dateComponents setWeekday:day];
  }
  if (month > 0) {
    [dateComponents setMonth:month];
  }
  NSDate *dateFromDateComponents = [[NSCalendar currentCalendar]
                                    dateByAddingComponents:dateComponents
                                    toDate:toDate options:0];
  
  return dateFromDateComponents;
}

- (NSInteger)dayOfWeek {
  NSCalendar *gregorian = [[NSCalendar alloc]
                           initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
  NSDateComponents *weekdayComponents =
  [gregorian components:(NSCalendarUnitDay |
                         NSCalendarUnitWeekday) fromDate:self];
  NSInteger weekday = [weekdayComponents weekday];
  return weekday;
}

- (NSInteger)dayOfMonth {
  NSCalendar *gregorian = [[NSCalendar alloc]
                           initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
  NSDateComponents *weekdayComponents =
  [gregorian components:(NSCalendarUnitDay |
                         NSCalendarUnitWeekday) fromDate:self];
  NSInteger day = [weekdayComponents day];
  return day;
}
+ (NSDate*)nextMonthDateFromDateComponentsHour:(NSInteger)hour
                                        minute:(NSInteger)minute
                                           day:(NSInteger)day {
  NSDate *currentDate = [self systemDate];
  NSCalendar *gregorian = [[NSCalendar alloc]
                           initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
  NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
  if (hour > 0) {
    [dateComponents setHour:hour];
  }
  if (minute > 0) {
    [dateComponents setMinute:minute];
  }
  if (day >= 0) {
    [dateComponents setDay:day];
  }
  
  NSDateComponents *currentDayComponents =
  [gregorian components:(NSCalendarUnitDay |
                         NSCalendarUnitWeekday | NSCalendarUnitYear | NSCalendarUnitMonth) fromDate:currentDate];
  [dateComponents setYear:currentDayComponents.year];
  [dateComponents setMonth:(currentDayComponents.month+1)];
  
  return [gregorian dateFromComponents:dateComponents];
}
+ (NSDate*)currentMonthDateFromDateComponentsHour:(NSInteger)hour
                                           minute:(NSInteger)minute
                                              day:(NSInteger)day {
  NSDate *currentDate = [self systemDate];
  NSCalendar *gregorian = [[NSCalendar alloc]
                           initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
  NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
  if (hour > 0) {
    [dateComponents setHour:hour];
  }
  if (minute > 0) {
    [dateComponents setMinute:minute];
  }
  if (day >= 0) {
    [dateComponents setDay:day];
  }
  
  NSDateComponents *currentDayComponents =
  [gregorian components:(NSCalendarUnitDay |
                         NSCalendarUnitWeekday | NSCalendarUnitYear | NSCalendarUnitMonth) fromDate:currentDate];
  [dateComponents setYear:currentDayComponents.year];
  [dateComponents setMonth:currentDayComponents.month];
  
  return [gregorian dateFromComponents:dateComponents];
}

@end
