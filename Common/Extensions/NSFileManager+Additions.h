//
//  NSFileManager+Additions.h
//  Common
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSFileManager (Additions)

+ (BOOL)doesFileExistAtPath:(NSString *)filePath;

@end
