//
//  NSString+Additions.h
//  Common
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
  ReadAccess = R_OK,
  WriteAccess = W_OK,
  ExecuteAccess = X_OK,
  PathExists = F_OK
} PAAccessKind;

@interface NSString (Additions)

+ (NSString *)directoryPathFor:(NSSearchPathDirectory)searchPath
                        domain:(NSSearchPathDomainMask)domain;

/// Returns unique id
+ (NSString *)uuid;

////Get Instance UUID
+ (NSString *)getInstanceUUID;

- (BOOL)doesMatchRegularExpression:(NSString *)regex;
- (BOOL)isEmptyOrHasOnlyWhiteSpaces;
- (NSString *)stringByRemovingSubString:(NSString *)subStringToRemove;
- (NSString*)stringByConvertingIntoMacCompatibleFilePath;
- (NSString *)subStringBetweenTags:(NSString *)fromTag and:(NSString *)toTag trimNewLine:(BOOL)trim;
- (NSString *)subStringFromTag:(NSString *)fromTag;
- (NSString *)subStringFromTag:(NSString *)fromTag
              needToExcludeTag:(BOOL)exclude;
- (NSString *)md5String;
- (NSString *)sha2HashStringWithSalt:(NSString *)salt;
- (BOOL)isPathExecutable;

@end
