//
//  NSArray+Additions.h
//  Common
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (Additions)

- (NSArray *)removeObjectsInArray:(NSArray *)otherArray;

- (NSArray *)commonObjectsInArray:(NSArray *)otherArray;

+ (BOOL)doesArray:(NSArray *)firstArray hasCommonObjectsIn:(NSArray *)secondArray;

- (NSArray *)arrayAfterRemovingDuplicates;
+ (NSArray *)arrayByRemovingCommonObjectsIn:(NSArray *)firstArray from:(NSArray *)secondArray;

@end
