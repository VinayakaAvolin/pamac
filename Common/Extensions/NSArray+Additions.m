//
//  NSArray+Additions.m
//  Common
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "NSArray+Additions.h"

@implementation NSArray (Additions)

- (NSArray *)removeObjectsInArray:(NSArray *)otherArray {
  //Create NSSet from Array
  NSSet* otherSet = [NSSet setWithArray:otherArray];
  NSSet* currentSet = [NSSet setWithArray:self];
  
  // retrieve the fileName of the objects in otherSet
  NSSet* otherSetNames = [otherSet valueForKey:@"fileName"];
  // only keep the objects of currentSet whose 'fileName' are not in otherSetNames
  NSSet* currentMinusOtherSet = [currentSet filteredSetUsingPredicate:
                                 [NSPredicate predicateWithFormat:@"NOT fileName IN %@", otherSetNames]];
  
  //Now convert back to Array from sets
  return [currentMinusOtherSet allObjects];
}

- (NSArray *)commonObjectsInArray:(NSArray *)otherArray {
  //Create NSSet from Array
  NSSet* otherSet = [NSSet setWithArray:otherArray];
  NSSet* currentSet = [NSSet setWithArray:self];
  
  // retrieve the fileName of the objects in otherSet
  NSSet* otherSetNames = [otherSet valueForKey:@"fileName"];
  // only keep the objects of currentSet whose 'fileName' are matching with otherSetNames
  NSSet* commonInCurrentAndOtherSet = [currentSet filteredSetUsingPredicate:
                                       [NSPredicate predicateWithFormat:@"fileName IN %@", otherSetNames]];
  
  //Now convert back to Array from sets
  return [commonInCurrentAndOtherSet allObjects];
}

+ (BOOL)doesArray:(NSArray *)firstArray hasCommonObjectsIn:(NSArray *)secondArray {
  NSMutableSet *set1 = [NSMutableSet setWithArray: firstArray];
  NSSet *set2 = [NSSet setWithArray: secondArray];
  [set1 intersectSet: set2];
  NSArray *resultArray = [set1 allObjects];
  return (resultArray.count);
}

+ (NSArray *)arrayByRemovingCommonObjectsIn:(NSArray *)firstArray from:(NSArray *)secondArray {
  NSMutableSet *set1 = [NSMutableSet setWithArray: firstArray];
  NSMutableSet *set2 = [NSMutableSet setWithArray: secondArray];
  [set2 minusSet:set1];
  NSArray *resultArray = [set2 allObjects];
  return resultArray;
}

- (NSArray *)arrayAfterRemovingDuplicates {
  if (!self.count) {
    return self;
  }
  NSOrderedSet *orderedSet = [NSOrderedSet orderedSetWithArray:self];
  NSArray *arrayWithoutDuplicates = [orderedSet array];
  return arrayWithoutDuplicates;
}
@end
