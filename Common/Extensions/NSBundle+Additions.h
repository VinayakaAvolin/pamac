//
//  NSBundle+Additions.h
//  Common
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSBundle (Additions)

+ (id)loadBundleWithPath:(NSString *)bundlePath;

@end
