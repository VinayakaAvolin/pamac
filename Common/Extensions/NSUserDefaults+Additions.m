//
//  NSUserDefaults+Additions.m
//  Common
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <AppKit/AppKit.h>
#import "NSUserDefaults+Additions.h"

NSString *const kTileData = @"tile-data";

NSString *const kDockDomain = @"com.apple.dock";
NSString *const kPersistentApps = @"persistent-apps";
NSString *const kFileData = @"file-data";

NSString *const kPAIsPrivilegedToolInstalled = @"IsPrivilegedToolInstalled";

@implementation NSUserDefaults (Additions)

- (BOOL)addApplicationToDock:(NSString *_Nonnull)path {
  NSDictionary *domain = [self persistentDomainForName:@"com.apple.dock"];
  NSArray *apps = [domain objectForKey:kPersistentApps];
  NSArray *matchingApps = [apps filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"%K CONTAINS %@", @"tile-data.file-data._CFURLString", path]];
  if ([matchingApps count] == 0) {
    NSMutableDictionary *newDomain = [domain mutableCopy];
    NSMutableArray *newApps = [apps mutableCopy];
    NSDictionary *app = [NSDictionary dictionaryWithObject:[NSDictionary dictionaryWithObject:[NSDictionary dictionaryWithObjectsAndKeys:path, @"_CFURLString", [NSNumber numberWithInt:0], @"_CFURLStringType", nil] forKey:kFileData] forKey:kTileData];
    [newApps addObject:app];
    [newDomain setObject:newApps forKey:kPersistentApps];
    [self setPersistentDomain:newDomain forName:kDockDomain];
    return [self synchronize];
  }
  return NO;
}

- (BOOL)removeApplicationFromDock:(NSString *_Nonnull)name {
  NSDictionary *domain = [self persistentDomainForName:kDockDomain];
  NSArray *apps = [domain objectForKey:kPersistentApps];
  NSArray *newApps = [apps filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"not %K CONTAINS %@", @"tile-data.file-data._CFURLString", name]];
  if (![apps isEqualToArray:newApps]) {
    NSMutableDictionary *newDomain = [domain mutableCopy];
    [newDomain setObject:newApps forKey:kPersistentApps];
    [self setPersistentDomain:newDomain forName:kDockDomain];
    return [self synchronize];
  }
  return NO;
}


+ (void)setBool:(BOOL)value forKey:(NSString *_Nonnull)defaultName {
  NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
  [userDefaults setBool:value forKey:defaultName];
  [userDefaults synchronize];
}
+ (void)setObject:(nullable id)value forKey:(NSString *_Nonnull)defaultName {
  NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
  [userDefaults setValue:value forKey:defaultName];
  [userDefaults synchronize];
}
+ (void)setInteger:(NSInteger)value forKey:(NSString *_Nonnull)defaultName {
  NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
  [userDefaults setInteger:value forKey:defaultName];
  [userDefaults synchronize];
}
+ (BOOL)boolForKey:(NSString *)defaultName {
  NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
  return [userDefaults boolForKey:defaultName];
}
@end
