//
//  NSProcessInfo+Additions.m
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "NSProcessInfo+Additions.h"

@implementation NSProcessInfo (Additions)

+ (NSString *)osCodeName {
  NSString *codeName = @"Mac OS X";
  NSOperatingSystemVersion osVersion = [[NSProcessInfo processInfo] operatingSystemVersion];
  switch (osVersion.minorVersion) {
    case 10:
      codeName = @"Yosemite";
      break;
    case 11:
      codeName = @"El Capitan";
      break;
    case 12:
      codeName = @"Sierra";
      break;
    case 13:
      codeName = @"High Sierra";
      break;
    case 14:
      codeName = @"Mojave";
      break;
    default:
      break;
  }
  return codeName;
}

+ (NSString *)osVersion {
  NSOperatingSystemVersion osVersion = [[NSProcessInfo processInfo] operatingSystemVersion];
  NSString *version = [NSString stringWithFormat:@"%ld.%ld.%ld",osVersion.majorVersion,osVersion.minorVersion,osVersion.patchVersion];
  return version;
}

@end
