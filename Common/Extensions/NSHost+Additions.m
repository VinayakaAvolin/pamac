//
//  NSHost+Additions.m
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//


#include <IOKit/IOKitLib.h>
#include <ifaddrs.h>
#include <arpa/inet.h>
#import "NSHost+Additions.h"

NSString *const kPAOPDotLocalKey = @".local";

@implementation NSHost (Additions)

+ (NSString*)computerName {
  NSString *hostName = [[NSHost currentHost] localizedName];
  if(nil != hostName)
  {
    NSRange localRange = [hostName rangeOfString:kPAOPDotLocalKey];
    if(nil != hostName && NSNotFound != localRange.location )
    {
      hostName = [hostName substringToIndex:localRange.location];
    }
  }
  return hostName;
}

+ (NSString *)ipAddress {
  NSString *address = nil;
  struct ifaddrs *interfaces = NULL;
  struct ifaddrs *temp_addr = NULL;
  int success = 0;
  // retrieve the current interfaces - returns 0 on success
  success = getifaddrs(&interfaces);
  if (success == 0) {
    // Loop through linked list of interfaces
    temp_addr = interfaces;
    while(temp_addr != NULL) {
      if(temp_addr->ifa_addr->sa_family == AF_INET) {
        // Check if interface is en0 which is the wifi connection
        if([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"en0"]) {
          // Get NSString from C String
          address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
        }
      }
      temp_addr = temp_addr->ifa_next;
    }
  }
  // Free memory
  freeifaddrs(interfaces);
  
  return address;
}

+ (NSString *)userName {
  return NSUserName();
}

+ (NSString*) macId {
  io_service_t platformExpert = IOServiceGetMatchingService(kIOMasterPortDefault,IOServiceMatching("IOPlatformExpertDevice"));
  if (!platformExpert)
    return nil;
  
  CFTypeRef serialNumberAsCFString = IORegistryEntryCreateCFProperty(platformExpert,CFSTR(kIOPlatformUUIDKey),kCFAllocatorDefault, 0);
  if (!serialNumberAsCFString)
    return nil;
  
  IOObjectRelease(platformExpert);
  return (__bridge NSString *)(serialNumberAsCFString);;
}

@end
