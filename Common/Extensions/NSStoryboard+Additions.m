//
//  NSStoryboard+Additions.m
//  Common
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "NSStoryboard+Additions.h"

NSString *const kPAMainStoryBoardName = @"Main";

@implementation NSStoryboard (Additions)

+ (NSStoryboard *)mainAppStoryboard {
  return [NSStoryboard storyboardWithName:kPAMainStoryBoardName];
}

+ (NSStoryboard *)storyboardWithName:(NSString *)name {
  return [NSStoryboard storyboardWithName:name bundle:nil];
}

@end
