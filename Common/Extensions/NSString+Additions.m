//
//  NSString+Additions.m
//  Common
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <CommonCrypto/CommonDigest.h>
#import "NSString+Additions.h"
#import <CommonCrypto/CommonHMAC.h>

@implementation NSString (Additions)

+ (NSString *)directoryPathFor:(NSSearchPathDirectory)searchPath
                        domain:(NSSearchPathDomainMask)domain {
  NSArray *paths = NSSearchPathForDirectoriesInDomains(searchPath, domain, YES);
  NSString *targetDirectory = [paths objectAtIndex:0];
  return targetDirectory;
}

+ (NSString *)uuid {
  return [[NSUUID UUID] UUIDString];
}

- (BOOL)doesMatchRegularExpression:(NSString *)regex {
  if ([self isEmptyOrHasOnlyWhiteSpaces] || [regex isEmptyOrHasOnlyWhiteSpaces]) {
    return NO;
  }
  NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
  return [predicate evaluateWithObject:self];
}

- (BOOL)isEmptyOrHasOnlyWhiteSpaces {
  if (self == nil) {
    return true;
  }
  BOOL isEmpty = (self.length == 0);
  
  if (!isEmpty) {
    NSString *trimmedString = [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    isEmpty = (trimmedString.length == 0);
  }
  
  return isEmpty;
}

- (NSString *)trimNewLine {
  BOOL isEmpty = (self.length == 0);
  
  if (!isEmpty) {
    NSString *trimmedString = [self stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    return trimmedString;
  }
  
  return self;
}
- (NSString *)stringByRemovingSubString:(NSString *)subStringToRemove {
  BOOL isSourceEmpty = [self isEmptyOrHasOnlyWhiteSpaces];
  BOOL isSubStringEmpty = [subStringToRemove isEmptyOrHasOnlyWhiteSpaces];
  
  if (isSourceEmpty || isSubStringEmpty) {
    return self;
  }
  NSString *finalString = [self stringByReplacingOccurrencesOfString:subStringToRemove withString:@""];
  return finalString;
}

- (NSString*)stringByConvertingIntoMacCompatibleFilePath {
  NSString *finalString = [self stringByReplacingOccurrencesOfString:@"\\\\" withString:@"\\"];
  
  finalString = [finalString stringByReplacingOccurrencesOfString:@"\\" withString:@"/" ];
  finalString = [finalString stringByReplacingOccurrencesOfString:@"\n" withString:@"" ];
  finalString = [finalString stringByReplacingOccurrencesOfString:@"\t" withString:@"" ];
  finalString = [finalString stringByReplacingOccurrencesOfString:@"\\/\\" withString:@"/" ];
  finalString = [finalString stringByReplacingOccurrencesOfString:@"///" withString:@"/" ];
  finalString = [finalString stringByReplacingOccurrencesOfString:@"//" withString:@"/" ];
  return finalString;
}

- (NSString *)subStringBetweenTags:(NSString *)fromTag and:(NSString *)toTag trimNewLine:(BOOL)trim {
  if (nil == fromTag ||
      nil == toTag) {
    return @"";
  }
  NSRange fromTagRange = [self rangeOfString:fromTag];
  NSRange toTagRange = [self rangeOfString:toTag];
  
  if (NSNotFound == fromTagRange.location ||
      NSNotFound == toTagRange.location) {
    return @"";
  }
  
  NSInteger subStringLength = toTagRange.location - fromTagRange.location - fromTagRange.length;
  NSUInteger subStringLocation = fromTagRange.location + fromTagRange.length;
  NSRange subStringRange = NSMakeRange(subStringLocation, subStringLength);
  NSString *outPutStr = [self substringWithRange:subStringRange];
  return trim ? [outPutStr trimNewLine] : outPutStr;
}

- (NSString *)subStringFromTag:(NSString *)fromTag {
  return [self subStringFromTag:fromTag needToExcludeTag:false];
}

- (NSString *)subStringFromTag:(NSString *)fromTag
              needToExcludeTag:(BOOL)exclude {
  if (nil == fromTag) {
    return @"";
  }
  NSRange fromTagRange = [self rangeOfString:fromTag];
  
  if (NSNotFound == fromTagRange.location) {
    return @"";
  }
  NSInteger subStringLength = self.length - fromTagRange.location;
  NSUInteger subStringLocation = fromTagRange.location;
  // exlcue tag string
  if (exclude) {
    subStringLength = self.length - (fromTagRange.location + fromTag.length);
    subStringLocation = fromTagRange.location + fromTag.length;
  }
  NSRange subStringRange = NSMakeRange(subStringLocation, subStringLength);
  return [self substringWithRange:subStringRange];
}
- (NSString *)md5String {
  const char *cStr = [self UTF8String];
  unsigned char result[CC_MD5_DIGEST_LENGTH];
  CC_MD5( cStr, (CC_LONG)strlen(cStr), result );
  
  return [NSString stringWithFormat:
          @"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
          result[0], result[1], result[2], result[3],
          result[4], result[5], result[6], result[7],
          result[8], result[9], result[10], result[11],
          result[12], result[13], result[14], result[15]
          ];
}

-(NSString *)sha2HashStringWithSalt:(NSString *)salt {
  NSString *keySalt = salt;
  if (!salt) {
    keySalt = @"ProactiveAssist";
  }
  const char *cKey  = [keySalt cStringUsingEncoding:NSUTF8StringEncoding];
  const char *cData = [self cStringUsingEncoding:NSUTF8StringEncoding];
  unsigned char cHMAC[CC_SHA256_DIGEST_LENGTH];
  CCHmac(kCCHmacAlgSHA256, cKey, strlen(cKey), cData, strlen(cData), cHMAC);
  
  NSString *hash;
  
  NSMutableString* output = [NSMutableString stringWithCapacity:CC_SHA256_DIGEST_LENGTH * 2];
  
  for(int index = 0; index < CC_SHA256_DIGEST_LENGTH; index++) {
    [output appendFormat:@"%02x", cHMAC[index]];
  }
  hash = output;
  return hash;
}

- (BOOL)isPathExecutable {
  return access([self UTF8String], X_OK) == 0;
}

+(NSString *)getInstanceUUID {
  if([[NSUserDefaults standardUserDefaults] objectForKey:@"instanceUUID"] == nil) {
    NSString *_instanceUUID = [[NSUUID UUID] UUIDString];
    [[NSUserDefaults standardUserDefaults] setValue:_instanceUUID forKey:@"instanceUUID"];
    [[NSUserDefaults standardUserDefaults] synchronize];
  }
  return [[NSUserDefaults standardUserDefaults] objectForKey:@"instanceUUID"];
}

@end
