//
//  NSError+Additions.h
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSError (Additions)

+ (NSError *)errorWithMessage:(NSString *)message code:(NSInteger)code;
+ (NSError *)errorWithMessage:(NSString *)message code:(NSInteger)code  info:(NSDictionary *)info;

@end
