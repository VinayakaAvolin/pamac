//
//  NSStoryboard+Additions.h
//  Common
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AppKit/AppKit.h>

@interface NSStoryboard (Additions)

/**
 Returns main story board instance
 
 @return NSStoryboard*
 */
+ (NSStoryboard *)mainAppStoryboard;

@end
