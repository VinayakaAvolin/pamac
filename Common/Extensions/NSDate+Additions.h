//
//  NSDate+Additions.h
//  Common
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum : NSUInteger {
  Format1, /// hh:mm:ss.sss
  Format2,
  Format3,
  Format4,
  Format5,
  Format6,
  Format7,
  Format8,
  Format9,
  Format10,
  Format11
} PADateFormat;

@interface NSDate (Additions)

+ (NSDate *)systemDate;

- (NSString *)dateStringWithFormat:(PADateFormat)format;
+ (NSDate *)dateAtBeginningOfDayForDate:(NSDate *)inputDate;

+ (NSDate *)dateFromString:(NSString*)dateString
                    format:(PADateFormat)format;

+ (NSDate*)dateByAddingDateComponentsHour:(NSInteger)hour
                                   minute:(NSInteger)minute
                                   toDate:(NSDate*)toDate;

+ (NSDate*)dateByAddingDateComponentsHour:(NSInteger)hour
                                   minute:(NSInteger)minute
                                      day:(NSInteger)day
                                   toDate:(NSDate*)toDate;

+ (NSDate*)dateByAddingDateComponentsHour:(NSInteger)hour
                                   minute:(NSInteger)minute
                                      day:(NSInteger)day
                                    month:(NSInteger)month
                                   toDate:(NSDate*)toDate;
- (NSInteger)dayOfWeek;
- (NSInteger)dayOfMonth;
+ (NSDate*)nextMonthDateFromDateComponentsHour:(NSInteger)hour
                                        minute:(NSInteger)minute
                                           day:(NSInteger)day;
+ (NSDate*)currentMonthDateFromDateComponentsHour:(NSInteger)hour
                                           minute:(NSInteger)minute
                                              day:(NSInteger)day;

@end
