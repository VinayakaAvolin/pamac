//
//  NSFileManager+Additions.m
//  Common
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "NSFileManager+Additions.h"

@implementation NSFileManager (Additions)

+ (BOOL)doesFileExistAtPath:(NSString *)filePath {
  NSFileManager *fileManager = [NSFileManager defaultManager];
  return [fileManager fileExistsAtPath:filePath];
}

@end
