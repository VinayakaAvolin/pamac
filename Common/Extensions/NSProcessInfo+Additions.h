//
//  NSProcessInfo+Additions.h
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSProcessInfo (Additions)

+ (NSString *)osCodeName;
+ (NSString *)osVersion;

@end
