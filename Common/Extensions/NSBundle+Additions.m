//
//  NSBundle+Additions.m
//  Common
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "NSBundle+Additions.h"
#import "NSString+Additions.h"

@implementation NSBundle (Additions)

+ (id)loadBundleWithPath:(NSString *)bundlePath {
  NSBundle *bundle = [NSBundle bundleWithPath:bundlePath];
  if (bundle) {
    Class pClass = [bundle principalClass];
    if (pClass) {
      id instance = [[pClass alloc] init];
      return instance;
    }
  }
  return nil;
}
@end
