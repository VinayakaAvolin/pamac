//
//  NSUserDefaults+Additions.h
//  Common
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * _Nonnull const kPAIsPrivilegedToolInstalled;

@interface NSUserDefaults (Additions)

- (BOOL)addApplicationToDock:(NSString *_Nonnull)path;
- (BOOL)removeApplicationFromDock:(NSString *_Nonnull)name;

// Setters
+ (void)setBool:(BOOL)value forKey:(NSString *_Nonnull)defaultName;
+ (void)setObject:(nullable id)value forKey:(NSString *_Nonnull)defaultName;
+ (void)setInteger:(NSInteger)value forKey:(NSString *_Nonnull)defaultName;

// Getters
+ (BOOL)boolForKey:(NSString *_Nonnull)defaultName ;

@end
