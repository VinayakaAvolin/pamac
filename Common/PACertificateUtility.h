//
//  PACertificateUtility.h
//  Common
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PACertificateUtility : NSObject

+ (BOOL)doesSignatureMatchWithSyncCertificate:(NSString *)signatureToCompare;

+ (BOOL)doesCertificateMatchesWithSyncCertificate:(NSString *)certifcateDataStr;

@end
