//
//  PAMonitorServiceProvider.h
//  PABaseMonitorService
//
//  Copyright © 2018 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

//#import "PAMonitorServiceConstants.h"
#import "PAMonitorServiceDelegate.h"
#import <PAEventBus/PAEventBus.h>

@class PAMonitorServiceInfo;

typedef void(^PAMonitorServiceProviderResult)(PASelfHealEvent *event);

@interface PAMonitorServiceProvider : NSObject

@property(weak) id<PAMonitorServiceDelegate> delegate;

- (BOOL)canHandleCapability:(SelfHealEventClass)capability forEvent:(PASelfHealTriggerInfo*)eventTrigger;

- (void) startMonitoringEvent:(PASelfHealTriggerInfo*)eventTrigger;

- (void) startMonitoringEvent:(PASelfHealTriggerInfo*)eventTrigger withReply:(PAMonitorServiceProviderResult)reply;


- (void) stopMonitoringEvent:(PASelfHealTriggerInfo*)eventTrigger;

@end
