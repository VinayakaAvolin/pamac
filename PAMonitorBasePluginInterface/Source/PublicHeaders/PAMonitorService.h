//
//  PAMonitorService.h
//  PAMonitorService
//
//  Copyright © 2018 Avolin. All rights reserved.
//

#import <Cocoa/Cocoa.h>

//! Project version number for PAEventBus.
FOUNDATION_EXPORT double PAMonitorServiceVersionNumber;

//! Project version string for PAEventBus.
FOUNDATION_EXPORT const unsigned char PAMonitorServiceVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <PAMonitorService/PublicHeader.h>

#import <PAMonitorService/PAMonitorServiceProvider.h>
#import <PAMonitorService/PAMonitorServiceDelegate.h>
