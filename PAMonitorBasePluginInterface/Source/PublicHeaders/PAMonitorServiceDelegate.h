//
//  PAMonitorServiceDelegate.h
//  PAMonitorService
//
//  Copyright © 2018 Avolin. All rights reserved.
//

#ifndef PAMonitorServiceDelegate_h
#define PAMonitorServiceDelegate_h

@class PAMonitorServiceProvider;
@class PASelfHealTriggerInfo;
@protocol PAMonitorServiceDelegate
@required

- (void)serviceProvider:(PAMonitorServiceProvider *)serviceProvider eventOccurred:(PASelfHealTriggerInfo*)event;
@end
#endif /* PAMonitorServiceDelegate_h */
