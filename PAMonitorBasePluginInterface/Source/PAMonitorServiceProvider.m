//
//  PAMonitorServiceProvider.m
//  BatterySample
//
//  Copyright © 2018 Avolin. All rights reserved.
//

#import "PAMonitorServiceProvider.h"

@implementation PAMonitorServiceProvider

- (BOOL)canHandleCapability:(SelfHealEventClass)capability forEvent:(PASelfHealTriggerInfo*)event{
    return false;
}

- (void) startMonitoringEvent:(PASelfHealTriggerInfo*)event
{
    
}

- (void) stopMonitoringEvent:(PASelfHealTriggerInfo*)event
{
    
}

- (void) startMonitoringEvent:(PASelfHealTriggerInfo*)eventTrigger withReply:(PAMonitorServiceProviderResult)reply
{
    
}


@end
