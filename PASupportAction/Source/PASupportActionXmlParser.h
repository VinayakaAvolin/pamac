//
//  PASupportActionXmlParser.h
//  PASupportAction
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <PACoreServices/PACoreServices.h>
#import "PAServiceInfo.h"

typedef void(^PASupportActionXmlParserCompletionBlock)(BOOL success, PAServiceInfo *serviceInfo);

@interface PASupportActionXmlParser : PAXmlParser

@property (nonatomic, readonly) NSString *supportActionRootGuidPath;
@property (nonatomic, readonly) NSString *supportActionGuidFolderName;

- (instancetype)initWithServiceInfo:(PAServiceInfo *)serviceInfo;
- (void)parseInputConfigurationWithCompletion:(PASupportActionXmlParserCompletionBlock)completionBlock;

@end
