//
//  PASupportActionInfo.h
//  PASupportAction
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <PACoreServices/PACoreServices.h>

@interface PASupportActionInfo : NSObject

@property (nonatomic, readonly) NSString *testScriptPath;
@property (nonatomic, readonly) NSString *testScriptType;
@property (nonatomic, readonly) PAScriptType testScriptTypeEnum;

@property (nonatomic, readonly) NSString *mainScriptPath;
@property (nonatomic, readonly) NSString *mainScriptType;
@property (nonatomic, readonly) PAScriptType mainScriptTypeEnum;

- (instancetype)initWithMainScriptPath:(NSString *)mainScriptPath
                               andType:(NSString *)mainScriptType
                        testScriptPath:(NSString *)testScriptPath
                               andType:(NSString *)testScriptType;

+ (PAScriptType)scriptTypeEnumFor:(NSString *)scriptType;
@end
