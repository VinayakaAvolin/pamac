//
//  PASupportActionInputParameter.h
//  PASupportAction
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PASupportActionInputParameter : NSObject

@property (nonatomic, readonly) NSString *key;
@property (nonatomic, readonly) NSString *value;

- (instancetype)initWithKey:(NSString *)key
                      value:(NSString *)value;

@end
