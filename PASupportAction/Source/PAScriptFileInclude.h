//
//  PAScriptFileInclude.h
//  PASupportAction
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PAScriptFileInclude : NSObject

@property (nonatomic, readonly) NSString *language;
@property (nonatomic, readonly) NSString *source;

- (instancetype)initWithLanguage:(NSString *)language
                          source:(NSString *)source;


@end
