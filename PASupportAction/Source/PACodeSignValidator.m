//
//  PACodeSignValidator.m
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <PACoreServices/PACoreServices.h>
#import "PACodeSignValidator.h"
#import "PACertificate.h"

@interface PACodeSignValidator ()

@property(nonatomic) NSMutableArray *codeSignCertificateList;
@property(nonatomic) NSString *codeSignedItemPath;
@property(nonatomic) PAFileSystemManager *fsManager;
@property(nonatomic) NSArray *authorizedCnList;

@end

@implementation PACodeSignValidator

- (instancetype)initWithItemPath:(NSString *)itemPath
{
  self = [super init];
  if (self) {
    _fsManager = [[PAFileSystemManager alloc]init];
    
    if ([_fsManager doesFileExist:itemPath]) {
      _codeSignCertificateList = [[NSMutableArray alloc] init];
      _codeSignedItemPath = itemPath;
      _authorizedCnList = _fsManager.applicationConfigurations.authorizedCnList;
    } else {
      self = nil;
    }
  }
  return self;
}

+ (BOOL)validateCodeSigningOfItemAtPath:(NSString *)itemPath {
  BOOL doesCodeSignMatch = false;
  PACodeSignValidator *validator = [[PACodeSignValidator alloc] initWithItemPath:itemPath];
  
  [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"Validate code signing of [%@]",itemPath];
  //[[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"Authorized CNs [%@]",validator.authorizedCnList];
  
  if ([validator getCertificatesFromCodeSignedItem] && validator.codeSignCertificateList.count) {
    for (PACertificate *certificate in validator.codeSignCertificateList) {
      NSString *issuerName = certificate.issuerCommonName;
      [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"Code signed certificate issuer name [%@]",issuerName];
      if (issuerName) {
        doesCodeSignMatch = [validator.authorizedCnList containsObject:issuerName];
      }
      
      if (!doesCodeSignMatch) {
          NSString *commonName = certificate.commonName;
          doesCodeSignMatch = [validator.authorizedCnList containsObject:commonName];
      }
      if (doesCodeSignMatch) {
        break;
      }
    }
  }
  
  [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"Does code signing match [%ld]",doesCodeSignMatch];
  
  return doesCodeSignMatch;
}

- (BOOL)getCertificatesFromCodeSignedItem {
  OSStatus secError = noErr;
  // retrieve this process's code object
  SecStaticCodeRef myCode;
  NSURL *itemUrl = [NSURL fileURLWithPath:_codeSignedItemPath];
  if (!itemUrl) {
    return false;
  }
  secError = SecStaticCodeCreateWithPath(CFBridgingRetain(itemUrl), kSecCSDefaultFlags, &myCode);
  if (noErr != secError || myCode == NULL) {
    [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"unable to retrieve code object, security error %d", secError];
    return false;
  }
  
  //some basic information about the code signature
  CFDictionaryRef signingInfoRef = nil;
  BOOL success = false;
  secError = SecCodeCopySigningInformation(myCode, kSecCSSigningInformation, &signingInfoRef);
  if (noErr != secError) {
    if(secError == errSecCSSignatureFailed) {
      [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"invalid signature"];
    }
    else {
      [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"cannot get signing information, security error %d", secError];
    }
  }
  else {
    NSDictionary *signInfo =  (NSDictionary *)CFBridgingRelease(signingInfoRef);
    NSArray *certs = signInfo[@"certificates"];
    NSUInteger totalCerts = certs.count;
    for (int index = 0; index < totalCerts ; index++) {
      SecCertificateRef certRef = (SecCertificateRef)CFBridgingRetain((certs[index]));
      if (certRef != NULL) {
        PACertificate *certificate = [[PACertificate alloc] initWithCertificateRef:certRef];
        if (certificate) {
          [_codeSignCertificateList addObject:certificate];
        }
      }
    }
    success = true;
  }
  
  CFRelease(myCode);
  return success;
}
@end
