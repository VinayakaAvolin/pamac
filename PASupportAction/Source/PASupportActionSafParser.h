//
//  PASupportActionSafParser.h
//  PASupportAction
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <PACoreServices/PACoreServices.h>
#import "PASupportActionInfo.h"
#import "PAServiceInfo.h"

typedef void(^PASupportActionSafParserCompletionBlock)(BOOL success, PASupportActionInfo *info);

@interface PASupportActionSafParser : PAXmlParser

- (instancetype)initWithSaf:(NSString *)safPath andServiceInfo:(PAServiceInfo *)serviceInfo;
- (void)parseSafAndCreateScriptFilesWithCompletion:(PASupportActionSafParserCompletionBlock)completionBlock;

@end
