//
//  PASupportActionInfo.m
//  PASupportAction
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PASupportActionInfo.h"
#import "NSString+Additions.h"

@implementation PASupportActionInfo

- (instancetype)initWithMainScriptPath:(NSString *)mainScriptPath
                               andType:(NSString *)mainScriptType
                        testScriptPath:(NSString *)testScriptPath
                               andType:(NSString *)testScriptType {
  self = [super init];
  if (self) {
    if (!mainScriptPath || !mainScriptType ||
        !testScriptPath || !testScriptType) {
      self = nil;
    } else {
      _mainScriptPath = mainScriptPath;
      _mainScriptType = mainScriptType;
      _testScriptPath = testScriptPath;
      _testScriptType = testScriptType;
      _mainScriptTypeEnum = [PASupportActionInfo scriptTypeEnumFor:_mainScriptType];
      _testScriptTypeEnum = [PASupportActionInfo scriptTypeEnumFor:_testScriptType];
    }
  }
  return self;
}

+ (PAScriptType)scriptTypeEnumFor:(NSString *)scriptType {
  PAScriptType scriptTypeEnumValue = PAScriptTypeNone;
  if (![scriptType isEmptyOrHasOnlyWhiteSpaces]) {
    NSArray *scriptTypeArray = [[NSArray alloc] initWithObjects:kPAScriptTypeArray1];
    NSUInteger index = [scriptTypeArray indexOfObject:scriptType];
    if (NSNotFound != index) {
      scriptTypeEnumValue = (PAScriptType)index;
    }
  }
  return scriptTypeEnumValue;
}

@end
