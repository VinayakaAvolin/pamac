//
//  PASupportActionInputParameter.m
//  PASupportAction
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PASupportActionInputParameter.h"

@implementation PASupportActionInputParameter

- (instancetype)initWithKey:(NSString *)key
                      value:(NSString *)value {
  self = [super init];
  if (self) {
    if (!key || !value) {
      self = nil;
    } else {
      _key = key;
      _value = value;
    }
  }
  return self;
}
@end
