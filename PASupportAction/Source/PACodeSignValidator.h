//
//  PACodeSignValidator.h
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PACodeSignValidator : NSObject


+ (BOOL)validateCodeSigningOfItemAtPath:(NSString *)itemPath;

@end
