//
//  PAScriptFileInclude.m
//  PASupportAction
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PAScriptFileInclude.h"

@implementation PAScriptFileInclude

- (instancetype)initWithLanguage:(NSString *)language
                          source:(NSString *)source {
  self = [super init];
  if (self) {
    if (!language || !source) {
      self = nil;
    } else {
      _language = language;
      _source = source;
    }
  }
  return self;
}

@end
