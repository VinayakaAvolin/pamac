//
//  PASupportActionSafParser.m
//  PASupportAction
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PASupportActionSafParser.h"
#import "NSString+Additions.h"
#import "PASupportActionInputParameter.h"
#import "PAScriptFileInclude.h"

NSString *const kPASAInclude = @"include";
NSString *const kPASAScript = @"script";
NSString *const kPASAInput = @"input";
NSString *const kPASATest = @"test";
NSString *const kPASAFunctions = @"functions";
NSString *const kPASAPayload = @"payload";
NSString *const kPASASrc = @"_src";
NSString *const kPASAValueKey = @"_value";
NSString *const kPASATextKey = @"__text";
NSString *const kPASANameKey = @"_name";
NSString *const kPASALanguageKey = @"_language";
NSString *const kPASAScriptfile = @"scriptfile";
NSString *const kPASAParam = @"param";

NSString *const kPASAAppleScriptExtension = @"scpt";
NSString *const kPASAShellScriptExtension = @"sh";

NSString *const kPASATestScriptFileName = @"test";
NSString *const kPASAMainScriptFileName = @"main";

NSString *const kPASAPrivilegeToolPlaceholder = @"%exec_with_privilege%";

typedef void(^PASGetGuidCompletionBlock)(BOOL success, NSString *guidWithVersionFolderName);

@interface PASupportActionSafParser () {
  NSString *_safFilePath;
  NSString *_safRootFolderPath;
  PAFileSystemManager *_fsManager;
  PAServiceInfo *_serviceInfo;
}

@end

@implementation PASupportActionSafParser

- (instancetype)initWithSaf:(NSString *)safPath
             andServiceInfo:(PAServiceInfo *)serviceInfo {
  self = [super init];
  if (self && safPath) {
    _safFilePath = safPath;
    _serviceInfo = serviceInfo;
    _safRootFolderPath = [self safRootFolderPath];
    _fsManager = [[PAFileSystemManager alloc]init];
  } else {
    self = nil;
  }
  return self;
}


- (void)parseSafAndCreateScriptFilesWithCompletion:(PASupportActionSafParserCompletionBlock)completionBlock {
  BOOL success = false;
  if ([_fsManager doesFileExist:_safFilePath]) {
    NSDictionary *xmlDict = [self dictionaryFromXmlAtPath:_safFilePath];
    if (xmlDict) {
      NSString *mainScript = nil;
      NSString *mainScriptLanguage = nil;
      NSString *testScript = nil;
      NSString *testScriptLanguage = nil;
      NSString *functions = nil;
      NSMutableArray *paramList = [[NSMutableArray alloc]init];
      NSMutableArray *scriptFileIncludeList = [[NSMutableArray alloc]init];
      
      // get main script
      NSDictionary *mainScriptInfoDict = [xmlDict valueForKey:kPASAScript];
      if ([mainScriptInfoDict isKindOfClass:[NSDictionary class]]) {
        NSString *script = mainScriptInfoDict[kPASATextKey];
        NSString *scriptLanguage = mainScriptInfoDict[kPASALanguageKey];
        if ([script isKindOfClass:[NSString class]]) {
          mainScript = script;
        }
        if ([scriptLanguage isKindOfClass:[NSString class]]) {
          mainScriptLanguage = scriptLanguage;
        }
      }
      
      // get test script
      NSDictionary *testScriptInfoDict = [xmlDict valueForKey:kPASATest];
      if ([testScriptInfoDict isKindOfClass:[NSDictionary class]]) {
        NSString *script = testScriptInfoDict[kPASATextKey];
        NSString *scriptLanguage = testScriptInfoDict[kPASALanguageKey];
        if ([script isKindOfClass:[NSString class]]) {
          testScript = script;
        }
        if ([scriptLanguage isKindOfClass:[NSString class]]) {
          testScriptLanguage = scriptLanguage;
        }
      }
      
      // get functions
      NSDictionary *functionsInfoDict = [xmlDict valueForKey:kPASAFunctions];
      if ([functionsInfoDict isKindOfClass:[NSDictionary class]]) {
        NSString *script = functionsInfoDict[kPASATextKey];
        if ([script isKindOfClass:[NSString class]]) {
          functions = script;
        }
      }
      
      
      // get input params
      NSDictionary *inputDict = [xmlDict valueForKey:kPASAInput];
      if ([inputDict isKindOfClass:[NSDictionary class]]) {
        NSMutableArray *paramArray = [[NSMutableArray alloc]init];
        id inputParams = inputDict[kPASAParam];
        if ([inputParams isKindOfClass:[NSDictionary class]]) {
          [paramArray addObject:inputParams];
        } else if ([inputParams isKindOfClass:[NSArray class]]) {
          for (NSDictionary *dict in inputParams) {
            if ([dict isKindOfClass:[NSDictionary class]]) {
              [paramArray addObject:dict];
            }
          }
        }
        NSDictionary *parametersFromUi = _serviceInfo.inputParameters;
        
        for (NSDictionary *paramDict in paramArray) {
          NSString *key = paramDict[kPASANameKey];
          NSString *value = paramDict[kPASAValueKey];
          NSString *valueFromUi = [parametersFromUi valueForKey:key];
          if (valueFromUi && ![valueFromUi isEmptyOrHasOnlyWhiteSpaces]) {
            value = valueFromUi;
          }
          PASupportActionInputParameter *inputParam = [[PASupportActionInputParameter alloc] initWithKey:key value:value];
          if (inputParam) {
            [paramList addObject:inputParam];
          }
        }
      }
      // get script file includes
      NSDictionary *includeDict = [xmlDict valueForKey:kPASAInclude];
      if ([includeDict isKindOfClass:[NSDictionary class]]) {
        NSMutableArray *includesArray = [[NSMutableArray alloc]init];
        id includeInfo = includeDict[kPASAScriptfile];
        if ([includeInfo isKindOfClass:[NSDictionary class]]) {
          [includesArray addObject:includeInfo];
        } else if ([includeInfo isKindOfClass:[NSArray class]]) {
          for (NSDictionary *dict in includeInfo) {
            if ([dict isKindOfClass:[NSDictionary class]]) {
              [includesArray addObject:dict];
            }
          }
        }
        for (NSDictionary *scriptFileDict in includesArray) {
          PAScriptFileInclude *scriptFile = [[PAScriptFileInclude alloc] initWithLanguage:scriptFileDict[kPASALanguageKey] source:scriptFileDict[kPASASrc]];
          if (scriptFile) {
            [scriptFileIncludeList addObject:scriptFile];
          }
        }
      }
      
      //===========================================
      
      NSString *finalMainScript = mainScript;
      NSString *finalTestScript = testScript;
      
      if (functions && ![functions isEmptyOrHasOnlyWhiteSpaces] &&
          mainScript && ![mainScript isEmptyOrHasOnlyWhiteSpaces]) {
        finalMainScript = [NSString stringWithFormat:@"%@\n%@",functions,mainScript];
      }
      if (functions && ![functions isEmptyOrHasOnlyWhiteSpaces] &&
          testScript && ![testScript isEmptyOrHasOnlyWhiteSpaces]) {
        finalTestScript = [NSString stringWithFormat:@"%@\n%@",functions,testScript];
      }
      
      /// replace params in script
      for (PAScriptFileInclude *scriptFile in scriptFileIncludeList) {
        NSString *scriptFileName = scriptFile.source;
        NSString *language = scriptFile.language;
        /// consider only apple scripts
        PAScriptType includeScriptType = [PASupportActionInfo scriptTypeEnumFor:language];
        PAScriptType mainScriptType = [PASupportActionInfo scriptTypeEnumFor:mainScriptLanguage];
        PAScriptType testScriptType = [PASupportActionInfo scriptTypeEnumFor:testScriptLanguage];
        if (includeScriptType == PAScriptTypeApple) {
          NSString *includeScriptFilePath = [_safRootFolderPath stringByAppendingPathComponent:scriptFileName];
          NSData *includeScriptData = [_fsManager contentOfFileAtPath:includeScriptFilePath];
          NSString *includeScriptDataStr = nil;
          if (includeScriptData) {
            includeScriptDataStr = [[NSString alloc] initWithData:includeScriptData encoding:NSASCIIStringEncoding];
          }
          if (mainScriptType == PAScriptTypeApple) {
            if (![includeScriptDataStr isEmptyOrHasOnlyWhiteSpaces] &&
                ![finalMainScript isEmptyOrHasOnlyWhiteSpaces]) {
              finalMainScript = [NSString stringWithFormat:@"%@\n%@",includeScriptDataStr,finalMainScript];
            }
          }
          if (testScriptType == PAScriptTypeApple) {
            if (![includeScriptDataStr isEmptyOrHasOnlyWhiteSpaces] &&
                ![finalTestScript isEmptyOrHasOnlyWhiteSpaces]) {
              finalTestScript = [NSString stringWithFormat:@"%@\n%@",includeScriptDataStr,finalTestScript];
            }
          }
        }
      }
      /// replace params in script
      for (PASupportActionInputParameter *inputParam in paramList) {
        NSString *key = inputParam.key;
        NSString *value = inputParam.value;
        NSString *keyPlaceholder = [NSString stringWithFormat:@"__%@__",key];
        finalMainScript = [finalMainScript stringByReplacingOccurrencesOfString:keyPlaceholder withString:value];
        finalTestScript = [finalTestScript stringByReplacingOccurrencesOfString:keyPlaceholder withString:value];
      }
      
      /// replace privilege tool helper path in script
      NSString *privilegeToolHelperPath = [_fsManager privilegeToolHelperPath];
      finalMainScript = [finalMainScript stringByReplacingOccurrencesOfString:kPASAPrivilegeToolPlaceholder withString:privilegeToolHelperPath];
      finalTestScript = [finalTestScript stringByReplacingOccurrencesOfString:kPASAPrivilegeToolPlaceholder withString:privilegeToolHelperPath];
      
      
      /// create script files
      NSString *mainScriptPath = [self createScriptFileWithName:kPASAMainScriptFileName script:finalMainScript type:mainScriptLanguage];
      NSString *testScriptPath = [self createScriptFileWithName:kPASATestScriptFileName script:finalTestScript type:testScriptLanguage];
      
      PASupportActionInfo *info = [[PASupportActionInfo alloc] initWithMainScriptPath:mainScriptPath andType:mainScriptLanguage testScriptPath:testScriptPath andType:testScriptLanguage];
      completionBlock(true, info);
    }
  }
  else {
    completionBlock(success, nil);
  }
}

- (NSString *)safRootFolderPath {
  return [_safFilePath stringByDeletingLastPathComponent];
}

- (NSString *)createScriptFileWithName:(NSString *)name
                                script:(NSString *)script
                                  type:(NSString *)scriptType {
  NSString *fileExtension = [self scriptFileExtensionFor:scriptType];
  if (!fileExtension) {
    return nil;
  }
  NSString *filePath = _safRootFolderPath;
  filePath = [filePath stringByAppendingPathComponent:name];
  filePath = [filePath stringByAppendingPathExtension:fileExtension];
  NSData *fileData = [script dataUsingEncoding:NSASCIIStringEncoding];
  if (fileData && [_fsManager createFileAtPath:filePath attributes:nil error:nil] && [_fsManager writeToFileAtPath:filePath data:fileData]) {
    return filePath;
  }
  return nil;
}

- (NSString *)scriptFileExtensionFor:(NSString *)scriptType {
  NSString *extension = nil;
  PAScriptType type = [PASupportActionInfo scriptTypeEnumFor:scriptType];
  switch (type) {
    case PAScriptTypeApple:
      extension = kPASAAppleScriptExtension;
      break;
    case PAScriptTypeShell:
      extension = kPASAShellScriptExtension;
      break;
    default:
      break;
  }
  return extension;
}
@end
