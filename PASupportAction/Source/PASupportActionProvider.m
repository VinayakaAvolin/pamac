//
//  PASupportActionProvider.m
//  PASupportAction
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <PAFileSystemService/PAFileSystemService.h>
#import "PASupportActionProvider.h"
#import "PASupportActionXmlParser.h"
#import "PASupportActionSafParser.h"
#import "PAServiceConstants.h"
#import "PACodeSignValidator.h"

NSString *const kPACabFileName = @"composite.cab";
NSString *const kPAScriptToGetSafFile = @"do shell script \"cd '%@'; ls -1t | grep *.saf\"";
NSString *const kPAScriptToGetShFile = @"do shell script \"cd '%@'; ls -1t | grep *.sh\"";

typedef void(^PASGetSafCompletionBlock)(BOOL success, NSString *safFileName);
typedef void(^PASGetShCompletionBlock)(BOOL success);

@interface PASupportActionProvider () {
  PAServiceCallback _callback;
  PAServiceInfo *_serviceInfo;
  NSString *_supportActionRootGuidFolderPath;
  NSString *_temporaryFolderPath;
  NSString *_guidWithVersionFolderName;
  NSString *_cabContentsFolderPath;
  NSString *_safFilePath;
  PAFileSystemManager *_fsManager;
  PASupportActionInfo *_supportActionInfo;
  int _exitCode;
  bool _hasExtractedZip;
}

@end

@implementation PASupportActionProvider

- (BOOL)canExecuteService:(PASupportService)service category:(PASupportServiceCategory)category {
  if (category == PASupportServiceCategorySupportAction &&
      (service == PASupportServiceSupportActionScan ||
       service == PASupportServiceSupportActionFix ||
       service == PASupportServiceOptimizationScan ||
       service == PASupportServiceOptimizationFix  ||
       service == PASupportServiceSupportActionMessageFix)) {
        return true;
      }
  return false;
}

- (void)executeService:(PASupportService)service category:(PASupportServiceCategory)category info:(id<PAServiceInfoProtocol>)info callback:(PAServiceCallback)callback {
  _fsManager = [[PAFileSystemManager alloc] init];
  _callback = callback;
  _exitCode = -1;
  _serviceInfo = (PAServiceInfo *)info;
  if (!_serviceInfo.guid) {
    [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"Guid missing"];
    _callback(false, nil, [self responseDict]);
    return;
  }
  [self parseInput];
}

- (void)parseInput {
  PASupportActionXmlParser *configParser = [[PASupportActionXmlParser alloc] initWithServiceInfo:_serviceInfo];
  [configParser parseInputConfigurationWithCompletion:^(BOOL success, PAServiceInfo *serviceInfo) {
    _supportActionRootGuidFolderPath = configParser.supportActionRootGuidPath;
    _hasExtractedZip = false;
    _guidWithVersionFolderName = configParser.supportActionGuidFolderName;
    if (success) {
      _serviceInfo = serviceInfo;
      [self didFinishInputParsing];
    } else {
      [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"Failed to parse xml for [%@]",configParser.supportActionGuidFolderName];
      _callback(false, nil, [self responseDict]);
    }
  }];
}

- (void)didFinishInputParsing {
  if (!_supportActionRootGuidFolderPath ||
      !_guidWithVersionFolderName) {
    _callback(false, nil, [self responseDict]);
    return;
  }
  switch (_serviceInfo.service) {
    case PASupportServiceOptimizationScan:
    case PASupportServiceOptimizationFix:
      [self handleOptimization];
      break;
    case PASupportServiceSupportActionFix:
    case PASupportServiceSupportActionScan:
    case PASupportServiceSupportActionMessageFix:
      [self handleAutofix];
      break;
    default:
      [self cleanup];
      _callback(false, nil, [self responseDict]);
      break;
  }
}
- (void)handleOptimization {
  [self executeSupportAction];
}

- (void)handleAutofix {
  [self executeSupportAction];
}
/// verify code signing of cab
- (BOOL)isValidCab {
  BOOL hasValidCodesign = [PACodeSignValidator validateCodeSigningOfItemAtPath:[self cabFilePath]];
  return hasValidCodesign;
}
- (void)executeSupportAction {
  
  NSString *srcCabPath = [self cabFilePath];
  
  NSString *dstFileName = [srcCabPath stringByDeletingPathExtension];
  dstFileName = [dstFileName lastPathComponent];
  NSError *error;
  
  _temporaryFolderPath = [_fsManager temporaryDownloadsFolderPath];
  NSString *tempZipFile = [_temporaryFolderPath stringByAppendingPathComponent:dstFileName];
  tempZipFile = [tempZipFile stringByAppendingPathExtension:@"zip"];
  
  if ( [[NSFileManager defaultManager] isReadableFileAtPath:srcCabPath] ){
    if([[NSFileManager defaultManager] copyItemAtPath:srcCabPath toPath:tempZipFile error:&error]) {
      NSTask * task = [[NSTask alloc] init];
      [task setLaunchPath:@"/usr/bin/ditto"];
      [task setArguments:@[@"-x", @"-k", tempZipFile, _temporaryFolderPath]];
      [task launch];
      [task waitUntilExit];
      [[NSFileManager defaultManager] removeItemAtPath:tempZipFile error:&error];
      _hasExtractedZip = true;
    }
  }
  
  if (![self isValidCab]) {
    [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"Cab code sign validation has failed"];
    _callback(false, nil, [self responseDict]);
    return;
  }
  
  //Rename ".zip"
  //
  
  // first extract cab
  if ([self extractSupportActionFromCab]) {
    [self getSafFileWithCallback:^(BOOL success, NSString *safFileName) {
      if (success && safFileName) {
        _safFilePath = [_cabContentsFolderPath stringByAppendingPathComponent:safFileName];
        [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"Saf file path [%@]",_safFilePath];
        [self parseSafFile];
      } else {
        [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"Unable to read saf file"];
        _callback(false, nil, [self responseDict]);
      }
    }];
  } else {
    [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"Unable to extract cab file content"];
    [self cleanup];
    _callback(false, nil, [self responseDict]);
  }
}

- (void)parseSafFile {
  if (!_safFilePath) {
    [self cleanup];
    _callback(false, nil, [self responseDict]);
    return;
  }
  PASupportActionSafParser *safParser = [[PASupportActionSafParser alloc] initWithSaf:_safFilePath andServiceInfo:_serviceInfo];
  [safParser parseSafAndCreateScriptFilesWithCompletion:^(BOOL success, PASupportActionInfo *info) {
    if (success && info) {
      _supportActionInfo = info;
      [self updateExecutablePermissionToShellScriptsWithCallback:^(BOOL success) {
        if (success) {
          [self executeSolutionScript];
        } else {
          [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"Unable to set executable permission to helper script"];
          [self cleanup];
          _callback(false, nil, [self responseDict]);
        }
      }];
    } else {
      [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"Unable to parse saf file [%@]",_safFilePath];
      [self cleanup];
      _callback(false, nil, [self responseDict]);
    }
  }];
}
- (void)cleanup {
  [_fsManager deleteDirectory:_cabContentsFolderPath];
  if(_hasExtractedZip) {
    [_fsManager deleteFile:[self cabFilePath]];
  }
}

- (NSString *)cabFilePath {
  if(_hasExtractedZip) {
    return [_temporaryFolderPath stringByAppendingPathComponent:kPACabFileName];
  }
  return [_supportActionRootGuidFolderPath stringByAppendingPathComponent:kPACabFileName];
  ;
}
- (BOOL)extractSupportActionFromCab {
  
  NSString *srcCabPath = [self cabFilePath];
  
  NSString *dstFileName = [srcCabPath stringByDeletingPathExtension];
  dstFileName = [dstFileName lastPathComponent];
  NSError *error;
  
  NSString *dstPath = [_fsManager temporaryDownloadsFolderPath];
  dstPath = [dstPath stringByAppendingPathComponent:dstFileName];
  _cabContentsFolderPath = dstPath;
  
  [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"Extract Support Action From Cab at path [%@] to [%@]",srcCabPath, dstPath];
  
  PACabServiceManager *cabManager = [[PACabServiceManager alloc]init];
  BOOL returnValue = [cabManager extractCabAtPath:srcCabPath toDestinationFolder:dstPath error:nil];
  
  [[NSFileManager defaultManager] removeItemAtPath:srcCabPath error:&error];
  return returnValue;
}

- (void)getSafFileWithCallback:(PASGetSafCompletionBlock)callback {
  NSString *getGuidFolderScriptStr = [NSString stringWithFormat:kPAScriptToGetSafFile, _cabContentsFolderPath];
  PAAppleScript *script = [[PAAppleScript alloc]initWithScriptString:getGuidFolderScriptStr];
  
  [script runWithCallBack:^(BOOL success, NSError *error, id result) {
    NSString *fileNamesStr = (NSString *)result;
    NSString *safFileName = nil;
    if ([fileNamesStr isKindOfClass:[NSString class]]) {
      NSArray *fileNames = [fileNamesStr componentsSeparatedByString:@"\r"];
      if (fileNames.count) {
        safFileName = fileNames.firstObject;
      }
    }
    [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"Saf file name [%@]",safFileName];
    
    callback(success, safFileName);
  }];
}
- (void)updateExecutablePermissionToShellScriptsWithCallback:(PASGetShCompletionBlock)callback {
  NSString *getShFilesScriptStr = [NSString stringWithFormat:kPAScriptToGetShFile, _cabContentsFolderPath];
  PAAppleScript *script = [[PAAppleScript alloc]initWithScriptString:getShFilesScriptStr];
  
  [script runWithCallBack:^(BOOL success, NSError *error, id result) {
    NSString *fileNamesStr = (NSString *)result;
    BOOL excPermissionSet = true;
    if ([fileNamesStr isKindOfClass:[NSString class]]) {
      NSArray *fileNames = [fileNamesStr componentsSeparatedByString:@"\r"];
      if (fileNames.count) {
        PAScript *script = [[PAScript alloc]init];
        for (NSString *shellScriptFile in fileNames) {
          NSString  *shFilePath = [_cabContentsFolderPath stringByAppendingPathComponent:shellScriptFile];
          
          excPermissionSet = [script setExecutablePermissionToScriptAtPath:shFilePath];
          
          [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"Executable permission set for Helper script at path [%@]? [%d]",shFilePath,excPermissionSet];
          
          if (!excPermissionSet) {
            break;
          }
          
        }
      }
      
    }
    callback(excPermissionSet);
  }];
}

- (void)executeSolutionScript {
  NSString *scriptPath = nil;
  PAScriptType scriptType = PAScriptTypeNone;
  switch (_serviceInfo.service) {
    case PASupportServiceOptimizationScan:
    case PASupportServiceSupportActionScan:
      scriptPath = _supportActionInfo.testScriptPath;
      scriptType = _supportActionInfo.testScriptTypeEnum;
      break;
    case PASupportServiceSupportActionFix:
    case PASupportServiceOptimizationFix:
    case PASupportServiceSupportActionMessageFix:
      scriptPath = _supportActionInfo.mainScriptPath;
      scriptType = _supportActionInfo.mainScriptTypeEnum;
      break;
    default:
      [self cleanup];
      _callback(false, nil, [self responseDict]);
      break;
  }
  if (scriptPath &&
      scriptType != PAScriptTypeNone) {
    [PAScriptRunner executeScriptWithPath:scriptPath ofType:scriptType callBack:^(BOOL success, NSError *error, id result) {
      NSString *exitCodeStr = (NSString *)result;
      [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"Script output [%@]",exitCodeStr];
      if ([exitCodeStr isKindOfClass:[NSString class]]) {
        _exitCode = exitCodeStr.intValue;
      }
      [self cleanup];
      _callback(success, error, [self responseDict]);
    }];
  } else {
    [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"Unable to run script"];
    [self cleanup];
    _callback(false, nil, [self responseDict]);
  }
}

- (NSDictionary *)responseDict {
  NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
  [dict setValue:_serviceInfo.guid forKey:kPASupportServiceResponseKeyGuid];
  [dict setValue:[NSNumber numberWithInt:_exitCode] forKey:kPASupportServiceResponseKeyExitCode];
  return [NSDictionary dictionaryWithDictionary:dict];
}
@end
