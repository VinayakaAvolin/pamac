//
//  PASupportActionXmlParser.m
//  PASupportAction
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PASupportActionXmlParser.h"
#import "NSString+Additions.h"

NSString *const kPAContentSetKey = @"content-set";
NSString *const kPAContentKey = @"content";
NSString *const kPAFieldSetKey = @"field-set";
NSString *const kPAFieldKey = @"field";
NSString *const kPANameKey = @"_name";
NSString *const kPAValueKey = @"value";
NSString *const kPATextKey = @"__text";
NSString *const kPAExecutionTypeKey = @"executiontype";

NSString *const kPAScriptToGetGuidFolders = @"do shell script \"cd '%@'; ls -1t | grep %@\"";

typedef void(^PASGetGuidCompletionBlock)(BOOL success, NSString *guidWithVersionFolderName);

@interface PASupportActionXmlParser () {
  NSString *_inputXmlPath;
  PAServiceInfo *_serviceInfo;
  PAFileSystemManager *_fsManager;
  NSString *_supportActionRootFolderPath;
}

@end

@implementation PASupportActionXmlParser

- (instancetype)initWithServiceInfo:(PAServiceInfo *)serviceInfo {
  self = [super init];
  if (self && serviceInfo.guid) {
    _serviceInfo = serviceInfo;
    _fsManager = [[PAFileSystemManager alloc]init];
  } else {
    self = nil;
  }
  return self;
}


- (void)parseInputConfigurationWithCompletion:(PASupportActionXmlParserCompletionBlock)completionBlock {
  [self setupInputConfigurationXmlWithCallback:^(BOOL success, NSString *guidWithVersionFolderName) {
    if (success) {
      NSDictionary *xmlDict = [self dictionaryFromXmlAtPath:_inputXmlPath];
      if (xmlDict) {
        NSDictionary *contentSetDict = [xmlDict valueForKey:kPAContentSetKey];
        if ([contentSetDict isKindOfClass:[NSDictionary class]]) {
          NSDictionary *contentDict = [contentSetDict valueForKey:kPAContentKey];
          if ([contentDict isKindOfClass:[NSDictionary class]]) {
            NSDictionary *fieldSetDict = [contentDict valueForKey:kPAFieldSetKey];
            if ([fieldSetDict isKindOfClass:[NSDictionary class]]) {
              NSArray *fieldsList = [fieldSetDict valueForKey:kPAFieldKey];
              if ([fieldsList isKindOfClass:[NSArray class]]) {
                NSString *executionType = nil;
                // support action key
                NSDictionary *executiontypeFieldDict = [self dictionaryMatchingName:kPAExecutionTypeKey inList:fieldsList];
                NSString *excType = [self valueFromFieldDictionary:executiontypeFieldDict];
                if ([excType isKindOfClass:[NSString class]]) {
                  executionType = excType;
                }
                
                PAServiceInfo *serviceInfo = [_serviceInfo copy];
                serviceInfo.executionTypeStr = executionType;
                completionBlock(true, serviceInfo);
                return;
              }
            }
          }
        }
      }
    }
    else {
      completionBlock(false, _serviceInfo);
    }
  }];
}

- (NSDictionary *)dictionaryMatchingName:(NSString *)name inList:(NSArray *)inputList {
  if (inputList.count) {
    NSArray *filtered = [inputList filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(_name == %@)", name]];
    if (filtered.count) {
      return filtered[0];
    }
  }
  
  return nil;
}

- (id)valueFromFieldDictionary:(NSDictionary *)fieldDict {
  if ([fieldDict isKindOfClass:[NSDictionary class]]) {
    NSDictionary *valueDict = [fieldDict valueForKey:kPAValueKey];
    if ([valueDict isKindOfClass:[NSDictionary class]]) {
      NSString *value = [valueDict valueForKey:kPATextKey];
      if ([value isKindOfClass:[NSString class]]) {
        return value;
      }
    }
  }
  return nil;
}


/// constructs app specific protection xml path
/// Eg: ~/Library/Application\ Support/ProactiveAssist/Experience92/data/sprt_msg/9c3b49b2-fa85-4e1b-b6a1-1ff832919167.1/9c3b49b2-fa85-4e1b-b6a1-1ff832919167.1.xml
- (void)setupInputConfigurationXmlWithCallback:(PASGetGuidCompletionBlock)callback {
  __block NSString *xmlPath = nil;
  switch (_serviceInfo.service) {
    case PASupportServiceOptimizationScan:
    case PASupportServiceOptimizationFix:
      xmlPath = [_fsManager optimizationFolderPath];
      break;
    case PASupportServiceSupportActionFix:
    case PASupportServiceSupportActionScan:
      xmlPath = [_fsManager supportActionFolderPath];
      break;
    case PASupportServiceSupportActionMessageFix:
      xmlPath = [_fsManager messagesFolderPath];
      break;
    default:
      break;
  }
  _supportActionRootFolderPath = xmlPath;
  
  [self getGuidFoldersWithCallback:^(BOOL success, NSString *guidWithVersionFolderName) {
    if (xmlPath && success && guidWithVersionFolderName) {
      NSString *xmlFileName = [NSString stringWithFormat:@"%@.xml",guidWithVersionFolderName];
      _supportActionGuidFolderName = guidWithVersionFolderName;
      xmlPath = [xmlPath stringByAppendingPathComponent:guidWithVersionFolderName];
      _supportActionRootGuidPath = xmlPath; // store guid folder path
      xmlPath = [xmlPath stringByAppendingPathComponent:xmlFileName];
      if ([_fsManager doesFileExist:xmlPath]) {
        _inputXmlPath = xmlPath;
      }
    }
    callback(success, guidWithVersionFolderName);
  }];
  
}

- (void)getGuidFoldersWithCallback:(PASGetGuidCompletionBlock)callback {
  if ([self doesGuidFolderHasVersion]) {
    callback(true, _serviceInfo.guid);
    return;
  }
  NSString *getGuidFolderScriptStr = [NSString stringWithFormat:kPAScriptToGetGuidFolders, _supportActionRootFolderPath, _serviceInfo.guid];
  PAAppleScript *script = [[PAAppleScript alloc]initWithScriptString:getGuidFolderScriptStr];
  
  [script runWithCallBack:^(BOOL success, NSError *error, id result) {
    NSString *folderNamesStr = (NSString *)result;
    NSString *highestVersionGuidFolderName = nil;
    if ([folderNamesStr isKindOfClass:[NSString class]]) {
      NSArray *folderNames = [folderNamesStr componentsSeparatedByString:@"\r"];
      NSUInteger lastGreaterVersion = 0;
      for (NSString *folderName in folderNames) {
        NSString *pathExtension = folderName.pathExtension;
        if (pathExtension.integerValue > lastGreaterVersion) {
          highestVersionGuidFolderName = folderName;
          lastGreaterVersion = pathExtension.integerValue;
        }
        
      }
    }
    callback(success, highestVersionGuidFolderName);
  }];
}

- (BOOL)doesGuidFolderHasVersion {
  NSString *guidFolderName = _serviceInfo.guid;
  NSString *pathExtension = guidFolderName.pathExtension;
  if (pathExtension && ![pathExtension isEmptyOrHasOnlyWhiteSpaces]) {
    return true;
  }
  return false;
}
@end
