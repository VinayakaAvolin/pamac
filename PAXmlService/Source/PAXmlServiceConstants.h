//
//  PAXmlServiceConstants.h
//  PAXmlService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifndef PAXmlServiceConstants_h
#define PAXmlServiceConstants_h

extern NSString *const kPAXmlVersionKey;
extern NSString *const kPAXmlEncodingKey;
extern NSString *const kPAXmlStandaloneKey;

extern NSString *const kPAXmlVersionValue;
extern NSString *const kPAXmlUtf8Encoding;
extern NSString *const kPAXmlRootValue;

#endif /* PAXmlServiceConstants_h */
