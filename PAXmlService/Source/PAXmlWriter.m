//
//  PAXmlWriter.m
//  PAXmlService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PAXmlWriter.h"
#import "PAXmlUtility.h"
#import "PAXmlAttribute.h"

@interface PAXmlWriter () {
  NSXMLDocument *_xmlDocument;
  NSString *_xmlPath;
}

@end

@implementation PAXmlWriter

- (instancetype)initWithXmlAtPath:(NSString *)xmlPath {
  self = [super init];
  if (self) {
    if (!xmlPath) {
      self = nil;
    } else {
      _xmlPath = xmlPath;
      _xmlDocument = [PAXmlUtility xmlDocAtPath:xmlPath error:nil];
      if (!_xmlDocument) {
        self = nil;
      }
    }
  }
  return self;
}
- (BOOL)replaceXmlElementWithName:(NSString *)name
                            value:(id)value
                       attributes:(NSArray *)attributes
                      parentXPath:(NSString *)parentXPath {
  BOOL isSuccess = false;
  NSError *error = nil;
  NSXMLElement *root = _xmlDocument.rootElement;
  NSXMLElement *childElement = [[NSXMLElement alloc]initWithName:name];
  
  if (childElement && root) {
    childElement.stringValue = value;
    
    for (PAXmlAttribute *attribute in attributes) {
      NSXMLNode *attributeNode = [NSXMLNode attributeWithName:attribute.name stringValue:attribute.value];
      [childElement addAttribute:attributeNode];
    }
    BOOL xmlUpdated = false;
    NSInteger indexOfChild = -1;
    NSXMLElement *matchingParentNode;
    /// get  parent node matching the input XPath
    NSArray *matchingParentNodes = [root nodesForXPath:parentXPath error:&error];
    if ([root.XPath containsString:parentXPath]) {
      matchingParentNode = root;
    } else if (matchingParentNodes.count) {
      matchingParentNode = (NSXMLElement *)matchingParentNodes.firstObject;
    }
    
    if (matchingParentNode) {
      NSArray *matchingChildren = [matchingParentNode elementsForName:name];
      if (matchingChildren.count) {
        NSXMLNode *matchingChild = matchingChildren.firstObject;
        if (matchingChild) {
          indexOfChild = [[matchingParentNode children] indexOfObject:matchingChild];
        }
      }
      if (indexOfChild >= 0) {
        [matchingParentNode replaceChildAtIndex:indexOfChild withNode:[childElement copy]];
        xmlUpdated = true;
      } else {
        [matchingParentNode addChild:[childElement copy]];
        xmlUpdated = true;
      }
    }
    if (xmlUpdated) {
      /// add input child to xml and update the xml file
      [_xmlDocument setRootElement:root];
      NSData *xmldata = [_xmlDocument XMLDataWithOptions: NSXMLNodePrettyPrint];
      isSuccess = [xmldata writeToFile:_xmlPath atomically:YES];
      
    }
  }
  return isSuccess;
}

- (BOOL)addXmlElementWithName:(NSString *)name
                        value:(id)value
                   attributes:(NSArray *)attributes
                  parentXPath:(NSString *)parentXPath {
  return [self addXmlElementWithName:name value:value attributes:attributes parentXPath:parentXPath withParentAttributeName:nil andValue:nil];
}
- (BOOL)addXmlElementWithName:(NSString *)name
                        value:(id)value
                   attributes:(NSArray *)attributes
                  parentXPath:(NSString *)parentXPath
      withParentAttributeName:(NSString *)attributeName
                     andValue:(NSString *)attributeValue {
  BOOL isSuccess = false;
  NSError *error = nil;
  NSXMLElement *root = _xmlDocument.rootElement;
  NSXMLElement *childElement = [[NSXMLElement alloc]initWithName:name];
  
  if (childElement && root) {
    childElement.stringValue = value;
    
    for (PAXmlAttribute *attribute in attributes) {
      NSXMLNode *attributeNode = [NSXMLNode attributeWithName:attribute.name stringValue:attribute.value];
      [childElement addAttribute:attributeNode];
    }
    BOOL xmlUpdated = false;
    /// get  parent node matching the input XPath
    NSArray *matchingParentNodes = [root nodesForXPath:parentXPath error:&error];
    if (!attributeName && [root.XPath containsString:parentXPath]) {
      [root addChild:[childElement copy]];
      xmlUpdated = true;
    } else if (matchingParentNodes.count) {
      /// plain node without any attribute
      if (!attributeName) {
        NSXMLElement *parent = matchingParentNodes.firstObject;
        [parent addChild:[childElement copy]];
        xmlUpdated = true;
      } else { // get node matching the input attribute
        NSXMLNode *matchingNode = [self nodeMatchingAttributeName:attributeName andValue:attributeValue inNodesList:matchingParentNodes];
        /// remove all matching xml nodes
        if (matchingNode) {
          NSXMLElement *actualParent = (NSXMLElement *)matchingNode;
          [actualParent addChild:[childElement copy]];
          xmlUpdated = true;
        }
      }
    }
    
    
    if (xmlUpdated) {
      /// add input child to xml and update the xml file
      [_xmlDocument setRootElement:root];
      NSData *xmldata = [_xmlDocument XMLDataWithOptions: NSXMLNodePrettyPrint];
      isSuccess = [xmldata writeToFile:_xmlPath atomically:YES];
      
    }
  }
  return isSuccess;
}
- (BOOL)addChildXmlString:(NSString *)childXml
              parentXPath:(NSString *)parentXPath {
  return [self addChildXmlString:childXml toParentXPath:parentXPath withParentAttributeName:nil andValue:nil];
}
- (BOOL)addChildXmlString:(NSString *)childXml
            toParentXPath:(NSString *)parentXPath
  withParentAttributeName:(NSString *)attributeName
                 andValue:(NSString *)attributeValue {
  BOOL isSuccess = false;
  NSError *error = nil;
  NSXMLElement *root = _xmlDocument.rootElement;
  NSXMLElement *childElement = [[NSXMLElement alloc]initWithXMLString:childXml error:&error];
  if (childElement && root) {
    BOOL isXmlUpdated = false;
    /// add children directly to root
    if ([root.XPath containsString:parentXPath]) {
      [root addChild:[childElement copy]];
      isXmlUpdated = true;
    } else {
      /// get  parent node matching the input XPath
      NSArray *matchingParentNodes = [root nodesForXPath:parentXPath error:&error];
      if (matchingParentNodes.count) {
        /// plain node without any attribute
        if (!attributeName) {
          NSXMLElement *parent = matchingParentNodes.firstObject;
          [parent addChild:[childElement copy]];
          isXmlUpdated = true;
        } else { // get node matching the input attribute
          NSXMLNode *matchingNode = [self nodeMatchingAttributeName:attributeName andValue:attributeValue inNodesList:matchingParentNodes];
          /// remove all matching xml nodes
          if (matchingNode) {
            NSXMLElement *actualParent = (NSXMLElement *)matchingNode;
            [actualParent addChild:[childElement copy]];
            isXmlUpdated = true;
          }
        }
      }
    }
    
    if (isXmlUpdated) {
      /// add input child to xml and update the xml file
      [_xmlDocument setRootElement:root];
      NSData *xmldata = [_xmlDocument XMLDataWithOptions: NSXMLNodePrettyPrint];
      isSuccess = [xmldata writeToFile:_xmlPath atomically:YES];
    }
  }
  return isSuccess;
}

- (BOOL)removeValueAtXPath:(NSString *)xPath
         withAttributeName:(NSString *)attributeName
                  andValue:(NSString *)attributeValue {
  BOOL isSuccess = false;
  NSError *error = nil;
  NSXMLElement *root = _xmlDocument.rootElement;
  if (root) {
    /// get the node matching the input XPath
    NSArray *matchingNodes = [root nodesForXPath:xPath error:&error];
    if (matchingNodes.count) {
      NSArray *matchingElements = [self nodesMatchingAttributeName:attributeName andValue:attributeValue inParentNodes:matchingNodes];
      /// remove all matching xml nodes
      if (matchingElements.count) {
        for (id matchingNode in matchingElements) {
          if ([matchingNode isKindOfClass:[NSXMLNode class]]) {
            [matchingNode detach];
          }
        }
        
        /// update the xml file after removing the input xml node
        [_xmlDocument setRootElement:root];
        NSData *xmldata = [_xmlDocument XMLDataWithOptions: NSXMLNodePrettyPrint];
        isSuccess = [xmldata writeToFile:_xmlPath atomically:YES];
      }
    }
  }
  return isSuccess;
}

- (NSArray *)nodesMatchingAttributeName:(NSString *)attributeName
                               andValue:(NSString *)attributeValue
                          inParentNodes:(NSArray *)nodesList {
  if (nodesList.count == 0) {
    return nil;
  }
  /// if no attribute to match; then remove all
  if (!attributeName || !attributeValue) {
    return nodesList;
  }
  NSMutableArray *outputNodeList = [[NSMutableArray alloc]init];
  for (NSXMLNode *node in nodesList) {
    if ([node isKindOfClass:[NSXMLElement class]]) {
      NSArray *children = node.children;
      for (NSXMLNode *childNode in children) {
        BOOL matchFound = false;
        if ([childNode isKindOfClass:[NSXMLElement class]]) {
          NSArray *childAttributes = ((NSXMLElement *)childNode).attributes;
          if (childAttributes.count) {
            for (NSXMLNode *attribute in childAttributes) {
              if ([attribute.name isEqualToString:attributeName] && [attribute.stringValue isEqualToString:attributeValue]) {
                [outputNodeList addObject:node];
                matchFound = true;
                break;
              }
            }
          }
        }
        if (matchFound) {
          break;
        }
      }
    }
  }
  if (outputNodeList.count) {
    return [NSArray arrayWithArray:outputNodeList];
  }
  return nil;
}
- (NSXMLNode *)nodeMatchingAttributeName:(NSString *)attributeName
                                andValue:(NSString *)attributeValue
                             inNodesList:(NSArray *)nodesList {
  if (nodesList.count == 0) {
    return nil;
  }
  /// if no attribute to match; then remove all
  if (!attributeName || !attributeValue) {
    return nil;
  }
  NSXMLNode *matchingNode = nil;
  for (NSXMLNode *node in nodesList) {
    if ([node isKindOfClass:[NSXMLElement class]]) {
      NSArray *childAttributes = ((NSXMLElement *)node).attributes;
      if (childAttributes.count) {
        for (NSXMLNode *attribute in childAttributes) {
          if ([attribute.name isEqualToString:attributeName] && [attribute.stringValue isEqualToString:attributeValue]) {
            matchingNode = node;
            break;
          }
        }
      }
      
    }
  }
  
  return matchingNode;
}
#pragma mark - Private methods


@end
