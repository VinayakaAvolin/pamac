//
//  PAXmlReader.h
//  PAXmlService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PAXmlReader : NSObject

- (instancetype)initWithXmlAtPath:(NSString *)xmlPath;

/// To get all nodes at parentXPath, pass nil in attributes parameter
- (NSString *)xmlStringAtXPath:(NSString *)parentXPath
            matchingAttributes:(NSArray *)attributes;

// to get value of multiple nodes at parentXPath, matching the "attributes"
- (NSArray *)xmlStringsAtXPath:(NSString *)parentXPath
            matchingAttributes:(NSArray *)attributes;

@end
