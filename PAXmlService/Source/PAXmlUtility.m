//
//  PAXmlUtility.m
//  PAXmlService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PAXmlUtility.h"

@implementation PAXmlUtility

+ (NSXMLDocument *)xmlDocAtPath:(NSString *)filePath
                          error:(NSError **)error {
  NSURL *fileUrl = [NSURL fileURLWithPath:filePath];
  if (!fileUrl) {
    return nil;
  }
  NSXMLDocument *xmlDoc = [[NSXMLDocument alloc]initWithContentsOfURL:fileUrl
                                                              options:NSXMLNodePrettyPrint
                                                                error:error];
  if (!xmlDoc) {
    xmlDoc = [[NSXMLDocument alloc] initWithContentsOfURL:fileUrl
                                                  options:(NSXMLNodePreserveWhitespace|NSXMLNodePreserveCDATA)
                                                    error:error];
  }
  return xmlDoc;
}

+ (NSXMLDocument *)xmlDocFromData:(NSData *)xmldata
                            error:(NSError **)error {
  if (!xmldata) {
    return nil;
  }
  NSXMLDocument *xmlDoc = [[NSXMLDocument alloc]initWithData:xmldata
                                                     options:NSXMLNodePrettyPrint
                                                       error:error];
  if (!xmlDoc) {
    xmlDoc = [[NSXMLDocument alloc] initWithData:xmldata
                                         options:(NSXMLNodePreserveWhitespace|NSXMLNodePreserveCDATA)
                                           error:error];
  }
  return xmlDoc;
}


@end
