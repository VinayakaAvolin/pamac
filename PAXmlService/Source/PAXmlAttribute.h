//
//  PAXmlAttribute.h
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PAXmlAttribute : NSObject

@property(nonatomic, readonly) NSString *name;
@property(nonatomic, readonly) NSString *value;

- (instancetype)initWithName:(NSString *)name
                       value:(NSString *)value;

@end
