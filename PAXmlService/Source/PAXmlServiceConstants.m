//
//  PAXmlServiceConstants.m
//  PAXmlService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PAXmlServiceConstants.h"

NSString *const kPAXmlVersionKey = @"version";
NSString *const kPAXmlEncodingKey = @"encoding";
NSString *const kPAXmlStandaloneKey = @"standalone";

NSString *const kPAXmlVersionValue = @"1.0";
NSString *const kPAXmlUtf8Encoding = @"UTF-8";
NSString *const kPAXmlRootValue = @"root";
