//
//  PAXmlServiceManager.m
//  PAXmlService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <PAFileSystemService/PAFileSystemService.h>
#import "PAXmlServiceManager.h"
#import "PAXmlUtility.h"

@implementation PAXmlServiceManager

- (BOOL)createXmlAtPath:(NSString *)filePath withProperties:(PAXmlFileProperties *)properties {
  /// if file path and root element name nil then do not proceed
  if (!filePath ||
      !properties.rootElementName) {
    return false;
  }
  /// first create the folder under which the file has to be created
  BOOL isFolderPresent = [self checkAndCreateTargetDirectoryForFileAtPath:filePath];
  if (!isFolderPresent) {
    return false;
  }
  BOOL isSuccess = false;
  NSXMLElement *rootElement = (NSXMLElement *)[NSXMLNode elementWithName:properties.rootElementName];
  NSXMLDocument *xmlDoc = [[NSXMLDocument alloc] initWithRootElement:rootElement];
  if (properties.version) {
    [xmlDoc setVersion:properties.version];
  }
  if (properties.standalone) {
    [xmlDoc setStandalone:properties.standalone.boolValue];
  }
  if (properties.encoding) {
    [xmlDoc setCharacterEncoding:properties.encoding];
  }
  
  NSData *xmlData = [xmlDoc XMLDataWithOptions:NSXMLNodePrettyPrint];
  if (!xmlData) {
    xmlData = [xmlDoc XMLDataWithOptions:NSXMLNodePreserveWhitespace|NSXMLNodePreserveCDATA];
  }
  if (xmlData) {
    isSuccess = [xmlData writeToFile:filePath atomically:YES];
  }
  return isSuccess;
}

- (BOOL)deleteXmlAtPath:(NSString *)filePath {
  PAFileSystemManager *manager = [[PAFileSystemManager alloc]init];
  return [manager deleteFile:filePath];
}

- (NSData *)xmlDataAtPath:(NSString *)filePath
                    error:(NSError **)error {
  NSXMLDocument *xmlDoc = [PAXmlUtility xmlDocAtPath:filePath error:error];
  if (!xmlDoc) {
    return nil;
  }
  NSData *xmlData = [xmlDoc XMLDataWithOptions:NSXMLNodePrettyPrint];
  if (!xmlData) {
    xmlData = [xmlDoc XMLDataWithOptions:NSXMLNodePreserveWhitespace|NSXMLNodePreserveCDATA];
  }
  return xmlData;
}

- (BOOL)writeXmlData:(NSData *)xmlData
              toPath:(NSString *)filePath
               error:(NSError **)error {
  BOOL isSuccess = false;
  
  /// first create the folder under which the file has to be created
  BOOL isFolderPresent = [self checkAndCreateTargetDirectoryForFileAtPath:filePath];
  if (!isFolderPresent) {
    return false;
  }
  
  NSXMLDocument *xmlDoc = [PAXmlUtility xmlDocFromData:xmlData error:error];
  
  if (xmlDoc) {
    isSuccess = [xmlData writeToFile:filePath atomically:YES];
  }
  
  return isSuccess;
}

- (BOOL)checkAndCreateTargetDirectoryForFileAtPath:(NSString *)filePath {
  /// first create the folder under which the file has to be created
  NSString *folderPath = [filePath stringByDeletingLastPathComponent];
  PAFileSystemManager *fsManager = [[PAFileSystemManager alloc]init];
  BOOL isFolderPresent = [fsManager createDirectoryAtPath:folderPath withIntermediateDirectories:YES attributes:nil error:nil];
  return isFolderPresent;
}
@end
