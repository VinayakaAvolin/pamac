//
//  PAXmlWriter.h
//  PAXmlService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PAXmlWriter : NSObject

- (instancetype)initWithXmlAtPath:(NSString *)xmlPath;

- (BOOL)addXmlElementWithName:(NSString *)name
                        value:(id)value
                   attributes:(NSArray *)attributes
                  parentXPath:(NSString *)parentXPath;
- (BOOL)replaceXmlElementWithName:(NSString *)name
                            value:(id)value
                       attributes:(NSArray *)attributes
                      parentXPath:(NSString *)parentXPath;

- (BOOL)addXmlElementWithName:(NSString *)name
                        value:(id)value
                   attributes:(NSArray *)attributes
                  parentXPath:(NSString *)parentXPath
      withParentAttributeName:(NSString *)attributeName
                     andValue:(NSString *)attributeValue;

/// Adds child xml node (constructed from input 'childXml' string) at input parent xpath
- (BOOL)addChildXmlString:(NSString *)childXml
              parentXPath:(NSString *)parentXPath;

- (BOOL)addChildXmlString:(NSString *)childXml
            toParentXPath:(NSString *)parentXPath
  withParentAttributeName:(NSString *)attributeName
                 andValue:(NSString *)attributeValue;

/// Removes xml node at input xpath matching the attributeName and attributeValue;
/// If you want to delete all children pass nil for attributeName and attributeValue
- (BOOL)removeValueAtXPath:(NSString *)xPath
         withAttributeName:(NSString *)attributeName
                  andValue:(NSString *)attributeValue;

@end
