//
//  PAXmlAttribute.m
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PAXmlAttribute.h"

@implementation PAXmlAttribute

- (instancetype)initWithName:(NSString *)name
                       value:(NSString *)value {
  self = [super init];
  if (self) {
    if (name &&
        value) {
      _name = name;
      _value = value;
    } else {
      self = nil;
    }
  }
  return self;
}

@end
