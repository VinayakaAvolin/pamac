//
//  PAXmlUtility.h
//  PAXmlService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PAXmlUtility : NSObject

+ (NSXMLDocument *)xmlDocAtPath:(NSString *)filePath
                          error:(NSError **)error;
+ (NSXMLDocument *)xmlDocFromData:(NSData *)xmldata
                            error:(NSError **)error;

@end
