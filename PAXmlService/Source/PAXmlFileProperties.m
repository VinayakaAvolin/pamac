//
//  PAXmlFileProperties.m
//  PAXmlService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PAXmlFileProperties.h"
#import "PAXmlServiceConstants.h"

@implementation PAXmlFileProperties

- (instancetype)initWithVersion:(NSString *)version
                       encoding:(NSString *)encoding
                     standalone:(NSNumber *)standalone
                           root:(NSString *)rootElementName {
  self = [super init];
  if (self) {
    _version = version;
    _standalone = standalone;
    _encoding = encoding;
    _rootElementName = rootElementName;
    if (!rootElementName) {
      _rootElementName = kPAXmlRootValue;
    }
  }
  return self;
}

+ (PAXmlFileProperties *)testXmlProperty {
  PAXmlFileProperties *property = [[PAXmlFileProperties alloc] initWithVersion:kPAXmlVersionValue encoding:kPAXmlUtf8Encoding standalone:@(false) root:@"UPLOADINFO"];
  return property;
}

@end
