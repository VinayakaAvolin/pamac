//
//  PAXmlServiceManager.h.h
//  PAXmlService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PAXmlFileProperties.h"

@interface PAXmlServiceManager : NSObject

/// Creates xml file at input 'filePath'; the 'filePath' should include the file name
- (BOOL)createXmlAtPath:(NSString *)filePath
         withProperties:(PAXmlFileProperties *)properties;

/// Deletes xml file at input 'filePath'; the 'filePath' should include the file name
- (BOOL)deleteXmlAtPath:(NSString *)filePath;

/// Returns xml data at file path; returns error in input 'error'
- (NSData *)xmlDataAtPath:(NSString *)filePath
                    error:(NSError **)error;

/// Writes xml data to input file path
- (BOOL)writeXmlData:(NSData *)xmlData
              toPath:(NSString *)filePath
               error:(NSError **)error;

@end
