//
//  PAXmlFileProperties.h
//  PAXmlService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PAXmlFileProperties : NSObject

@property(nonatomic, readonly) NSString *version;
@property(nonatomic, readonly) NSString *encoding;
@property(nonatomic, readonly) NSString *rootElementName;
@property(nonatomic, readonly) NSString *filePath;
@property(nonatomic, readonly) NSNumber *standalone;

- (instancetype)initWithVersion:(NSString *)version
                       encoding:(NSString *)encoding
                     standalone:(NSNumber *)standalone
                           root:(NSString *)rootElementName;

+ (PAXmlFileProperties *)testXmlProperty;

@end
