//
//  PAXmlReader.m
//  PAXmlService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PAXmlReader.h"
#import "PAXmlUtility.h"
#import "PAXmlAttribute.h"

@interface PAXmlReader () {
  NSXMLDocument *_xmlDocument;
  NSString *_xmlPath;
}

@end

@implementation PAXmlReader

- (instancetype)initWithXmlAtPath:(NSString *)xmlPath {
  self = [super init];
  if (self) {
    if (!xmlPath) {
      self = nil;
    } else {
      _xmlPath = xmlPath;
      _xmlDocument = [PAXmlUtility xmlDocAtPath:xmlPath error:nil];
      if (!_xmlDocument) {
        self = nil;
      }
    }
  }
  return self;
}

- (NSString *)xmlStringAtXPath:(NSString *)parentXPath
            matchingAttributes:(NSArray *)attributes {
  NSString *outputXmlString = nil;
  NSError *error = nil;
  NSXMLElement *root = _xmlDocument.rootElement;
  if (root) {
    /// get  parent node matching the input XPath
    NSArray *matchingNodes = [root nodesForXPath:parentXPath error:&error];
    if (matchingNodes.count) {
      NSXMLElement *parent = [self nodeMatchingAttributes:attributes inNodeList:matchingNodes].firstObject;
      if (parent) {
        outputXmlString = [parent XMLStringWithOptions:NSXMLNodePrettyPrint];
      }
    }
  }
  return outputXmlString;
}
- (NSArray *)xmlStringsAtXPath:(NSString *)parentXPath
            matchingAttributes:(NSArray *)attributes {
  NSMutableArray *xmlStringList = [[NSMutableArray alloc] init];
  NSError *error = nil;
  NSXMLElement *root = _xmlDocument.rootElement;
  if (root) {
    /// get  parent node matching the input XPath
    NSArray *matchingNodes = [root nodesForXPath:parentXPath error:&error];
    if (matchingNodes.count) {
      NSArray *parents = [self nodeMatchingAttributes:attributes inNodeList:matchingNodes];
      for (NSXMLElement *node in parents) {
        NSString *xmlString = [node XMLStringWithOptions:NSXMLNodePrettyPrint];
        if (xmlString) {
          [xmlStringList addObject:xmlString];
        }
      }
    }
  }
  return [NSArray arrayWithArray:xmlStringList];
}
- (NSArray *)nodeMatchingAttributes:(NSArray *)attributes inNodeList:(NSArray *)nodesList {
  NSArray *nodeList = nodesList;
  if (attributes.count) {
    NSMutableArray *nodes = [[NSMutableArray alloc] init];
    for (NSXMLElement *node in nodesList) {
      NSArray *nodeAttributes = [node attributes];
      if (nodeAttributes.count) {
        NSUInteger matchingAttributeCount = 0;
        for (PAXmlAttribute *attribute in attributes) {
          if ([self matchingNodeAttribute:attribute inNodes:nodeAttributes]) {
            matchingAttributeCount++;
          }
        }
        if (matchingAttributeCount == attributes.count) {
          [nodes addObject:node];
        }
      }
    }
    if (nodes.count) {
      nodeList = [NSArray arrayWithArray:nodes];
    } else {
      nodeList = nil;
    }
  }
  
  return nodeList;
}

- (NSXMLNode *)matchingNodeAttribute:(PAXmlAttribute *)attribute
                             inNodes:(NSArray *)xmlNodes {
  if (xmlNodes.count) {
    NSArray *filtered = [xmlNodes filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(name == %@ && stringValue == %@)", attribute.name, attribute.value]];
    if (filtered.count) {
      return filtered[0];
    }
  }
  
  return nil;
}

@end
