//
//  PAXmlService.h
//  PAXmlService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Cocoa/Cocoa.h>

//! Project version number for PAXmlService.
FOUNDATION_EXPORT double PAXmlServiceVersionNumber;

//! Project version string for PAXmlService.
FOUNDATION_EXPORT const unsigned char PAXmlServiceVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <PAXmlService/PublicHeader.h>

#import <PAXmlService/PAXmlServiceManager.h>
#import <PAXmlService/PAXmlWriter.h>
#import <PAXmlService/PAXmlFileProperties.h>
#import <PAXmlService/PAXmlReader.h>
#import <PAXmlService/PAXmlServiceConstants.h>
#import <PAXmlService/PAXmlAttribute.h>
