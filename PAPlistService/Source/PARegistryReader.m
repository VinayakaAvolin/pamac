//
//  PARegistryReader.m
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PARegistryReader.h"
#import "NSString+Additions.h"

#define DATA_KEY @"Data"
#define PATH_KEY @"Path"
@implementation PARegistryReader

- (instancetype)initWithPlistAtPath:(NSString *)filePath {
  return [super initWithPlistAtPath:filePath];
}

- (BOOL)isRegValueExistsWithKey:(NSString*)regKey regValue:(NSString*)regValue {
  id fileContents = [self rootDictionary];
  regKey = [self nativePathForPath:regKey];
  if ([fileContents isKindOfClass:[NSArray class]]) {
    NSArray* contents = (NSArray*)fileContents;
    for (NSDictionary* object in contents) {
      NSDictionary* data = [object valueForKey:DATA_KEY];
      NSString* key = [object valueForKey:PATH_KEY];
      key = [self nativePathForPath:key];
      if (key && [regKey containsString:key]) {
        return [[data allKeys]containsObject:regValue ];
      }
    }
  }
  
  return NO;
}

- (NSString*)regValueForPath:(NSString*)path withKey:(NSString*)regKey {
  id fileContents = [self rootDictionary];
  path = [self nativePathForPath:path];
  if ([fileContents isKindOfClass:[NSArray class]]) {
    NSArray* contents = (NSArray*)fileContents;
    for (NSDictionary* object in contents) {
      NSString* key = [object valueForKey:PATH_KEY];
      key = [self nativePathForPath:key];
      if (key && [path containsString:key]) {
        NSDictionary* data = [object valueForKey:DATA_KEY];
        if ([[data allKeys] containsObject:regKey]) {
          return [data valueForKey:regKey];
        }
        return @"";
      }
    }
  }
  return @"";
}

- (NSString*)enumRegKey:(NSString*)path delimeter:(NSString*)delimeter {
  id fileContents = [self rootDictionary];
  path = [self nativePathForPath:path];
  if ([fileContents isKindOfClass:[NSArray class]]) {
    NSArray* contents = (NSArray*)fileContents;
    for (NSDictionary* object in contents) {
      NSString* key = [object valueForKey:PATH_KEY];
      key = [self nativePathForPath:key];
      if (key && [path containsString:key]) {
        NSDictionary* data = [object valueForKey:DATA_KEY];
        NSArray* allkeys = [data allKeys];
        NSString*result = @"";
        for (int i = 0; i < allkeys.count ; i++) {
          NSString* subKey = allkeys[i];
          if(i == allkeys.count - 1) {
            result = [result stringByAppendingFormat:@"%@",subKey];
            
          }else {
            result = [result stringByAppendingFormat:@"%@%@",subKey,delimeter];
          }
        }
        return result;
      }
    }
  }
  return @"";
}
- (NSString*)nativePathForPath:(NSString*)path {
  if (path.length == 0) {
    return path;
  }
  path = [path lowercaseString];
  path = [path stringByConvertingIntoMacCompatibleFilePath];
  NSRange rg = [path rangeOfString:@"/" options:NSCaseInsensitiveSearch range:NSMakeRange(path.length -1, 1)];
  if (rg.location != NSNotFound) {
    path =[path stringByReplacingCharactersInRange:rg withString:@""];
    
  }
  return path;
}

@end
