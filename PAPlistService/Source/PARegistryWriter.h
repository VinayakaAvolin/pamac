//
//  PARegistryWriter.h
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PAPlistWriter.h"

@interface PARegistryWriter : PAPlistWriter

- (BOOL)setRegValueForPath:(NSString*)path withKey:(NSString*)regKey andData:(NSString*)regData;
- (BOOL)deleteNodeForPath: (NSString *)pathKey;

-(BOOL)deleteKey:(NSString *)key forPath: (NSString *)path;
@end
