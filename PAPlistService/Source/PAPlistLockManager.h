//
//  PAPlistLockManager.h
//  PAPlistService
//
//  Copyright © 2018 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PAPlistLockManager : NSObject

- (void) runWithReadLock: (NSString *) plistFile
               codeBlock: (void (^)(void))runWithLock;
-(void) runWithWriteLock: (NSString *) plistFile
               codeBlock: (void (^)(void))runWithLock;

+ (id)sharedInstance;
@end
