//
//  PAPlistWriter.m
//  PAPlistService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PAPlistWriter.h"
#import "PAPlistUtility.h"

@interface PAPlistWriter () {
  NSString *_plistPath;
  NSMutableDictionary *_plistDict;
}
@end

@implementation PAPlistWriter

- (instancetype)initWithPlistAtPath:(NSString *)filePath {
  self = [super init];
  if (self) {
    if (!filePath) {
      self = nil;
    } else {
      _plistPath = filePath;
      id plistContent = [PAPlistUtility plistContentAtPath:filePath];
      if (plistContent && [plistContent isKindOfClass:[NSDictionary class]]) {
        _plistDict = [(NSDictionary *)plistContent mutableCopy];
      } else if (plistContent && [plistContent isKindOfClass:[NSArray class]]) {
        _plistDict = [(NSArray *)plistContent mutableCopy];
      } else {
        self = nil;
      }
    }
  }
  return self;
}

- (void)setValue:(id)value
          forKey:(NSString *)key
       inSection:(NSString *)sectionKey {
  id parentValue = nil;
  if (sectionKey) {
    parentValue = [_plistDict valueForKey:sectionKey];
  }
  /// section present in plist
  if (parentValue) {
    if ([parentValue isKindOfClass:[NSDictionary class]]) {
      NSMutableDictionary *parentDict = [parentValue mutableCopy];
      [parentDict setValue:value forKey:key];
      [_plistDict setValue:parentDict forKey:sectionKey];
    }
  } else if (sectionKey) { // section not present; so create section in the plist
    NSMutableDictionary *parentDict = [NSMutableDictionary dictionary];
    [parentDict setValue:value forKey:key];
    [_plistDict setValue:parentDict forKey:sectionKey];
  } else { // add key-value to root
    [_plistDict setValue:value forKey:key];
  }
}

-(BOOL)synchronize {
    return [_plistDict writeToFile:_plistPath atomically:YES];
}
- (BOOL)removeKey:(NSString *)key
        inSection:(NSString *)sectionKey {
  id parentValue = nil;
  // nil check for key
  if (!key) {
    return false;
  }
  if (sectionKey) {
    parentValue = [_plistDict valueForKey:sectionKey];
  }
  
  if (parentValue) {
    if ([parentValue isKindOfClass:[NSDictionary class]]) {
      NSMutableDictionary *parentDict = [parentValue mutableCopy];
      [parentDict removeObjectForKey:key];
      [_plistDict setValue:parentDict forKey:sectionKey];
    }
  } else {
    [_plistDict removeObjectForKey:key];
  }
  return [_plistDict writeToFile:_plistPath atomically:YES];
}


- (id)rootDictionary {
  return _plistDict;
}

- (NSString*)filePath {
  return _plistPath;
}
@end
