//
//  PAPlistReader.h
//  PAPlistService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PAPlistReader : NSObject

- (instancetype)initWithPlistContent:(NSString *)content;
- (instancetype)initWithPlistAtPath:(NSString *)filePath;

/// To get value of a key under a specific section in the root dictionary; to get value of key under root dictionary; pass nil for 'sectionKey' parameter
- (id)valueForKey:(NSString *)key
        inSection:(NSString *)sectionKey;

- (id)rootDictionary;

- (id)valueForKeyPath:(NSString *)keyPath withSeparator: (NSString*)separator;
@end
