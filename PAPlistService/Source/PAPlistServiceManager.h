//
//  PAPlistServiceManager.h
//  PAPlistService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PAPlistServiceConstants.h"

@interface PAPlistServiceManager : NSObject

/// Creates plist file at input 'filePath'; the 'filePath' should include the file name
/// Newly created plist will have dictionary root element
- (BOOL)createPlistAtPath:(NSString *)filePath;
- (BOOL)createPlistAtPath:(NSString *)filePath withRootElementType:(PAPlistRootType)rootType;

/// Deletes plist file at input 'filePath'; the 'filePath' should include the file name
- (BOOL)deletePlistAtPath:(NSString *)filePath;

/// Returns plist data at file path; the content could be either array or dictionary
- (id)plistContentAtPath:(NSString *)filePath;

/// Writes plist data (either array or dictionary) to input file path;
/// The root object of plist is either an array or dictionary
- (BOOL)writePlistData:(id)plistData
                toPath:(NSString *)filePath;


@end
