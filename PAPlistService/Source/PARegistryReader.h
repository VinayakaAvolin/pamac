//
//  PARegistryReader.h
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <PAPlistService/PAPlistService.h>

@interface PARegistryReader : PAPlistReader

- (instancetype)initWithPlistAtPath:(NSString *)filePath;

- (BOOL)isRegValueExistsWithKey:(NSString*)regKey regValue:(NSString*)regValue;

- (NSString*)regValueForPath:(NSString*)path withKey:(NSString*)regKey;

- (NSString*)enumRegKey:(NSString*)path delimeter:(NSString*)delimeter;

@end
