//
//  PAPlistServiceManager.m
//  PAPlistService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <PAFileSystemService/PAFileSystemService.h>
#import "PAPlistServiceManager.h"
#import "PAPlistUtility.h"

@implementation PAPlistServiceManager

- (BOOL)createPlistAtPath:(NSString *)filePath {
  return [self createPlistAtPath:filePath withRootElementType:PAPlistRootTypeRecord];
}

- (BOOL)createPlistAtPath:(NSString *)filePath withRootElementType:(PAPlistRootType)rootType; {
  /// if file path nil then do not proceed
  if (!filePath) {
    return false;
  }
  BOOL isPlistCreated = false;
  /// first create the folder under which the file has to be created
  BOOL isFolderPresent = [self checkAndCreateTargetDirectoryForFileAtPath:filePath];
  if (!isFolderPresent) {
    return false;
  }
  switch (rootType) {
    case PAPlistRootTypeList:
    {
      NSMutableArray *emptyData = [[NSMutableArray alloc]init];
      isPlistCreated = [emptyData writeToFile:filePath atomically:YES];
    }
      break;
    default:
    {
      NSMutableDictionary *emptyData = [[NSMutableDictionary alloc]init];
      isPlistCreated = [emptyData writeToFile:filePath atomically:YES];
    }
      break;
  }
  return isPlistCreated;
}
- (BOOL)deletePlistAtPath:(NSString *)filePath {
  PAFileSystemManager *manager = [[PAFileSystemManager alloc]init];
  return [manager deleteFile:filePath];
}

- (id)plistContentAtPath:(NSString *)filePath {
  return [PAPlistUtility plistContentAtPath:filePath];
}

- (BOOL)writePlistData:(id)plistData
                toPath:(NSString *)filePath {
  if (!filePath || !plistData) {
    return false;
  }
  /// first create the folder under which the file has to be created
  BOOL isFolderPresent = [self checkAndCreateTargetDirectoryForFileAtPath:filePath];
  if (!isFolderPresent) {
    return false;
  }
  /// root object of plist is either dictionary or an array
  if ([plistData isKindOfClass:[NSDictionary class]] ||
      [plistData isKindOfClass:[NSArray class]]) {
    return [plistData writeToFile:filePath atomically:YES];
  }
  return false;
}

- (BOOL)checkAndCreateTargetDirectoryForFileAtPath:(NSString *)filePath {
  /// first create the folder under which the file has to be created
  NSString *folderPath = [filePath stringByDeletingLastPathComponent];
  PAFileSystemManager *fsManager = [[PAFileSystemManager alloc]init];
  BOOL isFolderPresent = [fsManager createDirectoryAtPath:folderPath withIntermediateDirectories:YES attributes:nil error:nil];
  return isFolderPresent;
}

@end
