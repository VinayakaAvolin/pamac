//
//  PAPlistWriter.h
//  PAPlistService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

/// class to handle plist with root element as dictionary
@interface PAPlistWriter : NSObject

- (instancetype)initWithPlistAtPath:(NSString *)filePath;

/// To set key-value pair under input section in the root dictionary; to update key-value in the root of plist; pass nil for 'sectionKey' parameter
- (void)setValue:(id)value
          forKey:(NSString *)key
       inSection:(NSString *)sectionKey;
/// Removes input key-value pair from the input section of root dictionary; to remove key-value from root dictionary; pass nil for 'sectionKey' parameter
- (BOOL)removeKey:(NSString *)key
        inSection:(NSString *)sectionKey;
-(BOOL)synchronize;
- (id)rootDictionary;

- (NSString*)filePath;
@end
