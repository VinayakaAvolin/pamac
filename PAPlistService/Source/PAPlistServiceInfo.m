//
//  PAPlistServiceInfo.m
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PAPlistServiceInfo.h"

NSString *const kPAPlistServiceInfoPlistFilePath = @"_plistFilePath";
NSString *const kPAPlistServiceInfoPath = @"_sectionPath";
NSString *const kPAPlistServiceInfoRegKey = @"_regKey";
NSString *const kPAPlistServiceInfoRegData = @"_regData";
NSString *const kPAPlistServiceInfoService = @"_service";
NSString *const kPAPlistServiceInfoServiceMode = @"_serviceMode";

@implementation PAPlistServiceInfo

+ (BOOL)supportsSecureCoding {
  return YES;
}
- (id)initWithCoder:(NSCoder *)coder {
  if ((self = [super init])) {
    // Decode the property values by key, specifying the expected class
    _plistFilePath = [coder decodeObjectOfClass:[NSString class] forKey:kPAPlistServiceInfoPlistFilePath];
    _sectionPath = [coder decodeObjectOfClass:[NSString class] forKey:kPAPlistServiceInfoPath];
    _regKey = [coder decodeObjectOfClass:[NSString class] forKey:kPAPlistServiceInfoRegKey];
    _regData = [coder decodeObjectOfClass:[NSString class] forKey:kPAPlistServiceInfoRegData];
    _service = [coder decodeIntegerForKey:kPAPlistServiceInfoService];
    _serviceMode = [coder decodeIntegerForKey:kPAPlistServiceInfoServiceMode];
  }
  return self;
}

- (void)encodeWithCoder:(NSCoder *)coder {
  // Encode our ivars using string keys as normal
  [coder encodeObject:_plistFilePath forKey:kPAPlistServiceInfoPlistFilePath];
  [coder encodeObject:_sectionPath forKey:kPAPlistServiceInfoPath];
  [coder encodeObject:_regKey forKey:kPAPlistServiceInfoRegKey];
  [coder encodeObject:_regData forKey:kPAPlistServiceInfoRegData];
  [coder encodeInteger:_service forKey:kPAPlistServiceInfoService];
  [coder encodeInteger:_serviceMode forKey:kPAPlistServiceInfoServiceMode];
}


@end
