//
//  PAPlistUtility.h
//  PAPlistService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PAPlistUtility : NSObject

+ (id)plistContentAtPath:(NSString *)filePath;
+ (BOOL)writeContent:(id)content atPath: (NSString *)filePath;
@end
