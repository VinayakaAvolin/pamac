//
//  PAPlistLockManager.m
//  PAPlistService
//
//  Copyright © 2018 Avolin. All rights reserved.
//

#import <PAFileSystemService/PAFileSystemService.h>
#import "PAPlistLockManager.h"
#import "PAPlistUtility.h"
//#import "PALogger.h"

@implementation PAPlistLockManager

static PAPlistLockManager *sharedInstance = nil;

// Get the shared instance and create it if necessary.
+ (PAPlistLockManager *)sharedInstance {
  if (sharedInstance == nil) {
    sharedInstance = [[super allocWithZone:NULL] init];
  }
  return sharedInstance;
}

- (void) runWithReadLock: (NSString *) plistFile
               codeBlock: (void (^)(void))runWithLock {
  dispatch_group_t group = dispatch_group_create();
  dispatch_group_enter(group);
  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    NSString *lockFilePath = [plistFile stringByAppendingPathExtension:@"lock"];
    NSDistributedLock *newLock = nil;
    if([[NSFileManager defaultManager] isWritableFileAtPath:lockFilePath]) {
      // [[PALogger sharedLoggerForClient:PALogClientSyncManager] debug:@"*** Read: Creating lock [%@]", plistFile];
      newLock = [NSDistributedLock lockWithPath:lockFilePath];
     // [[PALogger sharedLoggerForClient:PALogClientSyncManager] debug:@"*** Read: Locking [%@]", plistFile];
      while(![newLock tryLock]) {
        usleep(100);
      }
      // [[PALogger sharedLoggerForClient:PALogClientSyncManager] debug:@"*** Read: Locked [%@]", plistFile];
    }
    
    runWithLock();
    if(newLock != nil) {
      //   [[PALogger sharedLoggerForClient:PALogClientSyncManager] debug:@"*** Read: Unlocking [%@]", plistFile];
      [newLock unlock];
    }
    dispatch_group_leave(group);
    
  });
  // [[PALogger sharedLoggerForClient:PALogClientSyncManager] debug:@"*** Read: Waiting for lock thread [%@]", plistFile];
  dispatch_group_wait(group, DISPATCH_TIME_FOREVER);
  // [[PALogger sharedLoggerForClient:PALogClientSyncManager] debug:@"*** Read: Lock thread completed[%@]", plistFile];
}

- (void) runWithWriteLock: (NSString *) plistFile
                codeBlock: (void (^)(void))runWithLock {
  dispatch_group_t group = dispatch_group_create();
  dispatch_group_enter(group);
  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    if(![fileMgr fileExistsAtPath:[plistFile stringByDeletingLastPathComponent]]) {
      [fileMgr createDirectoryAtPath:[plistFile stringByDeletingLastPathComponent] withIntermediateDirectories:YES attributes:nil error:nil];
    }
    // [[PALogger sharedLoggerForClient:PALogClientSyncManager] debug:@"*** Write: Creating lock [%@]", plistFile];
    NSDistributedLock *newLock = [NSDistributedLock lockWithPath:[plistFile stringByAppendingPathExtension:@"lock"]];
    
    // [[PALogger sharedLoggerForClient:PALogClientSyncManager] debug:@"*** Write: Locking [%@]", plistFile];
    while(![newLock tryLock]) {
      usleep(100);
    }
    //    [self runWithReadLock:plistFile codeBlock:^{
    runWithLock();
    NSFileManager *fMgr = [NSFileManager defaultManager];

    // Since using singleton class below action not requireed
    //[fMgr copyItemAtPath:plistFile toPath:[plistFile stringByAppendingPathExtension:@"bkp"] error:nil];
    //[fMgr removeItemAtPath:plistFile error:nil];
    //[fMgr moveItemAtPath:[plistFile stringByAppendingPathExtension:@"bkp"] toPath:plistFile error:nil];
    //    }];
    // [[PALogger sharedLoggerForClient:PALogClientSyncManager] debug:@"*** Write: Unlocking [%@]", plistFile];

    [newLock unlock];
    dispatch_group_leave(group);
    
  });
  // [[PALogger sharedLoggerForClient:PALogClientSyncManager] debug:@"*** Write: Waiting for lock thread [%@]", plistFile];
  dispatch_group_wait(group, DISPATCH_TIME_FOREVER);
  // [[PALogger sharedLoggerForClient:PALogClientSyncManager] debug:@"*** Write: Lock thread completed[%@]", plistFile];
}


@end
