//
//  PARegistryWriter.m
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PARegistryWriter.h"
#import "NSString+Additions.h"

#define DATA_KEY @"Data"
#define PATH_KEY @"Path"

@implementation PARegistryWriter

- (instancetype)initWithPlistAtPath:(NSString *)filePath {
  self = [super initWithPlistAtPath:filePath];
  return self;
}

- (BOOL)setRegValueForPath:(NSString*)path withKey:(NSString*)regKey andData:(NSString*)regData {
  id fileContents = [self rootDictionary];
  if ([fileContents isKindOfClass:[NSArray class]]) {
    NSMutableArray* contents = [(NSMutableArray*)fileContents mutableCopy];
    BOOL containsPath = NO;
    for (NSDictionary* object in contents) {
      NSDictionary* data = [object valueForKey:DATA_KEY];
      NSString* key = [object valueForKey:PATH_KEY];
      path = [self nativePathForPath:path];
      key = [self nativePathForPath:key];
      if (key && [path containsString:key]) {
        NSMutableDictionary *mutableData = [data mutableCopy];
        NSMutableDictionary *mutableObject = [object mutableCopy];
        if (!mutableData) {
          mutableData = [NSMutableDictionary dictionary];
        }
        [mutableData setValue:regData forKey:regKey];
        [mutableObject setValue:mutableData forKey:DATA_KEY];
        [contents replaceObjectAtIndex:[contents indexOfObject:object] withObject:mutableObject];
        containsPath = YES;
        break;
      }
    }
    if (!containsPath) {
      NSMutableDictionary* newItem = [NSMutableDictionary dictionary];
      NSMutableDictionary* data = [NSMutableDictionary dictionary];
      [data setValue:regData forKey:regKey];
      [newItem setValue:data forKey:DATA_KEY];
      [newItem setValue:path forKey:PATH_KEY];
      [contents addObject:newItem];
    }
    return [contents writeToFile:[self filePath] atomically:YES];
  }
  return NO;
}

- (NSString*)nativePathForPath:(NSString*)path {
  if (path.length == 0) {
    return path;
  }
  path = [path lowercaseString];
  path = [path stringByConvertingIntoMacCompatibleFilePath];
  NSRange rg = [path rangeOfString:@"/" options:NSCaseInsensitiveSearch range:NSMakeRange(path.length -1, 1)];
  if (rg.location != NSNotFound) {
    path =[path stringByReplacingCharactersInRange:rg withString:@""];
    
  }
  return path;
}

/*
 Ex:
 {
 Data =     {
 ProgramRoot = "%DirUserServer%";
 };
 Path = "SOFTWARE\\SupportSoft\\ProviderList\\Experience92\\InstallPaths";
 }
 
 Here  input parameter,
 "pathKey" = "SOFTWARE\\SupportSoft\\ProviderList\\Experience92\\InstallPaths";
 
 */

- (BOOL)deleteNodeForPath: (NSString *)pathKey {
  NSString *pathToCompare = [self nativePathForPath:pathKey];
  
  id fileContents = [self rootDictionary];
  if ([fileContents isKindOfClass:[NSArray class]]) {
    NSArray* contents = [(NSArray*)fileContents copy];
    NSMutableArray *resultingContents = [NSMutableArray new];
    for (NSDictionary* eachNode in contents) {
      NSString *pathValue = eachNode[PATH_KEY];
      pathValue = [self nativePathForPath:pathValue];
      /// path value does not match
      if(![pathValue isEqualToString:pathToCompare]) {
        [resultingContents addObject:eachNode];
      }
    }
    return [resultingContents writeToFile:[self filePath] atomically:YES];
  }
  return NO;
}

/*
 Ex:
 {
 Data =     {
 ProgramRoot = "%DirUserServer%";
 };
 Path = "SOFTWARE\\SupportSoft\\ProviderList\\Experience92\\InstallPaths";
 }
 
 Here  input parameters,
 "path" = "SOFTWARE\\SupportSoft\\ProviderList\\Experience92\\InstallPaths";
 "key" = "ProgramRoot";
 
 */
-(BOOL)deleteKey:(NSString *)key forPath: (NSString *)path {
  NSString *pathToCompare = [self nativePathForPath:path];
  
  id fileContents = [self rootDictionary];
  if ([fileContents isKindOfClass:[NSArray class]]) {
    NSArray* contents = [(NSArray*)fileContents copy];
    NSMutableArray *resultingContents = [NSMutableArray new];
    for (NSDictionary* eachNode in contents) {
      NSString *pathValue = eachNode[PATH_KEY];
      pathValue = [self nativePathForPath:pathValue];
      /// path value does not match
      if(![pathValue isEqualToString:pathToCompare]) {
        [resultingContents addObject:eachNode];
      } else if ([eachNode[DATA_KEY] isKindOfClass:[NSDictionary class]])  { // matching path found; delete input "key" from Data dictionary
        NSMutableDictionary *targetNode = [eachNode mutableCopy];
        NSMutableDictionary *targetDataDict = [targetNode[DATA_KEY] mutableCopy];
        [targetDataDict removeObjectForKey:key];
        [targetNode setValue:targetDataDict forKey:DATA_KEY];
        [resultingContents addObject:targetNode];
      }
    }
    return [resultingContents writeToFile:[self filePath] atomically:YES];
  }
  
  return NO;
}

@end
