//
//  PAPlistUtility.m
//  PAPlistService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PAPlistUtility.h"

@implementation PAPlistUtility

+ (id)plistContentAtPath:(NSString *)filePath {
  id content = [NSDictionary dictionaryWithContentsOfFile:filePath];
  if (!content) {
    content = [NSArray arrayWithContentsOfFile:filePath];
  }
  return content;
}
+ (BOOL)writeContent:(id)content atPath: (NSString *)filePath {
    if (content && filePath) {
        if ([content isKindOfClass:[NSDictionary class]] || [content isKindOfClass:[NSArray class]]) {
           return  [content writeToFile:filePath atomically:YES];
        }
    }
    return NO;
}
@end
