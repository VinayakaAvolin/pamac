//
//  PAPlistServiceInfo.h
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PAPlistServiceConstants.h"

@interface PAPlistServiceInfo : NSObject <NSSecureCoding>

@property (nonatomic) NSString *plistFilePath;
/// arguments for functions
@property (nonatomic) NSString *sectionPath;
@property (nonatomic) NSString *regKey;
@property (nonatomic) NSString *regData;
@property (nonatomic) PAPlistService service;
@property (nonatomic) PAPlistServiceMode serviceMode;

@end
