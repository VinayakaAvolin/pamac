//
//  PAPlistServiceConstants.h
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifndef PAPlistServiceConstants_h
#define PAPlistServiceConstants_h

typedef enum : NSUInteger {
  PAPlistRootTypeRecord,
  PAPlistRootTypeList
} PAPlistRootType;


typedef enum : NSUInteger {
  PAPlistServiceNone,
  PAPlistServiceSetRegValue,
  PAPlistServiceDeleteNodeForPath,
  PAPlistServiceDeleteKey
} PAPlistService;

typedef enum : NSUInteger {
  PAPlistServiceModeNormal,
  PAPlistServiceModeElevated,
} PAPlistServiceMode;

#endif /* PAPlistServiceConstants_h */
