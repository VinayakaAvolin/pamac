//
//  PAPlistReader.m
//  PAPlistService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PAPlistReader.h"
#import "PAPlistUtility.h"

@interface PAPlistReader () {
  NSString *_plistPath;
  NSDictionary *_plistDict;
}

@end

@implementation PAPlistReader

- (instancetype)initWithPlistContent:(NSString *)content {
  self = [super init];
  if (self) {
    if (!content) {
      self = nil;
    } else {
      NSData* plistData = [content dataUsingEncoding:NSUTF8StringEncoding];
      NSError *error;
      NSPropertyListFormat format = NSPropertyListXMLFormat_v1_0;
      _plistDict = [NSPropertyListSerialization propertyListWithData:plistData options:NSPropertyListImmutable format:&format error:&error];
      if(!_plistDict ||
         [_plistDict isKindOfClass:[NSArray class]]){
        self = nil;
      }
    }
  }
  return self;
}
- (instancetype)initWithPlistAtPath:(NSString *)filePath {
  self = [super init];
  if (self) {
    if (!filePath) {
      self = nil;
    } else {
      _plistPath = filePath;
      id plistContent = [PAPlistUtility plistContentAtPath:filePath];
      if (plistContent && [plistContent isKindOfClass:[NSDictionary class]]) {
        _plistDict = [(NSDictionary *)plistContent mutableCopy];
      }
      else if (plistContent && [plistContent isKindOfClass:[NSArray class]]) {
        _plistDict = [(NSArray *)plistContent mutableCopy];
      }
      else{
        self = nil;
      }
    }
  }
  return self;
}

- (id)valueForKey:(NSString *)key
        inSection:(NSString *)sectionKey {
  id lookupDict = _plistDict;
  if (sectionKey) {
    lookupDict = [_plistDict valueForKey:sectionKey];
  }
  if ([lookupDict isKindOfClass:[NSDictionary class]]) {
    return [lookupDict valueForKey:key];
  }
  return nil;
}


- (id)rootDictionary {
  return _plistDict;
}

- (id)valueForKeyPath:(NSString *)keyPath withSeparator: (NSString*)separator {
    id keyValue = nil;
    NSString *keyDelimeter = @".";
    NSString *arrayIndexPrefix = @"[";
    NSString *arrayIndexPostfix = @"]";
    
    NSString *tmpKeyPath = keyPath;
    if (separator && [separator isNotEqualTo:keyDelimeter]) {
        tmpKeyPath = [tmpKeyPath stringByReplacingOccurrencesOfString:separator withString:keyDelimeter];
    }
    if([tmpKeyPath hasPrefix:keyDelimeter]){
        tmpKeyPath = [tmpKeyPath substringFromIndex:1];
    }
    
    NSDictionary *lookupDict = _plistDict;
    if ([lookupDict isKindOfClass:[NSDictionary class]]) {
        NSArray *keys = [tmpKeyPath componentsSeparatedByString:keyDelimeter];
        // key path is of the form a/[0]/[0]/[0]/b/c
        if (keys.count > 1 && [tmpKeyPath containsString:arrayIndexPrefix] && [tmpKeyPath containsString:arrayIndexPostfix] ) {
            id nextLookupValue = lookupDict;
            
            for (NSString *key in keys) {
                // key is array index
                if ([key containsString:arrayIndexPrefix] && [key containsString:arrayIndexPostfix]) {
                    NSString *tmpKeyWithoutArrayNotation = [key stringByReplacingOccurrencesOfString:arrayIndexPrefix withString:@""];
                    tmpKeyWithoutArrayNotation = [key stringByReplacingOccurrencesOfString:arrayIndexPostfix withString:@""];
                    NSInteger index = [tmpKeyWithoutArrayNotation integerValue];
                    if ([nextLookupValue isKindOfClass:[NSArray class]]) {
                        NSArray *lookupArray = nextLookupValue;
                        if (index < lookupArray.count) {
                            nextLookupValue = nextLookupValue[index];
                        }
                    }
                } else {
                    nextLookupValue = [nextLookupValue valueForKey:key];
                }
            }
            keyValue = nextLookupValue;
        } else {
            @try {
                keyValue = [lookupDict valueForKeyPath:tmpKeyPath];
            } @catch (NSException *exception) {
                keyValue = nil;
            }
        }
    }
    
    return keyValue;
}
@end
