//
//  PARepairConfigurationXmlParser.m
//  PARepairService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PARepairConfigurationXmlParser.h"

NSString *const kPARCContentSetKey = @"content-set";
NSString *const kPARCContentKey = @"content";
NSString *const kPARCFieldSetKey = @"field-set";
NSString *const kPARCFieldKey = @"field";
NSString *const kPARCNameKey = @"_name";
NSString *const kPARCTextKey = @"__text";
NSString *const kPARCValueKey = @"value";
NSString *const kPARCFilesToProtect = @"FilesToProtect";
NSString *const kPARCAppName = @"AppName";
NSString *const kPARCPropertySetKey = @"property-set";
NSString *const kPARCPropertyKey = @"property";
NSString *const kPARCTitleKey = @"scc_title";

@interface PARepairConfigurationXmlParser () {
  NSString *_inputXmlPath;
  PAServiceInfo *_serviceInfo;
  PAFileSystemManager *_fsManager;
}

@end

@implementation PARepairConfigurationXmlParser

- (instancetype)initWithServiceInfo:(PAServiceInfo *)serviceInfo {
  self = [super init];
  if (self && serviceInfo.guid && serviceInfo.version) {
    _serviceInfo = serviceInfo;
    _fsManager = [[PAFileSystemManager alloc]init];
    [self setupInputConfigurationXml];
  } else {
    self = nil;
  }
  return self;
}


- (void)parseInputConfigurationWithCompletion:(PARepairConfigurationXmlParserCompletionBlock)completionBlock {
  // NSString *defaultXmlFilePath = [fsManager localDefaultXmlFilePath];
  NSDictionary *xmlDict = [self dictionaryFromXmlAtPath:_inputXmlPath];
  if (xmlDict) {
    NSDictionary *contentSetDict = [xmlDict valueForKey:kPARCContentSetKey];
    if ([contentSetDict isKindOfClass:[NSDictionary class]]) {
      NSDictionary *contentDict = [contentSetDict valueForKey:kPARCContentKey];
      if ([contentDict isKindOfClass:[NSDictionary class]]) {
        
        NSString *appName = nil;
        NSDictionary *propertySetDict = [contentDict valueForKey:kPARCPropertySetKey];
        if ([propertySetDict isKindOfClass:[NSDictionary class]]) {
          NSArray *propertyList = [propertySetDict valueForKey:kPARCPropertyKey];
          if ([propertyList isKindOfClass:[NSArray class]]) {
            NSDictionary *titleDict = [self dictionaryMatchingName:kPARCTitleKey inList:propertyList];
            if ([titleDict isKindOfClass:[NSDictionary class]]) {
              appName = [titleDict valueForKey:kPARCTextKey];
            }
          }
        }
        
        NSDictionary *fieldSetDict = [contentDict valueForKey:kPARCFieldSetKey];
        if ([fieldSetDict isKindOfClass:[NSDictionary class]]) {
          NSArray *fieldsList = [fieldSetDict valueForKey:kPARCFieldKey];
          if ([fieldsList isKindOfClass:[NSArray class]]) {
            NSArray *filesToProtect = nil;
            // read files to protect
            NSDictionary *filesToProtectFieldDict = [self dictionaryMatchingName:kPARCFilesToProtect inList:fieldsList];
            if ([filesToProtectFieldDict isKindOfClass:[NSDictionary class]]) {
              id filesToProtectValue = [filesToProtectFieldDict valueForKey:kPARCValueKey];
              if ([filesToProtectValue isKindOfClass:[NSArray class]]) {
                filesToProtect = filesToProtectValue;
              } else if ([filesToProtectValue isKindOfClass:[NSString class]]) {
                filesToProtect = [filesToProtectValue componentsSeparatedByString:@";"];
              }else if ([filesToProtectValue isKindOfClass:[NSDictionary class]]) {
                NSString *textVal = [filesToProtectValue valueForKey:kPARCTextKey];
                filesToProtect = [textVal componentsSeparatedByString:@";"];
              }
            }
            // read app name
            //NSDictionary *appNameFieldDict = [self dictionaryMatchingName:kPARCAppName inList:fieldsList];
            //if ([appNameFieldDict isKindOfClass:[NSDictionary class]]) {
            //    appName = [appNameFieldDict valueForKey:kPARCValueKey];
            //    if (![appName isKindOfClass:[NSString class]]) {
            //        appName = nil;
            //    }
            //}
            if (filesToProtect && appName) {
              PAServiceInfo *serviceInfo = [_serviceInfo copy];
              serviceInfo.filesToProtect = filesToProtect;
              serviceInfo.applicationName = appName;
              completionBlock(true, serviceInfo);
              return;
            }
          }
        }
      }
    }
  }
  completionBlock(false, _serviceInfo);
}

- (NSDictionary *)dictionaryMatchingName:(NSString *)name inList:(NSArray *)inputList {
  if (inputList.count) {
    NSArray *filtered = [inputList filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(_name == %@)", name]];
    if (filtered.count) {
      return filtered[0];
    }
  }
  
  return nil;
}


/// constructs app specific protection xml path
/// Eg: ~/Library/Application\ Support/ProactiveAssist/Experience92/data/sprt_protection/c5aac1c8-2bb4-4574-93ed-d897c81be9cc.5/c5aac1c8-2bb4-4574-93ed-d897c81be9cc.5.xml
- (void)setupInputConfigurationXml {
  PAAppConfiguration *appConfiguration = [PAConfigurationManager appConfigurations];
  NSString *appProtectionFolderName = [NSString stringWithFormat:@"%@.%@",_serviceInfo.guid, _serviceInfo.version];
  NSString *appProtectionXmlFileName = [NSString stringWithFormat:@"%@.%@.xml",_serviceInfo.guid, _serviceInfo.version];
  
  NSString *protectXmlPath = appConfiguration.protectionFolderPath;
  protectXmlPath = [_fsManager stringByExpandingPath:protectXmlPath];
  protectXmlPath = [protectXmlPath stringByAppendingPathComponent:appProtectionFolderName];
  protectXmlPath = [protectXmlPath stringByAppendingPathComponent:appProtectionXmlFileName];
  if ([_fsManager doesFileExist:protectXmlPath]) {
    _inputXmlPath = protectXmlPath;
  }
}
@end
