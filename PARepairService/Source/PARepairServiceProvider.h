//
//  PARepairServiceProvider.h
//  PARepairService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <PACoreServices/PACoreServices.h>
#import "PAServiceProvider.h"
#import "PAAppBase.h"

extern NSString *const kPARepairScriptPath;
extern NSString *const kPAProtectedItemListScriptPath;
extern NSString *const kPARepairScriptBundleName;
extern NSString *const kPARepairScriptBundleType;

extern NSString *const kPARepairZipFileExtension;

typedef void(^PAProtectedItemCallback)(BOOL success, id result);

@interface PARepairServiceProvider : PAServiceProvider

@property (nonatomic) PAAppBase *appInfo;

- (void)archiveItemAtPath:(NSString *)rootFolderPath
                 callback:(PAServiceCallback)callback;
- (NSArray *)newLineSeperatedArrayFrom:(NSString *)stringWithNewLineDelimeter;
- (void)getSavedItemListFrom:(NSString *)rootFolderPath
                withCallback:(PAProtectedItemCallback)callback;

- (NSString *)protectionDbPath;
- (NSString *)restoreDbPath;
- (NSString *)protectionPasswordKeyFor:(NSString *)itemName;

@end
