//
//  PARestoreHistoryServiceProvider.m
//  PARepairService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PARestoreHistoryServiceProvider.h"
#import "PAServiceInfo.h"
#import "PAAppBase.h"
#import "NSString+Additions.h"
#import "PARepairConfigurationXmlParser.h"

@interface PARestoreHistoryServiceProvider () {
  PAServiceInfo *_serviceInfo;
  NSString *_baseRestoredDataPath;
  PAServiceCallback _callback;
}

@end

@implementation PARestoreHistoryServiceProvider

- (void)executeService:(PASupportService)service category:(PASupportServiceCategory)category info:(id<PAServiceInfoProtocol>)info callback:(PAServiceCallback)callback {
  _serviceInfo = (PAServiceInfo *)info;
  _callback = callback;
  /// for app protection, file paths and browser type is must
  if (!_serviceInfo.guid || !_serviceInfo.version) {
    callback(false, nil, nil);
    return;
  }
  [self parseInput];
}

- (void)getRestoredItemList {
  if (_serviceInfo.applicationName) {
    self.appInfo = [[PAAppBase alloc]initWithFilePaths:_serviceInfo.filesToProtect appName:_serviceInfo.applicationName guid:_serviceInfo.guid];
    _baseRestoredDataPath = self.appInfo.basePathToRestoredDataStorage;
  }
  if ([_baseRestoredDataPath isEmptyOrHasOnlyWhiteSpaces]) {
    _callback(false, nil, nil);
    return;
  }
  /// get list of restored items
  [self getSavedItemListFrom:_baseRestoredDataPath withCallback:^(BOOL success, id result) {
    if (success && [result isKindOfClass:[NSArray class]] && ((NSArray *)result).count) {
      NSArray *itemList = [self restoredItemInfoListFor:(NSArray *)result];
      NSArray *itemDictList = [self restoredItemInfoDictListFor:itemList];
      _callback(success, nil, itemDictList);
    } else {
      _callback(success, nil, nil);
    }
  }];
}

- (void)parseInput {
  PARepairConfigurationXmlParser *configParser = [[PARepairConfigurationXmlParser alloc] initWithServiceInfo:_serviceInfo];
  [configParser parseInputConfigurationWithCompletion:^(BOOL success, PAServiceInfo *serviceInfo) {
    if (success) {
      _serviceInfo = serviceInfo;
      [self getRestoredItemList];
    } else {
      _callback(false, nil, nil);
    }
  }];
}

- (NSArray *)restoredItemInfoListFor:(NSArray *)restoredItemFileNameList {
  NSArray *restoredItemInfoListFromDb = [self restoredItemInfoList];
  NSMutableArray *finalItemList = [[NSMutableArray alloc]init];
  for (PAProtectedItem *item in restoredItemInfoListFromDb) {
    if ([self doesRestoredItemNameList:restoredItemFileNameList containRestoredItemWithId:item.dateString]) {
      [finalItemList addObject:item];
    }
  }
  return [NSArray arrayWithArray:finalItemList];
}

- (NSArray *)restoredItemInfoDictListFor:(NSArray *)restoredItemList {
  NSMutableArray *finalItemDictList = [[NSMutableArray alloc]init];
  for (PAProtectedItem *item in restoredItemList) {
    [finalItemDictList addObject:item.dictionaryRepresentation];
  }
  return [NSArray arrayWithArray:finalItemDictList];
}


- (NSArray *)restoredItemInfoList {
  NSString *dbPath = [self restoreDbPath];
  PALocalCacheManager *cacheManager =  [[PALocalCacheManager alloc] initWithDatabase:dbPath];
  NSArray *itemList = [cacheManager cachedObjectsOfType:[PAProtectedItem class]];
  return itemList;
}

- (BOOL)doesRestoredItemNameList:(NSArray *)listOfZipFileNames
       containRestoredItemWithId:(NSString *)identifier  {
  if (listOfZipFileNames.count) {
    NSArray *filtered = [listOfZipFileNames filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF contains %@", identifier]];
    if (filtered.count) {
      return true;
    }
  }
  
  return false;
}


@end
