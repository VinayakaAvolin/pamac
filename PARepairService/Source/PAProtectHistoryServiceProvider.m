//
//  PAProtectHistoryServiceProvider.m
//  PARepairService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PAProtectHistoryServiceProvider.h"
#import "PAServiceInfo.h"
#import "PAAppBase.h"
#import "NSString+Additions.h"
#import "PARepairConfigurationXmlParser.h"

@interface PAProtectHistoryServiceProvider () {
  PAServiceInfo *_serviceInfo;
  NSString *_baseProtectedDataPath;
  PAServiceCallback _callback;
}

@end

@implementation PAProtectHistoryServiceProvider

- (void)executeService:(PASupportService)service category:(PASupportServiceCategory)category info:(id<PAServiceInfoProtocol>)info callback:(PAServiceCallback)callback {
  _serviceInfo = (PAServiceInfo *)info;
  _callback = callback;
  /// for app protection, file paths and browser type is must
  if (!_serviceInfo.guid || !_serviceInfo.version) {
    callback(false, nil, nil);
    return;
  }
  [self parseInput];
}

- (void)getProtectedItemList {
  if (_serviceInfo.applicationName) {
    self.appInfo = [[PAAppBase alloc]initWithFilePaths:_serviceInfo.filesToProtect appName:_serviceInfo.applicationName guid:_serviceInfo.guid];
    _baseProtectedDataPath = self.appInfo.basePathToProtectedDataStorage;
  }
  if ([_baseProtectedDataPath isEmptyOrHasOnlyWhiteSpaces]) {
    _callback(false, nil, nil);
    return;
  }
  /// TODO: For the time being, restore app data to the latest protected data; change it later
  /// First unzip the protected item; then copy it to app specific location by invoking restore script
  [self getSavedItemListFrom:_baseProtectedDataPath withCallback:^(BOOL success, id result) {
    if (success && [result isKindOfClass:[NSArray class]] && ((NSArray *)result).count) {
      NSArray *itemList = [self getProtectedItemInfoListFor:(NSArray *)result];
      NSArray *itemDictList = [self getProtectedItemInfoDictListFor:itemList];
      _callback(success, nil, itemDictList);
    } else {
      _callback(success, nil, nil);
    }
  }];
}

- (void)parseInput {
  PARepairConfigurationXmlParser *configParser = [[PARepairConfigurationXmlParser alloc] initWithServiceInfo:_serviceInfo];
  [configParser parseInputConfigurationWithCompletion:^(BOOL success, PAServiceInfo *serviceInfo) {
    if (success) {
      _serviceInfo = serviceInfo;
      [self getProtectedItemList];
    } else {
      _callback(false, nil, nil);
    }
  }];
}

- (NSArray *)getProtectedItemInfoListFor:(NSArray *)protectedItemFileNameList {
  NSArray *protectedItemInfoListFromDb = [self protectedItemInfoList];
  NSMutableArray *finalItemList = [[NSMutableArray alloc]init];
  for (PAProtectedItem *item in protectedItemInfoListFromDb) {
    if ([self doesProtectedItemNameList:protectedItemFileNameList containProtectedItemWithId:item.dateString]) {
      [finalItemList addObject:item];
    }
  }
  return [NSArray arrayWithArray:finalItemList];
}

- (NSArray *)getProtectedItemInfoDictListFor:(NSArray *)protectedItemList {
  NSMutableArray *finalItemDictList = [[NSMutableArray alloc]init];
  for (PAProtectedItem *item in protectedItemList) {
    [finalItemDictList addObject:item.dictionaryRepresentation];
  }
  return [NSArray arrayWithArray:finalItemDictList];
}


- (NSArray *)protectedItemInfoList {
  NSString *dbPath = [self protectionDbPath];
  PALocalCacheManager *cacheManager =  [[PALocalCacheManager alloc] initWithDatabase:dbPath];
  NSArray *itemList = [cacheManager cachedObjectsOfType:[PAProtectedItem class]];
  return itemList;
}

- (BOOL)doesProtectedItemNameList:(NSArray *)listOfZipFileNames
       containProtectedItemWithId:(NSString *)identifier  {
  if (listOfZipFileNames.count) {
    NSArray *filtered = [listOfZipFileNames filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF contains %@", identifier]];
    if (filtered.count) {
      return true;
    }
  }
  
  return false;
}


@end
