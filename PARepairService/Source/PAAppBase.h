//
//  PAAppBase.h
//  PARepairService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <PACoreServices/PACoreServices.h>
#import "PAServiceProvider.h"


@protocol PAAppBaseProtocol

- (NSArray *)filesToProtect; /// method to get list of files to protect
- (NSString *)pathToProtectedDataStorage; // method to be implemented by the subclasses to return path to the folder where protected data for the app is stored in user's Mac
- (NSString *)pathToRestoredDataStorage; // method to be implemented by the subclasses to return path to the folder where restored data for the app is stored in user's Mac
- (NSString *)basePathToProtectedDataStorage; // method to be implemented by the subclasses to return base path to the folder where protected data for the app is stored in user's Mac
- (NSString *)basePathToRestoredDataStorage; // method to be implemented by the subclasses to return base path to the folder where restored data for the app is stored in user's Mac

@optional
- (NSString *)protectedDataStorageRootDirectoryPath; /// Returns path to root folder where protected data is stored on application basis
- (NSString *)commaSeperatedListOfFilesToPotect; /// Returns comma seperated list of files to protect
- (NSString *)nameOfProtectedDataFolder; // method to get the name of the folder where app specific protected data is stored

@end


@interface PAAppBase : NSObject <PAAppBaseProtocol>

@property (nonatomic, readonly) NSArray *filePathsToProtect; // list of files/folders to protect
@property (nonatomic, readonly) NSString *protectedDataStoragePath; // path to the folder where protected data for the app is stored in user's Mac
@property (nonatomic, readonly) NSString *restoredDataStoragePath; // path to the folder where restored data for the app is stored in user's Mac
@property (nonatomic, readonly) NSUInteger maxHistoryCount; // max number of protection history to maintain; default is 10
@property (nonatomic, readonly) NSString *protectedDataFolderName; // name of the folder where protected data is stored in user's Mac
@property (nonatomic, readonly) NSString *appName; // name of the folder where protected data is stored in user's Mac
@property (nonatomic, readonly) NSString *guid; // content guid

@property (nonatomic, readonly) NSArray *listOfFilesToProtect; // list received from server

- (instancetype)initWithFilePaths:(NSArray *)fileList
                          appName:(NSString *)appName
                             guid:(NSString *)guid;

@end
