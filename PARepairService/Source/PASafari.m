//
//  PASafari.m
//  PARepairService
//
//  Created by Kavitha on 18/09/17.
//  Copyright © 2017 Aptean. All rights reserved.
//  Written under contract by Robosoft Technologies Pvt. Ltd.
//

#import "PASafari.h"


NSString *const kPASafariProtectFolderName = @"Protect";
NSString *const kPASafariRestoreFolderName = @"Restore";

@implementation PASafari

- (instancetype)initWithFilePaths:(NSArray *)fileList
                          appName:(NSString *)appName
{
    return [super initWithFilePaths:fileList appName:appName];
}
- (NSArray *)filesToProtect {
    PAFileSystemManager *fsManager = [[PAFileSystemManager alloc]init];
    NSMutableArray *expandedFileList = [NSMutableArray array];
    for (NSString *filePath in self.listOfFilesToProtect) {
        NSString *expandedPath = [fsManager stringByExpandingPath:filePath];
        [expandedFileList addObject:expandedPath];
    }
   
    if (expandedFileList.count) {
        return [NSArray arrayWithArray:expandedFileList];
    }

    return nil;
}

- (NSString *)pathToProtectedDataStorage {
    NSString *folderPath = [self basePathToProtectedDataStorage];
    folderPath = [folderPath stringByAppendingPathComponent:[self nameOfProtectedDataFolder]];
    return folderPath;
}

- (NSString *)pathToRestoredDataStorage {
    NSString *folderPath = [self basePathToRestoredDataStorage];
    folderPath = [folderPath stringByAppendingPathComponent:[self nameOfProtectedDataFolder]];
    return folderPath;
}

- (NSString *)basePathToProtectedDataStorage {
    PAFileSystemManager *fsManager = [[PAFileSystemManager alloc]init];
    NSString *folderPath = [self protectedDataStorageRootDirectoryPath];
    folderPath = [folderPath stringByAppendingPathComponent:self.appName];
    folderPath = [folderPath stringByAppendingPathComponent:kPASafariProtectFolderName];
    folderPath = [fsManager stringByExpandingPath:folderPath];
    return folderPath;
}
- (NSString *)basePathToRestoredDataStorage {
    PAFileSystemManager *fsManager = [[PAFileSystemManager alloc]init];
    NSString *folderPath = [self protectedDataStorageRootDirectoryPath];
    folderPath = [folderPath stringByAppendingPathComponent:self.appName];
    folderPath = [folderPath stringByAppendingPathComponent:kPASafariRestoreFolderName];
    folderPath = [fsManager stringByExpandingPath:folderPath];
    return folderPath;
}

@end
