//
//  PAProtectServiceProvider.m
//  PARepairService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PAProtectServiceProvider.h"
#import "PAServiceInfo.h"
#import "NSString+Additions.h"
#import "PARepairConfigurationXmlParser.h"

NSString *const kPAProtectActionType = @"Protect";

@interface PAProtectServiceProvider () {
  PAServiceInfo *_serviceInfo;
  PAServiceCallback _callback;
}

@end

@implementation PAProtectServiceProvider

- (void)executeService:(PASupportService)service category:(PASupportServiceCategory)category info:(id<PAServiceInfoProtocol>)info callback:(PAServiceCallback)callback {
  _serviceInfo = (PAServiceInfo *)info;
  _callback = callback;
  
  if (!_serviceInfo.guid || !_serviceInfo.version) {
    [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"Either guid or version is missing"];
    callback(false, nil, [self responseDict]);
    return;
  }
  [self parseInput];
}

- (void)protect {
  NSString *fileListString;
  NSString *protectedDataSavePath;
  NSUInteger maxHistory = 10;
  
  /// for app protection, file paths and app is must
  if (_serviceInfo.applicationName) {
    self.appInfo = [[PAAppBase alloc]initWithFilePaths:_serviceInfo.filesToProtect appName:_serviceInfo.applicationName guid:_serviceInfo.guid];
    fileListString = [self.appInfo commaSeperatedListOfFilesToPotect];
    maxHistory = self.appInfo.maxHistoryCount;
    protectedDataSavePath = self.appInfo.protectedDataStoragePath;
  }
  if (!fileListString) {
    [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"Files to protect is missing in the protect schema"];
    _callback(false, nil, [self responseDict]);
    return;
  }
  NSString *protectScriptPath = [[NSBundle mainBundle]pathForResource:kPARepairScriptBundleName ofType:kPARepairScriptBundleType];
  protectScriptPath = [protectScriptPath stringByAppendingPathComponent:kPARepairScriptPath];
  NSString *scriptPlaceholder = @"do shell script \"osascript %@ %@ [%@] \'%@\' %d\"";
  
  NSString *scriptString = [NSString stringWithFormat:scriptPlaceholder,protectScriptPath,kPAProtectActionType, fileListString,protectedDataSavePath,maxHistory];
  PAAppleScript *script = [[PAAppleScript alloc]initWithScriptString:scriptString];
  /// In case of app protection; first run protect script to copy the required folders/files to protect path and then password protect the protected app content
  [script runWithCallBack:^(BOOL success, NSError *error, id result) {
    if (success) {
      [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"Successfully protected app files. Now archive the protected folder."];
      
      [self archiveItemAtPath:protectedDataSavePath callback:^(BOOL success, NSError *error, id result) {
        PAProtectedItem *item = nil;
        if (success) {
          item = [self cacheProtectedItemDetail];
        }
        [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"File archive status [%d]",success];
        
        _callback(success, error, item.dictionaryRepresentation);
      }];
    } else {
      _callback(success, nil, [self responseDict]);
    }
  }];
}

- (void)parseInput {
  PARepairConfigurationXmlParser *configParser = [[PARepairConfigurationXmlParser alloc] initWithServiceInfo:_serviceInfo];
  [configParser parseInputConfigurationWithCompletion:^(BOOL success, PAServiceInfo *serviceInfo) {
    [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"Status of parsing protect schema [%d]",success];
    if (success) {
      _serviceInfo = serviceInfo;
      [self protect];
    } else {
      _callback(false, nil, [self responseDict]);
    }
  }];
}

- (PAProtectedItem *)cacheProtectedItemDetail {
  NSString *protectPath = self.appInfo.protectedDataStoragePath;
  NSString *dateString = protectPath.lastPathComponent;
  NSString *dbPath = [self protectionDbPath];
  
  PAProtectedItem *protectedItemDetail = [[PAProtectedItem alloc]initWithGuid:_serviceInfo.guid version:_serviceInfo.version date:dateString];
  PALocalCacheManager *cacheManager =  [[PALocalCacheManager alloc] initWithDatabase:dbPath];
  [cacheManager add:protectedItemDetail];
  return protectedItemDetail;
}

- (NSDictionary *)responseDict {
  NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
  [dict setValue:_serviceInfo.guid forKey:kPASupportServiceResponseKeyGuid];
  [dict setValue:_serviceInfo.version forKey:kPASupportServiceResponseKeyVersion];
  return [NSDictionary dictionaryWithDictionary:dict];
}
@end
