//
//  PARepairServiceProvider.m
//  PARepairService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PARepairServiceProvider.h"
#import "PAProtectServiceProvider.h"
#import "PARestoreServiceProvider.h"
#import "PAUndoRestoreServiceProvider.h"
#import "PAProtectHistoryServiceProvider.h"
#import "PARestoreHistoryServiceProvider.h"
#import "NSString+Additions.h"

NSString *const kPARepairScriptPath = @"Contents/Resources/Scripts/main.scpt";
NSString *const kPAProtectedItemListScriptPath = @"Contents/Resources/Scripts/protectedItemList.sh";
NSString *const kPARepairScriptBundleName = @"protect";
NSString *const kPARepairScriptBundleType = @"app";

NSString *const kPARepairZipFileExtension = @"zip";
NSString *const kPAProtectDBFolderName = @"Db";
NSString *const kPAProtectDBName = @"Protect.realm";
NSString *const kPARestoreDBName = @"Restore.realm";

@implementation PARepairServiceProvider

- (BOOL)canExecuteService:(PASupportService)service category:(PASupportServiceCategory)category {
  if (category == PASupportServiceCategoryRepair &&
      (service == PASupportServiceProtect ||
       service == PASupportServiceRestore ||
       service == PASupportServiceUndoRestore ||
       service == PASupportServiceGetProtectedHistory ||
       service == PASupportServiceGetRestoredHistory)) {
        return true;
      }
  return false;
}

- (void)executeService:(PASupportService)service category:(PASupportServiceCategory)category info:(id<PAServiceInfoProtocol>)info callback:(PAServiceCallback)callback {
  switch (service) {
    case PASupportServiceProtect:
    {
      PAProtectServiceProvider *serviceProvider = [[PAProtectServiceProvider alloc]init];
      [serviceProvider executeService:service category:category info:info callback:callback];
    }
      break;
    case PASupportServiceRestore:
    {
      PARestoreServiceProvider *serviceProvider = [[PARestoreServiceProvider alloc]init];
      [serviceProvider executeService:service category:category info:info callback:callback];
    }
      break;
    case PASupportServiceUndoRestore:
    {
      PAUndoRestoreServiceProvider *serviceProvider = [[PAUndoRestoreServiceProvider alloc]init];
      [serviceProvider executeService:service category:category info:info callback:callback];
    }
      break;
    case PASupportServiceGetProtectedHistory:
    {
      PAProtectHistoryServiceProvider *serviceProvider = [[PAProtectHistoryServiceProvider alloc]init];
      [serviceProvider executeService:service category:category info:info callback:callback];
    }
      break;
    case PASupportServiceGetRestoredHistory:
    {
      PARestoreHistoryServiceProvider *serviceProvider = [[PARestoreHistoryServiceProvider alloc]init];
      [serviceProvider executeService:service category:category info:info callback:callback];
    }
      break;
    default:
      break;
  }
}

/// Once data protection is complete; password protect and archive the protected folder.
/// MD5 hash is calculated from "protected folder path string" and used as password while creating archive of  protected folder path.
- (void)archiveItemAtPath:(NSString *)rootFolderPath
                 callback:(PAServiceCallback)callback {
  NSString *hashKey = [self protectionPasswordKeyFor:[rootFolderPath lastPathComponent]];
  NSString *folderChecksum = [hashKey sha2HashStringWithSalt:nil];
  PAFileSystemManager *fsManager = [[PAFileSystemManager alloc]init];
  
  if (!rootFolderPath ||
      ![fsManager doesDirectoryExist:rootFolderPath] ||
      !folderChecksum) {
    [fsManager deleteDirectory:rootFolderPath];
    callback(false, nil, nil);
    return;
  }
  
  NSString  *zipFilePath = [rootFolderPath stringByAppendingPathExtension:kPARepairZipFileExtension];
  PAResourceExtractor *resourceExtractor = [[PAResourceExtractor alloc]init];
  [resourceExtractor compressFile:rootFolderPath toLocation:zipFilePath password:folderChecksum completion:^(BOOL success) {
    if (success) {
      // on successful archive; remove the folder which got archived
      [fsManager deleteDirectory:rootFolderPath];
    }
    callback(success, nil, nil);
  }];
}

- (NSArray *)newLineSeperatedArrayFrom:(NSString *)stringWithNewLineDelimeter {
  NSArray *pathList = nil;
  if ([stringWithNewLineDelimeter containsString:@"\n"]) {
    pathList = [stringWithNewLineDelimeter componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    
  } else {
    pathList = @[stringWithNewLineDelimeter];
  }
  return pathList;
}

- (void)getSavedItemListFrom:(NSString *)rootFolderPath
                withCallback:(PAProtectedItemCallback)callback {
  if ([rootFolderPath isEmptyOrHasOnlyWhiteSpaces]) {
    return;
  }
  NSString *scriptPath = [[NSBundle mainBundle]pathForResource:kPARepairScriptBundleName ofType:kPARepairScriptBundleType];
  scriptPath = [scriptPath stringByAppendingPathComponent:kPAProtectedItemListScriptPath];
  
  PAShellScript *script = [[PAShellScript alloc]initWithPath:scriptPath handler:nil parameters:rootFolderPath, nil];
  [script runWithCallBack:^(BOOL success, NSError *error, id result) {
    /// output is of below form
    /*
     20-Sep-2017_11_15_59.zip
     20-Sep-2017_11_15_38.zip
     20-Sep-2017_10_59_58.zip
     20-Sep-2017_09_08_14.zip
     19-Sep-2017_05_52_24.zip
     */
    NSArray *pathList = nil;
    BOOL isSuccess = false;
    if ([result isKindOfClass:[NSString class]] && ![result isEmptyOrHasOnlyWhiteSpaces]) {
      pathList = [self newLineSeperatedArrayFrom:(NSString *)result];
      isSuccess = true;
    }
    callback(isSuccess, pathList);
    
  }];
}

- (NSString *)protectionDbPath {
  NSString *dbPath = self.appInfo.protectedDataStoragePath;
  dbPath = [dbPath stringByDeletingLastPathComponent];
  dbPath = [dbPath stringByAppendingPathComponent:kPAProtectDBFolderName];
  
  PAFileSystemManager *fsManager = [[PAFileSystemManager alloc]init];
  BOOL folderPresent = [fsManager doesDirectoryExist:dbPath];
  if (!folderPresent) {
    folderPresent = [fsManager createDirectoryAtPath:dbPath withIntermediateDirectories:NO attributes:nil error:nil];
  }
  if (folderPresent) {
    dbPath = [dbPath stringByAppendingPathComponent:kPAProtectDBName];
    return dbPath;
  }
  return nil;
}
- (NSString *)restoreDbPath {
  NSString *dbPath = self.appInfo.basePathToRestoredDataStorage;
  dbPath = [dbPath stringByAppendingPathComponent:kPAProtectDBFolderName];
  
  PAFileSystemManager *fsManager = [[PAFileSystemManager alloc]init];
  BOOL folderPresent = [fsManager doesDirectoryExist:dbPath];
  if (!folderPresent) {
    folderPresent = [fsManager createDirectoryAtPath:dbPath withIntermediateDirectories:NO attributes:nil error:nil];
  }
  if (folderPresent) {
    dbPath = [dbPath stringByAppendingPathComponent:kPARestoreDBName];
    return dbPath;
  }
  return nil;
}

- (NSString *)protectionPasswordKeyFor:(NSString *)itemName {
  PAAppConfiguration *configuration = [PAConfigurationManager appConfigurations];
  NSString *providerId = configuration.providerId;
  NSString *key = [NSString stringWithFormat:@"%@%@",providerId,itemName];
  return key;
}

@end
