//
//  PAAppBase.m
//  PARepairService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PAAppBase.h"
#import "NSDate+Additions.h"

NSUInteger const kPAMaxHistoryCount = 10;
NSString *const kPASafariProtectFolderName = @"Protect";
NSString *const kPASafariRestoreFolderName = @"Restore";

@implementation PAAppBase

- (instancetype)initWithFilePaths:(NSArray *)fileList
                          appName:(NSString *)appName
                             guid:(NSString *)guid {
  self = [super init];
  if (self) {
    if (fileList.count) {
      _listOfFilesToProtect = [NSArray arrayWithArray:fileList];
      _appName = appName;
      _guid = guid;
      _filePathsToProtect = [self filesToProtect];
      _maxHistoryCount = kPAMaxHistoryCount;
      _protectedDataStoragePath = [self pathToProtectedDataStorage];
      _restoredDataStoragePath = [self pathToRestoredDataStorage];
    } else {
      self = nil;
    }
  }
  return self;
}

- (NSArray *)filesToProtect {
  PAFileSystemManager *fsManager = [[PAFileSystemManager alloc]init];
  NSMutableArray *expandedFileList = [NSMutableArray array];
  for (NSString *filePath in self.listOfFilesToProtect) {
    NSString *expandedPath = [fsManager stringByExpandingPath:filePath];
    [expandedFileList addObject:expandedPath];
  }
  
  if (expandedFileList.count) {
    return [NSArray arrayWithArray:expandedFileList];
  }
  
  return nil;
}
- (NSString *)pathToProtectedDataStorage {
  NSString *folderPath = [self basePathToProtectedDataStorage];
  folderPath = [folderPath stringByAppendingPathComponent:[self nameOfProtectedDataFolder]];
  return folderPath;
}
- (NSString *)pathToRestoredDataStorage {
  NSString *folderPath = [self basePathToRestoredDataStorage];
  folderPath = [folderPath stringByAppendingPathComponent:[self nameOfProtectedDataFolder]];
  return folderPath;
}
- (NSString *)basePathToProtectedDataStorage {
  PAFileSystemManager *fsManager = [[PAFileSystemManager alloc]init];
  NSString *folderPath = [self protectedDataStorageRootDirectoryPath];
  folderPath = [folderPath stringByAppendingPathComponent:self.guid];
  folderPath = [folderPath stringByAppendingPathComponent:kPASafariProtectFolderName];
  folderPath = [fsManager stringByExpandingPath:folderPath];
  return folderPath;
}
- (NSString *)basePathToRestoredDataStorage {
  PAFileSystemManager *fsManager = [[PAFileSystemManager alloc]init];
  NSString *folderPath = [self protectedDataStorageRootDirectoryPath];
  folderPath = [folderPath stringByAppendingPathComponent:self.guid];
  folderPath = [folderPath stringByAppendingPathComponent:kPASafariRestoreFolderName];
  folderPath = [fsManager stringByExpandingPath:folderPath];
  return folderPath;
}

- (NSString *)commaSeperatedListOfFilesToPotect {
  return [self commaSeperatedListOfFilesFrom:_filePathsToProtect];
}

- (NSString *)commaSeperatedListOfFilesFrom:(NSArray *)fileList {
  if (!fileList) {
    return nil;
  }
  NSString *fileListString = [fileList componentsJoinedByString:@","];
  return fileListString;
}

- (NSString *)protectedDataStorageRootDirectoryPath {
  PAFileSystemManager *fsManager = [[PAFileSystemManager alloc]init];
  NSString *folderPath = [fsManager dataBackupFolderPath];
  return folderPath;
}
- (NSString *)nameOfProtectedDataFolder {
  return [[NSDate systemDate] dateStringWithFormat:Format7];
}
@end
