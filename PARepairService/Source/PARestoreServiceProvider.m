//
//  PARestoreServiceProvider.m
//  PARepairService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PARestoreServiceProvider.h"
#import "PAServiceInfo.h"
#import "NSString+Additions.h"
#import "PARepairConfigurationXmlParser.h"

NSString *const kPARestoreActionType = @"Restore";
NSString *const kPAGetLatestProtectedItemScriptPath = @"Contents/Resources/Scripts/latestProtectedItem.sh";

@interface PARestoreServiceProvider () {
  PAServiceInfo *_serviceInfo;
  NSString *_fileListString;
  NSString *_baseProtectedDataPath;
  NSString *_protectedDataPath;
  NSString *_restoreDataSavePath;
  NSString *_appName;
  PAServiceCallback _callback;
}

@end

@implementation PARestoreServiceProvider

- (void)executeService:(PASupportService)service category:(PASupportServiceCategory)category info:(id<PAServiceInfoProtocol>)info callback:(PAServiceCallback)callback {
  _serviceInfo = (PAServiceInfo *)info;
  _callback = callback;
  /// for app protection, file paths and browser type is must
  if (!_serviceInfo.guid || !_serviceInfo.version ||
      [_serviceInfo.protectedItemName isEmptyOrHasOnlyWhiteSpaces]) {
    [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"Either guid or version or protected item is missing"];
    callback(false, nil, [self responseDict]);
    return;
  }
  [self parseInput];
}
- (void)unarchiveProtectedItem:(NSString *)itemName {
  NSString *protectedFolderPathWithZipExtension = [_baseProtectedDataPath stringByAppendingPathComponent:itemName];
  NSString *protectedFolderPathWithoutZipExtension = [protectedFolderPathWithZipExtension stringByDeletingPathExtension];
  NSString *hashKey = [self protectionPasswordKeyFor:[protectedFolderPathWithoutZipExtension lastPathComponent]];
  NSString *folderChecksum = [hashKey sha2HashStringWithSalt:nil];
  
  _protectedDataPath = protectedFolderPathWithoutZipExtension;
  
  PAResourceExtractor *resourceExtractor = [[PAResourceExtractor alloc]init];
  [resourceExtractor extractFile:protectedFolderPathWithZipExtension toLocation:protectedFolderPathWithoutZipExtension password:folderChecksum completion:^(BOOL success) {
    [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"Status of unarchiving protected folder [%@] is [%d]",protectedFolderPathWithZipExtension,success];
    
    [self restoreOperation];
  }];
}

- (void)restoreOperation {
  NSString *protectScriptPath = [[NSBundle mainBundle]pathForResource:kPARepairScriptBundleName ofType:kPARepairScriptBundleType];
  protectScriptPath = [protectScriptPath stringByAppendingPathComponent:kPARepairScriptPath];
  NSString *scriptPlaceholder = @"do shell script \"osascript %@ %@ [%@] \'%@\' \'%@\' \'%@\'\"";
  NSString *scriptString = [NSString stringWithFormat:scriptPlaceholder,protectScriptPath,kPARestoreActionType, _fileListString, _protectedDataPath, _restoreDataSavePath, _appName];
  PAAppleScript *script = [[PAAppleScript alloc]initWithScriptString:scriptString];
  /// In case of app protection; first run protect script to copy the required folders/files to protect path and then password protect the protected app content
  [script runWithCallBack:^(BOOL success, NSError *error, id result) {
    [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"Status of restore is [%d]",success];
    
    if (success) {
      [self archiveItemAtPath:_restoreDataSavePath callback:^(BOOL success, NSError *error, id result) {
        [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"Status of archiving to restore folder [%@] is [%d]",_restoreDataSavePath,success];
        
        PAProtectedItem *item = [self cacheRestoredItemDetail];
        _callback(success, error, item.dictionaryRepresentation);
      }];
    } else {
      _callback(success, nil, [self responseDict]);
    }
  }];
  
}

- (void)restore {
  if (_serviceInfo.applicationName) {
    self.appInfo = [[PAAppBase alloc]initWithFilePaths:_serviceInfo.filesToProtect appName:_serviceInfo.applicationName guid:_serviceInfo.guid];
    _fileListString = [self.appInfo commaSeperatedListOfFilesToPotect];
    _appName = self.appInfo.appName;
    _baseProtectedDataPath = self.appInfo.basePathToProtectedDataStorage;
    _restoreDataSavePath = self.appInfo.restoredDataStoragePath;
  }
  if ([_fileListString isEmptyOrHasOnlyWhiteSpaces] ||
      [_appName isEmptyOrHasOnlyWhiteSpaces] ||
      [_baseProtectedDataPath isEmptyOrHasOnlyWhiteSpaces] ||
      [_restoreDataSavePath isEmptyOrHasOnlyWhiteSpaces] ||
      [_serviceInfo.protectedItemName isEmptyOrHasOnlyWhiteSpaces]) {
    _callback(false, nil, [self responseDict]);
    return;
  }
  // add .zip to portected item name of the form "23-Oct-2017_06_59_47"
  NSString *itemToRestore = [_serviceInfo.protectedItemName stringByAppendingPathExtension:kPARepairZipFileExtension];
  /// TODO: For the time being, restore app data to the latest protected data; change it later
  /// First unzip the protected item; then copy it to app specific location by invoking restore script
  [self getSavedItemListFrom:_baseProtectedDataPath withCallback:^(BOOL success, id result) {
    if (success) {
      NSArray *resultArray = (NSArray *)result;
      if ([resultArray isKindOfClass:[NSArray class]] && resultArray.count) {
        if ([resultArray containsObject:itemToRestore]) {
          [self unarchiveProtectedItem:itemToRestore];
        } else {
          _callback(false, nil, [self responseDict]);
        }
      } else {
        _callback(false, nil, [self responseDict]);
      }
    } else {
      _callback(success, nil, [self responseDict]);
    }
  }];
}

- (void)parseInput {
  PARepairConfigurationXmlParser *configParser = [[PARepairConfigurationXmlParser alloc] initWithServiceInfo:_serviceInfo];
  [configParser parseInputConfigurationWithCompletion:^(BOOL success, PAServiceInfo *serviceInfo) {
    [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"Status of parsing restore schema is [%d]",success];
    
    if (success) {
      _serviceInfo = serviceInfo;
      [self restore];
    } else {
      _callback(false, nil, [self responseDict]);
    }
  }];
}

- (PAProtectedItem *)cacheRestoredItemDetail {
  NSString *restorePath = self.appInfo.restoredDataStoragePath;
  NSString *dateString = restorePath.lastPathComponent;
  NSString *dbPath = [self restoreDbPath];
  
  PAProtectedItem *restoredItemDetail = [[PAProtectedItem alloc]initWithGuid:_serviceInfo.guid version:_serviceInfo.version date:dateString];
  PALocalCacheManager *cacheManager =  [[PALocalCacheManager alloc] initWithDatabase:dbPath];
  [cacheManager add:restoredItemDetail];
  return restoredItemDetail;
}
- (NSDictionary *)responseDict {
  NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
  [dict setValue:_serviceInfo.guid forKey:kPASupportServiceResponseKeyGuid];
  [dict setValue:_serviceInfo.version forKey:kPASupportServiceResponseKeyVersion];
  return [NSDictionary dictionaryWithDictionary:dict];
}
@end
