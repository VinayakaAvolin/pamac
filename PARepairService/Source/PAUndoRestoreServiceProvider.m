//
//  PAUndoRestoreServiceProvider.m
//  PARepairService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PAUndoRestoreServiceProvider.h"
#import "PAServiceInfo.h"
#import "NSString+Additions.h"
#import "PARepairConfigurationXmlParser.h"

NSString *const kPAUndoRestoreActionType = @"Undo";

@interface PAUndoRestoreServiceProvider () {
  PAServiceInfo *_serviceInfo;
  NSString *_fileListString;
  NSString *_baseRestoredDataPath;
  NSString *_restoredItemPath;
  NSString *_appName;
  PAServiceCallback _callback;
}

@end

@implementation PAUndoRestoreServiceProvider

- (void)executeService:(PASupportService)service category:(PASupportServiceCategory)category info:(id<PAServiceInfoProtocol>)info callback:(PAServiceCallback)callback {
  _serviceInfo = (PAServiceInfo *)info;
  _callback = callback;
  /// for app protection, file paths and browser type is must
  if (!_serviceInfo.guid || !_serviceInfo.version) {
    callback(false, nil, [self responseDict]);
    return;
  }
  [self parseInput];
}
- (void)unarchiveProtectedItem:(NSString *)itemName {
  NSString *folderPathWithZipExtension = [_baseRestoredDataPath stringByAppendingPathComponent:itemName];
  NSString *folderPathWithoutZipExtension = [folderPathWithZipExtension stringByDeletingPathExtension];
  NSString *hashKey = [self protectionPasswordKeyFor:[folderPathWithoutZipExtension lastPathComponent]];
  NSString *folderChecksum = [hashKey sha2HashStringWithSalt:nil];
  
  _restoredItemPath = folderPathWithoutZipExtension;
  
  PAResourceExtractor *resourceExtractor = [[PAResourceExtractor alloc]init];
  [resourceExtractor extractFile:folderPathWithZipExtension toLocation:folderPathWithoutZipExtension password:folderChecksum completion:^(BOOL success) {
    [self undoRestoreOperation];
  }];
}

- (void)undoRestoreOperation {
  NSString *scriptPath = [[NSBundle mainBundle]pathForResource:kPARepairScriptBundleName ofType:kPARepairScriptBundleType];
  scriptPath = [scriptPath stringByAppendingPathComponent:kPARepairScriptPath];
  NSString *scriptPlaceholder = @"do shell script \"osascript %@ %@ [%@] \'%@\' \'%@\'\"";
  NSString *scriptString = [NSString stringWithFormat:scriptPlaceholder,scriptPath,kPAUndoRestoreActionType, _fileListString, _restoredItemPath, _appName];
  PAAppleScript *script = [[PAAppleScript alloc]initWithScriptString:scriptString];
  [script runWithCallBack:^(BOOL success, NSError *error, id result) {
    _callback(success, nil, [self undoRestoreItemDetail].dictionaryRepresentation);
  }];
  
}

- (void)undoRestore {
  if (_serviceInfo.applicationName) {
    self.appInfo = [[PAAppBase alloc]initWithFilePaths:_serviceInfo.filesToProtect appName:_serviceInfo.applicationName guid:_serviceInfo.guid];
    _fileListString = [self.appInfo commaSeperatedListOfFilesToPotect];
    _appName = self.appInfo.appName;
    _baseRestoredDataPath = self.appInfo.basePathToRestoredDataStorage;
  }
  if ([_fileListString isEmptyOrHasOnlyWhiteSpaces] ||
      [_appName isEmptyOrHasOnlyWhiteSpaces] ||
      [_baseRestoredDataPath isEmptyOrHasOnlyWhiteSpaces]) {
    _callback(false, nil, [self responseDict]);
    return;
  }
  /// First unzip the restored item; then copy it to app specific location by invoking undo restore script
  [self getSavedItemListFrom:_baseRestoredDataPath withCallback:^(BOOL success, id result) {
    if (success) {
      if ([result isKindOfClass:[NSArray class]] && ((NSArray *)result).count) {
        [self unarchiveProtectedItem:((NSArray *)result).firstObject];
      } else {
        _callback(false, nil, [self responseDict]);
      }
    } else {
      _callback(success, nil, [self responseDict]);
    }
  }];
}
- (void)parseInput {
  PARepairConfigurationXmlParser *configParser = [[PARepairConfigurationXmlParser alloc] initWithServiceInfo:_serviceInfo];
  [configParser parseInputConfigurationWithCompletion:^(BOOL success, PAServiceInfo *serviceInfo) {
    [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"Status of parsing undo restore schema is [%d]",success];
    if (success) {
      _serviceInfo = serviceInfo;
      [self undoRestore];
    } else {
      _callback(false, nil, [self responseDict]);
    }
  }];
}


- (PAProtectedItem *)undoRestoreItemDetail {
  PAProtectedItem *protectedItemDetail = [[PAProtectedItem alloc]initWithGuid:_serviceInfo.guid version:_serviceInfo.version date:@""];
  return protectedItemDetail;
}
- (NSDictionary *)responseDict {
  NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
  [dict setValue:_serviceInfo.guid forKey:kPASupportServiceResponseKeyGuid];
  [dict setValue:_serviceInfo.version forKey:kPASupportServiceResponseKeyVersion];
  return [NSDictionary dictionaryWithDictionary:dict];
}


@end
