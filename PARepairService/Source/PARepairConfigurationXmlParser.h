//
//  PARepairConfigurationXmlParser.h
//  PARepairService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <PACoreServices/PACoreServices.h>
#import "PAServiceInfo.h"

typedef void(^PARepairConfigurationXmlParserCompletionBlock)(BOOL success, PAServiceInfo *serviceInfo);

@interface PARepairConfigurationXmlParser : PAXmlParser

- (instancetype)initWithServiceInfo:(PAServiceInfo *)serviceInfo;

- (void)parseInputConfigurationWithCompletion:(PARepairConfigurationXmlParserCompletionBlock)completionBlock;

@end
