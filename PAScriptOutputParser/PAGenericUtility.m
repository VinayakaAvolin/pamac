//
//  PAGenericUtility.m
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PAGenericUtility.h"

NSString *const kPAOPPropertyDelimeter = @"%Delimeter%";

NSString *const kPAOPValueTypeString = @"string";
NSString *const kPAOPValueTypeInteger = @"integer";
NSString *const kPAOPValueTypeArray = @"array";
NSString *const kPAOPValueTypeUnknown = @"unknown";

@implementation PAGenericUtility

+ (NSString *)propertyXmlForKey:(NSString *)key value:(id)value
{
  NSString *valueType = kPAOPValueTypeUnknown;
  if ([value isKindOfClass:[NSString class]]) {
    valueType = kPAOPValueTypeString;
  } else if ([value isKindOfClass:[NSNumber class]]) {
    valueType = kPAOPValueTypeInteger;
  } else if ([value isKindOfClass:[NSArray class]]) {
    valueType = kPAOPValueTypeArray;
  }
  
  NSMutableString *xml = [[NSMutableString alloc] initWithString:@""];
  
  [xml appendString:[NSString stringWithFormat:@"<PROPERTY NAME=\"%@\" TYPE=\"%@\">\n", key,valueType]];
  if ([value isKindOfClass:[NSNumber class]] || [value isKindOfClass:[NSString class]]) {
    [xml appendString:[NSString stringWithFormat:@"<VALUE>%@</VALUE>\n", value]];
  } else if ([value isKindOfClass:[NSArray class]]) {
    NSArray *valueArray = (NSArray *)value;
    for (id entry in valueArray) {
      [xml appendString:[NSString stringWithFormat:@"<VALUE>%@</VALUE>\n", entry]];
    }
  }
  [xml appendString:@"</PROPERTY>\n"];
  
  NSString *finalxml=[xml stringByReplacingOccurrencesOfString:@"&" withString:@"&amp;"];
  return finalxml;
}


@end
