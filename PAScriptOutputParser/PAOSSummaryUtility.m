//
//  PAOSSummaryUtility.m
//  PAScriptOutputParser
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PAOSSummaryUtility.h"
#import "PAOSInfo.h"
#import "NSProcessInfo+Additions.h"

// system info keys
NSString *const kPAOPOSName = @"OSName";
NSString *const kPAOPOSCodeName = @"CodeName";
NSString *const kPAOPOSVersion = @"version";

@implementation PAOSSummaryUtility

+ (NSString *)osInfoXml {
  PAOSSummaryUtility *utility = [[PAOSSummaryUtility alloc]init];
  PAOSInfo *osInfo = [utility osInfo];
  return [utility processOsInfoOutput:osInfo];
}

- (NSString *)processOsInfoOutput:(PAOSInfo *)osInfo {
  NSString *xmlElementStr = @"";
  NSString *osName = osInfo.name;
  NSString *codeName = osInfo.codeName;
  NSString *version = osInfo.version;
  if (osName) {
    xmlElementStr = [xmlElementStr stringByAppendingString:[PAGenericUtility propertyXmlForKey:kPAOPOSName value:osName]];
  }
  if (codeName) {
    xmlElementStr = [xmlElementStr stringByAppendingString:[PAGenericUtility propertyXmlForKey:kPAOPOSCodeName value:codeName]];
  }
  if (version) {
    xmlElementStr = [xmlElementStr stringByAppendingString:[PAGenericUtility propertyXmlForKey:kPAOPOSVersion value:version]];
  }
  return xmlElementStr;
}

- (PAOSInfo *)osInfo {
  NSOperatingSystemVersion osVersion = [[NSProcessInfo processInfo] operatingSystemVersion];
  NSString *version = [NSString stringWithFormat:@"%ld.%ld.%ld",osVersion.majorVersion,osVersion.minorVersion,osVersion.patchVersion];
  return [[PAOSInfo alloc]initWithName:@"Mac OS X" codeName:[NSProcessInfo osCodeName] version:version];
}

@end
