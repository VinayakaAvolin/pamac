//
//  PAScriptOutputParserConstants.h
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifndef PAScriptOutputParserConstants_h
#define PAScriptOutputParserConstants_h

typedef enum {
  PAPlistOutputParserCommandUnknown,
  PAPlistOutputParserCommandHWInfo,
  PAPlistOutputParserCommandPrinters,
  PAPlistOutputParserCommandNetworkInfo,
  PAPlistOutputParserCommandHardDiskInfo,
  PAPlistOutputParserCommandInstalledSoftwares,
  PAPlistOutputParserCommandOsName,
  PAPlistOutputParserCommandOsVersion,
  PAPlistOutputParserCommandFirewallState,
  PAPlistOutputParserCommandDefaultEmailClient,
  PAPlistOutputParserCommandDefaultBrowserApp,
  PAPlistOutputParserCommandRunningPrograms,
  PAPlistOutputParserCommandProcessEnvironmentVariables,
  PAPlistOutputParserCommandUserInfo,
  PAPlistOutputParserCommandSystemInfo,
  PAPlistOutputParserCommandOSSummary,
  PAPlistOutputParserCommandSetUserRegistry
} PAPlistOutputParserCommand;
/// order of elements in the kPAPlistOutputParserCommandArray should match the PAPlistOutputParserCommand enum order
#define kPAPlistOutputParserCommandArray @"unknown", @"hwInfo", @"printers", @"nwInfo", @"hardDiskInfo", @"installedSoftwares", @"osName",  @"osVersion",  @"firewallState",  @"defaultEmailClient",  @"defaultBrowserApp", @"runningPrograms", @"environmentVariables", @"userInfo", @"systemInfo", @"osSummary", @"setUserRegistry", nil


#endif /* PAScriptOutputParserConstants_h */
