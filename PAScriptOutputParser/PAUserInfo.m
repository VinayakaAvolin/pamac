//
//  PAUserInfo.m
//  PAScriptOutputParser
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PAUserInfo.h"

@implementation PAUserInfo

- (instancetype)initWithName: (NSString*)name fullName: (NSString*)fullName  {
  self = [super init];
  if (self) {
    _name = name;
    _fullName = fullName;
  }
  return self;
}
- (NSString *)description {
  return [NSString stringWithFormat:@"User info -> name: %@\r; full name: %@\r", _name, _fullName];
}

@end
