//
//  PASystemInfoUtility.m
//  PAScriptOutputParser
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <SystemConfiguration/SCNetworkConfiguration.h>
#import <IOKit/ps/IOPowerSources.h>
#import "PASystemInfoUtility.h"
#import "PASystemInfo.h"
#import "NSHost+Additions.h"

// system info keys
NSString *const kPAOPComputerName = @"ComputerName";
NSString *const kPAOPSystemType = @"SystemType";
NSString *const kPAOPSystemLanguage = @"SystemLanguage";
NSString *const kPAOPNetworkDomainName = @"NetworkDomainName";

NSString *const kPAOPSMBKey = @"State:/Network/Global/SMB";
NSString *const kPAOPWorkgroupKey = @"Workgroup";
NSString *const kPAOPWorkgroupNameKey = @"GetWorkgroup";
NSString *const kPAOPDesktop = @"Desktop";
NSString *const kPAOPLaptop = @"Laptop";

@implementation PASystemInfoUtility

+ (NSString *)systemInfoXml {
  PASystemInfoUtility *utility = [[PASystemInfoUtility alloc]init];
  PASystemInfo *sysInfo = [utility systemInfo];
  return [utility processSystemInfoOutput:sysInfo];
}

- (NSString *)processSystemInfoOutput:(PASystemInfo *)systemInfo {
  NSString *xmlElementStr = @"";
  NSString *systemName = systemInfo.computerName;
  NSString *systemtype = systemInfo.systemType;
  NSString *language = systemInfo.language;
  NSString *domain = systemInfo.domainName;
  if (systemName) {
    xmlElementStr = [xmlElementStr stringByAppendingString:[PAGenericUtility propertyXmlForKey:kPAOPComputerName value:systemName]];
  }
  if (systemtype) {
    xmlElementStr = [xmlElementStr stringByAppendingString:[PAGenericUtility propertyXmlForKey:kPAOPSystemType value:systemtype]];
  }
  if (language) {
    xmlElementStr = [xmlElementStr stringByAppendingString:[PAGenericUtility propertyXmlForKey:kPAOPSystemLanguage value:language]];
  }
  if (domain) {
    xmlElementStr = [xmlElementStr stringByAppendingString:[PAGenericUtility propertyXmlForKey:kPAOPNetworkDomainName value:domain]];
  }
  return xmlElementStr;
}


- (PASystemInfo *)systemInfo {
  PASystemInfoUtility *infoUtility = [[PASystemInfoUtility alloc]init];
  return [[PASystemInfo alloc]initWithComputerName:[NSHost computerName] language:[infoUtility languageName] systemType:[infoUtility systemType] domainName:[infoUtility networkDomainName]];
}

- (NSString*)systemType {
  CFTimeInterval time = IOPSGetTimeRemainingEstimate();
  if (time == kIOPSTimeRemainingUnlimited) {
    return kPAOPDesktop;
  } else {
    return kPAOPLaptop;
  }
}

- (NSString*)languageName {
  NSString* languageCode = [[NSLocale currentLocale] objectForKey:NSLocaleLanguageCode];
  NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:languageCode];
  return [locale displayNameForKey:NSLocaleIdentifier value:languageCode];
}

- (NSString *)networkDomainName {
  NSString *domainName = @"";
  SCDynamicStoreRef storeRef = SCDynamicStoreCreate(NULL, (CFStringRef)kPAOPWorkgroupNameKey, NULL, NULL);
  if (storeRef != NULL) {
    CFPropertyListRef global = SCDynamicStoreCopyValue (storeRef,(CFStringRef)kPAOPSMBKey);
    if (global != NULL) {
      id workgroup = [(__bridge NSDictionary *)global valueForKey:kPAOPWorkgroupKey];
      if ([workgroup isKindOfClass:[NSString class]]) {
        domainName = (NSString *)workgroup;
      }
      domainName = kPAOPWorkgroupKey.uppercaseString;
      CFRelease(global);
    }
    CFRelease(storeRef);
  }
  return domainName;
}

@end
