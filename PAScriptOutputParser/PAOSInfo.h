//
//  PAOSInfo.h
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PAOSInfo : NSObject

@property NSString *name;
@property NSString *codeName;
@property NSString *version;

- (instancetype)initWithName:(NSString*)name
                    codeName:(NSString*)codeName
                     version:(NSString*)version;
@end
