//
//  main.m
//  PAScriptOutputParser
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <PACoreServices/PACoreServices.h>
#import "PAPlistOutputParser.h"

int main(int argc, const char * argv[]) {
  @autoreleasepool {
    // insert code here...
    [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"The PAScriptOutputParser is launched with params [%@]",[[NSProcessInfo processInfo] arguments]];
    
    // NSString *content = [NSString stringWithContentsOfFile:@"/Users/username/Desktop/Test1/test.xml" encoding:NSASCIIStringEncoding error:&error];
    
    NSArray *arguments = [[NSProcessInfo processInfo] arguments];
    //        NSArray *arguments = @[@"path", @"novalue", @"setUserRegistry", @"software/supportsoft/providerlist/%PROVIDERPATH%/dynamicagent/users/%USER%/ss_config/global", @"Enable_Survey", @"true"];
    //        NSArray *arguments = @[@"path", @"data", @"environmentVariables"];
    PAPlistOutputParser *parser = [[PAPlistOutputParser alloc]initWithPlistArguments:arguments];
    NSString *output = [parser processOutput];
    [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"The PAScriptOutputParser output [%@]",output];
    printf( "%s", [output UTF8String] );
  }
  return 0;
}
