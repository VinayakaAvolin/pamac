//
//  PAOSInfo.m
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PAOSInfo.h"

@implementation PAOSInfo

- (instancetype)initWithName:(NSString*)name
                    codeName:(NSString*)codeName
                     version:(NSString*)version
{
  self = [super init];
  if (self) {
    _name = name;
    _version = version;
    _codeName = codeName;
  }
  return self;
}
@end
