//
//  PAPlistOutputParser.h
//  PAScriptOutputParser
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PAPlistOutputParser : NSObject

- (instancetype)initWithPlistArguments:(NSArray *)args;
- (NSString *)processOutput;

@end
