//
//  PASystemInfo.m
//  PAScriptOutputParser
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PASystemInfo.h"

@implementation PASystemInfo

- (instancetype)initWithComputerName:(NSString*)name
                            language:(NSString*)lang
                          systemType:(NSString*)type
                          domainName:(NSString*)domain {
  self = [super init];
  if (self) {
    _systemType = type;
    _language = lang;
    _computerName = name;
    _domainName = domain;
  }
  return self;
}
@end
