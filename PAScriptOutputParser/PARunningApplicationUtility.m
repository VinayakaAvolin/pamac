//
//  PARunningApplicationUtility.m
//  PAScriptOutputParser
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <AppKit/AppKit.h>
#import "PARunningApplicationUtility.h"
#include <sys/sysctl.h>
#include <pwd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <libproc.h>

#import "PAProcessInfo.h"

NSString *const kPARABundleVersion = @"CFBundleVersion";
NSString *const kPARABundleShortversion = @"CFBundleShortVersionString";

// Running process keys
NSString *const kPAOPProcessName = @"ProcessName";
NSString *const kPAOPProcessId = @"ProcessId";
NSString *const kPAOPProcessPath = @"Path";
NSString *const kPAOPProcessVersion = @"Version";
NSString *const kPAOPProcessUserName = @"UserName";

typedef struct kinfo_proc kinfo_proc;

static int GetBSDProcessList(kinfo_proc **procList, size_t *procCount)
// Returns a list of all BSD processes on the system.  This routine
// allocates the list and puts it in *procList and a count of the
// number of entries in *procCount.  You are responsible for freeing
// this list (use "free" from System framework).
// On success, the function returns 0.
// On error, the function returns a BSD errno value.
{
  int                 err;
  kinfo_proc *        result;
  bool                done;
  static const int    name[] = { CTL_KERN, KERN_PROC, KERN_PROC_ALL, 0 };
  // Declaring name as const requires us to cast it when passing it to
  // sysctl because the prototype doesn't include the const modifier.
  size_t              length;
  
  //    assert( procList != NULL);
  //    assert(*procList == NULL);
  //    assert(procCount != NULL);
  
  *procCount = 0;
  
  // We start by calling sysctl with result == NULL and length == 0.
  // That will succeed, and set length to the appropriate length.
  // We then allocate a buffer of that size and call sysctl again
  // with that buffer.  If that succeeds, we're done.  If that fails
  // with ENOMEM, we have to throw away our buffer and loop.  Note
  // that the loop causes use to call sysctl with NULL again; this
  // is necessary because the ENOMEM failure case sets length to
  // the amount of data returned, not the amount of data that
  // could have been returned.
  
  result = NULL;
  done = false;
  do {
    assert(result == NULL);
    
    // Call sysctl with a NULL buffer.
    
    length = 0;
    err = sysctl( (int *) name, (sizeof(name) / sizeof(*name)) - 1,
                 NULL, &length,
                 NULL, 0);
    if (err == -1) {
      err = errno;
    }
    
    // Allocate an appropriately sized buffer based on the results
    // from the previous call.
    
    if (err == 0) {
      result = malloc(length);
      if (result == NULL) {
        err = ENOMEM;
      }
    }
    
    // Call sysctl again with the new buffer.  If we get an ENOMEM
    // error, toss away our buffer and start again.
    
    if (err == 0) {
      err = sysctl( (int *) name, (sizeof(name) / sizeof(*name)) - 1,
                   result, &length,
                   NULL, 0);
      if (err == -1) {
        err = errno;
      }
      if (err == 0) {
        done = true;
      } else if (err == ENOMEM) {
        assert(result != NULL);
        free(result);
        result = NULL;
        err = 0;
      }
    }
  } while (err == 0 && ! done);
  
  // Clean up and establish post conditions.
  
  if (err != 0 && result != NULL) {
    free(result);
    result = NULL;
  }
  *procList = result;
  if (err == 0) {
    *procCount = length / sizeof(kinfo_proc);
  }
  
  assert( (err == 0) == (*procList != NULL) );
  
  return err;
}

@implementation PARunningApplicationUtility

+ (NSString *)runningAppsXml {
  PARunningApplicationUtility *utility = [[PARunningApplicationUtility alloc]init];
  NSArray *runningApps = [utility getListOfRunningApps];
  return [utility processRunningProgramsOutput:runningApps];
}

- (NSString *)processRunningProgramsOutput:(NSArray *)listOfRunningApps {
  NSString *xmlElementStr = @"";
  NSUInteger totalRecords = listOfRunningApps.count;
  NSUInteger index = 0;
  for (PAProcessInfo *processInfo in listOfRunningApps) {
    BOOL isvalidRecord = false;
    if (processInfo.name) {
      isvalidRecord = true;
      xmlElementStr = [xmlElementStr stringByAppendingString:[PAGenericUtility propertyXmlForKey:kPAOPProcessName value:processInfo.name]];
    }
    if (processInfo.pId) {
      isvalidRecord = true;
      xmlElementStr = [xmlElementStr stringByAppendingString:[PAGenericUtility propertyXmlForKey:kPAOPProcessId value:processInfo.pId]];
    }
    if (processInfo.userName) {
      isvalidRecord = true;
      xmlElementStr = [xmlElementStr stringByAppendingString:[PAGenericUtility propertyXmlForKey:kPAOPProcessUserName value:processInfo.userName]];
    }
    if (processInfo.path) {
      isvalidRecord = true;
      xmlElementStr = [xmlElementStr stringByAppendingString:[PAGenericUtility propertyXmlForKey:kPAOPProcessPath value:processInfo.path]];
    }
    if (processInfo.version) {
      isvalidRecord = true;
      xmlElementStr = [xmlElementStr stringByAppendingString:[PAGenericUtility propertyXmlForKey:kPAOPProcessVersion value:processInfo.version]];
    }
    index++;
    if (isvalidRecord && index < totalRecords) {
      xmlElementStr = [xmlElementStr stringByAppendingString:kPAOPPropertyDelimeter];
    }
  }
  return xmlElementStr;
}
- (NSArray*)getListOfRunningApps
{
  kinfo_proc *mylist =NULL;
  size_t mycount = 0;
  GetBSDProcessList(&mylist, &mycount);
  
  NSMutableArray *processes = [NSMutableArray arrayWithCapacity:(int)mycount];
  
  for (int i = 0; i < mycount; i++) {
    struct kinfo_proc *currentProcess = &mylist[i];
    struct passwd *user = getpwuid(currentProcess->kp_eproc.e_ucred.cr_uid);
    pid_t processId = currentProcess->kp_proc.p_pid;
    NSNumber *processID = [NSNumber numberWithInt:processId];
    NSString *processName = [NSString stringWithFormat: @"%s",currentProcess->kp_proc.p_comm];
    uid_t userID = currentProcess->kp_eproc.e_ucred.cr_uid;
    NSString *userName = [NSString stringWithFormat: @"%s",user->pw_name];
    //To get executable path
    NSString *executablePath = nil;
    char path[MAXPATHLEN+1] = {0};
    int ret = proc_pidinfo(processId, PROC_PIDPATHINFO, 0,
                           &path, sizeof(path));
    if (ret >= 0) {
      executablePath = [NSString stringWithCString:path encoding:NSUTF8StringEncoding];
    }
    
    NSRunningApplication * app = [NSRunningApplication
                                  runningApplicationWithProcessIdentifier:processId];
    NSString *bundlePath = app.bundleURL.path;
    NSString *processPath = nil;
    NSString *version = nil;
    /// get process path
    if (bundlePath) {
      processPath = bundlePath;
    } else if (executablePath) {
      processPath = executablePath;
    }
    
    /// get process version
    if (app.bundleURL) {
      NSBundle *bundle = [NSBundle bundleWithURL:app.bundleURL];
      NSDictionary *info = [bundle infoDictionary];
      NSString *versionStr = info[kPARABundleVersion];
      NSString *shortVersionStr = info[kPARABundleShortversion];
      version = [NSString stringWithFormat:@"%@ (%@)", shortVersionStr, versionStr];
    }
    
    
    PAProcessInfo *processInfo = [[PAProcessInfo alloc]initWithName:processName pId:(NSNumber *)processID userId:userID userName:userName processPath:processPath version:version];
    if (processInfo) {
      [processes addObject:processInfo];
    }
  }
  free(mylist);
  
  return [NSArray arrayWithArray:processes];
}

@end
