//
//  PAProcessInfo.m
//  PAScriptOutputParser
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PAProcessInfo.h"


@implementation PAProcessInfo

- (instancetype)initWithName:(NSString *)name
                         pId:(NSNumber *)pId
                      userId:(NSInteger)uId
                    userName:(NSString *)userName
                 processPath:(NSString *)path
                     version:(NSString *)version {
  self = [super init];
  if (self) {
    if (name) {
      _name = name;
      _path = path;
      _pId = pId;
      _userId = @(uId);
      _userName = userName;
      _version = version;
    } else {
      self = nil;
    }
  }
  return self;
}
- (NSString *)description {
  return [NSString stringWithFormat:@"{\nApp info ->\n name: %@\n process id: %@\n path: %@\n user id: %@\n user name: %@ version: %@\n}", _name, _pId, _path, _userId, _userName, _version];
}
@end
