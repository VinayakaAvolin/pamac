//
//  PAGenericUtility.h
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *const kPAOPPropertyDelimeter;

@interface PAGenericUtility : NSObject

+ (NSString *)propertyXmlForKey:(NSString *)key value:(id)value;

@end
