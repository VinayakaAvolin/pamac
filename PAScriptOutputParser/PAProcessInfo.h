//
//  PAProcessInfo.h
//  PAScriptOutputParser
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AppKit/AppKit.h>

@interface PAProcessInfo : NSObject

@property NSString  *name;
@property NSNumber *pId;
@property NSNumber *userId;
@property NSString *userName;
@property NSString *path;
@property NSString *version;

- (instancetype)initWithName:(NSString *)name
                         pId:(NSNumber *)pId
                      userId:(NSInteger)uId
                    userName:(NSString *)userName
                 processPath:(NSString *)path
                     version:(NSString *)version;

@end
