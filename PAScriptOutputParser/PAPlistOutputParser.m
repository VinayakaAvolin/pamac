//
//  PAPlistOutputParser.m
//  PAScriptOutputParser
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <AppKit/AppKit.h>
#import "PAPlistOutputParser.h"
#import "NSString+Additions.h"
#import "PARunningApplicationUtility.h"
#import "PAUserInfoUtility.h"
#import "PASystemInfoUtility.h"
#import "PAGenericUtility.h"
#import "PAOSSummaryUtility.h"
#import "PARegistryUpdater.h"
#import "PAScriptOutputParserConstants.h"

NSString *const kPAOPItemsKey = @"_items";
// HW info keys
NSString *const kPAOPHwUUID = @"platform_UUID";
NSString *const kPAOPPhysicalMemory = @"physical_memory";
NSString *const kPAOPMachineName = @"machine_name";
NSString *const kPAOPCpuType = @"cpu_type";
NSString *const kPAOPCurrentProcessorSpeed = @"current_processor_speed";
NSString *const kPAOPMachineModel = @"machine_model";
NSString *const kPAOPNoOfProcessors = @"number_processors";
NSString *const kPAOPSerialNumber = @"serial_number";

// HW info keys
NSString *const kPAOPPrinterName = @"_name";
NSString *const kPAOPPrinterDriver = @"driver";
NSString *const kPAOPPrinterIsDefault = @"default";
NSString *const kPAOPPrintServer = @"printserver";
NSString *const kPAOPPrinterDriverName = @"cups";

// Network info keys
NSString *const kPAOPIPAddress = @"ip_address";
NSString *const kPAOPIPV4 = @"IPv4";
NSString *const kPAOPAddresses = @"Addresses";
NSString *const kPAOPIPV6 = @"IPv6";
NSString *const kPAOPIDNS= @"DNS";
NSString *const kPAOPIDNSName = @"DomainName";
NSString *const kPAOPIDNSDomainName = @"DNS Domain Name";

// Storage info keys
NSString *const kPAOPVolumeName = @"_name";
NSString *const kPAOPVolumeSize = @"size_in_bytes";
NSString *const kPAOPVolumeAvailableSize = @"free_space_in_bytes";
NSString *const kPAOPVolumePhysicalDrive = @"physical_drive";
NSString *const kPAOPVolumeIsInternalDisk = @"is_internal_disk";
NSString *const kPAOPVolumeIsInternalDiskValueYes = @"yes";

// Installed software info keys
NSString *const kPAOPInstalledSoftwareName = @"_name";
NSString *const kPAOPInstalledSoftwareDisplayName = @"DisplayName";
NSString *const kPAOPInstalledSoftwareVersion = @"version";
NSString *const kPAOPInstalledSoftwarePath = @"path";

// default app keys
NSString *const kPAOPBundleNameKey = @"CFBundleName";
NSString *const kPAOPMailToUrlSchema = @"mailto:";
NSString *const kPAOPHttpUrlSchema = @"http:";

//Environment Keys
NSString *const kPAEnvKey = @"EnvKey";
NSString *const kPAEnvVal = @"EnvVal";


@interface PAPlistOutputParser ()

@property (nonatomic, assign) PAPlistOutputParserCommand commandEnumValue;
@property (nonatomic) NSString *commandString;
@property (nonatomic) NSString *propertyName;
@property (nonatomic) id dataToParse;
@property (nonatomic) PARegistryUpdater *registryUpdater;

@end

@implementation PAPlistOutputParser

- (instancetype)initWithPlistArguments:(NSArray *)args {
  self = [super init];
  if (self) {
    if (args.count < 3) {
      self = nil;
    } else {
      _commandString = args[2];
      
      id scriptOutput = nil;
      PAPlistOutputParserCommand cmd = self.commandEnumValue;
      switch (cmd) {
          /// in case of "installedSoftwares"; second argument is file path
        case PAPlistOutputParserCommandInstalledSoftwares:
          scriptOutput = [NSString stringWithContentsOfFile:args[1] encoding:NSASCIIStringEncoding error:nil];
          break;
        case PAPlistOutputParserCommandOsName:
        case PAPlistOutputParserCommandOsVersion:
        case PAPlistOutputParserCommandFirewallState:
        case PAPlistOutputParserCommandDefaultBrowserApp:
        case PAPlistOutputParserCommandDefaultEmailClient:
          if (args.count == 4) {
            _propertyName = args[3];
            _dataToParse = args[1];
          }
          break;
        case PAPlistOutputParserCommandSetUserRegistry:
          _registryUpdater = [[PARegistryUpdater alloc] initWithCommand:cmd arguments:[args subarrayWithRange:NSMakeRange(3, args.count-3)]];
          break;
        default:
          scriptOutput = args[1];
          break;
      }
      
      if (!_dataToParse && scriptOutput) {
        NSData* dataToParse = [scriptOutput dataUsingEncoding:NSUTF8StringEncoding];
        NSError *error;
        NSPropertyListFormat format = NSPropertyListXMLFormat_v1_0;
        _dataToParse = [NSPropertyListSerialization propertyListWithData:dataToParse options:NSPropertyListImmutable format:&format error:&error];
        
      }
      
      if (!_dataToParse && cmd != PAPlistOutputParserCommandSetUserRegistry) {
        self = nil;
      }
    }
  }
  return self;
}


- (NSString *)processOutput {
  switch (self.commandEnumValue) {
    case PAPlistOutputParserCommandHWInfo:
      return [self processHwInfoOutput];
      break;
    case PAPlistOutputParserCommandPrinters:
      return [self processPrintersOutput];
      break;
    case PAPlistOutputParserCommandNetworkInfo:
      return [self processNetworkOutput];
      break;
    case PAPlistOutputParserCommandHardDiskInfo:
      return [self processHardDiskOutput];
      break;
    case PAPlistOutputParserCommandInstalledSoftwares:
      return [self processInstalledSoftwaresOutput];
      break;
    case PAPlistOutputParserCommandOsVersion:
    case PAPlistOutputParserCommandOsName:
    case PAPlistOutputParserCommandFirewallState:
      return [self processSingleValueOutput];
      break;
    case PAPlistOutputParserCommandDefaultBrowserApp:
      _dataToParse = [self defaultWebBrowser];
      return [self processSingleValueOutput];
      break;
    case PAPlistOutputParserCommandDefaultEmailClient:
      _dataToParse = [self defaultEmailClient];
      return [self processSingleValueOutput];
    case PAPlistOutputParserCommandRunningPrograms:
    {
      return [PARunningApplicationUtility runningAppsXml];
    }
    case PAPlistOutputParserCommandProcessEnvironmentVariables:
    {
      NSDictionary *envVariables = [self processEnvironmentVariables];
      return [self processEnvironmentVariablesOutput:envVariables];
    }
    case PAPlistOutputParserCommandUserInfo:
    {
      return [PAUserInfoUtility userInfoXml];
    }
    case PAPlistOutputParserCommandSystemInfo:
    {
      return [PASystemInfoUtility systemInfoXml];
    }
    case PAPlistOutputParserCommandOSSummary:
    {
      return [PAOSSummaryUtility osInfoXml];
    }
    case PAPlistOutputParserCommandSetUserRegistry:
      return [_registryUpdater outputXml];
    default:
      break;
  }
  return nil;
}

- (NSDictionary *)processEnvironmentVariables {
  return [[NSProcessInfo processInfo] environment];
}

- (NSString *)defaultEmailClient {
  NSURL *emailAppURL = [[NSWorkspace sharedWorkspace] URLForApplicationToOpenURL:[NSURL URLWithString:kPAOPMailToUrlSchema]];
  NSBundle *appbundle = [NSBundle bundleWithURL:emailAppURL];
  NSString *appName = [appbundle objectForInfoDictionaryKey:kPAOPBundleNameKey];
  return appName;
}

- (NSString *)defaultWebBrowser {
  NSURL *browserAppURL = [[NSWorkspace sharedWorkspace] URLForApplicationToOpenURL:[NSURL URLWithString:kPAOPHttpUrlSchema]];
  NSBundle *appbundle = [NSBundle bundleWithURL:browserAppURL];
  NSString *appName = [appbundle objectForInfoDictionaryKey:kPAOPBundleNameKey];
  return appName;
  
}

- (NSString *)processEnvironmentVariablesOutput:(NSDictionary *)environment {
  NSString *xmlElementStr = @"";
  NSInteger totalRecords = environment.allKeys.count;
  NSInteger index = 0;
  for (NSString *key in environment.allKeys) {
    NSString *value = environment[key];
    if (value) {
      
      xmlElementStr = [xmlElementStr stringByAppendingString:[PAGenericUtility propertyXmlForKey:kPAEnvKey value:key]];
      xmlElementStr = [xmlElementStr stringByAppendingString:[PAGenericUtility propertyXmlForKey:kPAEnvVal value:value]];
      index++;
      if ( index < totalRecords) {
        xmlElementStr = [xmlElementStr stringByAppendingString:kPAOPPropertyDelimeter];
      }
    }
  }
  return xmlElementStr;
}

- (NSString *)processSingleValueOutput {
  if (_dataToParse && _propertyName) {
    return [PAGenericUtility propertyXmlForKey:_propertyName value:_dataToParse];
  }
  return nil;
}


- (NSString *)processInstalledSoftwaresOutput {
  NSArray *installedSoftwareRecords = [self systemProfilerMultipleRecordsOutput];
  NSUInteger totalRecords = installedSoftwareRecords.count;
  __block NSUInteger iitemIndex = 0;
  __block NSString *xmlElementStr = @"";
  __block BOOL isFinished = false;
  
  [installedSoftwareRecords enumerateObjectsUsingBlock:^(id installedSoftwareRecord, NSUInteger index, BOOL *stop) {
    BOOL isvalidRecord = false;
    if ([installedSoftwareRecord isKindOfClass:[NSDictionary class]]) {
      NSString *name = [installedSoftwareRecord valueForKey:kPAOPInstalledSoftwareName];
      NSString *path = [installedSoftwareRecord valueForKey:kPAOPInstalledSoftwarePath];
      NSString *version = [installedSoftwareRecord valueForKey:kPAOPInstalledSoftwareVersion];
      
      if (name) {
        xmlElementStr = [xmlElementStr stringByAppendingString:[PAGenericUtility propertyXmlForKey:kPAOPInstalledSoftwareDisplayName value:name]];
      }
      if (path) {
        xmlElementStr = [xmlElementStr stringByAppendingString:[PAGenericUtility propertyXmlForKey:kPAOPInstalledSoftwarePath value:path]];
      }
      if (version) {
        xmlElementStr = [xmlElementStr stringByAppendingString:[PAGenericUtility propertyXmlForKey:kPAOPInstalledSoftwareVersion value:version]];
      }
      isvalidRecord = true;
    }
    iitemIndex++;
    if (isvalidRecord && iitemIndex < totalRecords) {
      xmlElementStr = [xmlElementStr stringByAppendingString:kPAOPPropertyDelimeter];
    }
    isFinished = (index == totalRecords-1) ;
  }];
  
  while (!isFinished) {
    ;
  }
  
  return xmlElementStr;
}
- (NSString *)processHardDiskOutput {
  NSArray *storageRecords = [self systemProfilerMultipleRecordsOutput];
  
  NSUInteger totalRecords = storageRecords.count;
  NSUInteger index = 0;
  NSString *xmlElementStr = @"";
  for (NSDictionary *volumeRecord in storageRecords) {
    BOOL isvalidRecord = false;
    if ([volumeRecord isKindOfClass:[NSDictionary class]]) {
      NSString *name = [volumeRecord valueForKey:kPAOPVolumeName];
      NSString *sizeInBytes = [volumeRecord valueForKey:kPAOPVolumeSize];
      NSString *availableSizeInBytes = [volumeRecord valueForKey:kPAOPVolumeAvailableSize];
      NSDictionary *physicalDrive = [volumeRecord valueForKey:kPAOPVolumePhysicalDrive];
      NSString *isInternalDiskStr;
      if (physicalDrive != nil) {
        if ([physicalDrive isKindOfClass:[NSDictionary class]]) {
          isInternalDiskStr = [physicalDrive valueForKey:kPAOPVolumeIsInternalDisk];
        }
        if ([isInternalDiskStr isEqualToString:kPAOPVolumeIsInternalDiskValueYes]) {
          if (name) {
            xmlElementStr = [xmlElementStr stringByAppendingString:[PAGenericUtility propertyXmlForKey:kPAOPVolumeName value:name]];
          }
          if (sizeInBytes) {
            xmlElementStr = [xmlElementStr stringByAppendingString:[PAGenericUtility propertyXmlForKey:kPAOPVolumeSize value:sizeInBytes]];
          }
          if (availableSizeInBytes) {
            xmlElementStr = [xmlElementStr stringByAppendingString:[PAGenericUtility propertyXmlForKey:kPAOPVolumeAvailableSize value:availableSizeInBytes]];
          }
          isvalidRecord = true;
        }
        
      }else {
        if (name) {
          xmlElementStr = [xmlElementStr stringByAppendingString:[PAGenericUtility propertyXmlForKey:kPAOPVolumeName value:name]];
        }
        if (sizeInBytes) {
          xmlElementStr = [xmlElementStr stringByAppendingString:[PAGenericUtility propertyXmlForKey:kPAOPVolumeSize value:sizeInBytes]];
        }
        if (availableSizeInBytes) {
          xmlElementStr = [xmlElementStr stringByAppendingString:[PAGenericUtility propertyXmlForKey:kPAOPVolumeAvailableSize value:availableSizeInBytes]];
        }
        isvalidRecord = true;
      }
    }
    index++;
    if (isvalidRecord && index < totalRecords) {
      xmlElementStr = [xmlElementStr stringByAppendingString:kPAOPPropertyDelimeter];
    }
  }
  
  return xmlElementStr;
}
- (NSString *)processPrintersOutput {
  NSArray *printerRecords = [self systemProfilerMultipleRecordsOutput];
  NSUInteger totalRecords = printerRecords.count;
  NSUInteger index = 0;
  NSString *xmlElementStr = @"";
  for (NSDictionary *printerRecord in printerRecords) {
    BOOL isvalidRecord = false;
    if ([printerRecord isKindOfClass:[NSDictionary class]]) {
      NSString *name = [printerRecord valueForKey:kPAOPPrinterName];
      NSString *defaultValue = [printerRecord valueForKey:kPAOPPrinterIsDefault];
      NSString *printServer = [printerRecord valueForKey:kPAOPPrintServer];
      
      if (name) {
        xmlElementStr = [xmlElementStr stringByAppendingString:[PAGenericUtility propertyXmlForKey:kPAOPPrinterName value:name]];
      }
      if (defaultValue) {
        xmlElementStr = [xmlElementStr stringByAppendingString:[PAGenericUtility propertyXmlForKey:kPAOPPrinterIsDefault value:defaultValue]];
      }
      if (printServer) {
        xmlElementStr = [xmlElementStr stringByAppendingString:[PAGenericUtility propertyXmlForKey:kPAOPPrintServer value:printServer]];
      }
      
      //            xmlElementStr = [xmlElementStr stringByAppendingString:[PAGenericUtility propertyXmlForKey:kPAOPPrinterDriver value:kPAOPPrinterDriverName]];
      isvalidRecord = true;
    }
    index++;
    if (isvalidRecord && index < totalRecords) {
      xmlElementStr = [xmlElementStr stringByAppendingString:kPAOPPropertyDelimeter];
    }
  }
  
  return xmlElementStr;
}

- (NSString *)processNetworkOutput {
  NSArray *networkRecords = [self systemProfilerMultipleRecordsOutput];
  NSArray *ipAddresses;
  NSArray *ipv4Addresses;
  NSArray *ipv6Addresses;
  NSMutableSet *allIpAddresses = [NSMutableSet set];
  
  NSString *dnsDomainName = nil;
  
  for (NSDictionary *networkRecord in networkRecords) {
    ipAddresses = [networkRecord valueForKey:kPAOPIPAddress];
    // get dns info
    NSDictionary *dnsInfo = [networkRecord valueForKey:kPAOPIDNS];
    if ([dnsInfo isKindOfClass:[NSDictionary class]]) {
      NSString *dnsName = [dnsInfo valueForKey:kPAOPIDNSName];
      if ([dnsName isKindOfClass:[NSString class]]) {
        dnsDomainName = dnsName;
      }
    }
    // check ipv4 addresses
    NSDictionary *ipv4 = [networkRecord valueForKey:kPAOPIPV4];
    if ([ipv4 isKindOfClass:[NSDictionary class]]) {
      ipv4Addresses = [ipv4 valueForKey:kPAOPAddresses];
    }
    
    // check ipv6 addresses
    NSDictionary *ipv6 = [networkRecord valueForKey:kPAOPIPV6];
    if ([ipv6 isKindOfClass:[NSDictionary class]]) {
      ipv6Addresses = [ipv6 valueForKey:kPAOPAddresses];
    }
    
    if (([ipAddresses isKindOfClass:[NSArray class]] &&
         ipAddresses.count) ||
        ([ipv4Addresses isKindOfClass:[NSArray class]] &&
         ipv4Addresses.count) ||
        ([ipv6Addresses isKindOfClass:[NSArray class]] &&
         ipv6Addresses.count)) {
          break;
        }
  }
  if ([ipv4Addresses isKindOfClass:[NSArray class]] &&
      ipv4Addresses.count) {
    [allIpAddresses addObjectsFromArray:ipv4Addresses];
  }
  if ([ipAddresses isKindOfClass:[NSArray class]] &&
      ipAddresses.count) {
    [allIpAddresses addObjectsFromArray:ipAddresses];
  }
  
  if ([ipv6Addresses isKindOfClass:[NSArray class]] &&
      ipv6Addresses.count) {
    [allIpAddresses addObjectsFromArray:ipv6Addresses];
  }
  NSArray *allIpAddressList = [allIpAddresses allObjects];
  NSString *xmlElementStr = @"";
  if (allIpAddressList.count) {
    xmlElementStr = [xmlElementStr stringByAppendingString:[PAGenericUtility propertyXmlForKey:kPAOPIPAddress value:allIpAddressList]];
  }
  if (dnsDomainName) {
    xmlElementStr = [xmlElementStr stringByAppendingString:[PAGenericUtility propertyXmlForKey:kPAOPIDNSDomainName value:dnsDomainName]];
  }
  
  return xmlElementStr;
}
/*
 <dict>
 <key>MMM_entry</key>
 <dict>
 <key>MMM_state</key>
 <string>MMM_enabled</string>
 </dict>
 <key>SMC_version_system</key>
 <string>2.6f57</string>
 <key>_name</key>
 <string>hardware_overview</string>
 <key>boot_rom_version</key>
 <string>MBP102.0106.B0A</string>
 <key>cpu_type</key>
 <string>Intel Core i5</string>
 <key>current_processor_speed</key>
 <string>2.5 GHz</string>
 <key>l2_cache_core</key>
 <string>256 KB</string>
 <key>l3_cache</key>
 <string>3 MB</string>
 <key>machine_model</key>
 <string>MacBookPro10,2</string>
 <key>machine_name</key>
 <string>MacBook Pro</string>
 <key>number_processors</key>
 <integer>2</integer>
 <key>packages</key>
 <integer>1</integer>
 <key>physical_memory</key>
 <string>8 GB</string>
 <key>platform_UUID</key>
 <string>D282F78F-1D99-5640-B55C-31234892FB52</string>
 <key>serial_number</key>
 <string>C02JLC9ADR53</string>
 </dict>
 */

- (NSString *)processHwInfoOutput {
  NSDictionary *hwInfo = [self systemProfilerSingleRecordOutput];
  NSString *hwUuid = [hwInfo valueForKey:kPAOPHwUUID];
  NSString *physicalMemory = [hwInfo valueForKey:kPAOPPhysicalMemory];
  NSString *machineName = [hwInfo valueForKey:kPAOPMachineName];
  NSString *cpuType = [hwInfo valueForKey:kPAOPCpuType];
  NSString *currentProcessorSpeed = [hwInfo valueForKey:kPAOPCurrentProcessorSpeed];
  NSString *machineModel = [hwInfo valueForKey:kPAOPMachineModel];
  NSString *noOfProcessors = [hwInfo valueForKey:kPAOPNoOfProcessors];
  NSString *serialNumber = [hwInfo valueForKey:kPAOPSerialNumber];
  NSString *xmlElementStr = @"";
  
  if (hwUuid) {
    xmlElementStr = [xmlElementStr stringByAppendingString:[PAGenericUtility propertyXmlForKey:kPAOPHwUUID value:hwUuid]];
  }
  if (physicalMemory) {
    xmlElementStr = [xmlElementStr stringByAppendingString:[PAGenericUtility propertyXmlForKey:kPAOPPhysicalMemory value:physicalMemory]];
  }
  if (machineName) {
    xmlElementStr = [xmlElementStr stringByAppendingString:[PAGenericUtility propertyXmlForKey:kPAOPMachineName value:machineName]];
  }
  if (machineModel) {
    xmlElementStr = [xmlElementStr stringByAppendingString:[PAGenericUtility propertyXmlForKey:kPAOPMachineModel value:machineModel]];
  }
  if (cpuType) {
    xmlElementStr = [xmlElementStr stringByAppendingString:[PAGenericUtility propertyXmlForKey:kPAOPCpuType value:cpuType]];
  }
  if (currentProcessorSpeed) {
    xmlElementStr = [xmlElementStr stringByAppendingString:[PAGenericUtility propertyXmlForKey:kPAOPCurrentProcessorSpeed value:currentProcessorSpeed]];
  }
  if (noOfProcessors) {
    xmlElementStr = [xmlElementStr stringByAppendingString:[PAGenericUtility propertyXmlForKey:kPAOPNoOfProcessors value:noOfProcessors]];
  }
  if (serialNumber) {
    xmlElementStr = [xmlElementStr stringByAppendingString:[PAGenericUtility propertyXmlForKey:kPAOPSerialNumber value:serialNumber]];
  }
  
  return xmlElementStr;
}

- (NSDictionary *)systemProfilerSingleRecordOutput {
  NSArray *records = [self systemProfilerMultipleRecordsOutput];
  NSDictionary *firstRecord = records.firstObject;
  if ([firstRecord isKindOfClass:[NSDictionary class]]) {
    return firstRecord;
  }
  return nil;
}

- (NSArray *)systemProfilerMultipleRecordsOutput {
  if ([_dataToParse isKindOfClass:[NSArray class]]) {
    NSArray *plistArray = (NSArray *)_dataToParse;
    NSDictionary *firstElementInPlistArray = plistArray.firstObject;
    if ([firstElementInPlistArray isKindOfClass:[NSDictionary class]]) {
      NSArray *items = (NSArray *)[firstElementInPlistArray valueForKey:kPAOPItemsKey];
      if ([items isKindOfClass:[NSArray class]]) {
        return items;
      }
    }
  }
  return nil;
}
- (PAPlistOutputParserCommand)commandEnumValue {
  PAPlistOutputParserCommand cmd = PAPlistOutputParserCommandUnknown;
  if (![_commandString isEmptyOrHasOnlyWhiteSpaces]) {
    NSArray *cmdArray = [[NSArray alloc] initWithObjects:kPAPlistOutputParserCommandArray];
    NSUInteger index = [cmdArray indexOfObject:_commandString];
    if (NSNotFound != index) {
      cmd = (PAPlistOutputParserCommand)index;
    }
  }
  return cmd;
}
@end
