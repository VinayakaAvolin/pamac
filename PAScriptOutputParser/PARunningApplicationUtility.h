//
//  PARunningApplicationUtility.h
//  PAScriptOutputParser
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PAGenericUtility.h"

@interface PARunningApplicationUtility : PAGenericUtility

+ (NSString *)runningAppsXml;

@end
