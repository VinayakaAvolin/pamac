//
//  PAUserInfoUtility.m
//  PAScriptOutputParser
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PAUserInfoUtility.h"
#import "PAUserInfo.h"

// user info keys
NSString *const kPAOPUserName = @"UserName";
NSString *const kPAOPFullUserName = @"FullUserName";

@implementation PAUserInfoUtility

+ (NSString *)userInfoXml {
  PAUserInfoUtility *utility = [[PAUserInfoUtility alloc]init];
  PAUserInfo *userInfo = [utility userInfo];
  return [utility processUserInfoOutput:userInfo];
}

- (NSString *)processUserInfoOutput:(PAUserInfo *)userInfo {
  NSString *xmlElementStr = @"";
  NSString *userName = userInfo.name;
  NSString *fullUserName = userInfo.fullName;
  if (userName) {
    xmlElementStr = [xmlElementStr stringByAppendingString:[PAGenericUtility propertyXmlForKey:kPAOPUserName value:userName]];
  }
  if (fullUserName) {
    xmlElementStr = [xmlElementStr stringByAppendingString:[PAGenericUtility propertyXmlForKey:kPAOPFullUserName value:fullUserName]];
  }
  return xmlElementStr;
}

- (PAUserInfo *)userInfo {
  PAUserInfo *userInfo = [[PAUserInfo alloc] initWithName:NSUserName() fullName:NSFullUserName()];
  //    NSLog(@"userInfo = %@",userInfo);
  return userInfo;
}

@end
