//
//  PASystemInfo.h
//  PAScriptOutputParser
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PASystemInfo : NSObject

@property NSString* systemType; // Laptop/desktop
@property NSString* computerName;
@property NSString* domainName;
@property NSString* language;

- (instancetype)initWithComputerName:(NSString*)name
                            language:(NSString*)lang
                          systemType:(NSString*)type
                          domainName:(NSString*)domain;

@end
