//
//  PARegistryUpdater.m
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <PACoreServices/PACoreServices.h>
#import "PARegistryUpdater.h"
#import "PAPlistLockManager.h"

@interface PARegistryUpdater () {
  PAPlistOutputParserCommand _command;
  NSString *_registryPath;
  NSString *_key;
  NSString *_value;
}

@end

@implementation PARegistryUpdater

- (instancetype)initWithCommand:(PAPlistOutputParserCommand)command
                      arguments:(NSArray *)args {
  self = [super init];
  if (self) {
    if (args.count < 3) {
      self = nil;
    } else {
      _command = command;
      _registryPath = args[0];
      _key = args[1];
      _value = args[2];
    }
  }
  return self;
}

- (NSString *)outputXml {
  NSString *output = @"";
  switch (_command) {
    case PAPlistOutputParserCommandSetUserRegistry:
      [self setUserRegistry];
      output = [self xmlString];
      break;
      
    default:
      break;
  }
  return output;
}

- (NSString *)xmlString {
  NSString *xmlElementStr = @"";
  if (_key && _value) {
    xmlElementStr = [xmlElementStr stringByAppendingString:[PAGenericUtility propertyXmlForKey:_key value:_value]];
  }
  return xmlElementStr;
}
- (void)setUserRegistry {
  PAFileSystemManager *fsManager = [[PAFileSystemManager alloc] init];
  NSString* plistFilePath = [fsManager hkcuRegistryPlistPath];
  PAPlistLockManager *lckMgr = [PAPlistLockManager sharedInstance];
  [lckMgr runWithWriteLock:plistFilePath codeBlock:^{
    NSString *keyPath = [fsManager stringByExpandingPath:_registryPath];
    PARegistryWriter* writer = [[PARegistryWriter alloc] initWithPlistAtPath:plistFilePath];
    [writer setRegValueForPath:keyPath withKey:_key andData:_value];
  }];
}


@end
