//
//  PAUserInfo.h
//  PAScriptOutputParser
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PAUserInfo : NSObject

@property NSString *name;
@property NSString *fullName;

- (instancetype)initWithName:(NSString*)name
                    fullName:(NSString*)fullName;

@end
