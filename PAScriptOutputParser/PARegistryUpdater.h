//
//  PARegistryUpdater.h
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PAScriptOutputParserConstants.h"
#import "PAGenericUtility.h"

@interface PARegistryUpdater : PAGenericUtility

- (instancetype)initWithCommand:(PAPlistOutputParserCommand)command
                      arguments:(NSArray *)args;
- (NSString *)outputXml;

@end
