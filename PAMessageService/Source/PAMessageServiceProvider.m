//
//  PAMessageServiceProvider.m
//  PAMessageService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PAMessageServiceProvider.h"
#import "PAMessageServiceXmlParser.h"

@interface PAMessageServiceProvider () {
  PAServiceCallback _callback;
  PAServiceInfo *_serviceInfo;
}

@end

@implementation PAMessageServiceProvider

- (BOOL)canExecuteService:(PASupportService)service category:(PASupportServiceCategory)category {
  if (category == PASupportServiceCategoryMessage &&
      service == PASupportServiceMessages) {
    return true;
  }
  return false;
}

- (void)executeService:(PASupportService)service category:(PASupportServiceCategory)category info:(id<PAServiceInfoProtocol>)info callback:(PAServiceCallback)callback {
  _callback = callback;
  _serviceInfo = (PAServiceInfo *)info;
  
  switch (service) {
    case PASupportServiceMessages:
    {
      [self handleMessages];
    }
      break;
    default:
      break;
  }
}

- (void)handleMessages {
  if (!_serviceInfo.guid || !_serviceInfo.version) {
    _callback(false, nil, nil);
    return;
  }
  [self parseInput];
}

- (void)parseInput {
  PAMessageServiceXmlParser *configParser = [[PAMessageServiceXmlParser alloc] initWithServiceInfo:_serviceInfo];
  [configParser parseInputConfigurationWithCompletion:^(BOOL success, PAServiceInfo *serviceInfo) {
    if (success) {
      _serviceInfo = serviceInfo;
      [self didFinishInputParsing];
    } else {
      _callback(false, nil, nil);
    }
  }];
}

- (void)didFinishInputParsing {
  switch (_serviceInfo.service) {
    case PASupportServiceMessages:
      [self processMessages];
      break;
    default:
      _callback(false, nil, nil);
      break;
  }
}
- (void)processMessages {
  if (_serviceInfo.messageType == PASupportServiceMessageTypeNone) {
    _callback(false, nil, nil);
    return;
  }
  
  switch (_serviceInfo.messageType) {
    case PASupportServiceMessageTypeTellMe:
    case PASupportServiceMessageTypeFix:
      [self tellMe];
      break;
    case PASupportServiceMessageTypeDoItForMe:
      [self doItForMe];
      break;
    case PASupportServiceMessageTypeShowMe:
      [self showMe];
      break;
    default:
      break;
  }
}

- (void)tellMe {
  _callback(true, nil, nil);
  
}
- (void)doItForMe {
  _callback(true, nil, nil);
}
- (void)showMe {
  _callback(true, nil, nil);
}

@end
