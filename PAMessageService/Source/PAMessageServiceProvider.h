//
//  PAMessageServiceProvider.h
//  PAMessageService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <PACoreServices/PACoreServices.h>
#import "PAServiceProvider.h"

@interface PAMessageServiceProvider : PAServiceProvider

@end
