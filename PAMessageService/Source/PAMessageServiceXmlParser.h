//
//  PAMessageServiceXmlParser.h
//  PARepairService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <PACoreServices/PACoreServices.h>
#import "PAServiceInfo.h"

typedef void(^PAMessageServiceXmlParserCompletionBlock)(BOOL success, PAServiceInfo *serviceInfo);

@interface PAMessageServiceXmlParser : PAXmlParser

- (instancetype)initWithServiceInfo:(PAServiceInfo *)serviceInfo;
- (void)parseInputConfigurationWithCompletion:(PAMessageServiceXmlParserCompletionBlock)completionBlock;

@end
