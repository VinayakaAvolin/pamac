//
//  PAMessageServiceXmlParser.m
//  PARepairService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PAMessageServiceXmlParser.h"

NSString *const kPAMSContentSetKey = @"content-set";
NSString *const kPAMSContentKey = @"content";
NSString *const kPAMSFieldSetKey = @"field-set";
NSString *const kPAMSFieldKey = @"field";
NSString *const kPAMSNameKey = @"_name";
NSString *const kPAMSValueKey = @"value";
NSString *const kPAMSSupportActionKey = @"SupportAction";
NSString *const kPAMSTellMeKey = @"Tell Me";
NSString *const kPAMSCompletedLinkCaptionKey = @"Completed Link Caption";

@interface PAMessageServiceXmlParser () {
  NSString *_inputXmlPath;
  PAServiceInfo *_serviceInfo;
  PAFileSystemManager *_fsManager;
}

@end

@implementation PAMessageServiceXmlParser

- (instancetype)initWithServiceInfo:(PAServiceInfo *)serviceInfo {
  self = [super init];
  if (self && serviceInfo.guid && serviceInfo.version) {
    _serviceInfo = serviceInfo;
    _fsManager = [[PAFileSystemManager alloc]init];
    [self setupInputConfigurationXml];
  } else {
    self = nil;
  }
  return self;
}


- (void)parseInputConfigurationWithCompletion:(PAMessageServiceXmlParserCompletionBlock)completionBlock {
  // NSString *defaultXmlFilePath = [fsManager localDefaultXmlFilePath];
  NSDictionary *xmlDict = [self dictionaryFromXmlAtPath:_inputXmlPath];
  if (xmlDict) {
    NSDictionary *contentSetDict = [xmlDict valueForKey:kPAMSContentSetKey];
    if ([contentSetDict isKindOfClass:[NSDictionary class]]) {
      NSDictionary *contentDict = [contentSetDict valueForKey:kPAMSContentKey];
      if ([contentDict isKindOfClass:[NSDictionary class]]) {
        NSDictionary *fieldSetDict = [contentDict valueForKey:kPAMSFieldSetKey];
        if ([fieldSetDict isKindOfClass:[NSDictionary class]]) {
          NSArray *fieldsList = [fieldSetDict valueForKey:kPAMSFieldKey];
          if ([fieldsList isKindOfClass:[NSArray class]]) {
            BOOL hasSupportAction = false;
            NSString *tellMe = nil;
            NSString *caption = nil;
            // support action key
            NSDictionary *supportActionFieldDict = [self dictionaryMatchingName:kPAMSSupportActionKey inList:fieldsList];
            if ([supportActionFieldDict isKindOfClass:[NSDictionary class]]) {
              hasSupportAction = true;
            }
            // tell me key
            NSDictionary *tellMeFieldDict = [self dictionaryMatchingName:kPAMSTellMeKey inList:fieldsList];
            if ([tellMeFieldDict isKindOfClass:[NSDictionary class]]) {
              tellMe = [tellMeFieldDict valueForKey:kPAMSValueKey];
              if (![tellMe isKindOfClass:[NSString class]]) {
                tellMe = nil;
              }
            }
            // caption key
            NSDictionary *captionFieldDict = [self dictionaryMatchingName:kPAMSCompletedLinkCaptionKey inList:fieldsList];
            if ([captionFieldDict isKindOfClass:[NSDictionary class]]) {
              caption = [captionFieldDict valueForKey:kPAMSValueKey];
              if (![caption isKindOfClass:[NSString class]]) {
                caption = nil;
              }
            }
            PAServiceInfo *serviceInfo = [_serviceInfo copy];
            serviceInfo.messageTypeStr = caption;
            serviceInfo.tellMeLink = tellMe;
            serviceInfo.hasSupportAction = hasSupportAction;
            completionBlock(true, serviceInfo);
            return;
          }
        }
      }
    }
  }
  completionBlock(false, _serviceInfo);
}

- (NSDictionary *)dictionaryMatchingName:(NSString *)name inList:(NSArray *)inputList {
  if (inputList.count) {
    NSArray *filtered = [inputList filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(_name == %@)", name]];
    if (filtered.count) {
      return filtered[0];
    }
  }
  
  return nil;
}


/// constructs app specific protection xml path
/// Eg: ~/Library/Application\ Support/ProactiveAssist/Experience92/data/sprt_msg/9c3b49b2-fa85-4e1b-b6a1-1ff832919167.1/9c3b49b2-fa85-4e1b-b6a1-1ff832919167.1.xml
- (void)setupInputConfigurationXml {
  PAAppConfiguration *appConfiguration = [PAConfigurationManager appConfigurations];
  NSString *folderName = [NSString stringWithFormat:@"%@.%@",_serviceInfo.guid, _serviceInfo.version];
  NSString *xmlFileName = [NSString stringWithFormat:@"%@.%@.xml",_serviceInfo.guid, _serviceInfo.version];
  
  NSString *xmlPath = nil;
  switch (_serviceInfo.service) {
    case PASupportServiceMessages:
      xmlPath = appConfiguration.messagesFolderPath;
      break;
    default:
      break;
  }
  xmlPath = [_fsManager stringByExpandingPath:xmlPath];
  xmlPath = [xmlPath stringByAppendingPathComponent:folderName];
  xmlPath = [xmlPath stringByAppendingPathComponent:xmlFileName];
  if ([_fsManager doesFileExist:xmlPath]) {
    _inputXmlPath = xmlPath;
  }
}
@end
