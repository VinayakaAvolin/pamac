//
//  PAScriptRunnerService.h
//  PAScriptRunnerService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Cocoa/Cocoa.h>

//! Project version number for PAScriptRunnerService.
FOUNDATION_EXPORT double PAScriptRunnerServiceVersionNumber;

//! Project version string for PAScriptRunnerService.
FOUNDATION_EXPORT const unsigned char PAScriptRunnerServiceVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <PAScriptRunnerService/PublicHeader.h>

#import <PAScriptRunnerService/PAScriptRunnerService.h>
#import <PAScriptRunnerService/PAScriptRunner.h>
#import <PAScriptRunnerService/PAScript.h>
#import <PAScriptRunnerService/PAAppleScript.h>
#import <PAScriptRunnerService/PAShellScript.h>
#import <PAScriptRunnerService/PAScriptInfo.h>
