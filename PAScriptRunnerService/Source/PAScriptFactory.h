//
//  PAScriptFactory.h
//  PAScriptRunnerService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PAScriptProtocol.h"
#import "PAScriptRunnerConstants.h"

/**
 Factory class to return appropriate Script object
 */
@interface PAScriptFactory : NSObject

+ (id<PAScriptProtocol>)scriptWithPath:(NSString *)scriptPath
                            scriptType:(PAScriptType)type;
+ (id<PAScriptProtocol>)scriptWithString:(NSString *)scriptString
                              scriptType:(PAScriptType)type;
@end
