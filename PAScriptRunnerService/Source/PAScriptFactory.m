//
//  PAScriptFactory.m
//  PAScriptRunnerService
//
//  Copyright © 2017 Avolin. All rights reserved.
//


#import "PAScriptFactory.h"
#import "PAAppleScript.h"
#import "PAShellScript.h"

@implementation PAScriptFactory

+ (id<PAScriptProtocol>)scriptWithPath:(NSString *)scriptPath
                            scriptType:(PAScriptType)type {
  id<PAScriptProtocol> script;
  
  switch (type) {
    case PAScriptTypeApple:
      script = [[PAAppleScript alloc]initWithPath:scriptPath];
      break;
    case PAScriptTypeShell:
      script = [[PAShellScript alloc]initWithPath:scriptPath];
      break;
    default:
      break;
  }
  return script;
}

+ (id<PAScriptProtocol>)scriptWithString:(NSString *)scriptString
                              scriptType:(PAScriptType)type {
  id<PAScriptProtocol> script;
  
  switch (type) {
    case PAScriptTypeApple:
      script = [[PAAppleScript alloc]initWithScriptString:scriptString];
      break;
    default:
      break;
  }
  return script;
}

@end
