//
//  NSAppleEventDescriptor+Additions.m
//  PAScriptRunnerService
//
//  Copyright © 2017 Avolin. All rights reserved.
//


#import "NSAppleEventDescriptor+Additions.h"
#import <Carbon/Carbon.h>

@implementation NSAppleEventDescriptor (Additions)

/* Convert an AEDescList of strings to NSArray of strings preserving their ordering. */
- (NSArray*)arrayOfStrings {
  
  NSAppleEventDescriptor *listDescriptor = [self coerceToDescriptorType:typeAEList];
  NSArray *list = nil;
  if (!listDescriptor) {
    return nil;
  }
  /* count the number of items in the AEDescList */
  unsigned index, total = (unsigned)[self numberOfItems];
  
  /* create a local array */
  NSMutableArray* array = [NSMutableArray arrayWithCapacity:total];
  
  /* add all of the strings to the array */
  for (index=0; index<total; index++) {
    
    /* get the nth descriptor's content as a string */
    NSString* theString = [[self descriptorAtIndex:(index+1)] stringValue];
    
    /* append the object to the items in the array */
    [array addObject:theString];
  }
  if (array.count) {
    list = [NSArray arrayWithArray:array];
  }
  /* return the new array */
  return list;
}



/* Convert an AEDescList of strings to NSArray of strings sorted in alphabetical order. */
- (NSArray*)sortedArrayOfStrings {
  
  /* return a sorted version of the array returned by listOfStringsToArray */
  return [[self arrayOfStrings] sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
}

- (NSDictionary*)scriptingUserDefinedRecord {
    NSInteger                theIndex,
    theNumOfItems = [self numberOfItems];
    NSMutableDictionary        *theDictionary = [NSMutableDictionary dictionaryWithCapacity:theNumOfItems];
    
    for( theIndex = 1; theIndex <= theNumOfItems; theIndex++ )
    {
        AEKeyword    theKeyword = [self keywordForDescriptorAtIndex:theIndex];
        id                theObject = theKeyword == keyASUserRecordFields
        ? [self descriptorForKeyword:keyASUserRecordFields]
        : [self descriptorForKeyword:theKeyword];
        [theDictionary setObject:[theObject objectValue] forKey:[NSNumber numberWithUnsignedInt:theKeyword]];
    }
    
    return theDictionary;
}

- (NSArray*)scriptingUserListWithDescriptor:(NSAppleEventDescriptor*)desc {
    NSMutableArray* array = [NSMutableArray arrayWithCapacity:0];
    NSInteger numItems = [desc numberOfItems];
    
    // for each item in the list, convert to Foundation object and add to the array
    for ( NSInteger itemIndex = 1; itemIndex <= numItems; itemIndex++ ) {
        NSAppleEventDescriptor* itemDesc = [desc descriptorAtIndex:itemIndex];
        id objectValue = [itemDesc objectValue];
        
        if (objectValue) {
            [array addObject:objectValue];
        }
    }
    
    return [NSArray arrayWithArray:array];
}

-(id)objectValue {
    DescType    descType = [self descriptorType];
    id          object = nil;
    
    switch ( descType ) {
        case typeUnicodeText:
        case typeUTF8Text:
        case typeFileURL:
            object = [self stringValue];
            break;
        case typeTrue:
            object = [NSNumber numberWithBool:(BOOL)[self booleanValue]];
            break;
        case typeFalse:
            object = [NSNumber numberWithBool:(BOOL)[self booleanValue]];
            break;
        case typeAEList:
            object = [self scriptingUserListWithDescriptor:self];
            break;
        case typeAERecord:
            object = [self scriptingUserDefinedRecord];
            break;
        case typeSInt16:
        case typeUInt16:
        case typeSInt32:
        case typeUInt32:
        case typeSInt64:
        case typeUInt64:
            object = [NSNumber numberWithInteger:(NSInteger)[self int32Value]];
            break;
        case typeIEEE32BitFloatingPoint:
        case typeIEEE64BitFloatingPoint:
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wpartial-availability"
            object = [NSNumber numberWithDouble:(double)[self doubleValue]];
#pragma clang diagnostic pop
            break;
        case typeNull:
        case typeType:
            object = [NSNull null];
            break;
        case 'ldt ':
            object = [self dateValue];
            break;
        default:
            object = [self stringValue];
            break;
    }
    
    if (!object) {
        // FIXME: Do better logging here
        NSLog(@"ERROR: NSAppleEventDescriptor objectValue is nil. Given descriptorType is: %d", descType);
    }
    
    return object;
}


@end
