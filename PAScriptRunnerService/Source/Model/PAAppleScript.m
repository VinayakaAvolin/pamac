//
//  PAAppleScript.m
//  PAScriptRunnerService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PAAppleScript.h"
#import "NSAppleEventDescriptor+Additions.h"
#import "NSError+Additions.h"

NSString *const kPAAppleScriptScriptLaunchPath = @"/usr/bin/osascript";

@interface PAAppleScript ()

/* property for the 'theScript' field */
@property (nonatomic) NSAppleScript *script;
@property (nonatomic) NSString *handlerName;
@property (nonatomic) NSAppleEventDescriptor *parameters;
@property (nonatomic) NSAppleEventDescriptor *event;

@end

@implementation PAAppleScript

- (instancetype)initWithPath:(NSString *)path
                     handler:(NSString *)handlerName
                  parameters:(id)firstParameter, ... {
  self = [super init];
  if (self) {
    if (path) {
      self.scriptPath = path;
      _handlerName = handlerName;
      
      /// initialize parameters
      va_list args;
      int index = 1;
      id nthID;
      
      if (nil != firstParameter) {
        _parameters = [NSAppleEventDescriptor listDescriptor];
        va_start(args, firstParameter);
        
        for (nthID=firstParameter; nthID != nil; nthID = va_arg(args, id) ) {
          
          if ( [nthID isKindOfClass: [NSNumber class]] ) {
            [_parameters insertDescriptor:
             [NSAppleEventDescriptor descriptorWithInt32:(SInt32)[nthID longValue]] atIndex:(index++)];
          } else if ( [nthID isKindOfClass: [NSString class]] ) {
            [_parameters insertDescriptor:
             [NSAppleEventDescriptor descriptorWithString:nthID] atIndex:(index++)];
          } else if ( [nthID isKindOfClass: [NSAppleEventDescriptor class]] ) {
            [_parameters insertDescriptor: nthID atIndex:(index++)];
          }
          else if ( [nthID isKindOfClass: [NSArray class]] ) {
            NSAppleEventDescriptor *descriptor = [NSAppleEventDescriptor listDescriptor];
            NSUInteger listArgIndex = 1;
            for (id argument in nthID) {
              if ( [argument isKindOfClass: [NSString class]] ) {
                [descriptor insertDescriptor: [NSAppleEventDescriptor descriptorWithString:argument] atIndex:(listArgIndex++)];
              } else if ( [argument isKindOfClass: [NSNumber class]] ) {
                [descriptor insertDescriptor: [NSAppleEventDescriptor descriptorWithInt32:(SInt32)[argument longValue]] atIndex:(listArgIndex++)];
              }
            }
            [_parameters insertDescriptor: descriptor atIndex:(index++)];
          } else {
            return nil; /* bad parameter */
          }
        }
        
        va_end(args);
      }
    } else {
      self = nil;
    }
  }
  return self;
}
- (void)runWithCallBack:(PAScriptCallback)callback {
  dispatch_async(dispatch_get_main_queue(), ^{
    [self runOnMainThreadWithCallBack:callback];
  });
}
- (void)runOnMainThreadWithCallBack:(PAScriptCallback)callback {
  @autoreleasepool {
    if (self.scriptPath) {
      // initialize script
      NSURL *sourceURL = [NSURL fileURLWithPath:self.scriptPath];
      _script = [[NSAppleScript alloc]initWithContentsOfURL:sourceURL error: nil];
    } else if (self.scriptString) {
      _script = [[NSAppleScript alloc]initWithSource:self.scriptString];
    }
    
    // setup Apple event if required
    [self prepareAppleEvent];
    
    /// execute apple script
    NSAppleEventDescriptor *result;
    NSDictionary *errorInfo;
    if (nil != _event) {
      result = [[self script] executeAppleEvent:_event error:&errorInfo];
    } else {
      result = [_script executeAndReturnError:&errorInfo];
    }
    
    if (errorInfo) {
      NSString *message = errorInfo[NSAppleScriptErrorMessage];
      callback(false, [NSError errorWithMessage:message code:100 info:nil], nil);
      return;
    }
    id resultValue = [self scriptOutputFrom:result];
    callback(true, nil, resultValue);
  }
}

- (void)runInTerminalWithCallBack:(PAScriptCallback)callback {
  NSString *shellMainArgument = self.scriptString;
  
  BOOL canExecuteScript = (self.scriptString != nil);
  if (self.scriptPath) {
    shellMainArgument = self.scriptPath;
    canExecuteScript = true;// [self setExecutablePermissionToScriptAtPath:self.scriptPath];
  }
  
  if (canExecuteScript) {
    @try {
      NSPipe *newPipe = [NSPipe pipe];
      NSFileHandle *readHandle = [newPipe fileHandleForReading];
      NSData *inData = nil;
      NSString* returnValue = nil;
      
      NSTask * unixTask = [[NSTask alloc] init];
      [unixTask setStandardOutput:newPipe];
      [unixTask setLaunchPath:kPAAppleScriptScriptLaunchPath];
      NSString *arguments = [NSString stringWithFormat:@"%@", shellMainArgument];
      [unixTask setArguments:[NSArray arrayWithObjects:arguments, nil]];
      [unixTask launch];
      [unixTask waitUntilExit];
      int status = [unixTask terminationStatus];
      if (0 == status) {
        while ((inData = [readHandle availableData]) && [inData length]) {
          
          returnValue= [[NSString alloc]
                        initWithData:inData encoding:[NSString defaultCStringEncoding]];
          
          returnValue = [returnValue substringToIndex:[returnValue length]-1];
        }
        callback(true, nil, returnValue);
      } else {
        callback(false, [NSError errorWithMessage:returnValue code:100], nil);
      }
    } @catch (NSException *e) {
      callback(false, [NSError errorWithMessage:nil code:100], nil);
    }
  } else {
    callback(false, [NSError errorWithMessage:nil code:100], nil);
  }
}

/* callScript is the main workhorse routine we use for calling handlers
	in our AppleScript.  Essentially, this routine creates a 'call subroutine' Apple event
	and then it dispatches the event to the compiled AppleScript we loaded in our awakeFromNib
	routine. */
- (void)prepareAppleEvent {
  ProcessSerialNumber PSN = {0, kCurrentProcess};
  NSAppleEventDescriptor *address;
  NSAppleEventDescriptor* handler;
  
  /* create the target address descriptor specifying our own process as the target */
  address = [NSAppleEventDescriptor descriptorWithDescriptorType:typeProcessSerialNumber
                                                           bytes:&PSN length:sizeof(PSN)];
  if ( !address ) {
    return ;
  }
  
  if (_handlerName) {
    /* create a new Apple event of type typeAppleScript/kASSubroutineEvent */
    _event = [NSAppleEventDescriptor appleEventWithEventClass:typeAppleScript
                                                      eventID:kASSubroutineEvent targetDescriptor:address
                                                     returnID:kAutoGenerateReturnID transactionID:kAnyTransactionID];
  } else if (_parameters) {
    /* create a new Apple event of type typeAppleScript/kASSubroutineEvent */
    _event = [NSAppleEventDescriptor
              appleEventWithEventClass:kAECoreSuite
              eventID:kAEDoScript
              targetDescriptor:address
              returnID:kAutoGenerateReturnID
              transactionID:kAnyTransactionID];
  }
  
  if ( !_event ) {
    return;
  }
  
  
  if (_handlerName) {
    /* create a descriptor containing the handler's name.  AppleScript handler
     names must be converted to lowercase before including them in a call
     subroutine Apple event.  */
    handler = [NSAppleEventDescriptor descriptorWithString:[_handlerName lowercaseString]];
    if ( handler ) {
      /* add the handler's name to the Apple event using the keyASSubroutineName keyword */
      [_event setDescriptor:handler forKeyword:keyASSubroutineName];
    }
  }
  
  /* add the parameter list.  If none was specified, then create an empty one */
  if ( _parameters != nil ) {
    [_event setDescriptor:_parameters forKeyword:keyDirectObject];
  } else {
    NSAppleEventDescriptor* paramList = [NSAppleEventDescriptor listDescriptor];
    [_event setDescriptor:paramList forKeyword:keyDirectObject];
  }
}


- (id)scriptOutputFrom:(NSAppleEventDescriptor *)result {
  id resultValue = nil;
  if (result) {
    switch (result.descriptorType) {
      case typeAEList:
        resultValue = [result arrayOfStrings];
        break;
      case typeBoolean:
        resultValue = @([result booleanValue]);
        break;
      case typeAERecord:
        resultValue = [result scriptingUserDefinedRecord];
        break;
      default:
        resultValue = [result stringValue];
        break;
    }
  }
  return resultValue;
}
@end
