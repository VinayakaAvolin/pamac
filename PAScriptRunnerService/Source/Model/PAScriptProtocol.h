//
//  PAScriptProtocol.h
//  PAScriptRunnerService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PAScriptRunnerConstants.h"

/**
 Every Script class should adhere to this protocol
 */
@protocol PAScriptProtocol <NSObject>

- (instancetype)initWithPath: (NSString *)path;
- (instancetype)initWithPath:(NSString *)path
                     handler:(NSString *)handlerName
                  parameters:(id)firstParameter, ... ;
- (instancetype)initWithScriptString:(NSString *)script;

- (void)runWithCallBack:(PAScriptCallback)callback;

@optional
- (void)runInTerminalWithCallBack:(PAScriptCallback)callback;

@end

