//
//  PAShellScript.m
//  PAScriptRunnerService
//
//  Copyright © 2017 Avolin. All rights reserved.
//


#import "PAShellScript.h"
#import "NSError+Additions.h"

NSString *const kPABashScriptLaunchPath = @"/bin/bash";

@interface PAShellScript ()

/* property for the 'theScript' field */
@property (nonatomic) NSString *handlerName;
@property (nonatomic) NSArray *parameters;

@end

@implementation PAShellScript

- (instancetype)initWithPath:(NSString *)path
                     handler:(NSString *)handlerName
                  parameters:(id)firstParameter, ...{
  self = [super init];
  if (self) {
    if (path) {
      self.scriptPath = path;
      va_list args;
      int index = 1;
      id nthID;
      
      if (nil != firstParameter) {
        NSMutableArray *params = [[NSMutableArray alloc]init];
        va_start(args, firstParameter);
        
        for (nthID=firstParameter; nthID != nil; nthID = va_arg(args, id) ) {
          
          if ( [nthID isKindOfClass: [NSNumber class]] ) {
            [params addObject:nthID];
          } else if ( [nthID isKindOfClass: [NSString class]] ) {
            [params addObject:nthID];
          } else {
            self = nil; /* bad parameter */
          }
        }
        
        va_end(args);
        if (self && params.count) {
          _parameters = [NSArray arrayWithArray:params];
        }
      }
      
    } else {
      self = nil;
    }
  }
  return self;
}

- (void)runWithCallBack:(PAScriptCallback)callback {
  NSString *shellMainArgument = self.scriptPath;
  
  BOOL canExecuteScript = false;
  if (self.scriptPath) {
    canExecuteScript = true;//[self setExecutablePermissionToScriptAtPath:self.scriptPath];
  }
  
  if (canExecuteScript) {
    @try {
      NSPipe *outPipe = [NSPipe pipe];
      NSPipe *errorPipe = [NSPipe pipe];
      NSString* returnValue = nil;
      
      NSTask * unixTask = [[NSTask alloc] init];
      [unixTask setStandardOutput:outPipe];
      [unixTask setStandardError:errorPipe];
      
      [unixTask setLaunchPath:kPABashScriptLaunchPath];
      NSMutableArray *args = [NSMutableArray arrayWithObjects:shellMainArgument, nil];
      if (_parameters.count) {
        [args addObjectsFromArray:_parameters];
      }
      [unixTask setArguments:args];
      NSFileHandle *readHandle = [outPipe fileHandleForReading];
      NSFileHandle *errorHandle = [errorPipe fileHandleForReading];
      [unixTask launch];
      [unixTask waitUntilExit];
      int status = [unixTask terminationStatus];
      if (0 == status) {
        
        NSData *data = [readHandle readDataToEndOfFile];
        
        if (data && [data length]) {
          returnValue= [[NSString alloc]
                        initWithData:data encoding:NSUTF8StringEncoding];
          
          returnValue = [returnValue substringToIndex:[returnValue length]-1];
        }
        callback(true, nil, returnValue);
      } else {
        NSString* errorValue = nil;
        NSData *data = [errorHandle readDataToEndOfFile];
        
        if (data && [data length]) {
          errorValue= [[NSString alloc]
                       initWithData:data encoding:NSUTF8StringEncoding];
          
          errorValue = [errorValue substringToIndex:[errorValue length]-1];
        }
        callback(false, [NSError errorWithMessage:errorValue code:100], nil);
      }
    } @catch (NSException *e) {
      callback(false, [NSError errorWithMessage:nil code:100], nil);
    }
  } else {
    callback(false, [NSError errorWithMessage:nil code:100], nil);
  }
}

@end
