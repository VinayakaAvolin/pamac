//
//  PAScript.m
//  PAScriptRunnerService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PAScript.h"
#import "NSString+Additions.h"

@implementation PAScript

- (instancetype)initWithPath:(NSString *)path {
  self = [super init];
  if (self) {
    _scriptPath = path;
  }
  return self;
}

- (instancetype)initWithPath:(NSString *)path
                     handler:(NSString *)handlerName
                  parameters:(id)firstParameter, ...{
  self = [super init];
  if (self) {
    if (path) {
      _scriptPath = path;
    } else {
      self = nil;
    }
  }
  return self;
}

- (instancetype)initWithScriptString:(NSString *)script {
  self = [super init];
  if (self) {
    if (script) {
      _scriptString = script;
    } else {
      self = nil;
    }
  }
  return self;
}

- (void)runWithCallBack:(PAScriptCallback)callback {
}


- (BOOL)setExecutablePermissionToScriptAtPath: (NSString*)scriptPath {
  if ([scriptPath isPathExecutable]) {
    return true;
  }
  // create a dictionary of attributes
  NSDictionary *attributes;
  NSNumber *permissions;
  NSError *error;
  permissions = [NSNumber numberWithUnsignedLong: 0766];
  attributes = [NSDictionary dictionaryWithObject:permissions forKey:NSFilePosixPermissions];
  // Set the permissions
  return [[NSFileManager defaultManager] setAttributes:attributes ofItemAtPath:scriptPath error:&error];
}

@end
