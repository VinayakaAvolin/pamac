//
//  PAScriptInfo.h
//  PrivilegedTool
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PAScriptRunnerConstants.h"

@interface PAScriptInfo : NSObject <NSSecureCoding>

@property (nonatomic) NSString *scriptPath;
@property (nonatomic) NSString *scriptString;
@property (nonatomic, assign) PAScriptType scriptType;
@property (nonatomic, assign) PAScriptMode scriptMode;

- (instancetype)initWithPath:(NSString *)path
                        type:(PAScriptType)scriptType
                        mode:(PAScriptMode)scriptMode;

- (instancetype)initWithScript:(NSString *)scriptString
                          type:(PAScriptType)scriptType
                          mode:(PAScriptMode)scriptMode;

@end
