//
//  NSAppleEventDescriptor+Additions.h
//  PAScriptRunnerService
//
//  Copyright © 2017 Avolin. All rights reserved.
//


#import <Foundation/Foundation.h>

@interface NSAppleEventDescriptor (Additions)

/* Convert an AEDescList of strings to NSArray of strings sorted in alphabetical order. */
- (NSArray*) sortedArrayOfStrings;

/* Convert an AEDescList of strings to NSArray of strings preserving their ordering. */
- (NSArray*) arrayOfStrings;

- (NSDictionary*)scriptingUserDefinedRecord;

@end
