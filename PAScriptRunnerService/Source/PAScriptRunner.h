//
//  PAScriptRunner.h
//  PAScriptRunnerService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PAScriptRunnerConstants.h"

@interface PAScriptRunner : NSObject

+ (void)executeScriptWithPath:(NSString *)path
                     callBack:(PAScriptCallback)callback;
+ (void)executeScriptWithPath:(NSString *)path
                       ofType:(PAScriptType)type
                     callBack:(PAScriptCallback)callback;

+ (void)executeScript:(NSString *)scriptString
               ofType:(PAScriptType)type
             callBack:(PAScriptCallback)callback;

+ (void)executeScript:(NSString *)scriptString
               ofType:(PAScriptType)type
writeToFileBeforeExecute:(BOOL)writeToFile
           scriptName:(NSString *)fileName
             callBack:(PAScriptCallback)callback;

@end
