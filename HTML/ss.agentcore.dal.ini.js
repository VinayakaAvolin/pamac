/** @namespace Holds all ini related functionality*/
$ss.agentcore.dal.ini = $ss.agentcore.dal.ini || {};

(function()
{
  $.extend($ss.agentcore.dal.ini,
  {
    /**
    *  @name GetIniValue
    *  @memberOf $ss.agentcore.dal.ini
    *  @function
    *  @description  Returns the value from the INI file.
    *  @param iniFile path to ini file, if none specified the default ini is considered
    *  @param section section
    *  @param key key
    *  @param defaultVal default value if ini value is not found and default value is specified else null
    *  @returns string
    *  @example
    *         var objIni = $ss.agentcore.dal.ini;
    *         var retVal = objIni.GetIniValue("","SETUP","width",0);
    */
    GetIniValue:function (iniFile, section, key, defaultVal)
    {
      _logger.info('Entered function: $ss.agentcore.dal.ini.GetIniValue');
      var retVal = null;
      
      
      try
      {
        if (iniFile === "" && _iniHash[section + "_" + key]) {
          retVal = _iniHash[section + "_" + key];
        }
        else {
          //[MAC] Check Machine OS Windows/MAC
          if(!bMac)
            retVal = _objContainer.GetIniValue(iniFile, section, key);
          else {
            //=======================================================
            //[MAC] Native Call
            var message = {'JSClassNameKey':'CommonUtility','JSOperationNameKey':'GetIniValue', 'JSArgumentsKey':[iniFile, section, key,defaultVal],  'JSISSyncMethodKey' : '1'}
            var result = jsBridge.execute(message);
            var parsedJSONObject = JSON.parse(result);
            retVal = parsedJSONObject.Data;
            //=======================================================
          }
          _iniHash[section+"_"+key] =retVal;
        }
      }
      catch (err)
      {
        _exception.HandleException(err,'$ss.agentcore.dal.ini','GetIniValue',arguments);
    
        if(defaultVal !== undefined)
        {
          retVal = defaultVal;
        }
        
      }
      return retVal;
    },
    
    /**
    *  @name SetIniValue
    *  @memberOf $ss.agentcore.dal.ini
    *  @function
    *  @description  Sets the value in the specified INI file.
    *  @param iniFile path to ini file, if none specified the default ini is considered
    *  @param section section
    *  @param key key
    *  @param value value to be set
    *  @returns true on success else false
    *  @example
    *         var objIni = $ss.agentcore.dal.ini;
    *         var retVal = objIni.SetIniValue("","SETUP","width",600);
    */
    // TD : implementation in bcont
    SetIniValue:function (iniFile, section, key, value)
    {
      _logger.info('Entered function: $ss.agentcore.dal.ini.SetIniValue');
      var retVal = false;
      try
      {
        //[MAC] Check Machine OS Windows/MAC
        if(!bMac)
          retVal = _objContainer.SetIniValue(iniFile, section, key,value);
        else {
          //=======================================================
          //[MAC] Native Call
          var message = {'JSClassNameKey':'CommonUtility','JSOperationNameKey':'SetIniValue', 'JSArgumentsKey':[iniFile, section, key,value], 'JSISSyncMethodKey' : '1'}
          var result = jsBridge.execute(message);
          var parsedJSONObject = JSON.parse(result);
          retVal = parsedJSONObject.Data;
          //=======================================================
        }
        if (iniFile === "" && retVal) {
          _iniHash[section + "_" + key] = value;
        }
      }
      catch (err)
      {
        _exception.HandleException(err,'$ss.agentcore.dal.ini','SetIniValue',arguments);
      }
      return retVal;
    },
    
    /**
    *  @name GetPath
    *  @memberOf $ss.agentcore.dal.ini
    *  @function
    *  @description  look up path from [PATHS] section in bcont.ini. 
    *  @param pathName name of path to look up under [PATHS] section. <br/>
    *  @returns string
    *  @example
    *         var objIni = $ss.agentcore.dal.ini;
    *         var retVal = objIni.GetPath("EXE_PATH");
    */
    GetPath:function (pathName)
    {
      _logger.info('Entered function: $ss.agentcore.dal.ini.GetPath');
      var retVal = "";
      try
      {
        //if (ss_con_IsUsingBCont())
        {
        //[MAC] Check Machine OS Windows/MAC
        if(!bMac)
          retVal = _objContainer.GetPath(pathName);
        else {
          //=======================================================
          //[MAC] Native Call
          var message = {'JSClassNameKey':'CommonUtility','JSOperationNameKey':'GetPath', 'JSArgumentsKey':[pathName], 'JSISSyncMethodKey' : '1'}
          var result = jsBridge.execute(message);
          var parsedJSONObject = JSON.parse(result);
          retVal = parsedJSONObject.Data;
          //=======================================================
        }  
        }
      }
      catch (err)
      {
        _exception.HandleException(err,'$ss.agentcore.dal.ini','GetPath',arguments);
      }
      return retVal;
    }

  });
  var _iniHash = {};
  
  //[MAC] Get Machine OS type
  var bMac = $.browser.safari;

  //[MAC] JSBridge object used only for MAC
  try {
    var jsBridge = window.JSBridge;
  }
  catch(e) {
  }

  var _objContainer;
  //[MAC] Check Machine OS Windows/MAC
  if(!bMac)
    _objContainer = $ss.agentcore.utils.activex.GetObjInstance();

  var _logger = $ss.agentcore.log.GetDefaultLogger("$ss.agentcore.dal.ini");  
  var _exception = $ss.agentcore.exceptions;
})();