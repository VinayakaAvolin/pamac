﻿/** @namespace Holds all generice string related functionality*/
$ss.agentcore.utils.string = $ss.agentcore.utils;

(function()
{
  $.extend($ss.agentcore.utils.string,
  {
    
    /**
    *  @name IsMemberOfArray
    *  @memberOf $ss.agentcore.utils.string
    *  @function
    *  @description  Checks for a member in the specified array.
    *  @param arr array
    *  @param xMember member to check for
    *  @returns true if xMember is a member of array else false
    *  @example
    *
    *           var arr = ["1","2","3"];
    *           var ret objString.IsMemberOfArray(arr,"2");
    */
    IsMemberOfArray:function (arr, xMember) 
    {
      try {
			
        for (var i=0; i<arr.length; i++)
          if (arr[i] == xMember) return true;
      } catch (e) {
      }
      return false;
    },

    /**
    *  @name RecastString
    *  @memberOf $ss.agentcore.utils.string
    *  @function
    *  @description  Recasts a string into a native JS object.
    *  @param sInput input string
    *  @returns JS Native Type
    *  @example
    *
    *           var val = objString.RecastString("true");
    */
    RecastString:function ( sInput )
    {
      var xOutput = ""+sInput+""; // Make a string, but not an Object->String
      if ( ! isNaN( sInput*1 ) ) {
        xOutput = sInput*1;
      } else if (sInput == "true") {
        xOutput = true;
      } else if (sInput == "false") {
        xOutput = false;
      }
      return xOutput;
    },


    /**
    *  @name ConvertJSObjectToString
    *  @memberOf $ss.agentcore.utils.string
    *  @function
    *  @description  Provides string representation of JavaScript
    *  @param oToSerialize JavaScript object to be serialized
    *  @param nDepth Depth to which string representation should be <br/>
    *                done in cases of nested javascript. Default is 1
    *  @param bNoWhitespace Exclude whitespace from result this value is true
    *  @returns string representation of object.
    *  @example
    *
    *         var obj = new Object();
    *         obj.test1 = "tes";
    *         obj.test3 = "3";
    *         var str = objString.ConvertJSObjectToString(obj);
    */
    ConvertJSObjectToString:function (oToSerialize, nDepth, bNoWhitespace)
    {
      if (typeof(nDepth) == 'undefined')
        nDepth = 1;

      var SPACER = '', SPACERSHORT='', SPACERLONG='', CR='';
      if (bNoWhitespace !== true) {
        for (var i=nDepth; i>0; i--) {
           SPACER += _constants.SERIALIZE_SPACER;
           if (i>1)
             SPACERSHORT+=_constants.SERIALIZE_SPACER;
        }
        SPACERLONG = SPACER+_constants.SERIALIZE_SPACER;
        CR = '\r\n';
      }

      var myObjProps = [];
      var objStr     = "";
      var pre=SPACERSHORT+'{'+CR+SPACER,post=CR+SPACERLONG+'}';
      var bKey = true;

      if (oToSerialize === null)
        return 'null';

      if (oToSerialize === undefined)
        return 'undefined';

      if (oToSerialize === true)
        return 'true';

      if (oToSerialize === false)
        return 'false';

      // TD : is referring to UI,cross check again

      if (typeof window != 'undefined') {
        if (oToSerialize === window)
          return 'window';
        if (oToSerialize === parent && oToSerialize !== window)
          return 'parent';
        if (oToSerialize === opener && oToSerialize !== self)
          return 'opener';
        if (oToSerialize.tagName !== undefined && oToSerialize.id !== undefined)
          return 'document.getElementById("'+oToSerialize.id+'") || null';
      }
      if (oToSerialize.constructor !== undefined) {
        var ObjConstructor = oToSerialize.constructor.toString(); // create a switchable signature
        switch (ObjConstructor)
        {
          case _constants.ARRAY_CONSTRUCTOR :
              pre=SPACERSHORT+'['+CR+SPACER,post=CR+SPACERLONG+']';
              for (var i=0; i<oToSerialize.length; i++) {
                if (oToSerialize.hasOwnProperty(i)) {
                  nDepth++;
                  objStr = SPACER+this.ConvertJSObjectToString(oToSerialize[i],nDepth);
                  nDepth--;
                  myObjProps.push(objStr);
                }
              }
              return pre+myObjProps.join(','+CR+SPACER)+post;

          case _constants.OBJECT_CONSTRUCTOR :
              for (var i in oToSerialize) {
                if (oToSerialize.hasOwnProperty(i)) {
                  nDepth++;
                  objStr = SPACER+this.ConvertJSObjectToString(oToSerialize[i],nDepth);
                  nDepth--;
                  objStr = SPACERLONG+'"'+i+'" : '+objStr;
                  myObjProps.push(objStr);
                }
              }
              return pre+myObjProps.join(','+CR+SPACER)+post;

          case _constants.DATE_CONSTRUCTOR :
              if (oToSerialize.toLocaleString)
                return 'new Date("'+oToSerialize.toLocaleString()+'")';
              return 'new Date('+oToSerialize.valueOf()+')';

          case _constants.FUNCTION_CONSTRUCTOR : return oToSerialize.toString();
          case _constants.STRING_CONSTRUCTOR   : return _CreateStringMember(oToSerialize.toString());
          case _constants.STRING2_CONSTRUCTOR  : return _CreateStringMember(oToSerialize.toString());
          case _constants.NUMBER_CONSTRUCTOR   : return oToSerialize.valueOf();
        }
        return oToSerialize&&oToSerialize.toString()||'{}';
      }
      else if (typeof(oToSerialize) == "object")
      {
        if (typeof(oToSerialize.xml) != "undefined")
        {
          // handle cases when object is DOM
          var sXml = oToSerialize.xml.toString();
          sXml = sXml.replace(/\\"/g, "\\\"");
          return sXml;
        }
        else
        {
          // handle all other object types
          return "[object]";
        }
      }
      return 'undefined'; // TODO -- damage control
    },

    /**
    *  @name ConvertStringToJS
    *  @memberOf $ss.agentcore.utils.string
    *  @function
    *  @description  Converts a raw JS (serialized through ConvertJSObjectToString) <br/>
    *                back into a native JavaScript Object
    *  @param sRawJS Output from ConvertJSObjectToString.
    *  @returns object or null
    *  @example
    *
    *         var obj = new Object();
    *         obj.test1 = "tes";
    *         obj.test3 = "3";
    *         var str = objString.ConvertJSObjectToString(obj);
    *         var jsObj = objString.ConvertStringToJS(str);
    */
    ConvertStringToJS:function( sRawJS )
    {
      var retObj = null;
      try
      {
        eval('retObj = ' + sRawJS);
        return retObj;
      }
      catch (e)
      {
      }
      return null;
    }
    
  });
  
  
  /*******************************************************************************
  **           Functions that extend the native JavaScript String type
  *******************************************************************************/

  /**
  *  @name trim
  *  @memberOf $ss.agentcore.utils.string
  *  @function
  *  @description  Removes leading or trailing spaces from the current string <br/>
  *                object and returns the result
  *  @returns trimmed string object
  *  @example
  *
  *           (" test ").trim();
  */
  String.prototype.trim = function()
  {
    return this.replace(/^\s+|\s+$/g,'');
  }

  /**
  *  @name removehtml
  *  @memberOf $ss.agentcore.utils.string
  *  @function
  *  @description  Removes any html within the current string value and return <br/>
  *                the result
  *  @returns resultant string object
  *  @example
  *
  *           ("<test>t").removehtml();
  */
  String.prototype.removehtml = function()
  {
    return this.replace(/<[^>]*>/g, "");
  }

  /**
  *  @name htmltotext
  *  @memberOf $ss.agentcore.utils.string
  *  @function
  *  @description  Removes any html and replaces well-known html entities with <br/>
  *                their ascii cousin, and returns the result
  *  @returns resultant string object
  *  @example
  *
  *           ("<t >&lt;t&nbsp;").htmltotext();
  */
  String.prototype.htmltotext = function()
  {
    return this.removehtml().replace(/\&amp\;/, '&').replace(/\&nbsp\;/g, ' ').replace(/\&apos\;/g, '\'').replace(/\&lt\;/g, '<').replace(/\&gt\;/g, '>').replace(/\&quot\;/g, '"').trim();
  }

  /**
  *  @name condenseSpaces
  *  @memberOf $ss.agentcore.utils.string
  *  @function
  *  @description  Replaces all groups of spaces with a single space
  *  @param bCrLf Bool if true condenses carriage return; line feeds <br/>
  *               and tabs too
  *  @returns resultant string object
  *  @example
  *
  *         ("t t\r\nt").condenseSpaces(true);
  */
  String.prototype.condenseSpaces = function (bCrLf)
  {
    if((typeof bCrLf != "undefined") && bCrLf)
      return this.replace(/\s+/g, " ");
    else
      return this.replace(/\ +/g, " ");
  }

  /**
  *  @name startsWith
  *  @memberOf $ss.agentcore.utils.string
  *  @function
  *  @description  Checks to see if string starts with a certain prefix
  *  @param prefix prefix to check against
  *  @returns true if the string starts with the prefix else false
  *  @example
  *
  *          ("tttsfdasd").startsWith("ttt");   
  */
  if (!String.prototype.startsWith)
  {
    String.prototype.startsWith = function(prefix)
    {
      return (this.indexOf(prefix) === 0);
    };
  }

  /**
  *  @name endsWith
  *  @memberOf $ss.agentcore.utils.string
  *  @function
  *  @description  Checks to see if string ends with a certain suffix
  *  @param suffix suffix to check against
  *  @returns true if the string ends with the prefix
  *  @example
  *
  *           ("fdasdtttt").endsWith("ttt");
  */
  if (!String.prototype.endsWith)
  {
    String.prototype.endsWith = function(suffix)
    {
      var startPos = this.length - suffix.length;
      if (startPos < 0)  return false;
      return (this.lastIndexOf(suffix, startPos) == startPos);
    };
  }

  /**
  *  @name interpolateString
  *  @memberOf $ss.agentcore.utils.string
  *  @function
  *  @description  Extends string to support interpolation for e.g. replaces %1% <br/>
  *                in the string with the first argument in the arArg array
  *  @param arArg argument array
  *  @returns resultant string object
  *  @example
  *
  *          var arr = ["fdasdtttt","ttt"];
  *          ("test %1% sdfadf %2% asdf").interpolateString(arr);
  */
  String.prototype.interpolateString = function (aArguments)
  {
    var sReturn = this.toString();
    try {
      var objStr = new _InterpolatedString( sReturn , aArguments );
      sReturn = objStr.toString();
    } catch (e) {
      //g_Logger.Error("String.prototype.toInterpolatedString", e.message);
    }
    return sReturn;
  }
  
  /**
  *  @name  format
  *  @memberOf $ss.agentcore.utils.string
  *  @function
  *  @description  Extends string to support printf like functionality <br/>
  *  @param works on the arguments passed to the function
  *  @returns returns the object itself
  *  @example
  *
  *           var str = ("test %1% sdfadf %2% asdf").format("fdasdtttt","ttt");
  */
  String.prototype.format = function()
  {
    var argsArr = Array.prototype.slice.call(arguments);
    return this.interpolateString(argsArr);
    
  }

  /**
  *  @name push
  *  @memberOf $ss.agentcore.utils.string
  *  @function
  *  @description  Extension to native JavaScript Array type to support .push in <br/>
  *                environments where it is not supported (like Internet Explorer 5.0x)
  *  @returns resultant array object
  *  @example
  *
  *           arr.push("arr","asdf");
  */
  if (typeof (Array.prototype.push) == 'undefined' && typeof ([].push) == 'undefined')
  {
    Array.prototype.push = function()
    {
      for (var i=0;i<arguments.length; i++)
        this[this.length]=arguments[i];
      return this.length;
    }
  }
  
   if (!Array.prototype.indexOf) {
      Array.prototype.indexOf = function (value , startIndex) {
      var len = this.length;
      for (var i = (startIndex || 0); i < len; i++) {
        if (this[i] === value) {
          return i;
        }
      }
      return -1;
    }
  }

  if(!Array.prototype.contains){
    Array.prototype.contains    = function (value) {
      return this.indexOf(value) > -1;    
    };
  }
  
  if(!Array.prototype.containsMatch){
    Array.prototype.containsMatch    = function (pattern, flags) {
     if(!pattern || pattern.length==0 || this.length == 0) return false;
     var len = this.length;
     var flgs = flags || 'gi';
     var rgx = new RegExp(pattern,flgs);
      for (var i = 0; i < len; i++) {
        if (rgx.test(this[i])) {
          delete rgx;
          return true;       
        }
      }
      return false;  
    };
  }
  function _InterpolatedString( sBlob , aValues ) 
  {
      sBlob = sBlob || "";
      this.Value  = new String(sBlob).toString();
      this.Macros = aValues || [];
  }
  
  _InterpolatedString.prototype.AddMacro = function (sKey,sValue) {
    this.Macros[sKey] = sValue;
  }
  
  _InterpolatedString.prototype.Interpolate = function ( arrMacros ) {
    var sReturn = this.Value.toString();
    var rxMacro, i;
    try {
      if (typeof(arrMacros) == 'undefined') {
        arrMacros = [];
        for (i=0; i<this.Macros.length; i++ ) {
          arrMacros.push(this.Macros[i]);
        }
      }
      for (i=0; i<arrMacros.length; i++) {
        rxMacro  = new RegExp('%'+(i+1)+'%', "gi");
        sReturn = sReturn.replace(rxMacro,arrMacros[i]);
      }

      var iStart,iEnd,sBufferDone,sBufferRemaining,sMacroKey,sMacroKeyWithChars;
      try {
        sBufferDone = "";
        sBufferRemaining = sReturn;
        while (sBufferRemaining != "" && sBufferRemaining.indexOf("%")>-1) {
          iStart = sBufferRemaining.indexOf("%");
          sBufferDone += sBufferRemaining.substr(0,iStart);
          sBufferRemaining = sBufferRemaining.substring(iStart);
          iEnd = sBufferRemaining.indexOf("%",1);
          if (iEnd == -1) { // Stray %
            sBufferDone += sBufferRemaining.substr(0,1);
            sBufferRemaining = sBufferRemaining.substring(1);
          } else if (sBufferRemaining.indexOf(" ",1)>-1 && sBufferRemaining.indexOf(" ",1)<iEnd) { // %Invalid Macro%
            sBufferDone += sBufferRemaining.substr(0,1);
            sBufferRemaining = sBufferRemaining.substring(1);
          } else {
            sMacroKey = sBufferRemaining.substr(1,iEnd-1);
            sMacroKeyWithChars = sBufferRemaining.substr(0,iEnd+1);
            sBufferDone += this.Macros[sMacroKey] || this.Macros[sMacroKeyWithChars] || sMacroKey;
            sBufferRemaining = sBufferRemaining.substring(iEnd+1);
          }
        }
        sBufferDone += sBufferRemaining;
        sReturn = sBufferDone;
      } catch (ex2) {
				_exception.HandleException(ex,'$ss.agentcore.utils.string','ConvertStringToJS',arguments);
        //g_Logger.Error("InterpolatedString.prototype.Interpolate ex2", ex2.message);
      }
    } catch (ex) {
				_exception.HandleException(ex,'$ss.agentcore.utils.string','ConvertStringToJS',arguments);
      //g_Logger.Error("InterpolatedString.prototype.Interpolate", ex.message);
    }
    return sReturn;
  }

  function _CreateStringMember(sString)
  {
    sString = new String(sString).toString();
    sString = sString.replace(_regexQuote, "\\\"");
    sString = sString.replace(_regexNewLine, '"+\r\n"');
    return '"' + sString + '"';
  }
  
  _InterpolatedString.prototype.toString = _InterpolatedString.prototype.Interpolate;
  _InterpolatedString.prototype.valueOf  = _InterpolatedString.prototype.Interpolate;
  
  var _regexQuote = new RegExp(/\"/g);
  var _regexNewLine = new RegExp(/\r\n|\r|\n/g);
  var _constants = $ss.agentcore.constants;
  
})();










