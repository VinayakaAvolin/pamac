$ss.agentcore.state = $ss.agentcore.state || {};
/** @namespace Holds all ini related functionality*/
$ss.agentcore.state.busy = $ss.agentcore.state.busy || {};

(function()
{
  $.extend($ss.agentcore.state.busy,
  {
  
   /**
    *  @name AddToBusyList
    *  @memberOf $ss.agentcore.state.busy
    *  @function
    *  @description  adds busy string which will be shown on click of close of bcont
    *  @param sSnapinName ID for busy status
    *  @param sMessage busy status  message
    *  @returns true if insert succeeded else false
    *  @example
    *           var oState = $ss.agentcore.state.busy;
    *           oState.AddToBusyList("snapin_perfman", "Snapin Performance Manager","Optimization are currently running");
    */
    AddToBusyList: function(sSnapinName,  sMessage)
    { 
      var oBusy = new BusyObject(sSnapinName, sMessage);
      if(oBusy.name === undefined) return false;
      if(!_busy.aBusyObj[sSnapinName])
        _busy.len++;
      _busy.aBusyObj[sSnapinName] = oBusy;
      _Log(sSnapinName, "busy_start", sMessage);
      return true;
    },

    /**
    *  @name RemoveFromBusyList
    *  @memberOf $ss.agentcore.state.busy
    *  @function
    *  @description  removes the busy status for that ID
    *  @param sSnapinName ID of the busy status to be removed
    *  @returns string if ID is proper else false
    *  @example
    *           var oState = $ss.agentcore.state.busy;
    *           oState.RemoveFromBusyList("snapin_perfman");
    */
    RemoveFromBusyList: function(sSnapinName)
    {
      if(!_IsValidString(sSnapinName))  return false;
      if((_busy.len <= 0) || _busy.aBusyObj[sSnapinName] == undefined) return false;
      var oRet = _busy.aBusyObj[sSnapinName];
      delete _busy.aBusyObj[sSnapinName];
      _busy.len--;
      _Log(sSnapinName, "busy_end", oRet.message)
      return oRet;
    },

    /**
    *  @name IsAnyItemBusy
    *  @memberOf $ss.agentcore.state.busy
    *  @function
    *  @description  function to focus on the bcont
    *  @returns if any busy status set true else false
    *  @example
    *           var oState = $ss.agentcore.state.busy;
    *           oState.IsAnyItemBusy();
    */
    IsAnyItemBusy: function()
    {
      if(_busy.len > 0) return true;
      return false;
    },

    /**
    *  @name IsItemBusy
    *  @memberOf $ss.agentcore.state.busy
    *  @function
    *  @description  function to focus on the bcont
    *  @param sSnapinName ID for which Busy status is set, will be returned
    *  @returns if any busy status set true else false
    *  @example
    *           var oState = $ss.agentcore.state.busy;
    *           oState.IsItemBusy("snapin_perfman");
    */
    IsItemBusy: function(sSnapinName)
    {
      if(!_IsValidString(sSnapinName))  return false;
      if((_busy.len <= 0) || _busy.aBusyObj[sSnapinName] == undefined) return false;
      return true;
    },

    /**
    *  @name GetBusyItems
    *  @memberOf $ss.agentcore.state.busy
    *  @function
    *  @description  function to focus on the bcont
    *  @returns array of current busy messages
    *  @example
    *           var oState = $ss.agentcore.state.busy;
    *           oState.GetBusyItems();
    */
    GetBusyItems: function()
    {
      var aMsg = [], i = 0;
      for (var sMsg in _busy.aBusyObj) {
        if (_busy.aBusyObj.hasOwnProperty(sMsg))
          aMsg[i++] = _busy.aBusyObj[sMsg];
      }
      return aMsg;
    },

    ClearBusyChache: function() {
      _ClearBusyChache();
    }

  
  });
  
  var _utils = $ss.agentcore.utils;

  var _busy = {
    aBusyObj: [],
    len: 0
  };

  function _ClearBusyChache() {
    _busy = {
      aBusyObj: [],
      len: 0
    };
  }

  function BusyObject(sSnapinName, sMessage) {
    if(!_IsValidString(sSnapinName) || !_IsValidString(sMessage)) return;
    var sDisplayName = _utils.LocalXLate(sSnapinName, "titleSnapin");
    if(sDisplayName === "titleSnapin" || !_IsValidString(sDisplayName)) return;
    this.name = sSnapinName;
    this.dispName = sDisplayName;
    this.message = sMessage;
  }

  function _IsValidString(str) {
    if(str === "" || str == undefined || typeof(str) !== "string") return false;
    return true;
  }

  function _Log(sSnapinName, sVerb, sMessage) {
    var oSnapin = $ss.GetSnapin(sSnapinName);
    var oLog = $ss.agentcore.reporting.Reports.CreateLogEntry(oSnapin.id, oSnapin.version, sVerb, "0", "0", sMessage, "", "0", "0");
    $ss.agentcore.reporting.Reports.Log(oLog, sSnapinName);
  }

})()
