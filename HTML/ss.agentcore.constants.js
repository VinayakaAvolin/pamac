/** @namespace Holds all the constants*/
$ss.agentcore.constants = $ss.agentcore.constants || {};
  
(function() 
{
  var _const = $ss.agentcore.constants;     
  $.extend($ss.agentcore.constants, 
  {
    /*******************************************************************************
    **    Shell API
    *******************************************************************************/
    SHELL_LOAD_OK               : "SHELL_LOAD_OK",
    SHELL_PACKAGE_FOUND            : "SHELL_PACKAGE_FOUND",
    SHELL_PACKAGE_NOTFOUND            : "SHELL_PACKAGE_NOTFOUND",
    SHELL_LAYOUT_FOUND            : "SHELL_LAYOUT_FOUND",
    SHELL_LAYOUT_NOTFOUND            : "SHELL_LAYOUT_NOTFOUND",
    SHELL_SNAPIN_FOUND            : "SHELL_SNAPIN_FOUND",
    SHELL_SNAPIN_NOTFOUND            : "SHELL_SNAPIN_NOTFOUND",
    SHELL_SKINCSS_NOTFOUND          : "SHELL_SKIN_NOT_FOUND",
    SHELL_LOAD_CANCEL            : "SHELL_LOAD_CANCEL",
    SHELL_LOAD_UNKNOWN_ERROR     : "SHELL_LOAD_UNKNOWN_ERROR",
    SHELL_FIRST_SYNC_NOTDONE     : "SHELL_FIRST_SYNC_NOTDONE",
    SHELL_SYNC_IN_PROGRESS         : "SHELL_SYNC_IN_PROGRESS",    

    SHELL_NO_DEFCOPY_ON_ERROR : {
      SHELL_SKINCSS_NOTFOUND: "SHELL_SKIN_NOT_FOUND",
      SHELL_LOAD_UNKNOWN_ERROR: "SHELL_LOAD_UNKNOWN_ERROR",
      SHELL_FIRST_SYNC_NOTDONE: "SHELL_FIRST_SYNC_NOTDONE",
      SHELL_SYNC_IN_PROGRESS: "SHELL_SYNC_IN_PROGRESS"
    },

    
    /*******************************************************************************
    **    Events API
    *******************************************************************************/

    SENDMSG_RESULT_OK               : "SENDMSG_RESULT_OK",
    SENDMSG_RESULT_PROCESSED        : "SENDMSG_RESULT_PROCESSED",
    SENDMSG_RESULT_CANCEL_OPERATION : "SENDMSG_RESULT_CANCEL_OPERATION",
    SENDMSG_RESULT_ERROR            : "SENDMSG_RESULT_ERROR",

    SENDMSG_STACKEND_MSGSET : {
      SENDMSG_RESULT_CANCEL_OPERATION : _const.SENDMSG_RESULT_CANCEL_OPERATION,
      SENDMSG_RESULT_PROCESSED : _const.SENDMSG_RESULT_PROCESSED
    },

    SENDMSG_ERROR_FREEDSCRIPT : -2146823277,



    /*******************************************************************************
    **    History
    *******************************************************************************/
    HST_TYPE             : 'type',
    HST_SCOPE            : 'scope',
    HST_DELETED          : 'deleted',
    HST_TYPE_DATABAG     : 'databag',
    HST_TYPE_NAVIGATION  : 'navigation',
    HST_TYPE_GLOBAL      : 'global',
    HST_TYPE_NONE        : 'none',


    RESULT_ABORTED : 0,
    RESULT_FAILED  : 1,
    RESULT_SUCCESS : 2,
    RESULT_SUCCESS_NOCHANGE : 3,



    /*******************************************************************************
    **    BCONT Run Command
    *******************************************************************************/
    
    /**
    *  @name BCONT_RUNCMD_NORMAL
    *  @memberOf $ss.agentcore.constants
    *  @constant
    *  @description Runs a command in BCONT in normal mode
    */
    BCONT_RUNCMD_NORMAL    : 0,
    
    /**
    *  @name BCONT_RUNCMD_MINIMIZED
    *  @memberOf $ss.agentcore.constants
    *  @constant
    *  @description Runs a command in BCONT in minimized state
    */
    BCONT_RUNCMD_MINIMIZED : 1,
    
    /**
    *  @name BCONT_RUNCMD_HIDDEN
    *  @memberOf $ss.agentcore.constants
    *  @constant
    *  @description Runs a command in BCONT in hidden state
    */
    BCONT_RUNCMD_HIDDEN    : 2,
    
    /**
    *  @name BCONT_RUNCMD_ASYNC
    *  @memberOf $ss.agentcore.constants
    *  @constant
    *  @description Runs a command in BCONT in async mode, returns immedialtely
    */
    BCONT_RUNCMD_ASYNC     : 4,


    /*******************************************************************************
    ** BCONT Multiple Instance Event handling
    *******************************************************************************/
    BCONT_MINI_RUNNING : "/miniisrunning",
    BCONT_CLOSE        : "/close",

    /*******************************************************************************
    **    BCONT HTTP Request Modes
    *******************************************************************************/
    /**
    *  @name BCONT_REQUEST_GETFILE
    *  @memberOf $ss.agentcore.constants
    *  @constant
    *  @description Request to get a file from remore location
    */
    BCONT_REQUEST_GETFILE        : 0,
    
    /**
    *  @name BCONT_REQUEST_GETFILEIFNEWER
    *  @memberOf $ss.agentcore.constants
    *  @constant
    *  @description Request to get a file if newer from remote location
    */
    BCONT_REQUEST_GETFILEIFNEWER : 1,
    
    /**
    *  @name BCONT_REQUEST_PUTFILE
    *  @memberOf $ss.agentcore.constants
    *  @constant
    *  @description Request to put a file to remote location
    */
    BCONT_REQUEST_PUTFILE        : 2,
    
    /**
    *  @name BCONT_REQUEST_POSTFILE
    *  @memberOf $ss.agentcore.constants
    *  @constant
    *  @description Request to post a file to remote location
    */
    BCONT_REQUEST_POSTFILE       : 3,
    
    /**
    *  @name BCONT_REQUEST_EXISTS
    *  @memberOf $ss.agentcore.constants
    *  @constant
    *  @description Request to check if a file exists
    */
    BCONT_REQUEST_EXISTS         : 4,
    
    /**
    *  @name BCONT_REQUEST_GETFILE_WITH_AUTH
    *  @memberOf $ss.agentcore.constants
    *  @constant
    *  @description Request to get a file with authentication
    */
    BCONT_REQUEST_GETFILE_WITH_AUTH : 5,
    
    /**
    *  @name BCONT_REQUEST_TEST_AUTH
    *  @memberOf $ss.agentcore.constants
    *  @constant
    *  @description Request for authentication test
    */
    BCONT_REQUEST_TEST_AUTH : 6,
    
    
    /**
    *  @name BCONT_REQUEST_TYPES
    *  @memberOf $ss.agentcore.constants
    *  @constant
    *  @description Enum holding the BCONT HTTP request types
    */
    BCONT_REQUEST_TYPES : { // usage: if (type in BCONT_REQUEST_TYPES}
      0:_const.BCONT_REQUEST_GETFILE,
      1:_const.BCONT_REQUEST_GETFILEIFNEWER,
      2:_const.BCONT_REQUEST_PUTFILE,
      3:_const.BCONT_REQUEST_POSTFILE,
      4:_const.BCONT_REQUEST_EXISTS,
      5:_const.BCONT_REQUEST_GETFILE_WITH_AUTH ,
      6:_const.BCONT_REQUEST_TEST_AUTH
    },


    /*******************************************************************************
    **    Win32 Service Status Code
    *******************************************************************************/
    BCONT_SERVICE_STOPPED          : 1,
    BCONT_SERVICE_START_PENDING    : 2,
    BCONT_SERVICE_STOP_PENDING     : 3,
    BCONT_SERVICE_RUNNING          : 4,
    BCONT_SERVICE_CONTINUE_PENDING : 5,
    BCONT_SERVICE_PAUSE_PENDING    : 6,
    BCONT_SERVICE_PAUSED           : 7,

    /*******************************************************************************
    **    Win32 Service Startup Mode
    *******************************************************************************/
    BCONT_SERVICE_BOOT_START : 0,     //only valid for driver type of services
    BCONT_SERVICE_SYSTEM_START : 1,   //only valid for driver type of services
    BCONT_SERVICE_AUTO_START : 2,
    BCONT_SERVICE_DEMAND_START : 3,
    BCONT_SERVICE_DISABLED : 4,

    /*******************************************************************************
    **    Win32 Service Types
    *******************************************************************************/
    BCONT_SERVICE_KERNEL_DRIVER       :   0x00000001,
    BCONT_SERVICE_FILE_SYSTEM_DRIVER  :   0x00000002,
    BCONT_SERVICE_ADAPTER             :   0x00000004,
    BCONT_SERVICE_RECOGNIZER_DRIVER   :   0x00000008,
    BCONT_SERVICE_DRIVER              :   _const.BCONT_SERVICE_KERNEL_DRIVER | _const.BCONT_SERVICE_FILE_SYSTEM_DRIVER | _const.BCONT_SERVICE_RECOGNIZER_DRIVER,
    BCONT_SERVICE_WIN32_OWN_PROCESS   :   0x00000010,
    BCONT_SERVICE_WIN32_SHARE_PROCESS :   0x00000020,
    BCONT_SERVICE_WIN32               :   _const.BCONT_SERVICE_WIN32_OWN_PROCESS | _const.BCONT_SERVICE_WIN32_SHARE_PROCESS,
    BCONT_SERVICE_INTERACTIVE_PROCESS :   0x00000100,
    BCONT_SERVICE_TYPE_ALL            :   _const.BCONT_SERVICE_WIN32  | _const.BCONT_SERVICE_ADAPTER | _const.BCONT_SERVICE_DRIVER  | _const.BCONT_SERVICE_INTERACTIVE_PROCESS,

    /*******************************************************************************
    **    Drive Types Constants
    *******************************************************************************/
    BCONT_DRIVE_REMOVABLE  : 1,
    BCONT_DRIVE_FIXED      : 2,
    BCONT_DRIVE_REMOTE     : 4,
    BCONT_DRIVE_CDROM      : 8,
    BCONT_DRIVE_RAMDISK    : 16,

    /*******************************************************************************
    **    IEEE 802.11 OIDs
    *******************************************************************************/
    OID_GEN_LINK_SPEED                      : 0x00010107,
    OID_GEN_PHYSICAL_MEDIUM                 : 0x00010202,
    OID_802_3_CURRENT_ADDRESS               : 0x01010102,
    OID_802_11_BSSID                        : 0x0D010101,
    OID_802_11_SSID                         : 0x0D010102,
    OID_802_11_NETWORK_TYPES_SUPPORTED      : 0x0D010203,
    OID_802_11_NETWORK_TYPE_IN_USE          : 0x0D010204,
    OID_802_11_TX_POWER_LEVEL               : 0x0D010205,
    OID_802_11_RSSI                         : 0x0D010206,
    OID_802_11_RSSI_TRIGGER                 : 0x0D010207,
    OID_802_11_INFRASTRUCTURE_MODE          : 0x0D010108,
    OID_802_11_FRAGMENTATION_THRESHOLD      : 0x0D010209,
    OID_802_11_RTS_THRESHOLD                : 0x0D01020A,
    OID_802_11_NUMBER_OF_ANTENNAS           : 0x0D01020B,
    OID_802_11_RX_ANTENNA_SELECTED          : 0x0D01020C,
    OID_802_11_TX_ANTENNA_SELECTED          : 0x0D01020D,
    OID_802_11_SUPPORTED_RATES              : 0x0D01020E,
    OID_802_11_DESIRED_RATES                : 0x0D010210,
    OID_802_11_CONFIGURATION                : 0x0D010211,
    OID_802_11_STATISTICS                   : 0x0D020212,
    OID_802_11_ADD_WEP                      : 0x0D010113,
    OID_802_11_REMOVE_WEP                   : 0x0D010114,
    OID_802_11_DISASSOCIATE                 : 0x0D010115,
    OID_802_11_POWER_MODE                   : 0x0D010216,
    OID_802_11_BSSID_LIST                   : 0x0D010217,
    OID_802_11_AUTHENTICATION_MODE          : 0x0D010118,
    OID_802_11_PRIVACY_FILTER               : 0x0D010119,
    OID_802_11_BSSID_LIST_SCAN              : 0x0D01011A,
    OID_802_11_WEP_STATUS                   : 0x0D01011B,
    OID_802_11_ENCRYPTION_STATUS            : _const.OID_802_11_WEP_STATUS,
    OID_802_11_RELOAD_DEFAULTS              : 0x0D01011C,

    /*******************************************************************************
    **    typedef enum _NDIS_PHYSICAL_MEDIUM
    *******************************************************************************/
    NdisPhysicalMediumUnspecified : 0,
    NdisPhysicalMediumWirelessLan : 1,
    NdisPhysicalMediumCableModem  : 2,
    NdisPhysicalMediumPhoneLine   : 3,
    NdisPhysicalMediumPowerLine   : 4,
    NdisPhysicalMediumDSL         : 5,   //includes ADSL and UADSL (G.Lite)
    NdisPhysicalMediumFibreChannel: 6,
    NdisPhysicalMedium1394        : 7,
    NdisPhysicalMediumWirelessWan : 8,
    NdisPhysicalMediumMax         : 9,   //Not a real physical type, defined as an upper-bound

    /*******************************************************************************
    **  typedef enum Type of Network Adapters to Filter 
    *******************************************************************************/
    FilterTypeVirtual           : 0x00000001,
    FilterTypeBridge            : 0x00000002, //Not supported as of now
    FilterTypeDisabled          : 0x00000004,
    FilterTypeWireless          : 0x00000008,    
    FilterTypeFirewire          : 0x00000010,

    /*******************************************************************************
    **    Registry types
    *******************************************************************************/
    /**
    *  @name REG_SZ
    *  @memberOf $ss.agentcore.constants
    *  @constant 
    *  @description Specifies the type of data stored in the registry value as string
    */    
    REG_SZ       : 1,
    /**
    *  @name REG_BINARY
    *  @memberOf $ss.agentcore.constants
    *  @constant 
    *  @description Specifies the type of data stored in the registry value as binary
    */
    REG_BINARY   : 3,
    /**
    *  @name REG_DWORD
    *  @memberOf $ss.agentcore.constants
    *  @constant 
    *  @description Specifies the type of data stored in the registry value as DWORD
    */
    REG_DWORD    : 4,
    /**
    *  @name REG_MULTI_SZ
    *  @memberOf $ss.agentcore.constants
    *  @constant 
    *  @description Specifies the type of data stored in the registry value as array of null terminated strings
    */
    REG_MULTI_SZ : 7,

    /*******************************************************************************
    **    Registry Paths
    *******************************************************************************/
    REG_TREE  : "HKLM",
    REG_HKCU  : "HKCU",
    REG_SPRT  : "Software\\SupportSoft\\",
    REG_SETUP : _const.REG_SPRT + "setup",
    REG_USERS : _const.REG_SPRT + "users",


    /******************************************************************************
    Please Wait
    ******************************************************************************/
    PW_MANAGE_PLEASEWAITFORBUSY : "clsManagePleaseWaitForBusy",
    PW_MANAGE_CURSORFORBUSY     : "clsManageCursorForBusy",
    PW_MANAGE_FORCEPLEASEWAIT   : "clsForcePleaseWait",
    PW_IDPLEASEWAITTASK         : 'idPleaseWaitTask',
    PW_IDPLEASEWAITMESSAGE      : 'idPleaseWaitMessage',
    PW_IDPLEASEWAITANIMATION    : 'idPleaseWaitAnimation',
    PW_IDPLEASEWAITCANCEL       : 'idPleaseWaitCancel',
    
    CALLBACK_STATUS : { 
      NOT_STARTED : 0,
      IN_PROGRESS : 1,
      COMPLETE : 2,
      FAILED : 3
    },
    
    FILE_MODE:{
      READ : 1,
      WRITE : 2,
      APPEND : 8
    },
    
    OS_TYPE : {               // Numeric representation of OSes in chronological order
      UNKNOWN  : 0,
      WIN95    : 1,
      WIN98    : 2,
      WINNT    : 3,
      WINME    : 4,
      WIN2K    : 5,
      WIN2KS   : 6,  // 2000 Server
      WINXP    : 7,
      WINXPMCE : 8,  // XP Media Center Edition
      WIN2K3   : 9,  // 2003 Server
      WINVST   : 10, // Vista Consumer
      WINVSTS  : 11, // Vista Servers
      WIN7WS   : 12, // Windows 7
      WIN8     : 13,  // Windows 8
//SDK71:Changes for Win8.1  Compatability -start
      WIN8_1: 14,
//SDK71:Changes for Win8.1  Compatability -ends
//Changes for Windows10 Compatability
      WIN10: 15
    },
    
    SNAPINPATH_KEY : "snapinpath",
    ACCT_USERINFO_KEY : "UserInfo",
    UNDEFINED     : 'undefined',
    OBJECT        : 'object',
    STRING        : 'string',
    
    HISTORY : {
      TYPE : 'type',
      SCOPE : 'scope',
      DELETED : 'deleted'
    },
	
	REGVIEW : {
      DEFAULT_VIEW : 0,
      ONLY_64BIT_VIEW : 1,
      ONLY_32BIT_VIEW : 2,
	  NATIVE_VIEW : 3
    },
    
    STRING_CONSTRUCTOR   : "".constructor.toString(),
    STRING2_CONSTRUCTOR  : new String().constructor.toString(),
    ARRAY_CONSTRUCTOR    : [].constructor.toString(),
    OBJECT_CONSTRUCTOR   : {}.constructor.toString(),
    NUMBER_CONSTRUCTOR   : new Number().constructor.toString(),
    FUNCTION_CONSTRUCTOR : (function() { }).constructor.toString(),
    DATE_CONSTRUCTOR     : new Date().constructor.toString(),
    TRUE_CONSTRUCTOR     : true.constructor.toString(),
    FALSE_CONSTRUCTOR    : false.constructor.toString(),
    SERIALIZE_SPACER     : ' ',
    
    MOVEFILE_REPLACE_EXISTING : 1,  
    MOVEFILE_COPY_ALLOWED : 2  ,
    MOVEFILE_DELAY_UNTIL_REBOOT : 4,  
    MOVEFILE_WRITE_THROUGH : 8  ,
    MOVEFILE_CREATE_HARDLINK : 10, 
    MOVEFILE_FAIL_IF_NOT_TRACKABLE : 20 ,
    
    BCONT_MUTEX_STRING : "__SDC_BCONT_MUTEX__",
    
    NEWOVERWRITE : 1,
    USEFOLDERINFO :2,

    NORMAL_COPY: 0,
    XCOPY:1,
    
    INTERNET_PER_CONN_FLAGS : 1,
    PROXY_TYPE_AUTO_DETECT  : 0x00000008,   // use autoproxy detection
    INTERNET_PER_CONN_AUTOCONFIG_RELOAD_DELAY_MINS   : 7,
    INET_BROWSER_IE      : "Internet Explorer",
    INET_BROWSER_FIREFOX : "Mozilla Firefox",
    INET_BROWSER_SAFARI  : "Safari",
    INET_BROWSER_NETSCAPE: "Netscape",
    INET_BROWSER_OPERA   : "Opera",
    INET_BROWSER_CHROME  : "Google Chrome"
    
  });
})();
