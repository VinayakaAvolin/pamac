/**
 * Javascript MVC (http://code.google.com/p/javascriptmvc/)
 *
 * Software License Agreement (MIT License) http://www.opensource.org/licenses/mit-license.php  * 
 * Permission is hereby granted, free of charge, to any person obtaining 
 * a copy of this software and associated documentation files (the 
 * "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, 
 * distribute, sublicense, and/or sell copies of the Software, and to 
 * permit persons to whom the Software is furnished to do so. 
 * 
 * 
 */

MVC = {
  OPTIONS: {},
  Test: {},
  Included: {controllers: [], resources: [], models: [], plugins: [], views:[]},
  _no_conflict: false,
  no_conflict: function(){ MVC._no_conflict = true  },
  File: function(path){ this.path = path; },
  Initializer: function(f) {
    MVC.user_initialize_function = f;
    //include.set_path(MVC.mvc_root);
    //include('framework');
    //set path back to application root and load the user initialize function.
    //include.set_path(MVC.apps_root);
    MVC.user_initialize_function();
  },
  Runner: function(f){
    if(!window.in_command_window && !window._rhino)
      f();
  },
  Ajax: {},
  Browser: {
      IE:     !!(window.attachEvent && !window.opera),
      Opera:  !!window.opera,
      WebKit: navigator.userAgent.indexOf('AppleWebKit/') > -1,
      Gecko:  navigator.userAgent.indexOf('Gecko') > -1 && navigator.userAgent.indexOf('KHTML') == -1,
      MobileSafari: !!navigator.userAgent.match(/Apple.*Mobile.*Safari/)
  },
  
  mvc_root: null,
  include_path: null,
  root: null,
  Object:  { extend: function(d, s) { for (var p in s) d[p] = s[p]; return d;} },
  $E: function(id){ return typeof id == 'string' ? document.getElementById(id): id },
  app_name: 'app',
  request: function(path){
         var request = MVC.Ajax.factory();
        request.open("GET", path, false);
         try{request.send(null);}
      catch(e){return null;}
       if ( request.status == 404 || request.status == 2 ||(request.status == 0 && request.responseText == '') ) return null;
         return request.responseText;
  }
};
  
var File = MVC.File;
MVC.File.prototype = {
  clean: function(){
    return this.path.match(/([^\?#]*)/)[1];
  },
  dir: function(){
    var last = this.clean().lastIndexOf('/');
    return last != -1 ? this.clean().substring(0,last) : ''; //this.clean();
  },
  domain: function(){ 
    if(this.path.indexOf('file:') == 0 ) return null;
    var http = this.path.match(/^(?:https?:\/\/)([^\/]*)/);
    return http ? http[1] : null;
  },
  join: function(url){
    return new File(url).join_from(this.path);
  },
  join_from: function( url, expand){
    if(this.is_domain_absolute()){
      var u = new File(url);
      if(this.domain() && this.domain() == u.domain() ) 
        return this.after_domain();
      else if(this.domain() == u.domain()) { // we are from a file
        return this.to_reference_from_same_domain(url);
      }else
        return this.path;
    }else if(url == MVC.page_dir && !expand){
      return this.path;
    }else{
      if(url == '') return this.path.replace(/\/$/,'');
      var urls = url.split('/'), paths = this.path.split('/'), path = paths[0];
      if(url.match(/\/$/) ) urls.pop();
      while(path == '..' && paths.length > 0){
        paths.shift();
        urls.pop();
        path =paths[0];
      }
      return urls.concat(paths).join('/');
    }
  },
  relative: function(){    return this.path.match(/^(https?:|file:|\/)/) == null;},
  after_domain: function(){  return this.path.match(/(?:https?:\/\/[^\/]*)(.*)/)[1];},
  to_reference_from_same_domain: function(url){
    var parts = this.path.split('/'), other_parts = url.split('/'), result = '';
    while(parts.length > 0 && other_parts.length >0 && parts[0] == other_parts[0]){
      parts.shift(); other_parts.shift();
    }
    for(var i = 0; i< other_parts.length; i++) result += '../';
    return result+ parts.join('/');
  },
  is_cross_domain : function(){
    if(this.is_local_absolute()) return false;
    return this.domain() != new File(location.href).domain();
  },
  is_local_absolute : function(){  return this.path.indexOf('/') === 0},
  is_domain_absolute : function(){return this.path.match(/^(https?:|file:)/) != null}
};

//TODO - clean the root
MVC.root = new File("");
MVC.page_dir = new File(window.location.href).dir();  
MVC.random = '?'+Math.random();
MVC.Ajax.factory = function(){ return window.ActiveXObject ? new ActiveXObject("Microsoft.XMLHTTP") : new XMLHttpRequest();};



/**
 * Inclusion of the jquey_helpers file from the JavascriptMVC Framework
 * 
 */

/* Start of jquery_helpers.js */

MVC.String = {};
MVC.String.strip = function(string){
  return string.replace(/^\s+/, '').replace(/\s+$/, '');
};


MVC.Function = {};
MVC.Function.params = function(func){
  var ps = func.toString().match(/^[\s\(]*function[^(]*\((.*?)\)/)[1].split(",");
  if( ps.length == 1 && !ps[0]) return [];
  for(var i = 0; i < ps.length; i++) ps[i] = MVC.String.strip(ps[i]);
  return ps;
};


MVC.Native ={};
MVC.Native.extend = function(class_name, source){
  if(!MVC[class_name]) MVC[class_name] = {};
  var dest = MVC[class_name];
  for (var property in source){
    dest[property] = source[property];
    if(!MVC._no_conflict){
      window[class_name][property] = source[property];
      if(typeof source[property] == 'function'){
        var names = MVC.Function.params(source[property]);
          if( names.length == 0) continue;
        var first_arg = names[0];
        if( first_arg.match(class_name.toLowerCase()) || (first_arg == 'func' && class_name == 'Function' )  ){
          MVC.Native.set_prototype(class_name, property, source[property]);
        }
      }
    }
  }
};
MVC.Native.set_prototype = function(class_name, property_name, func){
  if(!func) func = MVC[class_name][property_name];
    window[class_name].prototype[property_name] = function(){
    var args = [this];
    for (var i = 0, length = arguments.length; i < length; i++) args.push(arguments[i]);
    return func.apply(this,args  );
  };
};

MVC.Object.extend = jQuery.extend

MVC.Object.to_query_string = function(object,name){
  if(typeof object != 'object') return object;
  return MVC.Object.to_query_string.worker(object,name).join('&');
};
MVC.Object.to_query_string.worker = function(obj,name){
  var parts2 = [];
  for(var thing in obj){
    if(obj.hasOwnProperty(thing)) {
      var value = obj[thing];
      if(typeof value != 'object'){
        var nice_val = encodeURIComponent(value.toString());
        var newer_name = encodeURIComponent(name ? name+'['+thing+']' : thing) ;
        parts2.push( newer_name+'='+nice_val )  ;
      }else{
        parts2 = parts2.concat( MVC.Object.to_query_string.worker(value,  name ? name+'['+thing+']' : thing ))
      }
    }
  }
  return parts2;
};

MVC.Native.extend('String', {
  capitalize : function(string) {
    return string.charAt(0).toUpperCase()+string.substr(1).toLowerCase();
  },
  include : function(string, pattern){
    return string.indexOf(pattern) > -1;
  },
  ends_with : function(string, pattern) {
      var d = string.length - pattern.length;
      return d >= 0 && string.lastIndexOf(pattern) === d;
  },
  camelize: function(string){
    var parts = string.split(/_|-/);
    for(var i = 1; i < parts.length; i++)
      parts[i] = MVC.String.capitalize(parts[i]);
    return parts.join('');
  },
  classize: function(string){
    var parts = string.split(/_|-/);
    for(var i = 0; i < parts.length; i++)
      parts[i] = MVC.String.capitalize(parts[i]);
    return parts.join('');
  },
  strip : MVC.String.strip
});




//Array helpers
Array.from = function(iterable){
  if (!iterable) return [];
  return jQuery.makeArray(iterable);
}

Array.prototype.include = function(thing){
  return jQuery.inArray(thing, this) != -1;
};


MVC.Native.extend('Array',{ 
  include: function(array, thing){
    return jQuery.inArray(thing, array) != -1;
  },
  from: function(iterable){
     if (!iterable) return [];
     return jQuery.makeArray(iterable);
  }
});



//Function Helpers
MVC.Native.extend('Function', {
  bind: function(func) {
    var args = MVC.Array.from(arguments);
    args.shift();args.shift();
    var __method = func, object = arguments[1];
    return function() {
      return __method.apply(object, args.concat(MVC.Array.from(arguments) )  );
    }
  },
  params: MVC.Function.params
});


/* End of jquery_helpers.js */

MVC.Included.plugins.push('helpers');


/**
 * Inclusion of the inflector.js file from the JavascriptMVC Framework
 * 
 */

/* Start of inflector.js */


// based on the Inflector class found on a DZone snippet contributed by Todd Sayre
// http://snippets.dzone.com/posts/show/3205

MVC.Inflector = {
  Inflections: {
    plural: [
    [/(quiz)$/i,               "$1zes"  ],
    [/^(ox)$/i,                "$1en"   ],
    [/([m|l])ouse$/i,          "$1ice"  ],
    [/(matr|vert|ind)ix|ex$/i, "$1ices" ],
    [/(x|ch|ss|sh)$/i,         "$1es"   ],
    [/([^aeiouy]|qu)y$/i,      "$1ies"  ],
    [/(hive)$/i,               "$1s"    ],
    [/(?:([^f])fe|([lr])f)$/i, "$1$2ves"],
    [/sis$/i,                  "ses"    ],
    [/([ti])um$/i,             "$1a"    ],
    [/(buffal|tomat)o$/i,      "$1oes"  ],
    [/(bu)s$/i,                "$1ses"  ],
    [/(alias|status)$/i,       "$1es"   ],
    [/(octop|vir)us$/i,        "$1i"    ],
    [/(ax|test)is$/i,          "$1es"   ],
    [/s$/i,                    "s"      ],
    [/$/,                      "s"      ]
    ],
    singular: [
    [/(quiz)zes$/i,                                                    "$1"     ],
    [/(matr)ices$/i,                                                   "$1ix"   ],
    [/(vert|ind)ices$/i,                                               "$1ex"   ],
    [/^(ox)en/i,                                                       "$1"     ],
    [/(alias|status)es$/i,                                             "$1"     ],
    [/(octop|vir)i$/i,                                                 "$1us"   ],
    [/(cris|ax|test)es$/i,                                             "$1is"   ],
    [/(shoe)s$/i,                                                      "$1"     ],
    [/(o)es$/i,                                                        "$1"     ],
    [/(bus)es$/i,                                                      "$1"     ],
    [/([m|l])ice$/i,                                                   "$1ouse" ],
    [/(x|ch|ss|sh)es$/i,                                               "$1"     ],
    [/(m)ovies$/i,                                                     "$1ovie" ],
    [/(s)eries$/i,                                                     "$1eries"],
    [/([^aeiouy]|qu)ies$/i,                                            "$1y"    ],
    [/([lr])ves$/i,                                                    "$1f"    ],
    [/(tive)s$/i,                                                      "$1"     ],
    [/(hive)s$/i,                                                      "$1"     ],
    [/([^f])ves$/i,                                                    "$1fe"   ],
    [/(^analy)ses$/i,                                                  "$1sis"  ],
    [/((a)naly|(b)a|(d)iagno|(p)arenthe|(p)rogno|(s)ynop|(t)he)ses$/i, "$1$2sis"],
    [/([ti])a$/i,                                                      "$1um"   ],
    [/(n)ews$/i,                                                       "$1ews"  ],
    [/s$/i,                                                            ""       ]
    ],
    irregular: [
    ['move',   'moves'   ],
    ['sex',    'sexes'   ],
    ['child',  'children'],
    ['man',    'men'     ],
    ['foreman', 'foremen'],
    ['person', 'people'  ]
    ],
    uncountable: [
    "sheep",
    "fish",
    "series",
    "species",
    "money",
    "rice",
    "information",
    "equipment"
    ]
  },
  pluralize: function(word) {
    for (var i = 0; i < MVC.Inflector.Inflections.uncountable.length; i++) {
      var uncountable = MVC.Inflector.Inflections.uncountable[i];
      if (word.toLowerCase() == uncountable) {
        return uncountable;
      }
    }
    for (var i = 0; i < MVC.Inflector.Inflections.irregular.length; i++) {
      var singular = MVC.Inflector.Inflections.irregular[i][0];
      var plural   = MVC.Inflector.Inflections.irregular[i][1];
      if ((word.toLowerCase() == singular) || (word == plural)) {
        return word.substring(0,1)+plural.substring(1);
      }
    }
    for (var i = 0; i < MVC.Inflector.Inflections.plural.length; i++) {
      var regex          = MVC.Inflector.Inflections.plural[i][0];
      var replace_string = MVC.Inflector.Inflections.plural[i][1];
      if (regex.test(word)) {
        return word.replace(regex, replace_string);
      }
    }
  },
  singularize: function(word) {
    for (var i = 0; i < MVC.Inflector.Inflections.uncountable.length; i++) {
      var uncountable = MVC.Inflector.Inflections.uncountable[i];
      if (word.toLowerCase() == uncountable) {
        return uncountable;
      }
    }
    for (var i = 0; i < MVC.Inflector.Inflections.irregular.length; i++) {
      var singular = MVC.Inflector.Inflections.irregular[i][0];
      var plural   = MVC.Inflector.Inflections.irregular[i][1];
      if ((word.toLowerCase() == singular) || (word.toLowerCase() == plural)) {
        return word.substring(0,1)+singular.substring(1);
      }
    }
    for (var i = 0; i < MVC.Inflector.Inflections.singular.length; i++) {
      var regex          = MVC.Inflector.Inflections.singular[i][0];
      var replace_string = MVC.Inflector.Inflections.singular[i][1];
      if (regex.test(word)) {
        return word.replace(regex, replace_string);
      }
    }
  }
};


MVC.Native.extend('String', {
  pluralize: function(string, count, plural) {
    if (typeof count == 'undefined') {
      return MVC.Inflector.pluralize(string);
    } else {
      return count + ' ' + (1 == parseInt(count) ? string : plural || MVC.Inflector.pluralize(string));
    }
  },
  singularize: function(string, count) {
    if (typeof count == 'undefined') {
      return MVC.Inflector.singularize(string);
    } else {
      return count + " " + MVC.Inflector.singularize(string);
    }
  },
  is_singular: function(string){
    if(MVC.String.singularize(string) == null && string)
        return true;
    return false;
  }
});

/* End of inflector.js */

MVC.Included.plugins.push('inflector');


/**
 * Inclusion of the event /standard.js file from the JavascriptMVC Framework
 * 
 */

/* Start of event standard.js */

// The code from the event plugin comes from 
// JavaScript: the Definitive Guide by David Flanagan
// Copyright 2006 O'Reilly Media

if(document.addEventListener) {
  MVC.Event = {
    observe: function(el, eventType, handler, capture) {
      if(capture == null) capture = false; 
      el.addEventListener(eventType, handler, capture);
      },
    stop_observing : function(el, eventType, handler) {
          if(capture == null) capture = false;
          el.removeEventListener(eventType, handler, false);
      }
  };
}else if(document.attachEvent) {
  MVC.Event={
  observe: function(element, eventType, handler) {
        if (MVC.Event._find(element, eventType, handler) != -1) return;
        var wrappedHandler = function(e) {
            if (!e) e = window.event;
            
            
            
            var event = {
                _event: e, 
                type: e.type, 
                target: e.srcElement,  
                currentTarget: element, 
                relatedTarget: eventType == 'mouseover' ?e.fromElement : e.toElement, //mouseout gets toElement
                eventPhase: (e.srcElement==element)?2:3,
                clientX: e.clientX, clientY: e.clientY,
                screenX: e.screenX, screenY: e.screenY,
                altKey: e.altKey, ctrlKey: e.ctrlKey,
                shiftKey: e.shiftKey, charCode: e.keyCode,
                stopPropagation: function() {this._event.cancelBubble = true;},
                preventDefault: function() {this._event.returnValue = false;}
            };

            if (Function.prototype.call) 
                handler.call(element, event);
            else {
                element._currentHandler = handler;
                element._currentHandler(event);
                element._currentHandler = null;
            }
        };
        element.attachEvent("on" + eventType, wrappedHandler);
        var h = {
            element: element,
            eventType: eventType,
            handler: handler,
            wrappedHandler: wrappedHandler
        };
        var d = element.document || element, w = d.parentWindow, id = MVC.Event._uid(); 
        if (!w._allHandlers) w._allHandlers = {}; 
        w._allHandlers[id] = h;
        if (!element._handlers) element._handlers = [];
        element._handlers.push(id);
        if (!w._onunloadHandlerRegistered) {
            w._onunloadHandlerRegistered = true;
            w.attachEvent("onunload", MVC.Event._removeAllHandlers);
        }
    },
  stop_observing: function(element, eventType, handler) {
        var i = MVC.Event._find(element, eventType, handler);
        if (i == -1) return; 
        var d = element.document || element, w = d.parentWindow, handlerId = element._handlers[i], h = w._allHandlers[handlerId];
        element.detachEvent("on" + eventType, h.wrappedHandler);
        element._handlers.splice(i, 1);
        delete w._allHandlers[handlerId];
    },
  _find: function(element, eventType, handler) {
        var handlers = element._handlers;
        if (!handlers) return -1;
        var d = element.document || element, w = d.parentWindow;
        for(var i = handlers.length-1; i >= 0; i--) {
            var h = w._allHandlers[handlers[i]];
            if(h.eventType == eventType && h.handler == handler)  return i;
        }
        return -1;
    },
  _removeAllHandlers: function() {
        var w = this;
        for(var id in w._allHandlers) {
            if(! w._allHandlers.hasOwnProperty(id) ) continue;
      var h = w._allHandlers[id]; 
            if(h.element) h.element.detachEvent("on" + h.eventType, h.wrappedHandler);
            delete w._allHandlers[id];
        }
    },
  _counter : 0,
  _uid : function() { return "h" + MVC.Event._counter++; }
  };
};

if(!MVC._no_conflict){
  Event = MVC.Event;
}

/* End of event standard.js */

MVC.Included.plugins.push('event');


/**
 * Inclusion of the ajax /jquery_ajax.js file from the JavascriptMVC Framework
 * 
 */

/* Start of ajax/jquery_ajax.js */


(function(){
  var factory = MVC.Ajax.factory;
  MVC.Ajax = function(url,options){
    //map options
    options.url = url;
    this.transport = {};
  
    options.complete = function(xmlhttp, status){
      if(options.onComplete){
        options.onComplete(xmlhttp);
      }
      if(options.onSuccess && status == 'success')
        options.onSuccess(xmlhttp);
      
      if(options.onFailure && status == 'error')
        options.onFailure(xmlhttp);
  
    };
  
    if(options.asynchronous != null){
      options.async = options.asynchronous;
    }
    if(options.parameters){
      options.data = options.parameters;
    }
    if(options.method){
      options.type = options.method
    }
    this.options = options;
    this.url = url;
    if(options.async){
      return jQuery.ajax(options);
    }else{
      return {transport: jQuery.ajax(options) };
    }
    
  };
  MVC.Ajax.factory = factory;
})();

if(!MVC._no_conflict) Ajax = MVC.Ajax;



/* End of event jquery_ajax.js */

MVC.Included.plugins.push('ajax');


/**
 * Inclusion of the class /setup.js file from the JavascriptMVC Framework
 * 
 */

/* Start of event setup.js */
//MVC.Class 
// This is a modified version of John Resig's class
// It provides class level inheritence and callbacks.

(function(){
  var initializing = false, fnTest = /xyz/.test(function(){xyz;}) ? /\b_super\b/ : /.*/;
  // The base Class implementation (does nothing)
  MVC.Class = function(){};
  // Create a new Class that inherits from this class
  MVC.Class.extend = function(className, klass, proto) {
    if(typeof className != 'string'){
        proto = klass;
        klass = className;
        className = null;
    }
    if(!proto){
        proto = klass;
        klass = null;
    }
    var _super_class = this;
    var _super = this.prototype;
    // Instantiate a base class (but only create the instance,
    // don't run the init constructor)
    initializing = true;
    var prototype = new this();
    initializing = false;
    // Copy the properties over onto the new prototype
    for (var name in proto) {
      // Check if we're overwriting an existing function
      prototype[name] = typeof proto[name] == "function" &&
        typeof _super[name] == "function" && (fnTest.test(proto[name]) || name === "index") ?
        (function(name, fn){
          return function() {
            var tmp = this._super;
           
            // Add a new ._super() method that is the same method
            // but on the super-class
            this._super = _super[name];
           
            // The method only need to be bound temporarily, so we
            // remove it when we're done executing
            var ret = fn.apply(this, arguments);       
            this._super = tmp;
           
            return ret;
          };
        })(name, proto[name]) :
        proto[name];
    }
    // The dummy class constructor
    function Class() {
      // All construction is actually done in the init method
      if ( !initializing && this.init )
        this.init.apply(this, arguments);
    }
    // Populate our constructed prototype object
    Class.prototype = prototype;
    Class.prototype.Class = Class;
    // Enforce the constructor to be what we expect
    Class.constructor = Class;
    // And make this class extendable
    
    for(var name in this){
        if(this.hasOwnProperty(name) && name != 'prototype'){
            Class[name] = this[name];
        }
    }
    
    for (var name in klass) {
      Class[name] = typeof klass[name] == "function" &&
        typeof Class[name] == "function" && fnTest.test(klass[name]) ?
        (function(name, fn){
          return function() {
            var tmp = this._super;
            this._super = _super_class[name];
            var ret = fn.apply(this, arguments);       
            this._super = tmp;
            return ret;
          };
        })(name, klass[name]) :
        klass[name];
  };
    Class.extend = arguments.callee;
    if(className) Class.className = className;
    
    if(Class.init) Class.init(Class);
    if(_super_class.extended) _super_class.extended(Class);
    
    return Class;
  };
})();

if(!MVC._no_conflict && typeof Class == 'undefined'){
  Class = MVC.Class;
}

/* End of  class/setup.js */

MVC.Included.plugins.push('class');


/**
 * Inclusion of the view /view.js file from the JavascriptMVC Framework
 * 
 */

/* Start of view/view.js */

/*--------------------------------------------------------------------------
 *  MVC.View - Embedded JavaScript, version 0.1.0
 *  Copyright (c) 2007 Edward Benson
 *  http://www.edwardbenson.com/projects/MVC.View
 *  ------------------------------------------------------------------------
 *
 *  EJS is freely distributable under the terms of an MIT-style license.
 *
 *  EJS is a client-side preprocessing engine written in and for JavaScript.
 *  If you have used PHP, ASP, JSP, or ERB then you get the idea: code embedded
 *  in <% // Code here %> tags will be executed, and code embedded in <%= .. %> 
 *  tags will be evaluated and appended to the output. 
 * 
 *  This is essentially a direct JavaScript port of Masatoshi Seki's erb.rb 
 *  from the Ruby Core, though it contains a subset of ERB's functionality. 
 * 
 * 
 *  Usage:
 *      // source should be either a string or a DOM node whose innerHTML
 *      // contains EJB source.
 *    var source = "<% var ejb="EJB"; %><h1>Hello, <%= ejb %>!</h1>"; 
 *      var compiler = new MVC.View.Compiler(source);    
 *      compiler.compile();  
 *      var output = eval(compiler.out);
 *      alert(output); // -> "<h1>Hello, EJB!</h1>"
 *       
 *  For a demo:      see demo.html
 *  For the license: see license.txt
 *
 *--------------------------------------------------------------------------*/


MVC.View = function( options ){
  
  this.set_options(options);
  //Added by Sprt 
  this.contentpath="";
  options.contentpath="";
  options.layoutpath="";
  options.snapinpath= "";
  
  if($ss) {
      try{
          options.layoutpath = $ss.GetLayoutAbsolutePath();
          if(options.controller_name) {
              options.snapinpath = $ss.getSnapinAbsolutePath(options.controller_name) || "";
              options.snapinname = options.controller_name;
          }
      } catch(e) {}
  }
  
  
  if(options.precompiled){
    this.template = {};
    this.template.process = options.precompiled;
    MVC.View.update(this.name, this);
    return;
  }
  var local_text = "";
  
  var guidFolderRegEx=/([\\\/][\w\S]{8}\-[\w\S]{4}\-[\w\S]{4}\-[\w\S]{4}\-[\w\S]{12}\.\d+[\\\/])/;

  if(options.absolute_url){
        var url = options.absolute_url ;
        options.url = options.absolute_url ;
        //Added by SPRT to get the base content URL
        try {
          var matchUrl = url.match(guidFolderRegEx);
          if (matchUrl) {
            var pos = matchUrl.lastIndex;
            //SPRT: lastIndex is undefined in higher IE versions (on Emulation mode)
            pos = (typeof pos == 'undefined' && matchUrl.length > 0) ? matchUrl.index + matchUrl[0].length : (pos || 0);
            this.contentpath = url.slice(0, pos);
            options.contentpath = this.contentpath;
          }
        } catch(e) {};
    var template = MVC.View.get(options.url, this.cache);
    if (template) {
      this.template = template;
      return;
    }
      if (template == MVC.View.INVALID_PATH) return null;
        local_text = MVC.request(url+(this.cache || window._rhino ? '' : '?'+Math.random() ));
    
    if(local_text == null){
      throw( {type: 'JMVC', message: 'There is no template at '+url}  );
    }
    this.name = options.url;
  }else if(options.hasOwnProperty('element'))
  {
        if(typeof options.element == 'string'){
      var name = options.element;
      options.element = MVC.$E(  options.element );
      if(options.element == null) throw name+'does not exist!';
    }
    if(options.element.value){
      local_text = options.element.value;
    }else{
      local_text = options.element.innerHTML;
    }
    this.name = options.element.id;
    this.type = '[';
  }
  var template = new MVC.View.Compiler(local_text, this.type);

  template.compile(options);
  template.contentpath = this.contentpath;
  template.text=null;
  local_text =null;
  MVC.View.update(this.name, template,options.cache);
  this.template = template;
  delete template.out;
  delete template.post_cmd;
  delete template.pre_cmd;
  delete template.scanner;
  delete template.source;
  delete template.text;
  template = null;
};
MVC.View.prototype = {
  render : function(object, extra_helpers){
    object = object || {};
    var v = new MVC.View.Helpers(object);
        MVC.Object.extend(v, extra_helpers || {} );
    var outString = this.template.process.call(object, object,v);
    delete this.template;
    delete v;
    return outString;
  },
  out : function(){
    return this.template.out;
  },
  set_options : function(options){
    this.type = options.type != null ? options.type : MVC.View.type;
    this.cache = options.cache != null ? options.cache : MVC.View.cache;
    this.text = options.text != null ? options.text : null;
    this.name = options.name != null ? options.name : null;
  },
  // called without options, returns a function that takes the object
  // called with options being a string, uses that as a url
  // called with options as an object
  update : function(element, options){
        if(typeof element == 'string'){
      element = MVC.$E(element);
    }
    if(options == null){
      _template = this;
      return function(object){
        MVC.View.prototype.update.call(_template, element, object);
      };
    }
    if(typeof options == 'string'){
      params = {};
      params.url = options;
      _template = this;
      params.onComplete = function(request){
        var object = eval( "("+ request.responseText+")" );
        MVC.View.prototype.update.call(_template, element, object);
      };
            if(!MVC.Ajax) alert('You must include the Ajax plugin to use this feature');
      new MVC.Ajax(params.url, params);
    }else
    {
      element.innerHTML = this.render(options);
    }
  }
};


/* Make a split function like Ruby's: "abc".split(/b/) -> ['a', 'b', 'c'] */
String.prototype.rsplit = function(regex) {
  var item = this;
  var result = regex.exec(item);
  var retArr = new Array();
  while (result != null)
  {
    var first_idx = result.index;
    var last_idx = regex.lastIndex;
    if ((first_idx) != 0)
    {
      var first_bit = item.substring(0,first_idx);
      retArr.push(item.substring(0,first_idx));
      item = item.slice(first_idx);
    }    
    retArr.push(result[0]);
    item = item.slice(result[0].length);
    result = regex.exec(item);  
  }
  if (! item == '')
  {
    retArr.push(item);
  }
  return retArr;
};

/* Chop is nice to have too */
String.prototype.chop = function() {
  return this.substr(0, this.length - 1);
};

/* Adaptation from the Scanner of erb.rb  */
MVC.View.Scanner = function(source, left, right) {
  this.left_delimiter =   left +'%';  //<%
  this.right_delimiter =   '%'+right;  //>
  this.double_left =     left+'%%';
  this.double_right =   '%%'+right;
  this.left_equal =     left+'%=';
  this.left_comment =   left+'%#';
  //Added by SPRT
  this.snapin_path = '%SNAPINPATH%';
  this.content_path = '%CONTENTPATH%';
  this.layout_path  = '%LAYOUTPATH%';
  this.layout_skin_path = '%LSKINPATH%';
  this.lang_code = '%LANGCODE%';
  
  if(left=='[')
    this.SplitRegexp = /(\[%%)|(%%\])|(\[%=)|(\[%#)|(\[%)|(%\]\n)|(%\])|(%CONTENTPATH%)|(%LAYOUTPATH%)| (%LSKINPATH%)| (%SNAPINPATH%)| (%LANGCODE%)| (%MACRO:([\w\S]+)%) | (%RES:([\w\S]+)%) | (%LOCG:([\w\-]+)%) | (%LOCL:?([\w\-]+:)?([\w\-]+)%) | (\n)/;
  else
    this.SplitRegexp = new RegExp('('+this.double_left+')|(%%'+this.double_right+')|('+this.left_equal+')|('+this.left_comment+')|('+this.left_delimiter+')|('+this.right_delimiter+'\n)|('+this.right_delimiter+')|('+this.snapin_path+')|('+this.layout_path+')|('+this.layout_skin_path+')|('+this.content_path+')|('+this.lang_code+')| (%MACRO:([\w\S]+)%)| (%RES:([\w\S]+)%) | (%LOCG:([\w\-]+)%) | (%LOCL:?([\w\-]+:)?([\w\-]+)%) | (\\n)') ;
  
  this.source = source;
  this.stag = null;
  this.lines = 0;
};

MVC.View.Helpers = function(data){
  this.data = data;
};
MVC.View.Helpers.prototype = {
  partial: function(options, data){
    if(!data) data = this.data;
    return new MVC.View(options).render(data);
  },
  to_text: function(input, null_text) {
      if(input == null || input === undefined) return null_text || '';
      if(input instanceof Date) return input.toDateString();
    if(input.toString) return input.toString().replace(/\n/g, '<br />').replace(/''/g, "'");
    return '';
  }
};


MVC.View.Scanner.to_text = function(input){
  if(input == null || input === undefined)
        return '';
    if(input instanceof Date)
    return input.toDateString();
  if(input.toString) 
        return input.toString();
  return '';
};

MVC.View.Scanner.prototype = {

  /* For each line, scan! */
  scan: function(block) {
     scanline = this.scanline;
   regex = this.SplitRegexp;
   if (! this.source == '')
   {
      var source_split = this.source.rsplit(/\n/);
      for(var i=0; i<source_split.length; i++) {
        var item = source_split[i];
       this.scanline(item, regex, block);
     }
   }
  },
  
  /* For each token, block! */
  scanline: function(line, regex, block) {
   this.lines++;
   var line_split = line.rsplit(regex);
    for(var i=0; i<line_split.length; i++) {
     var token = line_split[i];
       if (token != null) {
         try{
             block(token, this);
       }catch(e){
        throw {type: 'MVC.View.Scanner', line: this.lines};
      }
       }
   }
  }
};

/* Adaptation from the Buffer of erb.rb  */
MVC.View.Buffer = function(pre_cmd, post_cmd) {
  this.line = new Array();
  this.script = "";
  this.pre_cmd = pre_cmd;
  this.post_cmd = post_cmd;
  for (var i=0; i<this.pre_cmd.length; i++)
  {
    this.push(pre_cmd[i]);
  }
};
MVC.View.Buffer.prototype = {
  
  push: function(cmd) {
  this.line.push(cmd);
  },

  cr: function() {
  this.script = this.script + this.line.join('; ');
  this.line = new Array();
  this.script = this.script + "\n";
  },

  close: function() {
  if (this.line.length > 0)
  {
    for (var i=0; i<this.post_cmd.length; i++)
    {
      this.push(pre_cmd[i]);
    }
    this.script = this.script + this.line.join('; ');
    line = null;
  }
  }
   
};

/* Adaptation from the Compiler of erb.rb  */
MVC.View.Compiler = function(source, left) {
    this.pre_cmd = ['var ___ViewO = "";'];
  this.post_cmd = new Array();
  this.source = ' ';  
  if (source != null)
  {
    if (typeof source == 'string')
    {
        source = source.replace(/\r\n/g, "\n");
            source = source.replace(/\r/g,   "\n");
      this.source = source;
    }else if (source.innerHTML){
      this.source = source.innerHTML;
    } 
    if (typeof this.source != 'string'){
      this.source = "";
    }
  }
  left = left || '<';
  var right = '>';
  switch(left) {
    case '[':
      right = ']';
      break;
    case '<':
      break;
    default:
      throw left+' is not a supported deliminator';
      break;
  }
  this.scanner = new MVC.View.Scanner(this.source, left, right);
  this.out = '';
};
MVC.View.Compiler.prototype = {
  compile: function(options) {
    options = options || {};
    var contentpath = options.contentpath || "";
    var snapinpath = options.snapinpath || "";
    var layoutpath = options.layoutpath || "";
    var sLSkinPath = $ss.GetLayoutSkinPath() || layoutpath;
    var sLangCode = $ss.GetShellLang() || "en";
    var snapinname = options.controller_name || "";
    
  this.out = '';
  var put_cmd = "___ViewO += ";
  var insert_cmd = put_cmd;
  var buff = new MVC.View.Buffer(this.pre_cmd, this.post_cmd);    
  var content = '';
  var MacroRegEx = /%MACRO:([\w\S]+)%/;
  var resRegEx = /%RES:([\w\S]+)%/ ;
  var locGRegEx = /%LOCG:([\w\-]+)%/ ;
  var locLRegEx = /%LOCL:?([\w\-]+:)?([\w\-]+)%/ ;
  
  var clean = function(content)
  {
      content = content.replace(/\\/g, '\\\\');
        content = content.replace(/\n/g, '\\n');
        content = content.replace(/"/g,  '\\"'); //' Fixes Emacs syntax highlighting
        return content;
  };
  this.scanner.scan(function(token, scanner) {
    if (scanner.stag == null)
    {
      switch(token) {
        case '\n':
          content = content + "\n";
          buff.push(put_cmd + '"' + clean(content) + '";');
          buff.cr();
          content = '';
          break;
        case scanner.left_delimiter:
        case scanner.left_equal:
        case scanner.left_comment:
          scanner.stag = token;
          if (content.length > 0)
          {
            buff.push(put_cmd + '"' + clean(content) + '"');
          }
          content = '';
          break;
        case scanner.double_left:
          content = content + scanner.left_delimiter;
          break;
        case scanner.content_path: //sprt added to support few specialized macros
          content = content + contentpath;
          break;  
        case scanner.layout_path:
          content = content + layoutpath;
          break;
        case scanner.snapin_path:
          content = content + snapinpath;
          break;          
        case scanner.layout_skin_path:
          content = content + sLSkinPath;
          break;          
        case scanner.lang_code:
          content = content + sLangCode;
          break;          
        default:
          if(MacroRegEx.test(token)) token = $ss.agentcore.dal.config.ParseMacros(token.replace(/%MACRO:/g,"%")); //sprt support for the macros
          if (resRegEx.test(token)) {
            
            var y = token.match(/%RES:([\w\S]+)%/g);
            for(var x=0; x < y.length; x ++ ) {
              var guid = resRegEx.exec(y[x]);
              guid = guid || {};
              token = token.replace(y[x], $ss.agentcore.dal.content.GetResource(guid[1]));
            }
          };
          if (locGRegEx.test(token)) {
            var y = token.match(/%LOCG:([\w\-]+)%/g);
            for(var x=0; x < y.length; x ++ ) {
              var locStr = locGRegEx.exec(y[x]);
              locStr = locStr || {};
              token = token.replace(y[x], $ss.agentcore.utils.GlobalXLate(locStr[1]));
            }
          };
          if (locLRegEx.test(token)) {
            var y = token.match(/%LOCL:([\w\-:]+)%/g);
            for(var x=0; x < y.length; x ++ ) {
              var locStr = locLRegEx.exec(y[x]);
              locStr = locStr || {};
              var tempSnapinName = (locStr[1] || '').replace(":", ""); //SPRT: In latest IE mode, value for locStr[1] is undefined
              tempSnapinName = tempSnapinName === "" ?   snapinname : tempSnapinName
              token = token.replace(y[x], $ss.agentcore.utils.LocalXLate(tempSnapinName, (locStr[2] || '')));
            }
          };
          content = content + token;
          break;
      }
    }
    else {
      switch(token) {
        case scanner.right_delimiter:
          switch(scanner.stag) {
            case scanner.left_delimiter:
              if (content[content.length - 1] == '\n')
              {
                content = content.chop();
                buff.push(content);
                buff.cr();
              }
              else {
                buff.push(content);
              }
              break;
            case scanner.left_equal:
              buff.push(insert_cmd + "(MVC.View.Scanner.to_text(" + content + "))");
              break;
          }
          scanner.stag = null;
          content = '';
          break;
        case scanner.double_right:
          content = content + scanner.right_delimiter;
          break;
        case scanner.double_left:
          content = content + scanner.left_delimiter;
          break;
        case scanner.content_path:   //sprt added to support few specialized macros
          content = content + contentpath;
          break;  
        case scanner.layout_path:
          content = content + layoutpath;
          break;
        case scanner.snapin_path:   
          content = content + snapinpath;  
          break;
        case scanner.layout_skin_path:
          content = content + sLSkinPath;
          break;          
        case scanner.lang_code:
          content = content + sLangCode;
          break;          
        default:
          if(MacroRegEx.test(token)) token = $ss.agentcore.dal.config.ParseMacros(token.replace(/%MACRO:/g,"%")); //sprt added to support macros
          if(resRegEx.test(token)) {
            var y = token.match(/%RES:([\w\S]+)%/g);
            for(var x=0; x < y.length; x ++ ) {
              var guid = resRegEx.exec(y[x]);
              guid = guid || {};
              token = token.replace(y[x], $ss.agentcore.dal.content.GetResource(guid[1]));
            }
          }
          if (locGRegEx.test(token)) {
            
            var y = token.match(/%LOCG:([\w\-]+)%/g);
            for(var x=0; x < y.length; x ++ ) {
              var locStr = locGRegEx.exec(y[x]);
              locStr = locStr || {};
              token = token.replace(y[x], $ss.agentcore.utils.GlobalXLate(locStr[1]));
            }
          };
          if (locLRegEx.test(token)) {
            
            var y = token.match(/%LOCL:([\w\-]+)%/g);
            for(var x=0; x < y.length; x ++ ) {
              var locStr = locLRegEx.exec(y[x]);
              var tempSnapinName = locStr[1].replace(":","");
              tempSnapinName = tempSnapinName === "" ?   snapinname : tempSnapinName
              token = token.replace(y[x],  $ss.agentcore.utils.LocalXLate(tempSnapinName,locStr[2]));
            }
          };
          content = content + token;
          break;
      }
    }
  });
  if (content.length > 0)
  {
    if($ss.helper && $ss.helper.ReplaceResInContent) {
      content = $ss.helper.ReplaceResInContent(content);
    }
    // Chould be content.dump in Ruby
    buff.push(put_cmd + '"' + clean(content) + '"');
  }
  buff.close();
  this.out = buff.script + ";";
  var to_be_evaled = 'this.process = function(_CONTEXT,_VIEW) { try { with(_VIEW) { with (_CONTEXT) {'+this.out+" return ___ViewO;}}}catch(e){e.lineNumber=null;throw e;}};";
  
  try{
    eval(to_be_evaled);
  }catch(e){
    
    if(typeof JSLINT != 'undefined'){
      JSLINT(this.out);
      for(var i = 0; i < JSLINT.errors.length; i++){
        var error = JSLINT.errors[i];
        if(error.reason != "Unnecessary semicolon."){
          error.line++;
          var e = new Error();
          e.lineNumber = error.line;
          e.message = error.reason;
          if(options.url)
            e.fileName = options.url;
          throw e;
        }
      }
    }else{
      throw e;
    }
  }
  }
};


//type, cache, folder

MVC.View.config = function(options){
  MVC.View.cache = options.cache != null ? options.cache : MVC.View.cache;
  MVC.View.type = options.type != null ? options.type : MVC.View.type;
  var templates_directory = {}; //nice and private container
  
  MVC.View.get = function(path, cache){
    if(cache == false) return null;
    if(templates_directory[path]) return templates_directory[path];
      return null;
  };
  
  MVC.View.update = function(path, template,cache) { 
    if(path == null) return;
    //SPRT - Commented to remove the holding of anonymous view code
    if (template && cache) {
      delete template.out;
      delete template.post_cmd;
      delete template.pre_cmd;
      delete template.scanner;
      delete template.source;
      delete template.text;
      templates_directory[path] = template;
    }
  };
  
  MVC.View.INVALID_PATH =  -1;
};


MVC.View.PreCompiledFunction = function(name, f){
  new MVC.View({name: name, precompiled: f});
};


MVC.Included.views = [];

MVC.View.config( {cache: false, type: '<' } );

MVC.View.process_include = function(script){
    var view = new MVC.View({text: script.text});
  return 'MVC.View.PreCompiledFunction("'+script.original_path+
        '", function(_CONTEXT,_VIEW) { try { with(_VIEW) { with (_CONTEXT) {'+view.out()+" return ___ViewO;}}}catch(e){e.lineNumber=null;throw e;}})";
};

if(!MVC._no_conflict){
  View = MVC.View;
}


/* End of  view/view.js */

MVC.Included.plugins.push('view');


/**
 * Inclusion of the controller /delegator.js file from the JavascriptMVC Framework
 * 
 */

/* Start of controller/delegator.js */


MVC.Delegator={
  node_path: function(el){
    var body = document.documentElement,parents = [],iterator =el;
    while(iterator != body){
      parents.unshift({tag: iterator.nodeName, className: iterator.className, id: iterator.id, element: iterator});
      iterator = iterator.parentNode;
      if(iterator == null) return [];
    }
    parents.push(body);
    return parents;
  },
  dispatch_event: function(event){
    var target = event.target, matched = false, ret_value = true,matches = [];
    //SPRT modification
    if(!target) return false;
        //Check if the user is allowed to click or not ---
        var isAllowed = true;
        if(MainController) {
          if (MainController.CheckPropagation) {
            isAllowed = MainController.CheckPropagation(event);
            if (isAllowed !== false) 
              isAllowed = true;
          }
        }; 
    if(!isAllowed) 
    {
      event.preventDefault();
      return false;
    }
    
    var delegation_events = MVC.Delegator.events[event.type];
        var parents_path = MVC.Delegator.node_path(target);
        
    for(var i =0; i < delegation_events.length;  i++){
      var delegation_event = delegation_events[i];
      var match_result = delegation_event.match(target, event, parents_path);
      if(match_result){
        matches.push(match_result);
      }
    }

    if(matches.length == 0) return true;
    MVC.Controller.add_kill_event(event);
    matches.sort(MVC.Delegator.sort_by_order);
        var match;
    for(var m = 0; m < matches.length; m++){
            match = matches[m];
            ret_value = match.delegation_event._func( {event: event, element: match.node} ) && ret_value;
      if(event.is_killed()) return false;
    }
  },
    sort_by_order: function(a,b){
      if(a.order < b.order) return 1;
      if(b.order < a.order) return -1;
      var ae = a._event, be = b._event;
      if(ae == 'click' &&  be == 'change') return 1;
      if(be == 'click' &&  ae == 'change') return -1;
      return 0;
    }
};
MVC.Delegator.events = {};
MVC.DelegationEvent = function(selector, event, f){
    this._event = event;
    this._selector = selector;
    this._func = f;

    if(event == 'submit' && MVC.Browser.IE) return this.submit_for_ie();
  if(event == 'change' && MVC.Browser.IE) return this.change_for_ie();
  if(event == 'change' && MVC.Browser.WebKit) return this.change_for_webkit();
  
    this.add_to_delegator();
};
MVC.DelegationEvent.prototype = {
    event: function(){
      if(MVC.Browser.IE){
            if(this._event == 'focus')
          return 'activate';
        else if(this._event == 'blur')
          return 'deactivate';
      }
      return this._event;
    }, 
    capture: function(){
        return MVC.Array.include(['focus','blur'],this._event);
    },
    add_to_delegator: function(selector, event, func){
        var s = selector || this._selector;
        var e = event || this.event();
        var f = func || this._func;
        if(!MVC.Delegator.events[e]){
            MVC.Event.observe(document.documentElement, e, MVC.Delegator.dispatch_event, this.capture() );
            MVC.Delegator.events[e] = [];
    }
    MVC.Delegator.events[e].push(this);
    },
    
    submit_for_ie : function(){
    this.add_to_delegator(null, 'click');
        this.add_to_delegator(null, 'keypress');
        
        this.filters= {
      click : function(el, event){
        return el.nodeName.toUpperCase() == 'INPUT' && el.type.toLowerCase() == 'submit';
      },
      keypress : function(el, event){
        if(el.nodeName.toUpperCase()!= 'INPUT') return false;
        if(typeof Prototype != 'undefined'){ return event.keyCode == 13; }
        return event.charCode == 13;
      }
    };
  },
  change_for_ie : function(){
    this.add_to_delegator(null, 'click');
        this.filters= {
      click : function(el, event){
        if(typeof el.selectedIndex == 'undefined') return false; //sometimes it won't exist yet
        var old = el.getAttribute('_old_value');
        if(el.nodeName.toUpperCase() == 'SELECT' && old == null){
          el.setAttribute('_old_value', el.selectedIndex);
          return false;
        }else if(el.nodeName.toUpperCase() == 'SELECT'){
          if(old == el.selectedIndex.toString()) return false;
          el.setAttribute('_old_value', null);
          return true;
        }
      }
    };
  },
  change_for_webkit : function(){
    this.controller.add_register_action(this,document.documentElement, 'change');
    this.filters= {
      change : function(el, event){
        if(typeof el.value == 'undefined') return false; //sometimes it won't exist yet
        var old = el.getAttribute('_old_value');
        el.setAttribute('_old_value', el.value);
        return el.value != old;
      }
    };
  },
    selector_order : function(){
    if(this.order) return this.order;
    var selector_parts = this._selector.split(/\s+/);
    var patterns = {tag :        /^\s*(\*|[\w\-]+)(\b|$)?/,
                id :            /^#([\w\-\*]+)(\b|$)/,
              className :     /^\.([\w\-\*]+)(\b|$)/};
    var order = [];
    for(var i =0; i< selector_parts.length; i++){
      var v = {}, r, p =selector_parts[i];
      for(var attr in patterns){
        if( patterns.hasOwnProperty(attr) ){
          if( (r = p.match(patterns[attr]))  ) {
            if(attr == 'tag')
              v[attr] = r[1].toUpperCase();
            else
              v[attr] = r[1];
            p = p.replace(r[0],'');
          }
        }
      }
      order.push(v);
    }
    this.order = order;
    return this.order;
  },
    
    match: function(el, event, parents){
        if(this.filters && !this.filters[event.type](el, event)) return null;
    //if(this.controller.className != 'main' &&  (el == document.documentElement || el==document.body) ) return false;
    var matching = 0;
    for(var n=0; n < parents.length; n++){
      var node = parents[n], match = this.selector_order()[matching], matched = true;
      for(var attr in match){
        if(!match.hasOwnProperty(attr) || attr == 'element') continue;
        if(match[attr] && attr == 'className'){
          if(! MVC.Array.include(node.className.split(' '),match[attr])) matched = false;
        }else if(match[attr] && node[attr] != match[attr]){
          matched = false;
        }
      }
      if(matched){
        matching++;
        if(matching >= this.selector_order().length) return {node: node.element, order: n, delegation_event: this};
      }
    }
    return null;
    }
};


/* End of  controller/delegator.js */

MVC.Included.plugins.push('delegator');

/**

JavaScriptMVC is released under the MIT license. 
  
Copyright (c) 2008 Javascript MVC, http://javascriptmvc.com

The MIT License

Copyright (c) <year> <copyright holders>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.


*/
/**
 * Inclusion of the controller /controller.js file from the JavascriptMVC Framework
 * 
 */

/* Start of controller/controller.js */


// submitted by kangax
MVC.Object.is_number = function(o){
    return o &&(  typeof o == 'number' || ( typeof o == 'string' && !isNaN(o) ) );
};

MVC.Controller = MVC.Class.extend({
    init: function(){
        if(!this.className) return;
        MVC.Controller.controllers.push(this);
        var val, act;
        this.actions = {};
        for(var action_name in this.prototype){
        val = this.prototype[action_name];
        if( typeof val == 'function' && action_name != 'Class'){
                for(var a = 0 ; a < MVC.Controller.actions.length; a++){
                    act = MVC.Controller.actions[a];
                    if(act.matches(action_name)){
                        this.actions[action_name] =new act(action_name, val, this);
                    }
                }
            }
      }
        this.modelName = MVC.String.classize(
            this.className 
      //SPRT Update - No singularize function to be supported.
      //MVC.String.is_singular(this.className) ? this.className : MVC.String.singularize(this.className)
        );

//        //load tests
//        if(include.get_env() == 'test'){
//            var path = MVC.root.join('test/functional/'+this.className+'_controller_test.js');
//        var exists = include.check_exists(path);
//        if(exists)
//          MVC.Console.log('Loading: "test/functional/'+this.className+'_controller_test.js"');
//        else {
//          MVC.Console.log('Test Controller not found at "test/functional/'+this.className+'_controller_test.js"');
//          return;
//        }
//        var p = include.get_path();
//        include.set_path(MVC.root.path);
//        include('test/functional/'+ this.className+'_controller_test.js');
//        include.set_path(p);
//        }

    },
    add_kill_event: function(event){ //this should really be in event
    if(!event.kill){
      var killed = false;
      event.kill = function(){
        killed = true;
        if(!event) event = window.event;
          try{
            event.cancelBubble = true;
            if (event.stopPropagation)  event.stopPropagation(); 
            if (event.preventDefault)  event.preventDefault();
          }catch(e){}
      };
      event.is_killed = function(){return killed;};
    }  
  },
    event_closure: function(controller_name, f_name, element){
    return function(event){
      MVC.Controller.add_kill_event(event);
      var params = new MVC.Controller.Params({event: event, element: element, action: f_name, controller: controller_name   });
      return MVC.Controller.dispatch(controller_name, f_name, params);
    };
  },
    dispatch_closure: function(controller_name, f_name){
        return function(params){
      MVC.Controller.add_kill_event(params.event);
            params.action = f_name;
            params.controller = controller_name;
      return MVC.Controller.dispatch(controller_name, f_name, 
                new MVC.Controller.Params(params)
            );
    };
    },
  dispatch: function(controller, action_name, params){
      //debugger; // also check the type as String..

        if (typeof(controller) === "string" && !(MVC.Controller._ControllerStatus[controller])) {
         //debugger;          
          var snapin = $ss.GetSnapin(controller);
          
          if(snapin) {
          var incFiles = snapin.includes || [];
          var controllers = snapin.controllers || [];

        //[MAC] Check Machine OS Windows/MAC
        if(bMac) {
          _count += (incFiles.length + controllers.length);
          var c_name = controller;
          var a_name = action_name;
          var a_params = params;

          _controllers.push(c_name);
          _action_names.push(a_name);
          _params.push(a_params);
          _counts.push(_count);

          _logger.info('Dispatch Controller Namee = '+ controller + ' controller count = ' +_count + 'inc = '+incFiles.length + 'ccr = ' + controllers.length);
        }

        for (var i = 0; i < incFiles.length; i++) {
        //[MAC] Check Machine OS Windows/MAC
        if(!bMac) {
          $.include(incFiles[i]);
        }
        else {
          var message = {'JSClassNameKey':'File','JSOperationNameKey':'GetNativePath', 'JSArgumentsKey':[incFiles[i]], 'JSISSyncMethodKey' : '1'}
          var result = jsBridge.execute(message);
          var parsedJSONObject = JSON.parse(result);
          var res = $.include(parsedJSONObject.Data,function() {
          _index++;
          while (_controllers.length > 0) {
                var aCount = _counts[0];
          if (_index >= aCount) {
                   var aController = _controllers[0];
                   var aActionName = _action_names[0];
                   var aParams = _params[0];
             _controllers.splice(0,1);
                   _action_names.splice(0,1);
                   _params.splice(0,1); 
                   _counts.splice(0,1);

                   MVC.Controller._ControllerStatus[aController] = true;
                     MVC.Controller.dispatch_to(aController, aActionName, aParams); 
               }else {
                 break;
               }
             }
             if (_controllers.length == 0) {
             _logger.info('All controllers inc files included ');
             }
        });
      }
    };
    for (var i = 0; i < controllers.length; i++) {
      //[MAC] Check Machine OS Windows/MAC
      if(!bMac) {
        $.include(controllers[i]);
      }
      else {
        var message = {'JSClassNameKey':'File','JSOperationNameKey':'GetNativePath', 'JSArgumentsKey':[controllers[i]], 'JSISSyncMethodKey' : '1'}
        var result = jsBridge.execute(message);
        var parsedJSONObject = JSON.parse(result);
        var res =$.include(parsedJSONObject.Data,function() {
        _index++;
    
        while (_controllers.length > 0) {
          var aCount = _counts[0];

          if (_index >= aCount) {
            var aController = _controllers[0];
            var aActionName = _action_names[0];
            var aParams = _params[0];
            _controllers.splice(0,1);
            _action_names.splice(0,1);
            _params.splice(0,1); 
            _counts.splice(0,1);
            MVC.Controller._ControllerStatus[aController] = true;

            MVC.Controller.dispatch_to(aController, aActionName, aParams); 
  
          }else {
            break;
          }
        }
        if (_controllers.length == 0) {
          _logger.info('All controllers included ');
        }
        });
      }
    };
    }

    //[MAC] Check Machine OS Windows/MAC
    if(!bMac) {
      MVC.Controller._ControllerStatus[controller] = true;
      MVC.Controller.dispatch_to(controller, action_name, params);
    }
    else {
      if ( !snapin) {
        if (_controllers.length == 0) {
          MVC.Controller._ControllerStatus[controller] = true;                                 
          MVC.Controller.dispatch_to(controller, action_name, params); 
        }else {
          MVC.Controller._ControllerStatus[controller] = true;                
          MVC.Controller.dispatch_to(controller, action_name, params);
          while (_controllers.length > 0) {
            var aCount = _counts[0];
            if (_index >= aCount) {
              var aController = _controllers[0];
              var aActionName = _action_names[0];
              var aParams = _params[0];
              _controllers.splice(0,1);
              _action_names.splice(0,1);
              _params.splice(0,1); 
              _counts.splice(0,1);
              MVC.Controller._ControllerStatus[aController] = true;

              MVC.Controller.dispatch_to(aController, aActionName, aParams); 
            }else {
              break;
            }
          }
          if (_controllers.length == 0) {
            _logger.info('All controllers files included ');
          }        
        }
      }
    }
         //var inclFile = "C:\\Documents and Settings\\nilkamalm\\Local Settings\\Application Data\\SupportSoft\\SPRTI-QA118_Preview\\nilkamalm\\data\\sprt_snapin\\01e286c5-4286-4120-9924-f3815b98cbfc.1\\controller\\snapinexcontentcontroller.js"
         //var data = MVC.request(inclFile);
         //$.ajax({
         //   type: "GET",
         //   url: inclFile,
         //   dataType: "script",
         //   async: false
         //});
         //debugger;
         //$.include(inclFile)

         //setTimeout($.include(inclFile),1);
         //$ss.agentcore.utils.Sleep(20);
         //MVC.Controller._ControllerStatus[controller] = true;
         //eval(data);
         //$.include(inclFile, function(controller, action_name, params) { return MVC.Controller.dispatch_to(controller, action_name, params); } )
         //MVC.Controller.dispatch_to(controller, action_name, params);
       } else {
         MVC.Controller.dispatch_to(controller, action_name, params);
    }
    
  },
  _ControllerStatus: {},
 
  dispatch_to: function (controller, action_name, params) {
    var c_name = controller;
    if (typeof controller == 'string') { controller = window[MVC.String.classize(controller) + 'Controller']; }
    if (!controller) throw 'No controller named ' + c_name + ' was found for MVC.Controller.dispatch.';
    if (!action_name) action_name = 'index';

    if (typeof action_name == 'string') {
      if (!(action_name in controller.prototype)) throw 'No action named ' + action_name + ' was found for ' + c_name + '.';
    } else { //action passed
      action_name = action_name.name;
    }
    //debugger;
    var instance = this.instances[controller.className] || new controller();
    instance.params = params;
    instance.action_name = action_name;
    instance.controller_name = controller.className;
    //this.instances[controller.className] = instance;
    MVC.Controller._dispatch_action(instance, action_name, params);
    delete instance;
    delete params;
  },

  _dispatch_action: function(instance, action_name, params)
  {
    //SPRT added to include reporting
    if(!MVC.Controller.allowReport(action_name))
    {
      $ss.agentcore.reporting.Reports.LogSnapinStart(instance.controller_name, params);    
    }
    
    return instance[action_name](params);
  },
  //SPRT added to include reporting
  allowReport:function(action_name)
  {
    return this.ReportMatch.test(action_name);
  },
  //SPRT added to include reporting
    ReportMatch:new RegExp("(.*?)\\s?(change|contextmenu|dblclick|keydown|keyup|keypress|mousedown|mousemove|mouseout|mouseover|mouseup|reset|resize|scroll|select|submit|dblclick|focus|blur|load|unload|focusout)$"),
    controllers : [],
    actions: [],
    instances: {}
},{
    continue_to :function(action){
    if(!action) action = this.action.name+'ing';
    if(typeof this[action] != 'function'){ throw 'There is no action named '+action+'. ';}
    return MVC.Function.bind(function(){
      this.action_name = action;
      this[action].apply(this, arguments);
    }, this);
  },
    delay: function(delay, action_name){
    if(typeof this[action_name] != 'function'){ throw 'There is no action named '+actaction_nameion+'. ';}
    
        return setTimeout(MVC.Function.bind(function(){
      this.action_name = action_name;
      this[action_name].apply(this, arguments);
    }, this), delay );
    },
    dispatch_delay: function(delay, action_name, params){
        var controller_name = action_name.controller ? action_name.controller : this.Class.className;
        action_name = typeof action_name == 'string' ? action_name : action_name.action;
        return setTimeout(function(){
            MVC.Controller.dispatch(controller_name,action_name, params );
        }, delay );
    }
});

MVC.Controller.Action = MVC.Class.extend(
{
    init: function(){
        if(this.matches) MVC.Controller.actions.push(this);
    }
},{
    init: function(action, f, controller){
        this.action = action;
        this.func = f;
        this.controller = controller;
    }
});

MVC.Controller.DelegateAction = MVC.Controller.Action.extend({
    match: new RegExp("(.*?)\\s?(change|click|contextmenu|dblclick|keydown|keyup|keypress|mousedown|mousemove|mouseout|mouseover|mouseup|reset|resize|scroll|select|submit|dblclick|focus|blur|load|unload|focusout)$"),
    matches: function(action_name){
        return this.match.exec(action_name);
    }
},
//Prototype functions
{    
    init: function(action, f, controller){
        this._super(action, f, controller);
        this.css_and_event();
        
        var selector = this.selector();
        if(selector != null){
            new MVC.DelegationEvent(selector, this.event_type, 
                this.controller.dispatch_closure(controller.className, action ) );
        }
    },
    css_and_event: function(){
        this.parts = this.action.match(this.Class.match);
        this.css = this.parts[1];
        this.event_type = this.parts[2];
    },
    main_controller: function(){
      if(!this.css && MVC.Array.include(['blur','focus'],this.event_type)){
            MVC.Event.observe(window, this.event_type, MVC.Controller.event_closure(this.controller, this.event_type, window) );
            return;
        }
        return this.css;
    },
    plural_selector : function(){
    if(this.css == "#" || this.css.substring(0,2) == "# "){
      var newer_action_name = this.css.substring(2,this.css.length);
            return '#'+this.controller.className + (newer_action_name ?  ' '+newer_action_name : '') ;
    }else{
      return '.'+MVC.String.singularize(this.controller.className)+(this.css? ' '+this.css : '' );
    }
  },
    singular_selector : function(){
        return '#'+this.controller.className+(this.css? ' '+this.css : '' );
    },
    selector : function(){
        if(MVC.Array.include(['load','unload','resize','scroll'],this.event_type)){
            MVC.Event.observe(window, this.event_type, MVC.Controller.event_closure(this.controller, this.event_type, window) );
            return;
        }
        
        
        if(this.controller.className == 'main') 
            this.css_selector = this.main_controller();
        else
            this.css_selector = this.singular_selector();
      //SPRT removed to remove the support for pluralize
      //this.css_selector = MVC.String.is_singular(this.controller.className) ? 
            //    this.singular_selector() : this.plural_selector();
        return this.css_selector;
    }
});

MVC.Controller.Params = function(params){
  for(var thing in params){
    if( params.hasOwnProperty(thing) ) this[thing] = params[thing];
  }
};

MVC.Controller.Params.prototype = {
  form_params : function(){
    var data = {};
    if(this.element.nodeName.toLowerCase() != 'form') return data;
    var els = this.element.elements, uri_params = [];
    for(var i=0; i < els.length; i++){
      var el = els[i];
      if(el.type.toLowerCase()=='submit') continue;
      var key = el.name || el.id, key_components = key.match(/(\w+)/g), value;
            if(!key) continue;     
      /* Check for checkbox and radio buttons */
      switch(el.type.toLowerCase()) {
        case 'checkbox':
        case 'radio':
          value = !!el.checked;
          break;
        default:
          value = el.value;
          break;
      }
      if( MVC.Object.is_number(value) ) value = parseFloat(value);
      if( key_components.length > 1 ) {
        var last = key_components.length - 1;
        var nested_key = key_components[0].toString();
        if(! data[nested_key] ) data[nested_key] = {};
        var nested_hash = data[nested_key];
        for(var k = 1; k < last; k++){
          nested_key = key_components[k];
          if( ! nested_hash[nested_key] ) nested_hash[nested_key] ={};
          nested_hash = nested_hash[nested_key];
        }
        nested_hash[ key_components[last] ] = value;
      } else {
            if (key in data) {
              if (typeof data[key] == 'string' ) data[key] = [data[key]];
               data[key].push(value);
            }
            else data[key] = value;
      }
    }
    return data;
  },
  class_element : function(){
    var start = this.element, controller = this.controller;
    var className = controller;
//    SPRT  Modified to remove plurazie function
//    var className = MVC.String.is_singular(controller) ? controller : MVC.String.singularize(controller);
    while(start && start.className.indexOf(className) == -1 ){
      start = start.parentNode;
      if(start == document) return null;
    }
    return start;
  },
  is_event_on_element : function(){ return this.event.target == this.element; },
  object_data : function(){ return MVC.View.Helpers.get_data(this.class_element()); }
};

/* End of  controller/controller.js */

MVC.Included.plugins.push('controller');


/**
 * Inclusion of the controller_view /controller_view.js file from the JavascriptMVC Framework
 * 
 */

/* Start of controller_view/controller_view.js */


MVC.Controller.prototype.render = function(options) {
    var result, render_to_id = MVC.RENDER_TO, plugin_url;
    var controller_name = this.Class.className;
    var action_name = this.action_name;
        if(!options) options = {};

        //added by SPRT
        options.extra = options.extra || null;
        options.controller_name = controller_name;
        
        var helpers = {};
        if(options.helpers){
            for(var h =0; h < options.helpers.length; h++){
                var n = MVC.String.classize( options.helpers[h] );
                MVC.Object.extend(helpers, window[n] ? window[n].View().helpers : {} );
            }
        }
        
    if(typeof options == 'string'){
      result = new MVC.View({url:  options  }).render(this, helpers);
    }
    else if(options.text) {
            result = options.text;
        }
        else {
            var convert = function(url){
        var url =  MVC.String.include(url,'/') ? url.split('/').join('/') : controller_name+'/'+url;
        var url = url + '.ejs';
        return url;
      };
      if(options.plugin){
                plugin_url = options.plugin;
            }
            
      if(options.action) {
        var url = convert(options.action);
            }
      else if(options.partial) {
                var url = convert(options.partial);
      }else
            {
                var url = controller_name+'/'+action_name.replace(/\.|#/g, '').replace(/ /g,'_')+'.ejs';
            }
      var data_to_render = this;
      if(options.locals) {
        for(var local_var in options.locals) {
          data_to_render[local_var] = options.locals[local_var];
        }
      }
            var view;
            //modified by supportsoft
            if(!plugin_url){
                options.absolute_url = options.absolute_url || url;
                view = new MVC.View({absolute_url:options.absolute_url,cache:options.cache,controller_name:options.controller_name,extra:options.extra});
            }else{
                //load plugin if it has been included
                try{
                    var view = new MVC.View({url:  MVC.View.get(plugin_url) ? plugin_url :  url  });
                }catch(e){
                    if(e.type !='JMVC') throw e;
                    var view = new MVC.View({url:  plugin_url  });
                }
            }
            result = view.render(data_to_render, helpers);
    }
    //return result;
    var locations = ['to', 'before', 'after', 'append', 'prepend'];
    var element = null;
           
        for(var l =0; l < locations.length; l++){
        var cont = true;
      switch (locations[l]) {
        case "to" : 
            if(options[locations[l]]) {
            var id = options[locations[l]];
            if(! ($(id)[0])) { id = "#"+id;}; 
            $(id).html(result);
            result = "";
            cont = false;
             break;
           }
        case "before":
            if(options[locations[l]]) {
            var id = options[locations[l]];
            if(! ($(id)[0])) { id = "#"+id;}; 
            $(id).before(result);
            result = "";        
            cont = false;
            break;
            }
        case "after":
            if(options[locations[l]]) {
            var id = options[locations[l]];
            if(! ($(id)[0])) { id = "#"+id;}; 
            $(id).after(result);
            result = ""; 
            cont = false;
            break;
            }
        case "append":
            if(options[locations[l]]) {
            var id = options[locations[l]];
            if(! ($(id)[0])) { id = "#"+id;}; 
            $(id).append(result);
            result = ""; 
            cont = false;
            break;
            }
        case "prepend":
            if(options[locations[l]]) {
            var id = options[locations[l]];
            if(! ($(id)[0])) { id = "#"+id;}; 
            $(id).prepend(result);
            result = ""; 
            cont = false;
            break;
            }
       }
       if(!cont) break;
    //  if(typeof  options[locations[l]] == 'string'){
    //    var id = options[locations[l]];
    //    if(! ($(id)[0])) { id = "#"+id;}; 
    //    $(id).html(result);
        //SPRT modified to support css selector for to location
//        if ($(id)[0]) {
//          options[locations[l]] = $(id)[0];
//        } else {
//          options[locations[l]] = MVC.$E(id);
//        }
//        if(!options[locations[l]]) 
//          throw {message: "Can't find element with id: "+id, name: 'ControllerView: Missing Element'};
      //}
      
//      if(options[locations[l]]){
//        element = options[locations[l]];
//        if(locations[l] == 'to'){
//          options.to.innerHTML = result;
//        }else{
//          if(!MVC.$E.insert ) throw {message: "Include can't insert "+locations[l]+" without the element plugin.", name: 'ControllerView: Missing Plugin'};
//          var opt = {};
//          opt[locations[l]] = result;
//          MVC.$E.insert(element, opt );
//        }
//      } 
    
    
    }
    
        delete view;
    return result;

};

/* End of  controller_view/controller_view.js */

MVC.Included.plugins.push('controller_view');


/**
 * Inclusion of the element /jQuery_element.js file from the JavascriptMVC Framework
 * 
 */

/* Start of controller_view/jQuery_element.js */


MVC.$E = function(element){
  if(typeof element == 'string')
    element = document.getElementById(element);
  //added by SPRT
  if(element == null)  return null;
  return element._mvcextend ? element : MVC.$E.extend(element);
};



MVC.Object.extend(MVC.$E, {
  insert: function(element, insertions) {
    element = typeof element == 'string'? jQuery('#'+element) : jQuery(element) ;
    
    if(typeof insertions == 'string'){insertions = {bottom: insertions};};
    
    if(insertions.before) element.before(insertions.before)
    if(insertions.after) element.after(insertions.after)
    if(insertions.bottom) element.append(insertions.bottom)
    if(insertions.top) element.prepend(insertions.top)
    return element;
  }
});




MVC.$E.extend = function(el){
  for(var f in MVC.$E){
    if(!MVC.$E.hasOwnProperty(f)) continue;
    var func = MVC.$E[f];
    if(typeof func == 'function'){
      var names = MVC.Function.params(func);
      if( names.length == 0) continue;
      var first_arg = names[0];
      if( first_arg.match('element') ) MVC.$E._extend(func, f, el);
    }
  }
  el._mvcextend = true;
  return el;
}
MVC.$E._extend = function(f,name,el){
  el[name] = function(){
    var arg = MVC.Array.from(arguments);
    arg.unshift(el);
    return f.apply(el, arg); 
  }
}

if(!MVC._no_conflict){
  $E = MVC.$E;
}

/* End of  element/jQuery_element.js */

MVC.Included.plugins.push('element');


/**
 * Inclusion of the model /model.js file from the JavascriptMVC Framework
 * 
 */

MVC.Model = MVC.Class.extend(
{
    //determines which find to pick, calls find_all or find_one which should be overwritten
    
    find : function(id, params, callbacks){
        if(!params)  params = {};
        if(typeof params == 'function') {
            callback = params;
            params = {};
        }
        if(id == 'all'){
            return this.create_many_as_existing( this.find_all(params, callbacks)  );
        }else{
            if(!params[this.id] && id != 'first')
                params[this.id] = id;
            return this.create_as_existing( this.find_one(id == 'first'? null : params, callbacks) );
        }
    },
    // Called after creating something
    create_as_existing : function(attributes){
        if(!attributes) return null;
        if(attributes.attributes) attributes = attributes.attributes;
        var inst = new this(attributes);
        inst.is_new_record = this.new_record_func;
        return inst;
    },
    // Called after creating many
    create_many_as_existing : function(instances){
        if(!instances) return null;
        var res = [];
        for(var i =0 ; i < instances.length; i++)
            res.push( this.create_as_existing(instances[i]) );  
        return res;
    },
    id : 'id', //if null, maybe treat as an array?
    new_record_func : function(){return false;},
    validations: [],
    has_many: function(){
        for(var i=0; i< arguments.length; i++){
            this._associations.push(arguments[i]);
        }
    },
    belong_to: function(){
        for(var i=0; i< arguments.length; i++){
            this._associations.push(arguments[i]);
        }
    },
    _associations: [],
    from_html: function(element_or_id){
        var el =MVC.$E(element_or_id);
        var el_class = window[ MVC.String.classize(el.getAttribute('type')) ];
        
        if(! el_class) return null;
        //get data here
        var attributes = {};
        attributes[el_class.id] = this.element_id_to_id(el.id);
        return el_class.create_as_existing(attributes);
    },
    element_id_to_id: function(element_id){
        var re = new RegExp(this.className+'_', "");
        return element_id.replace(re, '');
    },
    add_attribute : function(property, type){
        if(! this.attributes[property])
            this.attributes[property] = type;
    },
    attributes: {},
    /**
     * Used for converting callbacks to to seperate error and succcess
     * @param {Object} callbacks
     */
    _clean_callbacks : function(callbacks){
        if(!callbacks) throw "You must supply a callback!";
        if(typeof callbacks == 'function')
            return {onSuccess: callbacks, onError: callbacks};
        if(!callbacks.onSuccess && !callbacks.onComplete) throw "You must supply a positive callback!";
        if(!callbacks.onSuccess) callbacks.onSuccess = callbacks.onComplete;
        if(!callbacks.onError && callbacks.onComplete) callbacks.onError = callbacks.onComplete;
    }
},
{   //Prototype functions
    init : function(attributes){
        //this._properties = [];
        this.errors = [];
        
        this.set_attributes(this.Class._attributes || {});
        this.set_attributes(attributes);
    },
    setup : function(){
        
    },
    set_attributes : function(attributes)
    {
        for(key in attributes){ if(attributes.hasOwnProperty(key)) this._setAttribute(key, attributes[key]);}
        return attributes;
    }, 
    update_attributes : function(attributes, callback)
    {
        this.set_attributes(attributes);
        return this.save(callback);
    },
    valid : function() {
        return  this.errors.length == 0;
    },
    validate : function(){
        //run validate function and any error functions  
        
    },
    _setAttribute : function(attribute, value) {
        if (value && typeof(value) == "object" && value.constructor != Date)
          this._setAssociation(attribute, value);
        else
          this._setProperty(attribute, value);
    },
    _setProperty : function(property, value) {  
        this[property] = MVC.Array.include(['created_at','updated_at'], property) ? MVC.Date.parse(value) :  value;

        //if (!(MVC.Array.include(this._properties,property))) this._properties.push(property);  
        
        this.Class.add_attribute(property, MVC.Object.guess_type(value)  );
    },
    _setAssociation : function(association, values) {
        this[association] = function(){
            
      // SPRT removed to remove Pluralize function
      //if(! MVC.String.is_singular(association ) ) association = MVC.String.singularize(association);
            
            var associated_class = window[MVC.String.capitalize(association)];
            if(!associated_class) return values;
            //alert(values.length)
            return associated_class.create_many_as_existing(values);
        }
        
    },
    attributes : function() {
        var attributes = {};
        var cas = this.Class.attributes;
        for(var attr in cas){
            if(cas.hasOwnProperty(attr) ) attributes[attr] = this[attr];
        }
        //for (var i=0; i<this.attributes.length; i++) attributes[this._properties[i]] = this[this._properties[i]];
        return attributes;
    },
    is_new_record : function(){ return true;},
    save: function(callback){
        var result;
        this.errors = [];
        this.validate();
        if(!this.valid()) return false;
        
        if(this.is_new_record())
            result = this.Class.create(this.attributes(), callback);
        else
            result = this.Class.update(this[this.Class.id], this.attributes(), callback);
        this.is_new_record = this.Class.new_record_func;
        return true;
    },
    destroy : function(callback){
        this.Class.destroy(this[this.Class.id], callback);
    },
    add_errors : function(errors){
        if(errors) this.errors = this.errors.concat(errors);
    },
    _resetAttributes : function(attributes) {
        this._clear();
        /*for (var attr in attributes){
        if(attributes.hasOwnProperty(attr)){
          this._setAttribute(attr, attributes[attr]);
        }
      }*/
    },
    _clear : function() {
        var cas = this.Class.attributes;
        for(var attr in cas){
            if(cas.hasOwnProperty(attr) ) this[attr] = null;
        }
    }
});


MVC.Object.guess_type = function(object){
    if(typeof object != 'string'){
        if(object == null) return typeof object;
        if( object.constructor == Date ) return 'date';
        if(object.constructor == Array) return 'array';
        return typeof object;
    }
    //check if true or false
    if(object == 'true' || object == 'false') return 'boolean';
    if(!isNaN(object)) return 'number'
    return typeof object;
}


/* End of  model/model.js */

MVC.Included.plugins.push('model');


/**
 * Inclusion of the model_view_helper /model_view_helper.js file from the JavascriptMVC Framework
 * 
 */

/* Start of model_view/model_view_helper.js */



MVC.ModelViewHelper = MVC.Class.extend(
{
    init: function(){
        if(!this.className) return;
        //add yourself to your model
        var modelClass;
        if(!this.className) return;
        
        if(!(modelClass = this.modelClass = window[MVC.String.classize(this.className)]) ) 
            throw "ModelViewHelpers can't find class "+this.className;
        var viewClass = this;
        this.modelClass.View = function(){ 
            return viewClass;
        };
        
        
        this.modelClass.prototype.View = function(){
            return new viewClass(this);
        };
        if(this.modelClass.attributes){
            this._view = new MVC.View.Helpers({});
            var type;
            for(var attr in this.modelClass.attributes){
                if(! this.modelClass.attributes.hasOwnProperty(attr) || typeof this.modelClass.attributes[attr] != 'string') continue;
                this.add_helper(attr);
            }
        }
    },
    form_helper: function(attr){
        if(! this.helpers[attr]+"_field" ){
            this.add_helper(attr);
        }
        var f = this.helpers[attr+"_field"];
        var args = MVC.Array.from(arguments);
        args.shift();
        return f.apply(this._view, args);
    },
    add_helper : function(attr){
        var h = this._helper(attr);
        this.helpers[attr+"_field"] = h;
    },
    helpers : {},
    _helper: function(attr){
        var helper = this._view_helper(attr);
        var modelh = this;
        var name = this.modelClass.className+'['+attr+']';
        var id = this.modelClass.className+'_'+attr;
        return function(){
            var args = MVC.Array.from(arguments);
            args.unshift(name);
            args[2] = args[2] || {};
            args[2].id = id;
            return helper.apply(modelh._view, args);
        }
    },
    _view_helper: function(attr){
         switch(this.modelClass.attributes[attr].toLowerCase()) {
        case 'boolean': 
                    return this._view.check_box_tag;
                case 'text':
                    return this._view.text_area_tag;
        default:
          return this._view.text_field_tag;
      }
    },
    clear: function(){
        var mname = this.modelClass.className, el;
        for(var attr in this.modelClass.attributes){
            if( (el = MVC.$E(mname+"_"+attr)) ){
                el.value = '';
            }
        }
    },
    from_html: function(element_or_id){
        var el =MVC.$E(element_or_id);
        
        var el_class = this.modelClass ? this.modelClass : window[ MVC.String.classize(el.getAttribute('type')) ];
        
        if(! el_class) return null;
        //get data here
        var attributes = {};
        attributes[el_class.id] = this.element_id_to_id(el.id);
        //for(var attr in modelClass.attributes){
        //    if(MVC.$E(  ) )
        //}
        
        return el_class.create_as_existing(attributes);
    },
    element_id_to_id: function(element_id){
        var re = new RegExp(this.className+'_', "");
        return element_id.replace(re, '');
    }
},
{
    init: function(model_instance){
        this._inst = model_instance;
        this._className = this._inst.Class.className;
    },
    element : function(){
        if(this._element) return this._element;
        this._element = MVC.$E(this.element_id());
        if(this._element) return this._element;
        this._element = document.createElement('div');
        this._element.id = this.element_id();
        this._element.className = this._className;
        this._element.setAttribute('type', this._className);
        return this._element;
    },
    element_id : function(){
        return this._className+'_'+this._inst[this._inst.Class.id];
    },
    show_errors : function(){
        var err = MVC.$E(this._className+"_error");
        var err = err || MVC.$E(this._className+"_error");
        var errs = [];
        for(var i=0; i< this._inst.errors.length; i++){
      var error = this._inst.errors[i];
      var el = MVC.$E(this._className+"_"+error[0]);
      if(el){
        el.className="error";
                var er_el = MVC.$E(this._className+"_"+error[0]+"_error" );
        if(er_el) er_el.innerHTML = error[1];
      }
      else
                errs.push(error[0]+' is '+error[1]);
            
    }
        if(errs.length > 0){
             if(err) err.innerHTML = errs.join(", "); 
             else alert(errs.join(", "));
        }
    },
    clear_errors: function(){
        var p;
        var cn = this._className;
        for(var i =0; i < this._inst._properties.length; i++){
            p = this._inst._properties[i];
            var el = MVC.$E(cn+"_"+p);
            if(el) el.className = el.className.replace(/(^|\\s+)error(\\s+|$)/, ' '); //from prototype
            var er_el = MVC.$E(cn+"_"+p+"_error" );
        if(er_el) er_el.innerHTML = '&nbsp;';
        }
        var bigel = MVC.$E(cn+"_error");
        if(bigel) bigel.innerHTML = '';
    },
    edit: function(attr){
        //get the helper function, add args, return
         var args = MVC.Array.from(arguments);
         var name = this._className+'['+attr+']'
         args.shift();
         args.unshift( {id: this.edit_id(attr)} ); //change to ID
         args.unshift(this._inst[attr]); //value
         args.unshift(name); //name
         var helper =this.Class._view_helper(attr)
         return helper.apply(this.Class._view, args);
    },
    edit_values: function(){
        var values = {};
        var cn = this._className, p, el;
        for(var i =0; i < this._inst._properties.length; i++){
            p = this._inst._properties[i];
            el = MVC.$E(this.edit_id(p));
            if(el) values[p] = el.value;
            
        }
        return values;
    },
    edit_id: function(attr){
        return this._className+'_'+this._inst.id+'_'+attr+'_edit';
    },
    destroy: function(){
        var el = this.element();
        el.parentNode.removeChild(el);
    }
}
);


/* End of  model_view_helper/model_view_helper.js */

MVC.Included.plugins.push('model_view_helper');


/**
 * Inclusion of the view_helper /view_helper.js file from the JavascriptMVC Framework
 * 
 */

/* Start of view_helper/view_helper.js */


// JavaScriptMVC framework and server, 1.1.22
//  - built on 2008/05/07 19:44

MVC.Object.extend(MVC.View.Helpers.prototype, {
  check_box_tag: function(name, value, options, checked){
        options = options || {};
        if(checked) options.checked = "checked";
        return this.input_field_tag(name, value, 'checkbox', options);
    },
    date_tag: function(name, value , html_options) {
      if(! (value instanceof Date)) value = new Date();
    
    var years = [], months = [], days =[];
    var year = value.getFullYear(), month = value.getMonth(), day = value.getDate();
    for(var y = year - 15; y < year+15 ; y++) years.push({value: y, text: y});
    for(var m = 0; m < 12; m++) months.push({value: (m), text: MVC.Date.month_names[m]});
    for(var d = 0; d < 31; d++) days.push({value: (d+1), text: (d+1)});
    
    var year_select = this.select_tag(name+'[year]', year, years, {id: name+'[year]'} );
    var month_select = this.select_tag(name+'[month]', month, months, {id: name+'[month]'});
    var day_select = this.select_tag(name+'[day]', day, days, {id: name+'[day]'});
    
      return year_select+month_select+day_select;
  },
  file_tag: function(name, value, html_options) {
      return this.input_field_tag(name+'[file]', value , 'file', html_options);
  },
  form_tag: function(url_for_options, html_options) {
      html_options = html_options  || {};
    if(html_options.multipart == true) {
          html_options.method = 'post';
          html_options.enctype = 'multipart/form-data';
      }
    html_options.action = url_for_options;
      return this.start_tag_for('form', html_options);
  },
  form_tag_end: function() { return this.tag_end('form'); },
  hidden_field_tag: function(name, value, html_options) { 
      return this.input_field_tag(name, value, 'hidden', html_options); 
  },
  input_field_tag: function(name, value , inputType, html_options) {
      html_options = html_options || {};
      html_options.id  = html_options.id  || name;
      html_options.value = value || '';
      html_options.type = inputType || 'text';
      html_options.name = name;
      return this.single_tag_for('input', html_options);
  },
  link_to: function(name, url, html_options) {
      if(!name) var name = 'null';
      if(!html_options) var html_options = {};
    this.set_confirm(html_options);
    html_options.href=url;
    return this.start_tag_for('a', html_options)+name+ this.tag_end('a');
  },
    link_to_if: function(condition, name, url, html_options) {
    return this.link_to_unless((!condition), name, url, html_options);
  },
    link_to_unless: function(condition, name, url, html_options){
        if(condition) return name;
        return this.link_to(name, url, html_options);
    },
  set_confirm: function(html_options){
    if(html_options.confirm){
      html_options.onclick = html_options.onclick || '';
      html_options.onclick = html_options.onclick+
      "; var ret_confirm = confirm(\""+html_options.confirm+"\"); if(!ret_confirm){ return false;} ";
      html_options.confirm = null;
    }
  },
  submit_link_to: function(name, options, html_options, post){
    if(!name) var name = 'null';
      if(!html_options) html_options = {};
    html_options.type = 'submit';
      html_options.value = name;
    this.set_confirm(html_options);
    html_options.onclick=html_options.onclick+';window.location="'+options+'"; return false;';
    return this.single_tag_for('input', html_options);
  },
  password_field_tag: function(name, value, html_options) { return this.input_field_tag(name, value, 'password', html_options); },
  select_tag: function(name, value, choices, html_options) {     
      html_options = html_options || {};
      html_options.id  = html_options.id  || name;
      //html_options.value = value;
    html_options.name = name;
      var txt = '';
      txt += this.start_tag_for('select', html_options);
      for(var i = 0; i < choices.length; i++)
      {
          var choice = choices[i];
          if(typeof choice == 'string') choice = {value: choice};
      if(!choice.text) choice.text = choice.value;
      if(!choice.value) choice.text = choice.text;
      
      var optionOptions = {value: choice.value};
          if(choice.value == value)
              optionOptions.selected ='selected';
          txt += this.start_tag_for('option', optionOptions )+choice.text+this.tag_end('option');
      }
      txt += this.tag_end('select');
      return txt;
  },
  single_tag_for: function(tag, html_options) { return this.tag(tag, html_options, '/>');},
  start_tag_for: function(tag, html_options)  { return this.tag(tag, html_options); },
  submit_tag: function(name, html_options) {  
      html_options = html_options || {};
      html_options.type = html_options.type  || 'submit';
      html_options.value = name || 'Submit';
      return this.single_tag_for('input', html_options);
  },
  tag: function(tag, html_options, end) {
      end = end || '>';
      var txt = ' ';
      for(var attr in html_options) { 
         if(html_options.hasOwnProperty(attr)){
         value = html_options[attr] != null ? html_options[attr].toString() : '';

           if(attr == "Class" || attr == "klass") attr = "class";
           if( value.indexOf("'") != -1 )
                txt += attr+'=\"'+value+'\" ' ;
           else
                txt += attr+"='"+value+"' " ;
       }
      }
      return '<'+tag+txt+end;
  },
  tag_end: function(tag)             { return '</'+tag+'>'; },
  text_area_tag: function(name, value, html_options) { 
      html_options = html_options || {};
      html_options.id  = html_options.id  || name;
      html_options.name  = html_options.name  || name;
    value = value || '';
      if(html_options.size) {
          html_options.cols = html_options.size.split('x')[0];
          html_options.rows = html_options.size.split('x')[1];
          delete html_options.size;
      }
      html_options.cols = html_options.cols  || 50;
      html_options.rows = html_options.rows  || 4;
      return  this.start_tag_for('textarea', html_options)+value+this.tag_end('textarea');
  },
  text_field_tag: function(name, value, html_options) { return this.input_field_tag(name, value, 'text', html_options); },
  img_tag: function(image_location, options){
    options = options || {};
    options.src = "resources/images/"+image_location;
    return this.single_tag_for('img', options);
  },
  to_text: function(input, null_text) {
      if(input == null || input === undefined) return null_text || '';
      if(input instanceof Date) return input.toDateString();
    if(input.toString) return input.toString().replace(/\n/g, '<br />').replace(/''/g, "'");
    return '';
  }
  
});

MVC.View.Helpers.prototype.text_tag = MVC.View.Helpers.prototype.text_area_tag;
//[MAC] Check Machine OS Windows/MAC
if(bMac) {
  var _logger = $ss.agentcore.log.GetDefaultLogger("$ss.agentcore.MVC");  
  var jsBridge = window.JSBridge;
  var _index = 0;
  var _count = 0;
  var  _controllers =[];
  var _action_names =[];
  var _params =[];
  var _counts = [];
  var _utils = $ss.agentcore.utils.ui;
}
(function(){
  var data = {};
  var name = 0;
  MVC.View.Helpers.link_data = function(store){
    var functionName = name++;
    data[functionName] = store;  
    return "_data='"+functionName+"'";
  };
  MVC.View.Helpers.get_data = function(el){
    if(!el) return null;
    var dataAt = el.getAttribute('_data');
    if(!dataAt) return null;
    return data[parseInt(dataAt)];
  };
  MVC.View.Helpers.prototype.link_data = function(store){
    return MVC.View.Helpers.link_data(store)
  };
  MVC.View.Helpers.prototype.get_data = function(el){
    return MVC.View.Helpers.get_data(el)
  };

  //[MAC] Get Machine OS type
  var bMac = $.browser.safari;

  //[MAC] JSBridge object used only for MAC
  try {
    var jsBridge = window.JSBridge;
  }
  catch(e) {
  }

})();


/* End of  view_helper/view_helper.js */

MVC.Included.plugins.push('view_helper');



