﻿/** @namespace Holds all navigation related information of snapins*/
$ss.agentcore.dal.history = $ss.agentcore.dal.history || {};
(function()
{
  $.extend($ss.agentcore.dal.history,
  {
    /**
    *  @name InitializeHistory
    *  @memberOf $ss.agentcore.dal.history
    *  @function
    *  @description  Initializes the History XML
    *  @returns true on success,false on failure
    *  @example
    *         var objHistory = $ss.agentcore.dal.history;
    *         var ret = objHistory.InitializeHistory(); 
    */
    InitializeHistory:function ( )
    {
			_logger.info('Entered function: $ss.agentcore.dal.history.InitializeHistory');
      var bRet = false;
      try
      {
        if (_bInitialized == false)
        {
          _xmlDI = _xml.LoadXMLFromString(_xmlDataIslandRootXml);
          bRet = true;
          _bInitialized = true;
        }
      } 
      catch (ex)
      {
				_exception.HandleException(ex,'$ss.agentcore.dal.history','InitializeHistory',arguments);
      }
      
      return bRet;
    },


    /**
    *  @name GetHistory
    *  @memberOf $ss.agentcore.dal.history
    *  @function
    *  @description  Returns the current History Stack.
    *  @param sScope XPath Argument for the @scope attribute.
    *  @param sType XPath Argument for the @type  attribute.
    *  @param bDeleted If true only deleted entries are considered
    *  @param sXpathQ Additional xpath information to add to query EG /tagName
    *  @param sSortBy specicies the sort order
    *  @returns Collection of XML DOM Nodes
    *  @example
    *           var objHistory = $ss.agentcore.dal.history;
    *           var ret = objHistory.GetHistory("dummy", $ss.agentcore.constants.HST_TYPE_NAVIGATION, false );  
    */
    GetHistory:function ( sScope, sType, bDeleted, sXpathQ, sSortBy )
    {
			_logger.info('Entered function: $ss.agentcore.dal.history.GetHistory');
      
      try
      {
        var xPath      = _dataIslandEntriesXPath;
        var aAttrs     = []; 
        var oNodes     = null;
        
        sSortBy = sSortBy || "";

        if (sScope != null && sScope != "" && sScope != "*")
          aAttrs.push(_xml.BuildAttributeForXPath(_constants.HISTORY.SCOPE,sScope));

        if (sType)
          aAttrs.push(_xml.BuildAttributeForXPath(_constants.HISTORY.TYPE,sType));

        if (bDeleted == false)
          aAttrs.push(_xml.BuildAttributeForXPath(_constants.HISTORY.DELETED,'0'));
        else if (bDeleted == true)
          aAttrs.push(_xml.BuildAttributeForXPath(_constants.HISTORY.DELETED,'1'));

        if (aAttrs.length)
          xPath += "[" + aAttrs.join(" and ") + "]";
        
        if (sXpathQ)
          xPath += sXpathQ;

        var sKey = xPath+sSortBy;

        if ( ! _resultsCache[sKey] ) 
        {
          _resultsCache[sKey] = _GetByXPath( xPath , _GetRoot(), sSortBy);
        }
        oNodes = _resultsCache[sKey];

        return oNodes;
      }
      catch (ex)
      {
				_exception.HandleException(ex,'$ss.agentcore.dal.history','GetHistory',arguments);
      }
      
      return null;
    },
    
    
    /**
    *  @name GetCount
    *  @memberOf $ss.agentcore.dal.history
    *  @function
    *  @description  Returns the current History Stack length.
    *  @param sScope XPath Argument for the @scope attribute.
    *  @param sType XPath Argument for the @type  attribute.
    *  @param bDeleted If true  only deleted entries are considered.
    *  @returns The count of the history entries
    *  @example
    *           var objHistory = $ss.agentcore.dal.history;
    *           var ret = objHistory.GetCount("dummy", $ss.agentcore.constants.HST_TYPE_NAVIGATION, false );
    */
    GetCount:function ( sScope, sType, bDeleted )
    {
			_logger.info('Entered function: $ss.agentcore.dal.history.GetCount');
      try
      {
        var oNodes = this.GetHistory( sScope, sType, bDeleted );
        if (oNodes != null)
        {
          return oNodes.length;
        }
        return 0;
      }
      catch (ex)
      {
				_exception.HandleException(ex,'$ss.agentcore.dal.history','GetCount',arguments);
      }
      return null;
    },

    /**
    *  @name GetLastEntry
    *  @memberOf $ss.agentcore.dal.history
    *  @function
    *  @description  Returns the last element in the History Stack.
    *  @param sScope XPath Argument for the @scope attribute.
    *  @param sType XPath Argument for the @type  attribute.
    *  @param bDeleted If set, considers the deleted entries
    *  @returns Last History Entry, or null.
    *  @example
    *         var objHistory = $ss.agentcore.dal.history;  
    *         var ret = objHistory.GetLastEntry("dummy", $ss.agentcore.constants.HST_TYPE_NAVIGATION, false );
    */
    GetLastEntry : function ( sScope, sType, bDeleted )
    {
			_logger.info('Entered function: $ss.agentcore.dal.history.GetLastEntry');
      
      try
      {
        var oNodes = this.GetHistory( sScope, sType, bDeleted );
        if (oNodes != null && oNodes.length)
        {
          return oNodes[oNodes.length-1];
        }
        return null;
      }
      catch (ex)
      {
				_exception.HandleException(ex,'$ss.agentcore.dal.history','GetLastEntry',arguments);
      }
      return null;
    },

    /**
    *  @name GetNodeBeforeOrAfter
    *  @memberOf $ss.agentcore.dal.history
    *  @function
    *  @description  Returns the current History Stack.
    *  @param sScope XPath Argument for the @scope attribute.
    *  @param sType XPath Argument for the @type  attribute.
    *  @param bDeleted If set, considers the deleted entries
    *  @param sSortBy specicies the sort order
    *  @returns Collection of XML DOM Nodes
    *  @example
    *
    *
    */
    GetNodeBeforeOrAfter:function( sKey, sValue, sBeforeAfter, bDeleted, sSortBy )
    {
			_logger.info('Entered function: $ss.agentcore.dal.history.GetNodeBeforeOrAfter');
      try
      {
        var xPath      = _dataIslandEntriesXPath;
        var aAttrs     = []; 
        
        sSortBy = sSortBy || "";
        sBeforeAfter = sBeforeAfter || " > ";
        sBeforeAfter = ( (sBeforeAfter=="<" || sBeforeAfter=="before") ? " < " : " > ");

        aAttrs.push("@" + sKey + sBeforeAfter + "'" + sValue + "'");
        
        if (bDeleted == false)
          aAttrs.push(_xml.BuildAttribute(_constants.HST_DELETED,'0'));
        else if (bDeleted == true)
          aAttrs.push(_xml.BuildAttribute(_constants.HST_DELETED,'1'));

        if (aAttrs.length)
          xPath += "[" + aAttrs.join(" and ") + "]";

        //dont try to cache since its relative and may change.
        //EG: createdraw > 1147777011 will change each new step so cache will be wrong.    
        oNodes = _GetByXPath( xPath , _GetRoot(), sSortBy);

        return oNodes;
      }
      catch (e)
      {
				_exception.HandleException(ex,'$ss.agentcore.dal.history','GetNodeBeforeOrAfter',arguments);
      }
      return null;
    },

    /**
    *  @name AddEntry
    *  @memberOf $ss.agentcore.dal.history
    *  @function
    *  @description  Adds an entry to the current HistoryStack.
    *  @param sScope XPath Argument for the @scope attribute.
    *  @param sType XPath Argument for the @type  attribute.
    *  @param bDeleted If set, considers the deleted entries
    *  @param oEntry The JS object to be pushed onto the stack, as XML.
    *  @param sBeforeRelatedType Supports putting a  <br/>
    *                            history entry in before a node of a certain type.
    *  @returns true on success false on failure
    *  @example
    *           var objHistory = $ss.agentcore.dal.history;
    *           var objTest = GetEntry("test"); // function to get a test object
    *           var ret = objHistory.AddEntry("dummy", $ss.agentcore.constants.HST_TYPE_NAVIGATION, false, objTest);
    */
    AddEntry:function ( sScope, sType, bDeleted, oEntry,sBeforeRelatedType )
    {
			_logger.info('Entered function: $ss.agentcore.dal.history.AddEntry');
      if(!sScope)sScope="dummy";
      
      try
      {
        var oRelatedNodes = this.GetHistory(sScope, sType, bDeleted);
        var nIndex = (oRelatedNodes == null) ? 0 : oRelatedNodes.length;
        var oContainer = _GetContainer();
        var oXmlDoc    = oContainer.ownerDocument;

        var oCreated = new Date();
        var iCreated = oCreated.valueOf();
        var sCreated = oCreated.toUTCString();
        var iDeleted = 0; 

        var sCreatedAsHex = _GetDateHex();

        if (bDeleted == true)
          iDeleted = 1;

        var oNewEntry = oXmlDoc.createElement(_dataIslandEntry);

        var sContext = "";

        oNewEntry.setAttribute("type",         sType);
        oNewEntry.setAttribute("scope",        sScope);
        oNewEntry.setAttribute("deleted",      iDeleted);
        oNewEntry.setAttribute("index",        nIndex);

        oNewEntry.setAttribute("created",      sCreated);
        oNewEntry.setAttribute("createdraw",   iCreated);
        oNewEntry.setAttribute("createdhex",   sCreatedAsHex);

        oNewEntry.setAttribute("context",      sContext);
     
        if ( true == _xml.JsToXml( oNewEntry, oEntry ) )
        {
          oContainer.appendChild(oNewEntry);
          
          //
          // Based on the callers parameters, support putting a 
          // history entry in before a node of a certain type.
          // Generally this will be used to push databag elements in before
          // the current page so they can be retrieved on the page
          // when returning to it through 'back' navigation.
          //
          if (sBeforeRelatedType && sBeforeRelatedType != _constants.HST_TYPE_NONE) 
          {
            var oPrev = oNewEntry.previousSibling;
            var bMoved = false;
            while (oPrev && ! bMoved) 
            {
              if (oPrev) {
                switch (oPrev.getAttribute("type")) 
                {
                  case sBeforeRelatedType : 
                    if (oPrev.getAttribute("deleted") == "0") {
                      oPrev.parentNode.insertBefore(oNewEntry, oPrev);
                      bMoved = true;
                    }
                    break;
                }
              }
              oPrev = oPrev.previousSibling;
            }
          }

          _ClearCachedResults();
          //return ss_hst_Success;
          return true;
        }
      }
      catch (e)
      {
				_exception.HandleException(ex,'$ss.agentcore.dal.history','AddEntry',arguments);
      }
      return false;
    },

    /**
    *  @name PopEntry
    *  @memberOf $ss.agentcore.dal.history
    *  @function
    *  @description  Pops off the last entry and returns it.  (Its state is "deleted")
    *  @param sScope XPath Argument for the @scope attribute.
    *  @param sType XPath Argument for the @type  attribute.
    *  @param bDeleted If set considers deleted entries also
    *  @returns History Element
    *  @example
    *         var objHistory = $ss.agentcore.dal.history;  
    *         var ret = objHistory.PopEntry("dummy", $ss.agentcore.constants.HST_TYPE_NAVIGATION, false );
    */
    PopEntry:function ( sScope, sType, bDeleted )
    {
			_logger.info('Entered function: $ss.agentcore.dal.history.PopEntry');
      try 
      {
        var oLast = this.GetLastEntry( sScope, sType, bDeleted );
        if (oLast) 
        { 
          oLast.setAttribute("deleted", 1);
          _ClearCachedResults();
        }
        return oLast;
      } 
      catch (e) 
      {
				_exception.HandleException(ex,'$ss.agentcore.dal.history','PopEntry',arguments);
        //g_Logger.Error("ss_hst_Pop", "Unexpected error " + e.message);
      }
      return null;
    },

    /**
    *  @name PopAndDeleteEntries
    *  @memberOf $ss.agentcore.dal.history
    *  @function
    *  @description  Pops off the last entry, returns it and trims everything after it. <br/>
                     (Its state is "deleted")
    *  @param sScope XPath Argument for the @scope attribute.
    *  @param sType XPath Argument for the @type  attribute.
    *  @param bDeleted If set considers deleted entries also
    *  @returns History Element
    *  @example
    *           var objHistory = $ss.agentcore.dal.history;    
    *           var ret = objHistory.PopAndDeleteEntries("dummy", $ss.agentcore.constants.HST_TYPE_NAVIGATION, false );
    */
    PopAndDeleteEntries:function( sScope, sType, bDeleted )
    {
			_logger.info('Entered function: $ss.agentcore.dal.history.PopAndDeleteEntries');
      try
      {
        var oLast = this.GetLastEntry( sScope, sType, bDeleted );
        if (oLast)
        { 
          //From current step, set all to delete.
          this.DeleteEntriesAfterNode("*", sType, null, oLast);
          oLast.setAttribute("deleted", 1);

          _ClearCachedResults();
        }
        return oLast;
      }
      catch (e)
      {
				_exception.HandleException(ex,'$ss.agentcore.dal.history','PopAndDeleteEntries',arguments);
        //g_Logger.Error("ss_hst_PopAndTrim", "Unexpected error " + e.message);
      }
      return null;
    },

    /**
    *  @name DeleteAllEntries
    *  @memberOf $ss.agentcore.dal.history
    *  @function
    *  @description  Marks the entries as deleted
    *  @param sScope XPath Argument for the @scope attribute.
    *  @param sType XPath Argument for the @type  attribute.
    *  @param bDeleted If set considers deleted entries also
    *  @returns true/false
    *  @example
    *         var objHistory = $ss.agentcore.dal.history;
    *         var ret = objHistory.DeleteAllEntries("dummy",$ss.agentcore.constants.HST_TYPE_NAVIGATION, false);
    */
    DeleteAllEntries : function( sScope, sType, bDeleted )
    {
			_logger.info('Entered function: $ss.agentcore.dal.history.DeleteAllEntries');
      try
      {
        var oNodes = this.GetHistory( sScope, sType, bDeleted );
        var oNode;
        if (oNodes != null)
        {
          for (var i=oNodes.length-1; i>=0; i--)
          {
            oNode = oNodes[i];
            oNode.setAttribute("deleted", 1);
          }
          _ClearCachedResults();
          return true;
        }
        return false;
      }
      catch (e)
      {
				_exception.HandleException(ex,'$ss.agentcore.dal.history','DeleteAllEntries',arguments);
      }
      return false;
    },


    /**
    *  @name DeleteEntriesAfterNode
    *  @memberOf $ss.agentcore.dal.history
    *  @function
    *  @description  Marks the entries as deleted for all the nodes after the node specified by oRef.  <br/>
    *                If oRef is not specified,then all nodes will be marked as deleted
    *  @param sScope XPath Argument for the @scope attribute.
    *  @param sType XPath Argument for the @type  attribute.
    *  @param bDeleted If set considers deleted entries also
    *  @param oRef reference node
    *  @param arrRemoved contains the removed nodes
    *  @returns true on success,false on failure
    *  @example
    *         var objHistory = $ss.agentcore.dal.history;
    *         var oRef = objHistory.GetLastEntry("dummy",$ss.agentcore.constants.HST_TYPE_NAVIGATION, false);
    *         var arrRem = new Array();
    *         var ret = objHistory.DeleteEntriesAfterNode("dummy",$ss.agentcore.constants.HST_TYPE_NAVIGATION, false,oRef,arrRem);
    */
    DeleteEntriesAfterNode:function ( sScope, sType, bDeleted, oRef ,arrRemoved)
    {
			_logger.info('Entered function: $ss.agentcore.dal.history.DeleteEntriesAfterNode');
      try
      {
        var oNodes = this.GetHistory( sScope, sType, bDeleted );
        if (oNodes != null)
        {
          for (var i=oNodes.length-1; i>=0; i--)
          {
            if (oRef == oNodes[i])
            {
              break;
            }
            oNode = oNodes[i];
            oNode.setAttribute("deleted", 1);

            if (arrRemoved) {
              arrRemoved.push(oNode);
            }
          }
          _ClearCachedResults()
          return true;
        } 
      }
      catch (e)
      {
				_exception.HandleException(ex,'$ss.agentcore.dal.history','DeleteEntriesAfterNode',arguments);
      }
      return false;
    },


    /**
    *  @name GetXML
    *  @memberOf $ss.agentcore.dal.history
    *  @function
    *  @description  Returns the XML Stream for the current History XML DOM.
    *  @param bPreserveWhitespace Indicates if the XML DOM's <br/>
    *                             preserveWhitespace property is set to true.
    *  @returns xml string
    *  @example
    *           var objHistory = $ss.agentcore.dal.history;
    *           objHistory.GetXML();
    */
    GetXML:function ( bPreserveWhitespace )
    {
			_logger.info('Entered function: $ss.agentcore.dal.history.GetXML');
      try
      {
        var oRoot = _GetRoot();
        if (oRoot)
        {
          var sXml = oRoot.xml;
          return sXml;
        }
      } 
      catch (e)
      {
				_exception.HandleException(ex,'$ss.agentcore.dal.history','GetXML',arguments);
      }
      return "";
    },

    /**
    *  @name SetHistoryXML
    *  @memberOf $ss.agentcore.dal.history
    *  @function
    *  @description  Sets the current HistoryStack to a copy of the Incoming array.
    *  @param sXml XML to set the history to
    *  @returns true on success,false on failure
    *  @example
    *           var objHistory = $ss.agentcore.dal.history;
    *           var xml = objHistory.GetHistoryXML();
    *           var ret = objHistory.SetHistoryXML(xml);
    */
    SetHistoryXML:function ( sXml )
    {
			_logger.info('Entered function: $ss.agentcore.dal.history.SetHistoryXML');
      try
      {
        var xmlDI = _xmlDI;
        if ( !xmlDI )
        {
          _Reset();
        }
        xmlDI.async = false;
        xmlDI.loadXML(sXml);

        if (! _GetContainer() )
        {
          _Reset();
          return false;
        }

        _ClearCachedResults();
        return true;
      }
      catch (e)
      {
				_exception.HandleException(ex,'$ss.agentcore.dal.history','SetHistoryXML',arguments);
        //g_Logger.Error("ss_hst_Set", "Unexpected error " + e.message);
      }
      return true;
    },

    /**
    *  @name GetHistoryXML
    *  @memberOf $ss.agentcore.dal.history
    *  @function
    *  @description  Gest the history xml
    *  @returns  xml string
    *  @example
    *           var objHistory = $ss.agentcore.dal.history;
    *           var xml = objHistory.GetHistoryXML();
    */
    GetHistoryXML:function ()
    {
			_logger.info('Entered function: $ss.agentcore.dal.history.GetHistoryXML');
      var xmlHistory = null;
      try
      {
        if ( _xmlDI )
        {
          xmlHistory = _xmlDI.xml;
        }
      }
      catch (e)
      {
				_exception.HandleException(ex,'$ss.agentcore.dal.history','GetHistoryXML',arguments);
      }
      return xmlHistory;
    },
    /******************************************************************************/
    /*                                                                            */
    /*                           SOLUTION PATH                                    */
    /*                                                                            */
    /******************************************************************************/

    /**
    *  @name GetNavigationNodes
    *  @memberOf $ss.agentcore.dal.history
    *  @function
    *  @description  Returns a collection of history nodes for the type 'navigation'.
    *  @param sScope XPath Argument for the @scope attribute.
    *  @param sType XPath Argument for the @type  attribute.
    *  @returns array of nodes
    *  @example
    *           var objHistory = $ss.agentcore.dal.history;  
    *           var ret = objHistory.GetNavigationNodes();
    */
    GetNavigationNodes:function (sScope, sType) 
    {
			_logger.info('Entered function: $ss.agentcore.dal.history.GetNavigationNodes');
      var arrReturn = [];
      try 
      {
        sScope = sScope || "*";
        sType  = sType  || "navigation";
        arrReturn = this.GetHistory(sScope, sType, null, null, null );
      } 
      catch (e) 
      {
				_exception.HandleException(ex,'$ss.agentcore.dal.history','GetNavigationNodes',arguments);
      } 
      
      return arrReturn;
    }

    
  });

  var _xmlDI = null;
  var _logger = $ss.agentcore.log.GetDefaultLogger("ss.agentcore.dal.history");
  var _bInitialized = false;  
  var _constants = $ss.agentcore.constants;
  var _history = $ss.agentcore.dal.history;
  var _utils = $ss.agentcore.utils;
  var _xml = $ss.agentcore.dal.xml;
  var _logger = $ss.agentcore.log.GetDefaultLogger("$ss.agentcore.dal.history");  
  var _exception = $ss.agentcore.exceptions;
  
  var _resultsCache     = {};    // 80-85% of the hits to the History are delivered from the cache.
      
  // TD : Check if the names needs to be changed
  var _xmlDataIslandRootXml      = "<ss_history><history_entries /></ss_history>";
  var _xmlDataIslandId           = "ss_xmldi_history";
  var _dataIslandEntry        = "history_entry";
  var _dataIslandRootXpath    = "/ss_history/history_entries";
  var _dataIslandEntriesXPath = _dataIslandRootXpath+"/"+_dataIslandEntry;


  /*******************************************************************************
  ** Name:        ss_hst_GetByXPath
  **
  ** Purpose:     Returns a slice of the history object as specified by the XPath1.0
  **              parameter.  XPath 2.0 is not supported.
  **
  **              This function allows for raw querying of the History DOM.
  **
  ** Parameter:   sXPath1  -- XPath 1.0 compatible query
  **              oXmlDI   -- DOM to Query (Default history object used if none specified)
  **
  ** Return:      oNodes collection
  *******************************************************************************/
  function _GetByXPath( sXPath, oXmlDI, sSortBy )
  {
    //g_Logger.Info("ss_hst_GetByXPath");
    try
    {
      //var oXmlDI = oXmlDI || document.getElementById(_xmlDataIslandId);
      var oXmlDI = _xmlDI;
      if (oXmlDI)
      {
        var oNodes = oXmlDI.selectNodes( sXPath );
        if (sSortBy)
        {
          var arrSortedNodes = _SortNodes(oNodes, sSortBy);
          return arrSortedNodes;
        }
        return oNodes;
      }
    }
    catch (e)
    {
				_exception.HandleException(ex,'$ss.agentcore.dal.history','GetNavigationNodes',arguments);
      //g_Logger.Error("ss_hst_GetByXPath", "Unexpected error " + e.message);
      throw(e);
    }
    return null;
  }

  /*******************************************************************************
  ** Name:        ss_hst_Sort
  **
  ** Purpose:     Returns the specified collection of XML Nodes as an array 
  **              sorted by sSortBy.
  **
  ** Parameter:   oNodes  -- Collection to sort.
  **              sSortBy -- Attribute to sort by.
  **
  ** Return:      array
  *******************************************************************************/
  function _SortNodes( oNodes, sSortBy )
  {
    //g_Logger.Info("ss_hst_Sort");
    try
    {
      var arrToBeSorted = [];
      var arrUnsorted = [];
      for (var i=0; i<oNodes.length; i++)
      {
        arrToBeSorted.push(oNodes[i]);
        arrUnsorted.push(oNodes[i]);
      }

      return arrToBeSorted.sort(_SortRaw);
    }
    catch (e)
    {
				_exception.HandleException(ex,'$ss.agentcore.dal.history','GetNavigationNodes',arguments);
      //g_Logger.Error("ss_hst_Sort", "Unexpected error " + e.message);
      throw(e);
    }
    return null;
  }

  // private function to provide sSortBy as an argument.
  function _SortRaw( left , right )
  {
    var iLeft  = left.getAttribute(sSortBy);
    var iRight = right.getAttribute(sSortBy);

    if (iLeft<iRight)
      return -1;

    if (iLeft>iRight)
      return 1;

    return 0;
  }
  
  /*******************************************************************************
  ** Name:        ss_hst_pvt_GetRoot
  **
  ** Purpose:     Returns the documentElement for the current history DOM.
  **
  ** Parameter:   none
  **
  ** Return:      XML DOM documentElement
  *******************************************************************************/
  function _GetRoot( id )
  {
    //g_Logger.Info("ss_hst_pvt_GetRoot");
    try
    {
      //id = id || _xmlDataIslandId;
      //var xmlDI = document.getElementById( id );
      var xmlDI = _xmlDI;
      return xmlDI;
    } 
    catch (e)
    {
				_exception.HandleException(ex,'$ss.agentcore.dal.history','GetNavigationNodes',arguments);
      //g_Logger.Error("ss_hst_pvt_GetRoot", e.message);
      throw(e);
    }
    return null;
  }


  /*******************************************************************************
  ** Name:        ss_hst_GetPreviousPage
  **
  ** Purpose:     Returns the last element in the History Stack.
  **
  ** Parameter:   sScope   -- XPath Argument for the @scope attribute.
  **              sType    -- XPath Argument for the @type  attribute.
  **              bDeleted -- If true  @deleted must be 1.
  **                          If false @deleted must be 0.
  **                          If unspecified, @deleted is not used.
  **
  ** Return:      Last History Entry, or null.
  *******************************************************************************/
  function _GetPreviousPage( sScope, sType, bDeleted )
  {
    //g_Logger.Info("ss_hst_GetPreviousPage");
    try 
    {
      bDeleted = bDeleted || false;
      sType = sType || _constants.SS_HST_TYPE_NAVIGATION;
      
      var oNodes = _history.GetHistory( sScope, sType, bDeleted );
      if (oNodes != null && oNodes.length) {
        if(oNodes.length > 1)
          return oNodes[oNodes.length-2];
        else
          return oNodes[oNodes.length-1];
      }
      return null;
    
    } catch (e) {
				_exception.HandleException(ex,'$ss.agentcore.dal.history','GetNavigationNodes',arguments);
      //g_Logger.Error("ss_hst_GetPreviousPage", "Unexpected error " + e.message);
      throw(e);
    }
    return null;
  }

  /*******************************************************************************
  ** Name:        ss_hst_pvt_GetContainer
  **
  ** Purpose:     Returns the history_entry containing node, specified by
  **              ss_hst_XmlDataIslandRootXpath
  **
  ** Parameter:   none
  **
  ** Return:      XML DOM Node
  *******************************************************************************/
  function _GetContainer(id)
  {
    //g_Logger.Info("ss_hst_pvt_GetContainer");
    try
    {
      //id = id || _xmlDataIslandId;
      //var xmlDI      = document.getElementById( id );
      var xmlDI = _xmlDI;
      if(!xmlDI) 
      { 
        _history.Initialize();
        //xmlDI = document.getElementById( id );
        xmlDI = _xmlDI;
      }  
        
      var oContainer = xmlDI.selectSingleNode(_dataIslandRootXpath);
      return oContainer;
    } 
    catch (e)
    {
				_exception.HandleException(ex,'$ss.agentcore.dal.history','GetNavigationNodes',arguments);
      //g_Logger.Error("ss_hst_pvt_GetContainer", e.message);
      throw(e);
    }
    return null;
  }      

  function _GetDateHex() 
  {
    var sDate = "";
    try 
    {
      sDate = _utils.GetCurTimeAsHex();
    } 
    catch (e) 
    {
				_exception.HandleException(ex,'$ss.agentcore.dal.history','GetNavigationNodes',arguments);
    }
    return sDate;
  }

  /*******************************************************************************
  ** Name:        ss_hst_pvt_ClearCachedResults
  **
  ** Purpose:     Clears any cached results, as the history DOM has changed.
  **              This helps with performance since the same DOM Node list can 
  **              be returned 15 or more times during a single page change.
  **
  ** Parameter:   none
  **
  ** Return:      none
  *******************************************************************************/
  function _ClearCachedResults()
  {
    //g_Logger.Info("ss_hst_pvt_ClearCachedResults");
    for (var i in _resultsCache)
      delete _resultsCache[i]; // Delete each reference explicitely.
    _resultsCache = {};
  }

/*******************************************************************************
  ** Name:        ss_hst_pvt_Reset
  **
  ** Purpose:     Resets the History XMLDI to the default.
  **
  ** Parameter:   none
  **
  ** Return:      ss_hst_Success or ss_hst_Error
  *******************************************************************************/
  function _Reset()
  {
    //g_Logger.Info("ss_hst_pvt_Reset");
    try
    {
      //var xmlDI = document.getElementById(_xmlDataIslandId);
      var xmlDI = _xmlDI;
      if (xmlDI)
      {
        document.getElementsByTagName("HTML")[0].removeChild(xmlDI);
      }
      _bInitialized = false;
      _history.Initialize();
      _ClearCachedResults();
      //return ss_hst_Success;
      return true;
    }
    catch (e)
    {
				_exception.HandleException(ex,'$ss.agentcore.dal.history','GetNavigationNodes',arguments);
      //g_Logger.Error("ss_hst_pvt_Reset", "Unexpected error " + e.message);
      throw(e);
    }
    //return ss_hst_Success;
    return false;
  }

  /*******************************************************************************
  ** Name:        ss_hst_RemoveAll
  **
  ** Purpose:     Removes all history_entry elements.
  **
  ** Parameter:   sScope   -- XPath Argument for the @scope attribute.
  **              sType    -- XPath Argument for the @type  attribute.
  **              bDeleted -- If true  @deleted must be 1.
  **                          If false @deleted must be 0.
  **                          If unspecified, @deleted is not used.
  **
  ** Return:      ss_hst_Success or ss_hst_Error
  *******************************************************************************/
  function _RemoveAll( sScope, sType, bDeleted )
  {
    //g_Logger.Info("ss_hst_RemoveAll");
    try
    {
      var oNodes = _history.GetHistory(sScope, sType, bDeleted);
      for (var i=0; i<oNodes.length; i++)
      {
        oNodes[i].parentNode.removeChild(oNodes[i]);
      }
      _ClearCachedResults();
      //return ss_hst_Success;
      return true;
    }
    catch (e)
    {
				_exception.HandleException(ex,'$ss.agentcore.dal.history','GetNavigationNodes',arguments);
      //g_Logger.Error("ss_hst_RemoveAll", "Unexpected error " + e.message);
      throw(e);
    }
    //return ss_hst_Error;
    return false;
  }

  function _UnDeleteEntries( arrEntries ) 
  {
    try {
      for (var i=0; i<arrEntries.length; i++) {
        _UnDeleteEntry(arrEntries[i]);
      }
      //return ss_hst_Success;
      return true;
    } catch (e) {
      //g_Logger.Error("ss_hst_UnTrim", "Unexpected error " + e.message);
      throw(e);
    }
    //return ss_hst_Error;
    return false;
  }
  
  function _UnDeleteEntry( oEntry ) 
  {
    try 
    {
      if (oEntry) {
        oEntry.setAttribute("deleted", "0");
      }
      //return ss_hst_Success;
      return true;
    } catch (e) {
      //g_Logger.Error("ss_hst_UnTrimItem", "Unexpected error " + e.message);
      throw(e);
    }
    //return ss_hst_Error;
    return false;
  }  

// TD : something to do with ui

//  function ss_hst_pvt_GetLocalContext()
//  {
//    return ss_shi_GetLocalContext();
//    //return ss_hst_GetCount("*", "contextchange", null );
//  }

})();

