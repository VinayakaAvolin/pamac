
SnapinBaseController = MVC.Controller.extend({
  hHTMLCache: {},
  cacheTemplate: null,
  cacheExternalContent: null
}, {
   /**
  *  @name renderSnapinView
  *  @memberOf SnapinBaseController
  *  @function
  *  @description function to render the snapin  
  *  @param sSnapinName name of the snapin
  *  @param sContainerId  container Id, in which the snapin will be rendered
  *  @param sViewFile path of the template file
  *  @param aLocal local arguments array
  *  @returns string/false
  *  @example
  *
  *
  */
  
  renderSnapinView: function(sSnapinName, sContainerId, sViewFile, aLocal){
    //[MAC] Check Machine OS Windows/MAC
    if(bMac)
      var _logger = $ss.agentcore.log.GetDefaultLogger("$ss.agentcore.renderSnapinView");

    if (this.Class.cacheTemplate === null) { 
      this.Class.cacheTemplate = ($ss.agentcore.dal.config.GetConfigValue("global","cache_template","false") === "true")
    }
    
    var sViewFile = $ss.agentcore.dal.config.ParseMacros(sViewFile);

    //[MAC] Check Machine OS Windows/MAC
    if(!bMac) {
      var options = {
        absolute_url: 'file://' + $ss.getSnapinAbsolutePath(sSnapinName) + sViewFile,
        locals: aLocal,
        cache:this.Class.cacheTemplate
      }
    }
    else {
      var url =  $ss.getSnapinAbsolutePath(sSnapinName) + sViewFile;
      var message = {'JSClassNameKey':'File','JSOperationNameKey':'GetNativePath', 'JSArgumentsKey':[url], 'JSISSyncMethodKey' : '1'}
          var result = jsBridge.execute(message);
          var parsedJSONObject = JSON.parse(result);
          url = parsedJSONObject.Data;
          url = 'file://' + url;
      _logger.info('renderSnapinView = ' + url);
      var options = {
        absolute_url: url,
        locals: aLocal,
        cache:this.Class.cacheTemplate
      }
    }
    if (sContainerId) {
      options.to = sContainerId;
    }
    try {
      return this.render(options);
    } 
    catch (ex) {
      return false;
    }
  },
  
   /**
  *  @name renderExContentView
  *  @memberOf SnapinBaseController
  *  @function
  *  @description This needs to be called by the snapin whent the snapin wants to render any external
 *                HTML page/template. To render any external content this method usage is must as
 *                this method is responsible to log the reporting data based on the configuration.
 *                
  *  @param opt optional parameters
  *             opt.contentType - content type
  *             opt.GUID - guid of the external content
  *             opt.version - version of the content
  *             opt.to - optional, container ID in which the external content is to be rendered
  *             opt.file - file of the external content
  *             opt.local - optional local arguments
  *             opt.locFile - optional, localized resource bundle file name. Must be inside the content folder.
  *  @returns string/false content rendered or false if any problem.
  *  @example
  *              var opt = {
 *                             contentType: "sprt_articlecontenttype", //must
 *                             GUID: "12345678-.....", //must
 *                             version: "4", //must
 *                             toLocation: "#mylocation #forthiscontent", //uses CSS selector for to. if location
 *                                                                         is specified it will render the content 
 *                                                                         in that location else will return the content
 *                             file: "%LANGCODE%/description.html", //must, supports macros in the string
 *                             local: {localvar1: "mylocalvar1", localvar2: "mylocatvar2"} //optional, any local data
 *                                                                                         to be passed to the HTML template
 *                             locFile: "resource.html" //optional, the localized resource bundle residing inside the content GUID.
 *                                                        It will load the resourcebundle in {key1:value1,key2:value2} and
 *                                                        pass it to the template as a local data .. The resource bundle (object) can be
 *                                                        accessed inside template using the command <% var resBundle = data["resources"] %>                                                               
 *                                                                         
 *                       }
 *                       this.renderExContentView(opt);  
 *
 */
 
  renderExContentView: function(opt){
    var sContentType = opt.contentType;
    var sContentGUID = opt.GUID;
    var sVersion = opt.version;
    var toLocation = opt.to;
    var sFileName = $ss.agentcore.dal.config.ParseMacros(opt.file);
    var aLocal = opt.local;
    var sLocFile = $ss.agentcore.dal.config.ParseMacros(opt.locFile);
    var htmlCache = opt.htmlCache || false;
    var templateCache = opt.templateCache;
    var result = null;
    if (this.Class.cacheExternalContent === null) { 
      this.Class.cacheExternalContent = ($ss.agentcore.dal.config.GetConfigValue("global","cache_contentlet","false") === "true")
    }
    if(typeof(templateCache) === "undefined") {
      templateCache = this.Class.cacheExternalContent;
    }
    if (sContentType && sContentGUID && sVersion && sFileName) {
      //try to get the content from cache if cache enabled true
      // if (htmlCache) {
      result = this.Class.hHTMLCache[sContentGUID + sFileName];
      if (result && htmlCache) {
        if (toLocation) {
          $(toLocation).html(cresult);
          
        }
      }
      else {
        var fileRoot = $ss.GetAttribute('startingpoint') + "\\" +
        sContentType +
        "\\" +
        sContentGUID +
        "." +
        sVersion +
        "\\";
        var options = {};
        //TODO try to load the localize file as an hashtable and pass it as a parameter to the HTML page;
        
        if (!aLocal) {
          aLocal = {};
        }
        if (sLocFile) {
          aLocal.resources = $ss.agentcore.utils.LoadExternalResourceBundle(sContentType, sContentGUID, sVersion, sLocFile);
        }
        
        var options = {
          absolute_url: 'file://' + fileRoot + sFileName,
          locals: aLocal,
          cache: templateCache
        };
        options.cache = true;
        if (toLocation) {
          options.to = toLocation;
        };
        result = this.render(options);
        //Cache HTML output
        if (htmlCache) {
          if (result) {
            this.Class.hHTMLCache[sContentGUID + sFileName] = result;
          }
        }
      }
      // }
    
    }
    
    this.ReportContentView(sContentGUID, sContentType, sVersion);
    return result;
  },
  ReportContentView: function(sContentGUID,sContentType,sVersion) {
    $ss.agentcore.reporting.Reports.LogContentStart(this.Class.className, sContentGUID, sContentType, sVersion);
  }, 
  index: function(params){
  
  }
});

//START of Steplist Base Controller










//


SteplistBaseController = SnapinBaseController.extend({
  sSnapinName: "",
  oSnapinModel: null,
  sCurrentSessionId: "",
  sCurrentStepId: null,
  sBackToPageURL: null,
  sForwardToPageURL:null,
  sErrorUrl: null,
  sAbortUrl: null,
  bHasFlow: true,
  sSnapinContainerDIV : "",
  SnapinType: "standalone",
  sCompleteNavFileHistory: [],

  
  Initalize: function(params, sName){
    //set the snapin name as the current snapin.
    this.sSnapinName = sName;
    //initialize session data 
    this.InitiateSessionHistory(params,sName);
    //if the model has been initialized for steplist or no steplist do nothing
   // this.RegsiterSnapinEntry(params,this.sSnapinName);
    
    if(this.oSnapinModel || this.bHasFlow == false) return true;
    //initialize the model 
    if (sName) {
      var snapinObj = $ss.getSnapin(sName);
      if (snapinObj && snapinObj.flows && snapinObj.flows.length) {
        this.oSnapinModel = new $ss.agentcore.model.StepListSnapinModel(sName, snapinObj.flows[0]);
        this.SnapinType = "steplist";
      }
    }
    if(!this.oSnapinModel) {
          this.bHasFlow = false;
          this.SnapinType = "standalone";
    }
  },

  //Initate the steplist and steps when the user come for the first time..
  InitiateStep: function(params){
    
    switch(this.SnapinType) {
      case "standalone":
        var returnObj  = this._InitiateStepStandAloneType(params);
        delete params.stepListSession.mode;
        delete params.stepListSession.resumemode;
        return returnObj;
        break;  
      case "steplist":
      default:
        var returnObj  = this._InitiateStepStepListType(params);
        delete params.stepListSession.mode;
        delete params.stepListSession.resumemode;
        return returnObj;
    }
  },
  
  _InitiateStepStandAloneType: function(params) {
       var retObj = {};
       this.RegsiterSnapinEntry(params,this.sSnapinName);
       params.stepListSession.mode = params.stepListSession.mode || "start";
       var doRenderThisSnapin = true;
       switch (params.stepListSession.mode) {
         case "resume":
           var resumeMode = params.stepListSession.resumemode || "auto"; 
           switch(resumeMode) {
             case "auto":
               doRenderThisSnapin = false;
               break;
              default:
                break; 
           }
           break;
         case "continue":
         case "start":
         case "from_back":
         default: 
            doRenderThisSnapin = true;
           break;
       } 
       ////delete params.stepListSession.mode;
       ////delete params.stepListSession.resumemode;
       var tmpSessionObject = this.GetSessionObject(this.sCurrentSessionId) || {};
       var oCurrentStep = tmpSessionObject.oCurrentStep;
       this.SetInitParamsOnResume(params,oCurrentStep);
       if (doRenderThisSnapin) {
         if (this.InitParam) {
           retObj.file = this.InitParam.sDefaultContentPage;
           retObj.type = "url";
         }
         
         this.UpdatedWithSession(this.sCurrentSessionId, retObj, params);

         tmpSessionObject.oCurrentStep = tmpSessionObject.oCurrentStep ||
         {};
         //associate the stepid
        retObj.stepid = tmpSessionObject.oCurrentStep.StepID || undefined;
        //No need to update the history as this is already there in the history object..
        //this is getting out of the snapin itself.. remove it from the snapin stack
        // this.RegsiterSnapinExit(params,this.sSnapinName);
        tmpSessionObject = null;
        oCurrentStep = null;
        return retObj;
      } else {
        tmpSessionObject = null;
        oCurrentStep = null;
        return this.ProcessNext(params);
      }
  },
  _InitiateStepStepListType: function(params) {
    var stepListToLoad = "start";
    params.stepListSession = params.stepListSession || {};
    var stepListStartMode = params.stepListSession.mode || "start";
    //params.stepListSession.mode = params.stepListSession.mode || "start";
    ////delete params.stepListSession.mode;
    var _const = $ss.agentcore.constants;
    //register the snapin entry 
    
    if (this.oSnapinModel) {
      switch (stepListStartMode) {
        case "continue":
          this.RegsiterSnapinEntry(params,this.sSnapinName);
          //It has come from a snapin
          if (params.stepListSession.lastSnpCompleted) {
            var lastSnapinComplStatus = params.stepListSession.lastSnpCompleteStatus;
            var lastResult = params.stepListSession.lastSnpCompleteStatus || _const.SL_SNAPIN_RESULT_ID_MAP["ss_snp_Success"];
            var retO = this.oSnapinModel.ProcessResultSet(lastResult);
            return this.ProcessStepRun(retO, params);
          }
          else {
            return this.ProcessNext(params);
          }
          break;
        case "from_back":
          this.RegsiterSnapinEntry(params,this.sSnapinName);
          var tmpSessionObject = this.GetSessionObject(this.sCurrentSessionId);
          var aHistoryFlows = tmpSessionObject.aHistoryFlows;
          if (aHistoryFlows && aHistoryFlows.length > 0) {
            var stepId = aHistoryFlows[aHistoryFlows.length - 1].StepID;
            var steplListId = aHistoryFlows[aHistoryFlows.length - 1].StepListId;
            this.PopHistoryOnBack(this.sCurrentSessionId);
            var retObj = this.oSnapinModel.StepBackward(steplListId, stepId)
            tmpSessionObject = null;
            aHistoryFlows = null;
            return this.ProcessStepRun(retObj,params);
            //return this.UpdatedWithSession(this.sCurrentSessionId, retObj)
          }
          break;
        case "resume":
          return this.ProcessResumeForSteplist(params);
          break;  
        case "start":
        default:
          this.RegsiterSnapinEntry(params,this.sSnapinName);
          $ss.agentcore.reporting.Reports.LogFlowStart(this.sSnapinName);
          this.sCompleteNavFileHistory = [];
          this.oSnapinModel.LoadStepList(stepListToLoad, true);
          retObj = this.oSnapinModel.RunStep();
          return this.ProcessStepRun(retObj,params);
          break;
      }
    }
  },
  

  
  ProcessNext: function(params){
    var mode = "continue";
    var _const = $ss.agentcore.constants;
    var retObj = {};
    if (this.oSnapinModel) {
      retObj = this.oSnapinModel.StepForward(this.sCurrentSessionId);
    }
    return this.ProcessStepRun(retObj,params);
  },

  ProcessPrevious: function(params){
    var mode = "from_back";
    params.stepListSession = params.stepListSession || {};
    params.stepListSession.sessionId = this.sCurrentSessionId;
    params.stepListSession.mode = "from_back";
    var tmpSessionObject = this.GetSessionObject(this.sCurrentSessionId);
    var aStepRecords = tmpSessionObject.aHistoryFlows;
    if(aStepRecords && aStepRecords.length >0) {
        var oPrevStep = aStepRecords[aStepRecords.length-1];
        var sSnapinNameOfPrevStep = oPrevStep.Snapin;
        //Check the type of step
        if(oPrevStep.StepType === "snapin") {
          var targetStepListSnapin = oPrevStep.target.snapin;
          var steplistController = window[ MVC.String.classize(targetStepListSnapin)+'Controller'];
          if(steplistController && steplistController.SnapinType === "steplist")  {
            steplistController = null;
            this.PopHistoryOnBack(this.sCurrentSessionId); 
            }
          steplistController = null;        
        }
      
      for (var i=aStepRecords.length-1; i>=0; i--) {
       
        var oPrevStep = aStepRecords[i];
        var sSnapinNameOfPrevStep = oPrevStep.Snapin;
        //the step belongs to some other snapin load the respective snapin
        
        
        if(sSnapinNameOfPrevStep !== this.sSnapinName) {
          //this.PopHistoryOnBack(this.sCurrentSessionId);
          this.ResetCallerSnapinEntryPosition(params);
          tmpSessionObject.aSnapinStack = tmpSessionObject.aSnapinStack || [];
          //TODO relook at the snapinback stack calculation --> Length has been capped to 1; will not scale if more than 2 snapins ar there in stack 
          if(tmpSessionObject.aSnapinStack.length > 1 && tmpSessionObject.aSnapinStack[tmpSessionObject.aSnapinStack.length-1] === this.sSnapinName) {
            tmpSessionObject.aSnapinStack.pop();
          }
          var retObj = oPrevStep;
          params.toLocation = params.toLocation || this.sSnapinContainerDIV;
          params.mode = "from_back";
          params=this.UpdateParams(params,retObj);
          tmpSessionObject  = null;
          aStepRecords = null;
          this.dispatch(sSnapinNameOfPrevStep,"index",params);
          return false;
          break;
        } else {
          //the step definition is part of the same snapin 
          var nodeType = oPrevStep.StepType;
          var stepId =  oPrevStep.StepID;
          var steplListId = oPrevStep.StepListId;
          var bHasFlows = oPrevStep.isPartOfFlow;
          //load the steplist,step and run
          this.PopHistoryOnBack(this.sCurrentSessionId); 
          var retObj = {};
          if (this.oSnapinModel) {
            retObj = this.oSnapinModel.StepBackward(steplListId, stepId)
          }
          tmpSessionObject  = null;
          aStepRecords = null;
          return this.ProcessStepRun(retObj,params);
        }
        break; //do not continue the loop;
      }
    } 
          

  //If the page has defined 

    if (this.sBackToPageURL) {
       //set the path
      $ss.agentcore.reporting.Reports.LogFlowDone(this.sSnapinName, this.sCompleteNavFileHistory, $ss.agentcore.constants.SL_SNAPIN_RESULT_ABORTED);
      tmpSessionObject  = null;
      aStepRecords = null;
       params.requesturl = this.sBackToPageURL;
       this.dispatch("navigation", "index", params);
    }
    tmpSessionObject  = null;
    aStepRecords = null;
    
  
  },
  ProcessStepRun: function(retObj,params) {
    
    var _const = $ss.agentcore.constants;
    retObj = retObj ||
    {};
    if (this.oSnapinModel) {
      if (retObj.status == _const.SL_CancelNavigation) {
        retObj.type = _const.SL_STEPTYPE_SNAPINCOMPLETE;
      }
      retObj = this.UpdatedWithSession(this.sCurrentSessionId, retObj);
    }
    else {
      //incase of standalone the type is complete
      retObj.type = _const.SL_STEPTYPE_SNAPINCOMPLETE;
    }
    try {
      switch (retObj.type) {
        case _const.SL_STEPTYPE_URL:
          this.sCompleteNavFileHistory.push(retObj.file);
          return retObj;
          break;
        case _const.SL_STEPTYPE_SNAPINCOMPLETE:
          this.RegsiterSnapinExit(params, this.sSnapinName);
          var tmpSessionObject = this.GetSessionObject(this.sCurrentSessionId);
          var aSnapins = tmpSessionObject.aSnapinStack;
          if (aSnapins.length) {
            var sSnapin = aSnapins[aSnapins.length - 1];
            params = this.UpdateParams(params, retObj, "continue");
            params.stepListSession.lastSnpCompleteStatus = retObj.completeStatus || _const.SL_SNAPIN_RESULT_ID_MAP["ss_snp_Success"];
            params.stepListSession.lastSnpCompleted = true;
            tmpSessionObject  = null;
            aSnapins = null;

            this.dispatch(sSnapin, "index", params);
          }
          else {
            var nextPath = null;
            var iLogResult = null;
            switch (retObj.completeStatus) {
              case _const.SL_SNAPIN_RESULT_ID_MAP["ss_snp_Failed"]:
                nextPath = this.sErrorUrl;
                iLogResult = $ss.agentcore.constants.SL_SNAPIN_RESULT_FAILED;                
                break;
              case _const.SL_SNAPIN_RESULT_ID_MAP["ss_snp_Aborted"]:
                nextPath = this.sAbortUrl;
                iLogResult = $ss.agentcore.constants.SL_SNAPIN_RESULT_ABORTED;                
                break;
              case _const.SL_SNAPIN_RESULT_ID_MAP["ss_snp_Success"]:
                nextPath = this.sForwardToPageURL;
                iLogResult = $ss.agentcore.constants.SL_SNAPIN_RESULT_SUCCESS;
                break;
              case _const.SL_SNAPIN_RESULT_ID_MAP["ss_snp_SuccessNoChange"]:
                nextPath = this.sForwardToPageURL;
                iLogResult = $ss.agentcore.constants.SL_SNAPIN_RESULT_SUCCESS_NOCHANGE;
              default:
                nextPath =this.sForwardToPageURL;
                iLogResult = $ss.agentcore.constants.SL_SNAPIN_RESULT_SUCCESS;                
                break;
            }
            if (!nextPath) {
              nextPath = this.sForwardToPageURL;
            }

            if (iLogResult) {
              $ss.agentcore.reporting.Reports.LogFlowDone(this.sSnapinName, this.sCompleteNavFileHistory, iLogResult);
              
            }
            
            if (nextPath) {
              //set the path
              delete params.requestPath;
              delete params.requestQS;
              delete params.requestArgs;
              params.requesturl = nextPath;
              tmpSessionObject  = null;
              aSnapins = null;
              var dbSessionName = "flow_session_" + this.sCurrentSessionId;
              $ss.agentcore.dal.databag.RemoveValue(dbSessionName);
              this.sCompleteNavFileHistory = null;
              this.dispatch("navigation", "index", params);
            }
          }
          break;
        case _const.SL_STEPTYPE_SNAPIN:
          var sSnapin = retObj.snapin;
          this.UpdateParams(params, retObj, "start");
          tmpSessionObject  = null;
          aSnapins = null;
          this.dispatch(sSnapin, "index", params);
          break;
        default:
          tmpSessionObject  = null;
          aSnapins = null;
          return retObj;
          break;
      }
    } 
    catch (ex) {
      var exception;
    }
  },
  ProcessResumeForSteplist: function(params) {
    //Resume modes are "auto -- resume from the next available step
    //"current" -- restart form the last executed mode
    //"lastmstone" -- start from the last known milestone mode - TODO
    var resumeMode = params.stepListSession.resumemode || "auto"; 
    var sDbVariableName = "flow_session_" + this.sCurrentSessionId;
    var oSessionObject = $ss.agentcore.dal.databag.GetValue(sDbVariableName);
    var oCurrentStep = oSessionObject.oCurrentStep;
    if(!oCurrentStep) {
      // restart the steplist - no current step was found
      ////delete params.stepListSession.mode;
      ////delete params.stepListSession.resumemode;
      params.stepListSession.mode = "start";
      this.Initalize(params,this.className);
      return this.InitiateStep(params);
    }
    //if this is not the correct snapin redirect the user to the correct snapin
    var aSnapins = oSessionObject.aSnapinStack;
    
    if (aSnapins.length) {
      var sSnapin = aSnapins[aSnapins.length - 1];
      if(sSnapin !== this.sSnapinName) {
        var bDispatchable = false;
        for (var ii = 0; ii < aSnapins.length ; ii++) {
          if (bDispatchable) {
            this.dispatch(aSnapins[ii], "index", params);
            return;
          }
          if(aSnapins[ii] === this.sSnapinName) {
            this.ResetCallerSnapinEntryPosition(params);
            bDispatchable = true;
          }
        }
        
      }  
    }

    var cSteplistId = oCurrentStep.StepListId;
    var currentStepId = oCurrentStep.StepID;
    var retObj = {};
    ////delete params.stepListSession.mode;
    ////delete params.stepListSession.resumemode;
    this.RegsiterSnapinEntry(params,this.sSnapinName);
    
    switch (resumeMode) {
      case "current":
         var status = this.oSnapinModel.LoadStepList(cSteplistId, true);
         status = this.oSnapinModel.LoadStepByID(currentStepId);
         retObj = this.oSnapinModel.RunStep();
         oSessionObject.oCurrentStep = null;
         return this.ProcessStepRun(retObj,params);   
        break;
      case "lastmstone":
        break;
      case "auto":
        var status = this.oSnapinModel.LoadStepList(cSteplistId, true);
        status = this.oSnapinModel.LoadStepByID(currentStepId);
        return this.ProcessNext(params);
        break;    
    }

  },
  ResetCallerSnapinEntryPosition: function(params) {
    var sDbVariableName = "flow_session_" + this.sCurrentSessionId;
    var oSessionObject = $ss.agentcore.dal.databag.GetValue(sDbVariableName);
    var aHistoryFlows = oSessionObject.aHistoryFlows;
    if(aHistoryFlows.length) {
      for (var i = aHistoryFlows.length - 1; i >= 0; i--) {
        var flowDetails = aHistoryFlows[i];
        if(flowDetails.StepType === "snapin" && flowDetails.Snapin === this.sSnapinName) {
          var cSteplistId = flowDetails.StepListId || "start";
          var currentStepId = flowDetails.StepID;
          if(this.oSnapinModel) {
             try {
                var status = this.oSnapinModel.LoadStepList(cSteplistId, true);
                if(currentStepId) status = this.oSnapinModel.LoadStepByID(currentStepId);
             } catch(ex) {
               }
          }
          break;
        }
      }
    } 

  },

  Retry: function(params) {
    var sDbVariableName = "flow_session_" + this.sCurrentSessionId;
    var oSessionObject = $ss.agentcore.dal.databag.GetValue(sDbVariableName);
    var oCurrentStep = oSessionObject.oCurrentStep;
    var retObj = {};
    retObj.type = oCurrentStep.StepType;
    retObj.stepid = oCurrentStep.StepID;
    retObj.file = oCurrentStep.StepPage;
    retObj.hasForward = oCurrentStep.hasForward;
    retObj.hasBackward = oCurrentStep.hasBackward;
    retObj.isPartOfFlow = oCurrentStep.hasBackward;
    return this.UpdatedWithSession(this.sCurrentSessionId,retObj);
    
  },
  GetSessionId: function() {
    return this.sCurrentSessionId;
  },
  GetSessionObject: function(sSessionId) {
    
    var sDbVariableName = "flow_session_" + sSessionId;
    return $ss.agentcore.dal.databag.GetValue(sDbVariableName);
  },
  InitiateSessionHistory: function(params,snapinName) {
    //set the inital parameters
    this.SetInitParams(params);
    //initiate databag object
    var sDbVariableName = "flow_session_" + this.sCurrentSessionId;
    var oSessionObject = $ss.agentcore.dal.databag.GetValue(sDbVariableName);
    if(!oSessionObject) {
      var tempSessionObject = {
                aSnapinStack: [],
                aHistoryFlows: [],
                aLogFlows:[],
                oCurrentStep: null
      };
      $ss.agentcore.dal.databag.SetValue(sDbVariableName,tempSessionObject);
    };

   },
   UpdateSessionHistory: function(retObj,params) {
    var sDbVariableName = "flow_session_" + this.sCurrentSessionId;
    var oSessionObject = $ss.agentcore.dal.databag.GetValue(sDbVariableName);
    if(!oSessionObject) this.InitiateSessionHistory(params);
    if (retObj && retObj.stepDetail && (retObj.type ==="url" || retObj.type ==="snapin" ) ) {
      if (oSessionObject.oCurrentStep && oSessionObject.oCurrentStep.StepSkipOnBack != "true") {
        if (oSessionObject.oCurrentStep) {
          var pushStepInHistory = true;
          if (pushStepInHistory) {
            oSessionObject.aHistoryFlows.push(oSessionObject.oCurrentStep);
          }
        }
        if (retObj.type ==="url") {
          oSessionObject.aLogFlows.push(retObj.file);
        }
      }
      
      oSessionObject.oCurrentStep = retObj.stepDetail;
      $ss.agentcore.dal.databag.SetValue(sDbVariableName,oSessionObject);
    }
      
             
   },
 
  UpdatedWithSession: function(sSessionId,retObj,params) {
    var sDbVariableName = "flow_session_" + this.sCurrentSessionId;
    var oSessionObject = $ss.agentcore.dal.databag.GetValue(sDbVariableName);
    if (retObj && retObj.stepDetail) {
      this.UpdateSessionHistory(retObj, params);
    }
    
    retObj.hasForward = (this.sForwardToPageURL ? true: false) || retObj.hasForward || false;
    if (oSessionObject.aSnapinStack && oSessionObject.aSnapinStack.length > 1) 
      retObj.hasForward = true;

    //retObj.hasForward = retObj.hasForward || oSessionObject.oCurrentStep.hasForward || false;

    retObj.hasBackward = this.sBackToPageURL ? true : false;
    if (oSessionObject.aHistoryFlows && oSessionObject.aHistoryFlows.length > 0) 
      retObj.hasBackward = true;
    retObj.sessionId = this.sCurrentSessionId;
    //retObj.stepid = retObj.stepid || oSessionObject.oCurrentStep.StepID;
    retObj.sBackToPageURL = this.sBackToPageURL;
    retObj.sForwardToPageURL = this.sForwardToPageURL;
    retObj.sErrorUrl = this.sErrorUrl;
    retObj.sAbortUrl = this.sAbortUrl;
    
    if (retObj && retObj.stepDetail) {
      retObj.stepDetail.hasForward = retObj.hasForward;
      retObj.stepDetail.hasBackward = retObj.hasBackward;
      retObj.stepDetail.isPartOfFlow = this.bHasFlow;
      retObj.stepDetail.sBackToPageURL = this.sBackToPageURL;
      retObj.stepDetail.sForwardToPageURL = this.sForwardToPageURL;
      retObj.stepDetail.sErrorUrl = this.sErrorUrl;
      retObj.stepDetail.sAbortUrl = this.sAbortUrl;
      //this.UpdateSessionHistory(retObj, params);
    }
    return retObj;
  },

  PopHistoryOnBack: function(sSessionId){
    var sDbVariableName = "flow_session_" + sSessionId;
    var oSessionObject = $ss.agentcore.dal.databag.GetValue(sDbVariableName);
    var aHistory = oSessionObject.aHistoryFlows;
    var oCurrent = oSessionObject.oCurrentStep;
    if(aHistory && aHistory.length >0) {
      oSessionObject.oCurrentStep = null; //current step will be loaded..
      oSessionObject.aHistoryFlows.pop();
    }
    $ss.agentcore.dal.databag.SetValue(sDbVariableName,oSessionObject);
  },
  UpdateParams : function(params,oTraObj,sMode) {
    params.stepListSession = {};
    params.stepListSession.sessionId = this.sCurrentSessionId;
    params.stepListSession.hasForward = oTraObj.hasForward;
    params.stepListSession.hasBack = oTraObj.hasForward;
    params.toLocation = params.toLocation || this.sSnapinContainerDIV;
    params.stepListSession.sBackToPageURL = params.stepListSession.sBackToPageURL || this.sBackToPageURL;
    params.stepListSession.sForwardToPageURL = params.stepListSession.sForwardToPageURL || this.sForwardToPageURL;
    params.stepListSession.mode = params.mode  || sMode || "start";
    params.stepListSession.sErrorUrl = params.stepListSession.sErrorUrl || this.sErrorUrl || null;
    params.stepListSession.sAbortUrl = params.stepListSession.sAbortUrl || this.sAbortUrl || null;
    return params;
  },
  SetActiveSession: function(params) {
    params.stepListSession = params.stepListSession || {};
    //First Get the Session Id From Element
    var sessionId = params.element.stepSessionId;
    //Next try to get session Id from params value
    sessionId = sessionId || params.stepListSession.sessionId || null;
    //If not found create new session
    if (!sessionId) {
      sessionId = new String(new Date().getTime());
      ;
      this.SetInitParams(params);
    }
    //set it to the member variable
    this.sCurrentSessionId = sessionId;
    params.stepListSession = params.stepListSession || {};
    params.stepListSession.sessionId = this.sCurrentSessionId;
    params.element.stepSessionId = this.sCurrentSessionId;
  },
  SetInitParams: function(params) {
    var tmpSession = params.stepListSession;
    if (params && params.stepListSession && (tmpSession.sBackToPageURL || tmpSession.sForwardToPageURL || tmpSession.sErrorUrl || tmpSession.sAbortUrl)) {
      this.sBackToPageURL = params.stepListSession.sBackToPageURL || this.sBackToPageURL || null;
      this.sForwardToPageURL = params.stepListSession.sForwardToPageURL || this.sForwardToPageURL || null;
      this.sErrorUrl = params.stepListSession.sErrorUrl || this.sErrorUrl || null;
      this.sAbortUrl = params.stepListSession.sAbortUrl || this.sAbortUrl || null;
    }
    else {
      this.sCurrentStepId = null;
      this.bHasFlow = true;
      this.sBackToPageURL = this.InitParam ? (this.InitParam.sBackToPageURL || null) : null;
      this.sForwardToPageURL = this.InitParam ? (this.InitParam.sForwardToPageURL || null) : null;
      this.sErrorUrl = this.InitParam ? (this.InitParam.sErrorPageURL || null) : null;
      this.sAbortUrl = this.InitParam ? (this.InitParam.sAbortPageURL || null) : null;
    }
    this.sSnapinContainerDIV = params.toLocation;
  },
  SetInitParamsOnResume: function(params,currentStep) {
      params.stepListSession.hasForward = currentStep.hasForward;
      params.stepListSession.hasBackward = currentStep.hasBackward;
      this.sBackToPageURL = params.stepListSession.sBackToPageURL = currentStep.sBackToPageURL;
      this.sForwardToPageURL =params.stepListSession.sForwardToPageURL = currentStep.sForwardToPageURL;
      this.sErrorUrl = params.stepListSession.sErrorUrl = currentStep.sErrorUrl;
      this.sAbortUrl = params.stepListSession.sAbortUrl = currentStep.sAbortUrl;
  }
  ,
  RegsiterSnapinEntry: function(params,snapinName) {
    
    if (!snapinName) 
      return false;
    var sDbVariableName = "flow_session_" + this.sCurrentSessionId;
    var oSessionObject = $ss.agentcore.dal.databag.GetValue(sDbVariableName);
    if (!oSessionObject) 
      this.InitiateSessionHistory(params);
    var aSnapins = oSessionObject.aSnapinStack;
    //create the snapin stack
    var lastSnapinName = null;
    if (aSnapins.length > 0) {
      lastSnapinName = aSnapins[aSnapins.length - 1];
    }
    if (snapinName === lastSnapinName) {
      return false;
    } //do nothing it is alrady included at the top;
    oSessionObject.aSnapinStack.push(snapinName);
    $ss.agentcore.dal.databag.SetValue(sDbVariableName, oSessionObject);
    return true;
  },
  RegsiterSnapinExit: function(params,snapinName) {
    
    if (!snapinName) 
      return false;
    var poppedSnapin = null;
    var sDbVariableName = "flow_session_" + this.sCurrentSessionId;
    var oSessionObject = $ss.agentcore.dal.databag.GetValue(sDbVariableName);
    if (!oSessionObject) 
      this.InitiateSessionHistory(params);
    var aSnapins = oSessionObject.aSnapinStack;
    //create the snapin stack
    var lastSnapinName = null;
    if (aSnapins.length > 1) {
      lastSnapinName = aSnapins[aSnapins.length - 1];
    }
    //    if (snapinName === lastSnapinName) {
    poppedSnapin = oSessionObject.aSnapinStack.pop();
    $ss.agentcore.dal.databag.SetValue(sDbVariableName, oSessionObject);
    //    }
    return poppedSnapin;
  },
  CheckResumeMode: function(params)  {
    //Resume Mode Checks Input Parameter..Input parameters must have following arguments in the Query
    //path - "sl_resume" == true; sl_session=<session id>. However "sl_resumemode" is optional. It can have
    //"auto" or "current". Bydefault "auto" will take the user to next step.
    if(params.requestArgs && params.requestArgs["sl_resume"]==="true" && params.requestArgs["sl_session"]) {
      params.stepListSession = params.stepListSession || {};
      params.stepListSession.mode = "resume";
      params.stepListSession.resumemode = params.requestArgs["sl_resumemode"] || "auto";
      params.stepListSession.sessionId = params.requestArgs["sl_session"];
      delete params.requestArgs["sl_resume"];
      delete params.requestArgs["sl_session"];
      delete params.requestArgs["sl_resumemode"];
      var sDbVariableName = "flow_session_" + params.stepListSession.sessionId;
      var oSessionObject = $ss.agentcore.dal.databag.GetValue(sDbVariableName);
      var oCurrentStep = oSessionObject.oCurrentStep;
      if(oCurrentStep) {
        this.SetInitParamsOnResume(params,oCurrentStep);
      }
      oSessionObject = null;
      oCurrentStep = null;
    }
  },
  HasPrevious: function() {
      var hasPrev = true;
      var sDbVariableName = "flow_session_" + this.sCurrentSessionId;
      var oSessionObject = $ss.agentcore.dal.databag.GetValue(sDbVariableName);
      var oCurrentStep = oSessionObject.oCurrentStep || {};
      if(oCurrentStep.hasBackward === false) hasPrev = false;
      oSessionObject = null;
      oCurrentStep = null;
      return hasPrev;
  },
  HasNext: function() {
      var hasNext = true;
      var sDbVariableName = "flow_session_" + this.sCurrentSessionId;
      var oSessionObject = $ss.agentcore.dal.databag.GetValue(sDbVariableName);
      var oCurrentStep = oSessionObject.oCurrentStep || {};
      if(oCurrentStep.hasForward === false) hasNext = false;
      oSessionObject = null;
      oCurrentStep = null;
      return hasNext;
  },
  ProcessNextThisSession: function(params) {
    if(!this.sCurrentSessionId) return false;
    var params = params || {};
    params.element = params.element || {};
    params.element.stepSessionId = this.sCurrentSessionId;
    MVC.Controller.dispatch(this.sSnapinName,"ProcessNext",params);
  },
  ProcessPreviousThisSession: function(params) {
    if(!this.sCurrentSessionId) return false;
    var params = params || {};
    params.element = params.element || {};
    params.element.stepSessionId = this.sCurrentSessionId;
    MVC.Controller.dispatch(this.sSnapinName,"ProcessPrevious",params);
  }
  
}
, //protoype methods

{
  index: function(params,sName){

    this.stepstart(params,sName);
    this._super();
  },
  stepstart: function(params,sName) {
    this.Class.CheckResumeMode(params);
    this.Class.SetActiveSession(params);
    this.Class.Initalize(params,sName);
  },
  resume: function(params,sName){
    this.Class.SetActiveSession(params);
  },  

  ProcessNext: function(obj,params){
    if (params && params.element && (params.element.disabled === true)) 
      return false;
    var lastStepSession = this.Class.sCurrentSessionId;
    this.Class.SetActiveSession(params);
    //The session is different so restart the steplist
    if(lastStepSession != this.Class.sCurrentSessionId) {
      params.stepListSession.mode = "start";
      this.Class.Initalize(params,this.Class.className);
      return this.Class.InitiateStep(params);
    }
    if(!this.Class.HasNext()) return false;
    return obj.Class.ProcessNext(params);
  },

  ProcessPrevious: function(obj,params){
    if (params && params.element && (params.element.disabled === true)) 
      return false;
    var lastStepSession = this.Class.sCurrentSessionId;
    this.Class.SetActiveSession(params);
    //The session is different restart the steplist
    if(lastStepSession != this.Class.sCurrentSessionId) {
      params.stepListSession.mode = "start";
      this.Class.Initalize(params,this.Class.className);
      return this.Class.InitiateStep(params);
    }
    if(!this.Class.HasPrevious()) return false;
    return obj.Class.ProcessPrevious(params);
  },
  ProcessRetry: function(obj,params,stepId){
    this.Class.SetActiveSession(params);
    return this.Class.Retry(params);
  },  
  ProcessFlow: function(obj,params,oFlow, opt) {
   var oFlowInfo = oFlow || {};

   switch(oFlowInfo.type) {
     case "url":
       var locals = null;
       if(opt && opt.oLocal) locals = opt.oLocal;
       this.RenderSteplistContent(obj,oFlowInfo,locals);
       break;
     case "snapin":
          var tmpSessionObject = this.Class.GetSessionObject(this.Class.sCurrentSessionId);
          params = this.Class.UpdateParams(params,oFlowInfo,"start");
          try {
            this.Class.dispatch(oFlowInfo.snapin,"index",params);
          }catch(ex) {
          }
       break;
     default:
       break;      
   }
  },
  
  RenderSteplistContent: function(obj,oRetObj, aLocal){
    try {
      if (oRetObj && oRetObj != -20) {
        //run before render function and pass the params
        this.renderSnapinView(this.Class.sSnapinName, this.Class.InitParam.aContentPlaceHolderDIV, "/" + oRetObj.file, aLocal);
        this.SetSessionOnNavButtons(oRetObj);
        //run after render function and take necssary option
      }
    } 
    catch (e) {
      var errorx = null;
    }
  },

  SetSessionOnNavButtons: function(oRetObj) {
    if (oRetObj) {
      for (var x = 0; this.Class.InitParam && this.Class.InitParam.aNavigationItems && x < this.Class.InitParam.aNavigationItems.length; x++) {
          $(this.Class.InitParam.aNavigationItems[x]).attr("stepSessionId", oRetObj.sessionId).attr("stepId", oRetObj.stepid);
      }
    }
  },
  
  SetSessionOnHTMLItems: function(oRetObj,aHTMLItems) {
    if (oRetObj) {
      for (var x = 0; aHTMLItems && x < aHTMLItems.length; x++) {
          $(aHTMLItems[x]).attr("stepSessionId", oRetObj.sessionId).attr("stepId", oRetObj.stepid);
      }
    }
  },
  
  GetFlowSessionId: function() {
    return this.Class.sCurrentSessionId;
  },
  GetFlowSessionObject: function(sSessionId) {
    var sDbVariableName = "flow_session_" + sSessionId;
    return $ss.agentcore.dal.databag.GetValue(sDbVariableName);
  },
  GetQueryStringForRestart: function(resumeMode) {
    resumeMode = resumeMode || "auto";
    var queryString = "sl_resume=true&";
    queryString += "sl_session="+this.GetFlowSessionId()+"&";
    queryString += "sl_resumemode="+resumeMode;
    return  queryString;   
  }
});

  //[MAC] Get Machine OS type
  var bMac = $.browser.safari;

  //[MAC] JSBridge object used only for MAC
  try {
    var jsBridge = window.JSBridge;
  }
  catch(e) {
  }
