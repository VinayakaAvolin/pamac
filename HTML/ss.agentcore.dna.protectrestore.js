$ss.agentcore.dna = $ss.agentcore.dna || {};
/** @namespace Holds all ini related functionality*/
$ss.agentcore.dna.protectrestore = $ss.agentcore.dna.protectrestore || {};

(function()
{
    /**
    *  @name Probe
    *  @memberOf $ss.agentcore.dna.protectrestore
    *  @function
    *  @description  Default constructore for the Probe class
    *  @param systemTrayProtectionPrefix  optional string for system tray icon
    *  @returns Probe object
    *  @example
    *       
    *       
    */
    $ss.agentcore.dna.protectrestore.Probe = Class.extend(
    {
      init:function(systemTrayProtectionPrefix)
      {
        try
        {
          this.objProtect = _utils.CreateObject('SPRT.TgProtect');   
          this.objProtect.EnableSysTrayIcon(true, true, "Protecting Stuff...");   
          this.objProtect.ShowUI = false;

          this.objTxHeal  = _utils.CreateObject('SPRT.TxHeal'); 
          this.objTxUndo  = _utils.CreateObject('SPRT.TxUndo'); 

          this.objSystem  = _utils.CreateObject('SPRT.System');
          this.objCatalog = _utils.CreateObject('SPRT.Catalog');
          this.objUserCtx = _utils.CreateObject('SPRT.UserContext');

          this.guids  = {}; // index of guids to {_Protection}
          this.names  = {}; // index of literal names to {_Protection}
          this.namesLowerCase = {}; // index of lower case names to {_Protection}
          //this.list   = []; // array of {_Protection}
         
          //max protections overriding in global sectio of config
          this.iMaxProtections = _config.GetConfigValue("protections", "protect_max_number_of_protections", -1);
          
          if(!systemTrayProtectionPrefix)
              systemTrayProtectionPrefix = "";
               
          this.systemTrayProtectionPrefix = systemTrayProtectionPrefix;
          
          this.userProtectionsOnly = true; // forces protections to be TG_HEAL_BY_USER
          
          //this.undoKeys = {}; // index of keys to {_Undo}
          //this.undoList = []; // array of undos

          //make sure we have merged all protections down to the current user
          // at least once this session
          if (_haveMergedProtections == null) {
            this.CopySystemUserLatestProtections();
            _haveMergedProtections = true;
          }
          
          this.EnumerateCatalog();
          this.EnumarateUndoLog();
        }
        catch(ex)
        {
        }
      },

      /**
      *  @name HaveProtection 
      *  @memberOf $ss.agentcore.dna.protectrestore
      *  @function
      *  @description  Returns Boolean result based on if the specific protection title is protected or not. 
      *  @param sProtectionName The title of the protection. 
      *  @returns True if the title is protected else false.
      *  @example
      *           var objDnaProbe = new $ss.agentcore.dna.protectrestore.Probe("");
      *           objDnaProbe.HaveProtection("Internet Explorer Favorites");
      */
      HaveProtection : function ( sProtectionName ) 
      {  
        return this.namesLowerCase[sProtectionName.toLowerCase()] ? true : false;  
      },
      
      /**
      *  @name HaveProtectionByGuid
      *  @memberOf $ss.agentcore.dna.protectrestore
      *  @function
      *  @description  Returns Boolean result based on if the specific protection guid is protected or not. 
      *  @param sProtectionGuid The guid of the protection. 
      *  @returns True if the guid is protected else false.
      *  @example
      *           var objDnaProbe = new $ss.agentcore.dna.protectrestore.Probe("");
      *           objDnaProbe.HaveProtectionByGuid("d4c2111e-ed04-46c9-badf-41ac634f4340");
      */
      HaveProtectionByGuid : function ( sProtectionGuid ) 
      {  //NOENUMERATION
        return this.guids[sProtectionGuid] ? true : false;  
      },
      
      /**
      *  @name IsProtected
      *  @memberOf $ss.agentcore.dna.protectrestore
      *  @function
      *  @description  Returns Boolean result based on if the specific protection title is protected . 
      *  @param sProtectionName The title of the protection. 
      *  @returns True if the title is protected else false.
      *  @example
      *           var objDnaProbe = new $ss.agentcore.dna.protectrestore.Probe("");
      *           objDnaProbe.IsProtected("Internet Explorer Favorites");
      */
      IsProtected : function ( sProtectionName ) 
      {  
        try
        {
          //NOENUMERATION
          var myProtection = this.namesLowerCase[sProtectionName.toLowerCase()]; 
          if ( ! myProtection ) 
            return false; 
          if ( ! myProtection.versions.length ) 
            return false;
          for (var i=0; i<myProtection.versions.length; i++) 
          {
            if (!(myProtection.versions[i].options & TG_RUN_NOTFOUND))
            {
              return true;
            }
          }
        }
        catch(ex)
        {
        }
        return false; 
      },
      
      /**
      *  @name IsProtectedByGuid
      *  @memberOf $ss.agentcore.dna.protectrestore
      *  @function
      *  @description  Returns Boolean result based on if the specific protection title is protected . 
      *  @param sProtectionGuid The guid of the protection. 
      *  @returns True if the title is protected else false.
      *  @example
      *           var objDnaProbe = new $ss.agentcore.dna.protectrestore.Probe("");
      *           objDnaProbe.IsProtectedByGuid("d4c2111e-ed04-46c9-badf-41ac634f4340");
      */
      IsProtectedByGuid : function ( sProtectionGuid ) 
      { 
        try
        { 
          //NOENUMERATION
          var myProtection = this.guids[sProtectionGuid];
          if ( ! myProtection ) 
            return false; 
          if ( ! myProtection.versions.length ) 
            return false;
          for (var i=0; i<myProtection.versions.length; i++) 
          {
            if (!(myProtection.versions[i].options & TG_RUN_NOTFOUND))
            {
              return true;
            }
          }
        }
        catch(ex)
        {
        }
        return false; 
      },
      
      /**
      *  @name GetProtection
      *  @memberOf $ss.agentcore.dna.protectrestore
      *  @function
      *  @description  Returns protected object based on title of the protection . 
      *  @param sProtectionName The title of the protection. 
      *  @returns The protected object if the title is protected else a false.
      *  @example
      *           var objDnaProbe = new $ss.agentcore.dna.protectrestore.Probe("");
      *           objDnaProbe.GetProtection("Internet Explorer Favorites ");
      */
      GetProtection : function ( sProtectionName )
      { 
        try
        {
          //NOENUMERATION
          if (this.HaveProtection(sProtectionName)) 
            return this.namesLowerCase[sProtectionName.toLowerCase()]; 
        }
        catch(ex)
        {
        }
        return false; 
      },
      
      /**
      *  @name GetProtectionByGuid
      *  @memberOf $ss.agentcore.dna.protectrestore
      *  @function
      *  @description  Returns protected object based on guid of the protection . 
      *  @param sProtectionGuid The guid of the protection. 
      *  @returns The protected object if the title is protected else a false.
      *  @example
      *           var objDnaProbe = new $ss.agentcore.dna.protectrestore.Probe("");
      *           objDnaProbe.GetProtectionByGuid("d4c2111e-ed04-46c9-badf-41ac634f4340");
      */
      GetProtectionByGuid : function ( sProtectionGuid )
      { 
        try
        {
          //NOENUMERATION
          if (this.HaveProtectionByGuid(sProtectionGuid)) 
            return this.guids[sProtectionGuid];
        }
        catch(ex)
        {
        }
        return false; 
      },
      
      EnumerateCatalog : function ( ) 
      { 
        try
        {
          //NOENUMERATION 
          var protectionList = _content.GetContentsByAnyType(["sprt_protection"]);
          this.list   = []; // array of {_Protection}
          
          var idx=0;
          for (idx=0; idx < protectionList.length; idx++) 
          {//
            var newProtection = new $ss.agentcore.dna.protectrestore.Protection(protectionList[idx],this,idx);
            if (this.userProtectionsOnly || (newProtection.healOptions & TG_HEAL_BY_USER)) {
              this.names [this.name] = newProtection; 
              this.guids [newProtection.guid] = newProtection; 
              this.namesLowerCase[newProtection.name.toLowerCase()] = newProtection; 
              this.list.push(newProtection); 
              
            }
          }
        }
        catch(ex)
        {
        } 
      },
      
      EnumarateUndoLog : function ( )
      {
        try
        {
          this.undoKeys = {}; // index of keys to {_Undo}
          this.undoList = []; // array of undos
          
          var currCtx = this.objUserCtx;  
          currCtx.SetIdentity(_config.GetContextValue("SdcContext:ProviderId"));
                
          var e, logEvent;
          e = new Enumerator(currCtx.UndoLogs);
           
          for (var RowCount=0; !e.atEnd(); e.moveNext(),RowCount++)
          { 
            logEvent = e.item();
            
            var msg = new String(logEvent.Msg);
            msg = msg.substr(0,9);
            
            if (msg.toLowerCase() != 'self heal') continue;
            
            var newUndoEntry = new $ss.agentcore.dna.protectrestore.Undo(this);    
            newUndoEntry.msg = logEvent.Msg;
            newUndoEntry.key = logEvent.key;
            newUndoEntry.dnaFile = new String(logEvent.DnaFile(logEvent.key));
            newUndoEntry.name = new String(logEvent.Msg.substring(10));

            // check if display name has "(appguid=".  If it does, save the app guid val
            // and strip appguid string from display name
            var appGuidStart = newUndoEntry.name.indexOf('(appguid=');
            if (appGuidStart > -1)
            {
              newUndoEntry.appGuid = newUndoEntry.name.substring(appGuidStart + 9, newUndoEntry.name.length - 1);
              newUndoEntry.name = newUndoEntry.name.substring(0, appGuidStart);
            }
            
            var ts = new Date(logEvent.TimeStamp);
            localeTime = ts.getTime() - ts.getTimezoneOffset()*60*1000;
            newUndoEntry.undoDate.setTime(localeTime);
            
            this.undoKeys[newUndoEntry.key] = newUndoEntry; 
            this.undoList.push(newUndoEntry);     
          }
        }
        catch(ex)
        {
        } 
      },
      
      /**
      *  @name Protect
      *  @memberOf $ss.agentcore.dna.protectrestore
      *  @function
      *  @param sProtectionGuid The title of the protection. 
      *  @returns true/false.
      *  @example
      *           var objDnaProbe = new $ss.agentcore.dna.protectrestore.Probe("");
      *           objDnaProbe.Protect("Internet Explorer Favorites");
      */
      Protect : function (sProtectionName) 
      { 
        try
        {
          //NOENUMERATION
          if (this.namesLowerCase[sProtectionName.toLowerCase()]) { 
            return this.namesLowerCase[sProtectionName.toLowerCase()].Protect();
          }
        }
        catch(ex)
        {
        }
        return false; 
      },
      
      /**
      *  @name ProtectAsync
      *  @memberOf $ss.agentcore.dna.protectrestore
      *  @function
      *  @param sProtectionGuid The title of the protection. 
      *  @returns true/false.
      *  @example
      *           var objDnaProbe = new $ss.agentcore.dna.protectrestore.Probe("");
      *           objDnaProbe.Protect("Internet Explorer Favorites");
      */
      ProtectAsync: function(sProtectionName)
      {
        try
        {
         //NOENUMERATION
          if (this.namesLowerCase[sProtectionName.toLowerCase()]) { 
            return this.namesLowerCase[sProtectionName.toLowerCase()].ProtectAsync();
          }
        }
        catch(ex)
        {
        }
        return false; 
      },
      
      /**
      *  @name ProtectAll
      *  @memberOf $ss.agentcore.dna.protectrestore
      *  @function
      *  @description  Protects all the settings . 
      *  @param None. 
      *  @returns none 
      *  @example
      *           var objDnaProbe = new $ss.agentcore.dna.protectrestore.Probe("");
      *           objDnaProbe.ProtectAll();
      */
      ProtectAll : function() 
      {  //NOENUMERATION

        // if we make this a synchronous call, the shell should be told that we are busy. 
        // and to not let this snapin unload itself?

        return _utils.RunCommand(this.GetProtectionCommand(),_constants.BCONT_RUNCMD_ASYNC); 

      },

      /**
      *  @name GetProtectionCommand
      *  @memberOf $ss.agentcore.dna.protectrestore
      *  @function
      *  @param None. 
      *  @returns Path for tgshell with parameters for protect which can be executed.
      *  @example
      *           var objDnaProbe = new $ss.agentcore.dna.protectrestore.Probe("");
      *           objDnaProbe.ProtectAll();
      */
      GetProtectionCommand : function ()  
      { 
        try
        {
          var tgShellPath,tgShellParms;

          tgShellPath   = '"'+ _config.GetProviderBinPath() + 'tgShell.exe"';
          tgShellParms  = ' /p '  + _config.GetContextValue('SdcContext:ProviderId'); 
          tgShellParms += ' /protect ';
          //zucktest tgShellParms += ' /nohelix ';

          return tgShellPath+tgShellParms;
        }
        catch(ex)
        {
        }
        return "";
      },

      BuildHashOfFileLines : function ( filestream ) 
      {
        var hash = new Object();
        try
        {
          var bomLine = true;
          while (!filestream.AtEndOfStream) {
            var lineText = filestream.ReadLine();
            if (bomLine) {
              bomLine = false;
            } else {
              hash[lineText] = lineText;
            }
          }
        }
        catch(ex)
        {
        }
        return hash;
      },

      /**
      *  @name CopySystemUserLatestProtections
      *  @memberOf $ss.agentcore.dna.protectrestore
      *  @function
      *  @description  If system is running in system mode or OS group is 9X then protections are stored at different location (from normal) which are copied. 
      *  @param None. 
      *  @returns True if protection are copied else false
      *  @example
      *           var objDnaProbe = new $ss.agentcore.dna.protectrestore.Probe("");
      *           objDnaProbe.CopySystemUserLatestProtections();
      */
      CopySystemUserLatestProtections : function ( ) 
      {
      
        try
        { 
          // for windows 98 and Me, the system account doesn't 'have' protections
          // in a broken state since there was a user logged in, normally, when the
          // network settings were protected.  We need to copy any available 
          // network settings we see on the machine to the local system account.

          var sCurrentUser = _config.GetContextValue("SdcContext:UserName");
          if ((sCurrentUser != 'SYSTEM') || (_utils.GetOSGroup() != '9x')) {
            return false;
          }
          
          var windowsFolder = _config.ExpandSysMacro("%WINDIR%"); //windows folder

          //User information is stored in different places on different operating systems:
          //  Win2k: \Documents and Settings\<User>\Local Settings\Application Data\SupportSoft\<Provider>\<User>\
          //  Windows 98 (w or w/o family logon): <Windows>\Local Settings\Application Data\SupportSoft\<Provider>\<User>\
          //  Windows ME (w or w/o family logon): <Windows>\Application Data\SupportSoft\<Provider>\<User>\
          var path = _file.BuildPath(windowsFolder, "\\Local Settings\\Application Data\\SupportSoft\\" + _config.GetContextValue('SdcContext:ProviderId'));
          if (_utils.GetOS()=="WINME") {
            path = _file.BuildPath(windowsFolder, "\\Application Data\\SupportSoft\\" + _configGetContextValue('SdcContext:ProviderId'));
          }
          var folder = _file.GetFolder(path);

          //if the folder exists, then we enumerate all of the users available, and copy the appropriate files into the System user's profile area
          if (folder != null) {
            var systemPath = _config.GetContextValue("SdcContext:DirUserServer");

            if (!_file.FolderExists(systemPath)) { _file.CreateDir(systemPath); }
            systemPath = _file.BuildPath(systemPath, "\\state\\");
            if (!_file.FolderExists(systemPath)) { _file.CreateDir(systemPath); }
            var fc = new Enumerator(folder.SubFolders);

            for (; !fc.atEnd(); fc.moveNext())
            {
              var userFolder = _file.GetFolder(fc.item().Path);
              //make sure we aren't trying to copy from system into system...
              if (new String(userFolder.Name).toLowerCase() == "system") { continue; }
          
              //alert(userFolder.Path);
              var userPath = _file.BuildPath(path, userFolder.Name + "\\state\\"); 
           
              if (_file.FolderExists(userPath)) {
              
                //copy protections
                var sourcePath = _file.BuildPath(userPath, "\\..\\data\\sprt_protection\\");
                var destPath = _file.BuildPath(systemPath, "\\..\\data\\sprt_protection\\");
                if (_file.FolderExists(sourcePath)) {
                  destPath = _file.BuildPath(systemPath, "\\..\\data\\");
                  if (!_file.FolderExists(destPath)) { _file.CreateDir(destPath); }
                  destPath = _file.BuildPath(systemPath, "\\..\\data\\sprt_protection\\");
                  if (!_file.FolderExists(destPath)) { _file.CreateDir(destPath); }
                  _file.CopyDir(sourcePath + "*", destPath);
                }
             
                //copy the vault
                sourcePath = _file.BuildPath(userPath, "\\backup\\");
                destPath = _file.BuildPath(systemPath, "\\backup\\");
                if (_file.FolderExists(sourcePath)) {
                  if (!_file.FolderExists(destPath)) { _file.CreateDir(destPath); }
                  _file.CopyDir(sourcePath + "*", destPath);
                }
         
                //copy DNA and update software.cat
                sourcePath = _file.BuildPath(userPath, "\\dnaback\\");
                destPath = _file.BuildPath(systemPath, "\\dnaback\\");
                if (_file.FolderExists(sourcePath)) {
                  if (!_file.FolderExists(destPath)) { _file.CreateDir(destPath); }
                  
                  sourcePath = _file.BuildPath(userPath, "\\dnaback\\*.dna");
                  destPath = _file.BuildPath(systemPath, "\\dnaback\\");
                  _file.CopyFile(sourcePath, destPath);
                  sourcePath = _file.BuildPath(userPath, "\\dnaback\\software.cat");
                  if (_file.FileExists(sourcePath)) {
                    destPath = _file.BuildPath(systemPath, "\\dnaback\\software.cat");
                    
                    var oldCatEntries = new Object();
                    var newCat = _file.OpenTextFile(sourcePath, 1); //ForReading
                    var newCatEntries = this.BuildHashOfFileLines(newCat);
                    newCat.Close(); 
                    newCat = null;
                    
                    var stream;
                    if (_file.FileExists(destPath)) {
                      var oldCat = _file.OpenTextFile(destPath, 1); //ForReading
                      oldcatEntries = this.BuildHashOfFileLines(oldCat);
                      oldCat.Close();
                      oldCat = null;
                      
                      stream = _file.OpenTextFile(destPath, 8, false, 0); //ForAppending, don't create, ASCII
                      for (var entry in newCatEntries) {
                        if (entry.length > 0 && oldCatEntries[entry] == null) {
                          stream.WriteLine(entry);  
                        }
                      }
                      stream.Close();
                      stream = null; 
                    } else {
                      _file.CopyFile(sourcePath, destPath);
                    }
                  }
                }
              }
            }
          }
          folder = null;
          shell = null;
        }
        catch(ex)
        {
        }
        return true; 
      },

      ToString : function ( ) 
      { //NOENUMERATION
        return '[object {Probe} Object]';
      }
  });

  $ss.agentcore.dna.protectrestore.Protection = Class.extend({},
  {
    // prototype methods
    init:function(contentItem, parentElement, index)
    {
      try
      {
        this.index  = index; 
        this.name   = contentItem.title;
        this.contentType   = contentItem.ctype;
        this.contentVersion = contentItem.version;
        this.guid   = contentItem.cid; 
        
        var oContentDom = $ss.agentcore.dal.content.GetContentDetailsAsXML("sprt_protection", this.guid, this.contentVersion);
        var _xml = $ss.agentcore.dal.xml;
        var healNode = _xml.GetNode("//field-set/field[@name='Heal']/value[@name='sccf_field_value_char']", "", oContentDom);
        //var modeNode = _xml.GetNode("//field-set/field[@name='Mode']/value[@name='sccf_field_value_char']", "", oContentDom);
        //var checkNode = _xml.GetNode("//field-set/field[@name='Check']/value[@name='sccf_field_value_char']", "", oContentDom);
        
        //this.mode   = modeNode ? modeNode.nodeTypedValue : ""
        this.healOptions  = healNode ? healNode.nodeTypedValue : ""
        //this.checkOptions = checkNode ? checkNode.nodeTypedValue : ""

        healNode = null;
        oContentDom = null;
        
        this.parentElement = parentElement; 

        this.LoadVersionList();  

        this.healedItems  = [];
        this.skippedItems = [];
        this.allItems     = [];
      }
      catch(ex)
      {
      }
    
    },

    BindTxHealEvents: function (ob)
    {
      var sLangCode = $ss.agentcore.utils.GetLanguage();
      var progressMessage=$ss.agentcore.dal.config.GetConfigValue_Ex("agentcore_messages",sLangCode, "progressMessage" ,"Repair is in progress. Click OK to Continue."); 
      alert(progressMessage);
      var that = this;
      function ob::OnProgressNext(cur,max,item,evt)
      {    
        try 
        { 
          return that.OnProgressNext(cur,max,item,evt);
        } 
        catch (e) 
        {
        }
      }
    },

    /****************** {_Protection}.loadVersionList() ******************/
    ReloadVersionList : function ( ) 
    {  
      delete this.versions;
      this.versions = null;
      
      this.LoadVersionList();
    },
    
    LoadVersionList : function ( ) 
    {  //NOENUMERATION
      var currCatalog = this.parentElement.objCatalog;
      currCatalog.SetIdentity(_config.GetContextValue("SdcContext:ProviderId"));
      currCatalog.Path = _config.GetContextValue("SdcContext:DirUserServer") + "state\\dnaback\\";

      var dnaItems = new Enumerator(currCatalog.DnaDescriptions); 

      this.versions   = [];
      this.maxVersion = 0;

      for (var dnaItem;!dnaItems.atEnd();dnaItems.moveNext()) 
      { 
        dnaItem = dnaItems.item();
        if (dnaItem.AppId && dnaItem.AppId==this.guid) 
        { 
          var ts = new Date(dnaItem.Checked);
          var localeTime = ts.getTime() - ts.getTimezoneOffset()*60*1000;
          var localizedDate = new Date(localeTime);

          this.versions.push({version:dnaItem.Version,
                              date:dnaItem.Checked,
                              localdate:localizedDate,
                              guid:dnaItem.AppId,
                              options:dnaItem.RunOptions});
      
          if (((dnaItem.RunOptions & (TG_RUN_ERROR | TG_RUN_NOTFOUND)) == 0) && dnaItem.Version > this.maxVersion ) 
            this.maxVersion = dnaItem.Version; 
        }
      }
      if(this.parentElement.iMaxProtections > -1)
      { 
        if (this.versions.length > this.parentElement.iMaxProtections*1) {
          this.versions.reverse(); // we want the LAST X protections
          this.versions.length = (this.parentElement.iMaxProtections*1);
          this.versions.reverse(); // return them in the logical date order 
        }
      }
     
      return this.versions;
    },

    /****************** {_Protection}.protect() **************************/

    Protect : function ( ) 
    { //NOENUMERATION
      
      var tgShellCommand = this.parentElement.GetProtectionCommand() + " /idlist " + this.guid;

      if (this.parentElement.systemTrayProtectionPrefix != "") 
        tgShellCommand  += ' /systraytext "' + this.parentElement.systemTrayProtectionPrefix + this.name + '"';

      var success = true;
      
      try {
        _utils.RunCommand(tgShellCommand , 0);
      }
      catch(e) { 
        success = false;
      }
      
      return success;
    },
    
    ProtectAsync : function () 
    { //NOENUMERATION
     
      var tgShellCommand = this.parentElement.GetProtectionCommand() + " /idlist " + this.guid;

      if (this.systemTrayProtectionPrefix != "") 
        tgShellCommand  += ' /systraytext "' + this.parentElement.systemTrayProtectionPrefix + this.name + '"';
      return _utils.RunCommand(tgShellCommand , 4);

    },

    /****************** {_Protection}.heal() **************************/

    /**
    *  @name Heal
    *  @memberOf $ss.agentcore.dna.protectrestore
    *  @function
    *  @description  Heals/Restores the specified version of protection.
    *  @param iVersion Version of the Protection nto restore
    *  @returns Number of healed items, -1 if error in Healing
    *  @example
    *           var objDnaProbe = new $ss.agentcore.dna.protectrestore.Probe("");
    *           oProt = objDnaProbe.GetProtection("Internet Explorer Favorites");
    *           oProt.Heal(1);
    */
    Heal : function ( iVersion ) 
    { //NOENUMERATION 

      if (typeof(iVersion)=='undefined')
        iVersion = this.maxVersion;
      
      //shellwindow.objTxHeal_OnProgressNext = this.OnProgressNext;
      //shellwindow.objTxHealItem            = this;
      
      if(this.parentElement.objTxHeal)
      {
        this.BindTxHealEvents(this.parentElement.objTxHeal);
      }

      var TX_GUIDVER           = 0x00000080; 
      this.healedItems.length  = 0; 
      //this.skippedItems.length = 0; 
      //this.allItems.length     = 0;

      this.parentElement.objTxHeal.SetIdentity(_config.GetContextValue("SdcContext:ProviderId")); 
      this.parentElement.objTxHeal.Preview   = false; 
      this.parentElement.objTxHeal.BuildList(TX_GUIDVER,this.guid+','+iVersion);
      this.parentElement.objTxHeal.TrackUndo = true; 
      try 
      {   
        this.parentElement.objTxHeal.Execute(); 
      } 
      catch (Probe_Protection_Heal_ERROR) 
      {    
        return -1; // failed
      } 
      
      return this.healedItems.length;    
    },

    /**
    *  @name Undo
    *  @memberOf $ss.agentcore.dna.protectrestore
    *  @function
    *  @description  Undo the recent restore which is done
    *  @param iVersion Version of the Protection nto restore
    *  @returns true if undo is done else false
    *  @example
    *           var objDnaProbe = new $ss.agentcore.dna.protectrestore.Probe("");
    *           oProt = objDnaProbe.GetProtection("Internet Explorer Favorites");
    *           ret = oProt.Undo("Internet Explorer Favorites");
    */
    Undo : function (key) 
    {  // undoes the LATEST protection
     
      var _latestHealLogForThisProtection;

      for (var i=0; i<this.parentElement.undoList.length; i++)
      {
        if (this.parentElement.undoList[i].name == this.name) { 
          _latestHealLogForThisProtection = _latestHealLogForThisProtection || this.parentElement.undoList[i];
          var _date1 = (_latestHealLogForThisProtection.undoDate).valueOf();
          var _date2 = (this.parentElement.undoList[i].undoDate).valueOf();
          if (_date2 > _date1) { 
            _latestHealLogForThisProtection=this.parentElement.undoList[i];
          }
        }
      }
      if (_latestHealLogForThisProtection)
        return _latestHealLogForThisProtection.Undo();

      return false; 
    },

    /****************** {_Protection}.onProgressNext() **************************/

    OnProgressNext : function (cur,max,item,evt) 
    { //NOENUMERATION 

      var tmpArray = [cur,max,item,evt]; 
      //this.allItems.push(tmpArray); 
      switch (evt) 
      { 
        case "Ignore": 
          //this.skippedItems.push(tmpArray);
         break; 

        case "Install" : case "Change" : 
          this.healedItems.push(tmpArray);
         break;
        
        default : 
      }  
    },

    ToString : function () 
    { //NOENUMERATION
      return '[object {Protection} Object]';
    }

  });

  $ss.agentcore.dna.protectrestore.Undo = Class.extend(
  {
    // prototype methods
    init:function(parentElement)
    {
      this.parentElement = parentElement;  
      this.name = "";
      this.undoDate = new Date();
      this.key = "";
      this.msg = "";  
      this.dnaFile = "";
      this.appGuid = "";
    },
    
    BindTxUndoEvents : function (ob)
    {
      var that = this;
      function ob::OnProgressNext(cur,max,item,evt)
      {   
        try { return that.OnProgressNext(cur,max,item,evt); } catch (e) { }
      }  
    },

    Undo : function ( )
    {     
      //shellwindow.objTxUndo_OnProgressNext = this.OnProgressNext;
      if(this.parentElement.objTxUndo)
      {
        this.BindTxUndoEvents(this.parentElement.objTxUndo);
      }
      if (typeof(this.force) != 'undefined')
      {
       this.parentElement.objTxUndo.Force = this.force;
      }
      this.parentElement.objTxUndo.SetIdentity(_config.GetContextValue("SdcContext:ProviderId")); 
      this.parentElement.objTxUndo.Preview = false;
      this.parentElement.objTxUndo.BuildList(8, this.dnaFile);
      this.parentElement.objTxUndo.Execute();  
      return true;
    }
    
  });

  var TG_RUN_NOTFOUND = 64;
  var TG_RUN_ERROR = 32;

  //  Protection healing options
  var TG_HEAL_BY_USER   = 1073741824;
  var TG_HEAL_AT_LAUNCH = 2147483648; // tbd, compare these masks when iterating the lists. 
  
  var _haveMergedProtections = null;
  var _config = $ss.agentcore.dal.config;
  var _file = $ss.agentcore.dal.file;
  var _utils = $ss.agentcore.utils;
  var _content = $ss.agentcore.dal.content;
  var _constants = $ss.agentcore.constants;
  
})()
