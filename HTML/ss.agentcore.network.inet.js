/** @namespace Provides all network related functionality*/
$ss.agentcore.network = $ss.agentcore.network || {};
/** @namespace Holds all the functionality related to connecting the net*/
$ss.agentcore.network.inet = $ss.agentcore.network.inet || {};

(function()
{
  $.extend($ss.agentcore.network.inet,
  {
    /**
    *  @name GetFirefoxProxySettings
    *  @memberOf $ss.agentcore.network.inet
    *  @function
    *  @description  Return hash of current firefox proxy settings
    *  @returns hash
    *  @example
    *           var _inet = $ss.agentcore.network.inet; 
    *           var currentProxySettings = _inet.GetFirefoxProxySettings();
    */
    GetFirefoxProxySettings:function()
    {
      var nFFProxyMode    = _utils.GetFirefoxUserPref("network.proxy.type");
      var sFFProxySvr     = _utils.GetFirefoxUserPref("network.proxy.http");
      var nFFProxySvrPort = _utils.GetFirefoxUserPref("network.proxy.http_port");
      var sFFProxyAutoUrl = _utils.GetFirefoxUserPref("network.proxy.autoconfig_url");
      var sFFProxyBypass  = _utils.GetFirefoxUserPref("network.proxy.no_proxies_on");

      var nTempProxyFlags    = PROXY_TYPE_DIRECT;
      var sTempProxySvr      = "";
      var sTempProxyBypass   = "";
      var sTempAutoConfigUrl = "";

      if (nFFProxyMode == 1 && sFFProxySvr != null && sFFProxySvr != "")   // static proxy
      {
        nTempProxyFlags = PROXY_TYPE_PROXY;
        if (nFFProxySvrPort == null || nFFProxySvrPort == 0) nFFProxySvrPort = 80;
        sTempProxySvr = sFFProxySvr + ":" + nFFProxySvrPort.toString();
      }
      else if (nFFProxyMode == 2 && sFFProxyAutoUrl != null && sFFProxyAutoUrl != "")  // auto config url
      {
        nTempProxyFlags = PROXY_TYPE_AUTO_PROXY_URL;
        sTempAutoConfigUrl = sFFProxyAutoUrl;
      }
      else if (nFFProxyMode == 4)
      {
        nTempProxyFlags = _constants.PROXY_TYPE_AUTO_DETECT;
      }

      // check if proxy enabled and bypass list specified
      if (nTempProxyFlags != PROXY_TYPE_DIRECT && sFFProxyBypass != null && sFFProxyBypass != "")
      {
        sTempProxyBypass = sFFProxyBypass.replace(",", ";");
      }

      var FFProxySettings = {
        flags:      nTempProxyFlags,
        server:     sTempProxySvr, 
        bypass:     sTempProxyBypass, 
        autoproxy:  sTempAutoConfigUrl 
      }; 

      return FFProxySettings;  
    },

    /**
    *  @name FirefoxHttpRequest
    *  @memberOf $ss.agentcore.network.inet
    *  @function
    *  @description  HttpRequest using Firefox proxy settings
    *  @param sUrl remote source file that needs to be checked
    *  @param sHTTPMethod int value for HTTP methods
    *         $ss.agentcore.constants.BCONT_REQUEST_GETFILE
    *         $ss.agentcore.constants.BCONT_REQUEST_GETFILEIFNEWER
    *         $ss.agentcore.constants.BCONT_REQUEST_PUTFILE
    *         $ss.agentcore.constants.BCONT_REQUEST_POSTFILE
    *         $ss.agentcore.constants.BCONT_REQUEST_EXISTS
    *  @param sHTTPLocalFile 
    *  @param nHTTPTimeout
    *  @returns true/false
    *  @example
    *         var _inet = $ss.agentcore.network.inet;          
    *         var ret = _inet.FirefoxHttpRequest("http://10.2.0.101", "GET", "C:\\client.test", "25000");
    */
    FirefoxHttpRequest:function(sURL, sHTTPMethod, sHTTPLocalFile, nHTTPTimeout)
    {
      // read settings from firefox and temporarily set them for IE
      var FFProxySettings = this.GetFirefoxProxySettings();

      // save current IE proxy settings before temporarily setting them to match Firefox
      var IEProxySettings = this.GetCurrentProxySettings();
      this.SetProxySettings(FFProxySettings);

      // test connectivity
      _utils.RefreshBrowserSettings();
      var bResult = _http.HttpRequest(sURL, sHTTPMethod, sHTTPLocalFile, nHTTPTimeout);

      // restore IE settings
      this.SetProxySettings(IEProxySettings);

      return bResult;
    },

    /**
    *  @name DefaultBrowser
    *  @memberOf $ss.agentcore.network.inet
    *  @function
    *  @description  get default browser
    *  @returns string
    *  @example
    *         var _inet = $ss.agentcore.network.inet;          
    *         var defBrowser = _inet.GetDefaultBrowser();
    */
    GetDefaultBrowser:function() {
      var sDefaultBrowser = INET_BROWSER_UNKNOWN;

      try
      {
        var sDefaultHttpHandler = _registry.GetRegValue("HKEY_CURRENT_USER", "Software\\Clients\\StartMenuInternet", "");
        if (sDefaultHttpHandler)
        {
          switch (sDefaultHttpHandler.toLowerCase()){
            case "firefox.exe" :
              sDefaultBrowser = _constants.INET_BROWSER_FIREFOX;
              break;
            case "iexplore.exe" :
              sDefaultBrowser = _constants.INET_BROWSER_IE;
              break;
            case "safari.exe" :
              sDefaultBrowser = _constants.INET_BROWSER_SAFARI;
              break;
            case "netscape.exe" :
              sDefaultBrowser = _constants.INET_BROWSER_NETSCAPE;
              break;
            case "opera.exe" :
              sDefaultBrowser = _constants.INET_BROWSER_OPERA;
              break;
            case "google chrome" :
              sDefaultBrowser = _constants.INET_BROWSER_CHROME;
              break;
          }
        }
        else
        {
          sDefaultHttpHandler = _registry.GetRegValue("HKEY_CLASSES_ROOT", "HTTP\\shell\\open\\command", "");
          if (sDefaultHttpHandler) {
            sDefaultHttpHandler = sDefaultHttpHandler.toLowerCase();
            if (sDefaultHttpHandler.indexOf("iexplore") > -1) sDefaultBrowser = _constants.INET_BROWSER_IE;
            else if (sDefaultHttpHandler.indexOf("firefox") > -1) sDefaultBrowser = _constants.INET_BROWSER_FIREFOX;
            else if (sDefaultHttpHandler.indexOf("netscape") > -1 || sDefaultHttpHandler.indexOf("netscp") > -1) sDefaultBrowser = INET_BROWSER_NETSCAPE;
            else sDefaultBrowser = INET_BROWSER_UNKNOWN;
          }
        }
      }
      catch (e)
      {
      }

      return sDefaultBrowser;
    },

    /**
    *  @name ConnectionTest
    *  @memberOf $ss.agentcore.network.inet
    *  @function
    *  @description  Test connectivity to Internet (or Walled Garden).  Lower level version of
                     BasicConnectionTest.
    *  @param sHost hostname to test against (no protocol, no path - default = "www.supportsoft.com")
    *  @param sHTTPMethod HTTP method to use (GET | EXITS - default = "GET")
    *  @param sHTTPLocalFile  local file to save file to
    *  @param bSwapIPForHostname  If true, use IP address for host (default = true)
    *  @param bCheckDnsSpoofing  If true, test for DNS spoofing (default = true)
    *  @param sSpoofDNSTestHost  Fake hostname to use in DNS spoof test (default = "www.google.com")
    *  @param sSocketTestAddress Address to do socket test if IE configured to autodetect proxy
    *  @param nDNSTimeout  timeout value in ms while doing DNS lookups (default = 5000)
    *  @param nHTTPTimeout  timeout value in ms while doing HTTP requests (default = 5000)
    *  @param nDNSIterations  number of times to do DNS lookup (default = 2)
    *  @param nHTPIterations  number of times to do HTTP request (default = 2)
    *  @param sBrowser  which browser proxy settings to use (default = INET_BROWSER_IE)
    *  @param bHttpAuthTest  if true,test proxy and server authentication.
    *  @returns true/false
    *  @example
    *         var _inet = $ss.agentcore.network.inet;          
    *         var ret = _inet.TestInternetConnectivity("http://www.supportsoft.com/", "GET", <br/>
    *                                    null, true, true, "http://www.supportsoft.com/", null, "15000", "25000", "2", "2", "default", false);
    */
    TestInternetConnectivity:function(sHost, sHTTPMethod, sHTTPLocalFile, bSwapIPForHostname,
                                   bCheckDnsSpoofing, sSpoofDNSTestHost, sSocketTestAddress, nDNSTimeout, 
                                   nHTTPTimeout, nDNSIterations, nHTTPIterations, sBrowser, bHttpAuthTest)
    {
      // validate params and set to default vals 
      var _serverbaseurl = _config.ExpandAllMacros("%SERVERBASEURL%");
      sHost              = _CheckParam(sHost, _serverbaseurl);
      sHTTPMethod        = _CheckParam(sHTTPMethod, "GET");
      sHTTPLocalFile     = _CheckParam(sHTTPLocalFile, "%TEMP%clientui.test");
      bSwapIPForHostname = _CheckParam(bSwapIPForHostname, true);
      bCheckDnsSpoofing  = _CheckParam(bCheckDnsSpoofing, true);
      sSpoofDNSTestHost  = "http://www.google.com"//_CheckParam(sSpoofDNSTestHost, _serverbaseurl);
      nDNSTimeout        = _CheckParam(nDNSTimeout, 5000);
      nHTTPTimeout       = _CheckParam(nHTTPTimeout, 5000);
      nDNSIterations     = _CheckParam(nDNSIterations, 2);
      nHTTPIterations    = _CheckParam(nHTTPIterations, 2);
      sBrowser           = _CheckParam(sBrowser, _constants.INET_BROWSER_IE);
      bHttpAuthTest      = _CheckParam(bHttpAuthTest, false);

      try 
      {
        _utils.Sleep(40);
        _utils.RefreshBrowserSettings();

        var bPerformDNS = bSwapIPForHostname || bCheckDnsSpoofing;
        var sDNSResult  = "";
        var bSuccess    = true;
        
        // check if we need to look up default browser
        if (sBrowser == INET_BROWSER_DEFAULT) sBrowser = this.GetDefaultBrowser();

        // Perform DNS Lookup
        if (bPerformDNS)
        {
		  
		      //flush dns before doing DNSlookups.
          _utils.FlushDNS();
          _utils.Sleep(50);
          //_DNSLookup will return "" instead of IP if we pass sHost value in this format "http://qaserver.supportsoft.com/" 
          //so removing http(s) and passing only qualified domain name.
          var dHost = sHost.replace(/^https?\:\/\//i,"").split("/")[0];
          sDNSResult = _DNSLookup(dHost, nDNSTimeout, nDNSIterations);
          if (sDNSResult == "") bSuccess = false; // All Iterations Exhausted, DNS Resolution Failed
        }

        // Check for DNS Spoofing
        /*
            some modems return unique modem IP when offline, all resolve back to modem.
            ping www.foo.com = 192.168.1.1
            ping www.bar.com = 192.168.1.2
            ping nothing.com = 192.168.1.3
            etc.
            So break down IP to subnet 192.168.1 and check to avoid this issue. (bSpoof2 below)
            !! Make sure you dont test DNS spoofing with IP/Hosts on same network or this will assume spoof !!
        */
        if (bCheckDnsSpoofing && bSuccess)
        {
          
          //flush dns before doing DNSlookups.
          //_utils.FlushDNS();
          _utils.Sleep(100);
          var sFakeDNSResult = _DNSLookup(sSpoofDNSTestHost, nDNSTimeout, nDNSIterations);
          var bSpoof2 = false;
          try {
            var sDNSResultSub     = sDNSResult.split(".").slice(0,3).join(".");
            var sFakeDNSResultSub = sFakeDNSResult.split(".").slice(0,3).join(".");
            if(sDNSResultSub == sFakeDNSResultSub) bSpoof2 = true;
          }catch(e) { bSpoof2 = false; }

          if (sFakeDNSResult == sDNSResult || bSpoof2)
          {
            bSuccess = false;
          }
        }

        if (bSuccess)
        {

          // If the client is configured to do socket tests instead of HTTP requests for machines w/ auto-detect proxy,
          // check the proxy settings.  If set to auto-detect, run socket test and if successful, return success result
          if (sSocketTestAddress != null && sSocketTestAddress != "") 
          {

            var currProxyFlags       = _utils.GetProxyOption("", _constants.INTERNET_PER_CONN_FLAGS);
            var bAutoDetectSettings  = ((currProxyFlags & _constants.PROXY_TYPE_AUTO_DETECT) != 0);

            if (bAutoDetectSettings) 
            {
              var addrArr = sSocketTestAddress.split(":");
              var host = addrArr[0];
              var port = 80;
              if (addrArr.length > 1) port = parseInt(addrArr[1]);
              _utils.Sleep(200);
              if (_netCheck.TestConnection(null, host, port, 5000)) 
              {
                bSuccess = true;
              } else {
                bSuccess = false;
              }
              return bSuccess;
            }
          }
        }
        
        if (bSuccess)
        {
          //Check if the proxy server is reachable
          var objPassCtl = _utils.CreateObject("sdcuser.tgpassctl");
          
          if (sBrowser == _constants.INET_BROWSER_FIREFOX)
          {
            if (this.IsFirefoxProxyEnabled())
            {
              var currentProxySettings = this.GetFirefoxProxySettings();
              var sProxyServer = currentProxySettings.server;
              _utils.Sleep(200);
              var bSocketOpen = objPassCtl.openSocket(sProxyServer);
              
              if (bSocketOpen)
              {
                objPassCtl.close();
              } else {
                bSuccess = false;          
              }
            }
          } else {
            if (this.IsProxyEnabled())
            {
              var currentProxySettings = this.GetCurrentProxySettings();
              var sProxyServer = currentProxySettings.server;
              _utils.Sleep(200);
              var bSocketOpen = objPassCtl.openSocket(sProxyServer);
              
              if (bSocketOpen)
              {
                objPassCtl.close();
              } else {
                bSuccess = false;          
              }
            } 
          }
        }

        if (bSuccess)
        {

          sHTTPLocalFile = _config.ExpandSysMacro(sHTTPLocalFile);

          //set cache to every page, and force a refresh of the autoconfigurl
          var cacheKey = "Software\\Microsoft\\Windows\\CurrentVersion\\Internet Settings"
          var oldCacheLevel = 4;   // defaults to 4 which means "Automatically"
          if (_registry.RegValueExists("HKCU", cacheKey, "SyncMode5"))
          {
            oldCacheLevel = _registry.GetRegValue("HKCU", cacheKey, "SyncMode5");         
          }

          _registry.SetRegValueByType("HKCU", cacheKey, "SyncMode5", 4, 3); 
          _utils.SetProxyOption("", _constants.INTERNET_PER_CONN_AUTOCONFIG_RELOAD_DELAY_MINS, 0);

          var sHomeRunURL = "";
          if ( bSwapIPForHostname){
            if(sHost.indexOf("http://") == 0) sHomeRunURL = "http://"+ sDNSResult;
            else if(sHost.indexOf("https://") == 0) sHomeRunURL = "https://"+ sDNSResult;
            else sHomeRunURL = sDNSResult;
          }
          else  sHomeRunURL = sHost;

          var xHTTPIterationResult = "";
          for (var i = 0; i < nHTTPIterations; i++) 
          {
            if (sBrowser == _constants.INET_BROWSER_FIREFOX)
            {
              xHTTPIterationResult = this.FirefoxHttpRequest(sHomeRunURL, sHTTPMethod, sHTTPLocalFile, nHTTPTimeout); 
            }
            else if (bHttpAuthTest)
            { 
              var proxyuser = "";
              var proxypwd = "";
              var returnCode;
     
              var sBase = _constants.REG_SPRT + "ProviderList\\" + _config.GetProviderID();
              var EncryptProxypName = _registry.GetRegValue(_constants.REG_TREE, sBase, "ProxyUsername");      
              if (EncryptProxypName != "")
                 proxyuser = _utils.DecryptHexToStr(EncryptProxypName);
             
              var EncryptProxypwd = _registry.GetRegValue(_constants.REG_TREE, sBase, "ProxyPassword"); 
              if (EncryptProxypwd != "")
                proxypwd = _utils.DecryptHexToStr(EncryptProxypwd);
        
              returnCode = _http.Authenticate(sHomeRunURL, 
                                                sHTTPLocalFile, 
                                                nHTTPTimeout,
                                                "", //username for web site
                                                "", //password for web site
                                                proxyuser,
                                                proxypwd
                                                ); 

              if (returnCode == HTTP_STATUS_OK)
                xHTTPIterationResult = true;
              else 
                xHTTPIterationResult = false;
              // TD : check the databags
              //ss_db_DataSet(DB_CONNTEST_RESULT,returnCode);
            }
            else {
              xHTTPIterationResult = _http.HttpRequest(sHomeRunURL, sHTTPMethod, sHTTPLocalFile, nHTTPTimeout); 
            }

            if (xHTTPIterationResult) 
            {
              break;
            }
            else 
            {
            }
          }

          bSuccess = xHTTPIterationResult;

          // clean up 
          _file.DeleteFile(sHTTPLocalFile);
          //restore old WinINet cache mode
          _registry.SetRegValueByType("HKCU", cacheKey, "SyncMode5", 4, oldCacheLevel); 
        }
      }
      catch (e) 
      {
        bSuccess = false;
      }
      return bSuccess;
    },

    /**
    *  @name BasicConnectionTest
    *  @memberOf $ss.agentcore.network.inet
    *  @function
    *  @description  Test connectivity to Internet (or Walled Garden).  Application level version
    *                of ConnectionTest.  Reads params from config files.
    *  @param bCheckCachedResult if true, use cached result if available
    *  @param bBroadcastResult if true, broadcast status to rest of app
    *  @param sBrowser which browser proxy settings to use.  If not specified, use IE
    *  @returns true/false
    *  @example
    *         var _inet = $ss.agentcore.network.inet;          
    *         var ret = _inet.BasicConnectionTest(false, true, "default");
    */
    BasicConnectionTest:function(bCheckCachedResult, bBroadcastResult, sBrowser)
    {
      if (bBroadcastResult) 
      { 
        // TD : check the eventing
        //ss_evt_SendByName(SS_EVT_CONNECT_STATUS_TESTING);
      }

      var bSuccess = false;

      if (bCheckCachedResult && _bConnectStatus != null)
      {
        bSuccess = _bConnectStatus;
      }
      else
      {
        //Prior to setting modem creds we cannot check hosts beyond the walled garden
        //Since this is BasicConnectionTest, lets test up to walled garden.
        var _serverbaseurl = _config.ExpandAllMacros("%SERVERBASEURL%");
        var sHost = _serverbaseurl;

        var sHTTPMethod        = _config.GetConfigValue("getconnect", "HomeRunCheckMethod", "GET");
        var SwapIPForHostname  = _config.GetConfigValue("getconnect", "SwapDNSInHomeRunURL", "true");
        var bSwapIPForHostname = (SwapIPForHostname.toLowerCase() != "false" && SwapIPForHostname != "1");
        var CheckDnsSpoofing   = _config.GetConfigValue("getconnect", "HomeRunFailsOnSpoofedDNS", "true");
        var bCheckDnsSpoofing  = (CheckDnsSpoofing.toLowerCase() != "false" && CheckDnsSpoofing != "1");
        var sSpoofDNSTestHost  = _serverbaseurl; 
        var sSocketTestAddress = _config.GetConfigValue("getconnect", "SocketConnect", null);
        var nDNSTimeout        = _config.GetConfigValue("getconnect", "DNSTimeout", 5000);
        var nHTTPTimeout       = _config.GetConfigValue("getconnect", "HTTPTimeout", 5000);
        var nDNSIterations     = _config.GetConfigValue("getconnect", "DNSIterations", 2);
        var nHTTPIterations    = _config.GetConfigValue("getconnect", "HTTPIterations", 2);
        var sProxyUseNTLM      = _config.GetConfigValue("getconnect", "ProxyUseNTLM", "true");
        var bHttpAuthTest      = sProxyUseNTLM.toLowerCase() == "false";

        if (sBrowser == null || typeof(sBrowser) == undefined || sBrowser == "") sBrowser = _constants.INET_BROWSER_IE;

        /* DNS Spoof checks subnets xxx.xxx.xxx so make sure sSpoofDNSTestHost and sHost are on different networks */
        bSuccess = this.TestInternetConnectivity(sHost, sHTTPMethod, null, bSwapIPForHostname,
                                         bCheckDnsSpoofing, sSpoofDNSTestHost, sSocketTestAddress, 
                                         nDNSTimeout, nHTTPTimeout, nDNSIterations, nHTTPIterations, 
                                         sBrowser, bHttpAuthTest);
      }

      _bConnectStatus = bSuccess;

      if (bBroadcastResult) 
      {
        // Check the eventing
        //var evtName = bSuccess ? SS_EVT_CONNECT_STATUS_UP : SS_EVT_CONNECT_STATUS_DOWN;
        //ss_evt_SendByName(evtName);
      }

      return bSuccess;
    },


    /**
    *  @name DNSLookup
    *  @memberOf $ss.agentcore.network.inet
    *  @function
    *  @description  Performs a DNS lookup on the given hostname.
    *  @param hostName host name to look up
    *  @returns true/false
    *  @example
    *         var _inet = $ss.agentcore.network.inet;          
    *         var ret = _inet.DNSLookup("www.supportsoft.com");
    */
    DNSLookup:function(hostName)
    {
      var nDNSTimeout        = _config.GetConfigValue("getconnect", "DNSTimeout", 5000);
      var nDNSIterations     = _config.GetConfigValue("getconnect", "DNSIterations", 2);

      return (_DNSLookup(hostName, nDNSTimeout, nDNSIterations));
    },
    
    /**
    *  @name GetConnectionStatus
    *  @memberOf $ss.agentcore.network.inet
    *  @function
    *  @description  Get The network status. If $ss.helper.IsConnectionAvailable,defined in layout, available it
    *                will return the n/w status else will do a connectivity check and give the
    *                result.  
    *  @param bCachedValue if true, use cached result if available
    *  @returns true/false
    *  @example
    *         var _inet = $ss.agentcore.network.inet;          
    *         var ret = _inet.GetConnectionStatus(true);
    */
    GetConnectionStatus:function(bCachedValue)
    {
      if($ss.helper && $ss.helper.IsConnectionAvailable) {
        return $ss.helper.IsConnectionAvailable();
      } else {
        bCachedValue = (bCachedValue == undefined) ? true : bCachedValue;
        return this.BasicConnectionTest(bCachedValue,false,"default");
      }

    },


    /**
    *  @name IsIEOffline
    *  @memberOf $ss.agentcore.network.inet
    *  @function
    *  @description  Returns if IE is in "Work Offline" mode.
    *  @returns true/false
    *  @example
    *         var _inet = $ss.agentcore.network.inet;          
    *         var ret = _inet.IsIEOffline();
    */
    IsIEOffline:function()
    {
      var offlineMode = _registry.GetRegValue("HKCU", 'Software\\Microsoft\\Windows\\CurrentVersion\\Internet Settings', 'GlobalUserOffline')
      return (offlineMode == "1"); 
    },

    /**
    *  @name DisableIEOffline
    *  @memberOf $ss.agentcore.network.inet
    *  @function
    *  @description  Turn off "Work Offline" in IE.
    *  @returns none
    *  @example
    *         var _inet = $ss.agentcore.network.inet;          
    *         var ret = _inet.DisableIEOffline();
    */
    DisableIEOffline:function()
    {
      _registry.SetRegValueByType('HKCU', 'Software\\Microsoft\\Windows\\CurrentVersion\\Internet Settings', 'GlobalUserOffline', 4, 0); 
      _utils.RefreshBrowserSettings();
    },

    /**
    *  @name IsProxyEnabled
    *  @memberOf $ss.agentcore.network.inet
    *  @function
    *  @description  Returns true if IE is configured to use proxy.
    *  @returns true/false
    *  @example
    *         var _inet = $ss.agentcore.network.inet;          
    *         var ret = _inet.IsProxyEnabled();
    */
    IsProxyEnabled:function()
    {
      var currProxyFlags    = _utils.GetProxyOption("", _constants.INTERNET_PER_CONN_FLAGS);
      var bProxyEnable      = _utils.HasMask(currProxyFlags, PROXY_TYPE_PROXY);
      var bProxyCfgScript = _utils.HasMask(currProxyFlags, PROXY_TYPE_AUTO_PROXY_URL);

      return (bProxyEnable || bProxyCfgScript);
    },

    /**
    *  @name DisableProxy
    *  @memberOf $ss.agentcore.network.inet
    *  @function
    *  @description  Turn off Proxy in IE.
    *  @returns none
    *  @example
    *         var _inet = $ss.agentcore.network.inet;          
    *         var ret = _inet.DisableProxy();
    */
    DisableProxy:function()
    {
      _utils.SetProxyOption("", _constants.INTERNET_PER_CONN_FLAGS, PROXY_TYPE_DIRECT);
    },

    /**
    *  @name GetCurrentProxySettings
    *  @memberOf $ss.agentcore.network.inet
    *  @function
    *  @description  Return hash of current proxy settings
    *  @returns hash (most likely to be used later by SetProxySettings)
    *  @example
    *         var _inet = $ss.agentcore.network.inet;          
    *         var ret = _inet.GetCurrentProxySettings();
    */
    GetCurrentProxySettings:function()
    {
      var currProxyFlags    = _utils.GetProxyOption("", _constants.INTERNET_PER_CONN_FLAGS);
      var currProxyServer   = _utils.GetProxyOption("", INTERNET_PER_CONN_PROXY_SERVER);
      var currProxyBypass   = _utils.GetProxyOption("", INTERNET_PER_CONN_PROXY_BYPASS);
      var currAutoConfigUrl = _utils.GetProxyOption("", INTERNET_PER_CONN_AUTOCONFIG_URL);

      var proxySettings = {
        flags:      currProxyFlags,
        server:     currProxyServer, 
        bypass:     currProxyBypass, 
        autoproxy:  currAutoConfigUrl 
      }; 

      return proxySettings;  
    },

    /**
    *  @name SetProxySettings
    *  @memberOf $ss.agentcore.network.inet
    *  @function
    *  @description  set proxy settings to current values in given hash
    *  @returns none
    *  @example
    *         var _inet = $ss.agentcore.network.inet;          
    *         var ret = _inet.SetProxySettings(proxyObj);
    */
    SetProxySettings:function(proxySettings)
    {
      _utils.SetProxyOption("", _constants.INTERNET_PER_CONN_FLAGS,          proxySettings.flags);
      _utils.SetProxyOption("", INTERNET_PER_CONN_PROXY_SERVER,   proxySettings.server);
      _utils.SetProxyOption("", INTERNET_PER_CONN_PROXY_BYPASS,   proxySettings.bypass);
      _utils.SetProxyOption("", INTERNET_PER_CONN_AUTOCONFIG_URL, proxySettings.autoproxy);
    },

    /**
    *  @name IsFirefoxProxyEnabled
    *  @memberOf $ss.agentcore.network.inet
    *  @function
    *  @description  Returns true if Firefox is configured to use proxy.
    *  @returns true/false
    *  @example
    *         var _inet = $ss.agentcore.network.inet;          
    *         var ret = _inet.IsFirefoxProxyEnabled();
    */
    IsFirefoxProxyEnabled:function() 
    {
      var bEnabled = false;
      
      var intFirefoxProxyType = _utils.GetFirefoxUserPref("network.proxy.type");
      if (intFirefoxProxyType == 1 || intFirefoxProxyType == 2 || intFirefoxProxyType == 4) 
        bEnabled = true;

      return(bEnabled);
    },

    /**
    *  @name DisableFirefoxProxy
    *  @memberOf $ss.agentcore.network.inet
    *  @function
    *  @description  Turn off Proxy in Firefox.
    *  @returns True/False
    *  @example
    *         var _inet = $ss.agentcore.network.inet;          
    *         var ret = _inet.DisableFirefoxProxy();
    */
    DisableFirefoxProxy:function() {
      var strProxyString = "network.proxy.type";
      
      return (this.RemoveFirefoxUserPref(strProxyString));
    },

    /**
    *  @name IsFirefoxOffline
    *  @memberOf $ss.agentcore.network.inet
    *  @function
    *  @description  Returns if Firefox is in "Work Offline" mode.
    *  @returns true/false
    *  @example
    *         var _inet = $ss.agentcore.network.inet;          
    *         var ret = _inet.IsFirefoxOffline();
    */
    IsFirefoxOffline:function() {
      var bIsOffline = false;
      
      var intFirefoxOfflineMode = _utils.GetFirefoxUserPref("browser.offline");
      if (intFirefoxOfflineMode === true || intFirefoxOfflineMode === 1 || intFirefoxOfflineMode === "true") 
        bIsOffline = true;

      return(bIsOffline);
    },

    /**
    *  @name DisableFirefoxOffline
    *  @memberOf $ss.agentcore.network.inet
    *  @function
    *  @description  Turn off "Work Offline" in Firefox.
    *  @returns none
    *  @example
    *         var _inet = $ss.agentcore.network.inet;          
    *         _inet.DisableFirefoxOffline();
    */
    DisableFirefoxOffline:function() {
      var strProxyString = "browser.offline";
      
      return (this.RemoveFirefoxUserPref(strProxyString));
    },

    /**
    *  @name RemoveFirefoxUserPref
    *  @memberOf $ss.agentcore.network.inet
    *  @function
    *  @description  Remove a pref setting from Firefox Prefs.js file
    *  @returns True/False
    *  @example
    *         var _inet = $ss.agentcore.network.inet;          
    *         var ret = _inet.RemoveFirefoxUserPref("browser.offline");
    */
    RemoveFirefoxUserPref:function(strUserPref) {
      try {
        var strFilePath = this.GetFirefoxPrefFile();
        if (!strFilePath) return (false);
        var strFileData = _file.ReadFile(strFilePath);

        var strSearchString = "user_pref(\"" + strUserPref + "\"";

        if (strFileData.indexOf(strSearchString) > 0) {
	        var intStartPos = strFileData.indexOf(strSearchString);
	        var intEndPos = strFileData.indexOf(";",intStartPos);

	        var strMatch = strFileData.substring(intStartPos, intEndPos + 3);
	        var strNewFileData = strFileData.replace(strMatch,"");
	        strNewFileData = strNewFileData.substring(0, strNewFileData.length-2);
    	    
	        var objTextFile = _file.CreateTextFile(strFilePath, true);
	        _file.WriteFile(objTextFile, strNewFileData);
    		    
	        objTextFile.close();
          return(true);
        }
      } catch (e) {
      }
      return(false);
    },

    /**
    *  @name SetFirefoxUserPref
    *  @memberOf $ss.agentcore.network.inet
    *  @function
    *  @description  Set a pref setting for Firefox Prefs.js file
    *  @param strUserPref  preference setting to be set
    *  @param strUserPrefValue  value to be set
    *  @param intDataType 
    *             1 - string
    *             2 - number
    *             3 - bool
    *  @returns True/False
    *  @example
    *         var _inet = $ss.agentcore.network.inet;          
    *         var ret = _inet.SetFirefoxUserPref("network.proxy.type", proxyObj, 2);
    */
    SetFirefoxUserPref:function(strUserPref, strUserPrefValue, intDataType) {
      try {
        var strFilePath = this.GetFirefoxPrefFile();
        if (!strFilePath) return (false);
        var strFileData = _file.ReadFile(strFilePath);

        var strSearchString = "user_pref(\"" + strUserPref + "\"";
        
        if (intDataType == 1)
          strUserPrefValue = "\"" + strUserPrefValue + "\"";
          
        var strSettingString= "user_pref(\"" + strUserPref + "\", " + strUserPrefValue + ");";

        if (strFileData.indexOf(strSearchString) > 0) { //setting already present
	      var intStartPos = strFileData.indexOf(strSearchString);
	      var intEndPos = strFileData.indexOf(";",intStartPos);

	      var strMatch = strFileData.substring(intStartPos, intEndPos + 1);
	      var strNewFileData = strFileData.replace(strMatch, strSettingString);
          strNewFileData = strNewFileData.substring(0, strNewFileData.length-2);
    	    
	      var objTextFile = _file.CreateTextFile(strFilePath, true);
	      _file.WriteFile(objTextFile, strNewFileData);
    		    
	      objTextFile.close();
          return(true);
        } else { 
          //setting previously not present, append a new line
          var objTextFile = _file.OpenTextFile(strFilePath, 8, false);
          _file.WriteFile(objTextFile, strSettingString);
          
          objTextFile.close();
          return(true);
        }
      } catch (e) {
      }
      return(false);
    },

    /**
    *  @name GetFirefoxPrefFile
    *  @memberOf $ss.agentcore.network.inet
    *  @function
    *  @description  Used to get the Firefox preference file path.
    *  @returns preference file path
    *  @example
    *         var _inet = $ss.agentcore.network.inet;          
    *         var ret = _inet.GetFirefoxPrefFile();
    */
    GetFirefoxPrefFile:function() {
      try {
        var strFirefoxINIFile = _config.ExpandSysMacro("%SF_OS_APPDATA%") + "\\Mozilla\\Firefox\\profiles.ini";
        var strProfileID;
        var intProfileCount = 0;
        var strDefaultProfile = "Profile0"; //If no profile is found with Default=1 then first profile
      
        do {
	      strProfileID = "Profile" + intProfileCount++;

	      if (_ini.GetIniValue(strFirefoxINIFile, strProfileID, "Default") == "1")
	        strDefaultProfile = strProfileID;

        } while (_ini.GetIniValue(strFirefoxINIFile,"Profile" + intProfileCount,"name") != null);
      
        var strPrefFilePath = _config.ExpandSysMacro("%SF_OS_APPDATA%") + "\\Mozilla\\Firefox\\" + _ini.GetIniValue(strFirefoxINIFile, strDefaultProfile, "path") + "\\Prefs.js";
        strPrefFilePath = strPrefFilePath.replace("\\\\","\\");
        strPrefFilePath = strPrefFilePath.replace("/","\\");
        return (strPrefFilePath);
      } catch (e) {
        return (false);
      }
    }

  });

  var _netCheck = $ss.agentcore.network.netcheck;
  var _utils = $ss.agentcore.utils;
  var _http = $ss.agentcore.dal.http;
  var _registry = $ss.agentcore.dal.registry;
  var _config = $ss.agentcore.dal.config;
  var _file = $ss.agentcore.dal.file;
  var _ini = $ss.agentcore.dal.ini;
  var _constants = $ss.agentcore.constants;
  
  // globals
  var _bConnectStatus = null;            // cached connect status

  /*******************************************************************************
  ** Proxy Constants
  ********************************************************************************/
  var INTERNET_PER_CONN_PROXY_SERVER                   =  2;
  var INTERNET_PER_CONN_PROXY_BYPASS                   =  3;
  var INTERNET_PER_CONN_AUTOCONFIG_URL                 =  4;
  var PROXY_TYPE_DIRECT                                =  0x00000001;   // direct to net
  var PROXY_TYPE_PROXY                                 =  0x00000002;   // via named proxy
  var PROXY_TYPE_AUTO_PROXY_URL                        =  0x00000004;   // autoproxy URL
  var HTTP_STATUS_PROXY_AUTH_REQ                       = 407; // proxy authentication required
  var HTTP_STATUS_OK                                   = 200; // request completed

  /*******************************************************************************
  ** Default Browser Constants
  ********************************************************************************/
  var INET_BROWSER_NETSCAPE = "Netscape";
  var INET_BROWSER_UNKNOWN = "Unknown";
  var INET_BROWSER_DEFAULT = "Default";

  function _CheckParam(paramVal, defaultVal)
  {
    if (paramVal == null || typeof(paramVal) == undefined) paramVal = defaultVal;
    return paramVal
  }

  function _DNSLookup(hostName, nDNSTimeout, nDNSIterations)
  {
    var sDNSResult  = "";

    for (var i=0; i < nDNSIterations; i++) 
    { 
      var xDNSIterationResult = _netCheck.ResolveHostname(null, hostName, nDNSTimeout); 
      if (xDNSIterationResult != "") 
      {
        sDNSResult = xDNSIterationResult;
        break;
      }
    }
    return sDNSResult;
  }

})()










