﻿/** @namespace Holds all Configuration related functionality*/
$ss.agentcore.dal.config = $ss.agentcore.dal.config || {};

(function() {
  $.extend($ss.agentcore.dal.config,
  {

    /**
    *  @name Initialize
    *  @memberOf $ss.agentcore.dal.config
    *  @function
    *  @description  Loads and intializes the configuration xml. This function has to be called <br/>
    *                first by passing config file with path.
    *  @param sConfigFile  Config file with path
    *  @param oQS  Optional,Contains the querystring object
    *  @returns {Dom Document} returns the dom of the config file
    *  @example
    *       var objConfig = $ss.agentcore.dal.config;
    *       var domConfig = objConfig.Initialize("c:\\Program Files\\W2K3S-W13\\agent\\clientui_config.xml");
    */
    Initialize: function(sConfigFile, oQS) {
      _logger.info('Entered function: $ss.agentcore.dal.config.Initialize');
      var oDOM = null;
      try {
        if (oQS) {
          _oQueryString = oQS;
        }

        oDOM = this.GetConfigDOM(sConfigFile);
        _SetGlobals(oDOM);

      }
      catch (ex) {
        _exception.HandleException(ex, '$ss.agentcore.dal.config', 'Initialize', arguments);
      }
      return oDOM;
    },

    /**
    *  @name ParseMacros
    *  @memberOf $ss.agentcore.dal.config
    *  @function
    *  @description  Interpolates a string with well known macro values.
    *  @param sStr String to perform substition on
    *  @param bRefreshCache Boolean to re hash the macro cache before interpolation 
    *  @paran sSnapinName  Name of the snapin to look in for SnapinPath.
    *  @returns string  interpolated string or null if there is an error
    *  @example
    *         var objConfig = $ss.agentcore.dal.config; 
    *         var str = objConfig.ParseMacros("%OSNAME%");
    */
    ParseMacros: function(sStr, bRefreshCache, sSnapinName) {
      _logger.info('Entered function: $ss.agentcore.dal.config.ParseMacros');
      return _ParseMacros(sStr, bRefreshCache, sSnapinName);
    },
    
   /**
    *  @name GetConfigValue_Ex - Extended method for 
    *  @memberOf $ss.agentcore.dal.config
    *  @function
    *  @description  Get a config value from the config file
    *  @param section section
    *  @param langCode lang code
    *  @param key key
    *  @param defaultval default value
    *  @returns value value of the key based on language if found else defaultval if passed
    *  @example
    *          var objConfig = $ss.agentcore.dal.config;
    *          var str = objConfig.GetConfigValue_Ex(section_name,langCode,keyname,defalutvalue); 
    */

    GetConfigValue_Ex: function(section, langCode,key, defaultVal) { 
      _logger.info('Entered function: $ss.agentcore.dal.config.GetConfigValue_Ex');
      var value = defaultVal;
      
      try {
        var cfgDOM = _cfgDOM
        if (cfgDOM == null) return value;
        var oNode = null;
        var expr = "";
        
        expr = _GetXPath_Ex(_cfgDefault, section,langCode, key);
        //[MAC] Check Machine OS Windows/MAC
        if(!bMac)
          oNode = cfgDOM.selectSingleNode(expr);
        else 
          oNode = _xml.SelectSingleNode(cfgDOM.responseXML,expr);

        if (oNode) {
          value = _GetNodeValue(oNode, value);
        }  
      }
      catch(ex){
        _exception.HandleException(ex, '$ss.agentcore.dal.config', 'GetConfigValue_Ex', arguments);        
      }
      return value;
    },

    /**
    *  @name GetConfigValue
    *  @memberOf $ss.agentcore.dal.config
    *  @function
    *  @description  Get a config value from the config file
    *  @param section section
    *  @param key key
    *  @param defaultval default value
    *  @returns value value of the key if found else defaultval if passed
    *  @example
    *          var objConfig = $ss.agentcore.dal.config;
    *          var str = objConfig.GetConfigValue("skin","skinroot",""); 
    */
    GetConfigValue: function(section, key, defaultVal) { 
      _logger.info('Entered function: $ss.agentcore.dal.config.GetConfigValue');
      var value = defaultVal;
      //check if DOM is loaded
      try {
        var cfgDOM = _cfgDOM;
        if (cfgDOM == null) return value;
        var oNode = null;
        var expr = "";

        //reg lookup
        if (_cfgRegLookup) {
          value = _DoRegLookup(section, key, value);
          if ((typeof (value) != _constants.UNDEFINED) && (value != defaultVal))
            return value;
        }

        //config lookup
        expr = _GetXPath(_cfgDefault, section, key);

        //[MAC] Check Machine OS Windows/MAC
        if(!bMac)
           oNode = cfgDOM.selectSingleNode(expr);
        else
          oNode = _xml.SelectSingleNode(cfgDOM.responseXML,expr);

        if (oNode) {
          value = _GetNodeValue(oNode, value);
        }
            
        //Parse %SNAPINPATH%
        if ((typeof (value) != _constants.UNDEFINED) && (value.indexOf(_SNAPINPATH) != -1)) {
          expr = _GetXPath(_cfgDefault, section, _constants.SNAPINPATH_KEY);

          //[MAC] Check Machine OS Windows/MAC
          if(!bMac)
            oNode = cfgDOM.selectSingleNode(expr);
          else
            oNode = _xml.SelectSingleNode(cfgDOM.responseXML,expr);

          var sSnapinPath = _GetNodeValue(oNode, _SNAPINPATH);
          value = value.replace(_SNAPINPATH, sSnapinPath);
        }
      } catch (ex) {
        _exception.HandleException(ex, '$ss.agentcore.dal.config', 'GetConfigValue', arguments);
      }
      return value;
    },

    /**
    *  @name GetConfigLanguage
    *  @memberOf $ss.agentcore.dal.config
    *  @function
    *  @description  Returns the current language code.
    *  @returns Language
    *  @example
    *           var objConfig = $ss.agentcore.dal.config;
    *           var strLang = objConfig.GetConfigLanguage();
    */
    GetConfigLanguage: function() {
      _logger.info('Entered function: $ss.agentcore.dal.config.GetConfigLanguage');
      var sLangCode = this.GetConfigValue("global", "language", "") || this.GetConfigValue("global", "default", "en");
      return sLangCode;
    },

    /**
    *  @name GetConfigFromHKLM
    *  @memberOf $ss.agentcore.dal.config
    *  @function
    *  @description  Reads the configuration values from HKLM
    *  @param section the name of the section
    *  @param name reg key name
    *  @returns String or Boolean value of the config if found else undefined
    *  @example
    *         var objConfig = $ss.agentcore.dal.config; 
    *         var ret = objConfig.GetConfigFromHKLM("global", "CDPATH");
    */
    GetConfigFromHKLM: function(section, name) {
      _logger.info('Entered function: $ss.agentcore.dal.config.GetConfigFromHKLM');
      return _GetMachineValue(section, name);
    },

    /**
    *  @name GetRegRoot
    *  @memberOf $ss.agentcore.dal.config
    *  @function
    *  @description  Function provides the registry root to be used for product.<br/>
    *                Registry hives are also made provider specific. This function uses<br/>
    *                values in configuration as is. It DOES NOT allow registry<br/>
    *                overwrites to config.
    *  @returns String   global value or config fetched value
    *  @example
    *
    *         var ret = objConfig.GetRegRoot();
    */
    GetRegRoot: function() {
      _logger.info('Entered function: $ss.agentcore.dal.config.GetRegRoot');
      return _cfgRegRoot;
    },

    /**
    *  @name GetAttribute
    *  @memberOf $ss.agentcore.dal.config
    *  @function
    *  @description  Get an attribute value from the config file
    *  @param config section
    *  @param key name
    *  @param attribute attribute
    *  @param defaultval default Value
    *  @returns value of the attribute if found else default value if passed
    *  @example
    *           var objConfig = $ss.agentcore.dal.config;
    *           var attr = objConfig.GetAttribute("email","email_domains","type");
    */
    GetAttribute: function(section, key, attribute, defaultVal) {
      _logger.info('Entered function: $ss.agentcore.dal.config.GetAttribute');
      var value = defaultVal;
      //check if DOM is loaded
      try {
        var cfgDOM = _cfgDOM;
        if (cfgDOM == null) return value;
        var oNode = null;
        var expr = "";

        //reg lookup
        if (_cfgRegLookup) {
          value = _DoRegLookup(section, key, value);
          if ((typeof (value) != _constants.UNDEFINED) && (value != defaultVal))
            return value;
        }

        //config lookup
        expr = _GetXPath(_cfgDefault, section, key);

        //[MAC] Check Machine OS Windows/MAC
        if(!bMac)
          oNode = cfgDOM.selectSingleNode(expr);
        else
          oNode = _xml.SelectSingleNode(oNode,expr);

        if (oNode) {
          value = _GetAttributeValue(oNode, attribute, value);
        }

      } catch (ex) {
        _exception.HandleException(ex, '$ss.agentcore.dal.config', 'GetAttribute', arguments);
      }
      return value;
    },

    /**
    *  @name GetValues
    *  @memberOf $ss.agentcore.dal.config
    *  @function
    *  @description  Get array of values from config file. <br/>
    *  @param section section
    *  @param key key
    *  @param defaultval default value
    *  @returns array containing all node values if found else array<br/>
    *                 with default value
    *  @example
    *         var objConfig = $ss.agentcore.dal.config;
    *         var ret = objConfig.GetValues("email","pop3_servers");
    */
    GetValues: function(section, key, defaultVal) {
      _logger.info('Entered function: $ss.agentcore.dal.config.GetValues');
      var arrReturn = [];
      var sValue = (typeof (defaultVal) == _constants.UNDEFINED) ? "" : defaultVal;
      var bUseDefault = (typeof (defaultVal) != _constants.UNDEFINED) ? true : false;

      try {
        var cfgDOM = _cfgDOM;
        if (cfgDOM == null) return sValue;
        var oNode = null;
        var sXPathExpr = "";

        // config lookup
        sXPathExpr = _GetXPath(_cfgDefault, section, key);

        //[MAC] Check Machine OS Windows/MAC
        if(!bMac)
          oNode = cfgDOM.selectSingleNode(sXPathExpr);
        else
          oNode = _xml.SelectSingleNode(cfgDOM.responseXML, sXPathExpr);

        // cache sSnapinPath
        var sSnapinExpr, oSnapinNode, sSnapinPath;
        sSnapinExpr = _GetXPath(_cfgDefault, section, _constants.SNAPINPATH_KEY);

        //[MAC] Check Machine OS Windows/MAC
        if(!bMac)
          oSnapinNode = cfgDOM.selectSingleNode(sSnapinExpr);
        else
          oSnapinNode = _xml.SelectSingleNode(cfgDOM.responseXML, sSnapinExpr);

        sSnapinPath = _GetNodeValue(oSnapinNode, _SNAPINPATH);

        if (oNode) {
          if (oNode.hasChildNodes()) {
            var aChildren = oNode.childNodes;
            for (var i = 0; i < aChildren.length; i++) {
              if (aChildren[i].nodeType in _TextElementNodeTypes) {
                sValue = _GetNodeValue(aChildren[i], sValue);
                //Parse %SNAPINPATH%
                if ((typeof (sValue) != _constants.UNDEFINED) && (sValue.indexOf(_SNAPINPATH) != -1)) {
                  sValue = sValue.replace(_SNAPINPATH, sSnapinPath);
                }
                arrReturn.push(sValue);
              }
            }
          }
        } else {
          if (bUseDefault) {
            //Parse %SNAPINPATH% && other Macros
            if (sValue.indexOf(_SNAPINPATH) != -1) {
              sValue = sValue.replace(_SNAPINPATH, sSnapinPath);
              if (_cfgExpandMacros)
                sValue = ParseMacros(sValue);
            }
            arrReturn.push(sValue);
          }
        }
      } catch (ex) {
        _exception.HandleException(ex, '$ss.agentcore.dal.config', 'GetValues', arguments);
        if (bUseDefault) {
          //Parse %SNAPINPATH% && other Macros
          if (sValue.indexOf(_SNAPINPATH) != -1) {
            sValue = sValue.replace(_SNAPINPATH, sSnapinPath);
            if (_cfgExpandMacros)
              sValue = ParseMacros(sValue);
          }
          arrReturn.push(sValue);
        }
      }
      return arrReturn;
    },


    /**
    *  @name IsConfigEnabled
    *  @memberOf $ss.agentcore.dal.config
    *  @function
    *  @description  Get a boolean config value.  Undefined results are returned false.
    *  @param sConfigSectionName config Section name
    *  @param sConfigItemName <config name=$> item name
    *  @param bDefaultValue boolean default to use if no value was found.
    *  @returns true if enabled,else false
    *  @example
    *           var objConfig = $ss.agentcore.dal.config;
    *           var bRet = objConfig.IsConfigEnabled("global","reglookup")
    */
    IsConfigEnabled: function(sConfigSectionName, sConfigItemName, bDefaultValue) {
      _logger.info('Entered function: $ss.agentcore.dal.config.IsConfigEnabled');
      var bFeatureEnabled = false;
      try {
        var xConfigValue = this.GetConfigValue(sConfigSectionName, sConfigItemName, "");
        if (xConfigValue === "true") {
          bFeatureEnabled = true;
        } else if (xConfigValue === "false") {
          bFeatureEnabled = false;
        } else {
          if (typeof (bDefaultValue) !== 'undefined') {
            if (bDefaultValue === true || bDefaultValue === false) {
              bFeatureEnabled = bDefaultValue;
            }
          }
        }
      } catch (ex) {
        _exception.HandleException(ex, '$ss.agentcore.dal.config', 'IsConfigEnabled', arguments);
      }
      return bFeatureEnabled;
    },


    /**
    *  @name GetSectionKeyNames
    *  @memberOf $ss.agentcore.dal.config
    *  @function
    *  @description  Get a collection of <config> elements, within a <config-section> in the default config
    *  @param section section
    *  @returns collection of nodes, or null
    *  @example
    *         var objConfig = $ss.agentcore.dal.config;
    *         var ret = objConfig.GetSectionKeyNames("global");
    */
    GetSectionKeyNames: function(section) {
      _logger.info('Entered function: $ss.agentcore.dal.config.GetSectionKeyNames');
      var arrReturn = [];
      try {
        var cfgDOM = _cfgDOM;
        if (cfgDOM != null) {
          var arrNodes = null;
          var sXpathExpr = "";
          sXpathExpr = _GetXPath(_cfgDefault, section);
          sXpathExpr += "/config";

          //[MAC] Check Machine OS Windows/MAC
          if(!bMac)
            arrNodes = cfgDOM.selectNodes(sXpathExpr);
          else
            arrNodes = _xml.SelectNodes(cfgDOM,sXpathExpr);
          if (arrNodes && arrNodes.length) {
            for (var i = 0; i < arrNodes.length; i++) {
              //[MAC] Check Machine OS Windows/MAC
              if(!bMac)
                arrReturn.push(arrNodes[i].getAttribute("name"));
              else
               arrReturn.push(arrNodes[i].getAttributeNode("name").nodeValue);
            }
          }
        }
      } catch (ex) {
        _exception.HandleException(ex, '$ss.agentcore.dal.config', 'GetSectionKeyNames', arguments);
      }
      return arrReturn;
    },

    /**
    *  @name GetHitCount
    *  @memberOf $ss.agentcore.dal.config
    *  @function
    *  @description  Gets the hit count for the macros
    *  @returns string with the hit count for each macros 
    *  @example
    *          var objConfig = $ss.agentcore.dal.config;
    *          var ret = objConfig.GetHitCount();
    */
    GetHitCount: function() {
      _logger.info('Entered function: $ss.agentcore.dal.config.GetHitCount');
      var _cache = _cfgMacroCache.sort(function(a, b) { return b[2] - a[2] });

      var arrReport = [], sCnt;
      for (var i = 0; i < _cache.length; i++) {
        sCnt = new String(_cache[i][2]);
        while (sCnt.length < 4) sCnt += ' ';
        arrReport.push(sCnt + '  ...  ' + _cache[i][0].toString());
      }
      return arrReport.join('\n');
    },

    /**
    *  @name GetProviderID
    *  @memberOf $ss.agentcore.dal.config
    *  @function
    *  @description  This is the one uniform way of getting the provider. The entire <br/>
    *                application uses this way of getting a provider ID. This<br/>
    *                function uses configuration as is. It DOES NOT allow registry <br/>
    *                overwrites to config.
    *  @returns String  global or config fetched value
    *  @example
    *          var objConfig = $ss.agentcore.dal.config;
    *          var ret = objConfig.GetProviderID(); 
    */
    GetProviderID: function() {
      _logger.info('Entered function: $ss.agentcore.dal.config.GetProviderID');
      try {
        if (_cfgProviderID == null) {

          // check INI file.  Provider Id in INI file takes highest precedence
          var sIniProviderId = _ini.GetIniValue("", "PROVIDERINFO", "provider", "");
          if (sIniProviderId !== "") {
            _cfgProviderID = sIniProviderId;
          }
          else {
            // Check the Augmented Config to see if it specifies the provider.
            var expr = _GetXPath(_cfgDefault, "global", "provider");
            
            //[MAC] Check Machine OS Windows/MAC
            if(!bMac)
              oNode = oConfigDOM.selectSingleNode(expr);
            else
              oNode = _xml.SelectSingleNode(oConfigDOM.responseXML, expr);
            _cfgProviderID = _GetNodeValue(oNode, "_default");
          }
        }
      }
      catch (ex) {
        _exception.HandleException(ex, '$ss.agentcore.dal.config', 'GetProviderID', arguments);
      }
      return _cfgProviderID;
    },


    /**
    *  @name SetUserValue
    *  @memberOf $ss.agentcore.dal.config
    *  @function
    *  @description  sets the config value in HKCU based on the section/name pair.<br/>
    *                this function writes the value to the registry, as long as we<br/>
    *                have write privs.
    *  @param section required, the name of the section
    *  @param name required, the name of the preference element
    *  @param value required, the value to be written to the registry
    *  @param valuetype optional, the shorthand hint for which type of <br/>
    *                   registry key to write
    *  @returns true on success false on failure
    *  @example
    *           var objConfig = $ss.agentcore.dal.config;
    *           var ret = objConfig.SetUserValue("UserInfo","first_name","SupportSoft");
    */
    SetUserValue: function(section, name, value, valuetype) {
      _logger.info('Entered function: $ss.agentcore.dal.config.SetUserValue');
      try {
        if (typeof (valuetype) == _constants.UNDEFINED)
          valuetype = 1; // default as REG_SZ

        value = value.toString();

        var rootUserConfigKey = this.GetRegRoot() +
                                "\\users\\" + this.GetContextValue("SdcContext:UserName") +
                                "\\ss_config";

        return _registry.SetRegValueByType(_constants.REG_HKCU, rootUserConfigKey + "\\" + section, name, valuetype, value);
      } catch (ex) {
        _exception.HandleException(ex, '$ss.agentcore.dal.config', 'SetUserValue', arguments);
        return false;
      }
    },


    /**
    *  @name SetMachineValue
    *  @memberOf $ss.agentcore.dal.config
    *  @function
    *  @description  sets the value in HKLM based on the section/name pair.<br/>
    *                this function writes the value to the registry, as long as we have<br/>
    *                write privs.
    *  @param section the name of the section which contains the preference <br/>
    *                 defaults to the "global" section
    *  @param name required, the name of the preference element
    *  @param value required, the value to be written to the registry
    *  @param valuetype optional, the shorthand hint for which type of registry <br/>
    *                   key to write
    *  @returns true on success false on failure
    *  @example
    *
    *
    */
    SetMachineValue: function(section, name, value, valuetype) {
      _logger.info('Entered function: $ss.agentcore.dal.config.SetMachineValue');
      try {
        if (typeof (valuetype) == _constants.UNDEFINED)
          valuetype = 1; // default as REG_SZ

        value = value.toString();

        var rootMachineConfigKey = this.GetRegRoot() + "\\ss_config";

        return _registry.SetRegValueByType(_constants.REG_TREE, rootMachineConfigKey + "\\" + section, name, valuetype, value);
      } catch (ex) {
        _exception.HandleException(ex, '$ss.agentcore.dal.config', 'SetMachineValue', arguments);
        return false;
      }
    },

    /**
    *  @name GetProviderRootPath
    *  @memberOf $ss.agentcore.dal.config
    *  @function
    *  @description  Returns an absolute path to the root folder for the current <br/>
    *                provider.
    *  @returns Path of the root folder
    *  @example
    *         var objConfig = $ss.agentcore.dal.config;
    *         var ret = objConfig.GetProviderRootPath();
    */
    GetProviderRootPath: function() {
      _logger.info('Entered function: $ss.agentcore.dal.config.GetProviderRootPath');
      try {
        var providerID = this.GetProviderID();
        var regKey = "SOFTWARE\\SupportSoft\\ProviderList\\" + providerID + "\\InstallPaths";
        var ProgramRoot = _registry.GetRegValue(_constants.REG_TREE, regKey, "ProgramRoot");
        return ProgramRoot;
      } catch (ex) {
        _exception.HandleException(ex, '$ss.agentcore.dal.config', 'GetProviderRootPath', arguments);
      }
    },

    /**
    *  @name GetProviderBinPath
    *  @memberOf $ss.agentcore.dal.config
    *  @function
    *  @description  Returns absolute path to the Bin\\ directory for the current provider <br/>
    *               based on the InstallPaths key in the registry
    *  @returns Full path to the current provider "bin" directory.
    *  @example
    *         var objConfig = $ss.agentcore.dal.config;
    *         var ret = objConfig.GetProviderBinPath();
    */
    GetProviderBinPath: function() {
      _logger.info('Entered function: $ss.agentcore.dal.config.GetProviderBinPath');
      return this.GetProviderRootPath() + "bin\\";
    },

    /**
    *  @name GetContextValue
    *  @memberOf $ss.agentcore.dal.config
    *  @function
    *  @description  Get the context object
    *  @param sCtxVal Context value name to retrieve (in form section:key) <br/>
    *         Most common values:<br/>
    *           SdcContext:DirUserServer <br/>
    *           SdcContext:ProviderId<br/>
    *           SdcContext:UserName<br/>
    *         Other Values<br/>
    *           Paths:DirActions = %SERVERROOT%actions\<br/>
    *           Paths:DirBackup = %SERVERROOT%backup\<br/>
    *           Paths:DirBin = %SERVERROOT%bin\<br/>
    *           Paths:DirData = %SERVERROOT%prefs\<br/>
    *           Paths:DirDna = %SERVERROOT%dna\<br/>
    *           Paths:DirDnaBackup = %SERVERROOT%profiles\<br/>
    *           Paths:DirIssue = %SERVERROOT%issue\<br/>
    *           Paths:DirLogs = %SERVERROOT%profiles\GLADE2 2K3\glade\logs\<br/>
    *           Paths:DirProfiles = %SERVERROOT%profiles\GLADE2 2K3\<br/>
    *           Paths:DirPublish = %SERVERROOT%dna\<br/>
    *           Paths:DirUserProfile = %SERVERROOT%profiles\GLADE2 2K3\glade\<br/>
    *           Paths:DirVault = %SERVERROOT%vault\<br/>
    *           SdcContext:DirSysProfile = C:\Documents and Settings\All Users\Application Data\SupportSoft\providerSprtDell\[SYSTEM]\<br/>
    *           SdcContext:DirUserServer = C:\Documents and Settings\glade\Local Settings\Application Data\SupportSoft\providerSprtDell\glade\<br/>
    *           SdcContext:HKCU = 2147483647<br/>
    *           SdcContext:ProviderId = providersprtdell<br/>
    *           SdcContext:UserName = glade<br/>
    *  @returns string
    *  @example
    *
    *
    */
    GetContextValue: function(sCtxVal) {
      _logger.info('Entered function: $ss.agentcore.dal.config.GetContextValue');
      var obj = _GetContextObj();
      return obj[sCtxVal];
    },

    /**
    *  @name ExpandSysMacro
    *  @memberOf $ss.agentcore.dal.config
    *  @function
    *  @description  Expands the system macros
    *  @param sMacro string containing macros to be expanded<br/>
    *         A list of supported macros are<br/>
    *           %WINSYS%<br/>
    *           %TEMP%<br/>
    *           %ProgramFiles%<br/>
    *           %CommonFiles%<br/>
    *           %SF_DESKTOP%<br/>
    *           %SF_FAVORITES%<br/>
    *           %SF_FONTS%<br/>
    *           %SF_NETHOOD%<br/>
    *           %SF_PERSONAL%<br/>
    *           %SF_PROGRAMS%<br/>
    *           %SF_RECENT%<br/>
    *           %SF_SENDTO%<br/>
    *           %SF_STARTMENU%<br/>
    *           %SF_STARTUP%<br/>
    *           %SF_TEMPLATES%<br/>
    *           %SF_STARTMENU%<br/>
    *           %SF_COMMON_PROGRAMS%<br/>
    *           %SF_COMMON_DESKTOP%<br/>
    *           %SF_COMMON_STARTMENU%<br/>
    *           %SF_COMMON_STARTUP%<br/>
    *           %User%<br/>
    *           %HomeDrive%<br/>
    *           %ClientRoot%<br/>
    *           %SystemRoot%<br/>
    *           %SystemDrive%<br/>
    *           %SdcProfile%<br/>
    *  @returns expanded path
    *  @example
    *          var objConfig = $ss.agentcore.dal.config; 
    *          vat strExpanded = objConfig.ExpandSysMacro("%TEMP%client_ui_http_temp.htm"); 
    */
    ExpandSysMacro: function(sMacro) {
      _logger.info('Entered function: $ss.agentcore.dal.config.ExpandSysMacro');
      var rVal = "";
      try {
        //[MAC] Check Machine OS Windows/MAC
        if(!bMac)
          rVal = _objContainer.ExpandSysMacro(sMacro);
        else {
          //=======================================================
          //[MAC] Native Call
          var message = {'JSClassNameKey':'File','JSOperationNameKey':'ExpandSysMacro', 'JSArgumentsKey':[sMacro], 'JSISSyncMethodKey' : '1'}
          var result = jsBridge.execute(message);
          var parsedJSONObject = JSON.parse(result);
          rVal = parsedJSONObject.Data;
          //=======================================================
        }
      }
      catch (err) {
        _exception.HandleException(err, '$ss.agentcore.dal.config', 'ExpandSysMacro', arguments);
      }
      return rVal;
    },

    /**
    *  @name ExpandAllMacros()
    *  @memberOf $ss.agentcore.dal.config
    *  @function
    *  @description  Expands both System Macros and Config Macros
    *  @param sMacroStr string to parse
    *  @returns string
    *  @example
    *          var objConfig = $ss.agentcore.dal.config; 
    *          vat strExpanded = objConfig.ExpandAllMacros("%TEMP%%SystemDrive%"); 
    */
    ExpandAllMacros: function(sMacroStr) {
      _logger.info('Entered function: $ss.agentcore.dal.config.ExpandAllMacros');
      return this.ExpandSysMacro(this.ParseMacros(sMacroStr));
    },

    /**
    *  @name GetBcontMutexPrefix
    *  @memberOf $ss.agentcore.dal.config
    *  @function
    *  @description  Gets the bcont prefix used for the single instance mutex
    *  @returns return the mutex string
    *  @example
    *          var objConfig = $ss.agentcore.dal.config;
    *          var ret = objConfig.GetBcontMutexPrefix()
    */
    GetBcontMutexPrefix: function() {
      _logger.info('Entered function: $ss.agentcore.dal.config.GetBcontMutexPrefix');
      var mutexStr = _constants.BCONT_MUTEX_STRING + this.GetProviderID();
      return mutexStr.toUpperCase();
    },

    /**
    *  @name AugmentConfigs 
    *  @memberOf $ss.agentcore.dal.config
    *  @function
    *  @description  Augments the config files in the passed array to the current config dom<br/>
    *                This looks for configuration element in the config file, checks the same in the current <br/>
    *                dom,if found will append all the childern to this.Else will create a new configuration element.
    *  @param configFiles List of the config files to augment
    *  @returns none
    *  @example
    *          var objConfig = $ss.agentcore.dal.config;
    *          objConfig.AugmentConfigs(["e:\\per\\del\\configsection.xml","e:\\per\\del\\configsection2.xml"]); 
    */
    AugmentConfigs: function(configFiles) {
      _logger.info('Entered function: $ss.agentcore.dal.config.AugmentConfigs');

      var oAugDOM = null;
      for (var i = 0; i < configFiles.length; i++) {
        if (!this.GetConfigDOM()) {
          // initialize

          this.Initialize(configFiles[i], null, null);
        }
        else {
          oAugDOM = _xml.LoadXML(configFiles[i], false);

          if (oAugDOM != null) {
            // check for configuration node
            var oConfigNode = this.GetConfigNode(oAugDOM);

            // Augment the node
            this.AugmentConfig(oConfigNode);
          }
        }
      }
    },

    /**
    *  @name AugmentConfig 
    *  @memberOf $ss.agentcore.dal.config
    *  @function
    *  @description  Augments the dom passed to the current config dom<br/>
    *                This looks for configuration element in the dom, checks the same in the current <br/>
    *                dom,if found will append all the childern to this.Else will create a new configuration element.
    *  @param oDom New config dom which is to be augmented
    *  @returns none
    *  @example
    *         var objConfig = $ss.agentcore.dal.config;
    *         var xml = $ss.agentcore.dal.xml;
    *         var dom = xml.LoadXMLFromString(str);
    *         objConfig.AugmentConfig(dom);
    */
    AugmentConfig: function(oDom) {
      _logger.info('Entered function: $ss.agentcore.dal.config.AugmentConfig');

      if (!oDom) {
        return;
      }

      try {
        var configName;
        //[MAC] Check Machine OS Windows/MAC
        if(!bMac)
          configName = oDom.getAttribute("name");
        else
          configName = oDom.getAttributeNode("name").nodeValue;

        var configs = oDom.childNodes;

        //[MAC] Check Machine OS Windows/MAC
        if(!bMac)
          oExistingNode = _cfgDOM.selectSingleNode("/sprt/configuration[@name='" + configName + "']");
        else
          oExistingNode = _xml.SelectSingleNode(_cfgDOM.responseXML,"/sprt/configuration[@name='" + configName + "']");
        if (!oExistingNode) {
          _cfgDOM.documentElement.appendChild(oDom.cloneNode(true));
          return;
        }

        for (var item = 0; item < configs.length; item++) {
          oElement = configs[item];
          if (oElement.nodeType != 1) continue; //skip on incompatible nodes
          oOverrideNode = oElement.cloneNode(true);

          // check if there is an existing section
          //[MAC] Check Machine OS Windows/MAC
          if(!bMac)
            oExistingSection = _xml.GetNodes("//config-section[@name='" + oOverrideNode.getAttribute("name") + "']", "", oExistingNode);
          else
            oExistingSection = _xml.GetNodes("/sprt/configuration/config-section[@name='" + oOverrideNode.getAttributeNode("name").nodeValue + "']", "", _cfgDOM);

          if (oExistingSection && oExistingSection[0]) {
            oExistingSection[0].parentNode.removeChild(oExistingSection[0]);
          }

          oExistingNode.appendChild(oOverrideNode);
        }
      }
      catch (ex) {
        _exception.HandleException(ex, '$ss.agentcore.dal.config', 'AugmentConfig', arguments);
      }
    },

    /**
    *  @name GetConfigNode 
    *  @memberOf $ss.agentcore.dal.config
    *  @function
    *  @description  Returns the configuration node fom the passed dom
    *  @param oDom XML Dom element
    *  @returns The configuration node if found else null
    *  @example
    *           var objConfig = $ss.agentcore.dal.config;  
    *           var oConfigNode = objConfig.GetConfigNode(oDom);
    */
    GetConfigNode: function(oDom) {
      _logger.info('Entered function: $ss.agentcore.dal.config.GetConfigNode');
      var oConfigNode = null;
      
      oConfigNode = _xml.GetNodes("/sprt/configuration[@name='" + this.GetConfigToUse() + "']", "", oDom);

      if (oConfigNode && oConfigNode.length > 0) {
        return oConfigNode[0];
      }

      return null;
    },

    /**
    *  @name DefineCustomMacro
    *  @memberOf $ss.agentcore.dal.config
    *  @function
    *  @description  Allows custom macros to be defined which is used during interpolation
    *                of resources allowing <res>Hello %PRODUCT% World</res> to work.  
    *  @param macroName The name of the macro, like %NEWMACRO% or ##MACROWASHERE##
    *  @param macroValue The final value of the macro; the value is used untouched.
    *  @returns none
    *  @example
    *         var objConfig = $ss.agentcore.dal.config;  
    *         ObjConfig.DefineCustomMacro('%CUSTOM%',"custom");
    */
    DefineCustomMacro: function(macroName, macroValue) {
      _AddMacro(macroName, macroValue);
    },

    /**
    *  @name GetConfigDOM()
    *  @memberOf $ss.agentcore.dal.config
    *  @function
    *  @description  Load config file into global DOM object or if already loaded
    *                at the shell level then make a local copy
    *  @param sConfigFile config file to load
    *  @returns The Dom of the loaded config file or null in case of error
    *  @example
    *
    *
    */
    GetConfigDOM: function(sConfigFile) {
      var iMaxConfigLoadTries = 3;

      for (var iTryCount = 0; iTryCount < iMaxConfigLoadTries; iTryCount++) {
        try {
          if (_cfgDOM == null && sConfigFile) {
            _cfgDOM = _xml.LoadXML(sConfigFile, false);
          }
          else {
            return _cfgDOM;
          }
        }
        catch (ex) {
          _exception.HandleException(ex, '$ss.agentcore.dal.config', 'GetConfigDOM', arguments);
        }
      }

      return null; // !! returning null is extremely important here
    },

    /**
    *  @name GetConfigToUse
    *  @memberOf $ss.agentcore.dal.config
    *  @function
    *  @description  
    *  @param 
    *  @returns 
    *  @example
    *
    *
    */
    GetConfigToUse: function() {
      return _GetConfigToUse();
    },

    /**
    *  @name SetConfigToUse
    *  @memberOf $ss.agentcore.dal.config
    *  @function
    *  @description  
    *  @param sConfigToUse
    *  @returns 
    *  @example
    *
    *
    */
    SetConfigToUse: function(sConfigToUse) {
      if (sConfigToUse) {
        _cfgToUse = sConfigToUse;
      }
    },

    GetReportingNode: function(snapinName) {
      var expr = "";
      //config lookup
      expr = _GetXPath(_cfgDefault, "");
      expr += "/config-section[@name='reporting_" + snapinName + "' and @snapinname ='" + snapinName + "']";

      var oNodes = _GetNodes(expr);

      if (oNodes)
        return oNodes[0];

      return null;
    },

    GetSnapinViewNode: function(snapinName) {
      var expr = "";
      //config lookup
      expr = _GetXPath(_cfgDefault);
      expr += "/config-section[@name='snapin_view_" + snapinName + "' and @snapinname ='" + snapinName + "']";

      var oNodes = _GetNodes(expr);

      if (oNodes)
        return oNodes[0];

      return null;
    },

    GetContentViewNode: function(snapinName) {
      var expr = "";
      //config lookup
      expr = _GetXPath(_cfgDefault);
      expr += "/config-section[@name='content_view_" + snapinName + "' and @snapinname ='" + snapinName + "']";
      var oNodes = _GetNodes(expr);

      if (oNodes)
        return oNodes[0];

      return null;
    },
    /**
    *  @name GetUserValue
    *  @memberOf $ss.agentcore.dal.config
    *  @function
    *  @description  retrieves the config value stored in HKCU for the given section/name
    *                values of "true" and "false" are converted to their boolean representation
    *  @param section 
    *          name
    *  @returns 
    *  @example
    *
    *
    */

    GetUserValue: function(section, name) {
      return _GetUserValue(section, name);
    }
  });

  var _objContainer;
  //[MAC] Check Machine OS Windows/MAC
  if(!bMac)
    _objContainer = $ss.agentcore.utils.activex.GetObjInstance();

  var _xml = $ss.agentcore.dal.xml;
  var _utils = $ss.agentcore.utils;
  var _registry = $ss.agentcore.dal.registry;
  var _logger = $ss.agentcore.log.GetDefaultLogger("$ss.agentcore.dal.config");
  var _exception = $ss.agentcore.exceptions;
  var _oQueryString = null;
  var _constants = $ss.agentcore.constants;
  var _config = $ss.agentcore.dal.config;
  var _aContext = null;
  var _ini = $ss.agentcore.dal.ini;

  var _cfgDOM = null;   // Dom representing config file data
  var _cfgConfigFile = "";     // Current DOM file location.
  var _cfgToUse = "";     // Tracks the value globally as it is used very often
  var _cfgDefault = "";     // Config default
  var _cfgProviderID = null;   // Tracks the provider name for application
  var _cfgServerRoot = null;   // Tracks the server root for the application
  var _cfgServerBaseUrl = null;   // Tracks the server base url for the application
  var _cfgLangCode = null;   // Tracks the language for the application
  var _cfgRegRoot = null;   // Tracks the registry root for application
  var _cfgRegLookup = null;   // Tracks whether the registry look up is enabled
  var _cfgRegLookupList = null;   // List of registry lookup keys
  var _cfgMacroCache = null;   // Array to store well known macro expansions
  var _cfgExpandMacros = false;  // Boolean to temporarily disable/enable macro expansions
  var _cfgMeasureMacros = false;  // Measure the Macro Cache's Performance for re-optimization.

  // Constants for optimizing ss_cfg_ParseMacros/ConfigAPI.
  var _SNAPINPATH = '%SNAPINPATH%';
  var _SNAPINPATHRx = new RegExp(_SNAPINPATH, 'gi');
  var _MacroNameList = [];
  var _reMacroNames = null;
  var _reSnapinPaths = [];
  var _reMacroHash = {};
  var _TextElementNodeTypes = { 1: "Element", 3: "Text", 4: "CDATA" };


  /*******************************************************************************
  **  Name:         ss_cfg_ToUse
  **
  **  Purpose:      Looks at which configuration is to be used by default
  **
  **  Parameter:    none
  **
  **  Return:       If finds the <config_to_use> else defaults to common
  *******************************************************************************/
  function _GetConfigToUse() {
    // lets first check global and return immediately
    if (_cfgToUse !== "") {
      return _cfgToUse;
    }

    var config = _cfgDefault;
    //check if DOM is loaded and if it has the config set
    try {
      var cfgDOM = _cfgDOM;
      if (cfgDOM != null) {
        if (_oQueryString && _oQueryString.configtouse != undefined && _ConfigExists(cfgDOM, _oQueryString.configtouse)) {
          config = _oQueryString.configtouse;
        }
        else {
          var oNode;
          //[MAC] Check Machine OS Windows/MAC
          if(!bMac)
            oNode = cfgDOM.selectSingleNode("//config_to_use");
          else
            oNode = _xml.SelectSingleNode(cfgDOM.responseXML,"//config_to_use");

          if (oNode != null && _ConfigExists(cfgDOM, oNode.text)) {
            config = oNode.text;
          }
        }
      }
    }
    catch (ex) {
      _exception.HandleException(ex, '$ss.agentcore.dal.config', '_GetConfigToUse', arguments);
      // do not log as goes recursive
    }
    // store back in global
    _cfgToUse = config;

    return _cfgToUse;
  }

  function _ConfigExists(oDOM, sCfgToCheck) {
    var oCfgSection;
    //[MAC] Check Machine OS Windows/MAC
    if(!bMac)
      oCfgSection = oDOM.selectSingleNode("//configuration[@name='" + sCfgToCheck + "']");
    else
       oCfgSection = _xml.SelectSingleNode(oDOM,"/sprt/configuration[@name='" + sCfgToCheck + "']");

    if (oCfgSection != undefined && oCfgSection != null) {
      return true;
    }
    return false;
  }

  /*******************************************************************************
  **  Name:         ss_cfg_pvt_AugmentDOM
  **
  **  Purpose:      Updates default configuration (config_default) in memory by
  **                augmenting custom configuration (config_to_use). This avoids
  **                multiple look ups while querying config values.
  **                Iterates thru each config section of custom and augments
  **                config section of default which has the same name.
  **
  **  Parameter:    oDOM - Config DOM to update
  **
  **  Return:       null. Updates given DOM.
  *******************************************************************************/
  function _AugmentDOM(oDOM) {
    //g_logger.Info("ss_cfg_pvt_AugmentDOM");
    try {
      var oBaseConfigSection = null;
      var oNewConfigSection = null;
      var sSectionName;
      //[MAC] Check Machine OS Windows/MAC
      if(!bMac)
        var arNewConfigSections = oDOM.selectNodes("//sprt/configuration[@name='" + _cfgToUse + "']/config-section");
      else
        var arNewConfigSections = _xml.SelectNodes(oDOM,"//sprt/configuration[@name='" + _cfgToUse + "']/config-section");

      for (var item = 0; item < arNewConfigSections.length; item++) {
        oNewConfigSection = arNewConfigSections[item];
        sSectionName = oNewConfigSection.getAttribute("name");
        if (sSectionName != undefined) {
          //[MAC] Check Machine OS Windows/MAC
          if(!bMac)
            oBaseConfigSection = oDOM.selectSingleNode("//sprt/configuration[@name='" + _cfgDefault + "']/config-section[@name='" + sSectionName + "']");
          else
            oBaseConfigSection = _xml.SelectSingleNode(oDOM.responseXML, "//sprt/configuration[@name='" + _cfgDefault + "']/config-section[@name='" + sSectionName + "']");
         
          if (oBaseConfigSection != undefined) {
            // if we have this config section in default config, do augment overwrite
            _xml.AugmentDoc(oBaseConfigSection, oNewConfigSection, "name", "./");
          }
          else {
            // if we don't have this section in base config, add it in.
            //[MAC] Check Machine OS Windows/MAC
            if(!bMac)
              var oBaseConfiguration = oDOM.selectSingleNode("//sprt/configuration[@name='" + _cfgDefault + "']");
            else
              var oBaseConfiguration = _xml.SelectSingleNode(oDOM.responseXML, "//sprt/configuration[@name='" + _cfgDefault + "']");
            oBaseConfiguration.appendChild(oNewConfigSection);
          }
        }
      }
    }
    catch (ex) {
      _exception.HandleException(ex, '$ss.agentcore.dal.config', '_AugmentDOM', arguments);
      throw (e);
    }
  }

  /*******************************************************************************
  **  Name:         ss_cfg_pvt_GetXPath
  **
  **  Purpose:      Constructs the XPath expression for sections and keys. Can
  **                handle nested level of config sections. Specify nesting by
  **                using a forward or backward slash. for e.g.
  **                ss_cfg_GetValue("requirements/os_me", "min_ram")
  **
  **  Parameter:    cfgToUse  Configuration to use
  **                section   Section (Eg: section1 or section1/section2)
  **                key       Key
  **
  **  Return:       config path
  *******************************************************************************/
  function _GetXPath(cfgToUse, section, key) {
    var expr = "", arSections;
    try {
      //[MAC] Check Machine OS Windows/MAC
      if(!bMac)
        expr = "//configuration[@name=\"" + cfgToUse + "\"]";
      else
        expr = "/sprt/configuration[@name=\"" + cfgToUse + "\"]";

      //Convert \ to /
      var rExp = /\\/g;
      section = section.replace(rExp, "\/");
      arSections = section.split("\/");


      for (var i = 0, j = arSections.length; i < j; i++) {
        if (arSections[i] != "") {
          expr += "/config-section[@name=\"" + arSections[i] + "\"]";
        }
      }
      if (key != "" && key != undefined) {
        expr += "/config[@name=\"" + key + "\"]";
      }
    } catch (ex) {
      _exception.HandleException(ex, '$ss.agentcore.dal.config', '_GetXPath', arguments);
    }
    return expr;
  }
  
  /*******************************************************************************
  **  Name:         _GetXPath_Ex - Extended method for GetXPath
  **
  **  Purpose:      Constructs the XPath expression for sections and keys based on language code.
  **
  **  Parameter:    cfgToUse  Configuration to use
  **                section   Section (Eg: section1 or section1/section2)
                    langCode  User preffered Language code
  **                key       Key
  **
  **  Return:       config path
  *******************************************************************************/

  function _GetXPath_Ex(cfgToUse, section, langCode, key) {
    var expr = "";
    try {
      //[MAC] Check Machine OS Windows/MAC
      if(!bMac)
        expr = "//configuration[@name=\"" + cfgToUse + "\"]";
      else
        expr = "/sprt/configuration[@name=\"" + cfgToUse + "\"]";

      //Convert \ to /
      var rExp = /\\/g;
      section = section.replace(rExp, "\/");
      
      expr += "/config-section[@name=\"" + section + "\"]";
      expr += "/lang[@code=\"" +langCode+ "\"]";
        
      if (key != "" && key != undefined) {
        expr += "/config[@name=\"" + key + "\"]";
      }
      
      
      
    } catch (ex) {
      _exception.HandleException(ex, '$ss.agentcore.dal.config', '_GetXPath_Ex', arguments);
    }
    return expr;
  }
 
  /*******************************************************************************
  **  Name:         ss_cfg_pvt_GetNodeValue
  **
  **  Purpose:      Parse node object and look for any c-data section. If any
  **                c-data section exists return c-data value or text value
  **
  **  Parameter:    oNode  Node object
  **                section   Section (Eg: section1 or section1/section2)
  **                key       Key
  **
  **  Return:       config path
  *******************************************************************************/
  function _GetNodeValue(oNode, defValue) {
    var retVal;

    if (oNode) {
      //[MAC] Check Machine OS Windows/MAC
      if(!bMac) {
        if (oNode.cdata != null && typeof (oNode.cdata) != _constants.UNDEFINED)
          retVal = oNode.cdata.toString();
        else if (oNode.text != null && typeof (oNode.text) != _constants.UNDEFINED)
          retVal = oNode.text.toString();
      }
      else {
      if (oNode.cdata != null && typeof (oNode.cdata) != _constants.UNDEFINED) {
        // retVal = oNode.cdata.toString();
              _logger.info('cdata: $ss.agentcore.dal.config.Initialize');

        retVal = oNode.childNodes[0].cdata.toString();
      }
      else {//if (oNode.text != null && typeof (oNode.text) != _constants.UNDEFINED) {
                // retVal = oNode.text.toString();
              if (oNode.nodeType in {3:true,1:true})
              {
                if (oNode.childNodes.length > 0) {
                var sText = oNode.childNodes[0].nodeValue;
                if (sText == null) {
                  sText = oNode.childNodes[0].textContent;
                }
                var sCData = oNode.childNodes[0].data;
                var sReturn = sText || sCData || "";
                retVal = sReturn;
              }
              else {
                retVal = oNode.textContent;
              }
            }
        // retVal = oNode.childNodes[0].nodeValue;

        if (retVal != null &&(retVal == "" || retVal == _constants.UNDEFINED)) {
          retVal = defValue;
        }
      }
    }
}
    else {
      retVal = defValue;
    }

    //Expand macros if any
    if (_cfgExpandMacros)
      return _ParseMacros(retVal);
    else
      return retVal;
  }

  function _GetNodes(expr) {
    //check if DOM is loaded
    try {
      var cfgDOM = _cfgDOM;
      if (cfgDOM == null)
        return null;

      var oNodes = null;

      //[MAC] Check Machine OS Windows/MAC
      if(!bMac)
        oNodes = cfgDOM.selectNodes(expr);
      else 
        oNodes = _xml.SelectNodes(cfgDOM.responseXML, expr);

      return oNodes;
    } catch (ex) {
      _exception.HandleException(ex, '$ss.agentcore.dal.config', '_GetNodes', arguments);
    }
    return null;

  }

  /****************************************************************************************
  ** Name:        ss_cfg_pvt_DefineMacros
  **
  ** Purpose:     interpolates a string with well known macro values.
  **
  ** Parameter:   bRefresh - Refresh macro expansions
  **
  ** Return:      None
  *****************************************************************************************/
  function _DefineMacros(bRefresh) {
    try {
      if (_cfgMacroCache != null && bRefresh != true) {
        return _cfgMacroCache;
      }

      // Cache some variables that might cause recursion.
      var bExpandMacros = _cfgExpandMacros;
      _cfgExpandMacros = false;

      // Save then clear the existing macros.
      var buMacroCache = _cfgMacroCache;
      if (_cfgMacroCache == null)
        _cfgMacroCache = [];

      /* 
      Measure Hit Frequency By Enabling _cfgMeasureMacros.
      Define the well-known macros.

          Total Hits For Each Macro After 10+ Pages Visited in SMA.

          130 ... /%COMMON%/ig
      105 ... /%ROOTPATH%/ig
      87  ... /%SKINROOT%/ig
      70  ... /%SKINNAME%/ig
      56  ... /%PRODUCT%/ig
      56  ... /%PRODUCTPATH%/ig
      37  ... /%IMAGEPATH%/ig
      9   ... /%SERVER%/ig

          System Tune-Up Scan

          121 ... /%ROOTPATH%/ig
      86  ... /%PRODUCTPATH%/ig
      68  ... /%SKINNAME%/ig
      44  ... /%COMMON%/ig
      15  ... /%IMAGEPATH%/ig
      3   ... /%SERVER%/ig
      2   ... /%USERNAME%/ig
      1   ... /%PRODUCT%/ig
      0   ... /%PROVIDERPATH%/ig
      0   ... /%SKINROOT%/ig
      0   ... /%LANGCODE%/ig
      0   ... /%PROVIDER%/ig
      0   ... /%CDPATH%/ig
      */

      //
      // Important Macros, Ordered by hit frequency.
      //
      _AddMacro('%PROVIDERPATH%', _config.GetProviderRootPath());
      _AddMacro('%PRODUCTPATH%', _config.GetConfigValue("global", "product_path", ""));
      _AddMacro('%ROOTPATH%', _utils.GetRootPath());
      _AddMacro('%SKINNAME%', _config.GetConfigValue("skin", "skinname", "default"));
      _AddMacro('%PRODUCT%', _config.GetConfigValue("global", "product", "SubAgent"));


      //
      // Define Newer, Lesser Used Macros Here...
      //
      _AddMacro('%SERVER%', _GetServerRoot(), "");     
      _AddMacro('%LANGCODE%', _cfgLangCode || $ss.agentcore.utils.GetLanguage());
      _AddMacro('%USERNAME%', _config.GetContextValue("SdcContext:UserName"));
      _AddMacro('%PROVIDER%', _config.GetProviderID());
      _AddMacro('%CDPATH%', _GetCDPath());
      _AddMacro('%SERVERBASEURL%', _GetServerBaseUrl());
      _AddMacro('%MSISOURCEDIR%', _ini.GetIniValue("", "PROVIDERINFO", "msisourcedir", "") || _utils.GetRootPath());
      _AddMacro('%PRODUCTCODE%', _ini.GetIniValue("", "PROVIDERINFO", "productcode", ""));
      _AddMacro('%BUSINESSID%', _ini.GetIniValue("", "PROVIDERINFO", "businessid", "") || _config.GetConfigValue("global", "business_id", ""));

      //
      // Custom macros will continue adding here...
      //
      // Macro to replace operating system name in the reporting strings
      _AddMacro('%OSNAME%', _GetOS());

      var contentPath = _ini.GetIniValue("", "SETUP", "contentPath", "")

      _AddMacro('%CONTENTPATH%', contentPath);
      _AddMacro('%SPRTPACKAGE%', contentPath + "\\sprt_package");
      _AddMacro('%SPRTLAYOUT%', contentPath + "\\sprt_layout");
      _AddMacro('%SPRTSNAPIN%', contentPath + "\\sprt_snapin");

      // Restore configuration variables.
      _cfgExpandMacros = bExpandMacros;

    }
    catch (ex) {
      _exception.HandleException(ex, '$ss.agentcore.dal.config', '_DefineMacros', arguments);
      //g_logger.Error("ss_cfg_pvt_DefineMacros()", ex.message);
      if (typeof (buMacroCache) != 'undefined') {
        // Restore from a busted routine?
        _cfgMacroCache = buMacroCache;
      }
    }
  }

  /****************************************************************************************
  ** Name:        ss_cfg_pvt_AddMacro
  **
  ** Purpose:     Manages adding or updating the macro list.
  **
  ** Parameter:   sMacroName - The name of the macro, like %NEWMACRO% or ##MACROWASHERE##
  **              sMacroValue - The final value of the macro; the value is used untouched.
  **
  ** Return:      An array of both the regular expression generated and the value passed in.
  *****************************************************************************************/
  function _AddMacro(sMacroName, sMacroValue) {
    try {
      _reMacroHash[sMacroName] = sMacroValue;
      var reMacroName = new RegExp(sMacroName, "gi");
      var sreMacroName = reMacroName.toString();
      for (var i = 0; i < _MacroNameList.length; i++) {
        if (sMacroName === _MacroNameList[i]) {
          for (var r = 0; r < _cfgMacroCache.length; r++) {
            if (_cfgMacroCache[r][0].toString() === sreMacroName) {
              _cfgMacroCache[r][1] = sMacroValue;
              _reMacroNames = new RegExp(_MacroNameList.join('|'), "gi");
              return _cfgMacroCache[r];
            }
          }
        }
      }
      var oEntry = [reMacroName, sMacroValue, 0];
      _MacroNameList.push(sMacroName);
      _cfgMacroCache.push(oEntry);
      _reMacroNames = new RegExp(_MacroNameList.join('|'), "gi");
      return oEntry;
    }
    catch (ex) {
      _exception.HandleException(ex, '$ss.agentcore.dal.config', '_AddMacro', arguments);
    }
    return false;
  }

  /****************************************************************************************
  ** Name:        ss_cfg_pvt_GetServerRoot
  **
  ** Purpose:     Returns the current server url.
  **
  ** Parameter:   none
  **
  ** Return:      None
  *****************************************************************************************/
  function _GetServerRoot() {
    if (_cfgServerRoot) {
      return _cfgServerRoot;
    }
    var sCfgValue = _config.GetConfigValue("global", "server", "");
    return sCfgValue;
  }

  /****************************************************************************************
  ** Name:        ss_cfg_pvt_GetServerBaseUrl
  **
  ** Purpose:     Returns the current server base url.
  **
  ** Parameter:   none
  **
  ** Return:      None
  *****************************************************************************************/
  function _GetServerBaseUrl() {
    if (_cfgServerBaseUrl) {
      return _cfgServerBaseUrl;
    }
    var sCfgValue = _config.GetConfigValue("global", "serverbaseurl", "");
    return sCfgValue;
  }

  /****************************************************************************************
  ** Name:        ss_cfg_pvt_GetMachineValue
  **
  ** Purpose:     retrieves the config value stored in HKLM for the given section/name
  **              values of "true" and "false" are converted to their boolean representation
  **
  ** Parameter:   section  the name of the section
  **              name     reg key name
  **
  ** Return:      String or Boolean value of the config
  *****************************************************************************************/
  function _GetMachineValue(section, name) {
    var rootMachineConfigKey = _config.GetRegRoot() + "\\ss_config";

    if (!_registry.RegValueExists(_constants.REG_TREE, rootMachineConfigKey + "\\" + section, name)) {
      return _constants.UNDEFINED;
    }

    var retVal = _registry.GetRegValue(_constants.REG_TREE, rootMachineConfigKey + "\\" + section, name);
    return retVal;
  }


  /****************************************************************************************
  ** Name:        ss_cfg_pvt_GetCDPath
  **
  ** Purpose:     Gets the path to CD if set by Self Updater (or others).
  **
  ** Parameter:   None
  **
  ** Return:      String with path to CD.  ss_utl_GetRootPath() before Self Updater copies local.
  *****************************************************************************************/
  function _GetCDPath() {
    var sCDPath = _GetMachineValue("global", "CDPATH");
    if (!sCDPath) {
      sCDPath = _utils.GetRootPath();
    }
    return sCDPath;
  }

  /****************************************************************************************
  ** Name:        ss_cfg_pvt_GetOS
  **
  ** Purpose:     Returns the current operating system.
  **
  ** Parameter:   none
  **
  ** Return:      string
  *****************************************************************************************/
  function _GetOS() {
    if (_utils.IsWinVST(false))
      return "Windows Vista";
    else if (_utils.IsWinXP(false))
      return "Windows XP";
  }

  function _ParseMacrosInStr(sStr) {
    if (_cfgMeasureMacros) {
      _ParseMacrosInStr = _ParseMacrosInStrWithCounts;
    } else {
      _ParseMacrosInStr = _ParseMacrosInStrWithoutCounts;
    }
    return _ParseMacrosInStr(sStr);
  }

  function _ParseMacrosInStrWithCounts(sStr) {
    // TODO -- Compare this against an indexOf scan for % and # macros
    //         versus the iterate a list and try for each one.
    var n = 0, i = 0, bBreak = false;
    while (bBreak === false && _MacroExists(sStr) && n++ < 4) {
      for (i = 0; i < _cfgMacroCache.length; i++) {
        if (sStr != (sStr = sStr.replace(
          _cfgMacroCache[i][0],   // regexp
          _cfgMacroCache[i][1]    // new string
        ))) {
          _cfgMacroCache[i][2]++; // Hit Count
        }
        if (sStr.indexOf('%') === -1 && sStr.indexOf('#') === -1) { //optimize high access part of the array
          bBreak = true;
          break;
        }
      }
    }
    return sStr;
  }

  function _ParseMacrosInStrWithoutCounts(sStr) {
    // TODO -- Compare this against an indexOf scan for % and # macros
    //         versus the iterate a list and try for each one.
    var n = 0, i = 0, bBreak = false;
    while (bBreak === false && _MacroExists(sStr) && n++ < 4) {
      for (i = 0; i < _cfgMacroCache.length; i++) {
        sStr = sStr.replace(
          _cfgMacroCache[i][0], // regexp
          _cfgMacroCache[i][1]  // new string
        );
        if (sStr.indexOf('%') === -1 && sStr.indexOf('#') === -1) { //optimize high access part of the array
          bBreak = true;
          break;
        }
      }
    }
    return sStr;
  }

  /*******************************************************************************
  **  Name:         ss_cfg_pvt_AugmentClientUIConfig
  **
  **  Purpose:      Augments the ClientUIConfig.xml (Synchronized from the server)
  **                over the current configuration DOM.
  **
  **                These settings currently take precidence over both
  **                config default and config to use.
  **
  **  Parameter:    oDOM - Config DOM to update
  **
  **  Return:       null. Updates given DOM.
  *******************************************************************************/
  function _AugmentClientUIConfig(oDOM) {
    try {
      if (oDOM) {
        var oBaseConfigSection = null;
        var oNewConfigSection = null;
        var sSectionName = "";
        var sClientUIConfig;
      //[MAC] Check Machine OS Windows/MAC
      if(!bMac)
        sClientUIConfig = _config.GetContextValue("SdcContext:DirUserServer") + "data\\clientuiconfig.xml";
      else
        sClientUIConfig = _config.GetContextValue("SdcContext:DirUserServer") + "/data\\clientuiconfig.xml";

        var oCUIDom = _xml.LoadXML(sClientUIConfig, false);
        if (oCUIDom) {

var arNewConfigSections;
      //[MAC] Check Machine OS Windows/MAC
      if(!bMac)
        arNewConfigSections = oCUIDom.selectNodes("//sprt/configuration/config-section");
      else
        arNewConfigSections = _xml.SelectNodes(oCUIDom.responseXML,"/sprt/configuration/config-section");

          for (var item = 0; item < arNewConfigSections.length; item++) {
            oNewConfigSection = arNewConfigSections[item];
            //[MAC] Check Machine OS Windows/MAC
            if(!bMac)
              sSectionName = oNewConfigSection.getAttribute("name"); 
            else
              sSectionName = oNewConfigSection.getAttributeNode("name").nodeValue;

            if (sSectionName != undefined) {
              //[MAC] Check Machine OS Windows/MAC
              if(!bMac)
                oBaseConfigSection = oDOM.selectSingleNode("//sprt/configuration[@name='" + _cfgDefault + "']/config-section[@name='" + sSectionName + "']");
              else
                oBaseConfigSection = _xml.SelectSingleNode(oDOM.responseXML,"/sprt/configuration[@name='" + _cfgDefault + "']/config-section[@name='" + sSectionName + "']");

              if (oBaseConfigSection) {
                _xml.AugmentDoc(oBaseConfigSection, oNewConfigSection, "name", "./");
              }
            }
          }
        }
      }
    } catch (ex) {
      _exception.HandleException(ex, '$ss.agentcore.dal.config', '_AugmentClientUIConfig', arguments);
    }
  }

  /*******************************************************************************
  **  Name:         ss_cfg_pvt_DoRegLookup
  **
  **  Purpose:      Do registry lookup for the given section/key. Keys are searched
  **                in the order specified in config key - reglookup_order. If
  **                none specified then returns default value.
  **
  **  Parameter:    section   Name of the section
  **                key       Key to search
  **
  **  Return:       string found in the registry or default value passed to the
  **                function.
  *******************************************************************************/
  function _DoRegLookup(section, key, defValue) {
    var MachineValue, UserValue, ar, RetValue;

    if (_cfgRegLookupList === "")
      return defValue;

    ar = _cfgRegLookupList.split(",");

    for (var i = 0; i < ar.length; i++) {
      if (ar[i] === "HKCU") {
        if ((UserValue = _GetUserValue(section, key)) != _constants.UNDEFINED)
          return UserValue;
      }
      else if (ar[i] === "HKLM") {
        if ((MachineValue = _GetMachineValue(section, key)) != _constants.UNDEFINED)
          return MachineValue;
      }
    }

    return defValue;
  }

  /****************************************************************************************
  ** Name:        ss_cfg_pvt_GetUserValue()
  **
  ** Purpose:     retrieves the config value stored in HKCU for the given section/name
  **              values of "true" and "false" are converted to their boolean representation
  **
  ** Parameter:   section  the name of the section
  **              name     reg key name
  **
  ** Return:      String or Boolean value of the config
  *****************************************************************************************/
  function _GetUserValue(section, name) {
    var rootUserConfigKey = _config.GetRegRoot() +
                            "\\users\\" + _config.GetContextValue("SdcContext:UserName") +
                            "\\ss_config";

    if (!_registry.RegValueExists(_constants.REG_HKCU, rootUserConfigKey + "\\" + section, name)) {
      return _constants.UNDEFINED;
    }

    var retVal = _registry.GetRegValue(_constants.REG_HKCU, rootUserConfigKey + "\\" + section, name);
    return retVal;
  }

  /*******************************************************************************
  **  Name:         ss_cfg_pvt_GetAttributeValue
  **
  **  Purpose:      Parse node object and look for any c-data section. If any
  **                c-data section exists return c-data value or text value
  **
  **  Parameter:    oNode  Node object
  **                attribute name
  **                default value
  **
  **  Return:       attribute value
  *******************************************************************************/
  function _GetAttributeValue(oNode, attribute, defValue) {
    var retVal;

    if (oNode) {
      if (oNode.getAttribute(attribute) != null && typeof (oNode.getAttribute(attribute)) != _constants.UNDEFINED)
      //[MAC] Check Machine OS Windows/MAC
      if(!bMac)
         retVal = oNode.getAttribute(attribute);
      else
        retVal = oNode.getAttributeNode(attribute).nodeValue;
    }
    else {
      retVal = defValue;
    }

    //Expand macros if any
    if (_cfgExpandMacros)
      return _ParseMacros(retVal);
    else
      return retVal;
  }

  /*******************************************************************************
  **  Name:         ss_cfg_pvt_SetGlobals
  **
  **  Purpose:      Sets all config globals
  **
  **  Parameter:    oConfigDOM  Dom object to query values
  **
  **  Return:       None
  *******************************************************************************/
  function _SetGlobals(oConfigDOM, sConfigToUse) {
    if (oConfigDOM) {
      var expr, oNode;

      //Config default
      if (_cfgDefault === "") {
        if (oConfigDOM != null) {
          var expr = "//config_default"

      //[MAC] Check Machine OS Windows/MAC
      if(!bMac) {
          var oNode = oConfigDOM.selectSingleNode(expr);
          if (oNode != null) {
            _cfgDefault = oNode.text;
          }
          else {
            _cfgDefault = _GetConfigToUse();
          }
      }
      else {
        var xmlDoc = oConfigDOM.responseXML;
        var oNode = _xml.SelectSingleNode(xmlDoc,expr);//xmlDoc.getElementsByTagName(expr)[0].childNodes[0];
        if (oNode != null) {
          _cfgDefault = oNode.childNodes[0].nodeValue;
        }
        else {
          _cfgDefault = _GetConfigToUse();
        }
      }
    }
  }


      //Config to use
      if (_cfgToUse === "") {
        _cfgToUse = _GetConfigToUse();
      }

      //Set the physical file location.
      try {
        _cfgConfigFile = oConfigDOM.url;
      }
      catch (ex) {
        _exception.HandleException(ex, '$ss.agentcore.dal.config', '_SetGlobals', arguments);
      }
      //Augment default/custom configurations
      if (_cfgDefault != _cfgToUse) {
        _AugmentDOM(oConfigDOM);
      }

      //Augment query string
      //Make sure this is called before any global such _cfgProviderID
      //is set. Caller may want to change values via QS.

      _AugmentQS(oConfigDOM, _oQueryString);

      //Set this right away, since ss_cfg_pvt_AugmentClientUIConfig() uses getProvider
      //and IE 7 strips off query string.
      if (_cfgProviderID == null) {
        // check INI file.  Provider Id in INI file takes highest precedence
        var sIniProviderId = _ini.GetIniValue("", "PROVIDERINFO", "provider", "");
        if (sIniProviderId !== "") {
          _cfgProviderID = sIniProviderId;
        }
        else {
          // Check the Augmented Config to see if it specifies the provider.
          expr = _GetXPath(_cfgDefault, "global", "provider");
          //[MAC] Check Machine OS Windows/MAC
          if(!bMac)
            oNode = oConfigDOM.selectSingleNode(expr);
          else
            oNode = _xml.SelectSingleNode(oConfigDOM.responseXML,expr);
          _cfgProviderID = _GetNodeValue(oNode, "_default");
        }
      }

      //
      // Override the configuration xml setting for the language, from the 
      // bcont.ini in our current environment.
      // 
      if (_cfgLangCode == null) {
        var sIniLang = _ini.GetIniValue("", "SETUP", "language", "");
        if (sIniLang !== "") {
          _cfgLangCode = sIniLang;
        }
      }

      //
      // Override the configuration xml setting for the server, from the 
      // bcont.ini in our current environment.
      // 
      if (_cfgServerRoot == null) {
        var sIniServer = _ini.GetIniValue("", "PROVIDERINFO", "server", "");
        if (sIniServer !== "" && sIniServer !== "[_SERVER_BASE_]") {
          _cfgServerRoot = sIniServer;
        }
      }

      //
      // Override the configuration xml setting for the server, from the 
      // bcont.ini in our current environment.
      // 
      if (_cfgServerBaseUrl == null) {
        var sIniServerBaseUrl = _ini.GetIniValue("", "PROVIDERINFO", "serverbaseurl", "");
        if (sIniServerBaseUrl !== "") { //prak do something // && sIniServerBaseUrl != "[_SERVER_BASE_]") {
          _cfgServerBaseUrl = sIniServerBaseUrl;
        }
      }


      //
      // The first call to ss_con_GetObjInstance caches an instance without Identity... set it once we know it...
      //
      if (_cfgProviderID) {
      //[MAC] Check Machine OS Windows/MAC
      if(!bMac)
        _objContainer.SetIdentity(_cfgProviderID);

        //=======================================================
        // [MAC] If required need to implement native call for below action
        // var message = {'JSClassNameKey':'WebView','JSOperationNameKey':'SetIdentity', 'JSArgumentsKey':[_cfgProviderID], 'JSISSyncMethodKey' : '1'}
        // jsBridge.execute(message)
        //=======================================================
      }
      else {
        //
        // IF this block is reached, the environment is not setup with a provider correctly.
        //
      }

      //
      // Can turn this on once we have the ProviderID set.
      //
      _cfgExpandMacros = true;

      // Add clientuiconfig.xml configuration
      _AugmentClientUIConfig(oConfigDOM);

      //Reg root - uses %PRODUCT% which is not expanded at this point
      //hence the call to it is direct
      if (_cfgRegRoot == null) {
        var sBase = _constants.REG_SPRT + "ProviderList\\" + _cfgProviderID;
        expr = _GetXPath(_cfgDefault, "global", "product");
      //[MAC] Check Machine OS Windows/MAC
      if(!bMac)
         oNode = oConfigDOM.selectSingleNode(expr);
      else
        oNode = _xml.SelectSingleNode(oConfigDOM.responseXML,expr);

        if (oNode) {
          var sRegRoot = _GetNodeValue(oNode, "_default");
          _cfgRegRoot = sBase + "\\" + sRegRoot;
        }
      }

      //Reg lookup
      if (_cfgRegLookup == null) {
        expr = _GetXPath(_cfgDefault, "global", "reglookup");
        //[MAC] Check Machine OS Windows/MAC
        if(!bMac)
          oNode = oConfigDOM.selectSingleNode(expr);
        else
          oNode = _xml.SelectSingleNode(oConfigDOM.responseXML,expr);

        _cfgRegLookup = _GetNodeValue(oNode, "true");
        _cfgRegLookup = (_cfgRegLookup == "true");
      }

      //Reg lookup list
      if (_cfgRegLookupList == null) {
        expr = _GetXPath(_cfgDefault, "global", "reglookup_order");
      //[MAC] Check Machine OS Windows/MAC
      if(!bMac)
        oNode = oConfigDOM.selectSingleNode(expr);
      else
        oNode = _xml.SelectSingleNode(oConfigDOM.responseXML,expr);
        _cfgRegLookupList = _GetNodeValue(oNode, "HKCU");
      }

      //Defining macros has to be the last call thus ensuring all
      //globals are set before we go into macro definitions
      _DefineMacros(true);

    }else {
      //[MAC] Check Machine OS Windows/MAC
      if(bMac) {
        var message = {'JSClassNameKey':'CommonUtility','JSOperationNameKey':'ConsoleLog', 'JSArgumentsKey':['Failed to load XML----------------------------------'], 'JSISSyncMethodKey' : '1'}
        var result = jsBridge.execute(message);
        var parsedJSONObject = JSON.parse(result);
      }
    }
  }

  /****************************************************************************************
  ** Name:        ss_cfg_pvt_MacroExists
  **
  ** Purpose:     Check if macro exists or not
  **
  ** Parameter:   None
  **
  ** Return:      None
  *****************************************************************************************/
  function _MacroExists(sStr) {
    return _reMacroNames && (sStr.match(_reMacroNames) != null);
  }

  /*******************************************************************************
  **  Name:         ss_cfg_pvt_AugmentQS
  **
  **  Purpose:      Update config DOM in memory by augmenting query string params.
  **                Reads thru querystring params and if param name has : then it
  **                is assumed as config section:key. The value of this param is
  **                updated in the corresponding config node. Also handles which
  configuration (common or custom) to use.
  **                Eg: <<page>>?global:disable_refresh=true  UPDATES
  **                    <config-section="global">
  **                      <config name="disable_refresh">false</config>
  **                    </config-section>
  **                    TO
  **                    <config-section="global">
  **                      <config name="disable_refresh">true</config>
  **                    </config-section>
  **
  **  Parameter:    oDOM - Config DOM to update
  **                oQS  - QueryString object to read.
  **
  **  Return:       null
  *******************************************************************************/
  function _AugmentQS(oDOM, oQS) {
    try {
      var oNode = null;
      if (oDOM != null) {
        for (var i in oQS) {
          if (i.indexOf(":") !== -1) {
            var ar = i.split(":");
            expr = _GetXPath(_cfgDefault, ar[0], ar[1]);
            //[MAC] Check Machine OS Windows/MAC
            if(!bMac)
              oNode = oDOM.selectSingleNode(expr);
            else
              oNode = _xml.SelectSingleNode(oDOM.responseXML,expr);
            if (oNode) oNode.text = oQS[i];
          }
        }
      }
    } catch (ex) {
      _exception.HandleException(ex, '$ss.agentcore.dal.config', '_AugmentQS', arguments);
    }
  }

  function _ParseMacros(sStr, bRefresh, sSnapinName) {
    try {
      if (typeof (sStr) != _constants.STRING)
        return sStr;

      _DefineMacros(bRefresh);

      //If this function is called directly to parse %SNAPINPATH%, look in current snapin context.
      if (typeof (sSnapinName) == _constants.STRING && sStr.indexOf(_SNAPINPATH) != -1) {
        var xName, xValue;
        if (_reSnapinPaths[sSnapinName] != undefined && !bRefresh) {
          xName = _reSnapinPaths[sSnapinName][0];
          xValue = _reSnapinPaths[sSnapinName][1];
        } else {
          xName = _SNAPINPATHRx; //new RegExp(_SNAPINPATH, 'gi');
          xValue = GetConfigValue(sSnapinName, _constants.SNAPINPATH_KEY, _SNAPINPATH);
          _reSnapinPaths[sSnapinName] = [xName, xValue];
        }
        if (xName && xValue) {
          sStr = sStr.replace(xName, xValue);
        }
      }

      sStr = _ParseMacrosInStr(sStr);

    } catch (ex) {
    }
    return sStr;
  }

  /*******************************************************************************
  ** Name:         ss_con_pvt_GetContextObj
  **
  ** Purpose:      Get the context object
  **
  ** Parameter:    none
  **
  ** Return:       array
  *******************************************************************************/
  function _GetContextObj() {
    if (_aContext != null)
      return _aContext;

    try {
      _aContext = new Object();

      //[MAC] Check Machine OS Windows/MAC
      if(!bMac) {
        var oCtx = _objContainer.getContext();
        var arr = (new VBArray(oCtx)).toArray();
      }
      else {
        //=======================================================
        //[MAC] Native Call
        var message = {'JSClassNameKey':'CommonUtility','JSOperationNameKey':'getContext', 'JSArgumentsKey':[], 'JSISSyncMethodKey' : '1'}
        var result = jsBridge.execute(message);
        var parsedJSONObject = JSON.parse(result);
        //=======================================================
        var arr = parsedJSONObject.Data;
        message = {'JSClassNameKey':'CommonUtility','JSOperationNameKey':'ConsoleLog', 'JSArgumentsKey':[arr], 'JSISSyncMethodKey' : '1'}
        jsBridge.execute(message);
      }
      for (var i1 = 0; i1 < arr.length; ++i1) {
        var tmp = arr[i1].split("=", 2);
        _aContext[tmp[0]] = tmp[1];
      }
    }
    catch (err) {
    }
    return _aContext;
  }

//[MAC] Get Machine OS type
var bMac = $.browser.safari;

//[MAC] JSBridge object used only for MAC
try {
  var jsBridge = window.JSBridge;
}
catch(e) {
}

})();