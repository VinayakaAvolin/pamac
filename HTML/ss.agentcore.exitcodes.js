/** @namespace Holds all functionality related to databags,basically used for storing snapin specific data*/
$ss.agentcore = $ss.agentcore || {};
$ss.agentcore.exitcodes = $ss.agentcore.exitcodes || {};

(function()
{
  $.extend($ss.agentcore.exitcodes,
  {
    /**
    *  @name GetExitCodeDetails 
    *  @memberOf $ss.agentcore.exitcodes
    *  @function 
    *  @description function to get the exit code description specific library type
    *  @param sLibType Name of the library for which exit code is needed
    *  @param sExitCode Exit code for which description is needed
    *  @param sLang optional Language code, by default it takes the default language code of DA
    *  @returns object containing Exit code, it's description and result type if found 
    *           else returns null
    *  @example
    *         var oExit = $ss.agentcore.exitcodes;
    *         oExit.GetExitCodeDetails("ASK_SA","1","en");
    */
    GetExitCodeDetails: function(sLibType, sExitCode, sLang) {
      var oExitRet = null;
      try {
        if(!sLang)
          sLang = _sLang;
        if(!_IsExitCodeLoaded(sLibType, sLang)) 
          _LoadExitCodes(sLibType, sLang);
        oExitRet = _GetErrorDesc(sLibType, sExitCode, sLang);
      } catch (e) {
        oExitRet = null;
      }
      if(!oExitRet) oExitRet = null;
      return oExitRet;
    },

    /**
    *  @name Refresh
    *  @memberOf $ss.agentcore.exitcodes
    *  @function 
    *  @description function to flush the cached exit codes
    *               should be called only from framework level
    *  @returns void
    *  @example
    *         var oExit = $ss.agentcore.exitcodes;
    *         oExit.Refresh();
    */
    Refresh: function() {
      _oExitCodes = {};
    }
      
  });
  
  var _oExitCodes = {};
  
  function _IsExitCodeLoaded(sLibType, sLang) {
    var bReturn = true;
    try {
      if(!_oExitCodes[sLibType] || !_oExitCodes[sLibType][sLang] || !_oExitCodes[sLibType][sLang].loaded)
        bReturn = false;
    } catch (e) {
      bReturn = false;
    }
    return bReturn;
  }

  function _GetErrorDesc(sLibType, sExitCode, sLang) {
    return _oExitCodes[sLibType][sLang][sExitCode];
  }
  
  //Caches only the accessed Library Type and language
  function _LoadExitCodes(sLibType, sLang) {
    try {
      var  oDom, oExitCode, oErrorNode, oLangNode, oLibNode;
      var sExitCodeResPath = $ss.GetAttribute('startingpoint') + "\\" + _config.GetConfigValue("supportactions", "exitcodesfile", "exit_code.xml");
      oDom = _xml.LoadXML(sExitCodeResPath, false);
      oExitCode = _xml.GetNode("//exit-codes", "", oDom);
      for (var i = 0; i < oExitCode.childNodes.length; i++) {
        oLibNode = oExitCode.childNodes[i];
        var sTempLibType = oLibNode.getAttribute("type");
        if(sLibType !== sTempLibType) continue;
        _oExitCodes[sTempLibType] = {};
        for (var j = 0; j < oLibNode.childNodes.length; j++) {
          oLangNode = oLibNode.childNodes[j];
          var sTempLang = oLangNode.getAttribute("code");
          if(sLang !== sTempLang) continue;
          _oExitCodes[sTempLibType][sTempLang] = {};
          for (var k = 0; k < oLangNode.childNodes.length; k++) {
            oErrorNode = oLangNode.childNodes[k];
            var sId = oErrorNode.getAttribute("id");
            _oExitCodes[sTempLibType][sTempLang][sId] = new _ErrorObj(oErrorNode);
          }
          _oExitCodes[sTempLibType][sTempLang].loaded = true;
          throw "Loaded: Library -" + sTempLibType + " Language -" + sTempLang;
        }
      }
    } catch (e) {
    } finally {
      oErrorNode = null;
      oLangNode = null;
      oLibNode = null;
      oExitCode = null;
      oDom = null;
    }
  }

  function _ErrorObj(oErrorNode) {
    this.id = oErrorNode.getAttribute("id");
    //[MAC] Check Machine OS Windows/MAC
    if(!bMac)
      this.desc = oErrorNode.text;
    else
      this.desc = oErrorNode.textContent;

    this.exittype = oErrorNode.getAttribute("result");
  }

  var _config = $ss.agentcore.dal.config;
  var _xml = $ss.agentcore.dal.xml;
  var _sLang = $ss.agentcore.utils.GetLanguage();

  //[MAC] Get Machine OS type
  var bMac = $.browser.safari;
})();
