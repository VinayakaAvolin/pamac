/** @namespace provides all reporting functionality*/
$ss.agentcore.reporting = $ss.agentcore.reporting || {};
$ss.agentcore.reporting.Reports = $ss.agentcore.reporting.Reports || {};

(function () {
  $.extend($ss.agentcore.reporting.Reports, {

    /**
    *  @name LogHealStart
    *  @memberOf $ss.agentcore.reporting
    *  @function
    *  @description Function to log the start of Restore functionlity in Protect/Restore snapin 
    *  @param options log options
    *  @returns true/false
    *  @example
    *    var opt = {
    *      sContGuid :  guid,
    *      iVersion : contentVersion,
    *      sTitle : name,
    *      snapinName : "snapin_protectrestore"
    *    }
    *    $ss.agentcore.reporting.Reports.LogHealStart(opt);
    */
    LogHealStart: function(options){
      try {
        if (_Report.IsLoggingEnabled(options.snapinName)) {
          //                                  sContGuid, version,  verb,         view, solved,  comment,  data,         rating,  result
          var oLogEntry = this.CreateLogEntry(options.sContGuid, options.iVersion, "heal_start", 1, "", options.sTitle, "sprt_protection", "", "");
          return _Log(oLogEntry);
        }
      } 
      catch (ex) {
      }
      
      return false;
    },
    
    /**
    *  @name LogHealDone
    *  @memberOf $ss.agentcore.reporting
    *  @function
    *  @description  Function to log the end of Restore functionlity in Protect/Restore snapin 
    *  @param options log options
    *  @returns 
    *  @example
    *      var opt = {
    *         sContGuid :  aHealParms[2],
    *         iVersion : aHealParms[3],
    *         iItemsHealed : aHealParms[1],
    *         sTitle : aHealParms[4],
    *         snapinName : "snapin_protectrestore",
    *         showSurvey: false
    *       }
    *       $ss.agentcore.reporting.Reports.LogHealDone(opt);
    */
    LogHealDone: function(options){
      var iResult = 0;
      var showSurvey = true;
      try {
      
        // map iItemsHealed to completion codes for reporting
        if (options.iItemsHealed === 0) 
          iResult = _constants.RESULT_SUCCESS_NOCHANGE; // success, but no actions taken
        if (options.iItemsHealed === -1) 
          iResult = _constants.RESULT_FAILED; // failure code
        if (options.iItemsHealed > 0) 
          iResult = _constants.RESULT_SUCCESS; // success
        //                                  sContGuid, version,  verb,       view, solved,  comment,  data,         rating,  result
        var oLogEntry = this.CreateLogEntry(options.sContGuid, options.iVersion, "heal_end", "", "", options.sTitle, "sprt_protection", "", iResult);
        if (_Report.IsLoggingEnabled(options.snapinName)) {
          _Log(oLogEntry);
        }
        if (options.showSurvey === false) {
          showSurvey = false;
        }
        if (_constants.RESULT_SUCCESS === iResult) {
          var doDisplaySurvey = $ss.agentcore.dal.config.GetConfigValue(options.snapinName, "show_survey_heal", "true") === "true";
          if (doDisplaySurvey && showSurvey) {
            this.DisplaySurvey(oLogEntry);
          }
          
        }
        
        return true;
        
      } 
      catch (ex) {
      }
    },

    /**
    *  @name LogSnapinStart
    *  @memberOf $ss.agentcore.reporting
    *  @function
    *  @description  Function to log the start of a snapin 
    *  @param snapinName friendly name of the snapin
    *  @param params log options
    *  @returns 
    *  @example
    *           $ss.agentcore.reporting.Reports.LogSnapinStart(snapinName,params);
    *
    */
    LogSnapinStart: function(snapinName, params){
      try {
        if (_Report.IsLoggingEnabled(snapinName)) {
          //                                  sContGuid, version,  verb,         view, solved,  comment,     data,   rating,  result
          var oLogEntry = this.CreateLogEntry(snapinName, 0, "snapin_view", 1, "", snapinName, "", "", "");
          return _Report.LogSnapinView(oLogEntry, snapinName, "index", params);
        }
      } 
      catch (ex) {
      }
      return false;
    },
    
    
    /**
    *  @name LogContentStart
    *  @memberOf $ss.agentcore.reporting
    *  @function
    *  @description  Function to log the start of external content 
    *  @param snapinName name of the snapin
    *  @param guid content guid
    *  @param contentType type of content
    *  @param version version of the content
    *  @returns 
    *  @example
    *           $ss.agentcore.reporting.Reports.LogContentStart( snapinName, sContentGUID, sContentType, sVersion)
    *
    */
    LogContentStart: function(snapinName, guid, contentType, version){
      try {
        if (_Report.IsLoggingEnabled(snapinName)) {
          //                                  sContGuid, version,  verb,         view, solved,  comment,     data,   rating,  result
          var oLogEntry = this.CreateLogEntry(guid, version, "content_view", 1, "", snapinName, "", "", "");
          return _Report.LogContentView(oLogEntry, snapinName, contentType);
        }
      } 
      catch (ex) {
      }
      return false;
    },

    /**
    *  @name LogToolUsage
    *  @memberOf $ss.agentcore.reporting
    *  @function
    *  @description Function to log the tool usage  
    *  @param toolName name of the tool
    *  @param snapinName name of the snapin
    *  @returns 
    *  @example
    *         $ss.agentcore.reporting.Reports.LogContentStart( toolName,snapinName)
    *
    */
    LogToolUsage: function(toolName, snapinName){
      try {
        if (_Report.IsLoggingEnabled(snapinName)) {
          //                                  sContGuid, version,  verb,         view, solved,  comment,     data,   rating,  result
          var oLogEntry = this.CreateLogEntry(toolName, 0, "tool_use", 1, "", toolName, "", "", "");
          return _Log(oLogEntry);
        }
      } 
      catch (ex) {
      }
      return false;
    },
    
    /**
    *  @name LogProtectionStart
    *  @memberOf $ss.agentcore.reporting
    *  @function
    *  @description  Function to log the start of Protect functionlity in Protect/Restore snapin 
    *  @param options log options
    *  @returns 
    *  @example
    *           var opt = {
    *                sContGuid :  oProbe.guids[item.guid].guid,
    *                iVersion : oProbe.guids[item.guid].contentVersion,
    *                sTitle : oProbe.guids[item.guid].name,
    *                snapinName : "snapin_protectrestore"
    *            }
    *            $ss.agentcore.reporting.Reports.LogProtectionStart(opt);
    *
    */
    LogProtectionStart: function(options){
    
      try {
        if (_Report.IsLoggingEnabled(options.snapinName)) {
          //                                  sContGuid, version,  verb,         view, solved,  comment,  data,         rating,  result
          var oLogEntry = this.CreateLogEntry(options.sContGuid, options.iVersion, "prot_start", 1, "", options.sTitle, "sprt_protection", "", "");
          return _Log(oLogEntry);
        }
      } 
      catch (ex) {
      }
      return false;
    },
    
    /**
    *  @name LogProtectionDone
    *  @memberOf $ss.agentcore.reporting
    *  @function
    *  @description  Function to log the end of Protect functionlity in Protect/Restore snapin 
    *  @param options log options
    *  @returns 
    *  @example
    *          var opt = {
    *              sContGuid :  oProbe.guids[item.guid].guid,
    *              iVersion : oProbe.guids[item.guid].contentVersion,
    *              sTitle : oProbe.guids[item.guid].name,
    *              snapinName : "snapin_protectrestore",
    *              success : 1
    *            }
    *          $ss.agentcore.reporting.Reports.LogProtectionDone(opt); 
    *
    */
    LogProtectionDone: function(options){
    
      var iResult = 0;
      
      // map iSuccess to completion codes for reporting
      if (options.iSuccess === 0) 
        iResult = _constants.RESULT_FAILED; // failure code
      else 
        if (options.iSuccess === 1) 
          iResult = _constants.RESULT_SUCCESS; // success
        else 
          iResult = _constants.RESULT_SUCCESS;
      
      try {
        if (_Report.IsLoggingEnabled(options.snapinName)) {
          //                                  sContGuid, version,  verb,       view, solved,  comment,  data,         rating,  result
          if(bMac) {
            var oLogEntry = this.CreateLogEntry(options.sContGuid, options.iVersion, "prot_end", 1, "", options.sTitle, "sprt_protection", "", iResult.toString());
          } else {
            var oLogEntry = this.CreateLogEntry(options.sContGuid, options.iVersion, "prot_end", 1, "", options.sTitle, "sprt_protection", "",iResult);
          }
          return _Log(oLogEntry);
        }
      } 
      catch (ex) {
      }
      return false;
    },
    
    /**
    *  @name LogUndoStart
    *  @memberOf $ss.agentcore.reporting
    *  @function
    *  @description  Function to log the start of Undo functionlity in Protect/Restore snapin 
    *  @param options log options
    *  @returns 
    *  @example
    *          var opt = {
    *              sContGuid :  guid,
    *              iVersion : contentVersion,
    *              sTitle : name,
    *              snapinName : "snapin_protectrestore"
    *            }
    *            $ss.agentcore.reporting.Reports.LogUndoStart(opt); 
    *
    */
    LogUndoStart: function(options){
    
      try {
        if (_Report.IsLoggingEnabled(options.snapinName)) {
          //                                  sContGuid, version,  verb,       view, solved,  comment,  data,         rating,  result
          var oLogEntry = this.CreateLogEntry(options.sContGuid, options.iVersion, "undo_start", 1, "", options.sTitle, "sprt_protection", "", "");
          return _Log(oLogEntry);
        }
      } 
      catch (ex) {
      }
      return false;
    },
    
    /**
    *  @name LogUndoDone
    *  @memberOf $ss.agentcore.reporting
    *  @function
    *  @description Function to log the end of Undo functionlity in Protect/Restore snapin  
    *  @param options log options
    *  @returns 
    *  @example
    *          var opt = {
    *              sContGuid :  guid,
    *              iVersion : contentVersion,
    *              sTitle : name,
    *              snapinName : "snapin_protectrestore",
    *              iSuccess : 1   
    *            }
    *            $ss.agentcore.reporting.Reports.LogUndoDone(opt); 
    *
    */
    LogUndoDone: function(options){
      var iResult = 0;
      
      // map iSuccess to completion codes for reporting
      if (options.iSuccess === 0) 
        iResult = _constants.RESULT_FAILED; // failure code
      else 
        if (options.iSuccess === 1 || options.iSuccess == true) 
          iResult = _constants.RESULT_SUCCESS; // success
        else 
          iResult = _constants.RESULT_SUCCESS;
      
      try {
        if (_Report.IsLoggingEnabled(options.snapinName)) {
          //                                  sContGuid, version,  verb,       view, solved,  comment,  data,         rating,  result
          var oLogEntry = this.CreateLogEntry(options.sContGuid, options.iVersion, "undo_end", 1, "", options.sTitle, "sprt_protection", "", iResult);
          return _Log(oLogEntry);
        }
      } 
      catch (ex) {
      }
      return false;
    },
    
    /**
    *  @name LogTriggerStart
    *  @memberOf $ss.agentcore.reporting
    *  @function
    *  @description Function to log the start of Trigger functionlity in Protect/Restore snapin  
    *  @param options log options
    *  @returns 
    *  @example
    *         var options = 
    *          {
    *           sContGuid : sContGuid,
    *           iVersion : sContVersion,
    *           sTitle : sContTitle,
    *           sCommandLine : sCmdLine,
    *           snapinName : "snapin_trigger"
    *          }
    *          $ss.agentcore.reporting.Reports.LogTriggerStart(options);
    *
    */
    LogTriggerStart: function(options){
      try {
        if (_Report.IsLoggingEnabled(options.snapinName)) {
          //                                  sContGuid, version,  verb,            view, solved,  comment,  data,        rating,  result
          var oLogEntry = this.CreateLogEntry(options.sContGuid, options.iVersion, "trigger_start", 1, "", options.sTitle, options.sCommandLine, "", "");
          return _Log(oLogEntry);
        }
      } 
      catch (ex) {
      }
      return false;
    },
    
    /****************************************************************************************
     ** Name:        LogFlowStart
     **
     ** Purpose:     This API is called when a user has started a StepList Snapin.
     **
     ** Parameter:   snapinName	String    Name of the StepList Snapin
     **
     ** Return:      Boolean result of logging the message
     *****************************************************************************************/
    /**
    *  @name LogFlowStart
    *  @memberOf $ss.agentcore.reporting
    *  @function
    *  @description Function to log the start of a steplist snapin  
    *  @param snapinName snapin name
    *  @returns 
    *  @example
    *         $ss.agentcore.reporting.Reports.LogFlowStart(sSnapinName);
    *
    */
    LogFlowStart: function(snapinName){
    
      try {
        if (_Report.IsLoggingEnabled(snapinName)) {
          //                                  sContGuid, version, verb,         view, solved,   comment,   data,  rating, result
          var oLogEntry = this.CreateLogEntry(snapinName, 0, "flow_start", 1, "", snapinName, "", "", "");
          return _Log(oLogEntry);
        }
      } 
      catch (ex) {
      }
      return false;
      
    },
    
    /**
    *  @name LogFlowDone
    *  @memberOf $ss.agentcore.reporting
    *  @function
    *  @description  Function to log the end of steplist snapin 
    *  @param snapinName name of the snapin
    *  @param arrStepPath String Array Containing all the steps user has gone through
    *  @param iResult
    *  @returns 
    *  @example
    *         $ss.agentcore.reporting.Reports.LogFlowDone(sSnapinName, sCompleteNavFileHistory, $ss.agentcore.constants.SL_SNAPIN_RESULT_ABORTED);
    *
    */
    LogFlowDone: function(snapinName, arrStepPath, iResult){
    
      var solutionPath = "";
      
      for (var i = 0; i < arrStepPath.length; i++) {
        if (i > 0) 
          solutionPath += "->";
        if (arrStepPath[i] !== undefined) {
          if (arrStepPath[i].indexOf("/") > -1) 
            solutionPath += arrStepPath[i].substring(arrStepPath[i].lastIndexOf("/") + 1);
          else 
            solutionPath += arrStepPath[i];
        }
      }
      
      
      if (solutionPath === "") 
        solutionPath = "-";
      //                                  sContGuid, version, verb,      view, solved,   comment,   data,         rating, result
      var oLogEntry = this.CreateLogEntry(snapinName, 0, "flow_end", "", "", snapinName, solutionPath, "", iResult);
      
      try {
        if (_Report.IsLoggingEnabled(snapinName)) {
          // only show survey when the flow returns a success
          _Log(oLogEntry);
        }
        if ($ss.agentcore.constants.SL_SNAPIN_RESULT_SUCCESS === iResult) {
          var doDisplaySurvey = $ss.agentcore.dal.config.GetConfigValue(snapinName, "show_survey", "true") === "true";
          if (doDisplaySurvey) {
            this.DisplaySurvey(oLogEntry);
          }
          
        }
        return true;
      } 
      catch (ex) {
      }
      
      return false;
    },
    
    DisplaySurvey: function(oLogEntry){
      try {
        var params = {};
        params.surveyData = oLogEntry;
        MVC.Controller.dispatch("snapin_survey", "index", params);
      } 
      catch (ex) {
      
      }
    },
    
    /**
    *  @name CreateLogEntry
    *  @memberOf $ss.agentcore.reporting
    *  @function
    *  @description Function to create the log entry object
    *  @param guidContent content guid
    *  @param version content version
    *  @param verb type of action
    *  @param viewed Indicates if the content was viewed
    *  @param solved Indicates if the content solved the problem
    *  @param comment extra information, such as path
    *  @param data extra information, such as trigger context
    *  @param rating survey rating
    *  @param result Indicates the action of the operation
    *  @returns 
    *  @example
    *           var oLogEvt = $ss.agentcore.reporting.Reports.CreateLogEntry(guidContent, version, verb, viewed, solved, comment, data, rating, result);
    *
    */
    CreateLogEntry: function(guidContent, version, verb, viewed, solved, comment, data, rating, result){
      var logEntry = new Object();
      logEntry.guidContent = guidContent;
      logEntry.version = version;
      logEntry.verb = verb;
      logEntry.viewed = viewed;
      logEntry.solved = solved;
      logEntry.comment = comment;
      logEntry.data = data;
      logEntry.rating = rating;
      logEntry.result = result;
      
      return logEntry;
    },
    
    /**
    *  @name Log
    *  @memberOf $ss.agentcore.reporting
    *  @function
    *  @description  Function to log the entry
    *  @param oLogEntry log entry object
    *  @param snapinName name of the snapin
    *  @returns 
    *  @example
    *         $ss.agentcore.reporting.Reports.Log(oLogEvt, "snapin_solutions");
    *
    */
    Log: function(oLogEntry, snapinName){
      try {
        if (_Report.IsLoggingEnabled(snapinName)) {
          return _Log(oLogEntry);
        }
      } 
      catch (ex) {
      }
      return false;
    }

  }); //$.extend [$ss.agentcore.reporting.Reports]

  var _objContainer = $ss.agentcore.utils.activex.GetObjInstance();
  var _utils = $ss.agentcore.utils;
  var _xml = $ss.agentcore.dal.xml;
  var _constants = $ss.agentcore.constants;
  var _config = $ss.agentcore.dal.config;
  var _reports = $ss.agentcore.reporting.Reports;
  var _logCache = [];
  var _cacheLogging = false;

  var _hashReportConfigs = {};

  var _ReportData = Class.extend({}, {
    init: function(){
      this.log = false;
      this.Actions = {};
      this.Contents = {};
    },
    log: false,
    Actions: null,
    Contents: null
  }); //Class.extend [_ReportData]

  var _Report = Class.extend({
    LoadConfig: function(snapinName){
      var retVal = false;
      
      if (snapinName) {
        var objTriggerData = new _ReportData();
        
        var oReportingNode = _config.GetReportingNode(snapinName);
        
        if (oReportingNode && this.IsEnabled(oReportingNode, snapinName)) {
          // get reporting types
          var typeNodes;
          if(bMac) {
            //Appending extra "/" to "config-item" as when this is passed as Xpath to SelectNodes function it removes one "/".
            typeNodes=_xml.GetNodes("///config-item","",_xml.GetNode("///config[@name=\"types\"]","",oReportingNode));
          } else{
            typeNodes = _xml.GetNodes("config-item", "", _xml.GetNode("config[@name='types']", "", oReportingNode));
          }
           if(typeNodes&&typeNodes.length>0) {
            for (var i = 0; i < typeNodes.length; i++) {
              switch (_xml.GetNodeText(typeNodes[i])) {
                case "snapin_view":
                  this.ReadSnapinActions(snapinName, objTriggerData);
                  break;
                case "content_view":
                  this.ReadContentsData(snapinName, objTriggerData);
                  break;
                default:
                  return retVal;
              }
            }
          }
        }
        else {
          return retVal;
        }
        
        
        if (_hashReportConfigs[snapinName]) {
          delete _hashReportConfigs[snapinName];
        }
        
        _hashReportConfigs[snapinName] = objTriggerData;
        objTriggerData = null;
        retVal = true;
      }
      
      return retVal;
    },

    ReadContentsData: function(snapinName, objTriggerData){
      var nodeContentView = _config.GetContentViewNode(snapinName);
      var nodesContentType;
      if(bMac){
        nodesContentType = _xml.GetNodes("///config", "", nodeContentView);
      }else{
        nodesContentType = _xml.GetNodes("config","",nodeContentView);
      }
      for(var i=0;i<nodesContentType.length;i++) {
        var node = nodesContentType[i];
        var typeContent = _xml.GetAttribute(node, "typeContent");
        var freq = _xml.GetAttribute(node, "frequency");
        
        objTriggerData.Contents[typeContent] = freq;
      }
      
      objTriggerData.Contents.hashIgnoreContents = {};
    },
    
    ReadSnapinActions: function(snapinName, objTriggerData){
      var nodeSnapinView = _config.GetSnapinViewNode(snapinName);
      var nodeActions;
      var nodeCols;
      if(bMac){
        nodeActions = _xml.GetNodes("///config", "", nodeSnapinView);
      }else{
        nodeActions = _xml.GetNodes("config", "", nodeSnapinView);
      }
      for (var i = 0; i < nodeActions.length; i++) {
        var node = nodeActions[i];
        
        var action = {};
        
        action.name = _xml.GetAttribute(node, "action");
        action.frequency = _xml.GetAttribute(node, "frequency");        
        
        
        if(bMac){
          nodeCols = _xml.GetNodes("///config-item", "", node);
        }else{
          nodeCols = _xml.GetNodes("config-item", "", node);
        }
        var pattern = /\bguidContent\b|\bverb\b|\bcomment\b/g;
        
        action.cols = {};
        
        for (var c = 0; c < nodeCols.length; c++) {
          var nodeCol = nodeCols[c];
          var colName = _xml.GetAttribute(nodeCol, "name");
          
          
          if (colName.match(pattern)) {
            var variable = _xml.GetAttribute(nodeCol, "variable");
            var colData = _xml.GetNodeText(nodeCol);
            
            
            action.cols[colName] = {};
            action.cols[colName]["data"] = colData;
            action.cols[colName]["variable"] = variable;
            
            if (colName.match(/\bguidContent\b/g)) {
              action.cols["data"] = {};
              action.cols["data"]["data"] = colData;
              action.cols["data"]["variable"] = variable;
            }
            
          }
          
        }
        
        objTriggerData.Actions[action.name] = action;
      }
    },
    
    IsEnabled: function(oReportingNode, snapinName){
      var enabled = _xml.GetAttribute(oReportingNode, "enabled");
      if (enabled === "true") {
        return true;
      }
      return false;
    },

    IsLoggingEnabled: function(snapinName){
        if (_hashReportConfigs[snapinName] === false) {
          return false;
        }
        else 
          if (_hashReportConfigs[snapinName] === undefined) {
            this.LoadConfig(snapinName);
            if (!_hashReportConfigs[snapinName]) {
              _hashReportConfigs[snapinName] = false;
              return false;
            }
          }
      return true;
    },
    
    LogSnapinView: function(oLogEntry, snapinName, actionName, params){
      var objTriggerData = _hashReportConfigs[snapinName];
      
      var action = objTriggerData.Actions[actionName];
      
      if (action) {
        if (action.frequency === "never") {
          return false;
        }
        
        this.OverwriteColValues(oLogEntry, action, params);
        
        if (action.frequency === "once") {
          action.frequency = "never";
          delete objTriggerData.Actions[actionName];
          objTriggerData.Actions[actionName] = null;
        }
        
        return _Log(oLogEntry);
      }
      
      return false;
      
    },
    
    LogContentView: function(oLogEntry, snapinName, contentType){
      var objTriggerData = _hashReportConfigs[snapinName];
      
      if (contentType) {
        // no reporting for this contenttype
        if (!objTriggerData.Contents[contentType]) 
          return false;
        
        // check for the contentguid in the hashIgnoreContents,if found bail out
        if (objTriggerData.Contents.hashIgnoreContents[oLogEntry.guidContent]) 
          return false;
        
        // if not,go ahead and log
        var retVal = _Log(oLogEntry);
        
        // check the frequency,if once then put it to hashIgnoreContents
        
        if (objTriggerData.Contents[contentType] == "once") 
          objTriggerData.Contents.hashIgnoreContents[oLogEntry.guidContent] = true;
        
        return retVal;
      }
      
      return false;
    },
    
    GetLogMsg: function(oLogEntry){
      var principal = "Client";
      var rating = rating || "";
      
      var entry = (_utils.GetObjectFromCmdLine()).entry;
      
      if (!entry) {
        if(bMac)
          entry = "MAC Application";
        else
          entry = "Others";
      }
      msg = "CONTENT__" + oLogEntry.guidContent + '__' + oLogEntry.version + '__' + oLogEntry.verb + '__' + principal +
      '__' +
      oLogEntry.viewed +
      '__' +
      oLogEntry.solved +
      '__' +
      oLogEntry.comment +
      '__' +
      oLogEntry.data +
      '__' +
      oLogEntry.rating +
      '__' +
      entry +
      '__' +
      oLogEntry.result;
      
      return msg;
    },
    
    OverwriteColValues: function(oLogEntry, action, params){
      for (var colName in action.cols) {
        var oCol = action.cols[colName];
        if (oCol.variable &&
        oCol.variable.match(/\btrue\b/g) &&
        params &&
        params.element) {
          oLogEntry[colName] = eval("params.element." + oCol.data);
        }
        else {
          oLogEntry[colName] = oCol.data;
        }
      }
    }

  }, {}); //Class.extend [_Report]

  function FlushCachedLog(stopCaching) {
    // flushes log entries to disk
    for (var i = 0; i < _logCache.length; i++) {
      _LogToDisk(_logCache[i]);
    }
    
    // clear cached contents
    _logCache = [];
    
    // change the cachelog flag based on the input parameter
    if (stopCaching) {
      _cacheLogging = false;
    }
  }

  function DeleteCachedLog(stopCaching){
    // clear cached contents
    _logCache = [];
    
    // change the cachelog flag based on the input parameter
    if (stopCaching) {
      _cacheLogging = false;
    }
  }

  function _Log(oLogEntry){
    var retVal = true;
    var msg = _Report.GetLogMsg(oLogEntry);
    
    if (_cacheLogging == null) {
      //[MAC] Check Machine OS Windows/MAC
      if(!bMac) {
        var oArgs = _objContainer.GetObjectFromCmdLine();
      }
      else {
        //=======================================================
        //[MAC] Native Call
        var message = {'JSClassNameKey':'Reporting','JSOperationNameKey':'GetObjectFromCmdLine', 'JSArgumentsKey':[], 'JSISSyncMethodKey' : '1'}
        var oArgs = jsBridge.execute(message)
        //=======================================================
      }

      var cacheLog = unescape(oArgs.cachelog);
      if (cacheLog) 
        _cacheLogging = true;
    }
    
    if (_cacheLogging) {
      _logCache(msg);
    }
    else {
      //[MAC] Check Machine OS Windows/MAC
      if(!bMac)
        retVal = _objContainer.LogMsg(msg);
      else {
        //=======================================================
        //[MAC] Native Call
        var message = {'JSClassNameKey':'CommonUtility','JSOperationNameKey':'WriteUsageLog', 'JSArgumentsKey':[msg], 'JSISSyncMethodKey' : '1'}
        retVal = jsBridge.execute(message)
        //=======================================================
      }
    }
    
    _LogToRegistry(oLogEntry);
    
    return retVal;
  }

  /****************************************************************************************
   ** Name:        ss_rl_pvt_RegContUsage
   **
   ** Purpose:     Future Usage with SupportSoft LiveAssist.
   **
   **
   ** Parameter:   sContGuid   String            GUID or ID of content
   **              version     String            Version, 0 if not version controlled.
   **              verb        String            [view|flow_end|flow_sucess|???]
   **              view        Integer/String    Indicates if the content was viewed "", 0, or 1
   **              solved      Integer/String    Indicates if the content solved the problem "", 0, or 1
   **              comment     String            extra information, such as path
   **              data        String            extra information, such as trigger context
   **
   ** Return:      none
   *****************************************************************************************/
  function _LogToRegistry(oLogEntry){
    var sUserName = $ss.agentcore.dal.config.GetContextValue("SdcContext:UserName");
    var sTimeStamp = $ss.agentcore.utils.FormatDateString(new Date(), true, true);
    var oArgs = $ss.agentcore.utils.GetObjectFromCmdLine();
    var sEntry = unescape(oArgs.entry);
    
    if (!sEntry || sEntry === "undefined") 
      sEntry = "Others";
    
    var subkey = "Software\\SupportSoft\\ProviderList\\" +
    $ss.agentcore.dal.config.GetContextValue("SdcContext:ProviderId") +
    "\\users\\" +
    sUserName +
    "\\LogCache\\" +
    sTimeStamp;
    
    $ss.agentcore.dal.registry.SetRegValueByType("HKCU", subkey, "sContGuid", 1, oLogEntry.guidContent);
    $ss.agentcore.dal.registry.SetRegValueByType("HKCU", subkey, "version", 1, oLogEntry.version);
    $ss.agentcore.dal.registry.SetRegValueByType("HKCU", subkey, "verb", 1, oLogEntry.verb);
    $ss.agentcore.dal.registry.SetRegValueByType("HKCU", subkey, "principal", 1, "Client");
    $ss.agentcore.dal.registry.SetRegValueByType("HKCU", subkey, "solved", 1, oLogEntry.solved);
    $ss.agentcore.dal.registry.SetRegValueByType("HKCU", subkey, "comment", 1, oLogEntry.comment);
    $ss.agentcore.dal.registry.SetRegValueByType("HKCU", subkey, "data", 1, oLogEntry.data);
    $ss.agentcore.dal.registry.SetRegValueByType("HKCU", subkey, "rating", 1, oLogEntry.rating);
    $ss.agentcore.dal.registry.SetRegValueByType("HKCU", subkey, "entry", 1, sEntry);
    $ss.agentcore.dal.registry.SetRegValueByType("HKCU", subkey, "result", 1, oLogEntry.result);
  }
  
  function _LogToDisk(msg){
    //[MAC] Check Machine OS Windows/MAC
    if(!bMac)
      return _objContainer.Log(msg);
    else {
      //=======================================================
      //[MAC] Native Call
      var message = {'JSClassNameKey':'Reporting','JSOperationNameKey':'Log', 'JSArgumentsKey':[msg], 'JSISSyncMethodKey' : '1'}
      jsBridge.execute(message)
      //=======================================================
    }
  }

  //[MAC] Get Machine OS type
  var bMac = $.browser.safari;

  //[MAC] JSBridge object used only for MAC
  try {
    var jsBridge = window.JSBridge;
  }
  catch(e) {
  }

})(); //global function

///****************************************************************************************
//** Name:        ss_rl_LogTriggerInfo()
//**
//** Purpose:     This function will log the usage of a trigger 
//**
//** Parameter:   none
//**
//** Return:      none
//*****************************************************************************************/
//function ss_rl_LogTriggerInfo()
//{
//  var sCmdLine = ss_con_GetObjInstance().GetCmdLine().toString().toLowerCase();
//  var sEntry = ss_con_CmdToObj()["entry"]
//  var triggers = ss_content_getContent("sprt_trigger");
//  var sContGuid = '';
//  var sContVersion = '0';
//  var sContTitle = '';
//  var bFound = false;
//  
//  if (sEntry)
//  {
//    for( var id in triggers )
//    {
//      sContGuid = triggers[id].guid;
//      sContVersion = triggers[id].version;
//      sContTitle = triggers[id].title;
//      
//      for( var fdid in triggers[id].fields )
//      {
//        var sFieldValue = triggers[id].fields[fdid].sccf_field_value_char.text.toLowerCase();
//        if( sFieldValue.indexOf(sEntry.toLowerCase())>-1 )
//        {
//          bFound = true;
//          break;
//        }
//      }
//      if( bFound ) {
//        break;
//      }
//    }
//  }
//  if( bFound ) {
//     shellwindow.ss_rl_LogTriggerStarted(sContGuid, sContVersion, sContTitle, sCmdLine);
//  }
//}
