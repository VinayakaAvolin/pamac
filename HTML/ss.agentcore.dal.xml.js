    /** @namespace Holds all XML related functionality*/
    $ss.agentcore.dal.xml = $ss.agentcore.dal.xml || {};

    (function()
    {
      $.extend($ss.agentcore.dal.xml,
      {

        /**
        *  @name LoadXML
        *  @memberOf $ss.agentcore.dal.xml
        *  @function
        *  @description  Function loads the XML passed through sUrl. It can load the<br/>
        *                DOM async or in sync fashion. If using async pass a callback <br/>
        *                function to handle the resultant DOM. <br/>
        *  @param sUrl Fully qualified url to hit
        *  @param bAsync Used to turn ON async; <optional for sync>
        *  @param fnHandler Callback function <optional for sync>
        *  @returns null in async calls and/or DOM object in synchronous calls
        *  @example
        *          var $xml = $ss.agentcore.dal.xml;
        *          domClientUI = $xml.LoadXML("c:\\test.xml",true,callback);   
        */
        LoadXML : function(sUrl, bAsync, fnHandler)
        {
          _logger.info('Entered function: $ss.agentcore.dal.xml.LoadXML');
          try 
          {
            //[MAC] Check Machine OS Windows/MAC
            if(!bMac) {
              var oDOM   = _utils.CreateObject("Microsoft.XMLDOM",true);
              oDOM.resolveExternals = false;
              oDOM.async = bAsync ? bAsync: false;
              oDOM.onreadystatechange = function(){_OnXMLResponse(oDOM, fnHandler);}
              oDOM.load(sUrl);
            }
            else {
              var message = {'JSClassNameKey':'File','JSOperationNameKey':'GetNativePath', 'JSArgumentsKey':[sUrl], 'JSISSyncMethodKey' : '1'}
              var result = _jsBridge.execute(message);
              var parsedJSONObject = JSON.parse(result);
              sUrl = parsedJSONObject.Data;
              _logger.info('Entered function: $ss.agentcore.dal.xml.LoadXML = ' + sUrl);

              if (!_file.FileExists(sUrl)) {
                return null;
              }

              var oDOM   =  new XMLHttpRequest();//
              oDOM.responseType = 'document';
              oDOM.open("GET", sUrl, bAsync ? bAsync: false); 
              oDOM.overrideMimeType('text/xml');

              // oDOM.setProperty("SelectionLanguage", "XPath");
              oDOM.onreadystatechange = function() {
                if (this.readyState == 4 /*&& this.status == 200*/) {
                  _logger.info('onreadystatechange function: $ss.agentcore.dal.xml.LoadXML= ' + sUrl + 'status = ' + this.status);
                  // _xml.cleanNode(oDOM.responseXML);
                  return oDOM;
                }
              };
              oDOM.send(null); 
            }
          } 
          catch (ex) 
          {
            _exception.HandleException(ex,'$ss.agentcore.dal.xml','LoadXML',arguments);
            return null;
          }

          return oDOM;
        },

        /**
        *  @name LoadXMLFromString
        *  @memberOf $ss.agentcore.dal.xml
        *  @function
        *  @description  Loads given xml into DOM object
        *  @param sXML XML string to load
        *  @param bAsync Used to turn ON async; <optional for sync>
        *  @param fnCallback Callback function <optional for sync>
        *  @returns DOM object if successful else null
        *  @example
        *         var $xml = $ss.agentcore.dal.xml;
        *         var sXml = "<xml><test>testValue</test></xml>"
        *         var dom =  $xml.LoadXMLFromString(sXml,true,null);  
        */
        LoadXMLFromString : function(sXML, bAsync,fnCallback)
        {
          _logger.info('Entered function: $ss.agentcore.dal.xml.LoadXMLFromString');
          var oDOM  = null; 
          //[MAC] Check Machine OS Windows/MAC
          if(!bMac) {
          try 
          {
            oDOM = _utils.CreateObject("Microsoft.XMLDOM",true);
            oDOM.async = bAsync ? bAsync : false;
            oDOM.onreadystatechange = function(){_OnXMLResponse(oDOM, fnCallback);}
            oDOM.loadXML(sXML);

            if (oDOM.parseError.errorCode != 0) 
            {
              var sMsg = "  Reason: " + oDOM.parseError.reason + " " +
                       "  Code: " + oDOM.parseError.errorCode + " " +
                       "  Line: " + oDOM.parseError.line + " " +
                      "  Linepos: " + oDOM.parseError.linepos
              _logger.info("Error in $ss.agentcore.dal.xml.LoadXMLFromString -- " + sMsg);
              return null;
            } 
          } 
          catch (ex) 
          {
            _exception.HandleException(ex,'$ss.agentcore.dal.xml','LoadXMLFromString',arguments);
            return null;
          } 
        }
        else {
          var message = {'JSClassNameKey':'File','JSOperationNameKey':'CreateXMLFromString', 'JSArgumentsKey':[sXML], 'JSISSyncMethodKey' : '1'}
          var result = _jsBridge.execute(message);
          var parsedJSONObject = JSON.parse(result);
          var filePath = parsedJSONObject.Data;
          oDOM  = this.LoadXML(filePath,false);
          message = {'JSClassNameKey':'File','JSOperationNameKey':'DeleteFile', 'JSArgumentsKey':[filePath], 'JSISSyncMethodKey' : '1'}
          result = _jsBridge.execute(message);
        }  
          return oDOM;
        },
        
        /**
        *  @name LoadRemoteXML 
        *  @memberOf $ss.agentcore.dal.xml
        *  @function
        *  @description  Loads a remote xml
        *  @param sURL URL of the remote xml to load
        *  @returns oDom dom of the remote xml
        *  @example
        *           var $xml = $ss.agentcore.dal.xml;
        *           var ret =  $xml.LoadRemoteXML("http://testServer/test.xml",callback);  
        */
        LoadRemoteXML : function(sURL,fnCallback)
        {
          _logger.info('Entered function: $ss.agentcore.dal.xml.LoadRemoteXML');
          var oDOM = null;
          $.ajax(
            {
              type:"GET", 
              url:sURL, 
              dataType:"xml",
              success: function(xml) 
              {
                oDOM = xml;
              },
              error: function(xml) 
              {
              }
            });
            
            return oDOM;
        },

        /**
        *  @name AugmentDoc
        *  @memberOf $ss.agentcore.dal.xml
        *  @function
        *  @description Augments the child nodes in primary document element with the <br/>
        *              child nodes in oAugDoc based on the pivot attribute.<br/>
        *              The following rules apply when augmenting primary documentElement:<br/>
        *           - Any element with a sPivotAttr attribute will override an <br/>
        *              element with the same sPivotAttr value in the primary Doc. <br/>
        *            - Any element with a sPivotAttr attribute which is not contained in <br/>
        *              the primary Doc will be appended to the primary Doc. <br/>
        *            - Any element without a sPivotAttr attribute will override that <br/>
        *              element in the primary Doc. <br/>
        *            - Any element without a sPivotAttr attribute not contained in the <br/>
        *              primary Doc will be appended to the primary Doc. <br/>
        *  @param oPrimaryDoc documentElement or parent Node treated as primary.
        *  @param oAugDoc node whose child elements with the pivot attribute <br/>
        *         will replace same elements in primary Doc.
        *  @param sPivotAttr Attribute to be looked into while comparing nodes.
        *  @param sRootXPath Preformed XPath expression. All children of the <br/>
        *         primary doc element from the root level formed by <br/>
        *         the given XPath expression will be traversed for <br/>
        *         a match.
        *  @returns Updates oPrimaryDoc
        *  @example
        *           var $xml = $ss.agentcore.dal.xml;
        *           var oNewDOM =  $xml.AugmentDoc(
        *           oDOM.selectSingleNode("//sprt/configuration[@name='smacommon']",
        *           oDOM.selectSingleNode("//sprt/configuration[@name='smacustom']",
        *           "name", "./");
        *
        */
        AugmentDoc:function (oPrimaryDoc, oAugDoc, sPivotAttr, sRootXPath )
        {
      _logger.info('Entered function: $ss.agentcore.dal.xml.AugmentDoc');
          var sTagXPath,sPivotVal,sPivotXPath;
          var oElement,oExistingNode;
          var aChildren = oAugDoc.childNodes;
          try 
          {
            for (var item=0; item<aChildren.length; item++) 
            {
              oElement = aChildren[item];
              if(oElement.nodeType != 1) continue; //skip on incompatible nodes
              oOverrideNode = oElement.cloneNode(true);

              sTagXPath   = sRootXPath + oElement.tagName;
              sPivotVal   = "";
              sPivotXPath = "";

              //[MAC] Check Machine OS Windows/MAC
              if(!bMac) {
                if (sPivotAttr != "" && oElement.getAttribute(sPivotAttr) != undefined) 
                {
                  sPivotVal = oElement.getAttribute(sPivotAttr);
                  sPivotXPath = "[@"+sPivotAttr+"='"+sPivotVal+"']";
                }
              }
              else {
                if (sPivotAttr != "" && oElement.getAttributeNode(sPivotAttr).nodeValue != undefined) 
                {
                  sPivotVal = oElement.getAttributeNode(sPivotAttr).nodeValue;
                  sPivotXPath = "[@"+sPivotAttr+"='"+sPivotVal+"']";
                }
              }

              if (sTagXPath != "") 
              {
                //[MAC] Check Machine OS Windows/MAC
                if(!bMac)
                  oExistingNode = oPrimaryDoc.selectSingleNode( sTagXPath+sPivotXPath );
                else
                  oExistingNode = _xml.SelectSingleNode(oPrimaryDoc, sTagXPath+sPivotXPath );
                if (typeof(oExistingNode) != 'undefined' && oExistingNode != null) 
                {
                  oExistingNode.parentNode.insertBefore( oOverrideNode, oExistingNode );
                  oExistingNode.parentNode.removeChild( oExistingNode );
                } 
                else 
                {
                  oPrimaryDoc.appendChild( oOverrideNode );
                }
              }
            }
          }
          catch (ex) 
          {
            _exception.HandleException(ex,'$ss.agentcore.dal.xml','AugmentDoc',arguments);
          }
        },


        /**
        *  @name AugmentDOMs
        *  @memberOf $ss.agentcore.dal.xml
        *  @function
        *  @description  Augments the primary DOM in oPrimaryDOM with all the xml files passed <br/>
        *              in the arFiles array based on the pivot attribute. If no primary <br/>
        *              DOM is passed then the first file in arFiles is used to create <br/>
        *              primary DOM. <br/>
        *              The following rules apply when constructing DOM:<br/>
        *            - Any element with a sPivotAttr attribute will override an<br/>
        *              element with the same sPivotAttr value in the primary DOM.<br/>
        *            - Any element with a sPivotAttr attribute which is not contained in<br/>
        *              the primary DOM will be appended to the primary DOM.<br/>
        *            - Any element without a sPivotAttr attribute will override that<br/>
        *              element in the primary DOM.<br/>
        *            - Any element without a sPivotAttr attribute not contained in the<br/>
        *              primary DOM will be appended to the primary DOM.
        *  @param  oPrimaryDOM DOM to be treated as primary. Pass null if first<br/>
        *                      value of arFiles is to be treated as primary.
        *  @param  arFiles     Array of xml files to be loaded to contruct new<br/>
        *                      primary DOM. Specify files in order of precedence.<br/>
        *                      The last file in array having the highest precedence<br/>
        *                      and so on.
        *  @param sPivotAttr  Attribute to be looked into while comparing DOM<br/>
        *                     elements.
        *  @param sRootXPath  Preformed XPath expression. All children of the<br/>
        *                     element formed by the given XPath expression will<br/>
        *                     be traversed.
        *  @returns oPrimaryDOM
        *  @example
        *        var $xml = $ss.agentcore.dal.xml;  
        *         var oNewDOM =  $xml.AugmentDOMs(
        *         null,
        *         ["sscommon/lang/en/ss_shell.xml", "smartaccess/lang/en/ss_shell.xml"],
        *         "id", "//resources/");
        *
        */
        AugmentDOMs:function (oPrimaryDOM, arFiles, sPivotAttr, sRootXPath)
        {
          _logger.info('Entered function: $ss.agentcore.dal.xml.AugmentDOMs');
          try {
            if(oPrimaryDOM == null) {
              // then it is the first one in the list and no augmentation is required
              if(arFiles.length != 0) {
                oPrimaryDOM = this.LoadXML(arFiles[0], false);
                arFiles.shift();
              }
              // need atleast a file to do augmentation hence check that
              if(arFiles.length == 0) {
                return oPrimaryDOM; // as base dom is the augmented dom
              }
            }
            var oAugDOM  = null;
            for (var i=0; i<arFiles.length; i++) 
            {
              oAugDOM = this.LoadXML(arFiles[i], false);
              if(oPrimaryDOM == null) {
                oPrimaryDOM = oAugDOM;
                oAugDOM = null;
                continue;
              }
              if(oAugDOM != null)
              {
                if(oPrimaryDOM.documentElement)
                {
                  this.AugmentDoc( oPrimaryDOM.documentElement, oAugDOM.documentElement, sPivotAttr, sRootXPath );
                }
                else
                {
                  this.AugmentDoc( oPrimaryDOM, oAugDOM.documentElement, sPivotAttr, sRootXPath );
                }
              }
            }
          } catch(ex) {
            _exception.HandleException(ex,'$ss.agentcore.dal.xml','AugmentDOMs',arguments);
            //g_Logger.Error("ss_utl_AugmentDOMs", ex.message);
          }
          return oPrimaryDOM;
        },
        
        
        /**
        *  @name GetNodes
        *  @memberOf $ss.agentcore.dal.xml
        *  @function
        *  @description  Retrieves a collection of elements from the specified oXmlDocumentElement
        *  @param sElementXPath ( xpath argument 1 )
        *  @param sAttributeXPath ( xpath argument 2, concated to 1)
        *  @param oXmlDocumentElement ( xml document to query )
        *  @returns String text of the node, or "" if null or text not found.
        *  @example
        *           var $xml = $ss.agentcore.dal.xml;
        *           var xpathNode = "/sprt/config_to_use"; 
        *           var ret = $xml.GetNodes(xpathNode,"",domClientUI);
        */
        GetNodes:function ( sElementXPath , sAttributeXPath , oXmlDocumentElement )
        {
      _logger.info('Entered function: $ss.agentcore.dal.xml.GetNodes');
          try
          {
            sElementXPath   = sElementXPath   || "";
            sAttributeXPath = sAttributeXPath || "";
            if (oXmlDocumentElement != null) {
              //[MAC] Check Machine OS Windows/MAC
              if(!bMac)
                return oXmlDocumentElement.selectNodes(sElementXPath + sAttributeXPath);
              else {
                var xmlDoc = oXmlDocumentElement;
                if (oXmlDocumentElement.responseXML != undefined) {
                  xmlDoc = oXmlDocumentElement.responseXML;
                }else{
                  var xmlserialize = new XMLSerializer();
                  var xmlobj = xmlserialize.serializeToString(xmlDoc);
                  var parser = new DOMParser();
                  xmlDoc = parser.parseFromString(xmlobj ,"text/xml");
                }
                return this.SelectNodes(xmlDoc,sElementXPath + sAttributeXPath);//xmlDoc.getElementsByTagName(sElementXPath + sAttributeXPath);
              }
            }
          } 
          catch (e) {
            _exception.HandleException(e,'$ss.agentcore.dal.xml','GetNodes',arguments);
            //g_Logger.Error("ss_xml_NodesGet", e.message);
          }
          return null;
        },

        /**
        *  @name GetNodeText
        *  @memberOf $ss.agentcore.dal.xml
        *  @function
        *  @description  Retrieves the CDATA or Text of the specified node
        *  @param oNode element to retrieve the data from
        *  @returns String text of the node, or "" if null or text not found.
        *  @example
        *           var $xml = $ss.agentcore.dal.xml;
        *           var xpathNode = "/sprt/config_default"; 
        *           var ret = $xml.GetNodes(xpathNode,"",domClientUI);
        *           var text = $xml.GetNodeText(ret[0]);
        */
        GetNodeText:function  ( oNode )
        {
          _logger.info('Entered function: $ss.agentcore.dal.xml.GetNodeText');
          try
          {
            // if we have CDATA, return that.
            // if we find a TEXT node, return that.
            if (oNode != null) 
            {
              //[MAC] Check Machine OS Windows/MAC
              if(!bMac) {
                if (oNode.nodeType in {3:true,1:true})
                {
                  var sText = oNode.text;
                  var sCData = oNode.cdata;
                  var sReturn = sText || sCData || "";
                  return sReturn;
                }
                var sText = oNode.text;
                var sCData = oNode.cdata;
                var sReturn = sText || sCData || "";
              }
              else {
                _xml.cleanNode(oNode);
                var sReturn =  "";
                if (oNode.nodeType in {3:true,1:true})
                {
                  if (oNode.childNodes.length > 0) {
                    var sText = oNode.childNodes[0].nodeValue;
                    var sCData = oNode.childNodes[0].data;
                    sReturn = sText || sCData || "";
                  }
                  else {
                    sReturn = oNode.textContent;
                  }
                  return sReturn;
                }
              }
            }
          } 
          catch (e) {
            _exception.HandleException(e,'$ss.agentcore.dal.xml','GetNodeText',arguments);
            //g_Logger.Error("ss_xml_NodeGetText", e.message);
          }
          return "";
        },

        /**
        *  @name GetNodeTextCasted
        *  @memberOf $ss.agentcore.dal.xml
        *  @function
        *  @description  Retrieves the CDATA or Text of the specified node, and casts <br/>
        *                the value back into a native JavaScript type.
        *  @param oNode element to retrieve the data from
        *  @param sType type specified upstream.
        *  @returns Native JavaScript object for the specified oNode's text.
        *  @example
        *         var $xml = $ss.agentcore.dal.xml;
        *         var xpathNode = "/sprt/configuration[@name='clientui']/config-section[@name='global']/config[@name='reglookup']"; 
        *         var ret = $xml.GetNodes(xpathNode,"",domClientUI);
        *         var text = $xml.GetNodeTextCasted(ret[0],'boolean');
        */
        GetNodeTextCasted:function  ( oNode, sType)
        {
          _logger.info('Entered function: $ss.agentcore.dal.xml.GetNodeTextCasted');
          try
          {
            //[MAC] Check Machine OS Windows/MAC
            if(bMac)
              _xml.cleanNode(oNode);

              var sText = this.GetNodeText( oNode );
              if (sText != "")
              {
                return this.CastString(sText, sType);
              }
              return sText; 
          }
          catch (e) {
            _exception.HandleException(e,'$ss.agentcore.dal.xml','GetNodeTextCasted',arguments);
            //g_Logger.Error("ss_xml_NodeGetTextCasted", e.message);
          }
          return "";
        },

        /**
        *  @name GetAttribute
        *  @memberOf $ss.agentcore.dal.xml
        *  @function
        *  @description  Retrieves the value of the attribute specified.
        *  @param oNode element to retrieve the data from
        *  @param sAttrName type specified upstream.
        *  @returns String value of the attribute.
        *  @example
        *         var $xml = $ss.agentcore.dal.xml;
        *         var xpathNode = "/sprt/configuration[@name='clientui']/config-section[@name='email']/config[@name='email_domains']"; 
        *         var ret = $xml.GetNodes(xpathNode,"",domClientUI);
        *         var attribute = $xml.GetAttribute(ret[0],"type");
        */
        GetAttribute:function ( oNode , sAttrName )
        {
          _logger.info('Entered function: $ss.agentcore.dal.xml.GetAttribute');
          try
          {
            //[MAC] Check Machine OS Windows/MAC
            if(!bMac)
              var sAttrValue  = oNode.getAttribute(sAttrName);
            else {
              var attribute  = oNode.getAttributeNode(sAttrName);
              var sAttrValue  = null;
              if (attribute) {
                sAttrValue  = oNode.getAttributeNode(sAttrName).nodeValue;
              }
              _logger.info('Entered function: $ss.agentcore.dal.xml.GetAttribute of ' + sAttrName + '= ' + sAttrValue);
            }
            return sAttrValue;
          } catch (e) {
            _exception.HandleException(e,'$ss.agentcore.dal.xml','GetAttribute',arguments);
            //g_Logger.Error("ss_xml_AttributeGet", e.message);
          }
        },

        /**
        *  @name GetAttributeCasted
        *  @memberOf $ss.agentcore.dal.xml
        *  @function
        *  @description  Retrieves the value of the attribute specified, aand casts
        *               the value back into a native JavaScript type.
        *  @param oNode element to retrieve the data from
        *  @param sType type specified upstream.
        *  @returns String value of the attribute.
        *  @example
        *         var $xml = $ss.agentcore.dal.xml;
        *         var xpathNode = "/sprt/configuration[@name='clientui']/config-section[@name='email']/config[@name='email_domains']"; 
        *         var ret = $xml.GetNodes(xpathNode,"",domClientUI);
        *         var attribute = $xml.GetAttributeCasted(ret[0],"type");
        */
        GetAttributeCasted:function ( oNode, sAttrName )
        {
          _logger.info('Entered function: $ss.agentcore.dal.xml.GetAttributeCasted');
          try
          {
            var sValue = this.GetAttribute(oNode,sAttrName);
            return this.CastString(sValue);
          } catch (e) {
            _exception.HandleException(e,'$ss.agentcore.dal.xml','GetAttributeCasted',arguments);
            //g_Logger.Error("ss_xml_AttributeGetCasted", e.message);
          }
        },

        /**
        *  @name AddAttribute
        *  @memberOf $ss.agentcore.dal.xml
        *  @function
        *  @description  Adds an attribute to an existing XML Node.
        *  @param oXML XML dom to update
        *  @param oNode element to add attribute to
        *  @param sAttrName name of attribute to create.
        *  @param sAttrValue value of newly created attribute.
        *  @returns true on success,false on failure
        *  @example
        *         var $xml = $ss.agentcore.dal.xml;
        *         var xpathNode = "/sprt/configuration[@name='clientui']"; 
        *         var ret = $xml.GetNodes(xpathNode,"",domClientUI);
        *         var ret = $xml.AddAttribute(domClientUI, ret[0],"testName","testVal");
        */
        AddAttribute:function ( oXML, oNode, sAttrName, sAttrValue )
        {
          _logger.info('Entered function: $ss.agentcore.dal.xml.AddAttribute');
          try 
          {
            var oAttribute = oXML.createAttribute(sAttrName);
            oAttribute.value = sAttrValue;
            oNode.attributes.setNamedItem(oAttribute);
          }catch(e) {
            _exception.HandleException(e,'$ss.agentcore.dal.xml','AddAttribute',arguments);
            //g_Logger.Error("ss_xml_AttributeAdd", "Unexpected Error " + e.message);  
          }
          return true;
        },

        /**
        *  @name SetAttribute
        *  @memberOf $ss.agentcore.dal.xml
        *  @function
        *  @description  Sets an existing attribute's value.
        *  @param oNode element to add attribute to
        *  @param sAttrName name of attribute to set.
        *  @param sAttrValue value of newly created attribute.
        *  @returns true/false
        *  @example
        *           var $xml = $ss.agentcore.dal.xml;
        *           var xpathNode = "/sprt/configuration[@name='clientui']"; 
        *           var ret = $xml.GetNodes(xpathNode,"",domClientUI);
        *           var ret = $xml.SetAttribute(ret[0],"testName","testValchanged");
        */
        SetAttribute:function ( oNode, sAttrName, sAttrValue )
        {
          _logger.info('Entered function: $ss.agentcore.dal.xml.SetAttribute');
          try {
            oNode.setAttribute(sAttrName, sAttrValue);
          }catch(e) {
            _exception.HandleException(e,'$ss.agentcore.dal.xml','SetAttribute',arguments);
            //g_Logger.Error("ss_xml_AttributeSet", "Unexpected Error " + e.message);  
          }
          return true;
        },

        /**
        *  @name JsToXml
        *  @memberOf $ss.agentcore.dal.xml
        *  @function
        *  @description  Converts the members of a JavaScript object or array into XML <br/>
        *                Elements as members of the specified XML Node.
        *  @param oNode The containing node, where all new elements are appended.
        *  @param xJS The JavaScript object to convert.
        *  @returns string
        *  @example
        *         var $xml = $ss.agentcore.dal.xml;
        *         var objJS = new Object();
        *         objJS["test1"] = "test1";
        *         objJS["test2"] = true;
        *     
        *         var element = domClientUI.createElement("JStoXMLNode");
        *         var xml = $xml.JsToXml(element,objJS);
        */
        JsToXml : function ( oNode, xJS )
        {
          _logger.info('Entered function: $ss.agentcore.dal.xml.JsToXml');
          try
          {
            if (typeof(xJS) != 'undefined' && xJS != null)
            {  
              var sType = this.GetTypeForConversion(xJS);
              
              if (sType == 'object')
              {
                oNode.setAttribute("jstype", "object");
                for (var i in xJS)
                {
                  if (xJS.hasOwnProperty(i))
                  {
                    _JsToXml_AddNode( oNode, xJS, i );
                  }
                }
              }
              else if (sType == 'array')
              {
                oNode.setAttribute("jstype", "array");
                for (var i=0; i<xJS.length; i++)
                {
                  _JsToXml_AddNode( oNode, xJS, i );
                }
              }
              
              var oTargetJS = {};
              
              this.XmlToJs(oNode, oTargetJS);
              
              return true;
            }
          } catch (e) {
            _exception.HandleException(e,'$ss.agentcore.dal.xml','JsToXml',arguments);
          }
        },

        
        /**
        *  @name SetNodeElementText
        *  @memberOf $ss.agentcore.dal.xml
        *  @function
        *  @description  Based on the result of ss_xml_GetTypeForConversion, set the <br/>
        *                text of the specified node to the string representation of xText. 
        *  @param oNode The containing node, where all new elements are appended.
        *  @param xText The JavaScript object to cast to a string.
        *  @returns none
        *  @example
        *
        *
        */
        SetNodeElementText :function ( oNode, xText )
        {
          _logger.info('Entered function: $ss.agentcore.dal.xml.SetNodeElementText');
          try
          {
            var sType = this.GetTypeForConversion(xText);
            oNode.setAttribute("jstype", sType);

            switch (sType)
            {
              case 'null'         : return this.SetTextAsCData(oNode, sType);
              case 'undefined'    : return this.SetTextAsCData(oNode, sType);
              case 'string'       : return this.SetTextAsCData(oNode, xText);
              case 'stringobject' : return this.SetTextAsCData(oNode, xText);
              case 'array'        : return this.JsToXml(oNode, xText);
              case 'object'       : return this.JsToXml(oNode, xText);
              case 'number'       : return this.SetTextAsCData(oNode, xText);
              case 'function'     : return this.SetTextAsCData(oNode, xText);
              case 'date'         : return this.SetTextAsCData(oNode, xText.toUTCString());
              case 'boolean'      : return this.SetTextAsCData(oNode, xText);

              default :
                  //g_Logger.Error("ss_xml_NodeSetElementText", "Unknown Type " + sType);
                  return this.SetTextAsCData(oNode, xText);
                break;
            }
          }
          catch (e)
          {
            _exception.HandleException(e,'$ss.agentcore.dal.xml','SetNodeElementText',arguments);
            //g_Logger.Error("ss_xml_NodeSetElementText", "Unexpected Error " + e.message);
          }
          return false;
        },

        /**
        *  @name SetTextAsCData
        *  @memberOf $ss.agentcore.dal.xml
        *  @function
        *  @description  Adds a CDATA section to oNode containing sText.
        *  @param oNode The XML Element to append to.
        *  @param sText The text of the CDATA section.
        *  @returns true/false
        *  @example
        *
        *
        */
        SetTextAsCData:function ( oNode, sText )
        {
          _logger.info('Entered function: $ss.agentcore.dal.xml.SetTextAsCData');
          try
          {
            var oXmlDoc = oNode.ownerDocument;
            var oCData = oXmlDoc.createCDATASection(new String(sText));
            for (var i=0; i<oNode.childNodes.length; i++)
              oNode.removeChild(oNode.childNodes[i]);
            oNode.appendChild(oCData);
            return true;
          }
          catch (e)
          {
            _exception.HandleException(e,'$ss.agentcore.dal.xml','SetTextAsCData',arguments);
            //g_Logger.Error("ss_xml_SetTextAsCData", "Unexpected error " + e.message);
          }
          return false;
        },

        /**
        *  @name XmlToJs
        *  @memberOf $ss.agentcore.dal.xml
        *  @function
        *  @description  Converts an XML Node (generally created through JsToXml) <br/>
        *                into a deep JavaScript object.
        *  @param oNode The XML Element to enumerate.
        *  @param xJS The JS object to add the attributes and members into.
        *  @param bSkipAttributes Controls whether or not the 'attributes' <br/>
        *         collection is populated as a member of xJS.
        *  @returns Javascript object
        *  @example
        *           var $xml = $ss.agentcore.dal.xml;
        *           var xpathNode = "/sprt/configuration[@name='clientui']/config-section[@name='global']"; 
        *           var ret = $xml.GetNodes(xpathNode,"",domClientUI);
        *           var objJS = new Object();
        *           var obj = $xml.XmlToJs(ret[0],objJS,true);
        */
        XmlToJs:function ( oNode, xJS, bSkipAttributes )
        {
          _logger.info('Entered function: $ss.agentcore.dal.xml.XmlToJs');
          try
          {
            if (! xJS) {
              var sType = this.GetAttribute(oNode, "jstype");
              if (sType == 'array') {
                xJS = [];
              } else if (sType == 'object') {
                xJS = {};
              } else {
                //g_Logger.Error("ss_xml_XmlToJs", "Unknown Type " + sType);
                return false;
              }
            }
            if (xJS)
            {
              xJS['attributes'] = {};
              var sName,xValue,sType;
              
              if (bSkipAttributes != true && oNode.attributes)
              {
                var aAttributes = oNode.attributes;
                for (var i=0; i<aAttributes.length; i++)
                {
                  sName  = aAttributes[i].nodeName;
                  xValue = aAttributes[i].nodeValue;
                  xJS['attributes'][sName] = xValue;
                }
              }
              
              var aChildren = oNode.childNodes;
              var oNextChild = null;
              for (var i=0; i<aChildren.length; i++)
              {
                oNextChild = aChildren[i];
                if(oNextChild.nodeType != 1) continue; //skip on incompatible nodes
                sName  = oNextChild.nodeName;
                //[MAC] Check Machine OS Windows/MAC
                if(!bMac)
                  sType  = oNextChild.getAttribute("jstype") || "string";
                else
                  sType  = oNextChild.getAttributeNode("jstype").nodeValue || "string";
                
                if (sType in {object:1,array:1}) {
                  this.XmlToJs(oNextChild, xJS[sName]);
                }
                else
                {
                  xValue = this.GetNodeTextCasted(oNextChild, sType);
                  xJS[sName] = xValue;
                }
              }
            }
            return xJS;
          } 
          catch (e)
          {
            _exception.HandleException(e,'$ss.agentcore.dal.xml','XmlToJs',arguments);
          }
          return null;
        },

        /**
        *  @name GetTypeForConversion
        *  @memberOf $ss.agentcore.dal.xml
        *  @function
        *  @description  Returns the type used for bidirectional conversion between <br/>
        *                js and xml.
        *  @param xJS The object to check the type of.
        *  @returns string classifying the type of object.
        *  @example
        *         var $xml = $ss.agentcore.dal.xml;
        *         var type = $xml.GetTypeForConversion("test");
        */
        GetTypeForConversion:function ( xJS )
        {
          _logger.info('Entered function: $ss.agentcore.dal.xml.GetTypeForConversion');
          try
          {
            if (xJS == null)
              return 'null';
            if (xJS == undefined)
              return 'undefined';

            switch (xJS.constructor.toString())
            {
              case _constants.STRING_CONSTRUCTOR   : return 'string';
              case _constants.STRING2_CONSTRUCTOR  : return 'stringobject';
              case _constants.ARRAY_CONSTRUCTOR    : return 'array';
              case _constants.OBJECT_CONSTRUCTOR   : return 'object';
              case _constants.NUMBER_CONSTRUCTOR   : return 'number';
              case _constants.FUNCTION_CONSTRUCTOR : return 'function';
              case _constants.DATE_CONSTRUCTOR     : return 'date';
              case _constants.TRUE_CONSTRUCTOR     : return 'boolean';
              case _constants.FALSE_CONSTRUCTOR    : return 'boolean';

              default :
                //g_Logger.Error("ss_xml_GetTypeForConversion", "Unknown Constructor " + xJS.constructor.toString());
                
                // blind test for enumerable objects
                for (var test in xJS) 
                { 
                  return 'object'; 
                }
                
                return 'string';
            }
            return 'string';
          } catch (e) {
            _exception.HandleException(e,'$ss.agentcore.dal.xml','GetTypeForConversion',arguments);
          }
        },

        /**
        *  @name CastString
        *  @memberOf $ss.agentcore.dal.xml
        *  @function
        *  @description  Casts a string into the specified sType, or determines a type <br/>
        *                based on the string value itself.  For instance, "true" is casted <br/>
        *                back to a native JavaScript Boolean object. 
        *  @param sStringValue a string represting the value
        *  @param sType the specified type to cast the value into.
        *  @returns based on sType.
        *  @example
        *
        *
        */
        CastString :function ( sStringValue, sType )
        {
          _logger.info('Entered function: $ss.agentcore.dal.xml.CastString');
          try
          {
            switch (sType)
            {
              case 'null'         : return null;
              case 'undefined'    : return undefined;
              case 'string'       : return sStringValue;
              case 'stringobject' : return new String(sStringValue);
              case 'number'       : return sStringValue * 1;
              case 'array'        : return sStringValue;
              case 'object'       : return sStringValue;
              case 'function'     : return sStringValue;
              case 'date'         : return new Date(sStringValue);
              case 'boolean'      :
                  if (sStringValue == 'true'  || sStringValue == '1')
                    return true;

                  if (sStringValue == 'false' || sStringValue == '0')
                    return false;

                return new Boolean(sStringValue);

              default :
                  if ( ! isNaN( sStringValue*1 ) ) {
                    return sStringValue*1;
                  } else if (sStringValue == "null") {
                    return null;
                  } else if (sStringValue == "undefined") {
                    return undefined;
                  } else if (sStringValue == "true") {
                    return true;
                  } else if (sStringValue == "false") {
                    return false;
                  }
                break;
            }
            return sStringValue;
          } catch (e) {
            _exception.HandleException(e,'$ss.agentcore.dal.xml','CastString',arguments);
          }
        },

        /**
        *  @name BuildAttributeForXPath
        *  @memberOf $ss.agentcore.dal.xml
        *  @function
        *  @description  Builds an XPath Attribute Argument Statement
        *  @param sName Attribute Name
        *  @param aMatches string or overload as an array of matches
        *  @param sOperator string XPath operator (" AND ", " OR ")
        *  @returns XPath string
        *  @example
        *        In:  "name", "foo", " OR "
        *          Out:  "(@name='foo')"
        *        In:  "name", ["foo", "bar"], " OR "
        *          Out:  "(@name='foo' OR @name='bar')"
        *        In:  "name", ["foo", "bar"], " AND "
        *          Out:  "(@name='foo' AND @name='bar')"
        */
        BuildAttributeForXPath:function ( sName, aMatches, sOperator )
        { 
          _logger.info('Entered function: $ss.agentcore.dal.xml.BuildAttributeForXPath');
          try
          {
            sOperator = " " + (sOperator || "OR").replace(new RegExp("\ ", "g"), "") + " ";
            if (aMatches.constructor.toString() == _constants.ARRAY_CONSTRUCTOR)
            {
              var aReturns = [];
              for (var i=0; i<aMatches.length; i++)
              {
                aReturns.push( "@"+sName+"='"+aMatches[i]+"'" );
              }
              return "("+aReturns.join(sOperator)+")";
            }
            return "@"+sName+"='"+aMatches+"'";
          } 
          catch (e) {
            _exception.HandleException(e,'$ss.agentcore.dal.xml','BuildAttributeForXPath',arguments);
          }
        },
        
        /**
        *  @name GetNode
        *  @memberOf $ss.agentcore.dal.xml
        *  @function
        *  @description  Retrieves an xml element from the specified oXmlDocumentElement
        *  @param sElementXPath xpath argument 1
        *  @param sAttributeXPath xpath argument 2, concated to 1
        *  @param oXmlDocumentElement xml document to query
        *  @returns String text of the node, or "" if null or text not found.
        *  @example
        *           var $xml = $ss.agentcore.dal.xml;
        *           var xpathNode = "/sprt/config_to_use"; 
        *           var ret = $xml.GetNode(xpathNode,"",domClientUI);
        */
        GetNode:function( sElementXPath , sAttributeXPath , oXmlDocumentElement )
        {
          _logger.info('Entered function: $ss.agentcore.dal.xml.GetNode');
          try
          {
            sElementXPath   = sElementXPath   || "";
            sAttributeXPath = sAttributeXPath || "";
            if (oXmlDocumentElement != null) {
              var xNodes = _xml.GetNodes(sElementXPath , sAttributeXPath , oXmlDocumentElement);
          if (xNodes && xNodes.length)
                return xNodes[0];
              }
            }
          catch (ex)
          {
            _exception.HandleException(ex,'$ss.agentcore.dal.xml','GetNode',arguments);
          }
          return null;
        },

        SelectNodes:function (xmlDoc, xPath) {
          //[MAC] Check Machine OS Windows/MAC
          if(bMac) {
            if (xmlDoc == null) return null;
            _xml.cleanNode(xmlDoc);
            var condition = xPath;
            condition = condition.substring(0, 1) == "/" ? condition.substring(1, condition.length) : condition;
            // condition = condition.replace(new RegExp("/","g"), ">").replace(new RegExp("@","g"), "");
            // return $(xmlDoc).find(condition);
            if (xmlDoc.evaluate) {
              var nsResolver = null;
              if (xmlDoc.createNSResolver) {
                   nsResolver = xmlDoc.createNSResolver (xmlDoc.documentElement);
              }
              _logger.info('createNSResolver:  = ' + nsResolver);
              var nodes = xmlDoc.evaluate(condition, xmlDoc, nsResolver, XPathResult.ANY_TYPE, null);
              var node, finalNodes = []
              var result = nodes.iterateNext();
              while (result) {
                finalNodes.push(result);  
                _logger.info('SelectNodes:  = ' + result.nodeName);
                result = nodes.iterateNext();
              }  
              if (finalNodes.length == 0) {
                return null;
              }
              return finalNodes;
            }else{
              _logger.info('failed to evaluate = ' + condition);
            }
          }
          return null;
        },

        SelectSingleNode: function (xmlDoc, xPath) {
          //[MAC] Check Machine OS Windows/MAC
          if(bMac) {
            if (xmlDoc == null) return null;
            //   var nodes = this.SelectNodes(xmlDoc, xPath);
            //   if (nodes.length > 0) {
            //     return nodes[0];
            //   }
            // return null;
            // _xml.cleanNode(xmlDoc);

            if (xmlDoc.evaluate) {
              var nsResolver = null;
              if (xmlDoc.createNSResolver) {
                nsResolver = xmlDoc.createNSResolver (xmlDoc.documentElement);
              }
              var nodes = xmlDoc.evaluate(xPath, xmlDoc, nsResolver, XPathResult.ANY_TYPE, null);
              var results = nodes.iterateNext();
              return results;
            }
          }
          return null;
        },
      /*******************************************************************************
    ** Name:        SelectSingleNodeFromNode
    **    
    ** Purpose:      
    **              
    **              
    **              
    **    
    ** Parameter:   inNode, sAttributeName, sAttributeValue
    **    
    **    
    **              
    ** Return:      none
    *******************************************************************************/
    SelectSingleNodeFromNode: function(inNode, sAttributeName, sAttributeValue) {
      //[MAC] Check Machine OS Windows/MAC
      if(bMac) {
        if (inNode) {
          for (i = 0 ; i < inNode.childNodes.length ; i++) {
            var node = inNode.childNodes[i];
            if (node.attributes.length > 0) {
              if (node.attributes[0].nodeValue == sAttributeValue) {
                return node;
              }
            }
          }
        }
      }
      return null;
    },

    cleanNode: function (node)
    {
      //[MAC] Check Machine OS Windows/MAC
      if(bMac) {
        for(var n = 0; n < node.childNodes.length; n ++)
        {
          var child = node.childNodes[n];
          if
          (
            child.nodeType === 8 
            || 
            (child.nodeType === 3 && !/\S/.test(child.nodeValue))
          )
          {
            // _logger.info('cleanNode = ' + child.nodeValue);
            node.removeChild(child);
            n --;
          }
          else if(child.nodeType === 1)
          {
            _xml.cleanNode(child);
          }
        }
      }
    }
      });
      

      var _xml = $ss.agentcore.dal.xml;
      var _constants = $ss.agentcore.constants;
      var _logger = $ss.agentcore.log.GetDefaultLogger("$ss.agentcore.dal.xml");  
      var _exception = $ss.agentcore.exceptions;
      var _utils = $ss.agentcore.utils;
      var _file = $ss.agentcore.dal.file; 
      //[MAC] Get Machine OS type
      var bMac = $.browser.safari;

      //[MAC] JSBridge object used only for MAC
      try {
        var _jsBridge = window.JSBridge;
      }
      catch(e) {
      }
      
      /////////////////////////// Private functions /////////////////////////////////

      function _OnXMLResponse(oDOM,fnCallBack)
      {
        // test if xml was fully loaded (COMPLETED(4))
        if (oDOM.readyState==4) 
        {
          if (oDOM.documentElement != null) 
          {
            //g_Logger.Info("ss_utl_pvt_OnXMLResponse", oDOM.xml, true);
            // check if caller asked for results
            if (oDOM.async && fnCallBack) 
            {
              fnCallBack(oDOM);
            } 
            else 
            {
              return oDOM;
            }
          } 
          else 
          {
            // XML loading finished but still the doc element is null.
            // This could happen if the page being hit is in error or unreachable.
            if (oDOM.parseError.errorCode != 0) 
            {
              if (oDOM.async && fnCallBack) 
              { // caller wants to handle it too
                fnCallBack(null);
              } 
              else 
              {
                oDOM = null;
                return null;
              }
            }
          }
        }
      }

      /*******************************************************************************
      ** Name:        ss_xml_pvt_JsToXml_AddNode
      **
      ** Purpose:     Private helper for ss_utl_JsToXml which adds an element if it is
      **              not found, and then sets the text to the specified members value.
      **
      ** Parameter:   oNode   - The containing node, where all new elements are appended.
      **              xJS     - The JavaScript object which contains the xMember
      **              xMember - The member of xJS to add.
      **
      ** Return:     true/false
      *******************************************************************************/
      function _JsToXml_AddNode( oNode, xJS, xMember )
      {
        try
        {
          var oNewNode = _xml.GetNode(oNode, xMember);
          if (! oNewNode )
          {
            var oXmlDoc = oNode.ownerDocument;
            oNewNode = oXmlDoc.createElement(xMember);
            oNode.appendChild(oNewNode);
          }
          return _xml.SetNodeElementText( oNewNode, xJS[xMember] );
        } catch (e) {
          throw(e);
        }
      }
      
    })();
