
function Dictionary() {
  this.Count = 0;
}
 
Dictionary.prototype.add = function (key, value) {
  if (!this.Exists(key)) {
    this.Count++;
  }
  this[key] = value;
}
 
Dictionary.prototype.Item = function (key) {
  return this.Exists(key) ? this[key] : undefined;
}
 
Dictionary.prototype.Exists = function (key) {
  return this.hasOwnProperty(key);
}
 
Dictionary.prototype.Keys = function () {
  var keys = [];
  for (var k in this) {
    if (this.Exists(k)) {
      keys.push(k);
    }
  }
  return keys;
}
 
Dictionary.prototype.Items = function () {
  var values = [];
  for (var k in this) {
    if (this.Exists(k)) {
      values[k] = this[k];
    }
  }
  return values;
}
 
Dictionary.prototype.remove = function (key) {
  if (this.Exists(key)) {
    previous = this[key];
    this.Count--;
    delete this[key];
    return previous;
  }
  else {
    return undefined;
  }
}
 
Dictionary.prototype.RemoveAll = function () {
  for (var k in this) {
    if (this.Exists(k)) {
      delete this[k];
    }
  }
  this.Count = 0;
}
/*
function doSomething()
{
  var table = new Dictionary();
  table.Add("White", "#ffffff");
  table.Add("Red", "#ff0000");
  table.Add("Green", "#00ff00");
  alert(table.Count); // 3
  alert(table.Exists("Red") + ", " + table.Exists("Blue")); // true, false
  table["Red"] = "#f00";
  alert(table["White"] + ", " + table.Item("Red") + ", " + table.Items()["Green"]); // #ffffff, #f00, #00ff00
table.Remove("Red");
  alert(table.Count); // 2
  alert(table["Red"]); // undefined
  table.RemoveAll();
  alert(table.Count); // 0
  alert(table.Item("Green")); // undefined
}*/