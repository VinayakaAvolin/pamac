/** @namespace Provides all network related functionality*/
$ss.agentcore.network = $ss.agentcore.network || {};
/** @namespace Holds all network related functionality*/
$ss.agentcore.network.network = $ss.agentcore.network.network || {};

(function()
{
    $ss.agentcore.network.network = Class.extend(
    {
      // prototype methods
      init:function()
      {
        this.objNetCheck = _utils.CreateActiveXObject("SPRT.SdcNetCheck");

        this.Probe = new $ss.agentcore.dna.protectrestore.Probe;

        this.filterMissing  = true;
        this.selectedAdapter = ""; 
        this.Refresh();

        return this; 
      },

      /**
      *  @name Refresh
      *  @memberOf $ss.agentcore.network.network
      *  @function
      *  @description standard way of refreshing the Network object.
      *  @param None
      *  @returns None
      *  @example
      *           var objNet = new $ss.agentcore.network.network();
      *           objNet.Refresh();
      */
      Refresh : function( ) 
      {
        this.cachedBasicNetworkTest = -1; 

        // get info (DHCP enabled) on adapter cards
        var cardArr;

        if (this.filterMissing) 
          cardArr = _netCheck.GetPresentAdapters(this.objNetCheck, true, false);
        else 
          cardArr = _netCheck.GetEthernetCards(this.objNetCheck);

        this.cardsProps = _netCheck.GetAdaptersProperties(this.objNetCheck, cardArr, "DhcpEnabled,NameServer,IPAddress");

        // loop through cards and fill in extra properties (name server, physical medium)
        for (card in this.cardsProps)
        {
          var currentCardProps = this.cardsProps[card];

          currentCardProps["PhysicalMedium"] = _netCheck.GetAdaptersPhysicalMedium(this.objNetCheck, card);
          currentCardProps["NameServer"] = _netCheck.GetAdapterNameServer(card);
          currentCardProps["ConnectionName"] = _netCheck.GetAdapterConnectionName(card);
        }
      },

      /**
      *  @name SendMsg
      *  @memberOf $ss.agentcore.network.network
      *  @function
      *  @description Broadcast the message
      *  @param msg Message to broadcast
      *  @returns true on broadcast else null
      *  @example
      *           var objNet = new $ss.agentcore.network.network();
      *           objNet.SendMsg("SYSTEM_RESTART");
      */
      SendMsg : function (msg) 
      { 
          try {  
            _event.Send(msg);
            return true;
          } catch (ex) { }
        return null;
      },

      /**
      *  @name BasicConnectionTest
      *  @memberOf $ss.agentcore.network.network
      *  @function
      *  @description Tests for basic internet connection
      *  @param bCancelBroadcastConnectionStatus
      *  @param sTestingURLHostname                   
      *  @param sSpoofedDNSTestHostname               
      *  @param sSocketTestAddress                  
      *  @param xHTTPTestMethod                     
      *  @param sHTTPTempFile                   
      *  @param bSwapIPForHostname
      *  @param bRefreshBrowserSettingsBeforeHTTPTest
      *  @param bPerformDNSBeforeHTTP        
      *  @param bSocketTestAutomaticProxyServers
      *  @param bFailOnSpoofedResult
      *  @param iDNSTimeout                            
      *  @param iHTTPTimeout
      *  @param iExternalAbortRequestTimeout
      *  @param iDNSIterations 
      *  @param iHTTPIterations
      *  @param iSpoofedDNSTestIterations   
      *  @returns true if there is a internet connection else false
      *  @example
      *           var objNet = new $ss.agentcore.network.network();
      *           var ret = objNet.BasicConnectionTest(false, "http://www.supportsoft.com/", "verify.modem.is.not.spoofing.dns.sprt", "NONE", "GET",
      *                                "%TEMP%\clientui.test", true, true, true, true, true, true, "5000", "5000", "1000", "2", "2", "1");
      */
      BasicConnectionTest : function ( 
          bCancelBroadcastConnectionStatus,       // null | true | false :: default false :: if true SendMsg will not be called.  This is argument 0 to support legacy code.
          sTestingURLHostname,                    // null | String Host  :: default false :: no protocol, no path, just the hostname like http://www.supportsoft.com/.
          sSpoofedDNSTestHostname,                // null | String Path  :: default verify.modem.is.not.spoofing.dns.sprt :: a hostname that should not resolve without DNS Spoofing.
          sSocketTestAddress,                     // null | String Path  :: default NONE  :: address to test via socket if Automatic Proxy is enabled.  default: none
          xHTTPTestMethod,                        // null | GET | EXISTS :: default GET   :: method to use for HTTP requests
          sHTTPTempFile,                          // null | String Path  :: default %TEMP%\clientui.test :: Temporary file to write out response from HTTP Requests.
          bSwapIPForHostname,                     // null | true | false :: default true  :: If testing DNS before HTTP, and true, use IP address for host
          bRefreshBrowserSettingsBeforeHTTPTest,      // null | true | false :: default true  :: Refresh BCont's IE settings before testing?  If IE was set to "work offline" when we created our instance
                                                    // but was cleared outside of BCont, this will make BCont pick up those new settings before the test.
          bResetBrowserCachingOptionsBeforeHTTPTest,  // null | true | false :: default true  :: if true force IE to avoid the cache on our request.
          bPerformDNSBeforeHTTP,                  // null | true | false :: default true  :: if true perform DNS requests before HTTP requests to confirm network connectivity.  
                                                    // set this to false in a high proxy environment where only the proxy server knows how to resolve the TEST url.
                                                    // setting this to true will make our HTTP request timeout more reliable as the HTTP request also has to perform the DNS lookup before 
                                                    // putting the request on the wire.
          bSocketTestAutomaticProxyServers,       // null | true | false :: default true  :: if true and IE is set to use an automatic proxy configuration, then socket test sSocketTestAddress first.
                                                    // if the socket test passes, BasicConnectionTest passes.  if it fails we continue with the rest of the tests.
          bFailOnSpoofedResult,                   // null | true | false :: default true  :: if true and the IP address for sSpoofedDNSTestHostname is equal to the IP of sTestingURLHostname then consider us offline.
          iDNSTimeout,                            // null | Integer Timeout in MS :: Default 5000 :: Timeout for each DNS request put on the wire.
          iHTTPTimeout,                           // null | Integer Timeout in MS :: Default 5000 :: Timeout for each HTTP request put on the wire. 
          iExternalAbortRequestTimeout,           // null | Integer Timeout in MS :: Default 1000 :: How long to wait for threads to complete themselves beyond our timeout before returning.
          iDNSIterations,                         // null | Integer Iterations    :: Default 2    :: How many total DNS request iterations to try before failing.
          iHTTPIterations,                        // null | Integer Iterations    :: Default 2    :: How many total HTTP request iterations to try before failing.
          iSpoofedDNSTestIterations               // null | Integer Iterations    :: Default iDNSIterations :: How many total DNS requests with the Spoofed address to try before "passing" the spoof test.
          )
      {
        /*
          Examples: 
          
            //
            // Test using Default Paramters in configHash and broadcast through SendMsg the connection status.
            Network.BasicConnectionTest(); 
            
            //
            // TEST GOOGLE, No DNS Tests, No Spoof Tests.
            Network.BasicConnectionTest(true,"http://www.google.com/",null,null,null,null,null,false,true,true,true,false,false,15000,15000,1000,null,2,null);
            
            //
            // TEST GOOGLE's Service Login, No DNS Tests, No Spoof Tests.
            Network.BasicConnectionTest(true,"http://www.google.com/","https://www.google.com/accounts/ServiceLogin",null,null,null,null,false,true,true,true,false,false,15000,15000,1000,null,2,null);

        */  
        if (!this.objNetCheck) { return false; }

        var bSuccess    = "";
        var sDNSResult  = "";
        var sHomeRunURL = "";
        
        //
        // utility function to determine acceptable parameters.
        function set() {
          var o = {};
          for(var i=0;i<arguments.length; i++) o[arguments[i]]=true;
          return o;
        }

        

        // 
        // Determine Testing Parameters and Defaults
        //
        
        //
        // Configure URL and Hostnames
        sTestingURLHostname = sTestingURLHostname || "";
        if (! sTestingURLHostname) {
        }
        
        sSpoofedDNSTestHostname = sSpoofedDNSTestHostname || "verify.modem.is.not.spoofing.dns.sprt";

        //
        // How does this site get tested, GET, EXISTS?
        // sprt_HTTPRequest will convert xHTTPTestMethod into "GET" if 
        // it doesn't match one of the known request types.
        //
        xHTTPTestMethod = xHTTPTestMethod;

        //
        // Temp file for GETS, not used for EXISTS (HTTP HEAD)
        if (typeof(sHTTPTempFile) == 'undefined'  || ! sHTTPTempFile) {
          sHTTPTempFile = (_ini.GetPath("%TEMP%")+"\\clientui.test").replace(/\\\\/gi, '\\');;
        }

        //
        // Configure Boolean and Pivot options
        if (typeof(bCancelBroadcastConnectionStatus) == 'undefined' || 
          ! (bCancelBroadcastConnectionStatus in set(true,false)) ) {
          bCancelBroadcastConnectionStatus = false;
        }
        //
        // Does BasicConnectionTest broadcast status messages?
        var bBroadcastConnectionStatus = ! (bCancelBroadcastConnectionStatus); // Set a more readable version

        // 
        // Use the IP in the request instead of the hostname?  Avoids some caching issues
        // but is not possible against server configurations that expect explicit host headers.
        //
        if ( !(bSwapIPForHostname in set(true,false)) )
          bSwapIPForHostname = true;
        
        //
        // Pre-Test the environment by making a DNS request before the HTTP test?
        // In a heavy proxy environment a host may not resolve on the local machine
        // but may resolve OK through the proxy server.  
        // This should be set to false in those environments.
        //
        if (bPerformDNSBeforeHTTP == false && bSwapIPForHostname == true) 
          bPerformDNSBeforeHTTP = true; // override, since these options would conflict.

        // 
        // Ensure our current instance of IE is using any setting that has been 
        // set outside of our instance, such as disabling or enabling of "Work Offline".
        // 
        if ( !(bRefreshBrowserSettingsBeforeHTTPTest in set(true,false)) )
          bRefreshBrowserSettingsBeforeHTTPTest = true;

        //
        // Set the browser to "ALWAYS GET THE PAGE" , versus any internal caching
        // to make the HTTP result more reliable.
        if ( !(bResetBrowserCachingOptionsBeforeHTTPTest in set(true,false)) )
          bResetBrowserCachingOptionsBeforeHTTPTest = true;

        //
        // When an Automatic Proxy Configuration URL is configured
        // perform a socket test to a specific host instead of a possibly expensive 
        // HTTP request.
          if (! sSocketTestAddress ) {
            bSocketTestAutomaticProxyServers = false;
          } else { 
            bSocketTestAutomaticProxyServers = true;
          }

        if (typeof(bSocketTestAutomaticProxyServers) == 'undefined' || 
               ! (bSocketTestAutomaticProxyServers in set(true,false)) ) { 
          bSocketTestAutomaticProxyServers = false;
        }
        
        //
        // Determine when DNS requests are spoofed by a modem or other walled garden environment.
        // Set this to false if you want to test the result of any HTTP request, whether
        // the modem is masquerading as the site in question, or not. 
        // typically this will be set to true as we expect to test the REAL site, not just a 
        // local device.
        if ( !(bFailOnSpoofedResult in set(true,false)) )
          bFailOnSpoofedResult = true;

        //
        // Configure Default Timeouts
          if ( isNaN(parseInt(iDNSTimeout)) )
            iDNSTimeout = 5000;
          else 
            iDNSTimeout = parseInt(iDNSTimeout);
        
          if ( isNaN(parseInt(iHTTPTimeout)) )
            iHTTPTimeout = 5000;
          else 
            iHTTPTimeout = parseInt(iHTTPTimeout);

          if ( isNaN(parseInt(iExternalAbortRequestTimeout)) )
            iExternalAbortRequestTimeout = 1000;
          else 
            iExternalAbortRequestTimeout = parseInt(iExternalAbortRequestTimeout);

          if ( isNaN(parseInt(iDNSIterations)) )
            iDNSIterations = 2;
          else 
            iDNSIterations = parseInt(iDNSIterations);

          if ( isNaN(parseInt(iHTTPIterations)) )
            iHTTPIterations = 2;
          else 
            iHTTPIterations = parseInt(iHTTPIterations);

          if ( isNaN(parseInt(iSpoofedDNSTestIterations)) )
            iSpoofedDNSTestIterations = iDNSIterations;
          else 
            iSpoofedDNSTestIterations = parseInt(iSpoofedDNSTestIterations);
        
        //
        // Start Testing
        //  
        if (bBroadcastConnectionStatus) { 
          this.SendMsg({type:"SS_EVT_CONNECT_STATUS_TESTING"});
        } else { 
        }
        
        if (iExternalAbortRequestTimeout != -1) {
        try { 
          var currentAbortRequestTimeout = $ss.agentcore.utils.activex.GetObjInstance().AbortRequestTimeout;
          $ss.agentcore.utils.activex.GetObjInstance().AbortRequestTimeout = iExternalAbortRequestTimeout;
        } catch (e) { 
            iExternalAbortRequestTimeout = -1;
          }
        }

        try {

          if (bRefreshBrowserSettingsBeforeHTTPTest) { 
            _utils.RefreshBrowserSettings();
          } 
          else { 
          }

          if (bPerformDNSBeforeHTTP) {
            var xDNSIterationResult = "";
            for (var i=0; i<iDNSIterations; i++) { 
              xDNSIterationResult = _netCheck.ResolveHostname(this.objNetCheck, sTestingURLHostname, iDNSTimeout); 
              if (xDNSIterationResult in set("", -1)) { // FAILED
              } else { // SUCCEEDED
                sDNSResult = xDNSIterationResult;
                break;
              }
            }
            delete(xDNSIterationResult);

            if (sDNSResult == "") {
              bSuccess = false; // All Iterations Exhausted, DNS Resolution Failed
            }
          } else {
          }
          
          if (bSuccess !== false && (! bPerformDNSBeforeHTTP || sDNSResult)) { // OK to continue testing
          
            if ( bFailOnSpoofedResult && bPerformDNSBeforeHTTP && sDNSResult ) {

              var sFakeDNSResult = "";
              for (var i=0; i<iSpoofedDNSTestIterations; i++) {

                xFakeDNSIterationResult = _netCheck.ResolveHostname(this.objNetCheck, sSpoofedDNSTestHostname, iDNSTimeout); 

                if (xFakeDNSIterationResult in set("", -1))
                { // FAILED
                } 
                else 
                { // SUCCEEDED
                  sFakeDNSResult = xFakeDNSIterationResult;
                  if (sFakeDNSResult == sDNSResult) { 
                    bSuccess = false;
                    break; // DNS Is Spoofed, consider this a failure.
                  }
                }
              }
              delete(xFakeDNSIterationResult);
              delete(sFakeDNSResult);
            }
            
            if (bSuccess !== false) { // OK to continue testing

              // If the client is configured to do socket tests instead of HTTP requests for machines w/ auto-detect proxy,
              // check the proxy settings.  If set to auto-detect, run socket test and if successful, return success result
              if (bSocketTestAutomaticProxyServers) {

                var currProxyFlags       = _utils.GetProxyOption("", _constants.INTERNET_PER_CONN_FLAGS);
                var bAutoDetectSettings  = _utils.HasMask(currProxyFlags, _constants.PROXY_TYPE_AUTO_DETECT);

                if (bAutoDetectSettings) {
                  var addrArr = sSocketTestAddress.split(":");
                  var host = addrArr[0];
                  var port = 80;
                  if (addrArr.length > 1) port = parseInt(addrArr[1]);
                  if (_netCheck.TestConnection(this.objNetCheck, host, port, 5000)) {
                    bSuccess = true;
                  } else {
                  }
                }
              } else { 
              }
              
              if (bSuccess !== true && bSuccess !== false) {

                if (bResetBrowserCachingOptionsBeforeHTTPTest) { 
                  //set cache to every page, and force a refresh of the autoconfigurl
                  var cacheKey = "Software\\Microsoft\\Windows\\CurrentVersion\\Internet Settings"

                  var oldCacheLevel = 4;   // defaults to 4 which means "Automatically"
                  if (_registry.RegValueExists("HKCU", cacheKey, "SyncMode5"))
                  {
                    oldCacheLevel = _registry.GetRegValue("HKCU", cacheKey, "SyncMode5");         
                  }

                  _registry.SetRegValueByType("HKCU", cacheKey, "SyncMode5", 4, 3); 
                  _utils.SetProxyOption("", _constants.INTERNET_PER_CONN_AUTOCONFIG_RELOAD_DELAY_MINS, 0);
                }

                if ( bSwapIPForHostname == true ) sHomeRunURL = sDNSResult;
                else  sHomeRunURL = sTestingURLHostname;

                var xHTTPIterationResult = "";
                for (var i=0; i<iHTTPIterations; i++) {
                  xHTTPIterationResult = _http.HttpRequest(sHomeRunURL, xHTTPTestMethod, sHTTPTempFile, iHTTPTimeout); 

                  if (xHTTPIterationResult == true) { // SUCCEEDED
                    bSuccess = xHTTPIterationResult;
                    break;
                  }
                  else { // FAILED
                  } 
                }

                // clean up 
                _file.DeleteFile(sHTTPTempFile);

                if (bResetBrowserCachingOptionsBeforeHTTPTest) { 
                  //restore old WinINet cache mode
                  _registry.SetRegValueByType("HKCU", cacheKey, "SyncMode5", 4, oldCacheLevel); 
                }
              }
            }
          }
        }
        catch (ex) {
          bSuccess = false;
        }
        finally {
          if (bSuccess == "") {
            bSuccess = false;
          }
          try { 
            if (currentAbortRequestTimeout && currentAbortRequestTimeout > 0 && iExternalAbortRequestTimeout != -1) { 
              $ss.agentcore.utils.activex.GetObjInstance().AbortRequestTimeout = currentAbortRequestTimeout; 
            }
          } catch (ex) {
            // TODO
          }
        }

        if ( bBroadcastConnectionStatus ) {
          if ( bSuccess == true ) {
            this.SendMsg({type:"SS_EVT_CONNECT_STATUS_UP"});
          }
          else {
            this.SendMsg({type:"SS_EVT_CONNECT_STATUS_DOWN"});
          }
        } else {
        }

        if (typeof(BasicConnectionTestUnspecifiedError) != 'undefined') { 
        }

        return bSuccess;
      },

      /**
      *  @name CacheConnectionTest
      *  @memberOf $ss.agentcore.network.network
      *  @function
      *  @description Broadcast the message
      *  @param bCancelBroadcastConnectionStatus Message to broadcast
      *  @returns true on broadcast else null
      *  @example
      *           var objNet = new $ss.agentcore.network.network();
      *           objNet.SendMsg("SYSTEM_RESTART");
      */
      CacheConnectionTest : function(bCancelBroadcastConnectionStatus ) 
      {
        if (this.cachedBasicNetworkTest == -1) {
          if (! bCancelBroadcastConnectionStatus) { 
            this.SendMsg({type:"SS_EVT_CONNECT_STATUS_TESTING"});
          }
          this.cachedBasicNetworkTest = this.BasicConnectionTest(true);
        }
        if (this.cachedBasicNetworkTest) { 
          if (! bCancelBroadcastConnectionStatus) { 
            this.SendMsg({type:"SS_EVT_CONNECT_STATUS_UP"});
          }
        } else  {
          if (! bCancelBroadcastConnectionStatus) { 
            this.SendMsg({type:"SS_EVT_CONNECT_STATUS_DOWN"});
          }
        }
        return this.cachedBasicNetworkTest;
      },

      /**
      *  @name IsDNSSpoofed
      *  @memberOf $ss.agentcore.network.network
      *  @function
      *  @description Tests if DNS is spoofed or not
      *  @param sDNSHostname
      *  @param sDNSFakeHostname
      *  @param iDNSTimeout
      *  @returns True if DNS is spoofed else false
      *  @example
      *           var objNet = new $ss.agentcore.network.network();
      *           var ret = objNet.IsDNSSpoofed("10.2.0.101", "209.85.153.104", "5000");
      */
      IsDNSSpoofed : function(sDNSHostname, sDNSFakeHostname, iDNSTimeout ) 
      { 
        var bPrimaryHostDNS,bFakeHostDNS;
        
        sDNSHostname = sDNSHostname || "";
        if (!sDNSHostname) return false;
        
        sDNSFakeHostname = sDNSFakeHostname || "";
        if (!sDNSFakeHostname) return false;
        
        iDNSTimeout  = iDNSTimeout || 5000;


        // if no result, then dns isn't working, but at its not spoofed either.
        if (! (bPrimaryHostDNS = _netCheck.ResolveHostname(this.objNetCheck, sDNSHostname, iDNSTimeout))) 
          return false;

        // if the real address is equal to the fake address response, we're spoofed.
        if (bPrimaryHostDNS == (bFakeHostDNS = _netCheck.ResolveHostname(this.objNetCheck, sDNSFakeHostname, iDNSTimeout)))
          return true;
        
        // the results didn't match, we're not spoofed.
        return false;
      },

      /**
      *  @name CheckForDisabledNetworkCards
      *  @memberOf $ss.agentcore.network.network
      *  @function
      *  @description Returns the network cards which are disabled.
      *  @param None
      *  @returns true if there is Disabled network card else false
      *  @example
      *           var objNet = new $ss.agentcore.network.network();
      *           var ret = objNet.CheckForDisabledNetworkCards();
      */
      CheckForDisabledNetworkCards : function () 
      {
        //
        // check for any disabled network adapters. 
        //
        for (card in this.cardsProps)
        {
          if (this.IsAdapterPresent(card) && !_netCheck.IsAdapterEnabled(this.objNetCheck, card)) 
          {
            var currentCardProps = this.cardsProps[card];
            return true;
          }
        }
        return false;
      },

      /**
      *  @name IsCableConnectedToAdapter
      *  @memberOf $ss.agentcore.network.network
      *  @function
      *  @description Check for any adapters with a cable connected
      *  @param None
      *  @returns true if there is a adapter which is connected to cable else false
      *  @example
      *           var objNet = new $ss.agentcore.network.network();
      *           var ret = objNet.IsCableConnectedToAdapter();
      */
      IsCableConnectedToAdapter : function () 
      { 
        // 
        // check for any adapters with a cable connected
        // 
        for (card in this.cardsProps)
        {
          if (_netCheck.IsAdapterConnected(this.objNetCheck, card)) return true;
        }
        return false;
      },

//      CheckForWirelessAdapters : function () 
//      { 
//        // 
//        // returns true if a wireless adapter is present
//        // TODO: make sure this works on 98
//        // 
//        for (card in this.cardsProps)
//        {
//          if (swi_IsAdapterWireless(this.objNetCheck, card)) return true;
//        }
//        return false;
//      },

//      CheckForWirelessNetworks : function (adapter)
//      { 
//        var APs = swi_GetBssidListInfo(this.objNetCheck, adapter);      
//        for (var i in APs)
//        {   
//          return true;
//        }
//        return false;
//      },

      /**
      *  @name CheckForValidIP
      *  @memberOf $ss.agentcore.network.network
      *  @function
      *  @description Tests if there is  at least one adapter that has a valid IP.
      *  @param None
      *  @returns true if there is at least one adapter that has a valid IP else false
      *  @example
      *           var objNet = new $ss.agentcore.network.network();
      *           var ret = objNet.IsCableConnectedToAdapter();
      */
      CheckForValidIP : function()
      {
        // 
        // return true if there is at least one adapter has a valid IP
        //
        for (card in this.cardsProps)
        {
          var currentCardProps = this.cardsProps[card];  
          var ip = currentCardProps["IPAddress"];
          if (ip && (ip.toString().indexOf("0.0.0.0") < 0) && (ip.toString().substring(0, 4).indexOf("169.") == -1))
          {
            return true;
          }
        }
        return false;
      },

      /**
      *  @name IsDHCPEnabled
      *  @memberOf $ss.agentcore.network.network
      *  @function
      *  @description Tests if DHCP is for the specific network card.
      *  @param sAdapterServiceGUID Guid of the network adapter
      *  @param currentCardProps Properties of the network adapter
      *  @returns true if DHCP enbled for the specific network adapter else false
      *  @example
      *          
      */
      IsDHCPEnabled : function (sAdapterServiceGUID, currentCardProps) 
      { 
        if (typeof(sAdapterServiceGUID) == 'undefined') 
        {
          return false;
        }

        // 9x and Vista machines always report "DhcpEnabled = No" when cable is unplugged or wireless nic without any association.
        // In those cases, we assume DHCP is enabled (todo: might enhance it to read from regvalue for NT based platforms)
        if ((_utils.GetOSGroup() == "9x" || _utils.IsWinVST(true)) && !_netCheck.IsAdapterConnected(this.objNetCheck, sAdapterServiceGUID))
        {    
          return true;
        }
        else
        {
          return (currentCardProps["DhcpEnabled"] == "Yes" && currentCardProps["DhcpEnabled"] != undefined);
        }
      },

      /**
      *  @name CheckForDHCPAdapters
      *  @memberOf $ss.agentcore.network.network
      *  @function
      *  @description Tests if there is atleast one adapter that uses dynamic IP.
      *  @param None
      *  @returns true if there is at least one adapter uses a dynamic IP else false
      *  @example
      *           var objNet = new $ss.agentcore.network.network();
      *           var ret = objNet.CheckForDHCPAdapters();
      */
      CheckForDHCPAdapters : function () 
      {
        // 
        // return true if there is at least one adapter uses a dynamic IP
        //
        for (card in this.cardsProps)
        {
          var currentCardProps = this.cardsProps[card];

          if(this.IsDHCPEnabled(card, currentCardProps))
          {
            return true;
          }
        }
        return false;
      },

      /**
      *  @name CheckForStaticAdapters
      *  @memberOf $ss.agentcore.network.network
      *  @function
      *  @description Tests if there is atleast one adapter that uses static IP.
      *  @param None
      *  @returns true if any adapters use a static IP else false
      *  @example
      *           var objNet = new $ss.agentcore.network.network();
      *           var ret = objNet.CheckForStaticAdapters();
      */
      CheckForStaticAdapters : function () 
      {
        // 
        // return true if any adapters use a static IP
        //
        for (card in this.cardsProps)
        {
          var currentCardProps = this.cardsProps[card];

          if (!this.IsDHCPEnabled(card, currentCardProps))
          {
            return true;
          }
        }
        return false;
      },

      /**
      *  @name CheckForStaticDnsAdapters
      *  @memberOf $ss.agentcore.network.network
      *  @function
      *  @description Tests if there is atleast one adapter that uses static DNS.
      *  @param None
      *  @returns true if any adapters use static DNS else false
      *  @example
      *           var objNet = new $ss.agentcore.network.network();
      *           var ret = objNet.CheckForStaticDnsAdapters();
      */
      CheckForStaticDnsAdapters : function () 
      {
        // if 9x machine, check if DNS is enabled.
        if (_utils.GetOSGroup() == "9x") 
        {
          return _netCheck.IsDnsEnabledForWin9x(); 
        }
        else
        {
          // 
          // return true if any adapters use static DNS 
          for (card in this.cardsProps)
          {
            var currentCardProps = this.cardsProps[card];
            var nameServer = currentCardProps["NameServer"];
            if (nameServer != null && nameServer!= "") 
            {
              return true;
            }
          }
        }
        return false;
      },

      /**
      *  @name CheckForBoundAdapters
      *  @memberOf $ss.agentcore.network.network
      *  @function
      *  @description Tests if there is atleast one adapter bound to TCP/IP.
      *  @param None
      *  @returns true if there is at least one adapter bound to TCP/IP else false
      *  @example
      *           var objNet = new $ss.agentcore.network.network();
      *           var ret = objNet.CheckForBoundAdapters();
      */
      CheckForBoundAdapters : function () 
      {  
        //
        // return true if there is at least one adapter bound to TCP/IP
        //
        for (card in this.cardsProps)
        {
          if (_netCheck.IsTCPIPboundToAdapter(this.objNetCheck, card))
          {
            var currentCardProps = this.cardsProps[card];
            return true;
          }
        }

        return false;
      },

      /**
      *  @name CheckForUnboundAdapters
      *  @memberOf $ss.agentcore.network.network
      *  @function
      *  @description Tests if there are any unbound network adapters.
      *  @param None
      *  @returns true if there are any unbound network adapters else false
      *  @example
      *           var objNet = new $ss.agentcore.network.network();
      *           var ret = objNet.CheckForUnboundAdapters();
      */
      CheckForUnboundAdapters : function () 
      {  
        //
        // return true if there are any unbound network adapters. 
        //
        for (card in this.cardsProps)
        {
          if (!_netCheck.IsTCPIPboundToAdapter(this.objNetCheck, card))
          {
            var currentCardProps = this.cardsProps[card];
            return true;
          }
        }

        return false;
      },

      /**
      *  @name MergeProtections
      *  @memberOf $ss.agentcore.network.network
      *  @function
      *  @description  merge all protections on the local machine that this user can access into the current user context.  This is applicable only to Windows 9x systems.
      *  @param None
      *  @returns 
      *  @example
      *           var objNet = new $ss.agentcore.network.network();
      *           var ret = objNet.MergeProtections();
      */
      MergeProtections : function () { //NOENUMERATION
        //NOTESTINLINE
        //
        //  merge all protections on the local machine that this user can access into the
        //  current user context.  This is applicable only to Windows 9x systems.
        if (this.Probe != null) {
          return this.Probe.CopySystemUserLatestProtections();
        }
        return false; 
      },

      /**
      *  @name CheckForNetworkProtection
      *  @memberOf $ss.agentcore.network.network
      *  @function
      *  @description checks if the provided guid is protected or not  
      *  @param sProtectionGuid Guid of the protection
      *  @returns true if the guid is protected else false
      *  @example
      *           var objNet = new $ss.agentcore.network.network();
      *           var ret = objNet.CheckForNetworkProtection("d4c2111e-ed04-46c9-badf-41ac634f4340");
      */
      CheckForNetworkProtection : function (sProtectionGuid) { 
        if (this.Probe != null) { 
          return this.Probe.HaveProtectionByGuid(sProtectionGuid);
        }
        return false;
      },

      /**
      *  @name Protect
      *  @memberOf $ss.agentcore.network.network
      *  @function
      *  @description  protects the provided guid.
      *  @param sProtectionGuid Guid of the protection
      *  @returns true if the protection of guid is success else false
      *  @example
      *           var objNet = new $ss.agentcore.network.network();
      *           var ret = objNet.Protect("d4c2111e-ed04-46c9-badf-41ac634f4340");
      */
      Protect : function (sProtectionGuid) { 
        if (this.Probe != null) {
          if (this.Probe.HaveProtectionByGuid(sProtectionGuid)) {
            return this.Probe.GetProtectionByGuid(sProtectionGuid).Protect();
          }
        }
        return false;
      },

      /**
      *  @name ProtectAsync
      *  @memberOf $ss.agentcore.network.network
      *  @function
      *  @description protects the provided guid making a asynchronous call.
      *  @param sProtectionGuid Guid of the protection
      *  @returns true if the protection of guid is success else false
      *  @example
      *           var objNet = new $ss.agentcore.network.network();
      *           var ret = objNet.ProtectAsync("d4c2111e-ed04-46c9-badf-41ac634f4340")
      */
      ProtectAsync : function (sProtectionGuid) { 
        if (this.Probe != null) {
          if (this.Probe.HaveProtectionByGuid(sProtectionGuid)) {
            return this.Probe.GetProtectionByGuid(sProtectionGuid).ProtectAsync();
          }
        }
        return false;
      },

      /**
      *  @name IsNetworkProtected
      *  @memberOf $ss.agentcore.network.network
      *  @function
      *  @description returns boolean based on if network is protected or not.
      *  @param sProtectionGuid Guid of the network protection
      *  @returns true if guid is protected else false
      *  @example
      *           var objNet = new $ss.agentcore.network.network();
      *           var ret = objNet.IsNetworkProtected("d4c2111e-ed04-46c9-badf-41ac634f4340")
      */
      IsNetworkProtected : function (sProtectionGuid) { //NOENUMERATION 
        // 
        //  determine if the probe object is available
        //  determine if we have a Network Settings protection scheme defined
        //  determine if we have a protection for Network Settings
        //
        if (this.Probe != null) {
          if (this.Probe.HaveProtectionByGuid(sProtectionGuid)) {
            return this.Probe.IsProtectedByGuid(sProtectionGuid);
          }
        }
        return false; 
      },

      /**
      *  @name RestoreNetworkConfiguration
      *  @memberOf $ss.agentcore.network.network
      *  @function
      *  @description Heal, if possible, the Network Settings protection scheme.
      *  @param sProtectionGuid Guid of the network protection which needs heal
      *  @returns true if the network protection is restored else false
      *  @example
      *           var objNet = new $ss.agentcore.network.network();
      *           var ret = objNet.RestoreNetworkConfiguration("d4c2111e-ed04-46c9-badf-41ac634f4340")
      */
      RestoreNetworkConfiguration : function (sProtectionGuid ) { 
        // NOENUMERATION
        // NOTESTINLINE
        //  Heal, if possible, the Network Settings protection scheme. 

        if (! this.IsNetworkProtected()) { return false; }
        var oProtection; 
        if ( (oProtection=this.Probe.GetProtectionByGuid(sProtectionGuid) ) ) {
          return oProtection.Heal();
        } 
        return false;
      },

      /**
      *  @name UndoRestoreNetworkConfiguration
      *  @memberOf $ss.agentcore.network.network
      *  @function
      *  @description Undo the previous restore of the network protection.
      *  @param sProtectionGuid Guid of the network protection which needs Undo
      *  @returns true if restored protection is undone else false
      *  @example
      *           var objNet = new $ss.agentcore.network.network();
      *           var ret = objNet.UndoRestoreNetworkConfiguration("d4c2111e-ed04-46c9-badf-41ac634f4340")
      */
      UndoRestoreNetworkConfiguration : function (sProtectionGuid ) { 
        // NOENUMERATION
        // NOTESTINLINE
        //  Heal, if possible, the Network Settings protection scheme. 


        if (! this.IsNetworkProtected()) { return false; }
        var oProtection; 
        if ( (oProtection=this.Probe.GetProtectionByGuid(sProtectionGuid) ) ) {
          return oProtection.Undo();
        } 
        return false;
      },

      /**
      *  @name IsWirelessProtected
      *  @memberOf $ss.agentcore.network.network
      *  @function
      *  @description Checks if the wireless settings are protected.
      *  @param sProtectionGuid Guid of the wireless settings.
      *  @returns true if the network protection is restored else false
      *  @example
      *           var objNet = new $ss.agentcore.network.network();
      *           var ret = objNet.IsWirelessProtected("d4c2111e-ed04-46c9-badf-41ac634f4340")
      */
      IsWirelessProtected : function (sProtectionGuid) { //NOENUMERATION 
        // 
        //  determine if the probe object is available
        //  determine if we have a Wireless Settings protection scheme defined
        //  determine if we have a protection for Wireless Zero Config Settings

        if (this.Probe != null) {
          if (this.Probe.HaveProtectionByGuid(sProtectionGuid)) {
            return this.Probe.IsProtectedByGuid(sProtectionGuid);
          }
        }
      },

//      RestoreWirelessConfig : function ( sProtectionGuid) { 
//        // NOENUMERATION
//        // NOTESTINLINE
//        //  Heal, if possible, the Network Settings protection scheme. 

//        if (! this.IsWirelessProtected()) { return false; }

//        var oProtection; 
//        if ( (oProtection=this.Probe.GetProtectionByGuid(sProtectionGuid) ) ) { 
//          if(swi_StartWZCSvc(false))
//          {
//            var iResult = oProtection.Heal();
//            swi_StartWZCSvc(true);
//            return iResult;
//          }
//        } 
//        return false;
//      },

      //**************
      // This doesn't work as of 5/19/2005 11:32:20 AM, needs more investigation
      // on BEFORE and BEFORE SEED
      //**************
      /**
      *  @name IsWirelessProtected
      *  @memberOf $ss.agentcore.network.network
      *  @function
      *  @description Undo the previous restore of the wireless network protection.
      *  @param sProtectionGuid Guid of the wireless settings.
      *  @returns true if restored wireless network protection is undone else false
      *  @example
      *           var objNet = new $ss.agentcore.network.network();
      *           var ret = objNet.UndoRestoreWirelessConfig("d4c2111e-ed04-46c9-badf-41ac634f4340")
      */
      UndoRestoreWirelessConfig : function (sProtectionGuid ) { 
        // NOENUMERATION
        // NOTESTINLINE
        //  Undo, if possible, the Wireless Settings protection scheme. 

        if (! this.IsWirelessProtected()) { return false; }
        return this.Probe.GetProtectionByGuid(sProtectionGuid).Undo();
      },

//      IsWZCSVCEnabledForAdapter : function (sAdapterServiceGUID) 
//      { 
//        if (typeof(sAdapterServiceGUID) == 'undefined') return false;
//        return swi_IsWZCSVCRunning() &&
//               swi_IsWZCEnabledForAdapter(Network.SelectedAdapter);
//      },

      /**
      *  @name SelectNetworkAdapter
      *  @memberOf $ss.agentcore.network.network
      *  @function
      *  @description Selects the adapter that is specified in parameter.
      *  @param sAdapterServiceGUID Network adapter guid.
      *  @returns None
      *  @example
      *           var objNet = new $ss.agentcore.network.network();
      *           var ret = objNet.SelectNetworkAdapter("4B89D5E8-615A-419A-BBC7-D14C9F8C3FE6");
      */
      SelectNetworkAdapter : function (sAdapterServiceGUID) 
      { 
        if (typeof(sAdapterServiceGUID) == 'undefined') return;   

        this.SelectedAdapter = sAdapterServiceGUID;
      },

      /**
      *  @name CheckForDisabledComponents
      *  @memberOf $ss.agentcore.network.network
      *  @function
      *  @description check for existance of HKLM\SYSTEM\CurrentControlSet\Enum\ROOT\NT_EACFILTMP
      *               If it exists, then there should be an nt_eacfilt bound to each adapter
      *               if there is, then loop through HKLM\SYSTEM\CurrentControlSet\Enum\ROOT\NT_EACFILTMP\
      *               make sure there keys exist for each network adapter, and that none of them have
      *               a \Control\DisableCount greater than 0, and that all have a \Control\ActiveService.
      *               If any of the keys do not exist, or if there is at least one DisableCount > 0, or one
      *               without an ActiveService defined, then return true.  Otherwise, return false.
      *  @param None.
      *  @returns refer description
      *  @example
      *           var objNet = new $ss.agentcore.network.network();
      *           var ret = objNet.CheckForDisabledComponents();
      */
      CheckForDisabledComponents : function ( ) 
      {  
        var keyString = _registry.EnumRegKey("HKLM", "SYSTEM\\CurrentControlSet\\Enum\\ROOT");
        var keys = keyString.split(",");
        var exists = false;
        for (var i = 0; i < keys.length; i++) {
          if (keys[i] == "NT_EACFILTMP") {
            exists = true;
          }
        }
        //if the user does not have Nortel adapters, then none need to be bound
        if (!exists) {
          return false;
        }
        //otherwise, we need to make sure each adapter is properly linked
        keyString = _registry.EnumRegKey("HKLM", "SYSTEM\\CurrentControlSet\\Enum\\ROOT\\NT_EACFILTMP");
        keys = keyString.split(",");
        
        //make sure each adapter exists in keys.... if it doesn't then we need to return true
        if (this.cardsProps.length > keys.length) return true;

        //make sure that that \Control\DisableCount > 0 for each key and that \Control\ActiveService is defined
        for (var i = 0; i < keys.length; i++) {

          var disabledCount = _registry.GetRegValue("HKLM", "SYSTEM\\CurrentControlSet\\Enum\\ROOT\\NT_EACFILTMP\\" + keys[i] + "\\Control", "DisableCount");
          if (!isNaN(disabledCount) && parseInt(disabledCount) > 0) {
            return true;
          }
          var activeService = _registry.GetRegValue("HKLM", "SYSTEM\\CurrentControlSet\\Enum\\ROOT\\NT_EACFILTMP\\" + keys[i] + "\\Control", "ActiveService");
          if (activeService == "") {
            return true;
          }
        }
        return false;
      },

      /**
      *  @name ResetAllComponents
      *  @memberOf $ss.agentcore.network.network
      *  @function
      *  @description reset the Nortel VPN component on all adapters.
      *  @param None.
      *  @returns true
      *  @example
      *           var objNet = new $ss.agentcore.network.network();
      *           var ret = objNet.ResetAllComponents();
      */
      ResetAllComponents : function ( ) 
      {
        // 
        //  reset the Nortel VPN component on all adapters
        //
        this.objNetCheck.EnableComponent("nt_eacfilt", false); 
        this.objNetCheck.EnableComponent("nt_eacfilt", true);
        return true;
      },

      /**
      *  @name RestartSelectedAdapter
      *  @memberOf $ss.agentcore.network.network
      *  @function
      *  @description Restarts the selected adapter whose guid is passed.
      *  @param sAdapterServiceGUID guid of the adapter to restart
      *  @returns true if restart is successful else false
      *  @example
      *           var objNet = new $ss.agentcore.network.network();
      *           var ret = objNet.RestartSelectedAdapter("4B89D5E8-615A-419A-BBC7-D14C9F8C3FE6");
      */
      RestartSelectedAdapter : function (sAdapterServiceGUID ) 
      {
        if (typeof(sAdapterServiceGUID) == 'undefined') return false; 
        
        if(sAdapterServiceGUID != "")
        {
          _netCheck.DisableAdapter(this.objNetCheck, sAdapterServiceGUID);
          _utils.Sleep(1000);
          _netCheck.EnableAdapter(this.objNetCheck, sAdapterServiceGUID);
          return true;
        }
        return false;
      },

      /**
      *  @name EnableAllDisabledNetworkAdapters
      *  @memberOf $ss.agentcore.network.network
      *  @function
      *  @description Enables all the disabled adapters.
      *  @param None.
      *  @returns true if there is any disabled adapter and it is enabled else false
      *  @example
      *           var objNet = new $ss.agentcore.network.network();
      *           var ret = objNet.EnableAllDisabledNetworkAdapters();
      */
      EnableAllDisabledNetworkAdapters : function ( ) 
      { 
        return _netCheck.EnableAdapter(this.objNetCheck, "");
      },

      /**
      *  @name BindAllNetworkAdapters
      *  @memberOf $ss.agentcore.network.network
      *  @function
      *  @description Binds all the Network adapters.
      *  @param None.
      *  @returns true if there is any unboud adapter and it is bound else false
      *  @example
      *           var objNet = new $ss.agentcore.network.network();
      *           var ret = objNet.BindAllNetworkAdapters();
      */
      BindAllNetworkAdapters : function ( ) 
      {
        // 
        //  for now tcpip bind all.
        // 
        return _netCheck.BindTcpipToAdapter(this.objNetCheck, "");
      },

      /**
      *  @name EnableDhcpToAllNetworkAdapters
      *  @memberOf $ss.agentcore.network.network
      *  @function
      *  @description Enables DHCP for all the network adapters.
      *  @param None.
      *  @returns count of number of adapters for which DHCP was enabled
      *           var objNet = new $ss.agentcore.network.network();
      *           var ret = objNet.EnableDhcpToAllNetworkAdapters();
      */
      EnableDhcpToAllNetworkAdapters : function () 
      {
        // 
        //  iterate all adapters, find any that are static, and .forceDHCP() them.
        // 
        var forcedDhcpCnt = 0;
        for (workingAdapter in this.cardsProps) { 
          var workingAdapterProps = this.cardsProps[workingAdapter];
          var nameServer          = workingAdapterProps["NameServer"];
          var bIsStaticDns        = (nameServer != null && nameServer != "");
          var bIsStatic           = (workingAdapterProps["DhcpEnabled"] != "Yes");

          if (bIsStatic || bIsStaticDns) 
          {
            if(forcedDhcpCnt == 0) this.SaveStaticInfo();  // save only once
            _netCheck.EnableDHCP(this.objNetCheck, workingAdapter);
            forcedDhcpCnt++;
          }
        }

        if (forcedDhcpCnt > 0) _utils.RefreshDHCP();

        return forcedDhcpCnt; 
      },

      /**
      *  @name SaveStaticInfo
      *  @memberOf $ss.agentcore.network.network
      *  @function
      *  @description Saves static info.
      *  @param None.
      *  @returns none.
      *           var objNet = new $ss.agentcore.network.network();
      *           var ret = objNet.SaveStaticInfo();
      */
      SaveStaticInfo : function()
      {
        var netDna = _config.ExpandSysMacro("%TEMP%net_save.dna");

        //[MAC] Check Machine OS Windows/MAC
        if(!bMac)
          _objContainer.ProtectNet(netDna);
        else {
          //=======================================================
          //[MAC] Native Call
          var message = {'JSClassNameKey':'Network','JSOperationNameKey':'ProtectNet', 'JSArgumentsKey':[netDna], 'JSISSyncMethodKey' : '1'}
          jsBridge.execute(message)
          //=======================================================
        }
      },

      /**
      *  @name RestoreStaticInfo
      *  @memberOf $ss.agentcore.network.network
      *  @function
      *  @description Restores static info.
      *  @param None.
      *  @returns none.
      *           var objNet = new $ss.agentcore.network.network();
      *           var ret = objNet.RestoreStaticInfo();
      */
      RestoreStaticInfo : function()
      {
        var netDna = _config.ExpandSysMacro("%TEMP%net_save.dna");
        //[MAC] Check Machine OS Windows/MAC
        if(!bMac)
          _objContainer.HealNet(netDna);
        else {
          //=======================================================
          //[MAC] Native Call
          var message = {'JSClassNameKey':'Network','JSOperationNameKey':'HealNet', 'JSArgumentsKey':[netDna], 'JSISSyncMethodKey' : '1'}
          jsBridge.execute(message)
          //=======================================================
        }
      },

//      _Network.prototype.toString = _Network.prototype.valueOf = function ()
//      {
//        return '[object _Network]';
//      }


      //
      //  GENERIC METHODS FOR ALL TYPES OF ADAPTERS
      // 
      IsTCPIPboundToAdapter : function (adapter) 
      {
        return _netCheck.IsTCPIPboundToAdapter(this.objNetCheck, adapter);
      },

      /**
      *  @name IsAdapterConnected
      *  @memberOf $ss.agentcore.network.network
      *  @function
      *  @description Tests if the adapter is connected to network.
      *  @param adapter adapter guid.
      *  @returns true if adapter is conncted else false.
      *           var objNet = new $ss.agentcore.network.network();
      *           var ret = objNet.IsAdapterConnected("4B89D5E8-615A-419A-BBC7-D14C9F8C3FE6");
      */
      IsAdapterConnected : function (adapter) 
      {
        return _netCheck.IsAdapterConnected(this.objNetCheck, adapter);
      },

      /**
      *  @name IsDHCPEnabledForAdapter
      *  @memberOf $ss.agentcore.network.network
      *  @function
      *  @description Tests if DHCP for adapter is enabled.
      *  @param adapter adapter guid.
      *  @returns true if DHCP for adapter is enabled else false.
      *           var objNet = new $ss.agentcore.network.network();
      *           var ret = objNet.IsDHCPEnabledForAdapter("4B89D5E8-615A-419A-BBC7-D14C9F8C3FE6");
      */
      IsDHCPEnabledForAdapter : function (adapter) 
      {
        var selectedCardProps = this.cardsProps[adapter];
        return (selectedCardProps["DhcpEnabled"] == "Yes");
      },

      /**
      *  @name IsAdapterPresent
      *  @memberOf $ss.agentcore.network.network
      *  @function
      *  @description Tests if the adapter is present.
      *  @param adapter Adapter guid.
      *  @returns true if adapter exists else false.
      *           var objNet = new $ss.agentcore.network.network();
      *           var ret = objNet.IsAdapterPresent("4B89D5E8-615A-419A-BBC7-D14C9F8C3FE6");
      */
      IsAdapterPresent : function (adapter)
      {
        var cardArr = _netCheck.GetPresentAdapters(this.objNetCheck, true, false);
        for(var card in cardArr)
        {
          if(adapter == card) return true;
        }
        return false;
      } ,

      /**
      *  @name IsWireless
      *  @memberOf $ss.agentcore.network.network
      *  @function
      *  @description Tests if the adapter is wireless or not.
      *  @param adapter Adapter guid.
      *  @returns true if is adapter wireless else false.
      *           var objNet = new $ss.agentcore.network.network();
      *           var ret = objNet.IsWireless("4B89D5E8-615A-419A-BBC7-D14C9F8C3FE6");
      */ 
      IsWireless : function (adapter)
      {
        var selectedCardProps = this.cardsProps[adapter];
        var medium = selectedCardProps["PhysicalMedium"];

        return (medium == _constants.NdisPhysicalMediumWirelessLan || 
                medium == _constants.NdisPhysicalMediumWirelessWan);
      },

      /**
      *  @name IsWired
      *  @memberOf $ss.agentcore.network.network
      *  @function
      *  @description Tests if the adapter is wired or not.
      *  @param adapter Adapter guid.
      *  @returns true if adapter wired else false.
      *           var objNet = new $ss.agentcore.network.network();
      *           var ret = objNet.IsWired("4B89D5E8-615A-419A-BBC7-D14C9F8C3FE6");
      */ 
      IsWired : function (adapter) 
      {
        return !this.IsWireless(adapter); 
      },

      //
      //  PCMCIA 
      //
      ISDriverOKAY : function (adapter) 
      {
        //  todo -- needs to have more aware of PCMCIA reinsertion states.  may fall out 
        //  during qa?  otherwise needs further investigation. 
        //  8/3/2004 gl 
        // 
        return _netCheck.IsTCPIPboundToAdapter(this.objNetCheck, adapter); 
      }

//      //
//      //  WIRELESS
//      // 
//      IsSignalOKAY = function (adapter) 
//      {
//        return (ss_net_GetSignalLevel(this.objNetCheck, adapter) >= this.configHash.Wireless_WarningSignal);  
//      }

  });
  
  //[MAC] Get Machine OS type
  var bMac = $.browser.safari;

  //[MAC] JSBridge object used only for MAC
  try {
    var jsBridge = window.JSBridge;
  }
  catch(e) {
  }
  
  var _utils = $ss.agentcore.utils;
  var _netCheck = $ss.agentcore.network.netcheck;
  var _ini = $ss.agentcore.dal.ini;
  var _registry = $ss.agentcore.dal.registry;
  var _file = $ss.agentcore.dal.file;
  var _config = $ss.agentcore.dal.config;
  var _http = $ss.agentcore.dal.http;
  var _constants = $ss.agentcore.constants;
  var _objContainer = $ss.agentcore.utils.activex.GetObjInstance();
  var _event = $ss.agentcore.events;

})()  







