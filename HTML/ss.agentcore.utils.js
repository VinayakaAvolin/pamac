/** @namespace Holds all generic utility functions*/
$ss.agentcore.utils = $ss.agentcore.utils ||
{};

(function(){

  $.extend($ss.agentcore.utils, {
  
    /**
     *  @name ConvertUnicodeHexToStringWithNewLine
     *  @memberOf $ss.agentcore.utils
     *  @function
     *  @description Converts Unicode hex string (reg binary) to a string
     *  @param sHex unicode hex string
     *  @returns string
     *  @example
     *           var objUtil = $ss.agentcore.utils;
     *           var ret = objUtil.ConvertUnicodeHexToStringWithNewLine("740065006d0070003200760061006c000000")
     */
    ConvertUnicodeHexToStringWithNewLine: function(sHex){
      _logger.info('Entered function: $ss.agentcore.utils.ConvertUnicodeHexToStringWithNewLine');
      var i = 0;
      var sResult = "";
      
      while (i + 3 < sHex.length) {
        var sLowerByte = sHex.charAt(i) + sHex.charAt(i + 1); // string representation of lower byte
        var lowerbyteVal = parseInt(sLowerByte, 16); // numerical value of lower byte
        var sUpperByte = sHex.charAt(i + 2) + sHex.charAt(i + 3); // string representation of upper byte
        var upperbyteVal = parseInt(sUpperByte, 16); // numerical value of upper byte
        var cCode = (upperbyteVal * 256) + lowerbyteVal;
        if (cCode == 0) 
          sResult += "\n";
        else 
          sResult += String.fromCharCode(cCode);
        
        i += 4;
      }
      
      return sResult;
    },
    
    /**
     *  @name ConvertUnicodeHexToString
     *  @memberOf $ss.agentcore.utils
     *  @function
     *  @description  Converts Unicode hex string (reg binary) to a string
     *  @param sHex unicode hex string
     *  @returns string
     *  @example
     *           var objUtil = $ss.agentcore.utils;
     *           var ret = objUtil.ConvertUnicodeHexToString("740065006d0070003200760061006c000000")
     */
    ConvertUnicodeHexToString: function(sHex){
      _logger.info('Entered function: $ss.agentcore.utils.ConvertUnicodeHexToString');
      var i = 0;
      var sResult = "";
      
      while (i + 3 < sHex.length) {
        var sLowerByte = sHex.charAt(i) + sHex.charAt(i + 1); // string representation of lower byte
        var lowerbyteVal = parseInt(sLowerByte, 16); // numerical value of lower byte
        var sUpperByte = sHex.charAt(i + 2) + sHex.charAt(i + 3); // string representation of upper byte
        var upperbyteVal = parseInt(sUpperByte, 16); // numerical value of upper byte
        var cCode = (upperbyteVal * 256) + lowerbyteVal;
        if (cCode != 0) 
          sResult += String.fromCharCode(cCode);
        
        i += 4;
      }
      
      return sResult;
    },
    
    /**
     *  @name ConvertStringToUnicodeHex
     *  @memberOf $ss.agentcore.utils
     *  @function
     *  @description  Converts string to binary hex value representation
     *  @param str registry value data (string representation)
     *  @returns converted string
     *  @example
     *           var objUtil = $ss.agentcore.utils;
     *           var ret = objUtil.ConvertStringToUnicodeHex("temp2val")
     */
    ConvertStringToUnicodeHex: function(str){
      _logger.info('Entered function: $ss.agentcore.utils.ConvertStringToUnicodeHex');
      var strLen = str.length;
      var i = 0;
      var hexStr = "";
      
      for (i = 0; i < strLen; i++) {
        var charCode = str.charCodeAt(i);
        var charCodeStr = charCode.toString(16);
        
        switch (charCodeStr.length) {
          case 0:
            break;
          case 1:
            hexStr += ('0' + charCodeStr + '00');
            break;
          case 2:
            hexStr += (charCodeStr + '00');
            break;
          case 3:
            hexStr += (charCodeStr.charAt(1) + charCodeStr.charAt(2) + '0' + charCodeStr.charAt(0));
            break;
          case 4:
            hexStr += (charCodeStr.charAt(2) + charCodeStr.charAt(3) + charCodeStr.charAt(0) + charCodeStr.charAt(1));
            break;
        }
      }
      
      // append trailing NULL
      hexStr += '0000';
      return hexStr;
    },
    
    /**
     *  @name EncryptStrToHex
     *  @memberOf $ss.agentcore.utils
     *  @function
     *  @description  Encrypts the given string
     *  @param str string to encrypt
     *  @returns hex string of encrypted value
     *  @example
     *           var objUtil = $ss.agentcore.utils;
     *           var ret = objUtil.EncryptStrToHex("temp2val");
     */
    EncryptStrToHex: function(str){
      _logger.info('Entered function: $ss.agentcore.utils.EncryptStrToHex');
      var retVal = "";
      if (!str) {
        return retVal;
      }
      
      try {
        //[MAC] Check Machine OS Windows/MAC
        if(!bMac) 
          retVal = _objContainer.EncryptStrToHex(str);
        else {
          //=======================================================
          //[MAC] Native Call
          var message = {'JSClassNameKey':'Utility','JSOperationNameKey':'EncryptStrToHex', 'JSArgumentsKey':[str], 'JSISSyncMethodKey' : '1'}
          retVal = jsBridge.execute(message)
          //=======================================================
        }
      } 
      catch (err) {
        _exception.HandleException(err, '$ss.agentcore.utils', 'EncryptStrToHex', arguments);
        //g_Logger.Error("ss_con_EncryptStrToHex()", err.message + " -- str: " + str);
      }
      return retVal;
    },
    
    /**
     *  @name DecryptHexToStr
     *  @memberOf $ss.agentcore.utils
     *  @function
     *  @description  Decrypts the given encrypted string
     *  @param encStr hex string representing encrypted value
     *  @returns string
     *  @example
     *           var objUtil = $ss.agentcore.utils;
     *           var ret = objUtil.DecryptHexToStr("temp2val");
     */
    DecryptHexToStr: function(encStr){
      _logger.info('Entered function: $ss.agentcore.utils.DecryptHexToStr');
      var retVal = "";
      // check for null, empty, undefined and string "undefined"
      if (!encStr) {
        return retVal;
      }
      
      try {
        //[MAC] Check Machine OS Windows/MAC
        if(!bMac)
          retVal = _objContainer.DecryptHexToStr(encStr);
        else {
          //=======================================================
          //[MAC] Native Call
          var message = {'JSClassNameKey':'Utility','JSOperationNameKey':'DecryptHexToStr', 'JSArgumentsKey':[encStr], 'JSISSyncMethodKey' : '1'}
          retVal = jsBridge.execute(message)
          //=======================================================
        }
      } 
      catch (err) {
        _exception.HandleException(err, '$ss.agentcore.utils', 'DecryptHexToStr', arguments);
        //g_Logger.Error("ss_con_DecryptHexToStr()", err.message + " -- encStr: " + encStr);
      }
      return retVal;
    },
    
    /**
     *  @name GenerateMD5
     *  @memberOf $ss.agentcore.utils
     *  @function
     *  @description  Generates MD5 value on the input string
     *  @param sInput input string
     *  @returns string
     *  @example
     *           var objUtil = $ss.agentcore.utils;
     *           var ret = objUtil.GenerateMD5("temp2val");
     */
    GenerateMD5: function(sInput){
      _logger.info('Entered function: $ss.agentcore.utils.GenerateMD5');
      var retVal = "";
      if (!sInput) 
        return retVal;
      
      try {
        //[MAC] Check Machine OS Windows/MAC
        if(!bMac)
           retVal = _objContainer.GenerateHash(3, sInput);
        else {
          //=======================================================
          //[MAC] Native Call
          var message = {'JSClassNameKey':'Utility','JSOperationNameKey':'GenerateHash', 'JSArgumentsKey':[3, sInput], 'JSISSyncMethodKey' : '1'}
          retVal = jsBridge.execute(message)
          //=======================================================
        }
      } 
      catch (err) {
        _exception.HandleException(err, '$ss.agentcore.utils', 'GenerateMD5', arguments);
        //g_Logger.Error("ss_con_GenerateMD5()", err.message + " -- sInput: " + sInput);
      }
      return retVal;
    },
    
    /**
     *  @name GenGuid
     *  @memberOf $ss.agentcore.utils
     *  @function
     *  @description  Generates a new guid
     *  @param bBraces If set the guid generated will include curly braces
     *  @returns string
     *  @example
     *           var objUtil = $ss.agentcore.utils;
     *           var guid = objUtil.GenGuid(true);
     */
    GenGuid: function(bBraces){
      _logger.info('Entered function: $ss.agentcore.utils.GenGuid');
      var retVal = "";
      try {
        //[MAC] Check Machine OS Windows/MAC
        if(!bMac)
          retVal = _objContainer.GenGuid(bBraces);
        else {
          //=======================================================
          //[MAC] Native Call
          var _jsBridge = window.JSBridge;
          var message = {'JSClassNameKey':'SystemUtility','JSOperationNameKey':'GenGuid', 'JSArgumentsKey':[bBraces], 'JSISSyncMethodKey' : '1'}
          var result = _jsBridge.execute(message);
          var parsedJSONObject = JSON.parse(result);
          retVal = parsedJSONObject.Data;
          //=======================================================
        }
      } 
      catch (err) {
        _exception.HandleException(err, '$ss.agentcore.utils', 'GenGuid', arguments);
        //g_Logger.Error("ss_con_GenGuid()", err.message);
      }
      return retVal;
    },

    /**
     *  @name GetTruncatedString
     *  @memberOf $ss.agentcore.utils
     *  @function
     *  @description  Provides the truncated string replacing extra characters with dots.
     *  @param str: Long String
     *  @param len: maxLength upto which string should get truncate
     *  @returns string
     *  @example
     *           var objUtil = $ss.agentcore.utils;
     *           var obj = objUtil.GetTruncatedString("Self Service Support",10);
     */
    GetTruncatedString: function (str, len) {
      if (str) {
        if (str.length > len) str = str.substring(0, len) + "...";
      }
      return str;
    },
    
    /**
     *  @name GetObject
     *  @memberOf $ss.agentcore.utils
     *  @function
     *  @description  Creates moniker object, normally used for WMI.
     *  @param sMoniker
     *  @returns object or null
     *  @example
     *           var objUtil = $ss.agentcore.utils;
     *           var obj = objUtil.GetObject("Winmgmts:");
     */
    GetObject: function(sMoniker){
      _logger.info('Entered function: $ss.agentcore.utils.GetObject');
      var xReturn;
      try {
        //[MAC] Check Machine OS Windows/MAC
        if(!bMac)
          var xTemp = _objContainer.GetObject(sMoniker);
        else {
          //=======================================================
          //[MAC] Native Call
          var message = {'JSClassNameKey':'Utility','JSOperationNameKey':'GetObject', 'JSArgumentsKey':[sMoniker], 'JSISSyncMethodKey' : '1'}
          var xTemp = jsBridge.execute(message)
          //=======================================================
        }
        xReturn = xTemp;
      } 
      catch (e) {
        _exception.HandleException(e, '$ss.agentcore.utils', 'GetObject', arguments);
        xReturn = null;
      }
      
      return xReturn;
      
    },
    
    /**
     *  @name GetCurTimeAsHex
     *  @memberOf $ss.agentcore.utils
     *  @function
     *  @description  Captures the current system time from Windows in Hex.
     *  @returns string
     *  @example
     *           var objUtil = $ss.agentcore.utils;
     *           var ret = objUtil.GetCurTimeAsHex();
     */
    GetCurTimeAsHex: function(){
      _logger.info('Entered function: $ss.agentcore.utils.GetCurTimeAsHex');
      var sReturn = "";
      try {
        //[MAC] Check Machine OS Windows/MAC
        if(!bMac)
          sReturn = _objContainer.GetCurTimeAsHex();
        else {
          //=======================================================
          //[MAC] Native Call
          var message = {'JSClassNameKey':'Utility','JSOperationNameKey':'GetCurTimeAsHex', 'JSArgumentsKey':[], 'JSISSyncMethodKey' : '1'}
          sReturn = jsBridge.execute(message)
          //=======================================================
        }
      } 
      catch (e) {
        _exception.HandleException(e, '$ss.agentcore.utils', 'GetCurTimeAsHex', arguments);
        sReturn = "";
      }
      
      return sReturn;
    },
    
    /**
     *  @name GetSessionId
     *  @memberOf $ss.agentcore.utils
     *  @function
     *  @description  Gets the current session ID.
     *  @returns string
     *  @example
     *           var objUtil = $ss.agentcore.utils;
     *           var ret = objUtil.GetSessionId();
     */
    GetSessionId: function(){
      _logger.info('Entered function: $ss.agentcore.utils.GetSessionId');
      var sReturn = "";
      try {
        //[MAC] Check Machine OS Windows/MAC
        if(!bMac)
          sReturn = _objContainer.SessionId;
        else {
          //=======================================================
          //[MAC] Native Call
          var message = {'JSClassNameKey':'Utility','JSOperationNameKey':'SessionId', 'JSArgumentsKey':[], 'JSISSyncMethodKey' : '1'}
          sReturn = jsBridge.execute(message)
          //=======================================================
        }
      } 
      catch (e) {
        _exception.HandleException(e, '$ss.agentcore.utils', 'GetSessionId', arguments);
        sReturn = "";
      }
      return sReturn;
    },
    
    /**
     *  @name GetFirefoxUserPref
     *  @memberOf $ss.agentcore.utils
     *  @function
     *  @description  Reads the user preference file (prefs.js under <br/>
     *                %SF_OS_APPDATA%\Mozilla\Firefox and extracts specified setting.
     *  @returns string/number
     *  @example
     *           var objUtil = $ss.agentcore.utils;
     *           var ret = objUtil.GetFirefoxUserPref("browser.offline");
     */
    GetFirefoxUserPref: function(prefName){
      _logger.info('Entered function: $ss.agentcore.utils.GetFirefoxUserPref');
      var retVal = null;
      try {
        //[MAC] Check Machine OS Windows/MAC
        if(!bMac)
          retVal = _objContainer.GetFirefoxUserPref(prefName);
        else {
          //=======================================================
          //[MAC] Native Call
          var message = {'JSClassNameKey':'Utility','JSOperationNameKey':'GetFirefoxUserPref', 'JSArgumentsKey':[prefName], 'JSISSyncMethodKey' : '1'}
          retVal = jsBridge.execute(message)
          //=======================================================
        }
      }
      catch (e) {
        _exception.HandleException(e, '$ss.agentcore.utils', 'GetFirefoxUserPref', arguments);
        //g_Logger.Error("ss_con_GetFirefoxUserPref()", e.message);
      }
      return retVal;
    },
    
    /**
     *  @name FormatDateString
     *  @memberOf $ss.agentcore.utils
     *  @function
     *  @description  Get a String representation of the date argument
     *  @param oDate the date object to convert
     *  @param blnTime boolean value specifying whether to include <br/>
     *         time fields or not
     *  @param bFormatDate boolean value specifying whether to format <br/>
     *         date fields by including "-"
     *  @returns String representation of the Date object
     *  @example
     *           var objUtil = $ss.agentcore.utils;
     *           var str = objUtil.FormatDateString(new Date("Thu Jul 31 14:22:17 UTC+0530 2008"),true,true);
     */
    FormatDateString: function(oDate, bInTime, bFormatDate){
      _logger.info('Entered function: $ss.agentcore.utils.FormatDateString');
      var sReturnValue = new String();
      try {
        var sYear = this.ConvertStringToMinLen(oDate.getFullYear(), 4, "0")
        var sMonth = this.ConvertStringToMinLen(oDate.getMonth() + 1, 2, "0")
        var sDay = this.ConvertStringToMinLen(oDate.getDate(), 2, "0")
        if (bFormatDate) 
          sReturnValue = "" + sYear + "-" + sMonth + "-" + sDay;
        else 
          sReturnValue = "" + sYear + sMonth + sDay;
        
        if (bInTime) {
          var sHour = this.ConvertStringToMinLen(oDate.getHours(), 2, "0")
          var sMinute = this.ConvertStringToMinLen(oDate.getMinutes(), 2, "0")
          var sSecond = this.ConvertStringToMinLen(oDate.getSeconds(), 2, "0")
          sReturnValue = sReturnValue + " " + sHour + ":" + sMinute + ":" + sSecond
        }
      } 
      catch (ex) {
        _exception.HandleException(ex, '$ss.agentcore.utils', 'FormatDateString', arguments);
        //g_Logger.Error("ss_utl_FormatDateString()", ex.message);
      }
      return sReturnValue;
    },
    
    /**
     *  @name ConvertStringToMinLen
     *  @memberOf $ss.agentcore.utils
     *  @function
     *  @description  Prefixes a given string with given characters to ensure <br/>
     the string length is at least the nMinLength
     *  @param sPrefix String to prefix
     *  @param nMinLen minimum string length
     *  @param cPrefix Character to use when prefixing
     *  @returns String sPrefix prefixed with cPrefix character with <br/>
     *           a minimum length
     *  @example
     *           var objUtil = $ss.agentcore.utils;
     *           var ret = objUtil.ConvertStringToMinLen(new Date("Thu Jul 31 14:22:17 UTC+0530 2008").getHours(),3,"0");
     */
    ConvertStringToMinLen: function(sPrefix, nMinLen, cPrefix){
      _logger.info('Entered function: $ss.agentcore.utils.ConvertStringToMinLen');
      var sRetVal = "";
      try {
        sPrefix = "" + sPrefix;
        var nLength = sPrefix.length;
        var strTemp = "";
        
        if (nLength < nMinLen) {
          for (var intIndex = 1; intIndex <= nMinLen - nLength; intIndex++) {
            strTemp = strTemp + cPrefix;
          }
          sRetVal = strTemp + sPrefix;
        }
        else {
          sRetVal = sPrefix;
        }
      } 
      catch (ex) {
        _exception.HandleException(ex, '$ss.agentcore.utils', 'ConvertStringToMinLen', arguments);
        //g_Logger.Error("ss_utl_ConvertStringToMinLen()", ex.message);
      }
      return sRetVal;
    },
    
    
    /**
     *  @name GetTimeZone
     *  @memberOf $ss.agentcore.utils
     *  @function
     *  @description  Get the current time zone
     *  @returns String
     *  @example
     *           var objUtil = $ss.agentcore.utils;
     *           var ret = objUtil.GetTimeZone();
     */
    GetTimeZone: function(){
      _logger.info('Entered function: $ss.agentcore.utils.GetTimeZone');
      var elements = (new Date()).toString().split(' ');
      var timeZone = "";
      try {
        timeZone = elements[elements.length - 2];
      } 
      catch (ex) {
        _exception.HandleException(ex, '$ss.agentcore.utils', 'GetTimeZone', arguments);
        //g_Logger.Error("ss_utl_GetTimeZone()", ex.message);
      }
      return timeZone;
    },
    
    
    /**
     *  @name MakeHashFromArrayValues
     *  @memberOf $ss.agentcore.utils
     *  @function
     *  @description  keys the values of an array into a hash table for <br/>
     *                easier test access
     *  @param arValue Array   required, array to convert
     *  @returns Object  of booleans keyed by value
     *  @example
     *           var objUtil = $ss.agentcore.utils;
     *           var arr = ["arg1","arg2","arg3"];
     *           var ret = objUtil.MakeHashFromArrayValues(arr);
     */
    MakeHashFromArrayValues: function(arValue){
      _logger.info('Entered function: $ss.agentcore.utils.MakeHashFromArrayValues');
      var newHash = {};
      try {
        for (var i = 0; i < arValue.length; i++) {
          newHash[arValue[i]] = true;
          //continue;
        }
      } 
      catch (ex) {
        _exception.HandleException(ex, '$ss.agentcore.utils', 'MakeHashFromArrayValues', arguments);
        //g_Logger.Error("ss_utl_MakeHashFromArrayValues()", ex.message);
      }
      return newHash;
    },
    
    /**
     *  @name MakeArrayFromHashKeys
     *  @memberOf $ss.agentcore.utils
     *  @function
     *  @description  converts a hash into an array for iteration
     *  @param oHash Object  required to convert
     *  @returns Array  of elements with the keys to the hash as the values <br/>
     of the array elements
     *  @example
     *           var objUtil = $ss.agentcore.utils;
     *           var arr = ["arg1","arg2","arg3"];
     *           var hash = objUtil.MakeHashFromArrayValues(arr);
     *           var ret = objUtil.MakeArrayFromHashKeys(hash);
     */
    MakeArrayFromHashKeys: function(oHash){
      _logger.info('Entered function: $ss.agentcore.utils.MakeArrayFromHashKeys');
      try {
        if (typeof(oHash) == 'object' || typeof(oHash) == 'array') {
          var arNew = [];
          for (var i in oHash) {
            arNew[arNew.length] = i;
          }
          return arNew;
        }
      } 
      catch (ex) {
        _exception.HandleException(ex, '$ss.agentcore.utils', 'MakeArrayFromHashKeys', arguments);
        //g_Logger.Error("ss_utl_MakeArrayFromHashKeys()", ex.message);
      }
      return false;
    },
    
    /**
     *  @name MakeArrayFromHashValues
     *  @memberOf $ss.agentcore.utils
     *  @function
     *  @description  converts hash values into an array
     *  @param oHash Object  required to convert
     *  @returns Array of elements with the values to the hash as the values <br/>
     *           of the array elements
     *  @example
     *          var objUtil = $ss.agentcore.utils;
     *          var arr = ["arg1","arg2","arg3"];
     *          var hash = objUtil.MakeHashFromArrayValues(arr);
     *          var ret = objUtil.MakeArrayFromHashKeys(hash);
     */
    MakeArrayFromHashValues: function(oHash){
      _logger.info('Entered function: $ss.agentcore.utils.MakeArrayFromHashValues');
      try {
        if (typeof(oHash) == 'object' || typeof(oHash) == 'array') {
          var arNew = [];
          for (var i in oHash) {
            arNew[arNew.length] = oHash[i];
          }
          return arNew;
        }
      } 
      catch (ex) {
        _exception.HandleException(ex, '$ss.agentcore.utils', 'MakeArrayFromHashValues', arguments);
        //g_Logger.Error("ss_utl_MakeArrayFromHashValues()", ex.message);
      }
      return false;
    },
    
    /**
    *  @name MakeSortedArrayFromHashValues
    *  @memberOf $ss.agentcore.utils
    *  @function
    *  @description  converts hash values into an sorted array
    *  @param oHash Object  required to convert
    *  @param sortFunc Function callback used for sorting
    *  @returns Array of elements with the sorted values to the hash <br/>
    *           as the values of the array elements
    *  @example
    *          var objUtil = $ss.agentcore.utils;
    *          var ret = objUtil.MakeSortedArrayFromHashValues(oHash,$ss.mynamespace.mysortfunction);
    */
    MakeSortedArrayFromHashValues: function(oHash, sortFunc){
      _logger.info('Entered function: $ss.agentcore.utils.MakeSortedArrayFromHashValues');
      var arSorted = this.MakeArrayFromHashValues(oHash);
      arSorted.sort(sortFunc);
      return arSorted;
    },
    
    /**
     *  @name EncodeBase64
     *  @memberOf $ss.agentcore.utils
     *  @function
     *  @description  Changes the input to a base64 encoded string
     *  @param sInput String to be encoded
     *  @returns A base64 encode string or "" {if null was passed}
     *  @example
     *           var objUtil = $ss.agentcore.utils;
     *           var ret = objUtil.EncodeBase64("temp2val");
     */
    EncodeBase64: function(sInput){
      _logger.info('Entered function: $ss.agentcore.utils.EncodeBase64');
      //g_Logger.Info("ss_utl_EncodeBase64()", "sInput=" + sInput);
      var keyStr = "ABCDEFGHIJKLMNOP" + "QRSTUVWXYZabcdef" +
      "ghijklmnopqrstuv" +
      "wxyz0123456789+/" +
      "=";
      
      var output = "";
      var chr1, chr2, chr3 = "";
      var enc1, enc2, enc3, enc4 = "";
      var i = 0;
      
      if (sInput == "") 
        return "";
      
      do {
        chr1 = sInput.charCodeAt(i++);
        chr2 = sInput.charCodeAt(i++);
        chr3 = sInput.charCodeAt(i++);
        
        enc1 = chr1 >> 2;
        enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
        enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
        enc4 = chr3 & 63;
        
        if (isNaN(chr2)) {
          enc3 = enc4 = 64;
        }
        else 
          if (isNaN(chr3)) {
            enc4 = 64;
          }
        
        output = output +
        keyStr.charAt(enc1) +
        keyStr.charAt(enc2) +
        keyStr.charAt(enc3) +
        keyStr.charAt(enc4);
        chr1 = chr2 = chr3 = "";
        enc1 = enc2 = enc3 = enc4 = "";
      }
      while (i < sInput.length);
      
      //g_Logger.Debug("ss_utl_EncodeBase64()", "Output=" + output);
      return output;
    },
    
    /**
     *  @name DecodeBase64
     *  @memberOf $ss.agentcore.utils
     *  @function
     *  @description  Decodes a base64 encoded string
     *  @param sInput String to be decoded
     *  @returns The decoded string or "" {if null was passed}
     *  @example
     *           var objUtil = $ss.agentcore.utils;
     *           var ret = objUtil.DecodeBase64("dGVtcDJ2YWw=");
     */
    DecodeBase64: function(sInput){
      _logger.info('Entered function: $ss.agentcore.utils.DecodeBase64');
      //g_Logger.Info("ss_utl_DecodeBase64()", "sInput=" + sInput);
      var keyStr = "ABCDEFGHIJKLMNOP" + "QRSTUVWXYZabcdef" +
      "ghijklmnopqrstuv" +
      "wxyz0123456789+\/" +
      "=";
      
      var output = "";
      var chr1, chr2, chr3 = "";
      var enc1, enc2, enc3, enc4 = "";
      var i = 0;
      
      if (sInput == "") 
        return "";
      // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
      var base64test = /[^A-Za-z0-9\+\/\=]/g;
      if (base64test.exec(sInput)) {
        //        g_Logger.Debug("ss_utl_DecodeBase64()",
        //              "There were invalid base64 characters in the input text.\n" +
        //              "Valid base64 characters are A-Z, a-z, 0-9, �+�, �/�, and �=�\n" +
        //              "Expect errors in decoding.");
      }
      sInput = sInput.replace(/[^A-Za-z0-9\+\/\=]/g, "");
      
      do {
        enc1 = keyStr.indexOf(sInput.charAt(i++));
        enc2 = keyStr.indexOf(sInput.charAt(i++));
        enc3 = keyStr.indexOf(sInput.charAt(i++));
        enc4 = keyStr.indexOf(sInput.charAt(i++));
        
        chr1 = (enc1 << 2) | (enc2 >> 4);
        chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
        chr3 = ((enc3 & 3) << 6) | enc4;
        
        output = output + String.fromCharCode(chr1);
        
        if (enc3 != 64) {
          output = output + String.fromCharCode(chr2);
        }
        if (enc4 != 64) {
          output = output + String.fromCharCode(chr3);
        }
        
        chr1 = chr2 = chr3 = "";
        enc1 = enc2 = enc3 = enc4 = "";
      }
      while (i < sInput.length);
      
      //g_Logger.Debug("ss_utl_DecodeBase64()", "Output=" + output);
      return output;
    },
    
    
    /**
     *  @name MatchObjectMembers
     *  @memberOf $ss.agentcore.utils
     *  @function
     *  @description  Checks if all object members of first object match object <br/>
     *                members of second object. It does NOT check vice versa hence <br/>
     *                object 1 culd be a subset of object2 and function will return true.
     *  @param obj1 Object to be matched
     *  @param obj2 Object against which it is to be matched
     *  @returns true/false
     *  @example
     *          var objUtil = $ss.agentcore.utils;
     *          var obj1 = new Object();
     *          obj1["test"] = 1;
     8          obj1["test2"] = 2;
     *          obj1["test3"] = 3;
     *
     *          var obj2 = new Object();
     *          obj2["test"] = 1;
     *          obj2["test2"] = 2;
     *          obj2["test3"] = 3;
     *
     *          var ret = objUtil.MatchObjectMembers(obj1,obj2);
     */
    MatchObjectMembers: function(obj1, obj2){
      _logger.info('Entered function: $ss.agentcore.utils.MatchObjectMembers');
      try {
        for (var i in obj1) {
          if (typeof obj1[i] == typeof obj2[i] &&
          obj1[i] == obj2[i] &&
          obj1[i].toString() == obj2[i].toString()) {
            if (this.MatchObjectMembers(obj1[i], obj2[i])) 
              continue;
            else 
              return false;
          }
          else {
            return false;
          }
        }
      } 
      catch (ex) {
        _exception.HandleException(ex, '$ss.agentcore.utils', 'MatchObjectMembers', arguments);
        //g_Logger.Error("ss_utl_ObjectsMembersAreMatching()", ex.message);
        return false;
      }
      return true;
    },
    
    /**
     *  @name CopyObject
     *  @memberOf $ss.agentcore.utils
     *  @function
     *  @description  Returns a copy of the passed object
     *  @param oSrc object to copy
     *  @returns true/false
     *  @example
     *           var objUtil = $ss.agentcore.utils;
     *           var copyObj = objUtil.CopyObject(obj1);
     */
    CopyObject: function(oSrc){
      _logger.info('Entered function: $ss.agentcore.utils.CopyObject');
      
      if (typeof(oSrc) == 'undefined') 
        return undefined;
      if (oSrc === null) 
        return null;
      if (oSrc === true) 
        return true;
      if (oSrc === false) 
        return false;
      
      if (typeof window != 'undefined') {
        if (oSrc === window) 
          return window;
        if (oSrc === parent && oSrc !== window) 
          return parent;
        if (oSrc === opener && oSrc !== self) 
          return opener;
        if (oSrc.tagName !== undefined && oSrc.id !== undefined) 
          return oSrc;
      }
      
      if (oSrc.constructor !== undefined) {
        var ObjConstructor = oSrc.constructor.toString();
        switch (ObjConstructor) {
          case _constants.ARRAY_CONSTRUCTOR:
            var ArrayCopy = [];
            for (var i = 0; i < oSrc.length; i++) {
              if (oSrc.constructor.prototype[i] == undefined) {
                ArrayCopy[i] = this.CopyObject(oSrc[i]);
              }
            }
            return ArrayCopy;
            
          case _constants.OBJECT_CONSTRUCTOR:
            var ObjectCopy = {};
            for (var itemkey in oSrc) {
              if (oSrc.constructor.prototype[itemkey] == undefined) {
                ObjectCopy[itemkey.toString()] = this.CopyObject(oSrc[itemkey]);
              }
            }
            return ObjectCopy;
            
          case _constants.FUNCTION_CONSTRUCTOR:
            return oSrc;
          case _constants.STRING_CONSTRUCTOR:
            return oSrc.toString();
          case _constants.DATE_CONSTRUCTOR:
            return new Date(oSrc.valueOf());
          case _constants.NUMBER_CONSTRUCTOR:
            return oSrc.valueOf();
        }
        return oSrc;
      }
    },
    
    /**
     *  @name CleanList
     *  @memberOf $ss.agentcore.utils
     *  @function
     *  @description  Does a safe cleanup of array
     *  @param List list/array to clean
     *  @param iStart Position to start at (the first hole, usually)
     *  @returns none
     *  @example
     *           var objUtil = $ss.agentcore.utils;
     *           objUtil.CleanList(list,2)
     */
    CleanList: function(List, iStart){
      _logger.info('Entered function: $ss.agentcore.utils.CleanList');
      try {
        iStart = iStart || 0;
        var i2;
        while (iStart < List.length) {
          if (List[iStart] == undefined) {
            i2 = iStart + 1;
            while (i2 < List.length) {
              if (List[i2] != undefined) {
                List[iStart] = List[i2];
                iStart++;
              }
              i2++;
            }
            List.length = iStart;
          }
          iStart++;
        }
      } 
      catch (e) {
        _exception.HandleException(e, '$ss.agentcore.utils', 'CleanList', arguments);
        //g_Logger.Error("ss_utl_CleanList", e.message);
      }
    },
    
    
    /**
     *  @name HasMask
     *  @memberOf $ss.agentcore.utils
     *  @function
     *  @description  test if specified flag(s) is set in the value
     *  @param iValue value to be tested on
     *  @param iFlag bit(s) in question
     *  @returns boolean
     *  @example
     *  @example
     *           var objUtil = $ss.agentcore.utils;
     *           var ret = objUtil.HasMask(0x00000011,0x00000001);
     */
    HasMask: function(iValue, iFlag){
      _logger.info('Entered function: $ss.agentcore.utils.HasMask');
      return ((iValue & iFlag) == iFlag);
    },
    
     /**
     *  @name GetHandler
     *  @memberOf $ss.agentcore.utils
     *  @function
     *  @description  this will return handler of any object/function having namespace
     *  @param sHandlerName Name of the Function/object to find
     *  @returns object reference / undefined , if not found
     *  @example
     *  @example
     *           var objUtil = $ss.agentcore.utils;
     *           var ret = objUtil.GetHandler("$ss.agetcore.utils.HasMask");
     */
    GetHandler: function(sHandlerName){
      _logger.info('Entered function: GetHandler');
      var root = window;
      var refHandler;
      if(!sHandlerName) return refHandler;
      if (sHandlerName) {
        var aObjectSep = sHandlerName.split(".");
        for (var x = 0; x < aObjectSep.length; x++) {
          if (refHandler) {
            root = refHandler;
          }
          refHandler = root[aObjectSep[x]];
          if (!refHandler) {
            return refHandler; //return undefined if the method is not found
          }
        }
        //use curry to return the method
        return function(){
          return refHandler.apply(root, arguments);
        };
      }
      
    },
 
    
    /**
     *  @name GetLanguage
     *  @memberOf $ss.agentcore.utils
     *  @function
     *  @description  Get the language Code
     *  @param none
     *  @returns string e.g. en for english. However for chinese langcode zh
     *  					the values will be either	chineshe traditional code zhtw or zhcn.
     *  @example
     *  @example
     *           var objUtil = $ss.agentcore.utils;
     *           var ret = objUtil.GetLanguage();
     */
    GetLanguage: function(){
      _logger.debug('Entered function: GetLanguage');
      var _cfg = $ss.agentcore.dal.config;
      var lang = _cfg.GetConfigValue("global", "language", "").toLowerCase();
      // If language isn't set grab from this key
      try {

        lang = (lang) ? lang : $ss.agentcore.dal.registry.GetRegValue("HKLM", "SOFTWARE\\SupportSoft\\ProviderList\\" + _cfg.GetContextValue("SdcContext:ProviderId") + "\\installinfo", "LocaleLangCode").toLowerCase();

        if (lang == "zh") {
          var cc = $ss.agentcore.dal.registry.GetRegValue("HKLM", "SOFTWARE\\SupportSoft\\ProviderList\\" + _cfg.GetContextValue("SdcContext:ProviderId") + "\\installinfo", "LocaleCountryCode").toLowerCase();
          if (cc == "tw" || cc == "hk" || cc == "mo") {
            lang = "zhtw"; // Chinese Traditional TW + HK + MO
          }
          else {
            lang = "zhcn"; // Chinese Simplified CH + SG
          }
        }
      }
      catch (e) {
        _logger.error('GetLanguage Function throws error');
      }
      return (lang) ? lang : _cfg.GetConfigValue("language", "default", "en");
    },
    
    
    /* @name GlobalXLate
     *  @memberOf $ss.agentcore.utils
     *  @function
     *  @description  Translate The Text From Global Resource Bundle.
     *  @param id The resource id to search the for within the global resource file.
     *  @param arStrings A single string or Array of string values to use
     *                   in interpolation. The values in array are used to
     *                   replace placeholders %1% %2% in the localized string.
     *  @returns string  The string found or the id interpolated with the passed values
     *  									and config macro substituted. If no string is found the resource
     *  									id itself is returned
     *  @example
     *  @example
     *           var objUtil = $ss.agentcore.utils;
     *           var ret = objUtil.GlobalXLate("TRANSLATEME");
     */
    GlobalXLate: function(id, arStrings){
      _logger.debug('Entered function: GlobalXLate');
      if (_oGlobalResource.len == 0) 
        return id; // make sure they are loaded
      var transId = id; // set to default
      try {
        if (_oGlobalResource.len && id != null) {
          var oVal = _oGlobalResource[id];
          if (oVal != null) {
            transId = oVal;
            transId = this.LangParseMacros(transId, null); // in common, use it
            if (arStrings != null && typeof(arStrings) != _UNDEFINED) {
              // check for array
              if (arStrings.constructor.toString() != ([]).constructor.toString()) {
                arStrings = new Array(arStrings);
              }
              transId = transId.interpolateString(arStrings);
            }
            
          }
        }
      } 
      catch (ex) {
        _logger.Error("Error in _GlobalXLate()");
      }
      return transId;
    },
    
    /* @name LocalXLate
     *  @memberOf $ss.agentcore.utils
     *  @function
     *  @description  	Translate The Text From Snapin Specific Resource Bundle
     *  @param sSnapin   Name of the Snapin from where local data to be translated
     *  @param id 			The resource id to search the for within the global resource file.
     *  @param arStrings A single string or Array of string values to use
     *                   in interpolation. The values in array are used to
     *                   replace placeholders %1% %2% in the localized string.
     *
     *  @returns string  The string found or the id interpolated with the passed values
     *  									and config macro substituted. If no string is found the resource
     *  									id itself is returned
     *  @example
     *  @example
     *           var objUtil = $ss.agentcore.utils;
     *           var ret = objUtil.LocaXLate("snapin_contact_us","TRANSLATEME");
     */
    LocalXLate: function(sSnapin, id, arStrings){
      _logger.debug('Entered function: LocalXLate');
      if (!_oLocalResource) 
        return id; // make sure they are loaded
      if (typeof(sSnapin) == _UNDEFINED || sSnapin == null || sSnapin == "") {
        //if snapin name is not provided get the data from global
        return this.GlobalXLate(id, arStrings);
      }
      var transId = id; // set to default
      try {
        //if the localized file have not been loaded try to load the files
        if (_oLocalResource[sSnapin] == undefined) {
          $ss.agentcore.utils.LoadSnapinResourceBundle(sSnapin);
        };
        if (_oLocalResource[sSnapin] != null && id != null) { // try the common file
          var oVal = _oLocalResource[sSnapin][id];
          if (oVal != null) {
            transId = oVal;
            transId = this.LangParseMacros(transId, sSnapin); // in common, use it
            if (arStrings != null && typeof(arStrings) != _UNDEFINED) {
              // check for array
              if (arStrings.constructor.toString() != ([]).constructor.toString()) {
                arStrings = new Array(arStrings);
              }
              transId = transId.interpolateString(arStrings);
            }
            
          }
        }
      } 
      catch (ex) {
        _logger.Error("Error in LocalXLate()");
      }
      return transId;
    },
    /* @name LangParseMacros
     *  @memberOf $ss.agentcore.utils
     *  @function
     *  @description   Looks up config macros and global xml id in the sText for
     *               	their substitution.
     *  @param sText 		The text on which macro substitution will be performed
     *  @param sSnapin   Name of the Snapin from where local data to be translated
     *  @example
     *  @example
     *           var objUtil = $ss.agentcore.utils;
     *           var ret = objUtil.LanParseMacros("TRANSLATEME","snapin_contact_us");
     */
    LangParseMacros: function(sText, sSnapin){
      // No Log -- Expensive
      _logger.debug('Entered function: GlobalXLate');
      var sResult = $ss.agentcore.dal.config.ParseMacros(sText);
      try {
        var arResult = sResult.match(/\%GRID:([\w\S]+)\%/g);
        var sResId = "", sReplace = "";
        if (arResult != null) {
          for (var i = 0; i < arResult.length; i++) {
            sResId = arResult[i].match(/\%GRID:([\w\S]+)\%/)[1];
            sReplace = this.GlobalXLate(sResId);
            sResult = sResult.replace(arResult[i], sReplace);
          }
        }
        
        if (typeof(sSnapin) != _UNDEFINED && sSnapin != null) {
          arResult = sResult.match(/\%LRID:([\w\S]+)\%/g);
          sResId = "";
          sReplace = "";
          if (arResult != null) {
            for (var i = 0; i < arResult.length; i++) {
              sResId = arResult[i].match(/\%LRID:([\w\S]+)\%/)[1];
              sReplace = this.LocalXLate(sSnapin, sResId);
              sResult = sResult.replace(arResult[i], sReplace);
            }
          }
        }
      } 
      catch (ex) {
        _logger.error("Error at LangParseMacros");
      }
      return sResult;
    },
     /* @name LoadGlobalResourceBundle
     *  @memberOf $ss.agentcore.utils
     *  @function
     *  @description  Load the global resource bundles specified in Layout config XML
     *  @param none 
     *  @example
     *  @example
     *           var objUtil = $ss.agentcore.utils;
     *           var ret = objUtil.LoadGlobalResourceBundle();
     */
    LoadGlobalResourceBundle: function(){
      var aResourceFile = $ss.GetLayoutResource();
      _loadResourceBundles(_oGlobalResource, aResourceFile)
      
    },

     /* @name LoadSnapinResourceBundle
     *  @memberOf $ss.agentcore.utils
     *  @function
     *  @description  Load the On Demand Snapin spcific resource bundles 
     *  							specified in respective snapin XML
     *  @param sSnapin name 
     *  @example
     *  @example
     *           var objUtil = $ss.agentcore.utils;
     *           var ret = objUtil.LoadSnapinResourceBundle("snapin_contact_us");
     */
    
    LoadSnapinResourceBundle: function(sSnapin){
      if (sSnapin) {
      
        var aResourceFile = $ss.GetSnapinResource(sSnapin);
        _oLocalResource[sSnapin] = _oLocalResource[sSnapin] ||
        {};
        _oLocalResource[sSnapin].status = true;
        _loadResourceBundles(_oLocalResource[sSnapin], aResourceFile)
        
      }
    },
     
     /* @name LoadExternalResourceBundle
     *  @memberOf $ss.agentcore.utils
     *  @function
     *  @description  Load External Content spcific resource bundle file
     *  							available as a part of external content and return the object
     *  @param sSnapin name 
     *  @example
     *  @example
     *           var objUtil = $ss.agentcore.utils;
     *           var ret = objUtil.LoadExternalResourceBundle("mycosutomcontenttype","1234-1312-221",2,"resources.html");
     */
    
    LoadExternalResourceBundle: function(sContentType,sGUID,sVersion,sFileName){
      try {
      if (sContentType && sGUID && sFileName ) {

        var fileRoot = $ss.GetAttribute('startingpoint') + "\\" +
        sContentType +
        "\\" +
        sGUID +
        "." +
        sVersion +
        "\\";
        var sResourceFile = fileRoot + sFileName;
        //[MAC] Check Machine OS Windows/MAC
        if(bMac)
          sResourceFile = _file.BuildPath(fileRoot, sFileName);

        return _loadResourceBundles(null, [sResourceFile])
      }
      } catch(e) {
        //Log the error in loading the external resource file
      }
      return {len:0};

    },
    // Ex url : var o = getQuery('maps.google.co.uk/maps?f=q&q=brighton&ie=UTF8&iwloc=addr');
    
    /**
    *  @name GetQSObjectFromURL
    *  @memberOf $ss.agentcore.utils
    *  @function
    *  @description  Function to get querystring object for a given url
    *  @param url url 
    *  @returns query string object
    *  @example
    *         var oQS = $ss.agentcore.utils.GetQSObjectFromURL('maps.google.co.uk/maps?f=q&q=brighton&ie=UTF8&iwloc=addr');
    *
    */
    GetQSObjectFromURL:function(url)
    {
      var index = url.indexOf('?');
      var query = {};
      
      if(index != -1)
      {
        url = url.substr(index+1,url.length-index);
        
        url.replace(/([^&=]*)=([^&=]*)/g, function (str, grp1, grp2) 
        {
          if (typeof query[grp1] != 'undefined') {
            query[grp1] += ',' + grp2;
          } else {
            query[grp1] = grp2;
          }
        });
      }

      return query;
    },
    
    /**
    *  @name GetQSFromURL
    *  @memberOf $ss.agentcore.utils
    *  @function
    *  @description  function to get the Querystring string from the url
    *  @param url url
    *  @returns querystring string
    *  @example
    *          var strQS = $ss.agentcore.utils.GetQSFromURL(url);
    *
    */
    GetQSFromURL:function(url)
    {
      var index = url.indexOf('?');
      var sQS = "";
      
      if(index != -1)
      {
        sQS = url.substr(index+1,url.length-index);
      }
      
      return sQS;
    },
    
    /**
    *  @name GetRootFromURL
    *  @memberOf $ss.agentcore.utils
    *  @function
    *  @description  function to get the root part from the url
    *  @param url url
    *  @returns root string
    *  @example
    *          var strRoot = $ss.agentcore.utils.GetRootFromURL(url);    *
    */
    GetRootFromURL:function(url)
    {
      var root = url;
      
      if(url && url.length > 0)
      {
        var index = url.indexOf('?');
        
        if(index != -1)
        {
          root = url.substr(0,index);
        }
      }
      
      return root;;
    },
    
    /**
    *  @name CreateRunKey
    *  @memberOf $ss.agentcore.utils
    *  @function
    *  @description Function to create run key under HKCU  
    *  @param value name of the value
    *  @param data data
    *  @returns true/false
    *  @example
    *           var ret = $ss.agentcore.utils.CreateRunKey("Test","TestData");
    *
    */
    CreateRunKey:function(value,data)
    {
      try 
      {
        var root  = _constants.REG_HKCU;
        var key   = "Software\\Microsoft\\Windows\\CurrentVersion\\Run\\";

        return $ss.agentcore.dal.registry.SetRegValue(root, key, value, data);

      } 
      catch (ex) 
      {
      }
      return false;
    },
    
    /**
    *  @name CreateRunOnceKey
    *  @memberOf $ss.agentcore.utils
    *  @function 
    *  @description Function to create runonce key under HKCU  
    *  @param value name of the value
    *  @param data data
    *  @returns true/false
    *  @example
    *           var ret = $ss.agentcore.utils.CreateRunOnceKey();
    *
    */
    CreateRunOnceKey:function(value,data)
    {
      try 
      {
        var root  = _constants.REG_HKCU;
        var key   = "Software\\Microsoft\\Windows\\CurrentVersion\\RunOnce\\";

        return $ss.agentcore.dal.registry.SetRegValue(root, key, value, data);

      } 
      catch (ex) 
      {
      }
      return false;
    },
    
    /**
    *  @name GetBCONTPath
    *  @memberOf $ss.agentcore.utils
    *  @function
    *  @description  Function to get the path of BCONT_NM exe
    *  @returns path of Bcont_nm exe
    *  @example
    *           var path = $ss.agentcore.utils.GetBCONTPath();
    *
    */
    GetBCONTPath:function()
    {
        _logger.info('Entered function: $ss.agentcore.utils.GetBCONTPath');
        var retVal = "";
        try {
          //[MAC] Check Machine OS Windows/MAC
          if(!bMac)
            return _objContainer.GetPath("EXE_PATH");
          else {
            //=======================================================
            //[MAC] Native Call
            var message = {'JSClassNameKey':'CommonUtility','JSOperationNameKey':'GetPath', 'JSArgumentsKey':["EXE_PATH"],   'JSISSyncMethodKey' : '1'}
             var result = _jsBridge.execute(message);
             var parsedJSONObject = JSON.parse(result);
             return parsedJSONObject.Data;
            //=======================================================
          }
        }
        catch (err) {
          _exception.HandleException(err, '$ss.agentcore.utils', 'GetBCONTPath', arguments);
        }
        return retVal;
    },
    
    /**
    *  @name GetSprtCmdPath
    *  @memberOf $ss.agentcore.utils
    *  @function
    *  @description  Function to get the sprtcmd path
    *  @returns path to sprtcmd exe
    *  @example
    *           var path = $ss.agentcore.utils.GetSprtCmdPath();
    *
    */
    GetSprtCmdPath:function() {
      var rootPath = $ss.agentcore.dal.config.GetProviderRootPath(); //get the root path
      if(rootPath) {
        rootPath = "\"" + rootPath;
        rootPath += "bin\\sprtcmd.exe\"";
      }
      return rootPath;
    },

    /**
    *  @name StartSprtCmd
    *  @memberOf $ss.agentcore.utils
    *  @function
    *  @description Function to start/stop sprtcmd 
    *  @param start - if true ,sprtcmd will be started, else sprtcmd will be stopped
    *  @returns none
    *  @example
    *           $ss.agentcore.utils.StartSprtCmd(true);
    *
    */
    StartSprtCmd:function(start) {
      var sSprtCmdPath = this.GetSprtCmdPath();
      if (!sSprtCmdPath)
        return;
      var cmd = sSprtCmdPath + " /P " + $ss.agentcore.dal.config.GetContextValue("SdcContext:ProviderId");
      if(start)
        cmd += " /start";
      else
        cmd += " /stop";
        
      $ss.agentcore.utils.RunCommand (cmd,  _constants.BCONT_RUNCMD_ASYNC);
    },
    
    /**
    *  @name IsSprtCmdRunning
    *  @memberOf $ss.agentcore.utils
    *  @function
    *  @description Function to check if sprtcmd is running or not
    *  @param none
    *  @returns true if sprtcmd is running, false if not running
    *  @example
    *           $ss.agentcore.utils.IsSprtCmdRunning();
    *
    */
    IsSprtCmdRunning: function()
    {    
      //[MAC] Check Machine OS Windows/MAC
      if(bMac) 
        return true;

      _logger.info('Entered function: $ss.agentcore.utils.IsSprtCmdRunning');
      var bProcessRunning = false;
      var providerID = $ss.agentcore.dal.config.GetProviderID().toLowerCase();
      var eServiceList = new Enumerator(_objContainer.GetObject("winmgmts:{impersonationLevel=impersonate}!root/cimv2").ExecQuery("Select CommandLine From Win32_Process where caption='sprtcmd.exe'"));
      for (; !eServiceList.atEnd(); eServiceList.moveNext()) 
      {
        var cmdLine = (new String(eServiceList.item().CommandLine)).toLowerCase();
        if (cmdLine.indexOf(providerID) > -1) 
        {
          bProcessRunning = true;
          break;
        }
      }
      delete eServiceList; 
      return bProcessRunning;
    },

    /**
    *  @name StartSprtSvc
    *  @memberOf $ss.agentcore.utils
    *  @function
    *  @description function to start/stop sprtsvc
    *  @param start - if true the service will be started, if false service will be stopped
    *  @returns none
    *  @example
    *           $ss.agentcore.utils.StartSprtSvc(true);
    *
    */
    StartSprtSvc:function(start)
    {

      var svcName = "sprtsvc";
      
      $ss.agentcore.dal.service.StartWin32Service(svcName,start);
    },

    IsRunningInMacMachine: function() {
      var ret = $.browser.safari;
      return ret;
    },
    
    WriteUsageLog: function (logContent) {
      //[MAC] Get Browser type IE / Safari
      if(bMac) { 
        var message = {'JSClassNameKey':'CommonUtility','JSOperationNameKey':'WriteUsageLog', 'JSArgumentsKey':[logContent], 'JSISSyncMethodKey' : '0'}
        jsBridge.execute(message)
      }
    }
    //end of extended public functions
  
  });
  //end of jQuery Extend
  
  var _UNDEFINED = 'undefined';
  //[MAC] Get Browser type IE / Safari
  var bMac = $.browser.safari;

  //[MAC] JSBridge object used only for MAC
  try {
    var jsBridge = window.JSBridge;
  }
  catch(e) {
  }

  var _objContainer;
  //[MAC] Check Machine OS Windows/MAC
  if(!bMac)
    _objContainer = $ss.agentcore.utils.activex.GetObjInstance();

  var _runCmd_LastErr = 0;
  var _os = null;
  var _constants = $ss.agentcore.constants;
  var _logger = $ss.agentcore.log.GetDefaultLogger("$ss.agentcore.utils");
  var _exception = $ss.agentcore.exceptions;
  
  var _oGlobalResource = {};
  _oGlobalResource.len = 0;
  var _oLocalResource = {};
  
  function _loadResourceBundles(oObjToStore, aResourceFile){
    oObjToStore = oObjToStore ||
    {};
    if (oObjToStore.len == undefined) 
      oObjToStore.len = 0;
    if (aResourceFile) {
      try {
        var langCode = $ss.agentcore.utils.GetLanguage();
        var oResTmpDom = null;
        var oTmpJsObj = null;
        var resDom = null;
        var xPath = null;
        for (var j = 0; j < aResourceFile.length; j++) {
          resDom = $ss.agentcore.dal.xml.LoadXML(aResourceFile[j]);
        //[MAC] Check Machine OS Windows/MAC
        if(!bMac)
          xPath = "//resources/lang[@code='" + langCode + "']/res";
        else
          xPath = ".//resources/lang[@code='" + langCode + "']/res";

          oResTmpDom = $ss.agentcore.dal.xml.GetNodes(xPath, "", resDom);
          //if the the language is not available try looking at default language
          if (oResTmpDom == null) {
            var defLangCode = $ss.agentcore.dal.config.GetConfigValue("global", "default", "en");
        //[MAC] Check Machine OS Windows/MAC
        if(!bMac)
             xPath = "//resources/lang[@code='" + defLangCode + "']/res";
        else
            xPath = ".//resources/lang[@code='" + defLangCode + "']/res";

            oResTmpDom = $ss.agentcore.dal.xml.GetNode(xPath, "", resDom);
          }
          if (oResTmpDom && oResTmpDom.length) {
            for (var i = 0; i < oResTmpDom.length; i++) {
              var oNode = oResTmpDom[i];

            //[MAC] Check Machine OS Windows/MAC
            if(!bMac)
              var iIdName = oNode.getAttribute("id");
            else
              var iIdName = oNode.getAttributeNode("id").nodeValue;
              var sValue = $ss.agentcore.dal.xml.GetNodeText(oNode);
              oObjToStore[iIdName] = sValue;
              oObjToStore.len += 1;
            }
            //oTmpJsObj = $.xmlToJSON(oResTmpDom, "id");
          }
          resDom = null;
          oResTmpDom = null;
        }
      } 
      catch (e) {
        _logger.error("Error at LoadGlobalResourceBundle");
      }
    }
    return oObjToStore;
  }
  
  function _GetCommonFilesPath()
  {
      return $ss.agentcore.dal.config.ExpandSysMacro("%CommonFiles%");
  }
  
})();

var _file = $ss.agentcore.dal.file;

//    /*******************************************************************************
//    ** Name:        ss_utl_Id
//    **
//    ** Purpose:     Short form for getting html element by Id. Will expand for
//    **              different browser and OS support.
//    **
//    ** Parameter:   sId: string representing the id
//    **
//    ** Return:      HTML Object else null
//    *******************************************************************************/
//    function ss_utl_Id(sId)
//    {
//      return document.getElementById(sId);
//    }
//    // TD : check this function is being used
//    /*******************************************************************************
//    ** Name:        set
//    **
//    ** Purpose:     short hand creation of a hash for use with "in" statements
//    **
//    ** Example:
//       if ("1" in set("0","1","2")) {
//         alert("Yes");
//       } else {
//        alert("No");
//       }
//    ** Parameters:  each argument becomes one member of the hash
//    ** Return:      Hash
//    *******************************************************************************/
//    function set() {
//      var o={};
//      for(var i=0;i<arguments.length; i++)
//        o[arguments[i]]=true;
//      return o;
//    }
//    /*******************************************************************************
//    ** Name:         ss_con_GetDriveVolumeName
//    **
//    ** Purpose:      Returns the volumn name for a particular drive
//    **
//    ** Parameter:    sDriveLetter
//    **
//    ** Return:       drive volume name
//    *******************************************************************************/
//    function ss_con_GetDriveVolumeName(sDriveLetter)
//    {
//      var volumeName = "";
//      try
//      {
//        var driveObj = ss_con_pvt_RefFSObject().GetDrive(sDriveLetter);
//        volumeName = driveObj.VolumeName;
//      }
//      catch (err)
//      {
//        g_Logger.Error("ss_con_GetDriveVolumeName()", err.message);
//      }
//      return volumeName;
//    }
///*******************************************************************************
//** Name:         ss_con_GetShortcutTarget
//**
//** Purpose:      Returns the Target of the shortcut
//**
//** Parameter:    Shortcut file name (.lnk)
//**               
//** Return:       Target file name with path
//*******************************************************************************/
//function ss_con_GetShortcutTarget(sLnkFile)
//{
//  var oLink, sTarget = "";
//  try
//  {
//    oLink = ss_con_pvt_RefShellObject().CreateShortcut(sLnkFile);
//    sTarget = oLink.TargetPath;
//  }
//  catch (err)
//  {
//    g_Logger.Error("ss_con_GetShortcutTarget()", err.message + " - File: " + sLnkFile);
//  }
//  return sTarget;
//}


