/** @namespace Holds all firewall related functionality*/
$ss.agentcore.firewall = $ss.agentcore.firewall || {};

(function()
{ 
  $.extend($ss.agentcore.firewall,
  {
  
    /**
    *  @name GetFirewallMgr
    *  @memberOf $ss.agentcore.firewall
    *  @function
    *  @description  Gets the Firewall Manager object
    *  @returns Firewall manager
    *  @example
    *           var objFW = $ss.agentcore.firewall;
    *           var fw = objFW.GetFirewallMgr();
    */
    GetFirewallMgr:function()
    {
      _logger.info('Entered function: $ss.agentcore.firewall.GetFirewallMgr');
      try
      {
        if(_fwMgr == null) 
        {
            _fwMgr  = _utils.CreateObject("SPRT.SprtFWMgr.1");
        }
      } 
      catch(e) 
      {
      }
      return _fwMgr;
    },

    /**
    *  @name GetInstalledPersonalFirewalls
    *  @memberOf $ss.agentcore.firewall
    *  @function
    *  @description Retrieves the list of installed personal firewalls.
    *  @returns List of installed personal firewalls
    *  @example
    *           var objFW = $ss.agentcore.firewall;
    *           var ret = objFW.GetInstalledPersonalFirewalls();
    */
    GetInstalledPersonalFirewalls:function ()
    {
      _logger.info('Entered function: $ss.agentcore.firewall.GetInstalledPersonalFirewalls');
      var pfwList = null;
      try
      {
        if (!(_fwMgr)) {
         _fwMgr = this.GetFirewallMgr();
        }
        pfwList = _fwMgr.GetAllFWs();

      } catch(e) 
      {
         pfwList = null;
      }
      return pfwList;
    },
    
    /**
    *  @name GetFirewallProductID
    *  @memberOf $ss.agentcore.firewall
    *  @function
    *  @description  Returns the product id of the firewall
    *  @param fwEnum enumerator of installed firewalls list obtained from GetInstalledPersonalFirewalls call
    *  @returns String; the product ID of the firewall pointed by fwEnum
    *  @example
    *         var objFW = $ss.agentcore.firewall;
    *         var ret = objFW.GetInstalledPersonalFirewalls();
    *         var iterator = new Enumerator(ret);
    *         var id =objFW.GetFirewallProductID(iterator); 
    */
    GetFirewallProductID:function(fwEnum)
    {
      _logger.info('Entered function: $ss.agentcore.firewall.GetFirewallProductID');
      var prodId = "";
      try
      {
        if ( ! fwEnum.atEnd() ) {
         var pfw = fwEnum.item();
         prodId = pfw.GetProductID();
        }
      } catch(e) {
      }
      return prodId;
    },

    /**
    *  @name GetFirewallProductVendor
    *  @memberOf $ss.agentcore.firewall
    *  @function
    *  @description  Returns the product vendor of the firewall
    *  @param fwEnum enumerator of installed firewalls list obtained from GetInstalledPersonalFirewalls call
    *  @returns String; the product vendor of the firewall pointed by fwEnum
    *  @example
    *         var objFW = $ss.agentcore.firewall;
    *         var ret = objFW.GetInstalledPersonalFirewalls();
    *         var iterator = new Enumerator(ret);
    *         var vendor = objFW.GetFirewallProductVendor(iterator);
    */
    GetFirewallProductVendor:function(fwEnum)
    {
      _logger.info('Entered function: $ss.agentcore.firewall.GetFirewallProductVendor');
      var vendor = "";
      try
      {
        if ( ! fwEnum.atEnd() ) {
         var pfw = fwEnum.item();
         vendor = pfw.GetProductVendor();
        }
      } catch(e) {
      }
      return vendor;
    },

    /**
    *  @name GetFirewallProductVersion
    *  @memberOf $ss.agentcore.firewall
    *  @function
    *  @description  Returns the product version of the firewall
    *  @param fwEnum enumerator of installed firewalls list obtained from GetInstalledPersonalFirewalls call
    *  @returns String; the product version of the firewall pointed by fwEnum
    *  @example
    *         var objFW = $ss.agentcore.firewall;
    *         var ret = objFW.GetInstalledPersonalFirewalls();
    *         var iterator = new Enumerator(ret);
    *         var ver = objFW.GetFirewallProductVersion(iterator);
    */
    GetFirewallProductVersion:function(fwEnum)
    {
      _logger.info('Entered function: $ss.agentcore.firewall.GetFirewallProductVersion');
      var ver = "";
      try
      {
        if ( ! fwEnum.atEnd() ) {
         var pfw = fwEnum.item();
         ver = pfw.GetProductVersion();
        }
      } catch(e) {
      }
      return ver;
    },

    /**
    *  @name GetFirewallProductDescription
    *  @memberOf $ss.agentcore.firewall
    *  @function
    *  @description  Returns the product description of the firewall
    *  @param fwEnum enumerator of installed firewalls list obtained from GetInstalledPersonalFirewalls call
    *  @returns String; the product description of the firewall pointed by fwEnum
    *  @example
    *         var objFW = $ss.agentcore.firewall;
    *         var ret = objFW.GetInstalledPersonalFirewalls();
    *         var iterator = new Enumerator(ret);
    *         var desc = objFW.GetFirewallProductDescription(iterator);
    */
    GetFirewallProductDescription:function(fwEnum)
    {
      _logger.info('Entered function: $ss.agentcore.firewall.GetFirewallProductDescription');
      var desc = "";
      try
      {
        if ( ! fwEnum.atEnd() ) {
         var pfw = fwEnum.item();
         desc = pfw.GetProductDescription();
        }
      } catch(e) {
      }
      return desc;
    },

    /**
    *  @name IsFirewallEnabled
    *  @memberOf $ss.agentcore.firewall
    *  @function
    *  @description Checks if the firewall is enabled 
    *  @param fwEnum enumerator of installed firewalls list obtained from GetPersonalFirewallsList
    *  @returns Boolean; True, if the firewall pointed by fwEnum is enabled else false
    *  @example
    *         var objFW = $ss.agentcore.firewall;
    *         var ret = objFW.GetInstalledPersonalFirewalls();
    *         var iterator = new Enumerator(ret);
    *         var bEnabled =objFW.IsFirewallEnabled(iterator); 
    */
    IsFirewallEnabled:function(fwEnum)
    {
      _logger.info('Entered function: $ss.agentcore.firewall.IsFirewallEnabled');
      var enabled = false;
      try
      {
        if ( ! fwEnum.atEnd() ) {
         var pfw = fwEnum.item();
         enabled = pfw.IsFirewallEnabled();
        }
      } catch(e) {
      }
      return enabled;
    }


  });
  
  var _fwMgr = null;
  var _utils = $ss.agentcore.utils.ui;
  
  
  
})();