/** @namespace Holds all diagnostics functionality*/
$ss.agentcore.diagnostics = $ss.agentcore.diagnostics || {};
/** @namespace Holds all SmartIssue related functions*/
$ss.agentcore.diagnostics.smartissue = $ss.agentcore.diagnostics.smartissue || {};

(function()
{
  
  $.extend($ss.agentcore.diagnostics.smartissue,
  {
    /**
    *  @name CreateIssue 
    *  @memberOf $ss.agentcore.diagnostics.smartissue
    *  @function
    *  @description  
    *  @param problemDescription description of the problem
    *  @param isCustomTemplate Is true is the Issue template is a custom template else false
    *  @param templateFileOrName If isCustomTemplate is true then templateFileOrName is the absoulte path of the template file.<br/>
    *                            If isCustomTemplate is false then templateFileOrName as given in the config file.
    *  @param isCache If true will cache the resulting issue xml
    *  @param isAsync If true will make and asynchronous call
    *  @param asynNotifyHandler Callback function,valid only if isAsync is true
    *  @returns a new issue id on success else null
    *  @example
    *         var objSI = $ss.agentcore.diagnostics.smartissue
    *         var issueID = objSI.CreateIssue("WirelessConfig", true, "E:\\Projects\\SDC\\trunk\\ridev\\user\\tgctlsi\\UnitTest\\si_templates\\default_static.xml", true, false, null);
    */
    CreateIssue : function (problemDescription, isCustomTemplate, templateFileOrName, isCache, isAsync, asynNotifyHandler)
    {
      _logger.info('Entered function: $ss.agentcore.diagnostics.smartissue.CreateIssue');
      var issueTemplate = "";
      
      //First check if isCache is true
      if(isCache == true)
      {
        var cacheObj = _GetCacheObject(problemDescription);
        if (cacheObj) 
        {
          return cacheObj; 
        }
      }
      
      //If it has come till here then it not cached or its miss.
      //Check if customTemplate is required
      if (isCustomTemplate == true)
      {
        //Check if templateFileName exists.
        issueTemplate = templateFileOrName;
      }
      else
      {
        // TD: Get the list of default files first
        issueTemplate = _GetStandardTemplate(templateFileOrName);
      }
      
      try 
      {
        var si = _GetSIObject();
        si.EnableErrorExceptions(true); //Its your reponsibility to catch the exception                      
        si.SetDataSpecFile(templateFileOrName);
        // Create a temp directory
        si.SetDataDirectory(_GetTempDirForIssueFiles());
        var issueId = si.NewIssue(problemDescription); 
        var fileName = si.GetIssueFile(issueId);
        
        _SetCacheObject(problemDescription, issueId) ; //returns a bool
        
        si.SetCurrentIssue(issueId);
        
        return issueId;
      }
      catch(ex)
      {
        _exception.HandleException(ex,'$ss.agentcore.diagnostics.smartissue','CreateIssue',arguments);
      }
      
      return null;
     },
    /**
    *  @name CreateIssueMac 
    *  @memberOf $ss.agentcore.diagnostics.smartissue
    *  @function
    *  @description  
    */
    CreateIssueMac: function(scope, problemDescription, templateFileOrName) {
      controller = scope;
      _CreateIssueMac(this, problemDescription, templateFileOrName);    
    },
    /**
    *  @name CreateIssueMacVA
    *  @memberOf $ss.agentcore.diagnostics.smartissue
    *  @function
    *  @description
    */
    CreateIssueMacVA: function(scope, problemDescription, templateFileOrName) {
      controller = scope;
      _CreateIssueMacVA(this, problemDescription, templateFileOrName);
    },
    
    /**
    *  @name SubmitIssue
    *  @memberOf $ss.agentcore.diagnostics.smartissue
    *  @function
    *  @description  Submitts an issue to the server
    *  @param xmlFilePath Path to the issue xml
    *  @param url url of the issue server
    *  @returns true on success else false
    *  @example
    *         var objSI = $ss.agentcore.diagnostics.smartissue
    *         var retVal = objSI.SubmitIssue(xmlPath,issueServer);    
    */
    SubmitIssue:function(issueID,url)
    {
      //[MAC] Check Machine OS Windows/MAC
      if(bMac) {
        var sIssuePath = $ss.getAttribute("startingpoint").replace("/data", "\\state\\Issues\\");
        sIssuePath = sIssuePath + issueID + ".xml"
        var message = {'JSClassNameKey':'SmartIssue','JSOperationNameKey':'SubmitIssue', 'JSArgumentsKey':[sIssuePath,url], 'JSISSyncMethodKey' : '0'};
        _jsBridge.execute(message);
        return;
      }
      _logger.info('Entered function: $ss.agentcore.diagnostics.smartissue.SubmitIssue');
      var si = _GetSIObject();
      var retVal = true;
        
      try
      {  
        if (!si.SubmitIssueEx(si.GetIssueFile(issueID),url)) 
        {
            retVal = false;
        }
      }
      catch(ex)
      {
        _exception.HandleException(ex,'$ss.agentcore.diagnostics.smartissue','SubmitIssue',arguments);
      }
      
      return retVal;
    },
    
    /**
    *  @name SubmitIssueMac 
    *  @memberOf $ss.agentcore.diagnostics.smartissue
    *  @function
    *  @description  
    */
    SubmitIssueMac:function(scope,issueID,url) {
      controller = scope;
      _SubmitIssueMac(this, issueID,url);

    },

    /**
    *  @name DeleteIssue
    *  @memberOf $ss.agentcore.diagnostics.smartissue
    *  @function
    *  @description Deletes the specified issue 
    *  @param issueID issue id obtained from CreateIssue call
    *  @returns true if the issue is deleted successfully else false
    *  @example
    *           var objSI = $ss.agentcore.diagnostics.smartissue
    *           var retVal = objSI.DeleteIssue(issueID);
    */
    DeleteIssue:function(issueID)
    {
      //[MAC] Check Machine OS Windows/MAC
      if(bMac) {
        var sIssuePath = $ss.getAttribute("startingpoint").replace("/data", "/state/issues/") + issueID + ".xml";
        // sIssuePath = sIssuePath + issueID + ".xml"
        var message = {'JSClassNameKey':'SmartIssue','JSOperationNameKey':'DeleteIssue', 'JSArgumentsKey':[sIssuePath], 'JSISSyncMethodKey' : '0'};
       _jsBridge.execute(message);
       return;
      }
      _logger.info('Entered function: $ss.agentcore.diagnostics.smartissue.DeleteIssue');
      
      var si = _GetSIObject();
      var retVal = true;
      // TD : delete issue not working is not return any status
      
      try
      {
        si.DeleteXMLIssueId(issueID);
      }
      catch(ex)
      {
        _exception.HandleException(ex,'$ss.agentcore.diagnostics.smartissue','DeleteIssue',arguments);
      }
      
      return retVal;
    },

    
    /**
    *  @name GetXMLResponse 
    *  @memberOf $ss.agentcore.diagnostics.smartissue
    *  @function
    *  @description  Returns the issue xml
    *  @param issueID issue id obtained from CreateIssue call
    *  @returns returns the xml string for valid issue id else null
    *  @example
    *           var objSI = $ss.agentcore.diagnostics.smartissue
    *           var xmlData = objSI.GetXMLResponse(issueID); 
    */
    GetXMLResponse : function(issueID)
    {
      _logger.info('Entered function: $ss.agentcore.diagnostics.smartissue.GetXMLResponse');
      
      var si = _GetSIObject();
      var issueFile = si.GetIssueFile(issueID);
      var retData = null;
      
      if(_issuesFolder)
      {
        retData = _file.ReadFile(issueFile);
      }
        
      return retData;       
    },
    
    
    /**
    *  @name GetIssueData
    *  @memberOf $ss.agentcore.diagnostics.smartissue
    *  @function
    *  @description  Get the issue data from the issue xml
    *  @param issueID ID of the issue got from CreateIssue function
    *  @param className class name in the issue xml
    *  @param instanceKeyName instance name in the issue xml
    *  @param propertyName property name ,which needs to be set
    *  @returns returns the value of the property if found else null
    *  @example
    *           var objSI = $ss.agentcore.diagnostics.smartissue
    *           var browser = objSI.GetIssueData(issueID,'BrowserInfo','BrowserInfo','DefaultBrowser');
    */
    GetIssueData: function(issueID, className,instanceKeyName, propertyName)
    {
      _logger.info('Entered function: $ss.agentcore.diagnostics.smartissue.GetIssueData');
      var si = _GetSIObject();
      var retVal = null;
      
      try
      {
      
        si.StartXMLIO(si.GetIssueFile(issueID));
        retVal = si.GetXMLValue(className,instanceKeyName,propertyName);
        si.EndXMLIO();
      }
      catch(ex)
      {
        _exception.HandleException(ex,'$ss.agentcore.diagnostics.smartissue','GetIssueData',arguments);
      }
      
      return retVal;
    },
    
    /**
    *  @name GetIssueDataMac 
    *  @memberOf $ss.agentcore.diagnostics.smartissue
    *  @function
    *  @description  
    */
    GetIssueDataMac: function(issueID, className, propertyName) {
      var message = {'JSClassNameKey':'SmartIssue','JSOperationNameKey':'GetIssueData', 'JSArgumentsKey':[issueID, className, propertyName], 'JSISSyncMethodKey' : '1'};
      var result = _jsBridge.execute(message);
      var parsedJSONObject = JSON.parse(result);
      return parsedJSONObject.Data;
    },

    /**
    *  @name SetIssueData 
    *  @memberOf $ss.agentcore.diagnostics.smartissue
    *  @function
    *  @description  Sets the Issue data in the Issue xml
    *  @param issueID ID of the issue got from CreateIssue function
    *  @param className class name in the issue xml
    *  @param instanceKeyName instance name in the issue xml
    *  @param propertyName property name ,which needs to be set
    *  @param propertyDataType datatype of the property
    *  @param propertyDataValue value of the property
    *  @returns true if the value is set else false
    *  @example
    *         var objSI = $ss.agentcore.diagnostics.smartissue
    *         var retVal = objSI.SetIssueData(issueID,'BrowserInfo','BrowserInfo','DefaultBrowser','string','Internet Explorer');
    */
    SetIssueData : function(issueID, className, instanceKeyName, propertyName, propertyDataType, propertyDataValue)
    {
      _logger.info('Entered function: $ss.agentcore.diagnostics.smartissue.SetIssueData');
      var si = _GetSIObject();
      var retVal = false;
      
      try
      {
        si.StartXMLIO(si.GetIssueFile(issueID));
        // TD: UpdateXMLFile is not returning a bool
        retVal = si.UpdateXMLFile(className,instanceKeyName,propertyName,propertyDataValue,propertyDataType);
        si.EndXMLIO();
      }
      catch(ex)
      {
        _exception.HandleException(ex,'$ss.agentcore.diagnostics.smartissue','SetIssueData',arguments);
      }
            
      return retVal;
    },
    
    // TD : some cleanup functions,this function needs to be called on exit of app/BCONT
    
    /**
    *  @name Cleanup
    *  @memberOf $ss.agentcore.diagnostics.smartissue
    *  @function
    *  @description  Cleans up the temp issue folder
    *  @param none
    *  @returns true on successful cleanup else false
    *  @example
    *         var objSI = $ss.agentcore.diagnostics.smartissue;
    *         var ret = objSI.Cleanup();
    */
    Cleanup:function()
    {
      _logger.info('Entered function: $ss.agentcore.diagnostics.smartissue.Cleanup');
      var retVal = true;
      
      if(_issuesFolder)
      {
        retVal = _file.DeleteDir(_issuesFolder);
      }
      
      return retVal;
    },
    
    /**
    *  @name GetXMLSections
    *  @memberOf $ss.agentcore.diagnostics.smartissue
    *  @function
    *  @description  Return command separated instances of the given
    *                class in the current SmartIssue
    *  @param issueID issue ID
    *  @param sClass class name
    *  @returns Comma sepearated list of instances
    *  @example
    *         var objSI = $ss.agentcore.diagnostics.smartissue
    *         var retVal = objSI.GetXMLSections(issueID,'SDC_Applications');
    */
    GetXMLSections : function(issueID,sClass)
    {
      _logger.info('Entered function: $ss.agentcore.diagnostics.smartissue.GetXMLSections');
      
      var sInstances="";
      try
      {
        
        var si = _GetSIObject();

        si.StartXMLIO(si.GetIssueFile(issueID));
        sInstances = si.GetXMLSections(sClass);
        si.EndXMLIO();
        
      } 
      catch(ex) 
      {
        sInstances = "";
        _exception.HandleException(ex,'$ss.agentcore.diagnostics.smartissue','GetXMLSections',arguments);
      }
      return sInstances;
    },

    /**
    *  @name GetXMLKeys
    *  @memberOf $ss.agentcore.diagnostics.smartissue
    *  @function
    *  @description  Return command separated keys of the given
                   class isntance in the current SmartIssue
    *  @param issueID issue Id
    *  @param sClass class name
    *  @param sInstance class instance name
    *  @returns Comma sepearated list of instances
    *  @example
    *         var objSI = $ss.agentcore.diagnostics.smartissue
    *         var retVal = objSI.GetXMLKeys(issueID,'SDC_Applications',instanceName);
    */
    GetXMLKeys:function(issueID,sClass, sInstance)
    {
      _logger.info('Entered function: $ss.agentcore.diagnostics.smartissue.GetXMLKeys');
      var sKeys = "";
      try
      {
        var si = _GetSIObject();
        
        si.GetIssueFile(issueID);
        si.StartXMLIO(issueFn);
        sKeys = si.GetXMLKeys(sClass, sInstance);
        si.EndXMLIO();
      } 
      catch(ex) {
        sKeys = "";
        _exception.HandleException(ex,'$ss.agentcore.diagnostics.smartissue','GetXMLKeys',arguments);
      }
      return sKeys;
    },

    /**
    *  @name GetXMLDisplayKeys
    *  @memberOf $ss.agentcore.diagnostics.smartissue
    *  @function
    *  @description  Return command separated keys of the given
                   class isntance in the current SmartIssue
    *  @param issueID issue ID
    *  @param sClass class name
    *  @param sInstance class instance name
    *  @returns Comma sepearated list of instances
    *  @example
    *         var objSI = $ss.agentcore.diagnostics.smartissue
    *         var retVal = objSI.GetXMLDisplayKeys(issueID,'SDC_Applications',instanceName);
    */
    GetXMLDisplayKeys:function(issueID,sClass, sInstance)
    {
      _logger.info('Entered function: $ss.agentcore.diagnostics.smartissue.GetXMLDisplayKeys');
      var sKeys = "";
      try
      {
        var si = _GetSIObject();
        
        var issueFn = si.GetIssueFile(issueID);
        si.StartXMLIO(issueFn);
        sKeys = si.GetXMLDisplayKeys(sClass, sInstance);
        si.EndXMLIO();
        
      } catch(ex) {
        sKeys = "";
        _exception.HandleException(ex,'$ss.agentcore.diagnostics.smartissue','GetXMLDisplayKeys',arguments);
      }
      return sKeys;
    },

    /**
    *  @name PersistInfo()
    *  @memberOf $ss.agentcore.diagnostics.smartissue
    *  @function
    *  @description  Persist SmartIssue extenstion data collected during the
    *               flow by writing into the registry. This informatin is
    *               later gathered and submitted to the server.
    *               Data is stored in the registry in the format
    *               Regroot -> SmartIssue
    *                              |-> class name1 -> property list
    *                              |-> class name2 -> property list
    *  @param sClassName class name Eg: SmA_GeneralInfo
    *  @param sPropName property name  Eg: modem_make
    *  @param sPropValue property value Eg: Westell
    *  @returns None
    *  @example
    *         var objSI = $ss.agentcore.diagnostics.smartissue
    *         var retVal = objSI.PersistInfo($ss.agentcore.constants.smartissue.EMAIL_CLASS, $ss.agentcore.constants.smartissue.EMAIL_CONFIGURED, "true");
    */
    PersistInfo:function(sClassName, sPropName, sPropValue)
    {
      var bRet = false;
      try {
        if (LOG_PREFIX == "") 
          LOG_PREFIX = _config.GetConfigValue("smartissue", "si_classname", "SMA_"); 
       
        var regkeySI = _config.GetRegRoot() + "\\" + REG_ROOT;
        bRet = _registry.SetRegValueByType(_constants.REG_TREE, regkeySI + "\\" + LOG_PREFIX + sClassName, sPropName, 1, sPropValue);
      } 
      catch(e) 
      {
      }
      return bRet;
    },


    /**
    *  @name ReadPersistInfo()
    *  @memberOf $ss.agentcore.diagnostics.smartissue
    *  @function
    *  @description  Read SmartIssue extenstion data written during the
    *               flow by writing into the registry.
    *  @param sClassName class name Eg: SmA_GeneralInfo
    *  @param sPropName property name  Eg: modem_make
    *  @param sDefault default value
    *  @returns Property value
    *  @example
    *         var objSI = $ss.agentcore.diagnostics.smartissue
    *         var retVal = objSI.ReadPersistInfo($ss.agentcore.constants.smartissue..ADAPTER_CLASS, $ss.agentcore.constants.smartissue..ADAPTER_CODE);
    */      
    ReadPersistInfo: function(sClassName, sPropName, sDefault) {
      if (LOG_PREFIX == "") 
        LOG_PREFIX = _config.GetConfigValue("smartissue", "si_classname", "SMA_"); 
      
      var value        = (typeof(sDefault) == 'undefined')? "" : sDefault;
      var regkeySI     = _config.GetRegRoot() + "\\" + REG_ROOT;
          var sPropValue = _registry.GetRegValue(_constants.REG_TREE, regkeySI + "\\" + LOG_PREFIX + sClassName, sPropName);

      return (sPropValue == "") ? value: sPropValue;
    },


    /**
    *  @name DeletePersistInfo()
    *  @memberOf $ss.agentcore.diagnostics.smartissue
    *  @function
    *  @description  Delete SmartIssue extenstion data written during the
    *               flow by deleting it from the registry.
    *  @param sClassName class name Eg: SmA_GeneralInfo
    *  @param sPropName property name  Eg: modem_make
    *  @returns None
    *  @example
    *         var objSI = $ss.agentcore.diagnostics.smartissue
    *         var retVal = objSI.DeletePersistInfo("ModemInfo","code");
    */
    DeletePersistInfo:function (sClassName, sPropName)
    {
      if (LOG_PREFIX == "") 
        LOG_PREFIX = _config.GetConfigValue("smartissue", "si_classname", "SMA_"); 
      
      var regkeySI     = _config.GetRegRoot() + "\\" + REG_ROOT;
      return _registry.DeleteRegVal(_constants.REG_TREE, regkeySI + "\\" + LOG_PREFIX + sClassName, sPropName);
    },
    
    /**
    *  @name GetMacId
    *  @memberOf $ss.agentcore.diagnostics.smartissue
    *  @function
    *  @description  Return the macid for the passed issueID,
    *  @param issueID issue Id
    *  @returns MacId
    *  @example
    *         var objSI = $ss.agentcore.diagnostics.smartissue
    *         var retVal = objSI.GetMacId(issueID);
    */
    GetMacId:function(issueID)
    {
    
      var macid ="";
      
      try
      {
        var si = _GetSIObject();
        var issueFn = si.GetIssueFile(issueID);
        si.StartXMLIO(issueFn);
        macid = si.GetXMLSections("PCH_SysInfo");
        if (macid.indexOf(",")>0)
        {
            macid = macid.substring(0, macid.indexOf(",") - 1);
        }
        si.EndXMLIO();
        
      } 
      catch(ex) {
        _exception.HandleException(ex,'$ss.agentcore.diagnostics.smartissue','GetMacId',arguments);
      }
      
      return macid;
    }
    
  });
  
  var _cache = $ss.agentcore.dal.databag;
  var _utils = $ss.agentcore.utils;
  var _config = $ss.agentcore.dal.config;
  var _file = $ss.agentcore.dal.file;
  var _constants = $ss.agentcore.constants;
  var _logger = $ss.agentcore.log.GetDefaultLogger("ss.agentcore.diagnostics.smartissue");
  var _exception = $ss.agentcore.exceptions;
  var _registry = $ss.agentcore.dal.registry;

  var _issuesFolder = null;
  var REG_ROOT = "SmartIssue";
  var LOG_PREFIX = "";
  var smartIssueObj = null;

  
  
  function _GetSIObject()
  {
    return _utils.CreateObject("SPRT.SmartIssue");
  }
  
  function _GetCacheObject(problemDescription)
  {
    var retObj = null;
    
    if(_cache.GetValue(problemDescription))
    {
      retObj = _cache.GetValue(problemDescription);
    }
    
    return retObj;
  }
  
  function _SetCacheObject(problemDescription,siObj)
  {
    // TD : check if we can blindly overwrite
    return _cache.SetValue(problemDescription,siObj);
  }
  
  function _GetTempDirForIssueFiles()
  {
    
    if(_issuesFolder)
    {
      return _issuesFolder;
    }
    
    var dirPath = "c:\\";
    var issuesFolder = "";
    try 
    {
      //Get %UserContext% path
      dirPath = _config.GetContextValue("SdcContext:DirUserServer");
      issuesFolder = dirPath + "\\" + "state\\Issues";
      if (!(_file.FolderExists(dirPath)))
      {
        _file.CreateDir(dirPath);
        
      }
      if (!(_file.FolderExists(issuesFolder)))
      {
         _file.CreateDir(issuesFolder);
      }
    } 
    catch(ex) 
    {
      _exception.HandleException(ex,'$ss.agentcore.diagnostics.smartissue','Cleanup',arguments);
      issuesFolder = dirPath;
    }
    
    _issuesFolder = issuesFolder;
    return _issuesFolder;
  }
  
  function _GetStandardTemplate(templateName)
  {
    return _config.GetConfigValue("smartissue",templateName,"");
  }
  
  //[MAC] New helper "_CreateIssueMac" method used to create smart issue instance
  function _CreateIssueMac(scope, problemDescription, inputFile) {
    smartIssueObj = scope;
    var message = {'JSClassNameKey':'SmartIssue','JSOperationNameKey':'CreateIssue', 'JSArgumentsKey':[problemDescription,inputFile], 'JSISSyncMethodKey' : '0', 'JSCallbackMethodNameKey': '_SmartIssueCreated'}
    _jsBridge.execute(message);
  }

  //[MAC] New helper "_CreateIssueMac" method used to create smart issue instance
  function _CreateIssueMacVA(scope, problemDescription, inputFile) {
    smartIssueObj = scope;
    var message = {'JSClassNameKey':'SmartIssue','JSOperationNameKey':'CreateIssueVACode', 'JSArgumentsKey':[problemDescription,inputFile], 'JSISSyncMethodKey' : '0', 'JSCallbackMethodNameKey': '_SmartIssueCreated'}
    _jsBridge.execute(message);
  }
 
  //[MAC] New helper "_SubmitIssueMac" method used to submit smart issue instance
  function _SubmitIssueMac(scope, issueId, url) {
    smartIssueObj = scope;
    currentIssueId = issueId;
    var sIssuePath = $ss.getAttribute("startingpoint").replace("/data", "\\state\\Issues\\");
    sIssuePath = sIssuePath + issueId + ".xml"
    var message = {'JSClassNameKey':'SmartIssue','JSOperationNameKey':'SubmitIssue', 'JSArgumentsKey':[sIssuePath,url], 'JSISSyncMethodKey' : '0','JSCallbackMethodNameKey': '_SmartIssueSubmitted'};
    _jsBridge.execute(message);
  }

  //[MAC] Get Machine OS type
  var bMac = $.browser.safari;

  //[MAC] JSBridge object used only for MAC
  try {
    var _jsBridge = window.JSBridge;
  }
  catch(e) {
  }
  
})() ;

  var controller = null;
  var currentIssueId = null;

  //[MAC] New helper "_SmartIssueCreated" method used to get state of create
  function _SmartIssueCreated(result) {
    var parsedJSONObject = JSON.parse(result);
    var issueID = parsedJSONObject.Data;
    controller.SmartIssueCreated(issueID);
  }   

  //[MAC] New helper "_SmartIssueCreated" method used to get state of submitted
  function _SmartIssueSubmitted(result) {
    var parsedJSONObject = JSON.parse(result);
    var status = parsedJSONObject.Data;
    controller.SmartIssueSubmitted(status, currentIssueId);
  }
