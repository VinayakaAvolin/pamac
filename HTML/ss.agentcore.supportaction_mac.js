/** @namespace Holds all functionality related to databags,basically used for storing snapin specific data*/
$ss.agentcore.supportaction = $ss.agentcore.supportaction || {};

(function()
{
  $ss.agentcore.supportaction = Class.extend(
    {
      /**
      *  @name init
      *  @memberOf $ss.agentcore.supportaction
      *  @function
      *  @description  constructor
      *  @param guid  guid of the supportaction
      *  @param provider provider name
      *  @param cab cab file name
      *  @param bElevate elevation is required if set to true
      *  @param sElevationLevel specifies the elevation level
      *  @param callback callback function
      *  @param authOption sets the authorization prompt options , For ex : 2 is no prompt
      *  @returns supportaction object
      *  @example
      *         var obj = new $ss.agentcore.supportaction(guid);
      *
      */
      init: function(scope, guid,provider,cab,bElevate,sElevationLevel,callback,authOption) 
      {
        if (!provider) {
          provider = _config.GetProviderID();
        }

        authOption = (typeof(authOption) === "undefined" || authOption === null) ? 2: authOption; //2 is no prompt
        this.HookEvents(this.srCtl);
        this.version    = -1;
        this.cabfile    = cab || this.LocateLatestVersion(guid);
        this.output     = new Array();
        this.callback = callback;
        this.guid = guid;
        _controller = scope;
      },

      HookEvents : function (oSR) 
      {
        var that = this;
      },

      InitializeScriptRunner : function(provider, bElevate, sElevationLevel)
      {
        var oSR = null;
        try
        {
          if (bElevate && _utils.IsWinVST(true))  // instantiate through CM if elevation is required
          {
            var cmCtl = new _utils.CreateObject("SdcUser.TgConfCtl");
            if (cmCtl)
            {
              //cmCtl.SetIdentity(provider);      
              oSR = cmCtl.GetObjectByProgId("SPRT.ScriptRunner","",sElevationLevel);
            }      
          }
          else
          {
            oSR = new _utils.CreateObject("SPRT.ScriptRunner");  
          }
        }
        catch(ex)
        {
        }
        return oSR;
      },

      //
      //Look for the CAB in our nonbrowsable content area.  We are looking
      //for the highest version number available for the specified GUID.
      //
      LocateLatestVersion : function (guid) {
        var rootFolder = _file.GetFolder(_config.ExpandAllMacros("%CONTENTPATH%"));
        var maxVer = -1;
        var actionFolder = null;

        var folder = _file.GetFolder(_file.BuildPath(rootFolder.Path, "sprt_actionlight")); // TODO -- Loosen Restriction.
        for (var oInnerEnum = new Enumerator(folder.SubFolders); !oInnerEnum.atEnd(); oInnerEnum.moveNext())
        {
          var contentFolder = oInnerEnum.item();
          if (contentFolder.Name.indexOf(guid) == 0) {
            var version = contentFolder.Name.substring(guid.length+1);
            if (parseInt(version) > maxVer) {
              maxVer = parseInt(version);
              actionFolder = contentFolder.Path;
            }
          }
          contentFolder = null;
        }

        folder = null;
        rootFolder = null;
        contentFolders = null;

        var result = null;
        if (maxVer > -1 && actionFolder != null) {
          this.version = maxVer;
          result = _file.BuildPath(actionFolder,"composite.cab");
        }
        return result;
      },

      /**
      *  @name EvaluateTest 
      *  @memberOf $ss.agentcore.supportaction
      *  @function
      *  @description  Function to Run the test portion of the script support action
      *  @returns exit code
      *  @example
      *         var obj = new $ss.agentcore.supportaction(guid);
      *         obj.EvaluateTest();
      */
      EvaluateTest : function (guid, cabfile ) {
        _EvaluateTest(guid, cabfile);
        return;

        var iExitCode = -1;
        try {
          if (_runningSupportAction == true) {
            throw (new Exception(-1, "ScriptRunner Re-entrance Protection"));
          }
          _runningSupportAction = true;

          var sCabFile = this.cabfile;
          var xReturn = this.srCtl.EvaluateTest(sCabFile);
          iExitCode = parseInt(xReturn);

          if (isNaN(iExitCode)) {
            throw(new Error(-1, "ScriptedAction.prototype.Evaluate Exit Code Invalid"));
          }
        } catch (ex) {
          iExitCode = -1;
        } finally {
          _runningSupportAction = false;
          return iExitCode;
        }
      },
    
      /**
      *  @name Evaluate 
      *  @memberOf $ss.agentcore.supportaction
      *  @function
      *  @description  Function to Run the current script support action
      *  @returns exit code 
      *  @example
      *         var obj = new $ss.agentcore.supportaction(guid);
      *         obj.Evaluate();
      */
      Evaluate : function (guid, cabfile  ) {
        _Evaluate(guid, cabfile);
        return;
      },
      
      /**
      *  @name EvaluateMessageSA
      *  @memberOf $ss.agentcore.supportaction
      *  @function
      *  @description  Function to Run the current script support action
      *  @returns exit code
      *  @example
      *         var obj = new $ss.agentcore.supportaction(guid);
      *         obj.EvaluateMessageSA();
      */
      EvaluateMsgSA : function (guid, cabfile  ) {
        _EvaluateMsgSA(guid, cabfile);
        return;
      },
      /**
      *  @name SetParameter 
      *  @memberOf $ss.agentcore.supportaction
      *  @function
      *  @description  Function to Set up the inbound parameters for the script
      *  @param parameterName parameter name 
      *  @param parameterValue parameter value
      *  @returns none
      *  @example
      *         var obj = new $ss.agentcore.supportaction(guid);
      *         obj.SetParameter(name,value);
      */
      SetParameter : function( parameterName, parameterValue ) {
        this.srCtl.SetParameter(parameterName, parameterValue);
      },

      /**
      *  @name Abort 
      *  @memberOf $ss.agentcore.supportaction
      *  @function
      *  @description  Function to Abort the current script support action if running
      *  @param iRetCode - return code
      *  @returns none
      *  @example
      *         var obj = new $ss.agentcore.supportaction(guid);
      *         obj.Evaluate();  
      *         obj.Abort();
      *         obj = null;
      *
      */
      Abort : function (iRetCode) 
      {
        if(_runningSupportAction) {
          iRetCode = iRetCode || 0;
          try {
            this.srCtl.Abort(iRetCode);
            _runningSupportAction = false;
          } catch(ex) {
          }
        }
      }    

    });
  
  var _runningSupportAction  = false;
  var _utils = $ss.agentcore.utils;
  var _config = $ss.agentcore.dal.config;
  var _file = $ss.agentcore.dal.file;
})();

var _controller = null;
var _jsBridge = window.JSBridge;

function _Evaluate(guid, cabfile) {
var message = {'JSClassNameKey':'SupportAction','JSOperationNameKey':'Evaluate', 'JSArgumentsKey':[guid,cabfile], 'JSCallbackMethodNameKey' : 'EvaluateCompleted', 'JSISSyncMethodKey' :'0'};
    _jsBridge.execute(message);
}

function _EvaluateMsgSA(guid, cabfile) {
    var message = {'JSClassNameKey':'SupportAction','JSOperationNameKey':'EvaluateMessageSA', 'JSArgumentsKey':[guid,cabfile], 'JSCallbackMethodNameKey' : 'EvaluateCompleted', 'JSISSyncMethodKey' :'0'};
    _jsBridge.execute(message);
}

function _EvaluateTest(guid, cabfile) {
var message = {'JSClassNameKey':'SupportAction','JSOperationNameKey':'EvaluateTest', 'JSArgumentsKey':[guid,cabfile],'JSCallbackMethodNameKey' : 'EvaluateTestCompleted', 'JSISSyncMethodKey' : '0'};
    _jsBridge.execute(message);
}

function EvaluateCompleted(result) {
var parsedJSONObject = JSON.parse(result);
_controller.EvaluateCompleted(parsedJSONObject.Data);
}

function EvaluateTestCompleted(result) {
  var parsedJSONObject = JSON.parse(result);
  _controller.EvaluateTestCompleted(parsedJSONObject.Data);
}
