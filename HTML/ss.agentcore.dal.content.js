//<dependsOn>  
//  <script type="text/javascript" src="..\JSUnit\app\jsUnitCore.js"></script>
//  <script type="text/javascript" src="..\jquery.js"></script>
//  <script type="text/javascript" src="..\json2.js"></script>
//  <script type="text/javascript" src="..\taffy.js"></script>
//  <script type="text/javascript" src="..\ss.agentcore.ns.js"></script>
//  <script type="text/javascript" src="..\log4js-mod.js"></script>
//  <script type="text/javascript" src="..\ss.agentcore.log.js"></script>
//  <script type="text/javascript" src="..\ss.agentcore.exceptions.js"></script>
//  <script type="text/javascript" src="..\ss.agentcore.dal.file.js"></script>
//  <script type="text/javascript" src="..\ss.agentcore.dal.xml.js"></script>
//  <script type="text/javascript" src="..\ss.agentcore.dal.content.js"></script>
////</dependsOn>

/** @namespace Holds all content related functionality*/
$ss.agentcore.dal.content = $ss.agentcore.dal.content || {};

(function() {
  $.extend($ss.agentcore.dal.content,
  {
    /**
    *  @name GetAllContents
    *  @memberOf $ss.agentcore.dal.content
    *  @function
    *  @description  Retrieves list of all contents deployed on client. The 
    *  function returns content for a particular language if specified as a 
    *  parameter
    *  @param oLanguage overloaded parameter that can have one of the following 
    *  values:
    *  <br/>- boolean indicating that the default language should be picked up from
    *  the config file. Note: true is the only value that is logical here. false
    *  will be treated equivalent to no language parameter specified
    *  <br/>- string containing the language code to be filtered against 
    *  e.g "en" for English
    *  @returns array of JSON objects. Each JSON object has the following format: <br/>
    *  { <br/>
    *    cid       : "54df12de-546f-448b-8423-97ef27aecc04", <br/>
    *    version   : "1", <br/>
    *    title     : "test content", <br/>
    *    ctype     : "sprt_articlefaq", <br/>
    *    fid       : "47681286-417c-4961-a38b-ba12d62389b7", <br/>
    *    language  : "en", <br/>
    *    category  : ["email", "network"] <br/>
    *  } <br/>
    *  @example
    *         var _content = $ss.agentcore.dal.content;
    *         Usage 1:
    *         var contents = _content.GetAllContents("en");  
    *         This query retrieves all english content deployed on the client
    *         Usage 2:
    *         var contents = _content.GetAllContents(true);
    *         This query retrieves all content that belong to the default 
    *         language specified in the config file
    *         
    */
    GetAllContents: function(oLanguage) {
      _logger.info('Entered function: $ss.agentcore.dal.content.GetAllContents');
      var oQuery = {};
      if (oLanguage) {
        //if boolean and true: use the default config language
        //else assume string and use as language code
        oQuery.language = typeof (oLanguage) == "boolean" ?
				_utils.GetLanguage() : oLanguage;
      }
      return _content.GetContentsAdvQuery(true, oQuery);
    },
    /**
    *  @name GetContentsByAnyType
    *  @memberOf $ss.agentcore.dal.content
    *  @function
    *  @description  Retrieves list of all contents of a particular 
    *  content type. Since a content can be of one content type only,
    *  this query is equivalent to executing an IN query in SQL. <br/>
    *    e.g select * from content where content_type IN (list of content types)
    *  @param arrContentTypes array containing one or more content type guids
    *  @param oLanguage overloaded parameter that can have one of the following 
    *  values:
    *  <br/>- boolean indicating that the default language should be picked up from
    *  the config file. Note: true is the only value that is logical here. false
    *  will be treated equivalent to no language parameter specified
    *  <br/>- string containing the language code to be filtered against 
    *  e.g "en" for English
    *  @returns array of JSON objects. Each JSON object has the following format: <br/>
    *  { <br/>
    *    cid       : "54df12de-546f-448b-8423-97ef27aecc04", <br/>
    *    version   : "1", <br/>
    *    title     : "test content", <br/>
    *    ctype     : "sprt_articlefaq", <br/>
    *    fid       : "47681286-417c-4961-a38b-ba12d62389b7", <br/>
    *    language  : "en", <br/>
    *    category  : ["email", "network"] <br/>
    *  } <br/>
    *  @example
    *         var _content = $ss.agentcore.dal.content;
    *         Usage 1:
    *         var contents = _content.GetContentsByType(["sprt_articlefaq","sprt_actionlight"], "en"); 
    *         This query returns all FAQs and SupportActions that have their language
    *         attribute set to English 
    *         Usage 2: 
    *         var contents = _content.GetContentsByType(["sprt_articlefaq","sprt_actionlight"], true);
    *         This query returns all FAQs and SupportActions that have their language
    *         attribute set to the default language specified in the config file 
    */
    GetContentsByAnyType: function(arrContentTypes, oLanguage) {
      _logger.info('Entered function: $ss.agentcore.dal.content.GetContentsByAnyType');
      var oQuery = {};
      if (oLanguage) {
        //if boolean and true: use the default config language
        //else assume string and use as language code
        oQuery.language = typeof (oLanguage) == "boolean" ?
				_utils.GetLanguage() : oLanguage;
      }
      return _content.GetContentsAdvQuery(arrContentTypes, oQuery);
    },
    /**
    *  @name GetContentsByAnyFolder
    *  @memberOf $ss.agentcore.dal.content
    *  @function
    *  @description  Retrieves list of contents from a particular folder 
    *  Since a content can belong to one folder only (in the Author Center),
    *  this query is equivalent to executing an IN query in SQL. <br/>
    *    e.g select * from content where folder_guid IN (list of folder guids) <br/>
    *  This search is within the specified content types only <br/>
    *  @param arrContentTypes array containing one or more content type guids
    *  @param arrFolderGuids array of one or more folder guids
    *  @param oLanguage overloaded parameter that can have one of the following 
    *  values:
    *  <br/>- boolean indicating that the default language should be picked up from
    *  the config file. Note: true is the only value that is logical here. false
    *  will be treated equivalent to no language parameter specified
    *  <br/>- string containing the language code to be filtered against 
    *  e.g "en" for English
    *  @returns array of JSON objects. Each JSON object has the following format: <br/>
    *  { <br/>
    *    cid       : "54df12de-546f-448b-8423-97ef27aecc04", <br/>
    *    version   : "1", <br/>
    *    title     : "test content", <br/>
    *    ctype     : "sprt_articlefaq", <br/>
    *    fid       : "47681286-417c-4961-a38b-ba12d62389b7", <br/>
    *    language  : "en", <br/>
    *    category  : ["email", "network"] <br/>
    *  } <br/>
    *  @example
    *         var _content = $ss.agentcore.dal.content;
    *         Usage 1:
    *         var contents = _content.GetContentsByAnyFolder(["sprt_resource"],["47681286-417c-4961-a38b-ba12d62389b7"],"en");  
    *         This query returns all English resource contents from the specified 
    *         folder
    *         Usage 2:
    *         var contents = _content.GetContentsByAnyFolder(["sprt_resource"],["47681286-417c-4961-a38b-ba12d62389b7"],true);  
    *         This query returns all the resource contents from the specified
    *         folder, whose language is set to the default language specified
    *         in the config file
    */
    GetContentsByAnyFolder: function(arrContentTypes, arrFolderGuids, oLanguage) {
      _logger.info('Entered function: $ss.agentcore.dal.content.GetContentsByAnyFolder');
      var oQuery = {
        // search for content in any of the folders
        fid: { has: arrFolderGuids }
      };
      if (oLanguage) {
        //if boolean and true: use the default config language
        //else assume string and use as language code
        oQuery.language = typeof (oLanguage) == "boolean" ?
				_utils.GetLanguage() : oLanguage;
      }

      return _content.GetContentsAdvQuery(arrContentTypes, oQuery);
    },


    /**
    *  @name GetContentByCid
    *  @memberOf $ss.agentcore.dal.content
    *  @function
    *  @description  Retrieves content which matched the passed CID.    
    *  This search is within the specified content types only <br/>
    *  @param arrContentTypes array containing one or more content type guids
    *  @param sCid cid of the content
    *  @param oLanguage overloaded parameter that can have one of the following 
    *  values:
    *  <br/>- boolean indicating that the default language should be picked up from
    *  the config file. Note: true is the only value that is logical here. false
    *  will be treated equivalent to no language parameter specified
    *  <br/>- string containing the language code to be filtered against 
    *  e.g "en" for English
    *  @returns a JSON objects. JSON object has the following format: <br/>
    *  { <br/>
    *    cid       : "54df12de-546f-448b-8423-97ef27aecc04", <br/>
    *    version   : "1", <br/>
    *    title     : "test content", <br/>
    *    ctype     : "sprt_articlefaq", <br/>
    *    fid       : "47681286-417c-4961-a38b-ba12d62389b7", <br/>
    *    language  : "en", <br/>
    *    category  : ["email", "network"] <br/>
    *  } <br/>
    *  @example
    *         var _content = $ss.agentcore.dal.content;
    *         Usage 1:
    *         var contents = _content.GetContentByCid(["sprt_resource"],"54df12de-546f-448b-8423-97ef27aecc04","en");  
    *         This query returns English resource content with the specified CID
    *         Usage 2:
    *         var contents = _content.GetContentByCid(["sprt_resource"],"54df12de-546f-448b-8423-97ef27aecc04",true);  
    *         This query returns the resource content with the specified 
    *         CID, whose language is set to the default language specified
    *         in the config file
    */
    GetContentByCid: function(arrContentTypes, sCid, oLanguage) {
      _logger.info('Entered function: $ss.agentcore.dal.content.GetContentByCid');
      var oQuery = {
        // search for content with the cid
        cid: sCid
      };
      if (oLanguage) {
        //if boolean and true: use the default config language
        //else assume string and use as language code
        oQuery.language = typeof (oLanguage) == "boolean" ?
				_utils.GetLanguage() : oLanguage;
      }

      var content = _content.GetContentsAdvQuery(arrContentTypes, oQuery) || null;
      if (content) {
        return content[0];
      }

      return content;
    },

    /**
    *  @name GetContentByTitle
    *  @memberOf $ss.agentcore.dal.content
    *  @function
    *  @description  Retrieves list of all contents whose title matches
    *  the given title. This function is typically expected to return 
    *  an array with just one content and can be used to uniquely identify
    *  a targeted content when its guid is not known. This search is 
    *  within the specified content type only
    *  @param sContentType string containing the content type guid
    *  @param sTitle string containing the content title
    *  @returns array of JSON objects. Each JSON object has the following format: <br/>
    *  { <br/>
    *    cid       : "54df12de-546f-448b-8423-97ef27aecc04", <br/>
    *    version   : "1", <br/>
    *    title     : "test content", <br/>
    *    ctype     : "sprt_articlefaq", <br/>
    *    fid       : "47681286-417c-4961-a38b-ba12d62389b7", <br/>
    *    language  : "en", <br/>
    *    category  : ["email", "network"] <br/>
    *  } <br/>
    *  @example
    *         var _content = $ss.agentcore.dal.content;
    *         var contents = _content.GetContentByTitle("sprt_articlefaq", "Sample FAQ");  
    */
    GetContentByTitle: function(sContentType, sTitle) {
      _logger.info('Entered function: $ss.agentcore.dal.content.GetContentByTitle');
      var oQuery = {
        title: sTitle
      };
      return _content.GetContentsAdvQuery([sContentType], oQuery);
    },
    /**
    *  @name GetContentsByAnyCategory
    *  @memberOf $ss.agentcore.dal.content
    *  @function
    *  @description  Retrieves list of contents that belong to one or more
    *  categories. Since a content can belong to multiple categories this
    *  query is equivalent to executing an OR query in SQL. <br/>
    *    e.g select * from content where (category = x OR category = y)<br/>
    *  This search is within the specified content types only
    *  @param arrContentTypes array containing one or more content type guids
    *  @param arrCategories array of one or more categories
    *  @param oLanguage overloaded parameter that can have one of the following 
    *  values:
    *  <br/>- boolean indicating that the default language should be picked up from
    *  the config file. Note: true is the only value that is logical here. false
    *  will be treated equivalent to no language parameter specified
    *  <br/>- string containing the language code to be filtered against 
    *  e.g "en" for English
    *  @returns array of JSON objects. Each JSON object has the following format: <br/>
    *  { <br/>
    *    cid       : "54df12de-546f-448b-8423-97ef27aecc04", <br/>
    *    version   : "1", <br/>
    *    title     : "test content", <br/>
    *    ctype     : "sprt_articlefaq", <br/>
    *    fid       : "47681286-417c-4961-a38b-ba12d62389b7", <br/>
    *    language  : "en", <br/>
    *    category  : ["email", "network"] <br/>
    *  } <br/>
    *  @example
    *         var _content = $ss.agentcore.dal.content;
    *         Usage 1:
    *         var contents = _content.GetContentsByAnyCategory(["sprt_articlefaq"],["email","network"], "en"); 
    *         This query returns english FAQ contents which belong to either 
    *         email or network categories or both 
    *         Usage 2:
    *         var contents = _content.GetContentsByAnyCategory(["sprt_articlefaq"],["email","network"], true); 
    *         This query returns FAQ contents which belong to either email or
    *         network categories or both and whose language is set to the 
    *         default language specified in the config file
    */
    GetContentsByAnyCategory: function(arrContentTypes, arrCategories, oLanguage) {
      _logger.info('Entered function: $ss.agentcore.dal.content.GetContentsByAnyCategory');
      var oQuery = {
        // search for content in any of the categories
        category: { has: arrCategories }
      };
      if (oLanguage) {
        //if boolean and true: use the default config language
        //else assume string and use as language code
        oQuery.language = typeof (oLanguage) == "boolean" ?
				_utils.GetLanguage() : oLanguage;
      }

      return _content.GetContentsAdvQuery(arrContentTypes, oQuery);
    },    
    /**
    *  @name GetContentsByAllCategories
    *  @memberOf $ss.agentcore.dal.content
    *  @function
    *  @description  Retrieves list of contents that belong to all the 
    *  listed categories. Since a content can belong to multiple categories
    *  this query is equivalent to executing an AND query in SQL. <br/>
    *    e.g select * from content where (category = x AND category = y)<br/>
    *  This search is within the specified content types only
    *  @param arrContentTypes array containing one or more content type guids
    *  @param arrCategories array of one or more categories
    *  @param oLanguage overloaded parameter that can have one of the following 
    *  values:
    *  <br/>- boolean indicating that the default language should be picked up from
    *  the config file. Note: true is the only value that is logical here. false
    *  will be treated equivalent to no language parameter specified
    *  <br/>- string containing the language code to be filtered against 
    *  e.g "en" for English
    *  @returns array of JSON objects. Each JSON object has the following format: <br/>
    *  { <br/>
    *    cid       : "54df12de-546f-448b-8423-97ef27aecc04", <br/>
    *    version   : "1", <br/>
    *    title     : "test content", <br/>
    *    ctype     : "sprt_articlefaq", <br/>
    *    fid       : "47681286-417c-4961-a38b-ba12d62389b7", <br/>
    *    language  : "en", <br/>
    *    category  : ["email", "network"] <br/>
    *  } <br/>
    *  @example
    *         var _content = $ss.agentcore.dal.content;
    *         Usage 1:
    *         var contents = _content.GetContentsByAllCategories(["sprt_articlefaq"],["email","network"],"en"); 
    *         This query returns english FAQ contents which belong to both 
    *         email and network categories 
    *         Usage 2:
    *         var contents = _content.GetContentsByAllCategories(["sprt_articlefaq"],["email","network"],true); 
    *         This query returns FAQ contents which belong to both email and 
    *         network categories and whose language is set to the default 
    *         language specified in the config file
    *         
    */
    GetContentsByAllCategories: function(arrContentTypes, arrCategories, oLanguage) {
      _logger.info('Entered function: $ss.agentcore.dal.content.GetContentsByAllCategories');
      var oQuery = {
        // search for content that belong to ALL the categories
        category: { hasAll: arrCategories }
      };
      if (oLanguage) {
        //if boolean and true: use the default config language
        //else assume string and use as language code
        oQuery.language = typeof (oLanguage) == "boolean" ?
				_utils.GetLanguage() : oLanguage;
      }

      return _content.GetContentsAdvQuery(arrContentTypes, oQuery);
    },
    /**
    *  @name GetContentCountByAllCategories
    *  @memberOf $ss.agentcore.dal.content
    *  @function
    *  @description  Gets the number of contents of a particular content type
    *  that belong to all the given categories
    *  @param sContentType string containing the content type guid
    *  @param arrCategories array of one or more categories
    *  @param oLanguage overloaded parameter that can have one of the following 
    *  values:
    *  <br/>- boolean indicating that the default language should be picked up from
    *  the config file. Note: true is the only value that is logical here. false
    *  will be treated equivalent to no language parameter specified
    *  <br/>- string containing the language code to be filtered against 
    *  e.g "en" for English
    *  @returns integer that indicates the number of contents
    *  @example
    *         var _content = $ss.agentcore.dal.content;
    *         Usage 1:
    *         var count = _content.GetContentCountByAllCategories(["sprt_articlefaq"],["email","network"],"en"); 
    *         This query returns the number of english FAQ contents that 
    *         belong to both email and network categories 
    *         Usage 2:
    *         var count = _content.GetContentCountByAllCategories(["sprt_articlefaq"],["email","network"],true); 
    *         This query returns the number of FAQ contents that belong to both 
    *         email and network categories and whose language is set to the 
    *         default language specified in the config file
    */
    GetContentCountByAllCategories: function(sContentType, arrCategories, oLanguage) {
      _logger.info('Entered function: $ss.agentcore.dal.content.GetContentCountByAllCategories');
      var contents = _GetContentsTaffyDB();
      var findResults = [];
      var oQuery = {
        // search for content that belong to ALL the categories
        category: { hasAll: arrCategories }
      };
      if (oLanguage) {
        //if boolean and true: use the default config language
        //else assume string and use as language code
        oQuery.language = typeof (oLanguage) == "boolean" ?
				_utils.GetLanguage() : oLanguage;
      }

      try {
        var oTaffyDB = _GetContentTypeTaffyDB(sContentType);
        // use find instead of get- we don't want the objects
        findResults = oTaffyDB.find(oQuery);
      }
      catch (ex) {
        findResults = [];
        _logger.error("Exception occured in %1% - \n Code: %2%,Name: %3% \n  Description: %4% \n Query: %5% \n", "GetContentCountByAllCategories", ex.number, ex.name, ex.message.JSON.stringify(oQuery));
      }
      return findResults.length;
    },
    /**
    *  @name GetContentDetailsAsXML
    *  @memberOf $ss.agentcore.dal.content
    *  @function
    *  @description  Loads and returns the deployed XML for the content. 
    *  This xml is the guid.ver.xml file located within the guid.ver
    *  folder for each content.
    *  @param sContentType string containing the content type guid
    *  @param sContentGuid string containing the content guid
    *  @param sContentVersion string containing the content version
    *  @returns xml dom of the content's xml file
    *  @example
    *         var _content = $ss.agentcore.dal.content;
    *         var contentXML = _content.GetContentDetailsAsXML("sprt_articlefaq","4e818805-db80-42ef-a7e8-6db6cd9c4b81","2");        
    */
    GetContentDetailsAsXML: function(sContentType, sContentGuid, sContentVersion) {
      _logger.info('Entered function: $ss.agentcore.dal.content.GetContentDetailsAsXML');
      var filePath = $ss.GetAttribute('startingpoint') + "\\"
	  					+ sContentType + "\\"
						+ sContentGuid + "." + sContentVersion + "\\"
						+ sContentGuid + "." + sContentVersion + ".xml";

      return _xml.LoadXML(filePath, false);
    },
    /**
    *  @name GetContentDetailsAsJSON
    *  @memberOf $ss.agentcore.dal.content
    *  @function
    *  @description  Loads and returns the contents of the deployed XML
    *  of a content, in JSON format. This returns the same information as
    *  GetContentDetailsAsXML() in JSON format
    *  @param sContentType string containing the content type guid
    *  @param sContentGuid string containing the content guid
    *  @param sContentVersion string containing the content version
    *  @returns JSON object
    *  @example
    *         var _content = $ss.agentcore.dal.content;
    *         var contentJSONObj = _content.GetContentDetailsAsJSON("sprt_articlefaq","4e818805-db80-42ef-a7e8-6db6cd9c4b81","2");        
    */
    GetContentDetailsAsJSON: function(sContentType, sContentGuid, sContentVersion) {
      _logger.info('Entered function: $ss.agentcore.dal.content.GetContentDetailsAsJSON');
      var contentXML = _content.GetContentDetailsAsXML(sContentType, sContentGuid, sContentVersion);
      var jsonObj;
      try {
        //[MAC] Check Machine OS Windows/MAC
        if(!bMac)
          jsonObj = $.xmlToJSON(contentXML);
        else
          jsonObj = $.xmlToJSON(contentXML.responseXML);
      }
      catch (ex) {
        jsonObj = {};
        _logger.error("Exception occured in %1% - \n Code: %2%,Name: %3% \n  Description: %4% \n", "GetContentDetailsAsJSON", ex.number, ex.name, ex.message);
      }
      contentXML = null;
      return jsonObj;
    },
    /**
    *  @name GetContentProperty
    *  @memberOf $ss.agentcore.dal.content
    *  @function
    *  @description  Loads deployed guid.ver.xml for the content and 
    *  returns the value for the requested property. This function should
    *  be used to retrieve a limited number of properties (one or two). 
    *  In order to retrieve many/all properties of a content, 
    *  use GetContentDetailsAsXML() and read all properties and fields from the XML
    *  @param sContentType string containing the content type guid
    *  @param sContentGuid string containing the content guid
    *  @param sContentVersion string containing the content version
    *  @param sProperty string containing the property column name
    *  @returns string containing the property value
    *  @example
    *         var _content = $ss.agentcore.dal.content;
    *         var contentDesc = _content.GetContentProperty("sprt_articlefaq","4e818805-db80-42ef-a7e8-6db6cd9c4b81","2","scc_description");        
    */
    GetContentProperty: function(sContentType, sContentGuid, sContentVersion, sProperty) {
      _logger.info('Entered function: $ss.agentcore.dal.content.GetContentProperty');
      var oContentDOM = _content.GetContentDetailsAsXML(sContentType, sContentGuid, sContentVersion);
      var sXPath;
      //[MAC] Check Machine OS Windows/MAC
      if(!bMac)
        sXPath = "//property-set/property[@name='" + sProperty + "']";
      else
        sXPath = "//sprt/content-set/content/property-set/property[@name='" + sProperty + "']";

      var sPropValue = "";
      try {
        if (oContentDOM) {
          var oPropNode = _xml.GetNode(sXPath, "", oContentDOM);
          if (oPropNode) {
            sPropValue = _xml.GetNodeText(oPropNode)
          }
        }
      }
      catch (ex) {
        sPropValue = "";
        _logger.error("Exception occured in %1% - \n Code: %2%,Name: %3% \n  Description: %4% \n Content: %5%, Version: %6%, Property: %7%, Content Type : %8% \n", "GetContentProperty", ex.number, ex.name, ex.message, sContentGuid, sContentVersion, sProperty, sContentType);
      }

      oContentDOM = null;
      return sPropValue;
    },
    /**
    *  @name GetContentFieldValue
    *  @memberOf $ss.agentcore.dal.content
    *  @function
    *  @description  Loads deployed guid.ver.xml for the content and 
    *  returns the value for the requested Field. This function should
    *  be used to retrieve a limited number of fields (one or two). 
    *  In order to retrieve many/all fields of a content, 
    *  use GetContentDetailsAsXML() and read all properties and fields from the XML
    *  @param sContentType string containing the content type guid
    *  @param sContentGuid string containing the content guid
    *  @param sContentVersion string containing the content version
    *  @param sField string containing the field name
    *  @params sValue string containing the field column name
    *  @returns string containing the property value
    *  @example
    *         var _content = $ss.agentcore.dal.content;
    *         var contentDesc = _content.GetContentProperty("sprt_contentlet","57f48bc1-9f5d-42ec-97a4-184ba35b0fbe","2","ID", "sccf_field_value_char");        
    */
    GetContentFieldValue: function(sContentType, sContentGuid, sContentVersion, sField, sValue) {
      _logger.info('Entered function: $ss.agentcore.dal.content.GetContentFieldValue');
      var oContentDOM = _content.GetContentDetailsAsXML(sContentType, sContentGuid, sContentVersion);
      var sXPath;
      //[MAC] Check Machine OS Windows/MAC
      if(!bMac)
        sXPath = "//field-set/field[@name='" + sField + "']/value [@name ='" + sValue + "']";
      else
        sXPath = "//sprt/content-set/content/field-set/field[@name='" + sField + "']/value [@name ='" + sValue + "']";
      var sFieldValue = "";
      try {
        if (oContentDOM) {
          var oFieldNode = _xml.GetNode(sXPath, "", oContentDOM);
          if (oFieldNode) {
            sFieldValue = _xml.GetNodeText(oFieldNode)
          }
        }
      }
      catch (ex) {
        sFieldValue = "";
        _logger.error("Exception occured in %1% - \n Code: %2%,Name: %3% \n  Description: %4% \n Content: %5%, Version: %6%, Field: %7%, Content Type : %8% \n", "GetContentFieldValue", ex.number, ex.name, ex.message, sContentGuid, sContentVersion, sField, sContentType);
      }

      oContentDOM = null;
      return sFieldValue;
    },
    /**
    *  @name GetContentsAdvQuery
    *  @memberOf $ss.agentcore.dal.content
    *  @function
    *  @description  Generic API that allows the author to execute a 
    *  custom TaffyDB query.
    *  @param oContentTypes overloaded parameter that can have one of the following 
    *  values:
    *  - boolean indicating if query has to be performed on all content types
    *  Note: true is the only logical parameter here
    *  - array containing list of content types to perform the query on
    *  @param oQuery overloaded parameter that can have one of the following 
    *  values:
    *  - JSON object containing the custom TaffyDB query
    *  - string containing the custom TaffyDB query
    *  @returns array of JSON objects
    *  @example
    *         var _content = $ss.agentcore.dal.content;
    *         Usage 1:
    *         var oQuery = {
    *                        category : {has : ["email","network"] }
    *                      };
    *         var contents = _content.GetContentsAdvQuery(["sprt_articlefaq", "sprt_resource"], oQuery);
    *         This query returns all FAQs and resources that belong to either 
    *         email or network or both categories
    *         Usage 2:
    *         var contents = _content.GetContentsAdvQuery(true,"{\"category\":{\"has\":[\"email\",\"network\"]}}"); 
    *         This query returns contents of any content type, that belong to
    *         either email or network or both categories
    *  		  Important: the passed in query string should be a valid JSON 
    *  		  object 
    */
    GetContentsAdvQuery: function(oContentTypes, oQuery) {
      _logger.info('Entered function: $ss.agentcore.dal.content.GetContentsAdvQuery');
      var contents;
      var retContents = [];
      var oTaffyDBQuery = {}, oTaffyDB;

      try {
        // handle the query parameter
        switch (typeof (oQuery)) {
          case "string":
            //convert the query string into a valid json obj
            oTaffyDBQuery = JSON.parse(oQuery);
            break;
          case "object":
            //assume already a valid json obj
            oTaffyDBQuery = oQuery;
            break;
          default:
            break;
        }

        //handle the content types parameter
        switch (typeof (oContentTypes)) {
          case "boolean":
            // if true, means search through all content types
            if (oContentTypes) {
              contents = _GetAllTaffyDBs();
              for (var ctype in contents) {
                var oTaffyDB = contents[ctype];
                retContents = retContents.concat(oTaffyDB.get(oTaffyDBQuery));
              }
            }
            break;
          case "object":
            if (oContentTypes instanceof Array) {
              for (var i = 0; i < oContentTypes.length; i++) {
                // get taffy db for this content type
                var oTaffyDB = _GetContentTypeTaffyDB(oContentTypes[i]);
                retContents = retContents.concat(oTaffyDB.get(oTaffyDBQuery));
              }
            }
            break;
          default:
            break;
        }
        //Get contents with latest version in case of duplication due improper pruning
        retContents = _RemoveOlderVersionOfEachContent(retContents);

      }
      catch (ex) {
        retContents = [];
        _logger.error("Exception occured in %1% - \n Code: %2%,Name: %3% \n  Description: %4% \n Query: %5% \n", "GetContentsAdvQuery", ex.number, ex.name, ex.message, JSON.stringify(oTaffyDBQuery));
      }
      return retContents;
    },
    /**
    *  @name GetFolderDetails
    *  @memberOf $ss.agentcore.dal.content
    *  @function
    *  @description  This function returns details for a particular folder. 
    *  Each folder maps to a folder created in the Author Center, that is 
    *  deployed and available as meta data on the client. Information for 
    *  a folder is loaded into the folder hash only on demand. Once loaded
    *  it is cached and can be accessed directly from the folder hash
    *  @param sFolderGuid string containing the guid of the folder
    *  @returns a JSON object containing the folder details. The JSON object 
    *  has the following format: <br/>
    *    { <br/>
    *      fid        : "54df12de-546f-448b-8423-97ef27aecc04", <br/>
    *      name       : "Support Content", <br/>
    *      attributes : "0", <br/>
    *      parent     : "04eb8b23-8af9-43ed-bc14-49f8a3840fb4", <br/>
    *      subfolders : [{ <br/>
    *                       fid         : "fb946c3d-29eb-4427-934b-d36952005bf0", <br/>
    *                       name        : "SupportActions", <br/>
    *                       attributes  : "0", <br/>
    *                       parent      : "sprt_root_folder" <br/>
    *                     },<br/>
    *                     {<br/>
    *                       fid         : "34a379bb-cfde-4862-a571-052fa73ba197",<br/>
    *                       name        : "FAQs",<br/>
    *                       attributes  : "0",<br/>
    *                       parent      : "sprt_root_folder"<br/>
    *                     },<br/>
    *                   ]<br/>
    *    } <br/>
    *  @example
    *         var _content = $ss.agentcore.dal.content;
    *         _content.GetFolderDetails("54df12de-546f-448b-8423-97ef27aecc04");  
    */
    GetFolderDetails: function(sFolderGuid) {
      _logger.info('Entered function: $ss.agentcore.dal.content.GetFolderDetails');
      var oFolderHash = _GetFolderHash();
      // JSON object to return
      var oRetFolder = {};

      try {
        if (!oFolderHash) {
          oFolderHash = new Object();
        }

        if (oFolderHash[sFolderGuid]) {
          // exists in cache - return object from cache
          return oFolderHash[sFolderGuid];
        }

        // does not exist in cache, create object and load into cache
        // load folder.xml
        var sFolderXMLPath = $ss.GetAttribute('startingpoint') + "\\folder.xml";
        var oFolderDOM = _xml.LoadXML(sFolderXMLPath, false);

        // get folder name, attributes, parent
        var sXPathFolderNode = "//folders/folder[guid='" + sFolderGuid + "']";
        var oJSONObj;

        if (oFolderDOM) {
          var oFolderNode = _xml.GetNode(sXPathFolderNode, "", oFolderDOM);
          oJSONObj = _GetFolderDetailsAsJSON(oFolderNode);

          if (oJSONObj) {
            oRetFolder.fid = oJSONObj.fid;
            oRetFolder.name = oJSONObj.name;
            oRetFolder.attributes = oJSONObj.attributes;
            oRetFolder.parent = oJSONObj.parent;
          }
        }

        // get subfolders for the folder
        var arrSubFolders = new Array();
        var sXPathSubFolders = "//folders/folder[parent='" + sFolderGuid + "']";

        if (oFolderDOM) {
          var oSubFolderNodes = _xml.GetNodes(sXPathSubFolders, "", oFolderDOM);
          if (oSubFolderNodes && oSubFolderNodes.length) {
            for (var j = 0; j < oSubFolderNodes.length; j++) {
              oJSONObj = _GetFolderDetailsAsJSON(oSubFolderNodes[j]);
              if (oJSONObj) {
                arrSubFolders[arrSubFolders.length] = oJSONObj;
              }
            }
          }
          oRetFolder.subfolders = arrSubFolders;
        }

        // update the hash with some minimal check: oRetFolder.fid should be defined
        // else don't update hash - something not ok
        if (oRetFolder.fid) {
          oFolderHash[sFolderGuid] = oRetFolder;
        }
        else {
          oRetFolder = null;
        }

        oFolderDOM = null;
      }
      catch (ex) {
        oRetFolder = null;
        oFolderDOM = null;
        _logger.error("Exception occured in %1% - \n Code: %2%,Name: %3% \n  Description: %4% \n Folder: %5% \n", "GetFolderDetails", ex.number, ex.name, ex.message, sFolderGuid);
      }

      return oRetFolder;
    },

    /**
    *  @name GetValidContentsForDisplay
    *  @memberOf $ss.agentcore.dal.content
    *  @function
    *  @description  This function function accepts array of JSON object along with field name 
    *  to look up in XML and Date for comparision.
    *  @param arrContentJson array of JSON objects
    *  @param oStartFieldDate optional, object containing sStartFieldName and oStartDate for comparision.
    *  @param oEndFieldDate optional, object containing sEndFieldName and oEndDate for comparision.
    *  @returns array of JSON object which are valid for display. 
    *  @example
    *         var _content = $ss.agentcore.dal.content;
    *         _content.GetValidContentsForDisplay(arrContentJson, oStartFieldDate, oEndFieldDate);
    */
    GetValidContentsForDisplay: function(arrContentJson, oStartFieldDate, oEndFieldDate) {
      var retArrJson = [];

      oStartFieldDate = oStartFieldDate || {};
      var oStartDate = (oStartFieldDate.date) || new Date();
      var sStartField = (oStartFieldDate.fieldName) || "displayTimestart";
      oEndFieldDate = oEndFieldDate || {};
      var oEndDate = (oEndFieldDate.date) || new Date();
      var sEndField = (oEndFieldDate.fieldName) || "displayTimeend";

      for (var i = 0; i < arrContentJson.length; i++) {
        var oContentDOM = _content.GetContentDetailsAsXML(arrContentJson[i]["ctype"], arrContentJson[i]["cid"], arrContentJson[i]["version"]);
        var oActDate = _GetDateFromXml(oContentDOM, sStartField);
        if (oActDate) {
          if (oActDate > oStartDate) {
            oContentDOM = null;
            continue;
          }
        }
        var oExpDate = _GetDateFromXml(oContentDOM, sEndField);
        if (oExpDate) {
          if (oExpDate < oEndDate) {
            oContentDOM = null;
            continue;
          }
        }
        retArrJson[retArrJson.length] = arrContentJson[i];
        oContentDOM = null;
      }
      //Returns array of JSON which are valid for Display
      return retArrJson;
    },

    /**
    *  @name GetResource
    *  @memberOf $ss.agentcore.dal.content
    *  @function
    *  @description  This function returns the resource path of any resource GUID
    *  @param resGUID resource GUID
    *  @param bRelative optional, boolean, if true returns the relative path 
    *                   from data folder else returns absolute path.
    *  @example
    *         var _content = $ss.agentcore.dal.content;
    *         _content.GetResource("2c49a7c1-0773-4171-b50d-d43a96eea23e");
    */
    GetResource: function(resGUID, bRelative) {
      var resPath = "";
      bRelative = bRelative || false;
      if (!resGUID) return resPath;
      //no cached resources available load it ..
      if (!_resources[resGUID]) {

        _loadResource(resGUID);
      }
      if (_resources[resGUID] && _resources[resGUID].isAvailable === false) return resPath;
      if (_resources[resGUID].isAvailable === true) {
        resPath = _resUrl + resGUID + "." + _resources[resGUID].v + "\\" + _resources[resGUID].file;
        if (!bRelative) resPath = window.startingpoint + "\\" + resPath;
      }
      return resPath;

    },

    /**
    *  @name IsContentValid
    *  @memberOf $ss.agentcore.dal.content
    *  @function
    *  @description  This function validates if the content passed is valid or not
    *  return: true if content is valid else false
    *  @param sContentGuid: Content Guid
    *  @param sContentVersion: Content Version
    *  @example
    *         var _content = $ss.agentcore.dal.content;
    *         var bContentValid = _content.IsContentValid("4da330ed-77fe-4c3c-a950-3a65542c0260", "4");
    */
    IsContentValid: function(sContentGuid, sContentVersion) {
      var bValid = !(_IsAPruneDelayedContent((sContentGuid + "." + sContentVersion)));
      //ToDo: Check if Content is not expired, not done coz binary also needs modification to support this
      //bValid = (bValid && _IsActiveContent((sContentGuid + "." + sContentVersion))); 
      return bValid;
    },

    /**
    *  @name RemovePruneDelayedContentsIn
    *  @memberOf $ss.agentcore.dal.content
    *  @function
    *  @description  This function returns the array of contents in which prune delayed content is removed
    *  @param arrContents: Array of contents folder name in form "ContentGUID.version"
    *  @example
    *         var _content = $ss.agentcore.dal.content;
    *         var aPruneDelayedContents = _content.RemovePruneDelayedContentsIn(arrContents);
    */
    RemovePruneDelayedContentsIn: function(arrContents) {
      //[MAC] Check Machine OS Windows/MAC
      if(!bMac) {
        var arrPruneDelayedContent = _GetPruneDelayedContentList();
        for(var i = 0; i < arrPruneDelayedContent.length; i ++)
          for(var j = 0; j < arrContents.length; j++)
            if(arrPruneDelayedContent[i] === arrContents[j]) {
              arrContents.splice(j, 1);
              break;
            } 
      }
      return arrContents;
    },

    /**
    *  @name GetPruneDelayedContentList
    *  @memberOf $ss.agentcore.dal.content
    *  @function
    *  @description  This function returns the array of contents which are marked for Delayed Pruning
    *  @param 
    *  @example
    *         var _content = $ss.agentcore.dal.content;
    *         var aPruneDelayedContents = _content.GetPruneDelayedContentList();
    */
    GetPruneDelayedContentList: function() {
      var arrPruneDelayedContentList = _GetPruneDelayedContentList();
      if(arrPruneDelayedContentList.length === 0) return new Array();
      else return new Array(arrPruneDelayedContentList);
    },

    /**
    *  @name IsPruneDelayed
    *  @memberOf $ss.agentcore.dal.content
    *  @function
    *  @description  This function returns true, if there is any content marked for Delayed Pruning
    *  else false, if no content is marked for Pruning.
    *  @param 
    *  @example
    *         var _content = $ss.agentcore.dal.content;
    *         var aPruneDelayedContents = _content.IsPruneDelayed();
    */
    IsPruneDelayed: function() {
      var arrPruneDelayedContentList = _GetPruneDelayedContentList();
      if(arrPruneDelayedContentList.length > 0) 
        return true;
      else 
        return false;
    },

    /**
    *  @name ClearContentCache
    *  @memberOf $ss.agentcore.dal.content
    *  @function
    *  @description  This function clears up the in-memory content DB and 
    *  folder hash. This function should be called only from the framework (e.g
    *  on completion of sync).
    *  @example
    *         var _content = $ss.agentcore.dal.content;
    *         _content.ClearContentCache();
    */
    ClearContentCache: function() {

      delete _contents;
      _contents = {
        oContents: {},
        bAllContentTypesLoaded: false,
        oFolderHash: {}
      };

      delete _pruneDelayedContents;
      _pruneDelayedContents = {
        aContents: [],
        bLoadedContents: false
      };
      //empty hash with folder data

      //empty resource hash
      _resources = {};
    }
  });

  var _contents = {
    oContents: {},
    bAllContentTypesLoaded: false,
    oFolderHash: {}
  };

  var _pruneDelayedContents = {
    aContents: [],
    bLoadedContents: false
  };

  function _IsAPruneDelayedContent(sContent) {
    var arrPruneDelayedContent = _GetPruneDelayedContentList();
    for(var i = 0; i < arrPruneDelayedContent.length; i ++)
      if(arrPruneDelayedContent[i] === sContent.toLowerCase()) {
        return true;
      }
    return false;
  }

  function _IsPruneDelayedContentsLoaded() {
    return _pruneDelayedContents.bLoadedContents;
  }

  function _GetPruneDelayedContentList() {
    if(!_IsPruneDelayedContentsLoaded()) {
      _LoadPruneDelayedContentList();
    }
    return _pruneDelayedContents.aContents;
  }

  function _LoadPruneDelayedContentList() {
    var sPruneDelayedContents = "";
    var sPruneFile = _config.GetContextValue("SdcContext:DirUserServer") + "state\\PruneDelayedContentList.txt";
    if (_file.FileExists(sPruneFile)) { //Read from file
      sPruneDelayedContents = _file.ReadFile(sPruneFile)
    } else { //Fall back to registry if file doesn't exits
      var sAppUser = _config.GetContextValue("SdcContext:UserName");
      var sRegSprt = "Software\\SupportSoft\\ProviderList\\";
      var sRegApplication = _config.GetProviderID() + "\\Users\\" + sAppUser;
      var sRegPath = sRegSprt + sRegApplication;
      sPruneDelayedContents = _registry.GetRegValue("HKCU", sRegPath, "PruneDelayedContentList");
    }

    if (sPruneDelayedContents !== "" && sPruneDelayedContents !== null) {
      sPruneDelayedContents = sPruneDelayedContents.toLowerCase();
      _pruneDelayedContents.aContents = sPruneDelayedContents.split(",");
    }
    _pruneDelayedContents.bLoadedContents = true;
  }

  function _GetDateFromXml(oContentDOM, fieldName) {
    if (oContentDOM) {
      var sXpathNode = "/sprt/content-set/content/field-set/field [@name='" + fieldName + "']";
      var oDate = (_xml.GetNode(sXpathNode, "", oContentDOM)) || {};
      var sDate = oDate.text || null;
      if (sDate) {
        if (sDate.indexOf("T") > -1) {
          sDate = sDate.split("T");             //Splits sDate to sDate[0] = Date and sDate[1] = Time
          if (sDate[0].indexOf("-") > -1) {
            sDate[0] = sDate[0].split("-");     //Splits sDate to sDate[0][0] = Year, sDate[0][1] = Month and sDate[0][2] = Date
            sDate[0][1] = sDate[0][1] * 1 - 1;  //Reduce month by 1 digit since Jan = 0
            sDate[1] = sDate[1].split(":");     //Splits sDate to sDate[1][0] = Hour, sDate[1][1] = Minutes
            var oDate = new Date(sDate[0][0], sDate[0][1], sDate[0][2], sDate[1][0], sDate[1][1]); //Get Date in Standard Format
            return oDate;
          }
        }
      }
    }
  }

  function _RemoveOlderVersionOfEachContent(aExistingContentList) {
    var oLatestVersionOfContents = {};
    for (var i = 0; i < aExistingContentList.length; i++) {
      var sCid = aExistingContentList[i].cid;
      if (!oLatestVersionOfContents[sCid]) {
        oLatestVersionOfContents[sCid] = aExistingContentList[i];
      }
      else if (parseInt(aExistingContentList[i].version) > parseInt(oLatestVersionOfContents[sCid].version)) {
        oLatestVersionOfContents[sCid] = aExistingContentList[i];
      }
    }
    return ($ss.agentcore.utils.MakeArrayFromHashValues(oLatestVersionOfContents));
  }

  function _GetContentsTaffyDB() {
    return _contents["oContents"];
  }

  function _AreAllContentTypesLoaded() {
    return _contents["bAllContentTypesLoaded"];
  }

  function _SetContentsTaffyDB(options) {
    jQuery.extend(true, _contents, options);
  }

  function _GetFolderHash() {
    return _contents["oFolderHash"];
  }

  function _SetFolderHash(options) {
    jQuery.extend(true, _folders, options);
  }

  function _GetAllTaffyDBs() {
    //we need to return the contents object holding all 
    //content type taffy dbs	
    if (_AreAllContentTypesLoaded()) {
      //all content types loaded, return the contents object
      return _GetContentsTaffyDB();
    }
    //need to load for all content types
    _CreateContentsTaffyDB(true);
    return _GetContentsTaffyDB();
  }

  function _GetContentTypeTaffyDB(sContentType) {
    var contents = _GetContentsTaffyDB();
    if (contents[sContentType]) {
      //content type already loaded. return taffydb object
      return contents[sContentType];
    }
    //load taffydb for the content type
    _CreateContentsTaffyDB(sContentType);
    return contents[sContentType];
  }

  function _CreateContentsTaffyDB(oContentType) {
    var arrJSONObjs;
    var contents = _GetContentsTaffyDB();
    var sRootPath = $ss.GetAttribute('startingpoint');
    var sContentFileName = "content.js";

    try {
      if (typeof (oContentType) == "boolean" && oContentType) {
        //create in-memory db for all content types
        // Get all content type folders EXCEPT sprt_package, sprt_snapin, sprt_layout
        var regex = new RegExp("^(?!sprt_package|sprt_layout|sprt_snapin)", "i");
        var arrCntTypeFolders = _file.GetSubFoldersList(sRootPath, regex);
        for (var i = 0; i < arrCntTypeFolders.length; i++) {
          // create a taffydb per content type if it does not exist
          if (!contents[arrCntTypeFolders[i]]) {
            arrJSONObjs = _GetContentJSON(sRootPath, arrCntTypeFolders[i], sContentFileName)
            contents[arrCntTypeFolders[i]] = new TAFFY(arrJSONObjs);
          }
          //alert(contents[arrCntTypeFolders[i]].stringify())
        }
        // we have loaded all content types. set flag to indicate this
        _SetContentsTaffyDB({
          bAllContentTypesLoaded: true
        });
      }
      else {
        //create in-memory DB only for content type requested
        if (!contents[oContentType]) {
          arrJSONObjs = _GetContentJSON(sRootPath, oContentType, sContentFileName)
          contents[oContentType] = new TAFFY(arrJSONObjs);
        }
      }
    }
    catch (ex) {
      _logger.error("Exception occured in %1% - \n Code: %2%,Name: %3% \n  Description: %4% \n", "_CreateContentsTaffyDB", ex.number, ex.name, ex.message);
    }
  }

  function _GetContentJSON(sRootPath, sContentTypeFolder, sContentFile) {
    var arrJSONObjs = [];
    var arrCntFolders = _file.GetSubFoldersList(sRootPath + "\\" + sContentTypeFolder);
    arrCntFolders = _content.RemovePruneDelayedContentsIn(arrCntFolders);
    var userPrefLanguage = _utils.GetLanguage();
    for (var j = 0; j < arrCntFolders.length; j++) {
      try {
        var sContentJSONFile = sRootPath + "\\" + sContentTypeFolder + "\\" + arrCntFolders[j] + "\\" + sContentFile;
        data = MVC.request(sContentJSONFile)
        if (data) {
          data = JSON.parse(data);
          if(data instanceof Array){
            data.forEach(function(elem){
              if(elem.hasOwnProperty("language") && elem.language == userPrefLanguage){              
                arrJSONObjs[arrJSONObjs.length] = elem;
              }
            });
          }else{
            if(data.hasOwnProperty("language") && data.language == userPrefLanguage){
              arrJSONObjs[arrJSONObjs.length] = data;
            }
          }
        }
      }
      catch (ex) {
        // only one content affected, continue loading the rest
        _logger.error("Exception occured in %1% - \n Code: %2%,Name: %3% \n  Description: %4% \n", "_GetContentJSON", ex.number, ex.name, ex.message);
      }
    }
    return arrJSONObjs;
  }

  function _GetFolderDetailsAsJSON(oFolderNode) {
    var retJSONObj;
    var sFolderGuid, sFolderName, sFolderAttr, sFolderParent;
    try {
      //parse folder node to get name, attributes, parent
      if (oFolderNode && oFolderNode.childNodes && oFolderNode.childNodes.length) {
        for (var i = 0; i < oFolderNode.childNodes.length; i++) {
          var oChildNode = oFolderNode.childNodes[i];
          switch (oChildNode.nodeName) {
            case "guid":
            //[MAC] Check Machine OS Windows/MAC
            if(!bMac)
              sFolderGuid = oChildNode.text;
            else
              sFolderGuid = oChildNode.textContent;
              break;
            case "name":
              //[MAC] Check Machine OS Windows/MAC
              if(!bMac)
                sFolderName = oChildNode.text;
              else
                sFolderName = oChildNode.textContent;
              break;
            case "attributes":
              //[MAC] Check Machine OS Windows/MAC
              if(!bMac)
                sFolderAttr = oChildNode.text;
              else
                sFolderAttr = oChildNode.textContent;
              break;
            case "parent":
              //[MAC] Check Machine OS Windows/MAC
              if(!bMac)
                sFolderParent = oChildNode.text;
              else
                sFolderParent = oChildNode.textContent;
              break;
            default:
              break;
          }
        }
      }

      retJSONObj = {
        fid: sFolderGuid,
        name: sFolderName,
        attributes: sFolderAttr,
        parent: sFolderParent
      };
    }
    catch (ex) {
      retJSONObj = null;
      _logger.error("Exception occured in %1% - \n Code: %2%,Name: %3% \n  Description: %4% \n", "_GetFolderDetailsAsJSON", ex.number, ex.name, ex.message);
    }

    return retJSONObj;
  }

  function _loadResource(resGUID) {
    var resObj = {};
    resObj.isAvailable = false;
    var sResourcePath = $ss.GetAttribute('startingpoint') + "\\sprt_resource";
    var resFolder = _file.GetSubFoldersList(sResourcePath, resGUID);
    resFolder = _content.RemovePruneDelayedContentsIn(resFolder);
    if (resFolder && resFolder.length > 0) {
      var version = resFolder[0].split(".")[1];
      var resDefXMLPath = sResourcePath + "\\" + resFolder + "\\" + resFolder + ".xml";
      var oContentDOM = _content.GetContentDetailsAsXML("sprt_resource", resGUID, version);
      var sXpathNode = "/sprt/content-set/content/field-set/field [@name='Raw Document']/value [@name ='sccfb_field_value_blob']";
      var oFileDom = (_xml.GetNode(sXpathNode, "", oContentDOM)) || {};
      var sFileName = oFileDom.text || null;
      if (sFileName) {
        resObj.isAvailable = true;
        resObj.v = version;
        resObj.file = sFileName;
      }
      oContentDOM = null;
      oFileDom = null;
    }
    _resources[resGUID] = resObj;

  }

  var _logger = $ss.agentcore.log.GetDefaultLogger("$ss.agentcore.dal.content");
  var _exception = $ss.agentcore.exceptions;
  var _file = $ss.agentcore.dal.file;
  var _xml = $ss.agentcore.dal.xml;
  var _config = $ss.agentcore.dal.config;
  var _content = $ss.agentcore.dal.content;
  var _registry = $ss.agentcore.dal.registry;
  var _utils = $ss.agentcore.utils;
  var _resources = {};
  var _resUrl = "sprt_resource\\";

})();
 
