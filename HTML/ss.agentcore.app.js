
MVC.Initializer(function(){
  //get the errors reported by
  
  var aErrors = $ss.GetShellError();
  var aLogger = $ss.agentcore.log.GetDefaultLogger("$ss.agentcore.app.Initializer");
  var _config = $ss.agentcore.dal.config;
  var _registry = $ss.agentcore.dal.registry;
  var _const = $ss.agentcore.constants;
  var _ini = $ss.agentcore.dal.ini;
  var _file = $ss.agentcore.dal.file;
  var _utils = $ss.agentcore.utils;

  //[MAC] Get Machine OS type
  var bMac = $.browser.safari;

  //[MAC] JSBridge object used only for MAC
  try {
    var _jsBridge = window.JSBridge;
  }
  catch(e) {
  }

  //load the includes as defined by the layouts and snapins

  var aStyles = $ss.GetLayoutStyles();
  for (var i = 0; i < aStyles.length; i++) {
    if (!_file.FileExists(aStyles[i])) {
      aErrors.push(_const.SHELL_SKINCSS_NOTFOUND);
    }
  }

  var isFirstSynNeeded = (_config.GetConfigValue("global", "force_first_sync", "true") === "true");
  var rootUserConfigKey = "Software\\SupportSoft\\ProviderList\\" + _config.GetProviderID() + "\\users\\" + _config.GetContextValue("SdcContext:UserName");
  var prune_date = _registry.GetRegValue("HKCU", rootUserConfigKey, "LastPrunedTime");
  var hasFirstPruneDone = (prune_date != "") ? true : false;
  var forceCopyDefault = (_config.GetConfigValue("global", "def_copy_on_no_snapin", "false") === "true");
  var isPreview = (_ini.GetIniValue("", "SETUP", "preview", "0") === "1");
  if (!isPreview) {
    if (isFirstSynNeeded && !hasFirstPruneDone) {
      aErrors.push(_const.SHELL_FIRST_SYNC_NOTDONE);
    }
    else {
      var isPruneComlete = _utils.IsPruneComplete();
      var waitForSyncToComplete = (_config.GetConfigValue("global", "wait_for_sync_complete", "false") === "true");
      if (!isPruneComlete && waitForSyncToComplete) {
        aErrors.push(_const.SHELL_SYNC_IN_PROGRESS);
      }
    }
    var hasError = false;
    //Handle all kind of errorrs
    if (aErrors.length > 0) {
      var configUsed = _config.GetConfigToUse();
      if (configUsed === "minibcont") {
        //[MAC] Check Machine OS Windows/MAC
        if(!bMac) { 
          aLogger.fatal("Minibocnt is being closed: reason: %1%", aErrors.toString());
        }
        //start sprtcmd fire Prune Event and go..
        hasError = true;
      }
      else {
        aLogger.fatal("Application is being closed: reason: %1%", aErrors.toString());
        var errorFile = _ini.GetIniValue("", "SETUP", "errorFile", "");

        if (errorFile && _file.FileExists(errorFile)) {
          errorFile = errorFile + "?errorCode=" + aErrors.toString();
          $("#errorIframe").attr("src", errorFile);
          $("#errorIframe").show();
          return;
        }
        else {
          hasError = true;
          var objCmdLine = _utils.GetObjectFromCmdLine();
          var hiddenConfig = _ini.GetIniValue("", "SETUP", "starthidden", "0");
          var isBcontStartHidden = objCmdLine.starthidden || (hiddenConfig === "1");
          //var errorMessage = _ini.GetIniValue("", "SETUP", "errorMessage", "Content for the page is not yet available. Application will be closed. Please check again later.");

          if (!isBcontStartHidden) {
            var sLangCode = $ss.agentcore.utils.GetLanguage();
            var errorMessage=$ss.agentcore.dal.config.GetConfigValue_Ex("agentcore_messages",sLangCode, "errorMessage" ,"Content for the page is not yet available. Application will be closed. Please check again later."); 
            alert(errorMessage);
          }
        }
      }
    }

    if (hasError) {
      window.external.Hide();
      if (forceCopyDefault) {
        var forceCopy = true;
        for (var x = 0; x < aErrors.length; x++) {
          if (_const.SHELL_NO_DEFCOPY_ON_ERROR[aErrors[x]]) {
            forceCopy = false;
            break;
          }
        }
        if (forceCopy) { //stop the sprt command
          _utils.StartSprtCmd(false);
          _utils.Sleep(2000);
          _registry.DeleteRegKey("HKCU", rootUserConfigKey);
        }
      }
      if(!_utils.IsSprtCmdRunning()) {
        _utils.StartSprtCmd(true);
        _utils.Sleep(1000);
        window.external.FireAgentEvent("sync_prune");
        _utils.Sleep(200);
      }

      _utils.CloseBcont();
      return;
    }
  }

  if (aStyles && aStyles.length) {
    for (var i = 0; i < aStyles.length; i++) {
      //var oStylesheet = document.createStyleSheet(aStyles[i]);
      //removed createstylesheets to avoid IE problem which limits the number of stylesheets
      //to be created using createStyleSheet to 32
      //[MAC] Check Machine OS Windows/MAC
      if(!bMac) {
        $("head").append('<link rel="stylesheet" href="' + aStyles[i] + '" id="layout_style_' + i + '" type="text/css" media="screen">');
      }
      else {
         var message = {'JSClassNameKey':'File','JSOperationNameKey':'GetNativePath', 'JSArgumentsKey':[aStyles[i]], 'JSISSyncMethodKey' : '1'}
        var result = _jsBridge.execute(message);
        var parsedJSONObject = JSON.parse(result);
        $("head").append('<link rel="stylesheet" href="' + parsedJSONObject.Data + '" id="layout_style_' + i + '" type="text/css" media="screen">');
      }
    }
  }

  //load the styles as defined by the snapins individually
  var aStyles = $ss.GetSnapinStyles();
  if (aStyles && aStyles.length) {
    for (var i = 0; i < aStyles.length; i++) {
      //[MAC] Check Machine OS Windows/MAC
      if(!bMac)
        $("head").append('<link rel="stylesheet" href="' + aStyles[i] + '" id="snapin_style_' + i + '" type="text/css" media="screen">');
      else {
        var message = {'JSClassNameKey':'File','JSOperationNameKey':'GetNativePath', 'JSArgumentsKey':[aStyles[i]], 'JSISSyncMethodKey' : '1'}
        var result = _jsBridge.execute(message);
        var parsedJSONObject = JSON.parse(result);
        $("head").append('<link rel="stylesheet" href="' + parsedJSONObject.Data + '" id="snapin_style_' + i + '" type="text/css" media="screen">');
      }
    }
  }

  var aLayoutIncludes = $ss.GetLayoutIncludes();
  if (aLayoutIncludes && aLayoutIncludes.length) {
    for (var i = 0; i < aLayoutIncludes.length; i++) {
      //[MAC] Check Machine OS Windows/MAC
      if(!bMac)
       $.include(aLayoutIncludes[i]);
      else {
        var message = {'JSClassNameKey':'File','JSOperationNameKey':'GetNativePath', 'JSArgumentsKey':[aLayoutIncludes[i]], 'JSISSyncMethodKey' : '1'}
        var result = _jsBridge.execute(message);
        var parsedJSONObject = JSON.parse(result);
        $.include(parsedJSONObject.Data);
     }
    }
  }

  var aLayoutControllers = $ss.GetLayoutControllers();
  if (aLayoutControllers && aLayoutControllers.length) {
    for (var i = 0; i < aLayoutControllers.length; i++) {
      //[MAC] Check Machine OS Windows/MAC
      if(!bMac)
        $.include(aLayoutControllers[i]);
      else {
        var message = {'JSClassNameKey':'File','JSOperationNameKey':'GetNativePath', 'JSArgumentsKey':[aLayoutControllers[i]], 'JSISSyncMethodKey' : '1'}
        var result = _jsBridge.execute(message);
        var parsedJSONObject = JSON.parse(result);
        $.include(parsedJSONObject.Data);
      }
    }
  }

  var aRegFiles = $ss.GetSnapinRegistrationFiles();
  if (aRegFiles && aRegFiles.length) {
    for (var i = 0; i < aRegFiles.length; i++) {
      //[MAC] Check Machine OS Windows/MAC
      if(!bMac)
        $.include(aRegFiles[i]);
      else {
        var message = {'JSClassNameKey':'File','JSOperationNameKey':'GetNativePath', 'JSArgumentsKey':[aRegFiles[i]], 'JSISSyncMethodKey' : '1'}
        var result = _jsBridge.execute(message);
        var parsedJSONObject = JSON.parse(result);
        $.include(parsedJSONObject.Data);
      }
    }
  }

});
