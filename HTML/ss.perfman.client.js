$ss.snapin = $ss.snapin || {};

$ss.snapin.perfman = $ss.snapin.perfman || {};

$ss.snapin.perfman.Optimizer = MVC.Class.extend({
	init: function(helper, aScope) {
	this.status = 0;
	this.scope = aScope;
	this.helper = helper;
	},

	Start: function(providerID, snapinGuidVersion) {
	_StartOptimizer(this, providerID, snapinGuidVersion);
	},

	Halt: function() {
		var message = {'JSClassNameKey':'Optimization','JSOperationNameKey':'HaltOptimzer', 'JSArgumentsKey':[], 'JSISSyncMethodKey' : '0'};
    	jsBridge.execute(message);
	},
	
	Schedule: function(providerId, snapinGuid, schedule_datetime, schedule_frequency, schedule_day) {
		_Schedule(providerId, snapinGuid, schedule_datetime, schedule_frequency, schedule_day);
	},

	Disable: function(providerId) {

	},

	OptimizationStart: function(optimizationGuid) {
		this.helper.OptimizationStartCallback(optimizationGuid);
	},

	OptimizationStop: function(optimizationGuid, optimizationListXml) {
		this.helper.OptimizationStopCallback(optimizationGuid, optimizationListXml);
	},

	OptimizerStart: function() {
		this.helper.OptimizerStartCallback();
	},

	OptimizerStop: function() {
		this.helper.OptimizerStopCallback();
		this.scope.OptimizationCompleted();
	},

	GetStatus: function(providerId) {
		var message = {'JSClassNameKey':'Optimization','JSOperationNameKey':'GetStatus', 'JSArgumentsKey':[], 'JSISSyncMethodKey' : '1'};
    	var result = jsBridge.execute(message);
    	var parsedJSONObject = JSON.parse(result);
		return parsedJSONObject.Data;
	}
});

var jsBridge = window.JSBridge;
var optimizerObj = null;

function _StartOptimizer(scope, providerId, snapinGuidVersion) {
	optimizerObj = scope;
	var message = {'JSClassNameKey':'Optimization','JSOperationNameKey':'StartOptimzer', 'JSArgumentsKey':[providerId,snapinGuidVersion], 'JSISSyncMethodKey' : '0'};
    jsBridge.execute(message);
}

function _HaltOptimzer() {
	var message = {'JSClassNameKey':'Optimization','JSOperationNameKey':'HaltOptimzer', 'JSArgumentsKey':[], 'JSISSyncMethodKey' : '0'};
    jsBridge.execute(message);
}

function _OptimizationStart(result) {
	var parsedJSONObject = JSON.parse(result);
	optimizerObj.OptimizationStart(parsedJSONObject.Data);
}

function _OptimizationStop(result) {
	var parsedJSONObject = JSON.parse(result);
	var optimizationGuid = parsedJSONObject.Data.guid;
	var optimizationListXml = parsedJSONObject.Data.optimizationXml;
	optimizerObj.OptimizationStop(optimizationGuid, optimizationListXml);
}

function _OptimizerStart() {
	optimizerObj.OptimizerStart();
}

function _OptimizerStop() {
	optimizerObj.OptimizerStop();
}

function _Schedule(providerId, snapinGuid, schedule_datetime, schedule_frequency, schedule_day) {
	var message = {'JSClassNameKey':'Optimization','JSOperationNameKey':'Schedule', 'JSArgumentsKey':[providerId, snapinGuid, schedule_datetime, schedule_frequency, schedule_day], 'JSISSyncMethodKey' : '0'};
    jsBridge.execute(message);
}
