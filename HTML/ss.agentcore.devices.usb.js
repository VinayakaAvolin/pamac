$ss.agentcore.devices = $ss.agentcore.devices || {};
/** @namespace Holds all ini related functionality*/
$ss.agentcore.devices.usb = $ss.agentcore.devices.usb || {};

(function()
{
  $.extend($ss.agentcore.devices.usb,
  {
    /**
    *  @name DetectUsbPort
    *  @memberOf $ss.agentcore.devices.usb
    *  @function
    *  @description  Detect whether there is a USB port available on the computer
    *  @returns true/false
    *  @example
    *           var objUtil = $ss.agentcore.utils;
    *           var rVal = objUtil.DetectUsbPort();
    */
    DetectUsbPort:function()
    {
      //EE9952: Missing USB support for 98SE fixed by below check.
      var os = _utils.GetOS(); 
      if ( os == "WIN98" || os == "WINME") {
        var UsbCount = new String(_registry.GetRegValue("HKLM", "SYSTEM\\CurrentControlSet\\Services\\Class\\USB\\0000", "NTMPDriver"));
      } else {
        var UsbCount = new String(_registry.GetRegValue("HKLM", "SYSTEM\\CurrentControlSet\\Services\\usbhub\\Enum", "Count"));
      } 
      return (UsbCount == '0') ? false : true;
    },

    /**
    *  @name GetUsbDeviceList
    *  @memberOf $ss.agentcore.utils.system
    *  @function
    *  @description  Get a list of USB devices
    *  @param bPresentOnly If true, only devices that are currently present are considered
    *  @returns DeviceList if any; else empty string
    *  @example
    *           var objUtil = $ss.agentcore.utils;
    *           var rVal = objUtil.GetUsbDeviceList();
    */
    GetUsbDeviceList:function (bPresentOnly)
    {
      var rVal = '';
      try {
        //[MAC] Check Machine OS Windows/MAC
        if(!bMac)
          rVal = _objContainer.GetUsbDeviceList(bPresentOnly);
        else {
          //=======================================================
          //[MAC] Native Call
          var message = {'JSClassNameKey':'Utility','JSOperationNameKey':'GetUsbDeviceList', 'JSArgumentsKey':[bPresentOnly], 'JSISSyncMethodKey' : '1'}
          rVal = jsBridge.execute(message)
          //=======================================================
        }
      } catch(e) {
      }    
      return rVal;
    },

    /**
    *  @name ss_usb_RemoveDevice
    *  @memberOf $ss.agentcore.devices.usb
    *  @function
    *  @description  Remove USB Device
    *  @param sDeviceDesc name of the USB device to be removed
    *  @returns true/false
    *  @example
    *           var objUtil = $ss.agentcore.utils;
    *           var rVal = objUtil.RemoveDevice(deviceName);
    */
    RemoveDevice:function (sDeviceDesc)
    {
      var rVal = false;
      try {
        //[MAC] Check Machine OS Windows/MAC
        if(!bMac)
          rVal = _objContainer.RemoveUsbDevice(sDeviceDesc);
        else {
          //=======================================================
          //[MAC] Native Call
          var message = {'JSClassNameKey':'Utility','JSOperationNameKey':'RemoveUsbDevice', 'JSArgumentsKey':[sDeviceDesc], 'JSISSyncMethodKey' : '1'}
          rVal = jsBridge.execute(message)
          //======================================================= 
        }
      } catch(e) {
      }    
      return rVal;
    }       
  });

  //[MAC] Get Machine OS type
  var bMac = $.browser.safari;

  //[MAC] JSBridge object used only for MAC
  try {
    var jsBridge = window.JSBridge;
  }
  catch(e) {
  }
  
  var _objContainer = $ss.agentcore.utils.activex.GetObjInstance();
  var _config = $ss.agentcore.dal.config;
  var _file = $ss.agentcore.dal.file;
  var _registry = $ss.agentcore.dal.registry;
  var _utils = $ss.agentcore.utils;

})()


//USB driver install constants
//var SS_USB_ERROR_NO_MODEM          = "SS_USB_ERROR_NO_MODEM"; 
//var SS_USB_ERROR_NO_DRIVERS        = "SS_USB_ERROR_NO_DRIVERS";
//var SS_USB_ERROR_REMOVE_DEVICE     = "SS_USB_ERROR_REMOVE_DEVICE";
//var SS_USB_INSTALL_DRIVERS_SUCCESS = "SS_USB_INSTALL_DRIVERS_SUCCESS";
//var SS_USB_INSTALL_DRIVERS_FAILED  = "SS_USB_INSTALL_DRIVERS_FAILED";

//commmenting because of smapi

//    /********************************************************************************
//    ** Name:         ss_usb_GetDriverPath
//    **
//    ** Purpose:      Gets the usb driver path for the selected modem looking at the
//    **               current Operating system value 
//    **
//    ** Parameter:    
//    **
//    ** Return:       path where drivers can be found
//    ********************************************************************************/
//    GetDriverPath:function(oModem) 
//    {
//      var expr = "drivers/item[@os=\""+ ss_con_GetOS().toLowerCase() + "\"]";
//      var driverPath = oModem.GetInstanceValue(expr, SMAPI_EMPTY_STR);
//      if(driverPath == SMAPI_EMPTY_STR) {
//        // try default value to use
//        expr = "drivers/item[@os=\"default\"]";
//        driverPath = oModem.GetInstanceValue(expr, SMAPI_EMPTY_STR);
//      }
//      
//      var relativePath = _config.ParseMacros(driverPath);
//      if (relativePath == SMAPI_EMPTY_STR) return SMAPI_EMPTY_STR;

//      var localPath = "%ROOTPATH%" + relativePath;
//      var serverPath = "%SERVER%" + "/" + relativePath;
//        
//      localPath = _config.ParseMacros(localPath, false);
//      serverPath = _config.ParseMacros(serverPath, false);
//      
//      return _file.FileExists(localPath) ? localPath : serverPath;
//    },


// commenting as it is not being used in any snapin

///*******************************************************************************
//** Name:         ss_usb_InstallDriver
//**
//** Purpose:      Installs a USB Driver
//**
//** Parameter:    sDriverPath  - USB driver path
//**               sUSBName     - USB name
//**
//** Return:       String constant (declared above) with success or failure information.
//*********************************************************************************/
//function ss_usb_InstallDriver(oModem)
//{
//  g_Logger.Info("ss_usb_InstallDriver");
//  var sRet = SS_USB_INSTALL_DRIVERS_FAILED;
//  if(typeof oModem == "undefined" || oModem == null) return SS_USB_ERROR_NO_MODEM;
//  try
//  {
//    var sCompressedDriverPath = ss_usb_GetDriverPath(oModem);
//    sCompressedDriverPath = sCompressedDriverPath.replace(/\\/g, "/");   // normalize slashes
//    
//    var bIsRemoteTarget = sCompressedDriverPath.startsWith("http:") || sCompressedDriverPath.startsWith("https:");
//    var bIsCompressed   = sCompressedDriverPath.endsWith(".zip");
//    var bResult = (sCompressedDriverPath != SMAPI_EMPTY_STR);

//    // if remote target, then download the file locally first
//    if (bIsRemoteTarget)
//    {
//      var nTimeout = 30000;
//      var localTarget = sCompressedDriverPath.replace(/.*\//, ss_con_ExpandSysMacro("%temp%"));      
//      g_Logger.Debug("ss_usb_InstallDriver()", "Downloading From: " + sCompressedDriverPath + "To: " + localTarget);
//      bResult = ss_con_HttpGetFile(localTarget, sCompressedDriverPath, nTimeout * 1000);
//      if (bResult)
//      {
//        sCompressedDriverPath = localTarget;
//        g_Logger.Debug("ss_usb_InstallDriver()", "Downloaded to: " + sCompressedDriverPath);
//      }
//    }    

//    if (bResult)
//    {
//      var localDriverPath = "";

//      // if zip, then extract to temp
//      if (bIsCompressed)            // .ZIP format
//      {
//        localDriverPath = ss_con_ExpandSysMacro("%temp%") + oModem.sCode;
//        bResult = ss_con_Decompress(sCompressedDriverPath, localDriverPath, 3);
//      }
//      
//      if (bResult)
//      {
//        localDriverPath = localDriverPath.replace(/\//g, "\\");
//        var sUSBName    = oModem.GetInstanceValue("usbname");  
//        
//        // Let's first remove the existing device if present so that
//        // when it is replugged it will find our drivers
//        var sUSBAdapters = ss_usb_GetDeviceList(true);
//        g_Logger.Debug("ss_usb_InstallDriver()", "USBAdapters before ss_usb_RemoveDevice: " + sUSBAdapters);
//        if(sUSBAdapters.indexOf(sUSBName) != -1) {
//          if(!ss_usb_RemoveDevice(sUSBName)) return SS_USB_ERROR_REMOVE_DEVICE;
//          sUSBAdapters = ss_usb_GetDeviceList(true);
//          g_Logger.Debug("ss_usb_InstallDriver()", "USBAdapters after ss_usb_RemoveDevice: " + sUSBAdapters);
//        }
//        ss_con_SetRegValue(REG_TREE, ss_cfg_GetRegRoot(), "existingUSBAdapters", sUSBAdapters);    

//        // Copy the usb drivers to the temp path for the OS to find
//        var sTempPath = ss_con_ExpandSysMacro("%temp%usb_drivers\\") + oModem.sCode;
//        g_Logger.Debug("ss_usb_InstallDriver()", "dest: " + sTempPath);
//        g_Logger.Debug("ss_usb_InstallDriver()", "src: " + localDriverPath);
//        ss_con_CopyDir(localDriverPath, sTempPath);
//        
//        // do special handling for westell_6100
//        ss_usb_HandleWin98ME(oModem, sTempPath);
//        
//        // if OS is Vista, preinstall driver package into driver store
//        if (ss_con_IsWinVST(true))
//        {
//          // strip trailing slashes
//          localDriverPath = localDriverPath.replace(/\\$/g, "");
//          
//          var exepath = "\"" + ss_cfg_ParseMacros("%ROOTPATH%%COMMON%\\software\\dpinst\\dpinst.exe") + "\"";
//          exepath += " /s";                                    // silent mode
//          exepath += " /sa";                                   // suppress Add/Remove Program
//          exepath += " /path " + "\"" + localDriverPath + "\"";  // specify driver path          

//          g_Logger.Debug("ss_usb_InstallDriver()", "DPInst Cmd Line: " + exepath);
//          ss_con_RunCommand(exepath, 2);
//        }
//                
//        // Tell the OS where to find the install files
//        ss_con_AddPnPPath(sTempPath);
//        
//        // return success
//        sRet = SS_USB_INSTALL_DRIVERS_SUCCESS;
//      }
//    }
//    else {
//      g_Logger.Error("ss_usb_InstallDriver()", "No driver path found");
//      sRet = SS_USB_ERROR_NO_DRIVERS;
//    }
//  } catch(ex){
//    g_Logger.Error("ss_usb_InstallDriver()", ex.message);
//  }
//  return sRet;
//}



//commenting as it is not used by any snapin

///********************************************************************************
//** Name:         ss_usb_HandleWin98ME
//**
//** Purpose:      Westell has special handling for OS ME and 98 
//**
//** Parameter:    
//**
//** Return:       path where drivers can be found
//********************************************************************************/
//function ss_usb_HandleWin98ME(oModem, sTempPath) 
//{
//  g_Logger.Info("ss_usb_HandleWin98ME");
//  var os = ss_con_GetOS();
//  // special handling for 95/98/ME for westell6100
//  if (os == 'WIN98' || os == 'WINME')  {
//    if (os == 'WIN98') {
//      // bring this out to common
//      var exepath = ss_cfg_ParseMacros("%ROOTPATH%%COMMON%\\software\\win98_usbpatch\\312339USA8.exe") + " /Q";
//      ss_con_RunCommand(exepath, 2);
//    } 

//    // get system inf path
//    var infPath = ss_con_ExpandSysMacro("%windir%")  + "inf";
//    
//    // add system inf path to all the inf files
//    ss_con_ModifyInf(sTempPath, infPath);

//    // sleep to ensure all infs get modified. 
//    // Bug# 8132. On Win 98/ME machines modifying inf file and releasing the file handle takes longer time.
//    ss_con_Sleep(30000);
//    
//    // copy infs from sTempPath to %windir%\inf
//    ss_con_CopyDir(sTempPath, infPath);

//    // delete drvdata.bin and drvidx.bin so drivers db could be rebuilt
//    // when cable is plugged in
//    ss_con_DeleteFile(infPath + "\\drvdata.bin");     
//    ss_con_DeleteFile(infPath + "\\drvidx.bin");
//  }
//}  