/**
 *
 */
$ss.agentcore.model = $ss.agentcore.model ||
{};
(function(){

  $.extend($ss.agentcore.constants, {
  
    /******************************************************************************/
    /*                           Return Value Constants                           */
    /******************************************************************************/
    SL_Success: 1,
    SL_Error: -10,
    SL_CancelNavigation: -20,
    SL_StepListEnd: -30,
    SL_SetpListStart: -40,
    
    SL_SNAPIN_RESULT_ABORTED: 0,
    SL_SNAPIN_RESULT_FAILED: 1,
    SL_SNAPIN_RESULT_SUCCESS: 2,
    SL_SNAPIN_RESULT_SUCCESS_NOCHANGE: 3,
    
    SL_SNAPIN_RESULT_NAME_MAP: {
      0 : "RESULT_ABORTED",
      1 : "RESULT_FAILED",
      2 : "RESULT_SUCCESS",
      3 : "RESULT_SUCCESS_NOCHANGE"
    },
    SL_SNAPIN_RESULT_ID_MAP: {
      ss_snp_Aborted: "RESULT_ABORTED",
      ss_snp_Failed: "RESULT_FAILED",
      ss_snp_Success: "RESULT_SUCCESS",
      ss_snp_SuccessNoChange: "RESULT_SUCCESS_NOCHANGE"
      },
    
    /******************************************************************************/
    /*                           XML Element Names                                */
    /******************************************************************************/
    SL_XMLNODE_ROOT: "navflow",
    SL_XMLNODE_MILESTONES: "milestones",
    SL_XMLNODE_MILESTONE: "milestone",
    SL_XMLNODE_PAGE: "page",
    SL_XMLNODE_STEPLIST: "steplist",
    SL_XMLNODE_STEP: "step",
    SL_XMLNODE_RESULT: "result",
    
    /******************************************************************************/
    /*                           STEP TYPE Constants                              */
    /******************************************************************************/
    SL_STEPTYPE_URL: "url",
    SL_STEPTYPE_FUNCTION: "function",
    SL_STEPTYPE_STEPLIST: "steplist",
    SL_STEPTYPE_SETPARAM: "setparam",
    SL_STEPTYPE_GETPARAM: "getparam",
    SL_STEPTYPE_PLACEHOLDER: "placeholder",
    
    
    SL_STEPTYPE_SETREG: "setreg",
    SL_STEPTYPE_GETREG: "getreg",
    SL_STEPTYPE_SNAPIN: "snapin",
    SL_STEPTYPE_SNAPINCOMPLETE: "snapincomplete",
    SL_STEPTYPE_EXIT: "exit",
    
    /******************************************************************************/
    /*                        STEP TYPE TARGET Constants                          */
    /******************************************************************************/
    SL_STEPTYPETARGET_URL: "page",
    SL_STEPTYPETARGET_FUNCTION: "name",
    SL_STEPTYPETARGET_PLACEHOLDER: "name",
    SL_STEPTYPETARGET_STEPLIST: "targetid",
    SL_STEPTYPETARGET_STEP: "targetid",
    SL_STEPTYPETARGET_SETPARAM: "key",
    SL_STEPTYPETARGET_GETPARAM: "key",
    SL_STEPTYPETARGET_SNAPIN: "name",
    
    /******************************************************************************/
    /*                           RESULT TYPE Constants                            */
    /******************************************************************************/
    SL_RESULTTYPE_STEPLIST: "steplist",
    SL_RESULTTYPE_STEP: "step",
    
    /******************************************************************************/
    /*                           XML Attribute Names                              */
    /******************************************************************************/
    SL_XMLATTR_ID: "id",
    SL_XMLATTR_TARGETID: "targetid",
    SL_XMLATTR_PAGE: "page",
    SL_XMLATTR_NAME: "name",
    SL_XMLATTR_PATH: "path",
    SL_XMLATTR_TYPE: "type",
    SL_XMLATTR_KEY: "key",
    SL_XMLATTR_VALUE: "value",
    SL_XMLATTR_CLASS: "class",
    SL_XMLATTR_CONST: "const",
    SL_XMLATTR_CLEANUP: "cleanup",
    SL_XMLATTR_PROGRESS: "progress",
    SL_XMLATTR_IDENTITY: "identity",
    SL_XMLATTR_INNERSTEP: "innerstep",
    SL_XMLATTR_LASTRESULT: "lastresult",
    SL_XMLATTR_SKIPONBACK: "skiponback",
    SL_XMLATTR_MILESTONEID: "milestoneid",
    SL_XMLATTR_STICKY: "stickyresult",
    SL_XMLATTR_STARTPROG: "startprogress",
    SL_XMLATTR_DEFAULTRESULT: "defaultresult",
    SL_XMLATTR_ACTUAL_PROG: "actualprogress",
    SL_XMLATTR_PARAM1: "param1",
    SL_XMLATTR_PARAM2: "param2",
    SL_XMLATTR_PARAM3: "param3",
    SL_XMLATTR_PARAM4: "param4",
    
    /******************************************************************************/
    /*                   Misfit Constants                                         */
    /******************************************************************************/
    SL_XMLATTR_VAL_TRUE: "true",
    SL_XMLATTR_VAL_FALSE: "false",
    SL_XMLCHAR_ROOT: "//",
    SL_XMLCHAR_SLASH: "/",
    SL_XMLCHAR_DOTSLASH: "./",
    SL_STEPAPI_STARTLIST: "start",
    SL_STEPTYPE_ReverselyNavigable: {
      url: true,
      snapin: true
    }
  });
  var _const = $ss.agentcore.constants;
  $.extend($ss.agentcore.constants, {
  
    /******************************************************************************/
    /*                   Derived XPATH Statements                                 */
    /******************************************************************************/
    SL_XPATH_ROOT: _const.SL_XMLCHAR_ROOT + _const.SL_XMLNODE_ROOT,
    SL_XPATH_ROOT_STEPLISTS: _const.SL_XMLCHAR_ROOT + _const.SL_XMLNODE_ROOT + _const.SL_XMLCHAR_SLASH + _const.SL_XMLNODE_STEPLIST,
    SL_XPATH_NEXT_STEP: _const.SL_XMLCHAR_DOTSLASH + _const.SL_XMLNODE_STEP,
    SL_XPATH_STEP_LIST: _const.SL_XMLCHAR_DOTSLASH + _const.SL_XMLNODE_STEP,
    SL_XPATH_NEXT_RESULT: _const.SL_XMLCHAR_DOTSLASH + _const.SL_XMLNODE_RESULT,
    SL_XPATH_RESULT_LIST: _const.SL_XMLCHAR_DOTSLASH + _const.SL_XMLNODE_RESULT,
    SL_XPATH_MILESTONE_LIST: _const.SL_XMLCHAR_ROOT + _const.SL_XMLNODE_ROOT + _const.SL_XMLCHAR_SLASH + _const.SL_XMLNODE_MILESTONES + _const.SL_XMLCHAR_SLASH + _const.SL_XMLNODE_MILESTONE,
    
    //
    // Registered and Implemented in ss_stepapi_client_bridge.js
    //
    //slc_TYPE_HANDLER_MAP[slc_STEPTYPE_GETREG]         = ss_slcl_StepType_GetReg;
    //slc_TYPE_HANDLER_MAP[slc_STEPTYPE_SETREG]         = ss_slcl_StepType_SetReg;
    //slc_TYPE_HANDLER_MAP[slc_STEPTYPE_SNAPIN]         = ss_slcl_StepType_Snapin;
    //slc_TYPE_HANDLER_MAP[slc_STEPTYPE_SNAPINCOMPLETE] = ss_slcl_StepType_SnapinComplete;
    
    /******************************************************************************/
    /*                   Map Step Type Targets                                    */
    /******************************************************************************/
    SL_TYPE_TARGET_MAP: {
      "function": _const.SL_STEPTYPETARGET_FUNCTION,
      "placeholder": _const.SL_STEPTYPETARGET_PLACEHOLDER,
      "url": _const.SL_STEPTYPETARGET_URL,
      "url": _const.SL_STEPTYPETARGET_URL,
      "steplist": _const.SL_STEPTYPETARGET_STEPLIST,
      "step": _const.SL_STEPTYPETARGET_STEP,
      "setparam": _const.SL_STEPTYPETARGET_SETPARAM,
      "getparam": _const.SL_STEPTYPETARGET_GETPARAM,
      "snapin": _const.SL_STEPTYPETARGET_SNAPIN
    }
  
  
  });
})();


$ss.agentcore.model.StepListSnapinModel = MVC.Class.extend({
  init: function(sSnapinName, sFlowFile, sSessionId, oSessionData, sNextButtId, sPrevButtId){
    //TODO Logger information to be added
    
    /******************************************************************************/
    /*                     States of the Steplist Loaded						  */
    /******************************************************************************/
    this._FlowFileName = sFlowFile;
    this._nextButtonId = sNextButtId;
    this._prevButtonId = sPrevButtId;
    this._SnapinName = sSnapinName;
    this._NavXmlDom = null;
    this._CurrentStepListDom = null;
    this._CurrentStepNode = null;
    this._CurrentProgressTotal = 0;
    this._CurrentMilestoneId = -1;
    this._CurrentMilestoneName = "";
    this._CurrentIdentityId = "";
    this._EventHandlerStack = [];
    this._setTimeoutDelay = 10;
    this._Debugger = null;
    this._pvt_Busy = false;
    this._pvt_BusyEndCallback = null;
    this._pvt_BusyStartCallback = null;
    this._LastDataValue = {};
    this._oHistory = {};
//    this._steplistSession = {};
//    this._lastSteplistSession = {};
//    this._currentSessionId = "TEMPSESSIONID";
    
    this._logger = $ss.agentcore.log.GetDefaultLogger("$ss.agentcore.model.StepListSnapinModel." + sSnapinName);
//    if (oSessionData) {
//      this._steplistSession = oSessionData;
//    };
//    if (sSessionId) {
//      this._currentSessionId = sSessionId;
//    }
    this.GetStepListDOM();
  },
  
  GetStepListDOM: function(){
    this._logger.info("Entered The Function GetStepListDOM");
    var async = false;
    if (this._NavXmlDom) {
      return this._NavXmlDom
    }
    if (this._FlowFileName) {
      try {
        this._NavXmlDom = $ss.agentcore.dal.xml.LoadXML(this._FlowFileName, async);
      } 
      catch (e) {
        this._logger.error("Exception occured in %1% - \n Code: %2%,Name: %3% \n  Description: %4% \n", "GetStepListDOM", e.number, e.name, e.message);
      }
    }
    return this._NavXmlDom;
  },
  LoadStepList: function(sStepListID, bInitialize){
  
    this._logger.info("Entered The Function LoadStepList");
    
    var sCurrentStepListID = null;
    var _xml = $ss.agentcore.dal.xml;
    var _const = $ss.agentcore.constants;
    var retObj = {};
    sStepListID = sStepListID || _const.SL_STEPAPI_STARTLIST;
    
//    if (aSessionData) {
//      this._steplistSession = aSessionData
//    }
//    if (sSessionId) {
//      this._currentSessionId = sSessionId
//    }
    if (this._CurrentStepListDom != null) 
      sCurrentStepListID = _xml.GetAttribute(this._CurrentStepListDom, _const.SL_XMLATTR_ID);
    
    if (sStepListID !== sCurrentStepListID) {
      var xNewNode;
      xNewNode = _xml.GetNode(_const.SL_XPATH_ROOT_STEPLISTS, "[@id='" + sStepListID + "']", this._NavXmlDom);
      if (xNewNode == null) {
        this._logger.error("LoadStepList: No Node was found for steplistid " + sStepListID);
        retObj.status = _const.SL_Error;
        return retObj;
      }
      
      this._CurrentStepListDom = xNewNode;
      
      /* TODO - Why is it needed
       if (bForBranching) {
       slProcessStartProgressForNode(slg_CurrentStepListDom);
       slProcessMilstonesForNode ( slg_CurrentStepListDom );
       }
       */
      this.LoadStepByID(""); // load the initial step in this steplist.
    }
    if(bInitialize) this.LoadStepByID(""); // load the initial step in this steplist.
    
    retObj.status = _const.SL_Success;
    return retObj;
  },
  
  LoadStep: function(sNameXPath){
  
    this._logger.info("Entered The Function LoadStep with input %1%", sNameXPath);
    var _xml = $ss.agentcore.dal.xml;
    var _const = $ss.agentcore.constants;
    
    var retObj = {};
    if (this._CurrentStepListDom == null) {
      retObj.status = _const.SL_Error;
      return retObj;
    }

    sNameXPath = sNameXPath || "";
    
    var xNewNode = _xml.GetNode(_const.SL_XPATH_NEXT_STEP, sNameXPath, this._CurrentStepListDom);
    if (xNewNode == null) {
      this._logger.error("LoadStep: Could Not Load The Step %1%", sNameXPath);
      retObj.status = _const.SL_Error;
      return retObj;
    }
    this._CurrentStepNode = xNewNode;
    retObj.status = _const.SL_Success;
    return retObj;
    
  },
  LoadStepByID: function(sStepID){
    this._logger.info("Entered The Function LoadStepByID with input %1%", sStepID);
    if (sStepID) 
      sStepID = "[@id='" + sStepID + "']";
    return this.LoadStep(sStepID);
  },
  GetCurrentStepID: function(){
    this._logger.info("Entered The Function GetCurrentStepID");
    return this.GetIDForNode(this._CurrentStepNode);
  },
  GetIDForNode: function(oNode){
    this._logger.info("Entered The Function GetIDForNode");
    var _xml = $ss.agentcore.dal.xml;
    var _const = $ss.agentcore.constants;
    return this.XmlAttributeGetEvaluated(oNode, _const.SL_XMLATTR_ID);
  },
  XmlAttributeGetEvaluated: function(oNode, sAttrName){
    this._logger.info("Entered The Function XmlAttributeGetEvaluated: %1% ", sAttrName);
    var _xml = $ss.agentcore.dal.xml;
    var _const = $ss.agentcore.constants;
    var sAttrValue = _xml.GetAttribute(oNode, sAttrName);
    var xConst = _xml.GetAttribute(oNode, _const.SL_XMLATTR_CONST);
    if (xConst == _const.SL_XMLATTR_VAL_TRUE) {
      if (_const[sAttrValue] != undefined) {
        sAttrValue = _const[sAttrValue];
      }
    }
    return sAttrValue;
  },
  GetStepTypeTarget: function(sStepType){
    this._logger.info("Entered The Function GetStepTypeTarget: %1% ", sStepType);
    var _const = $ss.agentcore.constants;
    if (typeof(_const.SL_TYPE_TARGET_MAP[sStepType]) == "undefined" || _const.SL_TYPE_TARGET_MAP[sStepType] == "") {
      this._logger.error("Step type target not found for Step Type:  %1% ", sStepType);
      return {
        status: _const.SL_Error
      };
    }
    else {
      return _const.SL_TYPE_TARGET_MAP[sStepType];
    }
  },
  
  GetCurrentStepTypeTarget: function(sStepType){
    this._logger.info("Entered Function GetCurrentStepTypeTarget: %1% ", sStepType);
    var _xml = $ss.agentcore.dal.xml;
    var _const = $ss.agentcore.constants;
    
    if (typeof(sStepType) == "undefined" || sStepType == "") {
      sStepType = _xml.GetAttribute(this._CurrentStepNode, _const.SL_XMLATTR_TYPE);
    }
    
    if (this.GetStepTypeTarget(sStepType).status == _const.SL_Error) {
      this._logger.error("Step type target not found for Step Type:  %1% ", sStepType);
      return {
        status: _const.SL_Error
      };
    }
    else {
      return this.XmlAttributeGetEvaluated(this._CurrentStepNode, _const.SL_TYPE_TARGET_MAP[sStepType]);
    }
  },
  GetNameForNode: function(oNode){
    this._logger.info("Entered Function GetNameForNode");
    var _const = $ss.agentcore.constants;
    return this.XmlAttributeGetEvaluated(oNode, _const.SL_XMLATTR_NAME);
  },
  
  GetCurrentStepPath: function(){
    this._logger.info("Entered Function GetCurrentStepPath");
    var _xml = $ss.agentcore.dal.xml;
    var _const = $ss.agentcore.constants;
    var sPath = _xml.GetAttribute(this._CurrentStepNode, _const.SL_XMLATTR_PATH);
    if (sPath == undefined || sPath == null) {
      sPath = "";
    }
    return sPath;
  },
  
  CheckListStart: function(){
    /* //TODO
     if (! slStepHaveBackward())
     {
     var oEvent = new Object();
     oEvent.name = slc_UPDATE_LIST_START;
     slSendMsg(oEvent);
     }
     */
  },
  
  CheckListEnd: function(){
    if (this.StepHaveForward()) {
      return false;
    }
    return true;
  },
  RunStep: function(){
    //TODO log slInfo("slRunStep");
    //slBusyStart();
    var _xml = $ss.agentcore.dal.xml;
    var _const = $ss.agentcore.constants;
    if (this._LastError != null) {
      //slError("slRunStep", "Cached Error Found " + slg_LastError.message);
      //slBusyEnd();
      //return slThrow(slg_LastError);
    }
    
    if (this._CurrentStepNode != null) {
      //slCheckListStart();
      
      //slProcessProgressForNode  ( slg_CurrentStepNode );
      //slProcessMilstonesForNode ( slg_CurrentStepNode );
      //slProcessIdentityForNode  ( slg_CurrentStepNode );
      
      var fnStepTypeHandler, sStepType, fnHandler;
      
      var sStepID = _xml.GetAttribute(this._CurrentStepNode, _const.SL_XMLATTR_ID);
      //slDebug("slRunStep", "sStepID : " + sStepID);
      
      sStepType = _xml.GetAttribute(this._CurrentStepNode, _const.SL_XMLATTR_TYPE);
      if (this.GetTypeHandler(sStepType) != undefined &&
      this.GetTypeHandler(sStepType) != null) {
        fnHandler = this.GetTypeHandler(sStepType);
        var fnHandlerResult = fnHandler();
        if (fnHandlerResult == null) {
          fnHandlerResult = {};
          fnHandlerResult.status = _const.SL_Error;
        };
        if (fnHandlerResult.status != _const.SL_CancelNavigation && fnHandlerResult.status != _const.SL_Error) {
          if (this.CheckListEnd()) {
            fnHandlerResult.stepListFlowStatus = _const.SL_StepListEnd;
            return fnHandlerResult;
          };
                  }
        //slDebug("slRunStep", "fnHandlerResult " + fnHandlerResult);
        return fnHandlerResult;
      }
      else {
        //slError("slRunStep", "Unhandled Step Type " + sStepType);
        //return slStepForward();
      }
    }
    //slBusyEnd();
    return {
      status: _const.SL_Error
    };
  },
  RunStepFunction: function(){
  
    //TODO Log slInfo("slRunStepFunction");
    var _xml = $ss.agentcore.dal.xml;
    var _const = $ss.agentcore.constants;
    var retObj = {};
    
    var fnName = this.GetCurrentStepTypeTarget(_const.SL_STEPTYPE_FUNCTION);
    var retVal = {}, PivotFunction, sNextStepName, sNextStepListName;
    
    //slDebug("slRunStepFunction", "Function Name " + fnName);
    
    if ( $ss.agentcore.utils.GetHandler(fnName) != undefined) {
    
      PivotFunction = $ss.agentcore.utils.GetHandler(fnName);
      
      var bSticky = _xml.GetAttribute(this._CurrentStepNode, _const.SL_XMLATTR_STICKY) === _const.SL_XMLATTR_VAL_TRUE;
      //var xLastVal = ss_db_DataGet(slc_XMLATTR_LASTRESULT+"_"+fnName);
      var xLastVal = this._LastDataValue[_const.SL_XMLATTR_LASTRESULT + "_" + fnName];
      var bStickyValExists = xLastVal != null && xLastVal != undefined;
      
      if (bSticky == true && bStickyValExists) {
        //retVal = ss_db_DataGet(slc_XMLATTR_LASTRESULT+"_"+fnName);
        retVal = this._LastDataValue[_const.SL_XMLATTR_LASTRESULT + "_" + fnName]
      }
      else {
        try {
        
          var xParam1 = this.XmlAttributeGetEvaluated(this._CurrentStepNode, _const.SL_XMLATTR_PARAM1);
          var xParam2 = this.XmlAttributeGetEvaluated(this._CurrentStepNode, _const.SL_XMLATTR_PARAM2);
          var xParam3 = this.XmlAttributeGetEvaluated(this._CurrentStepNode, _const.SL_XMLATTR_PARAM3);
          var xParam4 = this.XmlAttributeGetEvaluated(this._CurrentStepNode, _const.SL_XMLATTR_PARAM4);
          
          retVal = new String(PivotFunction(xParam1, xParam2, xParam3, xParam4));
          
          if (bSticky) {
            //ss_db_DataSet(slc_XMLATTR_LASTRESULT+"_"+fnName, retVal);
            this._LastDataValue[_const.SL_XMLATTR_LASTRESULT + "_" + fnName] = retVal;
          }
          
          //slDebug("slRunStepFunction PivotFunction() retVal", retVal);
        
        } 
        catch (e) {
          // TBD : Support an "errorpage=true" attribute?
          //var slg_ExceptionStepListPivotFunctionError = new Error(-4, "Step List Pivot Function ("+fnName+") threw an unhandled error "+e.description );
          //slError("slRunStepFunction", "slg_ExceptionStepListPivotFunctionError " + slg_ExceptionStepListPivotFunctionError.message);
          //slBusyEnd();
          //return slThrow(slg_ExceptionStepListPivotFunctionError);
          return {
            status: _const.SL_Error
          };
        }
      }
    } else {
      return {
          status: _const.SL_Error,
          errorMsg: "Function Not Found"
      };
    }
    if (retVal.status === _const.SL_CancelNavigation) {
      // Stop Processing.
      //slWarning("slRunStepFunction", "Pivot Function Canceled Navigation");
      //slBusyEnd();
      
      return {
        status: _const.SL_CancelNavigation
      };
    }
    else 
      if (retVal.status === _const.SL_Error) {
        //slWarning("slRunStepFunction", "Pivot Function Errored");
        //slBusyEnd();
        return {
          status: _const.SL_Error
        };
      }
    //return retVal;
    return this.ProcessResultSet(retVal);
  },
  
  RunStepURL: function(){
    //TODO log slDebug("slRunStepURL");
    var _const = $ss.agentcore.constants;
    var retObj = new Object();
    retObj.stepid = this.GetCurrentStepID();
    retObj.type = _const.SL_STEPTYPE_URL;
    retObj.file = this.GetCurrentStepTypeTarget(_const.SL_STEPTYPE_URL);
    retObj.status = _const.SL_Success;
    retObj.hasForward = this.StepHaveForward(); 
    retObj.stepDetail = this.GetStepDetails(
                            retObj.hasForward,
                            this.GetMilstone(),
                            this.GetIdentityForNode()
                          );
   
   retObj.stepDetail.target = {};
   retObj.stepDetail.target.type = retObj.type;
   retObj.stepDetail.target.file = retObj.file;

//    retObj.hasForward = this.StepHaveForward();
    
//    this.UpdateSessionInfo(retObj.hasForward, retObj.milestone, retObj.identity);
//    retObj.session = this._steplistSession;
//    retObj.sessionId = this._currentSessionId;
    //slHistoryMarker();
    //var sResponse = slSendMsg(UrlMsg);
    return retObj;
  },
  
  RunStepSnapin: function(){
    //TODO log slDebug("slRunStepURL");
    var _const = $ss.agentcore.constants;
    var retObj = new Object();
    retObj.type = _const.SL_STEPTYPE_SNAPIN;
    retObj.stepid = this.GetCurrentStepID();
    retObj.snapin = this.GetCurrentStepTypeTarget(_const.SL_STEPTYPE_SNAPIN);
    retObj.status = _const.SL_Success;
    retObj.hasForward = this.StepHaveForward(); 
    retObj.stepDetail = this.GetStepDetails(
                            retObj.hasForward,
                            this.GetMilstone(),
                            this.GetIdentityForNode()
   
                          );

    retObj.stepDetail.target = {};
    retObj.stepDetail.target.type = retObj.type;
    retObj.stepDetail.target.snapin = retObj.snapin;
    return retObj;
  },
  RunSnapinComplete: function() {
    var _const = $ss.agentcore.constants;
    var retObj = new Object();
    retObj.type = _const.SL_STEPTYPE_SNAPINCOMPLETE;
    retObj.stepid = this.GetCurrentStepID();
    var sSnapinCompleter = this.GetCurrentStepTypeTarget(_const.SL_STEPTYPE_SNAPIN);
    if(! sSnapinCompleter || _const.SL_SNAPIN_RESULT_ID_MAP[sSnapinCompleter] == undefined) {
      sSnapinCompleter = "ss_snp_SuccessNoChange";
    }
    retObj.isComplete = true;
    retObj.completeStatus = _const.SL_SNAPIN_RESULT_ID_MAP[sSnapinCompleter];
    retObj.status = _const.SL_Success;
    retObj.hasForward = false;
    retObj.stepDetail = this.GetStepDetails(
                            false,
                            this.GetMilstone(),
                            this.GetIdentityForNode()
   
                          );
    return retObj;
    
  },
  GetTypeHandler: function(sType){
    var self = this;
    var SL_TYPE_HANDLER_MAP = {
      "function": self.RunStepFunction,
      "url": self.RunStepURL,
      "steplist": self.RunStepList,
      "setparam": self.RunStepSetParam,
      "getparam": self.RunStepGetParam,
      "placeholder": self.RunStepPlaceholder,
      "snapin": self.RunStepSnapin,
      "snapincomplete": self.RunSnapinComplete
    };
    var tFunction;
    if (SL_TYPE_HANDLER_MAP[sType]) {
      tFunction = SL_TYPE_HANDLER_MAP[sType];
      return function(){
        return tFunction.apply(self, arguments);
      };
    };
      },
  
  RunStepList: function(){
    //TODO slInfo("slRunStepList");
    var _xml = $ss.agentcore.dal.xml;
    var _const = $ss.agentcore.constants;
    
    var sID = this.GetCurrentStepTypeTarget(_const.SL_STEPTYPE_STEPLIST);
    var oInnerStepListNodeCache = this._CurrentStepNode;
    var sInnerStep = _xml.GetAttribute(this._CurrentStepNode, _const.SL_XMLATTR_INNERSTEP);
    if (this.LoadStepList(sID).status == _const.SL_Success) {
      if (sInnerStep != "" && sInnerStep != null) {
        return this.ProcessInnerStepForNode(oInnerStepListNodeCache);
      }
      else {
        //setTimeout(slRunStep, this._setTimeoutDelay);
        return this.RunStep();
      }
    }
    var ExceptionStepListStepNotFound = new Error(-7, "StepList Step not found by ID: " + sID);
    return ExceptionStepListStepNotFound; //TODO slThrow(this._ExceptionStepListStepNotFound);
  },
  
  RunStepSetParam: function(){
    //TODO slInfo("slRunStepSetParam");
    var _xml = $ss.agentcore.dal.xml;
    var _const = $ss.agentcore.constants;
    
    try {
      var sTarget = this.GetStepTypeTarget(_const.SL_STEPTYPE_SETPARAM);
      var sKey = this.XmlAttributeGetEvaluated(this._CurrentStepNode, sTarget);
      if (sKey == null || sKey == "") 
        sKey = this.GetCurrentStepID();
      var sValue = this.XmlAttributeGetEvaluated(this._CurrentStepNode, _const.SL_XMLATTR_VALUE);
      var xValue = _xml.CastString(sValue);
      var sParamValue = $ss.agentcore.dal.databag.SetValue(sKey, xValue, _const.HST_TYPE_NONE);
      //setTimeout(slStepForward, this._setTimeoutDelay);
      return this.StepForward();
    } 
    catch (e) {
      //LOG THE ERROR
      //slError("slRunStepSetParam", "Unexpected Error " + this._LastError.message);
      //slBusyEnd();
      //return slThrow(e);
    }
    //slBusyEnd();
    return {
      status: _const.SL_Error
    };
  },
  
  RunStepGetParam: function(){
    //slInfo("slRunStepGetParam");
    var _const = $ss.agentcore.constants;
    try {
      var sTarget = this.GetStepTypeTarget(_const.SL_STEPTYPE_GETPARAM);
      var sKey = this.XmlAttributeGetEvaluated(this._CurrentStepNode, sTarget);
      if (sKey == null || sKey == "") 
        sKey = this.GetCurrentStepID();
      var sDbValue = $ss.agentcore.dal.databag.GetValue(sKey);
      return this.ProcessResultSet(sDbValue);
    } 
    catch (e) {
      //TODO log
    }
    return {
      status: _const.SL_Error
    };
  },
  
  RunStepPlaceholder: function(){
    //slInfo("slRunStepPlaceholder");
    //setTimeout(slStepForward, this._setTimeoutDelay);
    return this.StepForward();
  },
  
  ProcessStartProgressForNode: function(oNode){
    //slInfo("slProcessStartProgressForNode");
    try {
      var startprogress = oNode.getAttribute(_const.SL_XMLATTR_STARTPROG);
      if (startprogress != "" && startprogress != null) {
        var iStartProgress = parseInt(startprogress);
        if (this._CurrentProgressTotal < iStartProgress) {
          var iDiff = iStartProgress - this._CurrentProgressTotal;
          this._CurrentProgressTotal = iStartProgress;
          
          //slDebug("slProcessStartProgressForNode", "New Progress " + this._CurrentProgressTotal);
          
          var oLastStep = slHistory_GetLast();
          if (oLastStep) {
            //
            // Update the previous history record.
            //
            var iCurrentProgPart = ss_xml_NodeGetChildElementTextCasted(oLastStep, _const.SL_HISTORY_PROGPART);
            iCurrentProgPart += iDiff;
            
            ss_xml_NodeSetChildElementText(oLastStep, _const.SL_HISTORY_PROGTOTAL, this._CurrentProgressTotal);
            ss_xml_NodeSetChildElementText(oLastStep, _const.SL_HISTORY_PROGPART, iCurrentProgPart);
          }
        }
      }
      return {
        status: _const.SL_Success
      };
    } 
    catch (e) {
      slError("slProcessStartProgressForNode", "Unexpected Error " + e.message);
    }
    return {
      status: _const.SL_Error
    };
  },
  
  ProcessProgressForNode: function(oNode){
    slInfo("slProcessProgressForNode");
    try {
      // update progress bar with current progress
      var oEvent = new Object();
      oEvent.name = _const.SL_UPDATE_PROGRESS;
      oEvent.position = this._CurrentProgressTotal;
      
      slSendMsg(oEvent);
      
      // now add progress of current step.. but don't send progress update message.. this
      // happens after the user goes to the next step.
      var progress = oNode.getAttribute(_const.SL_XMLATTR_PROGRESS);
      if ((progress != "" && progress != null)) {
        if (oNode != this._CurrentStepListDom) {
          // we're on a step node, update with its progress value
          // We take the percentage assigned to the steplist, and then take the step's
          // progress as a fraction of the steplist's progress.
          var steplistProgress = "100";
          if (this._CurrentStepListDom != null) {
            var sNodeProgress = this._CurrentStepListDom.getAttribute(_const.SL_XMLATTR_PROGRESS);
            if (sNodeProgress != "" && sNodeProgress != null) 
              steplistProgress = parseInt(sNodeProgress);
          }
          
          if (progress != "" && progress != null) {
            var iProgress = parseInt(progress * 1);
            if (!isNaN(iProgress)) {
              var iSteplistProgress = parseInt(steplistProgress);
              if (!isNaN(iSteplistProgress)) {
                var nProgressPortion = iProgress / 100;
                var nSteplistProgressPortion = iSteplistProgress * nProgressPortion;
                var iIncreasedPortion = parseInt(Math.ceil(nSteplistProgressPortion));
                this._CurrentProgressTotal = this._CurrentProgressTotal + iIncreasedPortion;
                if (this._CurrentProgressTotal > 100) 
                  this._CurrentProgressTotal = 100;
                
                slDebug("slProcessProgressForNode", "New Progress " + this._CurrentProgressTotal);
              }
            }
          }
        }
      }
      return _const.SL_Success;
    } 
    catch (e) {
      slError("slProcessProgressForNode", "Unexpected Error " + e.message);
    }
    return _const.SL_Error;
  },
  
  
  ProcessMilstonesForNode: function(oNode){
    //slInfo("slProcessMilstonesForNode");
    var sMileStone = "";
    var milestoneNode = "";
    var milestoneid = ""
    var _const = $ss.agentcore.constants;
    var _xml = $ss.agentcore.dal.xml;
    
    try {
      sMileStone = oNode.getAttribute(_const.SL_XMLATTR_MILESTONEID);
      if (sMileStone != "" && sMileStone != null) {
        milestoneNode = _xml.GetNode(_const.SL_XPATH_MILESTONE_LIST, "[@id='" + sMileStone + "']", this._NavXmlDom);
        milestoneid = _xml.GetNodeText(milestoneNode, "", this._NavXmlDom);
        
        // update current milestone global
        this._CurrentMilestoneId = milestoneid;
        
        //slDebug("slProcessMilstonesForNode", "Milestone Reached " + this._CurrentMilestoneId);
        //slSendMsg(oEvent);
      }
      return {
        status: _const.SL_Success
      };
    } 
    catch (e) {
      //slError("slProcessMilstonesForNode", "Unexpected Error " + e.message);
    }
    return {
      status: _const.SL_Error
    };
  },
  
  GetIdentityForNode: function(){
    var sIdentity = "";
    var _const = $ss.agentcore.constants;
    try {
      sIdentity = this._CurrentStepNode.getAttribute(_const.SL_XMLATTR_IDENTITY);
      if (sIdentity == null) {
        sIdentity = "";
      }
    } 
    catch (e) {
    }
    return sIdentity;
  },
  
  
  GetMilstone: function(){
    //slInfo("slProcessMilstonesForNode");
    var sMileStone = "";
    var milestoneNode = "";
    var milestoneid = ""
    var _const = $ss.agentcore.constants;
    var _xml = $ss.agentcore.dal.xml;
    try {
      sMileStone = oNode.getAttribute(_const.SL_XMLATTR_MILESTONEID);
      if (sMileStone != "" && sMileStone != null) {
        milestoneNode = _xml.GetNode(_const.SL_XPATH_MILESTONE_LIST, "[@id='" + sMileStone + "']", this._NavXmlDom);
        milestoneid = _xml.GetNodeText(milestoneNode, "", this._NavXmlDom);
      }
    } 
    catch (e) {
      //slError("slProcessMilstonesForNode", "Unexpected Error " + e.message);
    }
    this._CurrentMilestoneId = milestoneid;
    return milestoneid;
  },
  ProcessInnerStepForNode: function(oNode){
    //slInfo("slProcessInnerStepForNode");
    //TODO Log
    var _const = $ss.agentcore.constants;
    try {
      var sID = oNode.getAttribute(_const.SL_XMLATTR_TARGETID);
      var innerstep = oNode.getAttribute(_const.SL_XMLATTR_INNERSTEP);
      if (innerstep != "" && innerstep != null) {
        if (this.LoadStepByID(innerstep).status == _const.SL_Success) {
          //setTimeout(slRunStep, this._setTimeoutDelay);
          return this.RunStep();
        }
        else {
          var ExceptionInnerStepNotFound = new Error(-8, "StepList " + sID + " InnerStep " + innerstep + " not found by ID ");
          //TODO Log it
          //return slThrow(this._ExceptionInnerStepNotFound);
        }
      }
    } 
    catch (e) {
      // TODO LOG ERROR slError("slProcessInnerStepForNode", "Unexpected Error " + e.message);
      //return slThrow(e);
    }
    return {
      status: _const.SL_Error
    };
  },
  
  ProcessResultSet: function(xResultValue){
    //TODO log slInfo("slProcessResultSet");
    var _xml = $ss.agentcore.dal.xml;
    var _const = $ss.agentcore.constants;
    
    var xResultNode, xDefaultResult, xSiblingNode;
    var sStepType = _xml.GetAttribute(this._CurrentStepNode, _const.SL_XMLATTR_TYPE);
    var fnName = this.GetCurrentStepTypeTarget(sStepType);
    
    xResultNode = _xml.GetNode(_const.SL_XPATH_NEXT_RESULT, "[@value='" + xResultValue + "']", this._CurrentStepNode);
    if (xResultNode == null) {
      //
      // Check for a node with @defaultresult='true'
      //
      //slWarning("slProcessResultSet", "Specific result not found for " + xResultValue);
      xDefaultResult = _xml.GetNode(_const.SL_XPATH_NEXT_RESULT, "[@" + _const.SL_XMLATTR_DEFAULTRESULT + "='" + _const.SL_XMLATTR_VAL_TRUE + "']", this._CurrentStepNode);
      
      if (xDefaultResult == null) {
        //
        // No results matched, move to the next Sibling
        //
        //slWarning("slProcessResultSet", "Default result not found for " + fnName.toString());
        xSiblingNode = this.GetNextSibling(this._CurrentStepNode, _const.SL_XMLNODE_STEP);
        
        if (xSiblingNode == null) {
          var _ExceptionStepListReturnNotFound = new Error(-5, "StepList return value (" + xResultValue + ") not found in result set");
          //slError("slProcessResultSet", "Function has no determinable path: " + this._ExceptionStepListReturnNotFound.message);
          //slThrow(this._ExceptionStepListReturnNotFound);
          return {
            status: _const.SL_Error
          };
        }
        else {
          //
          // Use the sibling
          //
          this._CurrentStepNode = xSiblingNode;
          //setTimeout(slRunStep, this._setTimeoutDelay);
          return this.RunStep();
          //return _const.SL_Success;
        }
      }
      else {
        xResultNode = xDefaultResult;
      }
    }
    
    var bSticky = _xml.GetAttribute(this._CurrentStepNode, _const.SL_XMLATTR_STICKY);
    
    if (bSticky == _const.SL_XMLATTR_VAL_TRUE) 
      this._LastDataValue[_const.SL_XMLATTR_LASTRESULT + "_" + fnName] = xResultNode.getAttribute(_const.SL_XMLATTR_VALUE);
    //ss_db_DataSet(_const.SL_XMLATTR_LASTRESULT+"_"+fnName, xResultNode.getAttribute(_const.SL_XMLATTR_VALUE));
    
    var sTargetStepOrStepListID = "";
    
    //A result should always have a targetid to determine where to go.
    var xResultNodeName = this.XmlAttributeGetEvaluated(xResultNode, _const.SL_XMLATTR_TARGETID);
    if (xResultNodeName != "" && xResultNodeName != null) {
      sTargetStepOrStepListID = this.XmlAttributeGetEvaluated(xResultNode, _const.SL_XMLATTR_TARGETID);
    }
    //slDebug("sTargetStepOrStepListID", sTargetStepOrStepListID);
    
    var iLoadResult = {
      status: _const.SL_Success
    };
    
    switch (xResultNode.getAttribute(_const.SL_XMLATTR_TYPE)) {
      case _const.SL_RESULTTYPE_STEPLIST:
        iLoadResult = this.LoadStepList(sTargetStepOrStepListID);
        if (iLoadResult.status == _const.SL_Success) {
          var sInnerStep = xResultNode.getAttribute(_const.SL_XMLATTR_INNERSTEP);
          if (sInnerStep != "" && sInnerStep != null) {
            return this.ProcessInnerStepForNode(xResultNode);
          }
          else {
            //setTimeout(slRunStep, this._setTimeoutDelay);
            return this.RunStep();
          }
        }
        else 
          if (iLoadResult.status == _const.SL_Error) {
          //return slThrow(new Error(-6, "StepList not found by name: " + sTargetStepOrStepListID));
          }
          else {
          //return slThrow(this._ExceptionConditionUnexpected);
          }
        break;
        
      case _const.SL_RESULTTYPE_STEP:
        iLoadResult = this.LoadStepByID(sTargetStepOrStepListID);
        if (iLoadResult.status == _const.SL_Success) {
          //setTimeout(slRunStep, this._setTimeoutDelay);
          return this.RunStep();
        }
        else 
          if (iLoadResult.status == _const.SL_Error) {
          //return slThrow(new Error(-7, "StepList Step not found by ID: " + sTargetStepOrStepListID));
          }
          else {
          //return slThrow(this._ExceptionConditionUnexpected);
          }
        break;
        
      default:
        return //slThrow(new Error(-9, "Step Type not found" + xResultNode.getAttribute(_const.SL_XMLATTR_TYPE)));
    }
    return {
      status: _const.SL_Error
    };
  },
  
  StepForward: function(){
    // TODO slInfo("slStepForward");
    // confirm with the page we are allowed to move forward
    var _xml = $ss.agentcore.dal.xml;
    var _const = $ss.agentcore.constants;
    var returnObject = {};
    
//    if (aSessionData) {
//      this._steplistSession = aSessionData
//    };
//    if (sSessionId) {
//      this._currentSessionId = sSessionId
//    }
    //slBusyStart();
    
    //var oEvent = new Object();
    //oEvent.name = _const.SL_CONFIRM_FORWARD;
    
    //  confirm with the page, or anyone who is listening
    //  that we are allowed to move forward at this time.
    
    //if (slSendMsg(oEvent) != _const.SL_CancelNavigation)
    //{
    var oNextStep = this.GetNextSibling(this._CurrentStepNode, _const.SL_XMLNODE_STEP);
    if (oNextStep != null) {
      var sSiblingID = _xml.GetAttribute(oNextStep, _const.SL_XMLATTR_ID);
      var iLoadStepSuccess = this.LoadStepByID(sSiblingID);
      //TODO debug slDebug("slStepForward", "iLoadStepSuccess : " + iLoadStepSuccess);
      if (_const.SL_Success == iLoadStepSuccess.status) {
        var iRunStepSuccess = this.RunStep();
        if (!iRunStepSuccess || iRunStepSuccess.status != _const.SL_Success) {
          //slStepResetPositionForCanceledNavigation();
          //slSendMsg({name : _const.SL_MOVE_CANCELED});
          //slBusyEnd();
        }
        //slDebug("slStepForward", "iRunStepSuccess : " + iRunStepSuccess);
        return iRunStepSuccess;
      }
    }
    //slError("slStepForward", "Can't find next step.");
    
        return { status: _const.SL_CancelNavigation };
  },
  
  /*******************************************************************************
   ** Name:        slStepBackward
   **
   ** Purpose:     This function handles the back button for steplists
   **
   ** Parameter:   none
   **
   ** Return:      iResult
   *******************************************************************************/
  StepBackward: function(sStepListId,sStepId){
    // slInfo("slStepBackward");
    //Log information
    var _const = $ss.agentcore.constants;

    var iLoadStepListStatus = this.LoadStepList(sStepListId);
        var iLoadStepSuccess = this.LoadStepByID(sStepId);
        if (_const.SL_Success == iLoadStepSuccess.status) {
          var iRunStepSuccess = this.RunStep();
          if (!iRunStepSuccess || iRunStepSuccess.status != _const.SL_Success) {
          //slStepResetPositionForCanceledNavigation();
          //slSendMsg({name : _const.SL_MOVE_CANCELED});
          //slBusyEnd();
          }
          //slDebug("slStepForward", "iRunStepSuccess : " + iRunStepSuccess);
          return iRunStepSuccess;
        }
    return { status: _const.SL_CancelNavigation };   
    /*
     //Why do need confirmbackward
     var iiConfirmBackward = "NOCANCEL"; //added dummy
     //if (iConfirmBackward != slc_CancelNavigation) {
     var aRecords = slHistory_Get();
     
     if (aRecords.length > 0) {
     var aDiscarded = [];
     var oPreviousStep = null;
     var oPreviousStepBoundry = null;
     var oPreviousStepPop = null;
     var sPreviousList, sPreviousStep;
     var xNodeType, bNodeTypeOK;
     var xSkipOnBack, bSkipNode;
     var bFoundNodeForNavigation = false;
     var oCurrentStep = aRecords[aRecords.length - 1];
     
     for (var i = aRecords.length - 1; i >= 0; i--) {
     oPreviousStep = aRecords[i];
     oPreviousStepBoundry = (i > 0) ? aRecords[i - 1] : null;
     //Pop-off last step (retrieve, set to deleted and clear cache)
     oPreviousStepPop = slHistory_Pop();
     
     aDiscarded.push(oPreviousStep);
     
     if (oCurrentStep != oPreviousStep) {
     
     var bIsNavigateable = slIsHistoryStepNavigateable(oPreviousStep);
     
     if (i == 0 || bIsNavigateable) {
     xNodeType = slXmlAttributeGetEvaluated(slg_CurrentStepNode, slc_XMLATTR_TYPE);
     
     var iReloadedToEntry = slStepResetPositonToEntry(oPreviousStep);
     if (slc_Success == iReloadedToEntry) {
     switch (xNodeType) {
     case slc_STEPTYPE_SNAPIN:
     //dont delete the actual call to snapin step.
     ss_xml_AttributeSet(oPreviousStep, "deleted", "0");
     var iRunStep = slStepLoadSnapinFromBack();
     if (iRunStep == slc_Success) {
     slHistory_Trim(oCurrentStep);
     }
     break;
     
     default:
     
     var iRunStep = slRunStep();
     if (iRunStep == slc_Success) {
     var oLastHistory = slHistory_GetLast();
     //Delete everything from landing step on, then restore the newly added step from slRunStep()
     slHistory_Trim(oPreviousStep);
     ss_xml_AttributeSet(oLastHistory, "deleted", "0");
     }
     break;
     }
     
     if (iRunStep != slc_Success) {
     for (var i = 0; i < aDiscarded.length; i++) {
     aDiscarded[i].setAttribute("deleted", "0");
     }
     slStepResetPositionForCanceledNavigation();
     slSendMsg({
     name: slc_MOVE_CANCELED
     });
     slBusyEnd();
     }
     
     slDebug("slStepBackward", "iRunStep : " + iRunStep);
     return iRunStep;
     }
     break;
     }
     }
     }
     }
     //}
     // oEvent.name = slc_MOVE_CANCELED;
     // slSendMsg(oEvent);
     // slBusyEnd();
     return slc_CancelNavigation;
     */
  },
  StepHaveForward: function(){
    //log to check the have forward
    var _const = $ss.agentcore.constants;
    var _xml = $ss.agentcore.dal.xml;
    var aResultsForCurrentStep = _xml.GetNodes(_const.SL_XMLNODE_RESULT, "", this._CurrentStepNode);
    
    if (aResultsForCurrentStep.length > 0) 
      return true;
    
    if (this.GetNextSibling(this._CurrentStepNode, _const.SL_XMLNODE_STEP) != null) 
      return true;
    //TODO look for the caller snapin and see if it has next step if it has next step make it true
    return false;
  },
  
  GetNextSibling: function(oNode, sType){
    //TODO log slInfo("slGetNextSibling");
    try {
      var oSibling = oNode.nextSibling;
      while (true) {
        if (typeof(oSibling) == 'undefined' || oSibling == null) 
          break;
        
        if (oSibling.nodeType == 1) {
          if (oSibling.nodeName == sType) {
            return oSibling;
          }
        }
        oSibling = oSibling.nextSibling;
      }
      //TODO slWarning("slGetNextSibling", "Unable to find sibling of type " + sType);
    } 
    catch (e) {
      //TODO slError("slGetNextSibling", "Error finding sibling " + e.message);
    }
    return null;
  },
  
  GetStepDetails: function(hasForward, milestone, identity) {
    var _const = $ss.agentcore.constants;
    var _xml = $ss.agentcore.dal.xml;
      var currentStep = {};
      currentStep.StepListId = _xml.GetAttribute(this._CurrentStepListDom, _const.SL_XMLATTR_ID);
      currentStep.StepID = _xml.GetAttribute(this._CurrentStepNode, _const.SL_XMLATTR_ID);
      currentStep.StepPath = _xml.GetAttribute(this._CurrentStepNode, _const.SL_XMLATTR_PATH);
      currentStep.StepPage = _xml.GetAttribute(this._CurrentStepNode, _const.SL_XMLATTR_PAGE);
      currentStep.StepType = _xml.GetAttribute(this._CurrentStepNode, _const.SL_XMLATTR_TYPE);
      currentStep.StepCleanup = _xml.GetAttribute(this._CurrentStepNode, _const.SL_XMLATTR_CLEANUP);
      currentStep.StepSkipOnBack = _xml.GetAttribute(this._CurrentStepNode, _const.SL_XMLATTR_SKIPONBACK);
      currentStep.Snapin = this._SnapinName;
      currentStep.milestone = milestone;
      currentStep.identity = identity;
      currentStep.progressTotal = "TODO";
      currentStep.previousProgress = "TODO";
      currentStep.hasForward = hasForward;
      return   currentStep;
  }
  
});
