﻿/** @namespace Holds all registry related functionality*/
$ss.agentcore.dal.registry = $ss.agentcore.dal.registry || {};
$ss.agentcore.lockdown = $ss.agentcore.lockdown || {};
(function()
{
  $.extend($ss.agentcore.dal.registry,
  {
  
  /**
    *  @name GetRegValueEx 
    *  @memberOf $ss.agentcore.dal.registry
    *  @function
    *  @description  
    *  @param sRegRoot registry root
    *  @param sRegKey registry key
    *  @param sRegVal registry value
  *  @param nRegView (DEFAULT_VIEW,ONLY_64BIT_VIEW,ONLY_32BIT_VIEW,NATIVE_VIEW)refer $ss.agentcore.constants for more information
    *  @returns data value, if successful else null
    *  @example
    *           var objReg = $ss.agentcore.dal.registry;
    *           var strConnServer = objReg.GetRegValueEx("HKCU", "Software\\Microsoft\\Windows\\CurrentVersion\\Explorer", "link" );
    */
    GetRegValueEx : function (sRegRoot, sRegKey, sRegVal,nRegView)
    {
      _logger.info('Entered function: $ss.agentcore.dal.registry.GetRegValueEx');
      var sData = null;
      var oInst = null;
      
      try 
      { 
        //[MAC] Check Machine OS Windows/MAC
        if(!bMac)
          sData = _objContainer.QueryRegValueEx(sRegRoot,sRegKey,sRegVal,nRegView);
        else {
          var message = {'JSClassNameKey':'Registry','JSOperationNameKey':'GetRegValue', 'JSArgumentsKey':[sRegRoot, sRegKey, sRegVal], 'JSISSyncMethodKey' : '1'};
          var result = _jsBridge.execute(message);
          var parsedJSONObject = JSON.parse(result);
          sData = parsedJSONObject.Data;
        } 
      } 
      catch (ex) {
        _exception.HandleException(ex,'$ss.agentcore.dal.registry','GetRegValueEx',arguments);
      }
      return sData;
    },

    /**
    *  @name GetRegValue 
    *  @memberOf $ss.agentcore.dal.registry
    *  @function
    *  @description  
    *  @param sRegRoot registry root
    *  @param sRegKey registry key
    *  @param sRegVal registry value
    *  @returns data value, if successful else null
    *  @example
    *           var objReg = $ss.agentcore.dal.registry;
    *           var strConnServer = objReg.GetRegValue("HKCU", "Software\\Microsoft\\Windows\\CurrentVersion\\Explorer", "link" );
    */
    GetRegValue : function (sRegRoot, sRegKey, sRegVal)
    {
      _logger.info('Entered function: $ss.agentcore.dal.registry.GetRegValue');
      var sData = null;
      var oInst = null;
      try 
      { 
        //[MAC] Check Machine OS Windows/MAC
        if(!bMac)
          sData = _objContainer.QueryRegValue(sRegRoot,sRegKey,sRegVal)
        else {
          var message = {'JSClassNameKey':'Registry','JSOperationNameKey':'GetRegValue', 'JSArgumentsKey':[sRegRoot, sRegKey, sRegVal], 'JSISSyncMethodKey' : '1'};
          var result = _jsBridge.execute(message);
          var parsedJSONObject = JSON.parse(result);
          sData = parsedJSONObject.Data;
          _logger.info('Entered function: $ss.agentcore.dal.registry.GetRegValue = ' + sData);
        }
      }
      catch (ex) {
        _exception.HandleException(ex,'$ss.agentcore.dal.registry','GetRegValue',arguments);
      }
      return sData;
    },
    
  /**
    *  @name SetRegValueEx 
    *  @memberOf $ss.agentcore.dal.registry
    *  @function
    *  @description  
    *  @param sRegRoot registry root
    *  @param sRegKey registry key
    *  @param sRegVal registry value
    *  @param sData registry value data 
  *  @param nRegView (DEFAULT_VIEW,ONLY_64BIT_VIEW,ONLY_32BIT_VIEW,NATIVE_VIEW)refer $ss.agentcore.constants for more information
    *  @returns true if successful else false
    *  @example
    *         var objReg = $ss.agentcore.dal.registry;
    *         var strConnServer = objReg.SetRegValueEx("HKCU", "Network\\Del","temp1","temp1val",0);  
    */
    SetRegValueEx : function (sRegRoot, sRegKey, sRegVal, sData, nRegView)
    {
      _logger.info('Entered function: $ss.agentcore.dal.registry.SetRegValueEx');
      var bRet = false;
      try 
      { 
        if(_IsHKLM(sRegRoot))
        {
          if(this.GetRegValueEx(sRegRoot, sRegKey, sRegVal,nRegView) === sData)
          {
            return true;
          }
          var guid = $ss.agentcore.dal.config.GetConfigValue("supportactions","guidRegistryWrite","170b4d1c-3589-4290-b722-b063d09509b0");
          var act = new $ss.agentcore.supportaction(guid);
          if (!act.cabfile){
            return bRet;
          }
          
      var regMacro = _GetRegViewMacro(nRegView);
          
          act.SetParameter("ValueName", sRegRoot + "\\" + sRegKey + regMacro + "\\" + sRegVal );
          act.SetParameter("ValueValue", sData);
          act.SetParameter("ValueType", "REG_SZ");
          
          bRet = (act.Evaluate() == 0);
          act = null;
        }
        else
        {
          bRet  = _objContainer.SetRegValueByTypeEx(sRegRoot, sRegKey, sRegVal, 1, sData,nRegView);
        }
      } 
      catch (ex) 
      {
        _exception.HandleException(ex,'$ss.agentcore.dal.registry','SetRegValue',arguments);
      }
      
      return bRet;
    },
    
    /**
    *  @name SetRegValue 
    *  @memberOf $ss.agentcore.dal.registry
    *  @function
    *  @description  
    *  @param sRegRoot registry root
    *  @param sRegKey registry key
    *  @param sRegVal registry value
    *  @param sData registry value data 
    *  @returns true if successful else false
    *  @example
    *         var objReg = $ss.agentcore.dal.registry;
    *         var strConnServer = objReg.SetRegValue("HKCU", "Network\\Del","temp1","temp1val");  
    */
    SetRegValue : function (sRegRoot, sRegKey, sRegVal, sData)
    {
      _logger.info('Entered function: $ss.agentcore.dal.registry.SetRegValue');
      var bRet = false;
      try 
      { 
        if(_IsHKLM(sRegRoot))
        {
          //[MAC] Check Machine OS Windows/MAC
          if(!bMac) {
            if(this.GetRegValue(sRegRoot, sRegKey, sRegVal) === sData)
              return true;
            var guid = $ss.agentcore.dal.config.GetConfigValue("supportactions","guidRegistryWrite","ff8cb1c7-a918-44f7-bfdb-11c7eebae9e9");
            var act = new $ss.agentcore.supportaction(guid);
            if (!act.cabfile){
              return bRet;
            }
            act.SetParameter("ValueName", sRegRoot + "\\" + sRegKey + "\\" + sRegVal);
            act.SetParameter("ValueValue", sData);
            act.SetParameter("ValueType", "REG_SZ");
            bRet = (act.Evaluate() == 0);
            act = null;
          }
          else {
            var message = {'JSClassNameKey':'Registry','JSOperationNameKey':'SetRegValue', 'JSArgumentsKey':["HKLM", sRegKey, sRegVal, sData], 'JSISSyncMethodKey' : '1'};
            var result = _jsBridge.execute(message);
            var parsedJSONObject = JSON.parse(result);
            bRet = parsedJSONObject.Data;
          }
        }
        else {
          //[MAC] Check Machine OS Windows/MAC
          if(!bMac) {
            bRet  = _objContainer.SetRegValueByType(sRegRoot, sRegKey, sRegVal, 1, sData);
          }
          else {
            var message = {'JSClassNameKey':'Registry','JSOperationNameKey':'SetRegValue', 'JSArgumentsKey':["HKCU", sRegKey, sRegVal, sData], 'JSISSyncMethodKey' : '1'};
            var result = _jsBridge.execute(message);
            var parsedJSONObject = JSON.parse(result);
            bRet = parsedJSONObject.Data;
          }
        }
      }
      catch (ex) {
        _exception.HandleException(ex,'$ss.agentcore.dal.registry','SetRegValue',arguments);
      }
      return bRet;
    },
  
  /**
    *  @name SetRegValueByTypeEx 
    *  @memberOf $ss.agentcore.dal.registry
    *  @function
    *  @description  Sets registry value by type
    *  @param sRegRoot registry root
    *  @param sRegKey registry key
    *  @param sRegVal registry value
    *  @param nType value type  (REG_SZ, REG_BINARY,REG_DWORD, REG_MULTI_SZ) , refer $ss.agentcore.constants for more information
    *  @param sData registry value data 
  *  @param nRegView (DEFAULT_VIEW,ONLY_64BIT_VIEW,ONLY_32BIT_VIEW,NATIVE_VIEW)refer $ss.agentcore.constants for more information
    *  @returns true if successful else false
    *  @example
    *         var objReg = $ss.agentcore.dal.registry;
    *         var = $ss.agentcore.constants;
    *         var strConnServer = objReg.SetRegValueByTypeEx("HKCU", "Network\\Del","temp1",_constants.REG_SZ,"temp1val");
    */
    SetRegValueByTypeEx : function(sRegRoot, sRegKey, sRegVal, nType, sData,nRegView)
    {
      _logger.info('Entered function: $ss.agentcore.dal.registry.SetRegValueByTypeEx');
      var bRet = false;
      try  
      {
        if(_IsHKLM(sRegRoot))
        {
          //[MAC] Check Machine OS Windows/MAC
          if(!bMac) {
            if(this.GetRegValueEx(sRegRoot, sRegKey, sRegVal,nRegView) === sData)
              return true;

            var guid = $ss.agentcore.dal.config.GetConfigValue("supportactions","guidRegistryWrite","ff8cb1c7-a918-44f7-bfdb-11c7eebae9e9");
            var act = new $ss.agentcore.supportaction(guid);
            if (!act.cabfile)
              return bRet;

            var regMacro = _GetRegViewMacro(nRegView);
            act.SetParameter("ValueName", sRegRoot + "\\" + sRegKey + regMacro + "\\" + sRegVal);
            act.SetParameter("ValueValue", sData);
            act.SetParameter("ValueType", _GetRegType(nType));
            bRet = (act.Evaluate() == 0);
            act = null;
          }
          else {
             var message = {'JSClassNameKey':'Registry','JSOperationNameKey':'SetRegValue', 'JSArgumentsKey':["HKLM", sRegKey, sRegVal, sData], 'JSISSyncMethodKey' : '1'};
            var result = _jsBridge.execute(message);
            var parsedJSONObject = JSON.parse(result);
            bRet = parsedJSONObject.Data;
          }
        }
        else
        {
          //[MAC] Check Machine OS Windows/MAC
          if(!bMac)
            bRet  = _objContainer.SetRegValueByTypeEx(sRegRoot, sRegKey, sRegVal, nType, sData,nRegView);
          else {
            var message = {'JSClassNameKey':'Registry','JSOperationNameKey':'SetRegValue', 'JSArgumentsKey':["HKCU", sRegKey, sRegVal, sData], 'JSISSyncMethodKey' : '1'};
            var result = _jsBridge.execute(message);
            var parsedJSONObject = JSON.parse(result);
            bRet = parsedJSONObject.Data;
          }
        }
      }
      catch (ex) 
      {
        _exception.HandleException(ex,'$ss.agentcore.dal.registry','SetRegValueByTypeEx',arguments);
      }
      return bRet;
    },

    /**
    *  @name SetRegValueByType 
    *  @memberOf $ss.agentcore.dal.registry
    *  @function
    *  @description  Sets registry value by type
    *  @param sRegRoot registry root
    *  @param sRegKey registry key
    *  @param sRegVal registry value
    *  @param nType value type  (REG_SZ, REG_BINARY,REG_DWORD, REG_MULTI_SZ) , refer $ss.agentcore.constants for more information
    *  @param sData registry value data 
    *  @returns true if successful else false
    *  @example
    *         var objReg = $ss.agentcore.dal.registry;
    *         var = $ss.agentcore.constants;
    *         var strConnServer = objReg.SetRegValueByType("HKCU", "Network\\Del","temp1",_constants.REG_SZ,"temp1val");
    */
    SetRegValueByType : function(sRegRoot, sRegKey, sRegVal, nType, sData)
    {
      _logger.info('Entered function: $ss.agentcore.dal.registry.SetRegValueByType');
      var bRet = false;
      try  
      {
        if(_IsHKLM(sRegRoot))
        {
          //[MAC] Check Machine OS Windows/MAC
          if(!bMac) {
            if(this.GetRegValue(sRegRoot, sRegKey, sRegVal) === sData)
              return true;

            var guid = $ss.agentcore.dal.config.GetConfigValue("supportactions","guidRegistryWrite","ff8cb1c7-a918-44f7-bfdb-11c7eebae9e9");
            var act = new $ss.agentcore.supportaction(guid);
            if (!act.cabfile){
              return bRet;
            }
            act.SetParameter("ValueName", sRegRoot + "\\" + sRegKey + "\\" + sRegVal);
            act.SetParameter("ValueValue", sData);
            act.SetParameter("ValueType", _GetRegType(nType));

            bRet = (act.Evaluate() == 0);
            act = null;
          }
          else {
            var message = {'JSClassNameKey':'Registry','JSOperationNameKey':'SetRegValue', 'JSArgumentsKey':["HKLM", sRegKey, sRegVal, sData], 'JSISSyncMethodKey' : '1'};
            var result = _jsBridge.execute(message);
            var parsedJSONObject = JSON.parse(result);
            bRet = parsedJSONObject.Data;
          }
        }
        else {
          //[MAC] Check Machine OS Windows/MAC
          if(!bMac)
            bRet  = _objContainer.SetRegValueByType(sRegRoot, sRegKey, sRegVal, nType, sData);
          else {
            var message = {'JSClassNameKey':'Registry','JSOperationNameKey':'SetRegValue', 'JSArgumentsKey':["HKCU", sRegKey, sRegVal, sData], 'JSISSyncMethodKey' : '1'};
            var result = _jsBridge.execute(message);
            var parsedJSONObject = JSON.parse(result);
            bRet = parsedJSONObject.Data;
          }
        }
      }
      catch (ex) {
        _exception.HandleException(ex,'$ss.agentcore.dal.registry','SetRegValueByType',arguments);
      }
      return bRet;
    },
  
  /**
    *  @name RegValueExistsEx 
    *  @memberOf $ss.agentcore.dal.registry
    *  @function
    *  @description  Determines if Registry value exists or not
    *  @param sRegRoot registry root
    *  @param sRegKey registry key
    *  @param sRegVal registry value
  *  @param nRegView (DEFAULT_VIEW,ONLY_64BIT_VIEW,ONLY_32BIT_VIEW,NATIVE_VIEW)refer $ss.agentcore.constants for more information
    *  @returns true if successful else false
    *  @example
    *         var objReg = $ss.agentcore.dal.registry;
    *         var bExists = var strConnServer = objReg.RegValueExistsEx("HKCU", "Network\\Del","temp1");  
    */
    RegValueExistsEx : function (sRegRoot, sRegKey, sRegVal, nRegView)
    {
      _logger.info('Entered function: $ss.agentcore.dal.registry.RegValueExistsEx');
      var bRet = false;
      try 
      {
        //[MAC] Check Machine OS Windows/MAC
        if(!bMac)
          bRet = _objContainer.RegValueExistsEx(sRegRoot, sRegKey, sRegVal, nRegView);
        else {
          var message = {'JSClassNameKey':'Registry','JSOperationNameKey':'RegValueExists', 'JSArgumentsKey':[sRegRoot, sRegKey, sRegVal], 'JSISSyncMethodKey' : '1'}
          var result = _jsBridge.execute(message);
          var parsedJSONObject = JSON.parse(result);
          bRet = parsedJSONObject.Data; 
        }
      }
      catch (ex) {
        _exception.HandleException(ex,'$ss.agentcore.dal.registry','RegValueExistsEx',arguments);
      }
      return bRet;
    },


    /**
    *  @name RegValueExists 
    *  @memberOf $ss.agentcore.dal.registry
    *  @function
    *  @description  Determines if Registry value exists or not
    *  @param sRegRoot registry root
    *  @param sRegKey registry key
    *  @param sRegVal registry value
    *  @returns true if successful else false
    *  @example
    *         var objReg = $ss.agentcore.dal.registry;
    *         var bExists = var strConnServer = objReg.RegValueExists("HKCU", "Network\\Del","temp1");  
    */
    RegValueExists : function (sRegRoot, sRegKey, sRegVal)
    {
      _logger.info('Entered function: $ss.agentcore.dal.registry.RegValueExists');
      var bRet = false;
      try 
      {
        //[MAC] Check Machine OS Windows/MAC
        if(!bMac)
          bRet = _objContainer.RegValueExists(sRegRoot, sRegKey, sRegVal);
        else {
          var message = {'JSClassNameKey':'Registry','JSOperationNameKey':'RegValueExists', 'JSArgumentsKey':[sRegRoot, sRegKey, sRegVal], 'JSISSyncMethodKey' : '1'}
          var result = _jsBridge.execute(message);
          var parsedJSONObject = JSON.parse(result);
          bRet = parsedJSONObject.Data;
        }
      }
      catch (ex) {
        _exception.HandleException(ex,'$ss.agentcore.dal.registry','RegValueExists',arguments);
      }
      return bRet;
    },

  /**
    *  @name DeleteRegKeyEx
    *  @memberOf $ss.agentcore.dal.registry
    *  @function
    *  @description  Deletes a registry key
    *  @param sRegRoot registry root
    *  @param sRegKey registry key
  *  @param nRegView (DEFAULT_VIEW,ONLY_64BIT_VIEW,ONLY_32BIT_VIEW,NATIVE_VIEW)refer $ss.agentcore.constants for more information
    *  @returns true if successful else false
    *  @example
    *         var objReg = $ss.agentcore.dal.registry;
    *         var retVal = objReg.DeleteRegKeyEx("HKCU", "Network\\Del");
    */
    DeleteRegKeyEx : function (sRegRoot, sRegKey, nRegView)
    {
  
      _logger.info('Entered function: $ss.agentcore.dal.registry.DeleteRegKeyEx');
      var bRet = false;
      try  
      {
        if(_IsHKLM(sRegRoot))
        {
          //[MAC] Check Machine OS Windows/MAC
          if(!bMac) {
            /*    
            if(this.EnumRegKeyEx(sRegRoot, sRegKey, "|",nRegView) === "")
            {
              return true;
            }  
            */      
            var guid = $ss.agentcore.dal.config.GetConfigValue("supportactions","guidRegistryDelete","2465e1b1-e256-4589-92da-d3ab98f17587");
            var act = new $ss.agentcore.supportaction(guid);
            if (!act.cabfile){
              return bRet;
            }
            var regMacro = _GetRegViewMacro(nRegView);
      
            if(sRegKey.charAt(sRegKey.length-1) !== "\\")
            {
              act.SetParameter("PathToDelete", sRegRoot + "\\" + sRegKey + regMacro + "\\");
            }
            else
            {
              act.SetParameter("PathToDelete", sRegRoot + "\\" + sRegKey + regMacro);
            }
          
            bRet = (act.Evaluate() === 0);
            act = null;
          }
          else {
            var message = {'JSClassNameKey':'Registry','JSOperationNameKey':'DeleteRegKeyEx', 'JSArgumentsKey':["HKLM", sRegKey], 'JSISSyncMethodKey' : '1'}
            var result = _jsBridge.execute(message);
            var parsedJSONObject = JSON.parse(result);
            bRet = parsedJSONObject.Data;
          }
        }
        else {
          //[MAC] Check Machine OS Windows/MAC
          if(!bMac)
            bRet = _objContainer.DeleteRegKeyEx(sRegRoot,sRegKey, nRegView);
          else {
            var message = {'JSClassNameKey':'Registry','JSOperationNameKey':'DeleteRegKeyEx', 'JSArgumentsKey':["HKCU", sRegKey], 'JSISSyncMethodKey' : '1'}
            var result = _jsBridge.execute(message);
            var parsedJSONObject = JSON.parse(result);
            bRet = parsedJSONObject.Data;
          }
        }
      }
      catch (ex) 
      {
        _exception.HandleException(ex,'$ss.agentcore.dal.registry','DeleteRegKeyEx',arguments);
      }
      return bRet;
    },

    /**
    *  @name DeleteRegKey 
    *  @memberOf $ss.agentcore.dal.registry
    *  @function
    *  @description  Deletes a registry key
    *  @param sRegRoot registry root
    *  @param sRegKey registry key
    *  @returns true if successful else false
    *  @example
    *         var objReg = $ss.agentcore.dal.registry;
    *         var retVal = objReg.DeleteRegKey("HKCU", "Network\\Del");
    */
    DeleteRegKey : function (sRegRoot, sRegKey)
    {  
      _logger.info('Entered function: $ss.agentcore.dal.registry.DeleteRegKey');
      var bRet = false;
      try  
      {
        if(_IsHKLM(sRegRoot))
        {
          if(this.EnumRegKey(sRegRoot, sRegKey) === "")
          {
            return true;
          }

        //[MAC] Check Machine OS Windows/MAC
        if(!bMac) {
          var guid = $ss.agentcore.dal.config.GetConfigValue("supportactions","guidRegistryDelete","2465e1b1-e256-4589-92da-d3ab98f17587");
          var act = new $ss.agentcore.supportaction(guid);
          if (!act.cabfile){
            return bRet;
          }
                
          if(sRegKey.charAt(sRegKey.length-1) !== "\\")
          {
            act.SetParameter("PathToDelete", sRegRoot + "\\" + sRegKey + "\\");
          }
          else
          {
            act.SetParameter("PathToDelete", sRegRoot + "\\" + sRegKey);
          }
          
          bRet = (act.Evaluate() === 0);
          act = null;
}
else {
           var message = {'JSClassNameKey':'Registry','JSOperationNameKey':'DeleteRegKey', 'JSArgumentsKey':["HKLM", sRegKey], 'JSISSyncMethodKey' : '1'}
        var result = _jsBridge.execute(message);
        var parsedJSONObject = JSON.parse(result);
        bRet = parsedJSONObject.Data;
}
        }
        else
        {
          //[MAC] Check Machine OS Windows/MAC
          if(!bMac)
            bRet = _objContainer.DeleteRegKey(sRegRoot,sRegKey);
          else {
            var message = {'JSClassNameKey':'Registry','JSOperationNameKey':'DeleteRegKey', 'JSArgumentsKey':["HKCU", sRegKey], 'JSISSyncMethodKey' : '1'}
            var result = _jsBridge.execute(message);
            var parsedJSONObject = JSON.parse(result);
            bRet = parsedJSONObject.Data;
          }
        }
      } 
      catch (ex) 
      {
        _exception.HandleException(ex,'$ss.agentcore.dal.registry','DeleteRegKey',arguments);
      }
      return bRet;
    },
  
  /**
    *  @name DeleteRegValEx 
    *  @memberOf $ss.agentcore.dal.registry
    *  @function
    *  @description  Deletes a registry value
    *  @param sRegRoot registry root
    *  @param sRegKey registry key
    *  @param sRegVal registry value
  *  @param nRegView (DEFAULT_VIEW,ONLY_64BIT_VIEW,ONLY_32BIT_VIEW,NATIVE_VIEW)refer $ss.agentcore.constants for more information
    *  @returns true if successful else false
    *  @example
    *         var objReg = $ss.agentcore.dal.registry;
    *         var retVal = objReg.DeleteRegValEx("HKCU", "Network\\Del","temp1");
    */
    DeleteRegValEx : function (sRegRoot, sRegKey, sRegVal, nRegView)
    {
      _logger.info('Entered function: $ss.agentcore.dal.registry.DeleteRegVal');
      var bRet = false;
      try  
      {
        if(_IsHKLM(sRegRoot))
        {
          if(!this.RegValueExistsEx(sRegRoot, sRegKey,sRegVal,nRegView))
          {
            return true;
          }

        //[MAC] Check Machine OS Windows/MAC
        if(!bMac) {
          var guid = $ss.agentcore.dal.config.GetConfigValue("supportactions","guidRegistryDelete","2465e1b1-e256-4589-92da-d3ab98f17587");
          var act = new $ss.agentcore.supportaction(guid);
          if (!act.cabfile){
            return bRet;
          }
          var regMacro = _GetRegViewMacro(nRegView);
      
          if(sRegKey.charAt(sRegKey.length-1) !== "\\")
          {
            act.SetParameter("PathToDelete", sRegRoot + "\\" + sRegKey + regMacro + "\\" + sRegVal);
          }
          else
          {
            act.SetParameter("PathToDelete", sRegRoot + "\\" + sRegKey + regMacro + sRegVal);
          }
          
          bRet = (act.Evaluate() === 0);
          act = null;
}
else {
          var message = {'JSClassNameKey':'Registry','JSOperationNameKey':'DeleteRegValEx', 'JSArgumentsKey':["HKLM", sRegKey, sRegVal], 'JSISSyncMethodKey' : '1'}
        var result = _jsBridge.execute(message);
        var parsedJSONObject = JSON.parse(result);
        bRet = parsedJSONObject.Data;
}
    
        }
        else
        {
          //[MAC] Check Machine OS Windows/MAC
          if(!bMac)
            bRet = _objContainer.DeleteRegValEx(sRegRoot,sRegKey,sRegVal,nRegView); 
          else {
            var message = {'JSClassNameKey':'Registry','JSOperationNameKey':'DeleteRegValEx', 'JSArgumentsKey':["HKCU", sRegKey, sRegVal], 'JSISSyncMethodKey' : '1'}
            var result = _jsBridge.execute(message);
            var parsedJSONObject = JSON.parse(result);
            bRet = parsedJSONObject.Data;
          }
        }
      }
      catch (ex) 
      {
        _exception.HandleException(ex,'$ss.agentcore.dal.registry','DeleteRegValEx',arguments);
      }
      return bRet;
    },

    /**
    *  @name DeleteRegVal 
    *  @memberOf $ss.agentcore.dal.registry
    *  @function
    *  @description  Deletes a registry value
    *  @param sRegRoot registry root
    *  @param sRegKey registry key
    *  @param sRegVal registry value
    *  @returns true if successful else false
    *  @example
    *         var objReg = $ss.agentcore.dal.registry;
    *         var retVal = objReg.DeleteRegVal("HKCU", "Network\\Del","temp1");
    */
    DeleteRegVal : function (sRegRoot, sRegKey, sRegVal)
    {
      _logger.info('Entered function: $ss.agentcore.dal.registry.DeleteRegVal');
      var bRet = false;
      try  
      {
        if(_IsHKLM(sRegRoot))
        {
          if(!this.RegValueExists(sRegRoot, sRegKey,sRegVal))
          {
            return true;
          }

          //[MAC] Check Machine OS Windows/MAC
          if(!bMac) {
            var guid = $ss.agentcore.dal.config.GetConfigValue("supportactions","guidRegistryDelete","2465e1b1-e256-4589-92da-d3ab98f17587");
            var act = new $ss.agentcore.supportaction(guid);
            if (!act.cabfile){
              return bRet;
            }
              
            if(sRegKey.charAt(sRegKey.length-1) !== "\\")
            {
              act.SetParameter("PathToDelete", sRegRoot + "\\" + sRegKey + "\\" + sRegVal);
            }
            else
            {
              act.SetParameter("PathToDelete", sRegRoot + "\\" + sRegKey + sRegVal);
            }
          
            bRet = (act.Evaluate() === 0);
            act = null;
          }
          else {
            var message = {'JSClassNameKey':'Registry','JSOperationNameKey':'DeleteRegVal', 'JSArgumentsKey':["HKLM", sRegKey, sRegVal], 'JSISSyncMethodKey' : '1'}
            var result = _jsBridge.execute(message);
            var parsedJSONObject = JSON.parse(result);
            bRet = parsedJSONObject.Data;
          }
        }
        else
        {
          //[MAC] Check Machine OS Windows/MAC
          if(!bMac)
            bRet = _objContainer.DeleteRegVal(sRegRoot,sRegKey,sRegVal);
          else {
            var message = {'JSClassNameKey':'Registry','JSOperationNameKey':'DeleteRegVal', 'JSArgumentsKey':["HKCU", sRegKey, sRegVal], 'JSISSyncMethodKey' : '1'}
            var result = _jsBridge.execute(message);
            var parsedJSONObject = JSON.parse(result);
            bRet = parsedJSONObject.Data;
          }
        }
      }
      catch (ex) 
      {
        _exception.HandleException(ex,'$ss.agentcore.dal.registry','DeleteRegVal',arguments);
      }
      return bRet;
    },
  
  /**
    *  @name EnumRegKeyEx
    *  @memberOf $ss.agentcore.dal.registry
    *  @function
    *  @description  Enumerates subkeys under a given key
    *  @param sRegRoot registry root
    *  @param sRegKey registry key
    *  @param delimeter deletimeter
  *  @param nRegView (DEFAULT_VIEW,ONLY_64BIT_VIEW,ONLY_32BIT_VIEW,NATIVE_VIEW)refer $ss.agentcore.constants for more information
    *  @returns comma seperated list of registry sub keys if successful else empty string
    *  @example
    *         var objReg = $ss.agentcore.dal.registry;
    *         var str = objReg.EnumRegKeyEx("HKCU", "Network\\test","|");
    */
    EnumRegKeyEx:function (sRegRoot, sRegKey,delimeter,nRegView)
    {
      _logger.info('Entered function: $ss.agentcore.dal.registry.EnumRegKeyEx');
      var sKeys = "";
      try
      {
        sKeys = _objContainer.EnumRegKeyEx(sRegRoot,sRegKey,delimeter,nRegView);
      } 
      catch (ex) 
      {
        _exception.HandleException(ex,'$ss.agentcore.dal.registry','EnumRegKeyEx',arguments);
      }
      return sKeys;
    },

    /**
    *  @name EnumRegKey
    *  @memberOf $ss.agentcore.dal.registry
    *  @function
    *  @description  Enumerates subkeys under a given key
    *  @param sRegRoot registry root
    *  @param sRegKey registry key
    *  @param delimeter deletimeter
    *  @returns comma seperated list of registry sub keys if successful else empty string
    *  @example
    *         var objReg = $ss.agentcore.dal.registry;
    *         var str = objReg.EnumRegKey("HKCU", "Network\\test","|");
    */
    EnumRegKey:function (sRegRoot, sRegKey,delimeter)
    {
      _logger.info('Entered function: $ss.agentcore.dal.registry.EnumRegKey');
      var sKeys = "";
      try
      {
        //[MAC] Check Machine OS Windows/MAC
        if(!bMac)
          sKeys = _objContainer.EnumRegKey(sRegRoot,sRegKey,delimeter);
        else {
          var message = {'JSClassNameKey':'Registry','JSOperationNameKey':'EnumRegKey', 'JSArgumentsKey':[sRegRoot, sRegKey, delimeter], 'JSISSyncMethodKey' : '1'}
          var result = _jsBridge.execute(message);
          var parsedJSONObject = JSON.parse(result);
          sKeys = parsedJSONObject.Data;
        }
      }
      catch (ex) {
        _exception.HandleException(ex,'$ss.agentcore.dal.registry','EnumRegKey',arguments);
      }
      return sKeys;
    },
  
  /**
    *  @name EnumRegValueEx 
    *  @memberOf $ss.agentcore.dal.registry
    *  @function
    *  @description  Enumerates value-name|value-value pairs under a given key
    *  @param sRegRoot registry root
    *  @param sRegKey registry key
    *  @param namedelimiter seperator for names
    *  @param valuedelimiter seperator for values
  *  @param nRegView (DEFAULT_VIEW,ONLY_64BIT_VIEW,ONLY_32BIT_VIEW,NATIVE_VIEW)refer $ss.agentcore.constants for more information
    *  @returns a comma separated value-name|value-pair list if successful else empty string
    *  @example
    *         var objReg = $ss.agentcore.dal.registry;
    *         var str = objReg.EnumRegValueEx("HKCU", "Network\\Del","|","|",0);
    */
    EnumRegValueEx : function (sRegRoot, sRegKey, namedelimiter, valuedelimiter, nRegView)
    {
      _logger.info('Entered function: $ss.agentcore.dal.registry.EnumRegValueEx');
      var sData = "";
      try 
      {
        sData = _objContainer.EnumRegValueEx(sRegRoot,sRegKey,namedelimiter,valuedelimiter,nRegView);
      } 
      catch (ex) 
      {
        _exception.HandleException(ex,'$ss.agentcore.dal.registry','EnumRegValueEx',arguments);
      }
      return sData;
    },

    /**
    *  @name EnumRegValue 
    *  @memberOf $ss.agentcore.dal.registry
    *  @function
    *  @description  Enumerates value-name|value-value pairs under a given key
    *  @param sRegRoot registry root
    *  @param sRegKey registry key
    *  @param namedelimiter seperator for names
    *  @param valuedelimiter seperator for values
    *  @returns a comma separated value-name|value-pair list if successful else empty string
    *  @example
    *         var objReg = $ss.agentcore.dal.registry;
    *         var str = objReg.EnumRegValue("HKCU", "Network\\Del","|","|");
    */
    EnumRegValue : function (sRegRoot, sRegKey, namedelimiter, valuedelimiter)
    {
      _logger.info('Entered function: $ss.agentcore.dal.registry.EnumRegValue');
      var sData = "";
      try 
      {
        sData = _objContainer.EnumRegValue(sRegRoot,sRegKey,namedelimiter,valuedelimiter);
      } 
      catch (ex) 
      {
        _exception.HandleException(ex,'$ss.agentcore.dal.registry','EnumRegValue',arguments);
      }
      return sData;
    },
  
  /**
    *  @name SetRegUnicodeStringValueEx 
    *  @memberOf $ss.agentcore.dal.registry
    *  @function
    *  @description Writes string as a UNICODE string in binary reg value 
    *  @param sRegRoot registry root
    *  @param sRegKey registry key
    *  @param sRegVal registry value
    *  @param sData data - registry value data (string representation)
  *  @param nRegView (DEFAULT_VIEW,ONLY_64BIT_VIEW,ONLY_32BIT_VIEW,NATIVE_VIEW)refer $ss.agentcore.constants for more information
    *  @returns true if successful else false
    *  @example
    *         var objReg = $ss.agentcore.dal.registry;
    *         var ret = objReg.SetRegUnicodeStringValueEx("HKCU", "Network\\Del","temp","tempval",0);  
    */
    SetRegUnicodeStringValueEx : function (sRegRoot, sRegKey, sRegVal, sData, nRegView)
    {
      _logger.info('Entered function: $ss.agentcore.dal.registry.SetRegUnicodeStringValueEx');
      var sHex = _utils.ConvertStringToUnicodeHex(sData);
      return (this.SetRegValueByType(sRegRoot, sRegKey, sRegVal, _constants.REG_BINARY, sHex, nRegView));
    },

    /**
    *  @name SetRegUnicodeStringValue 
    *  @memberOf $ss.agentcore.dal.registry
    *  @function
    *  @description Writes string as a UNICODE string in binary reg value 
    *  @param sRegRoot registry root
    *  @param sRegKey registry key
    *  @param sRegVal registry value
    *  @param sData data - registry value data (string representation)
    *  @returns true if successful else false
    *  @example
    *         var objReg = $ss.agentcore.dal.registry;
    *         var ret = objReg.SetRegUnicodeStringValue("HKCU", "Network\\Del","temp","tempval");  
    */
    SetRegUnicodeStringValue : function (sRegRoot, sRegKey, sRegVal, sData)
    {
      _logger.info('Entered function: $ss.agentcore.dal.registry.SetRegUnicodeStringValue');
      var sHex = _utils.ConvertStringToUnicodeHex(sData);
      return (this.SetRegValueByType(sRegRoot, sRegKey, sRegVal, _constants.REG_BINARY, sHex));
    },
  
  /**
    *  @name GetRegUnicodeStringValueWithNewlineEx 
    *  @memberOf $ss.agentcore.dal.registry
    *  @function
    *  @description  Reads a binary reg value as a UNICODE string
    *  @param sRegRoot registry root
    *  @param sRegKey registry key
    *  @param sRegVal registry value
  *  @param nRegView (DEFAULT_VIEW,ONLY_64BIT_VIEW,ONLY_32BIT_VIEW,NATIVE_VIEW)refer $ss.agentcore.constants for more information
    *  @returns string
    *  @example
    *         var objReg = $ss.agentcore.dal.registry;
    *         var ret = objReg.GetRegUnicodeStringValueWithNewlineEx("HKCU", "Network\\Del","temp",0);
    */
    GetRegUnicodeStringValueWithNewlineEx : function (sRegRoot, sRegKey, sRegVal,nRegView)
    {
      _logger.info('Entered function: $ss.agentcore.dal.registry.GetRegUnicodeStringValueWithNewline');
      return(_utils.ConvertUnicodeHexToStringWithNewLine(this.GetRegValueEx(sRegRoot, sRegKey, sRegVal,nRegView)));
    },

    /**
    *  @name GetRegUnicodeStringValueWithNewline 
    *  @memberOf $ss.agentcore.dal.registry
    *  @function
    *  @description  Reads a binary reg value as a UNICODE string
    *  @param sRegRoot registry root
    *  @param sRegKey registry key
    *  @param sRegVal registry value
    *  @returns string
    *  @example
    *         var objReg = $ss.agentcore.dal.registry;
    *         var ret = objReg.GetRegUnicodeStringValueWithNewline("HKCU", "Network\\Del","temp");
    */
    GetRegUnicodeStringValueWithNewline : function (sRegRoot, sRegKey, sRegVal)
    {
      _logger.info('Entered function: $ss.agentcore.dal.registry.GetRegUnicodeStringValueWithNewline');
      return(_utils.ConvertUnicodeHexToStringWithNewLine(this.GetRegValue(sRegRoot, sRegKey, sRegVal)));
    },
  
  /**
    *  @name GetRegUnicodeStringValueEx 
    *  @memberOf $ss.agentcore.dal.registry
    *  @function
    *  @description  Reads a binary reg value as a UNICODE string
    *  @param sRegRoot registry root
    *  @param sRegKey registry key
    *  @param sRegVal registry value
  *  @param nRegView (DEFAULT_VIEW,ONLY_64BIT_VIEW,ONLY_32BIT_VIEW,NATIVE_VIEW)refer $ss.agentcore.constants for more information
    *  @returns string
    *  @example
    *         var objReg = $ss.agentcore.dal.registry;
    *         var ret = objReg.GetRegUnicodeStringValueWithNewline("HKCU", "Network\\Del","temp");
    */
    GetRegUnicodeStringValueEx : function (sRegRoot, sRegKey, sRegVal,nRegView)
    {
      _logger.info('Entered function: $ss.agentcore.dal.registry.GetRegUnicodeStringValue');
      return(_utils.ConvertUnicodeHexToString(this.GetRegValueEx(sRegRoot, sRegKey, sRegVal,nRegView)));
    },

    /**
    *  @name GetRegUnicodeStringValue 
    *  @memberOf $ss.agentcore.dal.registry
    *  @function
    *  @description  Reads a binary reg value as a UNICODE string
    *  @param sRegRoot registry root
    *  @param sRegKey registry key
    *  @param sRegVal registry value
    *  @returns string
    *  @example
    *         var objReg = $ss.agentcore.dal.registry;
    *         var ret = objReg.GetRegUnicodeStringValueWithNewline("HKCU", "Network\\Del","temp");
    */
    GetRegUnicodeStringValue : function (sRegRoot, sRegKey, sRegVal)
    {
      _logger.info('Entered function: $ss.agentcore.dal.registry.GetRegUnicodeStringValue');
      return(_utils.ConvertUnicodeHexToString(this.GetRegValue(sRegRoot, sRegKey, sRegVal)));
    }
  });
  
  var _utils = $ss.agentcore.utils;
  var _objContainer = $ss.agentcore.utils.activex.GetObjInstance();
  var _logger = $ss.agentcore.log.GetDefaultLogger("$ss.agentcore.dal.registry");  
  var _exception = $ss.agentcore.exceptions;
  var _config = $ss.agentcore.dal.config;
  var _constants = $ss.agentcore.constants;
  
  ////////// Private functions /////////////////////////////////////////////////

  /*******************************************************************************
  ** Name:         ss_con_pvt_GetRegString
  **
  ** Purpose:      Returns String for each registry constant ("REG_SZ",etc)
  **
  ** Parameter:    intType - 1,3,4,7 (REG_SZ, etc.)
  **
  *******************************************************************************/
  function _GetRegType(nType)
  {
    switch(nType)
    {
      case $ss.agentcore.constants.REG_SZ:
        return "REG_SZ";
      case $ss.agentcore.constants.REG_BINARY: 
        return "REG_BINARY";
      case $ss.agentcore.constants.REG_DWORD:
        return "REG_DWORD";
      case $ss.agentcore.constants.REG_MULTI_SZ:
        return "REG_MULTI_SZ";
      default:
        return "REG_SZ";
    }
  }
  
  function _IsHKLM(sReg)
  {
    var bRet = false;
    
    if(sReg === "HKLM" || sReg === "HKEY_LOCAL_MACHINE")
    {
      bRet = true;
    }
    
    return bRet;
    
  }
  
  function _GetRegViewMacro(nRegView)
  {
  switch(nRegView)
  {
    case $ss.agentcore.constants.REGVIEW.DEFAULT_VIEW:
      return "__%%REG_VIEW_DEFAULT%%";
    case $ss.agentcore.constants.REGVIEW.ONLY_64BIT_VIEW:
      return "__%%REG_VIEW_64%%";
    case $ss.agentcore.constants.REGVIEW.ONLY_32BIT_VIEW:
      return "__%%REG_VIEW_32%%";
    case $ss.agentcore.constants.REGVIEW.NATIVE_VIEW:
      return "__%%REG_VIEW_NATIVE%%";
    default:
      return "";
  }
  }
  

  //[MAC] Get Machine OS type
  var bMac = $.browser.safari;

  //[MAC] JSBridge object used only for MAC
  try {
    var _jsBridge = window.JSBridge;
  }
  catch(e) {
  }

})();