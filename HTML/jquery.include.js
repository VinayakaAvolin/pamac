/** 
 * $.include - script inclusion jQuery plugin 
 * Based on idea from http://www.gnucitizen.org/projects/jquery-include/ 
 * @author Tobiasz Cudnik <tobiasz.cudnik/gmail.com> 
 * @link http://meta20.net/.include_script_inclusion_jQuery_plugin 
 */ 
// overload jquery's onDomReady 
if ( jQuery.browser.mozilla || jQuery.browser.opera ) { 
    document.removeEventListener( "DOMContentLoaded", jQuery.ready, false ); 
    document.addEventListener( "DOMContentLoaded", function(){ jQuery.ready(); }, false ); 
} 
jQuery.event.remove( window, "load", jQuery.ready ); 
jQuery.event.add( window, "load", function(){ jQuery.ready(); } ); 
jQuery.extend({ 
    includeJSStates: {}, 
    include: function(url, callback, dependency){ 
       ///<summary>Includes the Javascripts With Dependency</summary>
		///<param name="url" type="String/Array" optional="false">List of The Files To Be Loaded</param>
		///<param name="callback" type="Function" optional="optional">Call Back Function To Be Called After Load</param>
		///<param name="dependency" type="???" optional="optional">???</param>
		///<returns type="jQuery" />
        if ( typeof callback != 'function' && ! dependency ) { 
            dependency = callback; 
            callback = null; 
        } 
        url = url.replace('\n', ''); 
        jQuery.includeJSStates[url] = false; 
        var script = document.createElement('script'); 
        script.type = 'text/javascript'; 
        script.onload = function () { 
            jQuery.includeJSStates[url] = true; 
            if ( callback ) 
                callback.call(script); 
        }; 
        script.onreadystatechange = function () { 
            if ( this.readyState != "complete" && this.readyState != "loaded" ) return; 
            jQuery.includeJSStates[url] = true; 
            if ( callback ) 
                callback.call(script); 
        }; 
        script.src = url; 
        if ( dependency ) { 
            if ( dependency.constructor != Array ) 
                dependency = [dependency]; 
            setTimeout(function(){ 
                var valid = true; 
                $.each(dependency, function(k, v){ 
                    if (! v() ) { 
                        valid = false; 
                        return false; 
                    } 
                }) 
                if ( valid ) 
                    document.getElementsByTagName('head')[0].appendChild(script); 
                else 
                    setTimeout(arguments.callee, 10); 
            }, 10); 
        } 
        else 
            document.getElementsByTagName('head')[0].appendChild(script); 
        return function(){ 
            return jQuery.includeJSStates[url]; 
        } 
    }, 
    readyOld: jQuery.ready, 
    ready: function () { 
        ///<summary>Internal Use Only</summary>
        if (jQuery.isReady) return; 
        imReady = true; 
        $.each(jQuery.includeJSStates, function(url, state) { 
            if (! state) 
                return imReady = false; 
        }); 
        if (imReady) { 
            jQuery.readyOld.apply(jQuery, arguments); 
        } else { 
            setTimeout(arguments.callee, 10); 
        } 
    } 
});
