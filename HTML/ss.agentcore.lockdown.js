/** namespace Holds lock down environment related functionality*/
$ss.agentcore.lockdown = $ss.agentcore.lockdown || {};

(function()
{ 
  $.extend($ss.agentcore.lockdown,
  {
  
  /**
  *  @name ConvertStringToBoolean
  *  @memberOf $ss.agentcore.lockdown
  *  @function
  *  @description  Convert the string output returned by the SupportAction into Boolean
  *  @param str String Input                 
  *  @returns boolean
  *  @example
  *
  *
  */
  ConvertStringToBoolean : function(str)
  {
    var bRet = false;
    try{
      var value = str[0].toLowerCase();
      switch(value) {
        case "1":
        case "true":
        case "yes":
          bRet = true;
        case "0":
        case "false":
        case "":
        case "no":
          bRet = false;
        default:
          return Boolean(value);
      }
    } catch(ex){
       bRet = false;
    }
    return bRet;
  },
  /**
  *  @name EvaluateLockDownSA
  *  @memberOf $ss.agentcore.lockdown
  *  @function
  *  @description  Execute the Locked down SupportAction <br/>
  *  @param controlName for passing the name of the ActiveX control
  *  @param methodName for passing the name of the method in the ActiveX control 
  *  @param arrArg for passing the parameters of the method in the ActiveX control. <br/>
  *         This parameter is an array of the parameters.
  *  @returns the output of the SupportAction if succesful else an empty string is returned
  *  @example
  *
  *
  */
  EvaluateLockDownSA : function()
  {
  	var controlName   = arguments[0];
		var methodName  = arguments[1];
		var sa_guid = _config.GetConfigValue("supportactions", "netcheckSA_guid", "0c219a28-ffa5-4256-a800-caee9604b494");  //adding this as default for now if not available in config.xml
		
		var sa_obj = new $ss.agentcore.supportaction(sa_guid); //, _config.GetProviderID(), null, true);
		var strArg = "";
		for (var i=2; i < arguments.length; i++){
			if (typeof(arguments[i]) == "string")
				if (i > 2)
					strArg = strArg + ',"' + "'" + arguments[i] + "'" + '"' ;
				else
					strArg = strArg + '"' + "'" + arguments[i] + "'" + '"' ;
			else
				if (i > 2)
					strArg = strArg + ',' + arguments[i];
				else
					strArg = strArg + arguments[i];
  }
		if(sa_obj.cabfile != "undefined" && sa_obj.cabfile) 
  		{
  		  //sa_obj.SetParameter("cname", controlName);   - not passing the control name as this could be a possible security lapse by exposing all functions of sdcnetcheck
  		  sa_obj.SetParameter("fname", methodName);
   		  sa_obj.SetParameter('fargs', strArg);
   		  sa_obj.SetParameter('provider', _config.GetProviderID());

		  var exitCode = sa_obj.Evaluate();
		  if (exitCode == 0)
		  {
 		    return sa_obj.output; 
		  }
		  else return "";
  		}
  }
  });
  
  var _config = $ss.agentcore.dal.config;
  
})();