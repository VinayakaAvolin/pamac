 /*******************************************************************************
 **  File Name: ss.agentcore.smapi.methods.js
 **
 **  Summary: SupportSoft Modem API class
 **
 **  Description: This javascript contains SupportSoft's Modem API calls. All
 **               modems configured via supportsoft products adhere to this API
 **               and have to provide modem specific implementations based on API.
 **
 *******************************************************************************/
 //<dependsOn>  
//  <script type="text/javascript" src="..\JSUnit\app\jsUnitCore.js"></script>
//  <script type="text/javascript" src="..\jquery.js"></script>
//  <script type="text/javascript" src="..\log4js-mod.js"></script>
//  <script type="text/javascript" src="..\ss.agentcore.ns.js"></script>
//  <script type="text/javascript" src="..\ss.agentcore.log.js"></script>
//  <script type="text/javascript" src="..\ss.agentcore.constants.js"></script>
//  <script type="text/javascript" src="..\ss.agentcore.utils.activex.js"></script>
//  <script type="text/javascript" src="..\ss.agentcore.utils.js"></script>
//  <script type="text/javascript" src="..\ss.agentcore.utils.system.js"></script>
//  <script type="text/javascript" src="..\ss.agentcore.dal.config.js"></script>
//  <script type="text/javascript" src="..\ss.agentcore.lockdown.js"></script>
//  <script type="text/javascript" src="..\ss.agentcore.dal.registry.js"></script>
//  <script type="text/javascript" src="..\ss.agentcore.smapi.utils.js"></script>
//</dependsOn>

$ss.agentcore.smapi = $ss.agentcore.smapi || {};
/** @namespace Holds all smapi related functionality*/
$ss.agentcore.smapi.methods = $ss.agentcore.smapi.methods ||
{};
$ss.agentcore.smapi.methods = Class.extend('', {

  /*******************************************************************************
  ** Class Function List : Functions that Do NOT require an
  ** object instance for invocation
  *******************************************************************************/
  objInstance: null, // single instance of SMAPI class

  /**
  *  @name GetInstance 
  *  @memberOf $ss.agentcore.smapi.methods
  *  @function
  *  @description   SMAPI is used mainly as a singleton class and this function provides applications interface to instantiate SMAPI instance.
  *  @param  bNew : If true a new instance is created else the previously created instance is re-used.
  *  @returns SMAPI Instance Object
  *  @example
  *           var objModem = $ss.agentcore.smapi.methods;
  *           var objModemInstance = objModem.GetInstance(true);
  */
  GetInstance: function(bNew) {
    var bCreateNew = (typeof bNew != "undefined") ? bNew : false;
    return this.GetInstanceWithDetection(bCreateNew, true, []);
  },

  /**
  *  @name GetInstanceWithDetection
  *  @memberOf $ss.agentcore.smapi.methods
  *  @function
  *  @description   SMAPI is used mainly as a singleton class and this function provides applications interface to instantiate SMAPI instance.
  *  @param  bNew : If true a new instance is created else the previously created instance is re-used.
  *  @param  bAutoDetect : This parameter is respected only when bNew is true. If bAutoDetect true detection of the modem will take place, else modem detection will not take place.
  *  @param  arrModemCode : This parameter is respected only when bNew and bAutoDetect both are set to true. If arrModemCode contains a array of modem codes, then it tests if those modems are connected.
  *  @returns SMAPI Instance Object
  *  @example
  *           var objModem = $ss.agentcore.smapi.methods;
  *           var objModemInstance = objModem.GetInstanceWithDetection(true, true, ["ST330"]);
  */
  GetInstanceWithDetection: function(bNew, bAutoDetect, arrModemCode) {
    var bCreateNew = (typeof bNew != "undefined") ? bNew : false;
    if (bCreateNew || (this.objInstance == null)) {
      // create new
      _smapiutils.MapDependencies();
      if (bCreateNew) {
        delete this.objInstance;
        this.objInstance = new _smapiMethods(true);
        if (!arrModemCode) arrModemCode = []; //validate arrModemCode
        else if (typeof (arrModemCode) !== "object") arrModemCode = [arrModemCode]; //if string is passed make it a array
      }
      else {
        delete this.objInstance;
        this.objInstance = new _smapiMethods();
      }      
      if (this.objInstance.sCode == "") {
        this.objInstance._AutoDetect(bAutoDetect, arrModemCode);
      }
    }
    return this.objInstance;
  },

  /**
  *  @name GetInstanceByCode 
  *  @memberOf $ss.agentcore.smapi.methods
  *  @function
  *  @description   SMAPI is used mainly as a singleton class and this function provides applications with interface to instantiate SMAPI using a specific modem code.
  *  @param  modemCode : code of modem whose instance has to be created
  *  @returns SMAPI Instance Object
  *  @example
  *           var objModem = $ss.agentcore.smapi.methods;
  *           var objModemInstancebyCode = objModem.GetInstanceByCode(ST330);
  */
  GetInstanceByCode: function(modemCode) {
    // we need to loose any instance we may have and explicitly need to create 
    // instance for the specified code
    if (this.objInstance) {
      if (this.objInstance.sCode === modemCode) return this.objInstance;
      // remove instance and its includes; old includes have same script id that can
      // clash with new includes
      this.objInstance._RemoveIncludes();
      this.objInstance = null;
    }
    _smapiutils.MapDependencies();
    _smapiutils.SetCode(modemCode);

    // clear any existing firmware databag value if any
    _smapiutils.g_oSMAPIDependencies.RemoveValue(_constants.SMAPI_DB_FIRMWAREID);

    this.objInstance = new _smapiMethods;
    return this.objInstance;
  },

  /**
  *  @name SetLastError 
  *  @memberOf $ss.agentcore.smapi.methods
  *  @function
  *  @description   Exposed to callers to set last error that do NOT have "this" instance.
  *  @param  err : error number or constant
  *  @example
  *           var objModem = $ss.agentcore.smapi.methods;
  *           objModem.SetLastError(SMAPI_SUCCESS);
  */
  SetLastError: function(err) {
    var modem = this.GetInstance();
    modem.mLastError = err;
  },

  /**
  *  @name GetLastError 
  *  @memberOf $ss.agentcore.smapi.methods
  *  @function
  *  @description   Exposed to callers to set last error that do NOT have "this" instance.
  *  @returns  Last Error message in the message stack
  *  @example
  *           var objModem = $ss.agentcore.smapi.methods;
  *           objModem.GetLastError();
  */
  GetLastError: function() {
    var modem = this.GetInstance();
    return modem.mLastError;
  },

  /**
  *  @name SendMsg 
  *  @memberOf $ss.agentcore.smapi.methods
  *  @function
  *  @description   Exposed to callers to send message that do NOT have "this" instance.
  *  @param  oMsg.msgType: Message Type object where msg.type = <type of message> and all other properties (if any) within the object are related to that particular msg type
  *  @returns GetLastError to confirm success
  *  @example
  *           var objModem = $ss.agentcore.smapi.methods;
  *           var errStr = objModem.SendMsg(oEvent)
  */
  SendMsg: function(msgType) {
    var modem = this.GetInstance();
    var oMsg = new Object();
    oMsg.type = msgType;
    modem.SendMsg(oMsg);
  },

  /**
  *  @name GetInstanceValue 
  *  @memberOf $ss.agentcore.smapi.methods
  *  @function
  *  @description   Get values from this.xmlInstanceNode which if formed by augmenting current selected instance with common instance
  *  @param sPropertyName : node name to search in xmlIntance
  *  @param  sDefVal       : value to use if not found in xmlIntance
  *  @returns value of the property if found else sDefVal if passed
  *  @example
  *           var objModem = $ss.agentcore.smapi.methods;
  *           var strVal = objModem.GetInstanceValue("code", "ST330");
  */
  GetInstanceValue: function(sPropertyName, sDefVal) {
    var sValue = (typeof (sDefVal) == 'undefined') ? "" : sDefVal;
    try {
      var modem = this.GetInstance();
      if (modem.mLastError == _constants.SMAPI_SUCCESS) {
        sValue = modem.GetInstanceValue(sPropertyName, sDefVal);
      }
    }
    catch (ex) {
      _logger.error("GetInstanceValue", ex.message);
    }
    return sValue;
  },

  /**
  *  @name GetInstanceArray 
  *  @memberOf $ss.agentcore.smapi.methods
  *  @function
  *  @description   Get values from this.xmlInstanceNode which if formed by augmenting current selected instance with common instance
  *  @param sPropertyName : node name to search in xmlIntance
  *  @param  sDefVal       : value to use if not found in xmlIntance
  *  @returns array containing all node values if found else array with default value
  *  @example
  *           var objModem = $ss.agentcore.smapi.methods;
  *           var objModemInstance = objModem.GetInstanceArray("","");
  */
  GetInstanceArray: function(sPropertyName, sDefVal) {
    var arValues = [];
    var sValue = (typeof (sDefVal) == 'undefined') ? "" : sDefVal;
    try {
      // Get the modem instance
      var modem = this.GetInstance();
      if (modem.mLastError == _constants.SMAPI_SUCCESS) {
        arValues = modem.GetInstanceArray(sPropertyName, sDefVal);
      }
    }
    catch (ex) {
      _Logger.error("GetInstanceArray", "Name: " + sPropertyName + " error: " + ex.message);
    }
    // store default value if array is still empty before returning to the caller
    if (arValues.length == 0)
      arValues.push(sValue);
    return arValues;
  }


}, { // prototype methods
  /**
  *  @name init 
  *  @memberOf $ss.agentcore.smapi.methods
  *  @function
  *  @description   SMAPI (SupportSoft Modem API) Class. This class provides a  published and well documented API which should be used by all  client applications within SupportSoft that do CPE management.
  *  @returns SMAPI Instance Object
  *  @example
  *           var objModem = $ss.agentcore.smapi.methods;        
  */
  init: function(bNull) {
    try {
      this.mArrMsgHandlers = []; // used to register message handlers
      this.mObjResponse = new Object(); // keeps track of response from current call
      this.mLastError = _constants.SMAPI_FAILED_INITIALIZATIION; // keeps track of last error
      this.m_bInQueue = false; // tracks if running in Queue mode
      this.derivedModem = null; // holds the instance of derived modem 
      this.xmlModemDOM = null; // holds the modem xml
      this.xmlModemNode = null; // holds the modem node with the particular modem code
      this.xmlInstanceNode = null; // holds an aggregated/augmented instance node
      this.oInstProperties = null; // holds javascript object convertion of the xmlInstanceNode
      this.arIncPos = []; // holds all the scripts positions for derived implementation
      this.sFirmwareId = ""; // tracks the currently used firmware Id
      this.bDoFirmwareCheck = false; // tracks if firmware check is required to find the correct derived implementation  
      this.bIsFirmwareDefault = false; // tracks if the selected firmware is the default firmware
      if (bNull) {
        this.sCode = "";
      }
      else {
        this.sCode = _smapiutils.GetCode(); // stores the modem code
      }
      // try setting the derived modem
      if (this.sCode == "") {
        // will require auto detection to find the code
        // Don't autodetect here, autodetect calls Detect() which creates new instances.
        //this._AutoDetect();
        this.mLastError = _constants.SMAPI_SUCCESS;
      }
      else {
        // proceed to complete initialization
        if (this._GetModemNode() &&
         this._GetFirmwareIdandCheck() &&
         this._CreateInstance()) {
          // update the last error as initialization has finished successfuly
          this.mLastError = _constants.SMAPI_SUCCESS;
        }
      }
    }
    catch (ex) {
      _logger.error("init", ex.message);
    }
  },

  /*******************************************************************************
  **                  Member Function List : Specific to object instance
  *******************************************************************************/
  /**
  *  @name SetLastError 
  *  @memberOf $ss.agentcore.smapi.methods
  *  @function
  *  @description   Private function for instance members only to set last error.
  *  @param  err : error number or constant
  *  @example
  *           var objModem = $ss.agentcore.smapi.methods;
  *           objModem.SetLastError(SMAPI_SUCESS);
  */
  SetLastError: function(err) {
    this.mLastError = err;
  },

  /**
  *  @name GetLastError 
  *  @memberOf $ss.agentcore.smapi.methods
  *  @function
  *  @description   Private function for instance members only.
  *  @returns  Last Error message in the message stack
  *  @example
  *           var objModem = $ss.agentcore.smapi.methods;
  *           objModem.SetLastError(SMAPI_SUCESS);
  */
  GetLastError: function() {
    return this.mLastError;
  },

  /**
  *  @name GetInstanceValue 
  *  @memberOf $ss.agentcore.smapi.methods
  *  @function
  *  @description   Get values from this.xmlInstanceNode which if formed by augmenting current selected instance with common instance
  *  @param sPropertyName : node name to search in xmlIntance
  *  @param  sDefVal       : value to use if not found in xmlIntance
  *  @returns value of the property if found else sDefVal if passed
  *  @example
  *           var objModem = $ss.agentcore.smapi.methods;
  *           var strVal = objModem.GetInstanceValue("code", "ST330");
  */
  GetInstanceValue: function(sPropertyName, sDefVal) {
    var sValue = (typeof (sDefVal) == 'undefined') ? "" : sDefVal;
    var oNode = null;
    try {
      // Get the modem instance
      if (this.xmlInstanceNode != null) {
        var expr = _constants.SMAPI_XMLCHAR_DOTSLASH + sPropertyName;
        oNode = this.xmlInstanceNode.selectSingleNode(expr);
      }
    }
    catch (ex) {
      _logger.error("_sdcDSLModem_pvt_GetInstanceValue", ex.message);
    }
    return (oNode) ? oNode.text : sValue;
  },

  /**
  *  @name GetInstanceArray 
  *  @memberOf $ss.agentcore.smapi.methods
  *  @function
  *  @description   Get values from this.xmlInstanceNode which if formed by augmenting current selected instance with common instance
  *  @param sPropertyName : node name to search in xmlIntance
  *  @param  sDefVal       : value to use if not found in xmlIntance
  *  @returns array containing all node values if found else array with default value
  *  @example
  *           var objModem = $ss.agentcore.smapi.methods;
  *           var objModemInstance = objModem.GetInstanceArray("","");
  */
  GetInstanceArray: function(sPropertyName, sDefVal) {
    var arValues = [];
    var sValue = (typeof (sDefVal) == 'undefined') ? "" : sDefVal;
    var oNode = null;
    try {
      // Get the modem instance
      if (this.xmlInstanceNode != null) {
        var expr = _constants.SMAPI_XMLCHAR_DOTSLASH + sPropertyName;
        oNode = this.xmlInstanceNode.selectSingleNode(expr);
        if (oNode) {
          if (oNode.hasChildNodes()) {
            var aChildren = oNode.childNodes;
            for (var i = 0; i < aChildren.length; i++) {
              if (aChildren[i].nodeType in
              {
                1: "Element",
                3: "Text"
              }) {
                if (aChildren[i].cdata != null && typeof (aChildren[i].cdata) != "undefined") {
                  sValue = aChildren[i].cdata.toString();
                  arValues.push(sValue);
                }
                else
                  if (aChildren[i].text != null && typeof (aChildren[i].text) != "undefined") {
                  sValue = aChildren[i].text.toString();
                  arValues.push(sValue);
                }
              }
            }
          }
        }
        // store default value if array is still empty before returning to the caller
        if (arValues.length == 0)
          arValues.push(sValue);
      }
    }
    catch (ex) {
      arValues.push(sValue);
      _logger.error("_sdcDSLModem_pvt_GetInstanceArray", "Name: " + sPropertyName + " error: " + ex.message);
    }
    return arValues;
  },

  /**
  *  @name _GetModemNode 
  *  @memberOf $ss.agentcore.smapi.methods
  *  @function
  *  @description   Get xml Node from modemDOM based on the modem Code and sets xmlNode instance variable
  *  @returns  true if successful else false
  */
  _GetModemNode: function() {
    _logger.info("_sdcDSLModem_pvt_GetModemNode", "In _sdcDSLModem_pvt_GetModemNode");
    this.xmlModemDOM = _smapiutils._GetModemDOM();
    if (this.xmlModemDOM == null) {
      this.xmlModemNode = null;
      return false;
    } else {
      var expr = "//modem[@code=\"" + this.sCode + "\"]";
      this.xmlModemNode = this.xmlModemDOM.selectSingleNode(expr);
      return (this.xmlModemNode) ? true : false;
    }
  },

  /**
  *  @name _GetFirmwareIdandCheck 
  *  @memberOf $ss.agentcore.smapi.methods
  *  @function
  *  @description   Looks up the firmware for that modem and sets up the firmware check required flag.
  *  @returns   updates/adds data members to the object
  this.sFirmwareId 
  this.bDoFirmwareCheck
  this.bIsFirmwareDefault
  */
  _GetFirmwareIdandCheck: function() {
    /*
    Uses the following algorithm to look for firmware:
    - Look up databag to see if a firmware id is present. If present then uses it 
    and set the bDoFirmwareCheck to false.
    - If not present then looks up xmlNode for all firmwares. Pick up the default, 
    if present else the first one. Check if that is the only one (bDoFirmwareCheck 
    is set to false). If not the only one then bDoFirmwareCheck is set to true
    */
    var bRet = true;
    try {
      var sFirmwareId = _smapiutils.g_oSMAPIDependencies.GetValue(_constants.SMAPI_DB_FIRMWAREID);
      if (sFirmwareId != null && sFirmwareId != _constants.SMAPI_FIRMWARE_NULL) {
        this.sFirmwareId = sFirmwareId;
        this.bDoFirmwareCheck = false;
      } else {
        // get it from xmlNode
        if (this.xmlModemNode) {
          var oFirmwareList = this.xmlModemNode.selectNodes(_constants.SMAPI_XPATH_FIRMWARE);
          if (oFirmwareList.length == 1) {
            // that is the default and only firmware implementation 
            this.bDoFirmwareCheck = false;
            this.sFirmwareId = oFirmwareList[0].getAttribute(_constants.SMAPI_XMLATTR_ID);
            _smapiutils.g_oSMAPIDependencies.SetValue(_constants.SMAPI_DB_FIRMWAREID, this.sFirmwareId);

          } else if (oFirmwareList.length > 1) {
            // find if default firmware is specified
            for (var i = 0; i < oFirmwareList.length; i++) {
              if (oFirmwareList[0].getAttribute(_constants.SMAPI_XMLATTR_DEFAULT) == _constants.SMAPI_XMLATTR_VAL_TRUE) {
                this.sFirmwareId = oFirmwareList[0].getAttribute(_constants.SMAPI_XMLATTR_ID);
                this.bIsFirmwareDefault = true;
                break;
              }
            }
            if (!this.bIsFirmwareDefault) {
              //just take the first one from list
              this.sFirmwareId = oFirmwareList[0].getAttribute(_constants.SMAPI_XMLATTR_ID);
            }
          } else { // only possible if no firmware section in xmlNode. In such case
            // the one and only provided implementation is to be used.
            this.bDoFirmwareCheck = false;
            this.sFirmwareId = _constants.SMAPI_FIRMWARE_DONTCARE;
            var sMsg = "No specific firmware found in modem xml. Using instance directly" +
                       "Modem code -" + this.sCode + "Function: _sdcDSLModem_pvt_GetFirmwareIdandCheck";
            _logger.debug("_sdcDSLModem_pvt_GetFirmwareIdandCheck", sMsg);
          }
        } else {
          _logger.debug("_sdcDSLModem_pvt_GetFirmwareIdandCheck", "Initialize this.xmlModemNode before calling this function");
          bRet = false;
        }
      }
    } catch (ex) {
      _logger.error("_sdcDSLModem_pvt_GetFirmwareIdandCheck", ex.message);
    }
    return bRet;
  },

  /**
  *  @name _UpdateInstanceToActualFirmware 
  *  @memberOf $ss.agentcore.smapi.methods
  *  @function
  *  @description   Looks up the firmware by using derived modems API. Based on the actual firmware of the modem decides which implementation is to be used. Accordingly updates the instance and resets the  flags.
  *  @returns   updates/adds data members to the object
  this.sFirmwareId 
  this.bDoFirmwareCheck
  this.bIsFirmwareDefault
  */
  _UpdateInstanceToActualFirmware: function() {
    /*
    Uses the following algorithm to check firmware:
    - If firmware is default then the call to derived modem should fetch SMAPI a proper firmware.
    if not then no more checks will be performed and SMAPI returns the error.
    - If firmware is NOT default then SMAPI call to fetch firmware could fail or pass.
    If pass then compare the modem firmware with firmware in modem.xml.
    Use the appropriate firmware on match else report SMAPI error but continue using the instance.
    If failed then goto the second firmware and its associated instance in the list for doing the check.
    so on and so forth, which ever firmware passes use it.
    Please note instances uses the firmware check mechanics need to implement fetching the firmware for
    every derived modem implementation.
    */
    var bRet = false;
    try {
      if (typeof this.derivedModem["Firmware"] != "function") {
        this.SetLastError(_constants.SMAPI_FUNCTION_NOTIMPL);
        return bRet;
      }
      // Get actual firmware  
      var sFirmware = this.derivedModem.Firmware();
      if (this.GetLastError() != _constants.SMAPI_SUCCESS) {
        if (this.bIsFirmwareDefault) {
          if (this.GetLastError() != _constants.SMAPI_NO_AUTH) { // you may not have admin rights so ignore that case
            // A default firmware shouldn't have failed to detect the firmware now
            _logger.error("_sdcDSLModem_pvt_UpdateInstanceToActualFirmware", "Fetching modem firmware using the default implementation has failed.");
          }
          return bRet;
        } else {
          // Check all firmwares and their implementations for success leaving the first
          // one as that implementation has been already used
          var oFirmwareList = this.xmlModemNode.selectNodes(_constants.SMAPI_XPATH_FIRMWARE);
          for (var i = 1; i < oFirmwareList.length; i++) {
            this.sFirmwareId = oFirmwareList[i].getAttribute(_constants.SMAPI_XMLATTR_ID);
            if (this._CreateInstance()) {
              sFirmware = this.derivedModem.Firmware();
              if (this.GetLastError() == _constants.SMAPI_SUCCESS) // implementation worked hence 
                break;
            } else {
              // failure in actual instance creation is FATAL at this point we cannot
              // proceed further.
              _logger.error("_sdcDSLModem_pvt_UpdateInstanceToActualFirmware", "Initialization with new firmware instance has failed");
              return bRet;
            }
          }
        }
        if (this.GetLastError() != _constants.SMAPI_SUCCESS) {
          _logger.error("_sdcDSLModem_pvt_UpdateInstanceToActualFirmware", "None of the implementations worked for \"Firmware\" method.");
          return bRet;
        }
      }

      // Check firmware with one in modem xml  
      var sFirmwareId = this._GetFirmwareId(sFirmware);
      if (sFirmwareId == _constants.SMAPI_FIRMWARE_NOMATCH) {
        // whether we are on default firmware or not lets continue to use it
        // as anyway no match is found
        _logger.warn("_sdcDSLModem_pvt_UpdateInstanceToActualFirmware", "No firmware matched but will continue with current firmware.");
        _smapiutils.g_oSMAPIDependencies.SetValue(_constants.SMAPI_DB_FIRMWAREID, this.sFirmwareId);   // prevents from future checks
        bRet = true;
      } else if (sFirmwareId == this.sFirmwareId) {
        // we are on correct instance no need to modify anything continue as is
        _logger.debug("_sdcDSLModem_pvt_UpdateInstanceToActualFirmware", "No update to firmware is required.");
        _smapiutils.g_oSMAPIDependencies.SetValue(_constants.SMAPI_DB_FIRMWAREID, this.sFirmwareId);   // prevents from future checks
        bRet = true;
      } else {
        // create the actual instance
        this.sFirmwareId = sFirmwareId;
        if (this._CreateInstance()) {
          _smapiutils.g_oSMAPIDependencies.SetValue(_constants.SMAPI_DB_FIRMWAREID, this.sFirmwareId);   // prevents from future checks
          bRet = true;
        } else {
          // failure in actual instance creation is FATAL at this point we cannot
          // proceed further.
          _logger.error("_sdcDSLModem_pvt_UpdateInstanceToActualFirmware", "Initialization with new firmware instance has failed");
        }
      }
    } catch (ex) {
      _logger.error("_sdcDSLModem_pvt_UpdateInstanceToActualFirmware", ex.message);
      this.mLastError = _constants.SMAPI_EXCEPTION;
    }
    return bRet;
  },

  /**
  *  @name _GetFirmwareId 
  *  @memberOf $ss.agentcore.smapi.methods
  *  @function
  *  @description   Looks up the firmware Id in modem xml based on the passed  firmware.
  * @param    sFirmware
  *  @returns   actual firmware Id if match occurs else SMAPI_FIRMWARE_NOMATCH
  */
  _GetFirmwareId: function(sFirmware) {
    var sId = _constants.SMAPI_FIRMWARE_NOMATCH;
    try {
      if (!sFirmware || sFirmware == "") return sId;
      var oFirmwareList = this.xmlModemNode.selectNodes(_constants.SMAPI_XPATH_FIRMWARE);
      var oNode = null;
      var sMinVersion, sMaxVersion, arFirmwareInXml, nLen;
      var bGreaterThanMin, bLessThanMax;
      var arFirmwareActual = sFirmware.split(_constants.SMAPI_FIRMWARE_DELIMITER);
      for (var i = 0; i < oFirmwareList.length; i++) {
        bGreaterThanMin = true;
        bLessThanMax = true;  //assuming current check to pass 
        oNode = oFirmwareList[i].selectSingleNode(_constants.SMAPI_XPATH_FIRMWARE_MIN);
        sMinVersion = (oNode) ? oNode.text : _constants.SMAPI_FIRMWARE_MIN;
        oNode = oFirmwareList[i].selectSingleNode(_constants.SMAPI_XPATH_FIRMWARE_MAX);
        sMaxVersion = (oNode) ? oNode.text : _constants.SMAPI_FIRMWARE_MIN;

        // Do the min version check; Iterate till the smaller of the 2 lengths
        arFirmwareInXml = sMinVersion.split(_constants.SMAPI_FIRMWARE_DELIMITER);
        nLen = (arFirmwareInXml.length < arFirmwareActual.length) ? arFirmwareInXml.length : arFirmwareActual.length;
        for (var j = 0; j < nLen; j++) {
          if (Number(arFirmwareActual[j]) < Number(arFirmwareInXml[j])) {
            bGreaterThanMin = false;
            break;
          } else if (Number(arFirmwareActual[j]) > Number(arFirmwareInXml[j])) {
            break;
          }
        }
        if (!bGreaterThanMin) continue; // to next round

        // Now do the max version check; Iterate till the smaller of the 2 lengths
        arFirmwareInXml = sMaxVersion.split(_constants.SMAPI_FIRMWARE_DELIMITER);
        nLen = (arFirmwareInXml.length < arFirmwareActual.length) ? arFirmwareInXml.length : arFirmwareActual.length;
        for (j = 0; j < nLen; j++) {
          if (Number(arFirmwareActual[j]) > Number(arFirmwareInXml[j])) {
            bLessThanMax = false;
            break;
          } else if (Number(arFirmwareActual[j]) < Number(arFirmwareInXml[j])) {
            break;
          }
        }
        if (bLessThanMax) { //we found the correct firmware
          sId = oFirmwareList[i].getAttribute(_constants.SMAPI_XMLATTR_ID);
          break;
        }
      }
    } catch (ex) {
      _logger.error("_sdcDSLModem_pvt_GetFirmwareId", ex.message);
    }
    return sId;
  },

  /**
  *  @name _CreateInstance 
  *  @memberOf $ss.agentcore.smapi.methods
  *  @function
  *  @description   Private SMAPI function called to create instance of the derived  modem. It looks up the selected firmware section in xmlNode to find the instance referred. Loads appropriate files for that instance and then creates an instance of the node <class>
  *  @returns   sets this.derivedModem

    */
  _CreateInstance: function() {
    var bRet = false;
    try {
      // check firmware and get instance
      if (!this.xmlModemNode) return false;
      var expr = "";
      var oNode = null;
      var oNodeCommon = null;
      if (this.sFirmwareId != _constants.SMAPI_FIRMWARE_DONTCARE) {
        expr = _constants.SMAPI_XPATH_FIRMWARE + "[@id=\"" + this.sFirmwareId + "\"]/instance_ref";
        var oNode = this.xmlModemNode.selectSingleNode(expr);
        if (oNode)
          expr = _constants.SMAPI_XPATH_INSTANCE + "[@id=\"" + oNode.text + "\"]"
        else
          expr = "";
      } else {
        expr = _constants.SMAPI_XPATH_INSTANCE;
      }

      // Eitherways we should have an expression to use by now hence check for it
      if (expr != "") {
        oNode = this.xmlModemNode.selectSingleNode(expr);
        if (oNode != null) {  // we have the instance
          // now check if the instance is common
          var bCommon = (oNode.getAttribute(_constants.SMAPI_XMLATTR_ID) == _constants.SMAPI_XMLATTR_VAL_COMMON);
          if (!bCommon) {
            // check if we have a common instance to use
            expr = "./instance[@id=\"" + _constants.SMAPI_XMLATTR_VAL_COMMON + "\"]";
            oNodeCommon = this.xmlModemNode.selectSingleNode(expr);
            if (oNodeCommon != null) {
              // augment the instance node to common code to create the complete instance node
              _xml.AugmentDoc(oNodeCommon, oNode, "", "./");
            } else {
              oNodeCommon = oNode;
            }
          } else { // instance is common
            oNodeCommon = oNode;
          }
        } // else cannot do much returning null
      }

      // Lets now check the fully formed instance node and load required files
      // to create the actual instance
      if (oNodeCommon) {
        this.xmlInstanceNode = oNodeCommon.cloneNode(true);
        if (this._LoadIncludes()) {

          // monitoring here
          //ss_utl_pvt_LoadIncludeList_Monitor(); ////
          var nIter = 30;
          /*while(g_utl_LoadIncludeOutstandingScriptsCount != 0 && nIter-->0)
          {
          _smapiutils.g_oSMAPIDependencies.Sleep(1000);
          }  */

          var sCmd = "new " + this.GetInstanceValue(_constants.SMAPI_XMLNODE_CLASS) + "(this)";
          if (nIter != 0) // load was succesful
          {
            this.derivedModem = null; //delete any old instance as we
            try {
              this.oInstProperties = null;
              this.derivedModem = eval(sCmd);
            } catch (ex) {
              //also could be because of timing issue hence wait once more
              _logger.error("_sdcDSLModem_pvt_CreateInstance", nIter + " failed creating instance: " + sCmd);
            }
          } else {
            _logger.error("_sdcDSLModem_pvt_CreateInstance", nIter + " failed to load files dynamically: " + sCmd);
          }
        }
        else {
          // delete any old instances if we failed to load includes
          this.derivedModem = null;
        }

        // reaching here should set result to true anyways but to be safe
        bRet = (this.derivedModem != null);
      }
    } catch (ex) {
      _logger.error("_sdcDSLModem_pvt_CreateInstance", ex.message);
    }
    if (!bRet) this.SetLastError(_constants.SMAPI_FAILED_INSTANCE_CREATION);
    return bRet;
  },

  /**
  *  @name _RemoveIncludes 
  *  @memberOf $ss.agentcore.smapi.methods
  *  @function
  *  @description   Removes the scripts files that were appended using pvt_LoadInclude.
  */
  _RemoveIncludes: function() {
    try {
      var i = this.arIncPos.length;
      while (--i >= 0) {
        var oScript = this.arIncPos[i];
        document.getElementsByTagName("HTML")[0].removeChild(oScript);
        this.arIncPos.pop();
      }
    } catch (ex) {
      _logger.error("_sdcDSLModem_pvt_RemoveIncludes", "Failed to remove script. ");
    }
  },

  /**
  *  @name _LoadIncludes 
  *  @memberOf $ss.agentcore.smapi.methods
  *  @function
  *  @description    Creates scripts tag to dynamically include js files. Looks at xml if file name is not defined
  * @param    bRemove  : flag to do cleanup of earlier loads before proceeding by default this value is true 
  *  @returns  true if succesful else false
  */
  _LoadIncludes: function(bRemove /*= true default */) {
    var bRet = true;
    try {
      // Check if we need to clear previous loads
      var bCallRemove = (typeof bRemove == 'undefined') ? true : bRemove;
      if (bCallRemove) this._RemoveIncludes();
      // use script file(s) in the instance xml
      var arIncludes = this.GetInstanceArray(_constants.SMAPI_XMLNODE_SCRIPTFILE);
      var oScript = null;
      var modemSnapinPath = _smapiutils._GetModemSnapinPath();
      for (var i = 0; i < arIncludes.length; i++) {
        oScript = null;
        var sName = "dLoad_" + (i + 1);
        var bFileExists = true;
        var sIncPath = modemSnapinPath + "\\" + _smapiutils.g_oSMAPIDependencies.ParseMacros(arIncludes[i]);
        // KL: check for local file existence before loading includes to prevent
        // unnecessary blocking. To be safe, only do this check if file is of
        // fully qualified local path
        if (sIncPath.indexOf("http") != 0 && sIncPath.indexOf("..") != 0) {
          bFileExists = _smapiutils.g_oSMAPIDependencies.FileExists(sIncPath);
        }

        if (bFileExists) {
          oScript = $.include(_config.ParseMacros(sIncPath));
          if (oScript)
          // store unique id for use later
            this.arIncPos.push(oScript);
        }
        else {
          bRet = false;
          break;
        }
      }
    } catch (ex) {
      _logger.error("_sdcDSLModem_pvt_LoadIncludes", "Failed to load script to HTML: ");
      bRet = false;
    }
    return bRet;
  },

  /**
  *  @name _AutoDetect 
  *  @memberOf $ss.agentcore.smapi.methods
  *  @function
  *  @description   Looks up the firmware Id in modem xml based on the passed  firmware.
  *  @returns  GetLastError to confirm success
  */
  _AutoDetect: function(bAutoDetect, arrModem) {
    _logger.info("_sdcDSLModem_pvt_AutoDetect", "In _sdcDSLModem_pvt_AutoDetect");

    //If autodetect not turned on, just return
    //this wont make sence but still retained as it was present here before
    if (!bAutoDetect) return;

    if (this.derivedModem != null) {
      //already set, return.
      this.SetLastError(_constants.SMAPI_SUCCESS);
      return;
    }

    /*
    Loop through each modem and try to detect.  Since each modem may have a unique
    API's to DDM/TR64/Page Scraping/etc we need to check specific per modem, not protocol.
    */
    var aModemCodes = [];
    if (arrModem.length !== 0) {
      aModemCodes = arrModem;
    } else {
      aModemCodes = _smapiutils.GetAllCodes();
    }
    var sModemType = _config.GetConfigValue("modems", "modem_type");
    var oCheckPaths = new Object();
    var sPath = "";
    for (var i = 0; i < aModemCodes.length; i++) {
      if (aModemCodes[i].substr(0, 10) == "null_modem") continue;  //only check real modems.

      _smapiutils.SetCode(aModemCodes[i]);
      this.sCode = aModemCodes[i];

      //if we can't create protocol, try next...
      if (!(this._GetModemNode() && this._GetFirmwareIdandCheck() && this._CreateInstance())) continue;
      if (this.GetInstanceValue("type") != sModemType) continue;
      if (this.GetInstanceValue("hide") == "1") continue;

      var sPageInfo = this.GetInstanceValue("pageinfo");
      var sTR65Url = this.GetInstanceValue("tr64_location_url");
      if (sPageInfo != "") {
        sPath = this.GetInstanceValue("ip") + sPageInfo;
      } else {
        sPath = sTR65Url;
      }
      if (oCheckPaths[sPath] == true) continue; //Already checked path

      this.mObjResponse.NewStatus = this.derivedModem.Detect();
      if ((this.mLastError == _constants.SMAPI_SUCCESS) && (this.mObjResponse.NewStatus == 1)) {
        //Found modem, check/set code in case same path successful for multiple modems.
        var id = this.derivedModem.ID();
        if (_smapiMethods.GetLastError() == _constants.SMAPI_SUCCESS) {
          var code = _smapiutils.SetCodeById(id);
          if (code != "") {
            this.sCode = code;
            this._GetModemNode();
            this._GetFirmwareIdandCheck();
            this._CreateInstance();
            this.SetLastError(_constants.SMAPI_SUCCESS);
            //Agent doesnt have a modem select page, so make sure autodetect gets important info.
            _smapiutils.SetName(this.GetInstanceValue("name"));
            _smapiutils.SetConnType(this.GetInstanceValue("connection_type"));
            _smapiutils.SetType(this.GetInstanceValue("type"));
            //Also persist the last "known" and "detected" modem
            _smapiutils.g_oSMAPIDependencies.SetMachineValue(_constants.SMAPI_MODEM, _constants.SMAPI_DB_LASTDETECTED_MODEMCODE, aModemCodes[i]);
            _smapiutils.g_oSMAPIDependencies.SetMachineValue(_constants.SMAPI_MODEM, _constants.SMAPI_DB_LASTDETECTED_MODEMCONNTYPE, this.GetInstanceValue("connection_type"));
            _smapiutils.g_oSMAPIDependencies.SetMachineValue(_constants.SMAPI_MODEM, _constants.SMAPI_DB_LASTDETECTED_MODEMCLASS, this.GetInstanceValue("class"));
            _smapiutils.g_oSMAPIDependencies.SetMachineValue(_constants.SMAPI_MODEM, _constants.SMAPI_DB_LASTDETECTED_MODEMNAME, this.GetInstanceValue("name"));
            return;
          }
        } else if (_smapiMethods.GetLastError() == _constants.SMAPI_NO_AUTH) {
          this.SetLastError(_constants.SMAPI_NO_AUTH);
          return; //Let caller handle no auth or leave at prototol
        }
        this.SetLastError(_constants.SMAPI_SUCCESS);
        return;
      }

      //Keep track of URL's we've checked to avoid re-checking.
      oCheckPaths[sPath] = true;
    }  //end for

    // Detection has failed in autodetect mode
    // Setup a null modem to avoid running autodetect each time GetInstance is called
    // and to use basic network checking if no modem.
    var sNullCode = "null_modem_" + sModemType.toLowerCase();
    _smapiutils.SetCode(sNullCode);
    this.sCode = sNullCode;
    this._GetModemNode();
    this._GetFirmwareIdandCheck();
    this._CreateInstance();
    /*_smapiutils.SetName(this.GetInstanceValue("name"));
    Setup some default values, dont know type/connection so set to N/A
    */
    _smapiutils.SetName("No Modem");
    _smapiutils.SetConnType("N/A");
    _smapiutils.SetType("N/A");
    this.SetLastError(_constants.SMAPI_FAILED_AUTODETECT);
    return;
  },

  /**
  *  @name _StartFn 
  *  @memberOf $ss.agentcore.smapi.methods
  *  @function
  *  @description   Private SMAPI function called by every other function to perform
  **               function startup tasks such as:
  **               Logging function start
  **               Setting last Error
  **               Do firmware check if required
  **               Check to derived caller
  * @param    fnName : Name of the function sending the start call
  * @param    msgType: Any msg type if the function wants to sendMsg to application.
  *  @returns  GetLastError to confirm success
  */
  _StartFn: function(fnName, msgType) {
    _logger.info("_sdcDSLModem_pvt_StartFn", "In _sdcDSLModem_" + fnName);
    //clean up previous responses and errors if any
    this.mObjResponse = new Object();
    this.SetLastError(_constants.SMAPI_SUCCESS);
    if (this.derivedModem != null) {
      if (this.bDoFirmwareCheck) {
        // before updatig instance to actual firmware do the detection
        var bDetect = this.derivedModem.Detect();
        if (this.mLastError == _constants.SMAPI_SUCCESS && bDetect) {
          this.bDoFirmwareCheck = !this._UpdateInstanceToActualFirmware();
        }
      }

      if (this.mLastError == _constants.SMAPI_SUCCESS &&
       typeof this.derivedModem[fnName] != "function") {
        this.SetLastError(_constants.SMAPI_FUNCTION_NOTIMPL);
      }
    }
    //check if need to notify application for any message
    if ((typeof msgType != "undefined") && (this.GetLastError() == _constants.SMAPI_SUCCESS)) {
      var oMsg = new Object();
      oMsg.type = msgType;
      this.SendMsg(oMsg);
    }
  },

  /**
  *  @name ID 
  *  @memberOf $ss.agentcore.smapi.methods
  *  @function
  *  @description   Gets the unique identifier for the modem. This could be any  string value accessed from modem that modem vendors define as  unique enough to identify a modem or set of modems.  SMAPI will use this to identify modem(s) after detection. It should be possible to use same derived implementation for such modem(s). 
  *  @returns   GetLastError to confirm success and then check response object [NewID] property.
  */
  ID: function() {
    try {
      this._StartFn("ID");
      if (this.mLastError != _constants.SMAPI_SUCCESS) return;
      this.mObjResponse.NewID = this.derivedModem.ID();
    } catch (ex) {
      _logger.error("_sdcDSLModem_ID", ex.message);
      this.mLastError = _constants.SMAPI_EXCEPTION;
    }
  },

  /**
  *  @name Name 
  *  @memberOf $ss.agentcore.smapi.methods
  *  @function
  *  @description   Gets the modem name as defined in the ss_modems.xml
  *  @returns   GetLastError to confirm success and then check response object [NewName] property.
  */
  Name: function() {
    try {
      this._StartFn("Name");
      if (this.mLastError != _constants.SMAPI_SUCCESS) return;
      this.mObjResponse.NewName = this.derivedModem.Name();
    } catch (ex) {
      _logger.error("_sdcDSLModem_Name", ex.message);
      this.mLastError = _constants.SMAPI_EXCEPTION;
    }
  },

  /**
  *  @name ModemImage 
  *  @memberOf $ss.agentcore.smapi.methods
  *  @function
  *  @description   Looks up the firmware Id in modem xml based on the passed  firmware.
  *  @returns   GetLastError to confirm success and then check response object [NewModemImage] property.
  */
  ModemImage: function() {
    try {
      // this.pvt_StartFn("Name"); NOT CALLED AS DERIVED PROPERTIES CAN BE DIRECTLY LOOKED UP
      _logger.info("_sdcDSLModem_ModemImage", "In _sdcDSLModem_ModemImage");
      //clean up previous responses and errors if any
      this.mObjResponse = new Object();
      this.SetLastError(_constants.SMAPI_SUCCESS);
      if (typeof this.derivedModem.ModemImage != "function") {
        // not defined in child hence use base implementation
        this.mObjResponse.NewModemImage = this.derivedModem.mProperties.image;
      } else {
        // use derived implementation
        this.mObjResponse.NewModemImage = this.derivedModem.ModemImage();
      }
    } catch (ex) {
      _logger.error("_sdcDSLModem_ModemImage", ex.message);
      this.mLastError = _constants.SMAPI_EXCEPTION;
    }
  },

  /**
  *  @name SerialNumber 
  *  @memberOf $ss.agentcore.smapi.methods
  *  @function
  *  @description   Gets Serial Number of derived modem. This info is generally  present at back of the modem.
  *  @returns   GetLastError to confirm success and then check response object [NewSerialNumber] property.
  */
  SerialNumber: function() {
    try {
      this._StartFn("SerialNumber");
      if (this.mLastError != _constants.SMAPI_SUCCESS) return;
      this.mObjResponse.NewSerialNumber = this.derivedModem.SerialNumber();
    } catch (ex) {
      _logger.error("_sdcDSLModem_SerialNumber", ex.message);
      this.mLastError = _constants.SMAPI_EXCEPTION;
    }
  },

  /**
  *  @name MACAddress 
  *  @memberOf $ss.agentcore.smapi.methods
  *  @function
  *  @description   Gets MACAddress of derived modem. 
  *  @returns   GetLastError to confirm success and then check response object [NewMACAddress] property.
  */
  MACAddress: function() {
    try {
      this._StartFn("MACAddress");
      if (this.mLastError != _constants.SMAPI_SUCCESS) return;
      this.mObjResponse.NewMACAddress = this.derivedModem.MACAddress();
    } catch (ex) {
      _logger.error("_sdcDSLModem_MACAddress", ex.message);
      this.mLastError = _constants.SMAPI_EXCEPTION;
    }
  },

  /**
  *  @name Firmware 
  *  @memberOf $ss.agentcore.smapi.methods
  *  @function
  *  @description   Looks up the firmware Id in modem xml based on the passed  firmware.
  *  @returns   GetLastError to confirm success and then check response object [NewFirmwareVersion] property.
  */
  Firmware: function() {
    try {
      this._StartFn("Firmware");
      if (this.mLastError != _constants.SMAPI_SUCCESS) return;
      this.mObjResponse.NewFirmwareVersion = this.derivedModem.Firmware();
    } catch (ex) {
      _logger.error("_sdcDSLModem_Firmware", ex.message);
      this.mLastError = _constants.SMAPI_EXCEPTION;
    }
  },

  /**
  *  @name GetWirelessDefaults 
  *  @memberOf $ss.agentcore.smapi.methods
  *  @function
  *  @description   Retrieves all wireless default values from ss_modems.xml
  *  @returns   object containing the Wireless properties of the modem as defined in the modems.xml
  */
  GetWirelessDefaults: function() {
    // this.pvt_StartFn("Name"); NOT CALLED AS DERIVED PROPERTIES CAN BE DIRECTLY LOOKED UP
    _logger.info("GetWirelessDefaults", "In _sdcDSLModem_GetWirelessDefaults");
    //clean up previous responses and errors if any
    this.mObjResponse = new Object();
    this.SetLastError(_constants.SMAPI_SUCCESS);
    var retObj = new Object();
    var oNode = null;

    try {
      // Get the modem instance
      if (this.xmlInstanceNode != null) {
        var expr = _constants.SMAPI_XMLCHAR_DOTSLASH + _constants.SMAPI_XMLNODE_WIRELESS;
        oNode = this.xmlInstanceNode.selectSingleNode(expr);
      }

      // wireless node exists
      if (oNode) {
        xNodes = oNode.selectNodes("//*[@" + _constants.SMAPI_XMLATTR_DEFAULT + "='true']");
        for (var i = 0; i < xNodes.length; i++) {
          var curNode = xNodes[i];

          // if node is part of a collection, then use its parent instead
          var propName = (curNode.nodeName != _constants.SMAPI_XMLNODE_ITEM) ?
                       curNode.nodeName : curNode.parentNode.nodeName;
          if (curNode.nodeType in { 1: "Element", 3: "Text" }) {
            if (curNode.getAttribute("value") != null) {
              retObj[propName] = window[curNode.getAttribute("value")];
            }
            else if (curNode.cdata != null && typeof (aChildren[i].cdata) != "undefined") {
              retObj[propName] = curNode.cdata.toString();
            }
            else if (curNode.text != null && typeof (curNode.text) != "undefined") {
              retObj[propName] = curNode.text.toString();
            }
          }
        }
      }
    } catch (ex) {
      _logger.error("_sdcDSLModem_GetWirelessDefaults", ex.message);
      this.mLastError = _constants.SMAPI_EXCEPTION;
    }

    this.mObjResponse = retObj;
    return retObj;
  },

  /**
  *  @name GetWirelessSecurityMap 
  *  @memberOf $ss.agentcore.smapi.methods
  *  @function
  *  @description   Create the security map as configured in modems.xml
  *  @returns   object with the security/auth/encryption modes for the wireless router
  */
  GetWirelessSecurityMap: function() {
    // this.pvt_StartFn("Name"); NOT CALLED AS DERIVED PROPERTIES CAN BE DIRECTLY LOOKED UP
    _logger.info("GetWirelessSecurityMap", "In _sdcDSLModem_GetWirelessSecurityMap");
    //clean up previous responses and errors if any
    this.mObjResponse = new Object();
    this.SetLastError(_constants.SMAPI_SUCCESS);
    function _GetXRefIDValue(sNodeName, oNodeInput1, oNodeInput2) {
      oNode = oNodeInput1.selectSingleNode(sNodeName);
      if (oNode) {
        var val = oNode.text;
        var oItemNode = oNodeInput2.selectSingleNode(sNodeName + _constants.SMAPI_XMLCHAR_SLASH + _constants.SMAPI_XMLNODE_ITEM + "[@id='" + val + "']");
        if (oItemNode) {
          return _constants[oItemNode.getAttribute("value")];
        }
      }
    }

    var retObj = {};
    var oSecMapNode = null;
    var oWirelessNode = null;

    try {
      if (this.xmlInstanceNode != null) {
        var expr = _constants.SMAPI_XMLCHAR_DOTSLASH + _constants.SMAPI_XMLNODE_WIRELESS_SECURITY_MAP + _constants.SMAPI_XMLCHAR_SLASH + _constants.SMAPI_XMLNODE_ITEM;
        oSecMapNode = this.xmlInstanceNode.selectNodes(expr);

        expr = _constants.SMAPI_XMLCHAR_DOTSLASH + _constants.SMAPI_XMLNODE_WIRELESS;
        oWirelessNode = this.xmlInstanceNode.selectSingleNode(expr);
      }

      if (oSecMapNode && oWirelessNode)  // found both <wireless_security_map> and <wireless> nodes
      {
        for (var i = 0; i < oSecMapNode.length; i++) {
          var oSecLevel = new _smapiutils.WLSSecurityLevel();
          var curSecMapNode = oSecMapNode[i];

          // sDisplayName
          var oNode = curSecMapNode.selectSingleNode(_constants.SMAPI_XMLNODE_WIRELESS_NAME);
          if (oNode) {
            oSecLevel.sDisplayName = _smapiutils.g_oSMAPIDependencies.ParseLocMacros(oNode.text);
          }

          // oSecurity
          var oSecurity = new _smapiutils.WLSSecurity();
          oSecurity.ulSecMode = _GetXRefIDValue(_constants.SMAPI_XMLNODE_WIRELESS_SECURITY_MODE, curSecMapNode, oWirelessNode);
          oSecurity.ulAuthentication = _GetXRefIDValue(_constants.SMAPI_XMLNODE_WIRELESS_AUTHENTICATION, curSecMapNode, oWirelessNode);
          oSecurity.ulEncryption = _GetXRefIDValue(_constants.SMAPI_XMLNODE_WIRELESS_ENCRYPTION, curSecMapNode, oWirelessNode);
          oSecLevel.oSecurity = oSecurity;

          // bRecommended
          oSecLevel.bRecommended = (curSecMapNode.getAttribute("recommended") == "true");

          // bHide
          oSecLevel.bHide = (curSecMapNode.getAttribute("hide") == "true");

          // bFallback
          oSecLevel.bFallback = (curSecMapNode.getAttribute("fallback") == "true");

          // nOrdinal
          oSecLevel.nOrdinal = Number(curSecMapNode.getAttribute("ordinal"));

          // id
          oSecLevel.id = curSecMapNode.getAttribute("id");

          retObj[oSecLevel.id] = oSecLevel;
        }
      }
    }
    catch (ex) {
      _logger.error("_sdcDSLModem_GetWirelessSecurityMap", ex.message);
      this.mLastError = _constants.SMAPI_EXCEPTION;
    }

    this.mObjResponse = retObj;
    return retObj;
  },

  /**
  *  @name UpdateInstanceToActual 
  *  @memberOf $ss.agentcore.smapi.methods
  *  @function
  *  @description   SMAPI helper function that looks up the firmware by using  derived modems API. Based on the actual firmware of the modem  decides which derived implementation is to be used. Accordingly updates the instance and its properties.
  *  @returns   GetLastError to confirm success and if succeeded derived modem instance and its properties are updated
  */
  UpdateInstanceToActual: function() {
    try {
      // this.pvt_StartFn("Name"); NOT CALLED AS DERIVED PROPERTIES CAN BE DIRECTLY LOOKED UP
      _logger.info("UpdateInstanceToActual", "In _sdcDSLModem_UpdateInstanceToActual");
      //clean up previous responses and errors if any
      this.mObjResponse = new Object();
      this.SetLastError(_constants.SMAPI_SUCCESS);

      if (this.derivedModem != null) {
        if (this.bDoFirmwareCheck) {
          // before updatig instance to actual firmware do the detection
          var bDetect = this.derivedModem.Detect();
          if (this.mLastError == _constants.SMAPI_SUCCESS) {
            if (bDetect) {
              this.bDoFirmwareCheck = !this._UpdateInstanceToActualFirmware();
            } else {
              this.mLastError = _constants.SMAPI_FAIL;
            }
          }
        }
      }
    } catch (ex) {
      _logger.error("_sdcDSLModem_UpdateInstanceToActual", ex.message);
      this.mLastError = _constants.SMAPI_EXCEPTION;
    }
  },

  /**
  *  @name GetInstanceProperties 
  *  @memberOf $ss.agentcore.smapi.methods
  *  @function
  *  @description   SMAPI helper function to get derived modem properties
  *  @returns   mProperties Object if successfull else null
  */
  GetInstanceProperties: function() {
    // this.pvt_StartFn("Name"); NOT CALLED AS DERIVED PROPERTIES CAN BE DIRECTLY LOOKED UP
    _logger.info("_sdcDSLModem_GetInstanceProperties", "In _sdcDSLModem_GetInstanceProperties");
    //clean up previous responses and errors if any
    this.mObjResponse = new Object();
    this.SetLastError(_constants.SMAPI_SUCCESS);
    try {
      if (this.oInstProperties == null && this.xmlInstanceNode != null) {
        // convert xml representation to js object
        var aChildren = this.xmlInstanceNode.childNodes;
        var oInstProperties = {};
        var oElement = null;
        for (var i = 0; i < aChildren.length; i++) {
          oElement = aChildren[i];
          if (oElement.nodeType != 1) continue; //skip on incompatible nodes
          oInstProperties[oElement.nodeName] = _smapiutils.g_oSMAPIDependencies.ParseMacros(oElement.text);
        }
        // overwrite admin username and password if found in databag
        // Dont use databag, they aren't shared between products.  And SA/RM start at first page each
        // time setting all entries to deleted=1 (essentially wiping out DB on every close)
        var adminname = _smapiutils.g_oSMAPIDependencies.GetMachineValue(_constants.SMAPI_MODEM, _constants.SMAPI_DB_ADMINNAME) || "";
        var adminpwd = _smapiutils.g_oSMAPIDependencies.GetMachineValue(_constants.SMAPI_MODEM, _constants.SMAPI_DB_ADMINPASS) || "";
        if ((adminname != null) && (adminpwd != null) &&
          (adminname != "") && (adminpwd != "") &&
          (adminname != "undefined" && adminpwd != "undefined")) {
          oInstProperties[_constants.SMAPI_XMLNODE_ADMINNAME] = adminname;
          oInstProperties[_constants.SMAPI_XMLNODE_ADMINPASS] = _utils.DecryptHexToStr(adminpwd);
        }
        this.oInstProperties = oInstProperties;
      }
    } catch (ex) {
      _logger.error("_sdcDSLModem_GetInstanceProperties", ex.message);
      this.mLastError = _constants.SMAPI_EXCEPTION;
    }

    this.mObjResponse = this.oInstProperties;
    return this.oInstProperties;
  },

  /**
  *  @name GetInstanceProperties 
  *  @memberOf $ss.agentcore.smapi.methods
  *  @function
  *  @description   SMAPI helper function to get derived modem properties
  *  @returns    mProperties Object if successfull else null
  */
  GetInstanceProperties: function() {
    // this.pvt_StartFn("Name"); NOT CALLED AS DERIVED PROPERTIES CAN BE DIRECTLY LOOKED UP
    _logger.info("GetInstanceProperties", "In _sdcDSLModem_GetInstanceProperties");
    //clean up previous responses and errors if any
    this.mObjResponse = new Object();
    this.SetLastError(_constants.SMAPI_SUCCESS);
    try {
      if (this.oInstProperties == null && this.xmlInstanceNode != null) {
        // convert xml representation to js object
        var aChildren = this.xmlInstanceNode.childNodes;
        var oInstProperties = {};
        var oElement = null;
        for (var i = 0; i < aChildren.length; i++) {
          oElement = aChildren[i];
          if (oElement.nodeType != 1) continue; //skip on incompatible nodes
          oInstProperties[oElement.nodeName] = _smapiutils.g_oSMAPIDependencies.ParseMacros(oElement.text);
        }
        // overwrite admin username and password if found in databag
        // Dont use databag, they aren't shared between products.  And SA/RM start at first page each
        // time setting all entries to deleted=1 (essentially wiping out DB on every close)
        var adminname = _smapiutils.g_oSMAPIDependencies.GetMachineValue(_constants.SMAPI_MODEM, _constants.SMAPI_DB_ADMINNAME) || "";
        var adminpwd = _smapiutils.g_oSMAPIDependencies.GetMachineValue(_constants.SMAPI_MODEM, _constants.SMAPI_DB_ADMINPASS) || "";
        if ((adminname != null) && (adminpwd != null) &&
          (adminname != "") && (adminpwd != "") &&
          (adminname != "undefined" && adminpwd != "undefined")) {
          oInstProperties[_constants.SMAPI_XMLNODE_ADMINNAME] = adminname;
          oInstProperties[_constants.SMAPI_XMLNODE_ADMINPASS] = _utils.DecryptHexToStr(adminpwd);
        }
        this.oInstProperties = oInstProperties;
      }
    } catch (ex) {
      _logger.error("_sdcDSLModem_GetInstanceProperties", ex.message);
      this.mLastError = _constants.SMAPI_EXCEPTION;
    }

    this.mObjResponse = this.oInstProperties;
    return this.oInstProperties;
  },

  /**
  *  @name Detect 
  *  @memberOf $ss.agentcore.smapi.methods
  *  @function
  *  @description   Detects a particular modem. Can do auto detect via protocols or by use of derived implementations. Application configurations needs to turn on the autodetect feature.  Auto Detection would fail if PC to modem connection is broken  or modem connected is not protocol compliant.
  * @param     nTries:      Number of tries to do
  * @param     nInterval:   Time interval in seconds between each try
  *  @returns   GetLastError to confirm success and then check response object [NewStatus] property for 0(fail)/1(pass)
  */
  Detect: function(nTries, nInterval) {
    try {
      this._StartFn("Detect", _constants.SMAPI_MSG_DETECT);
      if (this.mLastError != _constants.SMAPI_SUCCESS) return;
      if (this.derivedModem == null) {
        // TBD: auto detect implementation
      } else { // found actual modem code to use
        nTries = (typeof nTries != "undefined") ? nTries : 1;
        nInterval = (typeof nInterval != "undefined") ? nInterval : 1;
        while (nTries) {
          _logger.debug("_sdcDSLModem_Detect", "Trial: " + nTries--);
          this.mObjResponse.NewStatus = this.derivedModem.Detect();
          if ((this.mObjResponse.NewStatus == 1) || (this.mLastError != _constants.SMAPI_SUCCESS)) break;
          _smapiutils.g_oSMAPIDependencies.Sleep(nInterval * 1000);
        }
      }
    } catch (ex) {
      _logger.error("_sdcDSLModem_Detect", ex.message);
      this.SetLastError(_constants.SMAPI_EXCEPTION);
    }
  },

  /**
  *  @name IsSync 
  *  @memberOf $ss.agentcore.smapi.methods
  *  @function
  *  @description   Determine if modem has acquired DSL sync. This is a required  procedure to check modem to DSLAM connection.
  * @param    nTries:      Number of tries to do
  * @param     nInterval:   Time interval in seconds between each try
  *  @returns   GetLastError to confirm success and then check response object [NewStatus] property for 0(fail)/1(pass)
  */
  IsSync: function(nTries, nInterval) {
    try {
      this._StartFn("IsSync", _constants.SMAPI_MSG_SYNC);
      if (this.mLastError != _constants.SMAPI_SUCCESS) return;
      nTries = (typeof nTries != "undefined") ? nTries : 1;
      nInterval = (typeof nInterval != "undefined") ? nInterval : 1;
      while (nTries) {
        _logger.debug("_sdcDSLModem_IsSync", "Trial: " + nTries--);
        this.mObjResponse.NewStatus = this.derivedModem.IsSync();
        if ((this.mObjResponse.NewStatus == 1) || (this.mLastError != _constants.SMAPI_SUCCESS)) break;
        _smapiutils.g_oSMAPIDependencies.Sleep(nInterval * 1000);
      }
    } catch (ex) {
      _logger.error("_sdcDSLModem_IsSync", ex.message);
      this.mLastError = _constants.SMAPI_EXCEPTION;
    }
  },

  /**
  *  @name IsConnected 
  *  @memberOf $ss.agentcore.smapi.methods
  *  @function
  *  @description   Checks (nTries) times to see if modem has WAN IP connectivity and if that's true it tries to ping to a well known site on the WG/Internet as pointed by (modem, internet_pingsite)  property in config xml.
  * @param    bConnectedTo : true  - means check connection to Internet
  * @param    nTries       : Number of tries to check for connectivity
  * @param    nInterval    : Time interval in seconds between each try
  *  @returns    GetLastError to confirm success and then check response object [NewStatus] property for 0(fail)/1(pass)
  */
  IsConnected: function(bConnectedTo, nTries, nInterval) {
    try {
      this._StartFn("IsConnected", _constants.SMAPI_MSG_CONNECT);
      if (this.mLastError != _constants.SMAPI_SUCCESS) return;
      var bPingInternet = ((typeof bConnectedTo == "undefined") || (bConnectedTo == "")) ? 0 : bConnectedTo;
      var nRet = 0;
      nTries = (typeof nTries != "undefined") ? nTries : 1;
      nInterval = (typeof nInterval != "undefined") ? nInterval : 1;
      while (nTries) {
        _logger.debug("_sdcDSLModem_IsConnected", "Trial: " + nTries--);
        this.mObjResponse.NewStatus = this.derivedModem.IsConnected();
        if ((this.mObjResponse.NewStatus == 1) || (this.mLastError != _constants.SMAPI_SUCCESS)) break;
        _smapiutils.g_oSMAPIDependencies.Sleep(nInterval * 1000);
      }
      if ((this.mLastError == _constants.SMAPI_SUCCESS) && (this.mObjResponse.NewStatus == 1)) {
        _logger.debug("_sdcDSLModem_IsConnected", "Modem Connection is UP");
        var status = false;
        if (bPingInternet) {
          // check to see if you can ping internet sites
          var sites = _config.GetValues("modems", "internet_pingsite");
          for (var i = 0; i < sites.length; i++) {
            status = _netCheck.Ping(sites[i]);
            _logger.debug("_sdcDSLModem_IsConnected", "Ping Status: " + status + " for site: " + sites[i]);
            if (!status) {
              this.SetLastError(_constants.SMAPI_PING_FAIL);
              break;
            }
            // BUGID: 28712 --- Circumvent SleepWithMsgDispatch() stalling on Win2k
            if (_utils.GetOS() != 'WIN2K') _smapiutils.g_oSMAPIDependencies.Sleep(1000);
          }
        }
      }
    } catch (ex) {
      _logger.error("_sdcDSLModem_IsConnected", ex.message);
      this.mLastError = _constants.SMAPI_EXCEPTION;
    }
  },

  /**
  *  @name SetUserCreds 
  *  @memberOf $ss.agentcore.smapi.methods
  *  @function
  *  @description   Sets user credentials on the modem and to the registry.
  * @param    sName     :  User name
  * @param    sPassword :  User passowrd  
  *  @returns   GetLastError to confirm success and then check response object [ErrorCode] property
  */
  SetUserCreds: function(sName, sPassword) {
    try {
      this._StartFn("SetUserCreds");
      if (this.mLastError != _constants.SMAPI_SUCCESS) return;
      var oRet = this.derivedModem.SetUserCreds(sName, sPassword);

      // if there is a return value from the derived modem, bubble it up to caller
      // if not, return default ErrorCode property
      if (typeof (oRet) == "object") {
        this.mObjResponse = oRet;
      }
      else {
        this.mObjResponse = { "ErrorCode: ": _constants.SMAPI_VAL_UNKNOWN };
      }

      if (this.GetLastError() == _constants.SMAPI_SUCCESS) { //set in registry too
        _smapiutils.g_oSMAPIDependencies.SetValue(_constants.SMAPI_DB_USERNAME, sName);
        _smapiutils.g_oSMAPIDependencies.SetValue(_constants.SMAPI_DB_USERPASS, _utils.EncryptStrToHex(sPassword));
      }
    } catch (ex) {
      _logger.error("_sdcDSLModem_SetUserCreds", ex.message);
      this.SetLastError(_constants.SMAPI_EXCEPTION);
    }
  },

  /**
  *  @name Reboot 
  *  @memberOf $ss.agentcore.smapi.methods
  *  @function
  *  @description  Reboots modem
  *  @returns  GetLastError to confirm success 
  */
  Reboot: function() {
    try {
      this._StartFn("Reboot");
      if (this.mLastError != _constants.SMAPI_SUCCESS) return;
      // call derived
      this.derivedModem.Reboot();
    } catch (ex) {
      _logger.error("_sdcDSLModem_Reboot", ex.message);
      this.SetLastError(_constants.SMAPI_EXCEPTION);
    }
  },

  /**
  *  @name GetDeviceInfo 
  *  @memberOf $ss.agentcore.smapi.methods
  *  @function
  *  @description    Provides bunch of CPE generic parameters in one shot. Many  implementaions have noticed such information available via 1 or 2 actual modem calls. Thus clubbing it together helps  reducing actual calls being made to modem. Modem implementations can choose to fill all required output parameters or default to -1 if a particular param is not implemented.
  *  @returns   GetLastError to confirm success and then check response object for the following properties:
  **   NewManufacturerName
  **   NewModelName
  **   NewSerialNumber
  **   NewFirmwareVersion
  **   NewHardwareVersion
  */
  GetDeviceInfo: function() {
    try {
      this._StartFn("GetDeviceInfo");
      if (this.mLastError != _constants.SMAPI_SUCCESS) return;
      this.mObjResponse = this.derivedModem.GetDeviceInfo();
    } catch (ex) {
      _logger.error("_sdcDSLModem_GetDeviceInfo", ex.message);
      this.SetLastError(_constants.SMAPI_EXCEPTION);
    }
  },

  /**
  *  @name GetDSLLinkInfo 
  *  @memberOf $ss.agentcore.smapi.methods
  *  @function
  *  @description   Provides bunch of DSL link parameters in one shot. Modem implementations can choose to fill all required output  parameters or default to -1 if a particular param is not implemented.
  *  @returns   GetLastError to confirm success and then check response object for the following properties:
  **               NewUpstreamCurrentRate
  **               NewDownstreamCurrentRate
  **               NewUpstreamNoiseMargin
  **               NewDownstreamNoiseMargin
  **               NewUpstreamPower
  **               NewDownstreamPower
  **               NewUpstreamAttenuation
  **               NewDownstreamAttenuation
  */
  GetDSLLinkInfo: function() {
    try {
      this._StartFn("GetDSLLinkInfo");
      if (this.mLastError != _constants.SMAPI_SUCCESS) return;
      this.mObjResponse = this.derivedModem.GetDSLLinkInfo();
    } catch (ex) {
      _logger.error("_sdcDSLModem_GetDSLLinkInfo", ex.message);
      this.SetLastError(_constants.SMAPI_EXCEPTION);
    }
  },
  /*******************************************************************************
  ** Name:         _sdcDSLModem_GetCWMPInfo
  **
  ** Purpose:      Get the settings on CPE for WAN Management protocol
  **
  ** Parameter:    none
  **
  ** Return:       GetLastError to confirm success and then check response object
  **               for the following properties
  **               NewACSUrl
  **               NewCPEUrl
  **               NewCPEUsername
  **               NewUpgradesManaged
  *******************************************************************************/
  /**
  *  @name GetCWMPInfo 
  *  @memberOf $ss.agentcore.smapi.methods
  *  @function
  *  @description   Get the settings on CPE for WAN Management protocol
  *  @returns   GetLastError to confirm success and then check response object for the following properties:
  **               NewACSUrl
  **               NewCPEUrl
  **               NewCPEUsername
  **               NewUpgradesManaged
  */
  GetCWMPInfo: function() {
    try {
      this._StartFn("GetCWMPInfo");
      if (this.mLastError != _constants.SMAPI_SUCCESS) return;
      this.derivedModem.GetCWMPInfo();
    } catch (ex) {
      _logger.error("_sdcDSLModem_GetCWMPInfo", ex.message);
      this.SetLastError(_constants.SMAPI_EXCEPTION);
    }
  },

  /**
  *  @name SetCWMPInfo 
  *  @memberOf $ss.agentcore.smapi.methods
  *  @function
  *  @description   Sets the CPE WAN Management Protocol parameters on the modem
  * @param    Object of type CWMPInfo  
  ** {
  **   this.sACSUrl        = SMAPI_STR_DONT_CARE; 
  **            // indicates the Url of the management server (ACS)
  **   this.ulACSAuthType  = SMAPI_HEX_DONT_CARE; 
  **            // Authentication used by ACS when CPE communicates
  **   this.sACSUsername   = SMAPI_STR_DONT_CARE; 
  **            // set/get Username that ACS will use to Authenticate CPE
  **   this.sACSPassword   = SMAPI_STR_DONT_CARE; 
  **            // set/get Password that ACS will use to Authenticate CPE
  **   this.ulCPEAuthType  = SMAPI_HEX_DONT_CARE; 
  **            // Authentication used by CPE when ACS communicates
  **   this.sCPEUsername   = SMAPI_STR_DONT_CARE; 
  **            // set/get Username that CPE will use to Authenticate ACS
  **   this.sCPEPassword   = SMAPI_STR_DONT_CARE; 
  **            // set/get Password that CPE will use to Authenticate ACS
  **   this.oUserDefined   = SMAPI_OBJ_DONT_CARE; 
  **            // for future expansion or use with custom implementations
  ** }
  *  @returns   GetLastError to confirm success 
  */
  SetCWMPInfo: function(oCWMPInfo) {
    try {
      this._StartFn("SetCWMPInfo");
      if (this.mLastError != _constants.SMAPI_SUCCESS) return;
      if ((typeof oCWMPInfo == "undefined") || !oCWMPInfo) {
        this.SetLastError(_constants.SMAPI_INVALID_ARGUMENT);
        return;
      }
      this.derivedModem.SetCWMPInfo(oCWMPInfo);
    } catch (ex) {
      _logger.error("_sdcDSLModem_SetCWMPInfo", ex.message);
      this.SetLastError(_constants.SMAPI_EXCEPTION);
    }
  },

  /**
  *  @name UpgradeFirmware 
  *  @memberOf $ss.agentcore.smapi.methods
  *  @function
  *  @description  Calls derived modem implementations for firmware upgrade
  * @param    bDownload : true means firmware has to be downloaded 
  * @param   sUrl      : Url of the firmware to be downloaded
  *  @returns   GetLastError to confirm success 
  */
  UpgradeFirmware: function(bDownload, sUrl) {
    try {
      this._StartFn("UpgradeFirmware");
      if (this.mLastError != _constants.SMAPI_SUCCESS) return;
      if ((typeof sUrl == "undefined") || !sUrl || sUrl == "") {
        this.SetLastError(_constants.SMAPI_INVALID_ARGUMENT);
        return;
      }
      this.derivedModem.UpgradeFirmware(bDownload, sUrl);
    } catch (ex) {
      _logger.error("_sdcDSLModem_UpgradeFirmware", ex.message);
      this.SetLastError(_constants.SMAPI_EXCEPTION);
    }
  },

  /**
  *  @name GetWirelessInfo 
  *  @memberOf $ss.agentcore.smapi.methods
  *  @function
  *  @description   Retrieves all the state variables other than the wireless statistics and security keys. Modem implementations can choose to fill all required output parameters or default to -1 if a  particular param is not implemented.
  *  @returns    GetLastError to confirm success and then check response object for the following properties:
  **               NewEnable
  **               NewStatus
  **               NewMaxBitRate
  **               NewChannel
  **               NewMACAddressControlEnabled
  **               NewStandard
  **               NewMACAddress
  **               ---------------------------------------------------------------
  **               Optional for vendor specific implementations if not TR64 as 
  **               proprietary implementations can use other SMAPI calls as well
  **               to return below information
  **               NewSSID                           OR use GetWirelessSSID
  **               NewSecurityMode                   OR use GetWirelessSecurity
  **               NewBasicEncryptionModes           OR use GetWirelessSecurity
  **               NewBasicAuthenticationMode        OR use GetWirelessSecurity
  */
  GetWirelessInfo: function() {
    try {
      this._StartFn("GetWirelessInfo");
      if (this.mLastError != _constants.SMAPI_SUCCESS) return;
      this.mObjResponse = this.derivedModem.GetWirelessInfo();
    } catch (ex) {
      _logger.error("_sdcDSLModem_GetWirelessInfo", ex.message);
      this.SetLastError(_constants.SMAPI_EXCEPTION);
    }
  },

  /**
  *  @name SetWirelessConfig 
  *  @memberOf $ss.agentcore.smapi.methods
  *  @function
  *  @description   Applies wireless configuration to the modem
  * @param    object of type WLSSecurity
  *  @returns  GetLastError to confirm success
  */
  SetWirelessConfig: function(oWLSConfig) {
    try {
      this._StartFn("SetWirelessConfig");
      if (this.mLastError != _constants.SMAPI_SUCCESS) return;
      this.derivedModem.SetWirelessConfig(oWLSConfig);
    } catch (ex) {
      _logger.error("_sdcDSLModem_SetWirelessConfig", ex.message);
      this.SetLastError(_constants.SMAPI_EXCEPTION);
    }
  },

  /**
  *  @name GetWirelessSecurity 
  *  @memberOf $ss.agentcore.smapi.methods
  *  @function
  *  @description   Retrieves all the security variables requested by caller.
  * @param    ulFlag : Helps implementations in knowing what is required by caller at this particular time. This helps in reducing the calls made to modem or targetting them better.
  **               SMAPI_HEX_DONT_CARE : return only what is set in modem
  **               WLS_SECMODE_BASIC   : return properties set for mode Basic
  **               WLS_SECMODE_WPA     : return properties set for mode WPA
  **               WLS_SECMODE_WPA2    : return properties set for 11i.
  *  @returns   GetLastError to confirm success and the check response object for object of type WLSSecurity
  */
  GetWirelessSecurity: function(ulFlag) {
    try {
      this._StartFn("GetWirelessSecurity");
      if (this.mLastError != _constants.SMAPI_SUCCESS) return;
      this.mObjResponse = this.derivedModem.GetWirelessSecurity(ulFlag);
    } catch (ex) {
      _logger.error("_sdcDSLModem_GetWirelessSecurity", ex.message);
      this.SetLastError(_constants.SMAPI_EXCEPTION);
    }
  },

  /**
  *  @name SetWirelessSecurity 
  *  @memberOf $ss.agentcore.smapi.methods
  *  @function
  *  @description   Applies Wireless security settings to the modem
  * @param   object of type WLSSecurity
  *  @returns   GetLastError to confirm success 
  */
  SetWirelessSecurity: function(oWLSSecurity) {
    try {
      this._StartFn("SetWirelessSecurity");
      if (this.mLastError != _constants.SMAPI_SUCCESS) return;
      this.derivedModem.SetWirelessSecurity(oWLSSecurity);
    } catch (ex) {
      _logger.error("_sdcDSLModem_SetWirelessSecurity", ex.message);
      this.SetLastError(_constants.SMAPI_EXCEPTION);
    }
  },

  /**
  *  @name GetSSIDInfo 
  *  @memberOf $ss.agentcore.smapi.methods
  *  @function
  *  @description  Sets Network name(SSID) and SSID broadcast values from modem. Implementations can choose to fill all required output parameters or default to -1 if a particular param is not implemented.
  * @param    ulFlag : Helps implementations in knowing what is required by caller at this particular time. This helps in reducing the calls made to modem or targetting them better.
  *  @returns  GetLastError to confirm success and then check response object for the following properties
  **               NewSSID
  **               NewSSIDBroadcastEnabled
  */
  GetSSIDInfo: function(ulFlag) {
    try {
      this._StartFn("GetSSIDInfo");
      if (this.mLastError != _constants.SMAPI_SUCCESS) return;
      this.mObjResponse = this.derivedModem.GetSSIDInfo(ulFlag);
    } catch (ex) {
      _logger.error("_sdcDSLModem_GetSSIDInfo", ex.message);
      this.SetLastError(_constants.SMAPI_EXCEPTION);
    }
  },

  /**
  *  @name SetSSIDInfo 
  *  @memberOf $ss.agentcore.smapi.methods
  *  @function
  *  @description   Sets Network name(SSID) and SSID broadcast values to modem.
  * @param   object of type oWLSSSID
  ** WLSSSID() {   // all default values mean no operation to perform for those values
  **               // leave settings in modem as is
  **   this.sSSID            = SMAPI_STR_DONT_CARE;   // Server Set ID
  **   this.nSSIDBroadcast   = SMAPI_INT_DONT_CARE;   // 1:On / 0: Off
  ** }
  *  @returns   GetLastError to confirm success 
  */
  SetSSIDInfo: function(oWLSSSID) {
    try {
      this._StartFn("SetSSIDInfo");
      if (this.mLastError != _constants.SMAPI_SUCCESS) return;
      this.derivedModem.SetSSIDInfo(oWLSSSID);
    } catch (ex) {
      _logger.error("_sdcDSLModem_SetSSIDInfo", ex.message);
      this.SetLastError(_constants.SMAPI_EXCEPTION);
    }
  },

  /**
  *  @name _GetFirmwareId 
  *  @memberOf $ss.agentcore.smapi.methods
  *  @function
  *  @description   By using this function caller is hinting to start queing of all the SMAPI calls made hence forth till a commit commands is received. Implementations can hook up their support for this accordingly and see how they can do a batch update on CPE's in their code. They should return a true /false to tell if queing was successful. 
  *  @returns    GetLastError to confirm success 
  */
  QueueCommands: function() {
    try {
      this._StartFn("QueueCommands");
      if (this.mLastError != _constants.SMAPI_SUCCESS) return;
      if (!this.m_bInQueue) {
        this.m_bInQueue = this.derivedModem.QueueCommands();
      } else {
        // we are already queued so cannot issue another call
        this.SetLastError(_constants.SMAPI_QUEUED_ALREADY);
      }
    } catch (ex) {
      _logger.error("_sdcDSLModem_QueueCommands", ex.message);
      this.SetLastError(_constants.SMAPI_EXCEPTION);
    }
  },

  /**
  *  @name InQueue 
  *  @memberOf $ss.agentcore.smapi.methods
  *  @function
  *  @description   Used to know if SMAPI is in Queue mode
  *  @returns   true: If In Queue else false. Could return -1 in exception  cases
  */
  InQueue: function() {
    // this.pvt_StartFn("UseAdminCreds"); NO DERIVED IMPLEMENTATIONS ARE REQUIRED
    _logger.info("_sdcDSLModem_InQueue", "In _sdcDSLModem_InQueue");
    //clean up previous responses and errors if any
    this.mObjResponse = new Object();
    this.SetLastError(_constants.SMAPI_SUCCESS);

    try {
      this.mObjResponse.NewQueueCommandsEnabled = this.m_bInQueue; //for test harness
      return this.m_bInQueue; //for ease with internal callers.
    } catch (ex) {
      _logger.error("_sdcDSLModem_InQueue", ex.message);
      this.SetLastError(_constants.SMAPI_EXCEPTION);
    }
    return -1;
  },

  /**
  *  @name CommitQueuedCommands 
  *  @memberOf $ss.agentcore.smapi.methods
  *  @function
  *  @description  By using this function caller is hinting to start queing of all the SMAPI calls made hence forth till a commit commands is received. Implementations can hook up their support for this  accordingly and see how they can do a batch up[date on CPE's in their code.
  *  @returns   GetLastError to confirm success 
  */
  CommitQueuedCommands: function() {
    try {
      this._StartFn("CommitQueuedCommands");
      if (this.mLastError != _constants.SMAPI_SUCCESS) return;
      if (this.m_bInQueue) {
        this.m_bInQueue = false; //set this before calling derived as irrespective of
        //success of failure we'll come out of queing
        this.mObjResponse.CommitResponse = this.derivedModem.CommitQueuedCommands();
      }
    } catch (ex) {
      _logger.error("_sdcDSLModem_CommitQueuedCommands", ex.message);
      this.SetLastError(_constants.SMAPI_EXCEPTION);
    }
  },

  /**
  *  @name ExpandSMAPI 
  *  @memberOf $ss.agentcore.smapi.methods
  *  @function
  *  @description   Looks up the firmware Id in modem xml based on the passed  firmware.
  * @param   sFName : Pass your function name to be called
  * @param    oArgs  : Can have all required function arguments as object's properties. Object is passed as is to derived  implementations
  *  @returns   GetLastError to confirm success and then check response object for properties set by implementations
  */
  ExpandSMAPI: function(sFName, oArgs) {
    try {
      // EE14863: this.pvt_StartFn("ExpandSMAPI"); 
      // Not called to relax implementation of ExpandSMAPI in derived modems. Check for
      // implementation of the function sFName instead; since that is what gets called.
      // clean up previous responses and errors if any
      this.mObjResponse = new Object();
      this.SetLastError(_constants.SMAPI_SUCCESS);
      if (typeof this.derivedModem[sFName] != "function") {
        this.SetLastError(_constants.SMAPI_FUNCTION_NOTIMPL);
        return;
      }
      this.mObjResponse = this.derivedModem[sFName](oArgs);
    } catch (ex) {
      _logger.error("_sdcDSLModem_ExpandSMAPI", ex.message);
      this.SetLastError(_constants.SMAPI_EXCEPTION);
    }
  },

  /**
  *  @name RegisterMsgHandler 
  *  @memberOf $ss.agentcore.smapi.methods
  *  @function
  *  @description   Applications using SMAPI should register their msg handlers with SMAPI by making use of this call. SMAPI will internally send messages to these handlers as and when required during cpe configuration.
  * @param    fnHandler : Actual function within application listening to SMAPI events
  * @param   arMessages: Array of SMAPI defined messages such as
  **    SMAPI_MSG_ALL - subscribes handler to all messsages
  **     SMAPI_MSG_ONSYNC_START..etc
  **      If array is null or not defined then msg handler is registered to receive all SMAPI msgs.
  *  @returns   GetLastError to confirm success 
  */
  RegisterMsgHandler: function(fnHandler, arMessages) {
    try {
      _logger.info("_sdcDSLModem_RegisterMsgHandler", "In _sdcDSLModem_RegisterMsgHandler");
      this.SetLastError(_constants.SMAPI_SUCCESS);

      // If arMessages is null or not defined then msg handler is registered to receive all SMAPI msgs.
      if ((typeof arMessages == "undefined") || (arMessages == null)) {
        arMessages = [_constants.SMAPI_MSG_ALL];
      }

      if ((fnHandler != null) && (typeof fnHandler == "function")) {
        var obj = new Object();
        obj.fnHandler = fnHandler;
        obj.arMessages = arMessages;
        this.mArrMsgHandlers.push(obj);
        _logger.debug("_sdcDSLModem_RegisterMsgHandler", "Registered " + fnHandler.toString().split("(")[0] + "[" + arMessages.toString() + "]");
        return true;
      } else {
        this.SetLastError(_constants.SMAPI_FAIL);
        return false;
      }
    } catch (ex) {
      _logger.error("_sdcDSLModem_RegisterMsgHandler", ex.message);
      _smapiMethods.SetLastError(_constants.SMAPI_EXCEPTION);
      return false;
    }
  },

  /**
  *  @name UseAdminCreds 
  *  @memberOf $ss.agentcore.smapi.methods
  *  @function
  *  @description   Allows applications to provide admin username and password to SMAPI. The username, password is used by SMAPI in all calls requiring authentication. This does not modify the actual password on the modem, for that us SetAdminPassword.
  * @param     sName     : Admin user name to use
  * @param      sPassword : Admin password to use
  * @param       nSaveVal  : 0/1. When 1 values are encrypted and saved in databag.  It helps in avoiding user to be asked again per application. Set to 1 by default if not defined.
  *  @returns   GetLastError to confirm success 
  */
  UseAdminCreds: function(sName, sPassword, nSaveVal) {
    try {
      // this.pvt_StartFn("UseAdminCreds"); NO DERIVED IMPLEMENTATIONS ARE REQUIRED
      _logger.info("_sdcDSLModem_UseAdminCreds", "In _sdcDSLModem_UseAdminCreds");
      //clean up previous responses and errors if any
      this.mObjResponse = new Object();
      this.SetLastError(_constants.SMAPI_SUCCESS);

      if (sPassword == null) sPassword = "cancelpressedbyuser";
      nSaveVal = (typeof nSaveVal == "undefined") ? 1 : nSaveVal;
      // set derived modem properties
      this.derivedModem.mProperties.modemuser = sName;
      this.derivedModem.mProperties.modempwd = sPassword;
      if (nSaveVal == 1) {
        //Dont set in databag, doesnt persist accross products or sessions in SA/RM.  Set in HKLM shared by all.
        _smapiutils.g_oSMAPIDependencies.SetMachineValue(_constants.SMAPI_MODEM, _constants.SMAPI_DB_ADMINNAME, sName);
        _smapiutils.g_oSMAPIDependencies.SetMachineValue(_constants.SMAPI_MODEM, _constants.SMAPI_DB_ADMINPASS, _utils.EncryptStrToHex(sPassword));
      }
      this.SetLastError(_constants.SMAPI_SUCCESS);
    } catch (ex) {
      _logger.error("_sdcDSLModem_UseAdminCreds", ex.message);
      this.SetLastError(_constants.SMAPI_EXCEPTION);
    }
  },

  /**
  *  @name SendMsg 
  *  @memberOf $ss.agentcore.smapi.methods
  *  @function
  *  @description   SMAPI and smapi derivations can make use of this function to communicate their current status back to the application.
  * @param    oMsg : Message Type object where msg.type = <type of message> and all other properties (if any) within the object are related to that particular msg type
  *  @returns   GetLastError to confirm success
  */
  SendMsg: function(oMsg) {
    try {
      _logger.info("_sdcDSLModem_SendMsg", "In _sdcDSLModem_SendMsg");
      this.SetLastError(_constants.SMAPI_SUCCESS);
      for (var i = 0; i < this.mArrMsgHandlers.length; i++) {
        var msgs = this.mArrMsgHandlers[i].arMessages.toString();
        if ((msgs.indexOf(_constants.SMAPI_MSG_ALL) != -1) || (msgs.indexOf(oMsg.type) != -1)) {
          // send message to this handler
          try {
            //setTimeout(this.mArrMsgHandlers[i].fnHandler(oMsg), 10);
            this.mArrMsgHandlers[i].fnHandler(oMsg);
            _logger.debug("_sdcDSLModem_SendMsg", "Message Sent To: " + this.mArrMsgHandlers[i].fnHandler.toString().split("(")[0]);
          } catch (ex) {
            _logger.error("_sdcDSLModem_SendMsg", ex.message);
          }
        }
      }
    } catch (ex) {
      _logger.error("_sdcDSLModem_SendMsg", ex.message);
      this.SetLastError(_constants.SMAPI_EXCEPTION);
    }
  }

});

  var _logger = $ss.agentcore.log.GetDefaultLogger("$ss.agentcore.smapi.methods");
  var _utils = $ss.agentcore.utils;
  var _netCheck = $ss.agentcore.network.netcheck;
  var _registry = $ss.agentcore.dal.registry;
  var _config = $ss.agentcore.dal.config;
  var _xml = $ss.agentcore.dal.xml;
  var _constants = $ss.agentcore.constants;
  var _smapiutils = $ss.agentcore.smapi.utils;
  var _smapiMethods = $ss.agentcore.smapi.methods;