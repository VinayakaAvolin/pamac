/** @namespace Holds all functionality related to databags,basically used for storing snapin specific data*/
$ss.agentcore.dal.databag = $ss.agentcore.dal.databag || {};

(function()
{
  $.extend($ss.agentcore.dal.databag,
  {
    
    
    /**
    *  @name SetValue 
    *  @memberOf $ss.agentcore.dal.databag
    *  @function
    *  @description  Sets a value in the databag
    *  @param strName name of the databag entry
    *  @param strValue value of the databag entry
    *  @param bOverwrite Optional,flag to overwriting the databag entry
    *  @returns true if the value is set,else false
    *  @example
    *           var objDatabag = $ss.agentcore.dal.databag;
    *           var ret = objDatabag.SetValue("string","test");
    *           ret = objDatabag.SetValue("number",13);
    *           ret = objDatabag.SetValue("bool",true);
    */
    SetValue : function(strName, strValue,bOverwrite)
    {
			_logger.info('Entered function: $ss.agentcore.dal.databag.SetValue');
      
      if(bOverwrite === undefined)
      {
        bOverwrite = true;
      }
      
      if(strName)
      {
        if(!bOverwrite)
        {
          if(_bag[strName])
          {
            return false;
          }  
        }
        
        try
        {
          // create a new item
          var objItem = new _item();
          objItem.SetItemValue(strValue);
          objItem.SetItemType(_xml.GetTypeForConversion(strValue));
          
          // Add item to bag
          _bag[strName] = objItem;
          _bagSize++;
        
          return true;
        }
        catch(err)
        {
				  _exception.HandleException(err,'$ss.agentcore.dal.databag','SetValue',arguments);
        }
        
      }     
      
      return false;
    },

    /**
    *  @name GetValue 
    *  @memberOf $ss.agentcore.dal.databag
    *  @function
    *  @description  Gets a value from the databag
    *  @param strName Specifies the name of the databag entry
    *  @returns returns the value if found else null
    *  @example
    *         var objDatabag = $ss.agentcore.dal.databag;    
    *         var ret = objDatabag.GetValue("test");
    */
    GetValue : function(strName)
    {
			_logger.info('Entered function: $ss.agentcore.dal.databag.GetValue');

      var objItem = _bag[strName]; 
      var value = null;
      
      if(objItem)
      {
        value = _xml.CastString(objItem.GetItemValue(), objItem.GetItemType());
      }
      
      return value;
    },
    
    /**
    *  @name RemoveValue 
    *  @memberOf $ss.agentcore.dal.databag
    *  @function
    *  @description  Removes a value from the databag
    *  @param strName Specifies the name of the databag entry
    *  @returns true on success,false on failure
    *  @example
    *           var objDatabag = $ss.agentcore.dal.databag;  
    *           objDatabag.SetValue("dummy","test");
    *           var ret = objDatabag.RemoveValue("dummy")
    */
    RemoveValue : function(strName)
    {
			_logger.info('Entered function: $ss.agentcore.dal.databag.RemoveValue');

      var ret = false;
      
      if(this.GetValue(strName))
      {
        delete _bag[strName];    
        ret = true;
        _bagSize--;
      }
      
      return ret;
    },
    
    /**
    *  @name RemoveAll 
    *  @memberOf $ss.agentcore.dal.databag
    *  @function 
    *  @description Remove all the entries from the databag  
    *  @returns none
    *  @example
    *         var objDatabag = $ss.agentcore.dal.databag;  
    *         objDatabag.RemoveAll();
    *         var retSize = objDatabag.GetSize();
    */
    RemoveAll : function()
    {
			_logger.info('Entered function: $ss.agentcore.dal.databag.RemoveAll');

      _bag = {};
      _bagSize = 0;
    },
    
    /**
    *  @name GetSize 
    *  @memberOf $ss.agentcore.dal.databag
    *  @function
    *  @description Gets the count of the databag 
    *  @returns returns the count of entries in the databag
    *  @example
    *           var objDatabag = $ss.agentcore.dal.databag;  
    *           var retSize = objDatabag.GetSize();
    */
    GetSize : function()
    {
			_logger.info('Entered function: $ss.agentcore.dal.databag.GetSize');

        return _bagSize;
    },
    
    /**
    *  @name PersistDatabag 
    *  @memberOf $ss.agentcore.dal.databag
    *  @function
    *  @description  Persists the databag
    *  @param strFilePath optional, path of the databag file <br/>
    *                   if not specified the databag will be saved in the state folder  
    *  @returns true on success,else false
    *  @example
    *          var objDatabag = $ss.agentcore.dal.databag; 
    *          objDatabag.PersistDatabag("c:\\test.txt"); 
    */
    PersistDatabag : function (strFilePath)
    {
			_logger.info('Entered function: $ss.agentcore.dal.databag.PersistDatabag');

      strFilePath = strFilePath || _GetDefaultDatabagFile();
      
      // Get a JSON object
      var objJSON = _ToJSONString();

      // store the JSON object to disk      
      return _file.WriteNewFile(strFilePath,objJSON);
    },
    
    /**
    *  @name RestoreDatabag 
    *  @memberOf $ss.agentcore.dal.databag
    *  @function
    *  @description  Restores the databag from the persistent store and recreates in the memory
    *  @param strFilePath optional,path of the databag file <br/>
    *         if not specified,will try to load it from the state folder(default path)   
    *  @returns none
    *  @example
    *          var objDatabag = $ss.agentcore.dal.databag;
    *          objDatabag.RestoreDatabag("c:\\test.txt"); 
    */
    RestoreDatabag : function(strFilePath)
    {
			_logger.info('Entered function: $ss.agentcore.dal.databag.RestoreDatabag');
     try{
      strFilePath = strFilePath || _GetDefaultDatabagFile();

      // Retrieve data from disk
      // this.RemoveAll();
      var objJSON = _file.ReadFile(strFilePath);

      var obj = _FromJSON(objJSON);    
      
      for(var i in obj)
      {
        var objItem = new _item();
        objItem.SetItemValue(_xml.CastString(obj[i]["itemValue"], obj[i]["itemType"]));
        objItem.SetItemType(obj[i]["itemType"]);
        
        _bag[i] = objItem;
        
        _bagSize++;
      }
      } catch (ex) {
      
      } finally {
      objJSON = null;
      obj = null;
      }

    },

    /**
    *  @name DeleteDatabag 
    *  @memberOf $ss.agentcore.dal.databag
    *  @function
    *  @description Deletes the databag from the persistent store 
    *  @param strFilePath optional,path of the databag file<br/>
    *                     if not specified,will delete the file from the default folder(state folder)
    *  @returns true on success,else false
    *  @example
    *         var objDatabag = $ss.agentcore.dal.databag;
    *         objDatabag.DeleteDatabag("c:\\test.txt");
    */
    DeleteDatabag : function(strFilePath)
    {
			_logger.info('Entered function: $ss.agentcore.dal.databag.DeleteDatabag');

      strFilePath = strFilePath || _GetDefaultDatabagFile();
      return _file.DeleteFile(strFilePath,true);
    }

  });

  var _xml = $ss.agentcore.dal.xml;
  var _file = $ss.agentcore.dal.file;
  var _logger = $ss.agentcore.log.GetDefaultLogger("$ss.agentcore.dal.databag");
  var _exception = $ss.agentcore.exceptions;
  var _bag = {};
  var _bagSize = 0;
  var _item = function()
  {
    var itemValue;
    var itemType;
    
    this.GetItemValue = function()
    {
      return itemValue;
    }
    
    this.SetItemValue = function(val)
    {
      itemValue = val;
    }
  
    this.GetItemType = function()
    {
      return itemType;
    }
    
    this.SetItemType = function(type)
    {
      itemType = type;
    }
    
  };
  
  
  function _ToJSONString()
  {
    var objToPersist = {};

    for(var i in _bag)
    {
       objToPersist[i] = {};
       objToPersist[i].itemValue =  _bag[i].GetItemValue();
       objToPersist[i].itemType = _bag[i].GetItemType();
       //"Key" : {"itemValue" : "value","itemType" : "type"},
    }
    
    var retSting = JSON.stringify(objToPersist);
    objToPersist = null;
    //return "{" + jsonText.replace(/,+$/,'') + "}";
    return retSting;
  }


  function _FromJSON(jsonStr)
  {
    if(!jsonStr)
    {
      return;
    }
    
    return JSON.parse(jsonStr);
  }
  
  function _RecomputeCount()
  {
    _bagSize = 0;
    
    for(var i in _bag)
    {
      _bagSize++;
    }
  }
  
  function _GetDefaultDatabagFile()
  {
    //var path = $ss.agentcore.dal.config.ExpandAllMacros("%CONTENTPATH%");
    var path = $ss.agentcore.dal.config.GetContextValue("SdcContext:DirUserServer")
    //removing trailing //, if any
    path = path.replace(new RegExp(/\\$/),"");
    //path = path.replace(new RegExp(/\\data$/),"");
    if(path)
    {
      
        path = path + "\\state\\databags";
        
        if(!_file.FolderExists(path))
          _file.CreateDir(path);
        var instaceName = $ss.agentcore.dal.ini.GetIniValue("","PROVIDERINFO","instancename","bigbcont");
        path = path + "\\" + instaceName + ".json";

    }
    
    return path;
  }
  
})();




///*******************************************************************************
//**  Name:         ss_db_pvt_OverrideLegacy
//**
//**  Purpose:      Replace the existing API functions within the current scope
//**                with our own.
//**
//**  Parameter:
//**
//**  Return:
//*******************************************************************************/
//function ss_db_pvt_OverrideLegacy( oWinScope )
//{
//  oWinScope = oWinScope || window;
//  oWinScope['sfDatabagSet']         = ss_db_DatabagSet;
//  oWinScope['sfDatabagGet']         = ss_db_DatabagGet;
//  oWinScope['sfDataSet']            = ss_db_DataSet;
//  oWinScope['sfDataGet']            = ss_db_DataGet;
//  oWinScope['sfDataRemoveAll']      = ss_db_DataRemoveAll;
//}


///*******************************************************************************
//**  Name:         ss_db_evt_pvt_SendByName
//**
//**  Purpose:
//**
//**  Parameter:
//**
//**  Return:       
//*******************************************************************************/
//function ss_db_evt_pvt_SendByName(sEvent, sKey, xValue, oBase)
//{
//  try  {
//    oBase = oBase || {};
//    oBase.name  = oBase.name  || sEvent;
//    oBase.key   = oBase.key   || sKey;
//    oBase.value = oBase.value || xValue;
//    ss_evt_Send(oBase);
//  } catch (e) { }
//}

