  /** @namespace Holds all file related functionality*/
  $ss.agentcore.dal.file = $ss.agentcore.dal.file || {};

  (function()
  {
    $.extend($ss.agentcore.dal.file,
    {
      /**
      *  @name CreateTextFile
      *  @memberOf $ss.agentcore.dal.file
      *  @function
      *  @description  Creates a text file in the specified location
      *  @param filename File name
      *  @param bOverwrite If bOverwrite is specified, it will overwrite <br/>
      *                    the file if already exists. Default is false.
      *  @param bUnicode To create a Unicode file, set bUnicode to true.<br/>
      *                  Default value is false. Only UCS 2 and MB files <br/>
      *                  can be created. if Unicode is specified as true,<br/>
      *                  then the file is created as UCS 2 otherwise in MB
      *  @returns TextStream object
      *  @example
      *         var objFile = $ss.agentcore.dal.file;
      *         var ret = objFile.CreateTextFile("e:\\test\\testCreatefile.txt");  
      */
      CreateTextFile : function(filename, bOverwrite, bUnicode)
      {
        _logger.info('Entered function: $ss.agentcore.dal.file.CreateTextFile');
        var oFile;
        var bRet = false;
        
        try 
        {
          //[MAC] Check Machine OS Windows/MAC
          if(!bMac) {
            oFile = _GetFSO().CreateTextFile(filename , bOverwrite, bUnicode);
          
            if(oFile)
            {
              bRet = true;
              oFile.Close();
            }
          }
          else {
            var message = {'JSClassNameKey':'File','JSOperationNameKey':'CreateTextFile', 'JSArgumentsKey':[filename, bOverwrite, bUnicode], 'JSISSyncMethodKey' : '1'}
            var result = jsBridge.execute(message);
            var parsedJSONObject = JSON.parse(result);
            bRet = parsedJSONObject.Data == '' ? false : true;
          }
        } 
        catch(ex) 
        {
          _exception.HandleException(ex,'$ss.agentcore.dal.file','CreateTextFile',arguments);
        }
        return bRet;
      },

      /**
      *  @name CreateUTF8TextFile
      *  @memberOf $ss.agentcore.dal.file
      *  @function
      *  @description Creates a text file in the specified location
      *  @param filename File name
      *  @param bOverwrite If bOverwrite is specified, it will overwrite <br/>
      *                    the file if already exists. Default is false.
      *  @returns Returns TextStream object if succedeed otherwise null
      *  @example
      *         var objFile = $ss.agentcore.dal.file;
      *         var ret = objFile.CreateUTF8TextFile("e:\\test\\testUTF8.txt");  
      */
      CreateUTF8TextFile: function(filename, bOverwrite)
      {
        _logger.info('Entered function: $ss.agentcore.dal.file.CreateUTF8TextFile');
        var oFile;
        var bRet = false;
        try 
        {
          //[MAC] Check Machine OS Windows/MAC
          if(!bMac) {
            oFile = _GetFSO().CreateUTF8TextFile(filename, bOverwrite);
            if(oFile)
            {
              bRet = true;
              oFile.Close();
            }
          }
          else {
            var message = {'JSClassNameKey':'File','JSOperationNameKey':'CreateUTF8TextFile', 'JSArgumentsKey':[filename, bOverwrite], 'JSISSyncMethodKey' : '1'}
            var result = jsBridge.execute(message);
            var parsedJSONObject = JSON.parse(result);
            bRet = parsedJSONObject.Data;
          }
        }
        catch(e)
        {
          _exception.HandleException(e,'$ss.agentcore.dal.file','CreateUTF8TextFile',arguments);
        }

        return bRet;
      },

      /**
      *  @name ReadFile
      *  @memberOf $ss.agentcore.dal.file
      *  @function
      *  @description  Reads entire file contents
      *  @param filename file name
      *  @param callback callback function
      *  @returns Data read, if successful
      *  @example
      *           var objFile = $ss.agentcore.dal.file;
      *           var ret = objFile.ReadFile("e:\\test\\testNew.txt");
      */
      ReadFile :function(filename,callback)
      {
        _logger.info('Entered function: $ss.agentcore.dal.file.ReadFile');
        var sData =null;
        var handle = null;

        try 
        {
          //[MAC] Check Machine OS Windows/MAC
          if(!bMac) {
             handle = _OpenTextFile(filename,_constants.FILE_MODE.READ,false,-1);
             if(handle) {
               sData = handle.ReadAll();
             }
          }
          else {
            var message = "";
            if (callback) {
              message = {'JSClassNameKey':'File','JSOperationNameKey':'ReadFile', 'JSArgumentsKey':[filename], 'JSISSyncMethodKey' : '0', 'JSCallbackMethodNameKey': callback};
            }
            else {
              message = {'JSClassNameKey':'File','JSOperationNameKey':'ReadFile', 'JSArgumentsKey':[filename], 'JSISSyncMethodKey' : '1'};
            }
            var result = jsBridge.execute(message);
            var parsedJSONObject = JSON.parse(result);
            sData = parsedJSONObject.Data;
          }
        }
        catch(ex) {
          _exception.HandleException(ex,'$ss.agentcore.dal.file','ReadFile',arguments);
        }
        if(handle)
        {
          handle.Close();
        }
        return sData ;
      },

      /**
      *  @name WriteFile
      *  @memberOf $ss.agentcore.dal.file
      *  @function
      *  @description  Writes passed data to the file
      *  @param filename file name
      *  @param filedata data to write 
      *  @param mode mode of the file
      *  @param callback callback function
      *  @returns None
      *  @example
      *           var objFile = $ss.agentcore.dal.file;
      *           var _constants = $ss.agentcore.constants;  
      *           var ret = objFile.WriteFile("e:\\test\\testNew.txt","test data",_constants.FILE_MODE.WRITE);
      */
      WriteFile:function(filename,sData,mode,callback)
      {
        _logger.info('Entered function: $ss.agentcore.dal.file.WriteFile');
        var handle = null;
        var bRet = false;
        try 
        {
          //[MAC] Check Machine OS Windows/MAC
          if(!bMac) {
            handle = _OpenTextFile(filename,mode,false,-1);
            if(handle) {
              handle.Write(sData);
              bRet = true;
            }
          }
          else {
            var message = '';
            if (callback != null) {
              message = {'JSClassNameKey':'File','JSOperationNameKey':'WriteFile', 'JSArgumentsKey':[filename,sData,mode], 'JSISSyncMethodKey' : '0', 'JSCallbackMethodNameKey': callback};
            }
            else {
              message = {'JSClassNameKey':'File','JSOperationNameKey':'WriteFile', 'JSArgumentsKey':[filename,sData,mode], 'JSISSyncMethodKey' : '1'}
            }
            var result = jsBridge.execute(message);
            var parsedJSONObject = JSON.parse(result);
            bRet = parsedJSONObject.Data == "1" ? true : false;
          }
        } 
        catch(ex) 
        {
          _exception.HandleException(ex,'$ss.agentcore.dal.file','WriteFile',arguments);
        }

        if(handle)
        {
          handle.Close();
        }

        return bRet;
      },
      

      /**
      *  @name WriteNewFile
      *  @memberOf $ss.agentcore.dal.file
      *  @function
      *  @description  Writes passed data to the file
      *  @param filename file name
      *  @param filedata data to write 
      *  @param callback callback function
      *  @returns true on success,else false
      *  @example
      *           var objFile = $ss.agentcore.dal.file;
      *           var ret = objFile.WriteNewFile("e:\\test\\testNew.txt","test data",_constants.FILE_MODE.WRITE);
      */
      WriteNewFile: function(filename, filedata,callback)
      {
        _logger.info('Entered function: $ss.agentcore.dal.file.WriteNewFile');
        try 
        {
          if(this.CreateTextFile(filename, true, true)) {
            return this.WriteFile(filename,filedata,_constants.FILE_MODE.WRITE,null);
          }
        } 
        catch(ex) {
          _exception.HandleException(ex,'$ss.agentcore.dal.file','WriteNewFile',arguments);
        }
        return false;
      },

      /**
      *  @name DeleteFile
      *  @memberOf $ss.agentcore.dal.file
      *  @function
      *  @description  Delete the specified file
      *  @param filepath path to the file to be deleted
      *  @param bForce flag for force delete
      *  @param callback callback function
      *  @returns true on success false on failure
      *  @example
      *         var objFile = $ss.agentcore.dal.file;
      *         var ret = objFile.DeleteFile("e:\\test\\testdeletefile.txt",true);
      */
      DeleteFile : function (filepath, bForce,callback)
      {
        _logger.info('Entered function: $ss.agentcore.dal.file.DeleteFile');
        var ret = false;
        
        try 
        {
          //[MAC] Check Machine OS Windows/MAC
          if(!bMac) {
            bForce = bForce ? true : false;
            ret = _objContainer.DeleteFile(filepath);
          }
          else {
            //[MAC] Native Call
            var message = {'JSClassNameKey':'File','JSOperationNameKey':'DeleteFile', 'JSArgumentsKey':[filepath,bForce], 'JSISSyncMethodKey' : '1'}
            var result = jsBridge.execute(message);
            var parsedJSONObject = JSON.parse(result);
            ret = parsedJSONObject.Data;
          }
        }
        catch(ex) {
          _exception.HandleException(ex,'$ss.agentcore.dal.file','DeleteFile',arguments);
        }
        return ret;
      },

      /**
      *  @name CopyFile
      *  @memberOf $ss.agentcore.dal.file
      *  @function
      *  @description  Copy the specified file to the specified destination
      *  @param srcPath path to source 
      *  @param dstPath destination path
      *  @param bOverwrite flag to overwrite the file
      *  @param callback callback function
      *  @returns true on success,false on failure
      *  @example
      *         var objFile = $ss.agentcore.dal.file;
      *         var ret = objFile.CopyFile("e:\\per\\del\\testNew.txt","e:\\per\\del\\testNewCopy.txt",true);
      */
      CopyFile : function (srcPath, dstPath, bOverwrite,callback)
      {
        _logger.info('Entered function: $ss.agentcore.dal.file.CopyFile');
        var retVal = false;
        try 
        {
          bOverwrite = bOverwrite ? true:false;
          //[MAC] Check Machine OS Windows/MAC
          if(!bMac)
             retVal = _GetFSO().CopyFile(srcPath, dstPath, bOverwrite);
          else {
            var message = {'JSClassNameKey':'File','JSOperationNameKey':'CopyFile', 'JSArgumentsKey':[srcPath, dstPath, bOverwrite], 'JSISSyncMethodKey' : '1'}
            var result = jsBridge.execute(message);
            var parsedJSONObject = JSON.parse(result);
            retVal = parsedJSONObject.Data;
          }
        }
        catch(ex) {
          _exception.HandleException(ex,'$ss.agentcore.dal.file','CopyFile',arguments);
        }
        return retVal;
      },

      /**
      *  @name MoveFile
      *  @memberOf $ss.agentcore.dal.file
      *  @function
      *  @description  Moves the specified file to the specified destination
      *  @param src source path 
      *  @param dest destination path
      *  @param dwFlags ,Refer $ss.agentcore.constants for more details
      *             MOVEFILE_REPLACE_EXISTING <br/>
      *             MOVEFILE_COPY_ALLOWED<br/>
      *             MOVEFILE_DELAY_UNTIL_REBOOT<br/>
      *             MOVEFILE_WRITE_THROUGH<br/>
      *             MOVEFILE_CREATE_HARDLINK<br/>
      *             MOVEFILE_FAIL_IF_NOT_TRACKABLE
      *  @param callback callback function
      *  @returns true on success false on failure
      *  @example
      *           var objFile = $ss.agentcore.dal.file;
      *           var _constants = $ss.agentcore.constants;    
      *           var ret = objFile.MoveFile("e:\\test\\testNewCopy.txt","e:\\test\\DelDir1\\testNewCopy.txt",_constants.MOVEFILE_REPLACE_EXISTING);
      */
      MoveFile:function (src, dst, dwFlags,callback)
      {
        _logger.info('Entered function: $ss.agentcore.dal.file.MoveFile');
        var ret = false;
        try 
        {
          dwFlags = dwFlags ? dwFlags:2;

          //[MAC] Check Machine OS Windows/MAC
          if(!bMac)
            ret = _objContainer.MoveFile(src, dst, dwFlags);
          else {
            var message = {'JSClassNameKey':'File','JSOperationNameKey':'MoveFile', 'JSArgumentsKey':[src, dst, dwFlags], 'JSISSyncMethodKey' : '1'}
            var result = jsBridge.execute(message);
            var parsedJSONObject = JSON.parse(result);
            ret = parsedJSONObject.Data;
          }
        }
        catch(ex) {
          _exception.HandleException(ex,'$ss.agentcore.dal.file','MoveFile',arguments);
        }
        return ret;
      },

      /**
      *  @name FileExists
      *  @memberOf $ss.agentcore.dal.file
      *  @function
      *  @description  Check whether a file exists or not
      *  @param filename file name
      *  @returns true on success, false on failure
      *  @example
      *         var objFile = $ss.agentcore.dal.file;
      *         var ret = objFile.FileExists("e:\\test\\testNewCopy.txt");
      */
      FileExists : function (filename)
      {
      _logger.info('Entered function: $ss.agentcore.dal.file.FileExists');
        var ret = false;
        try 
        {
          //[MAC] Check Machine OS Windows/MAC
          if(!bMac)
            ret = _GetFSO().FileExists(filename);
          else {
            var message = {'JSClassNameKey':'File','JSOperationNameKey':'GetNativePath', 'JSArgumentsKey':[filename], 'JSISSyncMethodKey' : '1'}
            var result = jsBridge.execute(message);
            var parsedJSONObject = JSON.parse(result);
            filename = parsedJSONObject.Data;
            message = {'JSClassNameKey':'File','JSOperationNameKey':'FileExists', 'JSArgumentsKey':[filename], 'JSISSyncMethodKey' : '1'}
            result = jsBridge.execute(message);
            parsedJSONObject = JSON.parse(result);
            ret = parsedJSONObject.Data;
          }
        }
        catch(ex) {
          _exception.HandleException(ex,'$ss.agentcore.dal.file','FileExists',arguments);
        }
        return ret;
      },

        /**
      *  @name GetMacFileVersion
      *  @memberOf $ss.agentcore.dal.file
      *  @function
      *  @description  Get Mac file version
      *  @param filename file name
      *  @returns true on success, false on failure
      *  @example
      *         var objFile = $ss.agentcore.dal.file;
      *         var ret = objFile.GetMacFileVersion("\Applications\Skype.app");
      */
      GetMacFileVersion : function (filename)
      {
      _logger.info('Entered function: $ss.agentcore.dal.file.GetMacFileVersion');
        var result = "";
        try 
        {
          //[MAC] Check Machine OS Windows/MAC
            var message = {'JSClassNameKey':'File','JSOperationNameKey':'GetFileVersion', 'JSArgumentsKey':[filename], 'JSISSyncMethodKey' : '1'}
            result = jsBridge.execute(message);
        }
        catch(ex) {
          _exception.HandleException(ex,'$ss.agentcore.dal.file','GetMacFileVersion',arguments);
        }
        return result;
      },

      GetNativePath: function(fileName) {
        var message = {'JSClassNameKey':'File','JSOperationNameKey':'GetNativePath', 'JSArgumentsKey':[fileName], 'JSISSyncMethodKey' : '1'}
          var result = jsBridge.execute(message);
          var parsedJSONObject = JSON.parse(result);
          fileName = parsedJSONObject.Data;
          return fileName;
      },

      /**
      *  @name CompressFile
      *  @memberOf $ss.agentcore.dal.file
      *  @function
      *  @description  Compresses a File
      *  @param src path to uncompressed src
      *  @param dest path to compressed dest
      *  @param bUseTempFile If set uses a temp file
      *  @param callback callback function
      *  @returns true on success, false on failure
      *  @example
      *           var objFile = $ss.agentcore.dal.file;
      *           var ret = objFile.CompressFile("e:\\test\\testNew.txt","e:\\test\\testNew.cab",true);
      */
      CompressFile : function (src, dest, bUseTempFile,callback)
      {
        _logger.info('Entered function: $ss.agentcore.dal.file.CompressFile');
        var ret = false;
        try 
        {
          //[MAC] Check Machine OS Windows/MAC
          if(!bMac) 
            ret = _objContainer.CompressFile(src, dest, bUseTempFile);
          else {
            var message = {'JSClassNameKey':'File','JSOperationNameKey':'CompressFile', 'JSArgumentsKey':[src, dest, bUseTempFile], 'JSISSyncMethodKey' : '1'}
            var result = jsBridge.execute(message);
            var parsedJSONObject = JSON.parse(result);
            ret = parsedJSONObject.Data;
          }
        }
        catch(ex) 
        {
          _exception.HandleException(ex,'$ss.agentcore.dal.file','CompressFile',arguments);
        }
        return ret;
      },

      /**
      *  @name Decompress
      *  @memberOf $ss.agentcore.dal.file
      *  @function
      *  @description  Decompress a zip or cab to a destination directory
      *  @param sSrcFile src path to compressed file
      *  @param sDestDir destination directory to extract to
      *  @param nOptions decompress option mask, Refer $ss.agentcore.constants for more details
      *                  NEWOVERWRITE
      *                  USEFOLDERINFO
      *  @param callback callback function
      *  @returns true on success, false on failure
      *  @example
      *           var objFile = $ss.agentcore.dal.file;
      *           var ret = objFile.Decompress("e:\\test\\testNew.cab","e:\\test\\testDir",$ss.agentcore.constants.NEWOVERWRITE);          
      */
      Decompress : function(sSrcFile, sDestDir, nOptions,callback)
      {
        _logger.info('Entered function: $ss.agentcore.dal.file.Decompress');
        var ret = false;
        try 
        {
          //[MAC] Check Machine OS Windows/MAC
          if(!bMac) 
            ret = _objContainer.Decompress(sSrcFile, sDestDir, nOptions);
          else {
            var message = {'JSClassNameKey':'File','JSOperationNameKey':'Decompress', 'JSArgumentsKey':[sSrcFile, sDestDir, nOptions], 'JSISSyncMethodKey' : '1'}
            var result = jsBridge.execute(message);
            var parsedJSONObject = JSON.parse(result);
            ret = parsedJSONObject.Data;
          }
        }
        catch(ex) {
          _exception.HandleException(ex,'$ss.agentcore.dal.file','Decompress',arguments);
        }
        return ret;
      },
      
      /**
      *  @name GetFolder
      *  @memberOf $ss.agentcore.dal.file
      *  @function
      *  @description  Returns an FSO Folder object
      *  @param folderName folder name
      *  @returns object or null
      *  @example
      *          var objFile = $ss.agentcore.dal.file; 
      *          var obj = objFile.GetFolder("e:\\test"); 
      */
      GetFolder:function(folderName) 
      {
        _logger.info('Entered function: $ss.agentcore.dal.file.GetFolder');
        var objFolder;
        try 
        {
          //TD : doing the Exists check here,this needs to be handled properly in the bcont
          if(this.FolderExists(folderName))
          {
            objFolder = _GetFSO().GetFolder(folderName);
          }
        } 
        catch(ex) 
        {
          _exception.HandleException(ex,'$ss.agentcore.dal.file','GetFolder',arguments);
        }
        return objFolder;
      },

      /**
      *  @name CreateDir
      *  @memberOf $ss.agentcore.dal.file
      *  @function
      *  @description  Create a directory
      *  @param dirPath Path with folder name to create
      *  @returns true on success, false on failure
      *  @example
      *         var objFile = $ss.agentcore.dal.file;
      *         var ret = objFile.CreateDir("e:\\test\\dir1");
      */
      CreateDir:function (dirPath)
      {
        _logger.info('Entered function: $ss.agentcore.dal.file.CreateDir');
        var retVal = false;
        try 
        {
          //[MAC] Check Machine OS Windows/MAC
          if(!bMac)
            retVal = _objContainer.CreateDir(dirPath);
          else {
            //=======================================================
            //[MAC] Native Call
            var message = {'JSClassNameKey':'File','JSOperationNameKey':'CreateDir', 'JSArgumentsKey':[dirPath], 'JSISSyncMethodKey' : '1'}
            retVal = jsBridge.execute(message)
            //=======================================================
          }
        }
        catch(ex) {
          _exception.HandleException(ex,'$ss.agentcore.dal.file','CreateDir',arguments);
        }
        return retVal;
      },

      /**
      *  @name DeleteDir
      *  @memberOf $ss.agentcore.dal.file
      *  @function
      *  @description  Deletes a directory
      *  @param dirPath Path with folder name to create
      *  @param bRemoveFiles flag to delete the files in the directory
      *  @param bClobber flag to set a force delete
      *  @param callback callback function
      *  @returns true on success, false on failure
      *  @example
      *           var objFile = $ss.agentcore.dal.file;
      *           var ret = objFile.DeleteDir("e:\\test\\dir1");
      */
      DeleteDir : function (dirPath, bRemoveFiles,bClobber,callback)
      {
        _logger.info('Entered function: $ss.agentcore.dal.file.DeleteDir');
        var retVal = false;
        try 
        {
          bClobber = bClobber ? true : false;
          bRemoveFiles = bRemoveFiles ? true : false;
          //[MAC] Check Machine OS Windows/MAC
          if(!bMac)
            retVal = _objContainer.DeleteDir(dirPath,bRemoveFiles,bClobber);
          else {
            //=======================================================
            //[MAC] Native Call
            var message = {'JSClassNameKey':'File','JSOperationNameKey':'DeleteDir', 'JSArgumentsKey':[dirPath,bRemoveFiles,bClobber], 'JSISSyncMethodKey' : '1'}
            retVal = jsBridge.execute(message)
            //=======================================================
          }
        }
        catch(ex) {
          _exception.HandleException(ex,'$ss.agentcore.dal.file','DeleteDir',arguments);
        }
        return retVal;
      },


      /**
      *  @name FolderExists
      *  @memberOf $ss.agentcore.dal.file
      *  @function
      *  @description  Check whether a folder exists or not
      *  @param foldername Folder name with path
      *  @returns true if the folder exists,else false
      *  @example
      *           var objFile = $ss.agentcore.dal.file;
      *           var ret = objFile.FolderExists("e:\\test\\dir1");  
      */
      FolderExists:function (foldername)
      {
        _logger.info('Entered function: $ss.agentcore.dal.file.FolderExists');
        var retVal = false;
        try 
        {
          //[MAC] Check Machine OS Windows/MAC
          if(!bMac)
             retVal = _GetFSO ().FolderExists(foldername);
          else {
            var message = {'JSClassNameKey':'File','JSOperationNameKey':'FolderExists', 'JSArgumentsKey':[foldername], 'JSISSyncMethodKey' : '1'}
            var result = jsBridge.execute(message);
            var parsedJSONObject = JSON.parse(result);
            retVal = parsedJSONObject.Data;
          }
        } 
        catch(ex) {
          _exception.HandleException(ex,'$ss.agentcore.dal.file','FolderExists',arguments);
        }
        return retVal;
      },


      /**
      *  @name CopyDir
      *  @memberOf $ss.agentcore.dal.file
      *  @function
      *  @description  Copy files from one dir to another
      *  @param        src  path to the source directory
      *  @param        dest  path to the destination directory
      *  @param        options , Refer $ss.agentcore.constants for details  <br/>
      *                   NORMAL_COPY <br/>
      *                   XCOPY
      *  @param callback callback function
      *  @returns true on success, false on failure
      *  @example
      *           var objFile = $ss.agentcore.dal.file;
      *           var _constants = $ss.agentcore.constants;  
      *           var ret = objFile.CopyDir("e:\\test\\DelDir1","e:\\test\\DelDir2",_constants.NORMAL_COPY);  
      */
      CopyDir:function (src, dest, options,callback)
      {
        _logger.info('Entered function: $ss.agentcore.dal.file.CopyDir');
        var retVal = false;
        try 
        {
          //[MAC] Check Machine OS Windows/MAC
          if(!bMac)
            retVal = _objContainer.CopyDir(src, dest, options);
          else {
            //=======================================================
            //[MAC] Native Call
            var message = {'JSClassNameKey':'File','JSOperationNameKey':'CopyDir', 'JSArgumentsKey':[src, dest, options], 'JSISSyncMethodKey' : '1'}
            retVal = jsBridge.execute(message)
            //=======================================================
          }
        } 
        catch(ex) {
          _exception.HandleException(ex,'$ss.agentcore.dal.file','CopyDir',arguments);
        }
        return retVal;
      },

      /**
      *  @name BuildPath
      *  @memberOf $ss.agentcore.dal.file
      *  @function
      *  @description  Build a path (concatenate dir and subdir/file)
      *  @param path path
      *  @param sName subdir or file
      *  @returns full path
      *  @example
      *           var objFile = $ss.agentcore.dal.file;
      *           var ret = objFile.BuildPath("e:\\test\\DelDir1","rootfile.txt");
      */
      BuildPath : function (path, sName)
      {
      _logger.info('Entered function: $ss.agentcore.dal.file.BuildPath');
        var fullpath  = "";
        
        try 
        {
          //[MAC] Check Machine OS Windows/MAC
          if(!bMac)
             fullpath = _GetFSO().BuildPath(path, sName);
          else {
            fullpath = path + '/' + sName;
             var message = {'JSClassNameKey':'File','JSOperationNameKey':'BuildPath', 'JSArgumentsKey':[fullpath], 'JSISSyncMethodKey' : '1'}
            var result = jsBridge.execute(message);
            var parsedJSONObject = JSON.parse(result);
            fullpath = parsedJSONObject.Data;
          }
        } 
        catch(ex) {
          _exception.HandleException(ex,'$ss.agentcore.dal.file','BuildPath',arguments);
        }
        return fullpath;
      },

      /**
      *  @name GetFolderSize
      *  @memberOf $ss.agentcore.dal.file
      *  @function
      *  @description  Retrievs the size of a given folder in bytes.
      *  @param sFolderName specifies the folder name
      *  @returns Folder size in bytes
      *  @example
      *         var objFile = $ss.agentcore.dal.file;
      *         var ret = objFile.GetFolderSize("e:\\testDir");
      */
      GetFolderSize:function (sFolderName) 
      {
        _logger.info('Entered function: $ss.agentcore.dal.file.GetFolderSize');
        var retVal = 0;
        try 
        {
          if(sFolderName.length > 0) 
          {
            //[MAC] Check Machine OS Windows/MAC
            if(!bMac)
              retVal = _objContainer.GetFolderSize(sFolderName);
            else {
              //=======================================================
              //[MAC] Native Call
              var message = {'JSClassNameKey':'File','JSOperationNameKey':'GetFolderSize', 'JSArgumentsKey':[sFolderName], 'JSISSyncMethodKey' : '1'}
              retVal = jsBridge.execute(message)
              //=======================================================
            }
          }
        }
        catch (e) {
          _exception.HandleException(ex,'$ss.agentcore.dal.file','GetFolderSize',arguments);
        }
        return retVal;
      },

      /**
      *  @name ShowBrowseFolderDialog
      *  @memberOf $ss.agentcore.dal.file
      *  @function
      *  @description  Shows Folder Browse dialog and returns selected folder path.
      *  @param        sMsg  message to show on top of the dialog window. Pass <br/>
      *                      an empty string ("") if no message to be shown.<br/>
      *                      Eg Please select a folder
      *  @param        sFolder Folder to pre-select when dialog is shown. Pass an <br/>
      *                empty string("") if none to be pre-selected.
      *  @returns Selected folder path OR empty string when none selected
      *  @example
      *           var objFile = $ss.agentcore.dal.file;
      *           var ret = objFile.ShowBrowseFolderDialog("Test Title","e:\\test");
      */
      ShowBrowseFolderDialog:function (sMsg, sFolder)
      { 
        _logger.info('Entered function: $ss.agentcore.dal.file.ShowBrowseFolderDialog');
        var sPath="";
        try
        {
          //[MAC] Check Machine OS Windows/MAC
          if(!bMac)
            sPath = _objContainer.ShowBrowseFolderDialog(sMsg, sFolder);
          else {
            //=======================================================
            //[MAC] Native Call
            var message = {'JSClassNameKey':'File','JSOperationNameKey':'ShowBrowseFolderDialog', 'JSArgumentsKey':[sMsg, sFolder], 'JSISSyncMethodKey' : '1'}
            sPath = jsBridge.execute(message)
            //=======================================================
          }
        }
        catch (err) {
          _exception.HandleException(err,'$ss.agentcore.dal.file','ShowBrowseFolderDialog',arguments);
        }
        return sPath;
      },

      /**
      *  @name GetFileContents()
      *  @memberOf $ss.agentcore.dal.file
      *  @function
      *  @description  Read a file contents from local or http.
      *  @param sUri specifies the path for remote or local file
      *  @param callback callback function
      *  @returns contents as string, or empty string on failure.
      *  @example
      *           var objFile = $ss.agentcore.dal.file;
      *           var ret = objFile.GetFileContents("e:\\per\\test\\testNewCopy.txt");
      */
      GetFileContents:function (sUri,callback)
      {
        _logger.info('Entered function: $ss.agentcore.dal.file.GetFileContents');
        try
        {
          if (sUri.toLowerCase().indexOf("http")==0)
          {
            var oXmlHttp = _utils.CreateObject("Microsoft.XMLHTTP");
            oXmlHttp.open("GET", sUri, false);
            oXmlHttp.send();
            return oXmlHttp.responseText;
          }
          return this.ReadFile(sUri);
        } catch (err) {
          _exception.HandleException(err,'$ss.agentcore.dal.file','GetFileContents',arguments);
        }
        return "";
      },
      
      /**
      *  @name GetSubFoldersList
      *  @memberOf $ss.agentcore.dal.file
      *  @function
      *  @description  Gets the subfolders under the specified root folder
      *  @param folderName Root folder name
      *  @param pattern Pattern for specific sub folders,if pattern is not passed <br/>
      *                 all the subfolders under the root folder are considered 
      *  @returns array of subfolders
      *  @example
      *           var objFile = $ss.agentcore.dal.file;
      *           // using normal string for filtering the directory list
      *           var ret = objFile.GetSubFoldersList("e:\\test","size");
      *           // using regular expression for filtering the directory list 
      *           ret = objFile.GetSubFoldersList("e:\\test",regex);  
      *
      */
      GetSubFoldersList : function(folderName,pattern)
      {
        _logger.info('Entered function: $ss.agentcore.dal.file.GetSubFoldersList');

        //[MAC] Check Machine OS Windows/MAC
        if(!bMac) {
          var objFolder = this.GetFolder(folderName);
          var arrSubFolders = [];
      
          if(objFolder)
          {
            var regEx = _GetRegEx(pattern);
            var iterator = new Enumerator(objFolder.SubFolders);
        
            try {
              for (;!iterator.atEnd(); iterator.moveNext())
              {
                var folder = iterator.item().name;
                if(regEx) {
                  if(regEx.test(folder)) {
                    arrSubFolders.push(folder);
                  }
                }
                else {
                  arrSubFolders.push(folder);
                }
              }
            }
            catch(ex) {
            _exception.HandleException(ex,'$ss.agentcore.dal.file','GetSubFoldersList',arguments);
            }
          }
        }
        else {
          var message = {'JSClassNameKey':'File','JSOperationNameKey':'GetSubFoldersList', 'JSArgumentsKey':[folderName], 'JSISSyncMethodKey' : '1'}
          var result = jsBridge.execute(message);
          var parsedJSONObject = JSON.parse(result);
          var subFolders = parsedJSONObject.Data;

          var arrSubFolders = [];
        
          if(subFolders) {
            var regEx = _GetRegEx(pattern);
            try
            {
              for (var i1 = 0; i1 < subFolders.length; ++i1) {
                var folder = subFolders[i1];
                if(regEx) {
                  if(regEx.test(folder))
                    arrSubFolders.push(folder);
                }
                else
                  arrSubFolders.push(folder);
              }
            }
            catch(ex)
            {
              _exception.HandleException(ex,'$ss.agentcore.dal.file','GetSubFoldersList',arguments);
            }
          }
          else {
            _logger.info('Entered function: $ss.agentcore.dal.file.GetSubFoldersList----');
          }
        }
        return arrSubFolders;      
      },
      
      /**
      *  @name GetFilesList 
      *  @memberOf $ss.agentcore.dal.file
      *  @function
      *  @description  Gets the files under the specified folder
      *  @param folderName Root folder name
      *  @param pattern Pattern for specific files,if pattern is not passed <br/>
      *                 all the files under the root are considered 
      *  @returns array of files
      *  @example
      *           var objFile = $ss.agentcore.dal.file;
      *           // using normal string for filtering the file list
      *           var ret = objFile.GetFilesList("e:\\test","new");
      *           // using regular expression for filtering the file list
      *           ret = objFile.GetFilesList("e:\\test",regex);
      */
      GetFilesList : function(folderName,pattern)
      {
        _logger.info('Entered function: $ss.agentcore.dal.file.GetFilesList');

        //[MAC] Check Machine OS Windows/MAC
        if(!bMac) {
          var objFolder = this.GetFolder(folderName);
          var arrFiles = [];
        
          if(objFolder)
          {
            try {
              var regEx = _GetRegEx(pattern);
              var iterator = new Enumerator(objFolder.Files);

              for (;!iterator.atEnd(); iterator.moveNext())
              {
                var file = iterator.item().name;
                if(regEx) {
                  if(regEx.test(file))
                    arrFiles.push(file);
                }
                else {
                  arrFiles.push(file);
                }
              }
            }
            catch(ex)
            {
              _exception.HandleException(ex,'$ss.agentcore.dal.file','GetFilesList',arguments);
            }
          }
        }
        else {
          var message = {'JSClassNameKey':'File','JSOperationNameKey':'GetFilesList', 'JSArgumentsKey':[folderName], 'JSISSyncMethodKey' : '1'}
          var result = jsBridge.execute(message);
          var parsedJSONObject = JSON.parse(result);
          var files = parsedJSONObject.Data;
          var arrFiles = [];
          var regEx = _GetRegEx(pattern);

          for(var i = 0 ; i < files.length ; i++){
            var file = files[i];
            if(regEx)
            {
              if(regEx.test(file)) {
                arrFiles.push(file);
              }
            }
            else
            {
              arrFiles.push(file);
            }
          }
        }
        return arrFiles;      
      },

      /**
      *  @name GetFileSystemObject 
      *  @memberOf $ss.agentcore.dal.file
      *  @function
      *  @description  Function to get the internal filesystem object
      *  @returns filesystemobjec
      *  @example
      *           var objFile = $ss.agentcore.dal.file;
      *           var ret = objFile.GetFileSystemObject();
      */
      GetFileSystemObject:function()
      {
        return _GetFSO();
      },
      GetModifiedDate: function(fileName) {
        var retVal = null;
        //[MAC] Check Machine OS Windows/MAC
        if(bMac) {
          _logger.info('Entered function: $ss.agentcore.dal.file.GetModifiedDate');
          var message = {'JSClassNameKey':'File','JSOperationNameKey':'GetModifiedDate', 'JSArgumentsKey':[fileName], 'JSISSyncMethodKey' : '1'}
          var result = jsBridge.execute(message);
          var parsedJSONObject = JSON.parse(result);
          retVal = parsedJSONObject.Data;
        }
        return retVal;
      },
      GetCreatedDate: function(fileName) {
        var retVal = null;
        //[MAC] Check Machine OS Windows/MAC
        if(bMac) {
          _logger.info('Entered function: $ss.agentcore.dal.file.GetCreatedDate');
          var message = {'JSClassNameKey':'File','JSOperationNameKey':'GetCreatedDate', 'JSArgumentsKey':[fileName], 'JSISSyncMethodKey' : '1'}
          var result = jsBridge.execute(message);
          var parsedJSONObject = JSON.parse(result);
          retVal = parsedJSONObject.Data;
        }
        return retVal;
      }
    });
    
  var _objContainer;
  //[MAC] Check Machine OS Windows/MAC
  if(!bMac)
    _objContainer = $ss.agentcore.utils.activex.GetObjInstance();

  var _constants = $ss.agentcore.constants;
  var _logger = $ss.agentcore.log.GetDefaultLogger("$ss.agentcore.dal.file");  
  var _exception = $ss.agentcore.exceptions;
  var _objFSO;
    
    /*******************************************************************************
    ** Name:         ss_con_GetFSO
    **
    ** Purpose:      Get global file system object.
    **
    ** Parameter:    None
    **
    ** Return:       File system object to perform File I/O operations
    *******************************************************************************/
    function _GetFSO() 
    {
       try 
       {
          if (!_objFSO) 
          {
            //[MAC] Check Machine OS Windows/MAC
            if(!bMac)
              _objFSO = _objContainer.CreateInternalObject("filesystem");
            else
              _objFSO = $ss.agentcore.dal.file;
          }
       }
       catch(ex) 
       {
          _exception.HandleException(ex,'$ss.agentcore.dal.file','GetFilesList',arguments);
          //g_Logger.Error("ss_con_SetFSObject()", ex.message);
       }
       return _objFSO;
    }
    
    /*******************************************************************************
    ** Name:         ss_con_OpenTextFile
    **
    ** Purpose:      Opens the specified existing text file and returns a
    **               TextStream object
    **
    ** Parameter:    fn      - File name
    **               mode    - Bitfield. 1 for read access, 2 for write access,
    **                         and 8 for appending. Default is 1 for read access.
    **               bCreate - If boolCreate is specified and true, then the file
    **                         will be created if it does not exist. The default
    **                         value for boolCreate is false.
    **               format  - Format can be -2 to use the systems default format,
    **                         -1 to use Unicode, or 0 to use ASCII. The default
    **                         value for intFormat is 0 for ASCII. The TextStream
    **                         object can read UCS-2, UTF-8, and MB text files
    **
    ** Return:       Returns TextStream object
    *******************************************************************************/
    function _OpenTextFile(filename, mode, bCreate, format)
    {
       var oFile;
       try 
       {
         oFile = _GetFSO().OpenTextFile(filename, mode, bCreate, format);
       } 
       catch(ex) 
       {
          _exception.HandleException(ex,'$ss.agentcore.dal.file','GetFilesList',arguments);
        //g_Logger.Error("ss_con_OpenTextFile()", ex.message);
        sMsg = ex.message;
       }
      return oFile;
    }
    
    function _GetRegEx(pattern)
    {
      var regex;
      
      if(!pattern)
      {
        return null;
      }
      
      if(pattern.constructor  === RegExp)
      {
        regex = pattern;
      }
      else if (pattern.constructor === String)
      {
        regex = new RegExp(".*" + pattern + ".*","i");
      }
      
      return regex;
    }


  //[MAC] Get Machine OS type
  var bMac = $.browser.safari;

  //[MAC] JSBridge object used only for MAC
  try {
    var jsBridge = window.JSBridge;
  }
  catch(e) {
  }

  })();  

  ///*******************************************************************************
  //** Name:         ss_con_pvt_RefFSObject
  //**
  //** Purpose:      Get global file system object. Cache an instance of the 
  //**               file system object.
  //**
  //** Parameter:    None
  //**
  //** Return:       File system object to perform File I/O operations
  //*******************************************************************************/
  ////function ss_con_pvt_RefFSObject()
  ////{
  ////  try {
  ////    var oScope = ss_con_pvt_GetTopScope();
  ////    if (oScope.g_con_FSObject == null) {
  ////      var oInst = ss_con_GetObjInstance();
  ////      oScope.g_con_FSObject = oInst.CreateInternalObject("filesystem");
  ////    }
  ////  } catch(ex) {
  ////    g_Logger.Error("ss_con_SetFSObject()", ex.message);
  ////  }
  ////  return oScope.g_con_FSObject;
  ////}