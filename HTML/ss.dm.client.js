$ss.snapin = $ss.snapin || {};

$ss.snapin.dm = $ss.snapin.dm || {};

$ss.snapin.dm.DMClient = MVC.Class.extend({

init: function() {

},

GetDMState: function(keys, items) {
if (downloadOffers != null && downloadOffers.length > 0) 
  return "1";
return "200";
},

GetMacroValues: function(keys, items) {

},

UICallBack: function(keys, items) {

},

AddDMFile: function(dictionary) {
	downloadOffers.push(dictionary);
},

StartDMJob: function() {
  totalAppsToInstall = downloadOffers.length;
  var downloadLength = downloadOffers.length;
  if ( downloadLength > 0)
  {
    for (var i=0;i<downloadLength;i++)
    {
      var dictionary = downloadOffers[i];
      current_job = dictionary;
      var fileGuuid = dictionary["FILE_GUID"];
      var fileVersion = dictionary["FILE_VERSION"];
      var downloadurl = dictionary["FILE_URL"];
      statusInfo[fileGuuid] = new Object();
      statusInfo[fileGuuid].status = 2; //STATUS_DOWNLOADING
      statusInfo[fileGuuid].fileName = dictionary["FILE_TITLE"];
      statusInfo[fileGuuid].progress = 0;
      statusInfo[fileGuuid].guid = fileGuuid;
      statusInfo[fileGuuid].version = fileVersion;
      var httpobj = new XMLHttpRequest();
      httpobj.open("HEAD",downloadurl,true);
      httpobj.timeout=5000;
      httpobj.send();
      httpobj.onreadystatechange = function(){
      if(httpobj.status != 200){
        if(statusInfo[fileGuuid].status == 2){
          statusInfo[fileGuuid].status = -1;
        }
      }
      };
    }
  }
  this.DownloadAndInstall();
},

DownloadAndInstall: function() {
if (downloadOffers.length > 0 && downloadOffers.length > totalAppsInstalled) {
		var dictionary = downloadOffers[totalAppsInstalled];
		current_job = dictionary;
		var fileGuuid = dictionary["FILE_GUID"];
		var fileVersion = dictionary["FILE_VERSION"];
                var downloadurl = dictionary["FILE_URL"];
		statusInfo[fileGuuid] = new Object();
		statusInfo[fileGuuid].status = 2; //STATUS_DOWNLOADING
		statusInfo[fileGuuid].fileName = dictionary["FILE_TITLE"];
		statusInfo[fileGuuid].progress = 0;
		statusInfo[fileGuuid].guid = fileGuuid;
		statusInfo[fileGuuid].version = fileVersion;
		_StartDownload(this, fileGuuid, fileVersion, "_InstallationStatus");    
	}else {
		//this.CompleteDMJob(null,null);
	}
},

StopDMJob: function(keys, items) {
	var message = {'JSClassNameKey':'DownloadOffers','JSOperationNameKey':'CancelDownload', 'JSArgumentsKey':[statusInfo[current_job["FILE_GUID"]].guid,statusInfo[current_job["FILE_GUID"]].version], 'JSISSyncMethodKey' : '1'};
    jsBridge.execute(message);
},

CompleteDMJob: function(keys, items) {
	downloadOffers = [];
	totalAppsInstalled = 0;
	totalAppsDownloaded = 0;
	totalAppsToInstall = 0;
},

SetMacroValues:  function(keys, items) {

},

SetDMProperties: function(keys, items) {

},

SetToolTipStrings: function(keys, items) {

},

CheckService: function(provider) {
	return "1";
},

CreateDMJob: function(keys, items) {

},
GetJobState: function() {
	var statusMap = new Object();
	var sJobState = undefined;
	if (current_job != null) {
		var status = statusInfo[current_job["FILE_GUID"]].status;
		switch(status) {
			case 0:
			break;

			case 1:
			sJobState = "1";
			break;
			case 2:
			sJobState = "3";
			break;
			case 3:
			sJobState = "4";
			break;
			case 5:
			sJobState = "5";
			break;
			case 6:  //STATUS_INSTALLED
			sJobState = "6";
			break;
			case 10:
			sJobState = "10";
			break;
			case 11:
			sJobState = "11";
			break;
			case  -1:
			sJobState = "2";
			break;
		}
	}
	if (downloadOffers.length == totalAppsInstalled) {
		statusMap["$$JOBSTATUS$$"] = "1";
		statusMap["$$FILESTATE$$"] = "1";
	}else {
		statusMap["$$JOBSTATUS$$"] = sJobState;
		statusMap["$$FILESTATE$$"] = sJobState;
}
	statusMap["$$DOGUID$$"] = statusInfo[current_job["FILE_GUID"]].guid;
	if (status == 2) {
		statusMap["$$DOWNLOADEDDOCOUNT$$"] = totalAppsDownloaded + 1;
	}else{
		statusMap["$$DOWNLOADEDDOCOUNT$$"] = totalAppsDownloaded;
	}
	if (status == 5) {
		statusMap["$$INSTALLEDDOCOUNT$$"] = totalAppsInstalled + 1;
	}
	else {
		statusMap["$$INSTALLEDDOCOUNT$$"] = totalAppsInstalled;
	}
	statusMap["$$TOTALDOCOUNT$$"] = totalAppsToInstall;
	statusMap["$$FILENAME$$"] = statusInfo[current_job["FILE_GUID"]].fileName;

	var progress = parseFloat(statusInfo[current_job["FILE_GUID"]].progress);
	progress = Math.round(progress * 100);
	statusMap["$$PERCENTAGEDLD$$"] = progress;
	return statusMap;
},
InstallationStatus: function(result) {
    var parsedJSONObject = JSON.parse(result);
	var object = parsedJSONObject.Data;
	statusInfo[object.guid].status = object.status; //STATUS_DOWNLOADING
	statusInfo[object.guid].progress = object.progress;
	switch(object.status) {
		case 3:
			// totalAppsDownloaded++;
		break;

		case 6:
		case 11:
			totalAppsDownloaded++;
		 	totalAppsInstalled++;
		 	//downloadOffers.splice(0,1);
		 	this.DownloadAndInstall();
		 break;
	}
}

});
var downloadOffers = [];
var statusInfo = new Object();
var jsBridge = window.JSBridge;
var dm_client = null;
var current_job = null;
var totalAppsToInstall = 0;
var totalAppsInstalled = 0;
var totalAppsDownloaded = 0;

function _StartDownload(scope, guid, version, callback) {
	dm_client = scope;
	var message = {'JSClassNameKey':'DownloadOffers','JSOperationNameKey':'DownloadAndInstall', 'JSArgumentsKey':[guid, version,callback],'JSCallbackMethodNameKey': callback, 'JSISSyncMethodKey' : '0'}
    jsBridge.execute(message);
}

function _InstallationStatus(result) {
	dm_client.InstallationStatus(result);
}