﻿/** @namespace Holds all service related functionality*/
$ss.agentcore.dal.service = $ss.agentcore.dal.service || {};
$ss.agentcore.lockdown = $ss.agentcore.lockdown || {};
(function()
{
  $.extend($ss.agentcore.dal.service,
  {
    /**
     *  @name GetWin32ServiceConfig
     *  @memberOf $ss.agentcore.dal.service
     *  @function
     *  @description Get configuration information of the specified win32 service
     *  @param sSvcName specifies the win32 service name
     *  @returns hash object 
     *  @example 
     *          var objService = $ss.agentcore.dal.service;
     *          var config = objService.GetWin32ServiceConfig("Alerter");
     */
    GetWin32ServiceConfig:function(sSvcName)
    {
			_logger.info('Entered function: $ss.agentcore.dal.service.GetWin32ServiceConfig');
      var configHash = {};
      try
      {
        if(_utils.GetOSGroup() === "NT")
        {
          var config = _objContainer.GetServiceConfig(sSvcName);
          if(typeof(config) == "object")
          {
            configHash["ServiceType"] = config.ServiceType;
            configHash["StartType"] = config.StartType;
            configHash["ErrorControl"] = config.ErrorControl;
            configHash["BinaryPathName"] = config.BinaryPathName;
            configHash["LoadOrderGroup"] = config.LoadOrderGroup;
            configHash["TagId"] = config.TagId;
            configHash["Dependencies"] = config.Dependencies;
            configHash["ServiceStartName"] = config.ServiceStartName;
            configHash["DisplayName"] = config.DisplayName;
            configHash["Description"] = config.Description;
          }
        }
      }
      catch (err)
      {
				_exception.HandleException(err,'$ss.agentcore.dal.service','GetWin32ServiceConfig',arguments);
      }
      return configHash;
    },

    /**
     *  @name GetWin32ServiceStatus
     *  @memberOf $ss.agentcore.dal.service
     *  @function
     *  @description Gets status of the service 
     *  @param sSvcName specifies the service name
     *  @returns hash object
     *  @example 
     *          var objService = $ss.agentcore.dal.service;
     *          var config = objService.GetWin32ServiceStatus("Alerter");
     */
    GetWin32ServiceStatus:function (sSvcName)
    {
			_logger.info('Entered function: $ss.agentcore.dal.service.GetWin32ServiceStatus');
      var statusHash = {};
      try
      {
        if(_utils.GetOSGroup() === "NT")
        {
          var status = _objContainer.GetServiceStatus(sSvcName);
          if(typeof(status) == "object")
          {
            statusHash["ServiceType"] = status.ServiceType;
            statusHash["CurrentState"] = status.CurrentState;
            statusHash["ControlsAccepted"] = status.ControlsAccepted;
            statusHash["Win32ExitCode"] = status.Win32ExitCode;
            statusHash["ServiceSpecificExitCode"] = status.ServiceSpecificExitCode;
            statusHash["CheckPoint"] = status.CheckPoint;
            statusHash["WaitHint"] = status.WaitHint;
          }
        }
      }
      catch (err)
      {
				_exception.HandleException(err,'$ss.agentcore.dal.service','GetWin32ServiceStatus',arguments);
      }
      return statusHash;
    },

    /**
     *  @name StartWin32Service
     *  @memberOf $ss.agentcore.dal.service
     *  @function
     *  @description Starts or stops a service(wzcsvc,wlansvc and supportsoft services) 
     *  @param sSvcName specifies the service name
     *  @param bStart specifies whether the service needs to be started or stopped
     *  @param fnCallback Callback function
     *  @returns boolean returns true on success else false
     *  @example 
     *          var objService = $ss.agentcore.dal.service;
     *          var config = objService.StartWin32Service("Alerter",true);
     */
    StartWin32Service:function (sSvcName, bStart,fnCallback)
    {
			_logger.info('Entered function: $ss.agentcore.dal.service.StartWin32Service');
      try
      {
        if(_utils.GetOSGroup() === "NT")
        {
          var status = this.GetWin32ServiceStatus(sSvcName);
          if(status.CurrentState != "undefined")
          {
            var bNeedAction = (bStart) ?
                              (status.CurrentState != _constants.BCONT_SERVICE_RUNNING) :
                              (status.CurrentState != _constants.BCONT_SERVICE_STOPPED) ;

            if(bNeedAction)
            {
              return _StartWin32Service(sSvcName, bStart);
            }
            else
            {
              return true;  // return true when no action is needed
            }
          }
        }
      }
      catch (err)
      {
				_exception.HandleException(err,'$ss.agentcore.dal.service','StartWin32Service',arguments);
      }
      return false;
    },

    /**
     *  @name ChangeWin32ServiceStartMode
     *  @memberOf $ss.agentcore.dal.service
     *  @function
     *  @description Changes the startup type of a service (wzcsvc,wlansvc and supportsoft services)
     *  @param sSvcName specifies the service name
     *  @param nStartMode Specifies the new start mode
     *  @returns boolean returns true on success
     *  @example 
     *          var objService = $ss.agentcore.dal.service;
     *          var objConstants = $ss.agentcore.constants;
     *          var retVal = objService.ChangeWin32ServiceStartMode("Alerter",objConstants.BCONT_SERVICE_AUTO_START);  
     */
    ChangeWin32ServiceStartMode:function (sSvcName, nStartMode)
    {
			_logger.info('Entered function: $ss.agentcore.dal.service.ChangeWin32ServiceStartMode');
      try
      {
        if(_utils.GetOSGroup() === "NT")
        {
          var config = this.GetWin32ServiceConfig(sSvcName);

          if(config.StartType !== "undefined")
          {
            if(config.StartType === nStartMode)
            {
              return true;
            }
            else
            {
              return _ChangeWin32ServiceStartMode(sSvcName, nStartMode);
            }
          }
        }
      }
      catch (err)
      {
				_exception.HandleException(err,'$ss.agentcore.dal.service','ChangeWin32ServiceStartMode',arguments);
      }
      return false;
    },

    /**
     *  @name GetServiceDependencies
     *  @memberOf $ss.agentcore.dal.service
     *  @function
     *  @description Gets the dependent services of the specified service 
     *  @param sSvcName specifies the service name
     *  @returns boolean returns a list of dependent services
     *  @example 
     *          var objService = $ss.agentcore.dal.service;
     *          var retVal = objService.GetServiceDependencies("Alerter");
     */
    GetServiceDependencies:function (svcName)
    {
			_logger.info('Entered function: $ss.agentcore.dal.service.GetServiceDependencies');
      var svcDependencies = [];
      try
      {
        var svcList = _objContainer.EnumServices(0x00000030).split(",");

        for (var i = 0; i < svcList.length; i++)
        {
          if (_objContainer.GetServiceConfig(svcList[i]).Dependencies.toLowerCase().indexOf(svcName.toLowerCase()) != -1)
          {
            svcDependencies.push(svcList[i]);
          }
        }
      }
      catch(err)
      {
				_exception.HandleException(err,'$ss.agentcore.dal.service','GetServiceDependencies',arguments);
      }
      return svcDependencies;
    }    
  });
  
  var _objContainer = $ss.agentcore.utils.activex.GetObjInstance();
  var _utils = $ss.agentcore.utils;
  var _constants = $ss.agentcore.constants;  
  var _logger = $ss.agentcore.log.GetDefaultLogger("$ss.agentcore.dal.service");  
  var _exception = $ss.agentcore.exceptions;
   
  function _StartWin32Service (sSvcName, bStart)
  {
    var guid = $ss.agentcore.dal.config.GetConfigValue("supportactions","guidService","a32614f1-7aa7-4171-b6a5-f150fbbfb589");
    var oSvc = new $ss.agentcore.supportaction(guid);
    var providerID = $ss.agentcore.dal.config.GetProviderID();
    providerID = providerID.toLowerCase();
    var result = false;
    if(oSvc.cabfile)
    {
      oSvc.SetParameter("svcname", sSvcName);  // svcname only works for "wzcsvc" or "wlansvc", sprtsvc(_providerID) and DHCP Client
      oSvc.SetParameter("providerID", providerID);
      oSvc.SetParameter("op", bStart ? "start" : "stop");
      result = (oSvc.Evaluate() == 0);
    }
    return result;
  }

  function _ChangeWin32ServiceStartMode(sSvcName, nStartMode)
  {
    var guid = $ss.agentcore.dal.config.GetConfigValue("supportactions","guidService","a32614f1-7aa7-4171-b6a5-f150fbbfb589");
    var oSvc = new $ss.agentcore.supportaction(guid);
    var providerID = $ss.agentcore.dal.config.GetProviderID();
    providerID = providerID.toLowerCase()
    var result = false;
    if(oSvc.cabfile)
    {
      oSvc.SetParameter("svcname", sSvcName); // svcname only works for "wzcsvc" or "wlansvc", sprtsvc(_providerID) and DHCP Client
      oSvc.SetParameter("providerID", providerID);
      oSvc.SetParameter("op", "auto");
      result = (oSvc.Evaluate() == 0);
    }
    return result;
  }


})();

