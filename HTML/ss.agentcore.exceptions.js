﻿/** @namespace Holds all exception logging related functionality*/
$ss.agentcore.exceptions = $ss.agentcore.exceptions || {};

(function()
{
  $.extend($ss.agentcore.exceptions,
  {
    
    /**
    *  @name HandleException 
    *  @memberOf $ss.agentcore.exceptions
    *  @function 
    *  @description function to log the exception details  
    *  @param ex exception object
    *  @param moduleName module or namespace name
    *  @param fnName function name
    *  @param fnParams function parameters
    *  @returns none
    *  @example
    *         var objEx = $ss.agentcore.exceptions;
    *         objEx.HandleException(ex,"$ss.agentcore.dal.file","WriteFile",arguments);
    */
    HandleException : function(ex,moduleName,fnName,fnParams)
    {
      try
      {
        fnName = fnName || "Anonymous Function";
        fileName = moduleName || "Unknown Module";
        
        fnParams = fnParams || "";
        fnParams = Array.prototype.slice.call(fnParams);
        fnParams = fnParams.join();

        _StoreException(ex,moduleName,fnName);
        
        var stack = _getStackTrace();
        _logger.error("Exception occured in %1%, %2%{%3%} - \n Code: %4%,Description: %5% ,Name:%6%\n StackTrace: \n %7%",moduleName,fnName,fnParams,ex.number,ex.message,ex.name,stack);            
      }
      catch(ex){}
    },
    
    /**
    *  @name GetLastExceptionDetails 
    *  @memberOf $ss.agentcore.exceptions
    *  @function 
    *  @description Gets the details of the last exception  
    *  @returns an object containing the details of the exception.
    *  @example
    *         var objEx = $ss.agentcore.exceptions;
    *         var exLast = objEx.GetLastExceptionDetails();
    */
    GetLastExceptionDetails : function()
    {
      try
      {
        return this.GetLastExceptionDetails(_lastModule);
      }
      catch(ex){}
      
      return {};
    },
    
    /**
    *  @name GetLastExceptionInModule 
    *  @memberOf $ss.agentcore.exceptions
    *  @function
    *  @description  Gets the details of last exception for a specific module
    *  @param moduleName module name
    *  @returns an object containing the details of the exception
    *  @example
    *         var objEx = $ss.agentcore.exceptions;
    *         var exLast = objEx.GetLastExceptionInModule();
    */
    GetLastExceptionInModule : function(moduleName)
    {
      var exObj = {};
    
      try
      {
        if(_exObjs[moduleName])
        {
          exObj = _exObjs[moduleName];
        }
      }
      catch(ex){}
      
      return exObj; 
    },
    
    /**
    *  @name ClearAllException 
    *  @memberOf $ss.agentcore.exceptions
    *  @function
    *  @description  Clears all the exceptions irrespective of the modules
    *  @returns none
    *  @example
    *         var objEx = $ss.agentcore.exceptions;
    *         objEx.ClearAllException();
    */
    ClearAllException : function()
    {
      try
      {
        _exObjs = {};
      }
      catch(ex){}
    },
    
    /**
    *  @name ClearExceptionsInModule
    *  @memberOf $ss.agentcore.exceptions
    *  @function
    *  @description Clears exception specific to a module 
    *  @param moduleName module name
    *  @returns none
    *  @example
    *         var objEx = $ss.agentcore.exceptions;
    *         objEx.ClearExceptionsInModule("$ss.agentcore.dal.file");
    */
    ClearExceptionsInModule: function(moduleName)
    {
      try
      {
        if(_exObjs[moduleName])
        {
          delete _exObjs[moduleName];
          _exObjs[moduleName] = null;
        }
      }
      catch(ex){}
    }
    
  });
  
  function _StoreException(ex,moduleName,fnName)
  {
  
    var exObj;
    
    if(_exObjs[moduleName])
    {
      exObj = _exObjs[moduleName];
    }
    else
    {
      exObj = {};
    }
    exObj.number = ex.number;
    exObj.message = ex.message;
    exObj.name = ex.name;
    exObj.moduleName = moduleName;
    exObj.functionName = fnName; 
    
    _lastModule = moduleName;
  }
  
  function _getFunctionName(aFunction) 
  {
    var regexpResult = aFunction.toString().match(/function(\s*)(\w*)/);
    if (regexpResult && regexpResult.length >= 2 && regexpResult[2]) {
        return regexpResult[2];
    }
    return 'anonymous';
  }

  function _getStackTrace() 
  {
      var result = '';

      if (typeof(arguments.caller) != 'undefined') { 
          for (var a = arguments.caller; a != null; a = a.caller) {
              result += '> ' + _getFunctionName(a.callee) + '\n';
              if (a.caller == a) {
                  result += '*';
                  break;
              }
          }
      }
      return result;
  }

  var _logger = $ss.agentcore.log.GetDefaultLogger("ss.agentcore.exceptions");
  var _exObjs = {};  
  var _lastModule;
  
})();


    
