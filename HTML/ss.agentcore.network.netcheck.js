/** @namespace Provides all network related functionality*/
$ss.agentcore.network = $ss.agentcore.network || {};
/** @namespace wrapper over functionlity provided in netcheck*/
$ss.agentcore.network.netcheck = $ss.agentcore.network.netcheck || {};

$ss.agentcore.lockdown = $ss.agentcore.lockdown || {};

(function()
{
  $.extend($ss.agentcore.network.netcheck,
  {

    /**
    *  @name ValidNIC
    *  @memberOf $ss.agentcore.network.netcheck
    *  @function
    *  @description  Tests a if a given adapter is valid
    *  @param sAdaptersNameToMatch name of the adapter
    *  @returns true/false
    *  @example
    *           var _netCheck = $ss.agentcore.network.netcheck;
    *           var ret = _netCheck.ValidNIC(nicid);
    */
    ValidNIC:function(sAdaptersNameToMatch)
    {
      _cache["adapters"] = _cache["adapters"] || {};
      _cache["adapters"][sAdaptersNameToMatch] =  _cache["adapters"][sAdaptersNameToMatch] || {};
      if(_cache["adapters"][sAdaptersNameToMatch] .isvalidnic !== undefined) return _cache["adapters"][sAdaptersNameToMatch] .isvalidnic;
      var badMaskRegExp, goodMaskRegExp;
      for (var eachOutMask=0; eachOutMask<_filterOut.length; eachOutMask++)
      {
        badMaskRegExp = new RegExp(_filterOut[eachOutMask], "gi");

        if (sAdaptersNameToMatch.match(badMaskRegExp))
        {
          for (var eachInMask=0; eachInMask<_filterIn.length; eachInMask++ )
          {
            goodMaskRegExp = new RegExp(_filterIn[eachInMask], "gi");

            // OKAY nic by the GOOD list.
            if (sAdaptersNameToMatch.match(goodMaskRegExp)) {
              _cache["adapters"][sAdaptersNameToMatch] .isvalidnic = true;
              return true;
            }
          }
          _cache["adapters"][sAdaptersNameToMatch] .isvalidnic  = false;
          return false;  // filtered out through the bad list.
        }
      }
      _cache["adapters"][sAdaptersNameToMatch] .isvalidnic = true;
      return true; // not matched by the bad list, accepted.
    },

    /**
    *  @name GetEthernetCards
    *  @memberOf $ss.agentcore.network.netcheck
    *  @function
    *  @description  Retrieve the adapters that have been installed
    *  @param axNetCheck (Optional) instance of sdcnetcheck ctl.  If none specified
    *                     function creates an instance and deletes it.
    *  @returns Hash in the format of
                {{E51892A0-A47F-4E1C-AB17-452CDAF2CAC1} : "Intel(R) PRO/100 VE Network Connection"}
    *  @example
    *           var _netCheck = $ss.agentcore.network.netcheck;
    *           var ret = _netCheck.GetEthernetCards(null);
    */
    GetEthernetCards:function (axNetCheck)
    {
      var objReturn = {};
      try
      {
        axNetCheck = _GetNetControl(axNetCheck);
        var arrCards = axNetCheck.GetEthernetCards().split(";");

        for (var i=0; i<arrCards.length; i++)
        {
          var arrCardParts = arrCards[i].split("=");
          if (arrCardParts.length && arrCardParts[1] != undefined)
          {
            objReturn[arrCardParts[0]] = arrCardParts[1];
          }
        }
      }
      catch (err)
      {
      }
      return objReturn;
    },

    /**
    *  @name GetPresentAdapters
    *  @memberOf $ss.agentcore.network.netcheck
    *  @function - Will be Deprecated in the future
    */
    GetPresentAdapters:function(axNetCheck, bFilterVirtual, bFilterBridge, bFilterDisabled, bFilterWireless)
    {
      var xValue = 0;
      xValue = (!bFilterVirtual)? (xValue | 0): (xValue | _constants.FilterTypeVirtual);
      xValue = (!bFilterBridge)? (xValue | 0): (xValue | _constants.FilterTypeBridge);
      xValue = (!bFilterDisabled)? (xValue | 0): (xValue | _constants.FilterTypeDisabled);
      xValue = (!bFilterWireless)? (xValue | 0): (xValue | _constants.FilterTypeWireless);
      xValue = xValue | _constants.FilterTypeFirewire;
      return this.GetFilteredAdapters(axNetCheck, xValue);
    },

    /**
    *  @name GetFilteredAdapters
    *  @memberOf $ss.agentcore.network.netcheck
    *  @function
    *  @description  Retrieve an array of present ethernet adapters on system 
    *  @param axNetCheck (Optional) instance of sdcnetcheck ctl.  If none specified
    *                    function creates an instance and deletes it.
    *  @param xFilterType filter out wireless adapters
    *  @returns Hash in the format of
    *                  {{E51892A0-A47F-4E1C-AB17-452CDAF2CAC1} : "Intel(R) PRO/100 VE Network Connection"}
    *  @example
    *           var _netCheck = $ss.agentcore.network.netcheck;
    *           var _constants = $ss.agentcore.constants;
    *           var ret = _netCheck.GetPresentAdapters(null,  _constants.FilterTypeVirtual | _constants.FilterTypeWireless | _constants.FilterTypeDisabled | _constants.FilterTypeFirewire);
    */
    GetFilteredAdapters:function(axNetCheck, xFilterType)
    {
      var bFilterVirtual = xFilterType & _constants.FilterTypeVirtual;
      var bFilterBridge = xFilterType & _constants.FilterTypeBridge;
      var bFilterWireless = xFilterType & _constants.FilterTypeWireless;
      var bFilterDisabled = xFilterType & _constants.FilterTypeDisabled;
      var bFilterFirewire = xFilterType & _constants.FilterTypeFirewire;

      var presentAdapters = {};
      try
      {
        axNetCheck = _GetNetControl(axNetCheck);
        var sNetDeviceList = axNetCheck.GetNetClassDevices().toString().split(";");
        var allcards = this.GetEthernetCards(axNetCheck);

        for (var i = 0; i < sNetDeviceList.length; i++)
        {
          if (sNetDeviceList[i] && String(sNetDeviceList[i]).indexOf("=") > -1)
          {
            var netdevice = sNetDeviceList[i].split("=");
            var svcName = netdevice[0];
            if (svcName)
            {
              if (allcards[svcName])
              {
                // TODO: test bridge adapters   GetEthernetCards() does NOT return Bridge Adapters and hence commenting out the test for bridgeAdapter. 
                //if (!bFilterBridge || (!this.IsBridgeAdapter(svcName) && !this.IsAdapterInBridge(svcName)))  // filter out bridge  
                if ((!bFilterVirtual || this.ValidNIC(svcName)))  // filter out virtual adapters
                {
                  if (!bFilterWireless || !this.IsAdapterWireless(axNetCheck, svcName))
                  {
                    if (!bFilterDisabled || this.IsAdapterEnabled(axNetCheck, svcName))
                    {
                      if(!bFilterFirewire || !this.IsAdapterFirewire(axNetCheck, svcName))
                      {
                        presentAdapters[svcName] = allcards[svcName];
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
      catch (err)
      {
      }

      return presentAdapters; // a hash by service name of adapters that are present on the machine.
    },

    /**
    *  @name GetAdaptersProperties
    *  @memberOf $ss.agentcore.network.netcheck
    *  @function
    *  @description  Retrieve an array of property arrays
    *  @param axNetCheck (Optional) instance of sdcnetcheck ctl.  If none specified
    *                    function creates an instance and deletes it.
    *  @param hashCardNames object of adapters to get properties on
    *  @param sPropertiesStr comma delimited list of properties to get
    *  @returns Hash
    *  @example
    *           var _netCheck = $ss.agentcore.network.netcheck;
    *           var ret = _netCheck.GetAdaptersProperties(null, wirelessNICs, "DefaultGateway");
    */
    GetAdaptersProperties:function(axNetCheck, hashCardNames, sPropertiesStr)
    {
      var hashAdapterProps = {};
      var propNames = sPropertiesStr.split(",");
      var macAddrMap = {};

      try
      {
        axNetCheck = _GetNetControl(axNetCheck);

        // if on 9X, build a hash of MacAddr -> serviceName
        if (_utils.GetOSGroup() == "9x")
        {
          for (var cardInst in hashCardNames)
          {
            var macAddr = axNetCheck.ndis9xQueryOid(cardInst, _constants.OID_802_3_CURRENT_ADDRESS);
            if (macAddr != "") macAddrMap[macAddr] = cardInst;
          }
        }

        axNetCheck.GetAdaptersInfo("");
        axNetCheck.MoveFirst();
        for (var i=0; i < axNetCheck.GetCount(); i++)
        {
          // check if the current adapter is in the requested list, skip if not
          var svcName = axNetCheck.GetAttribute("AdapterInfo", "ServiceName");

          // In 9x, we don't get the service name from GetAdaptersInfo(), so we have to use the MAC Addr map
          if (_utils.GetOSGroup() == "9x")
          {
            var macAddr = axNetCheck.GetAttribute("AdapterInfo", "PhysicalAddress");
            svcName = macAddrMap[macAddr];
          }

          if (hashCardNames[svcName])
          {
            // build property array
            var propValues = {};
            propValues["DisplayName"] = hashCardNames[svcName];  // init property array w/ display name (from GetEthernetCards);
            for (var j = 0; j < propNames.length; j++)
            {
              var propName = propNames[j];
              if (propName) {
                var propValue = axNetCheck.GetAttribute("AdapterInfo", propName);
                propValues[propName] = propValue;
              }
            }
            hashAdapterProps[svcName] = propValues;
          }
          axNetCheck.MoveNext();
        }

        // now check if any hashCardNames in the requested list weren't included in the axNetCheck.GetAdaptersInfo() call.
        // If not, just copy over the display name to the property array
        for (var card in hashCardNames)
        {
          if (card) {
            if (!hashAdapterProps[card])
            {
              var propValues = {};
              propValues["DisplayName"] = hashCardNames[card];
              hashAdapterProps[card] = propValues;
            }
          }
        }
      }
      catch (err)
      {
      }

      return hashAdapterProps;
    },

    /**
    *  @name GetAdapterProperty
    *  @memberOf $ss.agentcore.network.netcheck
    *  @function
    *  @description  Retrieve specified property for the specified NIC
    *  @param axNetCheck (Optional) instance of sdcnetcheck ctl.  If none specified
    *                    function creates an instance and deletes it.
    *  @param sSvcName adapter to get property on
    *  @param sProperty property to get
    *  @returns string
    *  @example
    *           var _netCheck = $ss.agentcore.network.netcheck;
    *           var ret = _netCheck.GetAdapterProperty(null,sAdapter,"DhcpEnabled");
    */
    GetAdapterProperty:function(axNetCheck, sSvcName, sProperty)
    {
      var hashCardNames = {};
      hashCardNames[sSvcName] = this.GetAdapterDisplayName(null, sSvcName) || true;
      if (sProperty == "DisplayName") { return hashCardNames[sSvcName]; }
      var hashCardProperties = this.GetAdaptersProperties(axNetCheck, hashCardNames, sProperty);
      return hashCardProperties[sSvcName][sProperty];
    },

    /**
    *  @name GetAdapterDisplayName
    *  @memberOf $ss.agentcore.network.netcheck
    *  @function
    *  @description  Retrieve display name for the specified NIC.  This is a specialized
    *               version of GetAdapterProperty.  (GetAdapterProperty wont't
    *               work for the display name
    *  @param axNetCheck (Optional) instance of sdcnetcheck ctl.  If none specified
    *                    function creates an instance and deletes it.
    *  @param sSvcName adapter to get property on
    *  @returns string
    *  @example
    *           var _netCheck = $ss.agentcore.network.netcheck;
    *           var ret = _netCheck.GetAdapterDisplayName(null,sAdapter);
    */
    GetAdapterDisplayName:function(axNetCheck, sSvcName)
    {
      var displayName = sSvcName;
      var hashCardNames = this.GetEthernetCards(axNetCheck);
      if (hashCardNames[sSvcName] != null && hashCardNames[sSvcName] != "")
        displayName = hashCardNames[sSvcName];
      return displayName;
    },

    /**
    *  @name GetConnectionName
    *  @memberOf $ss.agentcore.network.netcheck
    *  @function
    *  @description  Returns the friendly name for adapter connection
    *  @param sSvcName Service Name of adapter (basically id of adapter)
    *  @returns string (e.g, "Wireless Connection 3" or "Local Area Connection 1")
    *  @example
    *           var _netCheck = $ss.agentcore.network.netcheck;
    *           var ret = _netCheck.GetAdapterConnectionName(sAdapter);
    */
    GetAdapterConnectionName:function(sSvcName)
    {
      var connName = "";

      if (_utils.GetOSGroup() == "NT")
      {
        var baseKeyPath = "SYSTEM\\CurrentControlSet\\Control\\Network\\{4D36E972-E325-11CE-BFC1-08002BE10318}\\";
        var keyPath = baseKeyPath + sSvcName + "\\Connection";
        connName = _registry.GetRegValue("HKLM", keyPath, "Name");
      }

      return connName;
    },

    /**
    *  @name GetAdapterNameServer
    *  @memberOf $ss.agentcore.network.netcheck
    *  @function
    *  @description  Retrieve name server (static dns entries) for given adapter
    *  @param sSvcName Service Name of adapter (basically id of adapter)
    *  @returns string
    *  @example
    *           var _netCheck = $ss.agentcore.network.netcheck;
    *           var ret = _netCheck.GetAdapterNameServer(sAdapter);
    */
    GetAdapterNameServer:function(sSvcName)
    {
      return _registry.GetRegValue("HKLM","System\\CurrentControlSet\\Services\\Tcpip\\Parameters\\Interfaces\\" + sSvcName, "NameServer");
    },


    /**
    *  @name IsAdapterWireless
    *  @memberOf $ss.agentcore.network.netcheck
    *  @function
    *  @description  Returns if the adapter is of wireless or not
    *  @param axNetCheck (Optional) instance of sdcnetcheck ctl.  If none specified
    *                    function creates an instance and deletes it.
    *  @param sSvcName Service Name of adapter (basically id of adapter)
    *  @returns boolean
    *  @example
    *           var _netCheck = $ss.agentcore.network.netcheck;
    *           var ret = _netCheck.IsAdapterWireless(null,sAdapter);
    */
    IsAdapterWireless:function(axNetCheck, sSvcName)
    {
       _cache["adapters"] = _cache["adapters"] || {};
      _cache["adapters"][sSvcName] =  _cache["adapters"][sSvcName] || {};
      if(_cache["adapters"][sSvcName] .iswireless !== undefined) return _cache["adapters"][sSvcName] .iswireless;
      
      try
      {
        axNetCheck = _GetNetControl(axNetCheck);

        if (sSvcName)
        {
          // first check for MediaType in registry
          var baseKeyPath = "SYSTEM\\CurrentControlSet\\Control\\Network\\{4D36E972-E325-11CE-BFC1-08002BE10318}\\";
          var keyPath = baseKeyPath + sSvcName + "\\Connection";
          var mediaSubType = _registry.GetRegValue("HKLM", keyPath, "MediaSubType");

          if(mediaSubType != "")
          {
            var status = (mediaSubType == 2);
            _cache["adapters"][sSvcName] .iswireless = status;
            return status;
          }
          else
          {
            var status = (this.GetAdaptersPhysicalMedium(axNetCheck, sSvcName) == _constants.NdisPhysicalMediumWirelessLan);
            _cache["adapters"][sSvcName] .iswireless = status;
            return status;
          }
        }
      }
      catch(err)
      {
      }
      _cache["adapters"][sSvcName] .iswireless =  false;
      return false;
    },

    /**
    *  @name IsAdapterFirewire
    *  @memberOf $ss.agentcore.network.netcheck
    *  @function
    *  @description  Returns if the adapter is of wireless or not
    *  @param axNetCheck (Optional) instance of sdcnetcheck ctl.  If none specified
    *                    function creates an instance and deletes it.
    *  @param sSvcName Service Name of adapter (basically id of adapter)
    *  @returns boolean
    *  @example
    *           var _netCheck = $ss.agentcore.network.netcheck;
    *           var ret = _netCheck.IsAdapterFirewire(null,sAdapter);
    */
    IsAdapterFirewire:function(axNetCheck, sSvcName)
    {
       _cache["adapters"] = _cache["adapters"] || {};
      _cache["adapters"][sSvcName] =  _cache["adapters"][sSvcName] || {};
      if(_cache["adapters"][sSvcName] .isfirewire !== undefined) return _cache["adapters"][sSvcName] .isfirewire;
      
      try
      {
        axNetCheck = _GetNetControl(axNetCheck);

        if (sSvcName)
        {
          // first check for MediaType in registry
          var baseKeyPath = "SYSTEM\\CurrentControlSet\\Control\\Network\\{4D36E972-E325-11CE-BFC1-08002BE10318}\\";
          var keyPath = baseKeyPath + sSvcName + "\\Connection";
          var mediaSubType = _registry.GetRegValue("HKLM", keyPath, "MediaSubType");

          if(mediaSubType != "")
          {
            var status = (mediaSubType == 5);
            _cache["adapters"][sSvcName] .isfirewire= status;
            return status;
          }
          else
          {
            var status = (this.GetAdaptersPhysicalMedium(axNetCheck, sSvcName) == _constants.NdisPhysicalMedium1394);
            _cache["adapters"][sSvcName] .isfirewire = status;
            return status;
          }
        }
      }
      catch(err)
      {
      }
      _cache["adapters"][sSvcName] .isfirewire =  false;
      return false;
    },

    /**
    *  @name IsStaticDNS
    *  @memberOf $ss.agentcore.network.netcheck
    *  @function
    *  @description  Return if given adapter configured for static DNS
    *  @param axNetCheck (Optional) instance of sdcnetcheck ctl.  If none specified
    *                    function creates an instance and deletes it.
    *  @param sSvcName Service Name of adapter (basically id of adapter)
    *  @returns true/false
    *  @example
    *           var _netCheck = $ss.agentcore.network.netcheck;
    *           var ret = _netCheck.IsConfiguredForStaticDNS(null,sAdapter);
    */
    IsConfiguredForStaticDNS:function(axNetCheck, sSvcName)
    {
      if (_utils.GetOSGroup() == "9x")
      {
        return this.IsDnsEnabledForWin9x();
      }
      else
      {
        var nameServer = this.GetAdapterNameServer(sSvcName);
        if (nameServer != null && nameServer!= "") return true;
      }

      return false;
    },

    /**
    *  @name IsTCPIPboundToAdapter
    *  @memberOf $ss.agentcore.network.netcheck
    *  @function
    *  @description  Determines if the given Ethernet adapter is bount to TCP/IP
    *  @param axNetCheck (Optional) instance of sdcnetcheck ctl.  If none specified
    *                    function creates an instance and deletes it.
    *  @param sSvcName Service Name of adapter (basically id of adapter)
    *  @returns true/false
    *  @example
    *           var _netCheck = $ss.agentcore.network.netcheck;
    *           var ret = _netCheck.IsTCPIPboundToAdapter(null,sAdapter);
    */
    IsTCPIPboundToAdapter:function (axNetCheck, sSvcName)
    {
      var result = false;

      try
      {
        if (axNetCheck == null) axNetCheck = _utils.CreateObject("SPRT.SdcNetCheck")
        result = axNetCheck.IsTCPIPBoundToAdapter(sSvcName);
      }
      catch (err)
      {
      }

      return result;
    },

    /**
    *  @name IsComponentBoundToAdapter
    *  @memberOf $ss.agentcore.network.netcheck
    *  @function
    *  @description  Determines if the given Ethernet adapter is bound to the given
    *                component
    *  @param axNetCheck (Optional) instance of sdcnetcheck ctl.  If none specified
    *                     function creates an instance and deletes it.
    *  @param sSvcName Service Name of adapter (basically id of adapter)
    *  @param sComponnet component
    *  @param nType 
    *               0 Client   
    *               1 Service
    *               2 Protocol
    *               3 Adapter
    *  @returns true/false
    *  @example
    *           var _netCheck = $ss.agentcore.network.netcheck;
    *           var ret = _netCheck.IsComponentBoundToAdapter(null,sAdapter,"nt_eacfilt",3)
    */
    IsComponentBoundToAdapter:function (axNetCheck, sSvcName, sComponent, nType)
    {
      var result = false;

      try
      {
        if (axNetCheck == null) axNetCheck = _utils.CreateObject("SPRT.SdcNetCheck")
        result = axNetCheck.IsComponentBound(sComponent, sSvcName, nType);
      }
      catch (err)
      {
      }

      return result;
    },


    /**
    *  @name IsDHCPEnabled
    *  @memberOf $ss.agentcore.network.netcheck
    *  @function
    *  @description  Return if DHCP is enabled on a given adapter
    *  @param axNetCheck (Optional) instance of sdcnetcheck ctl.  If none specified
    *                    function creates an instance and deletes it.
    *  @param sSvcName Service Name of adapter (basically id of adapter)
    *  @returns true/false
    *  @example
    *           var _netCheck = $ss.agentcore.network.netcheck;
    *           var ret = _netCheck.IsDHCPEnabled(null, sAdapter);
    */
    IsDHCPEnabled:function(axNetCheck, sSvcName)
    {
      var result = false;
      try
      {
        if (_utils.GetOSGroup== "9x")
        {
          // The same issues happens for 9x machines, except we don't want to
          // assume true, since if the PC is confgured with a static IP address and DNS
          // the IsAdapterConnected() returns false, which is of course is not the case.
          result = _IsDHCPEnabledInWin9x(axNetCheck);
        }
        else
        {
          // We are an NT based machine, so we can just use the adapter straight up.
          result = (this.GetAdapterProperty(axNetCheck, sSvcName, "DhcpEnabled") == "Yes");
        }
      }
      catch (err)
      {
      }
      return result;
    },

    /**
    *  @name EnableDHCP
    *  @memberOf $ss.agentcore.network.netcheck
    *  @function
    *  @description  Enable DHCP on a given adapter
    *                After calling this, you should call RefreshDHCP() for the new
    *                DHCP to take effect
    *  @param axNetCheck (Optional) instance of sdcnetcheck ctl.  If none specified
    *                    function creates an instance and deletes it.
    *  @param sSvcName Service Name of adapter (basically id of adapter)
    *  @returns true/false
    *  @example
    *           var _netCheck = $ss.agentcore.network.netcheck;
    *           var ret = _netCheck.EnableDHCP(null, sAdapter);
    */
    EnableDHCP:function(axNetCheck, sSvcName)
    {
      var result = false;

      try
      {
        var result = _lockdown.EvaluateLockDownSA(_ControlName, "EnableDHCP", sSvcName);
      }
      catch (err)
      {
      }

      return _lockdown.ConvertStringToBoolean(result);
    },

    /**
    *  @name BindTcpipToAdapter
    *  @memberOf $ss.agentcore.network.netcheck
    *  @function
    *  @description  Binds TcpIP to adapter (or all adapters)
    *  @param axNetCheck (Optional) instance of sdcnetcheck ctl.  If none specified
    *                    function creates an instance and deletes it.
    *  @param sSvcName Service Name of adapter (basically id of adapter). If
    *                  empty string null, then bind all adapters.
    *  @returns true/false
    *  @example
    *           var _netCheck = $ss.agentcore.network.netcheck;
    *           var ret = _netCheck.BindTcpipToAdapter(null, sAdapter);
    */
    BindTcpipToAdapter:function (axNetCheck, sSvcName)
    {
      var result = false;

      try
      {
        var result = _lockdown.EvaluateLockDownSA(_ControlName, "EnableComponent", "ms_tcpip", true, sSvcName);
      }
      catch (err)
      {
      }

      return _lockdown.ConvertStringToBoolean(result);
    },

    /**
    *  @name EnableComponent
    *  @memberOf $ss.agentcore.network.netcheck
    *  @function
    *  @description  Enable component for adapter (or all adapters)
    *  @param axNetCheck (Optional) instance of sdcnetcheck ctl.  If none specified
    *                    function creates an instance and deletes it.
    *  @param sSvcName Service Name of adapter (basically id of adapter). If
    *                  empty string null, then bind all adapters.
    *  @param sComponent component
    *  @param bEnable true/false
    *  @returns true/false
    *  @example
    *           var _netCheck = $ss.agentcore.network.netcheck;
    *           var ret = _netCheck.EnableComponentForAdapter(null, sAdapter, "ms_tcpip6", true);
    */
    EnableComponentForAdapter:function(axNetCheck, sSvcName, sComponent, bEnable)
    {
      var result = false;

      try
      {
       	var result = _lockdown.EvaluateLockDownSA(_ControlName, "EnableComponent", sComponent, bEnable, sSvcName);

      }
      catch (err)
      {
      }

      return _lockdown.ConvertStringToBoolean(result);
    },

    /**
    *  @name EnableAdapter
    *  @memberOf $ss.agentcore.network.netcheck
    *  @function
    *  @description  Enable adapter (or all adapters)
    *  @param axNetCheck (Optional) instance of sdcnetcheck ctl.  If none specified
    *                    function creates an instance and deletes it.
    *  @param sSvcName Service Name of adapter (basically id of adapter).  If
    *                  empty string null, then enable all adapters.
    *  @returns true/false
    *  @example
    *           var _netCheck = $ss.agentcore.network.netcheck;
    *           var ret = _netCheck.EnableAdapter(null,nicID);
    */
    EnableAdapter:function (axNetCheck, sSvcName)
    {
      var result = false;

      try
      {
       	var result = _lockdown.EvaluateLockDownSA(_ControlName, "EnableAdapter", true, sSvcName);
      }
      catch (err)
      {
      }

      return _lockdown.ConvertStringToBoolean(result);
    },


    /**
    *  @name DisableAdapter
    *  @memberOf $ss.agentcore.network.netcheck
    *  @function
    *  @description  Disable adapter (or all adapters)
    *  @param axNetCheck (Optional) instance of sdcnetcheck ctl.  If none specified
    *                    function creates an instance and deletes it.
    *  @param sSvcName Service Name of adapter (basically id of adapter).  If
    *                  empty string null, then disable all adapters.
    *  @returns true/false
    *  @example
    *           var _netCheck = $ss.agentcore.network.netcheck;
    *           var ret = _netCheck.DisableAdapter(null,nicID);
    */
    DisableAdapter:function(axNetCheck, sSvcName)
    {
      var result = false;

      try
      {
        var result = _lockdown.EvaluateLockDownSA(_ControlName, "EnableAdapter", false, sSvcName);
      }
      catch (err)
      {
      }

      return _lockdown.ConvertStringToBoolean(result);
    },

    /**
    *  @name IsAdapterEnabled
    *  @memberOf $ss.agentcore.network.netcheck
    *  @function
    *  @description  Check if specified adapter is enabled.
    *  @param axNetCheck (Optional) instance of sdcnetcheck ctl.  If none specified
    *                    function creates an instance and deletes it.
    *  @param sSvcName Service Name of adapter (basically id of adapter).
    *  @returns true/false
    *  @example
    *           var _netCheck = $ss.agentcore.network.netcheck;
    *           var ret = _netCheck.IsAdapterEnabled(null,nicID);
    */
    IsAdapterEnabled:function(axNetCheck, sSvcName)
    {
      var result = false;

      try
      {
        axNetCheck = _GetNetControl(axNetCheck);
        result = axNetCheck.IsAdapterEnabled(sSvcName);
      }
      catch (err)
      {
      }

      return result;
    },
    
    /**
    *  @name ResolveHostname
    *  @memberOf $ss.agentcore.network.netcheck
    *  @function
    *  @description  Do a DNS lookup to resolve the given hostname to an IP address
    *  @param axNetCheck (Optional) instance of sdcnetcheck ctl.  If none specified
    *                    function creates an instance and deletes it.
    *  @param sHostName DNS name to resolve.
    *  @param nTimeoutMS timeout in milliseconds
    *  @returns string (resolved IP address)
    *  @example
    *           var _netCheck = $ss.agentcore.network.netcheck;
    *           var ret = _netCheck.ResolveHostname(null, "www.google.com", "15000");
    */
    ResolveHostname : function(axNetCheck, sHostName, nTimeoutMS,bUseSA )
    {
      bUseSA = typeof(bUseSA)==="undefined" ? true: bUseSA;
      if(_usaSAForDNSLookUp && bUseSA ) return $ss.agentcore.network.netcheck.ResolveHostnameSA(axNetCheck, sHostName, nTimeoutMS); 
      
      var ipaddr = "";

      // validate params, default to timeout of 1 sec if bad time specified
      if (typeof(sHostName) == 'undefined') return false;
      if (typeof(nTimeoutMS)=='undefined' || nTimeoutMS*1 < 1000) nTimeoutMS = 1000;

      try
      {
        axNetCheck = _GetNetControl(axNetCheck);
        ipaddr = axNetCheck.NameToIP(sHostName, nTimeoutMS );

        // if NameToIP returns -1 due to timeout, set it to empty anyway
        if(ipaddr == "-1") ipaddr = "";
      }
      catch (err)
      {
      }

      return ipaddr;
    },

    ResolveHostnameSA:function (axNetCheck, sHostName,nTimeoutMS)
    {
      var ipaddr = "";

      // validate params, default to timeout of 1 sec if bad time specified
      if (typeof(sHostName) == 'undefined') return false;
      if (typeof(nTimeoutMS)=='undefined' || nTimeoutMS*1 < 1000) nTimeoutMS = 1000;

      try
      {
        ipaddr = _lockdown.EvaluateLockDownSA(_ControlName, "NameToIP", sHostName, nTimeoutMS);
        if(ipaddr !== "") ipaddr = ipaddr[0].trim();
        if(ipaddr === "-1") ipaddr = "";
      }
      catch (err)
      {
       ipaddr = ""; 
      }

      return ipaddr;
    },


    /**
    *  @name IsAdapterConnected
    *  @memberOf $ss.agentcore.network.netcheck
    *  @function
    *  @description  Check if specified adapter is connected
    *  @param axNetCheck (Optional) instance of sdcnetcheck ctl.  If none specified
    *                    function creates an instance and deletes it.
    *  @param sSvcName Service Name of adapter (basically id of adapter).
    *  @returns true / false
    *  @example
    *           var _netCheck = $ss.agentcore.network.netcheck;
    *           var ret = _netCheck.IsAdapterConnected(null,nicId);
    */
    IsAdapterConnected:function (axNetCheck, sSvcName)
    {
      var result = false;

      try
      {
        axNetCheck = _GetNetControl(axNetCheck);
        result = (axNetCheck.GetAdapterStatus(sSvcName) == "Connected");
      }
      catch (err)
      {
      }

      return result;
    },

    /**
    *  @name GetAdapterPhysicalMedium
    *  @memberOf $ss.agentcore.network.netcheck
    *  @function
    *  @description  Return the physical medium that the specified adapter is connected to.
    *  @param axNetCheck (Optional) instance of sdcnetcheck ctl.  If none specified
    *                    function creates an instance and deletes it.
    *  @param sSvcName Service Name of adapter (basically id of adapter).
    *  @returns number (see ss_contants.js for values)
    *  @example
    *
    *
    */
    GetAdaptersPhysicalMedium:function(axNetCheck, sSvcName)
    {
      var result = _constants.NdisPhysicalMediumUnspecified;
      var resultStr = "";

      // TODO: verify what happens in 9x
      // for now, return unspecified for 9x
      if (_utils.GetOSGroup() == "NT")
      {
        try
        {
          axNetCheck = _GetNetControl(axNetCheck);

          if (_utils.GetOSGroup() == "9x")
          {
            resultStr = axNetCheck.ndis9xQueryOid(sSvcName, _constants.OID_GEN_PHYSICAL_MEDIUM);
          }
          else
          {
            resultStr = axNetCheck.ndisQueryGlobalStats(_constants.OID_GEN_PHYSICAL_MEDIUM, sSvcName);
          }

          // return of empty string means that OID_GEN_PHYSICAL_MEDIUM is probably unsupported, so default to unspecified
          if (resultStr != "") result = parseInt(resultStr);
        }
        catch (err)
        {
        }
      }

      return result;
    },

    /**
    *  @name TestConnection
    *  @memberOf $ss.agentcore.network.netcheck
    *  @function
    *  @description  Test a network or Internet connection
    *  @param axNetCheck (Optional) instance of sdcnetcheck ctl.  If none specified
    *                    function creates an instance and deletes it.
    *  @param sAddress url to test
    *  @param nPort port number
    *  @param nTimeout timeout in seconds
    *  @returns true/false
    *  @example
    *           var _netCheck = $ss.agentcore.network.netcheck;
    *           var obj = _netCheck.TestConnection(null, "mail.qa.supportsoft.com", "110", "5000");
    */
    TestConnection:function(axNetCheck, sAddress, nPort, nTimeout,bUseSA)
    {
      bUseSA = typeof(bUseSA)==="undefined" ? true: bUseSA;
      if(_usaSAForSocketTest && bUseSA) return $ss.agentcore.network.netcheck.TestConnectionSA(axNetCheck, sAddress, nPort, nTimeout);

      var result = false;

      try
      {
        sAddress = sAddress.replace(/(\/)|(\\)/ig, ""); //Make sure trailing slash/whack gone.

        axNetCheck = _GetNetControl(axNetCheck);
        if (typeof(nPort) == "undefined" || nPort == 0) port = 80;
        if (typeof(nTimeout) == "undefined") nTimeout = 30000;
        result = axNetCheck.TestConnect(sAddress, nPort, nTimeout);
      }
      catch (err)
      {
      }

      return result;
    },
    
    TestConnectionSA:function(axNetCheck, sAddress, nPort, nTimeout)
    {
      var result = false;
      try
      {
        sAddress = sAddress.replace(/(\/)|(\\)/ig, ""); //Make sure trailing slash/whack gone.

        axNetCheck = _GetNetControl(axNetCheck);
        if (typeof(nPort) == "undefined" || nPort == 0) port = 80;
        if (typeof(nTimeout) == "undefined") nTimeout = 30000;
        //result = axNetCheck.TestConnect(sAddress, nPort, nTimeout);
        result = _lockdown.EvaluateLockDownSA(_ControlName, "TestConnect", sAddress, nPort, nTimeout);
        result=result[0].trim();
        if (result === "1" || result === "-1" || result === "true" || result === true ) {
          result = true;
        } else {
          result = false;
        };
      }
      catch (err)
      {
        result = false;
      }
      return result;
    },

    /**
    *  @name PingTest
    *  @memberOf $ss.agentcore.network.netcheck
    *  @function
    *  @description  Test a network or Internet connection
    *  @param sAddress IP Address or name address (like www.yahoo.com) to be tested
    *  @param axNetCheck (Optional) Instance of sdcnetcheck ctl.  If none
    *         specified function creates an instance and deletes it.
    *         if not specified values from config.xml are picked up for
    *         the following params
    *  @param nTTL (Optional) Time-to-live
    *  @param nTimeoutMS (Optional) Time out in milliseconds
    *  @param nPacketSize (Optional) Packet size
    *  @returns true/false
    *  @example
    *           var _netCheck = $ss.agentcore.network.netcheck;
    *           var ret = _netCheck.ping(siteName);
    */
    Ping:function(sAddress, axNetCheck, nTTL, nTimeoutMS, nPacketSize,bUseSA)
    {
      bUseSA = typeof(bUseSA)==="undefined" ? true: bUseSA;
      if(_usaSAForPingTest && bUseSA) return $ss.agentcore.network.netcheck.PingSA(sAddress, axNetCheck, nTTL, nTimeoutMS, nPacketSize);
      
      var result = false;
      if (typeof(nTTL)=='undefined') nTTL = parseInt(_config.GetConfigValue("ping", "ttl", "128"));
      if (typeof(nTimeoutMS)=='undefined') nTimeoutMS = parseInt(_config.GetConfigValue("ping", "timeout", "5000"));
      if (typeof(nPacketSize)=='undefined') nPacketSize = parseInt(_config.GetConfigValue("ping", "packetsize", "50"));

      sAddress = sAddress.replace(/(\/)|(\\)/ig, ""); //Make sure trailing slash/whack gone.

      try
      {
        axNetCheck = _GetNetControl(axNetCheck);
        result = axNetCheck.PingTest(sAddress, nTTL, nTimeoutMS, nPacketSize);
      }
      catch (err)
      {
      }
      
      return result;
    },

    PingSA:function(sAddress, axNetCheck, nTTL, nTimeoutMS, nPacketSize)
    {
      
      var result = false;
      if (typeof(nTTL)=='undefined') nTTL = parseInt(_config.GetConfigValue("ping", "ttl", "128"));
      if (typeof(nTimeoutMS)=='undefined') nTimeoutMS = parseInt(_config.GetConfigValue("ping", "timeout", "5000"));
      if (typeof(nPacketSize)=='undefined') nPacketSize = parseInt(_config.GetConfigValue("ping", "packetsize", "50"));

      sAddress = sAddress.replace(/(\/)|(\\)/ig, ""); //Make sure trailing slash/whack gone.
       try
      {
        result = _lockdown.EvaluateLockDownSA(_ControlName, "PingTest", sAddress, nTTL, nTimeoutMS, nPacketSize);
        result=result[0].trim();
        if (result === "1" || result === "-1" || result === "true" || result === true ) {
          result = true;
        } else {
          result = false;
        };
      }
      catch (err)
      {
       return false;
      }
      

      return result;
    },
    /**
    *  @name IsUsbAdapterDetected
    *  @memberOf $ss.agentcore.network.netcheck
    *  @function
    *  @description  Determine if any USB Ethernet Adapter is currently present on the machine
    *  @param axNetCheck (Optional) instance of sdcnetcheck ctl.  If none specified
    *                    function creates an instance and deletes it.
    *  @returns true/false
    *  @example
    *
    *
    */
    IsUsbAdapterDetected:function(axNetCheck)
    {
      var bUsbDetected = false;
      try
      {
        axNetCheck = _GetNetControl(axNetCheck);
        var currentUSBAdapters = _usb.GetUsbDeviceList(true);
        var arrUSBAdapters = currentUSBAdapters.toLowerCase().split(";");
        var hashCardList = this.GetPresentAdapters(axNetCheck, true, true);
        for (var sCardID in hashCardList)
        {
          sCardName = hashCardList[sCardID];
          if (_utils.IsMemberOfArray(arrUSBAdapters, sCardName)) {
            bUsbDetected = true;
          }
        }
      }
      catch (err)
      {
      }
      return bUsbDetected;
    },


    /**
    *  @name IsMediaSenseInMeEnabled
    *  @memberOf $ss.agentcore.network.netcheck
    *  @function
    *  @description  Determine if any USB Ethernet Adapter is currently present on the machine
    *  @param sSvcName Service Name of adapter (basically id of adapter).
    *  @returns true/false,this method returns false on all platforms other than WinME
    *  @example
    *
    *
    */
    IsMediaSenseInMeEnabled:function(sSvcName)
    {
      if (_utils.GetOS() == "WINME")
      {
        var key = "System\\CurrentControlSet\\Services\\Class\\NetTrans\\" + sSvcName;
        var nDisable = _registry.GetRegValue("HKLM", key, "DisableDHCPMediaSense");
        if (nDisable == 0) return true;
      }
      return false;
    },

    /**
    *  @name DisableMediaSense
    *  @memberOf $ss.agentcore.network.netcheck
    *  @function
    *  @description  Determine if any USB Ethernet Adapter is currently present on the machine
    *                Requires a reboot to take effect
    *  @param sSvcName Service Name of adapter (basically id of adapter).
    *  @returns true/false,this method returns false on all platforms other than WinME
    *  @example
    *
    *
    */
    DisableMediaSense:function(sSvcName)
    {
      if (_utils.GetOS() == "WINME")
      {
        var key = "System\\CurrentControlSet\\Services\\Class\\NetTrans\\" + sSvcName;
        var nDisable = _registry.GetRegValue("HKLM", key, "DisableDHCPMediaSense");
        if (nDisable == 0)
        {
          _registry.SetRegValueByType("HKLM", key, "DisableDHCPMediaSense", 1, "1");
          return true;
        }
      }
      return false;
    },

    /**
    *  @name GetBridgeGuid
    *  @memberOf $ss.agentcore.network.netcheck
    *  @function
    *  @description  Retrieve the service name representing the bridged adapter
    *  @param axNetCheck (Optional) instance of sdcnetcheck ctl.  If none specified
    *                    function creates an instance and deletes it.
    *  @returns 
    *  @example
    *
    *
    */
    GetBridgeService:function(axNetCheck)
    {
      axNetCheck = _GetNetControl(axNetCheck);
      var sNetDeviceList = axNetCheck.GetNetClassDevices().toString();
      var arrNetDevices = sNetDeviceList.split(";");
      var bridgeGuid = '';

      for (var i = 0, ii = arrNetDevices.length; i < ii; i++)
      {
        var servicename = arrNetDevices[i].split("=");
        if (servicename.length == 1) continue;
        if (servicename[1].toLowerCase().indexOf("mac bridge") > -1)
        {
          bridgeGuid = servicename[0];
          break;
        }
      }
      return bridgeGuid;
    },

    /**
    *  @name IsInBridge
    *  @memberOf $ss.agentcore.network.netcheck
    *  @function
    *  @description  Determines if adapter is part of a bridged connection
    *  @param sSvcName Service Name of adapter (basically id of adapter).
    *  @returns true/false
    *  @example
    *           var _netCheck = $ss.agentcore.network.netcheck;
    *           var ret = _netCheck.IsAdapterInBridge(sSvcName);
    */
    IsAdapterInBridge:function(sSvcName)
    {
      var bridgeInfo   = _GetBridgedInfo();
      return (bridgeInfo && bridgeInfo.bridgedAdapters && bridgeInfo.bridgedAdapters[sSvcName] == true) ? true : false;
    },

    /**
    *  @name IsBridge
    *  @memberOf $ss.agentcore.network.netcheck
    *  @function
    *  @description  Determines if the service name is a bridged adapter guid
    *  @param sSvcName Service Name of adapter (basically id of adapter).
    *  @returns true/false
    *  @example
    *           var _netCheck = $ss.agentcore.network.netcheck;
    *           var ret = _netCheck.IsBridgeAdapter(sSvcName);
    */
    IsBridgeAdapter:function(sSvcName)
    {
      var bridgeInfo   = _GetBridgedInfo();
      return (bridgeInfo && bridgeInfo.bridgedAdapters && bridgeInfo.bridgeGuid.length && bridgeInfo.bridgeGuid == sSvcName) ? true : false;
    },


    /**
    *  @name GetOIDValue
    *  @memberOf $ss.agentcore.network.netcheck
    *  @function
    *  @description  Get a specific OID value by querying the nic driver\
    *  @param axNetCheck (Optional) instance of sdcnetcheck ctl.  If none specified
    *                    function creates an instance and deletes it.
    *  @param sSvcName Service Name of adapter (basically id of adapter)
    *  @param nOid string representation of the OID in question
    *  @returns string
    *  @example
    *           var _netCheck = $ss.agentcore.network.netcheck;
    *           var ret = _netCheck.GetOIDValue(null,sSvcName,$ss.agentcore.constants.OID_802_11_SSID);
    */
    GetOIDValue:function(axNetCheck, sSvcName, nOid)
    {
      var oidValue = "unspecified";
      try
      {
        axNetCheck = _GetNetControl(axNetCheck);
        if (sSvcName)
        {
          if (_utils.GetOSGroup() == "9x")
          {
            resultStr = axNetCheck.ndis9xQueryOid(sSvcName, nOid);
          }
          else
          {
            oidValue = axNetCheck.ndisQueryGlobalStats(nOid, sSvcName);
          }
        }
      }
      catch(err)
      {
      }
      return oidValue;
    },

    /**
    *  @name SetOIDValue
    *  @memberOf $ss.agentcore.network.netcheck
    *  @function
    *  @description  Set a specific OID value by communicating with the nic driver
    *  @param axNetCheck (Optional) instance of sdcnetcheck ctl.  If none specified
    *                     function creates an instance and deletes it.
    *  @param sSvcName Service Name of adapter (basically id of adapter)
    *  @param oid string representation of the OID in question
    *  @param sData data to be set
    *  @returns true/false
    *  @example
    *
    *
    */
    SetOIDValue:function(axNetCheck, sSvcName, nOid, sData)
    {
      var ret = false;

      try
      {
        if (_utils.GetOSGroup() == "9x") return ret;
        axNetCheck = _GetNetControl(axNetCheck);
        if (sSvcName)
        {
          ret = axNetCheck.ndisSetOidValue(nOid, sSvcName, sData);
        }
      }
      catch(err)
      {
      }
      return ret;
    },

    /**
    *  @name IsOIDSupported
    *  @memberOf $ss.agentcore.network.netcheck
    *  @function
    *  @description  Returns if an oid is supported for the adapter
    *  @param axNetCheck (Optional) instance of sdcnetcheck ctl.  If none specified
    *                    function creates an instance and deletes it.
    *  @param sSvcName Service Name of adapter (basically id of adapter)
    *  @param oid string representation of the OID in question
    *  @returns true/false
    *  @example
    *
    *
    */
    IsOIDSupported:function(axNetCheck, sSvcName, nOid)
    {
      try
      {
        axNetCheck = _GetNetControl(axNetCheck);

        var lst = axNetCheck.GetSupportedOIDList(sSvcName);
        var arr = lst.split(";");

        for (var i = 0; i < arr.length ; i++)
        {
          if(nOid == parseInt(arr[i])) return true;
        }
      }
      catch(err)
      {
      }
      return false;
    },


    /**
    *  @name GetPresentNetDeviceList
    *  @memberOf $ss.agentcore.network.netcheck
    *  @function
    *  @description  function to get the list of devices present
    *  @param axNetCheck netcheck object
    *  @param arrDevices device list
    *  @returns returns the present device list
    *  @example
    *           var _netCheck = $ss.agentcore.network.netcheck;
    *           var obj = _netCheck.GetPresentNetDeviceList(null,deviceList);
    */
    GetPresentNetDeviceList:function(axNetCheck, arrDevices)
    {
      var oFoundDevice = {};
      try
      {
        axNetCheck = _GetNetControl(axNetCheck);
        var sNetDeviceList = axNetCheck.GetNetClassDevices().toString();
        var arrNetDevices = sNetDeviceList.split(";");


        for (var i = 0; i < arrNetDevices.length; i++)
        {
          if (arrNetDevices[i]) {
            var netdevice = arrNetDevices[i].split("=");
            var devGuid = netdevice[0];
            var devName = netdevice[1];
            if (devName)
            {
              for (var j = 0; j < arrDevices.length; j++)
              {
                if (devName.toLowerCase() == arrDevices[j].toLowerCase())
                {
                  oFoundDevice[devGuid] = devName;
                }
              }
            }
          }
        }
      }
      catch (ex)
      {
        oFoundDevice = {};
      }
      return oFoundDevice;
    },

    /**
    *  @name FindIfDevicePresent
    *  @memberOf $ss.agentcore.network.netcheck
    *  @function
    *  @description  enumerate all net class devices present on the machine, <br/>
    *                see if any devices specified in "arrDevices" is currently present 
    *  @param axNetCheck netcheck object
    *  @param arrDevices device list
    *  @returns return device or empty string
    *  @example
    *           var _netCheck = $ss.agentcore.network.netcheck;
    *           var obj = _netCheck.FindIfDevicePresent(null,deviceList);
    */
    FindIfDevicePresent:function(axNetCheck, arrDevices)
    {
      var sFoundDevice = "";
      try
      {
        axNetCheck = _GetNetControl(axNetCheck);
        var oFoundDevices = this.GetPresentNetDeviceList(axNetCheck, arrDevices);
        for (var oDev in oFoundDevices)
        {
          sFoundDevice = oFoundDevices[oDev];
          break;   // return the first one found
        }
      }
      catch (ex)
      {
        sFoundDevice = "";
      }
      return sFoundDevice;
    },

    /**
    *  @name CreatePPPoARasEntry
    *  @memberOf $ss.agentcore.network.netcheck
    *  @function
    *  @description  function to create a PPPoA Ras Entry
    *  @param axNetCheck netcheck object
    *  @param sConnectionName connection name
    *  @param sAreaCode area code 
    *  @param sPhone phone number
    *  @param sUser user id
    *  @param sPassword password
    *  @param sAttributes attributes
    *  @param sModem modem info
    *  @returns true/false
    *  @example
    *           var _netCheck = $ss.agentcore.network.netcheck;
    *           var obj = _netCheck.CreatePPPoARasEntry(null, connectionname, "0", "0", username, password, "nodialrules", deviceName);
    */
    CreatePPPoARasEntry:function(axNetCheck, sConnectionName, sAreaCode, sPhone, sUser, sPassword, sAttributes, sModem)
    {
      var bRet = false;
      try
      {
        axNetCheck = _GetNetControl(axNetCheck);
        axNetCheck.CreatePPPoARasEntry(sConnectionName, sAreaCode, sPhone, sUser, sPassword, sAttributes, sModem);
        bRet = true;
      }
      catch(e)
      {
      }
      return bRet;
    },

    /**
    *  @name CreatePPPoERasEntry
    *  @memberOf $ss.agentcore.network.netcheck
    *  @function
    *  @description  function to create a PPPoE Ras Entry
    *  @param axNetCheck netcheck object
    *  @param sConnectionName connection name
    *  @param sAreaCode area code 
    *  @param sPhone phone number
    *  @param sUser user id
    *  @param sPassword password
    *  @param sAttributes attributes
    *  @param sModem modem info
    *  @returns true/false
    *  @example
    *           var _netCheck = $ss.agentcore.network.netcheck;
    *           var obj = _netCheck.CreatePPPoERasEntry(null, connectionname, "0", "0", username, password, "nodialrules");
    */
    CreatePPPoERasEntry:function(axNetCheck, sConnectionName, sAreaCode, sPhone, sUser, sPassword, sAttributes, sModem)
    {
      var bRet = false;
      try
      {
        axNetCheck = _GetNetControl(axNetCheck);
        // pppoe doesn't accept the last param?
        axNetCheck.CreatePPPoERasEntry(sConnectionName, sAreaCode, sPhone, sUser, sPassword, sAttributes, sModem);
        bRet = true;
      }
      catch(e)
      {
      }
      return bRet;
    },

    GetRasConnectStatus:function(axNetCheck, sConnectionName)
    {
      var ret = -1;
      try
      {
        axNetCheck = _GetNetControl(axNetCheck);
        ret = axNetCheck.GetRasConnectStatus(sConnectionName);
      }
      catch(e)
      {
      }
      return ret;
    },

    /**
    *  @name RasConnect
    *  @memberOf $ss.agentcore.network.netcheck
    *  @function
    *  @description  function to establish a RAS connection
    *  @param nRasError error code
    *  @returns true/false
    *  @example
    *           var _netCheck = $ss.agentcore.network.netcheck;
    *           var obj = _netCheck.RasConnect(null,connectionName); 
    */
    RasConnect:function(axNetCheck, sConnectionName)
    {
      var bRet = false;
      try
      {
        axNetCheck = _GetNetControl(axNetCheck);
        bRet = axNetCheck.RasConnect(sConnectionName);
      }
      catch(e)
      {
        return false;
      }
      return bRet;
    },

    /**
    *  @name RasDisconnect
    *  @memberOf $ss.agentcore.network.netcheck
    *  @function
    *  @description  function to disconnect the connection
    *  @param axNetCheck netcheck object
    *  @param sConnectionName connection name
    *  @returns true/false
    *  @example
    *           var _netCheck = $ss.agentcore.network.netcheck;
    *           var obj = _netCheck.RasDisconnect(null,connectionName); 
    */
    RasDisconnect:function(axNetCheck, sConnectionName)
    {
      var bRet = false;
      try
      {
        axNetCheck = _GetNetControl(axNetCheck);
        bRet = axNetCheck.RasDisconnect(sConnectionName);
      }
      catch(e)
      {
        return false;
      }
      return bRet;
    },

    /**
    *  @name IsValidRasError
    *  @memberOf $ss.agentcore.network.netcheck
    *  @function
    *  @description  Checks if the passes error param is a valid RAS Error
    *  @param nRasError error code
    *  @returns true if valid error else false
    *  @example
    *           var _netCheck = $ss.agentcore.network.netcheck;
    *           var obj = _netCheck.IsValidRasError(lastError); 
    */
    IsValidRasError:function(nRasError)
    {
      return (nRasError >= 600 && nRasError <= 801) || (nRasError >=900 && nRasError <= 955);
    },

    /**
    *  @name RasDialEntry
    *  @memberOf $ss.agentcore.network.netcheck
    *  @function
    *  @description  function to dial out the ras entry
    *  @param axNetCheck netcheck control
    *  @param sConnectionName connection name
    *  @param nTimeoutSecs timeout value in seconds
    *  @returns an object with success status(bSuccess) and lastError code(nLastError)
    *  @example
    *           var _netCheck = $ss.agentcore.network.netcheck;
    *           var obj = _netCheck.RasDialEntry(null,connectionName,timeout); 
    */
    RasDialEntry:function(axNetCheck, sConnectionName, nTimeoutSecs)
    {
      var oRet = { bSuccess : false, nLastError: -1 };
      try
      {
        axNetCheck = _GetNetControl(axNetCheck);
        if (this.RasConnect(axNetCheck, sConnectionName))
        {
          oRet = _RasDialEntryMonitor(_RasDialCallBack, axNetCheck, sConnectionName, nTimeoutSecs * 1000, 1000);
        }
        else
        {
          var rasError = axNetCheck.GetLastError();
          if (this.IsValidRasError(rasError))
          {
            oRet.nLastError = rasError;
          }
        }
      }
      catch(e)
      {
      }
      return oRet;
    },

    GetRasEntries:function(axNetCheck, sDeviceName)
    {
      var arrReturn = [];
      var bFilterOnDevice = ((sDeviceName != null) && (sDeviceName != "") && typeof(sDeviceName) != "undefined");

      try
      {
        var sRasEntryList = _GetRasEntries(axNetCheck);
        var arrRasEntries = sRasEntryList.split(",");
        for (var i = 0; i < arrRasEntries.length; i++)
        {
          if (arrRasEntries[i] != "")
          {
            var bInclude = true;
            if (bFilterOnDevice)
            {
              var connDevice = this.GetRasEntryInfo(axNetCheck, arrRasEntries[i], "DeviceName");
              bInclude = (connDevice.toLowerCase() == sDeviceName.toLowerCase());
            }
            if (bInclude)
            {
              arrReturn[arrReturn.length] = arrRasEntries[i];
            }
          }
        }
      }
      catch (e)
      {
        return false;
      }
      return arrReturn;
    },

    DelRasEntry:function(axNetCheck, sPhoneBook, sConnectionName)
    {
      var bRet = false;
      try
      {
        axNetCheck = _GetNetControl(axNetCheck);
        bRet = axNetCheck.DelRasEntry(sPhoneBook, sConnectionName);
      }
      catch(e)
      {
        return false;
      }
      return bRet;
    },

    GetRasEntryInfo:function(axNetCheck, sConnectionName, sField)
    {
      // supported fields are "DeviceType", "DeviceName", "LocalPhoneNumber"
      var sRetValue = "";
      try
      {
        axNetCheck = _GetNetControl(axNetCheck);
        sRetValue = axNetCheck.GetRasEntryInfo(sConnectionName, sField);
      }
      catch(e)
      {
      }
      return sRetValue;
    },

    /**
    *  @name GetActiveRasConnections
    *  @memberOf $ss.agentcore.network.netcheck
    *  @function
    *  @description  Gets active RAS connections
    *  @param axNetCheck netcheck object
    *  @param sDeviceName name of the device
    *  @returns active connection array
    *  @example
    *           var _netCheck = $ss.agentcore.network.netcheck;
    *           var ret = _netCheck.GetActiveRasConnections(null,deviceName); 
    */
    GetActiveRasConnections:function(axNetCheck, sDeviceName)
    {
      var arrReturn = [];
      var bFilterOnDevice = ((sDeviceName != null) && (sDeviceName != "") && typeof(sDeviceName) != "undefined");

      try
      {
        axNetCheck = _GetNetControl(axNetCheck);
        var sRasEntryList = _GetRasEntries(axNetCheck);
        var arrRasEntries = sRasEntryList.split(",");

        // verify if any of the RAS entries are active
        for (var i = 0; i < arrRasEntries.length; i++)
        {
          if (arrRasEntries[i] != "")
          {
            var bInclude = true;
            var nStatus = this.GetRasConnectStatus(axNetCheck, arrRasEntries[i]);

            if (bFilterOnDevice)
            {
              var connDevice = this.GetRasEntryInfo(axNetCheck, arrRasEntries[i], "DeviceName");
              bInclude = (connDevice.toLowerCase() == sDeviceName.toLowerCase());
            }

            if (nStatus == _RASCSConnected&& bInclude)
            {
              // add to active connection array if connected
              arrReturn[arrReturn.length] = arrRasEntries[i];
            }
          }
        }
      }
      catch (e)
      {
      }

      return arrReturn;
    },

    /**
    *  @name IsDnsEnabledForWin9x
    *  @memberOf $ss.agentcore.network.netcheck
    *  @function
    *  @description  Win9x way of determining if DNS is enabled or not
    *  @returns true/false
    *  @example
    *           var _netCheck = $ss.agentcore.network.netcheck;
    *           var ret = _netCheck.IsDnsEnabledForWin9x(); 
    */
    IsDnsEnabledForWin9x:function()
    {
      return (_registry.GetRegValue("HKLM","System\\CurrentControlSet\\Services\\VxD\\MSTCP", "EnableDNS") == "1");
    }

   
  });
  var _cache = {};
  var _utils = $ss.agentcore.utils;
  var _registry = $ss.agentcore.dal.registry;
  var _constants = $ss.agentcore.constants;
  var _config = $ss.agentcore.dal.config;
  var _usb = $ss.agentcore.devices.usb;
  var _netCheck = $ss.agentcore.network.netcheck;
  var _lockdown = $ss.agentcore.lockdown;  
  
  
  var _usaSAForDNSLookUp = (_config.GetConfigValue("shell", "use_sa_for_dns_lookup", "true") === "true");
  var _usaSAForPingTest = (_config.GetConfigValue("shell", "use_sa_for_ping_test", "true") === "true");
  var _usaSAForSocketTest = (_config.GetConfigValue("shell", "use_sa_for_socket_test", "true") === "true");

  var _filterOut = [".*PPTP.*",
    ".*NOC Extranet Access Adapter.*",
    ".*Eacfilt Miniport.*",
    ".*Deterministic Network Enhancer Miniport.*",
    ".*Eacfilt Miniport.*",
    "^$",
    ".*Internet Connection Sharing.*",
    ".*ADSL PPP.*",
    " .*SpeedStream PPP.*",
    ".*TI ADSL.*",
    ".*NTS Enternet P.P.P.o.E.*",
    ".*RAS Async Adapter.*",
    ".*WAN Network Driver.*",
    ".*TeleSystems.*",
    "Speed.*ATM.*",
    ".*SMC IrCC \\(Infrared.*",
    ".*Microsoft PPP over ATM.*",
    ".*VPN.*",
    ".*Infrared.*",
    ".*Remote Access.*",
    ".*Efficient.*P.*",
    ".*WAN Miniport.*",
    ".*Direct Parallel.*",
    "BroadJump Virtual NIC Adapter",
    "BroadJ.*PoE.*",
    ".*Dial-Up Adapter.*",
    ".*Microsoft Virtual Priv.*",
    ".*PSECSHM.*",
    "VMWare.*",
    "^PPP Adapte.*",
    "VM.*$",
    ".*VPN-1.*",
    ".*Nokia Virtual Adapter.*",
    ".*PPP\/SLIP.*"
  ];

  var _filterIn = [".*Efficient Networks.*",
    ".*NTS Enternet.*",
    ".*Network TeleSystems.*",
    ".*Alert on LAN.*"
  ];

  var _RASCSConnected     = 0x2000;
  var _RASCSDisconnected  = 0x2001;

  var _ControlName  = "SPRT.SdcNetCheck";

  

  /*******************************************************************************
  ** Name:         pvt_GetBridgedInfo
  **
  ** Purpose:      Private function that populates and return an object with bridging info
  **               (WinXP and up only)
  **
  ** Parameter:    None
  **
  ** Return:       object
  **
  ** Sample Output: {
  **      bindings :   [
  **       "\Device\{4D91055B-24BE-469B-B99F-0E91B6C5CFE3}",
  **       "\Device\{AD7CEF1E-73BE-4EC9-B6FA-F87B97C35D1D}"
  **      ],
  **      exports :   [
  **       "\Device\Bridge_{4D91055B-24BE-469B-B99F-0E91B6C5CFE3}",
  **       "\Device\Bridge_{AD7CEF1E-73BE-4EC9-B6FA-F87B97C35D1D}"
  **      ],
  **      bridgeGuid :  "{11E529B6-418C-4E3D-BDD1-2328CAE73CAB}",
  **      bridgedAdapters :   {
  **       {4D91055B-24BE-469B-B99F-0E91B6C5CFE3} :   true,
  **       {AD7CEF1E-73BE-4EC9-B6FA-F87B97C35D1D} :   true
  **      }
  **    }
  *******************************************************************************/
  function _GetBridgedInfo()
  {
    var bridgeRoot = ["HKEY_LOCAL_MACHINE", "SYSTEM\\CurrentControlSet\\Services\\Bridge\\Linkage"];

    var binds      = _registry.GetRegValue( bridgeRoot[0] , bridgeRoot[1], "Bind" );
    var exports    = _registry.GetRegValue( bridgeRoot[0] , bridgeRoot[1], "Export" );

    var bridgeExports      = exports ? exports.split("|") : [];
    var bridgeBindings     = binds   ? binds.split("|")   : [];

    haveBridgedAdapter = false;
    var bridgedAdapters    = {};

    if (bridgeExports.length && bridgeBindings.length) {
      haveBridgedAdapter = true;
      for (var i=0,ii=bridgeBindings.length; i<ii; i++) {
        bridgedAdapters["{"+bridgeBindings[i].split("{")[1]] = true;
      }
    }

    var bridgeGuid = _netCheck.GetBridgeService(null);

    bridgeInfo = {
      bindings:        bridgeBindings,
      exports:         bridgeExports,
      bridgeGuid:      bridgeGuid,
      bridgedAdapters: bridgedAdapters
    };

    return bridgeInfo;
  }


  function _GetRasEntries(axNetCheck)
  {
    var sRasEntries = "";
    try
    {
      axNetCheck = _GetNetControl(axNetCheck);
      sRasEntries = axNetCheck.GetRasEntries();
    }
    catch(e)
    {
    }
    return sRasEntries;
  }

  function _RasDialEntryMonitor(fnTest, axNetCheck, connectionname, maxWait, intervalWait)
  {
    var oTestResult = { bTestDone : false, bSuccess : false, nLastError: -1 };
    var startTime = (new Date()).valueOf();
    var currTime = startTime;

    do
    {
      oTestResult = fnTest(axNetCheck, connectionname);
      if (oTestResult.bTestDone)
      {
        break;
      }
      _utils.Sleep(intervalWait);
      currTime = (new Date()).valueOf();
    }
    while ((currTime - startTime) < maxWait);

    return { bSuccess : oTestResult.bSuccess , nLastError : oTestResult.nLastError };
  }

  // returns object with the following properties
  // bTestDone - Indicate if operation timed out
  // bSuccess - Indicate if connection success or failed with an error
  // nLastError - Set to RAS Error code if bSuccess is false
  function _RasDialCallBack(axNetCheck, connectionname)
  {
    var oRet = { bTestDone : false, bSuccess : false, nLastError: -1 };

    var rasConnectStatus = _netCheck.GetRasConnectStatus(axNetCheck, connectionname);

    if (rasConnectStatus == _RASCSConnected|| rasConnectStatus == _RASCSDisconnected)
    {
      oRet.bTestDone = true;
      oRet.bSuccess = true;
    }

    if (rasConnectStatus == -1)
    {
      var rasError = axNetCheck.GetLastError();

      // continue to wait until there's a valid RAS error
      if (_netCheck.IsValidRasError(rasError))
      {
        oRet.bTestDone = true;
        oRet.bSuccess = false;
        oRet.nLastError = rasError;
      }
    }
    return oRet;
  }

/*******************************************************************************
 **
 ** Name: pvt_GetNetControl
 **
 ** Purpose: Encapsulates the same check for null and creation of the sdcnetcheck control.
 **          An attempt to try and utilize different OO principles (i.e. reuse),
 **          eventhough JS is not truly OO.
 **
 ** Parameter:    axNetCheck - (Optional) instance of sdcnetcheck ctl.  If none specified
 **                        function creates an instance and deletes it.
 **
 ** Return: An instance of the ActiveX object Sprt.SdcNetCheck
 **
 *******************************************************************************/
  function _GetNetControl(axNetCheck)
  {
    return (axNetCheck == null) ? _utils.CreateObject("SPRT.SdcNetCheck") : axNetCheck;
  }

  /*******************************************************************************
   **
   ** Name: pvt_IsDHCPEnabled9x
   **
   ** Purpose: Used by IsDHCPEnabled if the OS is part of the 9x group.
   **
   ** Parameter:    axNetCheck - (Optional) instance of sdcnetcheck ctl.  If none specified
   **                        function creates an instance and deletes it.
   **
   ** Return: true if DHCP is enabled, false if not
   **
   *******************************************************************************/
  function _IsDHCPEnabledInWin9x(axNetCheck)
  {
    _GetNetControl(axNetCheck);
    var result = false;
    try
    {
      axNetCheck.GetAdaptersInfo("");
      axNetCheck.MoveFirst();
      for (var i=0; i<axNetCheck.GetCount(); i++)
      {
        if (axNetCheck.GetAttribute("AdapterInfo", "DhcpEnabled") == "Yes")
          result = true;
      }
      axNetCheck.MoveNext();
      }
    catch (err)
    {
      result = false;
    }
    return result;
  }
})()












