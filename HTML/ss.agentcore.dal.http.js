﻿/** @namespace Holds all HTTP related functionality*/
$ss.agentcore.dal.http = $ss.agentcore.dal.http  || {};

(function()
{
  $.extend($ss.agentcore.dal.http,
  {
    /**
    *  @name HTTPGetData
    *  @memberOf $ss.agentcore.dal.http
    *  @function
    *  @description  Make an authenticated HTTP "GET" Request and return contents of response
    *  @param sUrl remote URL to make a HTTP request on
    *  @param nTimeout timeout period in milliseconds
    *  @param user optional user parameter for authenticated request
    *  @param password optional password parameter for authenticated request
    *  @returns string  contents returned from GET request
    *  @example
    *         var objHttp = $ss.agentcore.dal.http;
    *         var retData = objHttp.HttpGetData("http://testserver/test.xml",3000);
    */
    HttpGetData:function(sURL, nTimeout, user, password)
    {
      _logger.info('Entered function: $ss.agentcore.dal.http.HttpGetData');
      var tempFile = _config.ExpandSysMacro("%TEMP%client_ui_http_temp.htm");
      var bResult = false;
      var data = "";

      try {
        //[MAC] Check Machine OS Windows/MAC
        if(!bMac)
          bResult = _objContainer.HttpRequest(_constants.BCONT_REQUEST_GETFILE, tempFile, sURL, nTimeout, user, password);
        else {
          //=======================================================
          //[MAC] Native Call
          var message = {'JSClassNameKey':'HTTP','JSOperationNameKey':'HttpRequest', 'JSArgumentsKey':[_constants.BCONT_REQUEST_GETFILE, tempFile, sURL, nTimeout, user, password], 'JSISSyncMethodKey' : '1'}
          bResult = jsBridge.execute(message)
          //=======================================================
        }
      } 
      catch (err) 
      {
        _exception.HandleException(err,'$ss.agentcore.dal.http','HttpGetData',arguments);
      }

      if (bResult)
      {
        data = _file.ReadFile(tempFile);
      }

      _file.DeleteFile(tempFile);
      return data;
    },

    /**
    *  @name GetFile
    *  @memberOf $ss.agentcore.dal.http
    *  @function
    *  @description  Make an authenticated HTTP "GET" Request and return the file specified
    *  @param path path to the local destination file
    *  @param url remote source file that needs to be retrieved
    *  @param nTimeout timeout period in milliseconds
    *  @param user optional username for authentication
    *  @param pwd optional password for authentication
    *  @returns true on success,false on failure
    *  @example
    *         var objHttp = $ss.agentcore.dal.http;
    *         var ret = objHttp.GetFile("C:\\http_temp.xml","http://testServer/test.xml",3000));
    */
    GetFile : function(path, url, nTimeout, user, pwd)
    {
      _logger.info('Entered function: $ss.agentcore.dal.http.HttpGetFile');
      var rVal = false;

      try 
      {
        //[MAC] Check Machine OS Windows/MAC
        if(!bMac)
          rVal = _objContainer.HttpRequest(_constants.BCONT_REQUEST_GETFILE, path, url, nTimeout, user, pwd);
        else {
          //=======================================================
          //[MAC] Native Call
          var message = {'JSClassNameKey':'HTTP','JSOperationNameKey':'HttpRequest', 'JSArgumentsKey':[_constants.BCONT_REQUEST_GETFILE, path, url, nTimeout, user, pwd], 'JSISSyncMethodKey' : '1'}
          rVal = jsBridge.execute(message)
          //=======================================================
        }
      } 
      catch (err) {
        _exception.HandleException(err,'$ss.agentcore.dal.http','HttpGetFile',arguments);
      }

      return rVal;
    },

    /**
    *  @name HttpGetFileIfNewer
    *  @memberOf $ss.agentcore.dal.http
    *  @function
    *  @description  Make an authenticated HTTP "GET" Request and return the file if newer than the specified file
    *  @param path path to the local destination file
    *  @param url remote source file that needs to be retrieved
    *  @param nTimeout timeout period in milliseconds
    *  @param user optional username for authentication
    *  @param pwd optional password for authentication
    *  @returns true on success,false on failure
    *  @example
    *         var objHttp = $ss.agentcore.dal.http;
    *         var ret = objHttp.HttpGetFileIfNewer("C:\\http_temp.xml","http://testServer/test.xml",3000);
    */
    HttpGetFileIfNewer:function(path, url, nTimeout, user, pwd)
    {
      _logger.info('Entered function: $ss.agentcore.dal.http.HttpGetFileIfNewer');
      var rVal = false;

      try 
      {
        //[MAC] Check Machine OS Windows/MAC
        if(!bMac)
          rVal = _objContainer.HttpRequest(_constants.BCONT_REQUEST_GETFILEIFNEWER, path, url, nTimeout, user, pwd);
        else {
          //=======================================================
          //[MAC] Native Call
          var message = {'JSClassNameKey':'HTTP','JSOperationNameKey':'HttpRequest', 'JSArgumentsKey':[_constants.BCONT_REQUEST_GETFILEIFNEWER, path, url, nTimeout, user, pwd], 'JSISSyncMethodKey' : '1'}
          rVal = jsBridge.execute(message)
          //=======================================================
        }
      } 
      catch (err) 
      {
        _exception.HandleException(err,'$ss.agentcore.dal.http','HttpGetFileIfNewer',arguments);
      }

      return rVal;
    },

    /**
    *  @name HttpPutFile
    *  @memberOf $ss.agentcore.dal.http
    *  @function
    *  @description  Make an authenticated HTTP "PUT" Request and upload the specified file
    *  @param path path to the local source file
    *  @param url remote destination file that file will be put
    *  @param nTimeout timeout period in milliseconds
    *  @param user optional username for authentication
    *  @param pwd optional password for authentication
    *  @returns true on success ,false on failure
    *  @example
    *         var objHttp = $ss.agentcore.dal.http;
    *         var ret = objHttp.HttpPutFile("C:\\test.txt","http://testServer/test.txt",3000)
    */
    HttpPutFile:function(path, url, nTimeout, user, pwd)
    {
      _logger.info('Entered function: $ss.agentcore.dal.http.HttpPutFile');
      var rVal = false;

      try {
        //[MAC] Check Machine OS Windows/MAC
        if(!bMac)
          rVal = _objContainer.HttpRequest(_constants.BCONT_REQUEST_PUTFILE, path, url, nTimeout, user, pwd);
        else {
          //=======================================================
          //[MAC] Native Call
          var message = {'JSClassNameKey':'HTTP','JSOperationNameKey':'HttpRequest', 'JSArgumentsKey':[_constants.BCONT_REQUEST_PUTFILE, path, url, nTimeout, user, pwd], 'JSISSyncMethodKey' : '1'}
          rVal = jsBridge.execute(message)
          //=======================================================
        }
      } 
      catch (err) {
        _exception.HandleException(err,'$ss.agentcore.dal.http','HttpPutFile',arguments);
      }

      return rVal;
    },

    /**
    *  @name HttpPost
    *  @memberOf $ss.agentcore.dal.http
    *  @function
    *  @description  Make an authenticated HTTP "POST" Request ; post the specified local file
    *  @param path path to the local source file
    *  @param url remote destination file that file will be posted to
    *  @param nTimeout timeout period in milliseconds
    *  @param user optional username for authentication
    *  @param pwd optional password for authentication
    *  @returns true on success false on failure
    *  @example
    *          var objHttp = $ss.agentcore.dal.http;
    *          var ret = objHttp.HttpPost("C:\\test.txt","http://localhost/test.txt",3000);
    */
    HttpPost:function (path, url, nTimeout, user, pwd)
    {
      _logger.info('Entered function: $ss.agentcore.dal.http.HttpPost');
      var rVal = false;

      try 
      {
        //[MAC] Check Machine OS Windows/MAC
        if(!bMac)
          rVal = _objContainer.HttpRequest(_constants.BCONT_REQUEST_POSTFILE, path, url, nTimeout, user, pwd);
        else {
          //=======================================================
          //[MAC] Native Call
          var message = {'JSClassNameKey':'HTTP','JSOperationNameKey':'HttpRequest', 'JSArgumentsKey':[_constants.BCONT_REQUEST_POSTFILE, path, url, nTimeout, user, pwd], 'JSISSyncMethodKey' : '1'}
          rVal = jsBridge.execute(message)
          //=======================================================
        }
      } 
      catch (err) 
      {
        _exception.HandleException(err,'$ss.agentcore.dal.http','HttpPost',arguments);
      }

      return rVal;
    },

    /**
    *  @name HttpPostUsingPasswordControl
    *  @memberOf $ss.agentcore.dal.http
    *  @function
    *  @description  Make an authenticated HTTP "POST" Request ; post the specified local file(Using password control)
    *  @param sData Data to be posted
    *  @param sUrl Remote destination where sData will be posted to
    *  @param nTimeout Timeout period in milliseconds
    *  @param sUsername Username for authentication
    *  @param sPassword Password for authentication
    *  @returns obj with properties <br/>
                    bSuccess: true or false depending upon function succeeded or failed<br/>
                    httpStatus: http status of Post<br/>
                    responseText: response from post if any
    *  @example
    *          var objHttp = $ss.agentcore.dal.http;
    *          var ret = objHttp.HttpPostUsingPasswordControl(xmlData,url,3000,username,pwd);
    */
    HttpPostUsingPasswordControl:function (sData, sUrl, nTimeout, sUsername, sPassword)
    {
      _logger.info('Entered function: $ss.agentcore.dal.http.HttpPostUsingPasswordControl');
      var oRet = {bSuccess: false};
      try {        
        var oPWCtl = _utils.CreateObject("sdcuser.tgpassctl");
        oPWCtl.open(sUrl, sUsername, sPassword);
        oPWCtl.setRequestHeader("Authorization", "Basic " + 
                                       _utils.EncodeBase64(sUsername + ":" + sPassword));
        
        if(typeof(nTimeout) === "undefined" || nTimeout <= 0 || nTimeout === "") nTimeout = 60000;

        oRet.bSuccess     = oPWCtl.SendEx("POST", sUrl, nTimeout, sData);
        oRet.httpStatus   = oPWCtl.status;
        oRet.responseText = oPWCtl.responseText;    
        oPWCtl.close();
      } catch(ex) {
        _exception.HandleException(ex,'$ss.agentcore.dal.http','HttpPostUsingPasswordControl',arguments);
      }    
      return oRet;
    },

    /**
    *  @name HttpFileExists
    *  @memberOf $ss.agentcore.dal.http
    *  @function
    *  @description  Checlks if the remote file exists
    *  @param url remote source file that needs to be checked
    *  @param nTimeout timeout period in milliseconds
    *  @param user optional username for authentication
    *  @param pwd optional password for authentication
    *  @returns true on success false on failure
    *  @example
    *           var objHttp = $ss.agentcore.dal.http;
    *           var ret = objHttp.HttpFileExists("http://testServer/test.xml",3000));
    */
    HttpFileExists:function(url, nTimeout, user, pwd)
    {
      _logger.info('Entered function: $ss.agentcore.dal.http.HttpFileExists');
      var rVal = false;

      try 
      {
        //[MAC] Check Machine OS Windows/MAC
        if(!bMac)
          rVal = _objContainer.HttpRequest(_constants.BCONT_REQUEST_EXISTS, "", url, nTimeout, user, pwd);
        else {
          //=======================================================
          //[MAC] Native Call
          var message = {'JSClassNameKey':'HTTP','JSOperationNameKey':'HttpRequest', 'JSArgumentsKey':[_constants.BCONT_REQUEST_EXISTS, "", url, nTimeout, user, pwd], 'JSISSyncMethodKey' : '1'}
          rVal = jsBridge.execute(message)
          //=======================================================
        }
      } 
      catch (err) 
      {
        _exception.HandleException(err,'$ss.agentcore.dal.http','HttpFileExists',arguments);
      }

      return rVal;
    },
    
    /**
    *  @name Authenticate
    *  @memberOf $ss.agentcore.dal.http
    *  @function
    *  @description  Makes an HTTP authentication test call
    *  @param sUrl remote source file that needs to be checked
    *  @param sDestination 
    *  @param nTimeout 
    *  @param user optional username for authentication
    *  @param password optional password for authentication
    *  @param proxyuser optional username for proxy authentication
    *  @param proxypwd optional password for proxy authentication
    *  @returns true on success, false on failure
    *  @example
    *          var objHttp = $ss.agentcore.dal.http;
    *          var ret = objHttp.Authenticate(sHomeRunURL,sHTTPLocalFile,nHTTPTimeout,"","",proxyuser,proxypwd);
    */
    Authenticate:function(sURL, sDestination, iTimeout,
                                  user, password, proxyuser, proxypwd)
    {
      _logger.info('Entered function: $ss.agentcore.dal.http.Authenticate');
      if(user == null)
      {
        user = "";
      }
      
      if (password == null)
      {
        password = "";
      }
      var proxyUser, proxyPwd;
      
      var rVal = 0;

      try 
      {
        //[MAC] Check Machine OS Windows/MAC
        if(!bMac) {
          rVal = _objContainer.HttpAuthenticationTest(_constants.BCONT_REQUEST_TEST_AUTH, 
                                                    sDestination,
                                                    sURL, 
                                                    iTimeout, 
                                                    user, 
                                                    password,
                                                    proxyuser, 
                                                    proxypwd);
        }
        else {
          //=======================================================
          //[MAC] Native Call
          var message = {'JSClassNameKey':'HTTP','JSOperationNameKey':'HttpAuthenticationTest', 'JSArgumentsKey':[_constants.BCONT_REQUEST_TEST_AUTH,sDestination,sURL, iTimeout, user, password, proxyuser, proxypwd], 'JSISSyncMethodKey' : '1'}
          rVal = true;//jsBridge.execute(message)
          //=======================================================
        }
      } 
      catch (err) 
      {
        _exception.HandleException(err,'$ss.agentcore.dal.http','Authenticate',arguments);
      }

      return rVal;
    },

    /**
    *  @name HttpRequest
    *  @memberOf $ss.agentcore.dal.http
    *  @function
    *  @description  Make an authenticated HTTP Request 
    *  @param sUrl remote source file that needs to be checked
    *  @param nMethod int value for HTTP methods, refere $ss.agentcore.constants for more details
    *                 BCONT_REQUEST_GETFILE <br/>
    *                 BCONT_REQUEST_GETFILEIFNEWER <br/>
    *                 BCONT_REQUEST_PUTFILE        <br/>
    *                 BCONT_REQUEST_POSTFILE       <br/>
    *                 BCONT_REQUEST_EXISTS         <br/>
    *                 BCONT_REQUEST_GETFILE_WITH_AUTH <br/>
    *                 BCONT_REQUEST_TEST_AUTH 
    *  @param sDestination destination file
    *  @param nTimeout timeout in milliseconds
    *  @param user optional username for authentication
    *  @param pwd optional password for authentication
    *  @returns true on success,false on failure
    *  @example
    *          var objHttp = $ss.agentcore.dal.http;
    *          var ret = objHttp.HttpRequest(sHomeRunURL,$ss.agentcore.constants.BCONT_REQUEST_GETFILE,tempFile,3000,"","");
    */
    HttpRequest:function (sURL, nMethod, sDestination, iTimeout, user, password)
    {
      _logger.info('Entered function: $ss.agentcore.dal.http.HttpRequest');
      if(!user)
      {
        user = "";
      }
      
      if (!password)
      {
        password = "";
      }
      
      switch (nMethod) {
        // 
        // These are OK 
        case _constants.BCONT_REQUEST_GETFILE:
        case _constants.BCONT_REQUEST_GETFILEIFNEWER:
        case _constants.BCONT_REQUEST_PUTFILE:
        case _constants.BCONT_REQUEST_POSTFILE:
        case _constants.BCONT_REQUEST_EXISTS:
            break;

        // 
        // Everything else must go through the rinse cycle 
        default:
            switch (nMethod.toString().toUpperCase()) {
                case "POST": nMethod = _constants.BCONT_REQUEST_POSTFILE; break;
                case "PUT": nMethod = _constants.BCONT_REQUEST_PUTFILE; break;
                case "EXISTS": nMethod = _constants.BCONT_REQUEST_EXISTS; break;
                case "IFNEWER": nMethod = _constants.BCONT_REQUEST_GETIFNEWER; break;
                default: nMethod = _constants.BCONT_REQUEST_GETFILE; break;
            }
            break;
        }
      
      var rVal = false;

      try 
      {  
        //[MAC] Check Machine OS Windows/MAC
        if(!bMac)
          rVal = _objContainer.HttpRequest(nMethod, sDestination, sURL, iTimeout, user, password);
        else {
          //=======================================================
          //[MAC] Native Call
          var message = {'JSClassNameKey':'HTTP','JSOperationNameKey':'HttpRequest', 'JSArgumentsKey':[nMethod, sDestination, sURL, iTimeout, user, password], 'JSISSyncMethodKey' : '1'}
          rVal = jsBridge.execute(message)
          //=======================================================
        }
      }
      catch (err) {
        _exception.HandleException(err,'$ss.agentcore.dal.http','HttpRequest',arguments);
      }
      return rVal;
    },

    /**
    *  @name AbortRequest
    *  @memberOf $ss.agentcore.dal.http
    *  @function
    *  @description  Cancel any outstanding connection
    *  @returns None
    *  @example
    *           var objHttp = $ss.agentcore.dal.http;
    *           objHttp.AbortRequest();
    */
    AbortRequest:function ()
    {
      _logger.info('Entered function: $ss.agentcore.dal.http.AbortRequest');
      try {
        //[MAC] Check Machine OS Windows/MAC
        if(!bMac)
          _objContainer.AbortRequest();
        else {
          //=======================================================
          //[MAC] Native Call
          var message = {'JSClassNameKey':'HTTP','JSOperationNameKey':'AbortRequest', 'JSArgumentsKey':[], 'JSISSyncMethodKey' : '1'}
          jsBridge.execute(message)
          //=======================================================
        }
      } 
      catch(err) {
        _exception.HandleException(err,'$ss.agentcore.dal.http','AbortRequest',arguments);
      }
    }
  });
  
  //[MAC] Get Machine OS type
  var bMac = $.browser.safari;

  //[MAC] JSBridge object used only for MAC
  try {
    var jsBridge = window.JSBridge;
  }
  catch(e) {
  }

  var _objContainer;
  //[MAC] Check Machine OS Windows/MAC
  if(!bMac)
    _objContainer = $ss.agentcore.utils.activex.GetObjInstance();
  var _constants = $ss.agentcore.constants;
  var _config = $ss.agentcore.dal.config;
  var _file = $ss.agentcore.dal.file;  
  var _logger = $ss.agentcore.log.GetDefaultLogger("$ss.agentcore.dal.http");  
  var _exception = $ss.agentcore.exceptions;  
})();