﻿/** @namespace Holds all UI related utility functions */
$ss.agentcore.utils.ui = $ss.agentcore.utils;
(function()
{
  $.extend($ss.agentcore.utils.ui,
  {
    /**
    *  @name SetTitle 
    *  @memberOf $ss.agentcore.utils.ui
    *  @function
    *  @description  Sets the title of the container
    *  @param strTitle title string
    *  @returns none
    *  @example
    *           var objUtil = $ss.agentcore.utils;
    *           objUtil.SetTitle("New Title");
    */
    SetTitle : function(strTitle)
    {
      strTitle = strTitle || "";
      //[MAC] Check Machine OS Windows/MAC
      if(!bMac)
        _objContainer.SetTitle(strTitle);
      else {
        //=======================================================
        //[MAC] Native Call
        var message = {'JSClassNameKey':'UIUtility','JSOperationNameKey':'SetTitle', 'JSArgumentsKey':[strTitle], 'JSISSyncMethodKey' : '1'}
        jsBridge.execute(message)
        //=======================================================
      }
    },
    
    /**
    *  @name CenterWindow
    *  @memberOf $ss.agentcore.utils.ui
    *  @function
    *  @description  Moves the container window to the center
    *  @returns none
    *  @example
    *           var objUtil = $ss.agentcore.utils;
    *           objUtil.CenterWindow();
    */
    CenterWindow:function()
    {
      //[MAC] Check Machine OS Windows/MAC
      if(!bMac)
        _objContainer.CenterWindow();
      else {
        //=======================================================
        //[MAC] Native Call
        var message = {'JSClassNameKey':'Utility','JSOperationNameKey':'CenterWindow', 'JSArgumentsKey':[strTitle], 'JSISSyncMethodKey' : '1'}
        jsBridge.execute(message)
        //=======================================================
      }
    },
  
    /**
    *  @name GetRootPath()
    *  @memberOf $ss.agentcore.utils.ui
    *  @function
    *  @description  Gets the root path of the current page. Could be local path or <br/>
    *                server path if page is rendered from the server.
    *  @returns Path of the file.
    *  @example
    *           var objUtil = $ss.agentcore.utils;     
    *           var path = objUtil.GetRootPath();
    */
    GetRootPath:function ()
    {
      _logger.info('Entered function: $ss.agentcore.utils.ui.GetRootPath');
      try 
      {
        return _config.ParseMacros("%PRODUCTPATH%");
      } 
      catch(e) 
      {
        _exception.HandleException(e,'$ss.agentcore.utils.ui','GetRootPath',arguments);
      }
    },
    
    /**
    *  @name GetObjectFromQueryString
    *  @memberOf $ss.agentcore.utils.ui
    *  @function
    *  @description  Converts query string name/value pairs to JavaScript Object <br/>
    *                with Object property = name and property's value = value
    *  @param strLoc optional argument specifying the url, by default the current location url is taken
    *  @returns Object having {name: "value"} pairs
    *  @example
    *           var objUtil = $ss.agentcore.utils;       
    *           var obj = objUtil.GetObjectFromQueryString(); 
    */
    GetObjectFromQueryString: function(strLoc) {
      _logger.info('Entered function: $ss.agentcore.utils.ui.GetObjectFromQueryString');
      var sQStr = "";
      var oQStr = new Object();
      try {
        if (strLoc == undefined) {
          strLoc = location.toString();
        }
        //Don't assume query path will have "?", modal windows don't!
        if (strLoc.indexOf("?")>-1) {    
          sQStr = strLoc.substring(strLoc.indexOf("?")+1);
        } else {
          sQStr = strLoc;
        }
        if ( sQStr ) {
          //split query string into name value pairs
          if (sQStr.indexOf("&") == -1 && sQStr.indexOf("%26") > -1) {
            sQStr = unescape(sQStr); // Needs a single level of unescape before the url can be usable.
          }
          var arNVPairs = sQStr.split("&");
          for (var i=0; i<arNVPairs.length; i++) {
            // take each pair and store it in new array
            arNVPair = arNVPairs[i].split("=");
            arNVPair[0] = unescape(arNVPair[0]); // unescape the particular key
            arNVPair[1] = (arNVPair[1] == undefined) ? "" : unescape(arNVPair[1]); // unescape the value
            oQStr[arNVPair[0]] = arNVPair[1];
          }
        }
      }
      catch (ex) {
        _exception.HandleException(ex,'$ss.agentcore.utils.ui','GetObjectFromQueryString',arguments);
      }

      //Add bcont command line parameters if they exist
      try {  //in case ss_container not loaded.
        var oCmdLine = this.GetObjectFromCmdLine();
        for(var cItem in oCmdLine) {
          oQStr[cItem] = oCmdLine[cItem];
        }
        // special case configxml because /config is hijacked by bcont.
        if (oCmdLine["configxml"] != undefined && oCmdLine["configxml"].toLowerCase().indexOf(".xml")>-1) {
          oQStr["config"]=oCmdLine["configxml"];
        }
      }
      catch(e) {
        _exception.HandleException(e,'$ss.agentcore.utils.ui','GetObjectFromQueryString',arguments);
      }
      return oQStr;
    },
    
    /**
    *  @name OpenWindow
    *  @memberOf $ss.agentcore.utils.ui
    *  @function
    *  @description  Wrapper around window.open. Look at window.open documentation<br/>
                     for details.
    *  @param sURL Optional. String that specifies the URL of the document<br/>
    *                        to display. If no URL is specified, a new window with<br/>
    *                        about:blank is displayed.<br/>
    *  @param sName Optional. String that specifies the name of the window.<br/>
    *                         This name is used as the value for the TARGET attribute<br/>
    *                         on a form or an anchor element. _blank, _media, _parent<br/>
    *                         _search, _self, _top are some of the default supported<br/>
    *                         values.
    *  @param sFeatures Optional. This String parameter is a list of items<br/>
    *                             separated by commas. Each item consists of an option<br/>
    *                             and a value, separated by an equals sign<br/>
    *                             (for example, "fullscreen=yes, toolbar=yes").<br/>
    *  @param bReplace Optional. When the sURL is loaded into the same window,<br/>
    *                            this Boolean parameter specifies whether the sURL<br/>
    *                            creates a new entry or replaces the current entry in<br/>
    *                            the window's history list.<br/>
    *                            (true) replaces the current document in the history list<br/>
    *                            (false) creates a new entry in the history list.<br/>
    *  @returns Returns a reference to the new window object. Use this reference<br/>
    *           to access properties and methods on the new window.
    *  @example
    *           var objUtil = $ss.agentcore.utils;
    *       var win = objUtil.OpenWindow( "..\\TestPages\\testpageAgentCoreDALXML.htm","Test","fullscreen=no, toolbar=yes");
    */
    OpenWindow:function (sURL, sName, sFeatures, bReplace)
    {
      _logger.info('Entered function: $ss.agentcore.utils.ui.OpenWindow');
      var hWin = null; //handle to the window
      try {
        if (typeof sFeatures != "undefined" && 
            sFeatures != null && sFeatures.length > 0 &&
            this.IsWinVST(true)) 
        {
           // For windows vista height and width feature doesn't work properly
           sFeatures = sFeatures.replace(/\s*(height|width)\s*=\s*\d+\s*\,/ig, "");
        }
        hWin = window.open(sURL, sName, sFeatures, bReplace);
      } catch (ex) {
        _exception.HandleException(ex,'$ss.agentcore.utils.ui','OpenWindow',arguments);
      }
      return hWin;
    },
    
    /**
    *  @name GetObjectFromCmdLine
    *  @memberOf $ss.agentcore.utils.ui
    *  @function
    *  @description  Sets Command line arguments to Object
    *  @returns Object
    *  @example
    *           var objUtil = $ss.agentcore.utils;
    *           var obj = objUtil.GetObjectFromCmdLine();
    *
    */
    GetObjectFromCmdLine:function ()
    {
      _logger.info('Entered function: $ss.agentcore.utils.ui.GetObjectFromCmdLine');
      var oQStr = new Object();
      
      try {
        //if(! ss_con_IsUsingBCont()) return oQStr;
        //[MAC] Check Machine OS Windows/MAC
        if(!bMac)
          var aParameters = new VBArray(_objContainer.getArguments()).toArray();
        else {
          //=======================================================
          //[MAC] Native Call
          var message = {'JSClassNameKey':'CommonUtility','JSOperationNameKey':'GetArguments', 'JSArgumentsKey':[], 'JSISSyncMethodKey' : '1'}
          var result = jsBridge.execute(message);
          var parsedJSONObject = JSON.parse(result);
          // var aParameters = new VBArray(jsBridge.execute(message)).toArray();
          //=======================================================
          var aParameters = parsedJSONObject.Data;
        }
        oQStr = _GetObjectFromCmdLine(aParameters);
      } catch (ex) {
        _exception.HandleException(ex,'$ss.agentcore.utils.ui','GetObjectFromCmdLine',arguments);
      }
      return oQStr;
    },
    
    /**
    *  @name GetIEVersion
    *  @memberOf $ss.agentcore.utils.ui
    *  @function
    *  @description  Determine the version of IE
    *  @returns returns the version in the format MajorVersion.MinorVersion where the majorVersion is greater than 0 if the browser is IE
    *  @example
    *           var objUtil = $ss.agentcore.utils;
    *           var ver = objUtil.GetIEVersion();
    */
    GetIEVersion:function()
    {
      _logger.info('Entered function: $ss.agentcore.utils.ui.GetIEVersion');
      var sRetVersion = "0.0";
      //Get UserAgent
      var sCurVersion = _registry.GetRegValue("HKLM", "Software\\Microsoft\\Internet Explorer\\Version Vector", "IE");
      if(sCurVersion !== "") {
        var iPosOfDot = sCurVersion.indexOf(".");
        iPosOfDot = iPosOfDot + 2; //To get the dot and next element after the dot
        sRetVersion = sCurVersion.substring(0,iPosOfDot);
      }
      return sRetVersion;
    },
    
    /**
    *  @name CreateActiveXObject
    *  @memberOf $ss.agentcore.utils.ui
    *  @function
    *  @description This is a wrapper around all control instantiations. Instead of<br/>
    *               directly calling new ActiveXObject the application calls this<br/>
    *               method. It sets the provider id for new instantiated control &<br/>
    *               renders license if required
    *  @param progId program id of the control
    *  @param bCache  If true returns the object from the cache if exists
    *  @returns object, if successful
    *  @example
    *           var objUtil = $ss.agentcore.utils;
    *           var objActiveX = objUtil.CreateActiveXObject("SPRT.SdcNetCheck");
    */
    CreateActiveXObject:function (progId, bCache)
    {
      _logger.info('Entered function: $ss.agentcore.utils.ui.CreateActiveXObject');

      if (!_bRenderedLicense) {
        if(typeof RenderLicense == "function") {
          RenderLicense();
        }
        _bRenderedLicense = true;
        _activeXObjectCache  = new Object;
      }
      
      var oRet = null;
      if(bCache) 
      {
        // need to use alternate cache to look for object instance
        if (!_activeXObjectCache[progId]) 
        {
          try 
          {
            _activeXObjectCache[progId] = _CreateActiveXObject(progId);
          } 
          catch (ex) 
          {
        _exception.HandleException(ex,'$ss.agentcore.utils.ui','CreateActiveXObject',arguments);
          }
        }
        oRet = _activeXObjectCache[progId];
      } 
      else 
      {
        try 
        {
          oRet = _CreateActiveXObject(progId);
        } 
        catch (ex) 
        {
          _exception.HandleException(ex,'$ss.agentcore.utils.ui','CreateActiveXObject',arguments);
        }
      }
      return oRet;
    },
    
    /**
    *  @name XMLToJSON 
    *  @memberOf $ss.agentcore.utils.ui
    *  @function
    *  @description Converts an xml dom to a json object 
    *  @param dom DOM of the xml
    *  @returns json object
    *  @example
    *           var objUtil = $ss.agentcore.utils;
    *           var objJSON = objUtil.XMLToJSON(xmlDom);
    */
    XMLToJSON : function(dom)
    {
      return $.xmlToJSON(dom);
    },
    
    /**
    *  @name RunDefaultBrowser
    *  @memberOf $ss.agentcore.utils.ui
    *  @function
    *  @description  Opens the link in a default browser
    *  @param oLink link to be opened
    *  @param oEvent browser event object
    *  @returns int
    *  @ 0 = SUCCESS
    *  @ -1 = Error(default browser is disabled)
    *  @example
    *           var objUtil = $ss.agentcore.utils;
    *           objUtil.RunDefaultBrowser(href);
    */
    RunDefaultBrowser: function(oLink, oEvent) 
    {
      //[MAC] Check Machine OS Windows/MAC
      if(!bMac) {
        var retVal = 0;         
        oEvent = oEvent || event;
        var sDefaultBrowser = _registry.GetRegValue("HKCU", "Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\FileExts\\.html\\UserChoice", "Progid");
        if(sDefaultBrowser == "")   
        {
          sDefaultBrowser = "HTTP";
        }
        var sDefaultHttpHandler = _registry.GetRegValue("HKEY_CLASSES_ROOT", sDefaultBrowser + "\\shell\\open\\command", "");
        // If exe does not exist, then revert back to using Internet Explorer
        var sExecFile = sDefaultHttpHandler.substr(0,sDefaultHttpHandler.toLowerCase().indexOf(".exe") + 4);
        sExecFile = sExecFile.replace(/"/g,"");    
        if (!_file.FileExists(sExecFile)) {
          sDefaultHttpHandler = _registry.GetRegValue("HKEY_CLASSES_ROOT", "Applications\\iexplore.exe\\shell\\open\\command", "");
        }
        retVal = this.LaunchBrowser(oLink, oEvent, sDefaultHttpHandler);
      }
      else {
        var retVal = 1;         
        var message = {'JSClassNameKey':'UIUtility','JSOperationNameKey':'LaunchBrowser', 'JSArgumentsKey':[oLink], 'JSISSyncMethodKey' : '1'}
        jsBridge.execute(message)
      } 
      return retVal;
    },
  
    /**
    *  @name RunIEBrowser
    *  @memberOf $ss.agentcore.utils.ui
    *  @function
    *  @description  Opens the link in a internet explorer
    *  @param oLink link to be opened
    *  @param oEvent browser event object
    *  @returns int
    *  @ 0 = SUCCESS
    *  @ -1 = Error(IE is disabled)   
    *  @example
    *           var objUtil = $ss.agentcore.utils;
    *           objUtil.RunIEBrowser(href);
    */
    RunIEBrowser: function(oLink, oEvent) {
      var retVal = 0;   // 0 = SUCCESS
      // -1 = Error(IE is disabled)

      oEvent = oEvent || event;
      var sDefaultHttpHandler = _registry.GetRegValue("HKEY_CLASSES_ROOT", "Applications\\iexplore.exe\\shell\\open\\command", "");     

      retVal = this.LaunchBrowser(oLink, oEvent, sDefaultHttpHandler);
      return retVal;
    },

    /**
    *  @name LaunchBrowser
    *  @memberOf $ss.agentcore.utils.ui
    *  @function
    *  @description  opens the link in the browser passed as parameter
    *  @param oLink link to be opened
    *  @param oEvent browser event object
    *  @param sHttpHandler binary path for the browser to open the link in 
    *  @returns int
    *  @ 0 = SUCCESS
    *  @ -1 = Error  
    *  @returns 
    *  @example
    *           var objUtil = $ss.agentcore.utils;
    *           objUtil.LaunchBrowser(oLink, oEvent, sHttpHandler);
    */
    LaunchBrowser: function(oLink, oEvent, sHttpHandler) {
      var retVal = 0;
      if (sHttpHandler) {
        // Clean up handler for known ways of specifying the %1 parameter
        sHttpHandler = sHttpHandler
        .replace('"%1"', '')
        .replace('%1', '');

        
        // Append the actual URL
        sHttpHandler = sHttpHandler + ' ' + oLink;
        // Replace double space with single space - safari doesn't like 2 spaces after -url
        sHttpHandler = sHttpHandler.replace(/  /g, " ");
        
        _system.RunCommand(sHttpHandler, _constants.BCONT_RUNCMD_ASYNC);
        if (oEvent) {
            oEvent.cancelBubble = true;
            oEvent.returnValue  = false;
        }
      }
      else {
        retVal = -1;
      }
      return retVal;
    },

    /**
    *  @name GetMiniBcontSpawnName
    *  @memberOf $ss.agentcore.utils.ui
    *  @function
    *  @description  function to get mini-bcont spawning instance name
    *  @param sUrl URL of the Mini Bcont page to be launched
    *  @returns instance name of the mini bcont for a URL
    *  @example
    *           var objUtil = $ss.agentcore.utils;
    *           objUtil.GetMiniBcontSpawnName("minibcont_messages");
    */
    GetMiniBcontSpawnName: function(sUrl) {
      var instanceName = sUrl;

      if(instanceName.substring(0,1) === "/")
        instanceName = instanceName.substring(1,instanceName.length);
      instanceName = "minibcont_" + instanceName;
      return instanceName;
    },

    /**
    *  @name IsMiniBcontRunning
    *  @memberOf $ss.agentcore.utils.ui
    *  @function
    *  @description  function to verify if the mini-bcont is running
    *  @param sInstanceName Instance name of the mini-bcont
    *  @returns true if mini-bcont is running else false
    *  @example
    *           var objUtil = $ss.agentcore.utils;
    *           objUtil.IsMiniBcontRunning("minibcont_messages");
    */
    IsMiniBcontRunning: function(sInstanceName) {

    //[MAC] Check Machine OS Windows/MAC
    if(!bMac) {
      if (this.CheckInstance(_config.GetBcontMutexPrefix() + sInstanceName)) {
        return true;
      }
      return false;
    }
    else {
      var message = {'JSClassNameKey':'UIUtility','JSOperationNameKey':'isAppRunning', 'JSArgumentsKey':['notificationTray'], 'JSISSyncMethodKey' : '1'}
      var result = jsBridge.execute(message);
      var parsedJSONObject = JSON.parse(result);
      return parsedJSONObject.Data;
    }
    },

    /**
    *  @name CloseMiniBcont
    *  @memberOf $ss.agentcore.utils.ui
    *  @function
    *  @description  function to close the mini-bcont
    *  @param sInstanceName Instance name of the mini-bcont
    *  @returns true is successfully launched else false
    *  @example
    *           var objUtil = $ss.agentcore.utils;
    *           objUtil.CloseMiniBcont("minibcont_messages");
    */
    CloseMiniBcont: function(sInstanceName) {
      //[MAC] Check Machine OS Windows/MAC
      if(bMac) {
        var message = {'JSClassNameKey':'UIUtility','JSOperationNameKey':'CloseNotificationTrayApplication', 'JSArgumentsKey':[], 'JSISSyncMethodKey' : '1'}
        jsBridge.execute(message);
        return true;
      }
      var bReturn = true;
      try {
        if (this.CheckInstance(_config.GetBcontMutexPrefix() + sInstanceName)) {
          sBcontInstance = " /instancename " + sInstanceName;
          var bcontExe = "\"" + $ss.agentcore.dal.ini.GetPath("EXE_PATH") + "\"";
          var bcontArgs = "/ini \"" + _config.GetProviderRootPath() + "\agent\\bin\\minibcont.ini\"";
          this.RunCommand( bcontExe + " " + bcontArgs + " " + _constants.BCONT_CLOSE + " " + sBcontInstance, _constants.BCONT_RUNCMD_ASYNC);
        }
       } 
       catch(e) {
        bReturn = false;
       }
      return bReturn;
    },

    /**
    *  @name CloseBcont
    *  @memberOf $ss.agentcore.utils.ui
    *  @function
    *  @description  function to close the bcont
    *  @returns none
    *  @example
    *           var objUtil = $ss.agentcore.utils;
    *           objUtil.CloseBcont();
    */
    CloseBcont:function()
    { 
     try
      {
        $ss.agentcore.events.SendByName("BROADCAST_ON_BCONTCLOSE");
      }
      catch (err) {
        //g_Logger.Error("ss_con_Close", err.message);
      } finally {
      //[MAC] Check Machine OS Windows/MAC
      if(!bMac) {
        setTimeout(function(){
        window.external.Close();
        },40);
        window.external.Close();
      }
      else {
        var message = {'JSClassNameKey':'UIUtility','JSOperationNameKey':'CloseApplication', 'JSArgumentsKey':[], 'JSISSyncMethodKey' : '0'}
       jsBridge.execute(message);
      }
      }
    },

    /**
    *  @name CanCloseBcont
    *  @memberOf $ss.agentcore.utils.ui
    *  @function
    *  @description  function to have default implementaion for CanCloseBcont condition check
    *               before closing bcont. Currently it calls the layout helper implementation of
    *               CanCloseBcont if it exists else returns true
    *  @returns true or false
    *  @example
    *           var objUtil = $ss.agentcore.utils;
    *           objUtil.CanCloseBcont();
    */
    CanCloseBcont:function()
    { 
      var bCanClose = true;
      try
      {
        if ($ss.helper && $ss.helper.CanCloseBcont)
          bCanClose = $ss.helper.CanCloseBcont();
      }
      catch (err) {
        bCanClose = true;
      }
      return bCanClose;
    },
   
    /**
    *  @name FocusBcont
    *  @memberOf $ss.agentcore.utils.ui
    *  @function
    *  @description  function to focus on the bcont
    *  @returns none
    *  @example
    *           var objUtil = $ss.agentcore.utils;
    *           objUtil.FocusBcont();
    */
    FocusBcont:function()
    {
      try {
      //[MAC] Check Machine OS Windows/MAC
      if(!bMac)
        _objContainer.Focus();
      else {
        //=======================================================
        //[MAC] Native Call
        var message = {'JSClassNameKey':'UIUtility','JSOperationNameKey':'ShowAgentWindow', 'JSArgumentsKey':[], 'JSISSyncMethodKey' : '1'}
        jsBridge.execute(message);
        //=======================================================
      }
      }
      catch (err) {
        //g_Logger.Error("ss_con_Focus", err.message);
      }
    },

    /**
    *  @name HideBcont
    *  @memberOf $ss.agentcore.utils.ui
    *  @function
    *  @description  function to hide the bcont
    *  @returns none
    *  @example
    *           var objUtil = $ss.agentcore.utils;
    *           objUtil.HideBcont();
    */
    HideBcont:function() {
      //g_Logger.Info("ss_con_Hide")
      try {
      //[MAC] Check Machine OS Windows/MAC
      if(!bMac)
        _objContainer.Hide();
      else {
        //=======================================================
        //[MAC] Native Call
        var message = {'JSClassNameKey':'UIUtility','JSOperationNameKey':'HideAgentWindow', 'JSArgumentsKey':[], 'JSISSyncMethodKey' : '1'}
        jsBridge.execute(message);
        //=======================================================
      }
      } catch (e) {
      } finally {
      }
    },

    /**
    *  @name ShowBcont
    *  @memberOf $ss.agentcore.utils.ui
    *  @function
    *  @description  function to show the bcont
    *  @returns none
    *  @example
    *           var objUtil = $ss.agentcore.utils;
    *           objUtil.ShowBcont();
    */
    ShowBcont:function() {
      //g_Logger.Info("ss_con_Show")
      try {
        //var objContainer = ss_con_GetObjInstance();
        //[MAC] Check Machine OS Windows/MAC
        if(!bMac)
          _objContainer.Show();
        else {
          //=======================================================
          //[MAC] Native Call
          var message = {'JSClassNameKey':'UIUtility','JSOperationNameKey':'ShowAgentWindow', 'JSArgumentsKey':[], 'JSISSyncMethodKey' : '1'}
          jsBridge.execute(message);
          //=======================================================
        }
      } catch (e) {
      } finally {
      }
    },

    /**
    *  @name BringBcontToFront
    *  @memberOf $ss.agentcore.utils.ui
    *  @function
    *  @description  function to bring the bcont to the front
    *  @returns none
    *  @example
    *           var objUtil = $ss.agentcore.utils;
    *           objUtil.BringBcontToFront();
    */
    BringBcontToFront:function() {
      //g_Logger.Info("ss_con_BringToFront")
      try {
        //var objContainer = ss_con_GetObjInstance();
        //[MAC] Check Machine OS Windows/MAC
        if(!bMac)
          _objContainer.BringToFront();
        else {
          //=======================================================
          //[MAC] Native Call
          var message = {'JSClassNameKey':'Utility','JSOperationNameKey':'BringToFront', 'JSArgumentsKey':[], 'JSISSyncMethodKey' : '1'}
          jsBridge.execute(message);
          //=======================================================
        }
      } catch (e) {
      } finally {
      }
    },
    
    
    
    /**
    *  @name UpdateUI
    *  @memberOf $ss.agentcore.utils.ui
    *  @function
    *  @description  Updates the UI, call this when a function needs to be called asynchronously
    *  @param options
    *                options.fnPtr - reference to a function that needs to be called asynchronously <br/> 
    *                options.timeout - Optional,the fnptr will be executed in loop untill it either succeds or   <br/>
    *                                  timeout is reached,to execute it only once, give a very small timeout(ex 1ms) <br/>
    *                options.sleepTime - Optional,the sleep time,calls containers sleep   <br/>
    *                options.callback  - Optional,Calls this method every one second.This can be used to show the progress<br/>
    *                options.targetDivID - Optional,The funtion fnPtr will be executed only if the targetDivID is present<br/>
    *  @returns value return value of the function that needs to be executed asynchronously<br/>
    *  @example
    *                var timer = 0;
    *                var opt = 
    *                {
    *                  fnPtr : function()
    *                          {
    *                            return that.DiagnoseSettings(data);
    *                          },
    *                  timeout : 10000,
    *                  sleepTime : 300,
    *                  callback : function()
    *                              {
    *                                if($('#snp_troubleshoot_main_back').length > 0 )
    *                                {
    *                                  $('#snp_troubleshoot_main_back').html(timer++);
    *                                }
    *                              },
    *                  targetDivID : '#snp_troubleshoot_main_back'
    *                }
    *                var result = $ss.agentcore.utils.UpdateUI(opt);
    */
    UpdateUI:function(options)
    {
      var updateUI = false;
      var sleepTime = options.sleepTime || 100;
      var disableInput = options.disableInput || false;
      var timeout = options.timeout || 100;
      var res = false;
      var startDate = new Date().getTime();
      var firstEntry = true;
      var delayStartTest = options.delayExec || 20;
      var makeShellBusy = true;
      var maxReTry = options.maxReTry == undefined ? 5 : options.maxReTry ;
      if(options.makeShellBusy === false) { makeShellBusy = false};
      var dbBagName = "timer_data_"+startDate.toString();
      
      if (!options.fnPtr) 
        return false;
      
      if(makeShellBusy)
        this.StartShellBusy();
      //var fnToCall = options.fnPtr;
      try {
        /*
        var functionToCall = function() {
          var _logger = $ss.agentcore.log.GetDefaultLogger("$ss.agentcore.utils.ui");
          try {
            var retVal = false;
            $ss.agentcore.dal.databag.SetValue(dbBagName+"_status",false);
            $ss.agentcore.dal.databag.SetValue(dbBagName+"_value",retVal);
            retVal = fnToCall();
          } catch(ex){
            _logger.error("Error while Evaluating updat ui timer function %1% " , ex.message);
          }finally {
            $ss.agentcore.dal.databag.SetValue(dbBagName+"_value",retVal);
            $ss.agentcore.dal.databag.SetValue(dbBagName+"_status",true);
          }
        };
        */      
        
        if (options.callback) {
          $().everyTime(1000, startDate.toString(), options.callback);
        }
        
        
        while (!updateUI) {
          if (firstEntry) {
            if (options.callback) 
              options.callback();
            //$().oneTime(1000,startDate.toString(),options.callback);
            $ss.agentcore.utils.Sleep(delayStartTest);
         //   $().oneTime(20, dbBagName, functionToCall);
            firstEntry = false;
          }
          maxReTry--;
          res = (options.fnPtr)();
        // res = $ss.agentcore.dal.databag.GetValue(dbBagName+"_status");
          if (res) {
            updateUI = true;
          }
          else {
            if (timeout <= (new Date().getTime() - startDate)) 
              updateUI = true;
            if(maxReTry === 0) {
              updateUI = true;
            }
            if (options.targetDivID) {
              if ($('#' + options.targetDivID).length == 0) 
                updateUI = true;
            }
          }
          if (!updateUI) {
            $ss.agentcore.utils.Sleep(sleepTime);
          }
        }
      } 
      catch (ex) {
      }
      
      $().stopTime(startDate.toString());
      
      if(makeShellBusy)
        this.EndShellBusy();
   //   var retVal = $ss.agentcore.dal.databag.GetValue(dbBagName+"_value");
   //   $ss.agentcore.dal.databag.RemoveValue(dbBagName+"_value");
   //   $ss.agentcore.dal.databag.RemoveValue(dbBagName+"_status");
      //fnToCall = null;
      return res;
    },

    /**
    *  @name GetRemainingTime
    *  @memberOf $ss.agentcore.utils.ui
    *  @function
    *  @description  Function to calculate the remaining time, provided the starttime and duration
    *  @param nStartTime Start time
    *  @param nDuration Total duration 
    *  @returns Returns the remaining time
    *  @example
    *           var startTime=new Date();
    *           var duration = 30;
    *           var remTime = $ss.agentcore.utils.ui.GetRemainingTime(startTime,duration);
    */
    GetRemainingTime: function(nStartTime,nDuration) {
      var time = new Date();
      var diffSec = nDuration - (Math.floor((time - nStartTime) / 1000));
      if(diffSec<0) diffSec =0;
      return diffSec;
    },
    
    /**
    *  @name FixPng
    *  @memberOf $ss.agentcore.utils.ui
    *  @function
    *  @description  Function to fix the transparency issue of pngs in IE6
    *  @returns none
    *  @example
    *           var objUtil = $ss.agentcore.utils;
    *           objUtil.FixPng();
    */
    FixPng: function() {
      var arVersion = navigator.appVersion.split("MSIE")
      var version = parseFloat(arVersion[1])
      
      if ((version >= 5.5 && version <7.0 ) && (document.body.filters)) {
        for (var i = 0; i < document.images.length; i++) {
          var img = document.images[i]
          var imgName = img.src.toUpperCase()
          if (imgName.substring(imgName.length - 3, imgName.length) == "PNG") {
            var imgID = (img.id) ? "id='" + img.id + "' " : ""
            var imgClass = (img.className) ? "class='" + img.className + "' " : ""
            var imgTitle = (img.title) ? "title='" + img.title + "' " : "title='" + img.alt + "' "
            var imgStyle = "display:inline-block;" + img.style.cssText
            if (img.align == "left") 
              imgStyle = "float:left;" + imgStyle
            if (img.align == "right") 
              imgStyle = "float:right;" + imgStyle
            if (img.parentElement.href) 
              imgStyle = "cursor:hand;" + imgStyle
            var strNewHTML = "<span " + imgID + imgClass + imgTitle +
            " style=\"" +
            "width:" +
            img.width +
            "px; height:" +
            img.height +
            "px;" +
            imgStyle +
            ";" +
            "filter:progid:DXImageTransform.Microsoft.AlphaImageLoader" +
            "(src=\'" +
            img.src +
            "\', sizingMethod='scale');\"></span>"
            img.outerHTML = strNewHTML
            i = i - 1
          }
        }
      }
    },

    /**
    *  @name StartShellBusy
    *  @memberOf $ss.agentcore.utils.ui
    *  @function
    *  @description  Sets UI to busy mode
    *  @returns none
    *  @example
    *           var objUtil = $ss.agentcore.utils;
    *           objUtil.StartShellBusy();
    */
    StartShellBusy:function()
    {
      var retVal = false;
      if(!$ss.agentcore.dal.databag.GetValue("ShellBusy"))
      {
        $ss.agentcore.dal.databag.SetValue("ShellBusy",true);
        retVal = true;
      }
      
      return retVal;
    },

    /**
    *  @name EndShellBusy
    *  @memberOf $ss.agentcore.utils.ui
    *  @function
    *  @description  Set UI to Non-Busy mode
    *  @returns none
    *  @example
    *           var objUtil = $ss.agentcore.utils;
    *           objUtil.EndShellBusy();
    */
    EndShellBusy:function()
    {
      var retVal = false;
      if($ss.agentcore.dal.databag.GetValue("ShellBusy"))
      {
        $ss.agentcore.dal.databag.SetValue("ShellBusy",false);
        retVal = true;
      }
      
      return retVal;
    },

    /**
    *  @name ShellBusy
    *  @memberOf $ss.agentcore.utils.ui
    *  @function
    *  @description  Checks if the UI is in the Busy mode
    *  @returns none
    *  @example
    *           var objUtil = $ss.agentcore.utils;
    *           objUtil.ShellBusy();
    */
    ShellBusy:function()
    {
      return $ss.agentcore.dal.databag.GetValue("ShellBusy");
    },

    /**
    *  @name RestartWithSave
    *  @memberOf $ss.agentcore.utils.ui
    *  @function
    *  @description  Save the state and restarts the system
    *  @returns none
    *  @example
    *           var objUtil = $ss.agentcore.utils;
    *           // If called from steplist snapin, the session id has to be saved
    *           objUtil.RestartWithSave(snapinURL + "?sl_resume=true&sl_session=" + sessionid, true); 
    *           // If called from a normal snapin
    *           objUtil.RestartWithSave(snapinURL, true); 
    */
    RestartWithSave:function(path,loadDatabag)
    {
      var objCmdLine = this.GetObjectFromCmdLine();
      var data = "\"" + $ss.agentcore.dal.ini.GetPath("EXE_PATH")  + "\"";
      
      if(objCmdLine["ini"])
        data += " /ini \"" + objCmdLine["ini"] + "\"";
        
      if(path)  
        data += " /path \"" + path + "\"";
       
      if(objCmdLine["p"])
        data += " /p " + objCmdLine["p"];
      
      if(objCmdLine["ignoresingle"])
        data += " /ignoresingle ";
                  
                        
      if(loadDatabag)
        data += " /loaddbbag";                        

      var value = _config.GetBcontMutexPrefix();
      
      // create run once key
      $ss.agentcore.utils.CreateRunOnceKey(value,data);
     
      // Save the state
      $ss.agentcore.dal.databag.PersistDatabag();
      
      $ss.agentcore.utils.Reboot();
    },

    /**
    *  @name ShowDAgentFirstTimeIfDAHidden
    *  @memberOf $ss.agentcore.utils.ui
    *  @function
    *  @description  Function to show the BCONT for the first time if starthidden is not set.
    *  @returns none
    *  @example
    *           var objUtil = $ss.agentcore.utils;
    *           objUtil.ShowDAgentFirstTimeIfDAHidden();
    */
    ShowDAgentFirstTimeIfDAHidden: function() {

      //[MAC] Check Machine OS Windows/MAC
      if(!bMac) {
        var objCmdLine = $ss.agentcore.utils.GetObjectFromCmdLine();
        var hiddenConfig = $ss.agentcore.dal.ini.GetIniValue("", "SETUP", "starthidden", "0");
        var isBcontStartHidden = objCmdLine.starthidden || (hiddenConfig === "1");
        if(!isBcontStartHidden) {
          if(!_firstDADisplayDone) {
            $ss.agentcore.utils.ui.EnableFirstDisplayDone();
            $ss.agentcore.utils.Sleep(40);
            window.external.focus();
          }
        }
      }
      else {
        var message = {'JSClassNameKey':'UIUtility','JSOperationNameKey':'ShowAgentWindow', 'JSArgumentsKey':[], 'JSISSyncMethodKey' : '1'}
        jsBridge.execute(message)
      }
    },

    /**
    *  @name EnableFirstDisplayDone
    *  @memberOf $ss.agentcore.utils.ui
    *  @function
    *  @description  Function to set the FirstDADisplayDone flag to true
    *  @returns none
    *  @example
    *           var objUtil = $ss.agentcore.utils;
    *           objUtil.EnableFirstDisplayDone();
    */
    EnableFirstDisplayDone: function() {
      _firstDADisplayDone = true;
    },

    /**
    *  @name GetEnableFirstDisplayStatus
    *  @memberOf $ss.agentcore.utils.ui
    *  @function
    *  @description  Function to check the status of FirstDADisplayDone flag
    *  @returns none
    *  @example
    *           var objUtil = $ss.agentcore.utils;
    *           objUtil.GetEnableFirstDisplayStatus();
    */
    GetEnableFirstDisplayStatus: function() {
      return _firstDADisplayDone;
    },

    //reference :"jquery.ss.utils.js"
    EncodeHtml: function (html) {
      if (html == null) return ''; 
      return encoder(html);
    },
    
    DecodeHtml: function (html) {
      if (html == null) return ''; 
      return decoder(html);
    }

  });
  
  //[MAC] Get Machine OS type
  var bMac = $.browser.safari;

  //[MAC] JSBridge object used only for MAC
  try {
    var jsBridge = window.JSBridge;
  }
  catch(e) {
  }

  var _objContainer;
  //[MAC] Check Machine OS Windows/MAC
  if(!bMac)
    _objContainer = $ss.agentcore.utils.activex.GetObjInstance();

  var _config = $ss.agentcore.dal.config;
  var _logger = $ss.agentcore.log.GetDefaultLogger("$ss.agentcore.utils.ui");  
  var _exception = $ss.agentcore.exceptions;
  var _registry = $ss.agentcore.dal.registry;
  var _system = $ss.agentcore.utils.system;
  var _file = $ss.agentcore.dal.file;
  var _constants = $ss.agentcore.constants;
  var _firstDADisplayDone=false;
  var _bRenderedLicense;
  var _activeXObjectCache;
  var _activeXObjectCache2;  
  
  var escapeMappings = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#39;'  //&apos for HTML 5
  ,'\\':'&#92;'  
  ,'<br>':'\n'  
  };

  var unescapeMappings = {};
  $.map(escapeMappings, function (val, key) { unescapeMappings[val.replace(/\\/g,'\\\\')] = key;});
  var _encoder = function (map) {
    var keys = [];
    $.map(map, function (val, key) { keys.push(key.replace(/\\/g,'\\\\')); });
  
    var source = '(?:' + keys.join('|') + ')';
    var testRegexp = RegExp(source);
    var replaceRegexp = RegExp(source, 'gm');
    return function (str) {
      if (str == null) return str;
      str = '' + str;
      return testRegexp.test(str) ? str.replace(replaceRegexp, function (match) { return map[match]; }) : str;
    };
  };
  var encoder = _encoder(escapeMappings);
  var decoder = _encoder(unescapeMappings);
    
  function _GetObjectFromCmdLine(aParameters)
  {
    var oQStr = new Object();
    
    try 
    {
      for(var iCnt=1;iCnt<aParameters.length;iCnt++) 
      {
        var p1=aParameters[iCnt];
        if(p1.substr(0,1) === "/") 
        {
          p1=p1.replace("\/","");
          oQStr[p1] = true;

          if(iCnt+1 < aParameters.length) // if we're on the last arg, don't look for another param
          {
            var x=aParameters[iCnt + 1];
            if(x.substr(0,1) !== "/" || p1 === "path") 
            {
              oQStr[p1]=aParameters[iCnt +1];
              iCnt++;
            }
          }
        }
      }
     } 
     catch (ex) 
     {
      //g_Logger.Error("ss_con_CmdToObjEx()", ex.message);
     }
     return oQStr;
  }
  
  /*******************************************************************************
  ** Name:         _CreateActiveXObject
  **
  ** Purpose:      Do the actual ActiveX Object creation
  **
  ** Parameter:    ProgID
  **
  ** Return:       true/false
  *******************************************************************************/
  function _CreateActiveXObject(progId) 
  {
    //Try block in case provider is not set, else caller will catch error and no object returned
    try 
    {
      var obj = new ActiveXObject(progId);
      obj.SetIdentity(_config.GetProviderID());
    } 
    catch (ex) {
      //g_Logger.Error("ss_con_pvt_ActiveXObject()",  progId + ":" + ex.message);
    }
    return obj;
  }

})();
