/*******************************************************************************
 **  File Name: smapi_util.js
 **
 **  Summary: SupportSoft Modem API utility classes and functions
 **
 **  Description: This javascript contains classes and methods that provide SMAPI
 **               protocol implementation and support for SMAPI test harness. It
 **               also helps in providing common API's to SMAPI for modem xml
 **               parsing, SmartIssue lookups, registry lookups etc.
 **               uses:  ss_constants.js
 **                      ss_container.js
 **                      ss_config.js
 **                      ss_netcheck.js
 **                      ss_log.js
 **                      ss_si.js
 **
 **
 **  Copyright SupportSoft Inc. 2006, All rights reserved.
 *******************************************************************************/
//<dependsOn>  
//  <script type="text/javascript" src="..\JSUnit\app\jsUnitCore.js"></script>
//  <script type="text/javascript" src="..\jquery.js"></script>
//  <script type="text/javascript" src="..\log4js-mod.js"></script>
//  <script type="text/javascript" src="..\ss.agentcore.ns.js"></script>
//  <script type="text/javascript" src="..\ss.agentcore.log.js"></script>
//  <script type="text/javascript" src="..\ss.agentcore.constants.js"></script>
//  <script type="text/javascript" src="..\ss.agentcore.utils.activex.js"></script>
//  <script type="text/javascript" src="..\ss.agentcore.utils.js"></script>
//  <script type="text/javascript" src="..\ss.agentcore.utils.system.js"></script>
//  <script type="text/javascript" src="..\ss.agentcore.dal.config.js"></script>
//  <script type="text/javascript" src="..\ss.agentcore.lockdown.js"></script>
//  <script type="text/javascript" src="..\ss.agentcore.dal.registry.js"></script>
//</dependsOn>
$ss.agentcore.smapi = $ss.agentcore.smapi || {};
/** @namespace Holds all smapi utils related functionality*/
$ss.agentcore.smapi.utils = $ss.agentcore.smapi.utils ||
{};

var _config = $ss.agentcore.dal.config;

(function(){
  $.extend($ss.agentcore.constants, {      // extending the constants namespace to include all the relevant SMAPI constants
    /*******************************************************************************
     **   ERROR CONSTANTS
     *******************************************************************************/
    SMAPI_SUCCESS: "SMAPI_SUCCESS",
    SMAPI_FAIL: "SMAPI_FAIL",
    SMAPI_DERIVED_FAIL: "SMAPI_DERIVED_FAIL",
    SMAPI_FAILED_INITIALIZATIION: "SMAPI_FAILED_INITIALIZATIION",
    SMAPI_FAILED_INSTANCE_CREATION: "SMAPI_FAILED_INSTANCE_CREATION",
    SMAPI_FUNCTION_NOTIMPL: "SMAPI_FUNCTION_NOTIMPL",
    SMAPI_OPERATION_NOTSUPPORTED: "SMAPI_OPERATION_NOTSUPPORTED",
    SMAPI_NO_AUTH: "SMAPI_NO_AUTH",
    SMAPI_DO_REBOOT: "SMAPI_DO_REBOOT",
    SMAPI_EXCEPTION: "SMAPI_EXCEPTION",
    SMAPI_DERIVED_EXCEPTION: "SMAPI_DERIVED_EXCEPTION",
    SMAPI_PING_FAIL: "SMAPI_PING_FAIL",
    SMAPI_FAIL_MODEM_ACCESS: "SMAPI_FAIL_MODEM_ACCESS",
    SMAPI_INVALID_ARGUMENT: "SMAPI_INVALID_ARGUMENT",
    SMAPI_FAILED_AUTODETECT: "SMAPI_FAILED_AUTODETECT",
    SMAPI_QUEUED_ALREADY: "SMAPI_QUEUED_ALREADY",
    
    /*******************************************************************************
     **   MESSAGE CONSTANTS
     *******************************************************************************/
    SMAPI_MSG_ALL: 0,
    SMAPI_MSG_DETECT: 1,
    SMAPI_MSG_SYNC: 2,
    SMAPI_MSG_CONNECT: 3,
    SMAPI_MSG_NO_AUTH: 4,
    SMAPI_MSG_FILE_DOWNLOAD: 5,
    SMAPI_MSG_FIRMWARE_UPGRADE: 6,
    
    /*******************************************************************************
     **   PROTOCOL NAMES or BASE SUPPORT CLASSES available IN SMAPI
     *******************************************************************************/
    SMAPI_TR64: "TR64",
    SMAPI_DDM: "DDM",
    SMAPI_CLI: "CLI",
    SMAPI_RAS: "RAS",
    
    /*******************************************************************************
     **   GLOBAL CONSTANTS
     *******************************************************************************/
    SMAPI_MODEM: "modem",
    SMAPI_DEFAULT_MODEM: _config.GetConfigValue("snapin_modem","default_modem_code","null_modem_dsl"),
    SMAPI_WESTELL_6100: "westell_6100",
    SMAPI_WESTELL_VERSALINK: "westell_versalink",
    SMAPI_THOMSON_716G: "th_st716g",
    SMAPI_LINKSYS_WAG54GV2: "linksys_wag54gv2",
    SMAPI_EMPTY_STR: "",
    SMAPI_STR_DONT_CARE: "",
    SMAPI_OBJ_DONT_CARE: null,
    SMAPI_INT_DONT_CARE: -1,
    SMAPI_HEX_DONT_CARE: 0x00000000,
    SMAPI_VAL_ENABLE: 1,
    SMAPI_VAL_DISABLE: 0,
    SMAPI_VAL_UNKNOWN: -1,
    SMAPI_OBJ_UNKNOWN: null,
    SMAPI_FIRMWARE_DONTCARE: "*",
    SMAPI_FIRMWARE_NULL: "null",
    SMAPI_FIRMWARE_NOMATCH: "-1",
    SMAPI_FIRMWARE_DELIMITER: ".",
    SMAPI_FIRMWARE_MIN: "0",
    SMAPI_FIRMWARE_MAX: "100",
    
    /******************************************************************************/
    // Databag values set and retrieved by SMAPI using Databag naming conventions
    SMAPI_DB_ADMINNAME: "ss_smapi_db_AdminName",
    SMAPI_DB_ADMINPASS: "ss_smapi_db_AdminPassword",
    SMAPI_DB_USERNAME: "ss_smapi_db_UserName",
    SMAPI_DB_USERPASS: "ss_smapi_db_UserPassword",
    SMAPI_DB_FIRMWAREID: "ss_smapi_db_FirmwareId",
    SMAPI_DB_TR64ENABLED: "ss_smapi_db_Tr64Enabled",
    SMAPI_DB_LASTDETECTED_MODEMCODE: "ss_smapi_db_LastDetectedModemCode",
    SMAPI_DB_LASTDETECTED_MODEMCONNTYPE: "ss_smapi_db_LastDetectedModemConnType",
    SMAPI_DB_LASTDETECTED_MODEMCLASS: "ss_smapi_db_LastDetectedModemClass",
    SMAPI_DB_LASTDETECTED_MODEMNAME: "ss_smapi_db_LastDetectedModemName",
    
    /******************************************************************************/
    // ss_modem.xml constants
    SMAPI_XMLNODE_ROOT: "sprt_modemlist",
    SMAPI_XMLNODE_MODEM: "modem",
    SMAPI_XMLNODE_FIRMWARE: "firmware",
    SMAPI_XMLNODE_FIRMWARE_MIN: "min_version",
    SMAPI_XMLNODE_FIRMWARE_MAX: "max_version",
    SMAPI_XMLNODE_INSTANCE: "instance",
    SMAPI_XMLNODE_SCRIPTFILE: "scriptfile",
    SMAPI_XMLNODE_CLASS: "class",
    SMAPI_XMLNODE_ADMINNAME: "modemuser",
    SMAPI_XMLNODE_ADMINPASS: "modempwd",
    SMAPI_XMLNODE_CONNTYPE: "connection_type",
    SMAPI_XMLNODE_ITEM: "item",
    
    SMAPI_XMLNODE_WIRELESS: "wireless",
    SMAPI_XMLNODE_WIRELESS_SECURITY_MAP: "wireless_security_map",
    SMAPI_XMLNODE_WIRELESS_SSID: "ssid",
    SMAPI_XMLNODE_WIRELESS_SSID_BROADCAST: "ssid_broadcast",
    SMAPI_XMLNODE_WIRELESS_PASSPHRASE: "passphrase",
    SMAPI_XMLNODE_WIRELESS_PRESHAREDKEY: "presharedkey",
    SMAPI_XMLNODE_WIRELESS_SECURITY_MODE: "security_mode",
    SMAPI_XMLNODE_WIRELESS_AUTHENTICATION: "authentication",
    SMAPI_XMLNODE_WIRELESS_ENCRYPTION: "encryption",
    SMAPI_XMLNODE_WIRELESS_NAME: "name",
    SMAPI_XMLNODE_WIRELESS_USERPROMPT: "ssid_userprompt",
    
    SMAPI_XMLATTR_ID: "id",
    SMAPI_XMLATTR_CODE: "code",
    SMAPI_XMLATTR_DEFAULT: "default",
    SMAPI_XMLATTR_HIDE: "hide",
    
    SMAPI_XMLATTR_VAL_COMMON: "common",
    SMAPI_XMLATTR_VAL_TRUE: "true",
    SMAPI_XMLATTR_VAL_FALSE: "false",
    SMAPI_XMLCHAR_ROOT: "//",
    SMAPI_XMLCHAR_SLASH: "/",
    SMAPI_XMLCHAR_DOTSLASH: "./",
    SMAPI_XMLCHAR_ATTRIBUTE: "[@",
    SMAPI_XMLCHAR_EQUAL: ":"
  
  });
  var _constants = $ss.agentcore.constants;
  $.extend($ss.agentcore.constants, {
    // Derived XPATH Statements
    SMAPI_XPATH_ROOT: _constants.SMAPI_XMLCHAR_ROOT + _constants.SMAPI_XMLNODE_ROOT,
    SMAPI_XPATH_ROOT_MODEM: _constants.SMAPI_XPATH_ROOT + _constants.SMAPI_XMLCHAR_SLASH + _constants.SMAPI_XMLNODE_MODEM,
    SMAPI_XPATH_FIRMWARE: _constants.SMAPI_XMLCHAR_DOTSLASH + _constants.SMAPI_XMLNODE_FIRMWARE,
    SMAPI_XPATH_INSTANCE: _constants.SMAPI_XMLCHAR_DOTSLASH + _constants.SMAPI_XMLNODE_INSTANCE,
    SMAPI_XPATH_CLASS: _constants.SMAPI_XMLCHAR_DOTSLASH + _constants.SMAPI_XMLNODE_CLASS,
    SMAPI_XPATH_FIRMWARE_MIN: _constants.SMAPI_XMLCHAR_DOTSLASH + _constants.SMAPI_XMLNODE_FIRMWARE_MIN,
    SMAPI_XPATH_FIRMWARE_MAX: _constants.SMAPI_XMLCHAR_DOTSLASH + _constants.SMAPI_XMLNODE_FIRMWARE_MAX,
    
    /*******************************************************************************
     **   WIRELESS CONSTANTS
     *******************************************************************************/
    WLS_SSID_LOOKUP_IMAGE: "lookup_image",
    WLS_LOOKUP_SSID: "WLS_LOOKUP_SSID",
    WLS_LOOKUP_MACADDR: "WLS_LOOKUP_MACADDR",
    WLS_LOOKUP_SERIALNUM: "WLS_LOOKUP_SERIALNUM",
    WLS_WEP64_KEYLENGTH: 10,
    WLS_WEP128_KEYLENGTH: 26,
    
    
    // Security Mode or Beacon type in TR64
    WLS_SECMODE_NONE: 0x00000001,
    WLS_SECMODE_BASIC: 0x00000002,
    WLS_SECMODE_WPA: 0x00000004,
    WLS_SECMODE_WPA2: 0x00000008, // also called 11i
    // Encryption
    WLS_ENCRYPT_NONE: 0x00000001,
    WLS_ENCRYPT_WEP: 0x00000002,
    WLS_ENCRYPT_TKIP: 0x00000004,
    WLS_ENCRYPT_AES: 0x00000008,
    
    // Authentication
    WLS_AUTH_OPEN: 0x00000001,
    WLS_AUTH_EAP: 0x00000002,
    WLS_AUTH_PSK: 0x00000004,
    
    // IEEE 802.11 standard
    WLS_STD_802_11A: 0x00000001,
    WLS_STD_802_11B: 0x00000002,
    WLS_STD_802_11G: 0x00000004,
    
    // SSID
    WLS_SSID_NAME: 0x00000001,
    WLS_SSID_BROADCAST: 0x00000002,
    
    /*******************************************************************************
     **   ACS Info condstants and structure
     *******************************************************************************/
    // Authentication used by CPE when ACS requests a connection
    CWMP_AUTH_NONE: 0x00000001,
    CWMP_AUTH_BASIC: 0x00000002,
    CWMP_AUTH_DIGEST: 0x00000004
  })
})();

var _constants = $ss.agentcore.constants;
(function()
{
  $.extend($ss.agentcore.smapi.utils, {
    /******************************************************************************/
    WLSSecurityLevel: function(){
      this.id = "";
      this.sDisplayName = "";
      this.oSecurity = new _smapiutils.WLSSecurity();
      this.bRecommended = false;
      this.nOrdinal = 0;
      this.bHide = false;
      this.bFallback = false;
    },
    /******************************************************************************/
    WLSSecurityKeys: function() // all default values mean no operation to perform for those values
    {
      this.sWEPKey0 = _constants.SMAPI_STR_DONT_CARE;
      this.sWEPKey1 = _constants.SMAPI_STR_DONT_CARE;
      this.sWEPKey2 = _constants.SMAPI_STR_DONT_CARE;
      this.sWEPKey3 = _constants.SMAPI_STR_DONT_CARE;
      this.sPreSharedKey = _constants.SMAPI_STR_DONT_CARE; /* 5 or 10 ascii used in WPA */
      this.sKeyPassphrase = _constants.SMAPI_STR_DONT_CARE; /* 8 to 63 ascii used in WEP */
      this.oUserDefined = _constants.SMAPI_OBJ_DONT_CARE; // for future expansion or use with custom implementations
    },
    
    /******************************************************************************/
    WLSSecurity: function() // all default values mean no operation to perform for those values
    // leave settings in modem as is
    {
      this.ulSecMode = _constants.SMAPI_HEX_DONT_CARE;
      this.ulAuthentication = _constants.SMAPI_HEX_DONT_CARE;
      this.ulEncryption = _constants.SMAPI_HEX_DONT_CARE;
      this.oKeys = new _smapiutils.WLSSecurityKeys();
      this.oUserDefined = _constants.SMAPI_OBJ_DONT_CARE; // for future expansion or use with custom implementations
    },
    /******************************************************************************/
    WLSSSID: function() // all default values mean no operation to perform for those values
    // leave settings in modem as is
    {
      this.sSSID = _constants.SMAPI_STR_DONT_CARE; // Server Set ID
      this.nSSIDBroadcast = _constants.SMAPI_INT_DONT_CARE; // 1:On / 0: Off
    },
    
    /******************************************************************************/
    WLSDataTransmission: function() // all default values mean no operation to perform for those values
    // leave settings in modem as is
    {
      this.sBasicTrxRates = _constants.SMAPI_STR_DONT_CARE; // comma separated string for unicast, multicast 
      // and broadcast frames in (Mbps)
      this.sOperationalTrxRates = _constants.SMAPI_STR_DONT_CARE; // comma separated string for unicast frames is a
      // superset of sBasicTrxRates in (Mbps)
      this.sPossibleDataTrxRate = _constants.SMAPI_STR_DONT_CARE; // a subset of sOperationalTrxRates. Rates for 
      // unicast frames at which an access point will
      // permit a station to connect (Mbps)
    },
    
    /******************************************************************************/
    WLSConfig: function() // all default values mean no operation to perform for those values
    // leave settings in modem as is
    {
      this.nEnable = _constants.SMAPI_INT_DONT_CARE; // set wireless radio mode in other words 
      // enable/disable wireless interface 1:on/0:off 
      this.sMaxBitRate = _constants.SMAPI_STR_DONT_CARE; // Auto, OR the largest of the OperationalTrxRates 
      // values in (Mbps)
      this.nChannel = _constants.SMAPI_INT_DONT_CARE; // set wireless channel
      this.ulStandard = _constants.SMAPI_HEX_DONT_CARE; // sets the IEEE 802.11 standard the device is operating in
      this.nEnableMACL = _constants.SMAPI_INT_DONT_CARE; // set MAC Address control filtering 1:on/0:off
      this.oWLSDataTransmission = _constants.SMAPI_OBJ_DONT_CARE;
      // new WLSDataTransmission(); 
      // In general used for setting frame transmission rates. 
      this.oWLSSSID = _constants.SMAPI_OBJ_DONT_CARE; // new WLSSSID(); set SSID Info
      this.oWLSSecurity = _constants.SMAPI_OBJ_DONT_CARE; // new WLSSecurity();   set wireless security                                          
      this.oUserDefined = _constants.SMAPI_OBJ_DONT_CARE; // for future expansion or use with custom implementations
    },
    /******************************************************************************/
    CWMPInfo: function() //Support for CPE WAN Management Protocol
    {
      this.sACSUrl = _constants.SMAPI_STR_DONT_CARE; // indicates the Url of the management server (ACS)
      this.ulACSAuthType = _constants.SMAPI_HEX_DONT_CARE; // Authentication used by ACS when CPE communicates
      this.sACSUsername = _constants.SMAPI_STR_DONT_CARE; // set/get Username that ACS will use to Authenticate CPE
      this.sACSPassword = _constants.SMAPI_STR_DONT_CARE; // set/get Password that ACS will use to Authenticate CPE
      this.ulCPEAuthType = _constants.SMAPI_HEX_DONT_CARE; // Authentication used by CPE when ACS communicates
      this.sCPEUsername = _constants.SMAPI_STR_DONT_CARE; // set/get Username that CPE will use to Authenticate ACS
      this.sCPEPassword = _constants.SMAPI_STR_DONT_CARE; // set/get Password that CPE will use to Authenticate ACS
      this.oUserDefined = _constants.SMAPI_OBJ_DONT_CARE; // for future expansion or use with custom implementations
    },
    g_oSMAPIDependencies: {},
    
    
    MapDependencies: function(){
      this.g_oSMAPIDependencies.Sleep = function(nTime){return _utils.Sleep(nTime);};
      this.g_oSMAPIDependencies.FileExists = function(filename){return _file.FileExists(filename);};
      this.g_oSMAPIDependencies.LoadDOM = function(sUrl, bAsync, fnHandler){return _xml.LoadXML(sUrl, bAsync, fnHandler);};
      this.g_oSMAPIDependencies.ParseMacros = function(sStr,bRefresh,sSnapinName){return _config.ParseMacros(sStr,bRefresh,sSnapinName);};
      this.g_oSMAPIDependencies.PersistValue = function(sClassName, sPropName, sPropValue){return _SI.PersistInfo(sClassName, sPropName, sPropValue);};
      this.g_oSMAPIDependencies.GetPersistedValue = function(sClassName, sPropName, sDefault){return _SI.ReadPersistInfo(sClassName, sPropName, sDefault);};
      this.g_oSMAPIDependencies.GetValue = function(strName){return _databag.GetValue(strName);};
      this.g_oSMAPIDependencies.SetValue = function(strName, strValue,bOverwrite){return _databag.SetValue(strName, strValue,bOverwrite);};
      this.g_oSMAPIDependencies.RemoveValue = function(strName){return _databag.RemoveValue(strName);};
      this.g_oSMAPIDependencies.ParseLocMacros = function(sStr,bRefresh,sSnapinName){return _config.ParseMacros(sStr,bRefresh,sSnapinName); };////
      this.g_oSMAPIDependencies.GetCfgValue = function(section, key, defaultVal){return _config.GetConfigValue(section, key, defaultVal);};
      this.g_oSMAPIDependencies.SetUserValue = function(section, name, value, valuetype){return _config.SetUserValue(section, name, value, valuetype);};
      this.g_oSMAPIDependencies.SetMachineValue = function(section, name, value, valuetype){return _config.SetMachineValue(section, name, value, valuetype);};
      this.g_oSMAPIDependencies.GetMachineValue = function(section, name){return _config.GetConfigFromHKLM(section, name);};
      
      return true;
    },

     /**
      *  @name SetType 
      *  @memberOf $ss.snapin.smapi.utils
      *  @function
      *  @description     Sets modem type by value passed
      * @param     modem type to set
      */    
    SetType: function(sType){
      this.g_oSMAPIDependencies.PersistValue(_SIconstants.MODEM_CLASS, _SIconstants.TYPE, sType);
    },
    
     /**
      *  @name SetName 
      *  @memberOf $ss.snapin.smapi.utils
      *  @function
      *  @description      Sets modem name by value passed
      * @param    modem name to set
      */     
    SetName: function(sName){
      this.g_oSMAPIDependencies.PersistValue(_SIconstants.MODEM_CLASS, _SIconstants.MODEM_NAME, sName);
    },

     /**
      *  @name SetConnType 
      *  @memberOf $ss.snapin.smapi.utils
      *  @function
      *  @description      Sets modem connection type by value passed
      * @param     modem connection type to set
      */    
    SetConnType: function(sConnType){
      this.g_oSMAPIDependencies.PersistValue(_SIconstants.MODEM_CLASS, _SIconstants.CONN_TYPE, sConnType);
    },

     /**
      *  @name SetCode 
      *  @memberOf $ss.snapin.smapi.utils
      *  @function
      *  @description       Sets global code with the value specified.
      * @param    modem code to set
      */     
    SetCode: function(code){
      _ModemCode = code;
      this.g_oSMAPIDependencies.PersistValue(_SIconstants.MODEM_CLASS, _SIconstants.MODEM_CODE, code);
    },
    
    
    /**
      *  @name SetModemCodeToEmpty 
      *  @memberOf $ss.snapin.smapi.utils
      *  @function
      *  @description       Sets the code to be an empty string to enable autodetect in the same session
      * @param    modem code to set
      */     
    SetModemCodeToEmpty: function(){
      _ModemCode = _constants.SMAPI_EMPTY_STR;
      _SI.PersistInfo(_SIconstants.MODEM_CLASS, _SIconstants.MODEM_CODE, _ModemCode);
    },
    
    
     /**
      *  @name SetCodeById 
      *  @memberOf $ss.snapin.smapi.utils
      *  @function
      *  @description     Gets the code from global variable or registry. When no code is found it returns null modem if autodetect if OFF else blank
      * @param    id : ID to match with modem xml ID
      *  @returns  set code name else empty
      */ 
    SetCodeById: function(id){
      if (id == _constants.SMAPI_EMPTY_STR) 
        return id;
      var code = _constants.SMAPI_EMPTY_STR;
      try {
        if (!this._GetModemDOM()) 
          return code;
        var oNode = null, oNodes = null;
        oNodes = _ModemDOM.selectNodes('//modem');
        for (var i = 0; i < oNodes.length; i++) {
          oNode = oNodes[i].selectSingleNode('./instance[@id="common"]/id');
          if (oNode != null) {
            if (oNode.text == id) {
              var code = oNodes[i].getAttribute("code");
              this.SetCode(code);
              break;
            }
          }
        }
      } 
      catch (ex) {
        _logger.error("sma_mut_SetCodeById", ex.message);
      }
      return code;
    },
    
     /**
      *  @name GetCode 
      *  @memberOf $ss.snapin.smapi.utils
      *  @function
      *  @description     Gets the code from global variable or registry. When no code is found it returns null modem if autodetect if OFF else blank
      *  @returns   modem Code
      */      
    GetCode: function(){
      var modemCode = (_ModemCode != _constants.SMAPI_EMPTY_STR) ? _ModemCode : _constants.SMAPI_EMPTY_STR;
      try {
        if (modemCode == _constants.SMAPI_EMPTY_STR) {
          // get it from registry 
          modemCode = this.g_oSMAPIDependencies.GetPersistedValue(_SIconstants.MODEM_CLASS, _SIconstants.MODEM_CODE); 
          if (modemCode == _constants.SMAPI_EMPTY_STR) {
            //check config to see if autodetect is turned on
            var nAD = Number(_config.GetConfigValue("modems", "autodetect", "0"));
            if (nAD == 0) { // autodetect is turned off
              modemCode = _constants.SMAPI_DEFAULT_MODEM;
              this.SetCode(modemCode);
              return modemCode;
            }
          }
        }
      } 
      catch (ex) {
        _logger.error("sma_mut_GetCode", "Enable to get modemCode, error: " + ex.message)
      }
      _ModemCode = modemCode;
      return modemCode;
    },
    
     /**
      *  @name GetAllCodes 
      *  @memberOf $ss.snapin.smapi.utils
      *  @function
      *  @description     Gets all modem codes from modems xml.
      *  @returns   Object instance
      */   
    GetAllCodes: function(){
      var valArray = new Array();
      // now check if we have modem xml loaded
      try {
        if (this._GetModemDOM()) {
          var oNodeList = null;
          var expr = "//sprt_modemlist/modem[@code]";
          oNodeList = _ModemDOM.selectNodes(expr);
          if (oNodeList != null) {
            for (var i = 0; i < oNodeList.length; i++) 
              valArray.push(oNodeList[i].getAttribute("code"));
          }
        }
      } 
      catch (ex) {
        _logger.error("sma_mut_GetAllCodes", ex.message);
      }
      return valArray;
    },

     /**
      *  @name MaskIn 
      *  @memberOf $ss.snapin.smapi.utils
      *  @function
      *  @description     test to see if specified flag(s) is set in the value
      * @param    ulValue  - value to be tested on
      * @param    ulFlag   - bit(s) in question
      *  @returns    boolean
      */
    MaskIn: function(ulValue, ulFlag){
      return ((ulValue & ulFlag) == ulFlag);
    },
    
    /********************************************************************************
     **     private helper functions
     ********************************************************************************/
     _GetModemSnapinPath: function(){
      if (_modemSnapinPath !== "") {
        // get modem snapin name from configuration
        var _modemSnapinName = this.g_oSMAPIDependencies.GetCfgValue("global","modem_def_snapin_holder","snapin_modem");
        var _modemSnapinPath = $ss.getSnapinAbsolutePath(_modemSnapinName);
        if (!_modemSnapinPath) {
          // error finding the modem snapin path
          _logger.debug("_GetModemSnapinPath()", "Error in loading file");
        }       
      }
      return _modemSnapinPath;
    },
     _GetModemSnapinConfigDOM: function(){
      if (!_supportedModemsDOM) {
        // get modem file name from configuration
         var sFile = this._GetModemSnapinPath() + "\\" + this.g_oSMAPIDependencies.GetCfgValue("snapin_modem","supported_modem_xml","modems.xml");
        _supportedModemsDOM = this.g_oSMAPIDependencies.LoadDOM(sFile);
        if (!_supportedModemsDOM) {
          // error in loading may be the file is not present
          _logger.debug("_GetModemSnapinConfigDOM", "Error in loading file");
          return null;
        }
        
      }
      return _supportedModemsDOM;
    },

    _GetAllSupportedModemsCodes: function(){
      var valArray = new Array();
      // now check if we have modem xml loaded
      try {
        if (this._GetModemSnapinConfigDOM()) {
          var oNodeList = null;
          var expr = "//sprtModemlist/modemSupported[@code]";
          oNodeList = _supportedModemsDOM.selectNodes(expr);
          if (oNodeList != null) {
            for (var i = 0; i < oNodeList.length; i++) 
              valArray.push(oNodeList[i].getAttribute("code"));
          }
        }
      } 
      catch (ex) {
        _logger.error("_GetAllSupportedModemsCodes", ex.message);
      }
      return valArray;
    } , 
     /**
      *  @name _GetModemDOM 
      *  @memberOf $ss.snapin.smapi.utils
      *  @function
      *  @description   Check global object and if null then load modem file
      * @param    The file to load
      *  @returns    A DOM object if success, otherwise null
      */
    _GetModemDOM: function(){
      if (!_ModemDOM) {
       //get the base file of the list of supported modems
        var xmlSupportedModemsDOM = this._GetModemSnapinConfigDOM();
        var arrSupportedModemCodes = this._GetAllSupportedModemsCodes();
        var exprForModemPath = _constants.SMAPI_XMLCHAR_DOTSLASH + "configPath";
        var arFiles = [];
        for(var i=0; i<arrSupportedModemCodes.length; i++) {
          var expr  = "//modemSupported[@code=\""+ arrSupportedModemCodes[i] + "\"]";
          var xmlSupportedModemNode = xmlSupportedModemsDOM.selectSingleNode(expr); 
          if(xmlSupportedModemNode)
           var modemXmlPath = this._GetModemSnapinPath() + "\\" + xmlSupportedModemNode.selectSingleNode(exprForModemPath).text;
           arFiles.push(modemXmlPath);
        }
        _ModemDOM =  _xml.AugmentDOMs(null, arFiles, "", "//modem/");  // augment the DOM's with the null modem dom as first pref.
        if (!_ModemDOM) {
          // error in loading may be the file is not present
          _logger.debug("_GetModemDOM", "Error in loading file");
          return null;
        }
      }
      return _ModemDOM;
    }
  });
  var _ModemDOM = null;
  var _supportedModemsDOM = null
  var _ModemCode = _constants.SMAPI_EMPTY_STR;
  var _modemSnapinPath = _constants.SMAPI_EMPTY_STR;
  var _SIconstants = $ss.agentcore.constants.smartissue;
  var _utils = $ss.agentcore.utils;
  var _file = $ss.agentcore.dal.file;
  var _databag = $ss.agentcore.dal.databag;
  var _xml = $ss.agentcore.dal.xml;
  var _SI = $ss.agentcore.diagnostics.smartissue;
  var _smapiutils = $ss.agentcore.smapi.utils;
  var _logger = $ss.agentcore.log.GetDefaultLogger("$ss.agentcore.smapi.utils");

})()  