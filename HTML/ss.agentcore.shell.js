    /// <reference path="jquery.js"/>
    /// <reference path="jquery.include.js"/>

    /** @namespace */
    $ss = $ss ||
    {};



    /*******************************************************************************
     **  File Name: ss.agentcore.shell.js
     **
     **  Summary: SupportSoft Shell Functions
     **
     **  Description: This file contains the functions to initialize and configure
     **               the dynamic layouts and snapins
     **
     **
     **  Copyright SupportSoft Inc. 2008, All rights reserved.
     *******************************************************************************/
    if (!window.sprt) {
      /**
       @namespace
       All objects live in the sprt namespace, which is also available in the
       abbreviation $ss.
       */
      if (typeof(self) != 'undefined') {
        var shellwindow = typeof(shellwindow) != 'undefined' ? shellwindow : self;
      }
    }

    (function() {
      //extend the $ss package. The following functions will be exposed as public functions.

      $.extend($ss, {
        /**
        *  @name Initialize
        *  @memberOf $ss.agentcore.shell
        *  @function
        *  @description  Function initializes the packages, snapins and layouts from the
        *               respective config files.
        *  @returns void
        *  @example
        *
        *
        */
        Initialize: function() {
          //read the query string passed as a part of the  url
          _logger.info("Initialize: Entered Initalize Function");
          var _ini = $ss.agentcore.dal.ini;
          var sUrl = window.location.href;
          //trap all global unhandled error
          window.onerror = $ss.ErrorTrap;
          //[MAC] Check Machine OS Windows/MAC
          if(!bMac)
            var contentPath = window.external.ExpandSysMacro(_ini.GetIniValue("", "SETUP", "contentPath", ""));
          else {
            //=======================================================
            //[MAC] Native Call
            var jsBridge = window.JSBridge;
            var argument = _ini.GetIniValue("", "SETUP", "contentPath", "");
            var message = {'JSClassNameKey':'File','JSOperationNameKey':'ExpandSysMacro', 'JSArgumentsKey':[argument], 'JSISSyncMethodKey' : '1'}
            var result = jsBridge.execute(message);
            var parsedJSONObject = JSON.parse(result);
            var contentPath = parsedJSONObject.Data;
            //=======================================================
          }
          //remove trailing /

          //[MAC] Check Machine OS Windows/MAC
          if(!bMac) {
            _logger.info("Initialize: Using contentpath %1%", contentPath);
            window.startingpoint = contentPath.replace(new RegExp(/\\$/), "") + "\\"; // make sure it has a trailing path
            var installConfigFile = window.external.ExpandSysMacro(_ini.GetIniValue("", "SETUP", "installconfigfile", ""));
            _logger.info("Initialize: Using configFile %1%", installConfigFile);
            var configToUse = window.external.ExpandSysMacro(_ini.GetIniValue("", "SETUP", "configtouse", "clientui"));
           _logger.info("Initialize: Using config %1%", configToUse);
          }
          else {
            window.startingpoint = contentPath.replace(new RegExp(/\\$/), "") + "/"; // make sure it has a trailing path
            argument = _ini.GetIniValue("", "SETUP", "installconfigfile", "");
            message = {'JSClassNameKey':'File','JSOperationNameKey':'ExpandSysMacro', 'JSArgumentsKey':[argument], 'JSISSyncMethodKey' : '1'}
            result = jsBridge.execute(message);
            parsedJSONObject = JSON.parse(result);
            var installConfigFile = parsedJSONObject.Data;
            // var installConfigFile = window.external.ExpandSysMacro(_ini.GetIniValue("", "SETUP", "installconfigfile", ""));

            argument = _ini.GetIniValue("", "SETUP", "configtouse", "clientui");
            message = {'JSClassNameKey':'File','JSOperationNameKey':'ExpandSysMacro', 'JSArgumentsKey':[argument], 'JSISSyncMethodKey' : '1'}
            result = jsBridge.execute(message);
            parsedJSONObject = JSON.parse(result);
            var configToUse = parsedJSONObject.Data;
            _logger.info("Initialize: Using window.startingpoint %1%", window.startingpoint);
          }
          if (configToUse) {
            $ss.agentcore.dal.config.SetConfigToUse(configToUse);
          }
          _SetAttribute({
            sConfigToUse: configToUse,
            startingpoint: window.startingpoint,
            installConfigFile: installConfigFile
          });


          if (installConfigFile) {
            try {
              if (_file.FileExists(installConfigFile)) {
                $ss.agentcore.dal.config.Initialize(installConfigFile);
              }
            }
            catch (e) {
              _logger.error("Initialize: While Initalizing Config File %1%", e.message);
            }
          };
          _SetIcon();
          _UpdateLogLevel();
          _DisableBrowserFeatures(document);
          _LoadDataBag();
          //first check if bcont is running 
          //see if update manager has run, if true, try to delete the
          //folder and copy the sprt_package, sprt_layout, sprt_snapin to 
          //back up folder. - Verify if it is needed now after bcont modification

          //once it is done, use the base refernce path for the application to 
          //back-up folder. 

          //look for the packages with highest weightage
          //set that as the default package
          var hasPackage = _ConfigPackage();
          //read the layout folders and set the config files
          var hasLayout = false;
          if (hasPackage) {
            hasLayout = _ConfigLayout();
          }
          else {
            _SetShellError(_const.SHELL_PACKAGE_NOTFOUND);
            _logger.fatal("Initialize: Package Not Found ");
          }
          //read the snapin folders and update config data in default values.
          var hasSnapin = false;
          if (hasLayout) {
            hasSnapin = _ConfigSnapin();
          }
          else {
            _SetShellError(_const.SHELL_LAYOUT_NOTFOUND);
            _logger.fatal("Initialize: Layout Not Found");
          }

          if (!hasSnapin) {
            _SetShellError(_const.SHELL_SNAPIN_NOTFOUND);
            _logger.fatal("Initialize: Snapin Not Found");
          }
          //called twice to make sure if the log level has been modified by any other snapin/layout config
          // reset it accordingly
          _UpdateLogLevel();

          //initalize and load the global resources
          $ss.agentcore.utils.LoadGlobalResourceBundle();

          _SetBroadCastEvents();


          //[MAC] Check Machine OS Windows/MAC
          if(!bMac)
            _ShowAgent();
          else {
            this.loadAllResources(filesToInclude).then(function() {
            for (var i = 0; i < snapinsNames.length ; i++) {
              MVC.Controller._ControllerStatus[snapinsNames[i]] = true;
            }
            _ShowAgent();
            });
          }
        },

        loadAllResources:function (jsFiles) {
        //[MAC] Check Machine OS Windows/MAC
        if(bMac) {
          var that = this;
          return jsFiles.reduce(function(prev, current) {
          return prev.then(function() {
            return that.loadScript(current);
          });
          }, Promise.resolve());
        }
        },

        loadScript:function (url){
        //[MAC] Check Machine OS Windows/MAC
        if(bMac) {
          var jsBridge = window.JSBridge;
          var message = {'JSClassNameKey':'File','JSOperationNameKey':'GetNativePath', 'JSArgumentsKey':[url], 'JSISSyncMethodKey' : '1'}
          var result = jsBridge.execute(message);  
          var parsedJSONObject = JSON.parse(result);     
          url = parsedJSONObject.Data;
              
          return new Promise(function(resolve, reject) {

          var script = document.createElement("script")
          script.type = "text/javascript";

          if (script.readyState){  //IE
            script.onreadystatechange = function(){
            if (script.readyState == "loaded" ||
              script.readyState == "complete"){
                script.onreadystatechange = null;
                resolve();
              }
            };
          } else {  //Others
            script.onload = function(){
              resolve();
            };
          }
          script.src = url;
          document.getElementsByTagName("head")[0].appendChild(script);
          }); 
        }
        },
        GetAttribute: function(name) {
          return _GetAttribute(name);
        },
        //TO BE DEPRICATED
        getAttribute: function(name) {
          return _GetAttribute(name);
        },

        SetAttribute: function(options) {
          _SetAttribute(options);
        },
        //TO BE DEPRICATED
        setAttribute: function(options) {
          _SetAttribute(options);
        },
        GetShellLang: function() {
          return _GetAttribute("sLang");
        },
        GetLayoutIncludes: function() {
          return _GetAttribute("oSprtLayout").aIncludes;
        },
        //TO BE DEPRICATED
        getLayoutIncludes: function() {
          return _GetAttribute("oSprtLayout").aIncludes;
        },

        GetLayoutStyles: function() {
          return _GetAttribute("oSprtLayout").aStyles;
        },
        //TO BE DEPRICATED
        getLayoutStyles: function() {
          return _GetAttribute("oSprtLayout").aStyles;
        },

        GetSnapinStyles: function() {
          return _GetAttribute("oSprtSnapin").aStyles;
        },

        GetSnapinIncludes: function() {
          return _GetAttribute("oSprtSnapin").aIncludes;
        },
        //TO BE DEPRICATED
        getSnapinIncludes: function() {
          return _GetAttribute("oSprtSnapin").aIncludes;
        },

        GetLayoutControllers: function() {
          return _GetAttribute("oSprtLayout").aControllers;
        },
        //TO BE DEPRICATED
        getLayoutControllers: function() {
          return _GetAttribute("oSprtLayout").aControllers;
        },

        GetSnapinControllers: function() {
          return _GetAttribute("oSprtSnapin").aControllers;
        },
        //TO BE DEPRICATED
        getSnapinControllers: function() {
          return _GetAttribute("oSprtSnapin").aControllers;
        },

        GetLayoutAbsolutePath: function() {
          return _GetAttribute("oSprtLayout").sAbsPath;
        },

        GetLayoutSkinPath: function() {
          return _GetAttribute("oSprtLayout").sSkinPath;
        },
        //TO BE DEPRICATED
        getLayoutAbsolutePath: function() {
          return _GetAttribute("oSprtLayout").sAbsPath;
        },
        GetSnapinAbsolutePath: function(sName) {
          var tempSnapinGUID = _GetAttribute("oSprtSnapin").aNames[sName];
          var tempOSnapin = _GetAttribute("oSprtSnapin").oAvailableSnapins[tempSnapinGUID];
          if (tempOSnapin) {
            //[MAC] Check Machine OS Windows/MAC
            if(bMac) {
              var jsBridge = window.JSBridge;
              var message = {'JSClassNameKey':'File','JSOperationNameKey':'GetNativePath', 'JSArgumentsKey':[tempOSnapin.absPath], 'JSISSyncMethodKey' : '1'}
              var result = jsBridge.execute(message);
              if (result != undefined) {
                var parsedJSONObject = JSON.parse(result);
                var res = parsedJSONObject.Data;
                return res;
              }              
            }
            return tempOSnapin.absPath;
          }
          else {
            return "";
          }
        },
        //TO BE DEPRICATED
        getSnapinAbsolutePath: function(sName) {
          return this.GetSnapinAbsolutePath(sName);
        },

        GetSnapinFlows: function() {
          return _GetAttribute("oSprtSnapin").aFlows;
        },
        //TO BE DEPRICATED
        getSnapinFlows: function() {
          return _GetAttribute("oSprtSnapin").aFlows;
        },
        GetSnapinRegistrationFiles: function() {
          return _GetAttribute("oSprtSnapin").aRegs;
        },
        GetSnapin: function(sName) {
          //[MAC] Check Machine OS Windows/MAC
          if(!bMac) {
            if (_GetAttribute("oSprtSnapin").aNames[sName])
              return _GetAttribute("oSprtSnapin").oAvailableSnapins[_GetAttribute("oSprtSnapin").aNames[sName]];
          }
          else {     
            var tempSnapinGUID = _GetAttribute("oSprtSnapin").aNames[sName];
            var tempOSnapin = _GetAttribute("oSprtSnapin").oAvailableSnapins[tempSnapinGUID];
            _logger.info('snapinName= ' + sName + ' value = ' + tempOSnapin);
            if (tempOSnapin) {
              return tempOSnapin;
            }
            else {
              return "";
            }  
          }
          return null;
        },
        //TO BE DEPRICATED
        getSnapin: function(sName) {
          return this.GetSnapin(sName);
        },
        GetSnapinResource: function(sSnapinName) {
          var sSnapin = this.GetSnapin(sSnapinName);
          if (sSnapin) {
            return sSnapin.resources;
          }
        },
        GetLayoutResource: function() {
          return _GetAttribute("oSprtLayout").aResources;
        },
        GetShellError: function() {
          return _shellError;
        },
        ErrorTrap: function(sMsg, sPage, sLine) {
          var sErrmsg = "An application error has occurred...";
          var sLangCode = $ss.agentcore.utils.GetLanguage();
          sErrmsg = _config.GetConfigValue_Ex("agentcore_messages",sLangCode, "ErrorMsg" ,sErrmsg); 
          var sErrorMsg = sErrmsg + "\n" +
          "Error: " +
          sMsg +
          "\n" +
          "Page: " +
          sPage +
          "\n" +
          "Line: " +
          sLine +
          "\n";

          // Suppress error generated by minibcont popup (only notification) and close it.
          if ($ss.GetAttribute("sConfigToUse") === "minibcont") {
            _logger.fatal("ErrorTrap: Unhandled Error: causing minibcont to close %1%", sErrorMsg);
            window.external.Close();
            event.returnValue = true;
            return true;
          }
          
          _logger.error("ErrorTrap: Unhandled Error: %1%", sErrorMsg);
          var g_shi_AlertOnError = (_config.GetConfigValue("global", "alert_unhandled_error", "false") === "true");
          if (g_shi_AlertOnError)
            alert(sErrorMsg);
          event.returnValue = true;
          return true;

        }

        //end of publicly exposed functions
      });
      //end of jQuery extension use

      function _GetAttribute(sAttributeName) {
        return _attributes[sAttributeName];
      };

      function _SetAttribute(options) {
        jQuery.extend(true, _attributes, options);
      };
      function _SetShellError(sErrorCode) {
        _shellError.push(sErrorCode);
      };

      function _SetBroadCastEvents() {
        //Set the broadcast on new sync to clear content cache  
        $ss.agentcore.events.Subscribe("BROADCAST_ON_SYNCNEWDATA", $ss.agentcore.dal.content.ClearContentCache);
        //$ss.agentcore.events.Subscribe("BROADCAST_ON_PRUNEDONE", $ss.agentcore.dal.content.ClearContentCache);
      };

      function _ConvertArrayToFolderList(aLatestVersionOfContent) {
        //Format the list as "ContentId.Version" (Folder List)
        var sContentGUID, i = 0, aFolderList = [];
        for (sContentGUID in aLatestVersionOfContent) {
          //[MAC] Check Machine OS Windows/MAC
          if(!bMac)
            aFolderList[i++] = sContentGUID + "." + aLatestVersionOfContent[sContentGUID];
          else {
            if (sContentGUID !== "containsMatch" && sContentGUID !== "contains") 
            {
               aFolderList[i] = sContentGUID + "." + aLatestVersionOfContent[sContentGUID];
               _logger.info('_ConvertArrayToFolderList = %1% and %2%',aFolderList[i],sContentGUID);
               i++;
            }else {
              _logger.info('Miss match');
            }
          }
        }
        return aFolderList;
      }

      function _RemoveOlderVersionOfEachContent(aExistingContentList) {
        //Remove if there are any un-pruned contents from the List of Folders
        var aLatestVersionOfContent = [];
        for (var i = 0; i < aExistingContentList.length; i++) {
          var ContentDetails = aExistingContentList[i].split(".");
          var sContentGUID = ContentDetails[0];
          var sCurrContVer = ContentDetails[1];
          var sExistingContVer = aLatestVersionOfContent[sContentGUID];
          if (!sExistingContVer) {
            aLatestVersionOfContent[sContentGUID] = sCurrContVer;
          } else if (parseInt(sCurrContVer) > parseInt(sExistingContVer)) {
            aLatestVersionOfContent[sContentGUID] = sCurrContVer;
          }
        }
        return _ConvertArrayToFolderList(aLatestVersionOfContent);
      }

      function _ConfigPackage() {
        try {
          var hasPackage = false;
          var sBasePackageFolder = "sprt_package";
          //[MAC] Check Machine OS Windows/MAC
          if(!bMac)
            var sBasePackageFolderAbsPath = _GetAttribute('startingpoint') + "\\" + sBasePackageFolder;
          else
            var sBasePackageFolderAbsPath = _GetAttribute('startingpoint')  + sBasePackageFolder;
            var sSprtPackageToUse = "";
            if (_file.FolderExists(sBasePackageFolderAbsPath)) {
              var aFolderList = _file.GetSubFoldersList(sBasePackageFolderAbsPath);
              aFolderList = _RemoveOlderVersionOfEachContent(aFolderList);
              aFolderList = _content.RemovePruneDelayedContentsIn(aFolderList);
              if (aFolderList && aFolderList.length) {
                var maxWeight = -100;
                //[MAC] Check Machine OS Windows/MAC
              if(!bMac)
                var weightScoreXpath = "//field[@name='Priority']/value[@name='sccf_field_value_int']";
              else
                var weightScoreXpath = "/sprt/content-set/content/field-set/field[@name='Priority']/value[@name='sccf_field_value_int']";
              for (var i = 0; i < aFolderList.length; i++) {
                //[MAC] Check Machine OS Windows/MAC
                if(!bMac)
                  var tempDescXML = sBasePackageFolderAbsPath + "\\" + aFolderList[i] + "\\" + aFolderList[i] + ".xml"
                else 
                var tempDescXML = sBasePackageFolderAbsPath + "/" + aFolderList[i] + "/" + aFolderList[i] + ".xml"
                var tempWeight = -100;
                try {
                  var tempPackageDescXML = _xml.LoadXML(tempDescXML, false);
                  var oWeightNode = _xml.GetNodes(weightScoreXpath, "", tempPackageDescXML);
                  //[MAC] Check Machine OS Windows/MAC
                  if(!bMac) {
                    if (oWeightNode && oWeightNode.length)
                      tempWeight = parseInt(oWeightNode[0].text.toString()); 
                    }
                    else {
                      var weight = oWeightNode[0].childNodes[0].nodeValue;
                      if (oWeightNode && oWeightNode.length && weight.length) {
                        // tempWeight = parseInt(oWeightNode[0].text.toString());
                        // tempWeight = parseInt(oWeightNode[0].childNodes[0].nodeValue);
                        tempWeight = parseInt(weight);
                      }
                    }
                    if (tempWeight > maxWeight) {
                      maxWeight = tempWeight;
                      sSprtPackageToUse = aFolderList[i];
                      hasPackage = true;
                    }
                }
                catch (e) {
                  //do nothing
                  _logger.error("_ConfigPackage: Exception : %1% ", e.message);
                }
              }

              //sSprtPackageToUse = aFolderList[0];
            }
            else {
              return hasPackage;
              // TODO - Decide what to do if the package is not available
            }
          }
          //[MAC] Check Machine OS Windows/MAC
          if(!bMac) {
            var sPackageRelPath = sBasePackageFolder + "\\" + sSprtPackageToUse;
            var sPackageAbsPath = sBasePackageFolderAbsPath + "\\" + sSprtPackageToUse;
            var sPackageDescXmlPath = sPackageAbsPath + "\\" + sSprtPackageToUse + ".xml";
          }
          else {
            var sPackageRelPath = sBasePackageFolder + "/" + sSprtPackageToUse;
            var sPackageAbsPath = sBasePackageFolderAbsPath + "/" + sSprtPackageToUse;
            var sPackageDescXmlPath = sPackageAbsPath + "/" + sSprtPackageToUse + ".xml";
          }
          var aLayoutGUIDToUse = [];
          var aSnapinGUIDToUse = [];
          var aSnapinNameToUse = [];

          //get the layout guid to be used
          //[MAC] Check Machine OS Windows/MAC
          if(!bMac)
            var sLayoutDescFile = sPackageAbsPath + "\\Layouts.html";
          else
            var sLayoutDescFile = sPackageAbsPath + "/Layouts.html";

          var oLayoutDescDOM = _xml.LoadXML(sLayoutDescFile, false);

          if (oLayoutDescDOM) {
            //[MAC] Check Machine OS Windows/MAC
           if(!bMac)
             var sXpath = "//layouts/layout";
           else
             var sXpath = "layouts/layout";
           
            var oNodes = _xml.GetNodes(sXpath, "", oLayoutDescDOM);
            if (oNodes && oNodes.length) {
              for (var k = 0; k < oNodes.length; k++) {
                var tmp = _xml.GetAttribute(oNodes[k], 'guid')
                if (!tmp) {
                  tmp = _xml.GetAttribute(oNodes[k], 'id');
                }
                aLayoutGUIDToUse.push(tmp);
              }
            }
          }
          oLayoutDescDOM = null;


          //get the list of snapins GUIDs to be used
          //[MAC] Check Machine OS Windows/MAC
          if(!bMac)
            var sSnapinDescPath = sPackageAbsPath + "\\Snapins.html";
          else
            var sSnapinDescPath = sPackageAbsPath + "/Snapins.html";
          
          var oSnapinListXML = _xml.LoadXML(sSnapinDescPath, false);
          var oSnapinList = {};
          if (oSnapinListXML) {
            //[MAC] Check Machine OS Windows/MAC
            if(!bMac)
              var sXpath = "//snapins/snapin"
            else
              var sXpath = "snapins/snapin"

            var oNodes = _xml.GetNodes(sXpath, "", oSnapinListXML);
            var sSnapinName = "";
            var sSnapinGUID = "";
            var sSnapinID = ""
            if (oNodes && oNodes.length) {
              for (var i = 0; i < oNodes.length; i++) {
                sSnapinGUID = _xml.GetAttribute(oNodes[i], 'guid');
                if (!sSnapinGUID) {
                  sSnapinGUID = _xml.GetAttribute(oNodes[i], 'id');
                }
                oSnapinList[sSnapinGUID] = new Object();
                aSnapinGUIDToUse.push(sSnapinGUID);
              }
            }

          }
          oSnapinListXML = null;

          //augment the config file to the local config files
          sPackageConfigFile = sPackageAbsPath + "\\" + "config.xml";
          _AugmentConfigFile(sPackageConfigFile, null);

          //set the attributes;
          _SetAttribute({
            sLang: $ss.agentcore.utils.GetLanguage(),
            oSprtPackage: {
              sBaseFolder: sBasePackageFolder,
              sBaseAbsPath: sBasePackageFolderAbsPath,
              sRelPath: sPackageRelPath,
              sAbsPath: sPackageAbsPath,
              sLang: $ss.agentcore.utils.GetLanguage(),
              sFolderName: sSprtPackageToUse,
              guid: sSprtPackageToUse.replace(new RegExp("\\b\\.[0-9]+"), "")
            },
            oSprtLayout: {
              guid: aLayoutGUIDToUse,
              name: undefined
            },
            oSprtSnapin: {
              aNames: aSnapinNameToUse,
              aGUIDs: aSnapinGUIDToUse,
              oAvailableSnapins: oSnapinList

            }
          });
        }
        catch (ex) {
          _logger.error("PackageConfig: Error Found %1%", ex.message);
        }
        finally {
          return hasPackage;
        }
      };

      function _ConfigLayout() {
        try {
          var hasLayout = false;
          var oLayoutConfigDOM = null;
          var sBaseLayoutFolder = "sprt_layout"

          var sConfigurationToUse = _GetAttribute('sConfigToUse');
          //[MAC] Check Machine OS Windows/MAC
          if(!bMac) {
            var xPathForConfiguration = "//configuration[@name='" + sConfigurationToUse + "']";
            var xPathExprForLayouts = "//configuration[@name='" + sConfigurationToUse + "']/config-section[@type='layout']";
            var xPathExprForIncludes = "//configuration[@name='" + sConfigurationToUse + "']/config-section[@type='layout']/config[@name='includes']/config-item";
            var xPathExprForControllers = "//configuration[@name='" + sConfigurationToUse + "']/config-section[@type='layout']/config[@name='controllers']/config-item";
            var xPathExprForResources = "//configuration[@name='" + sConfigurationToUse + "']/config-section[@type='layout']/config[@name='resources']/config-item";
            var xPathExprForSkinPath = "//configuration[@name='" + sConfigurationToUse + "']/config-section[@type='layout']/config[@name='skinpath']";
            var xPathExprForSkinName = "//configuration[@name='" + sConfigurationToUse + "']/config-section[@type='layout']/config[@name='skinname']";
            var xPathExprForStyles = "//configuration[@name='" + sConfigurationToUse + "']/config-section[@type='layout']/config[@name='styles']/config-item";
          }
          else {
          var xPathForConfiguration = "sprt/configuration[@name='" + sConfigurationToUse + "']";
          var xPathExprForLayouts = "sprt/configuration[@name='" + sConfigurationToUse + "']/config-section[@type='layout']";
          var xPathExprForIncludes = "sprt/configuration[@name='" + sConfigurationToUse + "']/config-section[@type='layout']/config[@name='includes']/config-item";
          var xPathExprForControllers = "sprt/configuration[@name='" + sConfigurationToUse + "']/config-section[@type='layout']/config[@name='controllers']/config-item";
          var xPathExprForResources = "sprt/configuration[@name='" + sConfigurationToUse + "']/config-section[@type='layout']/config[@name='resources']/config-item";
          var xPathExprForSkinPath = "sprt/configuration[@name='" + sConfigurationToUse + "']/config-section[@type='layout']/config[@name='skinpath']";
          var xPathExprForSkinName = "sprt/configuration[@name='" + sConfigurationToUse + "']/config-section[@type='layout']/config[@name='skinname']";
          var xPathExprForStyles = "sprt/configuration[@name='" + sConfigurationToUse + "']/config-section[@type='layout']/config[@name='styles']/config-item";
        }

        var aLayoutGUID = _GetAttribute('oSprtLayout').guid;
        //[MAC] Check Machine OS Windows/MAC
      if(!bMac)
        var sBaseLayoutFolderAbsPath = _GetAttribute('startingpoint') + "\\" + sBaseLayoutFolder;
      else
        var sBaseLayoutFolderAbsPath = _GetAttribute('startingpoint')  + sBaseLayoutFolder;

          var sLayoutGUIDFolder = "";
          var sRelativePathFromApp = "";
          var sLayoutConfigFile = "";
          var sFilePathPrefix = "";
          var sSkinPath = "";
          var sSkinName = "default";
          var aIncludedResourceList = [];
          var aControllerList = [];
          var aResourceList = [];
          var aStyleList = [];
          for (var j = 0; j < aLayoutGUID.length; j++) {
            if (_file.FolderExists(sBaseLayoutFolderAbsPath)) {
              var lMaxVersion = -1;
              var aFolderList = _file.GetSubFoldersList(sBaseLayoutFolderAbsPath, aLayoutGUID[j]);
              aFolderList = _content.RemovePruneDelayedContentsIn(aFolderList);
              //        if (aFolderList && aFolderList.length) {
              for (var x = 0; x < aFolderList.length; x++) {
                var tmpFolderName = aFolderList[x];
                var folderDetail = tmpFolderName.split(".");
                var sFolderGUID = folderDetail[0];
                var lFVersion = parseInt(folderDetail[1]);
                if (lFVersion < lMaxVersion)
                  continue;
                lMaxVersion = lFVersion;
                //[MAC] Check Machine OS Windows/MAC
                if(!bMac)
                  var tmpConfigFile = sBaseLayoutFolderAbsPath + "\\" + tmpFolderName + "\\" + "config.xml";
                else
                  var tmpConfigFile = sBaseLayoutFolderAbsPath + "/" + tmpFolderName + "/" + "config.xml";

                var tmpConfigDom = _xml.LoadXML(tmpConfigFile, false);
                if (tmpConfigDom == null || tmpConfigDom.xml == "") {
                  continue;
                }
                var tempNode = _xml.GetNodes(xPathForConfiguration, "", tmpConfigDom);
                if (tempNode && tempNode.length) {
                  hasLayout = true;
                  sLayoutGUIDToUse = aLayoutGUID[j];
                  sLayoutGUIDFolder = tmpFolderName;

                //[MAC] Check Machine OS Windows/MAC
                if(!bMac) {
                  sRelativePathFromApp = "sprt_layout\\" + sLayoutGUIDFolder;
                  sLayoutConfigFile = sBaseLayoutFolderAbsPath + "\\" + sLayoutGUIDFolder + "\\" + "config.xml";
                  sFilePathPrefix = sBaseLayoutFolderAbsPath + "\\" + sLayoutGUIDFolder + "\\";
                }
                else {
                  sRelativePathFromApp = "sprt_layout/" + sLayoutGUIDFolder;
                  sLayoutConfigFile = sBaseLayoutFolderAbsPath + "/" + sLayoutGUIDFolder + "/" + "config.xml";
                  sFilePathPrefix = sBaseLayoutFolderAbsPath + "/" + sLayoutGUIDFolder + "/";
                   _logger.info("_ConfigLayout: sSkinName = %1%",xPathExprForSkinName);
                }
                sSkinName = _xml.GetNodeText(_xml.GetNode(xPathExprForSkinName, "", tmpConfigDom)) || sSkinName;
                //add the SKINNAME to MACRO
                $ss.agentcore.dal.config.DefineCustomMacro("%SKINNAME%", sSkinName);
                sSkinPath = sFilePathPrefix + $ss.agentcore.dal.config.ParseMacros(_xml.GetNodeText(_xml.GetNode(xPathExprForSkinPath, "", tmpConfigDom)));
                 
            //[MAC] Check Machine OS Windows/MAC
                if(bMac) {
                  var message = {'JSClassNameKey':'File','JSOperationNameKey':'GetNativePath', 'JSArgumentsKey':[sSkinPath], 'JSISSyncMethodKey' : '1'}
                  var result = jsBridge.execute(message);
                  var parsedJSONObject = JSON.parse(result);
                  sSkinPath = parsedJSONObject.Data;
                 _logger.info("_ConfigLayout: sSkinName = %1%",sSkinName);
                }
                aIncludedResourceList = _GetFileList(tmpConfigDom, xPathExprForIncludes, sFilePathPrefix, "");
                aControllerList = _GetFileList(tmpConfigDom, xPathExprForControllers, sFilePathPrefix, "");
                aResourceList = _GetFileList(tmpConfigDom, xPathExprForResources, sFilePathPrefix, "");
                aStyleList = _GetFileList(tmpConfigDom, xPathExprForStyles, sFilePathPrefix, "");
                tmpConfigDom = null;
                }
                tmpConfigDom = null;
              }
            }
          }
          //if not layout found return immediately
          if (!hasLayout)
            return hasLayout;
          //augment the config file to the local config files

          _AugmentConfigFile(sLayoutConfigFile, null);


          _SetAttribute({
            oSprtLayout: {
              guid: sLayoutGUIDToUse,
              sBaseFolder: sBaseLayoutFolder,
              sBaseAbsPath: sBaseLayoutFolderAbsPath,
              sRelPath: sRelativePathFromApp,
              sAbsPath: sBaseLayoutFolderAbsPath + "\\" + sLayoutGUIDFolder,
              sFolderName: sLayoutGUIDFolder,
              sSkinPath: sSkinPath,
              sConfigFileUri: sLayoutConfigFile,
              aControllers: aControllerList,
              aIncludes: aIncludedResourceList,
              aResources: aResourceList,
              aStyles: aStyleList
            }
          });
          oLayoutConfigDOM = null;
        }
        catch (ex) {
          _logger.error("LayoutConfig: Error Found %1%", ex.message);
        }
        finally {
          return hasLayout;
        }

      };

      function _ConfigSnapin() {
        //var oLayoutConfigDOM = null;
        try {
          var hasSnapin = false;
          var sBaseSnapinFolder = "sprt_snapin";
          //[MAC] Check Machine OS Windows/MAC
          if(!bMac)
            var sBaseSnapinFolderAbsPath = _GetAttribute('startingpoint') + "\\" + sBaseSnapinFolder;
          else
            var sBaseSnapinFolderAbsPath = _GetAttribute('startingpoint') + "/" + sBaseSnapinFolder;
          var sConfigurationToUse = _GetAttribute('sConfigToUse');

          //[MAC] Check Machine OS Windows/MAC
          if(!bMac) {
            var xPathExprForSnapin = "//configuration[@name='" + sConfigurationToUse + "']/config-section[@type='snapin']";
            var xPathExprForIncludes = "//configuration[@name='" + sConfigurationToUse + "']/config-section[@type='snapin']/config[@name='includes']/config-item";
            var xPathExprForControllers = "//configuration[@name='" + sConfigurationToUse + "']/config-section[@type='snapin']/config[@name='controllers']/config-item";
            var xPathExprForResources = "//configuration[@name='" + sConfigurationToUse + "']/config-section[@type='snapin']/config[@name='resources']/config-item";
            var xPathExprForFlows = "//configuration[@name='" + sConfigurationToUse + "']/config-section[@type='snapin']/config[@name='flow']/config-item";
            var xPathExprForStyles = "//configuration[@name='" + sConfigurationToUse + "']/config-section[@type='snapin']/config[@name='styles']/config-item";
            var xPathExprForRegistrationJs = "//configuration[@name='" + sConfigurationToUse + "']/config-section[@type='snapin']/config[@name='registration']/config-item";
          }
          else {
            var includeFiles = filesToInclude.length > 0 ? false : true;
            var xPathExprForSnapin = "sprt/configuration[@name='" + sConfigurationToUse + "']/config-section[@type='snapin']";
            var xPathExprForIncludes = "sprt/configuration[@name='" + sConfigurationToUse + "']/config-section[@type='snapin']/config[@name='includes']/config-item";
            var xPathExprForControllers = "sprt/configuration[@name='" + sConfigurationToUse + "']/config-section[@type='snapin']/config[@name='controllers']/config-item";
            var xPathExprForResources = "sprt/configuration[@name='" + sConfigurationToUse + "']/config-section[@type='snapin']/config[@name='resources']/config-item";
            var xPathExprForFlows = "sprt/configuration[@name='" + sConfigurationToUse + "']/config-section[@type='snapin']/config[@name='flow']/config-item";
            var xPathExprForStyles = "sprt/configuration[@name='" + sConfigurationToUse + "']/config-section[@type='snapin']/config[@name='styles']/config-item";
            var xPathExprForRegistrationJs = "sprt/configuration[@name='" + sConfigurationToUse + "']/config-section[@type='snapin']/config[@name='registration']/config-item";
          }
          //return;

          var oSnapinList = _GetAttribute('oSprtSnapin').oAvailableSnapins;
          var aSnapinGUID = _GetAttribute('oSprtSnapin').aGUIDs;
          var aSnapinConfigs = _GetAttribute('oSprtSnapin').aConfigFileUri;
          var aSnapinControllers = _GetAttribute('oSprtSnapin').aControllers;
          var aSnapinIncludes = _GetAttribute('oSprtSnapin').aIncludes;
          var aSnapinResources = _GetAttribute('oSprtSnapin').aResources;
          var aSnapinFlows = _GetAttribute('oSprtSnapin').aFlows;
          var aSnapinStyles = _GetAttribute('oSprtSnapin').aStyles;
          var aSnapinNames = _GetAttribute('oSprtSnapin').aNames;
          var aSnapinRegs = _GetAttribute('oSprtSnapin').aRegs;

            //[MAC] Check Machine OS Windows/MAC
          if(!bMac)
            var sFilePathPrefix = sBaseSnapinFolderAbsPath + "\\";
          else
            var sFilePathPrefix = sBaseSnapinFolderAbsPath + "/";

          var tempFilePathPrefix = "";

          var aFolderList = _file.GetSubFoldersList(sBaseSnapinFolderAbsPath);
          aFolderList = _RemoveOlderVersionOfEachContent(aFolderList);
          aFolderList = _content.RemovePruneDelayedContentsIn(aFolderList);
          for (var i = 0; i < aFolderList.length; i++) {
            var folderDetail = aFolderList[i].split(".");
            var folderGUID = folderDetail[0];
            var fVersion = folderDetail[1];
            if (oSnapinList[folderGUID]) {

              if (oSnapinList[folderGUID] && oSnapinList[folderGUID].version && parseInt(oSnapinList[folderGUID].version) > parseInt(fVersion))
                continue;

            //[MAC] Check Machine OS Windows/MAC
            if(!bMac) {
              var tempSnapinConfigFile = sBaseSnapinFolderAbsPath + "\\" + aFolderList[i] + "\\config.xml";
              var tempSnapinConfigDOM = _xml.LoadXML(tempSnapinConfigFile, false);
              var tempOSnapin = _xml.GetNodes(xPathExprForSnapin, "", tempSnapinConfigDOM);
              var tempSnapinRelativePath = "sprt_snapin\\" + aFolderList[i];
              var tempFilePathPrefix = sFilePathPrefix + aFolderList[i] + "\\";
            } 
            else {
              var tempSnapinConfigFile = sBaseSnapinFolderAbsPath + "/" + aFolderList[i] + "/config.xml";
              var tempSnapinConfigDOM = _xml.LoadXML(tempSnapinConfigFile, false);
              var tempOSnapin = _xml.GetNodes(xPathExprForSnapin, "", tempSnapinConfigDOM);
              var tempSnapinRelativePath = "sprt_snapin/" + aFolderList[i];
              var tempFilePathPrefix = sFilePathPrefix + aFolderList[i] + "/";
            }

              if (tempOSnapin && tempOSnapin.length) {
                tempSnapinName = _xml.GetAttribute(tempOSnapin[0], 'name') || "";
                hasSnapin = true;
              }
              else
                continue;
              var tempControllers = _GetFileList(tempSnapinConfigDOM, xPathExprForControllers, tempFilePathPrefix, "");
              var tempIncludes = _GetFileList(tempSnapinConfigDOM, xPathExprForIncludes, tempFilePathPrefix, "");
              var tempResources = _GetFileList(tempSnapinConfigDOM, xPathExprForResources, tempFilePathPrefix, "");
              var tempFlows = _GetFileList(tempSnapinConfigDOM, xPathExprForFlows, tempFilePathPrefix, "");
              var tempStyles = _GetFileList(tempSnapinConfigDOM, xPathExprForStyles, tempFilePathPrefix, "");
              var tempRegFiles = _GetFileList(tempSnapinConfigDOM, xPathExprForRegistrationJs, tempFilePathPrefix, "");

              oSnapinList[folderGUID].name = tempSnapinName;
              oSnapinList[folderGUID].folderName = aFolderList[i];
              oSnapinList[folderGUID].configFile = tempSnapinConfigFile;
              oSnapinList[folderGUID].relPath = tempSnapinRelativePath;
            //[MAC] Check Machine OS Windows/MAC
            if(!bMac)
              oSnapinList[folderGUID].absPath = sBaseSnapinFolderAbsPath + "\\" + aFolderList[i];
            else
              oSnapinList[folderGUID].absPath = sBaseSnapinFolderAbsPath + "/" + aFolderList[i];
              oSnapinList[folderGUID].controllers = tempControllers;
              oSnapinList[folderGUID].includes = tempIncludes;
              oSnapinList[folderGUID].regFiles = tempRegFiles;
              oSnapinList[folderGUID].resources = tempResources;
              oSnapinList[folderGUID].flows = tempFlows;
              oSnapinList[folderGUID].styles = tempStyles;
              oSnapinList[folderGUID].version = fVersion;
              oSnapinList[folderGUID].id = folderGUID;


              _AugmentConfigFile(tempSnapinConfigFile, tempSnapinConfigDOM);

              aSnapinNames[tempSnapinName] = folderGUID;
              aSnapinConfigs.push(tempSnapinConfigFile);
              aSnapinControllers = aSnapinControllers.concat(tempControllers);
              aSnapinIncludes = aSnapinIncludes.concat(tempIncludes);
              aSnapinResources = aSnapinResources.concat(tempResources);
              aSnapinFlows = aSnapinFlows.concat(tempFlows);
              aSnapinStyles = aSnapinStyles.concat(tempStyles);
              aSnapinRegs = aSnapinRegs.concat(tempRegFiles);
              //[MAC] Check Machine OS Windows/MAC
              if(bMac) {
              for (var k = 0; k <tempIncludes.length ; k++) {
                if(_file.FileExists(tempIncludes[k]))
                  filesToInclude.push(tempIncludes[k]);
              }
              for (var j = 0; j <tempControllers.length ; j++) {
      
                if(_file.FileExists(tempControllers[j]))
                  filesToInclude.push(tempControllers[j]);
              }
              
              snapinsNames.push(tempSnapinName); 
              }
            }
          }
          // no snapin found return false;  
          if (!hasSnapin)
            return hasSnapin;

          _SetAttribute({
            oSprtSnapin: {
              oAvailableSnapins: oSnapinList,
              aConfigFileUri: aSnapinConfigs,
              aControllers: aSnapinControllers,
              aIncludes: aSnapinIncludes,
              aResources: aSnapinResources,
              aFlows: aSnapinFlows,
              aStyles: aSnapinStyles,
              sBaseAbsPath: sBaseSnapinFolderAbsPath,
              sBaseFolder: sBaseSnapinFolder,
              aNames: aSnapinNames,
              aRegs: aSnapinRegs
            }
          });
        }
        catch (ex) {
          _logger.error("SnapinConfig: Error Found %1%", ex.message);
        }
        finally {
          //[MAC] Check Machine OS Windows/MAC
          if(bMac)
            _logger.info("SnapinConfig: Error Found hasSnapin = %1%", hasSnapin);
          return hasSnapin;
        }
      };

      function _AugmentConfigFile(sConfigFile, oConfigFile) {
        //[MAC] Check Machine OS Windows/MAC
        if(bMac)
          _logger.info("_AugmentConfigFile : " + sConfigFile + 'oConfigFile = ' + oConfigFile);

        try {
          if (oConfigFile) {

            if ($ss.agentcore.dal.config.GetConfigDOM()) {
              $ss.agentcore.dal.config.AugmentConfig($ss.agentcore.dal.config.GetConfigNode(oConfigFile));
            }
            else {
              if (sConfigFile) {
                $ss.agentcore.dal.config.Initialize(sConfigFile, null, null);
              }
            }
          }
          else
            if (sConfigFile) {
            $ss.agentcore.dal.config.AugmentConfigs([sConfigFile]);
          }
        }
        catch (e) {
          _logger.error("_AugmentConfigFile: Exception : %1% ", e.message);
        }

      };


      function _GetFileList(oDom, sXpath, sPreFix, sPostFix) {
        //[MAC] Check Machine OS Windows/MAC
        if(bMac)
          _logger.info("_GetFileList :  sXpath =" + sXpath );

        var arrReturn = [];
        var sPreFix = (typeof (sPreFix) == undefined) ? "" : sPreFix;
        var sPostFix = (typeof (sPostFix) == undefined) ? "" : sPostFix;
        var sXpath = (typeof (sXpath) == undefined) ? "" : sXpath;
        var oDom = (typeof (oDom) == undefined) ? null : oDom;
        if (oDom == null) {
          return null;
        }
        var sValue = "";

        try {
          var oNodes = null;
          oNodes = _xml.GetNodes(sXpath, "", oDom);
          if (oNodes && oNodes.length) {
            for (var i = 0; i < oNodes.length; i++) {
            //[MAC] Check Machine OS Windows/MAC
            if(!bMac)
               sValue = $ss.agentcore.dal.config.ParseMacros(oNodes[i].text.toString());
            else {
              sValue = $ss.agentcore.dal.config.ParseMacros(_xml.GetNodeText(oNodes[i]));
               _logger.info("_GetFileList :  sValue =" + sValue );
            }
              arrReturn.push(sPreFix + sValue + sPostFix);
            }
          }

        }
        catch (ex) {
          _logger.error("_AugmentConfigFile: Exception : %1% ", ex.message);
        }
        //g_logger.Error("ss_cfg_GetArray2()", "section: " + section +"  key: "+ key + " Error Msg:" + ex.message);

        return arrReturn;
      };

      function _DisableBrowserFeatures(oDocument) {
        oDocument = oDocument || document;
        if ($ss.agentcore.dal.config.GetConfigValue("global", "disable_select", "true") == "true") {
          try {
            oDocument.onselectstart = function() {
              return false;
            }
          }
          catch (e) {
          }
        }

        if ($ss.agentcore.dal.config.GetConfigValue("global", "disable_refresh", "true") == "true") {
          try {
            oDocument.onkeydown = function(e) {
              var presskeyCode;
              if (!e) {
                e = event;
              }
              if(bMac){
                presskeyCode=(e.keyCode==116);
              }
              else {
                presskeyCode=(e.keyCode==116||e.keyCode==70||e.keyCode==80||e.keyCode==78);
             }
             if(presskeyCode){
                e.preventDefault();
                e.keyCode = 0;
                e.cancelBubble = true;
                e.returnValue = false;
                return true;
              }
            }
          }
          catch (e) {
          }
        }
        if ($ss.agentcore.dal.config.GetConfigValue("global", "disable_drag", "true") == "true") {
          try {
            oDocument.ondragstart = function() {
              return false;
            }
          }
          catch (e) {
          }
        }
        if ($ss.agentcore.dal.config.GetConfigValue("global", "disable_ctx", "true") == "true") {
          try {
            oDocument.oncontextmenu = function() {
              //[MAC] Check Machine OS Windows/MAC
              //if(!bMac)
                return false;
              //else
              //  return true;
            }
          }
          catch (e) {
          }
        }
      };
      function _SetIcon() {
        try {
          var _ini = $ss.agentcore.dal.ini;
            //[MAC] Check Machine OS Windows/MAC
        if(!bMac) {
          var iconFile = window.external.ExpandSysMacro(_ini.GetIniValue("", "SETUP", "icon", ""));
          if (iconFile != "")
            window.external.SetIcon(iconFile);
        }
        else {
          var iconFile = _ini.GetIniValue("", "SETUP", "icon", "");
          var message = {'JSClassNameKey':'File','JSOperationNameKey':'ExpandSysMacro', 'JSArgumentsKey':[iconFile], 'JSISSyncMethodKey' : '1'}
          var result = jsBridge.execute(message);
          var parsedJSONObject = JSON.parse(result);
          iconFile = parsedJSONObject.Data;
          if (iconFile != "") {
           message = {'JSClassNameKey':'UIUtility','JSOperationNameKey':'SetIcon', 'JSArgumentsKey':[iconFile], 'JSISSyncMethodKey' : '1'}
          jsBridge.execute(message);
          }
        }
        } catch (ex) {
        }
      }

      function _LoadDataBag() {
        var objCmdLine = $ss.agentcore.utils.GetObjectFromCmdLine();
        if (objCmdLine && objCmdLine["loaddbbag"]) {
          _logger.info("LoadDataBag: Loading Databag");
          $ss.agentcore.dal.databag.RestoreDatabag();
          $ss.agentcore.events.SendByName("BROADCAST_ON_INIT_DATABAGLOAD");
        }
        $ss.agentcore.utils.ui.EndShellBusy();
        $ss.agentcore.dal.databag.RemoveValue("ExternalShutdownInitiated");

      };
      function _UpdateLogLevel() {
        var logConfigValue = _config.GetConfigValue("logging", "log_level", "OFF");
        var logLevel = Log4js.Level[logConfigValue] || Log4js.Level.OFF;
        $ss.agentcore.log.ResetDefaultLogLevel(logLevel);
        _logger.info("_UpdateLogLevel: Reseting Log Level to %1%", logLevel);
      };

      function _ShowAgent() {
        //after 10 sec show the agent. if it has not been started as hidden.
        _logger.info("_ShowAgent: Entered Show Agent Function");
        var objCmdLine = $ss.agentcore.utils.GetObjectFromCmdLine();
        var hiddenConfig = $ss.agentcore.dal.ini.GetIniValue("", "SETUP", "starthidden", "0");
        var isBcontStartHidden = objCmdLine.starthidden || (hiddenConfig === "1");
        var defaultHiddenTimeout = parseInt($ss.agentcore.dal.ini.GetIniValue("", "SETUP", "DAHidenTimeOut", "15000"));
        if (!isBcontStartHidden) {
          $().oneTime(defaultHiddenTimeout, "ShowTheAgent", function() {
            //allow this sleep to remove the splash screen.
            $ss.agentcore.utils.ui.ShowDAgentFirstTimeIfDAHidden()
            //by default the da agent starts in hidden mode. Make sure this function show the agent.

          });
        }
      };

  //[MAC] Get Machine OS type
  var bMac = $.browser.safari;

  //[MAC] JSBridge object used only for MAC
  try {
    var jsBridge = window.JSBridge;
  }
  catch(e) {
  }

      //shortcut for ss.agentcore.dal.xml and ss.agentcore.dal.file namespaces
      var _xml = $ss.agentcore.dal.xml;
      var _file = $ss.agentcore.dal.file;
      var _content = $ss.agentcore.dal.content;
      var _utils = $ss.agentcore.utils;
      var _config = $ss.agentcore.dal.config;
      var _shellError = [];
      var _const = $ss.agentcore.constants;
      var _logger = $ss.agentcore.log.GetDefaultLogger("$ss.agentcore.shell");
      var filesToInclude = [];
      var snapinsNames = [];

var sessionIDPre
//[MAC] Check Machine OS Windows/MAC
if(!bMac)
  sessionIDPre = window.external.SessionId;
else
  sessionIDPre = _utils.GenGuid(false);

      var _attributes = {
        external: window.external,
        providerid: null,
        startingpoint: window.startingpoint,
        sConfigToUse: "clientui",
        installConfigFile: null,
        packageStartPath: null,
        packageBackStartPath: null,
        sLang: "en",
        osInfo: {
          sName: undefined,
          sShortName: undefined,
          sSpName: undefined,
          lNotSupportedOS: undefined
        },
        oSprtPackage: {
          sBaseFolder: undefined,
          sBaseAbsPath: undefined,
          sRelPath: undefined,
          sAbsPath: undefined,
          sFolderName: undefined,
          sConfigFileUri: undefined,
          guid: undefined
        },
        oSprtLayout: {
          sBaseFolder: undefined,
          sBaseAbsPath: undefined,
          sRelPath: undefined,
          sAbsPath: undefined,
          sSkinPath: undefined,
          sFolderName: undefined,
          sConfigFileUri: undefined,
          aControllers: [],
          aIncludes: [],
          aResources: [],
          aStyles: [],
          guid: undefined
        },
        oSprtSnapin: {
          sBaseFolder: undefined,
          sBaseAbsPath: undefined,
          aNames: {},
          aGUIDs: [],
          aControllers: [],
          aIncludes: [],
          aResources: [],
          aFlows: [],
          aStyles: [],
          aRegs: [],
          aConfigFileUri: [],
          oAvailableSnapins: {}
        },
        sessionTotalTime: 0,
        sessionStartTime: (new Date()).getTime(),
        sessionID: sessionIDPre
      };

      //call the initialize function      
      $ss.Initialize();

    })();

    
    function External_OnSyncNewData(){
      var logger = $ss.agentcore.log.GetDefaultLogger("$ss.agentcore.shell");
      logger.info("External_OnSyncNewData: External Sync Data Notified");
      $ss.agentcore.events.SendByName("BROADCAST_ON_SYNCNEWDATA");
    }

    function External_OnPruneDone(){
      var logger = $ss.agentcore.log.GetDefaultLogger("$ss.agentcore.shell");
      logger.info("External_OnPruneDone: Prune Notification Done");
      $ss.agentcore.events.SendByName("BROADCAST_ON_PRUNEDONE");
    }

    function External_OnClose(){

      var logger = $ss.agentcore.log.GetDefaultLogger("$ss.agentcore.shell");
      logger.debug("External_OnClose: External Notification Called");
      $ss.agentcore.events.SendByName("BROADCAST_ON_EXTERNALCLOSE");
      var bClose = true;
      if ($ss.helper && $ss.helper.IsBcontToBeClosed) {
        bClose = $ss.helper.IsBcontToBeClosed();
      }
      if (bClose === false) 
        return false;

      var hasBcontShutDownInitiated = $ss.agentcore.dal.databag.GetValue("ExternalShutdownInitiated");
      //try to clean up the open session
      if (!hasBcontShutDownInitiated) {
        try {
          try {
            var scriptRunner = _utils.CreateObject("SPRT.ScriptRunner");
            scriptRunner.Abort("999");
            scriptRunner = null;
          }
          catch (ex) {
            logger.fatal("External_OnClose: Failed to Abort Scriptrunner %1%", ex.message);
          }
          try {
            var passControl = _utils.CreateObject("sdcuser.tgpassctl");
            passControl.closeSocket();
            passControl.close();
            
          } 
          catch (ex) {
            logger.fatal("External_OnClose: Failed to Close passctl %1%", ex.message);
          }
          try {
            var passControlActive = _utils.CreateActiveXObject("sdcuser.tgpassctl", true);
            passControl.closeSocket();
            passControlActive.close();
          } 
          catch (ex) {
            logger.fatal("External_OnClose: Failed to Close passctl %1%", ex.message);
          }
          //abort http request
          $ss.agentcore.dal.http.AbortRequest();
          
        } 
        catch (ex) {
        
        }
        finally {
          $ss.agentcore.state.busy.ClearBusyChache();
          $ss.agentcore.dal.databag.SetValue("ExternalShutdownInitiated", true);
          setInterval("window.external.Close()", 200);
        }
      }
      scriptRunner = null;
      passControlActive = null;
      passControl = null;
      
    }


    function External_OnRefresh(cmdline){
      var logger = $ss.agentcore.log.GetDefaultLogger("$ss.agentcore.shell");
      logger.debug("External_OnRefresh: External OnRefresh Called %1%", cmdline);
      // Show and bring existing instance to foreground only if the following conditions are true
      // (1) existing bcont has WS_VISIBLE flag set (it will be set even if window is minimized or have other windows on top)
      // (2) the 2nd instance's cmdLine does not have the /starthidden flag indicating it wants to do its work in hidden mode
      var pattern = /\s+\/starthidden\s+|\s+\/starthidden$/;
      var hasBcontShutDownInitiated = $ss.agentcore.dal.databag.GetValue("ExternalShutdownInitiated");
      if (!hasBcontShutDownInitiated) {
        if (window.external.IsVisible() && !pattern.test(cmdline) && $ss.agentcore.utils.ui.GetEnableFirstDisplayStatus()) {
          window.external.focus();
        }
      }
      $ss.agentcore.events.SendByName("BROADCAST_ON_EXTERNALREFRESH", cmdline);
    }
