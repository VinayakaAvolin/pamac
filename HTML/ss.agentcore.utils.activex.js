/** @namespace Holds functionality related to COM objects/BCONT*/
$ss.agentcore.utils.activex = $ss.agentcore.utils;

(function()
{
  
  $.extend($ss.agentcore.utils.activex,
  {
    /**
    *  @name CheckInstance
    *  @memberOf $ss.agentcore.utils.activex
    *  @function
    *  @description  Determines if the specified instance of Bcont is running (via mutex)
    *  @param instanceName Instance name to check for
    *  @returns true if running else false
    *  @example
    *         var objActivex = $ss.agentcore.utils;
    *         var objConfig = $ss.agentcore.dal.config;
    *         var ret = objActivex.CheckInstance(objConfig.GetBcontMutexPrefix()); 
    */
    CheckInstance : function(instanceName)
    {
			_logger.info('Entered function: $ss.agentcore.utils.activex.CheckInstance');
      var retval = false;
      try {
        retval = this.GetObjInstance().CheckInstance(instanceName);
      } 
      catch (ex) {
				_exception.HandleException(ex,'$ss.agentcore.utils.activex','CheckInstance',arguments);
        //g_Logger.Error("ss_con_CheckInstance()", ex.message);
      }

     return retval;
    },

    
    /**
    *  @name CreateObject
    *  @memberOf $ss.agentcore.utils.activex
    *  @function
    *  @description  This is a wrapper around all object instantiations. Instead of <br/>
    *                directly calling new CreateObject the application calls this <br/>
    *                function.
    *  @param sProgID program id of the control
    *  @param bIgnoreCache If true, ignores the cache and creates a <br/>
    *                      second, unique instance that is not cached.
    *  @returns object, if successful
    *  @example
    *          var objActivex = $ss.agentcore.utils;
    *          var obj = objActivex.CreateObject("Microsoft.XMLHTTP",true) 
    */
    CreateObject: function(sProgID, bIgnoreCache)
    {
			_logger.info('Entered function: $ss.agentcore.utils.activex.CreateObject');
      var oCreated = null;
      
      try 
      {
        if (bIgnoreCache) 
        {
          oCreated = _CreateObject(sProgID);
        } 
        else 
        {
          if (!_createObjectCache) {
            _createObjectCache = {};
          }
          if ( ! _createObjectCache[sProgID] ) {
            _createObjectCache[sProgID] = _CreateObject(sProgID);
          }
          oCreated = _createObjectCache[sProgID];
        }
      } 
      catch (ex) {
				_exception.HandleException(ex,'$ss.agentcore.utils.activex','CreateObject',arguments);
        //g_Logger.Error("ss_con_CreateObject", ex.message);
      } 
      return oCreated;
    },

    /**
    *  @name GetObjInstance
    *  @memberOf $ss.agentcore.utils.activex
    *  @function
    *  @description  Returns a BCONT instance
    *  @returns Object instance
    *  @example
    *          var objActivex = $ss.agentcore.utils;
    *          var obj = objActivex.GetObjInstance(); 
    */
    GetObjInstance:function()
    {
      //[MAC] Check Machine OS Windows/MAC
      if(!bMac)
        return window.external;
      else 
        return window.JSBridge;
    }

  });
 
  var _createObjectCache;
  
  
  var _activex = $ss.agentcore.utils.activex;
  var _constants = $ss.agentcore.constants;
  var _logger = $ss.agentcore.log.GetDefaultLogger("$ss.agentcore.utils.activex");  
  var _exception = $ss.agentcore.exceptions;
  
  //------------------------------------------------------------------------------
  function _CreateObject(sProgID) 
  {
    var oReturn = null;
    try {
      var oObject = _activex.GetObjInstance().CreateObject(sProgID);
      if (oObject) 
      {
        oReturn = oObject;
        oObject.SetIdentity($ss.agentcore.dal.config.GetProviderID());
      }
    } 
    catch (e) {
    } 
    return oReturn;
  }
 
  //[MAC] Get Machine OS type
  var bMac = $.browser.safari;

  //[MAC] JSBridge object used only for MAC
  try {
    var jsBridge = window.JSBridge;
  }
  catch(e) {
  }

})();