/** @namespace Holds all events related functionality*/
$ss.agentcore.events = $ss.agentcore.events || {};


(function()
{
  $.extend($ss.agentcore.events,
  {
    /**
    *  @name Create
    *  @memberOf $ss.agentcore.events
    *  @function
    *  @description  Create a new Event Message of type/name sName
    *  @param sName Name of the event like SYSTEM_RESTART
    *  @param sDesc Description, helpful while debugging.
    *  @param oReferer The oEvent object causing this event.
    *  @returns The basic oEvent object.
    *  @example
    *         var eventObj = $ss.agentcore.events.Create("BROADCAST_CONNECT_STATUS");
    */
    Create:function( sName, sDesc, oReferer )
    {
      var oEvent = {
        name        : sName,
        description : sDesc,
        timestamp   : new Date(),
        referer     : oReferer || null,
        blocker     : null
      };
      return oEvent;
    },

    /**
    *  @name Subscribe
    *  @memberOf $ss.agentcore.events
    *  @function
    *  @description  Confirms fnCallback is subscribed to sName.  If it is not then
    *                creates a new subscription else uses the existing.
    *  @param sName Name of the event like SYSTEM_RESTART
    *  @param fnCallback The function to be called when the event fires.
    *  @returns true/false
    *  @example
    *         $ss.agentcore.events.Subscribe("BROADCAST_ON_PRUNEDONE", $ss.mynamespace.ClearCache);
    */
    Subscribe:function(sName, fnCallback )
    {
      try {
        if(typeof _evtSubscriptions[sName] != 'undefined') {
          for(var i=0; i<_evtSubscriptions[sName].length; i++) {
            if(_evtSubscriptions[sName][i] === fnCallback ) {
              // we are already subscribed hence no need to subscribe again
              return true;
            }
          }
        }
        return _Subscribe(sName, fnCallback);
      } catch(ex) {
      }
      return false;
    },

    /**
    *  @name Unsubscribe
    *  @memberOf $ss.agentcore.events
    *  @function
    *  @description  Event unsubscription
    *  @param sName Name of the event like SYSTEM_RESTART
    *  @param fnCallback The function to remove from the Queue for sName.
    *  @returns true/false
    *  @example
    *         $ss.agentcore.events.Unsubscribe("BROADCAST_ON_PRUNEDONE", $ss.mynamespace.ClearCache);
    */
    Unsubscribe:function( sName, fnCallback )
    {
      try
      {
        if (typeof _evtSubscriptions[sName] != 'undefined')
        {
          for (var i=0; i<_evtSubscriptions[sName].length; i++)
          {
            if (_evtSubscriptions[sName][i] === fnCallback )
            {
              delete _evtSubscriptions[sName][i];
              _evtSubscribersDirtyList[sName] = true;
              _evtSubscribersDirty = true;
              _CleanLists();
              return true;
            }
          }
        }
      }
      catch (ex)
      {
      }
      return false;
    },

    /**
    *  @name Send
    *  @memberOf $ss.agentcore.events
    *  @function
    *  @description  Event Broadcasting
    *  @param oEvent The event object to be sent around to the subscribers.
    *  @returns SENDMSG_RESULT_*   (OK, PROCESSED, CANCEL_OPERATION, ERROR)
    *  @example
    *         $ss.agentcore.events.Send(eventObj);
    */
    Send:function(oEvent)
    {
      try
      {
        _sendDepth++;

        var sName = oEvent.name;
        var MessageResponses = [];
        var LastResponse = _constants.SENDMSG_RESULT_OK;

        oEvent.blocker = null;

        var oEventSubscriptionsQueue = _evtSubscriptions[sName];
        if (oEventSubscriptionsQueue != undefined)
        {
          var MessagefnCallback        = null;
          for (var i=oEventSubscriptionsQueue.length-1; i>=0; i--)
          {
            LastResponse = _constants.SENDMSG_RESULT_OK;
            try
            {
              MessagefnCallback = oEventSubscriptionsQueue[i];

              if (MessagefnCallback != undefined)
              {
                LastResponse = MessagefnCallback( oEvent );

                if (LastResponse == _constants.SENDMSG_RESULT_ERROR)
                {
                  throw(new Error(_constants.SENDMSG_ERROR_FREEDSCRIPT, "Handler Errored, Removing Subscription"));
                }

                if (LastResponse in _constants.SENDMSG_STACKEND_MSGSET)
                {
                  oEvent.blocker = MessagefnCallback;
                  break;
                }
              }
            }
            catch (ex)
            {
              LastResponse = _constants.SENDMSG_RESULT_ERROR;
              if (_constants.SENDMSG_ERROR_FREEDSCRIPT == ex.number)
              {
                delete _evtSubscriptions[sName][i];
                _evtSubscribersDirty = true;
                _evtSubscribersDirtyList[sName] = true;
                //ss_evt_pvt_LogWarning(oEvent, MessagefnCallback, LastResponse, ss_evt_Error, "Freed Script");
              }
              else
              {
                //ss_evt_pvt_LogError(oEvent, MessagefnCallback, LastResponse, ss_evt_Error, "Handler Error");
              }
            }
            finally
            {
              if (typeof(LastResponse) == 'undefined')
              {
                LastResponse = _constants.SENDMSG_RESULT_OK;
              }
              MessageResponses.push(LastResponse);
            }
          }
        }
        else
        {
          MessageResponses.push(_constants.SENDMSG_RESULT_OK);
        }

        oEvent.MessageResponses = MessageResponses;
        oEvent.LastResponse     = LastResponse;

        return LastResponse;
      }
      catch (ex)
      {
      }
      finally {
        _sendDepth--;
        _CleanLists();
      }
      return _constants.SENDMSG_RESULT_ERROR;
    },

    /**
    *  @name SendByName
    *  @memberOf $ss.agentcore.events
    *  @function
    *  @description  Event Broadcasting by Name
    *  @param sName Name of the event like SYSTEM_RESTART
    *  @param sDesc Description, helpful while debugging.
    *  @param oReferer The oEvent object causing this event.
    *         An oEvent object will be created by Create and the result
    *         will be distributed through Send.
    *  @returns Send result.
    *  @example
    *         $ss.agentcore.events.SendByName("BROADCAST_CONNECT_STATUS");
    */
    SendByName:function (sName, sDesc, oReferer )
    {
      var oEvent = this.Create( sName, sDesc, oReferer );
      return this.Send( oEvent );
    },

    /**
    *  @name SendWithCallback
    *  @memberOf $ss.agentcore.events
    *  @function
    *  @description  Event Broadcasting with Callbacks
    *  @param oEvent oEvent object to distribute.
    *  @param fnCallbackSuccess Callback for a non
    *  @param fnCallbackFailure Callback for a canceled event.
    *  @param hCallbackDatabag Data to pass to the callback function.
    *  @returns 
    *  @example
    *           $ss.agentcore.events.SendWithCallback(eventObj,$ss.mynamespace.fnSuccess,$ss.mynamespace.fnFailure);
    */
    SendWithCallback:function(oEvent, fnCallbackSuccess, fnCallbackFailure, hCallbackDatabag )
    {
      //[MAC] Check Machine OS Windows/MAC
      if(bMac)
      {
        var _logger = $ss.agentcore.log.GetDefaultLogger("$ss.layout.LayoutModel");
        _logger.info('oEvent.....' + oEvent);
      }
      try
      {
        var MessageResponse = this.Send( oEvt );
        var bResultOK = ! (MessageResponse in SENDMSG_STACKEND_MSGSET);
        if (fnCallbackSuccess && bResultOK) {
          try {
            fnCallbackSuccess( oEvent, hCallbackDatabag );
          } catch (ex) {
          }
        }
        else if (fnCallbackFailure && ! bResultOK) {
          fnCallbackFailure( oEvent, hCallbackDatabag );
        }
        return MessageResponse;
      }
      catch (ex)
      {
      }
      return SENDMSG_RESULT_ERROR;
    },

    /**
    *  @name SendByNameWithCallback
    *  @memberOf $ss.agentcore.events
    *  @function
    *  @description  Event Broadcasting by Name with Callbacks
    *  @param sName Name of the event like SYSTEM_RESTART
    *  @param sDesc Description, helpful while debugging.
    *  @param fnCallbackSuccess Callback for a non
    *  @param fnCallbackFailure Callback for a canceled event.
    *  @param hCallbackDatabag Data to pass to the callback function.
    *  @param oReferer The oEvent object causing this event.
    *  @returns $ss.agentcore.constants.SENDMSG_RESULT_*   (OK, PROCESSED, CANCEL_OPERATION, ERROR)
    *  @example
    *           $ss.agentcore.events.SendByNameWithCallback("EVENT_NAME","description",$ss.mynamespace.fnSuccess,$ss.mynamespace.fnFailure);
    */
    SendByNameWithCallback:function(sName, sDesc, fnCallbackSuccess, fnCallbackFailure, hCallbackDatabag, oReferer )
    {
      var oEvent = this.Create( sName, sDesc, oReferer );
      return this.SendWithCallback( oEvent, fnCallbackSuccess, fnCallbackFailure, hCallbackDatabag );
    }
    
  });

  var _evtSubscriptions = {}; // Keyed by EVENT_NAME, each member an array of callbacks.
  var _evtRecorders     = []; // Each recorder is a member of the array.
  var _evtSubscribersDirtyList = {};
  var _evtSubscribersDirty = false;
  var _sendDepth   = 0;
  //[MAC] Check Machine OS Windows/MAC
  if(bMac)
    var g_Logger = $ss.agentcore.log.GetDefaultLogger("$ss.snapin.msgViewer.Helper");  

  var _constants = $ss.agentcore.constants;

  /*******************************************************************************
  ** Name:         ss_evt_pvt_Subscribe
  **
  ** Purpose:      Event subscription helper function used during subscribe
  **               or resubscribe
  **
  ** Parameter:    sName      - Name of the event like SYSTEM_RESTART
  **               fnCallback - The function to be called when the event fires.
  **
  ** Return:       true/false
  *******************************************************************************/
  function _Subscribe(sName, fnCallback )
  {
    try {
      if(typeof _evtSubscriptions[sName] == "undefined") {
        _evtSubscriptions[sName] = [];
      }
      _evtSubscriptions[sName].push( fnCallback );
      return true;
    } catch(e) {
    }
    return false;
  }

  /*******************************************************************************
    ** Name:         ss_evt_CleanLists
    **
    ** Purpose:      This function is called to clean the subscription queues when
    **               both the g_evt_SendDepth is 0 and a dirty flag is set.
    **
    ** Parameter:    none
    **
    ** Return:       none
    *******************************************************************************/
    function _CleanLists()
    {
      if (_sendDepth == 0)
      {
        if (_evtSubscribersDirty)
        {
          for (var sQueueName in _evtSubscribersDirtyList)
          {
            _CleanList(_evtSubscribersDirtyList[sQueueName]);
          }
          _evtSubscribersDirtyList = {};
        }
      }
    }

    /*******************************************************************************
    ** Name:         ss_evt_CleanList
    **
    ** Purpose:      Safe way of array clean up
    **
    ** Parameter:    List      - The list to clean
    **               iStart    - Position to start at (the first hole, usually)
    **
    ** Return:       none
    *******************************************************************************/
    function _CleanList ( List , iStart )
    {
      try
      {
        iStart = iStart || 0;
        var i2;
        while (iStart<List.length) {
          if (List[iStart] == undefined) {
            i2 = iStart+1;
            while (i2<List.length) {
              if (List[i2] != undefined) {
                List[iStart] = List[i2];
                iStart++;
              }
              i2++;
            }
            List.length = iStart;
          }
          iStart++;
        }
      }
      catch (ex)
      {
      }
    }

  //[MAC] Get Machine OS type
  var bMac = $.browser.safari;
})()
