$ss.agentcore.log = $ss.agentcore.log ||
{};
$ss.log = $ss.agentcore.log;

/*******************************************************************************
 **  File Name: ss.agentcore.log.js
 **
 **  Summary: SupportSoft Log Functions
 **
 **  Description: This file contains the functions and structures for error
 **               handling used by several pages.
 **
 **  Dependencies: jQuery.js,
 **                Log4js.js
 **                ss.agentcore.dal.file.js
 **
 **  Copyright SupportSoft Inc. 2008, All rights reserved.
 *******************************************************************************/
(function($){



  var _ModuleLogger = function(moduleName){
    this.moduleName = moduleName || "";
    this.id = new Date().getTime();
    
    this.isTraceEnabled = function(){
    
      return Log4js.getDefaultLogger().isTraceEnabled();
    };
    
    this.trace = function(params){
      var message = GetFormatedString.apply(this, arguments);
      if (message) {
        Log4js.getDefaultLogger().trace("[" + this.moduleName + "]-" + message);
      }
      message = null;
    };
    
    this.isDebugEnabled = function(){
    
      return Log4js.getDefaultLogger().isDebugEnabled();
    };
    
    this.debug = function(params){
      var message = GetFormatedString.apply(this, arguments);
      if (message) {
        Log4js.getDefaultLogger().debug("[" + this.moduleName + "]-" + message);
      }
      message = null;
    };
    
    this.isInfoEnabled = function(){
      return Log4js.getDefaultLogger().isInfoEnabled();
    };
    
    this.info = function(params){
      var message = GetFormatedString.apply(this, arguments);
      if (message) {
        //[MAC] Check Machine OS Windows/MAC
        if(bMac) {
            var jsMessage = {'JSClassNameKey':'CommonUtility','JSOperationNameKey':'ConsoleLog', 'JSArgumentsKey':["[" + this.moduleName + "]-" + message], 'JSISSyncMethodKey' : '1'}
             // jsBridge.execute(jsMessage);
        }
        Log4js.getDefaultLogger().info("[" + this.moduleName + "]-" + message);
      }
      message = null;
    };
    this.isErrorEnabled = function(){
      return Log4js.getDefaultLogger().isErrorEnabled();
    };
    this.error = function(params){
      var message = GetFormatedString.apply(this, arguments);
      if (message) {
        //[MAC] Check Machine OS Windows/MAC
        if(bMac) {
            var message = {'JSClassNameKey':'CommonUtility','JSOperationNameKey':'ConsoleLog', 'JSArgumentsKey':["[" + this.moduleName + "]-" + message], 'JSISSyncMethodKey' : '1'}
             jsBridge.execute(message);
        }
        Log4js.getDefaultLogger().error("[" + this.moduleName + "]-" + message);
      }
      message = null;      
    };
    
    this.isWarnEnabled = function(){
      return Log4js.getDefaultLogger().isWarnEnabled();
    };
    
    this.warn = function(params){
      var message = GetFormatedString.apply(this, arguments);
      if (message) {
        Log4js.getDefaultLogger().warn("[" + this.moduleName + "]-" + message);
      }
      message = null;      
    };
    
    this.isFatalEnabled = function(){
      return Log4js.getDefaultLogger().isFatalEnabled();
    };
    
    this.fatal = function(){
      var message = GetFormatedString.apply(this, arguments);
      if (message) {
        Log4js.getDefaultLogger().fatal("[" + this.moduleName + "]-" + message);
      }
      message = null;      
    };
    
    function GetFormatedString(){
      var message;
      var argsArr;
      
      switch (arguments.length) {
        case 0:
          return message;
        case 1:
          message = arguments[0].toString();
          return message;
        default:
          message = arguments[0].toString();
          var argsArr = Array.prototype.slice.call(arguments);
          argsArr.shift();
          
      }
      return message.interpolateString(argsArr);
    }
  }
  
  var _moduleLoggers = _moduleLoggers ||
  {};
  var _defaultAppender = _defaultAppender || new Log4js.NullAppender();
  
  var _defaultLevel = _defaultLevel || Log4js.Level.OFF;
  //[MAC] Check Machine OS Windows/MAC
  if(bMac)
    var _file = $ss.agentcore.dal.file;

  $.extend($ss.agentcore.log, {
    GetDefaultLogger: _GetDefaultLogger,
    Initialize: _Initialize,
    ResetDefaultLogLevel: _setDefaultLogLevel
  
  });
  
  
  function _GetLogger(moduleName, loggerName){
  
  
    // Use default logger if categoryName is not specified or invalid
    if (!(typeof moduleName == "string")) {
      moduleName = "default";
    }
    if (!(typeof loggerName == "string")) {
      loggerName = "default";
    }
    
    if (!_moduleLoggers[moduleName]) {
      // Create the logger for this name if it doesn't already exist
      _moduleLoggers[moduleName] = new _ModuleLogger(moduleName);
    }
    
    return _moduleLoggers[moduleName];
    
    
    
    
  };
  function _GetDefaultLogger(moduleName){
    // var moduleLogger = _GetLogger(moduleName,'default');
    
    return _GetLogger(moduleName, 'default');
  }
  
  function _Initialize(){
    //TURN OFF the logger used by log4js itself.
    log4jsLogger.addAppender(new Log4js.NullAppender());
    log4jsLogger.setLevel(Log4js.Level.OFF);
    //be default the log level is FATAL.. However if needed
    //this can be modified from the config.xml file of the agent
    var defLogger = Log4js.getDefaultLogger();
    defLogger.setLevel(Log4js.Level.FATAL);
    
    //Get the User context directory to use the log file ..
    //The the other helper files are not available at this point
    //use window.external object to create the files.
    var logRootPath = null;

    //[MAC] Check Machine OS Windows/MAC
    if(!bMac) {
      var oCtx = window.external.getContext();
      var arr = (new VBArray(oCtx)).toArray();
    }
    else {
      //=======================================================
      //[MAC] Native Call
      var message = {'JSClassNameKey':'CommonUtility','JSOperationNameKey':'getContext', 'JSArgumentsKey':[], 'JSISSyncMethodKey' : '1'}
      var result = jsBridge.execute(message);
      var parsedJSONObject = JSON.parse(result);
      //=======================================================
      var arr = parsedJSONObject.Data;
    }
    var rootPath = "";
    var providerId = "";
    for (var i1 = 0; i1 < arr.length; ++i1) {
      var tmp = arr[i1].split("=", 2);
      if (tmp[0] === "SdcContext:DirUserServer") {
        rootPath = tmp[1];
        
      }
      if (tmp[0] === "SdcContext:ProviderId") {
        providerId = tmp[1];
      }
    }

    oCtx = null;
    if (rootPath && providerId) {
      //[MAC] Check Machine OS Windows/MAC
      if(!bMac)
        logRootPath = rootPath + providerId + "\\logs";
      else
        logRootPath = rootPath  + "/state/logs";

      var fso = null;
      var handle = null;
      var logfile = null;
      var file = null;
      try {
        //[MAC] Check Machine OS Windows/MAC
        if(!bMac) {
          var folderSize = window.external.GetFolderSize(logRootPath);
          if (folderSize == 0) { //most likely the folder does not exist
            fso = window.external.CreateInternalObject("filesystem");
            if (!fso.FolderExists(logRootPath)) {
              var sucess = window.external.CreateDir(logRootPath);
              if (sucess === true) 
                logfile = logRootPath + "\\" + "ss_agent_ui.log";
            } else {
              logfile = logRootPath + "\\" + "ss_agent_ui.log";
            }
          }
          else 
          if (folderSize < 5000000) {
            logfile = logRootPath + "\\" + "ss_agent_ui.log";
          }
          else {
            fso = window.external.CreateInternalObject("filesystem");
            logfile = logRootPath + "\\" + "ss_agent_ui.log";
            if (fso.FileExists(logfile)) {
              file = fso.GetFile(logfile);
              file.Delete();
            }
          }
        }
        else {
          //=======================================================
          //[MAC] Native Call
          message = {'JSClassNameKey':'File','JSOperationNameKey':'GetFolderSize', 'JSArgumentsKey':[logRootPath], 'JSISSyncMethodKey' : '1'}
          result = jsBridge.execute(message);
          parsedJSONObject = JSON.parse(result);
          folderSize = parsedJSONObject.Data;

          if (folderSize == 0) { //most likely the folder does not exist
          message = {'JSClassNameKey':'File','JSOperationNameKey':'FolderExists', 'JSArgumentsKey':[logRootPath], 'JSISSyncMethodKey' : '1'}
          result = jsBridge.execute(message);
          parsedJSONObject = JSON.parse(result);
          var folderExists = parsedJSONObject.Data;
          if (folderExists == 0) {
            message = {'JSClassNameKey':'File','JSOperationNameKey':'CreateDir', 'JSArgumentsKey':[logRootPath], 'JSISSyncMethodKey' : '1'}
            result = jsBridge.execute(message);
            parsedJSONObject = JSON.parse(result);
            var sucess = parsedJSONObject.Data;;
            if (sucess === true) 
              logfile = logRootPath + "/" + "ss_agent_ui.log";
            } else {
              logfile = logRootPath + "/" + "ss_agent_ui.log";
            }
          }
          else if (folderSize < 5000000) {
            logfile = logRootPath + "/" + "ss_agent_ui.log";
          }
          else {
              fso = window.external.CreateInternalObject("filesystem");
              logfile = logRootPath + "" + "ss_agent_ui.log";
            if (_file.FileExists(logfile)) {
               _file.DeleteFile(logfile, true);
            }
          }
        }
      } 
      catch (ex) {
        //there is no way it can be logged .. 
      }
      finally {
        file = null;
        fso = null;
        if (logfile) {
          defLogger.setAppenders([new Log4js.FileAppender(logfile)]);
        }
      }
    }
  }
  
  function _setDefaultLogLevel(sLevel){
    var defLogger = Log4js.getDefaultLogger();
    defLogger.setLevel(sLevel);
  }
  
  _Initialize();
  
  //[MAC] Get Machine OS type
  var bMac = $.browser.safari;

  //[MAC] JSBridge object used only for MAC
  try {
    var jsBridge = window.JSBridge;
  }
  catch(e) {
  }

})(jQuery);

