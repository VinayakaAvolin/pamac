if (!window.sprt) {
  	/**
    	@namespace
    
    	All objects live in the sprt namespace, which is also available in the
    	abbreviation $ss.
  	*/
  	var sprt = window.sprt = window.$ss = window.SupportSoft = {};
	/** @namespace Holds all core agent functionality*/
	$ss.agentcore = {};

	
  $ss.agentcore.constants = {};
  $ss.agentcore.constants.smartissue = {};

  /** @namespace Holds all data access functionality*/
  $ss.agentcore.dal = {};
  $ss.agentcore.dal.config = {};
  $ss.agentcore.dal.content = {};
  $ss.agentcore.dal.databag = {};
  $ss.agentcore.dal.file = {};
  $ss.agentcore.dal.history = {};
  $ss.agentcore.dal.http = {};
  $ss.agentcore.dal.ini = {};
  $ss.agentcore.dal.registry = {};
  $ss.agentcore.dal.service = {};
  $ss.agentcore.dal.xml = {};
  
	$ss.agentcore.utils = {};
  $ss.agentcore.utils.ui = {};
  $ss.agentcore.utils.string = {};
  $ss.agentcore.utils.system = {};
  $ss.agentcore.utils.activex = {};
  $ss.agentcore.utils.credentials = {};
  
  $ss.agentcore.devices = {};
  $ss.agentcore.devices.usb = {};
  $ss.agentcore.diagnostics = {};
  $ss.agentcore.diagnostics.smartissue = {};
  
  	/** @namespace Holds all dna probe functionality*/
  $ss.agentcore.dna  = {};
  $ss.agentcore.dna.protectrestore = {};
  $ss.agentcore.events  = {};
  $ss.agentcore.exceptions = {};
  $ss.agentcore.firewall  = {};
  $ss.agentcore.lockdown = {};
  $ss.agentcore.log  = {};
  $ss.agentcore.model  = {};
  $ss.agentcore.network = {};
  $ss.agentcore.network.inet = {};
  $ss.agentcore.network.netcheck = {};
  $ss.agentcore.network.network  = {};
  $ss.agentcore.network.nics  = {};
  $ss.agentcore.reporting  = {};
  $ss.agentcore.smapi  = {};
  $ss.agentcore.smapi.methods  = {};
  $ss.agentcore.smapi.utils = {};
  $ss.agentcore.supportaction = {};
  $ss.agentcore.vacode = {};
    
  $ss.snapin ={};
  
  
	if (typeof(self) !='undefined'){
  		var shellwindow = typeof(shellwindow) != 'undefined' ? shellwindow : self;
	}  
}
