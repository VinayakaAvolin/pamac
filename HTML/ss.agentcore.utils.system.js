﻿/** @namespace Holds all the system related utility functions*/
$ss.agentcore.utils.system = $ss.agentcore.utils;
$ss.agentcore.lockdown = $ss.agentcore.lockdown || {};
(function()
{
  $.extend($ss.agentcore.utils.system,
  {
  
    /**
    *  @name Sleep
    *  @memberOf $ss.agentcore.utils.system
    *  @function
    *  @description  Sleeps for specified milliseconds
    *  @param nTime in milliseconds
    *  @returns none
    *  @example
    *           var objUtil = $ss.agentcore.utils;
    *           objUtil.Sleep(3000);
    */
    Sleep : function (nTime)
    {
      _logger.info('Entered function: $ss.agentcore.utils.system.Sleep');
      try  
      {
        //[MAC] Check Machine OS Windows/MAC
        if(!bMac)
          _objContainer.SleepWithMsgDispatch(nTime);
        else {
          //=======================================================
          //[MAC] Native Call
          var message = {'JSClassNameKey':'SystemUtility','JSOperationNameKey':'SleepWithMsgDispatch', 'JSArgumentsKey':[nTime], 'JSISSyncMethodKey' : '1'}
          jsBridge.execute(message)
          //=======================================================
        }
      } 
      catch (ex) 
      {
        _exception.HandleException(ex,'$ss.agentcore.utils.system','Sleep',arguments);
      }
    },
    
    /**
    *  @name RunCommand
    *  @memberOf $ss.agentcore.utils.system
    *  @function
    *  @description Executes a command
    *         sCmd - command to execute
    *  @param nRunOptions Refer $ss.agentcore.constants for more details<br/>
    *                    Normal -- BCONT_RUNCMD_NORMAL <br/>
    *                    Minimized -- BCONT_RUNCMD_MINIMIZED <br/>
    *                    Hidden -- BCONT_RUNCMD_HIDDEN <br/>
    *                    Async -- BCONT_RUNCMD_ASYNC <br/>
    *  @returns number   process exit code (0 if Async specified)
    *  @example
    *           var objUtil = $ss.agentcore.utils;
    *         objUtil.RunCommand("cmd /c",0);
    */
    RunCommand:function(sCmd, nRunOptions)
    {
      _logger.info('Entered function: $ss.agentcore.utils.system.RunCommand');
      _runCmd_LastErr = 0;
      try 
      {
        //[MAC] Check Machine OS Windows/MAC
        if(!bMac)
          return _objContainer.RunCommand(sCmd, nRunOptions);
        else {
          //=======================================================
          //[MAC] Native Call
          var message = {'JSClassNameKey':'UIUtility','JSOperationNameKey':'RunCommand', 'JSArgumentsKey':[sCmd, nRunOptions], 'JSISSyncMethodKey' : '1'}
          return jsBridge.execute(message)
          //=======================================================
        }
      } 
      catch (err) 
      {
        _exception.HandleException(err,'$ss.agentcore.utils.system','RunCommand',arguments);
        _runCmd_LastErr = err.number;
      }
    },
    
    /**
    *  @name GetLastRunCmdErr()
    *  @memberOf $ss.agentcore.utils.system
    *  @function
    *  @description  Get the error (if any) from the last call to RunCommand
    *  @returns number  last error code
    *  @example
    *           var objUtil = $ss.agentcore.utils;
    *           var ret = objUtil.GetLastRunCmdErr();
    */
    GetLastRunCmdErr:function()
    {
      _logger.info('Entered function: $ss.agentcore.utils.system.GetLastRunCmdErr');
      return _runCmd_LastErr;
    },
    
    /**
    *  @name Reboot
    *  @memberOf $ss.agentcore.utils.system
    *  @function
    *  @description  Reboots the machine
    *  @returns none
    *  @example
    *           var objUtil = $ss.agentcore.utils;
    *           objUtil.Reboot();
    */
    Reboot:function ()
    {
      _logger.info('Entered function: $ss.agentcore.utils.system.Reboot');
      try
      {
        $ss.agentcore.dal.databag.RemoveValue("ShellBusy");
        _PriviledgedReboot();
      } 
      catch (err) 
      {
        _exception.HandleException(err,'$ss.agentcore.utils.system','Reboot',arguments);
      }
    },
    
    /**
    *  @name Shutdown
    *  @memberOf $ss.agentcore.utils.system
    *  @function
    *  @description  Shuts the machine down
    *  @returns none
    *  @example
    *           var objUtil = $ss.agentcore.utils;
    *           objUtil.Shutdown();
    */
    Shutdown:function ()
    {
      _logger.info('Entered function: $ss.agentcore.utils.system.Shutdown');
      try
      {
        //[MAC] Check Machine OS Windows/MAC
        if(!bMac)
          _objContainer.Shutdown();
        else {
          //=======================================================
          //[MAC] Native Call
          var message = {'JSClassNameKey':'Utility','JSOperationNameKey':'Shutdown', 'JSArgumentsKey':[], 'JSISSyncMethodKey' : '1'}
          jsBridge.execute(message)
          //=======================================================
        }
      }
      catch (err)
      {
        _exception.HandleException(err,'$ss.agentcore.utils.system','Shutdown',arguments);
      }
    },
    
    /**
    *  @name IsMute
    *  @memberOf $ss.agentcore.utils.system
    *  @function
    *  @description  Checks if mute is set
    *  @returns number 0 if Mute, else 1
    *  @example
    *           var objUtil = $ss.agentcore.utils;
    *           objUtil.IsMute() 
    */
    IsMute:function ()
    {
      _logger.info('Entered function: $ss.agentcore.utils.system.IsMute');
      var retVal = null;
      try
      {
        //[MAC] Check Machine OS Windows/MAC
        if(!bMac)
          retVal = _objContainer.Mute;
        else {
          //=======================================================
          //[MAC] Native Call
          var message = {'JSClassNameKey':'Utility','JSOperationNameKey':'Mute', 'JSArgumentsKey':[], 'JSISSyncMethodKey' : '1'}
          retVal = jsBridge.execute(message)
          //=======================================================
        }
      }
      catch (err)
      {
         _exception.HandleException(err,'$ss.agentcore.utils.system','IsMute',arguments);
      }
      return retVal;
    },

    /**
    *  @name SetMute
    *  @memberOf $ss.agentcore.utils.system
    *  @function
    *  @description  set mute
    *  @param nMute 0 = off, 1 = on
    *  @returns none
    *  @example
    *           var objUtil = $ss.agentcore.utils;
    *           objUtil.SetMute(1);
    */
    SetMute:function (nMute)
    {
      _logger.info('Entered function: $ss.agentcore.utils.system.SetMute');
      try
      {
        //[MAC] Check Machine OS Windows/MAC
        if(!bMac)
          _objContainer.Mute = nMute;
        else {
          //=======================================================
          //[MAC] Native Call
          var message = {'JSClassNameKey':'Utility','JSOperationNameKey':'Mute', 'JSArgumentsKey':[nMute], 'JSISSyncMethodKey' : '1'}
          jsBridge.execute(message)
          //=======================================================
        }
      }
      catch (err)
      {
         _exception.HandleException(err,'$ss.agentcore.utils.system','SetMute',arguments);
      }
    },

    /**
    *  @name GetVolume
    *  @memberOf $ss.agentcore.utils.system
    *  @function
    *  @description  Gets the volume level
    *  @returns number
    *  @example
    *           var objUtil = $ss.agentcore.utils;
    *           var vol = objUtil.GetVolume();
    */
    GetVolume:function ()
    {
      _logger.info('Entered function: $ss.agentcore.utils.system.GetVolume');
      var retVal = null;
      try
      {
        //[MAC] Check Machine OS Windows/MAC
        if(!bMac)
          retVal = _objContainer.Volume;
        else {
        //=======================================================
        //[MAC] Native Call
        var message = {'JSClassNameKey':'Utility','JSOperationNameKey':'Volume', 'JSArgumentsKey':[], 'JSISSyncMethodKey' : '1'}
        retVal = jsBridge.execute(message)
        //=======================================================
        }
      }
      catch (err)
      {
        _exception.HandleException(err,'$ss.agentcore.utils.system','GetVolume',arguments);
      }
      return retVal;
    },

    /**
    *  @name SetVolume
    *  @memberOf $ss.agentcore.utils.system
    *  @function
    *  @description  set volume level
    *  @param nVolume volume level
    *  @returns none
    *  @example
    *           var objUtil = $ss.agentcore.utils;
    *           objUtil.SetVolume(7800);
    */
    SetVolume:function (nVolume)
    {
      _logger.info('Entered function: $ss.agentcore.utils.system.SetVolume');
      try
      {
        //[MAC] Check Machine OS Windows/MAC
        if(!bMac)
          _objContainer.Volume = nVolume;
        else {
          //=======================================================
          //[MAC] Native Call
          var message = {'JSClassNameKey':'Utility','JSOperationNameKey':'Volume', 'JSArgumentsKey':[nVolume], 'JSISSyncMethodKey' : '1'}
          jsBridge.execute(message)
          //=======================================================
        }
      }
      catch (err) {
        _exception.HandleException(err,'$ss.agentcore.utils.system','SetVolume',arguments);
      }
    },
    
    /**
    *  @name GetDriveStringsByType
    *  @memberOf $ss.agentcore.utils.system
    *  @function
    *  @description  Returns an array of drive letter based on the given type(s)
    *  @param nType Refer $ss.agentcore.constants for more details. <br/>
    *         BCONT_DRIVE_REMOVABLE  = 1<br/>
    *         BCONT_DRIVE_FIXED      = 2<br/>
    *         BCONT_DRIVE_REMOTE     = 4<br/>
    *         BCONT_DRIVE_CDROM      = 8<br/>
    *         BCONT_DRIVE_RAMDISK    = 16<br/>
    *  @returns An array of drive letters
    *  @example
    *           var objUtil = $ss.agentcore.utils;
    *           var retArr = objUtil.GetDriveStringsByType(objConstants.BCONT_DRIVE_REMOVABLE);
    */
    GetDriveStringsByType:function (nType)
    {
      _logger.info('Entered function: $ss.agentcore.utils.system.GetDriveStringsByType');
      var retArr = [];
      
      try
      {
        //[MAC] Check Machine OS Windows/MAC
        if(!bMac)
          var Drives = _objContainer.GetDriveStringsByType(nType);
        else {
          //=======================================================
          //[MAC] Native Call
          var message = {'JSClassNameKey':'Utility','JSOperationNameKey':'GetDriveStringsByType', 'JSArgumentsKey':[nType], 'JSISSyncMethodKey' : '1'}
          var Drives = jsBridge.execute(message)
          //=======================================================
        }
        retArr = Drives.split(';');
        
        if (retArr[retArr.length-1] == "")
        {
           retArr.pop();
        }
      }
      catch (err)
      {
        _exception.HandleException(err,'$ss.agentcore.utils.system','GetDriveStringsByType',arguments);
      }
      return retArr;      
    },
    
    /**
    *  @name GetUSBDriveNames
    *  @memberOf $ss.agentcore.utils.system
    *  @function
    *  @description  Returns a hash of drive letter with USB bus type and its name
    *  @returns A hash in the format of { "G:\" , "PenDrive AVIXE USB Device" }
    *  @example
    *           var objUtil = $ss.agentcore.utils;
    *           var retObj = objUtil.GetUSBDriveNames();
    */
    GetUSBDriveNames:function ()
    {
      _logger.info('Entered function: $ss.agentcore.utils.system.GetUSBDriveNames');
      var retObj = {};

      try
      {
        //[MAC] Check Machine OS Windows/MAC
        if(!bMac)
          var driveString = _objContainer.GetUSBDriveNames();
        else {
          //=======================================================
          //[MAC] Native Call
          var message = {'JSClassNameKey':'Utility','JSOperationNameKey':'GetUSBDriveNames', 'JSArgumentsKey':[], 'JSISSyncMethodKey' : '1'}
          var driveString = jsBridge.execute(message)
          //=======================================================
        }
        if (driveString)
        {
          drives = driveString.split(";");
          
          for (var i = 0; i < drives.length; i++)
          {
            var buf = drives[i].split(",");
            retObj[buf[0]] = buf[1];
          }
        }
      }
      catch (err)
      {
        _exception.HandleException(err,'$ss.agentcore.utils.system','GetUSBDriveNames',arguments);
      }
      return retObj;   
    },
    
    /**
    *  @name Beep
    *  @memberOf $ss.agentcore.utils.system
    *  @function
    *  @description  Generates a beep sound
    *  @param dwFreq Specifies the frequency of the beep
    *  @param dwDuaration specifies the duration of the beep
    *  @returns None
    *  @example
    *           var objUtil = $ss.agentcore.utils;
    *           objUtil.Beep(750,300);
    */
    Beep:function (dwFreq, dwDuration) 
    {
      _logger.info('Entered function: $ss.agentcore.utils.system.Beep');
      var retVal = 0;
      try 
      {    
        //[MAC] Check Machine OS Windows/MAC
        if(!bMac)
          retVal = _objContainer.SysBeep(dwFreq, dwDuration);
        else {
          //=======================================================
          //[MAC] Native Call
          var message = {'JSClassNameKey':'Utility','JSOperationNameKey':'SysBeep', 'JSArgumentsKey':[dwFreq, dwDuration], 'JSISSyncMethodKey' : '1'}
          retVal = jsBridge.execute(message)
          //=======================================================
        }
      } 
      catch (e) 
      {
        _exception.HandleException(e,'$ss.agentcore.utils.system','Beep',arguments);
      }
      return retVal;
    },

    /**
    *  @name IsProcessRunning
    *  @memberOf $ss.agentcore.utils.system
    *  @function
    *  @description  returns if the executable is currently running or not
    *  @param sExeName name of the executable
    *  @returns true if the given executable is running, else false
    *  @example
    *           var objUtil = $ss.agentcore.utils;
    *           objUtil.IsProcessRunning("bcont_nm.exe");
    */
    IsProcessRunning: function(sExeName) {
      _logger.info('Entered function: $ss.agentcore.utils.system.IsProcessRunning');
      var bRunning = false;
      sExeName = sExeName.toLowerCase().trim();
      if (sExeName.substr(sExeName.length - 4, 4) !== ".exe") return bRunning;
      var eServiceList = new Enumerator(_utils.GetObject("winmgmts:{impersonationLevel=impersonate}!root/cimv2").ExecQuery("Select CommandLine From Win32_Process where caption='" + sExeName + "'"));
      for (; !eServiceList.atEnd(); eServiceList.moveNext()) {
        var cmdLine = (new String(eServiceList.item().CommandLine)).toLowerCase();
        if (cmdLine.indexOf(sExeName) > -1) {
          bRunning = true;
          break;
        }
      }
      delete eServiceList;
      return bRunning;
    },
    
    /**
    *  @name GetOS
    *  @memberOf $ss.agentcore.utils.system
    *  @function
    *  @description  Returns current OS
    *  @returns string ("WIN95", "WIN98", "WINME", "WIN2K", "WINXP", "WIN2K3", "WINVST")
    *  @example
    *           var objUtil = $ss.agentcore.utils;
    *           objUtil.GetOS();
    */
    GetOS:function ()
    {
      _logger.info('Entered function: $ss.agentcore.utils.system.GetOS');
      // return cached value if already set
      if (_os) 
      {
        return _os;
      }
      try 
      {
        //[MAC] Check Machine OS Windows/MAC
        if(!bMac)
          _os = _objContainer.GetOS();
        else {
          //=======================================================
          //[MAC] Native Call
          var message = {'JSClassNameKey':'SystemUtility','JSOperationNameKey':'GetOS', 'JSArgumentsKey':[], 'JSISSyncMethodKey' : '1'}
          _os = jsBridge.execute(message)
          //=======================================================
        }
      } catch (ex) 
      {
        _exception.HandleException(ex,'$ss.agentcore.utils.system','GetOS',arguments);
      }
      return _os;
    },

    /**
    *  @name GetOSGroup
    *  @memberOf $ss.agentcore.utils.system
    *  @function
    *  @description  Returns current OS Family - NT based or 9X based
    *  @returns string ("Unknown", "NT", "9x")
    *  @example
    *           var objUtil = $ss.agentcore.utils;
    *           objUtil.GetOSGroup();
    */
    GetOSGroup:function ()
    {
      _logger.info('Entered function: $ss.agentcore.utils.system.GetOSGroup');
      var os = this.GetOS();
      if (os == "WIN95" || os == "WIN98" || os == "WINME") 
        return "9x";
      else if (os == "WINUNK") 
        return "Unknown";
      else 
        return "NT";
    },

    /**
    *  @name IsWin2K
    *  @memberOf $ss.agentcore.utils.system
    *  @function
    *  @description  Checks if current OS is Win2K
    *  @param bAndAbove Flag to indicate if OSes after Win2K should <br/>
    *                   return a match or not
    *  @returns boolean
    *  @example
    *           var objUtil = $ss.agentcore.utils;
    *           objUtil.IsWin2K(true) 
    */
    IsWin2K:function (bAndAbove)
    {
      _logger.info('Entered function: $ss.agentcore.utils.system.IsWin2K');
      var os = this.GetOS();
      var osType = _constants.OS_TYPE[os];
      if (!bAndAbove) 
        return (os == "WIN2K");
      return (osType >= _constants.OS_TYPE["WIN2K"]);
    },

    /**
    *  @name IsWinXP
    *  @memberOf $ss.agentcore.utils.system
    *  @function
    *  @description  Checks if current OS is WinXP
    *  @param bAndAbove Boolean to indicate if OSes after WinXP should <br/>
    *                   return a match or not
    *  @returns boolean
    *  @example
    *           var objUtil = $ss.agentcore.utils;
    *           objUtil.IsWinXP(true);
    */
    IsWinXP:function(bAndAbove)
    {
      _logger.info('Entered function: $ss.agentcore.utils.system.IsWinXP');
      var os = this.GetOS();
      var osType = _constants.OS_TYPE[os];
      if (!bAndAbove) return (os == "WINXP");
      return (osType >= _constants.OS_TYPE["WINXP"]);
    },

    /**
    *  @name IsWinVST
    *  @memberOf $ss.agentcore.utils.system
    *  @function
    *  @description  Checks if current OS is Windows Vista
    *  @param bAndAbove Boolean to indicate if OSes after Windows Vista should <br/>
    *                   return a match or not
    *  @returns boolean
    *  @example
    *           var objUtil = $ss.agentcore.utils;
    *         objUtil.IsWinVST(true);
    */
    IsWinVST:function (bAndAbove)
    {
      _logger.info('Entered function: $ss.agentcore.utils.system.IsWinVST');
      var os = this.GetOS();
      var osType = _constants.OS_TYPE[os];
      if (!bAndAbove) return (os == "WINVST");
      return (osType >= _constants.OS_TYPE["WINVST"]);
    },


    /**
    *  @name StartDhcp
    *  @memberOf $ss.agentcore.utils.system
    *  @function
    *  @description  Start/Stop DHCP service
    *  @param bStart  true, Starts the DHCP service. <br/>  If false stops the DHCP service
    *  @returns boolean 
    *  @example
    *           var objUtil = $ss.agentcore.utils;
    *           objUtil.StartDhcp(true);
    */
    StartDhcp:function (bStart)
    {
      _logger.info('Entered function: $ss.agentcore.utils.system.StartDhcp');
      var result = false;
      try
      {
        //[MAC] Check Machine OS Windows/MAC
        if(!bMac)
          result = _objContainer.StartDhcp(bStart);
        else {
        //=======================================================
        //[MAC] Native Call
        var message = {'JSClassNameKey':'Utility','JSOperationNameKey':'StartDhcp', 'JSArgumentsKey':[bStart], 'JSISSyncMethodKey' : '1'}
        result = jsBridge.execute(message)
        //=======================================================
        }
      }
      catch (err)
      {
        _exception.HandleException(err,'$ss.agentcore.utils.system','StartDhcp',arguments);
      }
      return result;
    },
    
    /**
    *  @name RenewIpConfig
    *  @memberOf $ss.agentcore.utils.system
    *  @function
    *  @description  Renew the IP address for the specified adapter.
    *  @returns boolean true on success else false
    *  @example
    *           var objUtil = $ss.agentcore.utils;
    *           objUtil.RenewIpConfig();
    */
    RenewIpConfig:function ()
    {
      _logger.info('Entered function: $ss.agentcore.utils.system.RenewIpConfig');
      var result = false;
      try
      {
        result = _IpConfigRenew();
      }
      catch (err)
      {
        _exception.HandleException(err,'$ss.agentcore.utils.system','RenewIpConfig',arguments);
      }
      return result;
    },

    /**
    *  @name ReleaseIpConfig
    *  @memberOf $ss.agentcore.utils.system
    *  @function
    *  @description  Release the IP address for the specified adapter.
    *  @returns boolean true on success else false
    *  @example
    *           var objUtil = $ss.agentcore.utils;
    *           objUtil.ReleaseIpConfig(); 
    */
    ReleaseIpConfig:function ()
    {
      _logger.info('Entered function: $ss.agentcore.utils.system.ReleaseIpConfig');
      var result = false;
      try
      {
        result = _IpConfigRelease();
      }
      catch (err)
      {
        _exception.HandleException(err,'$ss.agentcore.utils.system','ReleaseIpConfig',arguments);
      }
      return result;
    },
    
    /**
    *  @name FlushDNS
    *  @memberOf $ss.agentcore.utils.system
    *  @function
    *  @description  Purges the DNS Resolver cache.
    *  @returns boolean true on success else false
    *  @example
    *           var objUtil = $ss.agentcore.utils;
    *           objUtil.FlushDNS();
    */
    FlushDNS:function ()
    {
      _logger.info('Entered function: $ss.agentcore.utils.system.FlushDNS');
      var result = false;
      try
      {
        // reflush DNS if os is 2000+
        if (this.GetOSGroup() != "9x")
        {
          //result = RunCommand("ipconfig /flushdns",  _constants.BCONT_RUNCMD_HIDDEN);
          var guid = $ss.agentcore.dal.config.GetConfigValue("supportactions","guidIPReleaseRenew","4d360154-2fbf-4ba9-9793-d5524f06b841");
          var oFlushSA = new $ss.agentcore.supportaction(guid);
          var result = false;
          if(oFlushSA.cabfile)
          {
            oFlushSA.SetParameter("op", "flushdns");
            var result = (oFlushSA.Evaluate() == 0);
          } 
          this.Sleep(200);
        }
      }
      catch (err)
      {
        _exception.HandleException(err,'$ss.agentcore.utils.system','FlushDNS',arguments);
      }
      return result;
    },

    /**
    *  @name CreateLink
    *  @memberOf $ss.agentcore.utils.system
    *  @function
    *  @description  Creates a shortcut
    *  @param pathobj path to point link to
    *  @param pathlnk path of the link to create
    *  @param description description to assign to link
    *  @returns true on success ; false on failure
    *  @example
    *           var objUtil = $ss.agentcore.utils;
    *           objUtil.CreateLink("C:\\test.xml","c:\\test.lnk","description");
    */
    CreateLink:function (pathobj, pathlnk, description, launchargs, workingdir, customicon)
    {
      _logger.info('Entered function: $ss.agentcore.utils.system.CreateLink');
      var retVal = false;
      try
      {
        //[MAC] Check Machine OS Windows/MAC
        if(!bMac)
          retVal = _objContainer.CreateLink(pathobj, pathlnk, description, launchargs, workingdir, customicon);
        else {
          //=======================================================
          //[MAC] Native Call
          var message = {'JSClassNameKey':'Utility','JSOperationNameKey':'CreateLink', 'JSArgumentsKey':[pathobj, pathlnk, description, launchargs, workingdir, customicon], 'JSISSyncMethodKey' : '1'}
          retVal = jsBridge.execute(message)
          //=======================================================
        }
      }
      catch (err)
      {
        _exception.HandleException(err,'$ss.agentcore.utils.system','CreateLink',arguments);
      }
      return retVal;
    },
    
    /**
    *  @name GetFlashVersion
    *  @memberOf $ss.agentcore.utils.system
    *  @function
    *  @description  This function gets the version of Macromedia's Flash plugin is installed.<br/>
    *                 It is called by sma_main.htm to verify this before any pages are loaded.
    *  @returns Major version of flash control
    *  @example
    *           var objUtil = $ss.agentcore.utils;
    *           var ver = objUtil.GetFlashVersion();
    */
    GetFlashVersion : function() 
    {
      try
      {
        var ver = 0, maxVer = 10, objFlash;
        for (var i=0; i<maxVer; i++) 
        { 
          try 
          { 
            var objFlash = _utils.CreateActiveXObject('ShockwaveFlash.ShockwaveFlash.'+i);
            if (objFlash) 
            { 
              ver = i;
            } 
          } 
          catch(e) { } 
        }
        
      } 
      catch(ex) 
      {
      } 
      
      return ver; 
    },

    /**
    *  @name CheckMinFlashVersion
    *  @memberOf $ss.agentcore.utils.system
    *  @function
    *  @description  This function tests to determine the highest version of flash, starting at 20 <br/>
                     backwards unti we reach the minimum version specified.  if not version was <br/>
                     found, then we assume the flash version does not match the min req.<br/>
    *  @returns Boolean  if true flash is at least of the version specified
    *  @example
    *           var objUtil = $ss.agentcore.utils;
    *           var ret = objUtil.CheckMinFlashVersion(2);
    */
    CheckMinFlashVersion:function(minVersion) 
    {
      try
      {
        var ver = 0, maxVer = 20, objFlash;
        for (var i=maxVer; i>=0; i--) 
        { 
          try 
          {
            objFlash = _utils.CreateActiveXObject('ShockwaveFlash.ShockwaveFlash.'+i);
            if (objFlash && i >= minVersion) 
            { 
              return true; 
            }
          } 
          catch(e) 
          {
            if (i <= minVersion) 
            {
              return false; 
            }
          } 
        }
        
      } 
      catch(ex) {
      } 
      return false;  
    },

    /**
    *  @name InstallFlash
    *  @memberOf $ss.agentcore.utils.system
    *  @function
    *  @description  This function checks to see if the correct version of Macromedia's Flash
                    plugin is installed and if not calls the silent install.
                    This is only check in the flash tag as been set in the config file with
                    a min_version attribute.
    *  @returns none
    *  @example
    *           var objUtil = $ss.agentcore.utils;
    *           objUtil.InstallFlash();
    */
    InstallFlash : function() 
    {
      try
      {
        var flashVer = _config.GetConfigValue("flash", "min_version", "0") * 1;
        if (flashVer > 0) 
        {
          var bFlashVerOK = this.CheckMinFlashVersion(flashVer);
          if (bFlashVerOK == false) 
          {
            var exeFn = _config.GetConfigValue("flash", "software_location", "");
            _utils.RunCommand(exeFn, _constants.BCONT_RUNCMD_HIDDEN);
          } 
          else 
          {
            //g_Logger.Debug("ss_ui_CheckFlash()", "Flash installed, version OK");
          }
        } else {
          //g_Logger.Debug("ss_ui_CheckFlash()", "Flash not required");
        }
      } 
      catch(ex) {
        //g_Logger.Error("ss_ui_CheckFlash()", ex.message);
      }  
    },

    /**
    *  @name AddPnPPath
    *  @memberOf $ss.agentcore.utils.system
    *  @function
    *  @description  Append a path to the Plug-And-Play directory
    *  @param path path to append
    *  @returns true/false
    *  @example
    *           $ss.agentcore.utils.system.AddPnPPath(localDriverPath);
    *
    */
    AddPnPPath:function(path)
    {
      var result = false;
      try 
      {
        //[MAC] Check Machine OS Windows/MAC
        if(!bMac)
          return _objContainer.AddPnPPath(path);
        else {
          //=======================================================
          //[MAC] Native Call
          var message = {'JSClassNameKey':'Utility','JSOperationNameKey':'AddPnPPath', 'JSArgumentsKey':[path], 'JSISSyncMethodKey' : '1'}
          return jsBridge.execute(message)
          //=======================================================
        }
      } 
      catch (err) 
      {
      }

      return result;
    },

    /**
    *  @name RefreshBrowserSettings
    *  @memberOf $ss.agentcore.utils.system
    *  @function
    *  @description  Refresh Browser Settings
    *  @returns true/false
    *  @example
    *         $ss.agentcore.utils.system.RefreshBrowserSettings();
    *
    */
    RefreshBrowserSettings:function()
    {
      var result = false;
      try 
      {
        //[MAC] Check Machine OS Windows/MAC
        if(!bMac)
          return _objContainer.RefreshBrowserSettings();
        else {
          //=======================================================
          //[MAC] Native Call
          var message = {'JSClassNameKey':'Utility','JSOperationNameKey':'RefreshBrowserSettings', 'JSArgumentsKey':[], 'JSISSyncMethodKey' : '1'}
          return jsBridge.execute(message)
          //=======================================================
        }
      } 
      catch (err) 
      {
      }

      return result;
    },

  
    /**
    *  @name GetProxyOption
    *  @memberOf $ss.agentcore.utils.system
    *  @function
    *  @description  Get Proxy Option
    *  @param conn RAS Connection name.  Empty string to indicate LAN or default connection.
    *  @param nItem proxy option to get.
    *  @returns string (proxy option)
    *  @example
    *         var currProxyFlags       = $ss.agentcore.utils.system.GetProxyOption("", _constants.INTERNET_PER_CONN_FLAGS);
    *
    */
    GetProxyOption:function(conn, nItem)
    {
      try
      {
        //[MAC] Check Machine OS Windows/MAC
        if(!bMac)
          return _objContainer.GetProxyOpt(conn, nItem);
        else {
          //=======================================================
          //[MAC] Native Call
          var message = {'JSClassNameKey':'Utility','JSOperationNameKey':'GetProxyOpt', 'JSArgumentsKey':[conn, nItem], 'JSISSyncMethodKey' : '1'}
          return jsBridge.execute(message)
          //=======================================================
        }
      }
      catch (err)
      {
      }

      return "";
    },

    /**
    *  @name SetProxyOption
    *  @memberOf $ss.agentcore.utils.system
    *  @function
    *  @description  Set Proxy Option
    *  @param conn RAS Connection name.  Empty string to indicate LAN or default connection.
    *  @param nItem proxy option to get.
    *  @param value new value of option
    *  @returns true/false
    *  @example
    *         var _utils = $ss.agentcore.utils.system;
    *         var result = _utils.SetProxyOption("", _constants.INTERNET_PER_CONN_FLAGS, PROXY_TYPE_DIRECT);
    */
    SetProxyOption:function(conn,nItem,value) 
    {
      var result = false; 
      try
      {
        //[MAC] Check Machine OS Windows/MAC
        if(!bMac)
          return _objContainer.SetProxyOpt(conn,nItem,value);
        else {
          //=======================================================
          //[MAC] Native Call
          var message = {'JSClassNameKey':'Utility','JSOperationNameKey':'SetProxyOpt', 'JSArgumentsKey':[conn,nItem,value], 'JSISSyncMethodKey' : '1'}
          return jsBridge.execute(message)
          //=======================================================
        }
      }
      catch (err)
      {
      }

      return result;
    },

    /**
    *  @name RefreshDHCP
    *  @memberOf $ss.agentcore.utils.system
    *  @function
    *  @description  After enabling DHCP on one or more adapters, this function either restarts
    *                the DHCP client (on NT-based systems) or reboots the machine (on 9x-based
    *                This function should be called after any call to EnableDHCP
    *  @returns none
    *  @example
    *         $ss.agentcore.utils.system.RefreshDHCP();
    *
    */
    RefreshDHCP:function ()
    {
      if (this.GetOSGroup() == "NT")
      {
        var guid = $ss.agentcore.dal.config.GetConfigValue("supportactions","guidService","a32614f1-7aa7-4171-b6a5-f150fbbfb589");
        var oRefreshSA = new $ss.agentcore.supportaction(guid);
        var providerID = $ss.agentcore.dal.config.GetProviderID();
        var result = false;
        if(oRefreshSA.cabfile)
          {
          oRefreshSA.SetParameter("svcName", "Dhcp");
          oRefreshSA.SetParameter("op", "refresh");
          oRefreshSA.SetParameter("providerID", providerID);

          var result = (oRefreshSA.Evaluate() == 0);
        }

        this.RenewIpConfig();
      }
      else
      {
        // TODO:  reboot for 9x ?
      }
    },
    
    /**
    *  @name GetDefaultEmailClient
    *  @memberOf $ss.agentcore.utils.system
    *  @function
    *  @description  Determine the system's default email client
    *  @returns string containing the name of the email client <br/>
    *                 (should be either 'Microsoft Outlook' or 'Outlook Express')
    *  @example
    *         var defClient = $ss.agentcore.utils.system.GetDefaultEmailClient();
    *
    */
    GetDefaultEmailClient:function()
    {
      var defMailClient = _registry.GetRegValue("HKCU", "Software\\Clients\\Mail\\", "").toString();

      if (defMailClient == "")
      {
        defMailClient = _registry.GetRegValue("HKLM", "Software\\Clients\\Mail\\", "").toString();
      }

      return defMailClient;
    },
    
    /**
    *  @name IsPruneComplete
    *  @memberOf $ss.agentcore.utils.system
    *  @function
    *  @description  Function to check if the prune of contents at the user level is complete
    *  @returns  true if complete, else false
    *  @example
    *          var res = $ss.agentcore.utils.system.IsPruneComplete() ;
    *
    */
    IsPruneComplete: function()
    {
      var rootUserConfigKey =  "Software\\SupportSoft\\ProviderList\\" + _config.GetProviderID() + "\\users\\" + _config.GetContextValue("SdcContext:UserName");
      var prune_status = _registry.GetRegValue("HKCU", rootUserConfigKey, "CurrentPruneStatus");
      var prune_date = _registry.GetRegValue("HKCU", rootUserConfigKey, "LastPrunedTime");
      var ret = (prune_status == "0" && prune_date != "") ? true : false;
      return ret;
    },
    
    /**
    *  @name IsSVCPruneComplete
    *  @memberOf $ss.agentcore.utils.system
    *  @function
    *  @description  Function to check if the prune of contents at the system level is complete
    *  @returns  true if complete, else false
    *  @example
    *          var res = $ss.agentcore.utils.system.IsSVCPruneComplete() ;
    *
    */
    IsSVCPruneComplete: function()
    {
      var rootUserConfigKey =  "Software\\SupportSoft\\ProviderList\\" + _config.GetProviderID() + "\\users\\System";
      var prune_status = _registry.GetRegValue("HKLM", rootUserConfigKey, "CurrentPruneStatus");
      var prune_date = _registry.GetRegValue("HKLM", rootUserConfigKey, "LastPrunedTime");
      var ret = (prune_status == "0" && prune_date != "") ? true : false;
      return ret;
    },
    
        /**
    *  @name Is64BitOS
    *  @memberOf $ss.agentcore.utils.system
    *  @function
    *  @description  Checks if the OS is 64bit
    *  @returns True if 64 bit else false
    *  @example
    *           var objUtil = $ss.agentcore.utils;
    *           objUtil.Is64BitOS();
    */
    Is64BitOS:function ()
    {
      _logger.info('Entered function: $ss.agentcore.utils.system.Is64BitOS');
      var is64Bit = false;
      try 
      {
        //[MAC] Check Machine OS Windows/MAC
        if(!bMac)
          is64Bit = _objContainer.Is64BitOS();
        else {
          //=======================================================
          //[MAC] Native Call
          var message = {'JSClassNameKey':'Utility','JSOperationNameKey':'Is64BitOS', 'JSArgumentsKey':[], 'JSISSyncMethodKey' : '1'}
          is64Bit = jsBridge.execute(message)
          //=======================================================
        }
      } catch (ex) 
      {
        _exception.HandleException(ex,'$ss.agentcore.utils.system','Is64BitOS',arguments);
      }
      return is64Bit;
    }
  });
  
  //[MAC] Get Machine OS type
  var bMac = $.browser.safari;

  //[MAC] JSBridge object used only for MAC
  try {
    var jsBridge = window.JSBridge;
  }
  catch(e) {
  }

  var _objContainer;
  //[MAC] Check Machine OS Windows/MAC
  if(!bMac)
    _objContainer = $ss.agentcore.utils.activex.GetObjInstance();

  var _os;
  var _config = $ss.agentcore.dal.config;
  var _constants = $ss.agentcore.constants;
  var _utils = $ss.agentcore.utils;
  var _logger = $ss.agentcore.log.GetDefaultLogger('$ss.agentcore.utils.system');  
  var _exception = $ss.agentcore.exceptions;
  var _service = $ss.agentcore.dal.service;
  var _registry = $ss.agentcore.dal.registry;
  
  function _PriviledgedReboot()
  {
    var guid = $ss.agentcore.dal.config.GetConfigValue("supportactions","guidReboot","f7b6c494-c90a-442a-b988-efdfd143b99c");
    var PriviledgedReboot = new $ss.agentcore.supportaction(guid);
    
    var result = false;
    if(PriviledgedReboot.cabfile)
    {
      result = (PriviledgedReboot.Evaluate() == 0);
    }
    return result;
  }

  function _IpConfigRelease ()
  {
    var guid = $ss.agentcore.dal.config.GetConfigValue("supportactions","guidIPReleaseRenew","4d360154-2fbf-4ba9-9793-d5524f06b841");
    var oReleaseSA = new $ss.agentcore.supportaction(guid);
    
    var result = false;
    if(oReleaseSA.cabfile)
    {
      oReleaseSA.SetParameter("op", "release");
      result = (oReleaseSA.Evaluate() == 0);
    }
    return result;  
  }

  function _IpConfigRenew ()
  {
    var guid = $ss.agentcore.dal.config.GetConfigValue("supportactions","guidIPReleaseRenew","4d360154-2fbf-4ba9-9793-d5524f06b841");
    var oRenewSA = new $ss.agentcore.supportaction(guid);
    
    var result = false;
    if(oRenewSA.cabfile)
    {
      oRenewSA.SetParameter("op", "renew");
      result = (oRenewSA.Evaluate() == 0);
    }
    return result;  
  }


  
})();
  
