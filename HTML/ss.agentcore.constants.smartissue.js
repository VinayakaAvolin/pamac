/** @namespace Holds all the constants*/
$ss.agentcore.constants = $ss.agentcore.constants || {};
/** @namespace Holds all smartissue related constants*/
$ss.agentcore.constants.smartissue = $ss.agentcore.smartissue || {};
  
(function() 
{
      
  $.extend($ss.agentcore.constants.smartissue, 
  {
    //********* Classes **************************
    GEN_CLASS     : "GeneralInfo",
    SOFT_CLASS    : "Software",
    NAV_CLASS     : "NavInfo",
    SUR_CLASS     : "SurveyInfo",
    MODEM_CLASS   : "ModemInfo",
    EMAIL_CLASS   : "EmailInfo",
    USER_CLASS    : "UserInfo",
    SV_CLASS      : "ServiceVerify",
    MINREQ_CLASS  : "MinReqInfo",
    NWKCHK_CLASS  : "NetworkCheck",
    ADAPTER_CLASS : "AdapterInfo",
    CONNECTIVITY_CLASS  : "Connectivity",
    SG_FIRMWARE_UPDATE_CLASS : "firmware_update",
    SG_GET_ACCOUNT_INFO_CLASS : "get_accountinfo",
    SG_CONFIGURE_THOMPSON_CLASS : "configure_thomson",

    //********  Properties ***********************
    //Modem Info
    MODEM_NAME              : "name",
    MODEM_CODE              : "code",
    ADAPTER_CODE            : "adaptercode",
    ADAPTER_NAME            : "name",
    ADAPTER_INSTALLED       : "adapters_present",
    MODEM_FIRMWARE          : "firmware",
    TYPE                    : "type",       
    CONN_TYPE               : "conn_type",  //USB or Ethernet or Wireless
    MODEM_WIRELESS          : "wireless",
    DEF_USERNAME            : "def_username",
    DEF_PASSWORD            : "def_password",
    MODEM_STATUS_DETECT     : "detect_status",
    MODEM_STATUS_SYNC       : "sync_status",
    MODEM_STATUS_SERVER     : "server_status",
    MODEM_INSTALLUSBDRIVERS : "installusbdrivers",
    ADAPTER_INSTALLDRIVERS  : "installdrivers",
    FAIL_DETECT             : "detectpage_hits",
    STORE_SSID              : "ssid",
    SSID_BROADCAST          : "ssid_broadcast",
    WPA_KEY                 : "wpakey",
    WEP_KEY                 : "wepkey",
    ENCRYPT_TYPE            : "encrypttype",
    USB_SUPPORTED           : "usbsupported",
    PC_TYPE                 : "pctype",
    PC_REBOOT               : "pcireboot",
    USB_REBOOT              : "usbreboot",
    LAPTOP_REBOOT           : "laptopreboot",
    ADAPTER_FAIL_DETECT     : "adapterfail",
    ROUTER_FAIL_DETECT      : "routerfail",
    FROM_DOWNLOADDRIVER     : "downloadpage",
    FROM_SEARCHNETWORK      : "searchnetwork",
    FROM_CHANGENETWORK      : "changenetwork",
    DRIVER_EXECUTE           : "driverexecute",


    //Email Info
    EMAIL_ACCOUNTNAME   : "accountname",
    EMAIL_DISPLAYNAME   : "displayname",
    EMAIL_CLIENT        : "client",
    EMAIL_CLIENTVERSION : "clientversion",
    EMAIL_CONFIGURED    : "configured",
    EMAIL_SMTPSERVER    : "smtpserver",
    EMAIL_POPSERVER     : "popserver",
    EMAIL_POPCHECK      : "popcheck",
    EMAIL_SENDMAIL      : "sendmail",

    //Install Info
    INSTALL_START      : "install_start_time",
    INSTALL_FLOW       : "install_flow", //cd, walled garden, hotel
    INSTALL_LANG       : "install_lang",
    INSTALL_TIMEZONE   : "install_timezone",
    INSTALL_END        : "install_end_time",
    INSTALL_COMPLIANCE : "install_compliance",
    INSTALL_USERTYPE   : "install_usertype",
    INSTALL_VERSION    : "install_version",
    INSTALL_ADAPTER    : "install_adaptertype",

    //Tech Info
    TECH_ID : "tech_id",
    TECH_NAME : "tech_name",

    //User Info
    USER_PHONE      : "user_phone",
    USER_ACCOUNT_NO : "user_account_no",
    USER_FIRST_NAME : "first_name",
    USER_LAST_NAME  : "last_name",
    USER_USER_ID    : "user_id",
    USER_PASSWORD   : "user_password",
    USER_BIRTH_MONTH : "birth_month",
    USER_BIRTH_DAY : "birth_day",
    USER_BIRTH_YEAR : "birth_year",
    USER_SECRET_QUESTION : "secret_question",
    USER_SECRET_QUESTION_ANSWER : "secret_question_answer",
    USER_EMAIL      : "user_email",
    USER_SSN        : "user_ssn",
    USER_ADDRESS1   : "address_line_1",
    USER_ADDRESS2   : "address_line_2",
    USER_CITY       : "user_city",
    USER_STATE      : "user_state",
    USER_ZIP        : "user_zip",

    //Network Info
    NET_NIC_TYPE     : "network_nic_type",    //ISA/PICI/ PCMCIA/USB
    NET_DHCP_STATUS  : "network_dhcp_status", //Enabled,Disabled or Unknown

    //Software Info
    SOFT_ID : "id",
    SOFT_STATUS : "status",
    SOFT_TITLE : "title",
    SOFT_STATUS_SUCCEEDED : "succeeded",
    SOFT_STATUS_FAILED : "failed",
    SOFT_STATUS_SKIPPED : "skipped",
    SOFT_STATUS_ALREADY : "already",

    //ISP Info
    ISP_NAME  : "isp",

    //General Info
    GEN_LASTPAGE_VISITED : "lastpage_visited",
    SPEEDTEST1_DOWNLOAD : "speedtest1_download",
    SPEEDTEST1_UPLOAD : "speedtest1_upload",

    // Network Troubleshoot
    NWKCHK_PHONELINE_CHECKED  : "Phone_Line_Checked",
    NWKCHK_ACTIVATIONDATE_CHECKED  : "Activation_Date_Checked",
    NWKCHK_FILTERS_CHECKED  : "Filters_Checked",
    NWKCHK_MODEMPOWERCYCLE  : "Modem_Power_Cycle",

    // Service Verify Test
    SV_DEVICE_ID    : "device_id",
    SV_TEST_RESULT  : "test_result",
    SV_SERVICE : "service"
    
  });
})();