//
//  PACommonUtilityController.h
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
@class PAJSCommandDelegate;
@class PAJSResult;
@class PAJSInvokedCommand;

@interface PACommonUtilityController : NSObject

- (instancetype)initWithCommandDelegate:(PAJSCommandDelegate*)aDelegate;

- (PAJSResult*)executeCommand:(PAJSInvokedCommand*)command;

@end
