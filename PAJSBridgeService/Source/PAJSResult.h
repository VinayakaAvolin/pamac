//
//  PAJSResult.h
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.


#import <Foundation/Foundation.h>

@interface PAJSResult : NSObject
@property  int status;
@property (strong) NSString* message;
@property (strong) NSString* callbackMethodName;
@property (strong) id data;

- (NSString*)jsonRepresentation;
@end
