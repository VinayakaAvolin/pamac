//
//  PAJSInvokedCommand.h
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.


#import <Foundation/Foundation.h>

@interface PAJSInvokedCommand : NSObject

@property (strong) NSString* className;
@property (strong) NSString* methodName;
@property (strong) NSArray* arguments;
@property (strong) NSString* callbackMethod;
@property (nonatomic) BOOL isSync;

+ (PAJSInvokedCommand*) commandFromDictionary:(NSDictionary*)jsonDict;
@end
