//
//  PAJSBridgeService.h
//  PAJSBridgeService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Cocoa/Cocoa.h>

//! Project version number for PAJSBridgeService.
FOUNDATION_EXPORT double PAJSBridgeServiceVersionNumber;

//! Project version string for PAJSBridgeService.
FOUNDATION_EXPORT const unsigned char PAJSBridgeServiceVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <PAJSBridgeService/PublicHeader.h>


#import <PAJSBridgeService/PAJSBridgeController.h>
