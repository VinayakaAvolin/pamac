//
//  PARegistryController.m
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <PACoreServices/PACoreServices.h>
#import "PARegistryController.h"
#import "PAFileSystemAccessController.h"
#import "PAJSResult.h"
#import "PAJSInvokedCommand.h"
#import "PAJSCommandDelegate.h"
#import "NSString+Additions.h"
#import <PAIpcService/PAIpcService.h>
#import "PAPlistLockManager.h"

#define REG_HKLM @"HKLM"
#define REG_HKCU @"HKCU"
#define REG_HKCU_FILE_NAME @"HKCU_Registry.plist"
#define REG_HKLM_FILE_NAME @"HKLM_Registry.plist"

@interface  PARegistryController(){
  PAFileSystemManager *_fileSystemManager;
}
@property (weak)PAJSCommandDelegate* delegate;
@property (strong) NSOperationQueue *operationQueue;

@end

@implementation PARegistryController

- (instancetype)initWithCommandDelegate:(PAJSCommandDelegate*)aDelegate;
{
  self = [super init];
  if (self) {
    self.delegate = aDelegate;
    self.operationQueue = [[NSOperationQueue alloc] init];
    _fileSystemManager = [[PAFileSystemManager alloc] init];
  }
  return self;
}

- (PAJSResult*)executeCommand:(PAJSInvokedCommand*)command {
  if (command.isSync) {
    return [self executeSyncCommand:command];
  }
  [self executeAsyncCommand:command];
  return nil;
}

- (PAJSResult*)executeSyncCommand:(PAJSInvokedCommand*)command {
  if ([command.methodName isEqualToString:@"RegValueExists"]) {
    return [self isRegValueExists:command];
  }
  else if([command.methodName isEqualToString:@"GetRegValue"]) {
    return [self regValueForCommand:command];
  }
  else if([command.methodName isEqualToString:@"SetRegValue"]) {
    return [self setRegValue:command];
  }
  else if([command.methodName isEqualToString:@"EnumRegKey"]) {
    return [self enumRegKey:command];
  }
  else if([command.methodName isEqualToString:@"DeleteRegVal"] ||
          [command.methodName isEqualToString:@"DeleteRegValEx"]) {
    return [self deleteRegValue:command];
  }
  else if([command.methodName isEqualToString:@"DeleteRegKey"] ||
          [command.methodName isEqualToString:@"DeleteRegKeyEx"]) {
    return [self deleteRegKey:command];
  }
  
  return nil;
}

- (void)executeAsyncCommand:(PAJSInvokedCommand*)command  {
  
}

- (PAJSResult*)deleteRegKey:(PAJSInvokedCommand*)command {
  if (command.arguments.count == 2) {
    NSString* regRoot = (NSString*)command.arguments[0];
    NSString* regKey = (NSString*)command.arguments[1];
    
    NSString* plistFile = [self plistPathRorRegistry:regRoot];
    NSInteger serviceMode = PAPlistServiceModeNormal;
    if ([regRoot isEqualToString:REG_HKLM]) {
      serviceMode = PAPlistServiceModeElevated;
    }
    
    PAPlistServiceInfo *serviceInfo = [[PAPlistServiceInfo alloc] init];
    serviceInfo.sectionPath = regKey;
    serviceInfo.serviceMode = serviceMode;
    serviceInfo.service = PAPlistServiceDeleteKey;
    serviceInfo.plistFilePath = plistFile;
    
    [[PAInterProcessManager sharedManager] executePlistService:serviceInfo withReply:^(BOOL finish) {
      if (finish) {
        
      }
    }];
    PAJSResult* result = [[PAJSResult alloc] init];
    result.status = 0;
    result.data =  @(true);
    return result;
  }
  return nil;
}
- (PAJSResult*)deleteRegValue:(PAJSInvokedCommand*)command {
  if(command.arguments.count == 3) {
    NSString* regRoot = (NSString*)command.arguments[0];
    NSString* sectionPath = (NSString*)command.arguments[1];
    NSString* regKey = (NSString*)command.arguments[2];
    
    NSString* plistFile = [self plistPathRorRegistry:regRoot];
    NSInteger serviceMode = PAPlistServiceModeNormal;
    if ([regRoot isEqualToString:REG_HKLM]) {
      serviceMode = PAPlistServiceModeElevated;
    }
    
    PAPlistServiceInfo *serviceInfo = [[PAPlistServiceInfo alloc] init];
    serviceInfo.sectionPath = sectionPath;
    serviceInfo.regKey = regKey;
    serviceInfo.serviceMode = serviceMode;
    serviceInfo.service = PAPlistServiceDeleteKey;
    serviceInfo.plistFilePath = plistFile;
    
    [[PAInterProcessManager sharedManager] executePlistService:serviceInfo withReply:^(BOOL finish) {
      if (finish) {
        
      }
    }];
    PAJSResult* result = [[PAJSResult alloc] init];
    result.status = 0;
    result.data =  @(true);
    return result;
    
  }
  return nil;
}
- (PAJSResult*)isRegValueExists:(PAJSInvokedCommand*)command {
  if (command.arguments.count == 3) {
    NSString* regRoot = (NSString*)command.arguments[0];
    NSString* regKey = (NSString*)command.arguments[1];
    NSString* regVal = (NSString*)command.arguments[2];
    
    NSString* plistFile = [self plistPathRorRegistry:regRoot];
    __block PAJSResult* result = [[PAJSResult alloc] init];
    PAPlistLockManager *lckMgr = [PAPlistLockManager sharedInstance];
    [lckMgr runWithReadLock:plistFile codeBlock:^{
      
      PARegistryReader *reader = [[PARegistryReader alloc]initWithPlistAtPath:plistFile];
      BOOL regValueExists = [reader isRegValueExistsWithKey:regKey regValue:regVal];
      result = [[PAJSResult alloc] init];
      result.status = 0;
      result.data =  regValueExists ? @(true) : @(false);
    }];
    return result;
    
  }
  return nil;
  
}

- (PAJSResult*)regValueForCommand:(PAJSInvokedCommand*)command {
  if (command.arguments.count == 3) {
    NSString* regRoot = (NSString*)command.arguments[0];
    NSString* regKey = (NSString*)command.arguments[1];
    NSString* regVal = (NSString*)command.arguments[2];
    NSString* plistFile = [self plistPathRorRegistry:regRoot];
    __block PAJSResult* result = [[PAJSResult alloc] init];
    
    PAPlistLockManager *lckMgr = [PAPlistLockManager sharedInstance];
    [lckMgr runWithReadLock:plistFile codeBlock:^{
      
      PARegistryReader *reader = [[PARegistryReader alloc]initWithPlistAtPath:plistFile];
      result = [[PAJSResult alloc] init];
      result.status = 0;
      result.data =  [reader regValueForPath:regKey withKey:regVal];
    }];
    return result;
  }
  return nil;
  
}

- (PAJSResult*)setRegValue:(PAJSInvokedCommand*)command {
  //sRegRoot, sRegKey, sRegVal, sData
  if (command.arguments.count ==4) {
    NSString* regRoot = (NSString*)command.arguments[0];
    NSString* regPath = (NSString*)command.arguments[1];
    NSString* regKey = (NSString*)command.arguments[2];
    NSString* regData = (NSString*)command.arguments[3];
    
    NSString* plistFile = [self plistPathRorRegistry:regRoot];
    NSInteger serviceMode = PAPlistServiceModeNormal;
    if ([regRoot isEqualToString:REG_HKLM]) {
      serviceMode = PAPlistServiceModeElevated;
    }
    
    PAPlistServiceInfo *serviceInfo = [[PAPlistServiceInfo alloc] init];
    serviceInfo.sectionPath = regPath;
    serviceInfo.regKey = regKey;
    serviceInfo.regData = regData;
    serviceInfo.serviceMode = serviceMode;
    serviceInfo.service = PAPlistServiceSetRegValue;
    serviceInfo.plistFilePath = plistFile;
    
    [[PAInterProcessManager sharedManager] executePlistService:serviceInfo withReply:^(BOOL finish) {
      if (finish) {
        
      }
    }];
    
    PAJSResult* result = [[PAJSResult alloc] init];
    result.status = 0;
    result.data =  @(1);
    return result;
  }
  return nil;
}


- (PAJSResult*)enumRegKey:(PAJSInvokedCommand*)command {
  if (command.arguments.count == 3) {
    NSString* regRoot = (NSString*)command.arguments[0];
    NSString* regKey = (NSString*)command.arguments[1];
    NSString* delimeter = (NSString*)command.arguments[2];
    NSString* plistFile = [self plistPathRorRegistry:regRoot];
    __block PAJSResult* result = [[PAJSResult alloc] init];
    PAPlistLockManager *lckMgr = [PAPlistLockManager sharedInstance];
    [lckMgr runWithReadLock:plistFile codeBlock:^{
      
      PARegistryReader *reader = [[PARegistryReader alloc]initWithPlistAtPath:plistFile];
      result = [[PAJSResult alloc] init];
      result.status = 0;
      result.data =  [reader enumRegKey:regKey delimeter:delimeter];
    }];
    return result;
  }
  return nil;
}

- (NSString*)nativePathForPath:(NSString*)path {
  if (path.length == 0) {
    return path;
  }
  path = [path lowercaseString];
  path = [path stringByConvertingIntoMacCompatibleFilePath];
  NSRange rg = [path rangeOfString:@"/" options:NSCaseInsensitiveSearch range:NSMakeRange(path.length -1, 1)];
  if (rg.location != NSNotFound) {
    path =[path stringByReplacingCharactersInRange:rg withString:@""];
    
  }
  return path;
}

- (NSString *)plistPathRorRegistry:(NSString *)regRoot {
  NSString* plistFile = nil;
  if ([regRoot isEqualToString:REG_HKLM]) {
    plistFile = [_fileSystemManager systemAppRootPath];
    plistFile = [plistFile stringByAppendingPathComponent:REG_HKLM_FILE_NAME];
  }else {
    plistFile = [_fileSystemManager userAgentBinFolderPath];
    plistFile = [plistFile stringByAppendingPathComponent:REG_HKCU_FILE_NAME];
  }
  
  if (![_fileSystemManager doesFileExist:plistFile]) {
    [[NSMutableArray array] writeToFile:plistFile atomically:YES];
  }
  return plistFile;
}

@end
