//
//  PAJSResult.m
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.


#import "PAJSResult.h"
#import "PAJSConstants.h"

@implementation PAJSResult

- (NSDictionary*) toDictionary {
  NSMutableDictionary* dict = [NSMutableDictionary dictionary];
  [dict setObject:@1 forKey:JSStatus];
  if (self.message) {
    [dict setObject:self.message forKey:JSMessage];
  }
  if (self.data) {
    [dict setObject:self.data forKey:JSData];
  }
  return dict;
}

- (NSString*)jsonRepresentation {
  NSError *writeError = nil;
  
  NSData *jsonData = [NSJSONSerialization dataWithJSONObject:[self toDictionary] options:NSJSONWritingPrettyPrinted error:&writeError];
  
  NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
  
  return jsonString;
}

@end
