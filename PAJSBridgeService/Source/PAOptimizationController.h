//
//  PAOptimizationController.h
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
@class PAJSResult;
@class PAJSCommandDelegate;
@class PAJSInvokedCommand;

@interface PAOptimizationController : NSObject

- (instancetype)initWithCommandDelegate:(PAJSCommandDelegate*)aDelegate;

- (PAJSResult*)executeCommand:(PAJSInvokedCommand*)command;


@end
