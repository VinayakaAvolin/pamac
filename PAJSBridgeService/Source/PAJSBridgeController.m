//
//  PAJSBridgeController.m
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.


#import "PAJSBridgeController.h"
#import "PAJSResult.h"

@implementation PAJSBridgeController

- (instancetype)init
{
  self = [super init];
  if (self) {
    self.commandDelegate = [[PAJSCommandDelegate alloc] initWithController:self];
    self.currentApp = PAMainApp;
  }
  return self;
}
+ (NSString *) webScriptNameForSelector:(SEL)sel
{
  NSString* name = NSStringFromSelector(sel);
  
  if (sel == @selector(execute:))
  {
    name = @"execute";
  }
  
  return name;
}

+ (BOOL)isSelectorExcludedFromWebScript:(SEL)selector {
  //    NSLog(@"%@ received %@ for '%@'", self, NSStringFromSelector(_cmd), NSStringFromSelector(selector));
  if (selector == @selector(execute:)) {
    return NO;
  }
  return YES;
}

-(NSString*)execute:(WebScriptObject*)message {
  id objCObject = [[message JSValue] toObject];
  
  if ([objCObject isKindOfClass:[NSDictionary class]]) {
    PAJSResult * result = [self.commandDelegate executeCommand:objCObject];
    if (result) {
      NSString* resAsString = [result jsonRepresentation];
      return resAsString;
    }
  }
  return nil;
}

- (void)sendResult:(PAJSResult*)result {
  id domWindow = [self.webrView windowScriptObject];
  NSString* resAsString = [result jsonRepresentation];
  [domWindow callWebScriptMethod:result.callbackMethodName withArguments:@[resAsString]];
}

- (NSRect)defaultWindowFrame {
  return [self.commandDelegate defaultWindowFrame];
}

- (void)setCommandLineArguments:(NSArray*)arguments {
  self.arguments = arguments;
  
}
@end
