//
//  PARestoredListController.m
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PARestoredListController.h"
#import <PAIpcService/PAIpcService.h>
#import "PAJSResult.h"
#import "PAJSInvokedCommand.h"
#import "PAJSCommandDelegate.h"
#import "PAJSConstants.h"

@interface  PARestoredListController(){
}
@property (weak)PAJSCommandDelegate* delegate;
@property (strong)NSMutableArray* guidArray;
@property (strong)NSMutableArray* resultArray;
@property (strong)PAJSInvokedCommand* command;

@end

@implementation PARestoredListController

- (instancetype)initWithCommandDelegate:(PAJSCommandDelegate*)aDelegate command:(PAJSInvokedCommand*)command
{
  self = [super init];
  if (self) {
    self.delegate = aDelegate;
    self.guidArray = [NSMutableArray array];
    self.resultArray = [NSMutableArray array];
    self.command = command;
  }
  return self;
}

- (void)executeCommand {
  if (self.command.arguments.count > 0) {
    NSArray* array = (NSArray*) self.command.arguments[0];
    self.guidArray = [NSMutableArray arrayWithArray:array];
    if (self.guidArray.count) {
      [self executeScript:self.guidArray[0]];
    }
  }
}

- (void)executeScript:(NSDictionary*)info {
  NSString* guid = info[JSGuid];
  NSString* version = info[JSVersion];
  if (guid && version) {
    PAServiceInfo *info = [[PAServiceInfo alloc]init];
    info.guid = guid;
    info.version = version;
    info.serviceCategory = PASupportServiceCategoryRepair;
    info.service = PASupportServiceGetRestoredHistory;
    info.serviceMode = PAServiceModeNormal;
    [[PAInterProcessManager sharedManager]executeService:info withReply:^(BOOL success, NSError *error, id result) {
      if(result) {
        NSArray*resultArray = (NSArray*)result;
        NSDictionary* dict = nil;
        if (resultArray.count) {
          dict = resultArray[0];
        }
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:JSDateFormat1];
        NSDate* date = [formatter dateFromString:dict[JSDateString]];
        [formatter setDateFormat:JSDateFormat2];
        NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
        [formatter setTimeZone:timeZone];
        NSString *stringFromDate = [formatter stringFromDate:date];
        
        [self.resultArray addObject:@{JSGuid:dict[JSGuid], JSProtectedDate: stringFromDate}];
        [self.guidArray removeObjectAtIndex:0];
        if(self.guidArray.count == 0) {
          PAJSResult* jsResult = [[PAJSResult alloc] init];
          jsResult.status = 0;
          jsResult.data = self.resultArray;
          jsResult.callbackMethodName = self.command.callbackMethod;
          [self.delegate performSelectorOnMainThread:@selector(sendResult:) withObject:jsResult waitUntilDone:NO];
        }
        else {
          [self performSelectorOnMainThread:@selector(executeScript:) withObject:self.guidArray[0] waitUntilDone:NO];
        }
      }else {
        [self.guidArray removeObjectAtIndex:0];
        if(self.guidArray.count == 0) {
          PAJSResult* jsResult = [[PAJSResult alloc] init];
          jsResult.status = 0;
          jsResult.data = self.resultArray;
          jsResult.callbackMethodName = self.command.callbackMethod;
          [self.delegate performSelectorOnMainThread:@selector(sendResult:) withObject:jsResult waitUntilDone:NO];
        }
        else {
          [self performSelectorOnMainThread:@selector(executeScript:) withObject:self.guidArray[0] waitUntilDone:NO];
        }
        
      }
      
    }];
  }
}
@end
