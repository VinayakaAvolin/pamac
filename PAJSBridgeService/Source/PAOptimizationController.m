//
//  PAOptimizationController.m
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <PAIpcService/PAIpcService.h>
#import "PAOptimizationController.h"
#import "PAJSResult.h"
#import "PAJSInvokedCommand.h"
#import "PAJSCommandDelegate.h"
#import "PAJSConstants.h"
#import "PAOptimizerConstants.h"

@interface  PAOptimizationController()

@property (weak)PAJSCommandDelegate* delegate;
@property (strong) NSOperationQueue *operationQueue;
@property BOOL isOptimizationRunning;

@end

@implementation PAOptimizationController

- (instancetype)initWithCommandDelegate:(PAJSCommandDelegate*)aDelegate
{
  self = [super init];
  if (self) {
    self.delegate = aDelegate;
    self.operationQueue = [[NSOperationQueue alloc] init];
    self.isOptimizationRunning = NO;
  }
  return self;
}

- (PAJSResult*)executeCommand:(PAJSInvokedCommand*)command {
  if (command.isSync) {
    return [self executeSyncCommand:command];
  }
  [self executeAsyncCommand:command];
  return nil;
}


- (PAJSResult*)executeSyncCommand:(PAJSInvokedCommand*)command {
  if ([command.methodName isEqualToString:@"GetStatus"]) {
    PAJSResult* result = [[PAJSResult alloc] init];
    result.data = self.isOptimizationRunning ? @(1) : @(0);
    return result;
  }
  return nil;
}

- (void)executeAsyncCommand:(PAJSInvokedCommand*)command {
  if ([command.methodName isEqualToString:@"StartOptimzer"]) {
    NSInvocationOperation *operation = [[NSInvocationOperation alloc]
                                        initWithTarget:self
                                        selector:@selector(startOptimizer:)
                                        object:command];
    [self.operationQueue addOperation:operation];
  }
  else if([command.methodName isEqualToString:@"HaltOptimzer"]) {
    NSInvocationOperation *operation = [[NSInvocationOperation alloc]
                                        initWithTarget:self
                                        selector:@selector(haltOptimizer:)
                                        object:command];
    [self.operationQueue addOperation:operation];
  }else if([command.methodName isEqualToString:@"Schedule"]) {
    NSInvocationOperation *operation = [[NSInvocationOperation alloc]
                                        initWithTarget:self
                                        selector:@selector(scheduleOptimization:)
                                        object:command];
    [self.operationQueue addOperation:operation];
  }
}

- (void)startOptimizer:(PAJSInvokedCommand*)command {
  self.isOptimizationRunning = YES;
  [[NSDistributedNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveNotification:) name:kPASupportServiceStartOptimizerNotificationName object:kPASupportServiceOptimizationNotificationObjectName];
  
  [[NSDistributedNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveNotification:) name:kPASupportServiceStopOptimizerNotificationName object:kPASupportServiceOptimizationNotificationObjectName];
  
  [[NSDistributedNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveNotification:) name:kPASupportServiceStartOptimizationNotificationName object:kPASupportServiceOptimizationNotificationObjectName];
  
  [[NSDistributedNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveNotification:) name:kPASupportServiceStopOptimizationNotificationName object:kPASupportServiceOptimizationNotificationObjectName];
  
  [[PAInterProcessManager sharedManager] startOptimizerWithCallback:^(BOOL success) {
    
  }];
}

- (void)haltOptimizer: (PAJSInvokedCommand*)command {
  self.isOptimizationRunning = NO;
  [[PAInterProcessManager sharedManager] stopOptimizerWithCallback:^(BOOL success) {
    [[NSDistributedNotificationCenter defaultCenter] removeObserver:self name:kPASupportServiceStartOptimizerNotificationName object:kPASupportServiceOptimizationNotificationObjectName];
    
    [[NSDistributedNotificationCenter defaultCenter] removeObserver:self name:kPASupportServiceStopOptimizerNotificationName object:kPASupportServiceOptimizationNotificationObjectName];
    
    [[NSDistributedNotificationCenter defaultCenter] removeObserver:self name:kPASupportServiceStartOptimizationNotificationName object:kPASupportServiceOptimizationNotificationObjectName];
    
    [[NSDistributedNotificationCenter defaultCenter] removeObserver:self name:kPASupportServiceStopOptimizationNotificationName object:kPASupportServiceOptimizationNotificationObjectName];
  }];
}

- (void)scheduleOptimization:(PAJSInvokedCommand*)command {
  if (command.arguments.count > 0) {
    NSString *scheduleDateTime = (NSString*)command.arguments[2];
    NSString *scheduleFrequency = (NSString*)command.arguments[3];
    NSString *scheduleDay = (NSString*)command.arguments[4];
    
    [[PAInterProcessManager sharedManager] scheduleOptimization:
     @{kPAScheduleTimeKey: scheduleDateTime,
       kPAScheduleFrequencyKey: scheduleFrequency,
       kPAScheduleDayKey: scheduleDay           }
     ];
  }
}

- (void)didReceiveNotification:(NSNotification*)notification {
  PAJSResult* result = [[PAJSResult alloc] init];
  
  if([notification.name isEqualToString:kPASupportServiceStartOptimizerNotificationName]) {
    result.callbackMethodName = @"_OptimizerStart";
    
  }else if([notification.name isEqualToString:kPASupportServiceStartOptimizationNotificationName]) {
    result.callbackMethodName = @"_OptimizationStart";
    result.data = notification.userInfo[JSGuid];
  }
  else if([notification.name isEqualToString:kPASupportServiceStopOptimizationNotificationName]) {
    result.callbackMethodName = @"_OptimizationStop";
    result.data = notification.userInfo;
  }
  else if([notification.name isEqualToString:kPASupportServiceStopOptimizerNotificationName]) {
    result.callbackMethodName = @"_OptimizerStop";
    self.isOptimizationRunning = NO;
  }
  [self.delegate performSelectorOnMainThread:@selector(sendResult:) withObject:result waitUntilDone:NO];
}

@end
