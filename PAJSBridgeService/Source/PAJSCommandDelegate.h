//
//  PAJSCommandDelegate.h
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.


#import <Foundation/Foundation.h>
@class PAJSBridgeController;
@class PAJSResult;

@interface PAJSCommandDelegate : NSObject

@property (weak)PAJSBridgeController* controller;

- (id)initWithController:(PAJSBridgeController*)aController;

- (PAJSResult*)executeCommand:(NSDictionary*)commandInfo;

- (PAJSResult*)loadNewPage;

- (PAJSResult*)gotohomepage;

- (PAJSResult*)showAgentWindow;

- (PAJSResult*)hideAgentWindow;

- (void)sendResult:(PAJSResult*)result;

- (NSRect)defaultWindowFrame;

@end
