//
//  PAProtectedVersionListController.m
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PAProtectedVersionListController.h"
#import <PAIpcService/PAIpcService.h>
#import "PAJSResult.h"
#import "PAJSInvokedCommand.h"
#import "PAJSCommandDelegate.h"
#import "PAJSConstants.h"

@interface  PAProtectedVersionListController(){
}
@property (weak)PAJSCommandDelegate* delegate;
@property (strong)NSMutableArray* guidArray;
@property (strong)NSMutableArray* resultArray;
@property (strong)PAJSInvokedCommand* command;

@end

@implementation PAProtectedVersionListController

- (instancetype)initWithCommandDelegate:(PAJSCommandDelegate*)aDelegate command:(PAJSInvokedCommand*)command
{
  self = [super init];
  if (self) {
    self.delegate = aDelegate;
    self.guidArray = [NSMutableArray array];
    self.resultArray = [NSMutableArray array];
    self.command = command;
  }
  return self;
}

- (void)executeCommand {
  if (self.command.arguments.count > 0) {
    NSArray* array = (NSArray*) self.command.arguments[0];
    self.guidArray = [NSMutableArray arrayWithArray:array];
    if (self.guidArray.count) {
      [self executeScript:self.guidArray[0]];
    }
  }
}

- (void)executeScript:(NSDictionary*)info {
  NSString* guid = info[JSGuid];
  NSString* version = info[JSVersion];
  if (guid && version) {
    PAServiceInfo *info = [[PAServiceInfo alloc]init];
    info.guid = guid;
    info.version = version;
    info.serviceCategory = PASupportServiceCategoryRepair;
    info.service = PASupportServiceGetProtectedHistory;
    info.serviceMode = PAServiceModeNormal;
    [[PAInterProcessManager sharedManager]executeService:info withReply:^(BOOL success, NSError *error, id result) {
      
      NSArray* array = (NSArray*)result;
      NSMutableArray* mutableArr = [NSMutableArray array];
      for(NSDictionary* item in array) {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:JSDateFormat1];
        NSDate* date = [formatter dateFromString:item[JSDateString]];
        [formatter setDateFormat:JSDateFormat2];
        NSString *stringFromDate = [formatter stringFromDate:date];
        
        NSMutableDictionary* dict = [item mutableCopy];
        dict[JSProtectedDate] = stringFromDate;
        [mutableArr addObject:dict];
      }
      NSDictionary* resutVersion = @{JSGuid:guid, JSData:mutableArr};
      [self.resultArray addObject:resutVersion];
      [self.guidArray removeObjectAtIndex:0];
      if(self.guidArray.count == 0) {
        PAJSResult* jsResult = [[PAJSResult alloc] init];
        jsResult.status = 0;
        jsResult.data = self.resultArray;
        jsResult.callbackMethodName = self.command.callbackMethod;
        [self.delegate performSelectorOnMainThread:@selector(sendResult:) withObject:jsResult waitUntilDone:NO];
      }
      else {
        [self performSelectorOnMainThread:@selector(executeScript:) withObject:self.guidArray[0] waitUntilDone:NO];
      }
      
    }];
  }
}

@end
