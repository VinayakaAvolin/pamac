//
//  PAJSInvokedCommand.m
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.


#import "PAJSInvokedCommand.h"
#import "PAJSConstants.h"
@implementation PAJSInvokedCommand

+ (PAJSInvokedCommand*) commandFromDictionary:(NSDictionary*)jsonDict {
  return [[PAJSInvokedCommand alloc] initFromJson:jsonDict];
}

- (id)initFromJson:(NSDictionary*)jsonDict {
  NSString* callbackMethod = jsonDict[JSCallbackMethodNameKey];
  NSString* className = jsonDict[JSClassNameKey];
  NSString* methodName = jsonDict [JSOperationNameKey];
  BOOL isSync = [jsonDict[JSISSyncMethodKey] isEqualToString:@"1"] ? TRUE : FALSE;
  NSMutableArray* arguments = jsonDict [JSArgumentsKey];
  return [self initWithArguments:arguments callbackMethod:callbackMethod className:className methodName:methodName isSync:isSync];
  
}

- (id)initWithArguments:(NSArray*)arguments
         callbackMethod:(NSString*)callbackMethod
              className:(NSString*)className
             methodName:(NSString*)methodName
                 isSync:(BOOL)isSync
{
  self = [super init];
  if (self != nil) {
    _arguments = arguments;
    _callbackMethod = callbackMethod;
    _className = className;
    _methodName = methodName;
    _isSync = isSync;
  }
  return self;
}

@end
