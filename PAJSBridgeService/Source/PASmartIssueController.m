//
//  PASmartIssueController.m
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <PACoreServices/PACoreServices.h>
#import "PASmartIssueController.h"
#import "PAJSCommandDelegate.h"
#import "PAJSInvokedCommand.h"
#import "PAJSBridgeController.h"
#import "PAJSResult.h"
#import "NSString+Additions.h"

#define HOST @"http://posttestserver.com/post.php"

@interface PASmartIssueController()  {
  PAFileSystemManager *_fileSystemManager;
  
}
@property (weak)PAJSCommandDelegate* delegate;
@property (strong) NSOperationQueue *operationQueue;

@end
@implementation PASmartIssueController
- (instancetype)initWithCommandDelegate:(PAJSCommandDelegate*)aDelegate;
{
  self = [super init];
  if (self) {
    self.delegate = aDelegate;
    self.operationQueue = [[NSOperationQueue alloc] init];
    _fileSystemManager = [[PAFileSystemManager alloc] init];
  }
  return self;
}

- (PAJSResult*)executeCommand:(PAJSInvokedCommand*)command {
  if (command.isSync) {
    return [self executeSyncCommand:command];
  }
  [self executeAsyncCommand:command];
  return nil;
}

- (PAJSResult*)executeSyncCommand:(PAJSInvokedCommand*)command {
  if([command.methodName isEqualToString:@"GetIssueData"]) {
    return [self issueData:command];
  }
  else if([command.methodName isEqualToString:@"DeleteIssue"]) {
    return [self deleteIssue:command];
  }
  
  return nil;
}

- (void)executeAsyncCommand:(PAJSInvokedCommand*)command  {
  if ([command.methodName isEqualToString:@"CreateIssue"]) {
    NSInvocationOperation *operation = [[NSInvocationOperation alloc]
                                        initWithTarget:self
                                        selector:@selector(createIssue:)
                                        object:command];
    [self.operationQueue addOperation:operation];
  } else if([command.methodName isEqualToString:@"CreateIssueVACode"]) {
    NSInvocationOperation *operation = [[NSInvocationOperation alloc]
                                        initWithTarget:self
                                        selector:@selector(createIssueVaCode:)
                                        object:command];
    [self.operationQueue addOperation:operation];
  } else if([command.methodName isEqualToString:@"SubmitIssue"]) {
    NSInvocationOperation *operation = [[NSInvocationOperation alloc]
                                        initWithTarget:self
                                        selector:@selector(submitIssue:)
                                        object:command];
    [self.operationQueue addOperation:operation];
  }
  else if([command.methodName isEqualToString:@"DeleteIssue"]) {
    NSInvocationOperation *operation = [[NSInvocationOperation alloc]
                                        initWithTarget:self
                                        selector:@selector(deleteIssue:)
                                        object:command];
    [self.operationQueue addOperation:operation];
  }
  else if([command.methodName isEqualToString:@"GetIssueData"]) {
    NSInvocationOperation *operation = [[NSInvocationOperation alloc]
                                        initWithTarget:self
                                        selector:@selector(issueData:)
                                        object:command];
    [self.operationQueue addOperation:operation];
  }
}

- (void)createIssue:(PAJSInvokedCommand*)command {
  NSString *uuid = [NSString uuid];
  if (command.arguments.count > 1) {
    NSString* inputPath = command.arguments[1];
    NSString* outputPath = [_fileSystemManager smartIssuesFolderPath];
    outputPath = [outputPath stringByAppendingPathComponent:uuid];
    outputPath = [outputPath stringByAppendingString:@".xml"];
    PAEnPointInfoManager *manager = [[PAEnPointInfoManager alloc]initWithInputXml:inputPath outputXml:outputPath source:PATargetScriptSourceTypeOthers];
    [manager createAllEndpointInfoWithCompletion:^(BOOL success) {
      //[manager deleteEndpointInfo];
      PAJSResult* result = [[PAJSResult alloc] init];
      result.status = 0;
      result.data = uuid;
      result.callbackMethodName = command.callbackMethod;
      [self.delegate performSelectorOnMainThread:@selector(sendResult:) withObject:result waitUntilDone:NO];
    }];
  }
}

- (void)createIssueVaCode:(PAJSInvokedCommand*)command {
  NSString *uuid = [NSString uuid];
  @try
  {
    if (command.arguments.count > 1) {
      NSString* inputPath = command.arguments[1];
      NSString* outputPath = [_fileSystemManager smartIssuesFolderPath];
      outputPath = [outputPath stringByAppendingPathComponent:uuid];
      outputPath = [outputPath stringByAppendingString:@".xml"];
      PAEnPointInfoManager *manager = [[PAEnPointInfoManager alloc]initWithInputXml:inputPath outputXml:outputPath source:PATargetScriptSourceTypeOthers];
      [manager createAllEndpointInfoWithCompletion:^(BOOL success) {
        //[manager deleteEndpointInfo];
        PAJSResult* result = [[PAJSResult alloc] init];
        result.status = 0;
        result.data = uuid;
        result.callbackMethodName = command.callbackMethod;
        [self.delegate performSelectorOnMainThread:@selector(sendResult:) withObject:result waitUntilDone:NO];
      }];
    }
  }
  @catch (NSException *exception)
  {
    //Handle this error
  }
}

- (void)submitIssue:(PAJSInvokedCommand*)command {
  if (command.arguments.count > 1) {
    NSString* filePath = command.arguments[0];
    NSString* uploadURL = command.arguments[1];
    filePath = [filePath stringByConvertingIntoMacCompatibleFilePath];
    PAEnPointInfoManager *manager = [[PAEnPointInfoManager alloc] initWithOutputXml:filePath];
    
    [manager submitEndpointInfoWithUploadURL:uploadURL parameters:nil completion:^(BOOL success, NSError* error, NSString *fileUploaded) {
      if (success) {
        PAJSResult* result = [[PAJSResult alloc] init];
        result.status = 0;
        result.callbackMethodName = command.callbackMethod;
        result.data = @(success);
        [self.delegate performSelectorOnMainThread:@selector(sendResult:) withObject:result waitUntilDone:NO];
      }
    }];
  }
}

- (PAJSResult*)deleteIssue:(PAJSInvokedCommand*)command {
  if (command.arguments.count > 0) {
    NSString* filePath = command.arguments[0];
    filePath = [filePath stringByConvertingIntoMacCompatibleFilePath];
    PAEnPointInfoManager *manager = [[PAEnPointInfoManager alloc] initWithOutputXml:filePath];
    [manager deleteEndpointInfo];
    
    PAJSResult* result = [[PAJSResult alloc] init];
    result.status = 0;
    result.data = @(1);
    return result;
  }
  return nil;
}

- (PAJSResult*)issueData:(PAJSInvokedCommand*)command {
  if (command.arguments.count > 2) {
    NSString* issueId = command.arguments[0];
    NSString* className = command.arguments[1];
    NSString* propertyName = command.arguments[2];
    NSString* outputPath = [_fileSystemManager smartIssuesFolderPath];
    outputPath = [outputPath stringByAppendingPathComponent:issueId];
    outputPath = [outputPath stringByAppendingString:@".xml"];
    
    PAEnPointInfoManager *manager = [[PAEnPointInfoManager alloc]initWithOutputXml:outputPath];
    NSString *propertyValue = [manager valueOfProperty:propertyName inClass:className];
    PAJSResult* result = [[PAJSResult alloc] init];
    result.status = 0;
    result.data = propertyValue == nil ? @"" : propertyValue;
    return result;
  }
  return nil;
  
}
@end


