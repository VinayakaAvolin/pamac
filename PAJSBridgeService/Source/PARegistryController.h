//
//  PARegistryController.h
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
@class PAJSResult;
@class PAJSInvokedCommand;
@class PAJSCommandDelegate;
@interface PARegistryController : NSObject


- (instancetype)initWithCommandDelegate:(PAJSCommandDelegate*)aDelegate;

- (PAJSResult*)executeCommand:(PAJSInvokedCommand*)command;

@end
