//
//  PADownloadOffersController.m
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PADownloadOffersController.h"
#import <PAIpcService/PAIpcService.h>
#import "PAJSResult.h"
#import "PAJSInvokedCommand.h"
#import "PAJSCommandDelegate.h"
#import "PAJSConstants.h"
#import "PAServiceConstants.h"
#import "PACommonUtilityController.h"

@interface  PADownloadOffersController()

@property (weak)PAJSCommandDelegate* delegate;
@property (strong) NSOperationQueue *operationQueue;
@property (strong) NSString* callback;
@end


@implementation PADownloadOffersController

- (instancetype)initWithCommandDelegate:(PAJSCommandDelegate*)aDelegate
{
  self = [super init];
  if (self) {
    self.delegate = aDelegate;
    self.operationQueue = [[NSOperationQueue alloc] init];
  }
  return self;
}

- (PAJSResult*)executeCommand:(PAJSInvokedCommand*)command {
  if (command.isSync) {
    return [self executeSyncCommand:command];
  }
  [self executeAsyncCommand:command];
  return nil;
}
- (PAJSResult*)executeSyncCommand:(PAJSInvokedCommand*)command {
  if([command.methodName isEqualToString:@"CancelDownload"]) {
    return [self cancelDownload:command];
  }
  return nil;
}
- (void)executeAsyncCommand:(PAJSInvokedCommand*)command  {
  if([command.methodName isEqualToString:@"DownloadAndInstall"]) {
    NSInvocationOperation *operation = [[NSInvocationOperation alloc]
                                        initWithTarget:self
                                        selector:@selector(downloadAndInstall:)
                                        object:command];
    [self.operationQueue addOperation:operation];
  }
}

- (void)downloadAndInstall:(PAJSInvokedCommand*)command {
  if (command.arguments.count > 2) {
    NSString *guid = (NSString* )command.arguments[0];
    NSString *version = (NSString* )command.arguments[1];
    self.callback = command.callbackMethod;
    if (guid && version) {
      PAServiceInfo *info = [[PAServiceInfo alloc]init];
      info.guid = guid;
      info.version = version;
      info.service = PASupportServiceDownload;
      info.serviceCategory = PASupportServiceCategoryDownloadOffers;
      info.serviceMode = PAServiceModeNormal;
      // Add download progress notification
      [[NSDistributedNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveNotification:) name:kPASupportServiceDownloadStatusNotificationName object:kPASupportServiceDownloadOfferNotificationObjectName];
      
      __block NSDate *startTime;
      startTime = [NSDate date];
      
      NSString *jobguid = [[[NSUUID UUID] UUIDString] uppercaseString];
      jobguid = [@"{" stringByAppendingString:[jobguid stringByAppendingString:@"}"]];
      
      __block NSString *log = [@"CONTENT__" stringByAppendingString:[info.guid stringByAppendingString:[@"__" stringByAppendingString:[info.version stringByAppendingString:[@"__dm_do_dl_start" stringByAppendingString:[@"__Client__0__0__" stringByAppendingString:[jobguid stringByAppendingString:[@"__" stringByAppendingString:[@"0" stringByAppendingString:[@"__0__MAC Others__" stringByAppendingString:@"0"]]]]]]]]]];
      [self WriteDMLog:(NSString *)log];
      
      [[PAInterProcessManager sharedManager]executeService:info withReply:^(BOOL success, NSError *error, id result) {
        if (success && result && [result isKindOfClass:[NSDictionary class]]) {
          
          NSTimeInterval seconds = -[startTime timeIntervalSinceNow];
          NSString *duration = [NSString stringWithFormat:@"%.f", seconds];
          
          log = [@"CONTENT__" stringByAppendingString:[info.guid stringByAppendingString:[@"__" stringByAppendingString:[info.version stringByAppendingString:[@"__dm_do_dl_success" stringByAppendingString:[@"__Client__0__0__" stringByAppendingString:[jobguid stringByAppendingString:[@"__" stringByAppendingString:[@"0" stringByAppendingString:[@"__0__MAC Others__" stringByAppendingString:@"0"]]]]]]]]]];
          [self WriteDMLog:(NSString *)log];
          
          log = [@"CONTENT__" stringByAppendingString:[info.guid stringByAppendingString:[@"__" stringByAppendingString:[info.version stringByAppendingString:[@"__dm_dl_duration" stringByAppendingString:[@"__Client__0__0__" stringByAppendingString:[jobguid stringByAppendingString:[@"__" stringByAppendingString:[@"0" stringByAppendingString:[@"__0__MAC Others__" stringByAppendingString:duration]]]]]]]]]];
          [self WriteDMLog:(NSString *)log];
          
          log = [@"CONTENT__" stringByAppendingString:[info.guid stringByAppendingString:[@"__" stringByAppendingString:[info.version stringByAppendingString:[@"__dm_dl_size" stringByAppendingString:[@"__Client__0__0__" stringByAppendingString:[jobguid stringByAppendingString:[@"__" stringByAppendingString:[@"0" stringByAppendingString:[@"__0__MAC Others__" stringByAppendingString:@"0"]]]]]]]]]];
          [self WriteDMLog:(NSString *)log];
          
          // remove notification
          [[NSDistributedNotificationCenter defaultCenter] removeObserver:self name:kPASupportServiceDownloadStatusNotificationName object:kPASupportServiceDownloadOfferNotificationObjectName];
          
          NSMutableDictionary *responseDict = [(NSDictionary *)result mutableCopy];
          [responseDict setObject:@(1) forKey:JSProgress];
          
          PADownloadStatus downloadStatus = (PADownloadStatus)[responseDict[kPASupportServiceResponseKeyStatus] integerValue];
          if (downloadStatus == PADownloadStatusComplete) {
            info.service = PASupportServiceInstall;
            info.downloadedItemPath = responseDict[kPASupportServiceResponseKeyDownloadPath];
            if ([responseDict[kPASupportServiceResponseKeyInstallerType] isEqualToString:@"DMG"]) {
              [responseDict setObject:@(10) forKey:JSDownLoadStatus];
            }else if ([responseDict[kPASupportServiceResponseKeyInstallerType] isEqualToString:@"PKG"]) {
              [responseDict setObject:@(5) forKey:JSDownLoadStatus];
            }else {
              [responseDict setObject:@(6) forKey:JSDownLoadStatus];
            }
            PAJSResult* jsResult = [[PAJSResult alloc] init];
            jsResult.status = 0;
            jsResult.data = responseDict;
            jsResult.callbackMethodName = self.callback;
            [self.delegate performSelectorOnMainThread:@selector(sendResult:) withObject:jsResult waitUntilDone:NO];
            
            log = [@"CONTENT__" stringByAppendingString:[info.guid stringByAppendingString:[@"__" stringByAppendingString:[info.version stringByAppendingString:[@"__dm_do_inst_start" stringByAppendingString:[@"__Client__0__0__" stringByAppendingString:[jobguid stringByAppendingString:[@"__" stringByAppendingString:[@"0" stringByAppendingString:[@"__0__MAC Others__" stringByAppendingString:@"0"]]]]]]]]]];
            [self WriteDMLog:(NSString *)log];
            
            [[PAInterProcessManager sharedManager] executeService:info withReply:^(BOOL success, NSError *error, id result) {
              if (success) {
                log = [@"CONTENT__" stringByAppendingString:[info.guid stringByAppendingString:[@"__" stringByAppendingString:[info.version stringByAppendingString:[@"__dm_do_inst_success" stringByAppendingString:[@"__Client__0__0__" stringByAppendingString:[jobguid stringByAppendingString:[@"__" stringByAppendingString:[@"0" stringByAppendingString:[@"__0__MAC Others__" stringByAppendingString:@"0"]]]]]]]]]];
                [self WriteDMLog:(NSString *)log];
                
                log = [@"CONTENT__" stringByAppendingString:[info.guid stringByAppendingString:[@"__" stringByAppendingString:[info.version stringByAppendingString:[@"__dm_do_success" stringByAppendingString:[@"__Client__0__0__" stringByAppendingString:[jobguid stringByAppendingString:[@"__" stringByAppendingString:[@"0" stringByAppendingString:[@"__0__MAC Others__" stringByAppendingString:@"0"]]]]]]]]]];
                [self WriteDMLog:(NSString *)log];
                
                
                if ([responseDict[kPASupportServiceResponseKeyInstallerType] isEqualToString:@"DMG"]) {
                  [responseDict setObject:@(11) forKey:JSDownLoadStatus];
                }else  {
                  [responseDict setObject:@(6) forKey:JSDownLoadStatus];
                }
                PAJSResult* jsResult = [[PAJSResult alloc] init];
                jsResult.status = 0;
                jsResult.data = responseDict;
                jsResult.callbackMethodName = self.callback;
                [self.delegate performSelectorOnMainThread:@selector(sendResult:) withObject:jsResult waitUntilDone:NO];
                [self performSelectorOnMainThread:@selector(finishInstall:) withObject:jsResult waitUntilDone:NO];
              }
              else
              {
                log = [@"CONTENT__" stringByAppendingString:[info.guid stringByAppendingString:[@"__" stringByAppendingString:[info.version stringByAppendingString:[@"__dm_do_inst_fail" stringByAppendingString:[@"__Client__0__0__" stringByAppendingString:[jobguid stringByAppendingString:[@"__" stringByAppendingString:[@"0" stringByAppendingString:[@"__0__MAC Others__" stringByAppendingString:@"0"]]]]]]]]]];
                [self WriteDMLog:(NSString *)log];
                
                log = [@"CONTENT__" stringByAppendingString:[info.guid stringByAppendingString:[@"__" stringByAppendingString:[info.version stringByAppendingString:[@"__dm_do_fail" stringByAppendingString:[@"__Client__0__0__" stringByAppendingString:[jobguid stringByAppendingString:[@"__" stringByAppendingString:[@"0" stringByAppendingString:[@"__0__MAC Others__" stringByAppendingString:@"0"]]]]]]]]]];
                [self WriteDMLog:(NSString *)log];
                
              }
            }];
          }
        }
        else {
          log = [@"CONTENT__" stringByAppendingString:[info.guid stringByAppendingString:[@"__" stringByAppendingString:[info.version stringByAppendingString:[@"__dm_do_dl_fail" stringByAppendingString:[@"__Client__0__0__" stringByAppendingString:[jobguid stringByAppendingString:[@"__" stringByAppendingString:[@"0" stringByAppendingString:[@"__0__MAC Others__" stringByAppendingString:@"0"]]]]]]]]]];
          [self WriteDMLog:(NSString *)log];
          
          log = [@"CONTENT__" stringByAppendingString:[info.guid stringByAppendingString:[@"__" stringByAppendingString:[info.version stringByAppendingString:[@"__dm_do_fail" stringByAppendingString:[@"__Client__0__0__" stringByAppendingString:[jobguid stringByAppendingString:[@"__" stringByAppendingString:[@"0" stringByAppendingString:[@"__0__MAC Others__" stringByAppendingString:@"0"]]]]]]]]]];
          [self WriteDMLog:(NSString *)log];
          
          NSMutableDictionary *responseDict = [(NSDictionary *)result mutableCopy];
          [responseDict setObject:@(0) forKey:JSProgress];
          [responseDict setObject:@(-1) forKey:JSDownLoadStatus];
          PAJSResult* jsResult = [[PAJSResult alloc] init];
          jsResult.status = 0;
          jsResult.data = responseDict;
          jsResult.callbackMethodName = self.callback;
          [self.delegate performSelectorOnMainThread:@selector(sendResult:) withObject:jsResult waitUntilDone:NO];
        }
        
        
      }];
    }
  }
}

- (void)didReceiveNotification:(NSNotification *)notification {
  PAJSResult* jsResult = [[PAJSResult alloc] init];
  jsResult.status = 0;
  jsResult.data = notification.userInfo;
  jsResult.callbackMethodName = self.callback;
  [self.delegate performSelectorOnMainThread:@selector(sendResult:) withObject:jsResult waitUntilDone:NO];
}

- (void)finishInstall:(PAJSResult*)result {
  NSMutableDictionary* data = [(NSDictionary*)result.data mutableCopy];
  [data setObject:@(1) forKey:JSDownLoadStatus];
  result.data = data;
  //    [self.delegate sendResult:result];
  // [self.delegate performSelector:@selector(sendResult:) withObject:result afterDelay:3];
}

- (PAJSResult*)cancelDownload:(PAJSInvokedCommand*)command {
  if (command.arguments.count > 1) {
    NSString *guid = (NSString* )command.arguments[0];
    NSString *version = (NSString* )command.arguments[1];
    NSDictionary *responseDict =@{kPASupportServiceResponseKeyGuid:guid, kPASupportServiceResponseKeyVersion:version};
    [[NSDistributedNotificationCenter defaultCenter] postNotificationName:kPASupportServiceDownloadCancelNotificationName object:kPASupportServiceDownloadOfferNotificationObjectName userInfo:responseDict deliverImmediately:true];
  }
  return nil;
}

- (void)WriteDMLog:(NSString *)logmsg {
  NSMutableDictionary* dict = [NSMutableDictionary dictionary];
  [dict setObject:@[logmsg] forKey:JSArgumentsKey];
  [dict setObject:@"1" forKey:JSISSyncMethodKey];
  [dict setObject:@"WriteUsageLog" forKey:JSOperationNameKey];
  PAJSInvokedCommand*command = [PAJSInvokedCommand commandFromDictionary:dict];
  PACommonUtilityController *controller= [[PACommonUtilityController alloc] initWithCommandDelegate:self];
  PAJSResult*res = [controller executeCommand:command];
}
@end
