//
//  PALoggerController.m
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PALoggerController.h"
#import <PACoreServices/PACoreServices.h>
#import "PAJSResult.h"
#import "PAJSInvokedCommand.h"
#import "PAJSCommandDelegate.h"
#import "PAJSConstants.h"

@interface  PALoggerController()

@property (weak)PAJSCommandDelegate* delegate;
@property (strong) NSOperationQueue *operationQueue;

@end

@implementation PALoggerController
- (instancetype)initWithCommandDelegate:(PAJSCommandDelegate*)aDelegate
{
  self = [super init];
  if (self) {
    self.delegate = aDelegate;
    self.operationQueue = [[NSOperationQueue alloc] init];
  }
  return self;
}

- (PAJSResult*)executeCommand:(PAJSInvokedCommand*)command {
  if (command.isSync) {
    return [self executeSyncCommand:command];
  }
  [self executeAsyncCommand:command];
  return nil;
}

- (PAJSResult*)executeSyncCommand:(PAJSInvokedCommand*)command {
  if ([command.methodName isEqualToString:@"Debug"]) {
    
  }
  return nil;
}

- (void)executeAsyncCommand:(PAJSInvokedCommand*)command {
  
}
@end
