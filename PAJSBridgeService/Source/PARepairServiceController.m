//
//  PARepairServiceController.m
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <PACoreServices/PACoreServices.h>
#import <PASupportService/PASupportService.h>
#import <PAIpcService/PAIpcService.h>
#import "PARepairServiceController.h"
#import "PAJSResult.h"
#import "PAJSInvokedCommand.h"
#import "PAJSCommandDelegate.h"
#import "PARestoredListController.h"
#import "PAProtectedVersionListController.h"
#import "PAJSConstants.h"

@interface  PARepairServiceController(){
}
@property (weak)PAJSCommandDelegate* delegate;
@property (strong) NSOperationQueue *operationQueue;
@property (strong)NSMutableDictionary* protectStatusDict;

@end
@implementation PARepairServiceController

- (instancetype)initWithCommandDelegate:(PAJSCommandDelegate*)aDelegate;
{
  self = [super init];
  if (self) {
    self.delegate = aDelegate;
    self.operationQueue = [[NSOperationQueue alloc] init];
    self.protectStatusDict = [NSMutableDictionary dictionary];
  }
  return self;
}

- (PAJSResult*)executeCommand:(PAJSInvokedCommand*)command {
  if (command.isSync) {
    return [self executeSyncCommand:command];
  }
  [self executeAsyncCommand:command];
  return nil;
}

- (PAJSResult*)executeSyncCommand:(PAJSInvokedCommand*)command {
  if ([command.methodName isEqualToString:@"CheckProtectStatus"]) {
    return [self checkProtectStatus:command];
  }
  if ([command.methodName isEqualToString:@"DoProtect"]) {
    return [self doProtect:command];
  }
  return nil;
}

- (void)executeAsyncCommand:(PAJSInvokedCommand*)command {
  if([command.methodName isEqualToString:@"DoProtect"]) {
    NSInvocationOperation *operation = [[NSInvocationOperation alloc]
                                        initWithTarget:self
                                        selector:@selector(doProtect:)
                                        object:command];
    [self.operationQueue addOperation:operation];
  }else if ([command.methodName isEqualToString:@"GetProtectedVersionList"]) {
    NSInvocationOperation *operation = [[NSInvocationOperation alloc]
                                        initWithTarget:self
                                        selector:@selector(loadVersionList:)
                                        object:command];
    [self.operationQueue addOperation:operation];
  }
  else if([command.methodName isEqualToString:@"DoRestore"]) {
    NSInvocationOperation *operation = [[NSInvocationOperation alloc]
                                        initWithTarget:self
                                        selector:@selector(doRestore:)
                                        object:command];
    [self.operationQueue addOperation:operation];
  }else if([command.methodName isEqualToString:@"GetUndoList"]) {
    NSInvocationOperation *operation = [[NSInvocationOperation alloc]
                                        initWithTarget:self
                                        selector:@selector(loadUndoList:)
                                        object:command];
    [self.operationQueue addOperation:operation];
  }
  else if([command.methodName isEqualToString:@"UndoProtected"]) {
    NSInvocationOperation *operation = [[NSInvocationOperation alloc]
                                        initWithTarget:self
                                        selector:@selector(undoProtected:)
                                        object:command];
    [self.operationQueue addOperation:operation];
  }
}

- (PAJSResult*)doProtect:(PAJSInvokedCommand*)command {
  if (command.arguments.count > 1) {
    PAJSResult* result = [[PAJSResult alloc] init];
    result.status = 0;
    NSString *guid = (NSString* )command.arguments[0];
    NSString *version = (NSString* )command.arguments[1];
    if (guid && version) {
      PAServiceInfo *info = [[PAServiceInfo alloc]init];
      info.guid = guid;
      info.version = version;
      info.serviceCategory = PASupportServiceCategoryRepair;
      info.service = PASupportServiceProtect;
      info.serviceMode = PAServiceModeNormal;
      [[PAInterProcessManager sharedManager]executeService:info withReply:^(BOOL success, NSError *error, id result) {
        NSDictionary* dict = (NSDictionary*)result;
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:JSDateFormat1];
        NSDate* date = [formatter dateFromString:dict[JSDateString]];
        [formatter setDateFormat:JSDateFormat2];
        NSString *stringFromDate = [formatter stringFromDate:date];
        PAJSResult* jsResult = [[PAJSResult alloc] init];
        jsResult.status = 0;
        jsResult.data = @{JSGuid:dict[JSGuid], JSProtectedDate: stringFromDate};
        jsResult.callbackMethodName = command.callbackMethod;
        [self.delegate performSelectorOnMainThread:@selector(sendResult:) withObject:jsResult waitUntilDone:NO];
        
      }];
      
    }
  }
  return nil;
}

- (void)loadVersionList:(PAJSInvokedCommand*)command {
  if (command.arguments.count > 0) {
    PAProtectedVersionListController* controller = [[PAProtectedVersionListController alloc] initWithCommandDelegate:self.delegate command:command];
    [controller executeCommand];
  }
  
}

- (void)doRestore:(PAJSInvokedCommand*)command {
  if (command.arguments.count > 2) {
    PAJSResult* result = [[PAJSResult alloc] init];
    result.status = 0;
    NSString *guid = (NSString* )command.arguments[0];
    NSString *version = (NSString* )command.arguments[1];
    NSString *dateProtected = (NSString* )command.arguments[2];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:JSDateFormat2];
    NSDate* date = [formatter dateFromString:dateProtected];
    [formatter setDateFormat:JSDateFormat1];
    NSString *stringFromDate = [formatter stringFromDate:date];
    
    if (guid && version) {
      PAServiceInfo *info = [[PAServiceInfo alloc]init];
      info.guid = guid;
      info.version = version;
      info.serviceCategory = PASupportServiceCategoryRepair;
      info.service = PASupportServiceRestore;
      info.serviceMode = PAServiceModeNormal;
      info.protectedItemName = stringFromDate;
      [[PAInterProcessManager sharedManager]executeService:info withReply:^(BOOL success, NSError *error, id result) {
        NSDictionary* dict = (NSDictionary*)result;
        PAJSResult* jsResult = [[PAJSResult alloc] init];
        jsResult.status = 0;
        jsResult.data = @{JSGuid:dict[JSGuid]};
        jsResult.callbackMethodName = command.callbackMethod;
        [self.delegate performSelectorOnMainThread:@selector(sendResult:) withObject:jsResult waitUntilDone:NO];
        
      }];
    }
  }
}

- (void)undoProtected:(PAJSInvokedCommand*)command {
  if (command.arguments.count > 1) {
    PAJSResult* result = [[PAJSResult alloc] init];
    result.status = 0;
    NSString *guid = (NSString* )command.arguments[0];
    NSString *version = (NSString* )command.arguments[1];
    if (guid && version) {
      PAServiceInfo *info = [[PAServiceInfo alloc]init];
      info.guid = guid;
      info.version = version;
      info.serviceCategory = PASupportServiceCategoryRepair;
      info.service = PASupportServiceUndoRestore;
      info.serviceMode = PAServiceModeNormal;
      [[PAInterProcessManager sharedManager]executeService:info withReply:^(BOOL success, NSError *error, id result) {
        NSDictionary* dict = (NSDictionary*)result;
        PAJSResult* jsResult = [[PAJSResult alloc] init];
        jsResult.status = 0;
        jsResult.data = @{JSGuid:dict[JSGuid]};
        jsResult.callbackMethodName = command.callbackMethod;
        [self.delegate performSelectorOnMainThread:@selector(sendResult:) withObject:jsResult waitUntilDone:NO];
        
      }];
    }
    
  }
}

- (void)loadUndoList:(PAJSInvokedCommand*)command {
  PARestoredListController* restoredListController = [[PARestoredListController alloc] initWithCommandDelegate:self.delegate command:command];
  [restoredListController executeCommand];
}

- (PAJSResult*)checkProtectStatus:(PAJSInvokedCommand*)command {
  if (command.arguments.count > 0) {
    PAJSResult* result = [[PAJSResult alloc] init];
    result.status = 0;
    NSString *guid = (NSString* )command.arguments[0];
    result.data = self.protectStatusDict[guid];
    return result;
  }
  return nil;
}
@end
