//
//  PAJSConstants.m
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.


#import "PAJSConstants.h"

NSString *const JSClassNameKey = @"JSClassNameKey";

NSString *const JSOperationNameKey = @"JSOperationNameKey";

NSString *const JSArgumentsKey = @"JSArgumentsKey";

NSString *const JSCallbackMethodNameKey = @"JSCallbackMethodNameKey";

NSString *const JSISSyncMethodKey = @"JSISSyncMethodKey";

NSString *const JSClassFileSystemAccess = @"File";

NSString *const JSClassSystemUtility = @"SystemUtility";

NSString *const JSClassCommonUtility = @"CommonUtility";

NSString *const JSClassUIUtility = @"UIUtility";

NSString *const JSClassRegistry = @"Registry";

NSString *const JSClassSmartIssue = @"SmartIssue";

NSString *const JSClassRepairService = @"RepairService";

NSString *const JSClassEndpointInfo = @"EndpointInfo";

NSString *const JSClassDownloadOffers =  @"DownloadOffers";

NSString *const JSClassOptimization = @"Optimization";

NSString *const JSClassSupportAction = @"SupportAction";

NSString *const JSClassSync = @"Sync";

NSString *const JSStatus = @"Status";

NSString *const JSMessage = @"Message";

NSString *const JSData = @"Data";

NSString *const JSSetVolumeScript = @"set volume output volume";

NSString *const JSGuid = @"guid";

NSString *const JSVersion = @"version";

NSString *const JSDateFormat1 = @"dd-MMM-yyyy_HH_mm_ss";

NSString *const JSDateFormat2 = @"dd-MMM-yyyy HH:mm:ss";

NSString *const JSDateString = @"dateString";

NSString *const JSProtectedDate = @"lastProtectedDate";

NSString *const JSProgress = @"progress";

NSString *const JSDownLoadStatus = @"status";
