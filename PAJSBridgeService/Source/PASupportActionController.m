//
//  PASupportActionController.m
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PASupportActionController.h"
#import <PACoreServices/PACoreServices.h>
#import <PASupportService/PASupportService.h>
#import <PAIpcService/PAIpcService.h>
#import "PAJSResult.h"
#import "PAJSInvokedCommand.h"
#import "PAJSCommandDelegate.h"
#import "PAJSConstants.h"
#import "PALoginAgent.h"


@interface  PASupportActionController()

@property (weak)PAJSCommandDelegate* delegate;
@property (strong) NSOperationQueue *operationQueue;

@end

@implementation PASupportActionController
- (instancetype)initWithCommandDelegate:(PAJSCommandDelegate*)aDelegate
{
  self = [super init];
  if (self) {
    self.delegate = aDelegate;
    self.operationQueue = [[NSOperationQueue alloc] init];
  }
  return self;
}

- (PAJSResult*)executeCommand:(PAJSInvokedCommand*)command {
  if (command.isSync) {
    return [self executeSyncCommand:command];
  }
  [self executeAsyncCommand:command];
  return nil;
}

- (PAJSResult*)executeSyncCommand:(PAJSInvokedCommand*)command {
  return nil;
}

- (void)executeAsyncCommand:(PAJSInvokedCommand*)command {
  if ([command.methodName isEqualToString:@"Evaluate"]) {
    NSInvocationOperation *operation = [[NSInvocationOperation alloc]
                                        initWithTarget:self
                                        selector:@selector(evaluate:)
                                        object:command];
    [self.operationQueue addOperation:operation];
  }
  else if([command.methodName isEqualToString:@"EvaluateTest"]) {
    NSInvocationOperation *operation = [[NSInvocationOperation alloc]
                                        initWithTarget:self
                                        selector:@selector(evaluateTest:)
                                        object:command];
    [self.operationQueue addOperation:operation];
  }
  else if([command.methodName isEqualToString:@"EvaluateMessageSA"]) {
    NSInvocationOperation *operation = [[NSInvocationOperation alloc]
                                        initWithTarget:self
                                        selector:@selector(evaluateMessageSA:)
                                        object:command];
    [self.operationQueue addOperation:operation];
  }
}

- (void)evaluate:(PAJSInvokedCommand*)command {
  if(command.arguments.count  > 0) {
    PAServiceInfo *info = [[PAServiceInfo alloc]init];
    info.guid = command.arguments[0];
    info.serviceCategory = PASupportServiceCategorySupportAction;
    info.service = PASupportServiceSupportActionFix;
    [[PAInterProcessManager sharedManager] executeService:info withReply:^(BOOL success, NSError *error, id result) {
      PAJSResult* jsResult = [[PAJSResult alloc] init];
      jsResult.status = 0;
      if (error == nil) {
        jsResult.data = @"";
        jsResult.data = [[[NSString stringWithFormat:@"%@", ((NSDictionary*)result)[@"guid"]] stringByAppendingString:@("~")] stringByAppendingString: [NSString stringWithFormat:@"%@", ((NSDictionary*)result)[@"exit_code"]]];
        //jsResult.data = ((NSDictionary*)result)[@"exit_code"];
      }else {
        jsResult.data = [[[NSString stringWithFormat:@"%@", ((NSDictionary*)result)[@"guid"]] stringByAppendingString:@("~")] stringByAppendingString: @("-1")];
        //jsResult.data = @(-1);
      }
      jsResult.callbackMethodName = command.callbackMethod;
      [self.delegate performSelectorOnMainThread:@selector(sendResult:) withObject:jsResult waitUntilDone:NO];
    }];
  }
}

- (void)evaluateTest:(PAJSInvokedCommand*)command {
  if(command.arguments.count  > 0) {
    PAServiceInfo *info = [[PAServiceInfo alloc]init];
    info.guid = command.arguments[0];
    info.serviceCategory = PASupportServiceCategorySupportAction;
    info.service = PASupportServiceSupportActionScan;
    [[PAInterProcessManager sharedManager] executeService:info withReply:^(BOOL success, NSError *error, id result) {
      PAJSResult* jsResult = [[PAJSResult alloc] init];
      jsResult.status = 0;
      if (error == nil) {
        jsResult.data =  [[[NSString stringWithFormat:@"%@", ((NSDictionary*)result)[@"guid"]] stringByAppendingString:@("~")] stringByAppendingString: [NSString stringWithFormat:@"%@", ((NSDictionary*)result)[@"exit_code"]]];
        //jsResult.data = ((NSDictionary*)result)[@"exit_code"];
      }else {
        jsResult.data = [[[NSString stringWithFormat:@"%@", ((NSDictionary*)result)[@"guid"]] stringByAppendingString:@("~")] stringByAppendingString: @("-1")];
        //jsResult.data = @(-1);
      }
      jsResult.callbackMethodName = command.callbackMethod;
      [self.delegate performSelectorOnMainThread:@selector(sendResult:) withObject:jsResult waitUntilDone:NO];
      
    }];
  }
}

- (void)evaluateMessageSA:(PAJSInvokedCommand*)command {
  if(command.arguments.count  > 0) {
    PAServiceInfo *info = [[PAServiceInfo alloc]init];
    info.guid = command.arguments[0];
    info.serviceCategory = PASupportServiceCategorySupportAction;
    info.service = PASupportServiceSupportActionMessageFix;
    [[PAInterProcessManager sharedManager] executeService:info withReply:^(BOOL success, NSError *error, id result) {
      PAJSResult* jsResult = [[PAJSResult alloc] init];
      jsResult.status = 0;
      if (error == nil) {
        jsResult.data = @"";
        jsResult.data = ((NSDictionary*)result)[@"exit_code"];
      }else {
        jsResult.data = @(-1);
      }
      jsResult.callbackMethodName = command.callbackMethod;
      [self.delegate performSelectorOnMainThread:@selector(sendResult:) withObject:jsResult waitUntilDone:NO];
      
    }];
  }
  
}

@end
