//
//  PAUIUtilityController.m
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <PACoreServices/PACoreServices.h>
#import "PAUIUtilityController.h"
#import "PAJSCommandDelegate.h"
#import "PAJSInvokedCommand.h"
#import "PAJSResult.h"

@interface PAUIUtilityController()  {
  
}
@property (weak)PAJSCommandDelegate* delegate;
@property (strong) NSOperationQueue *operationQueue;

@end
@implementation PAUIUtilityController

- (instancetype)initWithCommandDelegate:(PAJSCommandDelegate*)aDelegate;
{
  self = [super init];
  if (self) {
    self.delegate = aDelegate;
    self.operationQueue = [[NSOperationQueue alloc] init];
  }
  return self;
}

- (PAJSResult*)executeCommand:(PAJSInvokedCommand*)command {
  if (command.isSync) {
    return [self executeSyncCommand:command];
  }
  [self executeAsyncCommand:command];
  return nil;
}

- (PAJSResult*)executeSyncCommand:(PAJSInvokedCommand*)command {
  if ([command.methodName isEqualToString:@"loadNewPage"]) {
    return [self.delegate loadNewPage];
  }
  else if ([command.methodName isEqualToString:@"gotohomepage"]) {
    return [self.delegate gotohomepage];
  }
  else if ([command.methodName isEqualToString:@"ShowAgentWindow"]) {
    return [self.delegate showAgentWindow];
  }
  else if([command.methodName isEqualToString:@"HideAgentWindow"]) {
    return [self.delegate hideAgentWindow];
  }
  else if ([command.methodName isEqualToString:@"LaunchBrowser"]) {
    return [self launchBrowser:command];
  }
  else if ([command.methodName isEqualToString:@"SetIcon"]) {
    return [self setAppIcon:command];
  }
  else if ([command.methodName isEqualToString:@"isAppRunning"]) {
    return [self isAppRunning:command];
  }
  else if([command.methodName isEqualToString:@"RunCommand"]) {
    return [self runCommand:command];
  }
  
  else if ([command.methodName isEqualToString:@"CloseNotificationTrayApplication"]) {
    return [self terminateNotificationTrayApp];
  }
  return nil;
}

- (void)executeAsyncCommand:(PAJSInvokedCommand*)command {
  if([command.methodName isEqualToString:@"updateUI"]) {
    NSInvocationOperation *operation = [[NSInvocationOperation alloc] initWithTarget:self
                                                                            selector:@selector(updateUI:) object:command];
    [self.operationQueue addOperation:operation];
  }
  else if([command.methodName isEqualToString:@"CloseApplication"]) {
    NSInvocationOperation *operation = [[NSInvocationOperation alloc] initWithTarget:self
                                                                            selector:@selector(terminateApp) object:nil];
    [self.operationQueue addOperation:operation];
  }
  else if([command.methodName isEqualToString:@"MinimizeAgentWindow"]) {
    NSInvocationOperation *operation = [[NSInvocationOperation alloc] initWithTarget:self
                                                                            selector:@selector(minimizeAgentWindow) object:nil];
    [self.operationQueue addOperation:operation];
  }
  else if([command.methodName isEqualToString:@"RunCommand"]) {
    NSInvocationOperation *operation = [[NSInvocationOperation alloc] initWithTarget:self
                                                                            selector:@selector(runCommand:) object:command];
    [self.operationQueue addOperation:operation];
  }
  
}

- (void)updateUI:(PAJSInvokedCommand*)command {
  PAJSResult* result = [[PAJSResult alloc] init];
  result.status = 0;
  result.message = @"updated UI through callback method";
  result.callbackMethodName = command.callbackMethod;
  [self.delegate performSelectorOnMainThread:@selector(sendResult:) withObject:result waitUntilDone:NO];
}

-(PAJSResult *)launchBrowser: (PAJSInvokedCommand*)command {
  
  if(command.arguments.count >= 1) {
    PAJSResult* result = [[PAJSResult alloc] init];
    result.status = 0;
    NSString *urlString = (NSString*)command.arguments[0];
    NSURL *url = [NSURL URLWithString:urlString];
    result.data = [[NSWorkspace sharedWorkspace] openURL:url] ? @(true) : @(false);
  }
  return nil;
}
- (void)terminateApp {
  [NSApp terminate:self];
}

- (PAJSResult *)terminateNotificationTrayApp {
  BOOL commandResult = [PALaunchServiceManager exitCommand:kPALSCommandNotificationTray];
  PAJSResult* result = [[PAJSResult alloc] init];
  result.status = 0;
  result.data = commandResult ? @(true) : @(false);
  return result;
}

- (void)minimizeAgentWindow {
  [[NSApp mainWindow ] miniaturize:self];
}


- (PAJSResult*)setAppIcon:(PAJSInvokedCommand*)command {
  PAJSResult* result = [[PAJSResult alloc] init];
  result.status = 0;
  NSString *urlString = (NSString*)command.arguments[0];
  NSImage* appIcon = [[NSImage alloc] initWithContentsOfFile:urlString];
  [NSApp setApplicationIconImage:appIcon];
  result.data = @(true);
  return result;
}

- (PAJSResult*)isAppRunning:(PAJSInvokedCommand*)command {
  
  if (command.arguments.count > 0) {
    NSString *commandString = (NSString*)command.arguments[0];
    BOOL isCommandRunning = [PALaunchServiceManager isCommandRunning:commandString];
    PAJSResult* result = [[PAJSResult alloc] init];
    result.status = 0;
    result.data = isCommandRunning ? @(true) : @(false);
    return result;
  }
  return nil;
}

- (PAJSResult*)runCommand:(PAJSInvokedCommand*)command {
  if (command.arguments.count > 0) {
    NSString *commandString = (NSString*)command.arguments[0];
    BOOL commandResult = [PALaunchServiceManager runCommand:commandString];
    PAJSResult* result = [[PAJSResult alloc] init];
    result.status = 0;
    result.data = commandResult ? @(true) : @(false);
    if (command.isSync) {
      return result;
    }
  }
  return nil;
}
@end
