//
//  PAJSCommandDelegate.m
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.


#import <PACoreServices/PACoreServices.h>

#import "PAJSCommandDelegate.h"
#import "PAJSBridgeController.h"
#import "PAJSInvokedCommand.h"
#import "PAJSResult.h"
#import "PAJSConstants.h"
#import "PAFileSystemAccessController.h"
#import "PASystemUtilityController.h"
#import "PACommonUtilityController.h"
#import "PAUIUtilityController.h"
#import "PARegistryController.h"
#import "PASyncController.h"
#import "PARepairServiceController.h"
#import "PAEndpointInfoController.h"
#import "PADownloadOffersController.h"
#import "PAOptimizationController.h"
#import "PASmartIssueController.h"
#import "PASupportActionController.h"


@interface PAJSCommandDelegate() {
  PAFileSystemAccessController* _fileSystemAccessController;
  PASystemUtilityController*    _systemUtilityController;
  PACommonUtilityController*    _commonUtilityController;
  PAUIUtilityController*        _uiUtilityController;
  PARegistryController*         _registryCOntroller;
  PASyncController*             _syncController;
  PARepairServiceController*    _repairServiceController;
  PAEndpointInfoController*     _endpointInfoController;
  PADownloadOffersController*   _downloadOffersController;
  PAOptimizationController*     _optimizationController;
  PASmartIssueController*       _smartissueController;
  PASupportActionController*    _supportActionController;
}

@end


@implementation PAJSCommandDelegate

- (id)initWithController:(PAJSBridgeController*)aController {
  self = [super init];
  if (self != nil) {
    _controller = aController;
    //This is to fetch ipAddress when app launched...
    [self commonUtilityController];
  }
  return self;
}

- (PAJSResult*)executeCommand:(NSDictionary*)commandInfo {
  PAJSInvokedCommand *command = [PAJSInvokedCommand commandFromDictionary:commandInfo];
  if ([command.className isEqualToString:JSClassFileSystemAccess]) {
    PAFileSystemAccessController* controller = [self fileSystemAccessController];
    return [controller executeCommand:command];
  }
  else if ([command.className isEqualToString:JSClassSystemUtility]) {
    PASystemUtilityController* controller = [self systemUtilityController];
    return [controller executeCommand:command];
    
  }
  else if ([command.className isEqualToString:JSClassCommonUtility]) {
    PACommonUtilityController* controller = [self commonUtilityController];
    return [controller executeCommand:command];
    
  }
  else if ([command.className isEqualToString:JSClassUIUtility]) {
    PAUIUtilityController* controller = [self uiUtilityController];
    return [controller executeCommand:command];
    
  }
  else if ([command.className isEqualToString:JSClassRegistry]) {
    PARegistryController* controller = [self registryController];
    return [controller executeCommand:command];
  }
  else if ([command.className isEqualToString:JSClassSync]) {
    PASyncController* controller = [self syncController];
    return [controller executeCommand:command];
  }
  else if ([command.className isEqualToString:JSClassRepairService]) {
    PARepairServiceController* controller = [self repairServiceController];
    return [controller executeCommand:command];
  } else if([command.className isEqualToString:JSClassEndpointInfo]) {
    PAEndpointInfoController *controller = [self endpointInfoController];
    return [controller executeCommand:command];
  }
  else if([command.className isEqualToString:JSClassDownloadOffers]) {
    PADownloadOffersController *controller = [self downloadOffersController];
    return [controller executeCommand:command];
  }
  else if([command.className isEqualToString:JSClassOptimization]) {
    PAOptimizationController *controller = [self optimizationController];
    return [controller executeCommand:command];
  }
  else if([command.className isEqualToString:JSClassSmartIssue]) {
    PASmartIssueController *controller = [self smartIssueController];
    return [controller executeCommand:command];
  }
  else if([command.className isEqualToString:JSClassSupportAction]) {
    PASupportActionController *controller = [self supportActionController];
    return [controller executeCommand:command];
  }
  
  return nil;
}

- (PAJSResult*)loadNewPage {
  PAFileSystemManager *fileSystemManager = [[PAFileSystemManager alloc]init];
  PAJSResult* result = [[PAJSResult alloc] init];
  result.status = 0;
  NSString* path = [[fileSystemManager appDataFolderPath] stringByAppendingPathComponent:@"index.html"];
  NSURL *url = [NSURL fileURLWithPath:path];
  
  result.message = @"Location change Button clicked";
  NSURLRequest *request = [NSURLRequest requestWithURL:url];
  [self.controller.webrView.mainFrame loadRequest:request];
  return result;
}

- (PAJSResult*)gotohomepage {
  NSString *filePath = [[NSBundle mainBundle] pathForResource:@"sample" ofType:@"html"];
  NSURL *url = [NSURL fileURLWithPath:filePath];
  NSURLRequest *request = [NSURLRequest requestWithURL:url];
  [self.controller.webrView.mainFrame loadRequest:request];
  
  return nil;
}

- (PAJSResult*)showAgentWindow {
  [self.controller.webrView.window makeKeyAndOrderFront:self];
  [self.controller.webrView.window.windowController showWindow:self];
  return nil;
}

- (PAJSResult*)hideAgentWindow {
  [self.controller.webrView.window orderOut:self];
  return nil;
}

- (PAJSResult*)setTitle:(PAJSInvokedCommand*)command {
  if (command.arguments.count > 0) {
    NSString* title = command.arguments[0];
    [self.controller.webrView.window setTitle:title];
  }
  return nil;
}

- (PAFileSystemAccessController*)fileSystemAccessController {
  if (! _fileSystemAccessController) {
    _fileSystemAccessController = [[PAFileSystemAccessController alloc] initWithCommandDelegate:self];
  }
  return _fileSystemAccessController;
}

- (PASystemUtilityController*)systemUtilityController {
  if (! _systemUtilityController) {
    _systemUtilityController = [[PASystemUtilityController alloc] initWithCommandDelegate:self];
  }
  return _systemUtilityController;
}

- (PACommonUtilityController*)commonUtilityController {
  if (! _commonUtilityController) {
    _commonUtilityController = [[PACommonUtilityController alloc] initWithCommandDelegate:self];
  }
  return _commonUtilityController;
}

- (PAUIUtilityController*)uiUtilityController {
  if (! _uiUtilityController) {
    _uiUtilityController = [[PAUIUtilityController alloc] initWithCommandDelegate:self];
  }
  return _uiUtilityController;
}

- (PARegistryController*)registryController {
  if (! _registryCOntroller) {
    _registryCOntroller = [[PARegistryController alloc] initWithCommandDelegate:self];
  }
  return _registryCOntroller;
}

- (PASyncController*)syncController {
  if(!_syncController) {
    _syncController = [[PASyncController alloc] initWithCommandDelegate:self];
  }
  return _syncController;
}

- (PARepairServiceController*)repairServiceController {
  if(!_repairServiceController) {
    _repairServiceController = [[PARepairServiceController alloc] initWithCommandDelegate:self];
  }
  return _repairServiceController;
}

- (PAEndpointInfoController*)endpointInfoController {
  if(!_endpointInfoController) {
    _endpointInfoController = [[PAEndpointInfoController alloc] initWithCommandDelegate:self];
  }
  return _endpointInfoController;
}

- (PADownloadOffersController*)downloadOffersController {
  if(!_downloadOffersController) {
    _downloadOffersController = [[PADownloadOffersController alloc] initWithCommandDelegate:self];
  }
  return _downloadOffersController;
}

- (PAOptimizationController*)optimizationController {
  if(!_optimizationController) {
    _optimizationController = [[PAOptimizationController alloc] initWithCommandDelegate:self];
  }
  return _optimizationController;
}

- (PASmartIssueController*)smartIssueController {
  if(!_smartissueController) {
    _smartissueController = [[PASmartIssueController alloc] initWithCommandDelegate:self];
  }
  return _smartissueController;
}

- (PASupportActionController*)supportActionController {
  if(!_supportActionController) {
    _supportActionController = [[PASupportActionController alloc] initWithCommandDelegate:self];
  }
  return _supportActionController;
}

- (void)sendResult:(PAJSResult*)result {
  [self.controller sendResult:result];
}

- (NSRect )defaultWindowFrame {
  NSMutableDictionary* dict = [NSMutableDictionary dictionary];
  [dict setObject:@[@"",@"SETUP",@"width",@"1000"] forKey:JSArgumentsKey];
  [dict setObject:@"1" forKey:JSISSyncMethodKey];
  [dict setObject:@"GetIniValue" forKey:JSOperationNameKey];
  PAJSInvokedCommand*command = [PAJSInvokedCommand commandFromDictionary:dict];
  PAJSResult*res = [[self commonUtilityController] executeCommand:command];
  NSString *widthAsString = res.data;
  
  [dict setObject:@[@"",@"SETUP",@"height",@"680"] forKey:JSArgumentsKey];
  
  command = [PAJSInvokedCommand commandFromDictionary:dict];
  res = [[self commonUtilityController] executeCommand:command];
  NSString *heightAsString = res.data;
  
  return NSMakeRect(0, 0, widthAsString.integerValue, heightAsString.integerValue);
}
@end
