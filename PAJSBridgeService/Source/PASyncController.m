//
//  PASyncController.m
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <PACoreServices/PACoreServices.h>
#import <PAIpcService/PAIpcService.h>
#import "PASyncController.h"
#import "PAJSResult.h"
#import "PAJSInvokedCommand.h"
#import "PAJSCommandDelegate.h"
#import "PAJSConstants.h"

@interface  PASyncController(){
}
@property (weak)PAJSCommandDelegate* delegate;
@property (strong) NSOperationQueue *operationQueue;

@end
@implementation PASyncController

- (instancetype)initWithCommandDelegate:(PAJSCommandDelegate*)aDelegate;
{
  self = [super init];
  if (self) {
    self.delegate = aDelegate;
    self.operationQueue = [[NSOperationQueue alloc] init];
  }
  return self;
}

- (PAJSResult*)executeCommand:(PAJSInvokedCommand*)command {
  if (command.isSync) {
    return [self executeSyncCommand:command];
  }
  [self executeAsyncCommand:command];
  return nil;
}

- (PAJSResult*)executeSyncCommand:(PAJSInvokedCommand*)command {
  return nil;
}

- (void)executeAsyncCommand:(PAJSInvokedCommand*)command {
  if([command.methodName isEqualToString:@"ForceSync"]) {
    NSInvocationOperation *operation = [[NSInvocationOperation alloc]
                                        initWithTarget:self
                                        selector:@selector(doForceSync:)
                                        object:command];
    [self.operationQueue addOperation:operation];
  }
}

- (void)doForceSync:(PAJSInvokedCommand*)command {
  
  [[PAInterProcessManager sharedManager]updateWithCallback:^(BOOL success) {
    //NSLog(@"sync completed");
  }];
}

- (void)syncCompleted {
  NSDictionary* commandForProviderInfo = @{JSClassNameKey: @"CommonUtility",JSOperationNameKey:@"GetIniValue", JSArgumentsKey:@[@"",@"PROVIDERINFO",@"provider",@""], JSISSyncMethodKey : @"1" };
  PAJSResult*result = [self.delegate executeCommand:commandForProviderInfo];
  NSString* runCommand = [NSString stringWithFormat:@"notificationTray /path /messages /ini /minibcont.ini /syncevent",result.data];
  [PALaunchServiceManager runCommand:runCommand];
}
@end
