//
//  PAProtectedVersionListController.h
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
@class PAJSCommandDelegate;
@class PAJSInvokedCommand;
@interface PAProtectedVersionListController : NSObject

- (instancetype)initWithCommandDelegate:(PAJSCommandDelegate*)aDelegate command:(PAJSInvokedCommand*)command;

- (void)executeCommand;
@end
