//
//  PASystemUtilityController.m
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PASystemUtilityController.h"
#import "PAJSResult.h"
#import "PAJSInvokedCommand.h"
#import "PAJSCommandDelegate.h"
#import "PAJSConstants.h"
#import <PACoreServices/PACoreServices.h>
#import "NSString+Additions.h"
#import "NSProcessInfo+Additions.h"

@interface PASystemUtilityController()  {
  PAAppConfiguration *_appconfigurations;
  
}
@property (weak)PAJSCommandDelegate* delegate;
@property (strong) NSOperationQueue *operationQueue;

@end
@implementation PASystemUtilityController

- (instancetype)initWithCommandDelegate:(PAJSCommandDelegate*)aDelegate;
{
  self = [super init];
  if (self) {
    self.delegate = aDelegate;
    self.operationQueue = [[NSOperationQueue alloc] init];
  }
  return self;
}

- (PAJSResult*)executeCommand:(PAJSInvokedCommand*)command {
  if (command.isSync) {
    return [self executeSyncCommand:command];
  }
  [self executeAsyncCommand:command];
  return nil;
}

- (PAJSResult*)executeSyncCommand:(PAJSInvokedCommand*)command {
  if ([command.methodName isEqualToString:@"SleepWithMsgDispatch"]) {
    unsigned int time = (unsigned int)[command.arguments[0] unsignedIntegerValue];
    [self sleep:time];
  }
  else if([command.methodName isEqualToString:@"GetOS"]) {
    NSString *osName = [NSProcessInfo osCodeName];
    PAJSResult* result = [[PAJSResult alloc] init];
    result.status = 0;
    result.data = osName;
    return result;
  } else if ([command.methodName isEqualToString:@"GenGuid"]) {
    return [self generateGuid: command];
  }
  else if ([command.methodName isEqualToString:@"GetInstallInfo"]) {
    return [self installInfo: command];
  }
  return nil;
}

-(PAJSResult*)generateGuid:(PAJSInvokedCommand*)command {
  if (command.arguments.count) {
    
    PAJSResult* result = [[PAJSResult alloc] init];
    result.status = 0;
    NSString* requiresBraces = (NSString*)command.arguments[0];
    NSString *uuid = [NSString uuid];
    
    if ([requiresBraces boolValue]) {
      uuid = [NSString stringWithFormat:@"{%@}", uuid];
    }
    result.data = uuid;
    return result;
  }
  return nil;
}

- (void)executeAsyncCommand:(PAJSInvokedCommand*)command  {
  if ([command.methodName isEqualToString:@"Shutdown"]) {
    NSInvocationOperation *operation = [[NSInvocationOperation alloc] initWithTarget:self
                                                                            selector:@selector(shutDown:) object:command];
    [self.operationQueue addOperation:operation];
  }
  else if ([command.methodName isEqualToString:@"IsMuted"]) {
    NSInvocationOperation *operation = [[NSInvocationOperation alloc] initWithTarget:self
                                                                            selector:@selector(verifyDeviceMuteStatus:) object:command];
    [self.operationQueue addOperation:operation];
  }
  else if ([command.methodName isEqualToString:@"SetVolume"]) {
    NSInvocationOperation *operation = [[NSInvocationOperation alloc] initWithTarget:self
                                                                            selector:@selector(setDeviceVolume:) object:command];
    [self.operationQueue addOperation:operation];
  }
  
}

-(PAJSResult*) installInfo:(PAJSInvokedCommand*)command {
  PAAppConfiguration* config = [self appConfigurations];
  PAJSResult* result = [[PAJSResult alloc] init];
  result.status = 0;
  result.data = [config.installInfo dictRepresentation];
  return result;
}
- (void)sleep:(unsigned int) milliSeconds{
  //    milliSeconds/= 1000;
  usleep(milliSeconds);
}

- (void)shutDown:(PAJSInvokedCommand*)command {
  PAAppConfiguration* configuration = [self appConfigurations];
  NSBundle* bundle = [NSBundle bundleWithPath:configuration.mainAppPath];
  NSString* scriptPath = [bundle pathForResource:@"CheckVolumeIsMuted" ofType:@"scpt"];
  [PAScriptRunner executeScriptWithPath:scriptPath callBack:^(BOOL success, NSError *error, id scriptResult) {
    if (success) {
      PAJSResult* result = [[PAJSResult alloc] init];
      result.status = 0;
      result.callbackMethodName = command.callbackMethod;
      [self.delegate performSelectorOnMainThread:@selector(sendResult:) withObject:result waitUntilDone:NO];
      
    }
  }];
}

- (void)verifyDeviceMuteStatus:(PAJSInvokedCommand*)command {
  PAAppConfiguration* configuration = [self appConfigurations];
  NSBundle* bundle = [NSBundle bundleWithPath:configuration.mainAppPath];
  NSString* scriptPath = [bundle pathForResource:@"CheckVolumeIsMuted" ofType:@"scpt"];
  [PAScriptRunner executeScriptWithPath:scriptPath callBack:^(BOOL success, NSError *error, id scriptResult) {
    if (success) {
      PAJSResult* result = [[PAJSResult alloc] init];
      result.status = 0;
      result.message = ((NSAppleEventDescriptor*)scriptResult).booleanValue ? @"Device is Muted" : @"Device is not muted";
      result.callbackMethodName = command.callbackMethod;
      result.data = @(((NSAppleEventDescriptor*)scriptResult).booleanValue);
      [self.delegate performSelectorOnMainThread:@selector(sendResult:) withObject:result waitUntilDone:NO];
      
    }
  }];
}

- (void)setDeviceVolume:(PAJSInvokedCommand*)command {
  if (command.arguments.count) {
    NSInteger volume = ((NSString*)command.arguments[0]).integerValue;
    NSString* scriptString = [NSString stringWithFormat:@"%@ %ld",JSSetVolumeScript,volume];
    [PAScriptRunner executeScript:scriptString ofType:PAScriptTypeApple callBack:^(BOOL success, NSError *error, id scriptResult) {
      if (success) {
        PAJSResult* result = [[PAJSResult alloc] init];
        result.status = 0;
        result.message = @"";
        result.data = command.arguments[0];
        result.callbackMethodName = command.callbackMethod;
        [self.delegate performSelectorOnMainThread:@selector(sendResult:) withObject:result waitUntilDone:NO];
      }
    }];
  }
  
}

- (PAAppConfiguration*)appConfigurations {
  if(!_appconfigurations) {
    _appconfigurations = [PAConfigurationManager appConfigurations];
  }
  return _appconfigurations;
}
@end
