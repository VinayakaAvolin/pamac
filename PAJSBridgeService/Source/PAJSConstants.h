//
//  PAJSConstants.h
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.


#import <Foundation/Foundation.h>

extern NSString * const JSClassNameKey;

extern NSString * const JSOperationNameKey;

extern NSString * const JSArgumentsKey;

extern NSString * const JSCallbackMethodNameKey;

extern NSString * const JSISSyncMethodKey;

extern NSString * const JSClassFileSystemAccess;

extern NSString * const JSClassSystemUtility;

extern NSString * const JSClassCommonUtility;

extern NSString * const JSClassUIUtility;

extern NSString * const JSClassRegistry;

extern NSString * const JSClassSmartIssue;

extern NSString * const JSClassSync;

extern NSString * const JSClassRepairService;

extern NSString * const JSClassEndpointInfo;

extern NSString * const JSClassDownloadOffers;

extern NSString * const JSClassOptimization;

extern NSString * const JSClassSupportAction;

extern NSString * const JSStatus;

extern NSString * const JSMessage;

extern NSString * const JSData;

extern NSString * const JSSetVolumeScript;

extern NSString * const JSGuid;

extern NSString * const JSVersion;

extern NSString * const JSDateFormat1;

extern NSString * const JSDateFormat2;

extern NSString * const JSDateString;

extern NSString * const JSProtectedDate;

extern NSString * const JSProgress;

extern NSString * const JSDownLoadStatus;



