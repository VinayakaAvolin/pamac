//
//  PAFileSystemAccessController.m
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <PACoreServices/PACoreServices.h>

#import "PAFileSystemAccessController.h"
#import "PAJSResult.h"
#import "PAJSInvokedCommand.h"
#import "PAJSCommandDelegate.h"
#import "NSString+Additions.h"
#import "NSDate+Additions.h"

@interface  PAFileSystemAccessController(){
  PAFileSystemManager *_fileSystemManager;
}
@property (weak)PAJSCommandDelegate* delegate;
@property (strong) NSOperationQueue *operationQueue;

@end
@implementation PAFileSystemAccessController

- (instancetype)initWithCommandDelegate:(PAJSCommandDelegate*)aDelegate;
{
  self = [super init];
  if (self) {
    _fileSystemManager = [[PAFileSystemManager alloc] init];
    self.delegate = aDelegate;
    self.operationQueue = [[NSOperationQueue alloc] init];
  }
  return self;
}

- (PAJSResult*)executeCommand:(PAJSInvokedCommand*)command {
  if (command.isSync) {
    return [self executeSyncCommand:command];
  }
  [self executeAsyncCommand:command];
  return nil;
  
  
}

- (PAJSResult*)executeSyncCommand:(PAJSInvokedCommand*)command {
  if ([command.methodName isEqualToString:@"getDataDir"]) {
    return [self dataDirectory];
  }
  else if([command.methodName isEqualToString:@"GetFolderSize"]) {
    return [self folderSize:command];
  }
  else if([command.methodName isEqualToString:@"FolderExists"]) {
    return [self checkFolderExists:command];
  }
  else if([command.methodName isEqualToString:@"CreateDir"]) {
    return [self createDirAtPath:command];
  }
  else if([command.methodName isEqualToString:@"ExpandSysMacro"]) {
    return [self expandSysMacro:command];
  }
  else if([command.methodName isEqualToString:@"FileExists"]) {
    return [self checkFileExists:command];
  }
  else if([command.methodName isEqualToString:@"GetSubFoldersList"]) {
    return [self subFoldersForFolder:(PAJSInvokedCommand*)command];
  }
  else if([command.methodName isEqualToString:@"GetNativePath"]) {
    return [self nativePath:(PAJSInvokedCommand*)command];
  }
  else if([command.methodName isEqualToString:@"GetFileVersion"]) {
    return [self getFileVersion:(PAJSInvokedCommand*)command];
  }
  else if([command.methodName isEqualToString:@"CreateTextFile"]) {
    return [self createTextFile:(PAJSInvokedCommand*)command];
  }
  else if([command.methodName isEqualToString:@"ReadFile"]) {
    return [self readFile:(PAJSInvokedCommand*)command];
  }
  else if([command.methodName isEqualToString:@"WriteFile"]) {
    return [self writeFile:(PAJSInvokedCommand*)command];
  }
  else if([command.methodName isEqualToString:@"CreateUTF8TextFile"]) {
    return [self createTextFile:(PAJSInvokedCommand*)command];
  }
  
  else if([command.methodName isEqualToString:@"DeleteFile"]) {
    return [self deleteFile:(PAJSInvokedCommand*)command];
  }
  else if([command.methodName isEqualToString:@"CopyFile"]) {
    return [self copyFile:(PAJSInvokedCommand*)command];
  }
  else if([command.methodName isEqualToString:@"MoveFile"]) {
    return [self moveFile:(PAJSInvokedCommand*)command];
  }
  else if([command.methodName isEqualToString:@"CompressFile"]) {
    return [self createTextFile:(PAJSInvokedCommand*)command];
  }
  else if([command.methodName isEqualToString:@"Decompress"]) {
    return [self createTextFile:(PAJSInvokedCommand*)command];
  }
  else if([command.methodName isEqualToString:@"DeleteDir"]) {
    return [self deleteDir:(PAJSInvokedCommand*)command];
  }
  else if([command.methodName isEqualToString:@"CopyDir"]) {
    return [self copyDir:(PAJSInvokedCommand*)command];
  }
  else if([command.methodName isEqualToString:@"CreateInternalObject"]) {
    return [self createTextFile:(PAJSInvokedCommand*)command];
  }
  else if ([command.methodName isEqualToString:@"BuildPath"]) {
    return [self buildPath: (PAJSInvokedCommand*)command];
  }
  else if ([command.methodName isEqualToString:@"GetModifiedDate"]) {
    return [self getModifiedDate: (PAJSInvokedCommand*)command];
  }
  else if ([command.methodName isEqualToString:@"GetCreatedDate"]) {
    return [self getCreatedDate: (PAJSInvokedCommand*)command];
  }
  else if ([command.methodName isEqualToString:@"CreateXMLFromString"]) {
    return [self writeXMLFile: (PAJSInvokedCommand*)command];
  }
  else if([command.methodName isEqualToString:@"GetFilesList"]) {
    return [self filesOfDirectory:command];
  }
  
  return nil;
}

- (void)executeAsyncCommand:(PAJSInvokedCommand*)command  {
  if([command.methodName isEqualToString:@"ReadFile"]) {
    NSInvocationOperation *operation = [[NSInvocationOperation alloc]
                                        initWithTarget:self
                                        selector:@selector(readFile:)
                                        object:command];
    [self.operationQueue addOperation:operation];
  }
  else if([command.methodName isEqualToString:@"WriteFile"]) {
    NSInvocationOperation *operation = [[NSInvocationOperation alloc]
                                        initWithTarget:self
                                        selector:@selector(writeFile:)
                                        object:command];
    [self.operationQueue addOperation:operation];
  }
  else if([command.methodName isEqualToString:@"ShowBrowseFolderDialog"]) {
    NSInvocationOperation *operation = [[NSInvocationOperation alloc]
                                        initWithTarget:self
                                        selector:@selector(showBrowseFolderDialog:)
                                        object:command];
    [self.operationQueue addOperation:operation];
  }
}

- (PAJSResult*)dataDirectory {
  PAJSResult* result = [[PAJSResult alloc] init];
  result.status = 0;
  result.message = [_fileSystemManager appDataFolderPath];
  return result;
}

- (PAJSResult*)folderSize:(PAJSInvokedCommand*)command {
  if (command.arguments.count) {
    PAJSResult* result = [[PAJSResult alloc] init];
    result.status = 0;
    PAItemAttributes* attributes = [_fileSystemManager attributesOfItemAtPath:command.arguments[0]];
    result.data = @(attributes.size);
    return result;
  }
  return nil;
}

- (PAJSResult*)expandSysMacro:(PAJSInvokedCommand*)command {
  if (command.arguments.count) {
    PAJSResult* result = [[PAJSResult alloc] init];
    result.status = 0;
    NSString* pathToExpand = (NSString*)command.arguments[0];
    result.data = [_fileSystemManager stringByExpandingPath:pathToExpand];
    return result;
  }
  return nil;
}

- (PAJSResult*)checkFolderExists:(PAJSInvokedCommand*)command {
  if (command.arguments.count) {
    PAJSResult* result = [[PAJSResult alloc] init];
    result.status = 0;
    NSString* pathToVerify = (NSString*)command.arguments[0];
    pathToVerify = [pathToVerify stringByConvertingIntoMacCompatibleFilePath];
    result.data = [_fileSystemManager doesDirectoryExist:pathToVerify] ? @(true) : @(false);
    return result;
  }
  return nil;
}

- (PAJSResult*)checkFileExists:(PAJSInvokedCommand*)command {
  if (command.arguments.count) {
    PAJSResult* result = [[PAJSResult alloc] init];
    result.status = 0;
    NSString* pathToVerify = (NSString*)command.arguments[0];
    result.data = [_fileSystemManager doesFileExist:pathToVerify] ? @(true) : @(false);
    return result;
  }
  return nil;
}

- (PAJSResult*)createDirAtPath:(PAJSInvokedCommand*)command {
  if (command.arguments.count) {
    PAJSResult* result = [[PAJSResult alloc] init];
    result.status = 0;
    NSString* path = (NSString*)command.arguments[0];
    path = [path stringByConvertingIntoMacCompatibleFilePath];
    result.data = [_fileSystemManager createDirectoryAtPath:path
                                withIntermediateDirectories:YES
                                                 attributes:nil
                                                      error:nil] ? @(1) : @(0);
    return result;
  }
  return nil;
}

- (PAJSResult*)subFoldersForFolder:(PAJSInvokedCommand*)command {
  
  if (command.arguments.count) {
    PAJSResult* result = [[PAJSResult alloc] init];
    result.status = 0;
    NSString* folderName = (NSString*)command.arguments[0];
    result.data = [_fileSystemManager subDirectoriesOfDirectory:[folderName stringByConvertingIntoMacCompatibleFilePath]];
    return result;
  }
  return nil;
}

- (PAJSResult*)filesOfDirectory:(PAJSInvokedCommand*)command {
  if (command.arguments.count) {
    PAJSResult* result = [[PAJSResult alloc] init];
    result.status = 0;
    NSString* folderName = (NSString*)command.arguments[0];
    result.data = [_fileSystemManager filesOfDirectory:[folderName stringByConvertingIntoMacCompatibleFilePath]];
    return result;
  }
  return nil;
}

- (PAJSResult*)nativePath:(PAJSInvokedCommand*)command {
  
  if (command.arguments.count) {
    PAJSResult* result = [[PAJSResult alloc] init];
    result.status = 0;
    NSString* pathToConvert = (NSString*)command.arguments[0];
    result.data = [pathToConvert stringByConvertingIntoMacCompatibleFilePath];
    return result;
  }
  return nil;
}

- (PAJSResult*)getFileVersion:(PAJSInvokedCommand*)command {
  if (command.arguments.count) {
    PAJSResult* result = [[PAJSResult alloc] init];
    result.status = 0;
    NSString* pathToConvert = (NSString*)command.arguments[0];
    result.data = [[[NSBundle bundleWithPath:pathToConvert] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    return result;
  }
  return nil;
}

- (PAJSResult*)getModifiedDate:(PAJSInvokedCommand*)command {
  
  if (command.arguments.count) {
    PAJSResult* result = [[PAJSResult alloc] init];
    result.status = 0;
    
    NSString* path = (NSString*)command.arguments[0];
    NSString *nativePath = [path stringByConvertingIntoMacCompatibleFilePath];
    NSDate * fileLastModifiedDate = nil;
    
    NSError * error = nil;
    NSDictionary * attrs = [[NSFileManager defaultManager] attributesOfItemAtPath:nativePath error:&error];
    if (attrs && !error)
    {
      fileLastModifiedDate = [attrs fileModificationDate];
      NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
      NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
      [formatter setTimeZone:timeZone];
      [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
      NSString *stringFromDate = [formatter stringFromDate:fileLastModifiedDate];
      result.data = stringFromDate;
      return result;
    }
  }
  return nil;
}

- (PAJSResult*)getCreatedDate:(PAJSInvokedCommand*)command {
  
  if (command.arguments.count) {
    PAJSResult* result = [[PAJSResult alloc] init];
    result.status = 0;
    
    NSString* path = (NSString*)command.arguments[0];
    NSString *nativePath = [path stringByConvertingIntoMacCompatibleFilePath];
    NSDate * fileLastModifiedDate = nil;
    
    NSError * error = nil;
    NSDictionary * attrs = [[NSFileManager defaultManager] attributesOfItemAtPath:nativePath error:&error];
    if (attrs && !error)
    {
      fileLastModifiedDate = [attrs fileCreationDate];
      NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
      NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
      [formatter setTimeZone:timeZone];
      [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
      NSString *stringFromDate = [formatter stringFromDate:fileLastModifiedDate];
      result.data = stringFromDate;
      return result;
    }
  }
  return nil;
}


- (PAJSResult *)buildPath: (PAJSInvokedCommand *)command {
  if (command.arguments.count) {
    PAJSResult* result = [[PAJSResult alloc] init];
    result.status = 0;
    NSString* pathToConvert = (NSString*)command.arguments[0];
    pathToConvert = [pathToConvert stringByConvertingIntoMacCompatibleFilePath];
    
    if ([pathToConvert hasPrefix:@"/"] && [pathToConvert length] > 1) {
      pathToConvert = [pathToConvert substringFromIndex:1];
    }
    NSArray *pathComponents = [pathToConvert componentsSeparatedByString:@"/" ];
    
    
    int occurrences = 0;
    for(NSString *string in pathComponents){
      occurrences += ([string isEqualToString:@".."]?1:0);
    }
    if(occurrences) {
      NSInteger anIndex = [pathComponents indexOfObject:@".."];
      
      NSMutableArray *buildPathComponents = [NSMutableArray new];
      NSInteger tillIndex = anIndex - occurrences;
      NSInteger afterIndex = anIndex + occurrences;
      for (int i = 0; i < tillIndex; i++) {
        [buildPathComponents addObject:pathComponents[i]];
      }
      for (int i = (int)afterIndex; i < pathComponents.count; i++) {
        [buildPathComponents addObject:pathComponents[i]];
      }
      result.data = [buildPathComponents componentsJoinedByString:@"/"];
    } else {
      result.data = [pathComponents componentsJoinedByString:@"/"];
    }
    
    NSString* path = result.data;
    if([path containsString:@"file://"]) {
      path = [path stringByReplacingOccurrencesOfString:@"file://" withString:@"/"];
      path = [path stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
      result.data = path;
    }else if([path containsString:@"file:/"]) {
      path = [path stringByReplacingOccurrencesOfString:@"file:/" withString:@"/"];
      path = [path stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
      result.data = path;
    }else {
      path = [NSString stringWithFormat:@"/%@", path];
      path = [path stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
      result.data = path;
    }
    return result;
  }
  return nil;
}

- (PAJSResult*)createTextFile:(PAJSInvokedCommand*)command {
  
  if (command.arguments.count) {
    PAJSResult* result = [[PAJSResult alloc] init];
    result.status = 0;
    NSString *filePath = (NSString* )command.arguments[0];
    filePath = [filePath stringByConvertingIntoMacCompatibleFilePath];
    BOOL bOverwrite = ((NSString *)command.arguments[1]).boolValue;
    result.data = [_fileSystemManager createFileAtPath:filePath
                                            attributes:nil
                                       shouldOverwrite:bOverwrite
                                                 error:nil];
    return result;
    
  }
  return nil;
}

- (PAJSResult*)readFile:(PAJSInvokedCommand*)command {
  
  if (command.arguments.count) {
    PAJSResult* result = [[PAJSResult alloc] init];
    result.status = 0;
    NSString *filePath = (NSString* )command.arguments[0];
    filePath = [filePath stringByConvertingIntoMacCompatibleFilePath];
    
    NSData *fileData = [_fileSystemManager contentOfFileAtPath:filePath];
    NSString *fileContentStr = [[NSString alloc] initWithData:fileData encoding:NSUTF8StringEncoding];
    
    result.data = fileContentStr;
    result.callbackMethodName = command.callbackMethod;
    if(command.isSync) {
      return result;
    }
    [self.delegate performSelectorOnMainThread:@selector(sendResult:) withObject:result waitUntilDone:NO];
  }
  return nil;
}

-(PAJSResult*) writeFile:(PAJSInvokedCommand*)command {
  if (command.arguments.count) {
    PAJSResult* result = [[PAJSResult alloc] init];
    result.status = 0;
    NSString *filePath = (NSString* )command.arguments[0];
    NSString *dataStr =  (NSString* )command.arguments[1];
    NSData *data = [dataStr dataUsingEncoding:NSUTF8StringEncoding];
    PAFileModeType modeType = PAFileModeWrite;
    filePath = [filePath stringByConvertingIntoMacCompatibleFilePath];
    if ([filePath.pathExtension isEqualToString:@"xml"]) {
      PAXmlServiceManager *xmlManager = [[PAXmlServiceManager alloc] init];
      
      PAJSResult* result = [[PAJSResult alloc] init];
      result.status = 0;
      result.data = [xmlManager writeXmlData:data toPath:filePath error:nil] ? @(1) : @(0);
      return result;
    }
    if (command.arguments.count > 2) {
      NSString *mode = (NSString*)command.arguments[2];
      NSInteger modeVal = mode.integerValue;
      if (modeVal == 2) {
        modeType = PAFileModeWrite; // FILE_MODE.WRITE
      } else if (modeVal == 8) {
        modeType = PAFileModeAppend; //FILE_MODE.APPEND
      }
    }
    
    result.data = [_fileSystemManager writeToFileAtPath:filePath data:data withMode:modeType]? @(1) : @(0);;
    if(command.isSync) {
      return result;
    }
    result.callbackMethodName = command.callbackMethod;
    [self.delegate performSelectorOnMainThread:@selector(sendResult:) withObject:result waitUntilDone:NO];
  }
  return nil;
}

-(PAJSResult *) writeXMLFile:(PAJSInvokedCommand*)command {
  
  if (command.arguments.count) {
    NSString *name = [NSString stringWithFormat:@"%@.xml", [[NSUUID UUID] UUIDString]];
    
    NSString *filePath = [[_fileSystemManager dataBackupFolderPath] stringByAppendingPathComponent: name];
    NSData *xmlData = [(NSString* )command.arguments[0] dataUsingEncoding:NSUTF8StringEncoding];
    PAXmlServiceManager *xmlManager = [[PAXmlServiceManager alloc] init];
    [xmlManager writeXmlData:xmlData toPath:filePath error:nil];
    PAJSResult* result = [[PAJSResult alloc] init];
    result.status = 0;
    result.data = filePath;
    return result;
  }
  
  return nil;
}

- (PAJSResult*)deleteFile:(PAJSInvokedCommand*)command {
  
  if (command.arguments.count) {
    PAJSResult* result = [[PAJSResult alloc] init];
    result.status = 0;
    NSString *filePath = (NSString* )command.arguments[0];
    filePath = [filePath stringByConvertingIntoMacCompatibleFilePath];
    result.data = [_fileSystemManager deleteFile:filePath] ? @(1):@(0);
    return result;
  }
  return nil;
}

- (PAJSResult*)copyFile:(PAJSInvokedCommand*)command {
  
  if (command.arguments.count) {
    PAJSResult* result = [[PAJSResult alloc] init];
    result.status = 0;
    NSString *srcFilePath = (NSString* )command.arguments[0];
    NSString *dstFilePath = (NSString* )command.arguments[1];
    srcFilePath = [srcFilePath stringByConvertingIntoMacCompatibleFilePath];
    dstFilePath = [dstFilePath stringByConvertingIntoMacCompatibleFilePath];
    result.data = [_fileSystemManager copyFile:srcFilePath destinationPath:dstFilePath] ? @(1):@(0);
    return result;
  }
  return nil;
}

- (PAJSResult*)moveFile:(PAJSInvokedCommand*)command {
  
  if (command.arguments.count) {
    PAJSResult* result = [[PAJSResult alloc] init];
    result.status = 0;
    NSString *srcFilePath = (NSString* )command.arguments[0];
    NSString *dstFilePath = (NSString* )command.arguments[1];
    srcFilePath = [srcFilePath stringByConvertingIntoMacCompatibleFilePath];
    dstFilePath = [dstFilePath stringByConvertingIntoMacCompatibleFilePath];
    result.data = [_fileSystemManager moveFile:srcFilePath destinationPath:dstFilePath] ? @(1):@(0);
    return result;
  }
  return nil;
}

- (PAJSResult*)deleteDir:(PAJSInvokedCommand*)command {
  
  if (command.arguments.count) {
    PAJSResult* result = [[PAJSResult alloc] init];
    result.status = 0;
    NSString *dirPath = (NSString* )command.arguments[0];
    dirPath = [dirPath stringByConvertingIntoMacCompatibleFilePath];
    
    result.data = [_fileSystemManager deleteDirectory: dirPath] ? @(1): @(0);
    return result;
  }
  return nil;
}

- (PAJSResult*)copyDir:(PAJSInvokedCommand*)command {
  
  if (command.arguments.count) {
    PAJSResult* result = [[PAJSResult alloc] init];
    result.status = 0;
    NSString *srcPath = (NSString* )command.arguments[0];
    NSString *dstPath = (NSString* )command.arguments[1];
    srcPath = [srcPath stringByConvertingIntoMacCompatibleFilePath];
    dstPath = [dstPath stringByConvertingIntoMacCompatibleFilePath];
    
    result.data = [_fileSystemManager copyDirectory:srcPath destinationPath:dstPath] ? @(1): @(0);
    return result;
  }
  return nil;
}

- (void)showBrowseFolderDialog: (PAJSInvokedCommand*)command {
  [self performSelectorOnMainThread:@selector(showDialog:) withObject:command waitUntilDone:NO];
}


- (void)showDialog:(PAJSInvokedCommand*)command {
  NSOpenPanel* panel = [NSOpenPanel openPanel];
  panel.allowsMultipleSelection = NO;
  [panel beginWithCompletionHandler:^(NSInteger result){
    if (result == NSFileHandlingPanelOKButton) {
      NSURL*  theDoc = [[panel URLs] objectAtIndex:0];
      PAJSResult* jsResult = [[PAJSResult alloc] init];
      jsResult.status = 0;
      jsResult.data = theDoc.path;
      jsResult.callbackMethodName = command.callbackMethod;
      [self.delegate sendResult:jsResult];
    }
  }];
  
}

@end
