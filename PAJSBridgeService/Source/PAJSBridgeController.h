//
//  PAJSBridgeController.h
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <WebKit/WebKit.h>
#import <JavaScriptCore/JavaScriptCore.h>
#import "PAJSCommandDelegate.h"

typedef enum {
  PAMainApp = 1,
  PANotificationApp = 2
} PACurrentApp;

@interface PAJSBridgeController : NSObject

@property (nonatomic, strong)  PAJSCommandDelegate* commandDelegate;
@property (weak)  WebView *webrView;
@property (nonatomic) PACurrentApp currentApp;
@property (strong)NSArray* arguments;

- (void)sendResult:(PAJSResult*)result;

- (NSRect)defaultWindowFrame;

- (void)setCommandLineArguments:(NSArray*)arguments;

@end
