//
//  PAEndpointInfoController.m
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PAEndpointInfoController.h"

#import "PAJSResult.h"
#import "PAJSInvokedCommand.h"
#import "PAJSCommandDelegate.h"
#import <PACoreServices/PACoreServices.h>


@interface  PAEndpointInfoController()

@property (weak)PAJSCommandDelegate* delegate;
@property (strong) NSOperationQueue *operationQueue;

@end

@implementation PAEndpointInfoController

- (instancetype)initWithCommandDelegate:(PAJSCommandDelegate*)aDelegate
{
  self = [super init];
  if (self) {
    self.delegate = aDelegate;
    self.operationQueue = [[NSOperationQueue alloc] init];
  }
  return self;
}

- (PAJSResult*)executeCommand:(PAJSInvokedCommand*)command {
  if (command.isSync) {
    return [self executeSyncCommand:command];
  }
  [self executeAsyncCommand:command];
  return nil;
}

- (PAJSResult*)executeSyncCommand:(PAJSInvokedCommand*)command {
  return nil;
}
- (void)executeAsyncCommand:(PAJSInvokedCommand*)command  {
  if([command.methodName isEqualToString:@"CollectEndpointInfo"]) {
    NSInvocationOperation *operation = [[NSInvocationOperation alloc]
                                        initWithTarget:self
                                        selector:@selector(collectEndpointInfo:)
                                        object:command];
    [self.operationQueue addOperation:operation];
  }
}

-(void) collectEndpointInfo:(PAJSInvokedCommand*)command {
  NSString *inputPath = (NSString *)command.arguments[0];
  NSString *outputPath = (NSString *)command.arguments[1];
  
  PAEnPointInfoManager *manager = [[PAEnPointInfoManager alloc]initWithInputXml:inputPath outputXml:outputPath source:PATargetScriptSourceTypeOthers];
  [manager createAllEndpointInfoWithCompletion:^(BOOL success) {
    //   PALogger *logger = [PALogger loggerWithConfiguration:nil];
    //   [logger debugWithInfo:nil andText:@"End point info collected"];
    PAJSResult* jsResult = [[PAJSResult alloc] init];
    jsResult.status = 0;
    jsResult.callbackMethodName = command.callbackMethod;
    [self.delegate performSelectorOnMainThread:@selector(sendResult:) withObject:jsResult waitUntilDone:NO];
  }];
}
@end

