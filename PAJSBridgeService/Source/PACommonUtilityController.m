//
//  PACommonUtilityController.m
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <PACoreServices/PACoreServices.h>
#import "NSString+Additions.h"
#import "PACommonUtilityController.h"
#import "PAJSCommandDelegate.h"
#import "PAJSInvokedCommand.h"
#import "PAJSBridgeController.h"
#import "PAJSResult.h"
#import "PAPlistLockManager.h"

#define MAIN_INI_PLIST_NAME @"bcont_nm.plist"
#define NOTIF_TRAY_INI_PLIST_NAME @"minibcont.plist"
#define INSTALL_CONFIG_FILE_FOR_MAIN_APP @"clientui_config.xml"
#define INSTALL_CONFIG_FILE_FOR_NOTIF_APP @"clientuimini_config.xml"
#define USAGE_LOG @"usagelog.xml"
#define CONTENT_LOG @"agentui.log"
#define CONTENT_BACKUP_LOG @"agentui_"
#define CONTENT_LOG_EXTENSION @".log"
#define MAX_FILES_COUNT 5
#define MAX_FIle_SIZe = 2000000;

@interface PACommonUtilityController()  {
  PAFileSystemManager *_fileSystemManager;
  
  
}
@property (weak)PAJSCommandDelegate* delegate;
@property (strong) NSOperationQueue *operationQueue;
@property (strong) NSString *ipAddress;
@property (strong) NSString *macAddress;

@end
@implementation PACommonUtilityController

- (instancetype)initWithCommandDelegate:(PAJSCommandDelegate*)aDelegate;
{
  self = [super init];
  if (self) {
    self.delegate = aDelegate;
    self.operationQueue = [[NSOperationQueue alloc] init];
    _fileSystemManager = [[PAFileSystemManager alloc] init];
    [self.operationQueue setMaxConcurrentOperationCount:1];
    [self performSelectorInBackground:@selector(fetchIPAddress) withObject:nil];
  }
  return self;
}

- (PAJSResult*)executeCommand:(PAJSInvokedCommand*)command {
  if (command.isSync) {
    return [self executeSyncCommand:command];
  }
  [self executeAsyncCommand:command];
  return nil;
}

- (PAJSResult*)executeSyncCommand:(PAJSInvokedCommand*)command {
  if ([command.methodName isEqualToString:@"GetIniValue"]) {
    return [self getINIValue:command];
  }
  else if ([command.methodName isEqualToString:@"getContext"]) {
    return [self userContextDirValues:command];
  }
  else if ([command.methodName isEqualToString:@"ConsoleLog"]) {
    [self log:command];
  }
  else if([command.methodName isEqualToString:@"GetPath"]) {
    return [self getPath:command];
  }
  else if([command.methodName isEqualToString:@"SetIniValue"]) {
    
  }
  else if ([command.methodName isEqualToString:@"GetArguments"]) {
    return [self objectsFromCommandLine];
  }
  else if ([command.methodName isEqualToString:@"WriteUsageLog"]) {
    return [self writeContentLog:command];
  }
  else if ([command.methodName isEqualToString:@"WriteUsageLogOptimizer"]) {
    return [self writeUsageLog:command];
  }
  return nil;
}

- (void)executeAsyncCommand:(PAJSInvokedCommand*)command  {
  
  if ([command.methodName isEqualToString:@"WriteUsageLog"]) {
    NSInvocationOperation *operation = [[NSInvocationOperation alloc] initWithTarget:self
                                                                            selector:@selector(writeContentLog:) object:command];
    [self.operationQueue addOperation:operation];
  }
}

- (PAJSResult*)getINIValue:(PAJSInvokedCommand*)command {
  if (self.delegate.controller.currentApp == PAMainApp) {
    [self setupIniFile];
  }else if (self.delegate.controller.currentApp == PANotificationApp) {
    [self setupNotificationAppIniFile];
  }
  if (command.arguments.count > 3) {
    NSString* plistFile = command.arguments[0];
    NSString* section = command.arguments[1];
    NSString* key = command.arguments[2];
    NSString* defVal = command.arguments[3];
    //        if (plistFile.length == 0)
    {
      plistFile = [_fileSystemManager clientUiConfigFileRootPath];
      if (self.delegate.controller.currentApp == PAMainApp) {
        plistFile = [plistFile stringByAppendingPathComponent:MAIN_INI_PLIST_NAME];
      }else if (self.delegate.controller.currentApp == PANotificationApp) {
        plistFile = [plistFile stringByAppendingPathComponent:NOTIF_TRAY_INI_PLIST_NAME];
      }
      PAPlistReader *reader = [[PAPlistReader alloc]initWithPlistAtPath:plistFile];
      NSString* value = [reader valueForKey:key inSection:section];
      
      NSString* resVal = defVal;
      if (value != nil) {
        resVal = value;
      }
      PAJSResult* result = [[PAJSResult alloc] init];
      result.status = 0;
      result.data = resVal;
      return result;
    }
    
  }
  return nil;
}

- (PAJSResult*)userContextDirValues:(PAJSInvokedCommand*)command {
  PAContextConfiguration* contextconfiguration = [PAConfigurationManager appConfigurations].contextConfiguration;
  
  //Replaced %SERVERROOT% macro to ServerBaseURL
  PAAppConfiguration *appconfiguration = [PAConfigurationManager appConfigurations];
  NSString* serverBaseURL = appconfiguration.serverBaseUrlPath;
  contextconfiguration.actionsPath = [self replaceMacroToServerURL:contextconfiguration.actionsPath:serverBaseURL];
  contextconfiguration.backupPath = [self replaceMacroToServerURL:contextconfiguration.backupPath:serverBaseURL];
  contextconfiguration.binPath = [self replaceMacroToServerURL:contextconfiguration.binPath:serverBaseURL];
  contextconfiguration.dataPath = [self replaceMacroToServerURL:contextconfiguration.dataPath:serverBaseURL];
  contextconfiguration.dnaPath = [self replaceMacroToServerURL:contextconfiguration.dnaPath:serverBaseURL];
  contextconfiguration.dnaBackupPath = [self replaceMacroToServerURL:contextconfiguration.dnaBackupPath:serverBaseURL];
  contextconfiguration.issuePath = [self replaceMacroToServerURL:contextconfiguration.issuePath:serverBaseURL];
  contextconfiguration.logsPath = [self replaceMacroToServerURL:contextconfiguration.logsPath:serverBaseURL];
  contextconfiguration.profilesPath = [self replaceMacroToServerURL:contextconfiguration.profilesPath:serverBaseURL];
  contextconfiguration.publishPath = [self replaceMacroToServerURL:contextconfiguration.publishPath:serverBaseURL];
  contextconfiguration.userProfilePath = [self replaceMacroToServerURL:contextconfiguration.userProfilePath:serverBaseURL];
  contextconfiguration.vaultPath = [self replaceMacroToServerURL:contextconfiguration.vaultPath:serverBaseURL];
  
  contextconfiguration.userServerPath = @"SdcContext:DirUserServer=";
  contextconfiguration.userServerPath = [contextconfiguration.userServerPath stringByAppendingString:[_fileSystemManager appRootFileSystemPath]];
  contextconfiguration.userServerPath = [contextconfiguration.userServerPath stringByAppendingString:@"/"];
  contextconfiguration.userName = @"SdcContext:UserName=";
  contextconfiguration.userName = [contextconfiguration.userName stringByAppendingString:NSUserName()];
  PAJSResult* result = [[PAJSResult alloc] init];
  result.status = 0;
  result.data = [contextconfiguration configurations];
  return result;
}

- (NSString*) replaceMacroToServerURL:(NSString *)configVal:(NSString *)serverBaseURL {
  return [configVal stringByReplacingOccurrencesOfString:@"%SERVERROOT%" withString: serverBaseURL];
}

- (PAJSResult*)objectsFromCommandLine {
  PAJSResult* result = [[PAJSResult alloc] init];
  result.status = 0;
  result.data = self.delegate.controller.arguments != nil ? self.delegate.controller.arguments : @[];
  return result;
}

- (void)log:(PAJSInvokedCommand*)command {
  //NSLog(@"Console : %@",command.arguments);
}

- (PAJSResult*)writeContentLog:(PAJSInvokedCommand*)command {
  if (command.arguments.count > 0) {
    NSString* logFile = [self logFilePath];
    if (![_fileSystemManager doesFileExist:logFile]) {
      [_fileSystemManager createFileAtPath:logFile attributes:nil error:nil];
    }
    NSString* content = command.arguments[0];
    content = [self formatString:content];
    
    NSData *data = [content dataUsingEncoding:NSUTF8StringEncoding];
    
    [_fileSystemManager writeToFileAtPath:logFile data:data withMode:PAFileModeAppend];
  }
  
  return nil;
}

- (PAJSResult*)getPath:(PAJSInvokedCommand*)command {
  if (command.arguments.count > 0) {
    NSString* pathType = command.arguments[0] == nil ? @"" : command.arguments[0];
    if([pathType isEqualToString:@"EXE_PATH"])
    {
      NSString *paMainAppPath = [PAConfigurationManager appConfigurations].mainAppPath;
      PAJSResult* result = [[PAJSResult alloc] init];
      result.status = 0;
      result.data = paMainAppPath;
      return result;
    }
  }
  
  return nil;
}

- (NSString*)logFilePath {
  NSString* logFile = [_fileSystemManager logsFileDirectoryPath];
  NSString* logDirectory = [_fileSystemManager logsFileDirectoryPath];
  logFile = [logFile stringByAppendingPathComponent:CONTENT_LOG];
  if (![_fileSystemManager doesFileExist:logFile]) {
    [_fileSystemManager createFileAtPath:logFile attributes:nil error:nil];
  }else {
    PAItemAttributes* attributes = [_fileSystemManager attributesOfItemAtPath:logFile];
    if (attributes.size > 2000000) {
      NSArray* files = [_fileSystemManager filesOfDirectory:[_fileSystemManager logsFileDirectoryPath]];
      NSMutableArray* backupFiles = [NSMutableArray array];
      for (NSString* fileName in files) {
        if ([fileName rangeOfString:CONTENT_BACKUP_LOG].location != NSNotFound) {
          [backupFiles addObject:fileName];
        }
      }
      [backupFiles sortUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        return [obj1 compare:obj2 options:NSCaseInsensitiveSearch];
      }];
      if (backupFiles.count == 0) {
        NSString*  newName = [NSString stringWithFormat:@"%@1%@",CONTENT_BACKUP_LOG,CONTENT_LOG_EXTENSION];
        [_fileSystemManager renameFile:logFile newFileName:newName];
        [_fileSystemManager createFileAtPath:logFile attributes:nil error:nil];
        return logFile;
      }else {
        if(backupFiles.count == MAX_FILES_COUNT) {
          NSString* fileToDelete = [logDirectory stringByAppendingPathComponent:backupFiles[backupFiles.count -1]];
          [_fileSystemManager deleteFile:fileToDelete];
          [backupFiles removeObject:backupFiles[backupFiles.count -1]];
        }
        for (NSInteger index = backupFiles.count -1; index >= 0 ; index--) {
          NSString* newFileName = [NSString stringWithFormat:@"%@%ld%@",CONTENT_BACKUP_LOG,index+2,CONTENT_LOG_EXTENSION];
          NSString* fileToRename = [logDirectory stringByAppendingPathComponent:backupFiles[index]];
          [_fileSystemManager renameFile:fileToRename newFileName:newFileName];
        }
        NSString*  newName = [NSString stringWithFormat:@"%@1%@",CONTENT_BACKUP_LOG,CONTENT_LOG_EXTENSION];
        [_fileSystemManager renameFile:logFile newFileName:newName];
        [_fileSystemManager createFileAtPath:logFile attributes:nil error:nil];
        return logFile;
        
      }
    }
  }
  return logFile;
}

- (NSString*)getHexTimeStamp {
  
  NSDate *now = [NSDate date];
  
  unsigned long long offsetEpoch = 0x19db1ded53e8000L;
  
  NSTimeInterval secondsBetween = [now timeIntervalSince1970];
  
  unsigned long long windowsTime = (unsigned long long) secondsBetween;
  windowsTime = (windowsTime * 10000000L) + offsetEpoch;
  
  unsigned long lowTime = (unsigned long)(windowsTime & 0x00000000FFFFFFFF);
  unsigned long highTime = (unsigned long)((windowsTime & 0xFFFFFFFF00000000) >> 32);
  long offset = -([[NSTimeZone localTimeZone] secondsFromGMT]) / 60;
  if(offset == 0) {
    offset = -1;
  }
  NSString *hexTime = [NSString stringWithFormat:@"%08lx%08lx%03x", highTime,lowTime, offset];
  return hexTime;
}

- (NSString*)formatString:(NSString*)content {
  NSDate* sourceDate = [NSDate date];
  
  NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
  NSTimeZone* destinationTimeZone = [NSTimeZone systemTimeZone];
  
  NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:sourceDate];
  NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:sourceDate];
  NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
  
  NSDate* destinationDate = [[NSDate alloc] initWithTimeInterval:interval sinceDate:sourceDate];
  NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
  NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"GMT"];
  [formatter setTimeZone:timeZone];
  [formatter setDateFormat:@"yyyy-MM-dd hh:mm:ss a"];
  NSString *stringFromDate = [formatter stringFromDate:destinationDate];
  
  NSString *stringAddress = [self fetchIPAddress];
  NSString *sessionID = [NSString getInstanceUUID];
  
  NSString* instanceID = @"0";
  NSInteger eventType = 4;
  NSInteger gmt = (NSInteger) interval/60;
  if(nil == self.macAddress) {
    self.macAddress = [self getMACAddress];
  }
  NSString* userId = [self getHexTimeStamp]; //@"01d34f367ff2cc70fffffeb6"
  
  NSString* result = [NSString stringWithFormat:@"%@|%ld|%@||%@__%@__%@__%@|%ld|%@|%@|\n",instanceID,eventType,userId,sessionID,stringAddress,userId,content,gmt,stringFromDate,instanceID];
  return result;
}

- (NSString*)fetchIPAddress {
  if(nil == self.ipAddress) {
    NSArray *addresses = [[NSHost currentHost] addresses];
    NSString* stringAddress = @"";
    for (NSString *anAddress in addresses) {
      if (![anAddress hasPrefix:@"127"] && [[anAddress componentsSeparatedByString:@"."] count] == 4) {
        if (stringAddress.length == 0) {
          stringAddress = anAddress;
        }else {
          stringAddress = [stringAddress stringByAppendingFormat:@"_%@",anAddress];
        }
      }
    }
    self.ipAddress = stringAddress;
  }
  return self.ipAddress;
}

- (PAJSResult*)writeUsageLog:(PAJSInvokedCommand*)command {
  if (command.arguments.count > 0) {
    NSString* logFile = [_fileSystemManager optimizationFolderPath];
    logFile = [logFile stringByAppendingPathComponent:USAGE_LOG];
    NSString* content = command.arguments[0];
    
    NSXMLElement *rootElement;
    if(![_fileSystemManager doesFileExist:logFile]) {
      rootElement = [self buildChangeLogXML];
    }else {
      rootElement = [self createXMLDocumentFromFile:logFile];
    }
    NSXMLElement * entriesNode = [rootElement elementsForName:@"entries"][0];
    [entriesNode addChild:[NSXMLNode elementWithName:@"string" stringValue:content]];
    NSData* xmlData = [[rootElement XMLString] dataUsingEncoding:NSUTF8StringEncoding];
    PAXmlServiceManager *xmlManager = [[PAXmlServiceManager alloc] init];
    [xmlManager writeXmlData:xmlData toPath:logFile error:nil];
  }
  return nil;
}

- (void)setupIniFile {
  NSString* path = [_fileSystemManager clientUiConfigFileRootPath];
  NSString* iniFileName = [path stringByAppendingPathComponent:MAIN_INI_PLIST_NAME];
  if (![_fileSystemManager doesDirectoryExist:path]) {
    [_fileSystemManager createDirectoryAtPath:path
                  withIntermediateDirectories:YES
                                   attributes:nil
                                        error:nil];
    [self createDefaultIniForMainAppFileAtPath:iniFileName];
  }
  else if(![_fileSystemManager doesFileExist:iniFileName]) {
    [self createDefaultIniForMainAppFileAtPath:iniFileName];
  }
}

- (void)setupNotificationAppIniFile {
  NSString* path = [_fileSystemManager clientUiConfigFileRootPath];
  NSString* iniFileName = [path stringByAppendingPathComponent:NOTIF_TRAY_INI_PLIST_NAME];
  if (![_fileSystemManager doesDirectoryExist:path]) {
    [_fileSystemManager createDirectoryAtPath:path
                  withIntermediateDirectories:YES
                                   attributes:nil
                                        error:nil];
    [self createDefaultIniForNotificationAppFileAtPath:iniFileName];
  }
  else if(![_fileSystemManager doesFileExist:iniFileName]) {
    [self createDefaultIniForNotificationAppFileAtPath:iniFileName];
  }
}

- (void)createDefaultIniForMainAppFileAtPath:(NSString*)path {
  
  PAPlistLockManager *lckMgr = [PAPlistLockManager sharedInstance];
  [lckMgr runWithWriteLock:path codeBlock:^{
    PAPlistServiceManager* manager = [[PAPlistServiceManager alloc] init];
    [manager createPlistAtPath:path];
    NSString* configFilePath = [_fileSystemManager clientUiConfigFileRootPath];
    configFilePath = [configFilePath stringByAppendingPathComponent:INSTALL_CONFIG_FILE_FOR_MAIN_APP];
    PAPlistWriter *writer = [[PAPlistWriter alloc]initWithPlistAtPath:path];
    [writer setValue:@"ProactiveAssist" forKey:@"instancename" inSection:@"PROVIDERINFO"];
    [writer setValue:@"Experience92" forKey:@"provider" inSection:@"PROVIDERINFO"];
    [writer setValue:@"https://experience92.avolin.com/sdcxuser" forKey:@"serverbaseurl" inSection:@"PROVIDERINFO"];
    [writer setValue:@"1" forKey:@"center" inSection:@"SETUP"];
    [writer setValue:@"clientui" forKey:@"configtouse" inSection:@"SETUP"];
    [writer setValue:@"%DirUserServer%/data/" forKey:@"contentPath" inSection:@"SETUP"];
    [writer setValue:@"1" forKey:@"disableDropTarget" inSection:@"SETUP"];
    [writer setValue:@"0,0,225,50" forKey:@"dragRegion" inSection:@"SETUP"];
    [writer setValue:configFilePath forKey:@"installconfigfile" inSection:@"SETUP"];
    [writer setValue:@"clientui" forKey:@"configtouse" inSection:@"SETUP"];
    [writer setValue:@"1" forKey:@"preview" inSection:@"SETUP"];
    [writer setValue:@"1000" forKey:@"width" inSection:@"SETUP"];
    [writer setValue:@"680" forKey:@"height" inSection:@"SETUP"];
    [writer synchronize];
    //    [writer setValue:@"ProactiveAssist" forKey:@"instancename" inSection:@"SETUP"];
    //    [writer setValue:@"ProactiveAssist" forKey:@"instancename" inSection:@"SETUP"];
    //    [writer setValue:@"ProactiveAssist" forKey:@"instancename" inSection:@"SETUP"];
    //    [writer setValue:@"ProactiveAssist" forKey:@"instancename" inSection:@"SETUP"];
    //    [writer setValue:@"ProactiveAssist" forKey:@"instancename" inSection:@"SETUP"];
    //    [writer setValue:@"ProactiveAssist" forKey:@"instancename" inSection:@"SETUP"];
    //    [writer setValue:@"ProactiveAssist" forKey:@"instancename" inSection:@"SETUP"];
    //    [writer setValue:@"ProactiveAssist" forKey:@"instancename" inSection:@"SETUP"];
  }];
}

- (void)createDefaultIniForNotificationAppFileAtPath:(NSString*)path {
  
  PAPlistLockManager *lckMgr = [PAPlistLockManager sharedInstance];
  [lckMgr runWithWriteLock:path codeBlock:^{
    PAPlistServiceManager* manager = [[PAPlistServiceManager alloc] init];
    [manager createPlistAtPath:path];
    NSString* configFilePath = [_fileSystemManager clientUiConfigFileRootPath];
    configFilePath = [configFilePath stringByAppendingPathComponent:INSTALL_CONFIG_FILE_FOR_NOTIF_APP];
    PAPlistWriter *writer = [[PAPlistWriter alloc]initWithPlistAtPath:path];
    [writer setValue:@"minibcont" forKey:@"instancename" inSection:@"PROVIDERINFO"];
    [writer setValue:@"Experience92" forKey:@"provider" inSection:@"PROVIDERINFO"];
    [writer setValue:@"https://experience92.avolin.com/sdcxuser" forKey:@"serverbaseurl" inSection:@"PROVIDERINFO"];
    [writer setValue:@"0" forKey:@"center" inSection:@"SETUP"];
    [writer setValue:@"minibcont" forKey:@"configtouse" inSection:@"SETUP"];
    [writer setValue:@"%DirUserServer%/data/" forKey:@"contentPath" inSection:@"SETUP"];
    [writer setValue:@"1" forKey:@"disableDropTarget" inSection:@"SETUP"];
    [writer setValue:@"0,0,225,50" forKey:@"dragRegion" inSection:@"SETUP"];
    [writer setValue:configFilePath forKey:@"installconfigfile" inSection:@"SETUP"];
    [writer setValue:@"1" forKey:@"preview" inSection:@"SETUP"];
    [writer setValue:@"212" forKey:@"height" inSection:@"SETUP"];
    [writer setValue:@"365" forKey:@"width" inSection:@"SETUP"];
    [writer synchronize];
    //    [writer setValue:@"ProactiveAssist" forKey:@"instancename" inSection:@"SETUP"];
    //    [writer setValue:@"ProactiveAssist" forKey:@"instancename" inSection:@"SETUP"];
    //    [writer setValue:@"ProactiveAssist" forKey:@"instancename" inSection:@"SETUP"];
    //    [writer setValue:@"ProactiveAssist" forKey:@"instancename" inSection:@"SETUP"];
    //    [writer setValue:@"ProactiveAssist" forKey:@"instancename" inSection:@"SETUP"];
    //    [writer setValue:@"ProactiveAssist" forKey:@"instancename" inSection:@"SETUP"];
    //    [writer setValue:@"ProactiveAssist" forKey:@"instancename" inSection:@"SETUP"];
    //    [writer setValue:@"ProactiveAssist" forKey:@"instancename" inSection:@"SETUP"];
    
  }];
}


- (NSXMLElement *)createXMLDocumentFromFile:(NSString *)file {
  NSError *err=nil;
  NSURL *furl = [NSURL fileURLWithPath:file];
  if (!furl) {
    //        NSLog(@"Can't create an URL from file %@.", file);
    return nil;
  }
  NSXMLDocument* xmlDoc = [[NSXMLDocument alloc] initWithContentsOfURL:furl
                                                               options:(NSXMLNodePreserveWhitespace|NSXMLNodePreserveCDATA)
                                                                 error:&err];
  if (xmlDoc == nil) {
    xmlDoc = [[NSXMLDocument alloc] initWithContentsOfURL:furl
                                                  options:NSXMLDocumentTidyXML
                                                    error:&err];
  }
  return [xmlDoc rootElement];
}

-(NSXMLElement *)buildChangeLogXML {
  NSXMLElement *root =
  (NSXMLElement *)[NSXMLNode elementWithName:@"UsageLog"];
  [root addNamespace:[NSXMLNode namespaceWithName:@"xsi" stringValue:@"http://www.w3.org/2001/XMLSchema-instance"]];
  [root addNamespace:[NSXMLNode namespaceWithName:@"xsd" stringValue:@"http://www.w3.org/2001/XMLSchema"]];
  NSXMLDocument* xmlDoc = [[NSXMLDocument alloc] initWithRootElement:root];
  [xmlDoc setVersion:@"1.0"];
  NSXMLElement *entriesNode = (NSXMLElement *)[NSXMLNode elementWithName:@"entries"];
  [root addChild:entriesNode];
  return root;
}

- (NSString*) getMACAddress {
  io_service_t platformExpert = IOServiceGetMatchingService(kIOMasterPortDefault,IOServiceMatching("IOPlatformExpertDevice"));
  if (!platformExpert)
    return nil;
  
  CFTypeRef serialNumberAsCFString = IORegistryEntryCreateCFProperty(platformExpert,CFSTR(kIOPlatformUUIDKey),kCFAllocatorDefault, 0);
  if (!serialNumberAsCFString)
    return nil;
  
  IOObjectRelease(platformExpert);
  return (__bridge NSString *)(serialNumberAsCFString);;
}

@end
