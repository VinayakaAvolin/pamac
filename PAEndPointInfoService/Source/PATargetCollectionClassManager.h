//
//  PATargetCollectionClassManager.h
//  PAEndPointInfoService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PATargetClass.h"

typedef void(^PATargetCollectionClassManagerCompletionBlock)(BOOL);

@interface PATargetCollectionClassManager : NSObject

- (instancetype)initWithScriptXml:(NSString *)xmlPath;

- (void)constructTargetCollectionClassesWithCompletion:(PATargetCollectionClassManagerCompletionBlock)block;

- (PATargetClass *)targetCollectionClassMatchingName:(NSString *)name;

- (NSArray *)allTargetCollectionClasses;


@end
