//
//  PATargetClassProperty.m
//  PAEndPointInfoService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PATargetClassProperty.h"

@implementation PATargetClassProperty

- (instancetype)initWithName:(NSString *)name
                        type:(NSString *)type
                   valueType:(PAPropertyValueType)valueType
                      script:(PATargetScript *)script
                 placeholder:(NSString *)valuePlaceholder {
  self = [super init];
  if (self) {
    if (name && (script || valuePlaceholder)) {
      _name = name;
      _type = type;
      _valueType = valueType;
      _script = script;
      _valuePlaceholder = valuePlaceholder;
    } else {
      self = nil;
    }
  }
  return self;
}
/*
 Returns xml string in below format:
 
 <PROPERTY NAME="AreMacrosDisabled" TYPE="string">
 <VALUE>True</VALUE>
 </PROPERTY>
 
 */
- (NSString *)xmlString {
  return nil;
}

@end
