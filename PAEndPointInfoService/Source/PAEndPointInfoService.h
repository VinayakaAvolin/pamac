//
//  PAEndPointInfoService.h
//  PAEndPointInfoService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Cocoa/Cocoa.h>

//! Project version number for PAEndPointInfoService.
FOUNDATION_EXPORT double PAEndPointInfoServiceVersionNumber;

//! Project version string for PAEndPointInfoService.
FOUNDATION_EXPORT const unsigned char PAEndPointInfoServiceVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <PAEndPointInfoService/PublicHeader.h>

#import <PAEndPointInfoService/PAEnPointInfoManager.h>
#import <PAEndPointInfoService/PAEndPointConstants.h>
