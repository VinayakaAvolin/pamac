//
//  PAEnPointInfoManager.h
//  PAEndPointInfoService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PAEndPointConstants.h"

typedef void(^PAEnPointInfoManagerCompletionBlock)(BOOL success);
typedef void(^PAUploadManagerCompletionBlock)(BOOL success, NSError* error, NSString *fileUploaded);

@interface PAEnPointInfoManager : NSObject
// for sourceType = PATargetScriptSourceTypeDefaultXml; no need to pass input xml
// for for other source types; inputXmlPath is must
- (instancetype)initWithInfoClass:(NSString *)infoClass
                         inputXml:(NSString *)inputXmlPath
                        outputXml:(NSString *)outputXmlmlPath
                           source:(PATargetScriptSourceType)sourceType;
// for sourceType = PATargetScriptSourceTypeDefaultXml; no need to pass input xml
- (instancetype)initWithInputXml:(NSString *)inputXmlPath
                       outputXml:(NSString *)outputXmlmlPath
                          source:(PATargetScriptSourceType)sourceType;

- (void)createEndpointInfoWithCompletion:(PAEnPointInfoManagerCompletionBlock)completionBlock;

- (void)createAllEndpointInfoWithCompletion:(PAEnPointInfoManagerCompletionBlock)completionBlock;

- (void)deleteEndpointInfo;

- (void)submitEndpointInfoWithUploadURL:(NSString*)uploadURL parameters:(NSDictionary*)parameters completion:(PAUploadManagerCompletionBlock)completionBlock;

/// methods to get property value from matching class instance of output xml
- (instancetype)initWithOutputXml:(NSString *)outputXmlmlPath;

- (NSString *)valueOfProperty:(NSString *)propertyName
                      inClass:(NSString *)className;
- (NSArray *)valuesOfProperty:(NSString *)propertyName
                      inClass:(NSString *)className;
@end
