//
//  PATargetClassXmlParser.m
//  PAEndPointInfoService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <PAFileSystemService/PAFileSystemService.h>
#import "PATargetClassXmlParser.h"
#import "PATargetClass.h"
#import "NSString+Additions.h"
#import "PATargetScript.h"
#import "PATargetClassProperty.h"

NSString *const kPATGClassName = @"CLASSNAME";
NSString *const kPATGObjectPath = @"VALUE.OBJECTWITHPATH/INSTANCE";

NSString *const kPATGParserObjectPathKey = @"VALUE.OBJECTWITHPATH";
NSString *const kPATGParserInstanceKey = @"INSTANCE";
NSString *const kPATGParserPropertyKey = @"PROPERTY";
NSString *const kPATGParserValueKey = @"VALUE";
NSString *const kPATGParserScriptKey = @"SCRIPT";
NSString *const kPATGParserLanguageKey = @"_LANG";
NSString *const kPATGParserTextKey = @"__text";
NSString *const kPATGParserTypeKey = @"_TYPE";
NSString *const kPATGParserNameKey = @"_NAME";
NSString *const kPATGParserClassNameKey = @"_CLASSNAME";

@interface PATargetClassXmlParser () {
  NSString *_scriptXmlPath;
}

@end

@implementation PATargetClassXmlParser

- (instancetype)initWithScriptXml:(NSString *)xmlPath {
  self = [super init];
  if (self && xmlPath) {
    _scriptXmlPath = xmlPath;
  } else {
    self = nil;
  }
  return self;
}

- (void)parseTargetCollectionClassesWithCompletion:(PAManifestParserCompletionBlock)completionBlock {
  NSMutableArray *targetClassList = [[NSMutableArray alloc]init];
  PAFileSystemManager *fsManager = [[PAFileSystemManager alloc]init];
  // NSString *defaultXmlFilePath = [fsManager localDefaultXmlFilePath];
  NSString *defaultXmlFilePath = [_scriptXmlPath stringByExpandingTildeInPath];
  if ([fsManager doesFileExist:defaultXmlFilePath]) {
    NSDictionary *xmlDict = [self dictionaryFromXmlAtPath:defaultXmlFilePath];
    if (xmlDict) {
      NSArray *instanceList = [xmlDict valueForKey:kPATGParserScriptKey];
      for (NSDictionary *scriptInstanceDict in instanceList) {
        NSDictionary *instanceDict = [scriptInstanceDict valueForKey:kPATGParserObjectPathKey];
        if (instanceDict) {
          id instanceValue = [instanceDict valueForKey:kPATGParserInstanceKey];
          NSMutableArray *targetClassDictList = [[NSMutableArray alloc]init];
          if([instanceValue isKindOfClass:[NSDictionary class]]) {
            [targetClassDictList addObject:(NSDictionary *)instanceValue];
          } else if (([instanceValue isKindOfClass:[NSArray class]])) {
            [targetClassDictList addObjectsFromArray:(NSArray *)instanceValue];
          }
          for (NSDictionary *targetClassDict in targetClassDictList) {
            PATargetClass *targetClass = [self targetClassFrom:targetClassDict];
            if (targetClass) {
              [targetClassList addObject:targetClass];
            }
          }
        }
      }
      if (targetClassList.count) {
        completionBlock(true, targetClassList);
        return;
      }
    }
  }
  completionBlock(false, nil);
}

- (PATargetClass *)targetClassFrom:(NSDictionary *)xmlDict {
  PATargetClass *targetClass = nil;
  NSString *tgClassName = [xmlDict valueForKey:kPATGParserClassNameKey];
  if (![tgClassName isEmptyOrHasOnlyWhiteSpaces]) {
    NSDictionary *tgClassScriptDict = [xmlDict valueForKey:kPATGParserScriptKey];
    PATargetScript *script = [self targetClassScriptFrom:tgClassScriptDict];
    NSArray *propertyList = nil;
    /// either entire PATargetClass has single script or individual property of PATargetClass have seperate scripts.
    if (!script) {
      propertyList = [self targetClassPropertiesFrom:xmlDict];
    }
    
    targetClass = [[PATargetClass alloc]initWithName:tgClassName script:script properties:propertyList];
  }
  return targetClass;
}

- (PATargetScript *)targetClassScriptFrom:(NSDictionary *)xmlDict {
  PATargetScript *script = nil;
  if (xmlDict) {
    NSString *tgClassScriptLanguage = [xmlDict valueForKey:kPATGParserLanguageKey];
    NSString *tgClassScript = [xmlDict valueForKey:kPATGParserTextKey];
    script = [[PATargetScript alloc]initWithType:tgClassScriptLanguage script:tgClassScript];
  }
  return script;
}

- (NSArray *)targetClassPropertiesFrom:(NSDictionary *)xmlDict {
  if (!xmlDict) {
    return nil;
  }
  NSMutableArray *propertyList = [[NSMutableArray alloc]init];
  
  id propertyValue = [xmlDict valueForKey:kPATGParserPropertyKey];
  NSMutableArray *propertyDictList = [[NSMutableArray alloc]init];
  if([propertyValue isKindOfClass:[NSDictionary class]]) {
    [propertyDictList addObject:(NSDictionary *)propertyValue];
  } else if (([propertyValue isKindOfClass:[NSArray class]])) {
    [propertyDictList addObjectsFromArray:(NSArray *)propertyValue];
  }
  for (NSDictionary *propertyDict in propertyDictList) {
    PATargetClassProperty *property = nil;
    NSString *propertyName = [propertyDict valueForKey:kPATGParserNameKey];
    NSString *propertyType = [propertyDict valueForKey:kPATGParserTypeKey];
    id propertyValue = [propertyDict valueForKey:kPATGParserValueKey];
    NSString *propertyPlaceholderValue = nil;
    PATargetScript *script = nil;
    PAPropertyValueType valueType = PAPropertyValueTypeNone;
    
    if([propertyValue isKindOfClass:[NSDictionary class]]) {
      NSDictionary *scriptInfo = [(NSDictionary *)propertyValue valueForKey:kPATGParserScriptKey];
      script = [self targetClassScriptFrom:scriptInfo];
      if (script) {
        valueType = PAPropertyValueTypeScript;
      }
    } else if (([propertyValue isKindOfClass:[NSString class]])) {
      propertyPlaceholderValue = (NSString *)propertyValue;
      valueType = PAPropertyValueTypePlaceholder;
    }
    property = [[PATargetClassProperty alloc]initWithName:propertyName type:propertyType valueType:valueType script:script placeholder:propertyPlaceholderValue];
    if (property) {
      [propertyList addObject:property];
    }
  }
  if (propertyList.count) {
    return [NSArray arrayWithArray:propertyList];
  }
  return nil;
}

@end
