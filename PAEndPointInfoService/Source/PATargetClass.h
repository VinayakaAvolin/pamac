//
//  PATargetClass.h
//  PAEndPointInfoService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PATargetScript.h"

typedef void(^PAScriptExecutionCallback)(BOOL success);

@interface PATargetClass : NSObject

@property (nonatomic) NSString *name;
@property (nonatomic) NSArray *properties;

@property (nonatomic, assign) BOOL hasMultipleInstance;
@property (nonatomic) PATargetScript *script;

@property (nonatomic) NSString *xmlString;

- (instancetype)initWithName:(NSString *)name
                      script:(PATargetScript *)script
                  properties:(NSArray *)properties;

/// method to initiate script execution
/// either one script will have to be executed to populate all property values
/// or individual properties will have seperate scripts to be executed to get its value
- (void)executeScriptWithCallback:(PAScriptExecutionCallback)callback;

@end
