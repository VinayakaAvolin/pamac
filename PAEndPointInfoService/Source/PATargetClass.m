//
//  PATargetClass.m
//  PAEndPointInfoService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <PAScriptRunnerService/PAScriptRunnerService.h>
#import "PATargetClass.h"
#import "PATargetClassProperty.h"

NSString *const kPATCInstanceDelimeter = @"%Delimeter%";

@interface PATargetClass () {
  NSUInteger _scriptCounter;
  PAScriptExecutionCallback _callback;
  BOOL _scriptExecutionStatus;
  NSString *_scriptOutputXml;
}


@end

@implementation PATargetClass

- (instancetype)initWithName:(NSString *)name
                      script:(PATargetScript *)script
                  properties:(NSArray *)properties {
  
  self = [super init];
  if (self) {
    if (!name) {
      self = nil;
    } else {
      _name = name;
      _script = script;
      if (!script) {
        if (!properties.count) {
          self = nil;
        } else {
          _properties = [NSArray arrayWithArray:properties];
        }
      }
    }
  }
  return self;
}

- (void)executeScriptWithCallback:(PAScriptExecutionCallback)callback {
  NSArray *scripts = [self targetClassScripts];
  _scriptCounter = scripts.count;
  _callback = callback;
  _scriptOutputXml = @"";
  for (PATargetScript *script in scripts) {
    [PAScriptRunner executeScript:script.script ofType:script.scriptType writeToFileBeforeExecute:true scriptName:_name callBack:^(BOOL success, NSError *error, id result) {
      _scriptCounter--;
      _scriptExecutionStatus = success;
      if (success && [result isKindOfClass:[NSString class]]) {
        _scriptOutputXml = [_scriptOutputXml stringByAppendingString:(NSString *)result];
      } else if (!success) {
        _scriptCounter = 0;
      }
      [self scriptExecuted];
    }];
  }
}

- (NSArray *)targetClassScripts {
  NSMutableArray *scriptList = [[NSMutableArray alloc]init];
  // either entire target class will have one script or; invididual properties will have its own scripts
  if (_script.script) {
    [scriptList addObject:_script];
  } else {
    for (PATargetClassProperty *property in _properties) {
      if (property.script.script) {
        [scriptList addObject:property.script];
      }
    }
  }
  
  if (scriptList.count) {
    return [NSArray arrayWithArray:scriptList];
  }
  return nil;
}
/// once all scripts are executed; inform the caller
- (void)scriptExecuted {
  if (_scriptCounter == 0 && _callback) {
    _callback(_scriptExecutionStatus);
  }
}
/*
 Returns xml string in below format:
 
 <INSTANCE CLASSNAME="MicrosoftExcelProblemDetectors">
 <PROPERTY NAME="AreMacrosDisabled" TYPE="string">
 <VALUE>True</VALUE>
 </PROPERTY>
 <PROPERTY NAME="AreMultipleExcelFilesOpening" TYPE="string">
 <VALUE>False</VALUE>
 </PROPERTY>
 </INSTANCE>
 */

- (NSString *)xmlString {
  NSString *xmlStr = nil;
  
  if (_scriptOutputXml) {
    xmlStr = @"";
    xmlStr = [xmlStr stringByAppendingString:@"<VALUE.OBJECTWITHPATH>\n"];
    
    NSArray *instanceList = @[_scriptOutputXml];
    if ([_scriptOutputXml containsString:kPATCInstanceDelimeter]) {
      instanceList = [_scriptOutputXml componentsSeparatedByString:kPATCInstanceDelimeter];
    }
    for (NSString *instanceDetailXml in instanceList) {
      xmlStr = [xmlStr stringByAppendingString:[NSString stringWithFormat:@"<INSTANCE CLASSNAME=\"%@\">\n", _name]];
      xmlStr = [xmlStr stringByAppendingString:instanceDetailXml];
      xmlStr = [xmlStr stringByAppendingString:@"</INSTANCE>\n"];
    }
    
    xmlStr = [xmlStr stringByAppendingString:@"</VALUE.OBJECTWITHPATH>\n"];
    xmlStr = [xmlStr stringByReplacingOccurrencesOfString:@"&" withString:@"&amp;"];
  }
  
  return xmlStr;
}

@end
