//
//  PATargetScript.h
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <PAScriptRunnerService/PAScriptRunnerService.h>
#import "PAEndPointConstants.h"

@interface PATargetScript : NSObject

@property (nonatomic) NSString *script; // script string
@property (nonatomic, assign) PATargetScriptType type; /// script tye Eg: apple
@property (nonatomic, assign) PAScriptType scriptType; /// script tye Eg: apple

@property (nonatomic) id scriptOutput; // script output; output is either NSString, NSNumber, NSDictionary or NSArray; this needs to be converted into required xml format

- (instancetype)initWithType:(NSString *)type
                      script:(NSString *)script;
@end
