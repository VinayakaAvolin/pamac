//
//  PATargetCollectionClassManager.m
//  PAEndPointInfoService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PATargetCollectionClassManager.h"
#import "PATargetClassXmlParser.h"

@interface PATargetCollectionClassManager () {
  NSArray *_targetCollectionClasses;
  NSString *_scriptXmlPath;
}

@end

@implementation PATargetCollectionClassManager

- (instancetype)initWithScriptXml:(NSString *)xmlPath {
  self = [super init];
  if (self && xmlPath) {
    _scriptXmlPath = xmlPath;
  } else {
    self = nil;
  }
  return self;
}

- (void)constructTargetCollectionClassesWithCompletion:(PATargetCollectionClassManagerCompletionBlock)block {
  if (_targetCollectionClasses.count) {
    block(true);
    return;
  }
  PATargetClassXmlParser *parser = [[PATargetClassXmlParser alloc]initWithScriptXml:_scriptXmlPath];
  
  [parser parseTargetCollectionClassesWithCompletion:^(BOOL success, NSArray *itemList) {
    if (success && itemList.count) {
      _targetCollectionClasses = [NSArray arrayWithArray:itemList];
    }
    block(success);
  }];
}

- (PATargetClass *)targetCollectionClassMatchingName:(NSString *)name {
  if (_targetCollectionClasses.count) {
    NSArray *filtered = [_targetCollectionClasses filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(name == %@)", name]];
    if (filtered.count) {
      return filtered[0];
    }
  }
  
  return nil;
}

- (NSArray *)allTargetCollectionClasses {
  return _targetCollectionClasses;
}
@end
