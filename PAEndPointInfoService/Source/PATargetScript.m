//
//  PATargetScript.m
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PATargetScript.h"
#import "NSString+Additions.h"

@interface PATargetScript () {
  NSString *_typeString;
}

@end

@implementation PATargetScript

- (instancetype)initWithType:(NSString *)type
                      script:(NSString *)script {
  self = [super init];
  if (self) {
    if (!type || !script) {
      self = nil;
    } else {
      _typeString = type;
      _script = script;
      
      _type = [self getScriptType];
    }
  }
  return self;
}

- (PATargetScriptType)getScriptType {
  PATargetScriptType type = PATargetScriptTypeNone;
  if (![_typeString isEmptyOrHasOnlyWhiteSpaces]) {
    NSArray *scriptTypeArray = [[NSArray alloc] initWithObjects:kPATargetScriptTypeArray];
    NSUInteger index = [scriptTypeArray indexOfObject:_typeString];
    if (NSNotFound == index) {
      type = PATargetScriptTypeNone;
    } else {
      type = (PATargetScriptType)index;
    }
  }
  return type;
}
- (PAScriptType)scriptType {
  PAScriptType scrptType = PAScriptTypeNone;
  switch (_type) {
    case PATargetScriptTypeApple:
      scrptType = PAScriptTypeApple;
      break;
    default:
      break;
  }
  return scrptType;
}

@end
