//
//  PATargetClassXmlParser.h
//  PAEndPointInfoService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <PAParserService/PAParserService.h>

typedef void(^PATargetClassXmlParserCompletionBlock)(BOOL success);

@interface PATargetClassXmlParser : PAXmlParser

- (instancetype)initWithScriptXml:(NSString *)xmlPath;

- (void)parseTargetCollectionClassesWithCompletion:(PAManifestParserCompletionBlock)completionBlock;

@end
