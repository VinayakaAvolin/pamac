//
//  PAEnPointInfoManager.m
//  PAEndPointInfoService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <PAFileSystemService/PAFileSystemService.h>
#import <PAXmlService/PAXmlService.h>
#import <PAParserService/PAParserService.h>
#import "PAEnPointInfoManager.h"
#import "PATargetCollectionClassManager.h"
#import "NSString+Additions.h"
#import "PAUploadManager.h"

NSString *const kPAScriptOutputXmlRootNodeName = @"UPLOADINFO";
NSString *const kPAEPClassName = @"CLASSNAME";
NSString *const kPAEPXPath = @"VALUE.OBJECTWITHPATH/INSTANCE";
NSString *const kPAEPProperty = @"PROPERTY";
NSString *const kPAEPValue = @"VALUE";
NSString *const kPAEPName = @"_NAME";

@interface PAEnPointInfoManager () {
  NSString *_infoClass;
  NSString *_outputXmlPath;
  NSString *_inputXmlPath;
  PATargetCollectionClassManager *_targetCollectionClassManager;
  PAFileSystemManager *_fsManager;
  PAEnPointInfoManagerCompletionBlock _completionBlock;
  NSUInteger _totalTargetClasses;
}

@end

@implementation PAEnPointInfoManager

- (instancetype)initWithInfoClass:(NSString *)infoClass
                         inputXml:(NSString *)inputXmlPath
                        outputXml:(NSString *)outputXmlmlPath
                           source:(PATargetScriptSourceType)sourceType {
  self = [super init];
  if (self) {
    if (outputXmlmlPath) {
      _outputXmlPath = outputXmlmlPath;
      _fsManager = [[PAFileSystemManager alloc]init];
      
      switch (sourceType) {
        case PATargetScriptSourceTypeDefaultXml:
        {
          _inputXmlPath = [_fsManager localDefaultXmlFilePath];
        }
          break;
        default:
          _inputXmlPath = inputXmlPath;
          break;
      }
      /// for sourceType other than PATargetScriptSourceTypeDefaultXml; inputXmlPath is must
      if (sourceType != PATargetScriptSourceTypeDefaultXml
          && !inputXmlPath) {
        self=  nil;
      }
      if (self) {
        _infoClass = infoClass;
        _targetCollectionClassManager = [[PATargetCollectionClassManager alloc]initWithScriptXml:_inputXmlPath];
        _totalTargetClasses = 0;
      }
      
    } else {
      self = nil;
    }
  }
  return self;
}

- (instancetype)initWithInputXml:(NSString *)inputXmlPath
                       outputXml:(NSString *)outputXmlmlPath
                          source:(PATargetScriptSourceType)sourceType {
  return [self initWithInfoClass:nil inputXml:inputXmlPath outputXml:outputXmlmlPath source:sourceType];
}


- (instancetype)initWithOutputXml:(NSString *)outputXmlmlPath {
  
  self = [super init];
  if (self) {
    _fsManager = [[PAFileSystemManager alloc]init];
    if (![_fsManager doesFileExist:outputXmlmlPath]) {
      self = nil;
    } else {
      _outputXmlPath = outputXmlmlPath;
    }
  }
  return self;
}
- (void)createEndpointInfoWithCompletion:(PAEnPointInfoManagerCompletionBlock)completionBlock {
  _completionBlock = completionBlock;
  [self getTargetClass];
}

- (void)createAllEndpointInfoWithCompletion:(PAEnPointInfoManagerCompletionBlock)completionBlock {
  _completionBlock = completionBlock;
  [self getAllTargetClasses];
}

- (NSString *)valueOfProperty:(NSString *)propertyName
                      inClass:(NSString *)className {
  if (![_fsManager doesFileExist:_outputXmlPath]) {
    return nil;
  }
  NSString *propertyValue = nil;
  @autoreleasepool {
    PAXmlReader *reader = [[PAXmlReader alloc] initWithXmlAtPath:_outputXmlPath];
    PAXmlAttribute *classNameAttribute = [[PAXmlAttribute alloc] initWithName:kPAEPClassName value:className];
    NSString *xmlStr = [reader xmlStringAtXPath:kPAEPXPath matchingAttributes:@[classNameAttribute]];
    NSDictionary *instanceDict = nil;
    if (xmlStr) {
      PAXmlParser *xmlParser = [[PAXmlParser alloc]init];
      instanceDict = [xmlParser dictionaryFromXmlString:xmlStr];
      if (instanceDict) {
        propertyValue = [self valueOfProperty:propertyName inDictionary:instanceDict];
      }
    }
  }
  
  return propertyValue;
}
- (NSArray *)valuesOfProperty:(NSString *)propertyName
                      inClass:(NSString *)className {
  NSMutableArray *propertyValueList = [[NSMutableArray alloc]init];
  if (![_fsManager doesFileExist:_outputXmlPath]) {
    return nil;
  }
  @autoreleasepool {
    PAXmlReader *reader = [[PAXmlReader alloc] initWithXmlAtPath:_outputXmlPath];
    PAXmlAttribute *classNameAttribute = [[PAXmlAttribute alloc] initWithName:kPAEPClassName value:className];
    NSArray *xmlStrList = [reader xmlStringsAtXPath:kPAEPXPath matchingAttributes:@[classNameAttribute]];
    NSDictionary *instanceDict = nil;
    NSString *propertyValue = nil;
    for (NSString *xmlStr in xmlStrList) {
      PAXmlParser *xmlParser = [[PAXmlParser alloc]init];
      instanceDict = [xmlParser dictionaryFromXmlString:xmlStr];
      if (instanceDict) {
        propertyValue = [self valueOfProperty:propertyName inDictionary:instanceDict];
        if (propertyValue) {
          [propertyValueList addObject:propertyValue];
        }
      }
    }
  }
  return [NSArray arrayWithArray:propertyValueList];
}

- (void)deleteEndpointInfo {
  PAFileSystemManager *fsManager = [[PAFileSystemManager alloc]init];
  
  if ([fsManager doesFileExist:_outputXmlPath]) {
    [fsManager deleteFile:_outputXmlPath];
  }
}

- (void)submitEndpointInfoWithUploadURL:(NSString*)uploadURL parameters:(NSDictionary*)parameters completion:(PAUploadManagerCompletionBlock)completionBlock {
  PAUploadManager *manager = [PAUploadManager uploadManagerGetInstance];
  [manager uploadFile:_outputXmlPath targetFolder:@"" uploadURL:uploadURL parameters:parameters callback:^(BOOL success, NSError *error, NSString * fileUploaded) {
    completionBlock(success,error, fileUploaded);
  }];
  ;
}


#pragma mark - Private methods

- (NSString *)valueOfProperty:(NSString *)property
                 inDictionary:(NSDictionary *)instanceDict {
  NSString *propertyValue = nil;
  NSMutableArray *propertyList = nil;
  id properties = instanceDict[kPAEPProperty];
  if ([properties isKindOfClass:[NSArray class]]) {
    propertyList = [(NSArray *)properties mutableCopy];
  } else if ([properties isKindOfClass:[NSDictionary class]]) {
    propertyList = [[NSMutableArray alloc] init];
    [propertyList addObject:(NSDictionary *)properties];
  }
  
  NSDictionary *propertyDict = [self propertyMatchingName:property inList:propertyList];
  if (propertyDict) {
    id value = propertyDict[kPAEPValue];
    if ([value isKindOfClass:[NSArray class]]) {
      NSArray *propertyArrayValue = (NSArray *)value;
      propertyValue = [propertyArrayValue componentsJoinedByString:@", "];
    } else if ([value isKindOfClass:[NSString class]]) {
      propertyValue = (NSString *)value;
    }
  }
  return propertyValue;
}

- (NSDictionary *)propertyMatchingName:(NSString *)name
                                inList:(NSArray *)propertyList {
  if (propertyList.count) {
    NSArray *filtered = [propertyList filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(_NAME == %@)", name]];
    if (filtered.count) {
      return filtered[0];
    }
  }
  
  return nil;
}

- (void)getTargetClass {
  if (_infoClass) {
    [_targetCollectionClassManager constructTargetCollectionClassesWithCompletion:^(BOOL success) {
      if (success) {
        [self runEndpointScript];
      } else {
        _completionBlock(false);
      }
    }];
  } else {
    _completionBlock(false);
  }
}

- (void)getAllTargetClasses {
  [_targetCollectionClassManager constructTargetCollectionClassesWithCompletion:^(BOOL success) {
    if (success) {
      [self runAllScripts];
    } else {
      _completionBlock(false);
    }
  }];
}

- (void)runEndpointScript {
  PATargetClass *targetCollectionClass = [_targetCollectionClassManager targetCollectionClassMatchingName:_infoClass];
  if (!targetCollectionClass) {
    _completionBlock(false);
  }
  _totalTargetClasses = 1;
  
  dispatch_queue_t queue = dispatch_queue_create("io.endpointinfo.asyncOpenDispatchQueue", DISPATCH_QUEUE_CONCURRENT);
  dispatch_async(queue, ^{
    @autoreleasepool {
      [targetCollectionClass executeScriptWithCallback:^(BOOL success) {
        if (_totalTargetClasses > 0) {
          --_totalTargetClasses;
        }
        
        if (success) {
          [self processTargetClassScriptExecution:targetCollectionClass];
        } else if (_totalTargetClasses <= 0 && !success) { /// no more target classs script outputs to process; and last script has failed
          PAFileSystemManager *fsManager = [[PAFileSystemManager alloc]init];
          dispatch_async(dispatch_get_main_queue(), ^{
            _completionBlock([fsManager doesFileExist:_outputXmlPath]);
          });
        }
      }];
    }
  });
  
  
  
}

- (void)runAllScripts {
  NSArray *allTargetClasses = [_targetCollectionClassManager allTargetCollectionClasses];
  if (!allTargetClasses.count) {
    _completionBlock(false);
  }
  _totalTargetClasses = allTargetClasses.count;
  dispatch_queue_t queue = dispatch_queue_create("io.endpointinfo.asyncOpenDispatchQueue", DISPATCH_QUEUE_CONCURRENT);
  dispatch_async(queue, ^{
    @autoreleasepool {
      @try{
        for (PATargetClass *targetClass in allTargetClasses) {
          [targetClass executeScriptWithCallback:^(BOOL success) {
            if (_totalTargetClasses > 0) {
              --_totalTargetClasses;
            }
            if (success) {
              [self processTargetClassesScriptExecution:targetClass];
            } else if (_totalTargetClasses <= 0 && !success) { /// no more target class script output to process; and last script has failed
              PAFileSystemManager *fsManager = [[PAFileSystemManager alloc]init];
              _completionBlock([fsManager doesFileExist:_outputXmlPath]);
            }
          }];
        }
      }
      @catch (NSException *exception)
      {
        //Handle this error
      }
    }
  });
  
}

/// Method to process individual script result on running specific target class script in default_mac.xml file
- (void)processTargetClassScriptExecution:(PATargetClass *)targetCollectionClass {
  NSString *xmlStr = targetCollectionClass.xmlString;
  if ([xmlStr isEmptyOrHasOnlyWhiteSpaces]) {
    return;
  }
  BOOL isXmlWriteSuccessful = false;
  
  PAXmlServiceManager *service = [[PAXmlServiceManager alloc]init];
  // delet old xml if present
  [service deleteXmlAtPath:_outputXmlPath];
  PAXmlFileProperties *properties = [[PAXmlFileProperties alloc]initWithVersion:kPAXmlVersionValue encoding:kPAXmlUtf8Encoding standalone:@(false) root:kPAScriptOutputXmlRootNodeName];
  if ([service createXmlAtPath:_outputXmlPath withProperties:properties]) {
    PAXmlWriter *xmlWriter = [[PAXmlWriter alloc]initWithXmlAtPath:_outputXmlPath];
    isXmlWriteSuccessful = [xmlWriter addChildXmlString:xmlStr parentXPath:kPAScriptOutputXmlRootNodeName];
  }
  
  /// script output is processed; so call completion block
  if (_totalTargetClasses == 0) {
    _completionBlock(isXmlWriteSuccessful);
  }
}
/// Method to process individual script result on running all scripts of default_mac.xml file
- (void)processTargetClassesScriptExecution:(PATargetClass *)targetCollectionClass {
  PAFileSystemManager *fsManager = [[PAFileSystemManager alloc]init];
  BOOL isFilePresent = [fsManager doesFileExist:_outputXmlPath];
  NSString *xmlStr = targetCollectionClass.xmlString;
  if ([xmlStr isEmptyOrHasOnlyWhiteSpaces]) {
    if (_totalTargetClasses == 0) {
      _completionBlock(isFilePresent);
    }
    return;
  }
  PAXmlServiceManager *service = [[PAXmlServiceManager alloc]init];
  
  /// on first script output; clear xml
  if (_totalTargetClasses == [_targetCollectionClassManager allTargetCollectionClasses].count-1) {
    [service deleteXmlAtPath:_outputXmlPath];
  }
  
  PAXmlFileProperties *properties = [[PAXmlFileProperties alloc]initWithVersion:kPAXmlVersionValue encoding:kPAXmlUtf8Encoding standalone:@(false) root:kPAScriptOutputXmlRootNodeName];
  
  if (!isFilePresent) {
    isFilePresent = [service createXmlAtPath:_outputXmlPath withProperties:properties];
  }
  BOOL isXmlWriteSuccessful = false;
  if (isFilePresent) {
    PAXmlWriter *xmlWriter = [[PAXmlWriter alloc]initWithXmlAtPath:_outputXmlPath];
    isXmlWriteSuccessful = [xmlWriter addChildXmlString:xmlStr parentXPath:kPAScriptOutputXmlRootNodeName];
  }
  /// last script output is processed; so call completion block
  if (_totalTargetClasses == 0) {
    _completionBlock(isXmlWriteSuccessful);
  }
}

@end
