//
//  PAEndPointConstants.h
//  PAEndPointInfoService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifndef PAEndPointConstants_h
#define PAEndPointConstants_h

typedef enum : NSUInteger {
  PATargetPropertyValueDataTypeNone,
  PATargetPropertyValueDataTypeScript,
  PATargetPropertyValueDataTypeKeyBinding,
  PATargetPropertyValueDataTypePlaceHolder
} PATargetPropertyValueDataType;


typedef enum : NSUInteger {
  PATargetScriptTypeNone,
  PATargetScriptTypeApple
} PATargetScriptType;

typedef enum : NSUInteger {
  PATargetScriptSourceTypeNone,
  PATargetScriptSourceTypeDefaultXml, /// use default_xml to generate the script output
  PATargetScriptSourceTypeOthers
} PATargetScriptSourceType;

#define kPATargetScriptTypeArray @"none", @"Apple", nil

/*
 PATargetScriptOutputFormatPlist1: where output is found inside inner dictionary as below:
 <array>
 <dict>
 <array>
 <dict></dict>
 </array>
 </dict>
 </array>
 
 */
typedef enum : NSUInteger {
  PATargetScriptOutputFormatNone,
  PATargetScriptOutputFormatPlist1,
  PATargetScriptOutputFormatString /// output is a string
} PATargetScriptOutputFormat;

typedef enum : NSUInteger {
  PAPropertyValueTypeNone,
  PAPropertyValueTypeScript,
  PAPropertyValueTypePlaceholder /// output is a string
} PAPropertyValueType;

#define kPAPropertyValueTypeArray @"none", @"script", @"placeholder", nil


extern NSString *const kPAEndPointTest;

#endif /* PAEndPointConstants_h */
