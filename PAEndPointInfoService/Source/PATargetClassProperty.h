//
//  PATargetClassProperty.h
//  PAEndPointInfoService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PATargetScript.h"

@interface PATargetClassProperty : NSObject

@property (nonatomic) NSString *name;
@property (nonatomic) NSString *type;

@property (nonatomic, assign) PAPropertyValueType valueType;

/*
 Value is either a placeholder enclosed within %
 Eg:
 <PROPERTY NAME="DisplayVersion" TYPE="string">
 <VALUE>%DISPLAYVERSION%</VALUE>
 </PROPERTY>
 
 OR script
 
 Eg:
 <PROPERTY NAME="ClockSpeed" TYPE="uint32">
 <VALUE><SCRIPT>ClockSpeed</SCRIPT></VALUE>
 </PROPERTY>
 
 */
@property (nonatomic) NSString *valuePlaceholder;
@property (nonatomic) PATargetScript *script;
@property (nonatomic) NSString *xmlString;

- (instancetype)initWithName:(NSString *)name
                        type:(NSString *)type
                   valueType:(PAPropertyValueType)valueType
                      script:(PATargetScript *)script
                 placeholder:(NSString *)valuePlaceholder;

@end
