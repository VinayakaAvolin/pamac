//
//  PADownloadManager.m
//  PADownloadService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PADownloadManager.h"
#import "ObjectiveCDM.h"

@interface PADownloadManager ()  <ObjectiveCDMUIDelegate, ObjectiveCDMDataDelegate> {
  NSMutableArray* pendingTasks;
}
@property(strong) ObjectiveCDM* objectiveCDM;

@end
@implementation PADownloadManager

- (instancetype)init
{
  self = [super init];
  if (self) {
    self.objectiveCDM = [ObjectiveCDM sharedInstance];
    self.objectiveCDM.uiDelegate = self;
    self.objectiveCDM.dataDelegate = self;
    pendingTasks = [[NSMutableArray alloc] init];
  }
  return self;
}

- (void)addFileToDownload:(PADownloadableFileInfo*)fileInfo {
  if (self.objectiveCDM.isDownloading) {
    [pendingTasks addObject:fileInfo.infoAsDictionary];
  }else {
    [self.objectiveCDM addBatch:@[fileInfo.infoAsDictionary]];
    [self.objectiveCDM startDownloadingCurrentBatch];
  }
}


- (void)addFileListToDownload:(NSArray*)filesInfo {
  if (self.objectiveCDM.isDownloading) {
    NSMutableArray* array = [NSMutableArray array];
    for (PADownloadableFileInfo* fileInfo in filesInfo) {
      [array addObject:[fileInfo infoAsDictionary]];
    }
    [pendingTasks addObjectsFromArray:array];
  }else {
    NSMutableArray* array = [NSMutableArray array];
    for (PADownloadableFileInfo* fileInfo in filesInfo) {
      [array addObject:[fileInfo infoAsDictionary]];
    }
    
    [self.objectiveCDM addBatch:array];
    [self.objectiveCDM startDownloadingCurrentBatch];
  }
}

- (void)cancelAllDownloads {
  [self.objectiveCDM suspendAllOnGoingDownloads];
  [self.objectiveCDM cancelAllOutStandingTasks];
}

- (PANetworkReachabilityStatus)networkReachabilityStatus{
  GCNetworkReachabilityStatus reachabilityStatus = self.objectiveCDM.networkReachabilityStatus;
  PANetworkReachabilityStatus nwReachStatus = PANetworkReachabilityStatusNotReachable;
  switch (reachabilityStatus) {
    case GCNetworkReachabilityStatusWWAN:
      nwReachStatus = PANetworkReachabilityStatusWWAN;
      break;
    case GCNetworkReachabilityStatusWiFi:
      nwReachStatus = PANetworkReachabilityStatusWiFi;
      break;
    default:
      break;
  }
  return nwReachStatus;
}

# pragma ObjectiveCDMDataDelegate

- (void) didFinishAllForDataDelegate {
  // handle whatever things that need to be done after the finish has been completed
}

- (void) didFinishDownloadTask:(ObjectiveCDMDownloadTask *)downloadInfo {
  // do anything with ObjectiveCDMDownloadTask instance
  if (self.uiDelegate && [self.uiDelegate respondsToSelector:@selector(didReachIndividualProgress:onDownloadTask:)]) {
    PADownloadTask *task = [[PADownloadTask alloc]initWithIdentifier:downloadInfo.identifier destination:downloadInfo.destination];
    [self.uiDelegate didReachIndividualProgress:1.0 onDownloadTask:task];
  }
}


# pragma ObjectiveCDMUIDelagate

- (void) didHitDownloadErrorOnTask:(ObjectiveCDMDownloadTask* ) downloadTask {
  if(self.uiDelegate && [self.uiDelegate respondsToSelector:@selector(didHitDownloadErrorOnTask:)]) {
    PADownloadTask *task = [[PADownloadTask alloc]initWithIdentifier:downloadTask.identifier destination:downloadTask.destination error:downloadTask.error];
    [self.uiDelegate didHitDownloadErrorOnTask:task];
  }
}

- (void) didReachProgress:(float)progress {
  //    float percentage = progress * 100.0;
  //    NSString* formattedPercentage = [NSString stringWithFormat:@"%.02f%%", percentage];
  //    NSLog(@"progress = %@",formattedPercentage);
}

- (void) didReachIndividualProgress:(float)progress onDownloadTask:(ObjectiveCDMDownloadTask* )downloadTask {
  float percentage = progress * 100.0;
  NSString* formattedPercentage = [NSString stringWithFormat:@"%.02f%%", percentage];
  if (self.uiDelegate && [self.uiDelegate respondsToSelector:@selector(didReachIndividualProgress:onDownloadTask:)]) {
    PADownloadTask *task = [[PADownloadTask alloc]initWithIdentifier:downloadTask.identifier destination:downloadTask.destination];
    [self.uiDelegate didReachIndividualProgress:progress onDownloadTask:task];
  }
}
- (void) didFinishAll {
  if (pendingTasks.count) {
    [self.objectiveCDM addBatch:pendingTasks];
    [self.objectiveCDM startDownloadingCurrentBatch];
    [pendingTasks removeAllObjects];
  }else {
    [self.uiDelegate didFinishAll];
  }
}
- (BOOL)verifyChecksum:(NSString*)checksum forFileAtPath:(NSString*)filePath andFileHashAlgorithm:(PAFileHashAlgorithm)filehashAlgorithm {
  NSString* retrievedChecksum = @"";
  if(filehashAlgorithm == PAFileHashAlgorithmMD5) {
    retrievedChecksum = [FileHash md5HashOfFileAtPath:filePath];
  } else if(filehashAlgorithm == PAFileHashAlgorithmSHA1) {
    retrievedChecksum = [FileHash sha1HashOfFileAtPath:filePath];
  } else if(filehashAlgorithm == PAFileHashAlgorithmSHA512) {
    retrievedChecksum = [FileHash sha512HashOfFileAtPath:filePath];
  }
  return [retrievedChecksum isEqualToString:checksum];
}

- (NSString *)checksumForFileAtPath:(NSString*)filePath fileHashAlgorithm:(PAFileHashAlgorithm)filehashAlgorithm {
  NSString* checksum = @"";
  if(filehashAlgorithm == PAFileHashAlgorithmMD5) {
    checksum = [FileHash md5HashOfFileAtPath:filePath];
  } else if(filehashAlgorithm == PAFileHashAlgorithmSHA1) {
    checksum = [FileHash sha1HashOfFileAtPath:filePath];
  } else if(filehashAlgorithm == PAFileHashAlgorithmSHA512) {
    checksum = [FileHash sha512HashOfFileAtPath:filePath];
  }
  return checksum;
}
@end
