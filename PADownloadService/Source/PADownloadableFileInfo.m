//
//  PADownloadableFileInfo.m
//  PADownloadService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PADownloadableFileInfo.h"

@implementation PADownloadableFileInfo

- (id)initWithUrl:(NSString*)url
      destination:(NSString*)destination
         fileSize:(long)fileSize
         checksum:(NSString*)checksum
       identifier:(NSString*)identifier {
  self = [super init];
  if (self) {
    self.downloadUrl = [NSURL URLWithString:url];
    self.destinationPath = destination;
    self.fileSize = fileSize;
    self.checksum = checksum;
    self.identifier = identifier;
  }
  return self;
}

- (NSDictionary*)infoAsDictionary {
  NSMutableDictionary *infoDict = [[NSMutableDictionary alloc]init];
  [infoDict setValue:self.downloadUrl forKey:@"url"];
  [infoDict setValue:self.destinationPath forKey:@"destination"];
  [infoDict setValue:[NSNumber numberWithLongLong:self.fileSize] forKey:@"fileSize"];
  [infoDict setValue:self.checksum forKey:@"checksum"];
  [infoDict setValue:self.identifier forKey:@"identifier"];
  return [NSDictionary dictionaryWithDictionary:infoDict];
}

- (NSString *)description
{
  return [NSString stringWithFormat:@"Description = %@\n\n", self.infoAsDictionary];
}
@end
