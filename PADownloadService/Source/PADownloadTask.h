//
//  PADownloadTask.h
//  PADownloadService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PADownloadTask : NSObject

@property NSString *downloadedFilePath;
@property NSString *taskId;
@property NSError *error;

- (instancetype)initWithIdentifier:(NSString *)identifier
                       destination:(NSString *)destination
                             error:(NSError *)error;
- (instancetype)initWithIdentifier:(NSString *)identifier
                       destination:(NSString *)destination;

@end
