//
//  PADownloadTask.m
//  PADownloadService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PADownloadTask.h"

@implementation PADownloadTask

- (instancetype)initWithIdentifier:(NSString *)identifier
                       destination:(NSString *)destination {
  return [self initWithIdentifier:identifier destination:destination error:nil];;
}

- (instancetype)initWithIdentifier:(NSString *)identifier
                       destination:(NSString *)destination
                             error:(NSError *)error {
  self = [super init];
  if (self) {
    _taskId = identifier;
    _downloadedFilePath = destination;
    _error = error;
  }
  return self;
}

@end
