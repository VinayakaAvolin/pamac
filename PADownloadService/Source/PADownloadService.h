//
//  PADownloadService.h
//  PADownloadService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Cocoa/Cocoa.h>

//! Project version number for PADownloadService.
FOUNDATION_EXPORT double PADownloadServiceVersionNumber;

//! Project version string for PADownloadService.
FOUNDATION_EXPORT const unsigned char PADownloadServiceVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <PADownloadService/PublicHeader.h>

#import <PADownloadService/PADownloadManager.h>
#import <PADownloadService/PADownloadTask.h>
#import <PADownloadService/PADownloadableFileInfo.h>
