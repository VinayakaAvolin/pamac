//
//  PADownloadManager.h
//  PADownloadService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PADownloadTask.h"
#import "PADownloadableFileInfo.h"

typedef enum {
  PAFileHashAlgorithmMD5 = 1,
  PAFileHashAlgorithmSHA1 = 2,
  PAFileHashAlgorithmSHA512 = 3
} PAFileHashAlgorithm;

typedef enum : unsigned char
{
  PANetworkReachabilityStatusNotReachable,
  PANetworkReachabilityStatusWWAN,
  PANetworkReachabilityStatusWiFi
} PANetworkReachabilityStatus;


@protocol PADownloadManagerUIDelegate <NSObject>

@required
- (void) didFinishAll;

@optional
- (void) didReachIndividualProgress:(float)progress onDownloadTask:(PADownloadTask *) task;

- (void)didHitDownloadErrorOnTask:(PADownloadTask*)task;
@end

@interface PADownloadManager : NSObject

@property(nonatomic, retain) id<PADownloadManagerUIDelegate> uiDelegate;

- (void)addFileToDownload:(PADownloadableFileInfo *)fileInfo;

- (void)addFileListToDownload:(NSArray*)filesInfo;

- (void)cancelAllDownloads;

- (PANetworkReachabilityStatus)networkReachabilityStatus;

- (BOOL)verifyChecksum:(NSString*)checksum forFileAtPath:(NSString*)filePath andFileHashAlgorithm:(PAFileHashAlgorithm)filehashAlgorithm;

- (NSString *)checksumForFileAtPath:(NSString*)filePath fileHashAlgorithm:(PAFileHashAlgorithm)filehashAlgorithm;
@end

