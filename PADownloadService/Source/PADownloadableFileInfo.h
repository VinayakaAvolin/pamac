//
//  PADownloadableFileInfo.h
//  PADownloadService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PADownloadableFileInfo : NSObject

@property NSURL* downloadUrl;
@property NSString* destinationPath;
@property long fileSize;
@property NSString* checksum;
@property NSString* identifier;

- (id)initWithUrl:(NSString*)url
      destination:(NSString*)destination
         fileSize:(long)fileSize
         checksum:(NSString*)checksum
       identifier:(NSString*)identifier;

- (NSDictionary*)infoAsDictionary;

@end
