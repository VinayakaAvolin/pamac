//
//  PACabServiceManager.m
//  PACabService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <PAFileSystemService/PAFileSystemService.h>
#import "PACabServiceManager.h"
#import "NSDate+Additions.h"
#include "mspack.h"
#include "cabdecrypt.h"

NSString *const kPACSEncryptedCabName = @"SupportActio_";
NSString *const kPACSDecryptCabName = @"Decrypted_SupportActio.cab";

@interface PACabServiceManager () {
  NSString *_cabFilePath;
  NSString *_cabExtractDestinationPath;
  NSString *_tempCabExtractDestinationPath;
  BOOL _isEncrypted;
  PAFileSystemManager *_fsManager;
}

@end

@implementation PACabServiceManager

- (instancetype)init
{
  self = [super init];
  if (self) {
    _fsManager = [[PAFileSystemManager alloc]init];
  }
  return self;
}

- (BOOL)extractCabAtPath:(NSString *)cabFilePath
     toDestinationFolder:(NSString *)destinationFolderPath
                   error:(NSError **)error {
  
  if (!cabFilePath || !destinationFolderPath) {
    return false;
  }
  _cabFilePath = cabFilePath;
  _cabExtractDestinationPath = destinationFolderPath;
  _tempCabExtractDestinationPath = [self temporaryCabExtractPath];
  
  [self deleteCabExtractDestinationPath]; // remove composite folder
  
  BOOL isExtracted = [self extractCabAtPath:_cabFilePath toDestinationFolder:_tempCabExtractDestinationPath];
  /// first extract cab into a temporary folder ; then check if cab is encrypted or not; if encrypted then first decrypt the first extracted cab and then extract the decrypted cab into "_cabExtractDestinationPath"
  if (isExtracted) {
    if ([self isCabEncrypted]) {
      NSString *decryptedCabPath = [self decryptCab];
      if (decryptedCabPath) {
        isExtracted = [self extractCabAtPath:decryptedCabPath toDestinationFolder:_cabExtractDestinationPath];
      }
    } else {
      // move cab contents inside _cabExtractDestinationPath
      isExtracted = [_fsManager renameDirectory:_tempCabExtractDestinationPath newFileName:[_cabExtractDestinationPath lastPathComponent]];
    }
  }
  [self cleanup];
  
  return isExtracted;
}

- (BOOL)isCabEncrypted {
  NSArray *cabContents = [_fsManager contentsOfDirectory:_tempCabExtractDestinationPath];
  if ([cabContents containsObject:kPACSEncryptedCabName]) {
    return true;
  }
  return false;
}

- (void)deleteCabExtractDestinationPath {
  if([_fsManager doesDirectoryExist:_cabExtractDestinationPath]) {
    [_fsManager deleteDirectory:_cabExtractDestinationPath];
  }
}
// if cab is encrypted then returns new path by appending time stamp string to "_cabExtractDestinationPath"; otherwise returns "_cabExtractDestinationPath"
- (NSString *)temporaryCabExtractPath {
  NSString *tempCabExtractFolderPath = [NSString stringWithFormat:@"%@-%@",_cabExtractDestinationPath, [[NSDate systemDate] dateStringWithFormat:Format7]];
  return tempCabExtractFolderPath;
}
// if cab is encrypted then returns new path by appending time stamp string to "_cabExtractDestinationPath"; otherwise returns "_cabExtractDestinationPath"
- (NSString *)encryptedCabPath {
  NSString *encryptedCabPath = _tempCabExtractDestinationPath;
  encryptedCabPath = [encryptedCabPath stringByAppendingPathComponent:kPACSEncryptedCabName];
  if ([_fsManager doesFileExist:encryptedCabPath]) {
    return encryptedCabPath;
  }
  return nil;
}
// Returns decrypt cab path
- (NSString *)decryptedCabPath {
  NSString *decryptCabPath = _tempCabExtractDestinationPath;
  decryptCabPath = [decryptCabPath stringByAppendingPathComponent:kPACSDecryptCabName];
  return decryptCabPath;
}
- (NSString *)decryptCab {
  NSString *encryptedCabPath = [self encryptedCabPath];
  NSString *decryptedCabPath = [self decryptedCabPath];
  BOOL isDecrypted = false;
  if (encryptedCabPath) {
    isDecrypted = decryptCabFile([[self encryptedCabPath] cStringUsingEncoding:NSUTF8StringEncoding], [decryptedCabPath cStringUsingEncoding:NSUTF8StringEncoding]);
  }
  if (isDecrypted) {
    return decryptedCabPath;
  }
  
  return nil;
}

- (void)cleanup {
  [_fsManager deleteDirectory:_tempCabExtractDestinationPath];
}
- (BOOL)extractCabAtPath:(NSString *)cabFilePath
     toDestinationFolder:(NSString *)destinationFolderPath {
  if (![_fsManager doesFileExist:cabFilePath]) {
    return false;
  }
  if (![_fsManager doesDirectoryExist:destinationFolderPath] &&
      ![_fsManager createDirectoryAtPath:destinationFolderPath withIntermediateDirectories:NO attributes:nil error:nil]) {
    return false;
  }
  
  const char *cabFilePathCstr = [cabFilePath UTF8String];
  const char *destinationPathCStr = [destinationFolderPath UTF8String];
  struct mscab_decompressor *cabd;
  struct mscabd_cabinet *cab;
  struct mscabd_file *file;
  int statusCode = -1;
  
  MSPACK_SYS_SELFTEST(statusCode);
  if (statusCode != MSPACK_ERR_OK) {
    return false;
  }
  statusCode = -1;
  if ((cabd = mspack_create_cab_decompressor(NULL))) {
    if ((cab = cabd->open(cabd, cabFilePathCstr))) {
      for (file = cab->files; file; file = file->next) {
        char outputFile[512];
        sprintf(outputFile, "%s/%s", destinationPathCStr, file->filename);
        statusCode = cabd->extract(cabd, file, outputFile);
      }
      cabd->close(cabd, cab);
    }
    mspack_destroy_cab_decompressor(cabd);
  }
  if (statusCode == MSPACK_ERR_OK) {
    return true;
  }
  
  return false;
}

@end
