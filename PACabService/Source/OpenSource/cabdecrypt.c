//
//  cabdecrypt.c
//  CabDecryptor
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>
#include <errno.h>

#include "cabdecrypt.h"

#define CAB_INT_PWD_LEN  4
#define CAB_KEY_LEN  38
#define DWORD int
const char  CAB_INT_PWD[]   = "sprt";

typedef struct _tgECabHolder{
  char  type[CAB_INT_PWD_LEN];  //4+sizeof(DWORD) * 2 + 38
  int startoffset;
  int length;
  char  key[CAB_KEY_LEN];
  char  *cabAsBytes;
} ECabHolder;

bool XorEncrypt(char *buf,unsigned long bufLen,const char *pkey,const DWORD keyLen);


bool decryptCabFile(const char* srcPath, const char* dstPath) {
  
  bool rVal = true;
  FILE *fpEncrypted  = NULL;
  FILE *fpDecrypted  = NULL;
  DWORD nIoBytes = 0;
  DWORD startoffset  = 0;
  
  fpEncrypted = fopen(srcPath, "rb");
  if (fpEncrypted == NULL) {
    rVal = false;
    fprintf(stderr, "Error: Source file not found Error = %s", strerror(errno));
    goto cleanup;
  }
  
  ECabHolder stECab;
  memset(&stECab,0,sizeof(stECab));
  
  //Read header
  startoffset = sizeof(char) * CAB_INT_PWD_LEN + sizeof(DWORD) * 2 + sizeof(char) * CAB_KEY_LEN;
  nIoBytes = (DWORD)fread((void *)&stECab,1,startoffset,fpEncrypted);
  
  //Read sprt signature
  char bufL[CAB_INT_PWD_LEN+1];
  memset(bufL,0,CAB_INT_PWD_LEN+1);
  strncpy(bufL,stECab.type,CAB_INT_PWD_LEN);
  
  if (strcmp(bufL,CAB_INT_PWD) != 0 ) {
    rVal = false;
    fprintf(stderr, "Error: File signature on cab file is missing");
    goto cleanup;
  }
  
  stECab.cabAsBytes = (char *)malloc(sizeof(char) * stECab.length);
  memset(stECab.cabAsBytes,0,stECab.length);
  nIoBytes = (DWORD)fread((void *)stECab.cabAsBytes,sizeof(char),stECab.length,fpEncrypted);
  fclose(fpEncrypted);
  fpEncrypted = NULL;
  
  if (nIoBytes != stECab.length)
  {
    rVal = false;
    fprintf(stderr, "Error: Corrupt cab file");
    goto cleanup;
  }
  
  XorEncrypt(stECab.cabAsBytes,stECab.length,(const char *)stECab.key,CAB_KEY_LEN);
  
  fpDecrypted = fopen(dstPath, "wb");
  if( fpDecrypted == NULL )
  {
    rVal = false;
    fprintf(stderr, "Error: Unknown");
    goto cleanup;
  }
  nIoBytes  = (DWORD)fwrite((const void *)(stECab.cabAsBytes), 1, stECab.length, fpDecrypted);
  
  if (nIoBytes  != stECab.length)
  {
    rVal = false;
    fprintf(stderr, "Error: Unknown");
    goto cleanup;
  }
cleanup:
  if (fpEncrypted)
  {
    fclose(fpEncrypted);
    fpEncrypted = NULL;
  }
  
  if(fpDecrypted)
  {
    fclose(fpDecrypted);
    fpDecrypted = NULL;
  }
  if (stECab.cabAsBytes)
  {
    free(stECab.cabAsBytes);
    stECab.cabAsBytes = NULL;
  }
  return rVal;
}

bool XorEncrypt(char *buf,unsigned long bufLen,const char *pkey,const DWORD keyLen) {
  if (!buf || bufLen == 0 || !pkey || keyLen == 0)
  {
    return false;
  }
  
  DWORD i = 0, j = 0;
  for (i = 0;i < bufLen; i++)
  {
    j = i % keyLen;
    buf[i] ^= pkey[j];
  }
  return true;
}
