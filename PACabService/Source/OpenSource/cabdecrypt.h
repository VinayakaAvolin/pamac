//
//  cabdecrypt.h
//  CabDecryptor
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>
#include <errno.h>

#ifndef _CABDECRYPT_H_
#define _CABDECRYPT_H_

bool decryptCabFile(const char* srcPath, const char* dstPath);

#endif

