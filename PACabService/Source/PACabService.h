//
//  PACabService.h
//  PACabService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Cocoa/Cocoa.h>

//! Project version number for PACabService.
FOUNDATION_EXPORT double PACabServiceVersionNumber;

//! Project version string for PACabService.
FOUNDATION_EXPORT const unsigned char PACabServiceVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <PACabService/PublicHeader.h>

#import <PACabService/PACabServiceManager.h>
