//
//  PACabServiceManager.h
//  PACabService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PACabServiceManager : NSObject

/// extracts cab file content
/// The destinationFolderPath is the folder where cab contents are extracted
- (BOOL)extractCabAtPath:(NSString *)cabFilePath
     toDestinationFolder:(NSString *)destinationFolderPath
                   error:(NSError **)error;

@end
