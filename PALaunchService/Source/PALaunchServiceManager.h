//
//  PALaunchServiceManager.h
//  PALaunchService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AppKit/AppKit.h>
#import "PALaunchServiceConstants.h"

@interface PALaunchServiceManager : NSObject

/// Method to launch an app with or without the arguments
+ (BOOL)runCommand:(NSString *)commandWithOrWithoutTheArgs;

/// Method to check if the app associated with the command is already running
+ (BOOL)isCommandRunning:(NSString *)commandWithOrWithoutTheArgs;

+ (BOOL)exitCommand:(NSString *)command;

@end
