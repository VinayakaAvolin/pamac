//
//  PALaunchServiceConstants.h
//  PALaunchService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifndef PALaunchServiceConstants_h
#define PALaunchServiceConstants_h

typedef enum {
  PALaunchOptionNone,
  PALaunchOptionLaunch,
  PALaunchOptionReLaunch, // quit and launch
  PALaunchOptionLaunchOrNotifiy, // launch and notify
  PALaunchOptionNotifiy, // only notify
} PALaunchOption;

extern NSString *const kPALSCommandFinder;
extern NSString *const kPALSCommandNotificationTray;
extern NSString *const kPALSCommandProactiveAssist;
extern NSString *const kPALSCommandAlertTray;
extern NSString *const kPALSCommandResidentAgent;

/// Used in real time alerts
// notification name constants
extern NSString *const kPANotificationNameNewRealTimeAlert;
extern NSString *const kPANotificationNameProactiveAssistTerminated;
// notification object name constants
extern NSString *const kPARealTimeAlertObjectName;
extern NSString *const kPAResidentAgentObjectName;
// notification info keys
extern NSString *const kPAAlertTypeRealTimeAlert;
extern NSString *const kPAAlertTypeNormalAlert;
extern NSString *const kPARealTimeAlertArgumentsKey;

#endif /* PALaunchServiceConstants_h */
