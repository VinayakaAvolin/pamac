//
//  PALaunchService.h
//  PALaunchService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Cocoa/Cocoa.h>

//! Project version number for PALaunchService.
FOUNDATION_EXPORT double PALaunchServiceVersionNumber;

//! Project version string for PALaunchService.
FOUNDATION_EXPORT const unsigned char PALaunchServiceVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <PALaunchService/PublicHeader.h>

#import <PALaunchService/PALaunchServiceManager.h>
