//
//  PALaunchServiceConstants.m
//  PALaunchService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PALaunchServiceConstants.h"

NSString *const kPALSCommandFinder = @"finder";
NSString *const kPALSCommandNotificationTray = @"notificationTray";
NSString *const kPALSCommandProactiveAssist = @"proactiveAssistUI";
NSString *const kPALSCommandAlertTray = @"alertTray";
NSString *const kPALSCommandResidentAgent = @"residentAgent";

NSString *const kPANotificationNameNewRealTimeAlert = @"PANotificationNameNewRealTimeAlert";
NSString *const kPANotificationNameProactiveAssistTerminated = @"PAAppTerminated";
NSString *const kPARealTimeAlertObjectName = @"PARealTimeAlertObjectName";
NSString *const kPAResidentAgentObjectName = @"PAResidentAgentObjectName";

NSString *const kPAAlertTypeRealTimeAlert = @"-realTimeAlert";
NSString *const kPAAlertTypeNormalAlert = @"-alert";
NSString *const kPARealTimeAlertArgumentsKey = @"arguments";
