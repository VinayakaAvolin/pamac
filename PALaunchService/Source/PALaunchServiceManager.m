//
//  PALaunchServiceManager.m
//  PALaunchService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <PAConfigurationService/PAConfigurationService.h>
#import <PAFileSystemService/PAFileSystemService.h>
#import "PALaunchServiceManager.h"

NSString *const kPASelfHealTrigger = @"/selfhealtrigger";

@interface PALaunchServiceManager () {
  NSString *_command;
  NSArray *_arguments;
  PACommandInfo *_commandInfo;
}

@property(readonly, assign) PALaunchOption launchOption;
@end

@implementation PALaunchServiceManager

- (instancetype)initWithCommand:(NSString *)command
                           args:(NSArray *)args
{
  self = [super init];
  if (self) {
    if (!command) {
      self = nil;
    } else {
      _command = command;
      _arguments = [NSArray arrayWithArray:args];
      [self readCommandInfo];
      _launchOption = [self launchOptionForCommand];
    }
  }
  return self;
}

+ (BOOL)runCommand:(NSString *)commandWithOrWithoutTheArgs {
  NSMutableArray *argsArray = [[commandWithOrWithoutTheArgs componentsSeparatedByString:@" "] mutableCopy];
  NSString *command = nil;
  if (argsArray.count >= 1) {
    // read command; and remove it from the array
    
    //Replace macro value [%SPACE%] to " " on each argument
    for(int i=0;i<argsArray.count;i++ )
    {
      argsArray[i] = [argsArray[i] stringByReplacingOccurrencesOfString:@"[%SPACE%]" withString:@" "];
    }
    
    command = argsArray[0];
    [argsArray removeObjectAtIndex:0];
  }
  
  if (command) {
    return [PALaunchServiceManager runCommand:command withArguments:argsArray];
  }
  return false;
}

+ (BOOL)isCommandRunning:(NSString *)commandWithOrWithoutTheArgs {
  NSMutableArray *argsArray = [[commandWithOrWithoutTheArgs componentsSeparatedByString:@" "] mutableCopy];
  NSString *command = nil;
  if (argsArray.count >= 1) {
    // read command; and remove it from the array
    command = argsArray[0];
    [argsArray removeObjectAtIndex:0];
  }
  BOOL isLaunched = false;
  
  if (command) {
    PALaunchServiceManager *launchService = [[PALaunchServiceManager alloc] initWithCommand:command args:argsArray];
    
    isLaunched = [launchService isAppWithBundleIdRunning:[launchService appBundleId]];
  }
  return isLaunched;
}

+ (BOOL)exitCommand:(NSString *)command {
  BOOL isAppClosed = false;
  
  if (command) {
    PALaunchServiceManager *launchService = [[PALaunchServiceManager alloc] initWithCommand:command args:nil];
    isAppClosed = ![launchService isAppWithBundleIdRunning:[launchService appBundleId]];
    if (!isAppClosed) {
      isAppClosed = [launchService quitAppWithBundleId:[launchService appBundleId]];
    }
  }
  return isAppClosed;
}
#pragma mark - Private methods

+ (BOOL)runCommand:(NSString *)command withArguments:(NSArray *)args {
  BOOL isLaunched = false;
  PALaunchServiceManager *launchService = [[PALaunchServiceManager alloc] initWithCommand:command args:args];
  
  switch (launchService.launchOption) {
    case PALaunchOptionLaunch:
      isLaunched = [launchService launchApp];
      break;
    case PALaunchOptionReLaunch:
      isLaunched = [launchService relaunchApp];
      break;
    case PALaunchOptionLaunchOrNotifiy:
      isLaunched = [launchService launchOrNotifyApp];
      break;
    case PALaunchOptionNotifiy:
      isLaunched = [launchService notifyApp];
      break;
    default:
      break;
  }
  return isLaunched;
}

- (void)readCommandInfo {
  PAAppConfiguration *appConfiguration = [PAConfigurationManager appConfigurations];
  _commandInfo = [appConfiguration commandInfoMatchingCommand:_command];
  // if command info not found in "AppConfigurations.plist" then command could be a path
  if (!_commandInfo) {
    _commandInfo = [[PACommandInfo alloc] initWithCommandPath:_command];
  }
}

- (PALaunchOption)launchOptionForCommand {
  PALaunchOption launchOption = (PALaunchOption)_commandInfo.launchOption;
  return launchOption;
}

- (NSString *)appBundleId {
  NSString *app = _commandInfo.appBundleId;
  return app;
}
- (NSString *)appName {
  NSString *app = _commandInfo.appName;
  return app;
}

- (BOOL)launchApp {
  if (_arguments.count) {
    // check if selfhealtrigger argument exists. If "Yes" then WorkspaceLaunch sets "NSWorkspaceLaunchNewInstance" option else NSWorkspaceLaunchDefault
    return [self launchAppWithArguments:[self appName] WorkspaceLaunchOption:([_arguments containsObject:kPASelfHealTrigger] ? NSWorkspaceLaunchNewInstance : NSWorkspaceLaunchDefault)];
  }
  return [self launchAppWithName:[self appName]];
}

- (BOOL)launchAppWithName:(NSString *)appName {
  return [[NSWorkspace sharedWorkspace] launchApplication:appName];
}
- (BOOL)launchAppWithArguments:(NSString *)appName WorkspaceLaunchOption:(NSWorkspaceLaunchOptions)WorkspaceLaunchOptionType {
  NSWorkspace *workspace = [NSWorkspace sharedWorkspace];
  NSString *appPath = _commandInfo.path;
  if (!appPath) {
    appPath = [workspace fullPathForApplication:appName];
  } else {
    PAFileSystemManager *fsManager = [[PAFileSystemManager alloc]init];
    appPath = [fsManager stringByExpandingPath:appPath];
  }
  NSURL *url = [NSURL fileURLWithPath:appPath];
  //Handle url==nil
  NSError *error = nil;
  NSArray *arguments = [NSArray arrayWithArray:_arguments];

  return ([workspace launchApplicationAtURL:url options:WorkspaceLaunchOptionType configuration:[NSDictionary dictionaryWithObject:arguments forKey:NSWorkspaceLaunchConfigurationArguments] error:&error] != nil);
}

- (BOOL)relaunchApp {
  BOOL isAppQuit = [self quitAppWithBundleId:[self appBundleId]];
  [self performSelector:@selector(launchApp) withObject:self afterDelay:1.0];
  return isAppQuit;
}

- (BOOL)launchOrNotifyApp {
  BOOL isAppRunning = [self isAppWithBundleIdRunning:[self appBundleId]];
  if (!isAppRunning) {
    isAppRunning = [self launchApp];
  } else {
    // notify if there are arguments; else do not
    if (_arguments.count) {
      NSDictionary *argsDict = @{kPARealTimeAlertArgumentsKey: _arguments};
      // notify
      [[NSDistributedNotificationCenter defaultCenter] postNotificationName:kPANotificationNameNewRealTimeAlert object:kPARealTimeAlertObjectName userInfo:argsDict deliverImmediately:true];
    }
  }
  return isAppRunning;
}
- (BOOL)notifyApp {
  BOOL isAppRunning = [self isAppWithBundleIdRunning:[self appBundleId]];
  if (isAppRunning) {
    // notify
    [[NSDistributedNotificationCenter defaultCenter] postNotificationName:kPANotificationNameProactiveAssistTerminated object:kPAResidentAgentObjectName userInfo:nil deliverImmediately:true];
  }
  return isAppRunning;
}
- (BOOL)isAppWithBundleIdRunning:(NSString *)bundleId {
  NSArray *matchingApps = [NSRunningApplication runningApplicationsWithBundleIdentifier:bundleId];
  if (matchingApps.count) {
    return true;
  }
  return false;
}
- (BOOL)quitAppWithBundleId:(NSString *)bundleId {
  NSArray *matchingApps = [NSRunningApplication runningApplicationsWithBundleIdentifier:bundleId];
  if (!matchingApps.count) {
    return true;
  }
  NSRunningApplication *app = matchingApps[0];
  return [app terminate];
}

@end
