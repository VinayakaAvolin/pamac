//
//  PAMainViewController.h
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <WebKit/WebKit.h>
#import <PACoreServices/PACoreServices.h>
#import <PAJSBridgeService/PAJSBridgeService.h>

@interface PAMainViewController : NSViewController <WebUIDelegate,WebFrameLoadDelegate>
@property (weak) IBOutlet WebView *webView;

@property (strong) PAJSBridgeController* jsBridgeController;
@property (strong) NSWindowController * modalWindowController;
@property (strong) NSWindow *modalWebWindow;
@property (strong) WebView *webViewInModalWindow;
@property (strong) NSString *webPageLink;
@property (strong) NSArray *arguments;

/// Returns PAMainViewController instance
+ (instancetype)mainViewController;

-(void) loadWebPage:(NSURLRequest*)request;

- (void)setAppType:(int)appType;

- (void)setCommandLineArguments:(NSArray*)arguments;

@end

