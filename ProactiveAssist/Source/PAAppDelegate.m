//
//  PAAppDelegate.m
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <PACoreServices/PACoreServices.h>
#import "PAAppDelegate.h"
#import "PAMainViewController.h"
#import "NSStoryboard+Additions.h"
NSString *const kPAMainWindowControllerId = @"MainWindowController";
NSString *const kMainHtmlFileName = @"sample";
NSString *const kHtmlFileExtension = @"html";

@interface PAAppDelegate () {
  NSWindowController *_mainWindowController;
}
@end

@implementation PAAppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
  // Insert code here to initialize your application
  
  //Create instance UUID and store in NSUserDefaults
  if([[NSUserDefaults standardUserDefaults] objectForKey:@"instanceUUID"] == nil) {
    NSString *_instanceUUID = [[NSUUID UUID] UUIDString];
    [[NSUserDefaults standardUserDefaults] setValue:_instanceUUID forKey:@"instanceUUID"];
    [[NSUserDefaults standardUserDefaults] synchronize];
  }
  
  NSMutableArray *args = [[[NSProcessInfo processInfo] arguments] mutableCopy];
  if (args.count > 1) {
    [args removeObjectAtIndex:0];
  }
  [self setMainWindowController: args];
}


- (void)applicationWillTerminate:(NSNotification *)aNotification {
  // Insert code here to tear down your application
  [self notifyResidentAgentOnAppTerminate];
  
  //Remove instance UUID
  if([[NSUserDefaults standardUserDefaults] objectForKey:@"instanceUUID"] != nil) {
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"instanceUUID"];
    [[NSUserDefaults standardUserDefaults] synchronize];
  }
}

//- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)theApplication {
//    return NO;
//}

#pragma mark - Private methods

- (void)notifyResidentAgentOnAppTerminate {
  // notify notification tray app
  NSString *command = [NSString stringWithFormat:@"%@",kPALSCommandResidentAgent];
  BOOL isSuccess = [PALaunchServiceManager runCommand:command];
  if (isSuccess) {
    [[PALogger sharedLoggerForClient:PALogClientAlertTray] debug:@"Notified ResidentAgent On PA App Terminate"];
  }
}

- (void)setMainWindowController:(NSArray*)arguments {
  _mainWindowController = [self mainWindowController];
  PAMainViewController *mainvc = [PAMainViewController mainViewController];
  mainvc.webPageLink = @"testpage/testpage";
  _mainWindowController.contentViewController = mainvc;
  [mainvc setAppType:PAMainApp];
  NSRect rect =  [mainvc.jsBridgeController defaultWindowFrame];
  [_mainWindowController.window setFrame:rect display:NO];
  [_mainWindowController.window center];
  [_mainWindowController.window makeKeyAndOrderFront:self];
  NSString *filePath = [self testHtmlPath];//[[NSBundle mainBundle] pathForResource:kMainHtmlFileName ofType:kHtmlFileExtension];
  NSURL *url = [NSURL fileURLWithPath:filePath];
  [mainvc setCommandLineArguments:arguments];
  NSURLRequest *request = [NSURLRequest requestWithURL:url];
  [mainvc loadWebPage:request];
}

- (NSWindowController *)mainWindowController {
  NSStoryboard *storyboard = [NSStoryboard mainAppStoryboard];
  NSWindowController *mainWindowVc = [storyboard instantiateControllerWithIdentifier:kPAMainWindowControllerId];
  return mainWindowVc;
}

- (NSString *)testHtmlPath {
  PAFileSystemManager *fsManager = [[PAFileSystemManager alloc]init];
  NSString *folderPath = [fsManager agentCoreFolderPath];
  folderPath = [folderPath stringByAppendingPathComponent:@"bootstrapper.html"];
  //    NSString *folderPath = @"/Volumes/Backup/Projects/Git/Ayuda/ProactiveAssist/HTML/bootstrapper.html";
  folderPath = [folderPath stringByExpandingTildeInPath];
  return folderPath;
}

@end
