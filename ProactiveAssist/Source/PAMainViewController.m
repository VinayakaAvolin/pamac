//
//  PAMainViewController.m
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PAMainViewController.h"
#import "NSStoryboard+Additions.h"
#import "PAJSConstants.h"
#import "PAJSResult.h"
#import "PAJSBridgeController.h"

@interface PAMainViewController (Private) <WKNavigationDelegate, WKScriptMessageHandler,WebPolicyDelegate, NSWindowDelegate>

@end

@implementation PAMainViewController
+ (instancetype)mainViewController {
  NSStoryboard *storyboard = [NSStoryboard mainAppStoryboard];
  PAMainViewController *mainVc = [storyboard instantiateControllerWithIdentifier:NSStringFromClass([PAMainViewController class])];
  return mainVc;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  self.jsBridgeController = [[PAJSBridgeController alloc] init];
  // Do any additional setup after loading the view.
  //    [self loadWebView];
}

- (void)setRepresentedObject:(id)representedObject {
  [super setRepresentedObject:representedObject];
  
  // Update the view, if already loaded.
}

-(void) loadWebPage:(NSURLRequest*)request {
  [self setupJavascriptHandlers];
  //    NSString *filePath = [[NSBundle mainBundle] pathForResource:kMainHtmlFileName ofType:kHtmlFileExtension];
  //    NSURL *url = [NSURL fileURLWithPath:filePath];
  //    NSURLRequest *request = [NSURLRequest requestWithURL:url];
  
  [self.webView.mainFrame loadRequest:request];
}

- (void)setupJavascriptHandlers {
  
  self.jsBridgeController.webrView = self.webView;
  self.webView.UIDelegate = self;
  self.webView.frameLoadDelegate = self;
  
  [[self.webView preferences] setJavaScriptEnabled:YES];
  [[self.webView preferences] setJavaScriptCanOpenWindowsAutomatically:YES];
}

- (void)webView:(WebView *)sender printFrameView:(WebFrameView *)frameView
{
  NSPrintInfo *printInfo = [NSPrintInfo sharedPrintInfo];
  NSPrintOperation *printOperation = [frameView printOperationWithPrintInfo:printInfo];
  // Open the print dialog
  [printOperation runOperation];
}

- (NSArray *)webView:(WebView *)sender contextMenuItemsForElement:(NSDictionary *)element defaultMenuItems:(NSArray *)defaultMenuItems;
{
  return defaultMenuItems;//@[]; //update oncontextmenu = false in agentcore.shell.js
}

- (NSUInteger)webView:(WebView *)webView dragDestinationActionMaskForDraggingInfo:(id <NSDraggingInfo>)draggingInfo {
  return WebDragSourceActionNone;
}

- (void)webView:(WebView *)webView
didClearWindowObject:(WebScriptObject *)windowObject
       forFrame:(WebFrame *)frame;
{
  id domWindow = [self.webView windowScriptObject];
  if(domWindow == windowObject)
  {
    [domWindow setValue:self.jsBridgeController forKey:@"JSBridge"];
    
    
    //        NSString* htmlTitle = [domWindow callWebScriptMethod:@"GetWindowTitle" withArguments:@[]];
    //        NSString* altHtmlTitle = [[domWindow valueForKey:@"document"] valueForKey:@"title"];
    //        NSLog(@"Window title is : %@", htmlTitle);
    //        NSLog(@"Alt Window title is : %@", altHtmlTitle);
    //        if(htmlTitle.length)
    //        {
    //            [NSApp mainWindow].title = htmlTitle;
    //        }
    //        else if (altHtmlTitle.length)
    //        {
    //            [NSApp mainWindow].title = altHtmlTitle;
    //        }
  }
  domWindow = [webView windowScriptObject];
  NSString* windowTitle = [[domWindow valueForKey:@"document"] valueForKey:@"title"];
  if (![windowTitle isEqualToString:@""]) {
    [webView window].title = windowTitle;
  }
  
}

- (void)webView:(WebView *)webView decidePolicyForMIMEType:(NSString *)type
        request:(NSURLRequest *)request
          frame:(WebFrame *)frame
decisionListener:(id<WebPolicyDecisionListener>)listener {
  if (![type isEqualToString:@"text/html"]) {
    [listener ignore];
    NSWorkspace * ws = [NSWorkspace sharedWorkspace];
    [ws openURL:request.URL];
  }else {
    [listener use];
  }
}
- (WebView *)webView:(WebView *)sender
createWebViewWithRequest:(NSURLRequest *)request {
  
  if(!self.modalWebWindow) {
    NSDictionary* commandInfo = @{JSArgumentsKey:    @[
                                      @"HKCU",
                                      @"Software/SupportSoft/ProviderList/global",
                                      @"ModalDialogWidth"
                                      ],
                                  JSClassNameKey : @"Registry",
                                  JSISSyncMethodKey :@"1",
                                  JSOperationNameKey:@"GetRegValue"};
    NSInteger width = 500;
    NSInteger height = 500;
    
    PAJSResult* result = [self.jsBridgeController.commandDelegate executeCommand:commandInfo];
    if (![result.data isEqualToString:@""]) {
      width = ((NSString*)result.data).integerValue;
    }
    
    commandInfo = @{JSArgumentsKey:    @[
                        @"HKCU",
                        @"Software/SupportSoft/ProviderList/global",
                        @"ModalDialogHeight"
                        ],
                    JSClassNameKey : @"Registry",
                    JSISSyncMethodKey :@"1",
                    JSOperationNameKey:@"GetRegValue"};
    
    result = [self.jsBridgeController.commandDelegate executeCommand:commandInfo];
    if (![result.data isEqualToString:@""]) {
      height = ((NSString*)result.data).integerValue;
    }
    
    commandInfo = @{JSArgumentsKey:    @[
                        @"HKCU",
                        @"Software/SupportSoft/ProviderList/global",
                        @"WindowTitle"
                        ],
                    JSClassNameKey : @"Registry",
                    JSISSyncMethodKey :@"1",
                    JSOperationNameKey:@"GetRegValue"};
    
    result = [self.jsBridgeController.commandDelegate executeCommand:commandInfo];
    
    NSRect frame = NSZeroRect;
    NSRect windowRect = self.webView.window.frame;
    frame.origin.x = NSMinX(windowRect) + ((NSWidth(windowRect) - width) / 2);
    frame.origin.y = NSMinY(windowRect) + ((NSHeight(windowRect) - height) / 2);
    frame.size = NSMakeSize(width, height);
    
    NSUInteger windowStyleMask = NSClosableWindowMask|NSTitledWindowMask;
    self.modalWebWindow = [[NSWindow alloc] initWithContentRect:frame styleMask:windowStyleMask backing:NSBackingStoreNonretained defer:YES];
    [self.modalWebWindow setReleasedWhenClosed:NO];
    WebView *newWebView = [[WebView alloc] initWithFrame:[self.modalWebWindow contentRectForFrameRect:self.modalWebWindow.frame]];
    [newWebView setAutoresizingMask:NSViewWidthSizable|NSViewHeightSizable];
    newWebView.UIDelegate = self;
    self.modalWebWindow.delegate = self;
    newWebView.frameLoadDelegate = self;
    newWebView.policyDelegate = self;
    [self.modalWebWindow setContentView:newWebView];
    self.webViewInModalWindow = newWebView;
    [[self.modalWebWindow standardWindowButton:NSWindowMiniaturizeButton] setHidden:YES];
    [[self.modalWebWindow standardWindowButton:NSWindowZoomButton] setHidden:YES];
    if (![result.data isEqualToString:@""])
    {
      [self.modalWebWindow setTitle:result.data];
    }
  }else {
    NSInteger width = 500;
    NSInteger height = 500;
    NSDictionary* commandInfo = @{JSArgumentsKey:    @[
                                      @"HKCU",
                                      @"Software/SupportSoft/ProviderList/global",
                                      @"ModalDialogWidth"
                                      ],
                                  JSClassNameKey : @"Registry",
                                  JSISSyncMethodKey :@"1",
                                  JSOperationNameKey:@"GetRegValue"};
    
    PAJSResult* result = [self.jsBridgeController.commandDelegate executeCommand:commandInfo];
    if (![result.data isEqualToString:@""]) {
      width = ((NSString*)result.data).integerValue;
    }
    
    commandInfo = @{JSArgumentsKey:    @[
                        @"HKCU",
                        @"Software/SupportSoft/ProviderList/global",
                        @"ModalDialogHeight"
                        ],
                    JSClassNameKey : @"Registry",
                    JSISSyncMethodKey :@"1",
                    JSOperationNameKey:@"GetRegValue"};
    
    result = [self.jsBridgeController.commandDelegate executeCommand:commandInfo];
    if (![result.data isEqualToString:@""]) {
      height = ((NSString*)result.data).integerValue;
    }
    
    commandInfo = @{JSArgumentsKey:    @[
                        @"HKCU",
                        @"Software/SupportSoft/ProviderList/global",
                        @"WindowTitle"
                        ],
                    JSClassNameKey : @"Registry",
                    JSISSyncMethodKey :@"1",
                    JSOperationNameKey:@"GetRegValue"};
    
    result = [self.jsBridgeController.commandDelegate executeCommand:commandInfo];
    if (![result.data isEqualToString:@""])
    {
      [self.modalWebWindow setTitle:result.data];
    }
    NSRect frame = NSZeroRect;
    NSRect windowRect = self.webView.window.frame;
    frame.origin.x = NSMinX(windowRect) + ((NSWidth(windowRect) - width) / 2);
    frame.origin.y = NSMinY(windowRect) + ((NSHeight(windowRect) - height) / 2);
    frame.size = NSMakeSize(width, height);
    [self.modalWebWindow setFrame:frame display:NO];
    
  }
  return self.modalWebWindow.contentView;
}

- (void)webView:(WebView *)sender runJavaScriptAlertPanelWithMessage:(NSString *)message initiatedByFrame:(WebFrame *)frame {
  NSAlert *alert = [[NSAlert alloc] init];
  [alert addButtonWithTitle:@"OK"];
  [alert setMessageText:message];
  [alert runModal];
}

- (BOOL)webView:(WebView *)sender runJavaScriptConfirmPanelWithMessage:(NSString *)message initiatedByFrame:(WebFrame *)frame {
  NSAlert *alert = [[NSAlert alloc] init];
  [alert addButtonWithTitle:@"OK"];
  [alert addButtonWithTitle:@"Cancel"];
  [alert setMessageText:message];
  [alert setAlertStyle:NSWarningAlertStyle];
  if ([alert runModal] == NSAlertFirstButtonReturn) {
    // OK clicked, delete the record
    return YES;
  }
  return NO;
}
- (WebView *)webView:(WebView *)sender createWebViewModalDialogWithRequest:(NSURLRequest *)request{
  
  return [self webView:sender createWebViewWithRequest:request];
}


- (void)webViewRunModal:(WebView *)sender{
  [sender.window makeKeyAndOrderFront:self];
  
}

- (void)webViewShow:(WebView *)sender{
  
  if (sender.window == self.modalWebWindow && [self.modalWebWindow isVisible]) {
    return;
  }
  dispatch_async(dispatch_get_main_queue(), ^{
    if (sender.window == self.modalWebWindow) {
      NSModalSession session = [NSApp beginModalSessionForWindow:self.modalWebWindow];
      NSInteger result = NSModalResponseContinue;
      
      // Loop until some result other than continues:
      while (result == NSModalResponseContinue)
      {
        // Run the window modally until there are no events to process:
        result = [NSApp runModalSession:session];
        // Give the main loop some time:
        //                [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode
        //                                         beforeDate:[NSDate distantFuture]];
        [[NSRunLoop currentRunLoop] limitDateForMode:NSDefaultRunLoopMode];
      }
      [NSApp endModalSession:session];
      //            [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode
      //                                     beforeDate:[NSDate distantFuture]];
      
    }
  });
  [sender.window makeKeyAndOrderFront:self];
  
}

- (void)webViewClose:(WebView *)sender{
  [sender.window close];
}

- (void)windowWillClose:(NSNotification *)notification {
  [NSApp stopModal];
}
- (void)setAppType:(int)appType {
  self.jsBridgeController.currentApp = appType;
}

- (void)setCommandLineArguments:(NSArray*)arguments {
  [self.jsBridgeController setCommandLineArguments:arguments];
  
}

@end
