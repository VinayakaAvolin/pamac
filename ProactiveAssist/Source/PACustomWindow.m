//
//  PACustomWindow.m
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PACustomWindow.h"

@interface  PACustomWindow() {
  BOOL _canDrag;
}
@property NSPoint initialLocation;

@end
@implementation PACustomWindow

/*
 In Interface Builder, the class for the window is set to this subclass. Overriding the initializer
 provides a mechanism for controlling how objects of this class are created.
 */
- (id)initWithContentRect:(NSRect)contentRect
                styleMask:(NSWindowStyleMask)aStyle
                  backing:(NSBackingStoreType)bufferingType
                    defer:(BOOL)flag {

  // Using NSBorderlessWindowMask results in a window without a title bar.
  self = [super initWithContentRect:contentRect styleMask:NSBorderlessWindowMask|NSClosableWindowMask|NSMiniaturizableWindowMask backing:NSBackingStoreBuffered defer:NO];
  
  if (self != nil) {
    self.titlebarAppearsTransparent = YES;
    // Start with no transparency for all drawing into the window
    [self setAlphaValue:1.0];
    // Turn off opacity so that the parts of the window that are not drawn into are transparent.
    [self setOpaque:YES];
  }
  return self;
}


/*
 Custom windows that use the NSBorderlessWindowMask can't become key by default. Override this method
 so that controls in this window will be enabled.
 */
- (BOOL)canBecomeKeyWindow {
  return YES;
}

- (BOOL)canBecomeMainWindow {
  return YES;
}


- (BOOL)isMovableByWindowBackground {
  return YES;
}

//- (NSTimeInterval)animationResizeTime:(NSRect)newWindowFrame
//{
//    return 0.1;
//}

- (void)sendEvent:(NSEvent *)theEvent
{
  if([theEvent type] == NSKeyDown)
  {
    if([theEvent keyCode] == 36)
      return;
  }
  
  if([theEvent type] == NSLeftMouseDown)
    [self mouseDown:theEvent];
  else if([theEvent type] == NSLeftMouseDragged)
    [self mouseDragged:theEvent];
  
  [super sendEvent:theEvent];
}


- (void)mouseDown:(NSEvent *)theEvent
{
  self.initialLocation = [theEvent locationInWindow];
  NSRect rect = NSMakeRect(0, NSHeight(self.frame) - 50, NSWidth(self.frame), 50);
  _canDrag = NSPointInRect(self.initialLocation, rect);
  [super mouseDown:theEvent];
}

- (void)mouseDragged:(NSEvent *)theEvent
{
  NSRect screenVisibleFrame = [[NSScreen mainScreen] visibleFrame];
  NSRect windowFrame = [self frame];
  NSPoint newOrigin = windowFrame.origin;
  
  NSPoint currentLocation = [theEvent locationInWindow];
  if(_canDrag)
  {
    newOrigin.x += (currentLocation.x - self.initialLocation.x);
    newOrigin.y += (currentLocation.y - self.initialLocation.y);
    
    if ((newOrigin.y + windowFrame.size.height) > (screenVisibleFrame.origin.y + screenVisibleFrame.size.height))
    {
      newOrigin.y = screenVisibleFrame.origin.y + (screenVisibleFrame.size.height - windowFrame.size.height);
    }
    
    [self setFrameOrigin:newOrigin];
  }
}
@end
