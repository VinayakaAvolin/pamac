#!/bin/sh

#  KillResidentAgent.sh
#  
#
#  Created by Avolin on 03/01/18.
#

killResidentAgentLaunched(){
  pid=$(ps aux | grep '97T89359R6.com.avolin.supportsoft' | awk '{print $2}')
  if [[ -n $pid ]]; then
   kill $pid
  fi
}

killResidentAgentLaunched
