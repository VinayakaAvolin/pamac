//
//  PAMonitoringAgentProtocol.h
//  ProactiveAssist
//
//  Copyright © 2019 Avolin. All rights reserved.
//

#import <PAEventBus/PAEventBus.h>

typedef void(^PAMonitoringAgentServiceResult)(PASelfHealEvent *event);
typedef void(^PAMonitoringAgentStopMonitorResult)(BOOL success);

@protocol PAMonitoringAgentProtocol
@required
- (void)executeMonitorService:(PASelfHealTriggerInfo *)triggerInfo withReply: (PAMonitoringAgentServiceResult)reply;
-(void) stopMonitoringForEvent:(NSString *)eventGuid withReply:(PAMonitoringAgentStopMonitorResult)reply;

-(void)registerListenerEndpoint:(NSXPCListenerEndpoint *)endpoint;

@end
