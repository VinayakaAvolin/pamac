//
//  PAMonitoringAgent.h
//  PAMonitoringAgent
//
//  Copyright © 2019 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PAMonitoringAgentProtocol.h"
NS_ASSUME_NONNULL_BEGIN

@interface PAMonitoringAgent : NSObject<NSXPCListenerDelegate, PAMonitoringAgentProtocol>
+ (id)sharedMonitoringAgent;
@end

NS_ASSUME_NONNULL_END
