//
//  EventDeliveryProtocol.h
//  ProactiveAssist
//
//  Copyright © 2019 Avolin. All rights reserved.
//
@class PASelfHealEvent;

@protocol EventDeliveryProtocol <NSObject>
@required
-(void) publishEvent:(PASelfHealEvent *)event;
@end
