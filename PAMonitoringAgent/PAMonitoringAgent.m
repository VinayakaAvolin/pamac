//
//  PAMonitoringAgent.m
//  PAMonitoringAgent
//
//  Copyright © 2019 Avolin. All rights reserved.
//
#import <PAEventMonitorManager/PAEventMonitorManager.h>
#import "PAMonitoringAgent.h"
#import "PASelfHealTriggerInfo.h"
#import "EventDeliveryProtocol.h"

@implementation PAMonitoringAgent
{
    NSXPCConnection *_processConnection;
    id<EventDeliveryProtocol> _remoteObjectProxy;
}

+ (id)sharedMonitoringAgent {
    static PAMonitoringAgent *sharedMonitoringAgent = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMonitoringAgent = [[self alloc] init];
    });
    return sharedMonitoringAgent;
}

- (BOOL)listener:(NSXPCListener *)listener shouldAcceptNewConnection:(NSXPCConnection *)newConnection {
    NSXPCInterface *interface = [NSXPCInterface interfaceWithProtocol:@protocol(PAMonitoringAgentProtocol)];
    NSSet *executeMonitorServiceClassList = [NSSet setWithObjects:[PASelfHealTriggerInfo class], [NSString class], [NSArray class],[NSDictionary class], [PASelfHealEvent class],[NSXPCListenerEndpoint class], nil];
    NSSet *stopMonitorClassList = [NSSet setWithObjects:[NSString class], nil];

    [interface setClasses:executeMonitorServiceClassList
              forSelector:@selector(executeMonitorService:withReply:)
            argumentIndex:0
                  ofReply:YES];
    [interface setClasses:stopMonitorClassList
              forSelector:@selector(stopMonitoringForEvent:withReply:) argumentIndex:0 ofReply:NO];
    
    [interface setClasses:executeMonitorServiceClassList
              forSelector:@selector(registerListenerEndpoint:)
            argumentIndex:0
                  ofReply:NO];
    
    newConnection.exportedInterface = interface;
    newConnection.exportedObject = self;
    [newConnection resume];
    
    return YES;
}
- (void)executeMonitorService:(PASelfHealTriggerInfo *)triggerInfo withReply:(PAMonitoringAgentServiceResult)reply {
    PAEventMonitorInterface *monitor = [PAEventMonitorInterface sharedInstance];
    __weak id<EventDeliveryProtocol>weakProxy = _remoteObjectProxy;
    [monitor setupMonitoringServiceForEvent: triggerInfo withReply: ^(PASelfHealEvent *event) {
        //reply(event);
       [weakProxy publishEvent:event];
    }];
}

-(void) stopMonitoringForEvent:(NSString *)eventGuid withReply:(PAMonitoringAgentStopMonitorResult)reply {
    NSLog(@"Stopping monitoring in monitoring agent...");
    PAEventMonitorInterface *monitor = [PAEventMonitorInterface sharedInstance];
    [monitor stopMonitoring:eventGuid withReply:^(BOOL success) {
        reply(success);
    }];
    
}
-(void)registerListenerEndpoint:(NSXPCListenerEndpoint *)endpoint {
    _processConnection = [[NSXPCConnection alloc] initWithListenerEndpoint:endpoint];
    NSXPCInterface *interface = [NSXPCInterface interfaceWithProtocol:@protocol(EventDeliveryProtocol)];
    NSSet *monitorServiceClassList = [NSSet setWithObjects:[PASelfHealTriggerInfo class], [NSString class], [NSArray class],[NSDictionary class], [PASelfHealEvent class], nil];
    [interface setClasses:monitorServiceClassList
              forSelector:@selector(publishEvent:) argumentIndex:0 ofReply:NO];
    _processConnection.remoteObjectInterface = interface;
    _processConnection.interruptionHandler = ^(){
        NSLog(@"interrupted");
    };
    
    _processConnection.invalidationHandler = ^() {
        NSLog(@"invalidated");
    };
    [_processConnection resume];
    _remoteObjectProxy = [_processConnection remoteObjectProxyWithErrorHandler:^(NSError * err) {
        NSLog(@"%@", err);
    }];
}
@end
