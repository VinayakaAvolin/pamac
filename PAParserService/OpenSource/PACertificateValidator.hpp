//
//  PACertificateValidator1.hpp
//  OpenSSLVerifier
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#ifndef PACertificateValidator1_hpp
#define PACertificateValidator1_hpp

#include <stdio.h>

bool verifyCertificateCommonName(char *xmlFilePath, char *certificateFilePath);


#endif /* PACertificateValidator1_hpp */
