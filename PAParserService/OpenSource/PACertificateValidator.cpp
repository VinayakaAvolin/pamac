//
//  PACertificateValidator1.cpp
//  OpenSSLVerifier
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#include "PACertificateValidator.hpp"
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <limits>
#include <map>
#include <vector>
#include <stdio.h>

#include <openssl/sha.h>
#include <openssl/hmac.h>
#include <openssl/evp.h>
#include <openssl/bio.h>
#include <openssl/buffer.h>
#include <openssl/pem.h>
#include <openssl/engine.h>
#include <openssl/evp.h>
#include <openssl/conf.h>
#include <openssl/x509.h>
#include <openssl/x509v3.h>
#include <openssl/ossl_typ.h>

class PACertificateValidator
{
private:
  std::string main;
  std::string scratch;
  
  const std::string::size_type ScratchSize = 1024;  // or some other arbitrary number
  
public:
  PACertificateValidator & append(const std::string & str) {
    scratch.append(str);
    if (scratch.size() > ScratchSize) {
      main.append(scratch);
      scratch.resize(0);
    }
    return *this;
  }
  
  const std::string & str() {
    if (scratch.size() > 0) {
      main.append(scratch);
      scratch.resize(0);
    }
    return main;
  }
};

//----------------------------------------------------------------------
std::string thumbprint(X509* x509)
{
  static const char hexbytes[] = "0123456789ABCDEF";
  unsigned int md_size;
  unsigned char md[EVP_MAX_MD_SIZE];
  const EVP_MD * digest = EVP_get_digestbyname("sha1");
  X509_digest(x509, digest, md, &md_size);
  std::stringstream ashex;
  for (int pos = 0; pos < md_size; pos++)
  {
    ashex << hexbytes[(md[pos] & 0xf0) >> 4];
    ashex << hexbytes[(md[pos] & 0x0f) >> 0];
  }
  return ashex.str();
}
//----------------------------------------------------------------------
int certversion(X509* x509)
{
  return X509_get_version(x509) + 1;
}
//----------------------------------------------------------------------
std::string pem(X509* x509)
{
  BIO * bio_out = BIO_new(BIO_s_mem());
  PEM_write_bio_X509(bio_out, x509);
  BUF_MEM *bio_buf;
  BIO_get_mem_ptr(bio_out, &bio_buf);
  std::string pem =  std::string(bio_buf->data, bio_buf->length);
  BIO_free(bio_out);
  return pem;
}
//----------------------------------------------------------------------
void _asn1dateparse(const ASN1_TIME *time, int& year, int& month, int& day, int& hour, int& minute, int& second)
{
  const char* str = (const char*)time->data;
  size_t i = 0;
  if (time->type == V_ASN1_UTCTIME) {/* two digit year */
    year = (str[i++] - '0') * 10;
    year += (str[i++] - '0');
    year += (year < 70 ? 2000 : 1900);
  }
  else if (time->type == V_ASN1_GENERALIZEDTIME) {/* four digit year */
    year = (str[i++] - '0') * 1000;
    year += (str[i++] - '0') * 100;
    year += (str[i++] - '0') * 10;
    year += (str[i++] - '0');
  }
  month = (str[i++] - '0') * 10;
  month += (str[i++] - '0') - 1; // -1 since January is 0 not 1.
  day = (str[i++] - '0') * 10;
  day += (str[i++] - '0');
  hour = (str[i++] - '0') * 10;
  hour += (str[i++] - '0');
  minute = (str[i++] - '0') * 10;
  minute += (str[i++] - '0');
  second = (str[i++] - '0') * 10;
  second += (str[i++] - '0');
}
//----------------------------------------------------------------------
std::string _asn1int(ASN1_INTEGER *bs)
{
  static const char hexbytes[] = "0123456789ABCDEF";
  std::stringstream ashex;
  for (int i = 0; i<bs->length; i++)
  {
    ashex << hexbytes[(bs->data[i] & 0xf0) >> 4];
    ashex << hexbytes[(bs->data[i] & 0x0f) >> 0];
  }
  return ashex.str();
}
//----------------------------------------------------------------------
std::string _asn1string(ASN1_STRING *d)
{
  std::string asn1_string;
  if (ASN1_STRING_type(d) != V_ASN1_UTF8STRING) {
    unsigned char *utf8;
    int length = ASN1_STRING_to_UTF8(&utf8, d);
    asn1_string =  std::string((char*)utf8, length);
    OPENSSL_free(utf8);
  }
  else {
    asn1_string =  std::string((char*)ASN1_STRING_data(d), ASN1_STRING_length(d));
  }
  return asn1_string;
}
//----------------------------------------------------------------------
std::string _subject_as_line(X509_NAME *subj_or_issuer)
{
  BIO * bio_out = BIO_new(BIO_s_mem());
  X509_NAME_print(bio_out, subj_or_issuer, 0);
  BUF_MEM *bio_buf;
  BIO_get_mem_ptr(bio_out, &bio_buf);
  std::string issuer =  std::string(bio_buf->data, bio_buf->length);
  BIO_free(bio_out);
  return issuer;
}
//----------------------------------------------------------------------
std::map< std::string,  std::string> _subject_as_map(X509_NAME *subj_or_issuer)
{
  std::map< std::string,  std::string> m;
  for (int i = 0; i < X509_NAME_entry_count(subj_or_issuer); i++) {
    X509_NAME_ENTRY *e = X509_NAME_get_entry(subj_or_issuer, i);
    ASN1_STRING *d = X509_NAME_ENTRY_get_data(e);
    ASN1_OBJECT *o = X509_NAME_ENTRY_get_object(e);
    const char* key_name = OBJ_nid2sn(OBJ_obj2nid(o));
    m[key_name] = _asn1string(d);
  }
  return m;
}
//----------------------------------------------------------------------
std::string issuer_one_line(X509* x509)
{
  return _subject_as_line(X509_get_issuer_name(x509));
}
//----------------------------------------------------------------------
std::string subject_one_line(X509* x509)
{
  return _subject_as_line(X509_get_subject_name(x509));
}
//----------------------------------------------------------------------
std::map< std::string,  std::string> subject(X509* x509)
{
  return _subject_as_map(X509_get_subject_name(x509));
}
//----------------------------------------------------------------------
std::map< std::string,  std::string> issuer(X509* x509)
{
  return _subject_as_map(X509_get_issuer_name(x509));
}
//----------------------------------------------------------------------
std::string serial(X509* x509)
{
  return _asn1int(X509_get_serialNumber(x509));
}
//----------------------------------------------------------------------
std::string signature_algorithm(X509 *x509)
{
  int sig_nid = OBJ_obj2nid((x509)->sig_alg->algorithm);
  return  std::string(OBJ_nid2ln(sig_nid));
}
//----------------------------------------------------------------------
std::string public_key_type(X509 *x509)
{
  EVP_PKEY *pkey = X509_get_pubkey(x509);
  int key_type = EVP_PKEY_type(pkey->type);
  EVP_PKEY_free(pkey);
  if (key_type == EVP_PKEY_RSA) return "rsa";
  if (key_type == EVP_PKEY_DSA) return "dsa";
  if (key_type == EVP_PKEY_DH)  return "dh";
  if (key_type == EVP_PKEY_EC)  return "ecc";
  return "";
}
//----------------------------------------------------------------------
int public_key_size(X509 *x509)
{
  EVP_PKEY *pkey = X509_get_pubkey(x509);
  int key_type = EVP_PKEY_type(pkey->type);
  int keysize = -1; //or in bytes, RSA_size() DSA_size(), DH_size(), ECDSA_size();
  keysize = key_type == EVP_PKEY_RSA && pkey->pkey.rsa->n ? BN_num_bits(pkey->pkey.rsa->n) : keysize;
  keysize = key_type == EVP_PKEY_DSA && pkey->pkey.dsa->p ? BN_num_bits(pkey->pkey.dsa->p) : keysize;
  keysize = key_type == EVP_PKEY_DH  && pkey->pkey.dh->p ? BN_num_bits(pkey->pkey.dh->p) : keysize;
  keysize = key_type == EVP_PKEY_EC ? EC_GROUP_get_degree(EC_KEY_get0_group(pkey->pkey.ec)) : keysize;
  EVP_PKEY_free(pkey);
  return keysize;
}
//----------------------------------------------------------------------
std::string public_key_ec_curve_name(X509 *x509)
{
  EVP_PKEY *pkey = X509_get_pubkey(x509);
  int key_type = EVP_PKEY_type(pkey->type);
  if (key_type == EVP_PKEY_EC)
  {
    const EC_GROUP *group = EC_KEY_get0_group(pkey->pkey.ec);
    int name = (group != NULL) ? EC_GROUP_get_curve_name(group) : 0;
    return name ? OBJ_nid2sn(name) : "";
  }
  return "";
}
//----------------------------------------------------------------------
std::string asn1datetime_isodatetime(const ASN1_TIME *tm)
{
  int year = 0, month = 0, day = 0, hour = 0, min = 0, sec = 0;
  _asn1dateparse(tm, year, month, day, hour, min, sec);
  
  char buf[25] = "";
  snprintf(buf, sizeof(buf) - 1, "%04d-%02d-%02d %02d:%02d:%02d GMT", year, month, day, hour, min, sec);
  return  std::string(buf);
}
//----------------------------------------------------------------------
std::string asn1date_isodate(const ASN1_TIME *tm)
{
  int year = 0, month = 0, day = 0, hour = 0, min = 0, sec = 0;
  _asn1dateparse(tm, year, month, day, hour, min, sec);
  
  char buf[25] = "";
  snprintf(buf, sizeof(buf) - 1, "%04d-%02d-%02d", year, month, day);
  return  std::string(buf);
}
//----------------------------------------------------------------------
std::vector< std::string> subject_alt_names(X509 *x509)
{
  std::vector< std::string> list;
  GENERAL_NAMES* subjectAltNames = (GENERAL_NAMES*)X509_get_ext_d2i(x509, NID_subject_alt_name, NULL, NULL);
  for (int i = 0; i < sk_GENERAL_NAME_num(subjectAltNames); i++)
  {
    GENERAL_NAME* gen = sk_GENERAL_NAME_value(subjectAltNames, i);
    if (gen->type == GEN_URI || gen->type == GEN_DNS || gen->type == GEN_EMAIL)
    {
      ASN1_IA5STRING *asn1_str = gen->d.uniformResourceIdentifier;
      std::string san =  std::string((char*)ASN1_STRING_data(asn1_str), ASN1_STRING_length(asn1_str));
      list.push_back(san);
    }
    else if (gen->type == GEN_IPADD)
    {
      unsigned char *p = gen->d.ip->data;
      if (gen->d.ip->length == 4)
      {
        std::stringstream ip;
        ip << (int)p[0] << '.' << (int)p[1] << '.' << (int)p[2] << '.' << (int)p[3];
        list.push_back(ip.str());
      }
      else //if(gen->d.ip->length == 16) //ipv6?
      {
        //std::cerr << "Not implemented: parse sans ("<< __FILE__ << ":" << __LINE__ << ")" <<  std::endl;
      }
    }
    else
    {
      //std::cerr << "Not implemented: parse sans ("<< __FILE__ << ":" << __LINE__ << ")" <<  std::endl;
    }
  }
  GENERAL_NAMES_free(subjectAltNames);
  return list;
}
//----------------------------------------------------------------------
std::vector< std::string> ocsp_urls(X509 *x509)
{
  std::vector< std::string> list;
  STACK_OF(OPENSSL_STRING) *ocsp_list = X509_get1_ocsp(x509);
  for (int j = 0; j < sk_OPENSSL_STRING_num(ocsp_list); j++)
  {
    list.push_back( std::string(sk_OPENSSL_STRING_value(ocsp_list, j)));
  }
  X509_email_free(ocsp_list);
  return list;
}
//----------------------------------------------------------------------
std::vector< std::string> crl_urls(X509 *x509)
{
  std::vector< std::string> list;
  int nid = NID_crl_distribution_points;
  STACK_OF(DIST_POINT) * dist_points = (STACK_OF(DIST_POINT) *)X509_get_ext_d2i(x509, nid, NULL, NULL);
  for (int j = 0; j < sk_DIST_POINT_num(dist_points); j++)
  {
    DIST_POINT *dp = sk_DIST_POINT_value(dist_points, j);
    DIST_POINT_NAME    *distpoint = dp->distpoint;
    if (distpoint->type == 0)//fullname GENERALIZEDNAME
    {
      for (int k = 0; k < sk_GENERAL_NAME_num(distpoint->name.fullname); k++)
      {
        GENERAL_NAME *gen = sk_GENERAL_NAME_value(distpoint->name.fullname, k);
        ASN1_IA5STRING *asn1_str = gen->d.uniformResourceIdentifier;
        list.push_back( std::string((char*)ASN1_STRING_data(asn1_str), ASN1_STRING_length(asn1_str)));
      }
    }
    else if (distpoint->type == 1)//relativename X509NAME
    {
      STACK_OF(X509_NAME_ENTRY) *sk_relname = distpoint->name.relativename;
      for (int k = 0; k < sk_X509_NAME_ENTRY_num(sk_relname); k++)
      {
        X509_NAME_ENTRY *e = sk_X509_NAME_ENTRY_value(sk_relname, k);
        ASN1_STRING *d = X509_NAME_ENTRY_get_data(e);
        list.push_back( std::string((char*)ASN1_STRING_data(d), ASN1_STRING_length(d)));
      }
    }
  }
  CRL_DIST_POINTS_free(dist_points);
  return list;
}

//----------------------------------------------------------------------
void parseCert1(X509* x509)
{
  std::cout << "--------------------" <<  std::endl;
  BIO *bio_out = BIO_new_fp(stdout, BIO_NOCLOSE);
  //PEM_write_bio_X509(bio_out, x509);//STD OUT the PEM
  
  fprintf(stdout, "---\n");
  X509_print(bio_out, x509);//STD OUT the details
  //X509_print_ex(bio_out, x509, XN_FLAG_COMPAT, X509_FLAG_COMPAT);//STD OUT the details
  BIO_flush(bio_out);
  
  fprintf(stdout, "---\n");
  BIO_free(bio_out);
  fprintf(stdout, "---\n");
  //fclose(fp);
}

//----------------------------------------------------------------------
void parseCert3(X509* x509)
{
  std::cout << "--------------------" << std::endl;
  //cout << pem(x509) << std::endl;
  std::cout << "Thumbprint: " << thumbprint(x509) << std::endl;
  std::cout << "Version: " << certversion(x509) << std::endl;
  std::cout << "Serial: " << serial(x509) << std::endl;
  std::cout << "Issuer: " << issuer_one_line(x509) << std::endl;
  std::map<std::string, std::string> ifields = issuer(x509);
  for (std::map<std::string, std::string>::iterator i = ifields.begin(), ix = ifields.end(); i != ix; i++)
    std::cout << " * " << i->first << " : " << i->second << std::endl;
  std::cout << "Subject: " << subject_one_line(x509) << std::endl;
  std::map<std::string, std::string> sfields = subject(x509);
  for (std::map<std::string, std::string>::iterator i = sfields.begin(), ix = sfields.end(); i != ix; i++)
    std::cout << " * " << i->first << " : " << i->second << std::endl;
  std::cout << "SignatureAlgorithm: " << signature_algorithm(x509) << std::endl;
  std::cout << "PublicKeyType: " << public_key_type(x509) << public_key_ec_curve_name(x509) << std::endl;
  std::cout << "PublicKeySize: " << public_key_size(x509) << std::endl;
  std::cout << "NotBefore: " << asn1datetime_isodatetime(X509_get_notBefore(x509)) << std::endl;
  std::cout << "NotAfter: " << asn1datetime_isodatetime(X509_get_notAfter(x509)) << std::endl;
  std::cout << "SubjectAltName(s):" << std::endl;
  std::vector<std::string> sans = subject_alt_names(x509);
  for (int i = 0, ix = sans.size(); i<ix; i++) {
    std::cout << " " << sans[i] << std::endl;
  }
  std::cout << "CRL URLs:" << std::endl;
  std::vector<std::string> crls = crl_urls(x509);
  for (int i = 0, ix = crls.size(); i<ix; i++) {
    std::cout << " " << crls[i] << std::endl;
  }
  std::cout << "OCSP URLs:" << std::endl;
  std::vector<std::string> urls = ocsp_urls(x509);
  for (int i = 0, ix = urls.size(); i<ix; i++) {
    std::cout << " " << urls[i] << std::endl;
  }
  std::cout << "--------------------" << std::endl;
}

long  StripBlanksFromBuffer(const char * data_in, char * data_out, long data_in_length) {
  long i, j = 0;
  for (i = 0; i < data_in_length; i++) {
    if (data_in[i] > 0x20) {
      data_out[j] = data_in[i];
      j++;
    }
  }
  return j;
}

time_t ASN1_GetTimeT(ASN1_TIME* asnTime) {
  struct tm time;
  const char* strTime = (const char*)asnTime->data;
  size_t i = 0;
  memset(&time, 0, sizeof(time));
  if(asnTime->type == V_ASN1_UTCTIME) {
    time.tm_year = (strTime[i] - '0') * 10 + (strTime[i+1] - '0');
    i+= 2;
    if(time.tm_year < 70) {
      time.tm_year += 100;
    }
  }else if(asnTime->type == V_ASN1_GENERALIZEDTIME) {
    time.tm_year = (strTime[i] - '0') * 1000 + (strTime[i+1] - '0') * 100 + (strTime[i+2] - '0') * 10 + (strTime[i+3] - '0');
    i+= 4;
  }
  time.tm_mon = (strTime[i] - '0') * 10 + (strTime[i+1] - '0') - 1;
  i+= 2;
  time.tm_mday = (strTime[i] - '0') * 10 + (strTime[i+1] - '0');
  i+= 2;
  time.tm_hour = (strTime[i] - '0') * 10 + (strTime[i+1] - '0');
  i+= 2;
  time.tm_min = (strTime[i] - '0') * 10 + (strTime[i+1] - '0');
  i+= 2;
  time.tm_sec = (strTime[i] - '0') * 10 + (strTime[i+1] - '0');
  return mktime(&time);
}

bool verifyCertificate(std::string pkcsFile, X509* _certificate) {
  
  BIO* bio = BIO_new(BIO_s_file());
  X509_STORE* _certificate_store = X509_STORE_new();
  
  OpenSSL_add_all_algorithms();
  std::cout << "Certificate from manifest xml is: " << std::endl;
  parseCert3(_certificate);
  
  time_t currentTime = time(NULL);
  
  if(ASN1_GetTimeT(X509_get_notBefore(_certificate)) > currentTime ||
     ASN1_GetTimeT(X509_get_notAfter(_certificate)) < currentTime) {
    std::cout << "Certificate expired or not yet valid" << std::endl;
    return false;
  }
  
  
  if ((bio != NULL) && (BIO_read_filename(bio, pkcsFile.c_str())>0)) {
    //rval = LoadPKCS7BIO(bio);
    PKCS7* p7 = d2i_PKCS7_bio(bio, NULL);
    STACK_OF(X509) *certs = NULL;
    STACK_OF(X509_CRL) *crls = NULL;
    
    int i = OBJ_obj2nid(p7->type);
    switch (i) {
      case NID_pkcs7_signed:
        certs = p7->d.sign->cert;
        crls = p7->d.sign->crl;
        break;
      case NID_pkcs7_signedAndEnveloped:
        certs = p7->d.signed_and_enveloped->cert;
        crls = p7->d.signed_and_enveloped->crl;
        break;
      default:
        break;
    }
    
    if (certs != NULL) {
      std::cout << std::endl << std::endl << "Certificates from p7b file are: " << std::endl;
      for (i = 0; i < sk_X509_num(certs); i++) {
        X509 *storeCert = sk_X509_value(certs, i);
        parseCert3(storeCert);
        X509_STORE_add_cert(_certificate_store, storeCert);
      }
    }
    if (crls != NULL) {
      for (i = 0; i < sk_X509_CRL_num(crls); i++) {
        X509_STORE_add_crl(_certificate_store, sk_X509_CRL_value(crls, i));
      }
    }
    
    if (p7 != NULL) PKCS7_free(p7);
  }
  BIO_free(bio);
  ERR_clear_error();
  std::cout << std::endl << std::endl;
  
  int rval;
  int _ex_index = X509_STORE_CTX_get_ex_new_index(0, (void*)"TuSSLCertificateStore Index", NULL, NULL, NULL);
  X509_STORE_CTX* certificate_store_context = X509_STORE_CTX_new();
  X509_STORE_CTX_init(certificate_store_context, _certificate_store, _certificate, NULL);
  X509_STORE_CTX_set_ex_data(certificate_store_context, _ex_index, (void*)NULL);
  rval = X509_verify_cert(certificate_store_context);
  X509_STORE_CTX_free(certificate_store_context);
  return rval ? true : false;
  
}

bool verifyXmlSignature(std::string certificateString, std::string signatureString, std::string pkcsFile, std::string xmlData) {
  
  const char *signature_in = signatureString.c_str();
  BIO* b64in_sign = BIO_new(BIO_s_mem());
  BIO_write(b64in_sign, signature_in, signatureString.length());
  BIO_flush(b64in_sign);
  BIO* b64_sign = BIO_new(BIO_f_base64());
  BIO_set_flags(b64_sign, BIO_FLAGS_BASE64_NO_NL);
  BIO_push(b64_sign, b64in_sign);
  char data[4096];
  int length = BIO_read(b64_sign, data, 4096);
  BIO_free(b64_sign);
  BIO_free(b64in_sign);
  
  X509* _certificate = NULL;
  const char *data_in = certificateString.c_str();
  BIO* b64in = BIO_new(BIO_s_mem());
  BIO_write(b64in, data_in, strlen(data_in));
  BIO_flush(b64in);
  BIO* b64 = BIO_new(BIO_f_base64());
  BIO_set_flags(b64, BIO_FLAGS_BASE64_NO_NL);
  BIO_push(b64, b64in);
  d2i_X509_bio(b64, &_certificate);
  BIO_free(b64);
  BIO_free(b64in);
  EVP_PKEY* rval = X509_get_pubkey(_certificate);
  
  
  if (verifyCertificate(pkcsFile, _certificate)) {
    std::cout << "Certificate Verification success" << std::endl;
  }
  else {
    std::cout << "Certificate Verification failed" << std::endl;
  }
  
  
  EVP_MD_CTX _md_ctx;
  
  EVP_VerifyInit(&_md_ctx, EVP_sha1());
  EVP_VerifyUpdate(&_md_ctx, xmlData.c_str(), xmlData.length());
  //char *tempBuff = new char[xmlData.length()];
  //int tmpLen = StripBlanksFromBuffer(xmlData.c_str(), tempBuff, xmlData.length());
  //EVP_VerifyUpdate(&_md_ctx, tempBuff, tmpLen);
  
  int err = EVP_VerifyFinal(&_md_ctx, (unsigned char*)data, length, rval);
  
  return (err == 1);
}

//int verifyCertificateCommonName(char *xmlFilePath, char *certificateFilePath)
//{
//    //    if(argc < 3) {
//    //        std::cout << "USAGE: " << argv[0] << " \"<xml file>\" \"<p7b file>\"" << std::endl;
//    //        return 0;
//    //    }
//    std::string xmlPath = xmlFilePath;
//    std::string certificatePath = certificateFilePath;
//
//    std::ifstream infile(xmlPath);
//    std::string line;
//    std::string certLine;
//    std::string signLine;
//    std::string delimiter = "=";
//    std::string signatureLine("Signature=");
//    std::string certificateLine("Certificate=");
//    std::string xmlStartLine("<?xml version=\"1.0\"?>");
//    int i = 0;
//    PACertificateValidator sb;
//    bool skipEmptyLine = true;
//    while (std::getline(infile, line))
//    {
//        if (line.compare("[Credentials]") == 0) {
//            //Remove INI header from final file
//            continue;
//        }
//
//        if (line.compare(0, signatureLine.length(), signatureLine) == 0)
//        {
//            signLine = line;
//            //Remove signature final file
//            continue;
//        }
//
//        if (line.compare(0, certificateLine.length(), certificateLine) == 0)
//        {
//            certLine = line;
//        }
//
//        if (line.compare(0, xmlStartLine.length(), xmlStartLine) == 0)
//        {
//            skipEmptyLine = false;
//        }
//
//        if (line.length() == 0 && skipEmptyLine) {
//            //Skip empty lines before start of actual XML
//            continue;
//        }
//    }
//
//    certLine = certLine.substr(certLine.find(delimiter)+1, certLine.length());
//    signLine.pop_back();
//    signLine = signLine.substr(signLine.find(delimiter)+1, signLine.length());
//
//
//    if (verifyXmlSignature(certLine, signLine, certificatePath, sb.str())) {
//        std::cout << "Signature Verification success" << std::endl;
//    }
//    else {
//
//        std::cout << "Signature Verification failed" << std::endl;
//    }
//    return 0;
//}
bool verifyCertificateCommonName(char *xmlFilePath, char *certificateFilePath)
{
  std::string xmlPath = xmlFilePath;
  std::string certificatePath = certificateFilePath;
  
  std::ifstream infile(xmlPath);
  std::string line;
  std::string certificateString;
  std::string signLine;
  std::string delimiter = "=";
  std::string signatureLine("Signature=");
  std::string certificateLine("Certificate=");
  std::string xmlStartLine("<?xml version=\"1.0\"?>");
  PACertificateValidator sb;
  bool skipEmptyLine = true;
  while (std::getline(infile, line))
  {
    if (line.compare("[Credentials]") == 0) {
      //Remove INI header from final file
      continue;
    }
    
    if (line.compare(0, signatureLine.length(), signatureLine) == 0)
    {
      signLine = line;
      //Remove signature final file
      continue;
    }
    
    if (line.compare(0, certificateLine.length(), certificateLine) == 0)
    {
      certificateString = line;
    }
    
    if (line.compare(0, xmlStartLine.length(), xmlStartLine) == 0)
    {
      skipEmptyLine = false;
    }
    
    if (line.length() == 0 && skipEmptyLine) {
      //Skip empty lines before start of actual XML
      continue;
    }
  }
  
  certificateString = certificateString.substr(certificateString.find(delimiter)+1, certificateString.length());
  signLine.pop_back();
  signLine = signLine.substr(signLine.find(delimiter)+1, signLine.length());
  
  
  X509* _certificate = NULL;
  const char *data_in = certificateString.c_str();
  BIO* b64in = BIO_new(BIO_s_mem());
  BIO_write(b64in, data_in, strlen(data_in));
  BIO_flush(b64in);
  BIO* b64 = BIO_new(BIO_f_base64());
  BIO_set_flags(b64, BIO_FLAGS_BASE64_NO_NL);
  BIO_push(b64, b64in);
  d2i_X509_bio(b64, &_certificate);
  BIO_free(b64);
  BIO_free(b64in);
  EVP_PKEY* rval = X509_get_pubkey(_certificate);
  
  bool success = verifyCertificate(certificatePath, _certificate);
  
  if (success) {
    std::cout << "Certificate Verification success" << std::endl;
  }
  else {
    std::cout << "Certificate Verification failed" << std::endl;
  }
  
  
  return success;
}
