//
//  PADataSetInfo.m
//  PAParserService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <PAFileSystemService/PAFileSystemService.h>

#import "PADataSetInfo.h"
#import "NSString+Additions.h"
#import "PAParserConstants.h"

NSString *const kPADataSetInfoFilterDelimeter = @",";

@implementation PADataSetInfo

- (instancetype)initWithFileName:(NSString *)fileName
                          source:(NSString *)sourceFolder
                          target:(NSString *)targetFolder
                        checksum:(NSString *)checksum
                          filter:(NSString *)filter
                    machineLevel:(BOOL)mchineLevel

{
  self = [super init];
  if (self) {
    if (nil != fileName) {
      _fileName = fileName;
      _sourceFolder = sourceFolder;
      _targetFolder = targetFolder;
      _checksum = checksum;
      _filterString = filter;
      _isMachineLevel = mchineLevel;
      
      [self setup];
    } else {
      self = nil;
    }
    
  }
  return self;
}


- (void)setup {
  //    _sourceRootPath = @"https://experience92.avolin.com/global/data/65aabe47-988e-482b-81bb-34d59e2c606f.39.zip";
  //    _destinationRootPath = @"/Users/vinayaka.s/Downloads/65aabe47-988e-482b-81bb-34d59e2c606f";
  
  /// to differentiate between "c94b0257-87e5-466a-8eed-e80f12eac10d.3.zip" and "config.zip". The prior zip requires intermediate directories to be created; the later zip has single file in it
  _isFile = ![_fileName doesMatchRegularExpression:kPADataSetNameRegEx];
  _fileSystemId = [_fileName stringByRemovingSubString:kPADotZip]; // string after removing .zip from "c94b0257-87e5-466a-8eed-e80f12eac10d.3.zip" i.e. "c94b0257-87e5-466a-8eed-e80f12eac10d.3"
  [self expandPath];
}

+ (instancetype)testFileSystemInfo {
  /*
   <file>
   <property name="filename">65aabe47-988e-482b-81bb-34d59e2c606f.39.zip</property>
   <property name="source">%SERVERROOT%data\</property>
   <property name="target">%DirUserServer%data\sprt_layout\</property>
   <property name="fchecksum">319dec3259883143e4ee05616f367fef</property>
   <property name="filters">Holmes</property>
   <property name="MachineLevel">0</property>
   </file>
   */
  PADataSetInfo *testFsInfo = [[PADataSetInfo alloc]initWithFileName:@"65aabe47-988e-482b-81bb-34d59e2c606f.39.zip" source:@"%SERVERROOT%data\\" target:@"%DirUserServer%data\\sprt_layout\\" checksum:@"319dec3259883143e4ee05616f367fef" filter:@"Holmes" machineLevel:NO];
  
  return testFsInfo;
}

- (void)expandPath {
  PAFileSystemManager *fileSystemManager = [[PAFileSystemManager alloc]init];
  _sourceRootPath = [[fileSystemManager stringByExpandingPath:_sourceFolder] stringByAppendingPathComponent:_fileName];
  _destinationRootPath = [[fileSystemManager stringByExpandingPath:_targetFolder] stringByAppendingPathComponent:_fileSystemId];
  _destinationBasePath = [fileSystemManager stringByExpandingPath:_targetFolder];
}

- (NSArray *)filterList {
  if (!_filterString.isEmptyOrHasOnlyWhiteSpaces) {
    return [_filterString componentsSeparatedByString:kPADataSetInfoFilterDelimeter];
  }
  return nil;
}

- (BOOL) hasMatchingFilter:(NSString *)filter {
  if (_filterString.isEmptyOrHasOnlyWhiteSpaces ||
      filter.isEmptyOrHasOnlyWhiteSpaces) {
    return false;
  }
  return [self.filterList containsObject:filter];
}

@end
