//
//  PAParserConstants.m
//  PAParserService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PAParserConstants.h"

NSString *const kPADataSetNameRegEx = @"^[A-Za-z0-9-]*.[0-9]*.[a-z]*";
NSString *const kPADataSetFileNameRegEx = @"[A-Za-z0-9-]*.[a-z]*";

NSString *const kPADotZip = @".zip";
