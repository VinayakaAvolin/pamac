//
//  PAXmlParser.m
//  PAParserService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PAXmlParser.h"
#import "XMLDictionary.h"
#import "NSString+Additions.h"

NSString *const kPAParserXmlTag = @"<?xml";
NSString *const kPAParserCertificateKey = @"Certificate=";
NSString *const kPAParserSignatureKey = @"Signature=";


@implementation PAXmlParser

- (NSDictionary *)dictionaryFromXmlAtPath:(NSString *)xmlPath {
  NSURL *URL = [[NSURL alloc] initFileURLWithPath:xmlPath isDirectory:NO];
  if (nil == URL) {
    return nil;
  }
  NSError *error = nil;
  NSString *xmlString = [[NSString alloc] initWithContentsOfURL:URL encoding:NSUTF8StringEncoding error:&error];
  
  // Escaping characters
  //xmlString =  (__bridge  NSString *)CFXMLCreateStringByUnescapingEntities(NULL, (CFStringRef)xmlString, NULL);
  
  NSString *subStringFromXmlTag = [xmlString subStringFromTag:kPAParserXmlTag];
  NSDictionary *xmlDoc = [NSDictionary dictionaryWithXMLString:subStringFromXmlTag];
  
  _certificateDataString = [xmlString subStringBetweenTags:kPAParserCertificateKey and:kPAParserSignatureKey trimNewLine:YES];
  _signatureString = [xmlString subStringBetweenTags:kPAParserSignatureKey and:kPAParserXmlTag trimNewLine:YES];
  
  return xmlDoc;
}

- (BOOL)verifyCerificateForXmlAtPath:(NSString *)xmlPath
                      certificateTag:(NSString *)certTag{
  return false;
}

- (NSDictionary *)dictionaryFromXmlString:(NSString *)xmlStr {
  NSDictionary *xmlDict = [NSDictionary dictionaryWithXMLString:xmlStr];
  return xmlDict;
}

@end
