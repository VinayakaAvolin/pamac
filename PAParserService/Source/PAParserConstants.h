//
//  PAParserConstants.h
//  PAParserService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifndef PAParserConstants_h
#define PAParserConstants_h

extern NSString *const kPADataSetNameRegEx;
extern NSString *const kPADataSetFileNameRegEx;
extern NSString *const kPADotZip;

#endif /* PAParserConstants_h */
