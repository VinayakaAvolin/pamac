//
//  PAManifestParser.h
//  PAParserService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PAXmlParser.h"

@interface PAManifestParser : PAXmlParser

- (void)parsedTargetCollectionFromManifest:(NSDictionary *)srcManifestDict
                                completion:(PAManifestParserCompletionBlock)completionBlock;
- (void)parsedDataSetFromManifest:(NSDictionary *)srcManifestDict
                       completion:(PAManifestParserCompletionBlock)completionBlock;
- (void)parsedProfileSetFromManifest:(NSDictionary *)srcManifestDict
                          completion:(PAManifestParserCompletionBlock)completionBlock;
- (void)validateManifestCertificate:(NSString *)manifestXml
                 andSyncCertificate:(NSString *)syncCertificatePath
                     withCompletion:(PAManifestParserCompletionBlock)completionBlock;

@end
