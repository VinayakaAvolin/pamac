//
//  PAXmlParser.h
//  PAParserService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PAParser.h"

typedef void(^PAManifestParserCompletionBlock)(BOOL success, NSArray *itemList);


@interface PAXmlParser : PAParser

@property (nonatomic, readonly) NSString *certificateDataString;
@property (nonatomic, readonly) NSString *signatureString;

@end
