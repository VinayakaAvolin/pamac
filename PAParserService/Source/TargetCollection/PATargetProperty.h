//
//  PATargetProperty.h
//  PAParserService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

/*
 instance type
 ====================
 <input type="radio" name="radiogroup" value="*" onFocus="Javascript:setInstance('*','hidden')" >All Instances<br>
 <input type="radio" name="radiogroup" value="?" onFocus="Javascript:setInstance('?','hidden')" CHECKED>First Instance Only<br>
 <input type="radio" name="radiogroup" value="?" onFocus="Javascript:setInstance('','visible')" >Specific Instance<br>
 */
typedef enum {
  PATargetPropertyAllInstance,
  PATargetPropertyFirstInstance,
  PATargetPropertySpecificInstance,
  PATargetPropertyInstanceNone
} PATargetPropertyInstanceType;

/*
 addConditionOption(objSelect, " =", "=", oldSelected)
 addConditionOption(objSelect, " !=", "!=", oldSelected)
 addConditionOption(objSelect, " <", "<", oldSelected)
 addConditionOption(objSelect, " >", ">", oldSelected)
 addConditionOption(objSelect, " <=", "<=", oldSelected)
 addConditionOption(objSelect, " >=", ">=", oldSelected)
 addConditionOption(objSelect, " contains", "**", oldSelected)
 addConditionOption(objSelect, " does not contain", "~*", oldSelected)
 addConditionOption(objSelect, " starts with", "*", oldSelected)
 addConditionOption(objSelect, " match pattern", "glob", oldSelected)
 addConditionOption(objSelect, " does not match pattern", "~glob", oldSelected)
 addConditionOption(objSelect, " matches any", "=*", oldSelected)
 addConditionOption(objSelect, " any in list", "=**", oldSelected)
 addConditionOption(objSelect, " none in list", "~=*", oldSelected)
 */
typedef enum {
  PATargetPropertyConditionEqualTo,
  PATargetPropertyConditionNotEqualTo,
  PATargetPropertyConditionLessThan,
  PATargetPropertyConditionGreaterThan,
  PATargetPropertyConditionLessThanOrEqualTo,
  PATargetPropertyConditionGreaterThanOrEqualTo,
  PATargetPropertyConditionContains,
  PATargetPropertyConditionDoesNotContain,
  PATargetPropertyConditionStartsWith,
  PATargetPropertyConditionMatchPattern,
  PATargetPropertyConditionDoesNotMatchPattern,
  PATargetPropertyConditionMatchesAny,
  PATargetPropertyConditionAnyInList,
  PATargetPropertyConditionNoneInList,
  PATargetPropertyConditionNone
} PATargetPropertyCondition;

#define kPATargetPropertyConditionArray @"=", @"!=", @"<", @">", @"<=", @">=", @"**", @"~*", @"*", @"glob", @"~glob", @"=*", @"=**", @"~=*", @"none", nil
#define kPATargetPropertyInstanceTypeArray @"*", @"?", @"s", @"none", nil


/**
 Class to store target collection property details
 */
@interface PATargetProperty : NSObject

@property(nonatomic) NSString *className;
@property(nonatomic) NSString *instanceTypeString;
@property(nonatomic) NSString *classPropertyName;
@property(nonatomic) NSString *friendlyName;
@property(nonatomic) NSString *conditionString;
@property(nonatomic) NSString *value; /// value to match
@property(nonatomic) id actualValue; /// actual value generated from defaultmac.xml script

@property(nonatomic) PATargetPropertyInstanceType instanceType;
@property(nonatomic) PATargetPropertyCondition condition;

@property(nonatomic) BOOL result; /// result after evaluation the property condition

@property(nonatomic) BOOL isActualValueList; /// actual value in the script output is list

- (instancetype)initWithClassName:(NSString *)className
                     instanceType:(NSString *)instanceTypeString
                     propertyName:(NSString *)propertyName
                     friendlyName:(NSString *)friendlyName
                        condition:(NSString *)condition
                            value:(NSString *)value;


@end
