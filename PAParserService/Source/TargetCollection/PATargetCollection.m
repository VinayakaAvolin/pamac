//
//  PATargetCollection.m
//  PAParserService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PATargetCollection.h"
#import "NSString+Additions.h"

@implementation PATargetCollection

- (instancetype)initWithTargetProperty:(NSString *)property
                                  name:(NSString *)name
                                 group:(NSString *)group
                              language:(NSString *)language;
{
  self = [super init];
  if (self) {
    if (![property isEmptyOrHasOnlyWhiteSpaces]) {
      _targetPropertyListString = property;
      _language = language;
      _name = name;
      _group = group;
      [self parseTargetProperties];
    } else {
      self = nil;
    }
  }
  return self;
}
/*
 
 Parse target property string of the below form into PATargetProperty object
 
 or|%InstalledSoftware::*::DisplayName%|Add/Remove Programs List|**|Cisco AnyConnect Secure Mobility Client
 or|%InstalledSoftware::*::DisplayName%|Add/Remove Programs List|**|Cisco Systems VPN
 or|%InstalledSoftwareX64::*::DisplayName%|Add/Remove Programs List X64|**|Cisco AnyConnect Secure Mobility Client
 or|%InstalledSoftwareX64::*::DisplayName%|Add/Remove Programs List X64|**|Cisco Systems VPN
 
 Grammar of target property is as follows, each target property is seperated by \n character:
 
 criteria|%TargetClassName::TargetInstanceType::TargetClassPropertyName%|TargetPropertyFriendlyName|Condition|Value\n
 criteria|%TargetClassName::TargetInstanceType::TargetClassPropertyName%|TargetPropertyFriendlyName|Condition|Value\n
 criteria|%TargetClassName::TargetInstanceType::TargetClassPropertyName%|TargetPropertyFriendlyName|Condition|Value\n
 
 */
- (void)parseTargetProperties {
  NSMutableArray *targetPropertyList = [[NSMutableArray alloc] init];
  NSArray *tgPropertyList = [_targetPropertyListString componentsSeparatedByString:@"\n"];
  
  for (NSString *property in tgPropertyList) {
    NSArray *propertyComps = [property componentsSeparatedByString:@"|"];
    if (propertyComps.count == 5) {
      NSString *criteria = propertyComps[0];
      NSString *expression = propertyComps[1];
      NSString *friendlyName = propertyComps[2];
      NSString *condition = propertyComps[3];
      NSString *value = propertyComps[4];
      NSString *className;
      NSString *propertyName;
      NSString *instanceType;
      
      if (![expression isEmptyOrHasOnlyWhiteSpaces]) {
        NSString *expString = [expression stringByReplacingOccurrencesOfString:@"%" withString:@""];
        NSArray *expCompList = [expString componentsSeparatedByString:@"::"];
        if (expCompList.count == 3) {
          className = expCompList[0];
          instanceType = expCompList[1];
          propertyName = expCompList[2];
        } else {
          continue;
        }
      }
      
      self.criteriaString = criteria;
      
      PATargetProperty *targetProperty = [[PATargetProperty alloc]initWithClassName:className instanceType:instanceType propertyName:propertyName friendlyName:friendlyName condition:condition value:value];
      
      if (targetProperty) {
        [targetPropertyList addObject:targetProperty];
      }
    } else {
      continue;
    }
    
  }
  
  if (targetPropertyList.count) {
    _targetPropertyList = [NSArray arrayWithArray:targetPropertyList];
  }
  _criteria = [self criteriaEnum];
}

- (PATargetCollectionCriteria)criteriaEnum {
  PATargetCollectionCriteria criteria = PATargetCollectionCriteriaUnknown;
  if (![_criteriaString isEmptyOrHasOnlyWhiteSpaces]) {
    NSArray *criteriaArray = [[NSArray alloc] initWithObjects:kPATargetCollectionCriteriaArray];
    NSUInteger index = [criteriaArray indexOfObject:_criteriaString];
    if (NSNotFound != index) {
      criteria = (PATargetCollectionCriteria)index;
    }
  }
  return criteria;
}

- (void)executeTargetCollectionRule {
  @autoreleasepool {
    BOOL result = false;
    int propertyIndex = 0;
    int totalProperties = (int)self.targetPropertyList.count;
    for (PATargetProperty *targetProperty in self.targetPropertyList) {
      ///first property; do not apply criteria
      if (propertyIndex == 0) {
        switch (_criteria) {
          case PATargetCollectionCriteriaAll:
          case PATargetCollectionCriteriaAny:
          case PATargetCollectionCriteriaNone:
            result = targetProperty.result;
            break;
          default:
            break;
        }
      } else {
        switch (_criteria) {
          case PATargetCollectionCriteriaAll:
            result = result && targetProperty.result;
            break;
          case PATargetCollectionCriteriaAny:
            result = result || targetProperty.result;
            break;
          case PATargetCollectionCriteriaNone:
            /// last property
            if (propertyIndex == totalProperties - 1) {
              result = !(result && targetProperty.result);
            } else {
              result = result && targetProperty.result;
            }
            break;
          default:
            break;
        }
      }
      propertyIndex++;
    }
    _isApplicable = result;
  }
}
//- (BOOL)isApplicable {
//    /// TODO: Remove "avolin" condition check; the script output needs to be returned here
//    if ([_name isEqualToString:@"avolin"]) {
//        return true;
//    }
//    return false;
//}

@end
