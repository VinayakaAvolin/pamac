//
//  PATargetProperty.m
//  PAParserService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PATargetProperty.h"
#import "NSString+Additions.h"

@implementation PATargetProperty

- (instancetype)initWithClassName:(NSString *)className
                     instanceType:(NSString *)instanceTypeString
                     propertyName:(NSString *)propertyName
                     friendlyName:(NSString *)friendlyName
                        condition:(NSString *)condition
                            value:(NSString *)value
{
  self = [super init];
  if (self) {
    _className = className;
    _instanceTypeString = instanceTypeString;
    _classPropertyName = propertyName;
    _friendlyName = friendlyName;
    _conditionString = condition;
    _value = value;
    _condition = [self conditionEnum];
  }
  return self;
}

- (PATargetPropertyInstanceType)instanceType {
  PATargetPropertyInstanceType instType = PATargetPropertyInstanceNone;
  if (![_instanceTypeString isEmptyOrHasOnlyWhiteSpaces]) {
    NSArray *instanceTypeArray = [[NSArray alloc] initWithObjects:kPATargetPropertyInstanceTypeArray];
    NSUInteger index = [instanceTypeArray indexOfObject:_instanceTypeString];
    if (NSNotFound == index) {
      instType = PATargetPropertySpecificInstance;
    } else {
      instType = (PATargetPropertyInstanceType)index;
    }
  }
  return instType;
}

- (PATargetPropertyCondition)conditionEnum {
  PATargetPropertyCondition condition = PATargetPropertyConditionNone;
  if (![_conditionString isEmptyOrHasOnlyWhiteSpaces]) {
    NSArray *conditionArray = [[NSArray alloc] initWithObjects:kPATargetPropertyConditionArray];
    NSUInteger index = [conditionArray indexOfObject:_conditionString];
    if (NSNotFound != index) {
      condition = (PATargetPropertyCondition)index;
    }
  }
  return condition;
}

- (BOOL)isActualValueList {
  BOOL isValueList = false;
  
  switch (_condition) {
    case PATargetPropertyConditionContains:
    case PATargetPropertyConditionAnyInList:
    case PATargetPropertyConditionMatchesAny:
    case PATargetPropertyConditionNoneInList:
      isValueList = true;
      break;
    default:
      break;
  }
  return isValueList;
}

- (BOOL)result {
  BOOL conditionEvaluationresult = false;
  if (! _actualValue || !_value) {
    return false;
  }
  switch (_condition) {
    case PATargetPropertyConditionEqualTo:
    {
      if ([_actualValue isKindOfClass:[NSString class]]) {
        NSComparisonResult compareResult = [_actualValue compare:_value];
        conditionEvaluationresult = (compareResult == NSOrderedSame);
      }
    }
      break;
    case PATargetPropertyConditionNotEqualTo:
    {
      if ([_actualValue isKindOfClass:[NSString class]]) {
        NSComparisonResult compareResult = [_actualValue compare:_value];
        conditionEvaluationresult = !(compareResult == NSOrderedSame);
      }
    }
      break;
    case PATargetPropertyConditionGreaterThan:
    {
      if ([_actualValue isKindOfClass:[NSString class]]) {
        NSComparisonResult compareResult = [_actualValue compare:_value];
        conditionEvaluationresult = (compareResult == NSOrderedDescending);
      }
    }
      break;
      
    case PATargetPropertyConditionLessThan:
    {
      if ([_actualValue isKindOfClass:[NSString class]]) {
        NSComparisonResult compareResult = [_actualValue compare:_value];
        conditionEvaluationresult = (compareResult == NSOrderedAscending);
      }
    }
      break;
    case PATargetPropertyConditionGreaterThanOrEqualTo:
    {
      if ([_actualValue isKindOfClass:[NSString class]]) {
        NSComparisonResult compareResult = [_actualValue compare:_value];
        conditionEvaluationresult = ((compareResult == NSOrderedDescending) || (compareResult == NSOrderedSame));
      }
    }
      break;
      
    case PATargetPropertyConditionLessThanOrEqualTo:
    {
      if ([_actualValue isKindOfClass:[NSString class]]) {
        NSComparisonResult compareResult = [_actualValue compare:_value];
        conditionEvaluationresult = ((compareResult == NSOrderedAscending) || (compareResult == NSOrderedSame));
      }
    }
      break;
    case PATargetPropertyConditionContains:
    {
      if ([_actualValue isKindOfClass:[NSString class]]) {
        conditionEvaluationresult = [_actualValue containsString:_value];
      } else if ([_actualValue isKindOfClass:[NSArray class]]) {
        conditionEvaluationresult = [_actualValue containsObject:_value];
      }
    }
      break;
    case PATargetPropertyConditionDoesNotContain:
    {
      if ([_actualValue isKindOfClass:[NSString class]]) {
        conditionEvaluationresult = ![_actualValue containsString:_value];
      }
    }
      break;
    case PATargetPropertyConditionStartsWith:
    {
      if ([_actualValue isKindOfClass:[NSString class]]) {
        conditionEvaluationresult = [_actualValue hasPrefix:_value];
      }
    }
      break;
    case PATargetPropertyConditionAnyInList:
    case PATargetPropertyConditionMatchesAny:
    {
      if ([_actualValue isKindOfClass:[NSArray class]]) {
        conditionEvaluationresult = [_actualValue containsObject:_value];
      }
    }
      break;
    case PATargetPropertyConditionNoneInList:
    {
      if ([_actualValue isKindOfClass:[NSArray class]]) {
        conditionEvaluationresult = ![_actualValue containsObject:_value];
      }
    }
      break;
    default:
      break;
  }
  
  return conditionEvaluationresult;
}

@end
