//
//  PATargetCollection.h
//  PAParserService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PATargetProperty.h"

/*
 criteria
 =========
 <input class="sanssmallblack" type="radio" name="matchao" value="and" checked>All
 <input class="sanssmallblack" type="radio" name="matchao" value="or">Any
 <input class="sanssmallblack" type="radio" name="matchao" value="not">None&nbsp;<br><br>
 */
typedef enum {
  PATargetCollectionCriteriaAll,
  PATargetCollectionCriteriaAny,
  PATargetCollectionCriteriaNone,
  PATargetCollectionCriteriaUnknown
} PATargetCollectionCriteria;

#define kPATargetCollectionCriteriaArray @"and", @"or", @"not", @"unknown", nil

/**
 Class to store target collection details
 */
@interface PATargetCollection : NSObject

@property(nonatomic) NSString *targetPropertyListString;
@property(nonatomic) NSString *criteriaString;
@property(nonatomic) NSString *name;
@property(nonatomic) NSString *group;
@property(nonatomic) NSString *language;
@property(nonatomic) NSString *tgDescription;

@property(nonatomic) NSArray *targetPropertyList;
@property(nonatomic) PATargetCollectionCriteria criteria;

@property(nonatomic) BOOL isApplicable; // Set to true if corresponding target collection script evaluates to true for the given condition; false otherwise

- (void)executeTargetCollectionRule;

- (instancetype)initWithTargetProperty:(NSString *)property
                                  name:(NSString *)name
                                 group:(NSString *)group
                              language:(NSString *)language;

@end
