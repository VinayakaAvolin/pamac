//
//  PAParser.h
//  PAParserService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface PAParser : NSObject

- (NSDictionary *)dictionaryFromXmlAtPath:(NSString *)xmlPath;

- (BOOL)verifyCerificateForXmlAtPath:(NSString *)xmlPath
                      certificateTag:(NSString *)certTag;

- (NSDictionary *)dictionaryFromXmlString:(NSString *)xmlStr;

@end
