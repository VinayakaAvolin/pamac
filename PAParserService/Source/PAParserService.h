//
//  PAParserService.h
//  PAParserService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Cocoa/Cocoa.h>

//! Project version number for PAParserService.
FOUNDATION_EXPORT double PAParserServiceVersionNumber;

//! Project version string for PAParserService.
FOUNDATION_EXPORT const unsigned char PAParserServiceVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <PAParserService/PublicHeader.h>


#import <PAParserService/PAXmlParser.h>
#import <PAParserService/PAXmlParser.h>
#import <PAParserService/PAManifestParser.h>
#import <PAParserService/PADataSetInfo.h>
#import <PAParserService/PATargetCollection.h>
#import <PAParserService/PATargetProperty.h>

