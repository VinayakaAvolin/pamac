//
//  PAManifestParser.m
//  PAParserService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PAManifestParser.h"
#import "PADataSetInfo.h"
#import "PAParserConstants.h"
#import "PACertificate.h"
#import "PATargetCollection.h"
#import  "PACertificateUtility.h"

#include "PACertificateValidator.hpp"

NSString *const kPAManifestParserDataSetKey = @"data-set";
NSString *const kPAManifestParserFileKey = @"file";

NSString *const kPAManifestParserPropertyKey = @"property";
NSString *const kPAManifestParserPropertyNameKey = @"_name";
NSString *const kPAManifestParserPropertyValueKey = @"__text";

NSString *const kPAManifestParserPropertyFilename = @"filename";
NSString *const kPAManifestParserPropertySource = @"source";
NSString *const kPAManifestParserPropertyTarget = @"target";
NSString *const kPAManifestParserPropertyChecksum = @"fchecksum";
NSString *const kPAManifestParserPropertyFilters = @"filters";
NSString *const kPAManifestParserPropertyMachineLevel = @"MachineLevel";

NSString *const kPAManifestParserProfileSetKey = @"profile-set-mac";

NSString *const kPAManifestFiltersKey = @"filters";
NSString *const kPAManifestScriptKey = @"script";

NSString *const kPAManifestParserPropertyGroupKey = @"_group";
NSString *const kPAManifestParserPropertyLanguageKey = @"_language";

@implementation PAManifestParser
/*
 <filters>
 <script name="All Clients" language="SPRT" group="General"><![CDATA[and|%Win32_OperatingSystem::Win32_OperatingSystem::OSName%|Operating System|!=|XYZ]]></script>
 <script name="All Clients for SupportPackage" language="SPRT" group="SupportPackage"><![CDATA[and|%Win32_OperatingSystem::Win32_OperatingSystem::OSName%|Operating System|!=|XYZ]]></script>
 <script name="BaseBuild" language="SPRT" group="SupportPackage"><![CDATA[and|%ClientSkin::?::ClientSkin%|ClientSkin|=|basebuild]]></script>
 <script name="BaseBuild" language="SPRT" group="SupportPackage"><![CDATA[and|%ClientSkin::?::ClientSkin%|ClientSkin|=|basebuild]]></script>
 <script name="BMCConfigMgr Service Stopped" language="SPRT" group="Environment"><![CDATA[and|%WindowsServiceDetails::*::NameOfService%|WindowsServiceState|~*|"BMCConfigMgr:Running"]]></script>
 <script name="Cisco VPN" language="SPRT" group="VPN"><![CDATA[or|%InstalledSoftware::*::DisplayName%|Add/Remove Programs List|**|Cisco AnyConnect Secure Mobility Client
 or|%InstalledSoftware::*::DisplayName%|Add/Remove Programs List|**|Cisco Systems VPN
 or|%InstalledSoftwareX64::*::DisplayName%|Add/Remove Programs List X64|**|Cisco AnyConnect Secure Mobility Client
 or|%InstalledSoftwareX64::*::DisplayName%|Add/Remove Programs List X64|**|Cisco Systems VPN]]></script>
 
 ...
 
 </filters>
 */
- (void)parsedTargetCollectionFromManifest:(NSDictionary *)srcManifestDict
                                completion:(PAManifestParserCompletionBlock)completionBlock {
  NSMutableArray *tgCollectionList = [[NSMutableArray alloc]init];
  NSArray *filtersDict = [srcManifestDict valueForKey:kPAManifestFiltersKey];
  NSArray *targetCollectionList = [filtersDict valueForKey:kPAManifestScriptKey];
  for (NSDictionary *tgCollection in targetCollectionList) {
    PATargetCollection *targetCollection = [self parseTargetCollection:tgCollection];
    if (targetCollection) {
      [tgCollectionList addObject:targetCollection];
    }
  }
  completionBlock(true, tgCollectionList);
}

/*
 Sample manifest XML Dictionary:
 
 EG:
 ------------------
 <data-set>
 <file>
 <property name="filename">exit_code.zip</property>
 <property name="source">%SERVERROOT%data\</property>
 <property name="target">%DirUserServer%data\</property>
 <property name="fchecksum">bfa81a47a7cefb214edb4d9ca4a864e8</property>
 </file>
 <file>
 <property name="filename">a351c228-2ec5-4998-9e76-5d700133c91c.1.zip</property>
 <property name="source">%SERVERROOT%data\</property>
 <property name="target">%DirUserServer%data\sprt_url\</property>
 <property name="fchecksum">b4706da527893e538165f8f0b3a09b29</property>
 <property name="filters">avolin</property>
 <property name="MachineLevel">0</property>
 </file>
 ------------------
 
 */
- (void)parsedDataSetFromManifest:(NSDictionary *)srcManifestDict
                       completion:(PAManifestParserCompletionBlock)completionBlock {
  NSMutableArray *dataSetList = [[NSMutableArray alloc]init];
  NSArray *dataSetDict = [srcManifestDict valueForKey:kPAManifestParserDataSetKey];
  NSArray *files = [dataSetDict valueForKey:kPAManifestParserFileKey];
  
  for (NSDictionary *fileDict in files) {
    PADataSetInfo *dataSetInfo = [self parseFileProperties:fileDict];
    if ( nil != dataSetInfo && ![self dataSetMatchingDataSet:dataSetInfo inList:dataSetList]) {
      [dataSetList addObject:dataSetInfo];
    }
  }
  
  /// TODO: Return entire data-set list
  completionBlock(true, dataSetList);
}
-(PADataSetInfo *)dataSetMatchingDataSet:(PADataSetInfo *)dataSet inList:(NSArray *)dataSets {
  NSArray *filtered = [dataSets filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(fileName == %@ && targetFolder == %@)", dataSet.fileName, dataSet.targetFolder]];
  if (filtered.count) {
    return filtered[0];
  }
  return nil;
}
/*
 EG:
 ------------------
 <profile-set-mac>
 <file>
 <property name="filename">defaultxml.zip</property>
 <property name="source">%SERVERROOT%data\</property>
 <property name="target">%DirUserServer%data\</property>
 <property name="fchecksum">128830b22b73b30d0651637f92725857</property>
 </file>
 </profile-set-mac>
 ------------------
 */
- (void)parsedProfileSetFromManifest:(NSDictionary *)srcManifestDict
                          completion:(PAManifestParserCompletionBlock)completionBlock {
  NSMutableArray *dataSetList = [[NSMutableArray alloc]init];
  NSArray *profileSetDict = [srcManifestDict valueForKey:kPAManifestParserProfileSetKey];
  NSDictionary *fileEntry = [profileSetDict valueForKey:kPAManifestParserFileKey];
  PADataSetInfo *dataSetInfo = [self parseFileProperties:fileEntry];
  if ( nil != dataSetInfo ) {
    [dataSetList addObject:dataSetInfo];
  }
  completionBlock(true, dataSetList);
}
/*
 [Credentials]
 Certificate=MIIDrjCCAxegAwIBAgIBATANBgkqhkiG9w0BAQQFADCBgzEUMBIGA1UECxMLRW5naW5lZXJpbmcxCzAJBgNVBAYTAlVTMSswKQYDVQQDEyJTdXBwb3J0U29mdCwgSW5jLiBFbXBsb3llZSBMaWNlbnNlMTEwLwYDVQQKEyhTdXBwb3J0U29mdCwgSW5jLiBQcmltYXJ5IENBIENlcnRpZmljYXRlMB4XDTE3MDIyMTA1MzYxMVoXDTE4MDIyMTA1MzYxMVowezEpMCcGA1UEChMgU3VwcG9ydFNvZnQsIEluYy4gTDIgQ2VydGlmaWNhdGUxFDASBgNVBAsTC0VuZ2luZWVyaW5nMQswCQYDVQQGEwJVUzErMCkGA1UEAxMiU3VwcG9ydFNvZnQsIEluYy4gRW1wbG95ZWUgTGljZW5zZTCBnzANBgkqhkiG9w0BAQEFAAOBjQAwgYkCgYEAp+sH7255M+ja+Wzozh7sRZn2wobjpqdV/V+CPqWcBVqfRo1/8m7DGrUmxGjork87+BYuILSjFdLqYMeuILF0GIZwfdkmPN3kb2TcdWhmIvG++2eKuUphZbXKhRiyibRrqcBw/0jD6e6h3jPF/ynfBlZDPmxuel7J0Q4uDg/mg1ECAwEAAaOCATcwggEzMB0GA1UdDgQWBBRGCV5Ol+Z6G8QTX90u3xugEuop+zAxBglghkgBhvhCAQ0EJBYiU3VwcG9ydFNvZnQsIEluYy4gRW1wbG95ZWUgTGljZW5zZTARBglghkgBhvhCAQEEBAMCBLAwCwYDVR0PBAQDAgXgMAwGA1UdEwEB/wQCMAAwgbAGA1UdIwSBqDCBpYAUBIL0XisQRJ5xMWOe+gf2bfasuWOhgYmkgYYwgYMxFDASBgNVBAsTC0VuZ2luZWVyaW5nMQswCQYDVQQGEwJVUzErMCkGA1UEAxMiU3VwcG9ydFNvZnQsIEluYy4gRW1wbG95ZWUgTGljZW5zZTExMC8GA1UEChMoU3VwcG9ydFNvZnQsIEluYy4gUHJpbWFyeSBDQSBDZXJ0aWZpY2F0ZYIBADANBgkqhkiG9w0BAQQFAAOBgQAVpx08w2trj1i8+di/Y5EnZzSDd2vSLBYsMxPaT8IjNLIofQR8I399PO03uPc9nqBs1G8295OTzuN0ETG0O8b0eicWKK7cfM6AC3xWHzcI8MeNZPOUPkUfAt/n2PNlMofPDpKbHU6ryMiQ3Ehqe1nDSdcVCRiW5CjZham3pbjl9w==
 Signature=cpOmWV1hSE30LJt2gmpbpdzDZ4H4mkk819ioJ1LlZ3vtQl39tlVDxwaoZI/r4r6+CDUC0VBxe32RQwa1+AXve5BwJxBp7sCc1KxsZD6XKwMbEnuHogcoST3YGrbuU+5KeJWpqheZVkD5Ssa06o0tN1v9bSaXbdIFOiGN1hbYiV8=
 
 */
- (void)validateManifestCertificate:(NSString *)manifestXml
                 andSyncCertificate:(NSString *)syncCertificatePath
                     withCompletion:(PAManifestParserCompletionBlock)completionBlock {
  /// TODO: Incomplete certificate validation; Currently only certificate common names are matched
  //    NSString *certificateDataStr = self.certificateDataString;
  //    BOOL doesCertificateMatch = [PACertificateUtility doesCertificateMatchesWithSyncCertificate:certificateDataStr];
  char *xmlPath = (char *)[manifestXml UTF8String];
  char *certificatePath = (char *)[syncCertificatePath UTF8String];
  bool doesCertificateMatch = verifyCertificateCommonName(xmlPath, certificatePath);
  
  completionBlock(doesCertificateMatch, nil);
}

#pragma mark - Private methods

/**
 Parses file properties into PADataSetInfo object
 
 EG:
 ------------------
 <file>
 <property name="filename">defaultxml.zip</property>
 <property name="source">%SERVERROOT%data\</property>
 <property name="target">%DirUserServer%data\</property>
 <property name="fchecksum">e7013604f4867a486a3a5f3171363beb</property>
 </file>
 ------------------
 
 @param fileEntry NSDictionary *
 @return PADataSetInfo *
 */
- (PADataSetInfo *)parseFileProperties:(NSDictionary *)fileEntry {
  NSArray *properties = [fileEntry valueForKey:kPAManifestParserPropertyKey];
  NSString *src;
  NSString *target;
  NSString *checksum;
  NSString *fileName;
  NSString *filters;
  BOOL isMachineLevel = false;
  
  for (NSDictionary *property in properties) {
    id propertyValue = [property valueForKey:kPAManifestParserPropertyValueKey];
    NSString *propertyName = [property valueForKey:kPAManifestParserPropertyNameKey];
    
    if ([propertyName isEqualToString: kPAManifestParserPropertyFilename]) {
      fileName = propertyValue;
      continue;
    }
    if ([propertyName isEqualToString: kPAManifestParserPropertySource]) {
      src = propertyValue;
      continue;
    }
    if ([propertyName isEqualToString: kPAManifestParserPropertyTarget]) {
      target = propertyValue;
      continue;
    }
    if ([propertyName isEqualToString: kPAManifestParserPropertyChecksum]) {
      checksum = propertyValue;
      continue;
    }
    if ([propertyName isEqualToString: kPAManifestParserPropertyFilters]) {
      filters = propertyValue;
      continue;
    }
    if ([propertyName isEqualToString: kPAManifestParserPropertyMachineLevel]) {
      isMachineLevel = [propertyValue boolValue];
      continue;
    }
  }
  PADataSetInfo *dataSetInfo = [[PADataSetInfo alloc] initWithFileName:fileName source:src target:target checksum:checksum filter:filters machineLevel:isMachineLevel];
  return dataSetInfo;
}

/*
 {
 "__text" = "and|%SPRT_ClientBinInfo::sprtcmd.exe::FileVersion%|SupportSoft Agent Version (sprtcmd.exe)|=|6.5.562";
 "_group" = "SupportSoft Client";
 "_language" = SPRT;
 "_name" = "Client Executable 6511";
 },
 {
 "__text" = "and|%SPRT_ClientBinInfo::sprtcmd.exe::FileVersion%|SupportSoft Agent Version (sprtcmd.exe)|=|6.7.325.0";
 "_group" = "SupportSoft Client";
 "_language" = SPRT;
 "_name" = "Client Executable 65sp2";
 },
 ...
 )
 */

- (PATargetCollection *)parseTargetCollection:(NSDictionary *)targetCollectionDict {
  NSString *name = [targetCollectionDict valueForKey:kPAManifestParserPropertyNameKey];
  NSString *group = [targetCollectionDict valueForKey:kPAManifestParserPropertyGroupKey];
  NSString *language = [targetCollectionDict valueForKey:kPAManifestParserPropertyLanguageKey];
  NSString *propertyString = [targetCollectionDict valueForKey:kPAManifestParserPropertyValueKey];
  
  PATargetCollection *targetCollection = [[PATargetCollection alloc]initWithTargetProperty:propertyString name:name group:group language:language];
  return targetCollection;
}

@end
