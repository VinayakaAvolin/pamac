//
//  PADataSetInfo.h
//  PAParserService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PADataSetInfo : NSObject

@property NSString *fileName;
@property NSString *sourceFolder;
@property NSString *targetFolder;
@property NSString *checksum;
@property NSString *filterString;
@property BOOL isMachineLevel;

@property NSString *sourceRootPath; // actual server path after replacing the placeholder in "sourceFolder"
@property NSString *destinationRootPath; // actual destination path in local file system after replacing the placeholder in "targetFolder" and appending the "fileSystemId"
@property NSString *destinationBasePath; // The destination base path in local file system after replacing the placeholder in "targetFolder"

@property NSString *fileSystemId; // Unique id of the Data set file entry Eg: 65aabe47-988e-482b-81bb-34d59e2c606f.38
@property BOOL isFile; // each data set entry is either a single file or a directory with intermediate directories and files

@property(nonatomic) NSArray *filterList;

- (instancetype)initWithFileName:(NSString *)fileName
                          source:(NSString *)sourceFolder
                          target:(NSString *)targetFolder
                        checksum:(NSString *)checksum
                          filter:(NSString *)filter
                    machineLevel:(BOOL)mchineLevel;

+ (instancetype)testFileSystemInfo;

- (BOOL) hasMatchingFilter:(NSString *)filter;

@end

/*
 
 Sample file info:
 
 <file>
 <property name="filename">584790ba-31c3-4c73-8b3f-3ca7b11fd926.15.zip</property>
 <property name="source">%SERVERROOT%data\</property>
 <property name="target">%DirUserServer%data\sprt_snapin\</property>
 <property name="fchecksum">3be95eb6d235a0822b98626323cb9935</property>
 <property name="filters">CSSCorp,Experience,Holmes,NTTData</property>
 <property name="MachineLevel">0</property>
 </file>
 
 */
