//
//  PAPlistWrapper.h
//  ProactiveAssistScriptWrapper
//
//  Copyright © 2018 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PAPlistWrapper : NSObject

- (instancetype)initWithFilePath: (NSString *)filePath;
- (id)valueForKeyPath:(NSString *)keyPath withSeperator:(NSString *)seperator;

@end

