//
//  PAApplicationWrapper.h
//  ProactiveAssistScriptWrapper
//
//  Copyright © 2018 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

// App attribute keys
FOUNDATION_EXPORT NSString * const kPAWrapperBundleIdentiferKey;
FOUNDATION_EXPORT NSString * const kPAWrapperDefaultLanguageKey;
FOUNDATION_EXPORT NSString * const kPAWrapperBundleNameKey;
FOUNDATION_EXPORT NSString * const kPAWrapperPackageNameKey;
FOUNDATION_EXPORT NSString * const kPAWrapperProductIDKey;
FOUNDATION_EXPORT NSString * const kPAWrapperBundleVersionKey;

// Launchd service attribute keys
FOUNDATION_EXPORT NSString * const kPAWrapperServicePathKey;
FOUNDATION_EXPORT NSString * const kPAWrapperServiceProcessIdKey;
FOUNDATION_EXPORT NSString * const kPAWrapperServiceIsStartedKey;
FOUNDATION_EXPORT NSString * const kPAWrapperServiceIsOnBootKey;
FOUNDATION_EXPORT NSString * const kPAWrapperServiceStateKey;

@interface PAApplicationWrapper : NSObject

@property (nonatomic) NSDictionary *appAttributes;
@property (nonatomic) NSDictionary *serviceAttributes;

- (instancetype) initWithAppPath: (NSString *)appPath;
- (instancetype) initWithLaunchdServiceIdentifier: (NSString *)serviceIdentifier;

@end


