//
//  PAFilesystemWrapper.m
//  ProactiveAssistScriptWrapper
//
//  Copyright © 2018 Avolin. All rights reserved.
//

#import "PAFilesystemWrapper.h"
NSString * const kPAWrapperFileCreationDateKey = @"WrapperFileCreationDate";
NSString * const kPAWrapperFileIsArchivedKey = @"WrapperFileIsArchived";
NSString * const kPAWrapperFileIsCompressedKey = @"WrapperFileIsCompressed";
NSString * const kPAWrapperFileIsHiddenKey = @"WrapperFileIsHidden";
NSString * const kPAWrapperFileIsReadOnlyKey = @"WrapperFileIsReadOnly";
NSString * const kPAWrapperFileIsSystemKey = @"WrapperFileIsSystem";
NSString * const kPAWrapperFileLastAccessDateKey = @"WrapperFileLastAccessDate";
NSString * const kPAWrapperFileLastWriteDateKey = @"WrapperFileLastWriteDate";
NSString * const kPAWrapperFileNameKey = @"WrapperFileName";
NSString * const kPAWrapperFilePathKey = @"WrapperFilePath";
NSString * const kPAWrapperFileSizeKey = @"WrapperFileSize";


@implementation PAFilesystemWrapper

+ (NSDictionary *)attributesForFileAtPath: (NSString *)path {
    NSMutableDictionary *attribs = nil;
    NSDictionary* fileAttribs = [[NSFileManager defaultManager] attributesOfItemAtPath: path error:nil];
    if (fileAttribs) {
        attribs = [NSMutableDictionary new];
        attribs[kPAWrapperFileCreationDateKey] = fileAttribs[NSFileCreationDate];
        attribs[kPAWrapperFileSizeKey] = fileAttribs[NSFileSize];
        attribs[kPAWrapperFileIsCompressedKey] = [self isCompressedFile:path] ? @YES : @NO;
        NSURL *url = [NSURL fileURLWithPath:path];
        id isHidden, isReadOnly, lastAccessdate, lastModificationDate;
        [url getResourceValue:&isHidden forKey:NSURLIsHiddenKey error:nil];
        [url getResourceValue:&isReadOnly forKey:NSURLIsUserImmutableKey error:nil];
        attribs[kPAWrapperFileIsHiddenKey] = isHidden;
        attribs[kPAWrapperFileIsReadOnlyKey] = isReadOnly;
        attribs[kPAWrapperFileIsSystemKey] = [NSNumber numberWithBool:[fileAttribs[NSFileOwnerAccountName] isEqualToString:@"root"]];
        [url getResourceValue:&lastAccessdate forKey:NSURLContentAccessDateKey error:nil];
        attribs[kPAWrapperFileLastAccessDateKey] = lastAccessdate;
        [url getResourceValue:&lastModificationDate forKey:NSURLContentModificationDateKey error:nil];
        attribs[kPAWrapperFileLastWriteDateKey] = lastModificationDate;
        attribs[kPAWrapperFileNameKey] = [url lastPathComponent];
        attribs[kPAWrapperFilePathKey] = [NSString stringWithCString:[url URLByDeletingLastPathComponent].fileSystemRepresentation encoding:NSUTF8StringEncoding];
        
    }

    return [attribs copy];
}

+ (BOOL)isCompressedFile: (NSString *)path {
    NSFileHandle *fh = [NSFileHandle fileHandleForReadingAtPath:path];
    NSData *data = [fh readDataOfLength:4];
    uint8_t magic[4];
    [data getBytes:&magic length:4];
    [fh closeFile];
    
    if (magic[0] == 0X1F && magic[1] == 0X8B) {
        //gz , tar.gz
        return YES;
    } else if (magic[0] == 0X50 && magic[1] == 0X4B && magic[2] == 0X03 && magic[3] == 0X04) {
        //zip file format and formats based on it,
        return YES;
    } else if (magic[0] == 0X42 && magic[1] == 0X5A && magic[2] == 0X68) {
        // bz2
        return YES;
    }
    return NO;
}
@end
