//
//  PAWrapperCommand.m
//  ProactiveAssistScriptWrapper
//
//  Copyright © 2019 Avolin. All rights reserved.
//

#import "PAWrapperCommand.h"
#import "PAFilesystemWrapper.h"
#import "PAApplicationWrapper.h"
#import "PAPlistWrapper.h"

@implementation PAWrapperCommand
- (nullable id)performDefaultImplementation {
    // Get the command and argument
    NSDictionary * theArguments = [self evaluatedArguments];
    id result = nil;
    
    // Execute the command
    NSLog(@"%@", self.commandDescription.commandName);
    if([self.commandDescription.commandName isEqualToString:@"get file attributes"]) {
        result = [self fileAttributesForPath:[theArguments objectForKey: @"path"]];
    } else if ([self.commandDescription.commandName isEqualToString:@"get app attributes"]) {
        result = [self appAttributesForPath:[theArguments objectForKey: @"appPath"]];
    } else if([self.commandDescription.commandName isEqualToString:@"get service attributes"]) {
        result = [self serviceAttributesForIdentifier:[theArguments objectForKey:@"svcPath"]];
    } else if([self.commandDescription.commandName isEqualToString:@"get value from plist file"]) {
        result = [[self valueFromPlist:theArguments] description];
    }
    
    return result;
    
}

- (NSDictionary *)fileAttributesForPath:(NSString *)path {
    return [PAFilesystemWrapper attributesForFileAtPath: path];
}

- (NSDictionary *)appAttributesForPath:(NSString *)path {
    PAApplicationWrapper *appWrapper = [[PAApplicationWrapper alloc] initWithAppPath:path];
    return appWrapper.appAttributes;
}

- (NSDictionary *)serviceAttributesForIdentifier: (NSString *)identifier {
    PAApplicationWrapper *svcWrapper = [[PAApplicationWrapper alloc] initWithLaunchdServiceIdentifier:identifier];
    return svcWrapper.serviceAttributes;
}

- (id)valueFromPlist:(NSDictionary *)desc {
    PAPlistWrapper *plistWrapper = [[PAPlistWrapper alloc] initWithFilePath:desc[@"plistPath"]];
    return [plistWrapper valueForKeyPath:desc[@"keyPath"] withSeperator:desc[@"seperator"]];
}

@end
