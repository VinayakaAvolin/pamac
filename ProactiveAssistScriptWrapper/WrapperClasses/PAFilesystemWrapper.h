//
//  PAFilesystemWrapper.h
//  ProactiveAssistScriptWrapper
//
//  Copyright © 2018 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
FOUNDATION_EXPORT NSString * const kPAWrapperFileCreationDateKey;
FOUNDATION_EXPORT NSString * const kPAWrapperFileIsArchivedKey;
FOUNDATION_EXPORT NSString * const kPAWrapperFileIsCompressedKey;
FOUNDATION_EXPORT NSString * const kPAWrapperFileIsHiddenKey;
FOUNDATION_EXPORT NSString * const kPAWrapperFileIsReadOnlyKey;
FOUNDATION_EXPORT NSString * const kPAWrapperFileIsSystemKey;
FOUNDATION_EXPORT NSString * const kPAWrapperFileLastWriteDateKey;
FOUNDATION_EXPORT NSString * const kPAWrapperFileNameKey;
FOUNDATION_EXPORT NSString * const kPAWrapperFilePathKey;
FOUNDATION_EXPORT NSString * const kPAWrapperFileSizeKey;
FOUNDATION_EXPORT NSString * const kPAWrapperFileLastAccessDateKey;

@interface PAFilesystemWrapper : NSObject

+ (NSDictionary *)attributesForFileAtPath: (NSString *)path;
@end

