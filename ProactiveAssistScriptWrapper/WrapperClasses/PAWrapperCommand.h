//
//  PAWrapperCommand.h
//  ProactiveAssistScriptWrapper
//
//  Copyright © 2019 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PAWrapperCommand : NSScriptCommand
- (nullable id)performDefaultImplementation;
@end

NS_ASSUME_NONNULL_END
