//
//  PAApplicationWrapper.m
//  ProactiveAssistScriptWrapper
//
//  Copyright © 2018 Avolin. All rights reserved.
//

#import "PAApplicationWrapper.h"
#import "PAPlistWrapper.h"
#include <unistd.h>
#include <sys/types.h>

NSString *const kPAWrapperBundleIdentiferKey = @"WrapperBundleIdentifer";
NSString *const kPAWrapperDefaultLanguageKey = @"WrapperDefaultLanguage";
NSString *const kPAWrapperBundleNameKey = @"WrapperBundleName";
NSString *const kPAWrapperPackageNameKey = @"WrapperPackageName";
NSString *const kPAWrapperProductIDKey = @"WrapperProductID";
NSString *const kPAWrapperBundleVersionKey = @"WrapperBundleVersion";


NSString * const kPAWrapperServicePathKey = @"WrapperServicePath";
NSString * const kPAWrapperServiceProcessIdKey = @"WrapperServiceProcessId";
NSString * const kPAWrapperServiceIsStartedKey = @"WrapperServiceIsStarted";
NSString * const kPAWrapperServiceIsOnBootKey = @"WrapperServiceIsOnBoot";
NSString * const kPAWrapperServiceStateKey = @"WrapperServiceState";

NSString * const kPAWrapperLaunchctlArgsKey = @"WrapperLaunchctlArgs";
NSString * const kPAWrapperGrepArgsKey = @"WrapperGrepArgs";

@implementation PAApplicationWrapper

- (instancetype)initWithAppPath: (NSString *)appPath
{
    self = [super init];
    if (self) {
        _appAttributes = [self appAttributesForBundle:appPath];
    }
    return self;
}
- (instancetype)initWithLaunchdServiceIdentifier: (NSString *)serviceIdentifier
{
    self = [super init];
    if (self) {
        _serviceAttributes = [self serviceAttributesForService:serviceIdentifier];
    }
    return self;
}

- (NSDictionary *)appAttributesForBundle:(NSString*)bundlePath {
    
    NSBundle *appBundle = [NSBundle bundleWithPath:bundlePath];
    if (appBundle) {
        NSDictionary *infoDict = appBundle.infoDictionary;
        NSMutableDictionary *attibs = [NSMutableDictionary new];
        attibs[kPAWrapperBundleIdentiferKey] = infoDict[(__bridge NSString *)kCFBundleIdentifierKey];
        attibs[kPAWrapperDefaultLanguageKey] = infoDict[(__bridge NSString *)kCFBundleDevelopmentRegionKey];
        attibs[kPAWrapperBundleNameKey] = infoDict[(__bridge NSString *)kCFBundleNameKey];
        attibs[kPAWrapperPackageNameKey] = infoDict[(__bridge NSString *)kCFBundleIdentifierKey];
        attibs[kPAWrapperProductIDKey] = infoDict[(__bridge NSString *)kCFBundleIdentifierKey];
        attibs[kPAWrapperBundleVersionKey] = infoDict[(__bridge NSString *)kCFBundleVersionKey];
        return [attibs copy];
    }
    return nil;
}

- (NSDictionary *)serviceAttributesForService: (NSString *)serviceIdentifier {
    NSMutableDictionary *attibs = [NSMutableDictionary new];
    NSString *servicePlistPath = [self extractInfo:@"path" forService:serviceIdentifier];
    PAPlistWrapper *wrapper = [[PAPlistWrapper alloc] initWithFilePath:servicePlistPath];
    attibs[kPAWrapperServicePathKey] = [wrapper valueForKeyPath:@"Program" withSeperator:nil];
    attibs[kPAWrapperServiceIsOnBootKey] = [wrapper valueForKeyPath:@"RunAtLoad" withSeperator:nil] ? [wrapper valueForKeyPath:@"RunAtLoad" withSeperator:nil] : @NO;
    NSString *pid = [self extractInfo:@"pid =" forService:serviceIdentifier];
    attibs[kPAWrapperServiceProcessIdKey] = pid;
    attibs[kPAWrapperServiceStateKey] = [self extractInfo:@"state =" forService:serviceIdentifier];
    attibs[kPAWrapperServiceIsStartedKey] = pid ? @YES : @NO;
    return [attibs copy];
}
- (NSString *)extractInfo: (NSString *)exp forService:(NSString *)serviceIdentifier {
    NSString *serviceDesc = [NSString stringWithFormat:@"gui/%d/%@", getuid(),serviceIdentifier];
    NSDictionary *argsMap = @{
                              kPAWrapperLaunchctlArgsKey: @[@"print", serviceDesc],
                              kPAWrapperGrepArgsKey: @[exp]
                              };
    NSString *output = [self runLaunchctlPipeline:argsMap];
    NSString *extractedInfo = nil;
    if ([output length]) {
        extractedInfo = [[output componentsSeparatedByString:@"="][1] stringByTrimmingCharactersInSet:
                         [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    }
    return extractedInfo;
}
- (NSString *)runLaunchctlPipeline: (NSDictionary *)argsMap {
    NSTask *lctlTask = [[NSTask alloc] init];
    NSTask *grepTask = [[NSTask alloc] init];
    
    [lctlTask setLaunchPath: @"/bin/launchctl"];
    [grepTask setLaunchPath: @"/usr/bin/grep"];
    
    /*
    [lctlTask setArguments: [NSArray arrayWithObjects: @"print", @"gui/501/com.malwarebytes.mbam.frontend.agent", nil]];
    [grepTask setArguments: [NSArray arrayWithObjects: @"path", nil]];*/
    
    [lctlTask setArguments:argsMap[kPAWrapperLaunchctlArgsKey]];
    [grepTask setArguments:argsMap[kPAWrapperGrepArgsKey]];
    
    /* launchctl ==> grep */
    NSPipe *pipeBetween = [NSPipe pipe];
    [lctlTask setStandardOutput: pipeBetween];
    [grepTask setStandardInput: pipeBetween];
    
    /* grep ==> output */
    NSPipe *pipeToMe = [NSPipe pipe];
    [grepTask setStandardOutput: pipeToMe];
    
    NSFileHandle *grepOutput = [pipeToMe fileHandleForReading];
    
    
    [lctlTask launch];
    [grepTask launch];
    
    NSString *output = [[NSString alloc] initWithData:[grepOutput availableData] encoding:NSUTF8StringEncoding];
    return output;
}

@end
