//
//  PAPlistWrapper.m
//  ProactiveAssistScriptWrapper
//
//  Copyright © 2018 Avolin. All rights reserved.
//

#import "PAPlistWrapper.h"
#import <PACoreServices/PACoreServices.h>

@interface PAPlistWrapper()
@property (nonatomic) PAPlistReader *reader;
@end
@implementation PAPlistWrapper

- (instancetype)initWithFilePath: (NSString *)filePath {
    self = [super init];
    if (self) {
        _reader = [[PAPlistReader alloc] initWithPlistAtPath: filePath];
        if (_reader == nil) {
            return nil;
        }
    }
    return self;
}

- (id)valueForKeyPath:(NSString *)keyPath withSeperator:(NSString *)seperator {
    return [self.reader valueForKeyPath:keyPath withSeparator:seperator];
}

@end
