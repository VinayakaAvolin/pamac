//
//  main.m
//  ProactiveAssistScriptWrapper
//
//  Copyright © 2018 Avolin. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "PAFilesystemWrapper.h"
#import "PAPlistWrapper.h"
#import "PAApplicationWrapper.h"
#import "PAPlistWrapper.h"

int main(int argc, const char * argv[]) {
    /*
    NSLog(@"%@", [PAFilesystemWrapper attributesForFileAtPath:@"/Users/vinayakas/.ssh"]);
    NSLog(@"%@", [PAFilesystemWrapper attributesForFileAtPath:@"/Users/vinayakas/Desktop/Resume.zip"]);
    NSLog(@"%@", [PAFilesystemWrapper attributesForFileAtPath:@"/bin"]);
    PAPlistWrapper *plistWrapper = [[PAPlistWrapper alloc] initWithFilePath:@"/Users/vinayakas/Library/LaunchAgents/org.virtualbox.vboxwebsrv.plist"];
    PAApplicationWrapper *serviceWrapper1 = [[PAApplicationWrapper alloc] initWithLaunchdServiceIdentifier:@"com.malwarebytes.mbam.frontend.agent"];
    PAApplicationWrapper *serviceWrapper2 = [[PAApplicationWrapper alloc] initWithLaunchdServiceIdentifier:@"97T89359R6.com.avolin.supportsoft"];
    PAApplicationWrapper *appWrapper = [[PAApplicationWrapper alloc] initWithAppPath:@"/Applications/Calculator.app"];
    NSLog(@"%@", [serviceWrapper1 serviceAttributes]);
    NSLog(@"%@", [serviceWrapper2 serviceAttributes]);

    NSLog(@"%@", [appWrapper appAttributes]);
    NSLog(@"%@", [plistWrapper valueForKeyPath:@"Sockets/Listeners/SockFamily" withSeperator: @"/"]);
*/
    return NSApplicationMain(argc, argv);
}
