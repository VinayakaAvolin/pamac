//
//  PAIniService.h
//  PAIniService
//
//  Copyright © 2017 Avolin. All rights reserved.

//

#import <Cocoa/Cocoa.h>

//! Project version number for PAIniService.
FOUNDATION_EXPORT double PAIniServiceVersionNumber;

//! Project version string for PAIniService.
FOUNDATION_EXPORT const unsigned char PAIniServiceVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <PAIniService/PublicHeader.h>

#import <PAIniService/PACfgFileProcessor.h>
