//
//  PACfgFileProcessor.m
//  PAIniService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <PAFileSystemService/PAFileSystemService.h>
#import <PALoggerService/PALoggerService.h>
#import "PACfgFileProcessor.h"
#include "iniparser.h"

@implementation PACfgFileProcessor

+ (NSDictionary *)parseConfigFileAtPath:(NSString *)filePath {
  PAFileSystemManager *fsManager = [[PAFileSystemManager alloc] init];
  if (![fsManager doesFileExist:filePath]) {
    return nil;
  }
  dictionary  *ini ;
  NSMutableDictionary *dict = [NSMutableDictionary new];
  ini = iniparser_load([filePath cStringUsingEncoding:NSUTF8StringEncoding]);
  if (ini == NULL) {
    [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"Cannot parse file: %@; So retry by copy-pasting the existing file content. This removes any invisible/spucious characters from the file\n",filePath];
    if ([PACfgFileProcessor handleParseErrorForFileAtPath:filePath]) {
      ini = iniparser_load([filePath cStringUsingEncoding:NSUTF8StringEncoding]);
      if (ini == NULL) {
        [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"Cannot parse file: %@;",filePath];
        return nil ;
      }
    }
  }
  
  for (int i=0 ; i < ini->n ; i++) {
    if (ini->key[i]) {
      const char *val = dictionary_get(ini, ini->key[i], "UNDEF");
      NSString *key = [NSString stringWithCString:(const char *)ini->key[i] encoding:NSUTF8StringEncoding];
      if (val)  {
        NSString *value = [NSString stringWithCString:val encoding:NSUTF8StringEncoding];
        dict[key] = value;
      }
    }
  }
  return [dict copy];
}

+ (BOOL)handleParseErrorForFileAtPath:(NSString *)filePath {
  NSData *fileData = [[NSFileManager defaultManager] contentsAtPath:filePath];
  if (fileData) {
    NSError *error = nil;
    NSString* fileDataStr = [[NSString alloc] initWithData:fileData encoding:NSUTF8StringEncoding];
    // Overwrite the file content with existing content; This removes any invisible/spucious characters from the file
    return [fileDataStr writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:&error];
  }
  return false;
}
@end
