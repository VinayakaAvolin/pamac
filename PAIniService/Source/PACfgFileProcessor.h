//
//  PACfgFileProcessor.h
//  PAIniService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PACfgFileProcessor : NSObject

+ (NSDictionary *)parseConfigFileAtPath:(NSString *)filePath;

@end
