//
//  AppDelegate.h
//  ProactiveAssistScriptEventNotifier
//
//  Copyright © 2018 Avolin. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>


@end

