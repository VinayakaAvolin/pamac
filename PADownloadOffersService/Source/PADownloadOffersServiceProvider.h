//
//  PADownloadOffersServiceProvider.h
//  PADownloadOffersService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PAServiceProvider.h"

@interface PADownloadOffersServiceProvider : PAServiceProvider<PADownloadManagerUIDelegate>

@end
