//
//  PADownloadOffersServiceProvider.m
//  PADownloadOffersService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <PACoreServices/PACoreServices.h>
#import "PADownloadOffersServiceProvider.h"
#import "PAServiceInfo.h"
#import "PADownloadOfferConfigurationXmlParser.h"
#import "PAServiceConstants.h"

NSString *const kPADODmgExtension = @"dmg";
NSString *const kPADOPkgExtension = @"pkg";

NSString *const kPADOMountPoint = @"/Volumes";

NSString *const kPADOOpenDmgScript = @"do shell script \"hdiutil attach '%@'; open '%@'\"";
NSString *const kPADOInstallPkgScript1 = @"do shell script \"/usr/sbin/installer -pkg '%@' -target /\"";
NSString *const kPADOInstallPkgScript = @"/usr/sbin/installer -pkg '%@' -target /";
NSString *const kPADOInstallPkgHelperScript = @"do shell script \"'/Applications/ProactiveAssist.app/Contents/Tools/PAPrivilegeToolHelper' scriptRunner shell script %@\"";
NSString *const kPADOISDmgMountedScript = @"do shell script \"hdiutil info | grep ^/dev/disk | grep -o '/Volumes/.*'\"";
NSString *const kPADOISUnMountDmgScript = @"do shell script \"hdiutil eject '%@'\"";

@interface PADownloadOffersServiceProvider () {
  PADownloadManager *_downloadManager;
  PAFileSystemManager *_fileSystemManager;
  PAServiceInfo *_serviceInfo;
  NSString *_dmgName;
}
@property (nonatomic, retain) PAFileSystemManager *fileSystemManager;

@property (nonatomic) NSString *downloadPath;
@property (nonatomic) PAServiceCallback callback;

@end
@implementation PADownloadOffersServiceProvider


- (void)executeService:(PASupportService)service category:(PASupportServiceCategory)category info:(id<PAServiceInfoProtocol>)info callback:(PAServiceCallback)callback {
  _serviceInfo = (PAServiceInfo *)info;
  self.callback = callback;
  
  switch (service) {
    case PASupportServiceDownload:
      [self downloadOffer];
      break;
    case PASupportServiceInstall:
      [self installOffer];
      break;
    default:
      self.callback(false, nil, [self installCallbackInfo]);
      break;
  }
}

- (void)downloadOffer {
  if (!_serviceInfo.guid || !_serviceInfo.version) {
    [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"[Download offers] Either guid or version is nil"];
    
    _callback(false, nil, [self installCallbackInfo]);
    return;
  }
  _downloadManager = [[PADownloadManager alloc] init];
  
  [self parseInput];
}

- (void)didFinishInputParsing {
  
  switch (_serviceInfo.service) {
    case PASupportServiceDownload:
      [self download];
      break;
    case PASupportServiceInstall:
      [self install];
      break;
    default:
      [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"[Download offers] Unsupported service = [%d]",_serviceInfo.service];
      self.callback(false, nil, [self installCallbackInfo]);
      break;
  }
}

- (void)download {
  if (!_serviceInfo.downloadUrl ||
      _serviceInfo.installerType == PASupportServiceInstallFileTypeNone) {
    [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"[Download offers] Either download url not present or not a dmg/pkg"];
    
    self.callback(false, nil, [self installCallbackInfo]);
    return;
  }
  if (_downloadManager.networkReachabilityStatus == PANetworkReachabilityStatusNotReachable) {
    [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"Cannot download; network unreachable"];
    self.callback(false, nil, [self installCallbackInfo]);
    return;
  }
  _fileSystemManager = [[PAFileSystemManager alloc] init];
  NSString *path = [_fileSystemManager stringByExpandingPath: _serviceInfo.downloadUrl];
  NSString *downloadFolder = [_fileSystemManager temporaryDownloadsFolderPath];
  _dmgName = path.lastPathComponent;
  NSString *fileExtension = [self downloadFileExtension];
  downloadFolder = [downloadFolder stringByAppendingPathComponent:_dmgName];
  if (fileExtension) {
    downloadFolder = [downloadFolder stringByAppendingPathExtension:fileExtension];
  }
  self.downloadPath = downloadFolder;
  PADownloadableFileInfo *downloadInfo = [[PADownloadableFileInfo alloc] initWithUrl:path destination:self.downloadPath fileSize:0 checksum:nil identifier:_dmgName];
  _downloadManager.uiDelegate = self;
  [_downloadManager addFileToDownload:downloadInfo];
  [self addDownloadNotifications]; // add download notifications
  [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"[Download offers] Download from url [%@]",_serviceInfo.downloadUrl];
}

- (void)installOffer {
  if (!_serviceInfo.guid || !_serviceInfo.version || !_serviceInfo.downloadedItemPath) {
    [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"[Download offers] Either guid/version/downloaded item path is nil"];
    _callback(false, nil, [self installCallbackInfo]);
    return;
  }
  [self parseInput];
}
- (void)install {
  if (!_serviceInfo.downloadedItemPath ||
      _serviceInfo.installerType == PASupportServiceInstallFileTypeNone) {
    [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"[Download offers] Either downloaded item path is nil or not a dmg/pkg"];
    
    self.callback(false, nil, [self installCallbackInfo]);
    return;
  }
  self.downloadPath = _serviceInfo.downloadedItemPath;
  
  switch (_serviceInfo.installerType) {
    case PASupportServiceInstallFileTypeDmg:
      [self checkIfDmgMounted];
      break;
    case PASupportServiceInstallFileTypePackage:
      [self installDownloadedItem];
      break;
    default:
      self.callback(false, nil, [self installCallbackInfo]);
      break;
  }
}
#pragma mark - Download related notification observers

- (void)addDownloadNotifications {
  // Add download progress notification
  [[NSDistributedNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveNotification:) name:kPASupportServiceDownloadCancelNotificationName object:kPASupportServiceDownloadOfferNotificationObjectName];
}
- (void)removeDownloadNotifications {
  // Add download progress notification
  [[NSDistributedNotificationCenter defaultCenter] removeObserver:self];
}
- (void)didReceiveNotification:(NSNotification *)notification {
  [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"[Download offers] cancel all downloads"];
  [_downloadManager cancelAllDownloads];
  _downloadManager.uiDelegate = nil;
  self.callback(false, nil, [self installCallbackInfo]);
}

#pragma mark - PADownloadManagerUIDelegate methods

- (void) didFinishAll {
  [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"[Download offers] Finished downloading to path [%@]",self.downloadPath];
  
  _downloadManager.uiDelegate = nil;
  NSDictionary *responseDict =@{kPASupportServiceResponseKeyGuid: _serviceInfo.guid, kPASupportServiceResponseKeyVersion: _serviceInfo.version, kPASupportServiceResponseKeyDownloadPath: self.downloadPath, kPASupportServiceResponseKeyStatus: @(PADownloadStatusComplete), kPASupportServiceResponseKeyInstallerType: _serviceInfo.installerKind};
  self.callback(true, nil, responseDict);
  [self removeDownloadNotifications]; // remove download notifications
}

- (void) didReachIndividualProgress:(float)progress onDownloadTask:(PADownloadTask *) task {
  NSDictionary *responseDict =@{kPASupportServiceResponseKeyGuid: _serviceInfo.guid, kPASupportServiceResponseKeyVersion: _serviceInfo.version, kPASupportServiceResponseKeyProgressValue: @(progress), kPASupportServiceResponseKeyStatus: @(PADownloadStatusInProgress)};
  [[NSDistributedNotificationCenter defaultCenter] postNotificationName:kPASupportServiceDownloadStatusNotificationName object:kPASupportServiceDownloadOfferNotificationObjectName userInfo:responseDict deliverImmediately:true];
}

- (void)didHitDownloadErrorOnTask:(PADownloadTask*)task {
  [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"[Download offers] Failed to download to path [%@] Error [%@]",self.downloadPath, task.error];
  [_downloadManager cancelAllDownloads];
  _downloadManager.uiDelegate = nil;
  NSDictionary *responseDict =@{kPASupportServiceResponseKeyGuid: _serviceInfo.guid, kPASupportServiceResponseKeyVersion: _serviceInfo.version, kPASupportServiceResponseKeyStatus: @(PADownloadStatusFailed)};
  self.callback(false, task.error, responseDict);
  [self removeDownloadNotifications]; // remove download notifications
}
#pragma mark -

- (void)installDownloadedItem {
  switch (_serviceInfo.installerType) {
    case PASupportServiceInstallFileTypeDmg:
      [self openDmg];
      break;
    case PASupportServiceInstallFileTypePackage:
      [self installPackageInstaller];
      break;
    default:
      self.callback(false, nil, [self installCallbackInfo]);
      break;
  }
}

- (void)checkIfDmgMounted {
  NSString *dmgOpenCheckScriptStr = kPADOISDmgMountedScript;
  PAAppleScript *script = [[PAAppleScript alloc]initWithScriptString:dmgOpenCheckScriptStr];
  [script runWithCallBack:^(BOOL success, NSError *error, id result) {
    if (success && [result isKindOfClass:[NSString class]]) {
      [self checkIfDmgMountPathExistsIn:(NSString *)result];
    } else {
      [self openDmg];
    }
  }];
}

- (void)checkIfDmgMountPathExistsIn:(NSString *)inPaths {
  NSArray *listOfMountPaths = [inPaths componentsSeparatedByString:@"\r"];
  if (listOfMountPaths.count) {
    NSString *dmgMountPath = kPADOMountPoint;
    dmgMountPath = [dmgMountPath stringByAppendingPathComponent:_dmgName];
    if ([listOfMountPaths containsObject:dmgMountPath]) {
      NSString *unmountDmgScriptStr = [NSString stringWithFormat:kPADOISUnMountDmgScript,dmgMountPath];
      PAAppleScript *script = [[PAAppleScript alloc]initWithScriptString:unmountDmgScriptStr];
      [script runWithCallBack:^(BOOL success, NSError *error, id result) {
        [self openDmg];
      }];
    } else {
      [self openDmg];
    }
  } else {
    [self openDmg];
  }
}

- (void)openDmg {
  [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"[Download offers] Open dmg [%@]",self.downloadPath];
  
  NSString *openDmgScriptStr = [NSString stringWithFormat:kPADOOpenDmgScript, self.downloadPath, self.downloadPath];
  
  //Run Shell Script to delete com.Apple.Quarantine external attribute
  @try {
    [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"[Download offers] Remove Quarantine attribute"];
    NSTask *task = [[NSTask alloc] init];
    [task setLaunchPath:@"/bin/bash"];
    [task setArguments:@[ @"-c", [@"xattr -rd com.apple.quarantine " stringByAppendingString: self.downloadPath]]];
    [task launch];
    [task waitUntilExit];
  } @catch (NSException *exception) {
    [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"[Download offers] File Quarantine Error: [%@]",exception];
  }
  //End
  
  PAAppleScript *script = [[PAAppleScript alloc]initWithScriptString:openDmgScriptStr];
  [script runWithCallBack:^(BOOL success, NSError *error, id result) {
    [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"[Download offers] Open dmg status [%d] Error [%@]",success, error];
    self.callback(success, error, [self installCallbackInfo]);
  }];
}
- (void)openPkg {
  BOOL isSuccess = [[NSWorkspace sharedWorkspace] openFile:self.downloadPath withApplication:nil];
  self.callback(isSuccess, nil, [self installCallbackInfo]);
}
- (void)installPackageInstaller {
  [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"[Download offers] Install pkg installer [%@]",self.downloadPath];
  
  //Run Shell Script to delete com.Apple.Quarantine external attribute
  @try {
    [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"[Download offers] Remove Quarantine attribute"];
    NSTask *task = [[NSTask alloc] init];
    [task setLaunchPath:@"/bin/bash"];
    [task setArguments:@[ @"-c", [@"xattr -rd com.apple.quarantine " stringByAppendingString: self.downloadPath]]];
    [task launch];
    [task waitUntilExit];
  } @catch (NSException *exception) {
    [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"[Download offers] File Quarantine Error: [%@]",exception];
  }
  //End
  
  
  NSString *installPkgScriptStr = [NSString stringWithFormat:kPADOInstallPkgScript,self.downloadPath];
  NSString *installPkgViaPrivilegeToolScriptStr = [NSString stringWithFormat:kPADOInstallPkgHelperScript,installPkgScriptStr];
  PAAppleScript *script = [[PAAppleScript alloc]initWithScriptString:installPkgViaPrivilegeToolScriptStr];
  
  [script runWithCallBack:^(BOOL success, NSError *error, id result) {
    [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"[Download offers] Install pkg status [%d] Error [%@]",success, error];
    self.callback(success, error, [self installCallbackInfo]);
  }];
}

- (NSDictionary *)installCallbackInfo {
  NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
  [dict setValue:_serviceInfo.guid forKey:kPASupportServiceResponseKeyGuid];
  [dict setValue:_serviceInfo.version forKey:kPASupportServiceResponseKeyVersion];
  [dict setValue:_serviceInfo.installerKind forKey:kPASupportServiceResponseKeyInstallerType];
  return dict;
}

- (NSString *)downloadFileExtension {
  NSString *fileExtension = nil;
  switch (_serviceInfo.installerType) {
    case PASupportServiceInstallFileTypeDmg:
      fileExtension = kPADODmgExtension;
      break;
    case PASupportServiceInstallFileTypePackage:
      fileExtension = kPADOPkgExtension;
      break;
    default:
      break;
  }
  return fileExtension;
}

- (void)parseInput {
  PADownloadOfferConfigurationXmlParser *configParser = [[PADownloadOfferConfigurationXmlParser alloc] initWithServiceInfo:_serviceInfo];
  [configParser parseInputConfigurationWithCompletion:^(BOOL success, PAServiceInfo *serviceInfo) {
    [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"[Download offers] Status of parsing download schema [%d]",success];
    if (success) {
      _serviceInfo = serviceInfo;
      [self didFinishInputParsing];
    } else {
      _callback(false, nil, [self installCallbackInfo]);
    }
  }];
}

@end
