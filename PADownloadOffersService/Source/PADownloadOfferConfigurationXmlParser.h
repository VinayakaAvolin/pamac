//
//  PADownloadOfferConfigurationXmlParser.h
//  PADownloadOffersService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <PACoreServices/PACoreServices.h>
#import "PAServiceInfo.h"

typedef void(^PADownloadOfferConfigurationXmlParserCompletionBlock)(BOOL success, PAServiceInfo *serviceInfo);

@interface PADownloadOfferConfigurationXmlParser : PAXmlParser

- (instancetype)initWithServiceInfo:(PAServiceInfo *)serviceInfo;

- (void)parseInputConfigurationWithCompletion:(PADownloadOfferConfigurationXmlParserCompletionBlock)completionBlock;

@end
