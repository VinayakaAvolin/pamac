//
//  PADownloadOfferConfigurationXmlParser.m
//  PADownloadOffersService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PADownloadOfferConfigurationXmlParser.h"

NSString *const kPADOContentSetKey = @"content-set";
NSString *const kPADOContentKey = @"content";
NSString *const kPADOFieldSetKey = @"field-set";
NSString *const kPADOFieldKey = @"field";
NSString *const kPADONameKey = @"_name";
NSString *const kPADOValueKey = @"value";
NSString *const kPADOTextKey = @"__text";
NSString *const kPADODownloadUrlKey = @"sdc_sd_DownloadURL:::1";
NSString *const kPADOInstallationArgumentsKey = @"sdc_sd_InstallationArguments:::1";
NSString *const kPADORestartKey = @"sdc_sd_requiresRestart:::1";
NSString *const kPADOSilentInstallKey = @"sdc_sd_silent:::1";
NSString *const kPADOVerificationPathKey = @"sdc_sd_VerificationPath:::1";

@interface PADownloadOfferConfigurationXmlParser () {
  NSString *_inputXmlPath;
  PAServiceInfo *_serviceInfo;
  PAFileSystemManager *_fsManager;
}

@end

@implementation PADownloadOfferConfigurationXmlParser

- (instancetype)initWithServiceInfo:(PAServiceInfo *)serviceInfo {
  self = [super init];
  if (self && serviceInfo.guid && serviceInfo.version) {
    _serviceInfo = serviceInfo;
    _fsManager = [[PAFileSystemManager alloc]init];
    [self setupInputConfigurationXml];
  } else {
    self = nil;
  }
  return self;
}
/*
 Array of fields from xml
 {
 "_name" = "sdc_sd_DownloadURL:::1";
 value =     {
 "__text" = "https://www.macupdate.com/download/25774/installpkg";
 "_name" = "sccf_field_value_char";
 "_type" = s;
 };
 },
 {
 "_name" = "sdc_sd_InstallationArguments:::1";
 value =     (
 {
 "__text" = "/S";
 "_name" = "sccf_field_value_char";
 "_type" = s;
 },
 {
 "__text" = DMG;
 "_name" = "sccf_field_value_char";
 "_type" = s;
 }
 );
 },
 {
 "_name" = "sdc_sd_requiresRestart:::1";
 value =     {
 "__text" = no;
 "_name" = "sccf_field_value_char";
 "_type" = s;
 };
 },
 {
 "_name" = "sdc_sd_silent:::1";
 value =     {
 "__text" = no;
 "_name" = "sccf_field_value_char";
 "_type" = s;
 };
 },
 {
 "_name" = "sdc_sd_VerificationPath:::1";
 value =     {
 "__text" = "%ProgramFiles%7-Zip\\7zFM.exe##File Version##1";
 "_name" = "sccf_field_value_char";
 "_type" = s;
 };
 }
 */
- (void)parseInputConfigurationWithCompletion:(PADownloadOfferConfigurationXmlParserCompletionBlock)completionBlock {
  // NSString *defaultXmlFilePath = [fsManager localDefaultXmlFilePath];
  NSDictionary *xmlDict = [self dictionaryFromXmlAtPath:_inputXmlPath];
  if (xmlDict) {
    NSDictionary *contentSetDict = [xmlDict valueForKey:kPADOContentSetKey];
    if ([contentSetDict isKindOfClass:[NSDictionary class]]) {
      NSDictionary *contentDict = [contentSetDict valueForKey:kPADOContentKey];
      if ([contentDict isKindOfClass:[NSDictionary class]]) {
        NSDictionary *fieldSetDict = [contentDict valueForKey:kPADOFieldSetKey];
        if ([fieldSetDict isKindOfClass:[NSDictionary class]]) {
          NSArray *fieldsList = [fieldSetDict valueForKey:kPADOFieldKey];
          if ([fieldsList isKindOfClass:[NSArray class]]) {
            NSString *downloadUrl = nil;
            NSString *installerKind = nil;
            NSString *installVerificationPath = nil;
            BOOL doesRequireRestart = false;
            BOOL isSilentInstall = false;
            // read download url
            NSDictionary *downloadUrlFieldDict = [self dictionaryMatchingName:kPADODownloadUrlKey inList:fieldsList];
            NSString *downloadUrlValue = [self valueFromFieldDictionary:downloadUrlFieldDict];
            if ([downloadUrlValue isKindOfClass:[NSString class]]) {
              downloadUrl = downloadUrlValue;
              
              // If URL as macro "%SERVERROOT%" then replace to host url
              if([downloadUrl containsString:@"%SERVERROOT%"]){
                NSArray *items = [downloadUrl componentsSeparatedByString:@"%SERVERROOT%"];
                
                PAAppConfiguration *appConfiguration = [PAConfigurationManager appConfigurations];
                downloadUrl = appConfiguration.serverBaseUrlPath;
                downloadUrl = [downloadUrl stringByAppendingFormat:@"%@",items[1]];
              }
            }
            
            // read installerKind name
            NSDictionary *installArgsFieldDict = [self dictionaryMatchingName:kPADOInstallationArgumentsKey inList:fieldsList];
            if ([installArgsFieldDict isKindOfClass:[NSDictionary class]]) {
              NSDictionary *valueDictList = [installArgsFieldDict valueForKey:kPADOValueKey];
              if ([valueDictList isKindOfClass:[NSDictionary class]]) {
                NSString *value = [valueDictList valueForKey:kPADOTextKey];
                if ([value isKindOfClass:[NSString class]]) {
                  installerKind = value;
                }
              }
            }
            // Read does require restart parameter value
            NSDictionary *restartFieldDict = [self dictionaryMatchingName:kPADORestartKey inList:fieldsList];
            NSString *restartValue = [self valueFromFieldDictionary:restartFieldDict];
            if ([restartValue isKindOfClass:[NSString class]]) {
              doesRequireRestart = [restartValue isEqualToString:@"no"] ? false : true;
            }
            
            // Read is silent installation parameter value
            NSDictionary *silentInstallFieldDict = [self dictionaryMatchingName:kPADOSilentInstallKey inList:fieldsList];
            NSString *silentValue = [self valueFromFieldDictionary:silentInstallFieldDict];
            if ([silentValue isKindOfClass:[NSString class]]) {
              isSilentInstall = [silentValue isEqualToString:@"no"] ? false : true;
            }
            // Read verification path
            NSDictionary *verPathFieldDict = [self dictionaryMatchingName:kPADOVerificationPathKey inList:fieldsList];
            NSString *verPathValue = [self valueFromFieldDictionary:verPathFieldDict];
            if ([verPathValue isKindOfClass:[NSString class]]) {
              //NSArray *pathValueArray = [verPathValue componentsSeparatedByString:@"#"];
              //installVerificationPath = [pathValueArray objectAtIndex:0];
              NSRange range = [verPathValue rangeOfString:@"#"];
              if(range.location != NSNotFound)
              {
                installVerificationPath = [verPathValue substringWithRange:NSMakeRange(0, range.location-1)];
              } else {
                installVerificationPath = verPathValue;
              }
            }
            
            PAServiceInfo *serviceInfo = [_serviceInfo copy];
            serviceInfo.downloadUrl = downloadUrl;
            serviceInfo.installVerificationPath = installVerificationPath;
            serviceInfo.isSilentInstall = isSilentInstall;
            serviceInfo.doesRequireRestart = doesRequireRestart;
            serviceInfo.installerKind = installerKind;
            completionBlock(true, serviceInfo);
            return;
          }
        }
      }
    }
  }
  completionBlock(false, _serviceInfo);
}

- (NSDictionary *)dictionaryMatchingName:(NSString *)name inList:(NSArray *)inputList {
  if (inputList.count) {
    NSArray *filtered = [inputList filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(_name == %@)", name]];
    if (filtered.count) {
      return filtered[0];
    }
  }
  
  return nil;
}

- (id)valueFromFieldDictionary:(NSDictionary *)fieldDict {
  if ([fieldDict isKindOfClass:[NSDictionary class]]) {
    NSDictionary *valueDict = [fieldDict valueForKey:kPADOValueKey];
    if ([valueDict isKindOfClass:[NSDictionary class]]) {
      NSString *value = [valueDict valueForKey:kPADOTextKey];
      if ([value isKindOfClass:[NSString class]]) {
        return value;
      }
    }
  }
  return nil;
}

/// constructs app specific protection xml path
/// Eg: ~/Library/Application\ Support/ProactiveAssist/Experience92/data/sprt_download/973283e0-37dd-4ab7-be76-7f6c9d2471f9.5/973283e0-37dd-4ab7-be76-7f6c9d2471f9.5.xml
- (void)setupInputConfigurationXml {
  PAAppConfiguration *appConfiguration = [PAConfigurationManager appConfigurations];
  NSString *appProtectionFolderName = [NSString stringWithFormat:@"%@.%@",_serviceInfo.guid, _serviceInfo.version];
  NSString *appProtectionXmlFileName = [NSString stringWithFormat:@"%@.%@.xml",_serviceInfo.guid, _serviceInfo.version];
  
  NSString *downloadOfferXmlPath = appConfiguration.downloadFolderPath;
  downloadOfferXmlPath = [_fsManager stringByExpandingPath:downloadOfferXmlPath];
  downloadOfferXmlPath = [downloadOfferXmlPath stringByAppendingPathComponent:appProtectionFolderName];
  downloadOfferXmlPath = [downloadOfferXmlPath stringByAppendingPathComponent:appProtectionXmlFileName];
  if ([_fsManager doesFileExist:downloadOfferXmlPath]) {
    _inputXmlPath = downloadOfferXmlPath;
  }
}
@end
