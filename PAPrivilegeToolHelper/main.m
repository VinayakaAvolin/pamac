//
//  main.m
//  PAPrivilegeToolHelper
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PACommandRequestHandler.h"

int main(int argc, const char * argv[]) {
  @autoreleasepool {
    // insert code here...
    /// args:
    /// script_path/script_string scriptType scriptContentType
    /// 1st argument : script type (apple/shell)
    /// 2nd argument : 3rd argument type (either path or script)
    /// 3rd and reamining arguments: either script path or script string
    NSArray *arguments = [[NSProcessInfo processInfo] arguments];
    //        NSArray *arguments = @[@"test", @"scriptRunner", @"shell", @"script", @"cp", @"/Users/username/Desktop/PATest/Enpoint info/preferences.plist", @"/Library/Preferences/SystemConfiguration/preferences.plist"];
    //        NSArray *arguments = @[@"test", @"runCommand", @"finder"];
    
    PACommandRequestHandler *cmdHandler = [[PACommandRequestHandler alloc]initWithArguments:arguments];
    [cmdHandler executeRequest];
  }
  return 0;
}
