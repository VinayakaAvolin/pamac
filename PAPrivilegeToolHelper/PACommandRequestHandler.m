//
//  PACommandRequestHandler.m
//  PAPrivilegeToolHelper
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <PAIpcService/PAIpcService.h>
#import <PACoreServices/PACoreServices.h>
#import "PACommandRequestHandler.h"
#import "NSString+Additions.h"

@interface PACommandRequestHandler () {
  NSString *_scriptPath;
  NSString *_scriptString;
  NSString *_scriptType;
  PAScriptType _scriptTypeEnum;
  NSString *_contentType;
  PAContentType _contentTypeEnum;
  
  NSString *_command;
  PAPTCommand _commandEnum;
  
  NSString *_runCommandOption;
  NSString *_runCommandArgs;
}

@end

@implementation PACommandRequestHandler

- (instancetype)initWithArguments:(NSArray *)args {
  self = [super init];
  if (self) {
    if (args.count < 3) {
      self = nil;
    } else {
      _command = args[1];
      _commandEnum = [self commandEnumValue];
      
      switch (_commandEnum) {
        case PAPTCommandScriptRunner:
          [self parseScriptRunnerArguments:args];
          break;
        case PAPTCommandRunCommand:
          [self parseRunCommandArguments:args];
          break;
        default:
          break;
      }
      
    }
  }
  return self;
}
- (void)parseRunCommandArguments:(NSArray *)args {
  if (args.count < 3) {
    return;
  }
  _runCommandOption = args[2];
  
  NSUInteger remainingArgs = args.count - 1 - 1; // exclude command and app path
  
  if (remainingArgs >= 1) {
    NSArray *subArray = [args subarrayWithRange:NSMakeRange(2, remainingArgs)];
    
    _runCommandArgs = [subArray componentsJoinedByString:@" "];
  }
}
- (void)parseScriptRunnerArguments:(NSArray *)args {
  if (args.count < 5) {
    return;
  }
  _contentType = args[3];
  _contentTypeEnum = [self contentTypeEnumValue];
  _scriptType = args[2];
  _scriptTypeEnum = [self scriptTypeEnumValue];
  switch (_contentTypeEnum) {
    case PAContentTypePath:
      _scriptPath = args[4];
      break;
    case PAContentTypeScript:
    {
      NSUInteger remainingArgs = args.count - 1 - 3;// exclude command, app path, script type, and content type
      
      if (remainingArgs >= 1) {
        NSArray *subArray = [args subarrayWithRange:NSMakeRange(4, remainingArgs)];
        
        NSMutableArray *updatedArgs = [[NSMutableArray alloc] init];
        if (_scriptTypeEnum == PAScriptTypeShell) {
          for (NSString *arg in subArray) {
            NSString *spaceEscapedArg = [NSString stringWithFormat:@"'%@'",arg];
            [updatedArgs addObject:spaceEscapedArg];
          }
        } else {
          updatedArgs = [subArray mutableCopy];
        }
        
        _scriptString = [updatedArgs componentsJoinedByString:@" "];
      }
      
      break;
    }
      
      
    default:
      break;
  }
}
- (void)executeRequest {
  switch (_commandEnum) {
    case PAPTCommandScriptRunner:
      [self executeScript];
      break;
    case PAPTCommandRunCommand:
      [self executeRunCommand];
      break;
    default:
      break;
  }
}
- (void)executeScript {
  PAScriptInfo *scriptInfo = nil;
  if (_scriptPath) {
    scriptInfo = [[PAScriptInfo alloc]initWithPath:_scriptPath type:_scriptTypeEnum mode:PAScriptModeElevated];
  } else if (_scriptString) {
    scriptInfo = [[PAScriptInfo alloc]initWithScript:_scriptString type:_scriptTypeEnum mode:PAScriptModeElevated];
  }
  if (scriptInfo) {
    [[PAInterProcessManager sharedManager] executeScript:scriptInfo withReply:^(BOOL success, NSError *error, id result) {
      printf( "%d", !success);
      int exitCode = success ? 0 : 1;
      exit(exitCode);
    }];
    
    [[NSRunLoop currentRunLoop] run];
  } else {
    exit(1);
  }
}
- (void)executeRunCommand {
  if (!_runCommandOption) {
    exit(1);
  }
  NSString *command = _runCommandOption;
  if (_runCommandArgs) {
    command = [command stringByAppendingFormat:@" %@",_runCommandArgs];
  }
  BOOL isSuccess = [PALaunchServiceManager runCommand:command];
  int exitCode = isSuccess ? 0 : 1;
  exit(exitCode);
}

- (PAContentType)contentTypeEnumValue {
  PAContentType contentType = PAContentTypeUnknown;
  if (![_contentType isEmptyOrHasOnlyWhiteSpaces]) {
    NSArray *contentTypeArray = [[NSArray alloc] initWithObjects:kPAContentTypeArray];
    NSUInteger index = [contentTypeArray indexOfObject:_contentType];
    if (NSNotFound != index) {
      contentType = (PAContentType)index;
    }
  }
  return contentType;
}
- (PAScriptType)scriptTypeEnumValue {
  PAScriptType scriptType = PAScriptTypeNone;
  if (![_scriptType isEmptyOrHasOnlyWhiteSpaces]) {
    NSArray *scriptTypeArray = [[NSArray alloc] initWithObjects:kPAScriptTypeArray];
    NSUInteger index = [scriptTypeArray indexOfObject:_scriptType];
    if (NSNotFound != index) {
      scriptType = (PAScriptType)index;
    }
  }
  return scriptType;
}
- (PAPTCommand)commandEnumValue {
  PAPTCommand cmdEnum = PAPTCommandUnknown;
  if (![_command isEmptyOrHasOnlyWhiteSpaces]) {
    NSArray *commandArray = [[NSArray alloc] initWithObjects:kPAPTCommandArray];
    NSUInteger index = [commandArray indexOfObject:_command];
    if (NSNotFound != index) {
      cmdEnum = (PAPTCommand)index;
    }
  }
  return cmdEnum;
}

@end
