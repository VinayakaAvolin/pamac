//
//  PACommandRequestHandler.h
//  PAPrivilegeToolHelper
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
  PAContentTypeUnknown,
  PAContentTypePath,
  PAContentTypeScript
} PAContentType;
/// order of elements in the kPAContentTypeArray should match the PAContentType enum order
#define kPAContentTypeArray @"unknown", @"path", @"script", nil

typedef enum {
  PAPTCommandUnknown,
  PAPTCommandScriptRunner,
  PAPTCommandRunCommand
} PAPTCommand;
/// order of elements in the kPAPTCommandArray should match the PAPTCommand enum order
#define kPAPTCommandArray @"unknown", @"scriptRunner", @"runCommand", nil

@interface PACommandRequestHandler : NSObject

- (instancetype)initWithArguments:(NSArray *)args;
- (void)executeRequest;

@end
