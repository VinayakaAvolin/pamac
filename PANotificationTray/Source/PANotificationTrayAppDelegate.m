//
//  PANotificationTrayAppDelegate.m
//  PANotificationTray
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <PACoreServices/PACoreServices.h>
#import "PANotificationTrayAppDelegate.h"
#import "PAMainViewController.h"
#import "NSStoryboard+Additions.h"

NSString *const kMainHtmlFileName = @"sample";
NSString *const kHtmlFileExtension = @"html";
NSString *const kPAMainWindowControllerId = @"MainWindowController";

@interface PANotificationTrayAppDelegate () <NSUserNotificationCenterDelegate> {
  NSWindowController *_mainWindowController;
}
@end

@implementation PANotificationTrayAppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
  // Insert code here to initialize your application
  NSMutableArray *args = [[[NSProcessInfo processInfo] arguments] mutableCopy];
  
  if (args.count > 1) {
    [args removeObjectAtIndex:0];
  }
  [self setMainWindowController: args];
  [self addNotificationObservers];
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
  // Insert code here to tear down your application
  [self removeNotificationObservers];
}
- (BOOL)userNotificationCenter:(NSUserNotificationCenter *)center
     shouldPresentNotification:(NSUserNotification *)notification
{
  return YES;
}

- (void)addNotificationObservers {
  NSUserNotificationCenter *userNotificationCenter = [NSUserNotificationCenter defaultUserNotificationCenter];
  userNotificationCenter.delegate = self;
  
  [[NSDistributedNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveNotification:) name:kPANotificationNameNewEvent object:kPANotificationObjectName];
}
- (void)removeNotificationObservers {
  [[NSDistributedNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveNotification:(NSNotification *)notification {
  [self postUserNotificationWithInfo:notification.userInfo];
}

- (void)postUserNotificationWithInfo:(NSDictionary *)notifInfo {
  NSUserNotification *notification = [[NSUserNotification alloc] init];
  notification.title = notifInfo[kPANotifTrayServiceEventTitleKey];
  notification.informativeText = notifInfo[kPANotifTrayServiceEventMessageKey];
  notification.soundName = NSUserNotificationDefaultSoundName;
  [[NSUserNotificationCenter defaultUserNotificationCenter] deliverNotification:notification];
  
}

#pragma mark - Private methods

- (void)setMainWindowController:(NSArray*)arguments {
  _mainWindowController = [self mainWindowController];
  PAMainViewController *mainvc = [PAMainViewController mainViewController];
  mainvc.webPageLink = @"testpage/testpage";
  _mainWindowController.contentViewController = mainvc;
  [mainvc setAppType:PANotificationApp];
  
  NSRect screenRect = [[NSScreen mainScreen] visibleFrame];
  NSRect windowRect = _mainWindowController.window.frame;
  windowRect.origin.x = NSWidth(screenRect) - NSWidth(windowRect) + NSMinX(screenRect);
  windowRect.origin.y = screenRect.origin.y;
  [_mainWindowController.window setFrameOrigin:windowRect.origin];
  //    [_mainWindowController.window makeKeyAndOrderFront:self];
  //    [_mainWindowController showWindow:self]; /// to show the window
  [mainvc setCommandLineArguments:arguments];
  NSString *filePath = [self testHtmlPath];
  NSURL *url = [NSURL fileURLWithPath:filePath];
  NSURLRequest *request = [NSURLRequest requestWithURL:url];
  [mainvc loadWebPage:request];
}

- (NSWindowController *)mainWindowController {
  NSStoryboard *storyboard = [NSStoryboard mainAppStoryboard];
  NSWindowController *mainWindowVc = [storyboard instantiateControllerWithIdentifier:kPAMainWindowControllerId];
  return mainWindowVc;
}

- (NSString *)testHtmlPath {
  PAFileSystemManager *fsManager = [[PAFileSystemManager alloc]init];
  NSString *folderPath = [fsManager agentCoreFolderPath];
  folderPath = [folderPath stringByAppendingPathComponent:@"bootstrapper.html"];
  
  //    NSString *folderPath = @"/Volumes/Backup/Projects/Git/Ayuda/ProactiveAssist/HTML/bootstrapper.html";
  folderPath = [folderPath stringByExpandingTildeInPath];
  return folderPath;
}


@end
