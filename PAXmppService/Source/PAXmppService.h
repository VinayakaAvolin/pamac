//
//  PAXmppService.h
//  PAXmppService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Cocoa/Cocoa.h>

//! Project version number for PAXmppService.
FOUNDATION_EXPORT double PAXmppServiceVersionNumber;

//! Project version string for PAXmppService.
FOUNDATION_EXPORT const unsigned char PAXmppServiceVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <PAXmppService/PublicHeader.h>

#import <PAXmppService/PAXmppServiceManager.h>
#import <PAXmppService/PAXmppServiceConstants.h>
#import <PAXmppService/PAXmppConnectInfo.h>
