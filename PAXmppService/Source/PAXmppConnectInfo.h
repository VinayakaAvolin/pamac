//
//  PAXmppConnectInfo.h
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PAXmppConnectInfo : NSObject

@property(nonatomic, readonly) NSString *userName;
@property(nonatomic, readonly) NSString *password;
@property(nonatomic, readonly) NSString *domainName;
@property(nonatomic, readonly) NSString *host;
@property(nonatomic, readonly) NSNumber *port;
@property(nonatomic, readonly) NSString *licenceKey;

- (instancetype)initWithDomain:(NSString *)domain
                          host:(NSString *)host
                          port:(NSNumber *)port
                    licenceKey:(NSString *)licenceKey
                      userName:(NSString *)userName
                      password:(NSString *)password;
@end
