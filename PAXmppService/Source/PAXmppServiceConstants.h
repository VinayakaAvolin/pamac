//
//  PAXmppServiceConstants.h
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifndef PAXmppServiceConstants_h
#define PAXmppServiceConstants_h

extern NSString *const kPADOFieldSets;
extern NSString *const kPADOAlertContent;
extern NSString *const kPADOAlertTime;
extern NSString *const kPADOAnimateWindow;
extern NSString *const kPADOWindowLength;
extern NSString *const kPADOWindowLook;
extern NSString *const kPADOWindowWidth;
extern NSString *const kPADOAcknowledgeRequired;
extern NSString *const kPADODisplayTimeEnd;
extern NSString *const kPADOTransperency;
extern NSString *const kPADOCertificate ;
extern NSString *const kPADOCertificateData;
extern NSString *const kPADOCertificateSignature;
extern NSString *const kPADORequestChannel;
extern NSString *const kPADORequestType;

#endif /* PAXmppServiceConstants_h */
