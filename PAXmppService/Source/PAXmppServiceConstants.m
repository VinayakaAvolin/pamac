//
//  PAXmppServiceConstants.m
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PAXmppServiceConstants.h"

NSString *const kPADOFieldSets = @"FieldSets";
NSString *const kPADOAlertContent = @"Alert Content";
NSString *const kPADOAlertTime = @"Alert Time";
NSString *const kPADOAnimateWindow = @"animatewindow";
NSString *const kPADOWindowLength = @"windowlength";
NSString *const kPADOWindowLook = @"windowlook";
NSString *const kPADOWindowWidth = @"windowwidth";
NSString *const kPADOAcknowledgeRequired = @"AcknowledgeRequired";
NSString *const kPADODisplayTimeEnd = @"displayTimeend";
NSString *const kPADOTransperency = @"transperency";
NSString *const kPADOCertificate = @"Certificate";
NSString *const kPADOCertificateData = @"CertificateData";
NSString *const kPADOCertificateSignature = @"CertificateSignature";
NSString *const kPADORequestChannel = @"SSRequestChannel";
NSString *const kPADORequestType = @"SSRequestType";
