//
//  PAXmppServiceManager.m
//  PAXmppService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PAXmppServiceManager.h"
#import "XMPPHandler.h"
#import "PAXmppServiceConstants.h"
#import "NSString+Additions.h"

NSString *const kPAXMPPCertificateTag = @"Certificate=";
NSString *const kPAXMPPSignatureTag = @"Signature=";

@interface PAXmppServiceManager () <XMPPHandlerDelegate> {
  PAXmppConnectInfo *_xmppConfiguration;
}

@end

@implementation PAXmppServiceManager

+ (id)sharedManager {
  static PAXmppServiceManager *sharedManager = nil;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    sharedManager = [[self alloc] init];
  });
  return sharedManager;
}

- (BOOL)connectToXmppWithConfiguration:(PAXmppConnectInfo *)configuration {
  BOOL isConnected = false;
  NSString *xmppHost = configuration.domainName;
  NSNumber *xmppPort = configuration.port;
  NSString *userName = configuration.userName;
  NSString *password = configuration.password;
  
  if (!xmppHost || !xmppPort || !userName || !password) {
    return isConnected;
  }
  _xmppConfiguration = configuration;
  
  XMPPHandler *xmppHandler = [XMPPHandler defaultXMPPHandler];
  [xmppHandler setDelegate:self];
  [xmppHandler setHostName:xmppHost];
  [xmppHandler setHostPort:xmppPort];
  [xmppHandler setUserId:userName];
  [xmppHandler setUserPassword:password];
  isConnected = [xmppHandler connectToXMPPServer];
  [xmppHandler registerUser];
  
  return isConnected;
}

- (void)disconnectFromXmpp {
  XMPPHandler *xmppHandler = [XMPPHandler defaultXMPPHandler];
  [xmppHandler disconnectFromXMPPServer];
}
#pragma mark - XMPPHandlerDelegate methods

- (void)didRecieveMessage:(XMPPMessage *)message {
  NSString *messageBody = message.body;
  NSError *error = nil;
  
  //    messageBody = [messageBody stringByReplacingOccurrencesOfString:@"\\\\\\" withString:@"\\\""];
  NSData *data = [messageBody dataUsingEncoding:NSUTF8StringEncoding];
  id json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
  NSMutableDictionary *payload = nil;
  
  if ([json isKindOfClass:[NSDictionary class]]) {
    payload = [(NSDictionary *)json mutableCopy];
  }
  /// parse field set
  NSMutableDictionary *fieldSetDict = [[self dictionaryFromJsonString:payload[kPADOFieldSets]] mutableCopy];
  
  if (fieldSetDict) {
    NSMutableArray *fieldSetsList = [[NSMutableArray alloc] init];
    // parse alert content
    NSDictionary *alertContent = [self dictionaryFromJsonString:fieldSetDict[kPADOAlertContent]];
    if (alertContent) {
      [fieldSetsList addObject:alertContent];
    }
    
    // parse alert time
    NSDictionary *alertTime = [self dictionaryFromJsonString:fieldSetDict[kPADOAlertTime]];
    if (alertTime) {
      [fieldSetsList addObject:alertTime];
    }
    
    // parse animate window
    NSDictionary *animateWindow = [self dictionaryFromJsonString:fieldSetDict[kPADOAnimateWindow]];
    if (animateWindow) {
      [fieldSetsList addObject:animateWindow];
    }
    
    // parse window length
    NSDictionary *windowLength = [self dictionaryFromJsonString:fieldSetDict[kPADOWindowLength]];
    if (windowLength) {
      [fieldSetsList addObject:windowLength];
    }
    
    // parse window look
    NSDictionary *windowLook = [self dictionaryFromJsonString:fieldSetDict[kPADOWindowLook]];
    if (windowLook) {
      [fieldSetsList addObject:windowLook];
    }
    
    // parse window width
    NSDictionary *windowWidth = [self dictionaryFromJsonString:fieldSetDict[kPADOWindowWidth]];
    if (windowWidth) {
      [fieldSetsList addObject:windowWidth];
    }
    
    // parse acknowledge required
    NSDictionary *ackRequired = [self dictionaryFromJsonString:fieldSetDict[kPADOAcknowledgeRequired]];
    if (ackRequired) {
      [fieldSetsList addObject:ackRequired];
    }
    
    // parse display time end
    NSDictionary *displayTimeEnd = [self dictionaryFromJsonString:fieldSetDict[kPADODisplayTimeEnd]];
    if (displayTimeEnd) {
      [fieldSetsList addObject:displayTimeEnd];
    }
    
    // parse transparency
    NSDictionary *transparency = [self dictionaryFromJsonString:fieldSetDict[kPADOTransperency]];
    if (transparency) {
      [fieldSetsList addObject:transparency];
    }
    
    payload[kPADOFieldSets] = fieldSetsList;
  }
  
  //// Commented the below code for Certificate Verification as we do not require this.
    
  //NSString *certificateInfoStr = payload[kPADOCertificate];
  //NSString *certificateDataString = [certificateInfoStr subStringBetweenTags:kPAXMPPCertificateTag and:kPAXMPPSignatureTag trimNewLine:YES];
  //if (certificateDataString) {
  //  payload[kPADOCertificateData] = certificateDataString;
  //}
  //NSString *signatureString = [certificateInfoStr subStringFromTag:kPAXMPPSignatureTag needToExcludeTag:YES];
  //if (signatureString) {
  //  payload[kPADOCertificateSignature] = signatureString;
  //}
  [_delegate didReceiveRealTimeAlert:payload];
}


- (NSDictionary *)dictionaryFromJsonString:(NSString *)inputStr {
  if ([inputStr isKindOfClass:[NSString class]]) {
    NSData *valueData = [inputStr dataUsingEncoding:NSUTF8StringEncoding];
    id valueJson = [NSJSONSerialization JSONObjectWithData:valueData options:NSJSONReadingAllowFragments error:nil];
    if ([valueJson isKindOfClass:[NSDictionary class]]) {
      return valueJson;
    }
  }
  return nil;
}
@end
