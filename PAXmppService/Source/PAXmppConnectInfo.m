//
//  PAXmppConnectInfo.m
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PAXmppConnectInfo.h"

@implementation PAXmppConnectInfo

- (instancetype)initWithDomain:(NSString *)domain
                          host:(NSString *)host
                          port:(NSNumber *)port
                    licenceKey:(NSString *)licenceKey
                      userName:(NSString *)userName
                      password:(NSString *)password {
  self = [super init];
  if (self) {
    if (!domain || !port) {
      self = nil;
    } else {
      _host = host;
      _port = port;
      _domainName = domain;
      _licenceKey = licenceKey;
      _userName = userName;
      _password = password;
    }
  }
  return self;
}
@end
