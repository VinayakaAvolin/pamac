//
//  PAXmppServiceManager.h
//  PAXmppService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PAXmppConnectInfo.h"

@protocol PAXmppServiceManagerDelegate <NSObject>

- (void)didReceiveRealTimeAlert:(NSDictionary *)info;

@end


@interface PAXmppServiceManager : NSObject

@property (nonatomic, weak) id<PAXmppServiceManagerDelegate> delegate;

+ (id)sharedManager;

- (BOOL)connectToXmppWithConfiguration:(PAXmppConnectInfo *)configuration;
- (void)disconnectFromXmpp;

@end
