//
//  PAResourceExtractor.m
//  PAResourceExtractorService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PAResourceExtractor.h"
#import "SSZipArchive.h"

@implementation PAResourceExtractor

-(void)extractFile:(NSString*)sourcePath toLocation:(NSString*)destinationPath password:(NSString *)password completion:(void (^)(BOOL success))completionBlock {
  BOOL success = [SSZipArchive unzipFileAtPath:sourcePath
                                 toDestination:destinationPath
                                     overwrite:YES
                                      password: password
                                         error:nil];
  
  if (completionBlock != nil) completionBlock(success);
}

-(void)compressFile:(NSString*)sourcePath toLocation:(NSString*)destinationPath password:(NSString *)password completion:(void (^)(BOOL success))completionBlock {
  BOOL success = [SSZipArchive createZipFileAtPath:destinationPath
                           withContentsOfDirectory:sourcePath
                                      withPassword: password];
  
  if (completionBlock != nil) completionBlock(success);
}
@end
