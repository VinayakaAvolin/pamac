//
//  PAResourceExtractorService.h
//  PAResourceExtractorService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Cocoa/Cocoa.h>

//! Project version number for PAResourceExtractorService.
FOUNDATION_EXPORT double PAResourceExtractorServiceVersionNumber;

//! Project version string for PAResourceExtractorService.
FOUNDATION_EXPORT const unsigned char PAResourceExtractorServiceVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <PAResourceExtractorService/PublicHeader.h>

#import <PAResourceExtractorService/PAResourceExtractor.h>
