//
//  PAResourceExtractor.h
//  PAResourceExtractorService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PAResourceExtractor : NSObject

-(void)extractFile:(NSString*)sourcePath toLocation:(NSString*)destinationPath password:(NSString *)password completion:(void (^)(BOOL success))completionBlock;

-(void)compressFile:(NSString*)sourcePath toLocation:(NSString*)destinationPath password:(NSString *)password  completion:(void (^)(BOOL success))completionBlock;
@end
