#!/bin/bash
#set -x

#Check parameters
if [ $# -lt 1 ]
then
  echo "Usage: $0 <srcfile>"
  exit 1
fi

SRCPATH=$1

FILEMD5=$(md5 "${SRCPATH}" | md5)
echo ${FILEMD5}

