#!/bin/bash --

isAppKilled=0

killApp(){
    appName="$1"
    appNameWithExtension="$1".app
    if ps ax | grep -v grep | grep "$appNameWithExtension" &> /dev/null; then
        killall "$appName"
        isAppKilled=1
    fi
}
relaunchApp(){
    appName="$1"
    echo "appName"
    echo "$appName"
    if [ $isAppKilled -eq 1 ] ; then
        open -a "$appName"
    fi
}
undo(){
    protectedItems=$1
    restoreFolderPath="$2"
    appName="$3"

    #convert comma seperated path strings to path array
    protectedItems=${protectedItems#*[}
    protectedItems=${protectedItems%]*}
    IFS=', ' read -r -a protectedItemsArray <<< "$protectedItems"
    # Continue if protected data folder exists
    if [ -d "$restoreFolderPath" ]; then
        killApp "$appName"
        cd "$restoreFolderPath"
        itemCount=`ls * | wc -l`
        echo "itemCount"
        echo "$itemCount"
        if [ $itemCount -eq 0 ] ; then
            echo "Nothing to copy"
        else
            copyRestoredItemsToAppPath "${protectedItemsArray[*]}" "$restoreFolderPath"
            #get root of restore path by removing last path component from $restoreFolderPath
            restoreRootDirectoryPath=$(echo "$restoreFolderPath" | rev | cut -d'/' -f2- | rev)
            #remove the restore folder and recreate the same; at any point of time; only one restore data should be present in user's mac
            removeDirectory "$restoreRootDirectoryPath"
        fi
        relaunchApp "$appName"
    fi
}

#copies the file/folders from restored folder into app sepecific path in user's mac
copyRestoredItemsToAppPath(){
    filesPaths=($1)
    restoreFolderPath="$2"
    ## now loop through the file path array
    for itemPath in "${filesPaths[@]}"
    do
        itemName=`basename $itemPath`
        itemToCopy="$restoreFolderPath"/"$itemName"
        echo "itemToCopy"
        echo "$itemPath"
        echo "$itemToCopy"
        echo "$itemName"
        if [ -d "$itemToCopy" ]; then
            echo "$itemToCopy is a directory"
            if [ `echo $itemPath | grep $HOME` ]; then
                cp -R "$itemToCopy"/ "$itemPath"
                echo "$HOME is in $itemPath"
            else
                echo "$HOME is not in $itemPath"
                /Applications/ProactiveAssist.app/Contents/Tools/PAPrivilegeToolHelper scriptRunner shell script cp -R "$itemToCopy"/ "$itemPath"
            fi
        elif [ -f "$itemToCopy" ]; then
            echo "$itemToCopy is a file"
            if [ `echo $itemPath | grep $HOME` ]; then
                cp "$itemToCopy" "$itemPath"
                echo "$HOME is in $itemPath"
            else
                echo "$HOME is not in $itemPath"
                /Applications/ProactiveAssist.app/Contents/Tools/PAPrivilegeToolHelper scriptRunner shell script cp "$itemToCopy" "$itemPath"
            fi
        else
            echo "$itemToCopy is not valid"
        fi
    done
}

removeDirectory(){
    folderPath="$1"
    if [ -d "$folderPath" ]; then
        #delete the directory
        rm -rd "$folderPath"
    fi
}

#protectedItems=$1
#restoreFolderPath="$2"
#appName="$3"
#undo
