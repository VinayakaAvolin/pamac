#!/bin/bash --

isAppKilled=0

killApp(){
    appName="$1"
    appNameWithExtension="$1".app
    if ps ax | grep -v grep | grep "$appNameWithExtension" &> /dev/null; then
        killall "$appName"
        isAppKilled=1
    fi
}
relaunchApp(){
    appName="$1"
    echo "appName"
    echo "$appName"
    if [ $isAppKilled -eq 1 ] ; then
        open -a "$appName"
    fi
}
restore(){
    protectedItems=$1
    protectedFolderPath="$2"
    restoreFolderPath="$3"
    appName="$4"

    #convert comma seperated path strings to path array
    protectedItems=${protectedItems#*[}
    protectedItems=${protectedItems%]*}
    IFS=', ' read -r -a protectedItemsArray <<< "$protectedItems"
    # Continue if protected data folder exists
    if [ -d "$protectedFolderPath" ]; then
        killApp "$appName"
        copyAppDataToRestorePath "${protectedItemsArray[*]}" "$restoreFolderPath"
        cd "$protectedFolderPath"
        itemCount=`ls * | wc -l`
        echo "itemCount"
        echo "$itemCount"
        if [ $itemCount -eq 0 ] ; then
            echo "Nothing to copy"
        else
            copyProtectedItemsToAppPath "${protectedItemsArray[*]}" "$protectedFolderPath"
        fi
        removeDirectory "$protectedFolderPath"
        relaunchApp "$appName"
    fi
}

#copies the file/folders from selected protect folder into app sepecific path in user's mac
copyProtectedItemsToAppPath(){
    filesPaths=($1)
    protectedFolderPath="$2"
    ## now loop through the file path array
    for itemPath in "${filesPaths[@]}"
    do
        #read last path component from the itemPath
        itemName=`basename $itemPath`
        itemToCopy="$protectedFolderPath"/"$itemName"
        echo "itemToCopy"
        echo "$itemPath"
        echo "$itemToCopy"
        echo "$itemName"
        if [ -d "$itemToCopy" ]; then
            echo "$itemToCopy is a directory"
            if [ `echo $itemPath | grep $HOME` ]; then
                echo "$HOME is in $itemPath"
                cp -R "$itemToCopy"/ "$itemPath"
            else
                /Applications/ProactiveAssist.app/Contents/Tools/PAPrivilegeToolHelper scriptRunner shell script cp -R "$itemToCopy"/ "$itemPath"
                echo "$HOME is not in $itemPath"
            fi
        elif [ -f "$itemToCopy" ]; then
            echo "$itemToCopy is a file"
            if [ `echo $itemPath | grep $HOME` ]; then
                echo "$HOME is in $itemPath"
                cp "$itemToCopy" "$itemPath"
            else
                /Applications/ProactiveAssist.app/Contents/Tools/PAPrivilegeToolHelper scriptRunner shell script cp "$itemToCopy" "$itemPath"
                echo "$HOME is not in $itemPath"
            fi
        else
            echo "$itemToCopy is not valid"
        fi
    done
}


#copies the file/folders from app sepecific path into restore folder
#In case of undo action, these files are moved to app specific path
copyAppDataToRestorePath(){
    filesPaths=($1)
    restoreFolderPath="$2"
    #get root of restore path by removing last path component from $restoreFolderPath
    restoreRootDirectoryPath=$(echo "$restoreFolderPath" | rev | cut -d'/' -f2- | rev)
    #remove the restore folder and recreate the same; at any point of time; only one restore data should be present in user's mac
    removeDirectory "$restoreRootDirectoryPath"
    createDirectory "$restoreFolderPath"
    ## now loop through the file path array
    for itemPath in "${filesPaths[@]}"
    do
        echo "$itemPath"
        # or do whatever with individual element of the array
        if [[ -d "$itemPath" ]]; then
            echo "$itemPath is a directory"
            cp -R "$itemPath" "$restoreFolderPath"
        elif [[ -f "$itemPath" ]]; then
            echo "$itemPath is a file"
            cp "$itemPath" "$restoreFolderPath"
        else
            echo "$itemPath is not valid"
        fi
    done
}

createDirectory(){
    folderPath="$1"
    echo "$folderPath"
    if [[ -d "$folderPath" ]]; then
        echo "Directory exist; do nothing"
    else
        mkdir -p "$folderPath"
        chmod 0766 "$folderPath"
    fi

}
removeDirectoryContent(){
    folderPath="$1"
    if [[ -d "$folderPath" ]]; then
        #empty the directory
        cd "$folderPath"
        cd ..
        rm -rf *
    fi
}
removeDirectory(){
    folderPath="$1"
    if [[ -d "$folderPath" ]]; then
        #delete the directory
        rm -rd "$folderPath"
    fi
}
#protectedItems=$1
#protectedFolderPath="$2"
#restoreFolderPath="$3"
#appName="$4"
#restore
