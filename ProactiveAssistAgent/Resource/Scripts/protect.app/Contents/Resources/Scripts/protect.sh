#!/bin/bash --

createDirectory(){
    folderPath="$1"
    echo "$folderPath"
    mkdir -p "$folderPath"
    chmod 0766 "$folderPath"
}

#copies the file/folders to protect into appropriate destination path
copyItems(){
    filesPaths=($1)
    protectedFolderPath="$2"
    createDirectory "$protectedFolderPath"
    ## now loop through the file path array
    for itemPath in "${filesPaths[@]}"
    do
        echo "$itemPath"
        # or do whatever with individual element of the array
        if [[ -d "$itemPath" ]]; then
            echo "$itemPath is a directory"
            cp -R "$itemPath" "$protectedFolderPath"
        elif [[ -f "$itemPath" ]]; then
            echo "$itemPath is a file"
            cp "$itemPath" "$protectedFolderPath"
        else
            echo "$itemPath is not valid"
        fi
    done
    # if copy fails then folder will be empty; remove empty folder
    #removeDirectoryIfEmpty "$protectedFolderPath"
}

removeDirectoryIfEmpty(){
    protectedFolderPath="$1"
    if [ -d "$protectedFolderPath" ]; then
        #check if folder is empty
        cd "$protectedFolderPath"
        dirCount=`ls | wc -l`
        if [ $dirCount -eq 0 ] ; then
            echo "Directory empty"
            rm -rd "$protectedFolderPath"
        else
            echo "else"
        fi
    fi
}

protect(){
    paths=$1
    protectedFolderPath="$2"
    maxHistoryCount=$3
    echo "protectedFolderPath"
    echo "$protectedFolderPath"

    paths=${paths#*[}
    paths=${paths%]*}
    IFS=', ' read -r -a pathsFromArgument <<< "$paths"
    #Check whether there are 10 folders present. If not create folder with date and timestamp as folder name else delete the oldest folder and create new one.
    #check if folder exists
    if [ -d "$protectedFolderPath" ]; then
        cd "$protectedFolderPath"
        cd ..
        dirCount=`ls *.zip | wc -l`
        echo "dirCount"
        echo "$dirCount"
        if [ $dirCount -lt $maxHistoryCount ] ; then
            echo "if"
        else
            echo "else"
            #check for the oldest folder and delete it, then create new folder.
            rm -f $(ls -1t | grep .zip$ | tail -1)
        fi
        copyItems "${pathsFromArgument[*]}" "$protectedFolderPath"
    fi
}
#paths=$1
#protectedFolderPath="$2"
#maxHistoryCount=$3
#createDirectory "$2"
#protect
