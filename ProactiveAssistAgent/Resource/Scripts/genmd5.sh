#!/bin/bash
#set -x

#Check parameters
if [ $# -lt 2 ]
then
  echo "Usage: $0 <srcdir> <dstdir>"
  exit 1
fi

print_header()
{
	echo "#########################################################################"
	echo "# zCat file Format                                                      #"
	echo "# <file name>:<md5>                                                     #"
	echo "# <content dir>:<md5>                                                   #"
	echo "#########################################################################"
}

#Change delimiter for find and for loop
OIFS="$IFS"
IFS=$'\n'

SRCPATH=$1
DSTPATH=$2
SCRIPTDIR="$( dirname "$0")"

ZCATFILE="${DSTPATH}/zcat.dat"
DUMMYOUT="/dev/null"
#DUMMYOUT="${DSTPATH}/zcat.dat"

echo "# $# $* " >> "${DUMMYOUT}"

print_header >> "${ZCATFILE}"

CONTENTLIST=$(find -s ${SRCPATH} -type f -maxdepth 1 | grep -v manifest.xml)

for contentFile in ${CONTENTLIST}
do
  echo "#** contentFile =  ${contentFile} ***" >> "${DUMMYOUT}"
  contentFileNoExt="${contentFile%.*}"
  echo "#** contentFile =  ${contentFile} ***" >> "${DUMMYOUT}"
  echo "#** contentFileNoExt =  ${contentFileNoExt} ***" >> "${DUMMYOUT}"
  FILEMD5=$("${SCRIPTDIR}/genfilemd5.sh" "${contentFile}")
  echo "${contentFileNoExt}:${FILEMD5}:${contentFile}" >> "${ZCATFILE}"
done

DIRLIST=$(find "${SRCPATH}" -type d -maxdepth 1 | grep -v "${SRCPATH}\$")
for contentType in ${DIRLIST}
do
  echo "#** contentType = ${contentType} ***" >> "${DUMMYOUT}"
  CONTENTLIST=$(find "${contentType}" -type d -maxdepth 1 | grep -v "${contentType}\$")
  for contentDir in ${CONTENTLIST}
  do
    echo "#** contentDir =  ${contentDir} ***" >> "${DUMMYOUT}"
    DIRMD5=$("${SCRIPTDIR}/gendirmd5.sh" "${contentDir}")
    echo "${contentDir}:${DIRMD5}" >> "${ZCATFILE}"
  done
done

IFS="$OIFS"

