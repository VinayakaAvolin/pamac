#!/bin/bash
#set -x

#Check parameters
if [ $# -lt 1 ]
then
  echo "Usage: $0 <srcdir>"
  exit 1
fi

#Change delimiter for find and for loop
OIFS="$IFS"
IFS=$'\n'

SRCPATH=$1

DIRMD5=$(find -s "${SRCPATH}" -type f -exec md5 "{}" \; | md5)
echo ${DIRMD5}

IFS="$OIFS"

