//
//  PAAppDelegate.m
//  ProactiveAssistAgent
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PAAppDelegate.h"

@interface PAAppDelegate ()

@end

@implementation PAAppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
  // Insert code here to initialize your application
}


- (void)applicationWillTerminate:(NSNotification *)aNotification {
  // Insert code here to tear down your application
}


@end
