//
//  PAOptimizationScheduler.h
//  ProactiveAssistAgent
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PAOptimizationScheduler : NSObject

- (void)scheduleOptimization:(NSDictionary *)info;
- (void)start;
- (void)stop;

@end
