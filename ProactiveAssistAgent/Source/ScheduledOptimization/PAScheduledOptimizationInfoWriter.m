//
//  PAScheduledOptimizationInfoWriter.m
//  ProactiveAssistAgent
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <PACoreServices/PACoreServices.h>
#import "PAScheduledOptimizationInfoWriter.h"

NSString *const kPAOPScheduleOptimizerXmlName = @"optimizerschedule.xml";
NSString *const kPAOPScheduleOptimizerXmlRoot = @"OptimizationJob";
NSString *const kPAOPLastScheduleDateTimeNodeName = @"LastScheduleDateTime";
NSString *const kPAOPLastRunDateTimeNodeName = @"LastRunDateTime";
NSString *const kPAOPNextScheduleDateTimeNodeName = @"NextScheduleDateTime";
NSString *const kPAOPScheduleFrequencyNodeName = @"ScheduleFrequency";
NSString *const kPAOPScheduleDayNodeName = @"ScheduleDay";

@interface PAScheduledOptimizationInfoWriter () {
  NSString *_scheduleOptimizerXmlPath;
  PAXmlWriter *_xmlWriter;
  PAFileSystemManager *_fsManager;
}

@end

@implementation PAScheduledOptimizationInfoWriter

- (instancetype)init
{
  self = [super init];
  if (self) {
    _fsManager = [[PAFileSystemManager alloc]init];
    NSString *optmizeFolderPath = _fsManager.optimizationFolderPath;
    _scheduleOptimizerXmlPath = optmizeFolderPath;
    _scheduleOptimizerXmlPath = [_scheduleOptimizerXmlPath stringByAppendingPathComponent:kPAOPScheduleOptimizerXmlName];
    _xmlWriter = [[PAXmlWriter alloc]initWithXmlAtPath:_scheduleOptimizerXmlPath];
  }
  return self;
}

- (BOOL)saveScheduledOptimizationInfo {
  BOOL result = [self createScheduledOptimizerXml];
  if (!result) {
    return false;
  }
  if (_lastScheduleDateTime) {
    result = [_xmlWriter replaceXmlElementWithName:kPAOPLastScheduleDateTimeNodeName value:_lastScheduleDateTime attributes:nil parentXPath:kPAOPScheduleOptimizerXmlRoot];
  }
  if (_lastRunDateTime) {
    result = [_xmlWriter replaceXmlElementWithName:kPAOPLastRunDateTimeNodeName value:_lastRunDateTime attributes:nil parentXPath:kPAOPScheduleOptimizerXmlRoot];
  }
  if (_nextScheduleDateTime) {
    result = [_xmlWriter replaceXmlElementWithName:kPAOPNextScheduleDateTimeNodeName value:_nextScheduleDateTime attributes:nil parentXPath:kPAOPScheduleOptimizerXmlRoot];
  }
  result = [_xmlWriter replaceXmlElementWithName:kPAOPScheduleFrequencyNodeName value:[NSString stringWithFormat:@"%lu",(unsigned long)_scheduleFrequency] attributes:nil parentXPath:kPAOPScheduleOptimizerXmlRoot];
  
  result = [_xmlWriter replaceXmlElementWithName:kPAOPScheduleDayNodeName value:[NSString stringWithFormat:@"%lu",(unsigned long)_scheduleDay] attributes:nil parentXPath:kPAOPScheduleOptimizerXmlRoot];
  
  return result;
}

- (BOOL)createScheduledOptimizerXml {
  if ([_fsManager doesFileExist:_scheduleOptimizerXmlPath]) {
    return true;
  }
  BOOL xmlCreated = false;
  
  PAXmlFileProperties *properties = [[PAXmlFileProperties alloc]initWithVersion:kPAXmlVersionValue encoding:nil standalone:nil root:kPAOPScheduleOptimizerXmlRoot];
  
  PAXmlServiceManager *manager = [[PAXmlServiceManager alloc]init];
  xmlCreated = [manager createXmlAtPath:_scheduleOptimizerXmlPath withProperties:properties];
  if (xmlCreated) {
    _xmlWriter = [[PAXmlWriter alloc]initWithXmlAtPath:_scheduleOptimizerXmlPath];
  }
  return xmlCreated;
}

@end
