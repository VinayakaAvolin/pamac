//
//  PAScheduledOptimizationInfoWriter.h
//  ProactiveAssistAgent
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PAScheduledOptimizationInfoWriter : NSObject

@property (nonatomic) NSString *lastScheduleDateTime;
@property (nonatomic) NSString *lastRunDateTime;
@property (nonatomic) NSString *nextScheduleDateTime;
@property (nonatomic) NSUInteger scheduleFrequency;
@property (nonatomic) NSUInteger scheduleDay;

- (BOOL)createScheduledOptimizerXml;
- (BOOL)saveScheduledOptimizationInfo;

@end
