//
//  PAOptimizationSchedulerInfoProvider.h
//  ProactiveAssistAgent
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum : NSUInteger {
    PAOptimFrequencyNone,
    PAOptimFrequencyDaily,
    PAOptimFrequencyWeekly,
    PAOptimFrequencyMonthly
} PAOptimFrequency;


@interface PAOptimizationSchedulerInfoProvider : NSObject

@property (nonatomic, readonly) PAOptimFrequency frequency;
@property (nonatomic, readonly) NSUInteger hour;
@property (nonatomic, readonly) NSUInteger minute;
@property (nonatomic, readonly) NSUInteger dayOfMonth;
@property (nonatomic, readonly) NSUInteger dayOfWeek;

@property (nonatomic, readonly) NSArray *guidList;
@property (nonatomic, readonly) BOOL isSchedulerOff;
@property (nonatomic, readonly) NSTimeInterval scheduledTimeInterval;
@property (nonatomic, readonly) NSUInteger scheduledDay;

- (void)getSchedulerInfo;
- (void)readScheduledOptimizationList;

@end
