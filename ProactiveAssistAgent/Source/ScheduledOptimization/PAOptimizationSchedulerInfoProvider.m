//
//  PAOptimizationSchedulerInfoProvider.m
//  ProactiveAssistAgent
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <PACoreServices/PACoreServices.h>
#import "PAOptimizationSchedulerInfoProvider.h"
#import "NSString+Additions.h"
#import "NSDate+Additions.h"
#import "PAPlistLockManager.h"

NSString *const kPASchedulerRegistryKey = @"software/supportsoft/providerlist/%PROVIDERPATH%/dynamicagent/users/%USER%/ss_config/optimizationschedule";
NSString *const kPASchedulerRegistryDataKey = @"DefaultScheduleOptimizations";
NSString *const kPASchedulerRegistryDayOfMonthKey = @"dayofmonth";
NSString *const kPASchedulerRegistryDayOfWeekKey = @"dayofweek";
NSString *const kPASchedulerRegistryFrequencyKey = @"schedule_frequency";
NSString *const kPASchedulerRegistryHourKey = @"hour";
NSString *const kPASchedulerRegistryMinuteKey = @"minute";

NSString *const kPAOptimizerScheduleXmlFileName = @"optimizerschedule.xml";
NSString *const kPALastScheduleDateTime = @"LastScheduleDateTime";
NSString *const kPALastRunDateTime = @"LastRunDateTime";
NSString *const kPANextScheduleDateTime = @"NextScheduleDateTime";
NSString *const kPAScheduleFrequency = @"ScheduleFrequency";
NSString *const kPAScheduleDay = @"ScheduleDay";

@interface PAOptimizationSchedulerInfoProvider () {
  NSString *_schedulerInfoXmlPath; // plist where scheduler detail is present
  NSString *_scheduledOptimizationInfoPlistPath; // plist where list of optimization to run is stored
  PAFileSystemManager *_fsManager;
}

@end

@implementation PAOptimizationSchedulerInfoProvider

- (instancetype)init
{
  self = [super init];
  if (self) {
    _fsManager = [[PAFileSystemManager alloc] init];
    
  }
  return self;
}

- (void)getSchedulerInfo {
  [self readScheduledOptimizationList];
  [self readSchedulerInfoFromPlist];
  [self readSchedulerInfo];
}

- (void)readSchedulerInfo {
  _schedulerInfoXmlPath = [_fsManager optimizationFolderPath];
  _schedulerInfoXmlPath = [_schedulerInfoXmlPath stringByAppendingPathComponent:kPAOptimizerScheduleXmlFileName];
  if ([_fsManager doesFileExist:_schedulerInfoXmlPath]) {
    PAXmlReader *xmlReader = [[PAXmlReader alloc] initWithXmlAtPath:_schedulerInfoXmlPath];
    [xmlReader xmlStringAtXPath:kPALastScheduleDateTime matchingAttributes:nil];
  }
}
- (void)readScheduledOptimizationList {
  _scheduledOptimizationInfoPlistPath = [_fsManager hkcuRegistryPlistPath];
  if ([_fsManager doesFileExist:_scheduledOptimizationInfoPlistPath]) {
    __block NSString *defaultScheduleOptimizations = nil;
    PAPlistLockManager *lckMgr = [PAPlistLockManager sharedInstance];
    [lckMgr runWithReadLock:_scheduledOptimizationInfoPlistPath codeBlock:^{
      PARegistryReader* reader = [[PARegistryReader alloc] initWithPlistAtPath:_scheduledOptimizationInfoPlistPath];
      NSString *keyPath = [_fsManager stringByExpandingPath:kPASchedulerRegistryKey];
      
      defaultScheduleOptimizations = [reader regValueForPath:keyPath withKey:kPASchedulerRegistryDataKey];
    }];
    if (![defaultScheduleOptimizations isEmptyOrHasOnlyWhiteSpaces]) {
      _guidList = [defaultScheduleOptimizations componentsSeparatedByString:@","];
    }
  }
}
- (void)readSchedulerInfoFromPlist {
  _scheduledOptimizationInfoPlistPath = [_fsManager hkcuRegistryPlistPath];
  if ([_fsManager doesFileExist:_scheduledOptimizationInfoPlistPath]) {
    PAPlistLockManager *lckMgr = [PAPlistLockManager sharedInstance];
    [lckMgr runWithReadLock:_scheduledOptimizationInfoPlistPath codeBlock:^{
      PARegistryReader* reader = [[PARegistryReader alloc] initWithPlistAtPath:_scheduledOptimizationInfoPlistPath];
      NSString *keyPath = [_fsManager stringByExpandingPath:kPASchedulerRegistryKey];
      
      _dayOfMonth = [[reader regValueForPath:keyPath withKey:kPASchedulerRegistryDayOfMonthKey] integerValue];
      _dayOfWeek = [[reader regValueForPath:keyPath withKey:kPASchedulerRegistryDayOfWeekKey] integerValue];
      _frequency = (PAOptimFrequency)[[reader regValueForPath:keyPath withKey:kPASchedulerRegistryFrequencyKey] integerValue];
      _hour = [[reader regValueForPath:keyPath withKey:kPASchedulerRegistryHourKey] integerValue];
      _minute = [[reader regValueForPath:keyPath withKey:kPASchedulerRegistryMinuteKey] integerValue];
    }];
  }
  [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"Is scheduler off [%d]",self.isSchedulerOff];
}
- (BOOL)isSchedulerOff {
  return (_frequency == PAOptimFrequencyNone);
}

- (NSTimeInterval)scheduledTimeInterval {
  switch (_frequency) {
    case PAOptimFrequencyDaily:
      return [self dailyTimeInterval];
      break;
    case PAOptimFrequencyWeekly:
      return [self weeklyTimeInterval];
      break;
    case PAOptimFrequencyMonthly:
      return [self monthlyTimeInterval];
      break;
    default:
      break;
  }
  return 0;
}

- (NSTimeInterval)dailyTimeInterval {
  NSDate *currentDate = [NSDate systemDate];
  NSDate *beginningOfDate = [NSDate dateAtBeginningOfDayForDate:currentDate];
  NSDate *scheduledDate = [NSDate dateByAddingDateComponentsHour:_hour minute:_minute toDate:beginningOfDate];
  [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"[Daily] Scheduled date = %@",[scheduledDate dateStringWithFormat:Format8]];
  
  NSTimeInterval timeInterval = 0;
  // is past date?
  if ([currentDate compare:scheduledDate] == NSOrderedDescending) {
    [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"[Daily] Scheduled date is past; so re calculate the date."];
    scheduledDate = [NSDate dateByAddingDateComponentsHour:0 minute:0 day:1 toDate:scheduledDate];
    timeInterval = [scheduledDate timeIntervalSinceDate:currentDate];
  } else {
    timeInterval = [scheduledDate timeIntervalSinceDate:currentDate];
  }
  
  [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"[Daily] Updated Scheduled date [%@] and interval [%f]",[scheduledDate dateStringWithFormat:Format8], timeInterval];
  return timeInterval;
}
- (NSTimeInterval)weeklyTimeInterval {
  NSDate *currentDate = [NSDate systemDate];
  NSInteger numberOfDaysToAdd = 0;
  NSInteger numberOfDaysInAWeek = 7;
  NSInteger currentDayOfWeek = [currentDate dayOfWeek];
  NSInteger sheduledDayOfWeek = _dayOfWeek+1;// From UI we get 0 based index of week day; 0 to 6; where 0 is for Sunday; But Date API week day starts from 1 to 7; with 1: sunday
  /// if different day
  if (currentDayOfWeek != sheduledDayOfWeek) {
    if (currentDayOfWeek < sheduledDayOfWeek) {
      numberOfDaysToAdd = sheduledDayOfWeek - currentDayOfWeek;
    } else {
      numberOfDaysToAdd = (numberOfDaysInAWeek - currentDayOfWeek) + sheduledDayOfWeek;
    }
  }
  NSDate *beginningOfDate = [NSDate dateAtBeginningOfDayForDate:currentDate];
  NSDate *scheduledDate = [NSDate dateByAddingDateComponentsHour:_hour minute:_minute day:numberOfDaysToAdd toDate:beginningOfDate];
  [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"[Weekly] Scheduled date = %@",[scheduledDate dateStringWithFormat:Format8]];
  NSTimeInterval timeInterval = 0;
  // is past date? schedule to next week by adding 7 days to current schedule time
  if ([currentDate compare:scheduledDate] == NSOrderedDescending) {
    [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"[Weekly] Scheduled date is past; so re calculate the date."];
    scheduledDate = [NSDate dateByAddingDateComponentsHour:0 minute:0 day:numberOfDaysInAWeek toDate:scheduledDate];
    timeInterval = [scheduledDate timeIntervalSinceDate:currentDate];
  } else {
    timeInterval = [scheduledDate timeIntervalSinceDate:currentDate];
  }
  
  [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"[Weekly] Updated Scheduled date [%@] and interval [%f]",[scheduledDate dateStringWithFormat:Format8], timeInterval];
  return timeInterval;
}
- (NSTimeInterval)monthlyTimeInterval {
  NSDate *currentDate = [NSDate systemDate];
  
  NSDate *beginningOfDate = [NSDate dateAtBeginningOfDayForDate:currentDate];
  NSDate *scheduledDate = [NSDate dateByAddingDateComponentsHour:_hour minute:_minute day:0 toDate:beginningOfDate];
  NSTimeInterval timeInterval = 0;
  NSInteger currentDateDayOfMonth = [beginningOfDate dayOfMonth];
  /// today is not the day selected in the scheduler; but it is not past
  if (currentDateDayOfMonth < _dayOfMonth) {
    scheduledDate = [NSDate currentMonthDateFromDateComponentsHour:_hour minute:_minute day:_dayOfMonth];
  }
  [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"[Monthly] Scheduled date = %@",[scheduledDate dateStringWithFormat:Format8]];
  
  // is past date? schedule to next month
  if ([currentDate compare:scheduledDate] == NSOrderedDescending) {
    [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"[Monthly] Scheduled date is past; so re calculate the date."];
    scheduledDate = [NSDate nextMonthDateFromDateComponentsHour:_hour minute:_minute day:_dayOfMonth];
    timeInterval = [scheduledDate timeIntervalSinceDate:currentDate];
  } else {
    timeInterval = [scheduledDate timeIntervalSinceDate:currentDate];
  }
  
  [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"[Monthly] Updated Scheduled date [%@] and interval [%f]",[scheduledDate dateStringWithFormat:Format8], timeInterval];
  return timeInterval;
}

- (NSUInteger)scheduledDay {
  switch (_frequency) {
    case PAOptimFrequencyDaily:
      return 0;
      break;
    case PAOptimFrequencyWeekly:
      return _dayOfWeek;
      break;
    case PAOptimFrequencyMonthly:
      return _dayOfMonth;
      break;
    default:
      break;
  }
  return 0;
}
@end
