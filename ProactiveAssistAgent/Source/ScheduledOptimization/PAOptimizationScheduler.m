//
//  PAOptimizationScheduler.m
//  ProactiveAssistAgent
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <PASupportService/PASupportService.h>
#import <PACoreServices/PACoreServices.h>
#import "PAOptimizationScheduler.h"
#import "PAOptimizerConstants.h"
#import "PAOptimizationSchedulerInfoProvider.h"
#import "PAOptimizerResultHandler.h"
#import "NSDate+Additions.h"
#import "PAScheduledOptimizationInfoWriter.h"

#import "PAJSResult.h"
#import "PAJSInvokedCommand.h"
#import "PAJSCommandDelegate.h"
#import "PAJSConstants.h"
#import "PACommonUtilityController.h"

@interface PAOptimizationScheduler() {
  NSTimer *_scheduleTimer;
  NSMutableArray *_optimizationGuidList;
  NSOperationQueue *_operationQueue;
  PAOptimizationInfo *_schedulerInfo;
  PAOptimizationSchedulerInfoProvider *_scheduleInfoProvider;
  NSString *_currentOptimizationGuid;
  PAOptimizerResultHandler *_optimizerResultHandler;
  PAScheduledOptimizationInfoWriter *_scheduledOptimizationInfoWriter;
  NSString *_optimizationState;
  PAFileSystemManager *_fsManager;
}

@end

@implementation PAOptimizationScheduler

- (instancetype)init
{
  self = [super init];
  if (self) {
    _operationQueue = [[NSOperationQueue alloc]init];
    _scheduleInfoProvider = [[PAOptimizationSchedulerInfoProvider alloc]init];
    _scheduledOptimizationInfoWriter = [[PAScheduledOptimizationInfoWriter alloc] init];
    _optimizationState = @"start";
    _fsManager = [[PAFileSystemManager alloc]init];
  }
  return self;
}
- (void)start {
  
  __block NSString *log = [@"CONTENT__" stringByAppendingString:[@"performance_manager" stringByAppendingString:[@"__" stringByAppendingString:[@"0" stringByAppendingString:[@"__optimizer_start" stringByAppendingString:[@"__Client__1____" stringByAppendingString:[@"optimizer_start" stringByAppendingString:[@"__" stringByAppendingString:[@"" stringByAppendingString:[@"____MAC Others__" stringByAppendingString:@""]]]]]]]]]];
  
  if(_optimizationState == @"start" ) {
    [self WriteOptimizerUsageLog:(NSString *)log];
    _optimizationState = @"stop";
  }
  
  [_operationQueue cancelAllOperations];
  NSInvocationOperation *startOperation = [[NSInvocationOperation alloc]
                                           initWithTarget:self
                                           selector:@selector(startOperation)
                                           object:nil];
  [_operationQueue addOperation:startOperation];
}
- (void)stop {
  [_operationQueue cancelAllOperations];
  
  __block NSString *log = [@"CONTENT__" stringByAppendingString:[@"performance_manager" stringByAppendingString:[@"__" stringByAppendingString:[@"0" stringByAppendingString:[@"__optimizer_stop" stringByAppendingString:[@"__Client__1____" stringByAppendingString:[@"optimizer_stop" stringByAppendingString:[@"__" stringByAppendingString:[@"" stringByAppendingString:[@"____MAC Others__" stringByAppendingString:@""]]]]]]]]]];
  
  if(_optimizationState == @"stop" ) {
    [self WriteOptimizerUsageLog:(NSString *)log];
    _optimizationState = @"start";
  }
}

/*
 @{kPAScheduleTimeKey: 11/20/2017 12:00:00,
 kPAScheduleFrequencyKey: 0(None),1(Daily),2(Weekly), 3(Monthly)
 kPAScheduleDayKey: 0(None), 1(Monday), 2(Tuesday)...,
 kPAScheduleGuidsKey: guids array
 }
 */

- (void)scheduleOptimization:(NSDictionary *)info {
  [_operationQueue cancelAllOperations];
  NSInvocationOperation *startOperation = [[NSInvocationOperation alloc]
                                           initWithTarget:self
                                           selector:@selector(startOperation)
                                           object:nil];
  [_operationQueue addOperation:startOperation];
}

- (void)startOperation {
  [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"Scheduled optimization started"];
  [_scheduleInfoProvider getSchedulerInfo];
  [self removeSchedulerTimer];
  if (!_scheduleInfoProvider.isSchedulerOff) {
    [self addSchedulerTimer];
  } else {
    [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"Scheduled optimization is turned off"];
  }
}

- (void)addSchedulerTimer {
  NSTimeInterval timeInderval = _scheduleInfoProvider.scheduledTimeInterval;
  [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"Scheduled optimization interval is [%f]",timeInderval];
  
  if (timeInderval <= 0) {
    return;
  }
  _scheduledOptimizationInfoWriter.nextScheduleDateTime = [[[NSDate systemDate] dateByAddingTimeInterval:timeInderval] dateStringWithFormat:Format10];
  [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"Next shcedule date is [%@]",_scheduledOptimizationInfoWriter.nextScheduleDateTime];
  
  [_scheduledOptimizationInfoWriter saveScheduledOptimizationInfo];
  
  dispatch_async(dispatch_get_main_queue(), ^{
    _scheduleTimer =  [NSTimer scheduledTimerWithTimeInterval:timeInderval target:self selector:@selector(schedulerTimerFired:) userInfo:nil repeats:YES];
    
  });
}

- (void)removeSchedulerTimer {
  dispatch_async(dispatch_get_main_queue(), ^{
    if (_scheduleTimer.isValid) {
      [_scheduleTimer invalidate];
    }
  });
  
}

- (NSTimeInterval)calculateScheduleInterval:(NSDictionary*)info {
  return 0;
}

- (void)schedulerTimerFired:(NSTimer *)timer {
  [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"Scheduler fired"];
  
  _optimizerResultHandler = [[PAOptimizerResultHandler alloc] init];
  
  [_operationQueue cancelAllOperations];
  NSInvocationOperation *startOperation = [[NSInvocationOperation alloc]
                                           initWithTarget:self
                                           selector:@selector(runScheduledOptimizer)
                                           object:nil];
  [_operationQueue addOperation:startOperation];
  
}
- (void)runScheduledOptimizer {
  _scheduledOptimizationInfoWriter.lastScheduleDateTime = [[NSDate systemDate] dateStringWithFormat:Format10];
  
  [self removeSchedulerTimer];
  [_scheduleInfoProvider readScheduledOptimizationList];
  _optimizationGuidList = [_scheduleInfoProvider.guidList mutableCopy];
  [self runOptimizationInQueue];
}

- (void)runOptimizationInQueue {
  [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"Total Scheduled optimizations are [%ld]",_optimizationGuidList.count];
  if (_optimizationGuidList.count) {
    _currentOptimizationGuid = _optimizationGuidList.firstObject;
    
    [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"Run optimization [%@]",_currentOptimizationGuid];
    
    [self startOptimization];
    PAServiceInfo *info = [[PAServiceInfo alloc]init];
    info.guid = _optimizationGuidList.firstObject;
    info.serviceCategory = PASupportServiceCategorySupportAction;
    info.service = PASupportServiceOptimizationScan;
    [PAServiceInterface executeService:info.service category:info.serviceCategory info:info callback:^(BOOL success, NSError *error, id result) {
      NSNumber *exitCode = result[kPASupportServiceResponseKeyExitCode];
      [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"Scan exit code [%@]",exitCode];
      __block NSString *log;
      
      //Below code to get version of optimization content
      NSString *sContentVersion;
      NSString *folderName = [_fsManager getMatchingFileName:[NSString stringWithFormat:@"%@", info.guid] inBaseDirectory:[_fsManager optimizationFolderPath]];
      NSArray *folderVersion = [folderName componentsSeparatedByString:@"."];
      if(folderVersion.count > 0) {
        sContentVersion = [folderVersion objectAtIndex:1];
      }
      //info.guid doesn't hold version so commented below line
      //NSArray *items = [info.guid componentsSeparatedByString:@"."];
      
      if (success && exitCode && [exitCode isKindOfClass:[NSNumber class]] && exitCode.integerValue == 0) {
        info.service = PASupportServiceOptimizationFix;
        [PAServiceInterface executeService:info.service category:info.serviceCategory info:info callback:^(BOOL success, NSError *error, id result) {
          NSNumber *exitCode = result[kPASupportServiceResponseKeyExitCode];
          [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"Run exit code [%@]",exitCode];
          
          log = [@"CONTENT__" stringByAppendingString:[info.guid stringByAppendingString:[@"__" stringByAppendingString:[sContentVersion stringByAppendingString:[@"__optimizationSchedule" stringByAppendingString:[@"__Client__1____" stringByAppendingString:[@"optimization" stringByAppendingString:[@"__" stringByAppendingString:[@"__" stringByAppendingString:[@"____MAC Others__" stringByAppendingString:[NSString stringWithFormat:@"%@", [exitCode stringValue]]]]]]]]]]]];
          [self WriteOptimizerUsageLog:(NSString *)log];
          
          if ([exitCode isKindOfClass:[NSNumber class]]) {
            _optimizerResultHandler.resultCode = exitCode;
          }
          /// check next item in queue
          [self stopOptimization];
          [self removeOptimizationFromQueue];
          [self runNextOptimizationInQueue];
        }];
      } else {
        log = [@"CONTENT__" stringByAppendingString:[info.guid stringByAppendingString:[@"__" stringByAppendingString:[sContentVersion  stringByAppendingString:[@"__optimizationSchedule" stringByAppendingString:[@"__Client__1____" stringByAppendingString:[@"optimization" stringByAppendingString:[@"__" stringByAppendingString:[@"__" stringByAppendingString:[@"____MAC Others__" stringByAppendingString:@""]]]]]]]]]];
        
        if ([exitCode isKindOfClass:[NSNumber class]]) {
          _optimizerResultHandler.resultCode = exitCode;
          
          log = [@"CONTENT__" stringByAppendingString:[info.guid stringByAppendingString:[@"__" stringByAppendingString:[sContentVersion  stringByAppendingString:[@"__optimizationSchedule" stringByAppendingString:[@"__Client__1____" stringByAppendingString:[@"optimization" stringByAppendingString:[@"__" stringByAppendingString:[@"__" stringByAppendingString:[@"____MAC Others__" stringByAppendingString:[NSString stringWithFormat:@"%@", [exitCode stringValue]]]]]]]]]]]];
        }
        
        [self WriteOptimizerUsageLog:(NSString *)log];
        /// check next item in queue
        [self stopOptimization];
        [self removeOptimizationFromQueue];
        [self runNextOptimizationInQueue];
      }
      
    }];
  } else {
    [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"No more scheduled optimization"];
    
    _scheduledOptimizationInfoWriter.lastRunDateTime = [[NSDate systemDate] dateStringWithFormat:Format10];
    _scheduledOptimizationInfoWriter.scheduleDay = (NSUInteger)_scheduleInfoProvider.scheduledDay;
    _scheduledOptimizationInfoWriter.scheduleFrequency = (NSUInteger)_scheduleInfoProvider.frequency;
    // log error; and notify
    [self addSchedulerTimer];
  }
}

- (void)removeOptimizationFromQueue {
  [_optimizationGuidList removeObject:_currentOptimizationGuid];
}
- (void)runNextOptimizationInQueue {
  [self runOptimizationInQueue];
}

- (void)startOptimization {
  _optimizerResultHandler.guid = _currentOptimizationGuid;
  _optimizerResultHandler.startTime = [[NSDate systemDate] dateStringWithFormat:Format10];
  _optimizerResultHandler.statusEnum =  PAORStatusStarted;
}
- (void)stopOptimization {
  _optimizerResultHandler.endTime = [[NSDate systemDate] dateStringWithFormat:Format10];
  _optimizerResultHandler.statusEnum =  PAORStatusStopped;
  [_optimizerResultHandler storeCurrentOptimizationResult];
}


- (void)WriteOptimizerUsageLog:(NSString *)logmsg {
  NSMutableDictionary* dict = [NSMutableDictionary dictionary];
  [dict setObject:@[logmsg] forKey:JSArgumentsKey];
  [dict setObject:@"1" forKey:JSISSyncMethodKey];
  [dict setObject:@"WriteUsageLog" forKey:JSOperationNameKey];
  PAJSInvokedCommand*command = [PAJSInvokedCommand commandFromDictionary:dict];
  PACommonUtilityController *controller= [[PACommonUtilityController alloc] initWithCommandDelegate:self];
  PAJSResult*res = [controller executeCommand:command];
}

@end
