//
//  PAAppSetupManager.m
//  ProactiveAssistAgent
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <PACoreServices/PACoreServices.h>
#import "PAAppSetupManager.h"


@implementation PAAppSetupManager

+ (BOOL)doAppSetup {
  BOOL isSetupComplete = false;
  // copy data folder
  PAFileSystemManager *fsManager = [[PAFileSystemManager alloc]init];
  NSString *srcPath = [fsManager systemAppRootPath];
  PAAppConfiguration *configuration = [PAConfigurationManager appConfigurations];
  srcPath = [srcPath stringByAppendingPathComponent:configuration.dataFolderName];
  
  NSString *dataDstPath = [fsManager appRootPath];
  dataDstPath = [dataDstPath stringByAppendingPathComponent:configuration.providerId];
  if (![fsManager doesDirectoryExist:dataDstPath]) {
    [fsManager createDirectoryAtPath:dataDstPath withIntermediateDirectories:true attributes:nil error:nil];
  }
  dataDstPath = [dataDstPath stringByAppendingPathComponent:configuration.dataFolderName];
  
  if (srcPath && dataDstPath && ![fsManager doesDirectoryExist:dataDstPath]) {
    isSetupComplete = [fsManager copyDirectory:srcPath destinationPath:dataDstPath];
  }// else {
  //   isSetupComplete = true;
  // }
  
  //Explicitly setting it to true because sync and other operation is dependent on this boolean value
  isSetupComplete = true;
  
  return isSetupComplete;
}
@end
