//
//  PAAppSetupManager.h
//  ProactiveAssistAgent
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

/// class to do initial setup required for smooth functioning of the app.
@interface PAAppSetupManager : NSObject

+ (BOOL)doAppSetup;

@end
