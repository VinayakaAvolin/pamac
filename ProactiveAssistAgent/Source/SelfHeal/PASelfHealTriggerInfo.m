//
//  PASelfHealTriggerInfo.m
//  ProactiveAssistAgent
//
//  Copyright © 2018 Avolin. All rights reserved.
//
#import <PACoreServices/PACoreServices.h>
#import <objc/runtime.h>

#import "PASelfHealTriggerInfo.h"
static NSString *const kPARTXmlExtension = @"xml";

@implementation PASelfHealTriggerInfo

- (instancetype)initWithContentGuid:(NSString *)guid
                            version:(NSString *)version
                     contentXmlPath:(NSString *)xmlPath
{
    self = [super init];
    if (self) {
        _contentGuid = guid;
        _contentVersion = version;
        _contentXmlPath = xmlPath;
        _monitorEventClass = InvalidEventClass;
    }
    return self;
}

- (NSDictionary *)propertyClassesByName
{
    // Check for a cached value (we use _cmd as the cache key,
    // which represents @selector(propertyNames))
    NSMutableDictionary *dictionary = objc_getAssociatedObject([self class], _cmd);
    if (dictionary)
    {
        return dictionary;
    }
    
    // Loop through our superclasses until we hit NSObject
    dictionary = [NSMutableDictionary dictionary];
    Class subclass = [self class];
    while (subclass != [NSObject class])
    {
        unsigned int propertyCount;
        objc_property_t *properties = class_copyPropertyList(subclass,
                                                             &propertyCount);
        for (int i = 0; i < propertyCount; i++)
        {
            // Get property name
            objc_property_t property = properties[i];
            const char *propertyName = property_getName(property);
            NSString *key = @(propertyName);
            
            // Check if there is a backing ivar
            char *ivar = property_copyAttributeValue(property, "V");
            if (ivar)
            {
                // Check if ivar has KVC-compliant name
                NSString *ivarName = @(ivar);
                if ([ivarName isEqualToString:key] ||
                    [ivarName isEqualToString:[@"_" stringByAppendingString:key]])
                {
                    // Get type
                    Class propertyClass = nil;
                    char *typeEncoding = property_copyAttributeValue(property, "T");
                    switch (typeEncoding[0])
                    {
                        case 'c': // Numeric types
                        case 'i':
                        case 's':
                        case 'l':
                        case 'q':
                        case 'C':
                        case 'I':
                        case 'S':
                        case 'L':
                        case 'Q':
                        case 'f':
                        case 'd':
                        case 'B':
                        {
                            propertyClass = [NSNumber class];
                            break;
                        }
                        case '*': // C-String
                        {
                            propertyClass = [NSString class];
                            break;
                        }
                        case '@': // Object
                        {
                            NSString *inputString = [NSString stringWithUTF8String:typeEncoding];
                            NSCharacterSet *delimiters = [NSCharacterSet characterSetWithCharactersInString:@"\"\""];
                            NSArray *splitString = [inputString componentsSeparatedByCharactersInSet:delimiters];
                            NSString *className = splitString[1];
                            propertyClass = NSClassFromString(className);
                            break;
                        }
                        case '{': // Struct
                        {
                            propertyClass = [NSValue class];
                            break;
                        }
                        case '[': // C-Array
                        case '(': // Enum
                        case '#': // Class
                        case ':': // Selector
                        case '^': // Pointer
                        case 'b': // Bitfield
                        case '?': // Unknown type
                        default:
                        {
                            propertyClass = nil; // Not supported by KVC
                            break;
                        }
                    }
                    free(typeEncoding);
                    
                    // If known type, add to dictionary
                    if (propertyClass) dictionary[key] = propertyClass;
                }
                free(ivar);
            }
        }
        free(properties);
        subclass = [subclass superclass];
    }
    
    // Cache and return dictionary
    objc_setAssociatedObject([self class], _cmd, dictionary,
                             OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    return dictionary;
}

+ (BOOL)supportsSecureCoding
{
    return YES;
}

- (id)initWithCoder:(NSCoder *)coder
{
    if ((self = [super init]))
    {
        // Decode the property values by key, specifying the expected class
        [[self propertyClassesByName] enumerateKeysAndObjectsUsingBlock:^(NSString *key, Class propertyClass, BOOL *stop) {
            id object = nil;
            if ([propertyClass isEqual:[NSArray class]] || [propertyClass isEqual:[NSDictionary class]]) {
                
                NSSet *setForDictArray = [NSSet setWithObjects:[NSDictionary class],[NSArray class], [NSString class], [NSNumber class], [NSDate class], [NSURL class], [NSData class], [NSError class], nil];
                
                object = [coder decodeObjectOfClasses: setForDictArray forKey:key];
            } else {
                object = [coder decodeObjectOfClass:propertyClass forKey:key];
            }
            if (object) [self setValue:object forKey:key];
        }];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    for (NSString *key in [self propertyClassesByName])
    {
        id object = [self valueForKey:key];
        if (object) [aCoder encodeObject:object forKey:key];
    }
}

@end
