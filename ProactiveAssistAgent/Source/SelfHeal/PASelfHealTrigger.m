//
//  PASelfHealTrigger.m
//  ProactiveAssistAgent
//
//  Copyright © 2018 Avolin. All rights reserved.
//

#import <PACoreServices/PACoreServices.h>
#import <PAEventSubscriptionManager/PAEventSubscriptionManager.h>
#import <PAIpcService/PAIpcService.h>

#import "PASelfHealTrigger.h"
#import "PASelfHealTriggerInfo.h"
#import "PASelfHealTriggerXmlParser.h"
#import "PAEventMonitorManager.h"

static NSString *const kPAzCatFileName = @"zcat.dat";
static NSString *const kPAzCatFolder = @"zcat";
static NSString *const kContentGuidKey = @"ContentGuid";
static NSString *const kContentChecksumKey = @"ContentChecksum";

@interface PASelfHealTrigger()
@property (nonatomic, readwrite) NSMutableDictionary *newlyDiscoveredEventstriggerInfo;
@property (nonatomic) PAFileSystemManager *fsManager;
@property (atomic, assign) BOOL selfHealInProgress;
@property (nonatomic) NSDictionary *zcatDictionary;
@end

@implementation PASelfHealTrigger

- (instancetype)init
{
    self = [super init];
    if (self) {
        _fsManager = [[PAFileSystemManager alloc]init];
        _newlyDiscoveredEventstriggerInfo = [NSMutableDictionary new];
        _selfHealInProgress = NO;
    }
    return self;
}

- (void)initiateSelfHeal
{
    [[PALogger sharedLoggerForClient:PALogClientSelfHeal] debug:@"Self heal operation initiated."];
    NSString *selfhealTriggerFolderPath = _fsManager.selfHealTriggerFolderPath;
    
    if(![_fsManager doesFileExist:_fsManager.selfHealRegistryPlistPath]) {
        [@{} writeToFile:_fsManager.selfHealRegistryPlistPath atomically:YES];
    }
    
    NSArray *contentsOfSelfhealTriggerFolderDir = [_fsManager contentsOfDirectory:selfhealTriggerFolderPath];
    for (NSString *itemName in contentsOfSelfhealTriggerFolderDir) {
        @autoreleasepool {
            BOOL checksumValid = [self checksumValidForContentFolder:[NSString stringWithFormat:@"%@/%@", selfhealTriggerFolderPath, itemName]];
            [[PALogger sharedLoggerForClient:PALogClientSelfHeal] debug:@"Is Checksum valid for conent folder %@ = %d", itemName, checksumValid];
            //checksumValid = YES; // disable checksum validation for time being, till zcat is configured.

            if (checksumValid) {
                NSString *contentFolderPath = [selfhealTriggerFolderPath stringByAppendingPathComponent:itemName];
                NSString *contentXmlPath = [contentFolderPath stringByAppendingPathComponent:itemName];
                contentXmlPath = [contentXmlPath stringByAppendingPathExtension:@"xml"];
                if ([_fsManager doesDirectoryExist:contentFolderPath] &&
                    [_fsManager doesFileExist:contentXmlPath]) {
                    PASelfHealTriggerInfo *triggerInfo = [[PASelfHealTriggerInfo alloc] initWithContentGuid:[itemName stringByDeletingPathExtension]                        version:itemName.pathExtension contentXmlPath:contentXmlPath];
                    PASelfHealTriggerXmlParser *xmlParser = [[PASelfHealTriggerXmlParser alloc] initWithTriggerInfo:triggerInfo];
                    [xmlParser parseInputConfigurationWithCompletion:^(BOOL success, PASelfHealTriggerInfo *triggerInfo){
                        NSLog(@"%@", triggerInfo);
                        if (success) {
                            triggerInfo.contentFolderChecksum = [self checksumForFolderAtPath:contentFolderPath];
                            _newlyDiscoveredEventstriggerInfo[triggerInfo.contentGuid] = triggerInfo;
                        }
                    }];
                }
            }
        }
    }
    [self startSelfHealOperation];
    self.selfHealInProgress = _newlyDiscoveredEventstriggerInfo.count > 0 ? YES : NO;
}

- (void)startSelfHealOperation {
    [self handleStaleEvents];
    [self setupMonitoringForNewEvents];
    [self updateRegistry];
}

- (void)handleStaleEvents {
    NSString *registryPath = _fsManager.selfHealRegistryPlistPath;
    NSDictionary *monitoredEventsFromregistry = [PAPlistUtility plistContentAtPath:registryPath];
    if (monitoredEventsFromregistry.count) {
        NSArray *eventsTobeStopped = [self eventsTobeStoppedMonitoring:monitoredEventsFromregistry];
        PAPlistWriter *writer = [[PAPlistWriter alloc] initWithPlistAtPath:_fsManager.selfHealRegistryPlistPath];
        
        for (NSString *eventGuid in eventsTobeStopped) {
            // These events are no longer active.So stop monitoring and subscription
            [self stopSelfHealForEvent:eventGuid];
            [writer removeKey:eventGuid inSection:nil];
        }
        [writer synchronize];
        NSArray *eventsTobeUpdated = [self eventsTobeUpdated:monitoredEventsFromregistry];
        for (NSString* eventGuid in eventsTobeUpdated) {
            // These events are being updated. So, stop them for current subscription and monitoring
            // Start fresh monitoring and subscription as part of new events...
            [self stopSelfHealForEvent:eventGuid];
        }
    }

}

- (void)setupMonitoringForNewEvents {
    for (PASelfHealTriggerInfo *triggerInfo in self.newlyDiscoveredEventstriggerInfo.allValues) {
        [[PALogger sharedLoggerForClient:PALogClientSelfHeal] debug:@"----Setting up monitoring for %@----", triggerInfo.contentGuid];
       [[PAInterProcessManager sharedManager] executeMonitorService:triggerInfo withReply:^(BOOL result) {
            
        }];
        [[PALogger sharedLoggerForClient:PALogClientSelfHeal] debug:@"----Setting up subscription for %@----", triggerInfo.contentGuid];
        PAEventSubscriptionManager *subscriptionManager = [PAEventSubscriptionManager sharedInstance];
        [subscriptionManager setupSubscriberForEventInfo:triggerInfo];
    }
}

- (void) updateRegistry {
    
    if (self.newlyDiscoveredEventstriggerInfo.count) {
        PAPlistWriter *writer = [[PAPlistWriter alloc] initWithPlistAtPath:_fsManager.selfHealRegistryPlistPath];
        
        [[self.newlyDiscoveredEventstriggerInfo allValues] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            PASelfHealTriggerInfo *triggerInfo = obj;
            [writer setValue:triggerInfo.contentFolderChecksum forKey:triggerInfo.contentGuid inSection:nil];
        }];
        [writer synchronize];
        [[PALogger sharedLoggerForClient:PALogClientSelfHeal] debug:@"Self heal registry updated."];
    }
}

- (void)stopSelfHeal {
    NSString *registryPath = _fsManager.selfHealRegistryPlistPath;
    NSArray *monitoredEventsFromregistry = [[PAPlistUtility plistContentAtPath:registryPath] allKeys];
    for (NSString *eventGuid in monitoredEventsFromregistry) {
        [self stopSelfHealForEvent:eventGuid];
    }
    [PAPlistUtility writeContent:@{} atPath:registryPath];
    self.selfHealInProgress = NO;
}
- (void)stopSelfHealForEvent: (NSString *)eventGuid {
    [[PALogger sharedLoggerForClient:PALogClientSelfHeal] debug:@"Event monitoring is being stopped for the event %@", eventGuid];
    // Tell the subscription Manager to remove subscription for the event...
    PAEventSubscriptionManager *subscriptionManager = [PAEventSubscriptionManager sharedInstance];
    [subscriptionManager removeSubscriberForEvent:eventGuid];
    // Tell the Monitor Manager to stop monitoring for the event...
    [[PAInterProcessManager sharedManager] stopMonitoringForEvent:eventGuid withReply:^(BOOL result) {
        
    }];
}

- (BOOL)isSelfHealInProgress {
    return self.selfHealInProgress;
}

- (NSArray *)eventsTobeStoppedMonitoring:(NSDictionary *)monitoredEventsFromregistry {
    
    NSArray *registryEvents = [monitoredEventsFromregistry allKeys];
    NSArray *newEvents = self.currentlyMonitoredEvents;
    NSSet *newEventSetA = [NSSet setWithArray:newEvents];
    NSMutableSet *previousEventsset = [NSMutableSet setWithArray:registryEvents];
    [previousEventsset minusSet:newEventSetA];
    return [previousEventsset allObjects];
}

- (NSArray *)eventsTobeUpdated:(NSDictionary *)monitoredEventsFromregistry {
    NSArray *registryEvents = [monitoredEventsFromregistry allKeys];
    NSArray *newEvents = self.currentlyMonitoredEvents;
    NSSet *newEventSet = [NSSet setWithArray:newEvents];
    NSMutableSet *previousEventsset = [NSMutableSet setWithArray:registryEvents];
    [previousEventsset intersectSet:newEventSet];
    NSArray *commonEvents = [previousEventsset allObjects];
    NSMutableArray *modifiedEvents = [NSMutableArray new];
    for (NSString *eventGuid in commonEvents) {
        NSString *newChecksum = ((PASelfHealTriggerInfo *)self.newlyDiscoveredEventstriggerInfo[eventGuid]).contentFolderChecksum;
        NSString *oldChecksum = monitoredEventsFromregistry[eventGuid];
        if (![oldChecksum isEqualToString:newChecksum]) {
            [modifiedEvents addObject:eventGuid];
        } else {
            [_newlyDiscoveredEventstriggerInfo removeObjectForKey:eventGuid];
        }
    }
    return [modifiedEvents copy];
}

- (NSArray *)currentlyMonitoredEvents {
    if (self.newlyDiscoveredEventstriggerInfo.count) {
        return [self.newlyDiscoveredEventstriggerInfo.allKeys copy];
    }
    NSDictionary *events = [PAPlistUtility plistContentAtPath: _fsManager.selfHealRegistryPlistPath];
    return [events allKeys];
}

- (NSString *)checksumForFolderAtPath:(NSString *)contentFolderPath {
    NSString *assistAgentPath = [PAConfigurationManager appConfigurations].assistAgentFolderPath;
    assistAgentPath = [self.fsManager stringByExpandingPath:assistAgentPath];
    NSBundle* bundle = [NSBundle bundleWithPath:assistAgentPath];
    NSString* dirScriptPath = [bundle pathForResource:@"gendirmd5" ofType:@"sh"];
    
    
    NSTask *task = [[NSTask alloc] init];
    task.launchPath = dirScriptPath;
    task.arguments = [NSArray arrayWithObjects:contentFolderPath, nil];
    
    NSPipe *pipe = [NSPipe pipe];
    task.standardOutput = pipe;
    
    NSFileHandle *fileHandle = [pipe fileHandleForReading];
    [task launch];
    [task waitUntilExit];
    
    NSString *newMd5 = [[NSString alloc] initWithData:[fileHandle availableData] encoding:NSUTF8StringEncoding];
    newMd5 = [newMd5 stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    return newMd5;
}

-(BOOL)checksumValidForContentFolder:(NSString *)contentFolderPath {
    
    NSString * newMd5 = [self checksumForFolderAtPath:contentFolderPath];
 
    NSDictionary *zcatEntries = self.zcatDictionary;
    NSString *zcatMd5 = [zcatEntries objectForKey:contentFolderPath];
    if (![newMd5 isEqualToString:zcatMd5]) {
        [[PALogger sharedLoggerForClient:PALogClientSelfHeal] debug:@"local content changed externally for = [%@]",contentFolderPath];
        return NO;
    }
    return YES;
}
- (NSDictionary *)zcatDictionary {
    if (_zcatDictionary == nil) {
        NSString *statePath = [self.fsManager stateFolderPath];
        NSString *zcatFilePath = [statePath stringByAppendingPathComponent:kPAzCatFolder];
        zcatFilePath = [zcatFilePath stringByAppendingPathComponent:kPAzCatFileName];
        
        NSMutableDictionary *zcatDict = [[NSMutableDictionary alloc] init];
        if([self.fsManager doesFileExist:zcatFilePath]) {
            // read everything from text
            NSString* fileContents =
            [NSString stringWithContentsOfFile:zcatFilePath
                                      encoding:NSUTF8StringEncoding error:nil];
            
            // first, separate by new line
            NSArray* allLinedStrings =
            [fileContents componentsSeparatedByCharactersInSet:
             [NSCharacterSet newlineCharacterSet]];
            
            for(id zcatEntry in allLinedStrings) {
                NSString *temp = zcatEntry;
                if(temp && temp.length > 0 && ![[temp substringWithRange:NSMakeRange(0, 1)] isEqualToString:@"#"]) {
                    NSArray* zcatKeyVal =  [zcatEntry componentsSeparatedByString:@":"];
                    [zcatDict setValue:zcatKeyVal[1] forKey:zcatKeyVal[0]];
                }
            }
        }
        _zcatDictionary = [zcatDict copy];
    }
    return _zcatDictionary;
}
@end
