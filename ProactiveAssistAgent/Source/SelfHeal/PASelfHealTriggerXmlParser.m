//
//  PASelfHealTriggerXmlParser.m
//  ProactiveAssistAgent
//
//  Copyright © 2018 Avolin. All rights reserved.
//

#import "PASelfHealTriggerXmlParser.h"
NSString *const kPASHContentSetKey = @"content-set";
NSString *const kPASHContentKey = @"content";
NSString *const kPASHFieldSetKey = @"field-set";
NSString *const kPASHPropertySetKey = @"property-set";
NSString *const kPASHFieldKey = @"field";
NSString *const kPASHPropertyKey = @"property";
NSString *const kPASHValueKey = @"value";
NSString *const kPASHTypeKey = @"_type";
NSString *const KPASHNameKey = @"_name";
NSString *const kPASHFieldValueChar = @"sccf_field_value_char";
NSString *const kPASHPropertyRequestGuid = @"scc_request_guid";
NSString *const kPASHPropertyTitle = @"scc_title";
NSString *const kPASHMonitoringTypeAttributesKey = @"MonitoringTypeAttributes";
NSString *const kPASHMonitoringTypeKey = @"MonitoringType";

static NSString *const XMLDictionaryAttributesKey   = @"__attributes";
static NSString *const XMLDictionaryCommentsKey     = @"__comments";
static NSString *const XMLDictionaryTextKey         = @"__text";
static NSString *const XMLDictionaryNodeNameKey     = @"__name";
static NSString *const XMLDictionaryAttributePrefix = @"_";

@interface PASelfHealTriggerXmlParser() {
    PASelfHealTriggerInfo *_triggerInfo;
    NSString *_inputXmlPath;
}
@end

@implementation PASelfHealTriggerXmlParser

- (instancetype)initWithTriggerInfo:(PASelfHealTriggerInfo *)triggerInfo
{
    self = [super init];
    if (self) {
        if (triggerInfo.contentGuid && triggerInfo.contentVersion) {
            _triggerInfo = triggerInfo;
            _inputXmlPath = triggerInfo.contentXmlPath;
        } else {
            self =nil;
        }
    }
    return self;
}

- (void)parseInputConfigurationWithCompletion:(PASelfHealTriggerXmlParserCompletionBlock)completionBlock
{
    NSDictionary *xmlDict = [self dictionaryFromXmlAtPath:_inputXmlPath];
    @try {
        if (xmlDict) {
            NSDictionary *contentSetDict = [xmlDict valueForKey:kPASHContentSetKey];
            if ([contentSetDict isKindOfClass:[NSDictionary class]]) {
                NSDictionary *contentDict = [contentSetDict valueForKey:kPASHContentKey];
                if ([contentDict isKindOfClass:[NSDictionary class]]) {
                    NSDictionary *fieldSetDict = [contentDict valueForKey:kPASHFieldSetKey];
                    if ([fieldSetDict isKindOfClass:[NSDictionary class]]) {
                        NSArray *fieldsList = [fieldSetDict valueForKey:kPASHFieldKey];
                        if ([fieldsList isKindOfClass:[NSArray class]]) {
                            id monitoringType = [self valueOfField:kPASHMonitoringTypeKey inFieldList:fieldsList];
                            if(monitoringType) {
                                NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
                                f.numberStyle = NSNumberFormatterDecimalStyle;
                                SelfHealEventClass value = [[f numberFromString:monitoringType] intValue];
                                PASelfHealTriggerInfo *resultInfo;
                              
                                id attributes = [self valueOfField:kPASHMonitoringTypeAttributesKey inFieldList:fieldsList];
                              
                                //Below code will unescape special characters for Monitoring Type attribute
                                attributes = (__bridge  NSString *)CFXMLCreateStringByUnescapingEntities(NULL, (CFStringRef)attributes, NULL);
                                attributes = [[@"<field>" stringByAppendingString:attributes] stringByAppendingString:@"</field>"];
                                NSDictionary *attributesKeyDict = [self dictionaryFromXmlString:attributes];
                                attributes = [attributesKeyDict valueForKey:kPASHFieldKey];
                              
                                //None of the monitoring attribute will have item node. All contains field so commented below line.
                                //attributes = attributes[kPASHFieldKey] ? attributes[kPASHFieldKey]: attributes[@"item"];
                              
                                if (attributes) {
                                    resultInfo = [self processTriggerAttribues:attributes forEventClass:value];
                                } else {
                                    resultInfo = [[PASelfHealTriggerInfo alloc] initWithContentGuid: _triggerInfo.contentGuid
                                                                                            version:_triggerInfo.contentVersion contentXmlPath:_triggerInfo.contentXmlPath];
                                    resultInfo.monitorEventClass = value;
                                }
                              
                                resultInfo.userMode = [[self valueOfField:@"Mode" inFieldList:fieldsList] integerValue];
                                resultInfo.interval =  [[self valueOfField:@"Interval" inFieldList:fieldsList] integerValue];
                                resultInfo.triggerCommand = [self valueOfField:@"Command" inFieldList:fieldsList];
                                [[PALogger sharedLoggerForClient:PALogClientSelfHeal] debug:@"Cotent xml parsing succeeded for [%@]",_triggerInfo.contentGuid];
                                completionBlock(YES, resultInfo);
                                return;
                            }
                        }
                    }
                }
            }
        }
    } @catch (NSException *exception) {
        // Could not parse...
        [[PALogger sharedLoggerForClient:PALogClientSelfHeal] debug:@"Cotent xml parsing failed for [%@]",_triggerInfo.contentGuid];
        completionBlock(NO, _triggerInfo);
        return;
    }
    [[PALogger sharedLoggerForClient:PALogClientSelfHeal] debug:@"Cotent xml parsing failed for [%@]",_triggerInfo.contentGuid];
    completionBlock(NO, _triggerInfo);
}
-(PASelfHealTriggerInfo*)processTriggerAttribues:(NSArray *)attributes forEventClass:(SelfHealEventClass)eventClass {
    PASelfHealTriggerInfo *triggerInfo = [[PASelfHealTriggerInfo alloc] initWithContentGuid: _triggerInfo.contentGuid
                                                                                    version:_triggerInfo.contentVersion contentXmlPath:_triggerInfo.contentXmlPath];
    switch (eventClass) {
        case FileMonitoring:
            triggerInfo.monitorEventClass = FileMonitoring;
            [self populateFileMonitorAttributes:triggerInfo fromFieldList: attributes];
            break;
            
        case PlistMonitoring:
            triggerInfo.monitorEventClass = PlistMonitoring;
            [self populatePlistMonitorAttributes:triggerInfo fromFieldList:attributes];
            break;
        case HardwareMonitoring:
            triggerInfo.monitorEventClass = HardwareMonitoring;
            [self populateHardwareMonitorAttributes:triggerInfo fromFieldList:attributes];
            break;
        case BatteryLevelMonitoring:
            triggerInfo.monitorEventClass = BatteryLevelMonitoring;
            [self populateBatteryMonitorAttributes:triggerInfo fromFieldList:attributes];
            break;
        case StorageMonitoring:
            triggerInfo.monitorEventClass = StorageMonitoring;
            [self populateStorageMonitorAttributes:triggerInfo fromFieldList:attributes];
            break;
        case AppiicationMonitoring:
            triggerInfo.monitorEventClass = AppiicationMonitoring;
            [self populateApplicationMonitorAttributes:triggerInfo fromFieldList:attributes];
            break;
        case NetworkMonitoring:
            triggerInfo.monitorEventClass = NetworkMonitoring;
            [self populateNetworkMonitorAttributes:triggerInfo fromFieldList:attributes];
            break;
        case AppleScriptMonitoring:
            triggerInfo.monitorEventClass = AppleScriptMonitoring;
            [self populateAppleScriptAttributes:triggerInfo fromFieldList:attributes];
            break;
        default:
            triggerInfo.monitorEventClass = InvalidEventClass;
            break;
    }
    return triggerInfo;
}

- (void)getEventFilePaths:(NSArray *)fieldList triggerInfo:(PASelfHealTriggerInfo *)triggerInfo {
    id value = [self valueOfField:@"event_file_path" inFieldList:fieldList];
    if ([value isKindOfClass: [NSArray class]]) {
        triggerInfo.fileMonitorPaths = value;
    } else {
        triggerInfo.fileMonitorPaths = [self componentsFor:value SeperatedByDelimiter:@";"];//@[value];
    }
    
    triggerInfo.fileMonitorPaths = [triggerInfo.fileMonitorPaths valueForKey:@"stringByExpandingTildeInPath"];
}

- (void)getEventPlistKeyPaths:(NSArray *)fieldList triggerInfo:(PASelfHealTriggerInfo *)triggerInfo {
    id value = [self valueOfField:@"event_plsit_key_path" inFieldList:fieldList];
    if ([value isKindOfClass: [NSArray class]]) {
        triggerInfo.plistKeyPaths = value;
    } else {
        triggerInfo.plistKeyPaths = [self componentsFor:value SeperatedByDelimiter:@";"];//@[value];
    }
}

-(NSArray *)componentsFor:(NSString*)delimString SeperatedByDelimiter:(NSString*)delim {
    NSArray *components = [delimString componentsSeparatedByString:@";"];
    NSMutableArray *trimmedStrings = [NSMutableArray array];
    for (NSString *string in components) {
        NSString *trimmedString = [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        [trimmedStrings addObject:trimmedString];
    }
    return [trimmedStrings copy];
}

- (void)getBatteryLevelPercentages:(NSArray *)fieldList triggerInfo:(PASelfHealTriggerInfo *)triggerInfo {
    id value = [self valueOfField:@"event_monitor_level_percentage" inFieldList:fieldList];
    if ([value isKindOfClass: [NSArray class]]) {
        triggerInfo.batteryLevelPercentages = value;
    } else {
        triggerInfo.batteryLevelPercentages = [self componentsFor:value SeperatedByDelimiter:@";"];//@[value];
    }
    triggerInfo.batteryLevelPercentages = [triggerInfo.batteryLevelPercentages valueForKey:@"integerValue"];
}

- (void)getEventMonitorType:(NSArray *)fieldList triggerInfo:(PASelfHealTriggerInfo *)triggerInfo {
    id eventMonitorType = [self valueOfField:@"event_monitor_type" inFieldList:fieldList];
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    triggerInfo.fileMonitorType = [[f numberFromString:eventMonitorType] intValue];
}

-(void)populateFileMonitorAttributes:(PASelfHealTriggerInfo *)triggerInfo fromFieldList:(NSArray *)fieldList{
    [self getEventFilePaths:fieldList triggerInfo:triggerInfo];
    triggerInfo.fileMonitorShouldExcludeSubDirs = [[self valueOfField:@"event_exclude_sub_directories" inFieldList:fieldList] boolValue];
    [self getEventMonitorType:fieldList triggerInfo:triggerInfo];
}

-(void)populatePlistMonitorAttributes:(PASelfHealTriggerInfo *)triggerInfo fromFieldList:(NSArray *)fieldList {
    [self getEventFilePaths:fieldList triggerInfo:triggerInfo];
    triggerInfo.fileMonitorShouldExcludeSubDirs = [[self valueOfField:@"event_exclude_sub_directories" inFieldList:fieldList] boolValue];
    [self getEventMonitorType:fieldList triggerInfo:triggerInfo];
    [self getEventPlistKeyPaths:fieldList triggerInfo:triggerInfo];

}

-(void)populateHardwareMonitorAttributes:(PASelfHealTriggerInfo *)triggerInfo fromFieldList:(NSArray *)fieldList {
    [self getEventFilePaths:fieldList triggerInfo:triggerInfo];
}

-(void)populateBatteryMonitorAttributes:(PASelfHealTriggerInfo *)triggerInfo fromFieldList:(NSArray *)fieldList {
    [self getBatteryLevelPercentages:fieldList triggerInfo:triggerInfo];
}

-(void)populateStorageMonitorAttributes:(PASelfHealTriggerInfo *)triggerInfo fromFieldList:(NSArray *)fieldList {
    NSLog(@"%@", fieldList);
    NSMutableArray *attribs = [NSMutableArray new];
    if ([fieldList isKindOfClass:[NSDictionary class]]) {
        // there is only one storage attribute...
        fieldList = ((NSDictionary *)fieldList)[kPASHFieldKey];
        NSString* volumePath = [self dictionaryMatchingName:@"event_monitor_volume" inList:fieldList][kPASHValueKey];
        NSString* thresholdPercent = [self dictionaryMatchingName:@"event_monitor_level_percentage" inList:fieldList][kPASHValueKey];
        [attribs addObject: @{
                              kPASHStorageVolumePathKey: volumePath,
                              kPASHStorageVolumeThresholdPercentKey: @(thresholdPercent.integerValue)
                              }];
    } else{
        for (NSDictionary *elem in fieldList) {
            NSArray *value = elem[kPASHFieldKey];
            NSString* volumePath = [self dictionaryMatchingName:@"event_monitor_volume" inList:value][kPASHValueKey];
            NSString* thresholdPercent = [self dictionaryMatchingName:@"event_monitor_level_percentage" inList:value][kPASHValueKey];
            [attribs addObject: @{
                                  kPASHStorageVolumePathKey: volumePath,
                                  kPASHStorageVolumeThresholdPercentKey: @(thresholdPercent.integerValue)
                                  }];
        }
    }

    triggerInfo.storageAttributes = [attribs copy];
}

- (void)populateApplicationMonitorAttributes:(PASelfHealTriggerInfo *)triggerInfo fromFieldList: (NSArray *)fieldList {
    NSLog(@"%@", fieldList);
    NSMutableArray *attribs = [NSMutableArray new];
    if ([fieldList isKindOfClass:[NSDictionary class]]) {
        // there is only one application attributes item...
        fieldList = ((NSDictionary *)fieldList)[kPASHFieldKey];
        NSString* bundleIdentifier = [self dictionaryMatchingName:@"event_monitor_app_bundle_identifier" inList:fieldList][kPASHValueKey];
        NSString* bundleName = [self dictionaryMatchingName:@"event_monitor_app_bundle_name" inList:fieldList][kPASHValueKey];
        NSString* appversion = [self dictionaryMatchingName:@"event_monitor_app_version" inList:fieldList][kPASHValueKey];
        
        [attribs addObject: @{
                              kPASHAppBundleIdentifierKey: bundleIdentifier,
                              kPASHAppBundleNameKey: bundleName,
                              kPASHAppVersionKey: @(appversion.integerValue)
                              }];
    } else {
        for (NSDictionary *elem in fieldList) {
            NSArray *value = elem[kPASHFieldKey];
            NSString* bundleIdentifier = [self dictionaryMatchingName:@"event_monitor_app_bundle_identifier" inList:value][kPASHValueKey];
            NSString* bundleName = [self dictionaryMatchingName:@"event_monitor_app_bundle_name" inList:value][kPASHValueKey];
            NSString* appversion = [self dictionaryMatchingName:@"event_monitor_app_version" inList:value][kPASHValueKey];
            
            [attribs addObject: @{
                                  kPASHAppBundleIdentifierKey: bundleIdentifier,
                                  kPASHAppBundleNameKey: bundleName,
                                  kPASHAppVersionKey: @(appversion.integerValue)
                                  }];
        }
    }

    triggerInfo.applicationAttributes = [attribs copy];
}

-(void)populateNetworkMonitorAttributes:(PASelfHealTriggerInfo *)triggerInfo fromFieldList: (NSArray *)fieldList {
    triggerInfo.networkMonitorType = [[self valueOfField:@"event_monitor_network_type" inFieldList:fieldList] integerValue];
    if (triggerInfo.networkMonitorType == Firewall) {
        triggerInfo.firewallStatus = [[self valueOfField:@"event_monitor_firewall_status" inFieldList:fieldList] integerValue];
    } else if (triggerInfo.networkMonitorType == Wireless) {
        triggerInfo.wirelessStatus = [[self valueOfField:@"event_monitor_wireless_status" inFieldList:fieldList] integerValue];
    }
}

- (void)populateAppleScriptAttributes:(PASelfHealTriggerInfo *)triggerInfo fromFieldList: (NSArray *)fieldList {
    NSArray *value = [self valueOfField:@"AppleScriptSnippet" inFieldList:fieldList];
    NSDictionary *fieldDict = value[0];
    NSString *scriptDesc = [fieldDict valueForKey:XMLDictionaryTextKey];
    NSArray *components = [scriptDesc componentsSeparatedByString:@"|"];
    NSString *contentFolderPath = [_inputXmlPath stringByDeletingLastPathComponent];
    triggerInfo.appleScriptPath = [NSString stringWithFormat:@"%@/%@", contentFolderPath, components[0]];
}

- (id)valueOfField:(NSString *)fieldName inFieldList:(NSArray *)fieldList {
    // read window length
    NSDictionary *fieldOuterDict = [self dictionaryMatchingName:fieldName inList:fieldList];
    if ([fieldOuterDict isKindOfClass:[NSDictionary class]]) {
        NSMutableArray *fieldValues = nil;
        id fieldValueObject = [fieldOuterDict valueForKey:kPASHValueKey];
        if ([fieldValueObject isKindOfClass:[NSDictionary class]]) {
            fieldValues = [[NSMutableArray alloc]init];
            [fieldValues addObject:fieldValueObject];
        }
        else
        {
            return fieldValueObject;
        }
        NSDictionary *fieldDict = [self dictionaryMatchingName:kPASHFieldValueChar inList:fieldValues];
        if ([fieldDict isKindOfClass:[NSDictionary class]]) {
            id value = [fieldDict valueForKey:XMLDictionaryTextKey];
            if(value)
            {
                return value;
            } else
            {
                return [self childNodesForXmlDict:fieldDict];
            }
        }
    }
    return nil;
}

- (id)dictionaryMatchingName:(NSString *)name inList:(id)input {
    if ([input isKindOfClass:[NSArray class]]) {
        NSArray *inputList = input;
        if (inputList.count) {
            @try {
                NSArray *filtered = [inputList filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(_name == %@)", name]];
                if (filtered.count) {
                    return filtered[0];
                }
            } @catch (NSException *exception) {
                return nil;
            }
        }
    } else if([input isKindOfClass:[NSDictionary class]]) {
        NSDictionary *inputDict = input;
        if ([[inputDict valueForKey:KPASHNameKey] isEqualToString:name])
            return inputDict;
    }
    return nil;
}

- (nullable NSDictionary *)childNodesForXmlDict:(NSDictionary *)xmlDict
{
    NSMutableDictionary *filteredDict = [xmlDict mutableCopy];
    [filteredDict removeObjectsForKeys:@[XMLDictionaryAttributesKey, XMLDictionaryCommentsKey, XMLDictionaryTextKey, XMLDictionaryNodeNameKey]];
    for (NSString *key in filteredDict.allKeys)
    {
        if ([key hasPrefix:XMLDictionaryAttributePrefix])
        {
            [filteredDict removeObjectForKey:key];
        }
    }
    return filteredDict.count? filteredDict: nil;
}


@end
