//
//  PASelfHealTriggerXmlParser.h
//  ProactiveAssistAgent
//
//  Copyright © 2018 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <PACoreServices/PACoreServices.h>
#import "PASelfHealTriggerInfo.h"

typedef void(^PASelfHealTriggerXmlParserCompletionBlock)(BOOL success, PASelfHealTriggerInfo *triggerInfo);


@interface PASelfHealTriggerXmlParser : PAXmlParser

- (instancetype)initWithTriggerInfo:(PASelfHealTriggerInfo *)triggerInfo;

- (void)parseInputConfigurationWithCompletion:(PASelfHealTriggerXmlParserCompletionBlock)completionBlock;

@end
