//
//  PASelfHealTriggerInfo.h
//  ProactiveAssistAgent
//
//  Copyright © 2018 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PASelfHealEvent.h"

static NSString *const kPASHStorageVolumePathKey   = @"event_monitor_volume";
static NSString *const kPASHStorageVolumeThresholdPercentKey   = @"event_monitor_level_percentage";

static NSString *const kPASHAppBundleIdentifierKey   = @"event_monitor_app_bundle_identifier";
static NSString *const kPASHAppBundleNameKey   = @"event_monitor_app_bundle_name";
static NSString *const kPASHAppVersionKey = @"event_monitor_app_version";



@interface PASelfHealTriggerInfo : NSObject<NSSecureCoding>

@property (nonatomic, readonly) NSString *contentXmlPath;
@property (nonatomic, readonly) NSString *contentGuid;
@property (nonatomic, readonly) NSString *contentVersion;
@property (nonatomic) NSString *contentFolderChecksum;
@property (nonatomic, assign) SelfHealEventClass monitorEventClass;
// Attributes
@property (nonatomic) NSArray *fileMonitorPaths;
@property (nonatomic) NSArray *plistKeyPaths;
@property (nonatomic) NSArray *batteryLevelPercentages;
@property (nonatomic) NSArray *storageAttributes;
@property (nonatomic) NSArray *applicationAttributes;
@property (nonatomic, assign) NetworkMonitorType networkMonitorType;
@property (nonatomic, assign) FirewallStatus firewallStatus;
@property (nonatomic, assign) WirelessStatus wirelessStatus;

@property (nonatomic, assign) UserMode userMode;
@property (nonatomic, assign) NSUInteger interval;

@property (nonatomic) NSString *appleScriptPath;

@property (nonatomic, assign) BOOL fileMonitorShouldExcludeSubDirs;
@property (nonatomic, assign) EventMonitorType fileMonitorType;
@property (nonatomic, assign) ApplicationMonitorType appMonitorType;

@property (nonatomic) NSString *triggerCommand;

- (instancetype)initWithContentGuid:(NSString *)guid
                            version:(NSString *)version
                     contentXmlPath:xmlPath;

@end
