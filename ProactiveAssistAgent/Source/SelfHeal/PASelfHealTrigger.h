//
//  PASelfHealTrigger.h
//  ProactiveAssistAgent
//
//  Copyright © 2018 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PASelfHealEvent.h"
@interface PASelfHealTrigger : NSObject
@property (nonatomic, readonly) NSArray *currentlyMonitoredEvents;

- (void)initiateSelfHeal;
- (void)stopSelfHeal;
- (void)stopSelfHealForEvent: (NSString *)eventGuid;
- (BOOL)isSelfHealInProgress;

@end

