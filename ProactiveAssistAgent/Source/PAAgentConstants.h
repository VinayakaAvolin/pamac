//
//  PAAgentConstants.h
//  ProactiveAssistAgent
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifndef PAAgentConstants_h
#define PAAgentConstants_h


typedef enum {
  PASyncTypeNone,
  PASyncTypeRegular,
  PASyncTypeForceUpdate
} PASyncType;

typedef enum {
  PAManifestSyncContentTypeFresh = 1,
  PAManifestSyncContentTypeUpdate = 2,
} PAManifestSyncContentType;

typedef enum {
  PAManifestContentTypeOld = 1,
  PAManifestContentTypeNew = 2,
} PAManifestContentType;

typedef enum {
  PASyncStatusNone,
  PASyncStatusStarted,
  PASyncStatusInProgress,
  PASyncStatusComplete
} PASyncStatus;
typedef enum {
  PAPruneStatusNone,
  PAPruneStatusInProgress,
  PAPruneStatusComplete
} PAPruneStatus;

typedef enum {
  PAFileTypeNone,
  PAFileTypeProfileSet,
  PAFileTypeDataSet
} PAFileType;
#endif /* PAAgentConstants_h */
