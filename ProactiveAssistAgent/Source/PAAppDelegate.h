//
//  PAAppDelegate.h
//  ProactiveAssistAgent
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface PAAppDelegate : NSObject <NSApplicationDelegate>


@end

