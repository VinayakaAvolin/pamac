//
//  PATestClass.h
//  ProactiveAssistAgent
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PATestClass : NSObject

@property(nonatomic) NSString *name;
@property(nonatomic) BOOL isValidTest;

@property(nonatomic, readonly) NSString *xmlString;

@end
