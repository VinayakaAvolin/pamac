//
//  main.m
//  ProactiveAssistAgent
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "PALoginAgent.h"
#import "PASelfHealTrigger.h"

#import <vproc.h>
static void SignalHandler(int sigraised);
static void Log(NSString* str);

int main(int argc, const char * argv[]) {
  // LaunchServices automatically registers a mach service of the same
  // name as our bundle identifier.
  NSString *bundleId = [[NSBundle mainBundle] bundleIdentifier];
  NSXPCListener *listener = [[NSXPCListener alloc] initWithMachServiceName:bundleId];
  
  // Create the delegate of the listener.
  PALoginAgent *decisionAgent = [PALoginAgent sharedLoginAgent];
  listener.delegate = decisionAgent;
  [decisionAgent initialize];
  // Begin accepting incoming connections.
  // For mach service listeners, the resume method returns immediately so
  // we need to start our event loop manually.
  [listener resume];
    
    // Configure a dispatch source to listen for SIGTERM
    // Adapted from https://mikeash.com/pyblog/friday-qa-2011-04-01-signal-handling.html
    
    @autoreleasepool
    {
        // Set up GCD handler for SIGTERM
        dispatch_source_t source = dispatch_source_create(DISPATCH_SOURCE_TYPE_SIGNAL, SIGTERM, 0, dispatch_get_global_queue(0, 0));
        dispatch_source_set_event_handler(source, ^{
            SignalHandler(SIGTERM);
        });
        dispatch_resume(source);
        
        // Tell the standard signal handling mechanism to ignore SIGTERM
        struct sigaction action = { 0 };
        action.sa_handler = SIG_IGN;
        sigaction(SIGTERM, &action, NULL);
      
        // Start the run loop
            // The runloop also listens for SIGTERM and will return from here, so I'm just sending it right back in.
    [[NSRunLoop currentRunLoop] run];
    }
  
  return 0;
  //    return NSApplicationMain(argc, argv);
}

static void SignalHandler(int sigraised)
{
    // Open a transaction so that we dont get killed before getting to the end of this handler
    xpc_transaction_begin();
    
    Log(@"Handling Termination------------>");
    
    PASelfHealTrigger *selfHealTrigger = [[PASelfHealTrigger alloc] init];
    [selfHealTrigger stopSelfHeal];
    
    // Close the transaction
    xpc_transaction_end();
    Log(@"------------>Finished Termination Handling");
    exit(0);
}

static void Log(NSString* str)
{
    static NSObject* lockObj = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        lockObj = [NSObject new];
    });
    
    @synchronized(lockObj)
    {
        int fd = open("/tmp/agent_termination.log", O_WRONLY | O_CREAT | O_APPEND, 0777);
        if (fd <= 0) return;
        dprintf(fd, "%s\n", str.UTF8String);
        close(fd);
    }
}


