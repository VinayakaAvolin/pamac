//
//  PASyncConfiguration.m
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <PACoreServices/PACoreServices.h>
#import "PASyncConfiguration.h"

NSString *const kPASCAgentUiLogUploadRemotePathKey = @"Log Uploads:TG_DATAW_AGENTUILOGS";
NSString *const kPASCEventLogUploadRemotePathKey = @"Log Uploads:TG_DATAW_EVENTLOGS";
NSString *const kPASCMaxUpdateTimeOutKey = @"Sync:MaxUpdateTimeOut";
NSString *const kPASCUpdateFreqKey = @"Sync:UpdateFreq";
NSString *const kPASCUploadPerUpdateRatioKey = @"Sync:UploadPerUpdateRatio";

NSTimeInterval const kPASCDefaultMaxSyncTimeout = 8 * 60.0 * 60.0; // set a default value of 8 hours for Mac environment

@implementation PASyncConfiguration

- (instancetype)init
{
  self = [super init];
  if (self) {
    [self update];
  }
  return self;
}


- (BOOL)update {
  BOOL hasUpdate = false;
  
  PAFileSystemManager *manager = [[PAFileSystemManager alloc] init];
  NSDictionary *cfgContentDict = [PACfgFileProcessor parseConfigFileAtPath:manager.configFilePath];
  if (cfgContentDict.count) {
    // read sync timer interval (convert minutes into seconds)
    NSTimeInterval lastSyncInterval = _syncTimeIntervalInSeconds;
    NSTimeInterval currentSyncInterval = (NSTimeInterval)[cfgContentDict[kPASCUpdateFreqKey.lowercaseString] doubleValue] * 60.0;
    hasUpdate = (currentSyncInterval != lastSyncInterval);
    _syncTimeIntervalInSeconds = currentSyncInterval;
    
    // read sync timeout (convert hours into seconds)
    //        NSTimeInterval lastSyncTimeoutInterval = _syncTimeOutInSeconds;
    //        NSTimeInterval currentSyncTimeoutInterval = (NSTimeInterval)[cfgContentDict[kPASCMaxUpdateTimeOutKey.lowercaseString] doubleValue]  * 60.0 * 60.0;
    //        hasUpdate = (currentSyncTimeoutInterval != lastSyncTimeoutInterval);
    //_syncTimeOutInSeconds = currentSyncTimeoutInterval;
    
    // read upload ratio
    _uploadPerUpdateRatio = [cfgContentDict[kPASCUploadPerUpdateRatioKey.lowercaseString] integerValue];
    
    // log upload path
    _agentUILogUploadRemotePath = cfgContentDict[kPASCAgentUiLogUploadRemotePathKey.lowercaseString];
    _agentUILogUploadRemotePath = [manager stringByExpandingPath:_agentUILogUploadRemotePath];
    
    _eventLogUploadRemotePath = cfgContentDict[kPASCEventLogUploadRemotePathKey.lowercaseString];
    _eventLogUploadRemotePath = [manager stringByExpandingPath:_eventLogUploadRemotePath];
  }
  
  if (_syncTimeIntervalInSeconds <= 0) {
    _syncTimeIntervalInSeconds = 10 * 60.0;
  }
  _syncTimeOutInSeconds = kPASCDefaultMaxSyncTimeout;
  
  return hasUpdate;
}

@end
