//
//  PASyncConditionManager.m
//  ProactiveAssistAgent
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <PACoreServices/PACoreServices.h>
#import "PASyncConditionManager.h"

NSString *const kPASyncConditionManagerGetAllProcessesScript = @"do shell script \"ps -awwx\"";
NSString *const kPASyncConditionManagerPAExecutablePath = @"Contents/MacOS/ProactiveAssist";

@interface PASyncConditionManager () {
  PAFileSystemManager *_fileSystemManager;
}

@end

@implementation PASyncConditionManager

- (instancetype)init
{
  self = [super init];
  if (self) {
    _fileSystemManager = [[PAFileSystemManager alloc]init];
  }
  return self;
}
+ (void)canPruneForType:(PASyncType)type withCallback:(PASyncConditionManagerCallback)callback {
  switch (type) {
    case PASyncTypeRegular:
    {
      PASyncConditionManager *manager = [[PASyncConditionManager alloc]init];
      [manager checkWhetherPAAppLaunchedWithCallback:^(BOOL success) {
        callback(!success); // if app is NOT launched then sync can be performed at regular interval
      }];
    }
      break;
    case PASyncTypeForceUpdate:
      callback(true);
      break;
    default:
    {
      callback(false);
    }
      break;
  }
  
}

/**
 For sync following conditions should be met
 1) PA app should not be running
 
 @param callback <#callback description#>
 */
- (void)checkWhetherPAAppLaunchedWithCallback:(PASyncConditionManagerCallback)callback {
  PAAppConfiguration *appConfiguration = [PAConfigurationManager appConfigurations];
  PAAppleScript *script = [[PAAppleScript alloc]initWithScriptString:kPASyncConditionManagerGetAllProcessesScript];
  [script runWithCallBack:^(BOOL success, NSError *error, id result) {
    BOOL isAppLaunched = false;
    if (success && [result isKindOfClass:[NSString class]]) {
      NSString *paExecutablePath = [appConfiguration.mainAppPath stringByAppendingPathComponent:kPASyncConditionManagerPAExecutablePath];
      if (paExecutablePath) {
        NSString *paAppPath = [_fileSystemManager stringByExpandingPath:paExecutablePath];
        if (paAppPath) {
          isAppLaunched = [(NSString *)result containsString:paAppPath];
        }
      }
    }
    callback(isAppLaunched);
  }];
}

- (BOOL)doesAppDataExist {
  return ([_fileSystemManager doesDirectoryExist:[_fileSystemManager appDataFolderPath]] && [_fileSystemManager doesFileExist:[_fileSystemManager localManifestFilePath]]);
}


@end
