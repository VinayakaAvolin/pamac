//
//  PALogUploader.h
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef void(^PALogUploaderCallback)(BOOL success, NSError* error);

@interface PALogUploader : NSObject


- (void)uploadLogsWithCallback:(PALogUploaderCallback) callback;
@end
