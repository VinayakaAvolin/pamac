//
//  PASyncManager.h
//  ProactiveAssistAgent
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <PACoreServices/PACoreServices.h>

@protocol PASyncManagerDelegate <NSObject>

@optional

- (void)didFinishSync:(BOOL)finish;

@end

@interface PASyncManager : NSObject <PADownloadManagerUIDelegate>

@property(nonatomic, weak) id<PASyncManagerDelegate> syncDelegate;

- (void)syncContent;
- (void)forceSyncContent;

@end
