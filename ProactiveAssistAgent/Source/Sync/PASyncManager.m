//
//  PASyncManager.m
//  ProactiveAssistAgent
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <PACoreServices/PACoreServices.h>
#import <PASupportService/PASupportService.h>
#import <PAIpcService/PAIpcService.h>

#import "PASyncManager.h"
#import "NSString+Additions.h"
#import "NSArray+Additions.h"
#import "NSDate+Additions.h"
#import "NSProcessInfo+Additions.h"
#import "PATestClass.h"
#import "PALoginAgent.h"
#import "PATargetCollectionManager.h"
#import "PASyncConditionManager.h"
#import "PAAgentConstants.h"
#import "PACertificate.h"
#import "PASyncRegistryConstants.h"
#import "PASyncConfiguration.h"
#import "PALogUploader.h"
#import "PANetworkServiceManager+Sync.h"
#import "NSHost+Additions.h"
#import "PAPlistLockManager.h"
#import "PASelfHealTrigger.h"
#import "PACodeSignValidator.h"

NSTimeInterval const kPASyncTimerInterval = 10 * 60;//1 * 60 * 60; /// 1 hour time interval, in seconds

@interface PASyncManager () {
  NSTimer* _syncTimer;
  NSArray *_dataSetEntries;
  NSArray *_profileSetEntries;
  
  NSArray *_lastDataSetEntries;
  NSArray *_lastProfileSetEntries;
  
  NSMutableArray *_currentDownloadList;
  NSMutableArray *_currentProfileSetDownloadList;
  NSMutableArray *_currentPruningList;
  NSMutableArray *_tmpDownloadedFileList;
  
  PAResourceExtractor *_resourceExtractor;
  PADownloadManager *_downloadManager;
  PATargetCollectionManager *_targetCollectionManager;
  PAAppConfiguration *_appconfigurations;
  PASyncConfiguration *_syncConfigurations;
  
  PAManifestSyncContentType _syncContentType; // to hold current sync content type;
  PAManifestContentType _manifestContentType; // to hold manifest content type sent for parsing;
  PASyncType _syncType; // to hold current sync type;
  PASyncStatus _syncStatus; // to hold current sync status;
  PAPruneStatus _pruneStatus; // to hold current prune status;
  NSOperationQueue *_operationQueue;
  PALogUploader* _logUploader;
  PAFileType _fileType; /// to differentiate between profile set and data set
}
@property (nonatomic, retain) PAFileSystemManager *fileSystemManager;

@end
@implementation PASyncManager

- (instancetype)init
{
  self = [super init];
  if (self) {
    self.fileSystemManager = [[PAFileSystemManager alloc]init];
    _resourceExtractor = [[PAResourceExtractor alloc]init];
    _downloadManager = [[PADownloadManager alloc]init];
    _targetCollectionManager = [[PATargetCollectionManager alloc]init];
    _syncType = PASyncTypeNone;
    _operationQueue = [[NSOperationQueue alloc]init];
    _logUploader = [[PALogUploader alloc] init];
    [self readConfigurations];
    [self scheduleSync];
    [self addNotificationObservers];
  }
  return self;
}

- (void)dealloc {
  [self removeNotificationObservers];
}
- (void)forceSyncContent {
  [[PALogger sharedLoggerForClient:PALogClientSyncManager] debug:@"Request received to Force sync"];
  _syncType = PASyncTypeForceUpdate;
  _syncStatus = PASyncStatusNone;
  [self syncContent];
}
- (void)syncContent {
  [_operationQueue cancelAllOperations];
  NSInvocationOperation *syncOperation = [[NSInvocationOperation alloc]
                                          initWithTarget:self
                                          selector:@selector(syncContentOperation)
                                          object:nil];
  [_operationQueue addOperation:syncOperation];
}
- (void)syncContentOperation {
  [self startSync];
  
  /// TODO: Remove this
  //    [self addToCache];
  //    [self checkSprocketService];
  //    [self testScriptRunner];
  //    [self testPlist];
  //    [self testPlistServiceViaPrivilegeTool];
  //    [self testXml];
  //    [self testEndpointInfo];
  //    [self testAllEndpointInfo];
  //    [self testDownloadOfferDmg];
  //    [self testDownloadOfferPkg];
  //    [self testCabExtractor];
  //    [self testNotificationTrayService];
  //    [self testJs];
  //    [self testHtm];
  //    [self testcertificate];
  //    [self testCfgFileParser];
  //    [self testLaunchService];
  //    [self checkSprocketSupportAction];
  //    [self testZipExtractor];
}

#pragma mark - Private methods

- (void)syncDidFinish:(BOOL)finish {
  [self.syncDelegate didFinishSync:finish];
}

- (void)scheduleSync {
  dispatch_async(dispatch_get_main_queue(), ^{
    NSTimeInterval syncInterval = [self syncTimerInterval];
    [[PALogger sharedLoggerForClient:PALogClientSyncManager] debug:@"Sync timer is set to [%f] seconds",syncInterval];
    _syncTimer = [NSTimer scheduledTimerWithTimeInterval:syncInterval
                                                  target:self
                                                selector:@selector(syncTimerFired:)
                                                userInfo:nil
                                                 repeats:YES];
  });
}

- (NSTimeInterval)syncTimerInterval {
  NSTimeInterval syncInterval = [self syncInterval];
  NSTimeInterval syncTimerInterval = syncInterval;
  NSString *nextSyncDateTimeInRegistry = [self getSyncRegistryValueAtPath:kPASyncRegistryKey key:kPANextUpdateTime];
  [[PALogger sharedLoggerForClient:PALogClientSyncManager] debug:@"Next sync date time set in the registry is [%@]",nextSyncDateTimeInRegistry];
  
  if (![nextSyncDateTimeInRegistry isEmptyOrHasOnlyWhiteSpaces]) {
    NSDate *nextSyncDate = [NSDate dateFromString:nextSyncDateTimeInRegistry format:Format9];
    NSDate *dateNow = [NSDate systemDate];
    NSComparisonResult comparisonResult = [dateNow compare:nextSyncDate];
    // is past date; find difference between nextSyncDate and dateNow
    if (comparisonResult == NSOrderedDescending) {
      NSTimeInterval diff = [dateNow timeIntervalSinceDate:nextSyncDate];
      // if sync interval is 10 minutes; and diff is 8, then need to set sync interval to 2 minutes
      if (diff < syncInterval) {
        syncTimerInterval = syncInterval - diff;
      }
    } else if (comparisonResult == NSOrderedAscending) { // if future date, find difference between nextSyncDate and dateNow
      NSTimeInterval diff = [nextSyncDate timeIntervalSinceDate:dateNow];
      // if sync interval is 10 minutes; and diff is 8, then need to set sync interval to 8 minutes
      if (diff < syncInterval) {
        syncTimerInterval = diff;
      }
      // if sync interval is 10 minutes; and diff is 12, then need to set sync interval to 10 minutes, which is nothing but actual sync interval in the config.cfg
    }
  }
  // update next sync date , time
  NSDate *nextSyncDateTime = [[NSDate systemDate] dateByAddingTimeInterval:syncTimerInterval];
  NSString *nextSyncDateTimeStr = [nextSyncDateTime dateStringWithFormat:Format9];
  [self updateSyncRegistryAtPath:kPASyncRegistryKey key:kPANextUpdateTime value:nextSyncDateTimeStr];
  [[PALogger sharedLoggerForClient:PALogClientSyncManager] debug:@"Next sync date time is reset to [%@]",nextSyncDateTimeStr];
  
  return syncTimerInterval;
}
- (NSTimeInterval)syncInterval {
  NSTimeInterval syncInterval = (_syncConfigurations.syncTimeIntervalInSeconds > 0) ? _syncConfigurations.syncTimeIntervalInSeconds : kPASyncTimerInterval;
  return syncInterval;
}

- (void)stopSyncTimer {
  dispatch_async(dispatch_get_main_queue(), ^{
    if (_syncTimer.isValid) {
      [_syncTimer invalidate];
    }
  });
}
// on data sync check if config.cfg file content has changed; if so do the necessary setup
- (void)reScheduleSyncTimerIfRequired {
  [_syncConfigurations update];
  [self stopSyncTimer];
  [self scheduleSync];
}

- (void)syncTimerFired:(NSTimer *)timer {
  [[PALogger sharedLoggerForClient:PALogClientSyncManager] debug:@"Regular sync timer fired"];
  
  _syncType = PASyncTypeRegular;
  [self syncContent];
}

- (void)readConfigurations {
  _appconfigurations = [PAConfigurationManager appConfigurations];
  _syncConfigurations = [[PASyncConfiguration alloc] init];
}

- (void)startSync {
  if (_downloadManager.networkReachabilityStatus == PANetworkReachabilityStatusNotReachable) {
    [[PALogger sharedLoggerForClient:PALogClientSyncManager] debug:@"Cannot sync network unreachable"];
    return;
  }
  
  if (_syncStatus != PASyncStatusNone) {
    return;
  }
  NSDate *currentSyncDateTime = [NSDate systemDate];
  //    NSDate *nextSyncDateTime = [currentSyncDateTime dateByAddingTimeInterval:[self syncInterval]];
  NSString *currentSyncDateTimeStr = [currentSyncDateTime dateStringWithFormat:Format9];
  //    NSString *nextSyncDateTimeStr = [nextSyncDateTime dateStringWithFormat:Format9];
  [[PALogger sharedLoggerForClient:PALogClientSyncManager] debug:@"Sync started at [%@]",currentSyncDateTimeStr];
  
  [self stopSyncTimer];
  [self updateSyncRegistryAtPath:kPASyncRegistryKey key:kPALastAttemptUpdateTime value:currentSyncDateTimeStr];
  //    [self updateSyncRegistryAtPath:kPASyncRegistryKey key:kPANextUpdateTime value:nextSyncDateTimeStr];
  
  _syncStatus = PASyncStatusStarted;
  _pruneStatus = PAPruneStatusNone;
  _fileType = PAFileTypeNone;
  _currentDownloadList = [NSMutableArray array];
  _currentProfileSetDownloadList = [NSMutableArray array];
  _currentPruningList = [NSMutableArray array];
  _tmpDownloadedFileList = [NSMutableArray array];
  /*
   Check if manifest file present at data folder path;
   if not present its fresh sync; else consider it as update
   */
  
  //Delete existing targetting collection output xml if exists
  [self.fileSystemManager deleteFile:[_targetCollectionManager targetCollectionXmlPath]];
  
  NSString *localManifestPath = [self localManifestFilePath];
  if (nil != localManifestPath &&
      ![self.fileSystemManager doesFileExist:localManifestPath]) {
    
    //Delete existing data folder and create empty Data named directory
    [self.fileSystemManager deleteDirectory:[self.fileSystemManager appDataFolderPath]];
    [self.fileSystemManager createDirectoryAtPath:[self.fileSystemManager appDataFolderPath] withIntermediateDirectories:YES attributes:nil error:nil];
    
    _syncContentType = PAManifestSyncContentTypeFresh;
    _manifestContentType = PAManifestContentTypeNew;
    [self downloadManifest];
  } else {
    _syncContentType = PAManifestSyncContentTypeUpdate;
    _manifestContentType = PAManifestContentTypeOld;
    [self parseOldManifest];
  }
}

#pragma mark - PADownloadManagerUIDelegate methods
- (void) didFinishAll {
  //    _downloadManager.uiDelegate = nil;
}
- (void) didReachIndividualProgress:(float)progress onDownloadTask:(PADownloadTask *) task {
  //- (void) didReachIndividualProgress:(float)progress onDownloadTask:(ObjectiveCDMDownloadTask* ) task {
  if (progress >= 1) {
    [[PALogger sharedLoggerForClient:PALogClientSyncManager] debug:@"Did complete downloading [%@] to path [%@]",task.taskId, task.downloadedFilePath ];
    NSString *downloadedFilePath = task.downloadedFilePath;
    
    if (downloadedFilePath &&
        [_fileSystemManager doesFileExist:downloadedFilePath] &&
        ![_tmpDownloadedFileList doesContain:downloadedFilePath]) {
      [_tmpDownloadedFileList addObject:downloadedFilePath];
      [self extractFile:task];
    }
  }
}
- (void)didHitDownloadErrorOnTask:(PADownloadTask*)task {
  NSString *manifestXmlFile = _appconfigurations.manifestXmlFile;
  if ( [task.taskId isEqualToString:manifestXmlFile]) {
    [self updateSyncRegistryAtPath:kPASyncRegistryKey key:kPALastUpdateErrorCode value:@(1)];
    [self setDataSyncStatus:PASyncStatusNone];
    [self cleanupOnSync:false];
  }
  [[PALogger sharedLoggerForClient:PALogClientSyncManager] debug:@"Failed to download [%]  Error [%@]",task.taskId, task.error];
}
#pragma mark -

- (void)downloadManifest {
  [[PALogger sharedLoggerForClient:PALogClientSyncManager] debug:@"Download manifest at remote path [%@]",[self remoteManifestFilePath]];
  
  NSString *tempFolderPath = [self.fileSystemManager temporaryDownloadsFolderPath];
  if (![self.fileSystemManager doesFileExist:tempFolderPath]) {
    [self.fileSystemManager createDirectoryAtPath:tempFolderPath withIntermediateDirectories:YES attributes:nil error:nil];
  }
  NSString *manifestZipFile = _appconfigurations.manifestZipFile;
  NSString *manifestXmlFile = _appconfigurations.manifestXmlFile;
  _downloadManager.uiDelegate = self;
  PADownloadableFileInfo *info = [[PADownloadableFileInfo alloc] initWithUrl:[self remoteManifestFilePath]
                                                                 destination:[NSString stringWithFormat:@"%@/%@",tempFolderPath,manifestZipFile]
                                                                    fileSize:0
                                                                    checksum:@"" identifier:manifestXmlFile ];
  
  [_downloadManager addFileToDownload:info];
  if (![self.fileSystemManager doesFileExist:[NSString stringWithFormat:@"%@/%@",tempFolderPath,manifestZipFile]]) {
    [[PALogger sharedLoggerForClient:PALogClientSyncManager] debug:@"Unable to download manifest file from remote path [%@]",[self remoteManifestFilePath]];
  }
}


- (void)extractFile:(PADownloadTask *)task  {
  NSString *manifestXmlFile = _appconfigurations.manifestXmlFile;
  if ( [task.taskId isEqualToString:manifestXmlFile]) {
    [self unarchiveManifest:task];
  } else {
    [self unarchiveManifestFileEntry:task];
  }
}

- (void)parseManifest {
  _manifestContentType = PAManifestContentTypeNew;
  PAManifestParser *xmlParser = [[PAManifestParser alloc] init];
  
  NSString *tmpManifestPath = [_fileSystemManager temporaryManifestFilePath];
  NSString *syncCertificatePath = [_fileSystemManager syncCertificatePath];
  NSString *syncCertificateFileType = syncCertificatePath.pathExtension;
  if (![_fileSystemManager doesFileExist:tmpManifestPath] ||
      ![_fileSystemManager doesFileExist:syncCertificatePath] ||
      ![syncCertificateFileType isEqualToString:@"p7b"]) {
    [[PALogger sharedLoggerForClient:PALogClientSyncManager] debug:@"Either Sync certificate does not exist or it is not a p7b file."];
    [self cleanupOnSync:false];
    return;
  }
  
  //    NSString *syncCertificatePath = @"/Users/vinayaka.s/Library/Application Support/ProactiveAssist/SyncCertificate_1.p7b ";
  // only if certificate validation succeeds then proceed
  [xmlParser validateManifestCertificate:tmpManifestPath andSyncCertificate:syncCertificatePath withCompletion:^(BOOL success, NSArray *itemList) {
    if (success) {
      [[PALogger sharedLoggerForClient:PALogClientSyncManager] debug:@"Manifest certificate and sync certificate are matching. So proceed."];
      
      // in case of update; copy manifest to data folder, after content update
      NSString *manifestFilePath;
      switch (_syncContentType) {
        case PAManifestSyncContentTypeFresh:
          [self copyLatestManifestToDataFolder];
          manifestFilePath = [self localManifestFilePath];
          break;
        default:
          manifestFilePath = [self temporaryManifestFilePath];
          break;
      }
      NSDictionary *xmlDict = [xmlParser dictionaryFromXmlAtPath:manifestFilePath];
      _fileType = PAFileTypeProfileSet;
      [xmlParser parsedProfileSetFromManifest:xmlDict completion:^(BOOL success, NSArray *dataSets) {
        [self handleManifestProfileSets:dataSets];
        [self updateProfileData];
      }];
    } else {
      [[PALogger sharedLoggerForClient:PALogClientSyncManager] debug:@"Manifest certificate and sync certificate does not match."];
      [self cleanupOnSync:false];
    }
  }];
}

- (void)parseManifestDatasets {
  _fileType = PAFileTypeDataSet;
  _manifestContentType = PAManifestContentTypeNew;
  PAManifestParser *xmlParser = [[PAManifestParser alloc] init];
  
  // in case of update; copy manifest to data folder, after content update
  NSString *manifestFilePath;
  switch (_syncContentType) {
    case PAManifestSyncContentTypeFresh:
      [self copyLatestManifestToDataFolder];
      manifestFilePath = [self localManifestFilePath];
      break;
    default:
      manifestFilePath = [self temporaryManifestFilePath];
      break;
  }
  
  NSDictionary *xmlDict = [xmlParser dictionaryFromXmlAtPath:manifestFilePath];
  
  [_targetCollectionManager constructTargetCollectionFromXml:xmlDict type:PATargetCollectionManifestTypeLatest completion:^(BOOL success) {
    if (success) {
      [xmlParser parsedDataSetFromManifest:xmlDict completion:^(BOOL success, NSArray *dataSets) {
        [self handleManifestDataSets:dataSets];
        /// update content
        [self updateData];
      }];
    } else {
      [self cleanupOnSync:false];
    }
  }];
}
- (void) copyLatestManifestToDataFolder {
  NSString *tempManifestPath = [self temporaryManifestFilePath];
  NSString *newManifestPath = [self localManifestFilePath];
  if (nil != tempManifestPath && [self.fileSystemManager doesFileExist:tempManifestPath]) {
    [self.fileSystemManager copyFile:tempManifestPath destinationPath:newManifestPath];
    [[PALogger sharedLoggerForClient:PALogClientSyncManager] debug:@"Copied latest manifest to data folder."];
  }
}
- (void) cleanupOnSync:(BOOL)syncStatus {
  NSString *tempManifestPath = [self temporaryManifestFilePath];
  if ([self.fileSystemManager doesFileExist:tempManifestPath]) {
    [self.fileSystemManager deleteFile:tempManifestPath];
  }
  [self syncDidFinish:syncStatus];
  /// sync successful
  [self reScheduleSyncTimerIfRequired];
  [_targetCollectionManager deleteTargetCollectionXml];
  if (syncStatus) {
    [[PALoginAgent sharedLoginAgent] checkForNewAlerts];
    
    PAAppConfiguration* config = [[PAAppConfiguration alloc] init];
    NSString* providerId = [config providerId];
    NSString* runCommand = [NSString stringWithFormat:@"%@ /p %@ /path /messages /ini /minibcont.ini /syncevent",kPALSCommandNotificationTray, providerId];
    [PALaunchServiceManager runCommand:runCommand];
  }
}

/*
 Parse old manifest and keep data-set and profile-set-mac entries ready to compare against the new/latest manifest.
 Once old manifest parsing is complete; download latest manifest
 */
- (void)parseOldManifest {
  _manifestContentType = PAManifestContentTypeOld;
  // Do any additional setup after loading the view.
  PAManifestParser *xmlParser = [[PAManifestParser alloc] init];
  NSDictionary *xmlDict = [xmlParser dictionaryFromXmlAtPath:[self localManifestFilePath]];
  [_targetCollectionManager constructTargetCollectionFromXml:xmlDict type:PATargetCollectionManifestTypeOld completion:^(BOOL success) {
    if (success) {
      [xmlParser parsedDataSetFromManifest:xmlDict completion:^(BOOL success, NSArray *dataSets) {
        [self handleManifestDataSets:dataSets];
        [xmlParser parsedProfileSetFromManifest:xmlDict completion:^(BOOL success, NSArray *dataSets) {
          [self handleManifestProfileSets:dataSets];
          [self downloadManifest]; // download latest manifest
        }];
      }];
    }
  }];
}
- (void)handleManifestProfileSets:(NSArray *)dataSets {
  switch (_manifestContentType) {
    case PAManifestContentTypeNew:
      _profileSetEntries = [NSArray arrayWithArray: dataSets];
      switch (_syncContentType) {
        case PAManifestSyncContentTypeUpdate:
          [self verifyManifestProfileSets];
          break;
        default:
          [self downloadManifestDataSets:dataSets];
          break;
      }
      break;
    default:
      _lastProfileSetEntries = [NSArray arrayWithArray: dataSets];
      break;
  }
}
- (void)handleManifestDataSets:(NSArray *)dataSets {
  NSDictionary *_dictdataSetEntries;
  switch (_manifestContentType) {
    case PAManifestContentTypeNew:
       //Commented line to get dictionarywithdictionary object
       //_dataSetEntries = [_targetCollectionManager filteredDataSetsAfterApplyingTargetCollectionRuleOn:dataSets type:PATargetCollectionManifestTypeLatest];

      _dictdataSetEntries = [_targetCollectionManager filteredDataSetsAfterApplyingTargetCollectionRuleOn:dataSets type:PATargetCollectionManifestTypeLatest];
      _dataSetEntries = [_dictdataSetEntries objectForKey:kPAFilteredataSets];
      
      //In case required for pruning contents for ne manifest content type uncomment below line
      //NSArray *missingEntriesOfOldManifestFilter = [_dict objectForKey:kPANonFilteredataSets];
      
            switch (_syncContentType) {
        case PAManifestSyncContentTypeUpdate:
          [self verifyManifestDataSets];
          break;
        default:
          [self downloadManifestDataSets:_dataSetEntries];
          break;
      }
      break;
    default:
      _dictdataSetEntries = [_targetCollectionManager filteredDataSetsAfterApplyingTargetCollectionRuleOn:dataSets type:PATargetCollectionManifestTypeOld];
      _lastDataSetEntries = [_dictdataSetEntries objectForKey:kPAFilteredataSets];
      NSArray *nonfilteredDatasets = [_dictdataSetEntries objectForKey:kPANonFilteredataSets];
      if (nonfilteredDatasets.count) {
        [_currentPruningList addObjectsFromArray:nonfilteredDatasets];
      }
      break;
  }
}

- (void) verifyManifestProfileSets {
  [self verifyCurrentManifestFileEntries:_profileSetEntries withLastManifestFileEntries:_lastProfileSetEntries];
}

- (void) verifyManifestDataSets {
  [self verifyCurrentManifestFileEntries:_dataSetEntries withLastManifestFileEntries:_lastDataSetEntries];
}

/*
 1) For each  data-set and profile-set-mac entries in the last saved manifest,
 a) Get matching entry in latest manifest.
 b) If matching entry found then,
 i) Compare the checksum.
 ii) If checksums are different; then mark data-set or profile-set-mac entry for update.
 iii) If checksums are same; then do nothing.
 c) If matching entry not found,
 i) Mark data-set or profile-set-mac entry for pruning.
 
 2) Once all data-set and profile-set-mac entries are iterated,
 a) Download and extract the files for data-set and profile-set-mac entries that are marked for update.
 b) Download and extract the files for new data-set and profile-set-mac entries in the latest manifest, which are not present in the old manifest.
 c) Delete files from user system for data-set and profile-set-mac entries that are marked for pruning.
 d) Copy the manifest to data folder.
 e) Perform cleanup on sync
 */
- (void) verifyCurrentManifestFileEntries:(NSArray *)currentDataSets
              withLastManifestFileEntries:(NSArray *)lastDataSets {
  _downloadManager.uiDelegate = self;
  
  NSString *assistAgentPath = _appconfigurations.assistAgentFolderPath;
  assistAgentPath = [self.fileSystemManager stringByExpandingPath:assistAgentPath];
  NSBundle* bundle = [NSBundle bundleWithPath:assistAgentPath];
  NSString* dirScriptPath = [bundle pathForResource:@"gendirmd5" ofType:@"sh"];
  NSString* fileScriptPath = [bundle pathForResource:@"genfilemd5" ofType:@"sh"];
  
  NSString *statePath = [self.fileSystemManager stateFolderPath];
  NSString *dataPath = [self.fileSystemManager appDataFolderPath];
  NSString *zcatFilePath = [statePath stringByAppendingPathComponent:kPAzCatFolder];
  zcatFilePath = [zcatFilePath stringByAppendingPathComponent:kPAzCatFileName];
  
  NSMutableDictionary *zcatDict = [[NSMutableDictionary alloc] init];
  NSMutableDictionary *zcatFilenameDict = [[NSMutableDictionary alloc] init];
  
  if([self.fileSystemManager doesFileExist:zcatFilePath]) {
    // read everything from text
    NSString* fileContents =
    [NSString stringWithContentsOfFile:zcatFilePath
                              encoding:NSUTF8StringEncoding error:nil];
    
    // first, separate by new line
    NSArray* allLinedStrings =
    [fileContents componentsSeparatedByCharactersInSet:
     [NSCharacterSet newlineCharacterSet]];
    
    for(id zcatEntry in allLinedStrings) {
      NSString *temp = zcatEntry;
      if(temp && temp.length > 0 && ![[temp substringWithRange:NSMakeRange(0, 1)] isEqualToString:@"#"]) {
        NSArray* zcatKeyVal =  [zcatEntry componentsSeparatedByString:@":"];
        [zcatDict setValue:zcatKeyVal[1] forKey:zcatKeyVal[0]];
        if([zcatKeyVal count] > 2) {
          [zcatFilenameDict setValue:zcatKeyVal[2] forKey:zcatKeyVal[0]];
        }
      }
    }
  }
  
  // Check for common entries in old and latest manifest files; need to check for update; if checksum mismatch then queue such file entries for downloading
  NSArray *commonEntriesOfOldAndLatestManifest = [lastDataSets commonObjectsInArray:currentDataSets];
  for (PADataSetInfo *dataSetFileInfo in commonEntriesOfOldAndLatestManifest) {
    PADataSetInfo *matchingEntryInCurrentManifest = [self dataSetMatchingFileSystemId:dataSetFileInfo.fileSystemId inList:currentDataSets];
    /// Match found
    if (matchingEntryInCurrentManifest) {
      bool resync = false;
      
      NSString *lastChecksum = dataSetFileInfo.checksum;
      NSString *newChecksum = matchingEntryInCurrentManifest.checksum;
      /// Checksum mismatch; queue for downloading
      if (![lastChecksum isEqualToString: newChecksum]) {
        [[PALogger sharedLoggerForClient:PALogClientSyncManager] debug:@"checksum different for = [%@]",matchingEntryInCurrentManifest.fileSystemId];
        resync = true;
      }
      
      if(!resync) {
        NSString *subPath = matchingEntryInCurrentManifest.targetFolder;
        subPath = [subPath stringByReplacingOccurrencesOfString:@"%DirUserServer%data" withString:dataPath];
        subPath = [subPath stringByAppendingPathComponent:matchingEntryInCurrentManifest.fileName];
        subPath = [subPath stringByDeletingPathExtension];
        subPath = [subPath stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
        subPath = [subPath stringByReplacingOccurrencesOfString:@"//" withString:@"/"];
        BOOL isDir = NO;
        [[NSFileManager defaultManager] fileExistsAtPath:subPath isDirectory:&isDir];
        
        if([zcatDict objectForKey:subPath] == nil) {
          [[PALogger sharedLoggerForClient:PALogClientSyncManager] debug:@"local content missing for = [%@]",matchingEntryInCurrentManifest.fileSystemId];
          resync = true;
        } else {
          
          NSTask *task = [[NSTask alloc] init];
          if (isDir) {
            task.launchPath = dirScriptPath;
          } else {
            task.launchPath = fileScriptPath;
          }
          if([zcatFilenameDict objectForKey:subPath] == nil) {
            task.arguments = [NSArray arrayWithObjects:subPath, nil];
          } else {
            NSString *fullFileName = [zcatFilenameDict objectForKey:subPath];
            task.arguments = [NSArray arrayWithObjects:fullFileName, nil];
          }
          
          NSPipe *pipe = [NSPipe pipe];
          task.standardOutput = pipe;
          
          NSFileHandle *fileHandle = [pipe fileHandleForReading];
          [task launch];
          [task waitUntilExit];
          
          NSString *newMd5 = [[NSString alloc] initWithData:[fileHandle availableData] encoding:NSUTF8StringEncoding];
          newMd5 = [newMd5 stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
          NSString *oldMd5 = [zcatDict objectForKey:subPath];
          
          if (![newMd5 isEqualToString:oldMd5]) {
            [[PALogger sharedLoggerForClient:PALogClientSyncManager] debug:@"local content changed externally for = [%@]",matchingEntryInCurrentManifest.fileSystemId];
            resync = true;
          }
        }
      }
      if (resync) {
        PADownloadableFileInfo *info = [self downloadableFileInfoFrom:matchingEntryInCurrentManifest];
        if (info) {
          switch (_fileType) {
            case PAFileTypeDataSet:
              [_currentDownloadList addObject:info];
              break;
            case PAFileTypeProfileSet:
              [_currentProfileSetDownloadList addObject:info];
              break;
            default:
              break;
          }
        }
        [[PALogger sharedLoggerForClient:PALogClientSyncManager] debug:@"checksum different for = [%@]",matchingEntryInCurrentManifest.fileSystemId];
      }
    }
  }
  
  // check for old manifest file entries that are missing in latest manifest;  queue them for pruning
  NSArray *missingEntriesOfOldManifest = [lastDataSets removeObjectsInArray:currentDataSets];
  if (missingEntriesOfOldManifest.count) {
    [_currentPruningList addObjectsFromArray:missingEntriesOfOldManifest];
  }
  // check for new manifest file entries which are not present in old manifest;  queue them for downloading
  NSArray *newEntriesInLatestManifest = [currentDataSets removeObjectsInArray:lastDataSets];
  for (PADataSetInfo *dataSetFileInfo in newEntriesInLatestManifest) {
    PADownloadableFileInfo *info = [self downloadableFileInfoFrom:dataSetFileInfo];
    if (info) {
      switch (_fileType) {
        case PAFileTypeDataSet:
          [_currentDownloadList addObject:info];
          break;
        case PAFileTypeProfileSet:
          [_currentProfileSetDownloadList addObject:info];
          break;
        default:
          break;
      }
    }
  }
}
- (void)updateProfileData {
  _syncStatus = PASyncStatusInProgress;
  // queue file sets for downloading
  if (_currentProfileSetDownloadList.count) {
    [[PALogger sharedLoggerForClient:PALogClientSyncManager] debug:@"Download profile sets now."];
    [_downloadManager addFileListToDownload:_currentProfileSetDownloadList];
  } else {
    [[PALogger sharedLoggerForClient:PALogClientSyncManager] debug:@"No profile sets to sync. Check for data sets"];
    [self parseManifestDatasets];
  }
}
- (void)updateData {
  _syncStatus = PASyncStatusInProgress;
  // queue file sets for downloading
  if (_currentDownloadList.count) {
    [_downloadManager addFileListToDownload:_currentDownloadList];
  } else {
    [[PALogger sharedLoggerForClient:PALogClientSyncManager] debug:@"Nothing to sync... now prune"];
    [self pruneData];
    [self setDataSyncStatus:PASyncStatusNone];
  }
}

- (void)setDataSyncStatus:(PASyncStatus)syncStatus {
  _syncStatus = syncStatus;
  if (_syncStatus == PASyncStatusComplete) {
    [self updateSyncRegistryAtPath:kPASyncRegistryKey key:kPALastSuccessUpdateTime value:[[NSDate systemDate] dateStringWithFormat:Format9]];
    [self updateSyncRegistryAtPath:kPASyncRegistryKey key:kPALastUpdateErrorCode value:@(0)];
    [[PALogger sharedLoggerForClient:PALogClientSyncManager] debug:@"Sync completetd... now prune"];
    [self pruneData];
    [self setDataSyncStatus:PASyncStatusNone];
  }
}

- (void)pruneData {
  [PASyncConditionManager canPruneForType:_syncType withCallback:^(BOOL canContinue) {
    if (canContinue) {
      [self pruneContent];
    } else {
      if (_currentPruningList.count) {
        [self addToDelayedPruneList:_currentPruningList];
        [[PALogger sharedLoggerForClient:PALogClientSyncManager] debug:@"PA app is open. Cannot prune now"];
        [self updateSyncRegistryAtPath:kPASyncRegistryKey key:kPACurrentPruneStatus value:@(0)];
      } else {
        [[PALogger sharedLoggerForClient:PALogClientSyncManager] debug:@"Nothing to prune"];
      }
      _pruneStatus = PAPruneStatusNone;
      
      [self copyLatestManifestToDataFolder];
      [self uploadData];
      [self updateProfile];
      [self generateZCat];
      [self cleanupOnSync:true];
      [self startSelfHeal];
    }
  }];
}

- (void)pruneContent {
  // content pruning is in progress
  if (_pruneStatus == PAPruneStatusInProgress) {
    return;
  }
  _pruneStatus = PAPruneStatusInProgress;
  
  // handle data pruning
  if (_currentPruningList.count) {
    //[[PALogger sharedLoggerForClient:PALogClientSyncManager] debug:@"Total items to prune [%ld]",_currentPruningList.count];
    [self pruneFileEntries:_currentPruningList];
  } else {
    [[PALogger sharedLoggerForClient:PALogClientSyncManager] debug:@"Nothing to prune"];
  }
  _pruneStatus = PAPruneStatusNone;
  
  [self copyLatestManifestToDataFolder];
  [self uploadData];
  [self updateProfile];
  [self generateZCat];
  [self cleanupOnSync:true];
  [self startSelfHeal];
}

- (void)generateZCat {
  
  NSString *statePath = [self.fileSystemManager stateFolderPath];
  NSString *dataPath = [self.fileSystemManager appDataFolderPath];
  NSString *zcatFolderPath = [statePath stringByAppendingPathComponent:kPAzCatFolder];
  if(![self.fileSystemManager doesDirectoryExist:zcatFolderPath]) {
    [self.fileSystemManager createDirectoryAtPath:zcatFolderPath withIntermediateDirectories:true attributes:nil error:nil];
  }
  NSString *zcatFilePath = [zcatFolderPath stringByAppendingPathComponent:kPAzCatFileName];
  if([self.fileSystemManager doesFileExist:zcatFilePath]) {
    [self.fileSystemManager deleteFile:zcatFilePath];
  }
  
  NSString *assistAgentPath = _appconfigurations.assistAgentFolderPath;
  assistAgentPath = [self.fileSystemManager stringByExpandingPath:assistAgentPath];
  NSBundle* bundle = [NSBundle bundleWithPath:assistAgentPath];
  NSString* scriptPath = [bundle pathForResource:@"genmd5" ofType:@"sh"];
  
  NSTask *task = [[NSTask alloc] init];
  task.launchPath = scriptPath;
  task.arguments = [NSArray arrayWithObjects:dataPath, zcatFolderPath, nil];
  [task launch];
  [task waitUntilExit];
}

- (void)updateProfile {
  NSString *filters = _targetCollectionManager.filters;
  [[PALogger sharedLoggerForClient:PALogClientSyncManager] debug:@"Newly Applied filters are [%@]",filters];
  NSString *lastAppliedFilters = [self getSyncRegistryValueAtPath:kPASyncRegistryKey key:kPAProfileKey];
  [[PALogger sharedLoggerForClient:PALogClientSyncManager] debug:@"Last applied filters are [%@]",lastAppliedFilters];
  
  // check if filter list is modified?
  NSArray *lastFilterList = [lastAppliedFilters componentsSeparatedByString:@","];
  NSArray *latestFilterList = [filters componentsSeparatedByString:@","];
  NSArray *newFilters = [NSArray arrayByRemovingCommonObjectsIn:lastFilterList from:latestFilterList];
  BOOL hasChange = ((lastFilterList.count != latestFilterList.count) || newFilters.count > 0);
  [[PALogger sharedLoggerForClient:PALogClientSyncManager] debug:@"Has change in filter [%d]",hasChange];
  if (hasChange) {
    [[PALogger sharedLoggerForClient:PALogClientSyncManager] debug:@"Has change in filter [%d]",hasChange];
    
    [self uploadProfile:filters];
    [self updateSyncRegistryAtPath:kPASyncRegistryKey key:kPAProfileKey value:filters];
  }
}

- (void)uploadData {
  [[PALogger sharedLoggerForClient:PALogClientSyncManager] debug:@"Upload logs"];
  [self updateSyncRegistryAtPath:kPASyncRegistryKey key:kPALastAttemptUploadTime value:[[NSDate systemDate] dateStringWithFormat:Format9]];
  [_logUploader uploadLogsWithCallback:^(BOOL success, NSError *error) {
    if (success) {
      [self updateSyncRegistryAtPath:kPASyncRegistryKey key:kPALastSuccessUploadTime value:[[NSDate systemDate] dateStringWithFormat:Format9]];
      [self updateSyncRegistryAtPath:kPASyncRegistryKey key:kPALastUploadErrorCode value:@(0)];
    }else if(error) {
      [self updateSyncRegistryAtPath:kPASyncRegistryKey key:kPALastUploadErrorCode value:@(error.code)];
    }
  }];
}
- (void) downloadManifestDataSets:(NSArray *)dataSets {
  _downloadManager.uiDelegate = self;
  for (PADataSetInfo *dataSetFileInfo in dataSets) {
    PADownloadableFileInfo *info = [self downloadableFileInfoFrom:dataSetFileInfo];
    if (info) {
      switch (_fileType) {
        case PAFileTypeDataSet:
          [_currentDownloadList addObject:info];
          break;
        case PAFileTypeProfileSet:
          [_currentProfileSetDownloadList addObject:info];
          break;
        default:
          break;
      }
    }
  }
}

-(PADataSetInfo *)dataSetMatchingFileSystemId:(NSString *)fileSystemId inList:(NSArray *)dataSets {
  NSArray *filtered = [dataSets filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(fileSystemId == %@)", fileSystemId]];
  if (filtered.count) {
    return filtered[0];
  }
  return nil;
}

-(void) unarchiveManifest:(PADownloadTask *)task  {
  /// Need to unarchive manifest into temp folder, then move inside data folder if manifest passes certificate verification
  NSString *tempDownloadsFolderPath = [self.fileSystemManager temporaryDownloadsFolderPath];
  
  [self unarchiveFile:task.downloadedFilePath toLocation:tempDownloadsFolderPath completion:^(BOOL success) {
    if (success) {
      [self.fileSystemManager deleteFile:task.downloadedFilePath];
      [[PALogger sharedLoggerForClient:PALogClientSyncManager] debug:@"File [%@] unarchive status [%d]",task.downloadedFilePath, success];
      [self parseManifest];
    } else {
      [[PALogger sharedLoggerForClient:PALogClientSyncManager] debug:@"Failed to unarchive file [%@]",task.downloadedFilePath];
      [self updateSyncRegistryAtPath:kPASyncRegistryKey key:kPALastUpdateErrorCode value:@(1)];
    }
  }];
}

-(void) unarchiveManifestFileEntry:(PADownloadTask *)task  {
  PADataSetInfo *dataSet = [self dataSetMatchingFileSystemId:task.taskId inList:[self allFileEntryList]];
  NSString* destination = [self.fileSystemManager appDataFolderPath];
  if (dataSet) {
    destination = dataSet.destinationBasePath;
  }
  [[PALogger sharedLoggerForClient:PALogClientSyncManager] debug:@"UnArchive file [%@]",task.downloadedFilePath];
  
  [self unarchiveFile:task.downloadedFilePath toLocation:destination completion:^(BOOL success) {
    //        if (success) {
    // delete the zip file
    [self.fileSystemManager deleteFile:task.downloadedFilePath];
    // update sync status
    [self didUpdateDataWithId:task.taskId];
    //        }
  }];
}

-(void)unarchiveFile:(NSString*)sourcePath toLocation:(NSString*)destinationPath completion:(void (^)(BOOL success))completionBlock {
  [_resourceExtractor extractFile:sourcePath toLocation:destinationPath password:nil completion:^(BOOL success) {
    completionBlock(success);
  }];
}

- (void)didUpdateDataWithId:(NSString*)fileId {
  NSMutableArray *currentDnldList = nil;
  switch (_fileType) {
    case PAFileTypeDataSet:
      currentDnldList = _currentDownloadList;
      break;
    case PAFileTypeProfileSet:
      currentDnldList = _currentProfileSetDownloadList;
      break;
    default:
      break;
  }
  PADownloadableFileInfo *fileInfo = [self downloadInfoMatchingFileSystemId:fileId inList:[currentDnldList copy]];
  if (fileInfo) {
    [currentDnldList removeObject:fileInfo];
    [[PALogger sharedLoggerForClient:PALogClientSyncManager] debug:@"Remaining download count [%lu]",(unsigned long)currentDnldList.count];
    
    switch (_fileType) {
      case PAFileTypeDataSet:
      {
        if (currentDnldList.count == 0 &&
            _syncStatus != PASyncStatusNone) {
          [self setDataSyncStatus:PASyncStatusComplete];
        }
      }
        break;
      case PAFileTypeProfileSet:
        if (currentDnldList.count == 0) {
          [[PALogger sharedLoggerForClient:PALogClientSyncManager] debug:@"Profile set sync done. Now check for data sets"];
          [self parseManifestDatasets];
        }
        break;
      default:
        break;
    }
  }
}

-(PADownloadableFileInfo *)downloadInfoMatchingFileSystemId:(NSString *)fileSystemId inList:(NSArray *)downloadInfoList {
  if (!fileSystemId || !downloadInfoList.count) {
    return nil;
  }
  NSArray *filtered = [downloadInfoList filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(identifier == %@)", fileSystemId]];
  if (filtered.count) {
    return filtered[0];
  }
  return nil;
}
-(PADownloadableFileInfo *)downloadableFileInfoFrom:(PADataSetInfo *)dataSetFileInfo {
  NSString *destinationDownloadPath = dataSetFileInfo.destinationBasePath;
  if (![self.fileSystemManager doesDirectoryExist:destinationDownloadPath]) {
    [self.fileSystemManager createDirectoryAtPath:destinationDownloadPath withIntermediateDirectories:YES attributes:nil error:nil];
  }
  destinationDownloadPath = [destinationDownloadPath stringByAppendingPathComponent:dataSetFileInfo.fileName];
  
  PADownloadableFileInfo* info = [[PADownloadableFileInfo alloc] initWithUrl:dataSetFileInfo.sourceRootPath
                                                                 destination:destinationDownloadPath
                                                                    fileSize:0
                                                                    checksum:dataSetFileInfo.checksum
                                                                  identifier:dataSetFileInfo.fileSystemId];
  return info;
}
- (NSString *)temporaryManifestFilePath {
  [[PALogger sharedLoggerForClient:PALogClientSyncManager] debug:@"Temporary manifest downdloaded temp path %", [self.fileSystemManager temporaryManifestFilePath]];
  return [self.fileSystemManager temporaryManifestFilePath];
}
- (NSString *)localManifestFilePath {
  [[PALogger sharedLoggerForClient:PALogClientSyncManager] debug:@"Local manifest path %", [self.fileSystemManager localManifestFilePath]];
  return [self.fileSystemManager localManifestFilePath];
}
- (NSString *)remoteManifestFilePath {
  return [self.fileSystemManager remoteManifestFilePath];
}

- (NSArray *)allFileEntryList {
  NSMutableArray *finalList = [NSMutableArray arrayWithArray:_dataSetEntries];
  if (_profileSetEntries.count) {
    [finalList addObjectsFromArray:_profileSetEntries];
  }
  return [NSArray arrayWithArray:finalList];
}

- (void)pruneFileEntries:(NSArray *)fileEntries {
  _pruneStatus = PAPruneStatusInProgress;
  [self updateSyncRegistryAtPath:kPASyncRegistryKey key:kPALastPrunedTime value:[[NSDate systemDate] dateStringWithFormat:Format9]];
  
  for (PADataSetInfo *dataSetInfo in fileEntries) {
    if (dataSetInfo.isFile) {
      NSString *itemToDelete = dataSetInfo.destinationBasePath;
      // get the name of the file extracted from zip
      // In case of individual file, there is no direct way to get the name of the file to delete, the file's root directory content needs to be traversed to get the matching file name
      NSString *itemName = [_fileSystemManager getMatchingFileName:dataSetInfo.fileSystemId inBaseDirectory:dataSetInfo.destinationBasePath];
      itemToDelete = [itemToDelete stringByAppendingPathComponent:itemName];
      if ([_fileSystemManager doesFileExist:itemToDelete]) {
        [[PALogger sharedLoggerForClient:PALogClientSyncManager] debug:@"Request to Prune [%@]",dataSetInfo.fileSystemId];
        BOOL success = [_fileSystemManager deleteFile:itemToDelete];
        [[PALogger sharedLoggerForClient:PALogClientSyncManager] debug:@"Status of pruning [%@] at path [%@] is [%d]",dataSetInfo.fileSystemId, itemToDelete, success];
      } /*else {
        [[PALogger sharedLoggerForClient:PALogClientSyncManager] debug:@"The [%@] not found on local machine",dataSetInfo.fileSystemId];
      }*/
    } else {
      NSString *itemToDelete = dataSetInfo.destinationRootPath;
      if ([_fileSystemManager doesDirectoryExist:itemToDelete]) {
        [[PALogger sharedLoggerForClient:PALogClientSyncManager] debug:@"Request to Prune [%@]",dataSetInfo.fileSystemId];
        BOOL success = [_fileSystemManager deleteDirectory:itemToDelete];
        [[PALogger sharedLoggerForClient:PALogClientSyncManager] debug:@"Status of pruning [%@] at path [%@] is [%d]",dataSetInfo.fileSystemId, itemToDelete, success];
      } /*else {
        [[PALogger sharedLoggerForClient:PALogClientSyncManager] debug:@"The [%@] not found on local machine",dataSetInfo.fileSystemId];
      }*/
    }
  }
  _pruneStatus = PAPruneStatusComplete;
  [self updateSyncRegistryAtPath:kPASyncRegistryKey key:kPACurrentPruneStatus value:@(1)];
}
- (BOOL)updateSyncRegistryAtPath:(NSString *)path
                             key:(NSString *)key
                           value:(id)value {
  NSString* plistFilePath = [self.fileSystemManager hkcuRegistryPlistPath];
  __block BOOL ret = NO;
 // PAPlistLockManager *lckMgr = [PAPlistLockManager sharedInstance];
 // [lckMgr runWithWriteLock:plistFilePath codeBlock:^{
    NSString *keyPath = [self.fileSystemManager stringByExpandingPath:path];
    PARegistryWriter* writer = [[PARegistryWriter alloc] initWithPlistAtPath:plistFilePath];
    ret = [writer setRegValueForPath:keyPath withKey:key andData:value];
 // }];
  return ret;
}
- (NSString *)getSyncRegistryValueAtPath:(NSString *)path
                                     key:(NSString *)key {
  NSString* plistFilePath = [self.fileSystemManager hkcuRegistryPlistPath];
  NSString *keyPath = [self.fileSystemManager stringByExpandingPath:path];
  __block NSString* registryValueAtPath = nil;
  //PAPlistLockManager *lckMgr = [PAPlistLockManager sharedInstance];
  //[lckMgr runWithReadLock:plistFilePath codeBlock:^{
    PARegistryReader* reader = [[PARegistryReader alloc] initWithPlistAtPath:plistFilePath];
    registryValueAtPath = [reader regValueForPath:keyPath withKey:key];
  //}];
  return registryValueAtPath;
}

- (void)addToDelayedPruneList:(NSArray *)fileEntries {
  NSMutableArray *pruneLaterList = [[NSMutableArray alloc] init];
  for (PADataSetInfo *dataSetInfo in fileEntries) {
    if (dataSetInfo.isFile) {
      NSString *itemToDelete = dataSetInfo.destinationBasePath;
      // get the name of the file extracted from zip
      // In case of individual file, there is no direct way to get the name of the file to delete, the file's root directory content needs to be traversed to get the matching file name
      NSString *itemName = [_fileSystemManager getMatchingFileName:dataSetInfo.fileSystemId inBaseDirectory:dataSetInfo.destinationBasePath];
      itemToDelete = [itemToDelete stringByAppendingPathComponent:itemName];
      if ([_fileSystemManager doesFileExist:itemToDelete]) {
        [[PALogger sharedLoggerForClient:PALogClientSyncManager] debug:@"Prune [%@] later as PA app is open now",dataSetInfo.fileSystemId];
        [pruneLaterList addObject:dataSetInfo.fileSystemId];
      } else {
        [[PALogger sharedLoggerForClient:PALogClientSyncManager] debug:@"The [%@] not found on local machine",dataSetInfo.fileSystemId];
      }
    } else {
      NSString *itemToDelete = dataSetInfo.destinationRootPath;
      if ([_fileSystemManager doesDirectoryExist:itemToDelete]) {
        [[PALogger sharedLoggerForClient:PALogClientSyncManager] debug:@"Prune [%@] later as PA app is open now",dataSetInfo.fileSystemId];
        [pruneLaterList addObject:dataSetInfo.fileSystemId];
      }  else {
        [[PALogger sharedLoggerForClient:PALogClientSyncManager] debug:@"The [%@] not found on local machine",dataSetInfo.fileSystemId];
      }
    }
  }
  // merge last prune list with current one
  NSArray *lastPruneList = [self pruneDelayedContentList];
  if (lastPruneList.count) {
    [pruneLaterList addObjectsFromArray:lastPruneList];
  }
  if (pruneLaterList.count) {
    [self updateSyncRegistryAtPath:kPASyncRegistryKey key:kPAPruneDelayedContentList value:[pruneLaterList componentsJoinedByString:@","]];
  }
}

- (NSArray *)pruneDelayedContentList {
  NSString *delayedPruneListStr = [self getSyncRegistryValueAtPath:kPASyncRegistryKey key:kPAPruneDelayedContentList];
  if ([delayedPruneListStr isEmptyOrHasOnlyWhiteSpaces]) {
    return nil;
  }
  return [delayedPruneListStr componentsSeparatedByString:@","];
}
- (void)pruneDelayedContent {
  [[PALogger sharedLoggerForClient:PALogClientSyncManager] debug:@"Check for delayed prune list"];
  NSArray *delayedPruneList = [self pruneDelayedContentList];
  if (!delayedPruneList.count) {
    [[PALogger sharedLoggerForClient:PALogClientSyncManager] debug:@"Nothing to prune"];
    return;
  }
  [self updateSyncRegistryAtPath:kPASyncRegistryKey key:kPALastPrunedTime value:[[NSDate systemDate] dateStringWithFormat:Format9]];
  
  NSString *dataFolderPath = [_fileSystemManager appDataFolderPath];
  for (NSString *itemName in delayedPruneList) {
    [[PALogger sharedLoggerForClient:PALogClientSyncManager] debug:@"Request to Prune [%@]",itemName];
    BOOL success = [self pruneItem:itemName ifFoundIn:dataFolderPath];
    if (!success) {
      NSArray *dataContents = [_fileSystemManager contentsOfDirectory:dataFolderPath];
      for (NSString *dataContentName in dataContents) {
        NSString *dataContentItemPath = [dataFolderPath stringByAppendingPathComponent:dataContentName];
        success = [self pruneItem:itemName ifFoundIn:dataContentItemPath];
        if (success) {
          break;
        }
      }
    }
  }
  [self updateSyncRegistryAtPath:kPASyncRegistryKey key:kPAPruneDelayedContentList value:@""];
  _pruneStatus = PAPruneStatusComplete;
  [self updateSyncRegistryAtPath:kPASyncRegistryKey key:kPACurrentPruneStatus value:@(1)];
}

- (BOOL)pruneItem:(NSString *)itemName ifFoundIn:(NSString *)targetPath {
  BOOL success = false;
  NSArray *dataContents = [_fileSystemManager contentsOfDirectory:targetPath];
  
  if ([dataContents containsObject:itemName]) {
    NSString *itemPath = [targetPath stringByAppendingPathComponent:itemName];
    success = [_fileSystemManager deleteDirectory:itemPath];
    [[PALogger sharedLoggerForClient:PALogClientSyncManager] debug:@"Status of pruning [%@] at path [%@] is [%d]",itemName, itemPath, success];
  }
  return success;
}

- (void)uploadProfile:(NSString *)profile {
  /// upload non-empty filter
  if ([profile isEmptyOrHasOnlyWhiteSpaces]) {
    return;
  }
  __block PANetworkServiceManager *serviceManager = [[PANetworkServiceManager alloc] init];
  PASyncRequest *request = [[PASyncRequest alloc] initWithRequestType:PASyncRequestTypeSetProfile profileKey:profile macId:[NSHost macId] ipAddress:[NSHost ipAddress] user:[NSHost userName] hostName:[NSHost computerName] version:@"1.0" ostype:@"macOS" osname:[NSProcessInfo osCodeName] osver:[NSProcessInfo osVersion]];
  [serviceManager setProfile:request callback:^(NSError *error, PABaseResponse *response) {
    if (response.statusCode == kPAResponseSuccess) {
      [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"Upload profile request success"];
    } else {
      [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"Upload profile request failed; Error: %@",error];
    }
  }];
}

#pragma mark -Notification related methods

- (void)addNotificationObservers {
  [[NSDistributedNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveNotification:) name:kPANotificationNameProactiveAssistTerminated object:kPAResidentAgentObjectName];
}
- (void)removeNotificationObservers {
  [[NSDistributedNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveNotification:(NSNotification *)notification {
  if ([notification.name isEqualToString:kPANotificationNameProactiveAssistTerminated]) {
    [self pruneDelayedContent];
  }
}

#pragma mark - Self Heal methods
-(void) startSelfHeal {
    PASelfHealTrigger *selfHealTrigger = [[PASelfHealTrigger alloc] init];
    [selfHealTrigger initiateSelfHeal];
}

#pragma mark - Test methods; need to be deleted
- (void)addToCache {
  NSString *dbPath = @"~/Desktop/PATest/Optimization/Optimization.realm";
  dbPath = [dbPath stringByExpandingTildeInPath];
  
  PAOptimizationInfo *optimInfo = [[PAOptimizationInfo alloc]init];
  optimInfo.frequency = PAOptimizationFrequencyDaily;
  optimInfo.hours = 4;
  optimInfo.identifier = [[NSUUID UUID] UUIDString];;
  PALocalCacheManager *cacheManager =  [[PALocalCacheManager alloc] initWithDatabase:dbPath];
  [cacheManager add:optimInfo];
  
  PAProtectedItem *protectedItemDetail = [[PAProtectedItem alloc]initWithGuid:@"test guid" version:@"1.0" date:@"2017-10-18 14:45:15.506619+0530 "];
  [cacheManager add:protectedItemDetail];
}
- (void)checkSprocketSupportAction {
  PAServiceInfo *info = [[PAServiceInfo alloc]init];
  info.guid = @"bfbc414c-e924-497e-ad2a-7abf1c200216.3";
  info.serviceCategory = PASupportServiceCategorySupportAction;
  info.service = PASupportServiceSupportActionFix;
  info.inputParameters = @{@"FixKeyBlank": @"fix"};
  [[PALoginAgent sharedLoginAgent] executeService:info withReply:^(PAServiceResponse *response) {
    if (response.success) {
      
    }
  }];
  
}
- (void)checkSprocketService {
  PAServiceInfo *info = [[PAServiceInfo alloc]init];
  info.guid = @"6ee471e2-4783-4e4b-b81d-861322d3a699";
  info.version = @"5";
  //    info.application = PASupportServiceAppSafari;
  //    info.filesToProtect = @[@"~/Library/Preferences/com.apple.Safari.plist",@"~/Library/Safari"];
  info.protectedItemName = @"05-Dec-2017_21_02_27";
  info.serviceCategory = PASupportServiceCategoryRepair;
  
  NSString *plistPath = [self testFolderPath];
  plistPath = [plistPath stringByAppendingPathComponent:@"TestAutomation.plist"];
  __block NSString *repairAction = nil;
  
  //PAPlistLockManager *lckMgr = [PAPlistLockManager sharedInstance];
  //[lckMgr runWithReadLock:plistPath codeBlock:^{
    PAPlistReader *reader = [[PAPlistReader alloc]initWithPlistAtPath:plistPath];
    repairAction = [reader valueForKey:@"repairAction" inSection:nil];
    
  //}];
  if ([repairAction isEqualToString:@"restore"]) {
    info.service = PASupportServiceRestore;
    [[PALoginAgent sharedLoginAgent] executeService:info withReply:^(PAServiceResponse *response) {
      if (response.success) {
        
      }
    }];
    
  } else if ([repairAction isEqualToString:@"undo"]) {
    info.service = PASupportServiceUndoRestore;
    [[PALoginAgent sharedLoginAgent] executeService:info withReply:^(PAServiceResponse *response) {
      if (response.success) {
        
      }
    }];
  } else if ([repairAction isEqualToString:@"getProtectHistory"]) {
    info.service = PASupportServiceGetProtectedHistory;
    [[PALoginAgent sharedLoginAgent] executeService:info withReply:^(PAServiceResponse *response) {
      if (response.success) {
        
      }
    }];
  } else if ([repairAction isEqualToString:@"getRestoreHistory"]) {
    info.service = PASupportServiceGetRestoredHistory;
    [[PALoginAgent sharedLoginAgent] executeService:info withReply:^(PAServiceResponse *response) {
      if (response.success) {
        
      }
    }];
  } else {
    info.service = PASupportServiceProtect;
    [[PALoginAgent sharedLoginAgent] executeService:info withReply:^(PAServiceResponse *response) {
      if (response.success) {
        
      }
    }];
  }
}

- (void)testDownloadOfferDmg {
  PAServiceInfo *info = [[PAServiceInfo alloc]init];
  info.guid = @"973283e0-37dd-4ab7-be76-7f6c9d2471f9";
  info.version = @"5";
  info.service = PASupportServiceDownload;
  info.serviceCategory = PASupportServiceCategoryDownloadOffers;
  info.serviceMode = PAServiceModeNormal;
  
  // Add download progress notification
  [[NSDistributedNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveNotification:) name:kPASupportServiceDownloadStatusNotificationName object:kPASupportServiceDownloadOfferNotificationObjectName];
  
  [[PAInterProcessManager sharedManager] executeService:info withReply:^(BOOL success, NSError *error, id result) {
    if (success && result && [result isKindOfClass:[NSDictionary class]]) {
      // remove notification
      [[NSDistributedNotificationCenter defaultCenter] removeObserver:self name:kPASupportServiceDownloadStatusNotificationName object:kPASupportServiceDownloadOfferNotificationObjectName];
      
      NSDictionary *responseDict = (NSDictionary *)result;
      PADownloadStatus downloadStatus = (PADownloadStatus)[responseDict[kPASupportServiceResponseKeyStatus] integerValue];
      if (downloadStatus == PADownloadStatusComplete) {
        info.service = PASupportServiceInstall;
        info.downloadedItemPath = responseDict[kPASupportServiceResponseKeyDownloadPath];
        
        [[PAInterProcessManager sharedManager] executeService:info withReply:^(BOOL success, NSError *error, id result) {
          if (success) {
            
          }
        }];
      }
    }
  }];
  
}

- (void)cancelDownload {
  NSDictionary *responseDict =@{kPASupportServiceResponseKeyGuid:@"973283e0-37dd-4ab7-be76-7f6c9d2471f9", kPASupportServiceResponseKeyVersion:@"5"};
  [[NSDistributedNotificationCenter defaultCenter] postNotificationName:kPASupportServiceDownloadCancelNotificationName object:kPASupportServiceDownloadOfferNotificationObjectName userInfo:responseDict deliverImmediately:true];
  
}

- (void)testCabExtractor {
  NSString *cabPath = [self testFolderPath];
  cabPath = [cabPath stringByAppendingPathComponent:@"composite.cab"];
  
  NSString *dstPath = [cabPath stringByDeletingPathExtension];
  PACabServiceManager *cabManager = [[PACabServiceManager alloc]init];
  BOOL returnValue = [cabManager extractCabAtPath:cabPath toDestinationFolder:dstPath error:nil];
  NSLog(@"Is cab extraction successful? = %d",returnValue);
}

- (void)testDownloadOfferPkg {
  PAServiceInfo *info = [[PAServiceInfo alloc]init];
  info.guid = @"973283e0-37dd-4ab7-be76-7f6c9d2471f8";
  info.version = @"5";
  info.service = PASupportServiceDownload;
  info.serviceCategory = PASupportServiceCategoryDownloadOffers;
  info.serviceMode = PAServiceModeNormal;
  
  // Add download progress notification
  [[NSDistributedNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveNotification:) name:kPASupportServiceDownloadStatusNotificationName object:kPASupportServiceDownloadOfferNotificationObjectName];
  
  [[PAInterProcessManager sharedManager] executeService:info withReply:^(BOOL success, NSError *error, id result) {
    if (success && result && [result isKindOfClass:[NSDictionary class]]) {
      // remove notification
      [[NSDistributedNotificationCenter defaultCenter] removeObserver:self name:kPASupportServiceDownloadStatusNotificationName object:kPASupportServiceDownloadOfferNotificationObjectName];
      
      NSDictionary *responseDict = (NSDictionary *)result;
      PADownloadStatus downloadStatus = (PADownloadStatus)[responseDict[kPASupportServiceResponseKeyStatus] integerValue];
      if (downloadStatus == PADownloadStatusComplete) {
        info.service = PASupportServiceInstall;
        info.downloadedItemPath = responseDict[kPASupportServiceResponseKeyDownloadPath];
        
        [[PAInterProcessManager sharedManager] executeService:info withReply:^(BOOL success, NSError *error, id result) {
          if (success) {
            
          }
        }];
      }
    }
  }];
  
}
- (void)testScriptRunner {
  NSString *scriptPath = [self testFolderPath];
  scriptPath = [scriptPath stringByAppendingPathComponent:@"EmptyTrash.scpt"];
  
  PAScriptInfo *scriptInfo = [[PAScriptInfo alloc]initWithPath:scriptPath type:PAScriptTypeApple mode:PAScriptModeElevated];
  //    PAScriptInfo *scriptInfo2 = [[PAScriptInfo alloc]initWithScript:@"set volume output volume 50" type:PAScriptTypeApple mode:PAScriptModeNormal];
  
  //    [PAScriptRunner executeScriptWithPath:scriptPath callBack:^(BOOL success, NSError *error, id result) {
  //        if (success) {
  //
  //        }
  //    }];
  //    [[PALoginAgent sharedLoginAgent] executeScript:scriptInfo2 withReply:^(PAServiceResponse *response) {
  //        if (response.success) {
  //
  //        }
  //    }];
  //    [[PALoginAgent sharedLoginAgent] executeScript:scriptInfo withReply:^(PAServiceResponse *response) {
  //        if (response.success) {
  //
  //        }
  //    }];
  [[PAInterProcessManager sharedManager] executeScript:scriptInfo withReply:^(BOOL success, NSError *error, id result) {
    if (success) {
      
    }
  }];
}

- (void)testNotificationTrayService {
  PANotificationInfo *notifInfo = [[PANotificationInfo alloc] initWithTitle:@"Test Title" messsage:@"Test Message" detailedMesssage:nil];
  [PANotificationTrayManager notifyTrayAppWithInfo:notifInfo];
}
- (NSString *)testFolderPath {
  NSString *folderPath = @"~/Desktop/PATest";
  folderPath = [folderPath stringByExpandingTildeInPath];
  if (![_fileSystemManager doesDirectoryExist:folderPath]) {
    if ([_fileSystemManager createDirectoryAtPath:folderPath withIntermediateDirectories:NO attributes:nil error:nil]) {
      return folderPath;
    }
  } else {
    return folderPath;
  }
  
  return nil;
}
- (void)testEndpointInfo {
  NSString *xmlPath = @"~/Desktop/PATest/OSSummary.xml";
  NSString *scriptXmlPath = @"~/Desktop/PATest/default_mac.xml";
  xmlPath = [xmlPath stringByExpandingTildeInPath];
  scriptXmlPath = [scriptXmlPath stringByExpandingTildeInPath];
  
  PAEnPointInfoManager *manager = [[PAEnPointInfoManager alloc]initWithInfoClass:@"OSSummary" inputXml:scriptXmlPath outputXml:xmlPath source:PATargetScriptSourceTypeNone];
  [manager createEndpointInfoWithCompletion:^(BOOL success) {
    //[manager deleteEndpointInfo];
  }];
}
- (void)testAllEndpointInfo {
  NSString *outputXmlPath = @"~/Desktop/PATest/userinfo_default_xml_output.xml";
  NSString *scriptXmlPath = @"~/Desktop/PATest/default_mac.xml";
  outputXmlPath = [outputXmlPath stringByExpandingTildeInPath];
  scriptXmlPath = [scriptXmlPath stringByExpandingTildeInPath];
  
  //    PAEnPointInfoManager *manager = [[PAEnPointInfoManager alloc]initWithInputXml:nil outputXml:outputXmlPath source:PATargetScriptSourceTypeDefaultXml];
  PAEnPointInfoManager *manager = [[PAEnPointInfoManager alloc]initWithInputXml:scriptXmlPath outputXml:outputXmlPath source:PATargetScriptSourceTypeOthers];
  [manager createAllEndpointInfoWithCompletion:^(BOOL success) {
    //[manager deleteEndpointInfo];
    
    PAEnPointInfoManager *manager1 = [[PAEnPointInfoManager alloc]initWithOutputXml:outputXmlPath];
    
    NSString *className = @"NetworkInfo";
    NSString *property = @"ip_address";
    NSString *propertyValue = [manager1 valueOfProperty:property inClass:className];
    NSLog(@"property val = %@",propertyValue);
    
    NSString *className1 = @"Printers";
    NSString *property1 = @"_name";
    NSArray *propertyValues = [manager1 valuesOfProperty:property1 inClass:className1];
    NSLog(@"property values = %@",propertyValues);
  }];
}

- (void)testPlistServiceViaPrivilegeTool {
  PAPlistServiceInfo *serviceInfo = [[PAPlistServiceInfo alloc] init];
  serviceInfo.sectionPath = @"Software\\SupportSoft\\ProviderList\\Experience92\\DynamicAgent\\ss_config\\global-test";
  serviceInfo.regKey = @"TestKey";
  serviceInfo.serviceMode = PAPlistServiceModeElevated;
  serviceInfo.service = PAPlistServiceSetRegValue;
  serviceInfo.plistFilePath = @"/Library/Application Support/ProactiveAssist/HKLM_Registry.plist";
  //    serviceInfo.plistFilePath = [@"~/Library/Application Support/ProactiveAssist/Experience92/agent/bin/HKCU_Registry.plist" stringByExpandingTildeInPath];
  [[PAInterProcessManager sharedManager] executePlistService:serviceInfo withReply:^(BOOL finish) {
    if (finish) {
      
    }
  }];
}

- (void)testPlist {
 // __block NSString *plistPath = [self testFolderPath];
 // PAPlistLockManager *lckMgr = [PAPlistLockManager sharedInstance];
 // [lckMgr runWithWriteLock:plistPath codeBlock:^{
    NSString *plistPath = [self testFolderPath];
    plistPath = [plistPath stringByAppendingPathComponent:@"bcont_nm.plist"];
    
    PAPlistServiceManager *manager = [[PAPlistServiceManager alloc]init];
    if ([manager createPlistAtPath:plistPath]) {
      PAPlistWriter *writer = [[PAPlistWriter alloc]initWithPlistAtPath:plistPath];
      [writer setValue:@(1000) forKey:@"spaceneeded" inSection:nil];
      [writer setValue:@"TestValueToRemove" forKey:@"test" inSection:nil];
      [writer setValue:@"Experience92" forKey:@"provider" inSection:@"PROVIDERINFO"];
      [writer setValue:@"TestValueToRemove" forKey:@"test" inSection:@"PROVIDERINFO"];
      PAPlistReader *reader = [[PAPlistReader alloc]initWithPlistAtPath:plistPath];
      id spaceneeded = [reader valueForKey:@"spaceneeded" inSection:nil];
      id provider = [reader valueForKey:@"provider" inSection:@"PROVIDERINFO"];
      [writer removeKey:@"test" inSection:@"ProviderInfo"];
      [writer removeKey:@"test" inSection:nil];
      [writer synchronize];
    }
  //}];
}

- (void)testXml {
  PATestClass *testObj = [[PATestClass alloc]init];
  
  NSString *xmlFilePathToCreate = [self testFolderPath];
  xmlFilePathToCreate = [xmlFilePathToCreate stringByAppendingPathComponent:@"test1.xml"];
  
  NSString *xmlFilePath = [self testFolderPath];
  xmlFilePath = [xmlFilePath stringByAppendingPathComponent:@"test.xml"];
  
  
  PAXmlFileProperties *properties = [[PAXmlFileProperties alloc]initWithVersion:kPAXmlVersionValue encoding:kPAXmlUtf8Encoding standalone:@(false) root:nil];
  
  PAXmlServiceManager *manager = [[PAXmlServiceManager alloc]init];
  [manager createXmlAtPath:xmlFilePathToCreate withProperties:properties];
  
  PAXmlWriter *xmlWriter = [[PAXmlWriter alloc]initWithXmlAtPath:xmlFilePath];
  [xmlWriter addChildXmlString:testObj.xmlString parentXPath:@"DataCollection/Snapshot/CIM/DECLARATION/DECLGROUP.WITHPATH"];
  PAXmlReader *reader = [[PAXmlReader alloc]initWithXmlAtPath:xmlFilePath];
  NSString *xmlStr = [reader xmlStringAtXPath:@"DataCollection/Snapshot/CIM/DECLARATION/DECLGROUP.WITHPATH" matchingAttributes:nil];
  [xmlWriter removeValueAtXPath:@"DataCollection/Snapshot/CIM/DECLARATION/DECLGROUP.WITHPATH/VALUE.OBJECTWITHPATH" withAttributeName:@"CLASSNAME" andValue:@"Test"];
}

- (void)testJs {
  NSString *filePath = [[self testFolderPath] stringByAppendingPathComponent:@"content.js"];
  
  NSString *fileContent = @"{\"cid\":\"26789f30-dd91-4675-ac75-5bd325b10121\",\"version\":\"1\",\"title\":\"test alert\",\"description\":\"test alert\",\"ctype\":\"sprt_realtimealert\",\"fid\":\"10cc6b67-e560-454a-9cc6-107204664a0b\",\"language\":\"en\",\"category\":\"\"}";
  NSData *fileData = [fileContent dataUsingEncoding:NSASCIIStringEncoding];
  
  if ([_fileSystemManager createFileAtPath:filePath attributes:nil error:nil]) {
    BOOL write = [_fileSystemManager writeToFileAtPath:filePath data:fileData];
    NSLog(@"Is file write successful? %d",write);
  }
}
- (void)testHtm {
  NSString *filePath = [[self testFolderPath] stringByAppendingPathComponent:@"Alert Content.htm"];
  
  NSString *fileContent = @"<p>Test Alert7</p>";
  NSData *fileData = [fileContent dataUsingEncoding:NSASCIIStringEncoding];
  if ([_fileSystemManager createFileAtPath:filePath attributes:nil error:nil]) {
    BOOL write = [_fileSystemManager writeToFileAtPath:filePath data:fileData];
    NSLog(@"Is file write successful? %d",write);
  }
}

- (void)testCertificate {
  NSString *certPath = [[self testFolderPath] stringByAppendingPathComponent:@"Certificate/der/pki_cacert.cer"];
  PACertificate *certificate = [[PACertificate alloc]initWithPath:certPath];
  /// TODO: Incomplete certificate validation
  
  NSLog(@"certificate signature = \n%@",certificate.signature);
  NSLog(@"certificate signature = \n%@",certificate.signature);
  NSLog(@"certificate data = \n%@",certificate.certificateDataString);
}

- (void)testZipExtractor {
  [_resourceExtractor extractFile:@"/Users/vinayaka.s/Downloads/config.zip" toLocation:@"/Users/vinayaka.s/Downloads/Config" password:nil completion:^(BOOL success) {
    if (success) {
    }
  }];
}

- (void)testCfgFileParser {
  NSString *configPath = @"/Users/vinayaka.s/Library/Application Support/ProactiveAssist/Experience92/data/config.cfg";
  NSDictionary *cfgDict = [PACfgFileProcessor parseConfigFileAtPath:configPath];
  NSLog(@"cfgDict = %@\n\n",cfgDict);
}

- (void)testLaunchService {
  //NSString *command = @"finder /p test /ini test.ini /path test/path";
  NSString *command = @"/Applications/Contacts.app";
  BOOL isSuccess = [PALaunchServiceManager runCommand:command];
  BOOL isCommandRunning = [PALaunchServiceManager isCommandRunning:command];
  
  NSLog(@"isSuccess = %d\n\n",isSuccess);
}

- (void)testPasswordDecryption {
  NSString *base64str = @"NzM5QTFFOUItQkFDQS00MEJCLThGQzgtODA1REVBRTkwODk3";
  NSData *base64data = [[NSData alloc] initWithBase64EncodedString:base64str options:0];
  NSString *utf8str = [[NSString alloc] initWithData:base64data encoding:NSUTF8StringEncoding];
  NSLog(@"utf8str = %@\n\n",utf8str);
  
}

//- (void)testCodeSign {
//    NSString *cabPath = @"/Users/vinayaka.s/Library/Application Support/ProactiveAssist/Experience92/data/sprt_actionlight/0c219a28-ffa5-4256-a800-caee9604b494.1/composite.cab";
//
//    BOOL validaeCodesign = [PACodeSignValidator validateCodeSigningOfItemAtPath:cabPath];
//    NSLog(@"isSuccess = %d\n\n",validaeCodesign);
//
//}
@end
