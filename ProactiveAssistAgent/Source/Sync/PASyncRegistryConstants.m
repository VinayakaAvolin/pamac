//
//  PASyncRegistryConstants.m
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PASyncRegistryConstants.h"

NSString *const kPALastAttemptUpdateTime = @"LastAttemptUpdateTime";
NSString *const kPALastAttemptUploadTime = @"LastAttemptUploadTime";

NSString *const kPALastSuccessUpdateTime = @"LastSuccessUpdateTime";
NSString *const kPALastSuccessUploadTime = @"LastSuccessUploadTime";

NSString *const kPALastPrunedTime = @"LastPrunedTime";

NSString *const kPANextUpdateTime = @"NextUpdateTime";

NSString *const kPAProfileKey = @"ProfileKey";

NSString *const kPALastUpdateErrorCode = @"LastUpdateErrCode";
NSString *const kPALastUploadErrorCode = @"LastUploadErrCode";
NSString *const kPACurrentPruneStatus = @"CurrentPruneStatus";
NSString *const kPAPruneDelayedContentList = @"PruneDelayedContentList";

NSString *const kPASyncRegistryKey = @"software/supportsoft/providerlist/%PROVIDERPATH%/users/%USER%";

NSString *const kPAUploadAgentUILogFileName = @"agentui_upload.log";
NSString *const kPAAgentUILogFileName = @"agentui.log";
NSString *const kPAAgentUILogBackupFileName = @"agentui.log.bak";

NSString *const kPAUploadEventLogFileName = @"event_upload.log";
NSString *const kPAEventLogFileName = @"event.log";
NSString *const kPAEventLogBackupFileName = @"event.log.bak";

NSString *const kPAzCatFileName = @"zcat.dat";
NSString *const kPAzCatFolder = @"zcat";
