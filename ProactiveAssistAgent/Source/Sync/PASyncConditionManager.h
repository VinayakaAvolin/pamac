//
//  PASyncConditionManager.h
//  ProactiveAssistAgent
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PAAgentConstants.h"

typedef void(^PASyncConditionManagerCallback)(BOOL canContinue);

@interface PASyncConditionManager : NSObject

+ (void)canPruneForType:(PASyncType)type withCallback:(PASyncConditionManagerCallback)callback;

@end
