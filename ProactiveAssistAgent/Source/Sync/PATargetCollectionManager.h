//
//  PATargetCollectionManager.h
//  ProactiveAssistAgent
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <PACoreServices/PACoreServices.h>

extern NSString *const kPAFilteredataSets;
extern NSString *const kPANonFilteredataSets;

typedef void(^PATargetCollectionManagerCompletionBlock)(BOOL);

typedef enum {
    PATargetCollectionManifestTypeOld,
    PATargetCollectionManifestTypeLatest
} PATargetCollectionManifestType;

@interface PATargetCollectionManager : NSObject

@property (nonatomic, readonly) NSString *filters; /// comma seperated string of filters

- (void)constructTargetCollectionFromXml:(NSDictionary *)xmlDict
                                    type:(PATargetCollectionManifestType)manifestType
                              completion:(PATargetCollectionManagerCompletionBlock)block;

- (void)deleteTargetCollectionXml;

- (NSString *)targetCollectionXmlPath;

- (NSDictionary *)filteredDataSetsAfterApplyingTargetCollectionRuleOn:(NSArray *)dataSets
                                                            type:(PATargetCollectionManifestType)manifestType;
@end
