//
//  PANetworkServiceManager+Sync.m
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PANetworkServiceManager+Sync.h"

@implementation PANetworkServiceManager (Sync)

- (void)setProfile:(PASyncRequest *)request
          callback:(PANetworkServiceManagerCallback)callback {
  NSURLSessionDataTask *task = nil;
  task = [self executeRequest:request completion:^(NSError *error, PABaseResponse *response) {
    [self removeDataTask:task];
    callback(error, response);
  }];
  [self addDataTask:task];
}

@end
