//
//  PASyncRequest.m
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <PAConfigurationService/PAConfigurationService.h>
#import "PASyncRequest.h"

NSString *const kPARARequestProfileKey = @"pkey";
NSString *const kPARARequestMacIdKey = @"macid";
NSString *const kPARARequestIpAddressKey = @"ip";
NSString *const kPARARequestUserKey = @"u";
NSString *const kPARARequestMachineHostNameKey = @"nb";
NSString *const kPARARequestVersionKey = @"ver";
NSString *const kPARARequestOSTypeKey = @"ostype";
NSString *const kPARARequestOSNameKey = @"osname";
NSString *const kPARARequestOSVersionKey = @"osver";

@interface PASyncRequest () {
  PASyncRequestType _requestType;
  NSString *_profileKey;
  NSString *_macId;
  NSString *_ipAddress;
  NSString *_user;
  NSString *_machineHostName;
  NSString *_version;
  NSString *_ostype;
  NSString *_osname;
  NSString *_osver;
}
@end

@implementation PASyncRequest

- (instancetype)initWithRequestType:(PASyncRequestType)type
                         profileKey:(NSString *)profile
                              macId:(NSString *)macId
                          ipAddress:(NSString *)ipAddress
                               user:(NSString *)user
                           hostName:(NSString *)hostName
                            version:(NSString *)version
                             ostype:(NSString *)ostype
                             osname:(NSString *)osname
                              osver:(NSString *)osver {
  self = [super init];
  if (self) {
    if (type == PASyncRequestTypeNone) {
      self = nil;
    } else {
      _requestType = type;
      _profileKey = profile;
      _macId = macId;
      _ipAddress = ipAddress;
      _user = user;
      _machineHostName = hostName;
      _version = version;
      _ostype = ostype;
      _osname = osname;
      _osver = osver;
    }
  }
  return self;
}
- (NSString *)baseUrl {
  return [super baseUrl];
}

- (NSString *)path {
  NSString *urlPath = nil;
  switch (_requestType) {
    case PASyncRequestTypeSetProfile:
      urlPath = [self setProfileUrlPath];
      break;
    default:
      break;
  }
  return urlPath;
}

- (NSString *)encoding {
  return nil;
}

- (NSString *)method {
  return kPABRMethodNameHead;
}

- (NSDictionary *)headers {
  return nil;
}

- (NSDictionary *)parameters {
  return nil;
}

- (NSString *)setProfileUrlPath {
  NSString *params = nil;
  NSString *syncUrlPath = [self syncUrlPath];
  if (syncUrlPath && _profileKey) {
    params = [NSString stringWithFormat:@"%@?%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@",syncUrlPath, kPARARequestProfileKey,_profileKey, kPARARequestMacIdKey,_macId, kPARARequestIpAddressKey, _ipAddress, kPARARequestUserKey, _user, kPARARequestMachineHostNameKey, _machineHostName, kPARARequestVersionKey, _version, kPARARequestOSTypeKey, _ostype, kPARARequestOSNameKey, _osname, kPARARequestOSVersionKey, _osver];
  }
  return params;
}

- (NSString *)syncUrlPath {
  return [PAConfigurationManager appConfigurations].setProfileUrlPath;
}
@end
