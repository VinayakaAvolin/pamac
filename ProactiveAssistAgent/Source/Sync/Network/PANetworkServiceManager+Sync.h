//
//  PANetworkServiceManager+Sync.h
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <PANetworkService/PANetworkService.h>
#import "PASyncRequest.h"

@interface PANetworkServiceManager (Sync)

- (void)setProfile:(PASyncRequest *)request
          callback:(PANetworkServiceManagerCallback)callback;

@end
