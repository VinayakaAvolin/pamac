//
//  PASyncRequest.h
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <PANetworkService/PANetworkService.h>

typedef enum {
    PASyncRequestTypeNone,
    PASyncRequestTypeSetProfile,
} PASyncRequestType;


@interface PASyncRequest : PABaseRequest

- (instancetype)initWithRequestType:(PASyncRequestType)type
                         profileKey:(NSString *)profile
                              macId:(NSString *)macId
                          ipAddress:(NSString *)ipAddress
                               user:(NSString *)user
                           hostName:(NSString *)hostName
                            version:(NSString *)version
                             ostype:(NSString *)ostype
                             osname:(NSString *)osname
                              osver:(NSString *)osver;

@end
