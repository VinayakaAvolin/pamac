//
//  PASyncRegistryConstants.h
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifndef PASyncRegistryConstants_h
#define PASyncRegistryConstants_h

extern NSString *const kPALastAttemptUpdateTime;
extern NSString *const kPALastAttemptUploadTime;

extern NSString *const kPALastSuccessUpdateTime;
extern NSString *const kPALastSuccessUploadTime;

extern NSString *const kPALastPrunedTime;

extern NSString *const kPANextUpdateTime;

extern NSString *const kPAProfileKey;

extern NSString *const kPALastUpdateErrorCode;
extern NSString *const kPALastUploadErrorCode;

extern NSString *const kPACurrentPruneStatus;
extern NSString *const kPAPruneDelayedContentList;

extern NSString *const kPASyncRegistryKey;

extern NSString *const kPAUploadAgentUILogFileName;
extern NSString *const kPAAgentUILogFileName;
extern NSString *const kPAAgentUILogBackupFileName;


extern NSString *const kPAUploadEventLogFileName;
extern NSString *const kPAEventLogFileName;
extern NSString *const kPAEventLogBackupFileName;

extern NSString *const kPAzCatFileName;
extern NSString *const kPAzCatFolder;

#endif /* PASyncRegistryConstants_h */
