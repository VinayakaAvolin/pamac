//
//  PASyncConfiguration.h
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PASyncConfiguration : NSObject

@property(nonatomic, readonly) NSTimeInterval syncTimeIntervalInSeconds; // from config.cfg we get it in minutes
@property(nonatomic, readonly) NSTimeInterval syncTimeOutInSeconds; // from config.cfg we get it in hours
@property(nonatomic, readonly) NSUInteger uploadPerUpdateRatio; // 
@property(nonatomic, readonly) NSString *agentUILogUploadRemotePath;
@property(nonatomic, readonly) NSString *eventLogUploadRemotePath;

// every time after content sync; need to update sync configuration to read new sync config details
// returns true; if there is change in sync configuration
- (BOOL)update;

@end
