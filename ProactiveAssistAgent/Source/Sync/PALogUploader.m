//
//  PALogUploader.m
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <PACoreServices/PACoreServices.h>
#import "PALogUploader.h"
#import "PAFileSystemManager.h"
#import "PASyncConfiguration.h"
#import "PASyncRegistryConstants.h"
#import "NSString+Additions.h"

#define MAC_ID @"macid"
#define CURRENT_USER @"currentuser"
#define CTYPE @"ctype"
#define CTYPE_VALUE @"TG_DATAW_AGENTUILOGS"
#define CTYPE_EVENT_VALUE @"TG_DATAW_EVENTLOGS"
#define CONTENT_LOG @"agentui.log"
#define CONTENT_BACKUP_LOG @"agentui_"
#define CONTENT_LOG_EXTENSION @".log"
#define MAX_FILES_COUNT 5


@interface PALogUploader() {
  PASyncConfiguration *_syncConfigurations;
  PALogUploaderCallback _callback;
  
  
}
@property (nonatomic, retain) PAFileSystemManager *fileSystemManager;
@property (strong) NSString *macAddress;

@end

@implementation PALogUploader

- (instancetype)init
{
  self = [super init];
  if (self) {
    self.fileSystemManager = [[PAFileSystemManager alloc] init];
    _syncConfigurations = [[PASyncConfiguration alloc] init];
  }
  return self;
}

- (void)uploadLogsWithCallback:(PALogUploaderCallback) callback {
  _callback = callback;
  [self uploadAgentUILogs];
  [self uploadEventLogs];
}

- (void)uploadAgentUILogs {
  NSString* logFolderPath = [_fileSystemManager logsFileDirectoryPath];
  NSString* uploadFilePath = [logFolderPath stringByAppendingPathComponent:kPAUploadAgentUILogFileName];
  NSString* filePath = @"";
  if([_fileSystemManager doesFileExist:uploadFilePath]) {
    filePath = uploadFilePath;
  }else {
    filePath = [self uploadFilePathForAgentUILog];
    if (![_fileSystemManager doesFileExist:filePath]) {
      _callback(NO,nil);
      return;
    }
    [_fileSystemManager renameFile:filePath newFileName:kPAUploadAgentUILogFileName];
    filePath = uploadFilePath;
  }
  NSString* uploadURL = _syncConfigurations.agentUILogUploadRemotePath;
  uploadURL = [uploadURL stringByReplacingOccurrencesOfString:@"||" withString:@""];
  filePath = [filePath stringByConvertingIntoMacCompatibleFilePath];
  PAEnPointInfoManager *manager = [[PAEnPointInfoManager alloc] initWithOutputXml:filePath];
  
  if(nil == self.macAddress) {
    self.macAddress = [self getMACAddress];
  }
  
  NSDictionary* params = @{MAC_ID: self.macAddress, CURRENT_USER: NSUserName(), CTYPE: CTYPE_VALUE};
  [manager submitEndpointInfoWithUploadURL:uploadURL parameters:params completion:^(BOOL success, NSError* error, NSString *fileUploaded) {
    if (success) {
      
      [_fileSystemManager deleteFile:filePath];
    }
    _callback(success,error);
  }];
  
}

- (NSString*)uploadFilePathForAgentUILog {
  NSString* logFile = [_fileSystemManager logsFileDirectoryPath];
  NSString* logDirectory = [_fileSystemManager logsFileDirectoryPath];
  logFile = [logFile stringByAppendingPathComponent:CONTENT_LOG];
  
  NSArray* files = [_fileSystemManager filesOfDirectory:[_fileSystemManager logsFileDirectoryPath]];
  NSMutableArray* backupFiles = [NSMutableArray array];
  for (NSString* fileName in files) {
    if ([fileName rangeOfString:CONTENT_BACKUP_LOG].location != NSNotFound) {
      [backupFiles addObject:fileName];
    }
  }
  [backupFiles sortUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
    return [obj1 compare:obj2 options:NSCaseInsensitiveSearch];
  }];
  if (backupFiles.count == 0) {
    return logFile;
  }else {
    NSString* fileToUpload = [logDirectory stringByAppendingPathComponent:backupFiles[backupFiles.count -1]];
    return fileToUpload;
    
  }
  return logFile;
}



- (void)uploadEventLogs {
  NSString* logFolderPath = [_fileSystemManager logsFileDirectoryPath];
  NSString* uploadFilePath = [logFolderPath stringByAppendingPathComponent:kPAUploadEventLogFileName];
  NSString* filePath = @"";
  if([_fileSystemManager doesFileExist:uploadFilePath]) {
    filePath = uploadFilePath;
  }else {
    filePath = [self uploadFilePathForEventLog];
    if (![_fileSystemManager doesFileExist:filePath]) {
      _callback(NO,nil);
      return;
    }
    [_fileSystemManager renameFile:filePath newFileName:kPAUploadEventLogFileName];
    filePath = uploadFilePath;
  }
  NSString* uploadURL = _syncConfigurations.eventLogUploadRemotePath;
  uploadURL = [uploadURL stringByReplacingOccurrencesOfString:@"||" withString:@""];
  filePath = [filePath stringByConvertingIntoMacCompatibleFilePath];
  PAEnPointInfoManager *manager = [[PAEnPointInfoManager alloc] initWithOutputXml:filePath];
  
  if(nil == self.macAddress) {
    self.macAddress = [self getMACAddress];
  }
  
  NSDictionary* params = @{MAC_ID: self.macAddress, CURRENT_USER: NSUserName(), CTYPE: CTYPE_EVENT_VALUE};
  [manager submitEndpointInfoWithUploadURL:uploadURL parameters:params completion:^(BOOL success, NSError* error, NSString *fileUploaded) {
    if (success) {
      
      [_fileSystemManager deleteFile:filePath];
    }
    _callback(success,error);
  }];
  
}

- (NSString*)uploadFilePathForEventLog {
  NSString* logFile = [_fileSystemManager logsFileDirectoryPath];
  NSString* logDirectory = [_fileSystemManager logsFileDirectoryPath];
  logFile = [logFile stringByAppendingPathComponent:@"event.log"];
  
  NSArray* files = [_fileSystemManager filesOfDirectory:[_fileSystemManager logsFileDirectoryPath]];
  NSMutableArray* backupFiles = [NSMutableArray array];for (NSString* fileName in files) {
    if ([fileName hasPrefix:[NSString stringWithFormat:@"%@.",@"event"]] && ![fileName isEqualToString:@"event.log"]) {
      [backupFiles addObject:fileName];
    }
  }
  [backupFiles sortUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
    return [obj1 compare:obj2 options:NSCaseInsensitiveSearch];
  }];
  if (backupFiles.count == 0) {
    return logFile;
  }else {
    NSString* fileToUpload = [logDirectory stringByAppendingPathComponent:backupFiles[backupFiles.count -1]];
    return fileToUpload;
    
  }
  return logFile;
}

- (NSString*) getMACAddress {
  io_service_t platformExpert = IOServiceGetMatchingService(kIOMasterPortDefault,IOServiceMatching("IOPlatformExpertDevice"));
  if (!platformExpert)
    return nil;
  
  CFTypeRef serialNumberAsCFString = IORegistryEntryCreateCFProperty(platformExpert,CFSTR(kIOPlatformUUIDKey),kCFAllocatorDefault, 0);
  if (!serialNumberAsCFString)
    return nil;
  
  IOObjectRelease(platformExpert);
  return (__bridge NSString *)(serialNumberAsCFString);;
}

@end
