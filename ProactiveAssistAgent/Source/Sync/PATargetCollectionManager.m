//
//  PATargetCollectionManager.m
//  ProactiveAssistAgent
//
//  Copyright © 2017 Avolin. All rights reserved.
//
#import <PACoreServices/PACoreServices.h>
#import "PATargetCollectionManager.h"
#import "NSString+Additions.h"
#import "NSArray+Additions.h"

NSString* const kPATCOutputXmlFileName = @"trget_collection_script_output.xml";

NSString *const kPAFilteredataSets = @"filtered";
NSString *const kPANonFilteredataSets = @"nonfiltered";

@interface PATargetCollectionManager () {
  NSArray *_oldTargetCollections;
  NSArray *_latestTargetCollections;
  PATargetCollectionManifestType _manifestType;
  PAFileSystemManager *_fsManager;
  NSString *_targetCollectionScriptOutputXml;
  NSMutableArray *_filterList;
}
@property (nonatomic) PATargetCollectionManagerCompletionBlock completionBlock;

@end

@implementation PATargetCollectionManager

- (instancetype)init
{
  self = [super init];
  if (self) {
    _fsManager = [[PAFileSystemManager alloc] init];
    _filterList = [[NSMutableArray alloc] init];
  }
  return self;
}
- (void)constructTargetCollectionFromXml:(NSDictionary *)xmlDict
                                    type:(PATargetCollectionManifestType)manifestType
                              completion:(PATargetCollectionManagerCompletionBlock)block {
  
  _manifestType = manifestType;
  _completionBlock = block;
  PAManifestParser *xmlParser = [[PAManifestParser alloc] init];
  [xmlParser parsedTargetCollectionFromManifest:xmlDict completion:^(BOOL success, NSArray *targetCollectionList) {
    if (targetCollectionList.count) {
      switch (manifestType) {
        case PATargetCollectionManifestTypeLatest:
          _latestTargetCollections = [NSArray arrayWithArray: targetCollectionList];
          break;
          
        default:
          _oldTargetCollections = [NSArray arrayWithArray: targetCollectionList];
          break;
      }
    }
    //        _completionBlock(true);
    [self runTargetCollectionScripts];
  }];
}

- (NSArray *)targetCollectionList {
  switch (_manifestType) {
    case PATargetCollectionManifestTypeLatest:
      if (_latestTargetCollections.count) {
        return [NSArray arrayWithArray: _latestTargetCollections];
      }
      break;
      
    default:
      if (_oldTargetCollections.count) {
        return [NSArray arrayWithArray: _oldTargetCollections];
      }
      break;
  }
  
  return nil;
}

- (PATargetCollection *)targetCollectionMatchingName:(NSString *)name {
  NSArray *tgCollections = [self targetCollectionList];
  if (tgCollections.count) {
    NSArray *filtered = [tgCollections filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(name == %@)", name]];
    if (filtered.count) {
      return filtered[0];
    }
  }
  
  return nil;
}

- (NSDictionary *)filteredDataSetsAfterApplyingTargetCollectionRuleOn:(NSArray *)dataSets
                                                            type:(PATargetCollectionManifestType)manifestType {
  _manifestType = manifestType;
  
  NSMutableArray *filteredDatasetList = [[NSMutableArray alloc]init];
  NSMutableArray *nonfilteredDatasetList = [[NSMutableArray alloc]init];
  
  for (PADataSetInfo *dataSetInfo in dataSets) {
    NSArray *filters = dataSetInfo.filterList;
    BOOL doesPassTgRule = true;
    // if no filter then its applicable to all clients
    if (filters.count) {
      doesPassTgRule = false;
      //Creating a group level dictionary
      NSMutableDictionary *groupDict = [[NSMutableDictionary alloc] init];
      for (NSString *filter in filters) {
        PATargetCollection *tgcollection = [self targetCollectionMatchingName:filter];
        bool isTrueForGroup = false;
        if(tgcollection) {
          NSString *tgGroup = tgcollection.group;
          if ([groupDict objectForKey:tgGroup]) {
            isTrueForGroup = [[groupDict objectForKey:tgGroup] boolValue];
          }
          if (tgcollection && tgcollection.isApplicable) {
            //If any item in group is applicable, set group to true
            isTrueForGroup = true;
            [_filterList addObject:filter];
          }
          [groupDict setValue:[NSNumber numberWithBool:isTrueForGroup] forKey:tgGroup];
        }
      }
      if(groupDict.count > 0) {
        doesPassTgRule = true;
        //Iterate through all groups. If any group returns flase, set dataset as false
        for(id key in groupDict) {
          NSString *message = [@"  Combined Filters for group " stringByAppendingString:key];
          bool tDoesPassTgRule = [[groupDict objectForKey:key] boolValue];
          doesPassTgRule = (doesPassTgRule && tDoesPassTgRule);
        }
      }
    }
    
    if (doesPassTgRule) {
      [filteredDatasetList addObject:dataSetInfo];
    }
    else {
      [nonfilteredDatasetList addObject:dataSetInfo];
    }
  }
  
  if (filteredDatasetList.count) {
    //        NSArray *subArray = [filteredDatasetList subarrayWithRange:NSMakeRange(0, (filteredDatasetList.count > 20 ? 20 : filteredDatasetList.count))];
    //        return subArray;
    /// TODO: Return filtered data-set list
    //return [NSArray arrayWithArray: filteredDatasetList];
    
    //Passing both filtered and nonfiltered datasets
    NSDictionary *dict =[NSDictionary dictionaryWithObjects:@[[NSArray arrayWithArray: filteredDatasetList],[NSArray arrayWithArray: nonfilteredDatasetList]] forKeys:@[kPAFilteredataSets,kPANonFilteredataSets]];
    return [NSDictionary dictionaryWithDictionary:dict];
    
  } else {
    [[PALogger sharedLoggerForClient:PALogClientSyncManager] debug:@"Nothing is targetted for this Mac"];
  }
  return nil;
}

- (NSString *)filters {
  NSArray *filtersList = [_filterList arrayAfterRemovingDuplicates];
  if (filtersList.count) {
    return [filtersList componentsJoinedByString:@","];
  }
  return @"";
}

- (NSString *)targetCollectionXmlPath {
  NSString *outputXmlPath = [_fsManager smartIssuesFolderPath];
  outputXmlPath = [outputXmlPath stringByAppendingPathComponent:@"targetCollection.xml"];
  return outputXmlPath;
}

- (void)deleteTargetCollectionXml {
  if ([_fsManager doesFileExist:_targetCollectionScriptOutputXml]) {
    PAEnPointInfoManager *manager = [[PAEnPointInfoManager alloc]initWithOutputXml:_targetCollectionScriptOutputXml];
    [manager deleteEndpointInfo];
  }
  
}
- (void)runTargetCollectionScripts {
  NSString *outputXmlPath = [self targetCollectionXmlPath];
  NSString *scriptXmlPath = [_fsManager localDefaultXmlFilePath];
  
  if ([_fsManager doesFileExist:outputXmlPath]) {
    _targetCollectionScriptOutputXml = outputXmlPath;
    [self processTargetCollectionFilters];
  }else {
    PAEnPointInfoManager *manager = [[PAEnPointInfoManager alloc]initWithInputXml:scriptXmlPath outputXml:outputXmlPath source:PATargetScriptSourceTypeOthers];
    if (!manager) {
      _completionBlock(true);
      return;
    }
    [manager createAllEndpointInfoWithCompletion:^(BOOL success) {
      if ([_fsManager doesFileExist:outputXmlPath]) {
        _targetCollectionScriptOutputXml = outputXmlPath;
        [self processTargetCollectionFilters];
      }
      else {
        _completionBlock(true);
      }
    }];
  }
}

- (void)processTargetCollectionFilters {
  NSArray *targetCollectionList = nil;
  switch (_manifestType) {
    case PATargetCollectionManifestTypeLatest:
      [[PALogger sharedLoggerForClient:PALogClientSyncManager] debug:@"Process TargetCollection Filters for latest remote manifest"];
      targetCollectionList = _latestTargetCollections;
      break;
      
    default:
      [[PALogger sharedLoggerForClient:PALogClientSyncManager] debug:@"Process TargetCollection Filters for local manifest"];
      targetCollectionList = _oldTargetCollections;
      break;
  }
  @autoreleasepool {
    PAEnPointInfoManager *manager = [[PAEnPointInfoManager alloc]initWithOutputXml:_targetCollectionScriptOutputXml];
    for (PATargetCollection *tgCollection in targetCollectionList) {
      for (PATargetProperty *targetProperty in tgCollection.targetPropertyList) {
        NSString *className = targetProperty.className;
        NSString *property = targetProperty.classPropertyName;
        BOOL isValueList = targetProperty.isActualValueList;
        id propertyValue = nil;
        if (isValueList) {
          propertyValue = [manager valuesOfProperty:property inClass:className];
        } else {
          propertyValue = [manager valueOfProperty:property inClass:className];
        }
        targetProperty.actualValue = propertyValue;
      }
      [tgCollection executeTargetCollectionRule];
      
      [[PALogger sharedLoggerForClient:PALogClientSyncManager] debug:@"Is filter [%@]  applicable [%ld]",tgCollection.name,tgCollection.isApplicable];
    }
  }
  
  _completionBlock(true);
}
@end
