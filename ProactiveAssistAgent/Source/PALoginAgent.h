//
//  PALoginAgent.h
//  ProactiveAssistAgent
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <Foundation/Foundation.h>

#import "PALoginAgentProtocol.h"

@interface PALoginAgent : NSObject <NSXPCListenerDelegate, PALoginAgentProtocol>

+ (id)sharedLoginAgent;
- (void)initialize;
- (void)checkForNewAlerts;

@end
