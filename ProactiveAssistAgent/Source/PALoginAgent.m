//
//  PALoginAgent.m
//  ProactiveAssistAgent
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <PACoreServices/PACoreServices.h>
#import <PAIpcService/PAIpcService.h>
#include <stdlib.h>

#import "PALoginAgent.h"
#import "PASyncManager.h"
#import "PAAppSetupManager.h"
#import "PARelaTimeAlertMonitor.h"
#import "PAOptimizer.h"
#import "PAOptimizationScheduler.h"
#import "PAUnreadAlertMonitor.h"
#import "PAPlistLockManager.h"
#import "PASelfHealTrigger.h"

@interface PALoginAgent () <PASyncManagerDelegate, PAOptimizerDelegate> {
  PALoginAgentSyncResult _syncReplyCallback;
  PALoginAgentOptimizationResult _optimizationReplyCallback;
}
@property (nonatomic, retain) PASyncManager *syncManager;
@property (nonatomic, retain) PARelaTimeAlertMonitor *realTimeAlertMonitor;
@property (nonatomic, retain) PAOptimizer *optimizer;
@property (nonatomic, retain) PAOptimizationScheduler *scheduledOptimizer;
@property (nonatomic, retain) PAUnreadAlertMonitor *unreadAlertMonitor;
@property (nonatomic) PAFileSystemManager *fsManager;

@end

@implementation PALoginAgent

+ (id)sharedLoginAgent {
  static PALoginAgent *sharedLoginAgent = nil;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    sharedLoginAgent = [[self alloc] init];
  });
  return sharedLoginAgent;
}

-  (void)initialize {
  if ([PAAppSetupManager doAppSetup]) {
    self.syncManager = [[PASyncManager alloc] init];
    _realTimeAlertMonitor = [[PARelaTimeAlertMonitor alloc]init];
    [_realTimeAlertMonitor start];
    _scheduledOptimizer = [[PAOptimizationScheduler alloc]init];
    [_scheduledOptimizer start];
    _unreadAlertMonitor = [[PAUnreadAlertMonitor alloc]init];
    [_unreadAlertMonitor start];
    _fsManager = [[PAFileSystemManager alloc]init];
      if([_fsManager doesDirectoryExist:_fsManager.selfHealTriggerFolderPath]) {
          PASelfHealTrigger *selfHealTrigger = [[PASelfHealTrigger alloc] init];
          [selfHealTrigger initiateSelfHeal];
      }
  }
}

- (void)checkForNewAlerts {
  [_unreadAlertMonitor checkForNewAlertsNow];
}
- (void)updateWithCallback:(PALoginAgentSyncResult)reply {
  _syncReplyCallback = reply;
  self.syncManager.syncDelegate = self;
  [self.syncManager forceSyncContent];
}

- (void)startOptimizerWithReply:(PALoginAgentOptimizationResult)reply {
  _optimizationReplyCallback = reply; /// save optimization callback
  if (!_optimizer) {
    _optimizer = [[PAOptimizer alloc] init];
    [_optimizer start];
  }
}
- (void)stopOptimizerWithReply:(PALoginAgentOptimizationResult)reply {
  [_optimizer stop];
  _optimizer = nil;
}


-(void)scheduleOptimization:(NSDictionary *)scheduleInfo {
  [_scheduledOptimizer scheduleOptimization:scheduleInfo];
}

- (void)dealloc {
  [_realTimeAlertMonitor stop];
}

#pragma mark -
#pragma mark NSXPCConnection method overrides

- (BOOL)listener:(NSXPCListener *)listener shouldAcceptNewConnection:(NSXPCConnection *)newConnection
{
  NSXPCInterface *interface = [NSXPCInterface interfaceWithProtocol:@protocol(PALoginAgentProtocol)];
  
  NSSet *expected = [NSSet setWithObjects:[PAScriptInfo class], [NSString class], nil];
  NSSet *executeServiceClasssSet = [NSSet setWithObjects:[PAServiceInfo class], [NSString class], nil];
  NSSet *executePlistServiceClassList = [NSSet setWithObjects:[PAPlistServiceInfo class], [NSString class], nil];
  
  [interface setClasses:expected
            forSelector:@selector(executeScript:withReply:)
          argumentIndex:0
                ofReply:NO];
  
  [interface setClasses:executeServiceClasssSet
            forSelector:@selector(executeService:withReply:)
          argumentIndex:0
                ofReply:NO];
  [interface setClasses:executePlistServiceClassList
            forSelector:@selector(executePlistService:withReply:)
          argumentIndex:0
                ofReply:NO];
  // This method is called by the NSXPCListener to filter/configure
  // incoming connections.
  newConnection.exportedInterface = interface;
  newConnection.exportedObject = self;
  
  // Start processing incoming messages.
  [newConnection resume];
  
  return YES;
}

#pragma mark -
#pragma mark PALoginAgentProtocol methods

- (void)executeService:(PAServiceInfo *)service withReply:(PALoginAgentServiceResult)reply {
  [PAServiceInterface executeService:service.service category:service.serviceCategory info:service callback:^(BOOL success, NSError *error, id result) {
    PAServiceResponse *response = [[PAServiceResponse alloc]initWithSucessState:success result:result error:error];
    reply(response);
  }];
}

- (void)executeScript:(PAScriptInfo *)script withReply:(PALoginAgentScriptResult)reply {
  switch (script.scriptMode) {
    case PAScriptModeElevated:
      [self executescriptInElevatedMode:script withReply:reply];
      break;
    case PAScriptModeNormal:
      [self executescriptInNormalMode:script withReply:reply];
      break;
    default:
      break;
  }
  
}

- (void)executePlistService:(PAPlistServiceInfo *)service withReply:(PALoginAgentPlistServiceResult)reply {
  __block BOOL result = false;
  PAPlistLockManager *lckMgr = [PAPlistLockManager sharedInstance];
  [lckMgr runWithWriteLock:service.plistFilePath codeBlock:^{
    switch (service.service) {
      case PAPlistServiceSetRegValue:
      {
        PARegistryWriter* writer = [[PARegistryWriter alloc] initWithPlistAtPath:service.plistFilePath];
        result = [writer setRegValueForPath:service.sectionPath withKey:service.regKey andData:service.regData];
      }
        
        break;
      case PAPlistServiceDeleteNodeForPath:
      {
        PARegistryWriter* writer = [[PARegistryWriter alloc] initWithPlistAtPath:service.plistFilePath];
        result = [writer deleteNodeForPath:service.sectionPath];
      }
        break;
        
      case PAPlistServiceDeleteKey:
      {
        PARegistryWriter* writer = [[PARegistryWriter alloc] initWithPlistAtPath:service.plistFilePath];
        result = [writer deleteKey:service.regKey forPath:service.sectionPath];
      }
        break;
        
      default:
        result = (false);
        break;
    }
  }];
  reply(result);
}
- (void)executescriptInNormalMode:(PAScriptInfo *)script withReply:(PALoginAgentScriptResult)reply {
  if (script.scriptPath) {
    [PAScriptRunner executeScriptWithPath:script.scriptPath callBack:^(BOOL success, NSError *error, id result) {
      PAServiceResponse *response = [[PAServiceResponse alloc]initWithSucessState:success result:result error:error];
      reply(response);
    }];
  } else if (script.scriptString) {
    [PAScriptRunner executeScript:script.scriptString ofType:script.scriptType callBack:^(BOOL success, NSError *error, id result) {
      PAServiceResponse *response = [[PAServiceResponse alloc]initWithSucessState:success result:result error:error];
      reply(response);
    }];
    
  }
}
- (void)executescriptInElevatedMode:(PAScriptInfo *)script withReply:(PALoginAgentScriptResult)reply {
  [[PAInterProcessManager sharedManager] executeScript:script withReply:^(BOOL success, NSError *error, id result) {
    PAServiceResponse *response = [[PAServiceResponse alloc]initWithSucessState:success result:result error:error];
    reply(response);
  }];
}

#pragma mark - PASyncManagerDelegate methods

- (void)didFinishSync:(BOOL)finish {
  self.syncManager.syncDelegate = nil;
  if (_syncReplyCallback) {
    _syncReplyCallback(finish);
  }
}

#pragma mark - PAOptimizerDelegate methods

- (void)didFinishOptimization:(BOOL)finish {
  _optimizationReplyCallback(finish);
}

@end
