//
//  PARealTimeAlertInfo.h
//  ProactiveAssistAgent
//
//  Created by Kavitha on 27/10/17.
//  Copyright © 2017 Aptean. All rights reserved.
//  Written under contract by Robosoft Technologies Pvt. Ltd.
//

#import <Foundation/Foundation.h>

@interface PARealTimeAlertInfo : NSObject <NSCopying>

@property (nonatomic) NSString *expiryTimeStr; // alert expiry date & time
@property (nonatomic) NSString *contentFolderPath; // alert folder of form <contentguid.contentversion>
@property (nonatomic) NSString *inputXmlPath; //
@property (nonatomic) NSDate *expiryDate; // alert expiry date & time

- (instancetype)initWithExpiryDate:(NSString *)expiryDate
                 contentFolderPath:(NSString *)contentPath
                    contentXmlPath:(NSString *)xmlPath;

@end
