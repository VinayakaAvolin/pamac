//
//  PAInputConfigurationXmlParser.m
//  ProactiveAssistAgent
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PAInputConfigurationXmlParser.h"

NSString *const kPARTContentSetKey = @"content-set";
NSString *const kPARTContentKey = @"content";
NSString *const kPARTFieldSetKey = @"field-set";
NSString *const kPARTPropertySetKey = @"property-set";
NSString *const kPARTFieldKey = @"field";
NSString *const kPARTPropertyKey = @"property";
NSString *const kPARTNameKey = @"_name";
NSString *const kPARTValueKey = @"value";
NSString *const kPARTTextKey = @"__text";
NSString *const kPARTFieldValueChar = @"sccf_field_value_char";
NSString *const kPARTPropertyRequestGuid = @"scc_request_guid";
NSString *const kPARTPropertyTitle = @"scc_title";

@interface PAInputConfigurationXmlParser () {
  PAAlertInfo *_alertInfo;
  NSString *_inputXmlPath;
}

@end

@implementation PAInputConfigurationXmlParser

- (instancetype)initWithAlertInfo:(PAAlertInfo *)alertInfo {
  self = [super init];
  if (self) {
    if (alertInfo.contentGuid && alertInfo.contentVersion) {
      _alertInfo = alertInfo;
      _inputXmlPath = alertInfo.contentXmlPath;
    } else {
      self = nil;
    }
  }
  return self;
}

- (void)parseInputConfigurationWithCompletion:(PAInputConfigurationXmlParserCompletionBlock)completionBlock {
  NSDictionary *xmlDict = [self dictionaryFromXmlAtPath:_inputXmlPath];
  if (xmlDict) {
    NSDictionary *contentSetDict = [xmlDict valueForKey:kPARTContentSetKey];
    if ([contentSetDict isKindOfClass:[NSDictionary class]]) {
      NSDictionary *contentDict = [contentSetDict valueForKey:kPARTContentKey];
      if ([contentDict isKindOfClass:[NSDictionary class]]) {
        NSString *expiryDatestr = nil;
        NSString *requestGuid = nil;
        NSUInteger windowLength = 150;
        NSUInteger windowWidth = 150;
        NSUInteger alertTime = 0;
        BOOL ackRequired = false;
        NSString *alertTitle = nil;
        
        NSDictionary *fieldSetDict = [contentDict valueForKey:kPARTFieldSetKey];
        NSDictionary *propertySetDict = [contentDict valueForKey:kPARTPropertySetKey];
        if ([fieldSetDict isKindOfClass:[NSDictionary class]]) {
          NSArray *fieldsList = [fieldSetDict valueForKey:kPARTFieldKey];
          if ([fieldsList isKindOfClass:[NSArray class]]) {
            
            // read expiry date
            expiryDatestr = [self valueOfField:kPADODisplayTimeEnd inFieldList:fieldsList];
            if (![expiryDatestr isKindOfClass:[NSString class]]) {
              expiryDatestr = nil;
            }
            /*
             {
             "_name" = windowlength;
             value =     (
             {
             "__text" = 150;
             "_name" = "sccf_field_value_char";
             "_type" = S;
             },
             {
             "_name" = "sccfc_field_value_clob";
             "_type" = c;
             }
             );
             }
             */
            // read window length
            id windowLengthValue = [self valueOfField:kPADOWindowLength inFieldList:fieldsList];
            if ([windowLengthValue isKindOfClass:[NSString class]]) {
              windowLength = [windowLengthValue integerValue];
            }
            // read window width
            id windowWidthValue = [self valueOfField:kPADOWindowWidth inFieldList:fieldsList];
            if ([windowWidthValue isKindOfClass:[NSString class]]) {
              windowWidth = [windowWidthValue integerValue];
            }
            
            // read alert time
            // read window width
            id alertTimeValue = [self valueOfField:kPADOAlertTime inFieldList:fieldsList];
            if ([alertTimeValue isKindOfClass:[NSString class]]) {
              alertTime = [alertTimeValue integerValue];
            }
            
            // read acknowledge required
            id ackRequiredValue = [self valueOfField:kPADOAcknowledgeRequired inFieldList:fieldsList];
            if ([ackRequiredValue isKindOfClass:[NSString class]]) {
              ackRequired = [ackRequiredValue boolValue];
            }
          }
        }
        if ([propertySetDict isKindOfClass:[NSDictionary class]]) {
          NSArray *propertyList = [propertySetDict valueForKey:kPARTPropertyKey];
          if ([propertyList isKindOfClass:[NSArray class]]) {
            
            NSDictionary *propertyDict = [self dictionaryMatchingName:kPARTPropertyRequestGuid inList:propertyList];
            if ([propertyDict isKindOfClass:[NSDictionary class]]) {
              // read request guid
              requestGuid = [propertyDict valueForKey:kPARTTextKey];
              if (![requestGuid isKindOfClass:[NSString class]]) {
                requestGuid = nil;
              }
            }
            
            NSDictionary *propertyDict2 = [self dictionaryMatchingName:kPARTPropertyTitle inList:propertyList];
            if ([propertyDict2 isKindOfClass:[NSDictionary class]]) {
              // read request guid
              alertTitle = [propertyDict2 valueForKey:kPARTTextKey];
              if (![alertTitle isKindOfClass:[NSString class]]) {
                alertTitle = nil;
              }
            }
          }
        }
        PAAlertInfo *alert = [[PAAlertInfo alloc] initWithContentGuid:_alertInfo.contentGuid version:_alertInfo.contentVersion requestGuid:requestGuid windowWidth:windowWidth windowHeight:windowLength acknowledgeRequired:ackRequired alertTimeInterval:alertTime expiryDate:expiryDatestr alertType:_alertInfo.alertType contentTitle:alertTitle];
        completionBlock(true, alert);
        return;
      }
    }
  }
  completionBlock(false, _alertInfo);
}

- (id)valueOfField:(NSString *)fieldName inFieldList:(NSArray *)fieldList {
  // read window length
  NSDictionary *fieldOuterDict = [self dictionaryMatchingName:fieldName inList:fieldList];
  if ([fieldOuterDict isKindOfClass:[NSDictionary class]]) {
    NSMutableArray *fieldValues = [[NSMutableArray alloc]init];
    id fieldValueObject = [fieldOuterDict valueForKey:kPARTValueKey];
    if ([fieldValueObject isKindOfClass:[NSDictionary class]]) {
      [fieldValues addObject:fieldValueObject];
    } else if ([fieldValueObject isKindOfClass:[NSArray class]]) {
      [fieldValues addObjectsFromArray:fieldValueObject];
    }
    
    NSDictionary *fieldDict = [self dictionaryMatchingName:kPARTFieldValueChar inList:fieldValues];
    if ([fieldDict isKindOfClass:[NSDictionary class]]) {
      return [fieldDict valueForKey:kPARTTextKey];
      
    }
  }
  return nil;
}
- (NSDictionary *)dictionaryMatchingName:(NSString *)name inList:(NSArray *)inputList {
  if (inputList.count) {
    NSArray *filtered = [inputList filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(_name == %@)", name]];
    if (filtered.count) {
      return filtered[0];
    }
  }
  
  return nil;
}



@end
