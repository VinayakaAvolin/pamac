//
//  PARealTimeAlertHandler.h
//  ProactiveAssistAgent
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PARealTimeAlertHandler : NSObject

- (void)handleAlertWithInfo:(NSDictionary *)alertInfo;

@end
