//
//  PAXmppDetailsProvider.h
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <PACoreServices/PACoreServices.h>

typedef void(^PAXmppDetailsProviderCompletionBlock)(BOOL success, PAXmppConnectInfo *xmppInfo);

@interface PAXmppDetailsProvider : NSObject

@property(nonatomic, readonly) PAXmppConnectInfo *xmppConnectDetail;

- (void)getXmppConnectDetailsWithCompletion:(PAXmppDetailsProviderCompletionBlock)completionBlock;

@end
