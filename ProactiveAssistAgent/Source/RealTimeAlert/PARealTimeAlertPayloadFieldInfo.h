//
//  PARealTimeAlertPayloadFieldInfo.h
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

/*
 Class to represent a filed under "field-set"

 <field name="Alert Content">
    <value name="sccf_field_value_char" type="S">|</value>
    <value name="sccf_field_value_int" type="N">1762</value>
    <value name="sccfc_field_value_clob" type="c">Alert Content.htm</value>
 </field>
*/
@interface PARealTimeAlertPayloadFieldInfo : NSObject

@property (nonatomic, readonly) NSString *fieldName;
@property (nonatomic, readonly) NSString *charValue;
@property (nonatomic, readonly) NSString *intValue;
@property (nonatomic, readonly) NSString *clobValue;

- (instancetype)initWithFieldPayload:(NSDictionary *)fieldDict;

@end
