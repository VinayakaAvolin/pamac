//
//  PAGetServerResponse.m
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PAGetServerResponse.h"

NSString *const kPARAResponseKeyXmppDomain = @"xmppDomain";
NSString *const kPARAResponseKeyXmppHost = @"xmppHost";
NSString *const kPARAResponseKeyXmppPort = @"xmppPort";
NSString *const kPARAResponseKeyXmppLicence = @"xmppLicense";

@implementation PAGetServerResponse

- (instancetype)initWithHeaders:(NSDictionary *)headers
                     statusCode:(NSInteger)code
                     outputData:(NSData *)data {
  self = [super initWithHeaders:headers statusCode:code outputData:data];
  if (self) {
    NSDictionary *responseDict = self.jsonResponse;
    if ([responseDict isKindOfClass:[NSString class]]) {
      responseDict = nil;
      NSString *responseStr = (NSString *)self.jsonResponse;
      /// remove quotes from string
      responseStr = [responseStr stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:@""];
      responseStr = [responseStr stringByReplacingCharactersInRange:NSMakeRange(responseStr.length-1, 1) withString:@""];
      /// Replace \" with "
      responseStr = [responseStr stringByReplacingOccurrencesOfString:@"\\\"" withString:@"\""];
      responseDict = [self dictionaryFromJsonString:responseStr];
    }
    if ([responseDict isKindOfClass:[NSDictionary class]]) {
      NSString *host = responseDict[kPARAResponseKeyXmppHost];
      NSString *domain = responseDict[kPARAResponseKeyXmppDomain];
      NSNumber *port = responseDict[kPARAResponseKeyXmppPort];
      NSString *licence = responseDict[kPARAResponseKeyXmppLicence];
      
      _xmppServerInfo = [[PAXmppConnectInfo alloc] initWithDomain:domain host:host port:port licenceKey:licence userName:nil password:nil];
    }
  }
  return self;
}

- (NSDictionary *)dictionaryFromJsonString:(NSString *)inputStr {
  if ([inputStr isKindOfClass:[NSString class]]) {
    NSData *valueData = [inputStr dataUsingEncoding:NSUTF8StringEncoding];
    id valueJson = [NSJSONSerialization JSONObjectWithData:valueData options:NSJSONReadingAllowFragments error:nil];
    if ([valueJson isKindOfClass:[NSDictionary class]]) {
      return valueJson;
    }
  }
  return nil;
}

@end
/*
 {
 SocketConnectionType = bosh;
 httpBind = "https://experience931.avolin.com:7070/http-bind/";
 xmppConnectionTimeout = 20000;
 xmppDomain = "https://experience931.avolin.com";
 xmppHost = DGA1APP12SUPSFT;
 xmppLicense = "67YW3vI79uFg0nGzZEICHn1YVYTz7ZCGEkax%2fSrb2OuR78rhaPhLa%2bD7YkQshwIUPzwR1CiHbKMs69XZpD%2b3Peb%2bKu51WkQAb9N%2bqoj%2btrV1XqjucE0Cm95nLLFcE6ctKyqRM552o8V6De9IPDNelNsLd2THXGR%2bLiXUlCW4TLHzdNa5IRtqLjbb9HGLoYxFVKJLABI8QeW24hug3k3yqTGTRFdCsMc7XeDdiKrSMPLOJ38mJCmK35523GR9tVHR2SnulKmyPMYMlnslYfnDzMcCUBZiaCuj0Z6GX2QUezDaggcEHkTilawQ6fgcq8kpAhiJS9Xoi9TdQfdQUo7p6TIT4lrF4lDYoM53Esiu%2bUh1Oh%2bjNXT1hsbxxXDrMduBMk7FxtR6q7gnlcxu4bKsaLImlO19%2bjR0wNF0RXpxzek4r%2f40RxLAJ11yPQt8Pajp%2bLgxEj8FmyP6rTPc%2b%2bx4jLmHv3naCTAZn80%2bGNC7s67w%2b9wSt%2b1VLg0najQW%2bsLsoANX2yFSa6wPzgLWrlk5bHvukz7ky0avsgKq00SuBYCGE3gilO6ujN6KiFVD6X1HIgHtsSwTKgOR2%2fx%2bmRcHYtO%2fbAwLNEKsnnQTRYwTbLK286xKXnbX86OCU7H%2bz8r9qyaqDE%2fLgeYsahMeAqWQlmUaZS1iKoAbWLlqdJbPLCUqpOp0%2bSARgD0YbhSSd6Tz0kFZWytKoP4FERw%2b9OxOws6%2b9jZ87wMrVf%2f5afR4Oiwmpw32LKCZ9%2fE39v5zGg450jYnLLsxhFqEAp5fKna0KIfRyNcAIS9gJs9%2fd9AWEGHHMK2D9gfMQIdCRNVVBlIaq7ieUdVojScJ92DnMU%2fa56m%2b7EMC%2f901EsVSCi541FtSDA0Fob3ZVqFWuSildWwGhYugBcCEi4ln6jMw9MtuXGsmFrMcT24yTn%2beGODPbD4Q8c4QLVRdiS2786P20BlZKhofMhDZdhLt2hdRGpzDp0iGevyWGhKGwhWKcE%2bl7OzMa1eEn7FEE%2fMauh9h1XiuwBw2JJeMsAs1hNV0gV%2bqe2RObz7LghHSNd4cxwIlgT0syR2XlkNu4z%2bc8Zhbz1nQ8qeXP9VTYxjXV5FkXZsGbVTM5IbCCf71UwZ42UZ4rkUvS8EWCcDA0eqoHl5nF4cM5PlLuJNPMZpZbi2OYbRuujQ6YjONURXVeL8XKLzk3toa8G5Oh4kbKoTBPh9k5NM%2bNiwlloINm8DFInd2XSrnM3YDkTfNQ43l2MXk530qib4%3d";
 xmppPort = 5222;
 }
 */
