//
//  PAGetTokenResponse.h
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <PANetworkService/PANetworkService.h>

@interface PAGetTokenResponse : PABaseResponse

@property(nonatomic, readonly) NSString *accessToken;
@property(nonatomic, readonly) NSUInteger expiresIn;
@property(nonatomic, readonly) NSString *tokenType;

@property(nonatomic, readonly) NSString *authorizationKey;

@end
