//
//  PAGetPasswordResponse.m
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PAGetPasswordResponse.h"

@implementation PAGetPasswordResponse

- (instancetype)initWithHeaders:(NSDictionary *)headers
                     statusCode:(NSInteger)code
                     outputData:(NSData *)data {
  self = [super initWithHeaders:headers statusCode:code outputData:data];
  if (self) {
    NSDictionary *responseDict = self.jsonResponse;
    if ([responseDict isKindOfClass:[NSString class]]) {
      responseDict = nil;
      NSString *responseStr = (NSString *)self.jsonResponse;
      /// remove quotes from string
      responseStr = [responseStr stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:@""];
      responseStr = [responseStr stringByReplacingCharactersInRange:NSMakeRange(responseStr.length-1, 1) withString:@""];
      
      _password = responseStr;
    }
  }
  return self;
}


@end
