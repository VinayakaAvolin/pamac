//
//  PARealTimeAlertRequest.m
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <PAConfigurationService/PAConfigurationService.h>
#import "PARealTimeAlertRequest.h"

NSString *const kPARARequestGuidKey = @"requestguid";
NSString *const kPARARequestClientNameKey = @"clientname";
NSString *const kPARARequestIsIntendedNameKey = @"IsIntended";
NSString *const kPARARequestExitCodeKey = @"exitcode";

@interface PARealTimeAlertRequest () {
  PARealTimeAlertRequestType _requestType;
  NSString *_guid;
  NSString *_clientName;
  NSNumber *_isIntended;
  NSNumber *_exitCode;
  PARealTimeAlertConfiguration *_realTimeAlertConfiguration;
}

@end

@implementation PARealTimeAlertRequest

- (instancetype)initWithRequestType:(PARealTimeAlertRequestType)type
                               guid:(NSString *)guid
                         clientName:(NSString *)client
                         isIntended:(NSNumber *)isIntended{
  return [self initWithRequestType:type guid:guid clientName:client isIntended:isIntended exitCode:nil];
}
- (instancetype)initWithRequestType:(PARealTimeAlertRequestType)type
                               guid:(NSString *)guid
                         clientName:(NSString *)client
                         isIntended:(NSNumber *)isIntended
                           exitCode:(NSNumber *)exitCode {
  self = [super init];
  if (self) {
    if (type == PARealTimeAlertRequestTypeNone) {
      self = nil;
    } else {
      _requestType = type;
      _guid = guid;
      _clientName = client;
      _isIntended = isIntended;
      _exitCode = exitCode;
      _realTimeAlertConfiguration = [PAConfigurationManager appConfigurations].realtimeAlertconfigurations;
    }
  }
  return self;
}
- (NSString *)baseUrl {
  return [super baseUrl];
}

- (NSString *)path {
  NSString *urlPath = nil;
  switch (_requestType) {
    case PARealTimeAlertRequestTypeMessageAcknowledgement:
      urlPath = [self realTimeAcknowledgementUrlPath];
      break;
    case PARealTimeAlertRequestTypeMessageDelivered:
      urlPath = [self realTimeDeliveredUrlPath];
      break;
    default:
      break;
  }
  return urlPath;
}

- (NSString *)encoding {
  return nil;
}

- (NSString *)method {
  return kPABRMethodNamePost;
}

- (NSDictionary *)headers {
  return [super headers];
}

- (NSDictionary *)parameters {
  return nil;
}

- (NSString *)realTimeAcknowledgementUrlPath {
  NSString *urlPath = _realTimeAlertConfiguration.acknowledgeUrlPath;
  NSString *params = nil;
  if (_exitCode) {
    params = [NSString stringWithFormat:@"%@?%@=%@&%@=%@&%@=%@",urlPath, kPARARequestGuidKey,_guid, kPARARequestClientNameKey,_clientName, kPARARequestExitCodeKey, _exitCode];
  } else {
    params = [NSString stringWithFormat:@"%@?%@=%@&%@=%@",urlPath, kPARARequestGuidKey,_guid, kPARARequestClientNameKey,_clientName];
  }
  return params;
}
- (NSString *)realTimeDeliveredUrlPath {
  NSString *urlPath = _realTimeAlertConfiguration.deliveryUrlPath;
  NSString *params = [NSString stringWithFormat:@"%@?%@=%@&%@=%@&%@=%@",urlPath, kPARARequestGuidKey,_guid, kPARARequestClientNameKey,_clientName, kPARARequestIsIntendedNameKey, _isIntended];
  return params;
}
- (NSDictionary *)realTimeAcknowledgementRequestParams {
  NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
  [params setValue:_guid forKey:kPARARequestGuidKey];
  [params setValue:_clientName forKey:kPARARequestClientNameKey];
  return [NSDictionary dictionaryWithDictionary:params];
}
- (NSDictionary *)realTimeDeliveredRequestParams {
  NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
  [params setValue:_guid forKey:kPARARequestGuidKey];
  [params setValue:_isIntended forKey:kPARARequestIsIntendedNameKey];
  [params setValue:_clientName forKey:kPARARequestClientNameKey];
  return [NSDictionary dictionaryWithDictionary:params];
}
@end
