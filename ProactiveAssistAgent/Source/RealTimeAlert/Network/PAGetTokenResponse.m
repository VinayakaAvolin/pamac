//
//  PAGetTokenResponse.m
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PAGetTokenResponse.h"

NSString *const kPARAResponseKeyAccessToken = @"access_token";
NSString *const kPARAResponseKeyTokenType = @"token_type";
NSString *const kPARAResponseKeyExpiresIn = @"expires_in";

@implementation PAGetTokenResponse

- (instancetype)initWithHeaders:(NSDictionary *)headers
                     statusCode:(NSInteger)code
                     outputData:(NSData *)data {
  self = [super initWithHeaders:headers statusCode:code outputData:data];
  if (self && [self.jsonResponse isKindOfClass:[NSDictionary class]]) {
    _accessToken = self.jsonResponse[kPARAResponseKeyAccessToken];
    _tokenType = self.jsonResponse[kPARAResponseKeyTokenType];
    _expiresIn = [self.jsonResponse[kPARAResponseKeyExpiresIn] integerValue];
  }
  return self;
}

- (NSString *)authorizationKey {
  NSString *authKey = @"";
  if (_tokenType && _accessToken) {
    authKey = [NSString stringWithFormat:@"%@ %@",_tokenType, _accessToken];
  }
  return authKey;
}

@end
/*
 {
 "access_token": "82-PZt_9ThabkqxGf1CL0K2zvXq9L9aOaju-HO7ODB9kR8Ci8_6N2ERg4hyfDAdhywxogPIs0bO8U8tZR2cjlYOg7NJzBYQC8d-gopPUdqMooXE2Z9TOEXm7_WMpUtaQ_1ouDUan-T269q_aeoWimrJMyPcowSPwRK_S1r2f0xCklisZn6sLC-cblZpGlxXS00WMC-AHk42fdKYxOumHyDy9wWBHsh9rgfuqC9LQeCN6coj7YbE2xD-BkJDAmnBBtMHELSboYu-jnTkaeHnvEM5mE4XgqTyE-cfzk7yem-5no7hVL3xNJdpmEIir1rlfYsqEU5YXFFjjAktHK9oppRyDqO8EyMUYu85Pm30WM9Y",
 "token_type": "bearer",
 "expires_in": 119
 }
 */
