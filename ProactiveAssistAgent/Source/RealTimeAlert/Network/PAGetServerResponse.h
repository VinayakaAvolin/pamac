//
//  PAGetServerResponse.h
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <PACoreServices/PACoreServices.h>

@interface PAGetServerResponse : PABaseResponse

@property(nonatomic, readonly) PAXmppConnectInfo *xmppServerInfo;

@end
