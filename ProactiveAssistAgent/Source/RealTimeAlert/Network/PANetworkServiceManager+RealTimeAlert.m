//
//  PANetworkServiceManager+RealTimeAlert.m
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PANetworkServiceManager+RealTimeAlert.h"

@implementation PANetworkServiceManager (RealTimeAlert)

- (void)realTimeAlertDelivered:(PARealTimeAlertRequest *)request
                      callback:(PANetworkServiceManagerCallback)callback {
  NSURLSessionDataTask *task = nil;
  task = [self executeRequest:request completion:^(NSError *error, PABaseResponse *response) {
    [self removeDataTask:task];
    callback(error, response);
  }];
  [self addDataTask:task];
}

- (void)realTimeAlertAcknowledgement:(PARealTimeAlertRequest *)request
                            callback:(PANetworkServiceManagerCallback)callback {
  NSURLSessionDataTask *task = nil;
  task = [self executeRequest:request completion:^(NSError *error, PABaseResponse *response) {
    [self removeDataTask:task];
    callback(error, response);
  }];
  [self addDataTask:task];
}

- (void)getToken:(PARealTimeAlertDetailRequest *)request
        callback:(PANetworkServiceManagerCallback)callback {
  NSURLSessionDataTask *task = nil;
  task = [self executeRequest:request completion:^(NSError *error, PABaseResponse *response) {
    [self removeDataTask:task];
    callback(error, response);
  }];
  [self addDataTask:task];
}

- (void)getServer:(PARealTimeAlertDetailRequest *)request
         callback:(PANetworkServiceManagerCallback)callback {
  NSURLSessionDataTask *task = nil;
  task = [self executeRequest:request completion:^(NSError *error, PABaseResponse *response) {
    [self removeDataTask:task];
    callback(error, response);
  }];
  [self addDataTask:task];
}

- (void)getPassword:(PARealTimeAlertDetailRequest *)request
           callback:(PANetworkServiceManagerCallback)callback{
  NSURLSessionDataTask *task = nil;
  task = [self executeRequest:request completion:^(NSError *error, PABaseResponse *response) {
    [self removeDataTask:task];
    callback(error, response);
  }];
  [self addDataTask:task];
}
@end
