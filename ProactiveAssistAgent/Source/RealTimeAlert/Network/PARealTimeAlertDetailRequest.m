//
//  PARealTimeAlertDetailRequest.m
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <PAConfigurationService/PAConfigurationService.h>
#import "PARealTimeAlertDetailRequest.h"

NSString *const kPARARequestMachineNameKey = @"MachineName";
NSString *const kPARARequestUserNameKey = @"username";
NSString *const kPARARequestPasswordKey = @"password";
NSString *const kPARARequestGrantTypeKey = @"grant_type";
NSString *const kPARARequestHeaderAuthorization = @"Authorization";

@interface PARealTimeAlertDetailRequest () {
  PARealTimeAlertDetailRequestType _requestType;
  NSString *_userName;
  NSString *_password;
  NSString *_grantType;
  NSString *_authorization;
  NSString *_clientName;
  PARealTimeAlertConfiguration *_realTimeAlertConfiguration;
}

@end

@implementation PARealTimeAlertDetailRequest

- (instancetype)initWithRequestType:(PARealTimeAlertDetailRequestType)type
                           userName:(NSString *)userName
                           password:(NSString *)password
                          grantType:(NSString *)grantType
                      authorization:(NSString *)authorization
                         clientName:(NSString *)client {
  self = [super init];
  if (self) {
    _requestType = type;
    _userName = userName;
    _password = password;
    _grantType = grantType;
    _authorization = authorization;
    _clientName = client;
    _realTimeAlertConfiguration = [PAConfigurationManager appConfigurations].realtimeAlertconfigurations;
  }
  return self;
}

- (NSString *)baseUrl {
  return [super baseUrl];
}

- (NSString *)path {
  NSString *urlPath = nil;
  switch (_requestType) {
    case PARealTimeAlertDetailRequestTypeGetToken:
      urlPath = [self getTokenUrlPath];
      break;
    case PARealTimeAlertDetailRequestTypeGetServer:
      urlPath = [self getServerUrlPath];
      break;
    case PARealTimeAlertDetailRequestTypeGetPassword:
      urlPath = [self getPasswordUrlPath];
      break;
    default:
      break;
  }
  return urlPath;
}

- (NSString *)encoding {
  return nil;
}

- (NSString *)method {
  NSString *httpMethod = nil;
  switch (_requestType) {
    case PARealTimeAlertDetailRequestTypeGetToken:
      httpMethod = kPABRMethodNamePost;
      break;
    case PARealTimeAlertDetailRequestTypeGetServer:
    case PARealTimeAlertDetailRequestTypeGetPassword:
      httpMethod = kPABRMethodNameGet;
      break;
    default:
      break;
  }
  return httpMethod;
}

- (NSDictionary *)headers {
  NSDictionary *headers = [super headers];
  switch (_requestType) {
    case PARealTimeAlertDetailRequestTypeGetServer:
    case PARealTimeAlertDetailRequestTypeGetPassword:
      headers = [self headerWithAuthorizationKey];
      break;
    case PARealTimeAlertDetailRequestTypeGetToken:
      headers = [self headerForFormUrlEncodedRequest];
      break;
    default:
      break;
  }
  return headers;
}

- (NSDictionary *)parameters {
  NSDictionary *params = nil;
  switch (_requestType) {
    case PARealTimeAlertDetailRequestTypeGetToken:
      params = [self getTokenParameters];
      break;
    default:
      break;
  }
  return params;
}


- (NSDictionary *)getTokenParameters {
  NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
  [dict setValue:_userName forKey:kPARARequestUserNameKey];
  [dict setValue:_password forKey:kPARARequestPasswordKey];
  [dict setValue:_grantType forKey:kPARARequestGrantTypeKey];
  return [NSDictionary dictionaryWithDictionary:dict];
}
- (NSDictionary *)headerForFormUrlEncodedRequest {
  NSMutableDictionary *dict = [[super headers] mutableCopy];
  [dict setValue:kPABRContentTypeFormUrlEncoded forKey:kPABRContentType];
  return [NSDictionary dictionaryWithDictionary:dict];
}

- (NSDictionary *)headerWithAuthorizationKey {
  NSMutableDictionary *dict = [[super headers] mutableCopy];
  [dict setValue:_authorization forKey:kPARARequestHeaderAuthorization];
  return dict;
}

- (NSString *)getTokenUrlPath {
  NSString *urlPath = _realTimeAlertConfiguration.getTokenUrlPath;
  return urlPath;
}
- (NSString *)getServerUrlPath {
  NSString *urlPath = _realTimeAlertConfiguration.getServerUrlPath;
  return urlPath;
}
- (NSString *)getPasswordUrlPath {
  NSString *urlPath = _realTimeAlertConfiguration.getPasswordUrlPath;
  NSString *path = [NSString stringWithFormat:@"%@?%@=%@",urlPath, kPARARequestMachineNameKey,_clientName];
  
  return path;
}
@end
