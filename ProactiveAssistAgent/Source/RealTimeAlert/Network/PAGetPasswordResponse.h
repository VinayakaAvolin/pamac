//
//  PAGetPasswordResponse.h
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <PANetworkService/PANetworkService.h>

@interface PAGetPasswordResponse : PABaseResponse

@property(nonatomic, readonly) NSString *password;

@end
