//
//  PARealTimeAlertRequest.h
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <PANetworkService/PANetworkService.h>

typedef enum {
    PARealTimeAlertRequestTypeNone,
    PARealTimeAlertRequestTypeMessageDelivered,
    PARealTimeAlertRequestTypeMessageAcknowledgement
} PARealTimeAlertRequestType;


@interface PARealTimeAlertRequest : PABaseRequest

- (instancetype)initWithRequestType:(PARealTimeAlertRequestType)type
                               guid:(NSString *)guid
                         clientName:(NSString *)client
                         isIntended:(NSNumber *)isIntended;
- (instancetype)initWithRequestType:(PARealTimeAlertRequestType)type
                               guid:(NSString *)guid
                         clientName:(NSString *)client
                         isIntended:(NSNumber *)isIntended
                           exitCode:(NSNumber *)exitCode;

@end
