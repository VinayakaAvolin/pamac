//
//  PARealTimeAlertDetailRequest.h
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <PANetworkService/PANetworkService.h>

typedef enum {
    PARealTimeAlertDetailRequestTypeNone,
    PARealTimeAlertDetailRequestTypeGetToken,
    PARealTimeAlertDetailRequestTypeGetServer,
    PARealTimeAlertDetailRequestTypeGetPassword
} PARealTimeAlertDetailRequestType;

@interface PARealTimeAlertDetailRequest : PABaseRequest

- (instancetype)initWithRequestType:(PARealTimeAlertDetailRequestType)type
                           userName:(NSString *)userName
                           password:(NSString *)password
                         grantType:(NSString *)grantType
                      authorization:(NSString *)authorization
                         clientName:(NSString *)client;


@end
