//
//  PANetworkServiceManager+RealTimeAlert.h
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <PANetworkService/PANetworkService.h>
#import "PARealTimeAlertRequest.h"
#import "PARealTimeAlertDetailRequest.h"

@interface PANetworkServiceManager (RealTimeAlert)

- (void)realTimeAlertDelivered:(PARealTimeAlertRequest *)request
                      callback:(PANetworkServiceManagerCallback)callback;

- (void)realTimeAlertAcknowledgement:(PARealTimeAlertRequest *)request
                            callback:(PANetworkServiceManagerCallback)callback;

- (void)getToken:(PARealTimeAlertDetailRequest *)request
        callback:(PANetworkServiceManagerCallback)callback;

- (void)getServer:(PARealTimeAlertDetailRequest *)request
         callback:(PANetworkServiceManagerCallback)callback;

- (void)getPassword:(PARealTimeAlertDetailRequest *)request
           callback:(PANetworkServiceManagerCallback)callback;

@end
