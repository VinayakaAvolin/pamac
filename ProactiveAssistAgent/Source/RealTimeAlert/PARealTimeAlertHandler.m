//
//  PARealTimeAlertHandler.m
//  ProactiveAssistAgent
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <PACoreServices/PACoreServices.h>
#import <PASupportService/PASupportService.h>
#import "PARealTimeAlertHandler.h"
#import "PANetworkServiceManager+RealTimeAlert.h"
#import "NSHost+Additions.h"
#import "PARealTimeAlertPayloadInfo.h"
#import "PARealTimeAlertContentCreator.h"
#import "NSString+Additions.h"
#import "NSDate+Additions.h"
#import "PASyncRegistryConstants.h"
#import "NSArray+Additions.h"
#import "PACertificateUtility.h"
#import "PAPlistLockManager.h"

@interface PARealTimeAlertHandler () {
  NSDictionary *_alertInfo;
  NSMutableArray *_networkServiceManagers;
  PARealTimeAlertPayloadInfo *_payload;
  PARealTimeAlertContentCreator *_contentCreator;
  PAFileSystemManager *_fsManager;
  BOOL _isAlertIntendedToTarget;
  NSString *sContentVersion;
  NSString *sExitCode;
  NSString *sExecTimeFoSA;
  NSString *sRequestStatus;
}

@end

@implementation PARealTimeAlertHandler

- (instancetype)init
{
  self = [super init];
  if (self) {
    _networkServiceManagers = [[NSMutableArray alloc] init];
    _contentCreator = [[PARealTimeAlertContentCreator alloc] init];
    _fsManager = [[PAFileSystemManager alloc]init];
  }
  return self;
}

- (void)handleAlertWithInfo:(NSDictionary *)alertInfo {
  if (!alertInfo) {
    [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"Real time alert info absent"];
    return;
  }
  _alertInfo = alertInfo;

  //Send Status to Server  - Request Received.
  sRequestStatus = @"Request Received";
  [self UpdateStatus];

  // parse xmpp payload first
  [self parseAlertPayload];
}

- (void)parseAlertPayload {
  if([[NSString stringWithFormat:@"%@", _alertInfo[@"SSRequestType"]] isEqualToString:@"RealtimeAlert"] == YES)
  {
    _payload = [[PARealTimeAlertPayloadInfo alloc]initWithPayload:_alertInfo];
    if (![self canHandleAlert]) {
      [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"Real time alert cannot be handle due to incorrect request channel"];
      return;
    }
    //if (![self isAlertFromTrustedSource]) {
    //    return;
    //}
    
    // check if alert is expired; if not expired then continue;
    if (![self isAlertExpired]) {
      _isAlertIntendedToTarget = [self isAlertIntendedToUser];
      // inform server that alert is deleivered
      [self alertDelivered];
      // check if alert is targetted to user; if targetted then only proceed
      if (_isAlertIntendedToTarget) {
        [self handleRealTimeAlert];
      } else {
        [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"Real time alert not intended to this target"];
      }
    } else {
      [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"Real time alert is expired"];
    }
  }
  else if([[NSString stringWithFormat:@"%@", _alertInfo[@"SSRequestType"]] isEqualToString:@"Execute"] == YES)
  {
    [self handleSupportAction];
  }
}

//----------Support Action Related Code Commence------------------------------------------------------------
- (void)handleSupportAction {
  if([[NSString stringWithFormat:@"%@", _alertInfo[@"SSRequestChannel"]] isEqualToString:@"SupportAction"] == NO){
    return;
  }
  if ([[NSString stringWithFormat:@"%@", _alertInfo[@"sprt_contentguid"]] isEmptyOrHasOnlyWhiteSpaces] == YES) {
    return;
  }
  NSDate *startTime = [NSDate date];
  PAServiceInfo *info = [[PAServiceInfo alloc]init];
  info.guid = [NSString stringWithFormat:@"%@", _alertInfo[@"sprt_contentguid"]];
  info.serviceCategory = PASupportServiceCategorySupportAction;
  NSString *folderName = [_fsManager getMatchingFileName:[NSString stringWithFormat:@"%@", _alertInfo[@"sprt_contentguid"]] inBaseDirectory:[_fsManager supportActionFolderPath]];
  NSArray *folderVersion = [folderName componentsSeparatedByString:@"."];
  if(folderVersion.count > 0) {
    sContentVersion = [folderVersion objectAtIndex:1];
  }
  
  info.service = [self supportActionService];
  if (info.service == PASupportServiceNone) {
    return;
  }
  
  NSArray *NameArray =[(NSArray *)_alertInfo[@"Parameters"] valueForKey:@"Name"];
  NSArray *ValueArray =[(NSArray *)_alertInfo[@"Parameters"] valueForKey:@"Value"];
  NSDictionary *iParameters = [[NSDictionary alloc] initWithObjects: ValueArray forKeys: NameArray];
  info.inputParameters = iParameters;
  
  //Send Status to Server  - Started
  sRequestStatus = @"Started";
  [self UpdateStatus];
  
  
  [PAServiceInterface executeService:info.service category:info.serviceCategory info:info callback:^(BOOL success, NSError *error, id result) {
    NSNumber *exitCode = result[kPASupportServiceResponseKeyExitCode];
    sContentVersion = info.version;
    sExitCode = [NSString stringWithFormat:@"%@", exitCode];
    @try{
      if (success && exitCode && [exitCode isKindOfClass:[NSNumber class]] && exitCode.integerValue == 0) {
        if ([[NSString stringWithFormat:@"%@", _alertInfo[@"eval_option"]] isEqualToString:@"RUN_SA_MODE_TEST_AND_RUN"] == YES) {
          info.service = PASupportServiceOptimizationFix;
          [PAServiceInterface executeService:info.service category:info.serviceCategory info:info callback:^(BOOL success, NSError *error, id result) {
            NSNumber *exitCode = result[kPASupportServiceResponseKeyExitCode];
            sExitCode = [NSString stringWithFormat:@"%@", exitCode];
            
            //Send Status to Server  - Completed
            NSDate *endDate = [NSDate date];
            NSTimeInterval ExecutionTimeofSA = [endDate timeIntervalSinceDate:startTime];
            float seconds = (float)ExecutionTimeofSA;
            int milliSeconds = (int)lroundf(seconds * 1000);
            sExecTimeFoSA = [NSString stringWithFormat:@"%d",milliSeconds];
            sRequestStatus = @"Completed";
          }];
        } else {
          sExitCode = [NSString stringWithFormat:@"%@", exitCode];
          
          //Send Status to Server  - Completed
          NSDate *endDate = [NSDate date];
          NSTimeInterval ExecutionTimeofSA = [endDate timeIntervalSinceDate:startTime];
          float seconds = (float)ExecutionTimeofSA;
          int milliSeconds = (int)lroundf(seconds * 1000);
          sExecTimeFoSA = [NSString stringWithFormat:@"%d",milliSeconds];
          sRequestStatus = @"Completed";
        }
      } else {
        sExitCode = [NSString stringWithFormat:@"%@", exitCode];
        if([[NSString stringWithFormat:@"%@", sExitCode] isEqualToString:@"-1"] == YES){
          sExitCode = @"-992";
          sRequestStatus = @"Solution Not Available";
        }
        else {
          sRequestStatus = @"Completed";
        }
        
        //If the solution is returning empty as exit code, then assign the default exit code as -994.
              if ([sExitCode isEqualToString:@""]){
          sExitCode = @"-994";
        }
        
        //Send Status to Server
        NSDate *endDate = [NSDate date];
        NSTimeInterval ExecutionTimeofSA = [endDate timeIntervalSinceDate:startTime];
        float seconds = (float)ExecutionTimeofSA;
        int milliSeconds = (int)lroundf(seconds * 1000);
        sExecTimeFoSA = [NSString stringWithFormat:@"%d",milliSeconds];
      }
    }
    @catch (NSException *exception) {
          [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"Get Token error: [%@]",exception];
      sExitCode = @"-993";
      sRequestStatus = @"Failed";
    }
      [self UpdateStatus];
  }];
}

- (void)UpdateStatus{
  //Setting the values to a Dictionary
  NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
  [dict setObject:[NSString stringWithFormat:@"%@", _alertInfo[@"sprt_contentguid"]] forKey:@"REQUEST_CONTENT_GUID"];
  if (sContentVersion.length == 0) {
    [dict setObject:@"1" forKey:@"RESPONSE_CONTENT_VERSION"];
  }
  else{
    [dict setObject:[NSString stringWithFormat:@"%@", sContentVersion] forKey:@"RESPONSE_CONTENT_VERSION"];
  }
  if (sExitCode.length == 0) {
    [dict setObject:@"-994" forKey:@"RESPONSE_EXITCODE"];
  }
  else{
    [dict setObject:[NSString stringWithFormat:@"%@", sExitCode] forKey:@"RESPONSE_EXITCODE"];
  }
  if (sExecTimeFoSA.length == 0) {
    [dict setObject:@"0" forKey:@"RESPONSE_TIMETAKEN"];
  }
  else{
    [dict setObject:[NSString stringWithFormat:@"%@", sExecTimeFoSA] forKey:@"RESPONSE_TIMETAKEN"];
  }
  [dict setObject:[NSString stringWithFormat:@"%@", _alertInfo[@"transaction_id"]] forKey:@"REQUEST_TRANSACTION_ID"];
  [dict setObject:[NSString stringWithFormat:@"%@", _alertInfo[@"customer_transaction_id"]] forKey:@"CUSTOMER_TRANSACTION_ID"];
  [dict setObject:[NSHost userName] forKey:@"RESPONSE_USERNAME"];
  [dict setObject:[NSHost computerName] forKey:@"RESPONSE_MACHINE_NAME"];
  [dict setObject:[NSString stringWithFormat:@"%@", sRequestStatus] forKey:@"RESPONSE_EXECUTION_STATUS"];
  [dict setObject:@"0" forKey:@"RESPONSE_EXECUTION_STATUS_CODE"];
  
  //Converting the Dictionary to JSON string.
  NSError *error;
  NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&error];
  if(!jsonData)
  {
    NSLog(@"Error while parsing the dictionary object in UpdateStatus() : %@", error);
  }
  NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
  
  //The JSON string that will be sent to the server should be in the below sample format.
  //{
  //"REQUEST_CONTENT_GUID":"fbabc888-f59f-4e0f-91f4-67a94bd3bff3",
  //"RESPONSE_CONTENT_VERSION":"0",
  //"RESPONSE_EXITCODE":"0",
  //"RESPONSE_TIMETAKEN":"0",
  //"REQUEST_TRANSACTION_ID":"c20dcbe7-b100-47fe-9f38-c6cc494ebadd",
  //"RESPONSE_USERNAME":"admin",
  //"RESPONSE_MACHINE_NAME":"APT04-SprtMacBookPro",
  //"RESPONSE_EXECUTION_STATUS":"Request Received"
  //"RESPONSE_EXECUTION_STATUS_CODE":"0"
  //"CUSTOMER_TRANSACTION_ID":"c20dcbe7-b100-47fe-9f38-c6cc494ebadd"
  //}
  [self sendDataToServer:[NSString stringWithFormat:@"%@", _alertInfo[@"callback_url"]]:[NSString stringWithFormat:@"%@", jsonString]];
}

- (void)sendDataToServer:(NSString *) url : (NSString *) data {
  //This method is responsible to only to accept the WEB API URL to which the call has to be made and data which needs to be
  //appended to the body section of the request and make the call.
  //This is a generic method.
  NSString *post = data;
  NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
  NSString *postLength = [NSString stringWithFormat:@"%lu", [postData length]];
  NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
  [request setURL:[NSURL URLWithString:url]];     [request setHTTPMethod:@"POST"];
  [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
  [request setHTTPBody:postData];
  NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
  [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
    NSString *requestReply = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
    NSLog(@"Request reply: %@", requestReply);
  }] resume];
}

- (PASupportService)supportActionService {
  PASupportService service = PASupportServiceNone;
  if(_alertInfo[@"eval_option"] == nil)
  {
    if([[NSString stringWithFormat:@"%@", _alertInfo[@"sprt_contenttype"]] isEqualToString:@"sprt_actionlight"] == YES)
    {
      service = PASupportServiceSupportActionFix;
    }
    if([[NSString stringWithFormat:@"%@", _alertInfo[@"sprt_contenttype"]] isEqualToString:@"sprt_optimize"] == YES)
    {
      service = PASupportServiceOptimizationFix;
    }
  }
  else if([[NSString stringWithFormat:@"%@", _alertInfo[@"eval_option"]] isEqualToString:@"RUN_SA_MODE_TEST"] == YES)
  {
    if([[NSString stringWithFormat:@"%@", _alertInfo[@"sprt_contenttype"]] isEqualToString:@"sprt_actionlight"] == YES)
    {
      service = PASupportServiceSupportActionScan;
    }
    if([[NSString stringWithFormat:@"%@", _alertInfo[@"sprt_contenttype"]] isEqualToString:@"sprt_optimize"] == YES)
    {
      service = PASupportServiceOptimizationScan;
    }
  }
  else if([[NSString stringWithFormat:@"%@", _alertInfo[@"eval_option"]] isEqualToString:@"RUN_SA_MODE_TEST_AND_RUN"] == YES)
  {
    if([[NSString stringWithFormat:@"%@", _alertInfo[@"sprt_contenttype"]] isEqualToString:@"sprt_actionlight"] == YES)
    {
      service = PASupportServiceSupportActionFix;
    }
    if([[NSString stringWithFormat:@"%@", _alertInfo[@"sprt_contenttype"]] isEqualToString:@"sprt_optimize"] == YES)
    {
      service = PASupportServiceOptimizationFix;
    }
  }
  else if([[NSString stringWithFormat:@"%@", _alertInfo[@"eval_option"]] isEqualToString:@"RUN_SA_MODE_SCRIPT"] == YES)
  {
    if([[NSString stringWithFormat:@"%@", _alertInfo[@"sprt_contenttype"]] isEqualToString:@"sprt_actionlight"] == YES)
    {
      service = PASupportServiceSupportActionFix;
    }
    if([[NSString stringWithFormat:@"%@", _alertInfo[@"sprt_contenttype"]] isEqualToString:@"sprt_optimize"] == YES)
    {
      service = PASupportServiceOptimizationFix;
    }
  }
  return service;
}
//----------Support Action Related Code Conclude------------------------------------------------------------

//----------Alert Related Code Commence---------------------------------------------------------------------
- (BOOL)canHandleAlert {
  PARTRequestChannel reqChannel = _payload.requestChannelEnum;
  return (reqChannel == PARTRequestChannelAuthorCenter);
}
- (BOOL)canHandleMessageAlert {
  PARTRequestType reqType = _payload.requestTypeEnum;
  return (reqType == PARTRequestTypeRealtimeAlert);
}
- (void)handleRealTimeAlert {
  switch (_payload.requestChannelEnum) {
    case PARTRequestChannelAuthorCenter:
      [self handleAlertMessage];
      break;
    default:
      break;
  }
}

- (void)handleAlertMessage {
  if (![self canHandleMessageAlert]) {
    [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"Invalid request type"];
    return;
  }
  if ([self createAlertContent]) {
    [self displayAlertInUi];
  }
}

- (BOOL)isAlertIntendedToUser {
  NSString *profile = [self getSyncRegistryValueAtPath:kPASyncRegistryKey key:kPAProfileKey];
  BOOL isIntended = true;
  
  //#<Group ID>|<Collection Name 1>,<Collection Name 2>,……..,<Collection Name n>,
  //#<Group ID>|<Collection Name 1>,<Collection Name 2>,……..,<Collection Name n>,
  NSString *filters = _payload.filters;
  [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"Sync filters [%@]",profile];
  [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"Real time alert filters [%@]",filters];
  
  //Parse with # to get group name and filters for the group
  NSArray *groupList = [filters componentsSeparatedByString:@"#"];
  id groupItem;
  for (groupItem in groupList) {
    if (![groupItem isEmptyOrHasOnlyWhiteSpaces]) {
      //Parse with | to seperate groupname from filterlist
      NSArray *groupItemList = [groupItem componentsSeparatedByString:@"|"];
      NSUInteger elements = [groupItemList count];
      if(elements > 1) {
        NSString *groupFilters = groupItemList[1];
        // if filter is empty; then alert is for all
        if (![groupFilters isEmptyOrHasOnlyWhiteSpaces]) {
          // check if filter list has user's computer name?
          NSArray *filterList = [groupFilters componentsSeparatedByString:@","];
          NSArray *profileList = [profile componentsSeparatedByString:@","];
          //If any group returns false it is not intented for user
          isIntended = isIntended && [NSArray doesArray:filterList hasCommonObjectsIn:profileList];
        }
      }
    }
  }
  return isIntended;
}

- (NSString *)getSyncRegistryValueAtPath:(NSString *)path
                                     key:(NSString *)key {
  NSString *plistFilePath = [_fsManager hkcuRegistryPlistPath];
  NSString *keyPath = [_fsManager stringByExpandingPath:path];
  __block NSString * regValueAtPath = nil;
  PAPlistLockManager *lckMgr = [PAPlistLockManager sharedInstance];
  [lckMgr runWithReadLock:plistFilePath codeBlock:^{
    PARegistryReader* reader = [[PARegistryReader alloc] initWithPlistAtPath:plistFilePath];
    regValueAtPath = [reader regValueForPath:keyPath withKey:key];
  }];
  return regValueAtPath;
}

- (NSDate *)getLocalDateTimeFromUTC:(NSString *)strDate {
  NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
  [formatter setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
  [formatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
  NSDate *date = [formatter dateFromString:strDate];
  
  //[[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"Real time alert new expiry date [%@]",[formatter stringFromDate:date]];
  return date;
}

- (BOOL)isAlertExpired {
  BOOL isExpired = false;
  NSString *alertExpiryDateStr = _payload.alertDisplayTimeEnd;
  
  [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"Real time alert expiry date [%@]",alertExpiryDateStr];
  if (![alertExpiryDateStr isEmptyOrHasOnlyWhiteSpaces]) {
    NSDate *alertExpiryDate = [self getLocalDateTimeFromUTC : alertExpiryDateStr];
    
    // check if expiry date is past date
    NSDate *currentDate = [NSDate systemDate];
    
    // is past date; then alert is expired
    if ([currentDate compare:alertExpiryDate] == NSOrderedDescending) {
      isExpired = true;
    }
  }
  
  return isExpired;
}

- (void)alertDelivered {
  __block PANetworkServiceManager *serviceManager = [[PANetworkServiceManager alloc] init];
  NSUInteger isIntended = _isAlertIntendedToTarget ? 1 : 0;
  PARealTimeAlertRequest *request = [[PARealTimeAlertRequest alloc]initWithRequestType:PARealTimeAlertRequestTypeMessageDelivered guid:_payload.requestGuid clientName:[NSHost computerName] isIntended:@(isIntended)];
  [_networkServiceManagers addObject:serviceManager];
  [serviceManager realTimeAlertDelivered:request callback:^(NSError *error, PABaseResponse *response) {
    if (response.statusCode == kPAResponseSuccess) {
      [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"Real Time Alert delivered request success"];
    } else {
      [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"Real Time Alert delivered request failed; Error: %@",error];
    }
    [_networkServiceManagers removeObject:serviceManager];
  }];
}

- (BOOL)createAlertContent {
  return [_contentCreator createContentFromPayload:_payload];
}

- (void)displayAlertInUi {
  // notify notification tray app
  NSString *alertCmd = kPAAlertTypeRealTimeAlert;
  NSString *arg1 = _payload.contentGuid;
  NSString *arg2 = _payload.contentVersion;
  
  NSString *command = [NSString stringWithFormat:@"%@ %@ %@ %@",kPALSCommandAlertTray, alertCmd, arg1, arg2];
  BOOL isSuccess = [PALaunchServiceManager runCommand:command];
  if (isSuccess) {
    [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"Real Time Alert:: Successfullly notified Notification Tray"];
  }
}

- (BOOL)isAlertFromTrustedSource {
  /// TODO: enable certificate validation
  return true;
  return [PACertificateUtility doesSignatureMatchWithSyncCertificate:_payload.certificateSignature];
}

- (void)supportActionAcknowledgementWithExitCode:(NSNumber *)exitCode {
  __block PANetworkServiceManager *serviceManager = [[PANetworkServiceManager alloc] init];
  PARealTimeAlertRequest *request = [[PARealTimeAlertRequest alloc]initWithRequestType:PARealTimeAlertRequestTypeMessageAcknowledgement guid:[NSString stringWithFormat:@"%@", _alertInfo[@"RequestGuid"]] clientName:[NSHost computerName] isIntended:nil exitCode:exitCode];
  [_networkServiceManagers addObject:serviceManager];
  [serviceManager realTimeAlertAcknowledgement:request callback:^(NSError *error, PABaseResponse *response) {
    if (response.statusCode == kPAResponseSuccess) {
      [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"Real Time Support Action acknowledgement request success"];
    } else {
      [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"Real Time Support Action acknowledgement request failed; Error: %@",error];
    }
    [_networkServiceManagers removeObject:serviceManager];
  }];
}
//----------Alert Related Code Conclude---------------------------------------------------------------------
@end
