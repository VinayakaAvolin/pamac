//
//  PARealTimeAlertContentCreator.m
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <PAFileSystemService/PAFileSystemService.h>
#import <PAXmlService/PAXmlService.h>

#import "PARealTimeAlertContentCreator.h"

NSString *const kPARTJsFileName = @"content.js";
NSString *const kPARTHtmFileName = @"Alert Content.htm";
NSString *const kPARTXmlFileExtension = @"xml";

NSString *const kPARTSprt = @"sprt";
NSString *const kPARTContentSet = @"content-set";
NSString *const kPARTContent = @"content";
NSString *const kPARTPropertySet = @"property-set";
NSString *const kPARTProperty = @"property";
NSString *const kPARTPropertyName = @"name";
NSString *const kPARTPropertyType = @"type";
NSString *const kPARTFieldSet = @"field-set";
NSString *const kPARTField = @"field";
NSString *const kPARTFieldValue = @"value";

@interface PARealTimeAlertContentCreator (){
  PARealTimeAlertPayloadInfo *_payload;
  PAFileSystemManager *_fsManager;
  NSString *_alertFolderName;
  NSString *_alertFolderPath;
  NSString *_htmlFilePath;
}

@end

@implementation PARealTimeAlertContentCreator

- (instancetype)init
{
  self = [super init];
  if (self) {
    _fsManager = [[PAFileSystemManager alloc] init];
  }
  return self;
}

- (BOOL)createContentFromPayload:(PARealTimeAlertPayloadInfo *)payload {
  if (!payload) {
    return false;
  }
  
  _payload = payload;
  _alertFolderName = [self alertFolderName];
  _alertFolderPath = [self alertFolderPath];
  if (!_alertFolderName || !_alertFolderPath) {
    return false;
  }
  BOOL contentCreated = ([self createJs] && [self createHtm] && [self createXml]);
  if (!contentCreated) {
    [_fsManager deleteDirectory:_alertFolderPath];
  }
  return contentCreated;
}


- (NSString *)htmlFilePath {
  return _htmlFilePath;
}

#pragma mark - Private methods

- (BOOL)createHtm {
  BOOL isCreated = false;
  NSString *filePath = _alertFolderPath;
  filePath = [filePath stringByAppendingPathComponent:kPARTHtmFileName];
  NSString *alertContent = _payload.alertContentString;
  if (alertContent) {
    NSData *fileData = [alertContent dataUsingEncoding:NSUTF8StringEncoding];
    if ([_fsManager createFileAtPath:filePath attributes:nil error:nil]) {
      isCreated = [_fsManager writeToFileAtPath:filePath data:fileData];
      _htmlFilePath = filePath;
    }
  }
  
  return isCreated;
}
- (BOOL)createJs {
  BOOL isCreated = false;
  NSString *filePath = _alertFolderPath;
  filePath = [filePath stringByAppendingPathComponent:kPARTJsFileName];
  NSString *alertContent = _payload.alertContentDescription;
  if (alertContent) {
    NSData *fileData = [alertContent dataUsingEncoding:NSUTF8StringEncoding];
    if ([_fsManager createFileAtPath:filePath attributes:nil error:nil]) {
      isCreated = [_fsManager writeToFileAtPath:filePath data:fileData];
    }
  }
  
  return isCreated;
}

- (BOOL)createXml {
  BOOL isCreated = false;
  NSString *filePath = _alertFolderPath;
  filePath = [filePath stringByAppendingPathComponent:_alertFolderName];
  filePath = [filePath stringByAppendingPathExtension:kPARTXmlFileExtension];
  
  PAXmlFileProperties *properties = [[PAXmlFileProperties alloc]initWithVersion:kPAXmlVersionValue encoding:kPAXmlUtf8Encoding standalone:@(false) root:kPARTSprt];
  
  PAXmlServiceManager *manager = [[PAXmlServiceManager alloc]init];
  isCreated = [manager createXmlAtPath:filePath withProperties:properties];
  if (isCreated) {
    PAXmlWriter *xmlWriter = [[PAXmlWriter alloc]initWithXmlAtPath:filePath];
    if (!xmlWriter) {
      return false;
    }
    
    /// add property set
    NSString *propertyParentXPath = [NSString stringWithFormat:@"%@/%@/%@", kPARTContentSet, kPARTContent, kPARTPropertySet];
    BOOL propertyRootCreated = ([xmlWriter addXmlElementWithName:kPARTContentSet value:nil attributes:nil parentXPath:kPARTSprt] &&
                                [xmlWriter addXmlElementWithName:kPARTContent value:nil attributes:nil parentXPath:kPARTContentSet] &&
                                [xmlWriter addXmlElementWithName:kPARTPropertySet value:nil attributes:nil parentXPath:[NSString stringWithFormat:@"%@/%@", kPARTContentSet,kPARTContent]]);
    if (propertyRootCreated) {
      if (_payload.rowGuid) {
        NSArray *attributes = [self attributesForProperty:PARTPropertyRowGuid];
        if (attributes) {
          [xmlWriter addXmlElementWithName:kPARTProperty value:_payload.rowGuid attributes:attributes parentXPath:propertyParentXPath];
        }
      }
      if (_payload.contentGuid) {
        NSArray *attributes = [self attributesForProperty:PARTPropertyContentGuid];
        if (attributes) {
          [xmlWriter addXmlElementWithName:kPARTProperty value:_payload.contentGuid attributes:attributes parentXPath:propertyParentXPath];
        }
      }
      if (_payload.requestGuid) {
        NSArray *attributes = [self attributesForProperty:PARTPropertyRequestGuid];
        if (attributes) {
          [xmlWriter addXmlElementWithName:kPARTProperty value:_payload.requestGuid attributes:attributes parentXPath:propertyParentXPath];
        }
      }
      if (_payload.contentVersion) {
        NSArray *attributes = [self attributesForProperty:PARTPropertyContentVersion];
        if (attributes) {
          [xmlWriter addXmlElementWithName:kPARTProperty value:_payload.contentVersion attributes:attributes parentXPath:propertyParentXPath];
        }
      }
      if (_payload.contentType) {
        NSArray *attributes = [self attributesForProperty:PARTPropertyContentType];
        if (attributes) {
          [xmlWriter addXmlElementWithName:kPARTProperty value:_payload.contentType attributes:attributes parentXPath:propertyParentXPath];
        }
      }
      if (_payload.folderGuid) {
        NSArray *attributes = [self attributesForProperty:PARTPropertyFolderGuid];
        if (attributes) {
          [xmlWriter addXmlElementWithName:kPARTProperty value:_payload.folderGuid attributes:attributes parentXPath:propertyParentXPath];
        }
      }
      if (_payload.contentTitle) {
        NSArray *attributes = [self attributesForProperty:PARTPropertyContentTitle];
        if (attributes) {
          [xmlWriter addXmlElementWithName:kPARTProperty value:_payload.contentTitle attributes:attributes parentXPath:propertyParentXPath];
        }
      }
      if (_payload.keywords) {
        NSArray *attributes = [self attributesForProperty:PARTPropertyKeywords];
        if (attributes) {
          [xmlWriter addXmlElementWithName:kPARTProperty value:_payload.keywords attributes:attributes parentXPath:propertyParentXPath];
        }
      }
      if (_payload.comments) {
        NSArray *attributes = [self attributesForProperty:PARTPropertyComments];
        if (attributes) {
          [xmlWriter addXmlElementWithName:kPARTProperty value:_payload.comments attributes:attributes parentXPath:propertyParentXPath];
        }
      }
      if (_payload.contentDescription) {
        NSArray *attributes = [self attributesForProperty:PARTPropertyContentDescription];
        if (attributes) {
          [xmlWriter addXmlElementWithName:kPARTProperty value:_payload.contentDescription attributes:attributes parentXPath:propertyParentXPath];
        }
      }
      if (_payload.status) {
        NSArray *attributes = [self attributesForProperty:PARTPropertyStatus];
        if (attributes) {
          [xmlWriter addXmlElementWithName:kPARTProperty value:_payload.status attributes:attributes parentXPath:propertyParentXPath];
        }
      }
      if (_payload.lastModifiedDateStr) {
        NSArray *attributes = [self attributesForProperty:PARTPropertyLastModified];
        if (attributes) {
          [xmlWriter addXmlElementWithName:kPARTProperty value:_payload.lastModifiedDateStr attributes:attributes parentXPath:propertyParentXPath];
        }
      }
      if (_payload.owner) {
        NSArray *attributes = [self attributesForProperty:PARTPropertyOwner];
        if (attributes) {
          [xmlWriter addXmlElementWithName:kPARTProperty value:_payload.owner attributes:attributes parentXPath:propertyParentXPath];
        }
      }
      if (_payload.author) {
        NSArray *attributes = [self attributesForProperty:PARTPropertyAuthor];
        if (attributes) {
          [xmlWriter addXmlElementWithName:kPARTProperty value:_payload.author attributes:attributes parentXPath:propertyParentXPath];
        }
      }
      if (_payload.expirationType) {
        NSArray *attributes = [self attributesForProperty:PARTPropertyExpirationType];
        if (attributes) {
          [xmlWriter addXmlElementWithName:kPARTProperty value:_payload.expirationType attributes:attributes parentXPath:propertyParentXPath];
        }
      }
      if (_payload.activateDateStr) {
        NSArray *attributes = [self attributesForProperty:PARTPropertyActiveDate];
        if (attributes) {
          [xmlWriter addXmlElementWithName:kPARTProperty value:_payload.activateDateStr attributes:attributes parentXPath:propertyParentXPath];
        }
      }
      if (_payload.expiryDateStr) {
        NSArray *attributes = [self attributesForProperty:PARTPropertyExpireDate];
        if (attributes) {
          [xmlWriter addXmlElementWithName:kPARTProperty value:_payload.expiryDateStr attributes:attributes parentXPath:propertyParentXPath];
        }
      }
      if (_payload.language) {
        NSArray *attributes = [self attributesForProperty:PARTPropertyLanguage];
        if (attributes) {
          [xmlWriter addXmlElementWithName:kPARTProperty value:_payload.language attributes:attributes parentXPath:propertyParentXPath];
        }
      }
      if (_payload.retired) {
        NSArray *attributes = [self attributesForProperty:PARTPropertyRetired];
        if (attributes) {
          [xmlWriter addXmlElementWithName:kPARTProperty value:@(_payload.retired) attributes:attributes parentXPath:propertyParentXPath];
        }
      }
      if (_payload.referenceGuid) {
        NSArray *attributes = [self attributesForProperty:PARTPropertyReferenceGuid];
        if (attributes) {
          [xmlWriter addXmlElementWithName:kPARTProperty value:_payload.referenceGuid attributes:attributes parentXPath:propertyParentXPath];
        }
      }
      if (_payload.referenceVersion) {
        NSArray *attributes = [self attributesForProperty:PARTPropertyReferenceVersion];
        if (attributes) {
          [xmlWriter addXmlElementWithName:kPARTProperty value:_payload.referenceVersion attributes:attributes parentXPath:propertyParentXPath];
        }
      }
      if (_payload.attributes) {
        NSArray *attributes = [self attributesForProperty:PARTPropertyAttributes];
        if (attributes) {
          [xmlWriter addXmlElementWithName:kPARTProperty value:_payload.attributes attributes:attributes parentXPath:propertyParentXPath];
        }
      }
      if (_payload.reviewDateStr) {
        NSArray *attributes = [self attributesForProperty:PARTPropertyReviewDate];
        if (attributes) {
          [xmlWriter addXmlElementWithName:kPARTProperty value:_payload.reviewDateStr attributes:attributes parentXPath:propertyParentXPath];
        }
      }
      if (_payload.friendlyId) {
        NSArray *attributes = [self attributesForProperty:PARTPropertyFriendlyId];
        if (attributes) {
          [xmlWriter addXmlElementWithName:kPARTProperty value:_payload.friendlyId attributes:attributes parentXPath:propertyParentXPath];
        }
      }
      if (_payload.categories) {
        NSArray *attributes = [self attributesForProperty:PARTPropertyCategories];
        if (attributes) {
          [xmlWriter addXmlElementWithName:kPARTProperty value:_payload.categories attributes:attributes parentXPath:propertyParentXPath];
        }
      }
    }
    
    /// add field set
    NSString *fieldParentXPath = [NSString stringWithFormat:@"%@/%@/%@", kPARTContentSet, kPARTContent, kPARTFieldSet];
    NSString *fieldValueParentXPath = [NSString stringWithFormat:@"%@/%@/%@/%@", kPARTContentSet, kPARTContent, kPARTFieldSet, kPARTField];
    BOOL fieldsetRootCreated = ([xmlWriter addXmlElementWithName:kPARTFieldSet value:nil attributes:nil parentXPath:[NSString stringWithFormat:@"%@/%@", kPARTContentSet,kPARTContent]]);
    
    if (fieldsetRootCreated && _payload.fields.count) {
      for (PARealTimeAlertPayloadFieldInfo *field in _payload.fields) {
        NSArray *attributes = [self attributesForField:field];
        if (attributes) {
          if ([xmlWriter addXmlElementWithName:kPARTField value:nil attributes:attributes parentXPath:fieldParentXPath]) {
            if (field.charValue && ![field.charValue  isEqualTo:[NSNull null]]) {
              [xmlWriter addXmlElementWithName:kPARTFieldValue value:field.charValue  attributes:[self attributesForFieldValue:PARTFieldValueNameChar] parentXPath:fieldValueParentXPath withParentAttributeName:kPARTPropertyName andValue:field.fieldName];
            }
            if (field.intValue && ![field.intValue  isEqualTo:[NSNull null]]) {
              [xmlWriter addXmlElementWithName:kPARTFieldValue value:field.intValue  attributes:[self attributesForFieldValue:PARTFieldValueNameInt] parentXPath:fieldValueParentXPath withParentAttributeName:kPARTPropertyName andValue:field.fieldName];
            }
            if (field.clobValue && ![field.clobValue  isEqualTo:[NSNull null]]) {
              [xmlWriter addXmlElementWithName:kPARTFieldValue value:field.clobValue  attributes:[self attributesForFieldValue:PARTFieldValueNameClob] parentXPath:fieldValueParentXPath withParentAttributeName:kPARTPropertyName andValue:field.fieldName];
            }
          }
        }
      }
    }
    
  }
  
  return isCreated;
}

- (NSArray *)attributesForFieldValue:(PARTFieldValueName)fieldValueEnum {
  NSString *name = [_payload fieldValueNameFor:fieldValueEnum];
  PARTPropertyType typeEnum = PARTPropertyTypeUnknown;
  switch (fieldValueEnum) {
    case PARTFieldValueNameChar:
      typeEnum = PARTPropertyTypeString;
      break;
    case PARTFieldValueNameInt:
      typeEnum = PARTPropertyTypeNumber;
      break;
    case PARTFieldValueNameClob:
      typeEnum = PARTPropertyTypeCharacter;
      break;
    default:
      typeEnum = PARTPropertyTypeString;
      break;
  }
  NSString *type = [_payload propertyTypeFor:typeEnum];
  PAXmlAttribute *nameAttribute = [[PAXmlAttribute alloc] initWithName:kPARTPropertyName value:name];
  PAXmlAttribute *typeAttribute = [[PAXmlAttribute alloc] initWithName:kPARTPropertyType value:type];
  NSMutableArray *attributes = [[NSMutableArray alloc] init];
  if (nameAttribute) {
    [attributes addObject:nameAttribute];
  }
  if (typeAttribute) {
    [attributes addObject:typeAttribute];
  }
  if (attributes.count) {
    return [NSArray arrayWithArray:attributes];
  }
  return nil;
}
- (NSArray *)attributesForField:(PARealTimeAlertPayloadFieldInfo *)fieldInfo {
  NSString *name = fieldInfo.fieldName;
  
  PAXmlAttribute *nameAttribute = [[PAXmlAttribute alloc] initWithName:kPARTPropertyName value:name];
  NSMutableArray *attributes = [[NSMutableArray alloc] init];
  if (nameAttribute) {
    [attributes addObject:nameAttribute];
  }
  if (attributes.count) {
    return [NSArray arrayWithArray:attributes];
  }
  return nil;
}


- (NSArray *)attributesForProperty:(PARTPropertyName)propertyEnum {
  NSString *name = [_payload propertyNameFor:propertyEnum];
  PARTPropertyType typeEnum = PARTPropertyTypeUnknown;
  switch (propertyEnum) {
    case PARTPropertyContentVersion:
    case PARTPropertyRetired:
    case PARTPropertyReferenceVersion:
      typeEnum = PARTPropertyTypeInteger;
      break;
    case PARTPropertyLastModified:
    case PARTPropertyReviewDate:
      typeEnum = PARTPropertyTypeDate;
      break;
    case PARTPropertyActiveDate:
    case PARTPropertyExpireDate:
      typeEnum = PARTPropertyTypeNumber;
      break;
    default:
      typeEnum = PARTPropertyTypeString;
      break;
  }
  NSString *type = [_payload propertyTypeFor:typeEnum];
  PAXmlAttribute *nameAttribute = [[PAXmlAttribute alloc] initWithName:kPARTPropertyName value:name];
  PAXmlAttribute *typeAttribute = [[PAXmlAttribute alloc] initWithName:kPARTPropertyType value:type];
  NSMutableArray *attributes = [[NSMutableArray alloc] init];
  if (nameAttribute) {
    [attributes addObject:nameAttribute];
  }
  if (typeAttribute) {
    [attributes addObject:typeAttribute];
  }
  if (attributes.count) {
    return [NSArray arrayWithArray:attributes];
  }
  return nil;
}

- (NSString *)alertFolderPath {
  NSString *targetFolderPath = [_fsManager realTimeAlertFolderPath];
  targetFolderPath = [targetFolderPath stringByAppendingPathComponent:_alertFolderName];
  BOOL isDirPresent = [_fsManager doesDirectoryExist:targetFolderPath];
  if (isDirPresent) {
    [_fsManager deleteDirectory:targetFolderPath];
  }
  isDirPresent = [_fsManager createDirectoryAtPath:targetFolderPath withIntermediateDirectories:YES attributes:nil error:nil];
  
  if (isDirPresent) {
    return targetFolderPath;
  }
  return nil;
}

- (NSString *)alertFolderName {
  NSString *contentGuid = _payload.contentGuid;
  NSString *contentVersion = _payload.contentVersion;
  
  if (!contentGuid || !contentVersion) {
    return nil;
  }
  return [NSString stringWithFormat:@"%@.%@",contentGuid,contentVersion];
}
@end
