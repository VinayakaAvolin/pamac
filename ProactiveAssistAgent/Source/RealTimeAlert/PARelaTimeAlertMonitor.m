//
//  PARelaTimeAlertMonitor.m
//  ProactiveAssistAgent
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <PACoreServices/PACoreServices.h>
#import "PARelaTimeAlertMonitor.h"
#import "PARealTimeAlertHandler.h"
#import "PARealTimeAlertPruner.h"
#import "PAXmppDetailsProvider.h"

NSTimeInterval const kRTAPruneTimerInterval = 1 * 60 * 60; /// 1 hour time interval

@interface PARelaTimeAlertMonitor () <PAXmppServiceManagerDelegate> {
  BOOL _isRealTimeAlertMonitorOn;
  NSOperationQueue *_operationQueue;
  NSTimer *_alertPruneTimer;
  PARealTimeAlertPruner *_alertPruner;
  PAXmppDetailsProvider *_xmppDetailProvider;
}

@end

@implementation PARelaTimeAlertMonitor
- (instancetype)init
{
  self = [super init];
  if (self) {
    _operationQueue = [[NSOperationQueue alloc]init];
  }
  return self;
}

- (void)dealloc {
  [self removeAlertPruneTimer];
}

- (void)addAlertPruneTimer {
  dispatch_async(dispatch_get_main_queue(), ^{
    _alertPruneTimer =  [NSTimer scheduledTimerWithTimeInterval:kRTAPruneTimerInterval target:self selector:@selector(pruneTimerFired:) userInfo:nil repeats:YES];
    
  });
}

- (void)removeAlertPruneTimer {
  dispatch_async(dispatch_get_main_queue(), ^{
    if (_alertPruneTimer.isValid) {
      [_alertPruneTimer invalidate];
    }
  });
  
}

- (void)pruneTimerFired:(NSTimer *)timer {
  [self pruneAlerts];
}

- (void)start {
  [_operationQueue cancelAllOperations];
  [self pruneAlerts];
  [self removeAlertPruneTimer];
  NSInvocationOperation *startOperation = [[NSInvocationOperation alloc]
                                           initWithTarget:self
                                           selector:@selector(startOperation)
                                           object:nil];
  [_operationQueue addOperation:startOperation];
}
- (void)stop {
  [_operationQueue cancelAllOperations];
  _isRealTimeAlertMonitorOn = false;
  [[PAXmppServiceManager sharedManager] disconnectFromXmpp];
}

#pragma mark - Private Methods

- (void)startOperation {
  if (!_isRealTimeAlertMonitorOn) {
    [self getXmppConnectDetails];
  }
  _isRealTimeAlertMonitorOn = true;
}
- (void)startMonitoringRealTimeAlerts {
  PAXmppServiceManager *xmppManager = [PAXmppServiceManager sharedManager];
  _isRealTimeAlertMonitorOn = [xmppManager connectToXmppWithConfiguration:_xmppDetailProvider.xmppConnectDetail];
  if (_isRealTimeAlertMonitorOn) {
    xmppManager.delegate = self;
    [self addAlertPruneTimer];
  }
  [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"Real time alert monitor is on? [%ld]",_isRealTimeAlertMonitorOn];
}


- (void)getXmppConnectDetails {
  _xmppDetailProvider = [[PAXmppDetailsProvider alloc] init];
  
  [_xmppDetailProvider getXmppConnectDetailsWithCompletion:^(BOOL success, PAXmppConnectInfo *xmppInfo) {
    if (success && xmppInfo) {
      [self startMonitoringRealTimeAlerts];
    }
  }];
}


#pragma mark - PAXmppServiceManagerDelegate Methods

- (void)didReceiveRealTimeAlert:(NSDictionary *)info {
  PARealTimeAlertHandler *alertHandler = [[PARealTimeAlertHandler alloc] init];
  NSInvocationOperation *alertOperation = [[NSInvocationOperation alloc]
                                           initWithTarget:alertHandler
                                           selector:@selector(handleAlertWithInfo:)
                                           object:info];
  [_operationQueue addOperation:alertOperation];
  [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"Did receive real time alert with info %@",info];
}

- (void)pruneAlerts {
  if (!_alertPruner) {
    _alertPruner = [[PARealTimeAlertPruner alloc] init];
  }
  
  if (!_alertPruner.isPruning) {
    NSInvocationOperation *pruneOperation = [[NSInvocationOperation alloc]
                                             initWithTarget:_alertPruner
                                             selector:@selector(start)
                                             object:nil];
    [_operationQueue addOperation:pruneOperation];
  }
}

@end
