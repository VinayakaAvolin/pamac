//
//  PARealTimeAlertContentCreator.h
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PARealTimeAlertPayloadInfo.h"

@interface PARealTimeAlertContentCreator : NSObject

@property (nonatomic, readonly) NSString *htmlFilePath;

- (BOOL)createContentFromPayload:(PARealTimeAlertPayloadInfo *)payload;

@end
