//
//  PARealTimeAlertPruner.h
//  ProactiveAssistAgent
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PARealTimeAlertPruner : NSObject

- (void)start;
- (void)stop;
- (BOOL)isPruning;

@end
