//
//  PARealTimeAlertPayloadFieldInfo.m
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PARealTimeAlertPayloadFieldInfo.h"

NSString *const kPARTFieldName = @"FieldName";
NSString *const kPARTCharValue = @"sccf_field_value_char";
NSString *const kPARTIntValue = @"sccf_field_value_int";
NSString *const kPARTClobValue = @"sccfc_field_value_clob";

@implementation PARealTimeAlertPayloadFieldInfo

- (instancetype)initWithFieldPayload:(NSDictionary *)fieldDict {
  self = [super init];
  if (self) {
    _fieldName = fieldDict[kPARTFieldName];
    _charValue = fieldDict[kPARTCharValue];
    _intValue = fieldDict[kPARTIntValue];
    _clobValue = fieldDict[kPARTClobValue];
  }
  return self;
}

@end
