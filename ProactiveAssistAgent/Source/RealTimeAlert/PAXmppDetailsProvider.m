//
//  PAXmppDetailsProvider.m
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <PACoreServices/PACoreServices.h>
#import "PAXmppDetailsProvider.h"
#import "PARealTimeAlertDetailRequest.h"
#import "NSString+Additions.h"
#import "PANetworkServiceManager+RealTimeAlert.h"
#import "NSHost+Additions.h"
#import "PAGetTokenResponse.h"
#import "PAGetServerResponse.h"
#import "PAGetPasswordResponse.h"

#import "PAAESDecrypt.hpp"

NSString *const AES_SALT = @"C81B7A36-132B-479E-8D5A-601C1F82C562";

@interface PAXmppDetailsProvider () {
  PAXmppDetailsProviderCompletionBlock _completionBlock;
  NSMutableArray *_networkServiceManagers;
  NSString *_authorizationKey;
  NSString *_encryptedPassword;
  PAGetServerResponse *_serverDetailResponse;
  PAGetPasswordResponse *_passwordResponse;
}
@end

@implementation PAXmppDetailsProvider

- (instancetype)init
{
  self = [super init];
  if (self) {
    _networkServiceManagers = [[NSMutableArray alloc] init];
  }
  return self;
}
- (void)getXmppConnectDetailsWithCompletion:(PAXmppDetailsProviderCompletionBlock)completionBlock {
  if (!completionBlock) {
    return;
  }
  
  _completionBlock = completionBlock;
  [self getToken];
}

- (void)getToken {
  PARealTimeAlertConfiguration *xmppConfig = [[PAConfigurationManager appConfigurations] realtimeAlertconfigurations];
  
  if ([xmppConfig.userName isEmptyOrHasOnlyWhiteSpaces] ||
      [xmppConfig.password isEmptyOrHasOnlyWhiteSpaces] ||
      [xmppConfig.grantType isEmptyOrHasOnlyWhiteSpaces]) {
    return;
  }
  
  /// decrypt encypted token password;
  NSString *decryptedPassword = [self AESDecryptPassword: xmppConfig.password : AES_SALT];
  
  __block PANetworkServiceManager *serviceManager = [[PANetworkServiceManager alloc] init];
  
  PARealTimeAlertDetailRequest *request;
  @try {
    request = [[PARealTimeAlertDetailRequest alloc] initWithRequestType:PARealTimeAlertDetailRequestTypeGetToken userName:xmppConfig.userName password:decryptedPassword grantType:xmppConfig.grantType authorization:nil clientName:nil];
  }
  @catch (NSException *exception) {
    [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"Get Token error: [%@]",exception];
  }
  
  [_networkServiceManagers addObject:serviceManager];
  [serviceManager getToken:request callback:^(NSError *error, PABaseResponse *response) {
    if (response.statusCode == kPAResponseSuccess) {
      PAGetTokenResponse *detailResponse = [[PAGetTokenResponse alloc] initWithHeaders:response.allHeaderFields statusCode:response.statusCode outputData:response.responseData];
      _authorizationKey = detailResponse.authorizationKey;
      [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"Real Time Get token request success"];
      [self getPassword];
    } else {
      [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"Real Time Alert Get token request failed with Error: %@",error];
    }
    [_networkServiceManagers removeObject:serviceManager];
  }];
}

- (void)getPassword {
  if ([_authorizationKey isEmptyOrHasOnlyWhiteSpaces]) {
    return;
  }
  __block PANetworkServiceManager *serviceManager = [[PANetworkServiceManager alloc] init];
  PARealTimeAlertDetailRequest *request = [[PARealTimeAlertDetailRequest alloc] initWithRequestType:PARealTimeAlertDetailRequestTypeGetPassword userName:nil password:nil grantType:nil authorization:_authorizationKey clientName:[NSHost computerName]];
  [_networkServiceManagers addObject:serviceManager];
  [serviceManager getToken:request callback:^(NSError *error, PABaseResponse *response) {
    if (response.statusCode == kPAResponseSuccess) {
      _passwordResponse = [[PAGetPasswordResponse alloc] initWithHeaders:response.allHeaderFields statusCode:response.statusCode outputData:response.responseData];
      [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"Real Time Get Password request success"];
      [self getServerDetail];
    } else {
      [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"Real Time Alert Get Password request failed with Error: %@",error];
    }
    [_networkServiceManagers removeObject:serviceManager];
  }];
}

- (void)getServerDetail  {
  if ([_authorizationKey isEmptyOrHasOnlyWhiteSpaces]) {
    return;
  }
  __block PANetworkServiceManager *serviceManager = [[PANetworkServiceManager alloc] init];
  PARealTimeAlertDetailRequest *request = [[PARealTimeAlertDetailRequest alloc] initWithRequestType:PARealTimeAlertDetailRequestTypeGetServer userName:nil password:nil grantType:nil authorization:_authorizationKey clientName:nil];
  [_networkServiceManagers addObject:serviceManager];
  [serviceManager getToken:request callback:^(NSError *error, PABaseResponse *response) {
    if (response.statusCode == kPAResponseSuccess) {
      [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"Real Time Get Server details request success"];
      _serverDetailResponse = [[PAGetServerResponse alloc] initWithHeaders:response.allHeaderFields statusCode:response.statusCode outputData:response.responseData];
      [self constructXmppConnectInfo];
    } else {
      [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"Real Time Alert Get Server request failed with Error: %@",error];
    }
    [_networkServiceManagers removeObject:serviceManager];
  }];
}

- (void)constructXmppConnectInfo {
  if ([_encryptedPassword isEmptyOrHasOnlyWhiteSpaces] ||
      !_serverDetailResponse.xmppServerInfo) {
    _completionBlock(false, nil);
  }
  
  PAXmppConnectInfo *serverInfoWithoutUserCredentials = _serverDetailResponse.xmppServerInfo;
  
  /// decrypt encypted salt;
  PARealTimeAlertConfiguration *xmppConfig = [[PAConfigurationManager appConfigurations] realtimeAlertconfigurations];
  NSString *decryptedSalt = [self AESDecryptPassword: xmppConfig.key : AES_SALT];
  
  // decrypt password;
  NSString *decryptedPassword = [self AESDecryptPassword: _passwordResponse.password : decryptedSalt];
  NSString *userName = [NSHost computerName];
  
  if (userName && decryptedPassword) {
    _xmppConnectDetail = [[PAXmppConnectInfo alloc] initWithDomain:serverInfoWithoutUserCredentials.domainName host:serverInfoWithoutUserCredentials.host port:serverInfoWithoutUserCredentials.port licenceKey:serverInfoWithoutUserCredentials.licenceKey userName:[NSHost computerName] password:decryptedPassword];
  }
  
  _completionBlock((_xmppConnectDetail != nil), _xmppConnectDetail);
}

//Changed base64 encoding to AES encryption
- (NSString *)AESDecryptPassword : (NSString *)encryptedText :(NSString *)Salt {
  NSString *utf8Password = nil;
  @try {
    NSString *base64Password = encryptedText;
    if (base64Password) {
      const char* array = [base64Password UTF8String];
      char *decryptedPassword = AESDecrypt(array, [Salt cStringUsingEncoding:NSASCIIStringEncoding]);
      
      utf8Password = [NSString stringWithUTF8String:decryptedPassword];
      free(decryptedPassword);
    }
  }
  @catch (NSException *exception) {
    [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"Real Time Alert Error while connection: [%@]",exception];
  }
  return utf8Password;
}
@end
