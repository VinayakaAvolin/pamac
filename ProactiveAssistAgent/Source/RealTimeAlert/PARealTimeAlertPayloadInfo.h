//
//  PARealTimeAlertPayloadInfo.h
//  ProactiveAssistAgent
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PARealTimeAlertPayloadFieldInfo.h"

typedef enum {
  PARTPropertyNone,
  PARTPropertyRowGuid,
  PARTPropertyContentGuid,
  PARTPropertyRequestGuid,
  PARTPropertyContentVersion,
  PARTPropertyContentType,
  PARTPropertyFolderGuid,
  PARTPropertyContentTitle,
  PARTPropertyKeywords,
  PARTPropertyComments,
  PARTPropertyContentDescription,
  PARTPropertyStatus,
  PARTPropertyLastModified,
  PARTPropertyOwner,
  PARTPropertyAuthor,
  PARTPropertyExpirationType,
  PARTPropertyActiveDate,
  PARTPropertyExpireDate,
  PARTPropertyLanguage,
  PARTPropertyRetired,
  PARTPropertyReferenceGuid,
  PARTPropertyReferenceVersion,
  PARTPropertyAttributes,
  PARTPropertyReviewDate,
  PARTPropertyFriendlyId,
  PARTPropertyCategories
} PARTPropertyName;
/// order of elements in the kPAPlistOutputParserCommandArray should match the PAPlistOutputParserCommand enum order
#define kPARTPropertyNameArray @"unknown", @"scc_row_guid", @"scc_content_guid", @"scc_request_guid", @"scc_version", @"scc_content_type_guid", @"scc_folder_guid",  @"scc_title",  @"scc_keywords",  @"scc_comments",  @"scc_description", @"scc_status", @"scc_last_mod", @"scc_owner", @"scc_author", @"scc_expiration_type", @"scc_active_date", @"scc_expire_date", @"scc_language", @"scc_retired", @"scc_reference_guid", @"scc_reference_version", @"scc_attributes", @"scc_review_date", @"scc_friendly_id", @"scc_categories", nil

typedef enum {
  PARTContentPropertyUnknown,
  PARTContentPropertyId,
  PARTContentPropertyType,
  PARTContentPropertyVersion,
  PARTContentPropertyTitle,
  PARTContentPropertyDescription,
  PARTContentPropertyFid,
  PARTContentPropertyLanguage,
  PARTContentPropertyCategory,
} PARTContentProperty;
#define kPARTContentPropertyArray @"unknown", @"cid", @"ctype", @"version", @"title", @"description",  @"fid",  @"language",  @"category", nil

typedef enum {
  PARTPropertyTypeUnknown,
  PARTPropertyTypeString,
  PARTPropertyTypeDate,
  PARTPropertyTypeInteger,
  PARTPropertyTypeNumber,
  PARTPropertyTypeCharacter,
} PARTPropertyType;
#define kPARTPropertyTypeArray @"unknown", @"S", @"D", @"I", @"N", @"c", nil

typedef enum {
  PARTFieldValueNameUnknown,
  PARTFieldValueNameChar,
  PARTFieldValueNameInt,
  PARTFieldValueNameClob,
} PARTFieldValueName;
#define kPARTFieldValueNameArray @"unknown", @"sccf_field_value_char", @"sccf_field_value_int", @"sccfc_field_value_clob", nil

typedef enum {
  PARTRequestTypeUnknown,
  PARTRequestTypeRealtimeAlert,
  PARTRequestTypeExecute,
} PARTRequestType;
#define kPARTRequestTypeArray @"unknown", @"RealtimeAlert", @"Execute", nil

typedef enum {
  PARTRequestChannelUnknown,
  PARTRequestChannelAuthorCenter,
  PARTRequestChannelSupportAction,
} PARTRequestChannel;
#define kPARTRequestChannelArray @"unknown", @"AuthorCenter", @"SupportAction", nil

typedef enum {
    PARTEvalOptionsUnknown,
    PARTEvalOptionsScan,
    PARTEvalOptionsScanAndFix,
    PARTEvalOptionsFix,
} PARTEvalOptions;
#define kPARTEvalOptionsArray @"unknown", @"Scan", @"ScanAndFix", @"Fix", nil

@interface PARealTimeAlertPayloadInfo : NSObject

// content to be stored in "Alert Content.htm" file
@property (nonatomic, readonly) NSString *alertContent;

// content to be stored in "content.js" and "1aaa881f-e93b-49fd-b3c8-354ea40b78d5.1.xml" files
@property (nonatomic, readonly) NSString *contentGuid;
@property (nonatomic, readonly) NSString *contentVersion;
@property (nonatomic, readonly) NSString *contentTitle;
@property (nonatomic, readonly) NSString *contentDescription;
@property (nonatomic, readonly) NSString *contentType;
@property (nonatomic, readonly) NSString *folderGuid;
@property (nonatomic, readonly) NSString *language;
@property (nonatomic, readonly) NSString *category ;

// Property sets to be stored in "1aaa881f-e93b-49fd-b3c8-354ea40b78d5.1.xml" file
@property (nonatomic, readonly) NSString *rowGuid;
@property (nonatomic, readonly) NSString *requestGuid;
@property (nonatomic, readonly) NSString *keywords;
@property (nonatomic, readonly) NSString *comments;
@property (nonatomic, readonly) NSString *status;
@property (nonatomic, readonly) NSString *lastModifiedDateStr;
@property (nonatomic, readonly) NSString *owner;
@property (nonatomic, readonly) NSString *author;
@property (nonatomic, readonly) NSString *expirationType;
@property (nonatomic, readonly) NSString *activateDateStr;
@property (nonatomic, readonly) NSString *expiryDateStr;
@property (nonatomic, readonly) NSUInteger retired;
@property (nonatomic, readonly) NSString *referenceGuid;
@property (nonatomic, readonly) NSString *referenceVersion;
@property (nonatomic, readonly) NSString *attributes;
@property (nonatomic, readonly) NSString *reviewDateStr;
@property (nonatomic, readonly) NSString *friendlyId;
@property (nonatomic, readonly) NSString *categories;

// field sets to be stored in "1aaa881f-e93b-49fd-b3c8-354ea40b78d5.1.xml" file
@property (nonatomic, readonly) NSArray *fields;

// additional detail; no need to be added to any file
@property (nonatomic, readonly) NSUInteger alertTime;
@property (nonatomic, readonly) NSUInteger windowLength;
@property (nonatomic, readonly) NSUInteger windowWidth;
@property (nonatomic, readonly) NSUInteger windowLook;
@property (nonatomic, readonly) NSUInteger animateWindow;
@property (nonatomic, readonly) BOOL acknowledgeRequired;
@property (nonatomic, readonly) NSString *alertDisplayTimeEnd; // date of form 10/28/2017 12:01:01 AM
@property (nonatomic, readonly) NSUInteger transparency;
@property (nonatomic, readonly) NSString *filters;


@property (nonatomic, readonly) NSString *alertContentDescription;
@property (nonatomic, readonly) NSString *alertContentString;

@property (nonatomic, readonly) NSString *certificateDataStr;
@property (nonatomic, readonly) NSString *certificateSignature;
@property (nonatomic, readonly) NSString *requestType;
@property (nonatomic, readonly) PARTRequestType requestTypeEnum;
@property (nonatomic, readonly) NSString *requestChannel;
@property (nonatomic, readonly) PARTRequestChannel requestChannelEnum;
@property (nonatomic, readonly) PARTEvalOptions evalOptionsEnum;

- (instancetype)initWithPayload:(NSDictionary *)payloadDict;

/// name of the property used for xml creation
- (NSString *)propertyNameFor:(PARTPropertyName)propertyEnum;
- (NSString *)propertyTypeFor:(PARTPropertyType)propertyTypeEnum;
- (NSString *)fieldValueNameFor:(PARTFieldValueName)fieldNameEnum;

@end
