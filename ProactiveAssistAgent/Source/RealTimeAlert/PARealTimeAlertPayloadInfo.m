//
//  PARealTimeAlertPayloadInfo.m
//  ProactiveAssistAgent
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <PAXmppService/PAXmppService.h>
#import "PARealTimeAlertPayloadInfo.h"
#import "NSString+Additions.h"

NSString *const kPARTAlertContent = @"Alert Content";

NSString *const kPARTContentGuid = @"ContentGuid";
NSString *const kPARTContentVersion = @"ContentVersion";
NSString *const kPARTContentTitle = @"ContentTitle";
NSString *const kPARTContentDescription = @"ContentDescription";
NSString *const kPARTContentType = @"ContentType";
NSString *const kPARTFolderGuid = @"folderId";
NSString *const kPARTLanguage = @"Language";
NSString *const kPARTCategory = @"category";

NSString *const kPARTRowGuid = @"scc_row_guid";
NSString *const kPARTRequestGuid = @"RequestGuid";
NSString *const kPARTKeywords = @"scc_keywords";
NSString *const kPARTComments = @"scc_comments";
NSString *const kPARTStatus = @"scc_status";
NSString *const kPARTLastModifiedDateStr = @"scc_last_mod";
NSString *const kPARTOwner = @"scc_owner";
NSString *const kPARTAuthor = @"scc_author";
NSString *const kPARTExpirationType = @"scc_expiration_type";
NSString *const kPARTActivateDateStr = @"SCC_ACTIVE_DATE";
NSString *const kPARTExpiryDateStr = @"SCC_EXPIRE_DATE";
NSString *const kPARTRetired = @"scc_retired";
NSString *const kPARTReferenceGuid = @"scc_reference_guid";
NSString *const kPARTReferenceVersion = @"scc_reference_version";
NSString *const kPARTAttributes = @"scc_attributes";
NSString *const kPARTReviewDateStr = @"scc_review_date";
NSString *const kPARTFriendlyId = @"scc_friendly_id";
NSString *const kPARTCategories = @"scc_categories";
NSString *const kPARTFilters = @"filters";

NSString *const kPARTFieldSets = @"FieldSets";

@implementation PARealTimeAlertPayloadInfo

- (instancetype)initWithPayload:(NSDictionary *)payloadDict {
  self = [super init];
  if (self) {
    if (payloadDict) {
      _alertContent = payloadDict[kPARTAlertContent];
      
      _contentGuid = payloadDict[kPARTContentGuid];
      _contentVersion = payloadDict[kPARTContentVersion];
      _contentTitle = payloadDict[kPARTContentTitle];
      _contentDescription = payloadDict[kPARTContentDescription];
      _contentType = payloadDict[kPARTContentType];
      _folderGuid = payloadDict[kPARTFolderGuid];
      _language = payloadDict[kPARTLanguage];
      _category = payloadDict[kPARTCategory];
      
      _rowGuid = payloadDict[kPARTRowGuid];
      _requestGuid = payloadDict[kPARTRequestGuid];
      _keywords = payloadDict[kPARTKeywords];
      _comments = payloadDict[kPARTComments];
      _status = payloadDict[kPARTStatus];
      _lastModifiedDateStr = payloadDict[kPARTLastModifiedDateStr];
      _owner = payloadDict[kPARTOwner];
      _author = payloadDict[kPARTAuthor];
      _expirationType = payloadDict[kPARTExpirationType];
      _activateDateStr = payloadDict[kPARTActivateDateStr];
      _expiryDateStr = payloadDict[kPARTExpiryDateStr];
      _retired = [payloadDict[kPARTRetired] integerValue];
      _referenceGuid = payloadDict[kPARTReferenceGuid];
      _referenceVersion = payloadDict[kPARTReferenceVersion];
      _attributes = payloadDict[kPARTAttributes];
      _reviewDateStr = payloadDict[kPARTReviewDateStr];
      _friendlyId = payloadDict[kPARTFriendlyId];
      _categories = payloadDict[kPARTCategories];
      
      _certificateDataStr = payloadDict[kPADOCertificateData];
      _certificateSignature = payloadDict[kPADOCertificateSignature];
      
      _requestType = payloadDict[kPADORequestType];
      _requestChannel = payloadDict[kPADORequestChannel];
      
      _requestTypeEnum = [self requestTypeEnumValue];
      _requestChannelEnum = [self requestChannelEnumValue];
      
      [self constructFieldSetFrom:payloadDict];
      
      _alertTime = [payloadDict[kPADOAlertTime] integerValue];
      _windowWidth = [payloadDict[kPADOWindowWidth] integerValue];
      _windowLength = [payloadDict[kPADOWindowLength] integerValue];
      _windowLook = [payloadDict[kPADOWindowLook] integerValue];
      _acknowledgeRequired = [payloadDict[kPADOAcknowledgeRequired] boolValue];
      _animateWindow = [payloadDict[kPADOAnimateWindow] integerValue];
      _transparency = [payloadDict[kPADOTransperency] integerValue];
      _alertDisplayTimeEnd = payloadDict[kPADODisplayTimeEnd];
      _filters = payloadDict[kPARTFilters];
    } else {
      self = nil;
    }
  }
  return self;
}

- (void)constructFieldSetFrom:(NSDictionary *)payloadDict {
  /// parse field set
  NSArray *fieldSets = payloadDict[kPARTFieldSets];
  if (fieldSets.count) {
    NSMutableArray *fields = [[NSMutableArray alloc] init];
    
    for (NSDictionary *fieldDict in fieldSets) {
      PARealTimeAlertPayloadFieldInfo *fieldInfo = [[PARealTimeAlertPayloadFieldInfo alloc] initWithFieldPayload:fieldDict];
      if (fieldInfo) {
        [fields addObject:fieldInfo];
      }
    }
    
    if (fields.count) {
      _fields = [NSArray arrayWithArray:fields];
    }
  }
}
- (NSString *)alertContentDescription {
  NSString *contentDescription = @"{";
  NSString *currentValue = _contentGuid;
  if (!currentValue) {
    currentValue = @"";
  }
  contentDescription = [contentDescription stringByAppendingFormat:@"\"%@\":\"%@\",",[self contentPropertyNameFor:PARTContentPropertyId],currentValue];
  
  currentValue = _contentVersion;
  if (!currentValue) {
    currentValue = @"";
  }
  contentDescription = [contentDescription stringByAppendingFormat:@"\"%@\":\"%@\",",[self contentPropertyNameFor:PARTContentPropertyVersion],currentValue];
  
  currentValue = _contentTitle;
  if (!currentValue) {
    currentValue = @"";
  }
  contentDescription = [contentDescription stringByAppendingFormat:@"\"%@\":\"%@\",",[self contentPropertyNameFor:PARTContentPropertyTitle], currentValue];
  
  currentValue = _contentDescription;
  if (!currentValue) {
    currentValue = @"";
  }
  contentDescription = [contentDescription stringByAppendingFormat:@"\"%@\":\"%@\",",[self contentPropertyNameFor:PARTContentPropertyDescription], currentValue];
  
  currentValue = _contentType;
  if (!currentValue) {
    currentValue = @"";
  }
  contentDescription = [contentDescription stringByAppendingFormat:@"\"%@\":\"%@\",",[self contentPropertyNameFor:PARTContentPropertyType], currentValue];
  
  currentValue = _folderGuid;
  if (!currentValue) {
    currentValue = @"";
  }
  contentDescription = [contentDescription stringByAppendingFormat:@"\"%@\":\"%@\",",[self contentPropertyNameFor:PARTContentPropertyFid],currentValue];
  
  currentValue = _language;
  if (!currentValue) {
    currentValue = @"";
  }
  contentDescription = [contentDescription stringByAppendingFormat:@"\"%@\":\"%@\",",[self contentPropertyNameFor:PARTContentPropertyLanguage], currentValue];
  
  currentValue = _category;
  if (!currentValue) {
    currentValue = @"";
  }
  contentDescription = [contentDescription stringByAppendingFormat:@"\"%@\":\"%@\"",[self contentPropertyNameFor:PARTContentPropertyCategory], currentValue];
  
  contentDescription = [contentDescription stringByAppendingString:@"}"];
  return contentDescription;
}

- (NSString *)alertContentString {
  NSString *content =
  [[[[[_alertContent stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"]
      stringByReplacingOccurrencesOfString:@"&quot;" withString:@"\""]
     stringByReplacingOccurrencesOfString:@"&#39;" withString:@"'"]
    stringByReplacingOccurrencesOfString:@"&gt;" withString:@">"]
   stringByReplacingOccurrencesOfString:@"&lt;" withString:@"<"];
  return content;
}

- (NSString *)contentPropertyNameFor:(PARTContentProperty)contentEnum {
  NSString *property = @"";
  NSArray *propertyArray = [[NSArray alloc] initWithObjects:kPARTContentPropertyArray];
  NSUInteger index = (NSUInteger)contentEnum;
  if (index < propertyArray.count) {
    property = propertyArray[index];
  }
  return property;
}
- (NSString *)propertyNameFor:(PARTPropertyName)propertyEnum {
  NSString *property = @"";
  NSArray *propertyArray = [[NSArray alloc] initWithObjects:kPARTPropertyNameArray];
  NSUInteger index = (NSUInteger)propertyEnum;
  if (index < propertyArray.count) {
    property = propertyArray[index];
  }
  return property;
}
- (NSString *)propertyTypeFor:(PARTPropertyType)propertyTypeEnum {
  NSString *propertyType = @"";
  NSArray *propertyTypeArray = [[NSArray alloc] initWithObjects:kPARTPropertyTypeArray];
  NSUInteger index = (NSUInteger)propertyTypeEnum;
  if (index < propertyTypeArray.count) {
    propertyType = propertyTypeArray[index];
  }
  return propertyType;
}

- (NSString *)fieldValueNameFor:(PARTFieldValueName)fieldNameEnum {
  NSString *fieldName = @"";
  NSArray *fieldValueArray = [[NSArray alloc] initWithObjects:kPARTFieldValueNameArray];
  NSUInteger index = (NSUInteger)fieldNameEnum;
  if (index < fieldValueArray.count) {
    fieldName = fieldValueArray[index];
  }
  return fieldName;
}
- (PARTRequestType)requestTypeEnumValue {
  PARTRequestType reqType = PARTRequestTypeUnknown;
  if (![_requestType isEmptyOrHasOnlyWhiteSpaces]) {
    NSArray *reqTypeArray = [[NSArray alloc] initWithObjects:kPARTRequestTypeArray];
    NSUInteger index = [reqTypeArray indexOfObject:_requestType];
    if (NSNotFound != index) {
      reqType = (PARTRequestType)index;
    }
  }
  return reqType;
}

- (PARTRequestChannel)requestChannelEnumValue {
  PARTRequestChannel channelEnum = PARTRequestChannelUnknown;
  if (![_requestChannel isEmptyOrHasOnlyWhiteSpaces]) {
    NSArray *channelArray = [[NSArray alloc] initWithObjects:kPARTRequestChannelArray];
    NSUInteger index = [channelArray indexOfObject:_requestChannel];
    if (NSNotFound != index) {
      channelEnum = (PARTRequestChannel)index;
    }
  }
  return channelEnum;
}

@end
