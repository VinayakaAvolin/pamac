//
//  PAInputConfigurationXmlParser.h
//  ProactiveAssistAgent
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <PACoreServices/PACoreServices.h>
#import "PAAlertInfo.h"

typedef void(^PAInputConfigurationXmlParserCompletionBlock)(BOOL success, PAAlertInfo *alertInfo);

@interface PAInputConfigurationXmlParser : PAXmlParser

- (instancetype)initWithAlertInfo:(PAAlertInfo *)alertInfo;

- (void)parseInputConfigurationWithCompletion:(PAInputConfigurationXmlParserCompletionBlock)completionBlock;

@end
