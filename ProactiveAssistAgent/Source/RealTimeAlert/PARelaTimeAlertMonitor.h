//
//  PARelaTimeAlertMonitor.h
//  ProactiveAssistAgent
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PARelaTimeAlertMonitor : NSObject

- (void)start;
- (void)stop;

@end
