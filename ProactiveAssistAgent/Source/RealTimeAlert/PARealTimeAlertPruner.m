//
//  PARealTimeAlertPruner.m
//  ProactiveAssistAgent
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <PACoreServices/PACoreServices.h>
#import "PARealTimeAlertPruner.h"
#import "PAInputConfigurationXmlParser.h"
#import "NSDate+Additions.h"
#import "NSString+Additions.h"

NSString *const kPARPXmlExtension = @"xml";

@interface PARealTimeAlertPruner () {
  NSOperationQueue *_operationQueue;
  BOOL _isAlertPruneOn;
  NSUInteger _totalAlertsToPrune;
  PAFileSystemManager *_fsManager;
}

@end

@implementation PARealTimeAlertPruner

- (instancetype)init
{
  self = [super init];
  if (self) {
    _operationQueue = [[NSOperationQueue alloc]init];
    _isAlertPruneOn = false;
    _fsManager = [[PAFileSystemManager alloc]init];
    _totalAlertsToPrune = 0;
  }
  return self;
}

- (void)start {
  if (_isAlertPruneOn) {
    return;
  }
  _totalAlertsToPrune = 0;
  _isAlertPruneOn = true;
  [_operationQueue cancelAllOperations];
  NSInvocationOperation *startOperation = [[NSInvocationOperation alloc]
                                           initWithTarget:self
                                           selector:@selector(startOperation)
                                           object:nil];
  [_operationQueue addOperation:startOperation];
}

- (void)stop{
  _totalAlertsToPrune = 0;
  _isAlertPruneOn = false;
  [_operationQueue cancelAllOperations];
}

- (BOOL)isPruning {
  return _isAlertPruneOn;
}
#pragma mark - Private Methods

- (void)startOperation {
  NSString *realTimeAlertsPath = _fsManager.realTimeAlertFolderPath;
  NSArray *contentsOfRealTimeAlertDir = [_fsManager contentsOfDirectory:realTimeAlertsPath];
  _totalAlertsToPrune = contentsOfRealTimeAlertDir.count;
  for (NSString *itemName in contentsOfRealTimeAlertDir) {
    @autoreleasepool {
      NSString *contentFolderPath = [realTimeAlertsPath stringByAppendingPathComponent:itemName];
      NSString *contentXmlPath = [contentFolderPath stringByAppendingPathComponent:itemName];
      contentXmlPath = [contentXmlPath stringByAppendingPathExtension:kPARPXmlExtension];
      if ([_fsManager doesDirectoryExist:contentFolderPath] &&
          [_fsManager doesFileExist:contentXmlPath]) {
        PAAlertInfo *alertInfo = [[PAAlertInfo alloc] initWithContentGuid:[itemName stringByDeletingPathExtension] version:itemName.pathExtension alertType:PAAlertTypeRealTime];
        PAInputConfigurationXmlParser *xmlParser = [[PAInputConfigurationXmlParser alloc]initWithAlertInfo:alertInfo];
        [xmlParser parseInputConfigurationWithCompletion:^(BOOL success, PAAlertInfo *alertInfo) {
          [self checkAndPruneAlertContentNow:alertInfo];
        }];
      } else {
        [self updatePruneOnCheck];
      }
    }
  }
}

- (void)updatePruneOnCheck {
  --_totalAlertsToPrune;
  if (_totalAlertsToPrune <= 0) {
    _totalAlertsToPrune = 0;
    _isAlertPruneOn = false;
  }
}

- (NSDate *)getLocalDateTimeFromUTC:(NSString *)strDate
{
  NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
  [formatter setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
  [formatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
  NSDate *date = [formatter dateFromString:strDate];
  
  return date;
}

- (void)checkAndPruneAlertContentNow:(PAAlertInfo *)alertInfo {
  NSString *alertDisplayEndDateStr = alertInfo.expiryTimeStr;
  
  [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"Real time alert prune - expiry date [%@]",alertDisplayEndDateStr];
  if (![alertDisplayEndDateStr isEmptyOrHasOnlyWhiteSpaces]) {
    NSDate *alertDisplayEndDate = [self getLocalDateTimeFromUTC : alertDisplayEndDateStr];
    // check if expiry date is past date
    NSDate *currentDate = [NSDate systemDate];
    
    // is past date; then prune the alert content from machine
    if ([currentDate compare:alertDisplayEndDate] == NSOrderedDescending) {
      [_fsManager deleteDirectory: alertInfo.contentFolderPath];
    }
  }
  [self updatePruneOnCheck];
}
@end
