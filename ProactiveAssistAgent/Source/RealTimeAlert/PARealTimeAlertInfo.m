//
//  PARealTimeAlertInfo.m
//  ProactiveAssistAgent
//
//  Created by Kavitha on 27/10/17.
//  Copyright © 2017 Aptean. All rights reserved.
//  Written under contract by Robosoft Technologies Pvt. Ltd.
//

#import "PARealTimeAlertInfo.h"
#import "NSDate+Additions.h"
#import "NSString+Additions.h"

@implementation PARealTimeAlertInfo

- (instancetype)initWithExpiryDate:(NSString *)expiryDate
                 contentFolderPath:(NSString *)contentPath
                    contentXmlPath:(NSString *)xmlPath {
    self = [super init];
    if (self) {
        _expiryTimeStr = expiryDate;
        if (![_expiryTimeStr isEmptyOrHasOnlyWhiteSpaces]) {
            _expiryDate = [NSDate dateFromString:_expiryTimeStr format:Format9];
        }
        _contentFolderPath = contentPath;
        _inputXmlPath = xmlPath;
    }
    return self;
}

- (id)copyWithZone:(NSZone *)zone {
    PARealTimeAlertInfo *newAlertInfo = [[[self class] allocWithZone:zone] init];
    newAlertInfo.expiryTimeStr = self.expiryTimeStr;
    newAlertInfo.expiryDate = self.expiryDate;
    newAlertInfo.contentFolderPath = self.contentFolderPath;
    newAlertInfo.inputXmlPath = self.inputXmlPath;
    return newAlertInfo;
}

@end
