//
//  PALoginAgentProtocol.h
//  ProactiveAssistAgent
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <PACoreServices/PACoreServices.h>
#import <PASupportService/PASupportService.h>
#import "PAServiceResponse.h"

#define kPALoginAgentServiceName @"97T89359R6.com.avolin.supportsoft"
#define kPAMonitoringAgentServiceName @"com.avolin.PAMonitoringAgent"

typedef void(^PALoginAgentScriptResult)(PAServiceResponse *response);
typedef void(^PALoginAgentServiceResult)(PAServiceResponse *response);
typedef void(^PALoginAgentSyncResult)(BOOL success);
typedef void(^PALoginAgentPlistServiceResult)(BOOL success);
typedef void(^PALoginAgentOptimizationResult)(BOOL success);

@protocol PALoginAgentProtocol

// to execute solution script
- (void)executeScript:(PAScriptInfo *)script withReply:(PALoginAgentScriptResult)reply;
// to execute support services like protection, download offer
- (void)executeService:(PAServiceInfo *)service withReply:(PALoginAgentServiceResult)reply;
/// Method to execute plist write using plist service framework
- (void)executePlistService:(PAPlistServiceInfo *)service withReply:(PALoginAgentPlistServiceResult)reply;

// to update or initiate sync
- (void)updateWithCallback:(PALoginAgentSyncResult)reply;

// to initiate optimization
- (void)startOptimizerWithReply:(PALoginAgentOptimizationResult)reply;
- (void)stopOptimizerWithReply:(PALoginAgentOptimizationResult)reply;

// to schedule optimization
-(void)scheduleOptimization:(NSDictionary *)scheduleInfo;

@end
