//
//  PAOptimizerConstants.h
//  ProactiveAssistAgent
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *const kPAScheduleTimeKey;
extern NSString *const kPAScheduleFrequencyKey;
extern NSString *const kPAScheduleDayKey;
extern NSString *const kPAScheduleGuidsKey;
