//
//  PAOptimizer.h
//  ProactiveAssistAgent
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PAOptimizerDelegate <NSObject>

@optional

- (void)didFinishOptimization:(BOOL)finish;

@end

@interface PAOptimizer : NSObject

@property(nonatomic, weak) id<PAOptimizerDelegate> optimizerDelegate;

- (void)start;
- (void)startOptimization;
- (void)stopOptimization;
- (void)stop;

@end
