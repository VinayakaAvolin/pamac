//
//  PAOptimizerResultHandler.h
//  ProactiveAssistAgent
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
  PAORStatusUnknown,
  PAORStatusStarted,
  PAORStatusStopped,
} PAORStatus;
#define kPAORStatusArray @"unknown", @"Started", @"Stopped", nil

@interface PAOptimizerResultHandler : NSObject

@property (nonatomic) NSString *guid;
@property (nonatomic) NSString *startTime;
@property (nonatomic) NSString *endTime;
@property (nonatomic) NSNumber *resultCode;
@property (nonatomic) NSString *resultDesc;
@property (nonatomic) PAORStatus statusEnum;

@property (nonatomic) NSString *resultXml;
@property (nonatomic) NSString *optimizationsInfoXmlFileContent;

- (BOOL)createResultXml;
- (BOOL)storeCurrentOptimizationResult;


@end
