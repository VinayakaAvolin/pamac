//
//  PAOptimizationTodoListParser.m
//  ProactiveAssistAgent
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PAOptimizationTodoListParser.h"
#import "NSArray+Additions.h"

NSString *const kPAOPTodoXmlName = @"optimizationstodolist.xml";
NSString *const kPAOPOptimizationsKey = @"Optimizations";
NSString *const kPAOPOptimizationKey = @"Optimization";
NSString *const kPAOPOptimizationGuidKey = @"OptimizationGuid";

@interface PAOptimizationTodoListParser () {
  NSString *_optimizeTodoXmlPath;
  PAFileSystemManager *_fsManager;
  NSArray *_optimizationGuidsList;
  NSArray *_optimizationGuidsWithoutVersionList;
}

@end

@implementation PAOptimizationTodoListParser

- (instancetype)init {
  self = [super init];
  if (self) {
    _fsManager = [[PAFileSystemManager alloc] init];
    _optimizeTodoXmlPath = [self todoListXmlPath];
    if (!_optimizeTodoXmlPath) {
      self = nil;
    }
  }
  return self;
}
/*
 {
 Optimizations =     {
 Optimization =         (
 {
 OptimizationGuid = "04362ab0-d7e8-4e4c-b6bc-4c6e9ee656e8.9";
 },
 {
 OptimizationGuid = "0eefb918-416a-4b37-bd6e-2e21c94bad5e.8";
 },
 {
 OptimizationGuid = "0024a3d6-021f-4634-a055-7ea81785ffc6.9";
 }
 );
 };
 "__name" = OptimizationsList;
 }
 */
- (void)parseOptimizationTodoListWithCompletion:(PAOptimizationTodoListParserCompletionBlock)completionBlock {
  NSMutableArray *guidList = [[NSMutableArray alloc] init];
  NSDictionary *xmlDict = [self dictionaryFromXmlAtPath:_optimizeTodoXmlPath];
  if (xmlDict) {
    NSDictionary *optimizationsDict = xmlDict[kPAOPOptimizationsKey];
    id optimizationInfo = optimizationsDict[kPAOPOptimizationKey];
    if ([optimizationInfo isKindOfClass:[NSArray class]]) {
      for (NSDictionary *guidDict in optimizationInfo) {
        NSString *guid = guidDict[kPAOPOptimizationGuidKey];
        if ([guid isKindOfClass:[NSString class]]) {
          [guidList addObject:guid];
        }
      }
    } else if ([optimizationInfo isKindOfClass:[NSDictionary class]]) {
      NSString *guid = optimizationInfo[kPAOPOptimizationGuidKey];
      if ([guid isKindOfClass:[NSString class]]) {
        [guidList addObject:guid];
      }
    }
    
  }
  
  // remove common objects from array
  _optimizationGuidsList = [guidList arrayAfterRemovingDuplicates];
  //[self prepareGuidsWithoutVersion];
  // remove common
  completionBlock((_optimizationGuidsList.count > 0), _optimizationGuidsList);
}

- (NSString *)todoListXmlPath {
  NSString *xmlPath = [_fsManager optimizationFolderPath];
  xmlPath = [xmlPath stringByAppendingPathComponent:kPAOPTodoXmlName];
  if ([_fsManager doesFileExist:xmlPath]) {
    return xmlPath;
  }
  return nil;
}

- (void)prepareGuidsWithoutVersion {
  NSMutableArray *guidList = [[NSMutableArray alloc] init];
  for (NSString *guidWithVersion in _optimizationGuidsList) {
    NSString *guidwithoutVersion = [guidWithVersion stringByDeletingPathExtension];
    if (guidwithoutVersion) {
      [guidList addObject:guidwithoutVersion];
    }
  }
  
  if (guidList.count) {
    _optimizationGuidsWithoutVersionList = [guidList copy];
  }
}

@end
