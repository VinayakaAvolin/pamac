//
//  PAOptimizerConstants.m
//  ProactiveAssistAgent
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PAOptimizerConstants.h"

NSString *const kPAScheduleTimeKey = @"PAScheduleTimeKey";
NSString *const kPAScheduleFrequencyKey = @"PAScheduleFrequencyKey";
NSString *const kPAScheduleDayKey = @"PAScheduleDayKey";
NSString *const kPAScheduleGuidsKey = @"PAScheduleGuidsKey";

