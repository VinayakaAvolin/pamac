//
//  PAOptimizationTodoListParser.h
//  ProactiveAssistAgent
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <PACoreServices/PACoreServices.h>

typedef void(^PAOptimizationTodoListParserCompletionBlock)(BOOL success, NSArray *guidList);

@interface PAOptimizationTodoListParser : PAXmlParser

- (void)parseOptimizationTodoListWithCompletion:(PAOptimizationTodoListParserCompletionBlock)completionBlock;

@end
