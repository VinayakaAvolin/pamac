//
//  PAOptimizerResultHandler.m
//  ProactiveAssistAgent
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <PACoreServices/PACoreServices.h>
#import "PAOptimizerResultHandler.h"
#import "NSDate+Additions.h"

NSString *const kPAOPOptimizationInfoXmlName = @"optimizationsinfo-%@.xml";
NSString *const kPAOPOptimizationInfoXmlRoot = @"OptimizationsList";
NSString *const kPAOPOptimizationsNodeName = @"Optimizations";

@interface PAOptimizerResultHandler () {
  NSString *_optimizationsInfoXmlPath;
  PAXmlWriter *_xmlWriter;
  PAFileSystemManager *_fsManager;
}

@end
@implementation PAOptimizerResultHandler

- (instancetype)init
{
  self = [super init];
  if (self) {
    _fsManager = [[PAFileSystemManager alloc]init];
    NSString *optmizeFolderPath = _fsManager.optimizationFolderPath;
    
    NSString *xmlFileName = [NSString stringWithFormat:kPAOPOptimizationInfoXmlName,[[NSDate systemDate] dateStringWithFormat:Format11]];
    _optimizationsInfoXmlPath = [optmizeFolderPath stringByAppendingPathComponent:xmlFileName];
    _xmlWriter = [[PAXmlWriter alloc]initWithXmlAtPath:_optimizationsInfoXmlPath];
  }
  return self;
}
/*
 <Optimization>
 <OptimizationGuid>0024a3d6-021f-4634-a055-7ea81785ffc6</OptimizationGuid>
 <OptimizationStartTime>2017-11-06T15:04:16.402495+05:30</OptimizationStartTime>
 <OptimizationEndTime>2017-11-06T15:04:16.6449478+05:30</OptimizationEndTime>
 <OptimizationResult>0</OptimizationResult>
 <OptimizationResultDesc>C:\Users\Vinayaka.S\AppData\Roaming\Microsoft\Windows\Recent\\Users\\Vinayaka.S\\AppData\\Roaming\\Microsoft\\Windows\\Recent\\REPORTING:Recently Used Document Count is:15SupportAction succeeded : Recently used Document list erased successfully.</OptimizationResultDesc>
 <OptimizationStatus>Stopped</OptimizationStatus>
 </Optimization>
 */
- (NSString *)resultXml {
  NSString *xmlStr = nil;
  
  if (_guid) {
    xmlStr = @"";
    xmlStr = [xmlStr stringByAppendingString:@"<Optimization>\n"];
    xmlStr = [xmlStr stringByAppendingFormat:@"<OptimizationGuid>%@</OptimizationGuid>\n",_guid];
    
    if (_startTime) {
      xmlStr = [xmlStr stringByAppendingFormat:@"<OptimizationStartTime>%@</OptimizationStartTime>\n",_startTime];
    }
    if (_endTime) {
      xmlStr = [xmlStr stringByAppendingFormat:@"<OptimizationEndTime>%@</OptimizationEndTime>\n",_endTime];
    }
    if (_resultCode) {
      xmlStr = [xmlStr stringByAppendingFormat:@"<OptimizationResult>%@</OptimizationResult>\n",_resultCode];
    }
    if (_resultDesc) {
      xmlStr = [xmlStr stringByAppendingFormat:@"<OptimizationResultDesc>%@</OptimizationResultDesc>\n",_resultDesc];
    }
    if (self.status) {
      xmlStr = [xmlStr stringByAppendingFormat:@"<OptimizationStatus>%@</OptimizationStatus>\n",self.status];
    }
    xmlStr = [xmlStr stringByAppendingString:@"</Optimization>\n"];
    xmlStr = [xmlStr stringByReplacingOccurrencesOfString:@"&" withString:@"&amp;"];
  }
  return xmlStr;
}

- (NSString *)status {
  NSString *statusValue = @"";
  NSArray *statusArray = [[NSArray alloc] initWithObjects:kPAORStatusArray];
  NSUInteger index = (NSUInteger)_statusEnum;
  if (index < statusArray.count) {
    statusValue = statusArray[index];
  }
  return statusValue;
}

- (BOOL)storeCurrentOptimizationResult {
  BOOL result = [self createResultXml];
  if (!result) {
    return false;
  }
  NSString *resultXmlStr = self.resultXml;
  if (resultXmlStr) {
    result = [_xmlWriter addChildXmlString:resultXmlStr parentXPath:kPAOPOptimizationsNodeName];
  }
  return result;
}

- (BOOL)createResultXml {
  if ([_fsManager doesFileExist:_optimizationsInfoXmlPath]) {
    return true;
  }
  BOOL xmlCreated = false;
  
  PAXmlFileProperties *properties = [[PAXmlFileProperties alloc]initWithVersion:kPAXmlVersionValue encoding:nil standalone:nil root:kPAOPOptimizationInfoXmlRoot];
  
  PAXmlServiceManager *manager = [[PAXmlServiceManager alloc]init];
  xmlCreated = [manager createXmlAtPath:_optimizationsInfoXmlPath withProperties:properties];
  if (xmlCreated) {
    _xmlWriter = [[PAXmlWriter alloc]initWithXmlAtPath:_optimizationsInfoXmlPath];
    if (_xmlWriter) {
      xmlCreated = [_xmlWriter addXmlElementWithName:kPAOPOptimizationsNodeName value:nil attributes:nil parentXPath:kPAOPOptimizationInfoXmlRoot];
    }
  }
  return xmlCreated;
}
- (NSString *)optimizationsInfoXmlFileContent {
  if (_optimizationsInfoXmlPath && [_fsManager doesFileExist:_optimizationsInfoXmlPath]) {
    NSError *error =nil;
    PAXmlServiceManager *manager = [[PAXmlServiceManager alloc]init];
    NSData *xmldata = [manager xmlDataAtPath:_optimizationsInfoXmlPath error:&error];
    if (xmldata) {
      NSString *xmlString = [[NSString alloc] initWithData:xmldata encoding:NSUTF8StringEncoding];
      return xmlString;
    }
  }
  return nil;
}
@end
