//
//  PAOptimizer.m
//  ProactiveAssistAgent
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <PASupportService/PASupportService.h>
#import "PAOptimizer.h"
#import "PAOptimizationTodoListParser.h"
#import "PAOptimizerResultHandler.h"
#import "NSDate+Additions.h"

#import "PAJSResult.h"
#import "PAJSInvokedCommand.h"
#import "PAJSCommandDelegate.h"
#import "PAJSConstants.h"
#import "PACommonUtilityController.h"

@interface PAOptimizer () {
  NSOperationQueue *_operationQueue;
  NSMutableArray *_optimizationGuidList;
  NSString *_currentOptimizationGuid;
  PAOptimizerResultHandler *_optimizerResultHandler;
  NSString *_optimizationState;
}

@end

@implementation PAOptimizer

- (instancetype)init
{
  self = [super init];
  if (self) {
    _operationQueue = [[NSOperationQueue alloc]init];
    _optimizationGuidList = nil;
    _optimizationState = @"start";
  }
  return self;
}
- (void)start {
  
  __block NSString *log = [@"CONTENT__" stringByAppendingString:[@"performance_manager" stringByAppendingString:[@"__" stringByAppendingString:[@"0" stringByAppendingString:[@"__optimizer_start" stringByAppendingString:[@"__Client__1____" stringByAppendingString:[@"optimizer_start" stringByAppendingString:[@"__" stringByAppendingString:[@"" stringByAppendingString:[@"____MAC Others__" stringByAppendingString:@""]]]]]]]]]];
  
  if(_optimizationState == @"start" ) {
    [self WriteOptimizerUsageLog:(NSString *)log];
    _optimizationState = @"stop";
  }
  
  _optimizerResultHandler = [[PAOptimizerResultHandler alloc] init];
  
  // notify
  [[NSDistributedNotificationCenter defaultCenter] postNotificationName:kPASupportServiceStartOptimizerNotificationName object:kPASupportServiceOptimizationNotificationObjectName userInfo:nil deliverImmediately:true];
  
  [_optimizationGuidList removeAllObjects];
  [_operationQueue cancelAllOperations];
  NSInvocationOperation *startOperation = [[NSInvocationOperation alloc]
                                           initWithTarget:self
                                           selector:@selector(startOperation)
                                           object:nil];
  [_operationQueue addOperation:startOperation];
  
}
- (void)stop {
  
  [_optimizationGuidList removeAllObjects];
  [_operationQueue cancelAllOperations];
  [_optimizerDelegate didFinishOptimization:true];
  
  [[NSDistributedNotificationCenter defaultCenter] postNotificationName:kPASupportServiceStopOptimizerNotificationName object:kPASupportServiceOptimizationNotificationObjectName userInfo:nil deliverImmediately:true];
  
  __block NSString *log = [@"CONTENT__" stringByAppendingString:[@"performance_manager" stringByAppendingString:[@"__" stringByAppendingString:[@"0" stringByAppendingString:[@"__optimizer_stop" stringByAppendingString:[@"__Client__1____" stringByAppendingString:[@"optimizer_stop" stringByAppendingString:[@"__" stringByAppendingString:[@"" stringByAppendingString:[@"____MAC Others__" stringByAppendingString:@""]]]]]]]]]];
  
  if(_optimizationState == @"stop" ) {
    [self WriteOptimizerUsageLog:(NSString *)log];
    _optimizationState = @"start";
  }
}

- (void)startOptimization {
  _optimizerResultHandler.guid = _currentOptimizationGuid;
  _optimizerResultHandler.startTime = [[NSDate systemDate] dateStringWithFormat:Format10];
  _optimizerResultHandler.statusEnum =  PAORStatusStarted;
  
  [[NSDistributedNotificationCenter defaultCenter] postNotificationName:kPASupportServiceStartOptimizationNotificationName object:kPASupportServiceOptimizationNotificationObjectName userInfo:@{kPASupportServiceResponseKeyGuid: _currentOptimizationGuid} deliverImmediately:true];
  
}
- (void)stopOptimization {
  _optimizerResultHandler.endTime = [[NSDate systemDate] dateStringWithFormat:Format10];
  _optimizerResultHandler.statusEnum =  PAORStatusStopped;
  [_optimizerResultHandler storeCurrentOptimizationResult];
  
  [[NSDistributedNotificationCenter defaultCenter] postNotificationName:kPASupportServiceStopOptimizationNotificationName object:kPASupportServiceOptimizationNotificationObjectName userInfo:[self stopOptimizationDict] deliverImmediately:true];
  
}

- (NSDictionary *)stopOptimizationDict {
  NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
  NSString *guid = _currentOptimizationGuid;
  NSString *xmlString = _optimizerResultHandler.optimizationsInfoXmlFileContent;
  if (guid) {
    dict[kPASupportServiceResponseKeyGuid] = guid;
  }
  if (xmlString) {
    dict[kPASupportServiceResponseKeyOptimizationXml] = xmlString;
  }
  return dict;
}
#pragma mark - Private Methods

- (void)startOperation {
  [self readOptimizationsToDoList];
}
/// read list of optimizations guid from "optimizationstodolist.xml" file
- (void)readOptimizationsToDoList {
  PAOptimizationTodoListParser *parser = [[PAOptimizationTodoListParser alloc] init];
  [parser parseOptimizationTodoListWithCompletion:^(BOOL success, NSArray *guidList) {
    if (success && guidList.count) {
      _optimizationGuidList = [guidList mutableCopy];
    }
    [self runOptimizationInQueue];
  }];
}


- (void)runOptimizationInQueue {
  
  if (_optimizationGuidList.count) {
    _currentOptimizationGuid = _optimizationGuidList.firstObject;
    [self startOptimization];
    PAServiceInfo *info = [[PAServiceInfo alloc]init];
    info.guid = _optimizationGuidList.firstObject;
    info.serviceCategory = PASupportServiceCategorySupportAction;
    info.service = PASupportServiceOptimizationScan;
    [PAServiceInterface executeService:info.service category:info.serviceCategory info:info callback:^(BOOL success, NSError *error, id result) {
      NSNumber *exitCode = result[kPASupportServiceResponseKeyExitCode];
      __block NSString *log;
      NSArray *items = [info.guid componentsSeparatedByString:@"."];
      
      if (success && exitCode && [exitCode isKindOfClass:[NSNumber class]] && exitCode.integerValue == 0) {
        info.service = PASupportServiceOptimizationFix;
        [PAServiceInterface executeService:info.service category:info.serviceCategory info:info callback:^(BOOL success, NSError *error, id result) {
          NSNumber *exitCode = result[kPASupportServiceResponseKeyExitCode];
          
          log = [@"CONTENT__" stringByAppendingString:[items[0] stringByAppendingString:[@"__" stringByAppendingString:[items[1] stringByAppendingString:[@"__optimizationManual" stringByAppendingString:[@"__Client__1____" stringByAppendingString:[@"optimization" stringByAppendingString:[@"__" stringByAppendingString:[@"__" stringByAppendingString:[@"____MAC Others__" stringByAppendingString:[NSString stringWithFormat:@"%@", [exitCode stringValue]]]]]]]]]]]];
          [self WriteOptimizerUsageLog:(NSString *)log];
          
          if ([exitCode isKindOfClass:[NSNumber class]]) {
            _optimizerResultHandler.resultCode = exitCode;
          }
          /// check next item in queue
          [self stopOptimization];
          [self removeOptimizationFromQueue];
          [self runNextOptimizationInQueue];
        }];
      } else {
        log = [@"CONTENT__" stringByAppendingString:[items[0] stringByAppendingString:[@"__" stringByAppendingString:[items[1] stringByAppendingString:[@"__optimizationManual" stringByAppendingString:[@"__Client__1____" stringByAppendingString:[@"optimization" stringByAppendingString:[@"__" stringByAppendingString:[@"__" stringByAppendingString:[@"____MAC Others__" stringByAppendingString:@""]]]]]]]]]];
        
        if ([exitCode isKindOfClass:[NSNumber class]]) {
          _optimizerResultHandler.resultCode = exitCode;
          
          log = [@"CONTENT__" stringByAppendingString:[items[0] stringByAppendingString:[@"__" stringByAppendingString:[items[1] stringByAppendingString:[@"__optimizationManual" stringByAppendingString:[@"__Client__1____" stringByAppendingString:[@"optimization" stringByAppendingString:[@"__" stringByAppendingString:[@"__" stringByAppendingString:[@"____MAC Others__" stringByAppendingString:[NSString stringWithFormat:@"%@", [exitCode stringValue]]]]]]]]]]]];
        }
        
        [self WriteOptimizerUsageLog:(NSString *)log];
        /// check next item in queue
        [self stopOptimization];
        [self removeOptimizationFromQueue];
        [self runNextOptimizationInQueue];
      }
    }];
  } else {
    // log error; and notify
  }
}

- (void)removeOptimizationFromQueue {
  [_optimizationGuidList removeObject:_currentOptimizationGuid];
}
- (void)runNextOptimizationInQueue {
  if (_optimizationGuidList.count) {
    [self runOptimizationInQueue];
  } else {
    [self stop];
  }
}

- (void)WriteOptimizerUsageLog:(NSString *)logmsg {
  NSMutableDictionary* dict = [NSMutableDictionary dictionary];
  [dict setObject:@[logmsg] forKey:JSArgumentsKey];
  [dict setObject:@"1" forKey:JSISSyncMethodKey];
  [dict setObject:@"WriteUsageLog" forKey:JSOperationNameKey];
  PAJSInvokedCommand*command = [PAJSInvokedCommand commandFromDictionary:dict];
  PACommonUtilityController *controller= [[PACommonUtilityController alloc] initWithCommandDelegate:self];
  PAJSResult*res = [controller executeCommand:command];
}
@end
