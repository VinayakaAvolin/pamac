//
//  PATestClass.m
//  ProactiveAssistAgent
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PATestClass.h"

@implementation PATestClass

- (NSString *)xmlString {
  NSMutableString *xml = [[NSMutableString alloc] initWithString:@""];
  
  [xml appendString:@"<VALUE.OBJECTWITHPATH>\n"];
  [xml appendString:[NSString stringWithFormat:@"<INSTANCE CLASSNAME=\"%@\">\n", @"MicrosoftExcelProblemDetectors"]];
  
  [xml appendString:[NSString stringWithFormat:@"<PROPERTY NAME=\"%@\" TYPE=\"%@\">\n", @"AreMacrosDisabled",@"string"]];
  [xml appendString:[NSString stringWithFormat:@"<VALUE>%@</VALUE>\n", @"True"]];
  [xml appendString:@"</PROPERTY>\n"];
  
  [xml appendString:[NSString stringWithFormat:@"<PROPERTY NAME=\"%@\" TYPE=\"%@\">\n", @"AreMultipleExcelFilesOpening",@"string"]];
  [xml appendString:[NSString stringWithFormat:@"<VALUE>%@</VALUE>\n", @"False"]];
  [xml appendString:@"</PROPERTY>\n"];
  
  [xml appendString:@"</INSTANCE>\n"];
  [xml appendString:@"</VALUE.OBJECTWITHPATH>\n"];
  
  NSString *finalxml=[xml stringByReplacingOccurrencesOfString:@"&" withString:@"&amp;"];
  return finalxml;
}

@end
