//
//  PAUnreadAlertMonitor.m
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <PACoreServices/PACoreServices.h>
#import "PAUnreadAlertMonitor.h"

NSTimeInterval const kRTAUnreadAlertCheckTimerInterval = 5 * 60 ; /// 1  minute of time interval

@interface PAUnreadAlertMonitor () {
  NSOperationQueue *_operationQueue;
  NSTimer *_unreadAlertCheckTimer;
}

@end

@implementation PAUnreadAlertMonitor

- (instancetype)init
{
  self = [super init];
  if (self) {
    _operationQueue = [[NSOperationQueue alloc]init];
  }
  return self;
}

- (void)dealloc {
  [self removeUnreadAlertCheckTimer];
}


- (void)start {
  [self addUnreadAlertCheckTimer];
}

- (void)stop {
  [self removeUnreadAlertCheckTimer];
  [_operationQueue cancelAllOperations];
}

- (void)checkForNewAlertsNow {
  [_operationQueue cancelAllOperations];
  [self removeUnreadAlertCheckTimer];
  NSInvocationOperation *startOperation = [[NSInvocationOperation alloc]
                                           initWithTarget:self
                                           selector:@selector(startOperation)
                                           object:nil];
  [_operationQueue addOperation:startOperation];
}
#pragma mark -

- (void)addUnreadAlertCheckTimer {
  dispatch_async(dispatch_get_main_queue(), ^{
    _unreadAlertCheckTimer =  [NSTimer scheduledTimerWithTimeInterval:kRTAUnreadAlertCheckTimerInterval target:self selector:@selector(unreadAlertCheckTimerFired:) userInfo:nil repeats:YES];
    
  });
}

- (void)removeUnreadAlertCheckTimer {
  dispatch_async(dispatch_get_main_queue(), ^{
    if (_unreadAlertCheckTimer.isValid) {
      [_unreadAlertCheckTimer invalidate];
    }
  });
  
}

- (void)unreadAlertCheckTimerFired:(NSTimer *)timer {
  [self checkForNewAlertsNow];
}

- (void)startOperation {
  [self checkForNewRealTimeAlerts];
  [self addUnreadAlertCheckTimer];
}


- (void)checkForNewRealTimeAlerts {
  // notify alert tray app
  NSString *alertType = kPAAlertTypeRealTimeAlert;
  
  NSString *command = [NSString stringWithFormat:@"%@ %@",kPALSCommandAlertTray, alertType];
  BOOL isSuccess = [PALaunchServiceManager runCommand:command];
  if (isSuccess) {
    [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"Check for new real time alerts:: Successfullly notified Alert Tray"];
  }
  sleep(10.0);
  [self checkForNewAlerts];
}
- (void)checkForNewAlerts {
  // notify alert tray app
  NSString *alertType = kPAAlertTypeNormalAlert;
  
  NSString *command = [NSString stringWithFormat:@"%@ %@",kPALSCommandAlertTray, alertType];
  BOOL isSuccess = [PALaunchServiceManager runCommand:command];
  if (isSuccess) {
    [[PALogger sharedLoggerForClient:PALogClientResidentAgent] debug:@"Check for new alerts:: Successfullly notified Alert Tray"];
  }
}
@end
