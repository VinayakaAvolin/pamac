//
//  PAUnreadAlertMonitor.h
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PAUnreadAlertMonitor : NSObject

- (void)start;
- (void)stop;
- (void)checkForNewAlertsNow;

@end
