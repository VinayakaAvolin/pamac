// ConsoleApplication1.cpp : Defines the entry point for the console application.
//

#include <openssl/evp.h>
#include <openssl/aes.h>
#include <openssl/bio.h>
#include <openssl/evp.h>

#include <iostream>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <assert.h>

extern "C" {
#include "PAAESDecrypt.hpp"
  
  size_t calcDecodeLength(const char* b64input) { //Calculates the length of a decoded string
    size_t len = strlen(b64input),
    padding = 0;
    
    if (b64input[len - 1] == '=' && b64input[len - 2] == '=') //last two chars are =
      padding = 2;
    else if (b64input[len - 1] == '=') //last char is =
      padding = 1;
    
    return (len * 3) / 4 - padding;
  }
  
  size_t Base64Decode(const char* b64message, unsigned char** buffer, size_t* length) { //Decodes a base64 encoded string
    BIO *bio, *b64;
    
    size_t decodeLen = calcDecodeLength(b64message);
    *buffer = (unsigned char*)malloc(decodeLen + 1);
    (*buffer)[decodeLen] = '\0';
    
    bio = BIO_new_mem_buf(b64message, -1);
    b64 = BIO_new(BIO_f_base64());
    bio = BIO_push(b64, bio);
    
    BIO_set_flags(bio, BIO_FLAGS_BASE64_NO_NL); //Do not use newlines to flush buffer
    *length = BIO_read(bio, *buffer, strlen(b64message));
    assert(*length == decodeLen); //length should equal decodeLen, else something went horribly wrong
    BIO_free_all(bio);
    
    return (0); //success
  }
  
  /**
   * Create a 256 bit key and IV using the supplied key_data. salt can be added for taste.
   * Fills in the encryption and decryption ctx objects and returns 0 on success
   **/
  int aes_init(const char *key_data, int key_data_len, unsigned char *salt, int salt_len, EVP_CIPHER_CTX *e_ctx,
               EVP_CIPHER_CTX *d_ctx)
  {
    int i, nrounds = 1000;
    unsigned char *keyIv = new unsigned char[48];
    unsigned char key[32], iv[16];
    
    /*
     * Gen key & IV for AES 256 CBC mode. A SHA1 digest is used to hash the supplied key material.
     * nrounds is the number of times the we hash the material. More rounds are more secure but
     * slower.
     */
    i = PKCS5_PBKDF2_HMAC_SHA1(key_data, key_data_len, salt, salt_len, nrounds, 48, keyIv);
    if (i != 1) {
      printf("PBKDF2 HMACSHA1 failed\n", i);
      return -1;
    }
    memcpy(key, keyIv, 32);
    memcpy(iv, keyIv+32, 16);
    
    EVP_CIPHER_CTX_init(e_ctx);
    EVP_EncryptInit_ex(e_ctx, EVP_aes_256_cbc(), NULL, key, iv);
    EVP_CIPHER_CTX_init(d_ctx);
    EVP_DecryptInit_ex(d_ctx, EVP_aes_256_cbc(), NULL, key, iv);
    
    return 0;
  }
  
  /*
   * Encrypt *len bytes of data
   * All data going in & out is considered binary (unsigned char[])
   */
  unsigned char *aes_encrypt(EVP_CIPHER_CTX *e, unsigned char *plaintext, int *len)
  {
    /* max ciphertext len for a n bytes of plaintext is n + AES_BLOCK_SIZE -1 bytes */
    int c_len = *len + AES_BLOCK_SIZE, f_len = 0;
    unsigned char *ciphertext = new unsigned char[c_len];
    
    /* allows reusing of 'e' for multiple encryption cycles */
    EVP_EncryptInit_ex(e, NULL, NULL, NULL, NULL);
    
    /* update ciphertext, c_len is filled with the length of ciphertext generated,
     *len is the size of plaintext in bytes */
    EVP_EncryptUpdate(e, ciphertext, &c_len, plaintext, *len);
    
    /* update ciphertext with the final remaining bytes */
    EVP_EncryptFinal_ex(e, ciphertext + c_len, &f_len);
    
    *len = c_len + f_len;
    return ciphertext;
  }
  
  /*
   * Decrypt *len bytes of ciphertext
   */
  unsigned char *aes_decrypt(EVP_CIPHER_CTX *e, unsigned char *ciphertext, int *len)
  {
    /* plaintext will always be equal to or lesser than length of ciphertext*/
    int p_len = *len, f_len = 0;
    unsigned char *plaintext = new unsigned char[p_len];
    
    EVP_DecryptInit_ex(e, NULL, NULL, NULL, NULL);
    EVP_DecryptUpdate(e, plaintext, &p_len, ciphertext, *len);
    EVP_DecryptFinal_ex(e, plaintext + p_len, &f_len);
    
    *len = p_len + f_len;
    return plaintext;
  }
  
  
  //int main()
  char *AESDecrypt(const char *encryptedStr, const char *keyData)
  {
    char *plaintext;
    
    try {
    EVP_CIPHER_CTX *en = EVP_CIPHER_CTX_new(),*de = EVP_CIPHER_CTX_new();
    //size_t key_data_len = strlen(keyData);
    int keyDataLen = strlen(keyData);
    char salt[100] = {0};
    for(int count = 0; count < 8; count++) {
      snprintf(salt, 100, "%s%d", salt, keyDataLen);
    }
    int saltLen = strlen(salt);
    size_t decodedBytesLen;
    unsigned char *decodedBytes = NULL;
    Base64Decode(encryptedStr, (unsigned char**)&decodedBytes, &decodedBytesLen);
    
    /* gen key and iv. init the cipher ctx object */
    if (aes_init(keyData, keyDataLen, (unsigned char *)salt, saltLen, en, de)) {
      std::cout << "Couldn't initialize AES cipher" << std::endl;;
      return NULL;
    }
    
    int decryptedLen = decodedBytesLen;
    
    plaintext = (char *)aes_decrypt(de, decodedBytes, &decryptedLen);
    
    plaintext[decryptedLen] = '\0';
    
    std::cout << "Decrypted text = " << plaintext << std::endl;
    //free(plaintext);
    free(decodedBytes);
    EVP_CIPHER_CTX_free(de);
    EVP_CIPHER_CTX_free(en);
      
    } catch (const std:: exception &e) {
      plaintext = NULL;
    } catch (...) {
      plaintext = NULL;
    }
    return plaintext;
  }
  
}

