//
//  PAAESDecrypt11.hpp
//  OpenSSLDecrypt
//
//  Copyright © 2018 Avolin. All rights reserved.
//

#ifndef PAAESDecrypt1_hpp
#define PAAESDecrypt1_hpp

char *AESDecrypt(const char *encryptedStr, const char *keyData);


#endif /* PAAESDecrypt1_hpp */
