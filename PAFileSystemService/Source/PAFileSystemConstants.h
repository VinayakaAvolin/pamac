//
//  PAFileSystemConstants.h
//  PAFileSystemService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifndef PAFileSystemConstants_h
#define PAFileSystemConstants_h

extern NSString *const kPADataSetInnerFileNameRegEx;
extern NSString *const kPADataSetInnerFileNameRegEx1;
extern NSString *const kPADataSetInnerFileNameRegEx2;

typedef enum : NSUInteger {
  PAFileModeRead = 1,
  PAFileModeWrite = 2,
  PAFileModeAppend = 3
}PAFileModeType;

#endif /* PAFileSystemConstants_h */
