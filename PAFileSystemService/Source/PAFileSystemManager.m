//
//  PAFileSystemManager.m
//  PAFileSystemService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PAFileSystemManager.h"
#import "PAFileSystemUtility.h"
#import "PAItemAttributes.h"
#import "PAFileSystemConstants.h"

@interface PAFileSystemManager () {
  PAAppConfiguration *_appconfigurations;
}
@end


@implementation PAFileSystemManager

- (instancetype)init
{
  self = [super init];
  if (self) {
    [self readConfigurations];
  }
  return self;
}

- (PAAppConfiguration *)applicationConfigurations {
  return _appconfigurations;
}

- (void)readConfigurations {
  _appconfigurations = [PAConfigurationManager appConfigurations];
}

- (BOOL)createDirectoryAtPath:(NSString *_Nullable)path withIntermediateDirectories:(BOOL)createIntermediates attributes:(nullable NSDictionary<NSString *, id> *)attributes error:(NSError *_Nullable*_Nullable)error {
  return [PAFileSystemUtility checkAndCreateItemAtPath:path isDirectory:YES shouldOverwrite:NO withIntermediateDirectories:createIntermediates attributes:attributes error:error];
}
- (BOOL)createFileAtPath:(NSString *_Nullable)path
              attributes:(nullable NSDictionary<NSString *, id> *)attributes
                   error:(NSError *_Nullable*_Nullable)error {
  return [PAFileSystemUtility checkAndCreateItemAtPath:path isDirectory:NO shouldOverwrite:NO withIntermediateDirectories:NO attributes:attributes error:error];
}

- (NSString *)createFileAtPath:(NSString *_Nullable)path
                    attributes:(nullable NSDictionary<NSString *, id> *)attributes
               shouldOverwrite:(BOOL)bOverwrite
                         error:(NSError *_Nullable*_Nullable)error {
  BOOL retVal = [PAFileSystemUtility checkAndCreateItemAtPath:path isDirectory:NO shouldOverwrite:bOverwrite withIntermediateDirectories:NO attributes:attributes error:error];
  return retVal == YES ? path: @"";
}
- (BOOL)doesFileExist:(NSString *_Nonnull)filePath {
  return [PAFileSystemUtility doesItemExistAtPath:filePath];
}

- (BOOL)doesDirectoryExist:(NSString *_Nonnull)directoryPath {
  return [PAFileSystemUtility doesItemExistAtPath:directoryPath];
}
- (NSString *_Nullable)appRootPath {
  NSString *mainDirectoryPath = [_appconfigurations.userRoot stringByExpandingTildeInPath];
  NSString *appFolderName = _appconfigurations.appFolder;
  NSString *appSpecificFolderPath = [mainDirectoryPath stringByAppendingPathComponent:appFolderName];
  if ([PAFileSystemUtility checkAndCreateItemAtPath:appSpecificFolderPath isDirectory:YES shouldOverwrite:NO withIntermediateDirectories:NO attributes:nil error:nil]) {
    return appSpecificFolderPath;
  }
  return nil;
}
- (NSString *_Nullable)systemAppRootPath {
  NSString *mainDirectoryPath = [_appconfigurations.systemRoot stringByExpandingTildeInPath];
  NSString *appFolderName = _appconfigurations.appFolder;
  NSString *appSpecificFolderPath = [mainDirectoryPath stringByAppendingPathComponent:appFolderName];
  if ([PAFileSystemUtility checkAndCreateItemAtPath:appSpecificFolderPath isDirectory:YES shouldOverwrite:NO withIntermediateDirectories:NO attributes:nil error:nil]) {
    return appSpecificFolderPath;
  }
  return nil;
}
- (NSString *_Nullable)appRootFileSystemPath {
  NSString *appSpecificFolderPath = [PAFileSystemUtility userLocalFileSystemPathWithConfiguration:_appconfigurations];
  if ([PAFileSystemUtility checkAndCreateItemAtPath:appSpecificFolderPath isDirectory:YES shouldOverwrite:NO withIntermediateDirectories:YES attributes:nil error:nil]) {
    return appSpecificFolderPath;
  }
  return nil;
}
- (NSString *_Nullable)appDataFolderPath {
  NSString *appSpecificFolderPath = [PAFileSystemUtility appDataFolderPathWithConfiguration:_appconfigurations];
  if ([PAFileSystemUtility checkAndCreateItemAtPath:appSpecificFolderPath isDirectory:YES shouldOverwrite:NO withIntermediateDirectories:YES attributes:nil error:nil]) {
    return appSpecificFolderPath;
  }
  return nil;
}
- (NSString *_Nonnull)serverRootPath{
  return _appconfigurations.serverRoot;
}
- (NSString *_Nonnull)serverBasePath {
  return _appconfigurations.serverBaseUrlPath;
}
- (NSString *_Nonnull)temporaryDownloadsFolderPath {
  return [PAFileSystemUtility tempDownloadsDirectoryPathWithAppConfiguration:_appconfigurations];
}

- (NSString *_Nonnull)temporaryManifestFilePath {
  NSString *manifestFilePath = [self temporaryDownloadsFolderPath];
  NSString *manifestFile = _appconfigurations.manifestXmlFile;
  manifestFilePath = [manifestFilePath stringByAppendingPathComponent:manifestFile];
  return manifestFilePath;
}
- (NSString *_Nullable)localManifestFilePath {
  NSString *manifestFilePath = [self appDataFolderPath];
  NSString *manifestFile = _appconfigurations.manifestXmlFile;
  manifestFilePath = [manifestFilePath stringByAppendingPathComponent:manifestFile];
  return manifestFilePath;
}
- (NSString *_Nullable)configFilePath {
  NSString *cfgFilePath = [self appDataFolderPath];
  NSString *cfgFile = _appconfigurations.configCfgFile;
  cfgFilePath = [cfgFilePath stringByAppendingPathComponent:cfgFile];
  return cfgFilePath;
}
- (NSString *_Nonnull)remoteManifestFilePath {
  NSString* manifestFilePath = [self serverRootPath];
  NSString *manifestFile = _appconfigurations.manifestZipFile;
  NSString *dataFolder = @"data";
  manifestFilePath = [manifestFilePath stringByAppendingPathComponent:dataFolder];
  manifestFilePath = [manifestFilePath stringByAppendingPathComponent:manifestFile];
  return manifestFilePath;
}
- (NSString *_Nullable)logsFileDirectoryPath {
  NSString *logsFolderPath = _appconfigurations.logsFolderPath;
  logsFolderPath = [PAFileSystemUtility stringByExpandingPath:logsFolderPath configuration:_appconfigurations];
  if (logsFolderPath) {
    if ([PAFileSystemUtility checkAndCreateItemAtPath:logsFolderPath isDirectory:YES shouldOverwrite:NO withIntermediateDirectories:YES attributes:nil error:nil]) {
      return logsFolderPath;
    }
  }
  
  return nil;
  
  //    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
  //    NSString *documentsDirectory = [paths objectAtIndex:0];
  //    return documentsDirectory;
}

- (NSString *_Nullable)dataBackupFolderPath {
  NSString *backupPath = _appconfigurations.backupFolderPath;
  backupPath = [PAFileSystemUtility stringByExpandingPath:backupPath configuration:_appconfigurations];
  if (backupPath) {
    if ([PAFileSystemUtility checkAndCreateItemAtPath:backupPath isDirectory:YES shouldOverwrite:NO withIntermediateDirectories:YES attributes:nil error:nil]) {
      return backupPath;
    }
  }
  
  return nil;
}
- (NSString *_Nullable)smartIssuesFolderPath {
  NSString *issuesPath = _appconfigurations.smartIssuesFolderPath;
  issuesPath = [PAFileSystemUtility stringByExpandingPath:issuesPath configuration:_appconfigurations];
  if (issuesPath) {
    if ([PAFileSystemUtility checkAndCreateItemAtPath:issuesPath isDirectory:YES shouldOverwrite:NO withIntermediateDirectories:YES attributes:nil error:nil]) {
      return issuesPath;
    }
  }
  return nil;
}

- (NSString *_Nullable)snapinFolderPath {
  NSString *snapinPath = _appconfigurations.snapinFolderPath;
  snapinPath = [PAFileSystemUtility stringByExpandingPath:snapinPath configuration:_appconfigurations];
  if ([self doesDirectoryExist:snapinPath]) {
    return snapinPath;
  }
  return nil;
}

- (NSString *_Nullable)realTimeAlertFolderPath {
  NSString *rtAlertPath = _appconfigurations.realTimeAlertsFolderPath;
  rtAlertPath = [PAFileSystemUtility stringByExpandingPath:rtAlertPath configuration:_appconfigurations];
  if (rtAlertPath) {
    if ([PAFileSystemUtility checkAndCreateItemAtPath:rtAlertPath isDirectory:YES shouldOverwrite:NO withIntermediateDirectories:YES attributes:nil error:nil]) {
      return rtAlertPath;
    }
  }
  return nil;
}

- (NSString *_Nullable)optimizationFolderPath {
  NSString *optimPath = _appconfigurations.optimizationFolderPath;
  optimPath = [PAFileSystemUtility stringByExpandingPath:optimPath configuration:_appconfigurations];
  if ([self doesDirectoryExist:optimPath]) {
    return optimPath;
  }
  return nil;
}

- (NSString *_Nullable)supportActionFolderPath {
  NSString *saPath = _appconfigurations.supportActionFolderPath;
  saPath = [PAFileSystemUtility stringByExpandingPath:saPath configuration:_appconfigurations];
  if ([self doesDirectoryExist:saPath]) {
    return saPath;
  }
  return nil;
}

- (NSString *_Nullable)messagesFolderPath {
  NSString *msgPath = _appconfigurations.messagesFolderPath;
  msgPath = [PAFileSystemUtility stringByExpandingPath:msgPath configuration:_appconfigurations];
  if ([self doesDirectoryExist:msgPath]) {
    return msgPath;
  }
  return nil;
}

- (NSString *_Nullable)privilegeToolHelperPath {
  NSString *saPath = _appconfigurations.privilegeToolHelperPath;
  saPath = [PAFileSystemUtility stringByExpandingPath:saPath configuration:_appconfigurations];
  return saPath;
}

- (NSString *_Nullable)agentCoreFolderPath {
  NSString *agentCorePath = _appconfigurations.agentCoreFolderPath;
  agentCorePath = [PAFileSystemUtility stringByExpandingPath:agentCorePath configuration:_appconfigurations];
  if ([self doesDirectoryExist:agentCorePath]) {
    return agentCorePath;
  }
  return nil;
}
- (NSString *_Nullable)hkcuRegistryPlistPath {
  NSString *hkcuPlistPath = _appconfigurations.hkcuRegistryPath;
  hkcuPlistPath = [PAFileSystemUtility stringByExpandingPath:hkcuPlistPath configuration:_appconfigurations];
  return hkcuPlistPath;
}

- (NSString *_Nullable)hklmRegistryPlistPath {
    NSString *hklmPlistPath = _appconfigurations.hklmRegistryPath;
    hklmPlistPath = [PAFileSystemUtility stringByExpandingPath:hklmPlistPath configuration:_appconfigurations];
    return hklmPlistPath;
}
- (NSString * _Nullable)selfHealRegistryPlistPath {
    NSString *selfHealRegistryPlistPath = _appconfigurations.selfHealRegistryPlistPath;
    selfHealRegistryPlistPath = [PAFileSystemUtility stringByExpandingPath:selfHealRegistryPlistPath configuration:_appconfigurations];
    return selfHealRegistryPlistPath;
}

- (NSString * _Nullable)selfHealTriggerFolderPath {
    NSString *wmiTriggerPath = _appconfigurations.selfHealTriggerFolderPath;
    wmiTriggerPath = [PAFileSystemUtility stringByExpandingPath:wmiTriggerPath configuration:_appconfigurations];
    if ([self doesDirectoryExist:wmiTriggerPath]) {
        return wmiTriggerPath;
    }
    return nil;
}

- (NSArray * _Nullable)selfHealMonitorPluginPaths {
    NSArray * pluginPaths = _appconfigurations.selfHealMonitorPluginPaths;
    NSMutableArray *paths = [NSMutableArray new];
    for (NSString *path in pluginPaths) {
        [paths addObject:[PAFileSystemUtility stringByExpandingPath: path configuration: _appconfigurations]];
    }
    return [paths copy];
}

- (NSString *_Nullable)clientUiConfigFileRootPath {
  NSString *configfileTargetFolderPath = _appconfigurations.clientUiConfigFilePath;
  configfileTargetFolderPath = [PAFileSystemUtility stringByExpandingPath:configfileTargetFolderPath configuration:_appconfigurations];
  return configfileTargetFolderPath;
}
- (NSString *_Nullable)localDefaultXmlFilePath {
  NSString *defaultXmlFilePath = _appconfigurations.macDefaultXmlFilePath;
  defaultXmlFilePath = [PAFileSystemUtility stringByExpandingPath:defaultXmlFilePath configuration:_appconfigurations];
  return defaultXmlFilePath;
}
- (NSString *_Nullable)userAgentBinFolderPath {
  NSString *agentBinPath = _appconfigurations.userAgentBinFolderPath;
  agentBinPath = [PAFileSystemUtility stringByExpandingPath:agentBinPath configuration:_appconfigurations];
  if ([PAFileSystemUtility checkAndCreateItemAtPath:agentBinPath isDirectory:YES shouldOverwrite:NO withIntermediateDirectories:YES attributes:nil error:nil]) {
    return agentBinPath;
  }
  return nil;
}
- (NSString *_Nullable)syncCertificatePath {
  /// TODO: Change this path
  NSString *certPath = _appconfigurations.syncCertificatePath;
  certPath = [PAFileSystemUtility stringByExpandingPath:certPath configuration:_appconfigurations];
  return certPath;
}
- (NSString *_Nullable)supportActionCertificatePath {
  /// TODO: Change this path
  NSString *certPath = _appconfigurations.supportActionCertificatePath;
  certPath = [PAFileSystemUtility stringByExpandingPath:certPath configuration:_appconfigurations];
  return certPath;
}

- (NSString *_Nullable)alertRunListFilePath {
  NSString *filePath = _appconfigurations.alertRunListFilePath;
  filePath = [PAFileSystemUtility stringByExpandingPath:filePath configuration:_appconfigurations];
  NSString *lastRunFolderPath = [filePath stringByDeletingLastPathComponent];
  if ([PAFileSystemUtility checkAndCreateItemAtPath:lastRunFolderPath isDirectory:YES shouldOverwrite:NO withIntermediateDirectories:YES attributes:nil error:nil]) {
    return filePath;
  }
  return nil;
}
- (NSString *_Nullable)realTimeAlertRunListFilePath {
  NSString *filePath = _appconfigurations.realTimeAlertRunListFilePath;
  filePath = [PAFileSystemUtility stringByExpandingPath:filePath configuration:_appconfigurations];
  NSString *lastRunFolderPath = [filePath stringByDeletingLastPathComponent];
  if ([PAFileSystemUtility checkAndCreateItemAtPath:lastRunFolderPath isDirectory:YES shouldOverwrite:NO withIntermediateDirectories:YES attributes:nil error:nil]) {
    return filePath;
  }
  return nil;
}

- (NSString *_Nullable)alertsFolderPath {
  NSString *alertPath = _appconfigurations.alertsFolderPath;
  alertPath = [PAFileSystemUtility stringByExpandingPath:alertPath configuration:_appconfigurations];
  if ([self doesDirectoryExist:alertPath]) {
    return alertPath;
  }
  return nil;
}

- (NSString *_Nullable)stateFolderPath {
  NSString *statePath = _appconfigurations.stateFolderPath;
  statePath = [PAFileSystemUtility stringByExpandingPath:statePath configuration:_appconfigurations];
  if ([self doesDirectoryExist:statePath]) {
    return statePath;
  }
  return nil;
}

#pragma mark -
- (BOOL)moveDirectory:(NSString *_Nonnull)srcPath destinationPath:(NSString *_Nonnull)dstPath {
  return [PAFileSystemUtility moveItem:srcPath destinationPath:dstPath];
}
- (BOOL)moveFile:(NSString *_Nonnull)srcPath destinationPath:(NSString *_Nonnull)dstPath {
  NSString *srcFileName = [srcPath lastPathComponent];
  NSString *dstFileName = [dstPath lastPathComponent];
  BOOL fileDeleted = true;
  // Both src and dst path includes file name
  // Before copying, check if file at destination path exists. If exists remove the file and then copy.
  if ([srcFileName isEqualToString:dstFileName]) {
    if ([self doesFileExist:dstPath]) {
      fileDeleted = [self deleteFile:dstPath];
    }
  }
  if (fileDeleted) {
    return [PAFileSystemUtility moveItem:srcPath destinationPath:dstPath];
  }
  return false;
}
- (BOOL)copyDirectory:(NSString *_Nonnull)srcPath destinationPath:(NSString *_Nonnull)dstPath {
  return [PAFileSystemUtility copyItem:srcPath destinationPath:dstPath];
}
- (BOOL)copyFile:(NSString *_Nonnull)srcPath destinationPath:(NSString *_Nonnull)dstPath {
  NSString *srcFileName = [srcPath lastPathComponent];
  NSString *dstFileName = [dstPath lastPathComponent];
  BOOL fileDeleted = true;
  // Both src and dst path includes file name
  // Before copying, check if file at destination path exists. If exists remove the file and then copy.
  if ([srcFileName isEqualToString:dstFileName]) {
    if ([self doesFileExist:dstPath]) {
      fileDeleted = [self deleteFile:dstPath];
    }
  }
  if (fileDeleted) {
    return [PAFileSystemUtility copyItem:srcPath destinationPath:dstPath];
  }
  return false;
}
- (BOOL)renameDirectory:(NSString *_Nonnull)srcPath newFileName:(NSString *_Nonnull)newName {
  return [PAFileSystemUtility renameItem:srcPath newFileName:newName];
}
- (BOOL)renameFile:(NSString *_Nonnull)srcFilePath newFileName:(NSString *_Nonnull)newName {
  return [PAFileSystemUtility renameItem:srcFilePath newFileName:newName];
}
- (BOOL)deleteDirectory:(NSString *_Nonnull)srcPath {
  return [PAFileSystemUtility deleteItem:srcPath];
}
- (BOOL)deleteFile:(NSString *_Nonnull)srcPath{
  return [PAFileSystemUtility deleteItem:srcPath];
}
- (NSString *_Nonnull)stringByExpandingPath:(NSString *_Nonnull)inputFilePath {
  return [PAFileSystemUtility stringByExpandingPath:inputFilePath configuration:_appconfigurations];
}

- (NSArray *_Nullable)contentsOfDirectory:(NSString *_Nonnull)dirPath {
  NSArray *dirContents;
  if ( [PAFileSystemUtility doesItemExistAtPath:dirPath] ) {
    dirContents = [PAFileSystemUtility contentsOfDirectory:dirPath];
  }
  return dirContents;
}

- (NSString *_Nonnull)appDataFolderByAddingSubPath:(NSString *_Nonnull)subPath {
  NSString *datafolderPath = [self appDataFolderPath];
  NSString *finalPath = [datafolderPath stringByAppendingPathComponent:subPath];
  return finalPath;
}

- (BOOL) isFile:(NSString *_Nonnull)filePath {
  return [PAFileSystemUtility isFile:filePath];
}
- (BOOL)verifyChecksum:(NSString *_Nonnull)checksum
               forItem:(NSString *_Nonnull)itemPath {
  return true;
}
- (PAItemAttributes *_Nullable)attributesOfItemAtPath:(NSString *_Nonnull)itemPath {
  PAItemAttributes *attributes;
  if ( [PAFileSystemUtility doesItemExistAtPath:itemPath] ) {
    NSError *error;
    NSDictionary *attributesDict = [[NSFileManager defaultManager] attributesOfItemAtPath:itemPath error:&error];
    if ( nil == error && nil != attributesDict ) {
      attributes = [[PAItemAttributes alloc] initWithFileAttributes:attributesDict];
    }
  }
  return attributes;
}

- (BOOL) writeToFileAtPath:(NSString *_Nonnull)filePath data:(NSData *_Nonnull)data {
  return [self writeToFileAtPath:filePath data:data withMode:PAFileModeAppend];
}

- (BOOL) writeToFileAtPath:(NSString *_Nonnull)filePath data:(NSData *_Nonnull)data withMode:(PAFileModeType)mode {
  /// first create the folder under which the file has to be created
  NSString *folderPath = [filePath stringByDeletingLastPathComponent];
  BOOL isFolderPresent = [PAFileSystemUtility checkAndCreateItemAtPath:folderPath isDirectory:YES shouldOverwrite:NO withIntermediateDirectories:YES attributes:nil error:nil];
  if (!isFolderPresent) {
    return false;
  }
  NSFileHandle *fileHandle = [NSFileHandle fileHandleForWritingAtPath:filePath];
  if (nil != data && nil != fileHandle) {
    if (mode == PAFileModeAppend) {
      [fileHandle seekToEndOfFile];
    }
    [fileHandle writeData:data];
    [fileHandle synchronizeFile];
    [fileHandle closeFile];
    return true;
  }
  return false;
}

- (NSData *_Nullable)contentOfFileAtPath:(NSString *_Nonnull)filePath {
  if (![PAFileSystemUtility doesItemExistAtPath:filePath] ||
      ![PAFileSystemUtility isFile:filePath]) {
    return nil;
  }
  return [[NSFileManager defaultManager] contentsAtPath:filePath];
}

- (NSArray *_Nullable)subDirectoriesOfDirectory:(NSString*)dirPath {
  return [PAFileSystemUtility subDirectoriesOfDirectory:dirPath];
}

- (NSArray *_Nullable)filesOfDirectory:(NSString *_Nonnull)dirPath {
  return [PAFileSystemUtility filesOfDirectory:dirPath];
}

- (NSString *_Nullable)getMatchingFileName:(NSString *_Nonnull)itemName
                           inBaseDirectory:(NSString *_Nonnull)baseDirectory{
  
  NSArray *dirContents = [self contentsOfDirectory:baseDirectory];
  if (dirContents.count) {
    NSArray *matchingItemNames = [self itemNameMatching:itemName inList:dirContents];
    for (NSString *matchingItemName in matchingItemNames) {
      NSString *itemPath = [baseDirectory stringByAppendingPathComponent:matchingItemName];
      NSString *itemNameWithExtension = [itemPath lastPathComponent];
      NSString *itemPathWithoutExtension = [itemPath stringByDeletingPathExtension];
      NSString *itemNameWithoutExtension = [itemPathWithoutExtension lastPathComponent];
      if ([itemNameWithoutExtension isEqualToString: itemName]) {
        return itemNameWithExtension;
      }
    }
  }
  return nil;
}

- (NSString *_Nullable)assistAgentFolderPath {
  NSString *assistAgentPath = _appconfigurations.assistAgentFolderPath;
  assistAgentPath = [PAFileSystemUtility stringByExpandingPath:assistAgentPath configuration:_appconfigurations];
  if ([self doesDirectoryExist:assistAgentPath]) {
    return assistAgentPath;
  }
  return nil;
}

#pragma mark - Private methods
-(NSArray *)itemNameMatching:(NSString *)name inList:(NSArray *)dirContents {
  NSPredicate *predicate = [NSPredicate predicateWithFormat:
                            @"SELF BEGINSWITH[cd] %@", name];
  NSArray *filtered = [dirContents filteredArrayUsingPredicate:predicate];
  if (filtered.count) {
    return filtered;
  }
  return nil;
}

@end
