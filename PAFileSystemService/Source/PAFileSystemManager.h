//
//  PAFileSystemManager.h
//  PAFileSystemService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <PAConfigurationService/PAConfigurationService.h>
#import "PAFileSystemConstants.h"

@class PAItemAttributes;

typedef void(^PAFileSystemManagerCompletionBlock)(BOOL);

@interface PAFileSystemManager : NSObject

@property (nonatomic) PAAppConfiguration *_Nonnull applicationConfigurations;

/**
 Returns directory path where all application specific data will reside
 Eg: ~/Library/Application\ Support/ProactiveAssist/
 
 @return NSString *
 */
- (NSString *_Nullable)appRootPath;
/**
 Returns directory path where all application specific data will reside
 Eg: /Library/Application\ Support/ProactiveAssist/
 
 @return NSString *
 */
- (NSString *_Nullable)systemAppRootPath;

/**
 Returns directory path where all the contents downloaded as part of sync will reside
 Eg: ~/Library/Application\ Support/ProactiveAssist/Experience92
 
 @return NSString *
 */
- (NSString *_Nullable)appRootFileSystemPath;

/**
 Returns directory path where all the data of the app reside
 Eg: ~/Library/Application\ Support/ProactiveAssist/Experience92/data
 
 @return NSString *
 */
- (NSString *_Nullable)appDataFolderPath;
/**
 Returns server url to fetch app data like manifest and snapins
 
 @return NSString *
 */
- (NSString *_Nonnull)serverRootPath;
/**
 Returns base url to server
 
 @return NSString *
 */
- (NSString *_Nonnull)serverBasePath;

/**
 Returns downloads folder path; all temporary downloads happen here;
 
 @return NSString *
 */
- (NSString *_Nonnull)temporaryDownloadsFolderPath;
/**
 Returns temporary path to manifest file where it gets downloaded initially during sync;
 
 @return NSString *
 */
- (NSString *_Nonnull)temporaryManifestFilePath;
/**
 Returns path to manifest file where it gets saved finally during sync;
 
 @return NSString *
 */
- (NSString *_Nullable)localManifestFilePath;
/**
 Returns remote server path to manifest file; to download during sync;
 
 @return NSString *
 */
- (NSString *_Nonnull)remoteManifestFilePath;
/**
 Returns path to config.cfg file
 Eg: ~/Library/Application Support/ProactiveAssist/Experience92/data/config.cfg
 
 @return NSString *
 */
- (NSString *_Nullable)configFilePath;

/**
 Returns path to root directory where all app logs will be stored;
 
 @return NSString *
 */
- (NSString *_Nullable)logsFileDirectoryPath;

/**
 Returns backup folder path
 Eg: ~/Library/Application\ Support/ProactiveAssist/Experience92/state/backup
 
 @return NSString *
 */
- (NSString *_Nullable)dataBackupFolderPath;

/**
 Returns path to directory where smart issues files are created;
 
 @return NSString *
 */
- (NSString *_Nullable)smartIssuesFolderPath;
/**
 Returns path to directory where snapins are located;
 
 @return NSString *
 */
- (NSString *_Nullable)snapinFolderPath;

/**
 Returns path to directory where real time alert contents are located;
 
 @return NSString *
 */
- (NSString *_Nullable)realTimeAlertFolderPath;

/**
 Returns path to directory where optimization contents are located;
 
 @return NSString *
 */
- (NSString *_Nullable)optimizationFolderPath;

/**
 Returns path to directory where UI related js files are located;
 Eg: /Applications/ProactiveAssist.app/Contents/AgentCore/HTML
 
 @return NSString *
 */
- (NSString *_Nullable)agentCoreFolderPath;

/**
 Returns path to HKCU_Registry plist;
 Eg: ~/Library/Application Support/ProactiveAssist/Experience92/agent/bin/HKCU_Registry.plist
 @return NSString *
 */
- (NSString *_Nullable)hkcuRegistryPlistPath;

/**
 Returns path to HKLM_Registry plist
 Eg: /Library/Application\ Support/ProactiveAssist/HKLM_Registry.plist
 @return NSString *
 */
- (NSString *_Nullable)hklmRegistryPlistPath;

/**
 Returns path to file where currently monitored self heal events are stored
 Eg: ~/Library/Application Support/ProactiveAssist/Experience92/agent/bin/Monitored_Events_Registry.plist
 @return NSString *
 */
- (NSString * _Nullable)selfHealRegistryPlistPath;
/**
 Returns path to directory where bcont_nm.plist, clientui_config.xml, clientuimini_config.xml, minibcont.plist files are located;
 Eg: /Applications/ProactiveAssist.app/Contents/Resources
 @return NSString *
 */
- (NSString *_Nullable)clientUiConfigFileRootPath;
/**
 Returns the data folder path after appending the input subpath
 Eg: ~/Library/Application\ Support/ProactiveAssist/Experience92/data/65aabe47-988e-482b-81bb-34d59e2c606f
 
 @param subPath The subpath to be appended to the data folder path
 @return NSString*
 */
- (NSString *_Nonnull)appDataFolderByAddingSubPath:(NSString *_Nonnull)subPath;


/**
 The path after expanding server/local folder placeholders in it (if present)
 
 @param inputFilePath The path to the file or directory with a placeholders for local/server path
 @return The path to the directory or the file after expanding the placeholders in it; if no placeholders present then returns inputFilePath itself
 */
- (NSString *_Nonnull)stringByExpandingPath:(NSString *_Nonnull)inputFilePath;

/**
 Returns path to default xml file;
 
 @return NSString *
 */
- (NSString *_Nullable)localDefaultXmlFilePath;

/**
 Returns agent bin folder path
 Eg: ~/Library/Application\ Support/ProactiveAssist/Experience92/agent/bin
 
 @return NSString *
 */
- (NSString *_Nullable)userAgentBinFolderPath;
/**
 Returns sync certificate path
 
 @return NSString *
 */
- (NSString *_Nullable)syncCertificatePath;
/**
 Returns support action certificate path
 
 @return NSString *
 */
- (NSString *_Nullable)supportActionCertificatePath;
/**
 Returns path to support action folder "sprt_actionlight"
 
 @return NSString *
 */
- (NSString *_Nullable)supportActionFolderPath;
/**
 Returns path to support message folder "sprt_msg"
 
 @return NSString *
 */
- (NSString *_Nullable)messagesFolderPath;
/**
 Returns path to privilege tool helper
 
 @return NSString *
 */
- (NSString *_Nullable)privilegeToolHelperPath;
/**
 Returns path to "eventRunList.xml" under "state/lastrun" folder
 
 @return NSString *
 */
- (NSString *_Nullable)alertRunListFilePath;
/**
 Returns path to "RealAlertEventRunList.xml" under "state/lastrun" folder
 
 @return NSString *
 */
- (NSString *_Nullable)realTimeAlertRunListFilePath;

/**
 Returns path to "sprt_alertevents" folder
 
 @return NSString *
 */
- (NSString *_Nullable)alertsFolderPath;

/**
 Returns path to "state" folder
 
 @return NSString *
 */
- (NSString *_Nullable)stateFolderPath;

/**
 Returns path to "sprt_wmitrigger" folder
 
 @return NSString *
 */
- (NSString * _Nullable)selfHealTriggerFolderPath;

/**
 Returns path to monitor plugins folder
 
 @return NSArray *
 */
- (NSArray * _Nullable)selfHealMonitorPluginPaths;

#pragma mark -
/**
 creates a directory at the specified path. If you pass 'NO' for createIntermediates, the directory must not exist at the time this call is made. Passing 'YES' for 'createIntermediates' will create any necessary intermediate directories. This method returns YES if all directories specified in 'path' were created and attributes were set. Directories are created with attributes specified by the dictionary passed to 'attributes'. If no dictionary is supplied, directories are created according to the umask of the process. This method returns NO if a failure occurs at any stage of the operation. If an error parameter was provided, a presentable NSError will be returned by reference.
 
 @param path NSString * Directory path
 @param createIntermediates BOOL Flag Indicates whether any necessary intermediate directories should be created or not
 @param attributes NSDictionary<NSString *, id> * Attributes for directory; you can pass 'nil'
 @param error NSError * To store error in creating directory; you can pass 'nil'
 @return YES if the directory was created, YES if createIntermediates is set and the directory already exists, or NO if an error occurred.
 */
- (BOOL)createDirectoryAtPath:(NSString *_Nullable)path
  withIntermediateDirectories:(BOOL)createIntermediates
                   attributes:(nullable NSDictionary<NSString *, id> *)attributes
                        error:(NSError *_Nullable*_Nullable)error;

/**
 creates a file at the specified path.
 
 @param path NSString * File path
 @param attributes NSDictionary<NSString *, id> * Attributes for file; you can pass 'nil'
 @param error NSError * To store error in creating file; you can pass 'nil'
 @return YES if the file was created, or NO if an error occurred.
 */
- (BOOL)createFileAtPath:(NSString *_Nullable)path
              attributes:(nullable NSDictionary<NSString *, id> *)attributes
                   error:(NSError *_Nullable*_Nullable)error;


- (NSString *_Nullable)createFileAtPath:(NSString *_Nullable)path
                             attributes:(nullable NSDictionary<NSString *, id> *)attributes
                        shouldOverwrite:(BOOL)bOverwrite
                                  error:(NSError *_Nullable*_Nullable)error;
/**
 Returns a Boolean value that indicates whether a file exists at a specified path.
 
 @param filePath The path of the file or directory.
 @return YES, if a file at the specified path exists, or NO if the file does not exist or its existence could not be determined.
 */
- (BOOL)doesFileExist:(NSString *_Nonnull)filePath;

/**
 Returns a Boolean value that indicates whether a directory exists at a specified path.
 
 @param directoryPath The path of the directory.
 @return YES, if a directory at the specified path exists, or NO if the directory does not exist or its existence could not be determined.
 */
- (BOOL)doesDirectoryExist:(NSString *_Nonnull)directoryPath;

/**
 Moves the directory at the specified path to a new location synchronously.
 
 @param srcPath The path to the directory you want to move. This parameter must not be nil.
 @param dstPath The new path for the item in srcPath. This path must include the name of the directory in its new location. This parameter must not be nil.
 
 @return YES if the directory was moved successfully or the file manager’s delegate stopped the operation deliberately. Returns NO if an error occurred.
 */
- (BOOL)moveDirectory:(NSString *_Nonnull)srcPath
      destinationPath:(NSString *_Nonnull)dstPath;

/**
 Moves the file at the specified path to a new location synchronously.
 
 @param srcPath The path to the file you want to move. This parameter must not be nil.
 @param dstPath The new path for the item in srcPath. This path must include the name of the file in its new location. This parameter must not be nil.
 
 @return YES if the file was moved successfully or the file manager’s delegate stopped the operation deliberately. Returns NO if an error occurred.
 */
- (BOOL)moveFile:(NSString *_Nonnull)srcPath
 destinationPath:(NSString *_Nonnull)dstPath;

/**
 Copies the directory at the specified path to a new location synchronously.
 
 @param srcPath The path to the directory you want to move. This parameter must not be nil.
 @param dstPath The path at which to place the copy of srcPath. This path must include the name of the directory in its new location. This parameter must not be nil.
 @return YES if the directory was copied successfully or the file manager’s delegate stopped the operation deliberately. Returns NO if an error occurred.
 */
- (BOOL)copyDirectory:(NSString *_Nonnull)srcPath
      destinationPath:(NSString *_Nonnull)dstPath;
/**
 Copies the file at the specified path to a new location synchronously.
 
 @param srcPath The path to the file you want to move. This parameter must not be nil.
 @param dstPath The path at which to place the copy of srcPath. This path must include the name of the file in its new location. This parameter must not be nil.
 @return YES if the file was copied successfully or the file manager’s delegate stopped the operation deliberately. Returns NO if an error occurred.
 */
- (BOOL)copyFile:(NSString *_Nonnull)srcPath
 destinationPath:(NSString *_Nonnull)dstPath;

/**
 Renames directory
 
 @param srcPath The path to the directory you want to rename
 @param newName The new file name
 @return YES if the directory was renamed successfully. Returns NO if an error occurred.
 */
- (BOOL)renameDirectory:(NSString *_Nonnull)srcPath
            newFileName:(NSString *_Nonnull)newName;
/**
 Renames file
 
 @param srcFilePath The path to the file you want to rename
 @param newName The new file name
 @return YES if the file was renamed successfully. Returns NO if an error occurred.
 */
- (BOOL)renameFile:(NSString *_Nonnull)srcFilePath
       newFileName:(NSString *_Nonnull)newName;

/**
 Removes the file at the specified path.
 
 @param srcPath A path string indicating the file to remove. If the path specifies a directory, the contents of that directory are recursively removed. You may specify nil for this parameter.
 
 @return YES if the item was removed successfully or if path was nil. Returns NO if an error occurred. If the delegate stops the operation for a file, this method returns YES. However, if the delegate stops the operation for a directory, this method returns NO.
 */
- (BOOL)deleteFile:(NSString *_Nonnull)srcPath;

/**
 Removes the directory at the specified path.
 
 @param srcPath A path string indicating the file or directory to remove. If the path specifies a directory, the contents of that directory are recursively removed. You may specify nil for this parameter.
 
 @return YES if the item was removed successfully or if path was nil. Returns NO if an error occurred. If the delegate stops the operation for a file, this method returns YES. However, if the delegate stops the operation for a directory, this method returns NO.
 */
- (BOOL)deleteDirectory:(NSString *_Nonnull)srcPath;
/**
 An array of NSString objects, each of which identifies a file, directory, or symbolic link contained in path. Returns an empty array if the directory exists but has no contents. If an error occurs, this method returns nil and assigns an appropriate error object to the error parameter.
 
 @param dirPath The path to the directory you want to get the contents
 @return An array of file/directory NSString objects, nil if the directory does not exist and is empty directory
 */
- (NSArray *_Nullable)contentsOfDirectory:(NSString *_Nonnull)dirPath;

/**
 Returns content of file
 
 @param filePath The path to the file where you want to read
 @return The file content as NSData*
 */
- (NSData *_Nullable)contentOfFileAtPath:(NSString *_Nonnull)filePath;


/**
 Returns sub directories of the directory
 
 @param dirPath The path to the directory where you want to read
 @return An array of directory NSString objects, nil if the directory does not exist and is empty directory
 */
- (NSArray *_Nullable)subDirectoriesOfDirectory:(NSString *_Nonnull)dirPath;

/**
 Returns files of the directory
 
 @param dirPath The path to the directory where you want to read
 @return An array of file NSString objects, nil if the directory does not exist and is empty directory
 */
- (NSArray *_Nullable)filesOfDirectory:(NSString *_Nonnull)dirPath;

/**
 Writes the input data to the file at the specified path
 
 @param filePath The path to the file where you want to write
 @param data The data to write
 @return YES if file write is successful; NO otherwise
 */
- (BOOL) writeToFileAtPath:(NSString *_Nonnull)filePath
                      data:(NSData *_Nonnull)data;
- (BOOL) writeToFileAtPath:(NSString *_Nonnull)filePath data:(NSData *_Nonnull)data withMode:(PAFileModeType)mode;
/**
 Returns a Boolean value that indicates whether item at input path is a file.
 
 @param filePath The path/sub path to the file or directory
 @return YES, if item at the specified path is a file, NO otherwise.
 */
- (BOOL) isFile:(NSString *_Nonnull)filePath;

#pragma mark -

/**
 Returns a Boolean value that indicates whether the checksum verification is successful or not.
 
 This method generates checksum against the 'itemPath' and compares the generated checksum with 'checksum' input; if checksums match then returns YES; NO otherwise
 
 @param checksum The checksum to verify for itemPath
 @param itemPath The path to either file or directory on which checksum needs to be verified
 @return Yes, if the checksum verification is successful; NO otherwise
 */
- (BOOL)verifyChecksum:(NSString *_Nonnull)checksum
               forItem:(NSString *_Nonnull)itemPath;


/**
 Returns attributes of input file or directory.
 EG: Creation date, modification date, size
 
 @param itemPath The path to either file or directory for which you want to get the attributes
 @return PAItemAttributes
 */
- (PAItemAttributes *_Nullable)attributesOfItemAtPath:(NSString *_Nonnull)itemPath;


/**
 Returns the name of the file matching with the input 'itemName' in 'baseDirectory'
 
 @param itemName NSString * The name of the file for comparison
 @param baseDirectory The path to the base directory in which the content search should happen
 @return Returns name of the file if a matching file is found in 'baseDirectory'; nil otherwise
 */
- (NSString *_Nullable)getMatchingFileName:(NSString *_Nonnull)itemName
                           inBaseDirectory:(NSString *_Nonnull)baseDirectory;
@end
