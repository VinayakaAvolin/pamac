//
//  PAItemAttributes.h
//  PAFileSystemService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PAItemAttributes : NSObject

@property NSDate *creationDate;
@property NSDate *modificationDate;
@property unsigned long long size; // file or directory size
@property NSString *type; // file/directory type

- (instancetype)initWithFileAttributes:(NSDictionary *)attributes;

@end
