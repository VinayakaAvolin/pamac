//
//  PAFileSystemService.h
//  PAFileSystemService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Cocoa/Cocoa.h>

//! Project version number for PAFileSystemService.
FOUNDATION_EXPORT double PAFileSystemServiceVersionNumber;

//! Project version string for PAFileSystemService.
FOUNDATION_EXPORT const unsigned char PAFileSystemServiceVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <PAFileSystemService/PublicHeader.h>

#import <PAFileSystemService/PAFileSystemManager.h>
#import <PAFileSystemService/PAItemAttributes.h>
#import <PAFileSystemService/PAFileSystemConstants.h>
