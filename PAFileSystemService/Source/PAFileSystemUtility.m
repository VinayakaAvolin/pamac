//
//  PAFileSystemUtility.m
//  PAFileSystemService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PAFileSystemUtility.h"
#import "NSString+Additions.h"
#import "NSFileManager+Additions.h"
#import "PAFileSystemConstants.h"

@implementation PAFileSystemUtility


+ (NSString *_Nonnull)stringByExpandingPath:(NSString *_Nonnull)inputFilePath configuration:(PAAppConfiguration *)config {
  NSString *finalPath = @"";
  NSString *macCompatibleFilePath = [inputFilePath stringByConvertingIntoMacCompatibleFilePath];
  NSString* serverRootPlaceholder = config.serverRootPlaceholder;
  NSString* userRootPlaceholder = config.userRootPlaceholder;
  NSString* systemRootPlaceholder = config.systemRootPlaceholder;
  NSString* appFolderNamePlaceholder = config.appFolderPlaceholder;
  NSString* providerPathPlaceholder = config.providerPathPlaceholder;
  NSString* userDataPlaceholder = config.userDataPlaceholder;
  NSString* serverRoot = config.serverRoot;
  NSString* systemRoot = config.systemRoot;
  NSString* appFolderName = config.appFolder;
  NSString* providerId = config.providerId;
  NSString* userNamePlaceHolder = config.userNamePlaceholder;
  NSString* mainAppPathPlaceHolder = config.mainAppPathPlaceholder;
  NSString* mainAppPath = config.mainAppPath;
  NSString* skinName = config.skinName;
  NSString* skinNamePlaceholder = config.skinNamePlaceHolder;
  NSString* serverBasePlaceholder = config.serverBasePlaceholder;
  NSString* serverBaseUrlPath = config.serverBaseUrlPath;
  NSString* desktopPathPlaceholder = config.desktopPathPlaceholder;
  
  if (serverRootPlaceholder && [inputFilePath containsString:serverRootPlaceholder]) {
    finalPath = [macCompatibleFilePath stringByReplacingOccurrencesOfString:serverRootPlaceholder withString:serverRoot];
  } else if (systemRootPlaceholder && [inputFilePath containsString:systemRootPlaceholder]) {
    finalPath = [macCompatibleFilePath stringByReplacingOccurrencesOfString:systemRootPlaceholder withString:systemRoot];
  } else if (userRootPlaceholder && [inputFilePath containsString:userRootPlaceholder]) {
    NSString *userLocalFileSystemPath = [PAFileSystemUtility userLocalFileSystemPathWithConfiguration:config];
    NSString *subPath = [macCompatibleFilePath stringByReplacingOccurrencesOfString:userRootPlaceholder withString:@""];
    finalPath = [macCompatibleFilePath stringByReplacingOccurrencesOfString:subPath withString:@""];
    finalPath = [finalPath stringByReplacingOccurrencesOfString:userRootPlaceholder withString:userLocalFileSystemPath];
    finalPath = [finalPath stringByAppendingPathComponent:subPath];
  }
  else if (userDataPlaceholder && [inputFilePath containsString:userDataPlaceholder]) {
    NSString *dataFolderPath = [PAFileSystemUtility appDataFolderPathWithConfiguration:config];
    finalPath = [macCompatibleFilePath stringByReplacingOccurrencesOfString:userDataPlaceholder withString:dataFolderPath];
  } else {
    finalPath = macCompatibleFilePath;
  }
  // same path may contain multiple placeholders
  if (providerPathPlaceholder && [finalPath containsString:providerPathPlaceholder]) {
    finalPath = [finalPath stringByReplacingOccurrencesOfString:providerPathPlaceholder withString:providerId];
  }
  if (userNamePlaceHolder && [finalPath containsString:userNamePlaceHolder]) {
    finalPath = [finalPath stringByReplacingOccurrencesOfString:userNamePlaceHolder withString:NSUserName()];
  }
  if (mainAppPathPlaceHolder && [finalPath containsString:mainAppPathPlaceHolder]) {
    finalPath = [finalPath stringByReplacingOccurrencesOfString:mainAppPathPlaceHolder withString:mainAppPath];
  }
  if (skinNamePlaceholder && [finalPath containsString:skinNamePlaceholder]) {
    finalPath = [finalPath stringByReplacingOccurrencesOfString:skinNamePlaceholder withString:skinName];
  }
  if (serverBasePlaceholder && [finalPath containsString:serverBasePlaceholder]) {
    finalPath = [finalPath stringByReplacingOccurrencesOfString:serverBasePlaceholder withString:serverBaseUrlPath];
  }
  
  if (desktopPathPlaceholder && [finalPath containsString:desktopPathPlaceholder]) {
    NSString* desktop = [NSString stringWithFormat:@"%@/",[PAFileSystemUtility desktopPath]];
    finalPath = [finalPath stringByReplacingOccurrencesOfString:desktopPathPlaceholder withString:desktop];
    
  }
  if (appFolderNamePlaceholder && [finalPath containsString:appFolderNamePlaceholder]) {
     finalPath = [finalPath stringByReplacingOccurrencesOfString:appFolderNamePlaceholder withString:appFolderName];
  }
  finalPath = [finalPath stringByExpandingTildeInPath];
  
  if (finalPath.length == 0) {
    finalPath = inputFilePath;
  }
  return finalPath;
}

+ (NSString *_Nullable)userLocalFileSystemPathWithConfiguration:(PAAppConfiguration *_Nonnull)config {
  NSString *mainDirectoryPath = [config.userRoot stringByExpandingTildeInPath];
  NSString *appFolder = config.appFolder;
  NSString *providerId = config.providerId;
  NSString *userRootFileSystemPath = [mainDirectoryPath stringByAppendingPathComponent:appFolder];
  userRootFileSystemPath = [userRootFileSystemPath stringByAppendingPathComponent:providerId];
  return userRootFileSystemPath;
}

+ (NSString *_Nullable)appDataFolderPathWithConfiguration:(PAAppConfiguration *_Nonnull)config {
  NSString *dataFolderPath = config.dataFolderPath;
  dataFolderPath = [PAFileSystemUtility stringByExpandingPath:dataFolderPath configuration:config];
  return dataFolderPath;
}

+ (NSString *_Nonnull)userDirectoryPath {
  return [NSString directoryPathFor:NSUserDirectory domain:NSUserDomainMask];
}
+ (NSString *_Nonnull)userApplicationSupportDirectoryPath {
  return [NSString directoryPathFor:NSApplicationSupportDirectory domain:NSUserDomainMask];
}
+ (NSString *_Nonnull)tempDownloadsDirectoryPathWithAppConfiguration:(PAAppConfiguration *)config {
  NSString* path = NSTemporaryDirectory();//[NSString directoryPathFor:NSDownloadsDirectory domain:NSUserDomainMask];
  NSString* appFolder = config.appFolder;
  path =  [path stringByAppendingPathComponent:appFolder];
  BOOL isFileExist = [PAFileSystemUtility doesItemExistAtPath:path];
  
  if ( !isFileExist ) {
    [[NSFileManager defaultManager] createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:nil];
  }
  
  return path;
}

+ (BOOL)checkAndCreateItemAtPath:(NSString *_Nonnull)itemPath
                     isDirectory:(BOOL)isDir
                 shouldOverwrite:(BOOL)bOverwrite
     withIntermediateDirectories:(BOOL)createIntermediates
                      attributes:(nullable NSDictionary<NSString *, id> *)attributes
                           error:(NSError *_Nullable*_Nullable)error {
  BOOL isFileExist = [PAFileSystemUtility doesItemExistAtPath:itemPath];
  
  if ( !isFileExist ) {
    if ( isDir ) {
      isFileExist = [[NSFileManager defaultManager] createDirectoryAtPath:itemPath withIntermediateDirectories:createIntermediates attributes:attributes error:error];
      
    } else {
      isFileExist = [[NSFileManager defaultManager] createFileAtPath:itemPath contents:nil attributes:attributes];
    }
  } else {
    if (bOverwrite) {
      if ( isDir ) {
        isFileExist = [[NSFileManager defaultManager] createDirectoryAtPath:itemPath withIntermediateDirectories:createIntermediates attributes:attributes error:error];
        
      } else {
        isFileExist = [[NSFileManager defaultManager] createFileAtPath:itemPath contents:nil attributes:attributes];
      }
    }
  }
  return isFileExist;
}

+ (BOOL)doesItemExistAtPath:(NSString *_Nonnull)itemPath {
  return [NSFileManager doesFileExistAtPath:itemPath];
}

+ (BOOL)moveItem:(NSString *_Nonnull)srcPath destinationPath:(NSString *_Nonnull)dstPath {
  NSError *error;
  BOOL fileMoved = [[NSFileManager defaultManager] moveItemAtPath:srcPath toPath:dstPath error:&error];
  return fileMoved;
}
+ (BOOL)copyItem:(NSString *_Nonnull)srcPath destinationPath:(NSString *_Nonnull)dstPath {
  NSError *error;
  BOOL fileCopied = [[NSFileManager defaultManager] copyItemAtPath:srcPath toPath:dstPath error:&error];
  return fileCopied;
}
+ (BOOL)renameItem:(NSString *_Nonnull)srcPath newFileName:(NSString *_Nonnull)newName {
  NSError *error;
  BOOL isFileReNamed = false;
  if ( [PAFileSystemUtility doesItemExistAtPath:srcPath] ){
    NSString *newPath = [[srcPath stringByDeletingLastPathComponent] stringByAppendingPathComponent:newName];
    isFileReNamed = [[NSFileManager defaultManager] moveItemAtPath:srcPath toPath:newPath error:&error];
  }
  return isFileReNamed;
}
+ (BOOL)deleteItem:(NSString *_Nonnull)srcPath {
  NSError *error;
  return [[NSFileManager defaultManager] removeItemAtPath:srcPath error:&error];
}

+ (NSArray *_Nullable)contentsOfDirectory:(NSString *_Nonnull)dirPath {
  NSFileManager *fileManager = [NSFileManager defaultManager];
  NSArray *contentsOfDirectory = [fileManager contentsOfDirectoryAtPath:dirPath error:nil];
  if (contentsOfDirectory.count) {
    NSMutableArray *arrayWithoutDsStoreFile = [contentsOfDirectory mutableCopy];
    [arrayWithoutDsStoreFile removeObject:@".DS_Store"];
    contentsOfDirectory = [NSArray arrayWithArray:arrayWithoutDsStoreFile];
  }
  return contentsOfDirectory;
}
+ (BOOL) isFile:(NSString *_Nonnull)filePath {
  if ( [filePath hasSuffix:@"/"]) {
    return NO;
  }
  NSString *lastPathComponent = [filePath lastPathComponent];
  BOOL isFile = ([lastPathComponent doesMatchRegularExpression:kPADataSetInnerFileNameRegEx] || [lastPathComponent doesMatchRegularExpression:kPADataSetInnerFileNameRegEx1] ||
                 [lastPathComponent doesMatchRegularExpression:kPADataSetInnerFileNameRegEx2]);
  return isFile;
}

+ (NSArray *_Nullable)subDirectoriesOfDirectory:(NSString *_Nonnull)dirPath {
  NSArray* directories = [PAFileSystemUtility contentsOfDirectory:dirPath];
  NSMutableArray *directoryList = [NSMutableArray arrayWithCapacity:10];
  NSFileManager *fileManager = [NSFileManager defaultManager];
  
  for(NSString *file in directories) {
    NSString *path = [dirPath stringByAppendingPathComponent:file];
    BOOL isDir = NO;
    [fileManager fileExistsAtPath:path isDirectory:(&isDir)];
    if(isDir) {
      [directoryList addObject:file];
    }
  }
  return directoryList;
}

+ (NSArray *_Nullable)filesOfDirectory:(NSString *_Nonnull)dirPath {
  NSArray* directories = [PAFileSystemUtility contentsOfDirectory:dirPath];
  NSMutableArray *directoryList = [NSMutableArray arrayWithCapacity:10];
  NSFileManager *fileManager = [NSFileManager defaultManager];
  
  for(NSString *file in directories) {
    NSString *path = [dirPath stringByAppendingPathComponent:file];
    BOOL isDir = NO;
    [fileManager fileExistsAtPath:path isDirectory:(&isDir)];
    if(!isDir) {
      [directoryList addObject:file];
    }
  }
  return directoryList;
}

+ (NSString *_Nonnull)desktopPath {
  NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDesktopDirectory, NSUserDomainMask, YES );
  return  [paths objectAtIndex:0];
  
}
@end
