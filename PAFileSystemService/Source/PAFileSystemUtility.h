//
//  PAFileSystemUtility.h
//  PAFileSystemService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <PAConfigurationService/PAConfigurationService.h>

@interface PAFileSystemUtility : NSObject

+ (NSString *_Nonnull)stringByExpandingPath:(NSString *_Nonnull)inputFilePath configuration:(PAAppConfiguration *_Nonnull)config;

+ (NSString *_Nonnull)userDirectoryPath;
+ (NSString *_Nonnull)userApplicationSupportDirectoryPath;
+ (NSString *_Nonnull)tempDownloadsDirectoryPathWithAppConfiguration:(PAAppConfiguration *_Nonnull)config;

+ (BOOL)checkAndCreateItemAtPath:(NSString *_Nonnull)itemPath
                     isDirectory:(BOOL)isDir
                 shouldOverwrite: (BOOL)bOverwrite
     withIntermediateDirectories:(BOOL)createIntermediates
                      attributes:(nullable NSDictionary<NSString *, id> *)attributes
                           error:(NSError *_Nullable*_Nullable)error;
+ (BOOL)doesItemExistAtPath:(NSString *_Nonnull)itemPath;
+ (BOOL)moveItem:(NSString *_Nonnull)srcPath destinationPath:(NSString *_Nonnull)dstPath;
+ (BOOL)copyItem:(NSString *_Nonnull)srcPath destinationPath:(NSString *_Nonnull)dstPath;
+ (BOOL)renameItem:(NSString *_Nonnull)srcPath newFileName:(NSString *_Nonnull)newName;
+ (BOOL)deleteItem:(NSString *_Nonnull)srcPath;
+ (NSArray *_Nullable)contentsOfDirectory:(NSString *_Nonnull)dirPath;
+ (BOOL) isFile:(NSString *_Nonnull)filePath;
+ (NSArray *_Nullable)subDirectoriesOfDirectory:(NSString *_Nonnull)dirPath;
+ (NSArray *_Nullable)filesOfDirectory:(NSString *_Nonnull)dirPath;
+ (NSString *_Nullable)appDataFolderPathWithConfiguration:(PAAppConfiguration *_Nonnull)config;
+ (NSString *_Nullable)userLocalFileSystemPathWithConfiguration:(PAAppConfiguration *_Nonnull)config;

+ (NSString *_Nonnull)desktopPath;

@end
