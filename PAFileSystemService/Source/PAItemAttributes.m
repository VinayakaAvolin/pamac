//
//  PAItemAttributes.m
//  PAFileSystemService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PAItemAttributes.h"

@implementation PAItemAttributes

- (instancetype)initWithFileAttributes:(NSDictionary *)attributes {
  self = [super init];
  if (self) {
    _size = attributes.fileSize;
    _creationDate = attributes.fileCreationDate;
    _modificationDate = attributes.fileModificationDate;
    _type = attributes.fileType;
  }
  return self;
}

@end
