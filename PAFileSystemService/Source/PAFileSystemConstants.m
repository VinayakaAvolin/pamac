//
//  PAFileSystemConstants.m
//  PAFileSystemService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PAFileSystemConstants.h"

NSString *const kPADataSetInnerFileNameRegEx = @"^[A-Za-z0-9-]*.[0-9]*.[a-z]*";
NSString *const kPADataSetInnerFileNameRegEx1 = @"[A-Za-z0-9_.]*.[a-z]*";
NSString *const kPADataSetInnerFileNameRegEx2 = @"[A-Za-z0-9_-]*.[a-z]*";

