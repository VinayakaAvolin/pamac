//
//  PASelfHealEvent.h
//  ProactiveAssistAgent
//
//  Copyright © 2018 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <PAEventBus/PAEventType.h>

@class PASelfHealTriggerInfo;

@interface PASelfHealEvent : NSObject<PAEventType, NSSecureCoding>

@property (nonatomic, readonly) NSString *eventGUID;
@property (nonatomic, readonly) NSData *eventData;
@property (nonatomic, readonly) PASelfHealTriggerInfo *triggerInfo;

- (instancetype)initWithGuid: (NSString*)guid triggerInfo: (PASelfHealTriggerInfo *)triggerInfo andData: (NSData *)data;

- (NSString *)eventGuid; // Should be unique
- (SelfHealEventClass)eventClass;

@end
