//
//  PAEventSubscribable.h
//  PAeventBus
//
//  Copyright © 2018 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <PAEventBus/PAEventType.h>
@protocol PAEventSubscribable <NSObject>

@required
- (NSString *)objectIdentifier; // This should be a uuid
- (void)onEvent:(id<PAEventType>)event;
@optional
- (NSOperationQueue *)eventNotificationQueue;

@end

