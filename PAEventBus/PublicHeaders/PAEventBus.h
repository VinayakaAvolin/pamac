//
//  PAEventBus.h
//  PAeventBus
//
//  Copyright © 2018 Avolin. All rights reserved.
//

#import <Cocoa/Cocoa.h>

//! Project version number for PAEventBus.
FOUNDATION_EXPORT double PAEventBusVersionNumber;

//! Project version string for PAEventBus.
FOUNDATION_EXPORT const unsigned char PAEventBusVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <PAEventBus/PublicHeader.h>


#import <PAEventBus/PAEventSubscribable.h>
#import <PAEventBus/PAEventType.h>
#import <PAEventBus/PASelfHealConstants.h>
#import <PAEventBus/PASelfHealEvent.h>


@interface PAEventBus : NSObject

+ (PAEventBus *)sharedInstance;
- (void)registerSubscriber: (id<PAEventSubscribable>)subsciber forEventClass: (SelfHealEventClass)eventClass;
- (NSArray<id<PAEventSubscribable>> *)registeredSubscribers;
- (void)unregisterSubscriber: (id<PAEventSubscribable>)subscriber;
// Delivers event on operation queue provided by subscribers, if they provide one. Otherwise works as delegate delivering event
// on onEvent: method.
- (void)postEvent: (id<PAEventType>)event;

@end
