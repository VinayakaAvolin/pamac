//
//  PASelfHealConstants.h
//  ProactiveAssistAgent
//
//  Copyright © 2018 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, SelfHealEventClass)
{
    InvalidEventClass = -1,
    FileMonitoring = 0,
    PlistMonitoring = 1,
    HardwareMonitoring = 2,
    BatteryLevelMonitoring = 3,
    StorageMonitoring = 4,
    AppiicationMonitoring = 5,
    NetworkMonitoring = 6,
    AppleScriptMonitoring = 7
};

typedef NS_ENUM(NSInteger, UserMode )
{
    user = 0,
    admin = 1
};

typedef NS_ENUM(NSInteger, EventMonitorType)
{
    ItemCreated = 1,
    ItemModified = 2,
    ItemDeleted = 4
};

typedef NS_ENUM(NSInteger, NetworkMonitorType)
{
    Firewall = 1,
    Wireless = 2
};
typedef NS_ENUM(NSInteger, FirewallStatus)
{
    Enabled = 1,
    Disabled = 2
};
typedef NS_ENUM(NSInteger, WirelessStatus)
{
    Connected = 1,
    Disconnected = 2
};
typedef NS_ENUM(NSInteger, ApplicationMonitorType)
{
    appSettingChanged = 1,
    appCrashFileCreated = 2,
    appStatusMonitoring = 3
};

