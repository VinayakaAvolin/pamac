//
//  PAEventType.h
//  PAeventBus
//
//  Copyright © 2018 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PASelfHealConstants.h"


@protocol PAEventType <NSObject>
@required
- (NSString *)eventGuid; // Should be unique
- (SelfHealEventClass)eventClass;
@end

