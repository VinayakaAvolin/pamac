//
//  PAEventBus.m
//  PAeventBus
//
//  Copyright © 2018 Avolin. All rights reserved.
//

#import "PAEventBus.h"
#import "PAEventSubscribable.h"
#import "PAEventType.h"
#import "PASelfHealConstants.h"

NSString* const kEventClassNameKey = @"event";
NSString* const kSubscriberObjectKey = @"subscriber";
@interface PAEventBus()

@property (atomic) NSMutableDictionary *subscribers;

@end

@implementation PAEventBus

+ (PAEventBus *)sharedInstance
{
    static PAEventBus *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        _subscribers = [NSMutableDictionary new];
    }
    return self;
}

- (void)registerSubscriber: (id<PAEventSubscribable>)subsciber forEventClass: (SelfHealEventClass)eventClass
{

    NSDictionary *subscriberInfo = @{
                                     kEventClassNameKey: @(eventClass),
                                     kSubscriberObjectKey: subsciber
    };
    self.subscribers[[subsciber objectIdentifier]] = subscriberInfo;
}

- (NSArray<id<PAEventSubscribable>> *)registeredSubscribers
{
    NSMutableArray *mapped = [NSMutableArray arrayWithCapacity:[[self.subscribers allValues] count]];
    [[self.subscribers allValues] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        id mapObj = obj[kSubscriberObjectKey];
        [mapped addObject:mapObj];
    }];
    return mapped;
}

- (void)unregisterSubscriber: (id<PAEventSubscribable>)subscriber
{
    return [self.subscribers removeObjectForKey:[subscriber objectIdentifier]];
}

- (void)postEvent: (id<PAEventType>)event
{
    @synchronized (self) {
        NSArray *allSubscribers = [self registeredSubscribers];
        for (id<PAEventSubscribable> subscriber in allSubscribers) {
            if ([[subscriber objectIdentifier] isEqualToString:event.eventGuid]){
                if ([subscriber respondsToSelector:@selector(eventNotificationQueue)])
                {
                    NSInvocationOperation *operation = [[NSInvocationOperation alloc]
                                                        initWithTarget:subscriber
                                                        selector:@selector(eventNotificationQueue)
                                                        object:event];
                    [[subscriber eventNotificationQueue] addOperation:operation];
                } else {
                    [subscriber onEvent: event];
                }
            }
        }
    }
}
@end
