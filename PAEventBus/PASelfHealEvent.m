//
//  PASelfHealEvent.m
//  ProactiveAssistAgent
//
//  Copyright © 2018 Avolin. All rights reserved.
//

#import "PASelfHealEvent.h"
#import "PASelfHealTriggerInfo.h"
@implementation PASelfHealEvent
- (instancetype)initWithGuid: (NSString*)guid triggerInfo: (PASelfHealTriggerInfo *)triggerInfo andData: (NSData *)data
{
    self = [super init];
    if (self) {
        _eventGUID = [guid copy];
        _triggerInfo = triggerInfo;
        _eventData = [data copy];
    }
    return self;
}

- (NSString *)eventGuid {
    return self.eventGUID;
}
- (SelfHealEventClass)eventClass {
    return self.triggerInfo.monitorEventClass;
}

+ (BOOL)supportsSecureCoding {
    return YES;
}

- (id)initWithCoder:(NSCoder *)coder {
    if ((self = [super init])) {
        // Decode the property values by key, specifying the expected class
        _eventGUID = [coder decodeObjectOfClass:[NSString class] forKey:@"_eventGUID"];
        _eventData = [coder decodeObjectOfClass:[NSData class] forKey:@"_eventData"];
        _triggerInfo = [coder decodeObjectOfClass:[PASelfHealTriggerInfo class] forKey:@"_triggerInfo"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)coder {
    // Encode our ivars using string keys as normal
    [coder encodeObject:_eventGUID forKey:@"_eventGUID"];
    [coder encodeObject:_eventData forKey:@"_eventData"];
    [coder encodeObject:_triggerInfo forKey:@"_triggerInfo"];
}

@end
