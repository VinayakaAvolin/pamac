//
//  PAAppConfiguration.h
//  PAConfigurationService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PARealTimeAlertConfiguration.h"
#import "PALogInfoConfiguration.h"
#import "PAInstallInfo.h"
#import "PAContextConfiguration.h"
#import "PACommandInfo.h"

@interface PAAppConfiguration : NSObject

@property(nonatomic) NSString *mainAppPath;
@property(nonatomic) NSString *serverRoot;
@property(nonatomic) NSString *userRoot;
@property(nonatomic) NSString *systemRoot;
@property(nonatomic) NSString *appFolder;
@property(nonatomic) NSString *providerId;
@property(nonatomic) NSString *dataFolderPath;
@property(nonatomic) NSString *serverRootPlaceholder;
@property(nonatomic) NSString *userRootPlaceholder;
@property(nonatomic) NSString *systemRootPlaceholder;
@property(nonatomic) NSString *appFolderPlaceholder;
@property(nonatomic) NSString *manifestXmlFile;
@property(nonatomic) NSString *manifestZipFile;
@property(nonatomic) NSString *logsFolderPath;
@property(nonatomic) NSString *backupFolderPath;
@property(nonatomic) NSString *stateFolderPath;
@property(nonatomic) NSString *macDefaultXmlFilePath;
@property(nonatomic) NSString *providerPathPlaceholder;
@property(nonatomic) NSString *userNamePlaceholder;
@property(nonatomic) NSString *mainAppPathPlaceholder;
@property(nonatomic) NSString *notificationTrayExecutablePath;
@property(nonatomic) NSString *userDataPlaceholder;
@property(nonatomic) NSString *protectionFolderPath;
@property(nonatomic) NSString *downloadFolderPath;
@property(nonatomic) NSString *messagesFolderPath;
@property(nonatomic) NSString *userAgentBinFolderPath;
@property(nonatomic) NSString *smartIssuesFolderPath;
@property(nonatomic) NSString *snapinFolderPath;
@property(nonatomic) NSString *smartIssueDefaultXmlSubPath;
@property(nonatomic) NSString *dataFolderName;
@property(nonatomic) NSString *guidVersionPlaceholder;
@property(nonatomic) NSString *realTimeAlertsFolderPath;
@property(nonatomic) NSString *agentCoreFolderPath;
@property(nonatomic) NSString *hkcuRegistryPath;
@property(nonatomic) NSString *hklmRegistryPath;
@property(nonatomic) NSString *selfHealRegistryPlistPath;
@property(nonatomic) NSString *skinName;
@property(nonatomic) NSString *skinNamePlaceHolder;
@property(nonatomic) NSString *configCfgFile;
@property(nonatomic) NSString *serverBaseUrlPath;
@property(nonatomic) NSString *serverBasePlaceholder;
@property(nonatomic) NSString *clientUiConfigFilePath;
@property(nonatomic) NSString *optimizationFolderPath;
@property(nonatomic) NSString *syncCertificatePath;
@property(nonatomic) NSString *supportActionCertificatePath;
@property(nonatomic) NSString *supportActionFolderPath;
@property(nonatomic) NSString *privilegeToolHelperPath;
@property(nonatomic) NSString *alertRunListFilePath;
@property(nonatomic) NSString *realTimeAlertRunListFilePath;
@property(nonatomic) NSString *alertsFolderPath;
@property(nonatomic) NSString *setProfileUrlPath;
@property(nonatomic) NSString *selfHealTriggerFolderPath;
@property(nonatomic) NSArray *selfHealMonitorPluginPaths;

@property(nonatomic) NSString *desktopPathPlaceholder;

@property(nonatomic) PARealTimeAlertConfiguration *realtimeAlertconfigurations;
@property(nonatomic) PALogInfoConfiguration *agentLogConfiguration;
@property(nonatomic) PALogInfoConfiguration *paUiLogConfiguration;
@property(nonatomic) PAInstallInfo *installInfo;
@property(nonatomic) NSArray *authorizedCnList;
@property(nonatomic) NSArray *supportedSprockets;
@property(nonatomic) PAContextConfiguration *contextConfiguration;
@property(nonatomic) NSArray *commandList;

@property(nonatomic) NSString *assistAgentFolderPath;

- (PACommandInfo *)commandInfoMatchingCommand:(NSString *)command;

@end
