//
//  PAInstallInfo.h
//  PAConfigurationService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PAInstallInfo : NSObject

@property(nonatomic, readonly) NSString *buildVersion;
@property(nonatomic, readonly) NSString *displayVersion;
@property(nonatomic, readonly) NSString *installDateStr;
@property(nonatomic, readonly) NSString *installTime;
@property(nonatomic, readonly) NSString *installedSetup;
- (instancetype)initWithInstallInfoDict:(NSDictionary *)installDetail;

- (NSDictionary*)dictRepresentation;

@end
