//
//  PAContextConfigurationConstants.h
//  PAConfigurationService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifndef PAContextConfigurationConstants_h
#define PAContextConfigurationConstants_h

extern NSString *const kPAActionsPath;
extern NSString *const kPABackupPath;
extern NSString *const kPABinPath;
extern NSString *const kPADataPath;
extern NSString *const kPADnaPath;
extern NSString *const kPADnaBackupPath;
extern NSString *const kPAIssuePath;
extern NSString *const kPALogsPath;
extern NSString *const kPAProfilesPath;
extern NSString *const kPAPublishPath;
extern NSString *const kPAUserProfilePath;
extern NSString *const kPAVaultPath;
extern NSString *const kPASysProfilePath;
extern NSString *const kPAUserServerPath;

#endif
