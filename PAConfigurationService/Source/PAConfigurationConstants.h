//
//  PAConfigurationConstants.h
//  PAConfigurationService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifndef PAConfigurationConstants_h
#define PAConfigurationConstants_h

extern NSString *const kPAMainAppPath;
extern NSString *const kPAServerRoot;
extern NSString *const kPAUserRoot;
extern NSString *const kPAProactiveAssistFolder;
extern NSString *const kPAProviderId;
extern NSString *const kPADataFolderPath;

extern NSString *const kPAServerRootPlaceholder;
extern NSString *const kPAUserRootPlaceholder;
extern NSString *const kPAProviderPathPlaceholder;
extern NSString *const kPAUserNamePlaceholder;
extern NSString *const kPASystemRootPlaceholder;
extern NSString *const kPAAppFolderNamePlaceholder;

extern NSString *const kPAManifestXmlFileName;
extern NSString *const kPAManifestZipFileName;

extern NSString *const kPALogsFolderPath;
extern NSString *const kPALogLevel;

extern NSString *const kPABackupFolderPath;
extern NSString *const kPAStateFolderPath;
extern NSString *const kPADefaultXmlFilePath;

extern NSString *const kPANotificationTrayExecutablePath;
extern NSString *const kPAMainAppPathPlaceholder;
extern NSString *const kPAUserDataPlaceholder;
extern NSString *const kPAProtectionFolderPath;
extern NSString *const kPADownloadFolderPath;
extern NSString *const kPAUserAgentBinFolderPath;
extern NSString *const kPAMessagesFolderPath;
extern NSString *const kPASmartIssuesFolderPath;
extern NSString *const kPASnapinFolderPath;
extern NSString *const kPASmartIssueDefaultXmlSubPath;
extern NSString *const kPADataFolderName;
extern NSString *const kPASelfHealTriggerFolderPath;
extern NSString *const kPASelfHealMonitorPluginPaths;

extern NSString *const kPAUserAgentLogInfo;
extern NSString *const kPAPAUILogInfo;
extern NSString *const kPALogFormat;

extern NSString *const kPAGuidDotVersionPlaceholder;
extern NSString *const kPARealTimeAlertsFolderPath;
extern NSString *const kPASystemRoot;
extern NSString *const kPAHkcuRegistryPlistPath;
extern NSString *const kPAHklmRegistryPlistPath;
extern NSString *const kPASelfHealRegistryPlistPath;
/// Real time alert configuration keys
extern NSString *const kPARealTimeAlertInfo;
extern NSString *const kPAXmppHost;
extern NSString *const kPAXmppPort;
extern NSString *const kPAXmppUserName;
extern NSString *const kPAXmppPassword;
extern NSString *const kPAXmppKey;
extern NSString *const kPAAcknowledgeUrlPath;
extern NSString *const kPADeliveryUrlPath;
extern NSString *const kPAGetTokenUrlPath;
extern NSString *const kPAGetServerUrlPath;
extern NSString *const kPAGetPasswordUrlPath;
extern NSString *const kPAPasswordDecryptionKey;
extern NSString *const kPAXmppGrantType;

/// install keys
extern NSString *const kPAInstallInfo;
extern NSString *const kPABuildVersion;
extern NSString *const kPADisplayVersion;
extern NSString *const kPAInstallDate;
extern NSString *const kPAInstallTime;
extern NSString *const kPAInstalledSetup;

// authorized CNs name
extern NSString *const kPAAuthorizedCNs;
// supported sprockets
extern NSString *const kPASupportedSprockets;

extern NSString *const kPAAgentCoreFolderPath;

extern NSString *const kPAContextInfo;
extern NSString *const kPAConfigCfgFileName;

extern NSString *const kPAServerBasePlaceholder;
extern NSString *const kPAServerBaseUrl;

extern NSString *const kPASkinName;
extern NSString *const kPASkinNamePlaceHolder;

extern NSString *const kPAClientUiConfigFilePath;
extern NSString *const kPAOptimizationFolderPath;
extern NSString *const kPASyncCertificatePath;
extern NSString *const kPASupportActionCertificatePath;
extern NSString *const kPASupportActionFolderPath;
extern NSString *const kPAPrivilegeToolHelperPath;
extern NSString *const kPADesktopPathPlaceholder;
extern NSString *const kPAAlertRunListFilePath;
extern NSString *const kPARealTimeAlertRunListFilePath;
extern NSString *const kPAAlertsFolderPath;
extern NSString *const kPASetProfileUrlPath;

/// command info keys
extern NSString *const kPACommandInfo;
extern NSString *const kPACommand;
extern NSString *const kPAAppName;
extern NSString *const kPAIBundleId;
extern NSString *const kPALaunchOption;
extern NSString *const kPAPath;

extern NSString *const kPAAssistAgentFolderPath;

#endif /* PAConfigurationConstants_h */
