//
//  PAAppConfiguration.m
//  PAConfigurationService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PAAppConfiguration.h"
#import "PAConfigurationConstants.h"

NSString *const kPAAppPath = @"/Applications/ProactiveAssist.app/Contents/Resources";
NSString *const kPAAppConfigurationsFile = @"AppConfigurations.plist";

@implementation PAAppConfiguration

- (instancetype)init
{
  self = [super init];
  if (self) {
    [self readConfigurations];
  }
  return self;
}

- (void)readConfigurations {
  NSString *configPath = kPAAppPath;
  configPath = [configPath stringByAppendingPathComponent:kPAAppConfigurationsFile];
  
  NSDictionary *configData = [NSDictionary dictionaryWithContentsOfFile:configPath];
  _mainAppPath = configData[kPAMainAppPath];
  _serverRoot = configData[kPAServerRoot];
  _userRoot = configData[kPAUserRoot];
  _appFolder = configData[kPAProactiveAssistFolder];
  _providerId = configData[kPAProviderId];
  _dataFolderPath = configData[kPADataFolderPath];
  _serverRootPlaceholder = configData[kPAServerRootPlaceholder];
  _userRootPlaceholder = configData[kPAUserRootPlaceholder];
  _systemRootPlaceholder = configData[kPASystemRootPlaceholder];
  _appFolderPlaceholder = configData[kPAAppFolderNamePlaceholder];
  _manifestXmlFile = configData[kPAManifestXmlFileName];
  _manifestZipFile = configData[kPAManifestZipFileName];
  _logsFolderPath = configData[kPALogsFolderPath];
  _backupFolderPath = configData[kPABackupFolderPath];
  _stateFolderPath = configData[kPAStateFolderPath];
  _macDefaultXmlFilePath = configData[kPADefaultXmlFilePath];
  
  _providerPathPlaceholder = configData[kPAProviderPathPlaceholder];
  _userNamePlaceholder = configData[kPAUserNamePlaceholder];
  _mainAppPathPlaceholder = configData[kPAMainAppPathPlaceholder];
  _notificationTrayExecutablePath = configData[kPANotificationTrayExecutablePath];
  _protectionFolderPath = configData[kPAProtectionFolderPath];
  _downloadFolderPath = configData[kPADownloadFolderPath];
  _userDataPlaceholder = configData[kPAUserDataPlaceholder];
  _userAgentBinFolderPath = configData[kPAUserAgentBinFolderPath];
  _messagesFolderPath = configData[kPAMessagesFolderPath];
  _smartIssuesFolderPath = configData[kPASmartIssuesFolderPath];
  _snapinFolderPath = configData[kPASnapinFolderPath];
  _smartIssueDefaultXmlSubPath = configData[kPASmartIssueDefaultXmlSubPath];
  _dataFolderName = configData[kPADataFolderName];
  _guidVersionPlaceholder = configData[kPAGuidDotVersionPlaceholder];
  _realTimeAlertsFolderPath = configData[kPARealTimeAlertsFolderPath];
  _systemRoot = configData[kPASystemRoot];
  _agentCoreFolderPath = configData[kPAAgentCoreFolderPath];
  _hkcuRegistryPath = configData[kPAHkcuRegistryPlistPath];
  _hklmRegistryPath = configData[kPAHklmRegistryPlistPath];
  _selfHealRegistryPlistPath = configData[kPASelfHealRegistryPlistPath];
  _skinName = configData[kPASkinName];
  _skinNamePlaceHolder = configData[kPASkinNamePlaceHolder];
  _configCfgFile = configData[kPAConfigCfgFileName];
  _serverBaseUrlPath = configData[kPAServerBaseUrl];
  _serverBasePlaceholder = configData[kPAServerBasePlaceholder];
  _clientUiConfigFilePath = configData[kPAClientUiConfigFilePath];
  _optimizationFolderPath = configData[kPAOptimizationFolderPath];
  _syncCertificatePath = configData[kPASyncCertificatePath];
  _supportActionCertificatePath = configData[kPASupportActionCertificatePath];
  _supportActionFolderPath = configData[kPASupportActionFolderPath];
  _privilegeToolHelperPath = configData[kPAPrivilegeToolHelperPath];
  _desktopPathPlaceholder = configData[kPADesktopPathPlaceholder];
  _alertRunListFilePath = configData[kPAAlertRunListFilePath];
  _realTimeAlertRunListFilePath = configData[kPARealTimeAlertRunListFilePath];
  _alertsFolderPath = configData[kPAAlertsFolderPath];
  _setProfileUrlPath = configData[kPASetProfileUrlPath];
  _assistAgentFolderPath = configData[kPAAssistAgentFolderPath];
  _selfHealTriggerFolderPath = configData[kPASelfHealTriggerFolderPath];
  _selfHealMonitorPluginPaths = configData[kPASelfHealMonitorPluginPaths];
  /// get context info
  _contextConfiguration = [[PAContextConfiguration alloc] initWithConfigDict:configData[kPAContextInfo]];
  
  /// get real time alert info
  _realtimeAlertconfigurations = [[PARealTimeAlertConfiguration alloc] initWithXmppDict:configData[kPARealTimeAlertInfo]];
  // agent log info
  NSDictionary *agentLogInfo = configData[kPAUserAgentLogInfo];
  _agentLogConfiguration =[[PALogInfoConfiguration alloc] initWithLogInfoDict:agentLogInfo];
  // install info
  NSDictionary *installInfo = configData[kPAInstallInfo];
  _installInfo =[[PAInstallInfo alloc] initWithInstallInfoDict:installInfo];
  // PA UI log info
  NSDictionary *appUiLogInfo = configData[kPAPAUILogInfo];
  _paUiLogConfiguration =[[PALogInfoConfiguration alloc] initWithLogInfoDict:appUiLogInfo];
  // get authorized CNs
  NSArray *authorizedCns = configData[kPAAuthorizedCNs];
  if ([authorizedCns isKindOfClass:[NSArray class]]) {
    _authorizedCnList = authorizedCns;
  }
  // get supported sprockets
  NSArray *supportedSprockets = configData[kPASupportedSprockets];
  if ([supportedSprockets isKindOfClass:[NSArray class]]) {
    _supportedSprockets = supportedSprockets;
  }
  
  /// get command info
  [self constructCommandInfoListFrom:configData];
}

- (void)constructCommandInfoListFrom:(NSDictionary *)configDict {
  /// get command info
  NSArray *commandInfoList = configDict[kPACommandInfo];
  if ([commandInfoList isKindOfClass:[NSArray class]]) {
    NSMutableArray *cmdList = [[NSMutableArray alloc] init];
    for (NSDictionary *commandInfoDict in commandInfoList) {
      PACommandInfo *cmdInfo = [[PACommandInfo alloc] initWithCommandInfoDict:commandInfoDict];
      if (cmdInfo) {
        [cmdList addObject:cmdInfo];
      }
    }
    if (cmdList.count) {
      _commandList = [NSArray arrayWithArray:cmdList];
    }
  }
  
  
}

- (PACommandInfo *)commandInfoMatchingCommand:(NSString *)command {
  if (!_commandList.count) {
    return nil;
  }
  NSArray *filtered = [_commandList filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(command == %@)", command]];
  if (filtered.count) {
    return filtered[0];
  }
  return nil;
}


@end
