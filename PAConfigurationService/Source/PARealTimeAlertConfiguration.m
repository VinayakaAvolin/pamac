//
//  PARealTimeAlertConfiguration.m
//  PAConfigurationService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PARealTimeAlertConfiguration.h"
#import "PAConfigurationConstants.h"

@implementation PARealTimeAlertConfiguration


- (instancetype)initWithXmppDict:(NSDictionary *)xmppDetail {
  self = [super init];
  if (self) {
    if (xmppDetail) {
      _userName = xmppDetail[kPAXmppUserName];
      _password = xmppDetail[kPAXmppPassword];
      _key = xmppDetail[kPAXmppKey];
      _grantType = xmppDetail[kPAXmppGrantType];
      _acknowledgeUrlPath = xmppDetail[kPAAcknowledgeUrlPath];
      _deliveryUrlPath = xmppDetail[kPADeliveryUrlPath];
      _getTokenUrlPath = xmppDetail[kPAGetTokenUrlPath];
      _getServerUrlPath = xmppDetail[kPAGetServerUrlPath];
      _getPasswordUrlPath = xmppDetail[kPAGetPasswordUrlPath];
      _passwordDecryptionKey = xmppDetail[kPAPasswordDecryptionKey];
    } else {
      self= nil;
    }
  }
  return self;
}
@end
