//
//  PAConfigurationConstants.m
//  PAConfigurationService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PAConfigurationConstants.h"

NSString *const kPAMainAppPath = @"MainAppPath";
NSString *const kPAServerRoot = @"ServerRoot";
NSString *const kPAUserRoot = @"UserRoot";
NSString *const kPAProactiveAssistFolder = @"AppFolder";
NSString *const kPAProviderId = @"ProviderId";
NSString *const kPADataFolderPath = @"DataFolderPath";

NSString *const kPAServerRootPlaceholder = @"ServerRootPlaceholder";
NSString *const kPAUserRootPlaceholder = @"UserRootPlaceholder";
NSString *const kPAProviderPathPlaceholder = @"ProviderPathPlaceholder";
NSString *const kPAUserNamePlaceholder = @"UserNamePlaceholder";
NSString *const kPASystemRootPlaceholder = @"SystemRootPlaceholder";
NSString *const kPAAppFolderNamePlaceholder = @"AppFolderPlaceholder";

NSString *const kPAManifestXmlFileName = @"ManifestFile_xml";
NSString *const kPAManifestZipFileName = @"ManifestFile_zip";
NSString *const kPALogsFolderPath = @"LogsFolderPath";
NSString *const kPALogLevel = @"LogLevel";

NSString *const kPABackupFolderPath = @"BackupFolderPath";
NSString *const kPAStateFolderPath = @"StateFolderPath";
NSString *const kPADefaultXmlFilePath = @"MacDefaultXmlPath";
NSString *const kPANotificationTrayExecutablePath = @"NotificationTrayExecutablePath";
NSString *const kPAMainAppPathPlaceholder = @"MainAppPathPlaceholder";

NSString *const kPAUserDataPlaceholder = @"UserDataPlaceholder";
NSString *const kPAProtectionFolderPath = @"ProtectionFolderPath";
NSString *const kPADownloadFolderPath = @"DownloadFolderPath";
NSString *const kPAUserAgentBinFolderPath = @"UserAgentBinPath";
NSString *const kPAMessagesFolderPath = @"MessagesFolderPath";
NSString *const kPASmartIssuesFolderPath = @"SmartIssuesFolderPath";
NSString *const kPASnapinFolderPath = @"SnapinFolderPath";
NSString *const kPASmartIssueDefaultXmlSubPath = @"SmartIssueDefaultXmlSubPath";
NSString *const kPADataFolderName = @"DataFolderName";
NSString *const kPASelfHealTriggerFolderPath = @"SelfHealTriggerFolderPath";
NSString *const kPASelfHealMonitorPluginPaths = @"SelfHealMonitorPluginPaths";

NSString *const kPAUserAgentLogInfo = @"AgentLogInfo";
NSString *const kPAPAUILogInfo = @"PAUILogInfo";
NSString *const kPALogFormat = @"LogFormat";

NSString *const kPAGuidDotVersionPlaceholder = @"GuidDotVersionPlaceholder";
NSString *const kPARealTimeAlertsFolderPath = @"RealTimeAlertsFolderPath";
NSString *const kPASystemRoot = @"SystemRoot";
NSString *const kPAAgentCoreFolderPath = @"AgentCorePath";
NSString *const kPAHkcuRegistryPlistPath = @"HkcuRegistryPath";
NSString *const kPAHklmRegistryPlistPath = @"HklmRegistryPath";
NSString *const kPASelfHealRegistryPlistPath = @"SelfHealRegistryPath";

/// Real time alert configuration keys
NSString *const kPARealTimeAlertInfo = @"RealTimeAlertInfo";
NSString *const kPAXmppHost = @"XMPP Host";
NSString *const kPAXmppPort = @"XMPP Port";
NSString *const kPAXmppUserName = @"UserName";
NSString *const kPAXmppPassword = @"Password";
NSString *const kPAXmppKey = @"Key";
NSString *const kPAAcknowledgeUrlPath = @"AcknowledgeUrlPath";
NSString *const kPADeliveryUrlPath = @"DeliveryUrlPath";
NSString *const kPAGetTokenUrlPath = @"GetTokenUrlPath";
NSString *const kPAGetServerUrlPath = @"GetServerUrlPath";
NSString *const kPAGetPasswordUrlPath = @"GetPasswordUrlPath";
NSString *const kPAPasswordDecryptionKey = @"Key";
NSString *const kPAXmppGrantType = @"GrantType";

/// install keys
NSString *const kPAInstallInfo = @"InstallInfo";
NSString *const kPABuildVersion = @"BuildVersion";
NSString *const kPADisplayVersion = @"DisplayVersion";
NSString *const kPAInstallDate = @"InstallDate";
NSString *const kPAInstallTime = @"InstallTime";
NSString *const kPAInstalledSetup = @"InstalledSetup";

// authorized CNs name
NSString *const kPAAuthorizedCNs = @"AuthorizedCNs";
// supported sprockets
NSString *const kPASupportedSprockets = @"SupportedSprockets";

NSString *const kPAContextInfo = @"ContextConfiguration";
NSString *const kPAConfigCfgFileName = @"ConfigCfgFileName";

NSString *const kPAServerBasePlaceholder = @"ServerBasePlaceholder";
NSString *const kPAServerBaseUrl = @"ServerBaseUrl";

NSString *const kPASkinName = @"SkinName";
NSString *const kPASkinNamePlaceHolder = @"SkinNamePlaceholder";

NSString *const kPAClientUiConfigFilePath = @"ClientUiConfigFilePath";
NSString *const kPAOptimizationFolderPath = @"OptimizationFolderPath";
NSString *const kPASyncCertificatePath = @"SyncCertificatePath";
NSString *const kPASupportActionCertificatePath = @"SupportActionCertificatePath";
NSString *const kPASupportActionFolderPath = @"SupportActionFolderPath";
NSString *const kPAPrivilegeToolHelperPath = @"PrivilegeToolHelperPath";
NSString *const kPADesktopPathPlaceholder = @"DesktopPathPlaceholder";
NSString *const kPAAlertRunListFilePath = @"AlertRunListFilePath";
NSString *const kPARealTimeAlertRunListFilePath = @"RealTimeAlertRunListFilePath";
NSString *const kPAAlertsFolderPath = @"AlertsFolderPath";
NSString *const kPASetProfileUrlPath = @"SetProfileUrlPath";

/// command info keys
NSString *const kPACommandInfo = @"RunCommandInfo";
NSString *const kPACommand = @"command";
NSString *const kPAAppName = @"appName";
NSString *const kPAIBundleId = @"bundleId";
NSString *const kPALaunchOption = @"launchOption";
NSString *const kPAPath = @"path";

NSString *const kPAAssistAgentFolderPath = @"AssistAgentPath";

