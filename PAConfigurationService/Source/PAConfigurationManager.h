//
//  PAConfigurationManager.h
//  PAConfigurationService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PAAppConfiguration.h"
#import "PAContextConfiguration.h"

@interface PAConfigurationManager : NSObject

+ (PAAppConfiguration *)appConfigurations;

@end
