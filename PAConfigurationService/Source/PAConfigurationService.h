//
//  PAConfigurationService.h
//  PAConfigurationService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Cocoa/Cocoa.h>

//! Project version number for PAConfigurationService.
FOUNDATION_EXPORT double PAConfigurationServiceVersionNumber;

//! Project version string for PAConfigurationService.
FOUNDATION_EXPORT const unsigned char PAConfigurationServiceVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <PAConfigurationService/PublicHeader.h>

#import <PAConfigurationService/PAConfigurationManager.h>
#import <PAConfigurationService/PAAppConfiguration.h>
#import <PAConfigurationService/PAContextConfiguration.h>
#import <PAConfigurationService/PARealTimeAlertConfiguration.h>
#import <PAConfigurationService/PALogInfoConfiguration.h>
#import <PAConfigurationService/PAInstallInfo.h>
#import <PAConfigurationService/PACommandInfo.h>
