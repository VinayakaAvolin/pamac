//
//  PAConfigurationManager.m
//  PAConfigurationService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PAConfigurationManager.h"

@implementation PAConfigurationManager

+ (PAAppConfiguration *)appConfigurations {
  return [[PAAppConfiguration alloc]init];
}

@end
