//
//  PARealTimeAlertConfiguration.h
//  PAConfigurationService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PARealTimeAlertConfiguration : NSObject

@property(nonatomic, readonly) NSString *userName;
@property(nonatomic, readonly) NSString *password;
@property(nonatomic, readonly) NSString *key;
@property(nonatomic, readonly) NSString *grantType;
@property(nonatomic, readonly) NSString *acknowledgeUrlPath;
@property(nonatomic, readonly) NSString *deliveryUrlPath;
@property(nonatomic, readonly) NSString *getTokenUrlPath;
@property(nonatomic, readonly) NSString *getServerUrlPath;
@property(nonatomic, readonly) NSString *getPasswordUrlPath;
@property(nonatomic, readonly) NSString *passwordDecryptionKey;

- (instancetype)initWithXmppDict:(NSDictionary *)xmppDetail;

@end
