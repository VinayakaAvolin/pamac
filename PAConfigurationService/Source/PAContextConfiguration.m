//
//  PAContextConfiguration.m
//  PAConfigurationService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PAContextConfiguration.h"
#import "PAContextConfigurationConstants.h"

@implementation PAContextConfiguration

- (instancetype)initWithConfigDict:(NSDictionary *)configDict {
  self = [super init];
  if (self) {
    if (configDict) {
      _actionsPath = configDict[kPAActionsPath];
      _backupPath = configDict[kPABackupPath];
      _binPath = configDict[kPABinPath];
      _dataPath = configDict[kPADataPath];
      _dnaPath = configDict[kPADnaPath];
      _dnaBackupPath = configDict[kPADnaBackupPath];
      _issuePath = configDict[kPAIssuePath];
      _logsPath = configDict[kPALogsPath];
      _profilesPath = configDict[kPAProfilesPath];
      _publishPath = configDict[kPAPublishPath];
      _userProfilePath = configDict[kPAUserProfilePath];
      _vaultPath = configDict[kPAVaultPath];
      _sysProfilePath = configDict[kPASysProfilePath];
      _userServerPath = @"";
      _hkcu = @"";
      _providerId = @"SdcContext:ProviderId=Experience92";
      _userName = @"Test";
    } else {
      self= nil;
    }
  }
  return self;
}

- (NSArray*)configurations {
  return  @[_actionsPath,
            _backupPath,
            _binPath,
            _dataPath,
            _dnaPath,
            _dnaBackupPath,
            _issuePath,
            _logsPath,
            _profilesPath,
            _publishPath,
            _userProfilePath,
            _vaultPath,
            _sysProfilePath,
            _userServerPath,
            _hkcu,
            _providerId,
            _userName];
  
}
@end
