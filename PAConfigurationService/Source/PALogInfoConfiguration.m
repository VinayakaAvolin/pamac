//
//  PALogInfoConfiguration.m
//  PAConfigurationService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PALogInfoConfiguration.h"
#import "PAConfigurationConstants.h"

NSString *const kPALogFormatAlternate = @"[%d] [%l] [%n] [%f] [%M] %m";

@implementation PALogInfoConfiguration

- (instancetype)initWithLogInfoDict:(NSDictionary *)logDetail {
  self = [super init];
  if (self) {
    if (logDetail) {
      _logLevel = [logDetail[kPALogLevel] integerValue];
      _logFormat = logDetail[kPALogFormat];
      
      if (!_logFormat) {
        _logFormat = kPALogFormatAlternate;
      }
    } else {
      self= nil;
    }
  }
  return self;
}

@end
