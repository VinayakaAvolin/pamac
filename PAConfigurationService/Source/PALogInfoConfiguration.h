//
//  PALogInfoConfiguration.h
//  PAConfigurationService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PALogInfoConfiguration : NSObject

@property(nonatomic, readonly) NSUInteger logLevel;
@property(nonatomic, readonly) NSString *logFormat;

- (instancetype)initWithLogInfoDict:(NSDictionary *)logDetail;

@end
