//
//  PAInstallInfo.m
//  PAConfigurationService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PAInstallInfo.h"
#import "PAConfigurationConstants.h"

@implementation PAInstallInfo


- (instancetype)initWithInstallInfoDict:(NSDictionary *)installDetail {
  self = [super init];
  if (self) {
    if (installDetail) {
      _buildVersion = installDetail[kPABuildVersion];
      _displayVersion = installDetail[kPADisplayVersion];
      _installTime = installDetail[kPAInstallTime];
      _installDateStr = installDetail[kPAInstallDate];
      _installedSetup = installDetail[kPAInstalledSetup];
    } else {
      self= nil;
    }
  }
  return self;
}

- (NSDictionary*)dictRepresentation {
  return @{kPABuildVersion: _buildVersion, kPADisplayVersion: _displayVersion, kPAInstallTime: _installTime, kPAInstallDate: _installDateStr, kPAInstalledSetup:_installedSetup};
}
@end
