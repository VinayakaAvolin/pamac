//
//  PACommandInfo.h
//  PAConfigurationService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PACommandInfo : NSObject

@property(nonatomic, readonly) NSString *command;
@property(nonatomic, readonly) NSString *appName;
@property(nonatomic, readonly) NSString *appBundleId;
@property(nonatomic, readonly) NSUInteger launchOption;
@property(nonatomic, readonly) NSString *path;

- (instancetype)initWithCommandInfoDict:(NSDictionary *)commandDetail;
- (instancetype)initWithCommandPath:(NSString *)commandPath;

@end
