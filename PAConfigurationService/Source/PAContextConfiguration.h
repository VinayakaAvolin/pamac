//
//  PAContextConfiguration.h
//  PAConfigurationService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PAContextConfiguration : NSObject

@property(nonatomic) NSString *actionsPath;
@property(nonatomic) NSString *backupPath;
@property(nonatomic) NSString *binPath;
@property(nonatomic) NSString *dataPath;
@property(nonatomic) NSString *dnaPath;
@property(nonatomic) NSString *dnaBackupPath;
@property(nonatomic) NSString *issuePath;
@property(nonatomic) NSString *logsPath;
@property(nonatomic) NSString *profilesPath;
@property(nonatomic) NSString *publishPath;
@property(nonatomic) NSString *userProfilePath;
@property(nonatomic) NSString *vaultPath;
@property(nonatomic) NSString *sysProfilePath;
@property(nonatomic) NSString *userServerPath;
@property(nonatomic) NSString *hkcu;
@property(nonatomic) NSString *providerId;
@property(nonatomic) NSString *userName;

- (NSArray*)configurations;
- (instancetype)initWithConfigDict:(NSDictionary *)configDict;

@end
