//
//  PAContextConfigurationConstants.m
//  PAConfigurationService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PAContextConfigurationConstants.h"

NSString *const kPAActionsPath = @"ActionsPath";

NSString *const kPABackupPath = @"BackupPath";

NSString *const kPABinPath = @"BinPath";

NSString *const kPADataPath = @"DataPath";

NSString *const kPADnaPath = @"DnaPath";

NSString *const kPADnaBackupPath = @"DnaBackupPath";

NSString *const kPAIssuePath = @"IssuePath";

NSString *const kPALogsPath = @"LogsPath";

NSString *const kPAProfilesPath = @"ProfilesPath";

NSString *const kPAPublishPath = @"PublishPath";

NSString *const kPAUserProfilePath = @"UserProfilePath";

NSString *const kPAVaultPath = @"VaultPath";

NSString *const kPASysProfilePath = @"SysProfilePath";

NSString *const kPAUserServerPath = @"UserServerPath";



