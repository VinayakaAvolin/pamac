//
//  PACommandInfo.m
//  PAConfigurationService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PACommandInfo.h"
#import "PAConfigurationConstants.h"

@implementation PACommandInfo

- (instancetype)initWithCommandInfoDict:(NSDictionary *)commandDetail {
  self = [super init];
  if (self) {
    if (commandDetail) {
      _appName = commandDetail[kPAAppName];
      _appBundleId = commandDetail[kPAIBundleId];
      _command = commandDetail[kPACommand];
      _launchOption = [commandDetail[kPALaunchOption] integerValue];
      _path = commandDetail[kPAPath];
    } else {
      self= nil;
    }
  }
  return self;
}

- (instancetype)initWithCommandPath:(NSString *)commandPath {
  self = [super init];
  if (self) {
    if (commandPath) {
      _path = commandPath;
      _appName = _path.lastPathComponent;
      _command = commandPath;
      _launchOption = 1;
    } else {
      self= nil;
    }
  }
  return self;
}

@end
