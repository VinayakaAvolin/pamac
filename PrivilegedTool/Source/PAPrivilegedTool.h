//
//  PAPrivilegedTool.h
//  PrivilegedTool
//
//  Copyright © 2017 Avolin. All rights reserved.
//


#import <Foundation/Foundation.h>

// The following is the interface to the class that implements the helper tool.
// It's called by the helper tool's main() function, but not by the app directly.

@interface PAPrivilegedTool : NSObject

- (id)init;
- (void)run;

@end
