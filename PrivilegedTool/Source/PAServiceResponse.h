//
//  PAServiceResponse.h
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PAServiceResponse : NSObject <NSSecureCoding>

@property (nonatomic, assign) BOOL success; //
@property (nonatomic) NSError *error; //
@property (nonatomic) NSString *resultStr; //
@property (nonatomic) NSArray *resultArray; //
@property (nonatomic) NSNumber *resultNumber; //
@property (nonatomic) NSDictionary *resultDict; //

- (instancetype)initWithSucessState:(BOOL)success
                             result:(id)result
                              error:(NSError *)error;

- (id)getResult;

@end
