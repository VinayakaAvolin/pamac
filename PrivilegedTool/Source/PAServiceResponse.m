//
//  PAServiceResponse.m
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PAServiceResponse.h"

@implementation PAServiceResponse

+ (BOOL)supportsSecureCoding {
  return YES;
}

- (id)initWithCoder:(NSCoder *)coder {
  if ((self = [super init])) {
    // Decode the property values by key, specifying the expected class
    _resultStr = [coder decodeObjectOfClass:[NSString class] forKey:@"_resultStr"];
    _resultNumber = [coder decodeObjectOfClass:[NSNumber class] forKey:@"_resultNumber"];
    
    NSSet *setForDictArray = [NSSet setWithObjects:[NSDictionary class],[NSArray class], [NSString class], [NSNumber class], [NSDate class], [NSURL class], [NSData class], [NSError class], nil];
    _resultArray = [coder decodeObjectOfClasses:setForDictArray forKey:@"_resultArray"];
    _resultDict = [coder decodeObjectOfClasses:setForDictArray forKey:@"_resultDict"];
    
    _success = [coder decodeBoolForKey:@"_success"];
    _error = [coder decodeObjectOfClass:[NSError class] forKey:@"_error"];
  }
  return self;
}

- (void)encodeWithCoder:(NSCoder *)coder {
  // Encode our ivars using string keys as normal
  [coder encodeObject:_resultStr forKey:@"_resultStr"];
  [coder encodeObject:_resultNumber forKey:@"_resultNumber"];
  [coder encodeObject:_resultArray forKey:@"_resultArray"];
  [coder encodeObject:_resultDict forKey:@"_resultDict"];
  [coder encodeBool:_success forKey:@"_success"];
  [coder encodeObject:_error forKey:@"_error"];
}

- (instancetype)initWithSucessState:(BOOL)success
                             result:(id)result
                              error:(NSError *)error {
  self = [super init];
  if (self) {
    _success = success;
    [self setResultValue:result];
    _error = error;
  }
  return self;
}


- (void)setResultValue:(id)result {
  if(!result) {
    return;
  }
  if ([result isKindOfClass:[NSString class]]) {
    _resultStr = (NSString *)result;
  } else if ([result isKindOfClass:[NSNumber class]]) {
    _resultNumber = (NSNumber *)result;
  }else if ([result isKindOfClass:[NSArray class]]) {
    _resultArray = (NSArray *)result;
  } else if ([result isKindOfClass:[NSDictionary class]]) {
    _resultDict = (NSDictionary *)result;
  }
}

- (id)getResult {
  if (_resultStr) {
    return _resultStr;
  }
  if (_resultNumber) {
    return _resultNumber;
  }
  if (_resultArray) {
    return _resultArray;
  }
  if (_resultDict) {
    return _resultDict;
  }
  
  return nil;
}
@end
