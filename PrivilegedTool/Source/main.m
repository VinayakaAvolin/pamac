//
//  main.m
//  PrivilegedTool
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PAPrivilegedTool.h"

int main(int argc, const char * argv[]) {
  @autoreleasepool {
    // insert code here...
    PAPrivilegedTool *  m;
    
    m = [[PAPrivilegedTool alloc] init];
    [m run];
  }
  return EXIT_FAILURE;
}
