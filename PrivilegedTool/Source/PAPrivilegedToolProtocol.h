//
//  PAPrivilegedToolXpcProtocol.h
//  PrivilegedTool
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <PACoreServices/PACoreServices.h>
#import <PASupportService/PASupportService.h>
#import "PAServiceResponse.h"
#import <PAEventBus/PAEventBus.h>

typedef void(^PAPrivilegedToolScriptResult)(PAServiceResponse *response);
typedef void(^PAPrivilegedToolServiceResult)(PAServiceResponse *response);
typedef void(^PAPrivilegedToolPlistServiceResult)(BOOL success);
typedef void(^PAPrivilegedToolMonitorServiceResult)(PASelfHealEvent *event);
typedef void(^PAPrivilegedToolStopMonitorResult)(BOOL success);


// kHelperToolMachServiceName is the Mach service name of the helper tool.  Note that the value
// here has to match the value in the MachServices dictionary in "HelperTool-Launchd.plist".

#define kPAPrivilegedToolMachServiceName @"PAPrivilegedTool"

// HelperToolProtocol is the NSXPCConnection-based protocol implemented by the helper tool
// and called by the app.

@protocol PAPrivilegedToolProtocol

@required

// Not used by the standard app (it's part of the sandboxed XPC service support).
- (void)executeScript:(PAScriptInfo *)scriptInfo withReply:(PAPrivilegedToolScriptResult)reply;
- (void)executeService:(PAServiceInfo *)service withReply:(PAPrivilegedToolServiceResult)reply;
- (void)executePlistService:(PAPlistServiceInfo *)service withReply:(PAPrivilegedToolPlistServiceResult)reply;
- (void)executeMonitorService:(PASelfHealTriggerInfo *)triggerInfo withReply: (PAPrivilegedToolMonitorServiceResult)reply;

-(void) stopMonitoringForEvent:(NSString *)eventGuid withReply:(PAPrivilegedToolStopMonitorResult)reply;
-(void)registerListenerEndpoint:(NSXPCListenerEndpoint *)endpoint;

@end
