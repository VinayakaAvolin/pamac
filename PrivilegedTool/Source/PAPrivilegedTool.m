//
//  PAPrivilegedTool.m
//  PrivilegedTool
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <PACoreServices/PACoreServices.h>
#import <PAEventMonitorManager/PAEventMonitorManager.h>
#import "PAPrivilegedTool.h"
#import "PAPrivilegedToolProtocol.h"
#import "PAPlistLockManager.h"
#import "PASelfHealTriggerInfo.h"
#import "EventDeliveryProtocol.h"

@interface PAPrivilegedTool () <NSXPCListenerDelegate, PAPrivilegedToolProtocol>
{
    NSXPCConnection *_processConnection;
    id<EventDeliveryProtocol> _remoteObjectProxy;
}
@property (atomic, strong, readwrite) NSXPCListener *    listener;

@end

@implementation PAPrivilegedTool

- (id)init {
  self = [super init];
  if (self != nil) {
    // Set up our XPC listener to handle requests on our Mach service.
    self->_listener = [[NSXPCListener alloc] initWithMachServiceName:kPAPrivilegedToolMachServiceName];
    self->_listener.delegate = self;
  }
  return self;
}

- (void)run {
  // Tell the XPC listener to start processing requests.
  [self.listener resume];
  
  // Run the run loop forever.
  [[NSRunLoop currentRunLoop] run];
}

- (BOOL)listener:(NSXPCListener *)listener shouldAcceptNewConnection:(NSXPCConnection *)newConnection {
  // Called by our XPC listener when a new connection comes in.  We configure the connection
  // with our protocol and ourselves as the main object.
  assert(listener == self.listener);
#pragma unused(listener)
  assert(newConnection != nil);
  
  NSXPCInterface *interface= [NSXPCInterface interfaceWithProtocol:@protocol(PAPrivilegedToolProtocol)];
  NSSet *executeScriptClassList = [NSSet setWithObjects:[PAScriptInfo class], [NSString class], nil];
  NSSet *executeServiceClassList = [NSSet setWithObjects:[PAServiceInfo class], [NSString class], nil];
  NSSet *executePlistServiceClassList = [NSSet setWithObjects:[PAPlistServiceInfo class], [NSString class], nil];
    NSSet *executeMonitorServiceClassList = [NSSet setWithObjects:[PASelfHealTriggerInfo class], [PASelfHealEvent class], [NSString class], [NSArray class],[NSDictionary class],[NSXPCListenerEndpoint class], nil];
    NSSet *stopMonitorClassList = [NSSet setWithObjects:[NSString class], nil];
    
  [interface setClasses:executeScriptClassList
            forSelector:@selector(executeScript:withReply:)
          argumentIndex:0
                ofReply:NO];
  [interface setClasses:executeServiceClassList
            forSelector:@selector(executeService:withReply:)
          argumentIndex:0
                ofReply:NO];
  [interface setClasses:executePlistServiceClassList
            forSelector:@selector(executePlistService:withReply:)
          argumentIndex:0
                ofReply:NO];
    [interface setClasses:executeMonitorServiceClassList
              forSelector:@selector(executeMonitorService:withReply:)
            argumentIndex:0
                  ofReply:YES];
    [interface setClasses:stopMonitorClassList
              forSelector:@selector(stopMonitoringForEvent:withReply:)
            argumentIndex:0
                  ofReply:NO];
    [interface setClasses:executeMonitorServiceClassList
              forSelector:@selector(registerListenerEndpoint:)
            argumentIndex:0
                  ofReply:NO];
    
  newConnection.exportedInterface = interface;
  newConnection.exportedObject = self;
  [newConnection resume];
  
  return YES;
}


#pragma mark * HelperToolProtocol implementation

- (void)executeScript:(PAScriptInfo *)scriptInfo withReply:(PAPrivilegedToolScriptResult)reply {
  NSString *path = scriptInfo.scriptPath;
  NSString *script = scriptInfo.scriptString;
  PAScriptType scriptType = scriptInfo.scriptType;
  
  if (path) {
    [PAScriptRunner executeScriptWithPath:path callBack:^(BOOL success, NSError *error, id result) {
      PAServiceResponse *resp = [[PAServiceResponse alloc]initWithSucessState:success result:result error:error];
      reply(resp);
    }];
  } else if (script && scriptType != PAScriptTypeNone) {
    [PAScriptRunner executeScript:script ofType:scriptType callBack:^(BOOL success, NSError *error, id result) {
      PAServiceResponse *resp = [[PAServiceResponse alloc]initWithSucessState:success result:result error:error];
      reply(resp);
    }];
  } else {
    PAServiceResponse *resp = [[PAServiceResponse alloc]initWithSucessState:false result:nil error:nil];
    reply(resp);
  }
}

- (void)executeService:(PAServiceInfo *)service withReply:(PAPrivilegedToolServiceResult)reply {
  [PAServiceInterface executeService:service.service category:service.serviceCategory info:service callback:^(BOOL success, NSError *error, id result) {
    PAServiceResponse *response = [[PAServiceResponse alloc]initWithSucessState:success result:result error:error];
    reply(response);
  }];
}
- (void)executePlistService:(PAPlistServiceInfo *)service withReply:(PAPrivilegedToolPlistServiceResult)reply {
  
  __block BOOL result = false;
  PAPlistLockManager *lckMgr = [PAPlistLockManager sharedInstance];
  [lckMgr runWithWriteLock:service.plistFilePath codeBlock:^{
    switch (service.service) {
      case PAPlistServiceSetRegValue:
      {
        PARegistryWriter* writer = [[PARegistryWriter alloc] initWithPlistAtPath:service.plistFilePath];
        result = [writer setRegValueForPath:service.sectionPath withKey:service.regKey andData:service.regData];
        
      }
        
        break;
      case PAPlistServiceDeleteNodeForPath:
      {
        PARegistryWriter* writer = [[PARegistryWriter alloc] initWithPlistAtPath:service.plistFilePath];
        result = [writer deleteNodeForPath:service.sectionPath];
        
      }
        break;
        
      case PAPlistServiceDeleteKey:
      {
        PARegistryWriter* writer = [[PARegistryWriter alloc] initWithPlistAtPath:service.plistFilePath];
        result = [writer deleteKey:service.regKey forPath:service.sectionPath];
        
      }
        break;
        
      default:
        result = (false);
        break;
    }
  }];
  reply(result);
}

- (void)executeMonitorService:(PASelfHealTriggerInfo *)triggerInfo withReply: (PAPrivilegedToolMonitorServiceResult)reply {
    NSLog(@"Running in previlege...");
    __weak id<EventDeliveryProtocol>weakProxy = _remoteObjectProxy;
    PAEventMonitorInterface *monitor = [PAEventMonitorInterface sharedInstance];
    [monitor setupMonitoringServiceForEvent: triggerInfo withReply: ^(PASelfHealEvent *event) {
        //reply(event);
        [weakProxy publishEvent:event];
    }];
}

-(void) stopMonitoringForEvent:(NSString *)eventGuid withReply:(PAPrivilegedToolStopMonitorResult)reply {
    NSLog(@"Stopping monitoring in Priv Tool...");
    PAEventMonitorInterface *monitor = [PAEventMonitorInterface sharedInstance];
    [monitor stopMonitoring:eventGuid withReply:^(BOOL success) {
        reply(success);
    }];
}

-(void)registerListenerEndpoint:(NSXPCListenerEndpoint *)endpoint {
    _processConnection = [[NSXPCConnection alloc] initWithListenerEndpoint:endpoint];
    NSXPCInterface *interface = [NSXPCInterface interfaceWithProtocol:@protocol(EventDeliveryProtocol)];
    NSSet *monitorServiceClassList = [NSSet setWithObjects:[PASelfHealTriggerInfo class], [NSString class], [NSArray class],[NSDictionary class], [PASelfHealEvent class], nil];
    [interface setClasses:monitorServiceClassList
              forSelector:@selector(publishEvent:) argumentIndex:0 ofReply:NO];
    _processConnection.remoteObjectInterface = interface;
    _processConnection.interruptionHandler = ^(){
        NSLog(@"interrupted");
    };
    
    _processConnection.invalidationHandler = ^() {
        NSLog(@"invalidated");
    };
    [_processConnection resume];
    _remoteObjectProxy = [_processConnection remoteObjectProxyWithErrorHandler:^(NSError * err) {
        NSLog(@"%@", err);
    }];
}
@end
