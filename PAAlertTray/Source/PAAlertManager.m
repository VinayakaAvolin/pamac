//
//  PAAlertManager.m
//  PAAlertTray
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <PACoreServices/PACoreServices.h>
#import "PAAlertManager.h"
#import "PAMainViewController.h"
#import "NSStoryboard+Additions.h"
#import "PAAlertManager.h"
#import "PARealTimeAlertRequest.h"
#import "PANetworkServiceManager+RealTimeAlert.h"
#import "NSHost+Additions.h"
#import "PAAlertTrayConstants.h"
#import "PAEventRunListWriter.h"

#import <stdlib.h>
#import <limits.h>

NSString *const kPAMainWindowControllerId = @"MainWindowController";
NSTimeInterval const kRTAPruneTimerInterval = 1 * 60 * 60; /// 1 hour time interval

@interface PAAlertManager () <NSWindowDelegate> {
  NSWindowController *_mainWindowController;
  PAFileSystemManager *_fsManager;
  NSMutableArray *_alertQueue;
  NSMutableArray *_networkServiceManagers;
  PAAlertInfo *_currentAlertInfo;
  NSTimer *_alertDismissTimer;
  NSString *_tempFolderPath;
  BOOL _isUserDismissed;
}

@end

@implementation PAAlertManager

- (instancetype)init
{
  self = [super init];
  if (self) {
    _fsManager = [[PAFileSystemManager alloc] init];
    _alertQueue = [[NSMutableArray alloc] init];
    _networkServiceManagers = [[NSMutableArray alloc] init];
    _isUserDismissed = YES;
  }
  return self;
}

- (void)showRealTimeAlert:(PAAlertInfo *)alertInfo {
  /// if alert has valid html path and currently no alert is being shown; then proceed to show the alert window
  BOOL canShowAlert = [self canShowAlert:alertInfo];
  
  if (!canShowAlert) {
    [self terminateApp];
    return;
  }
  if (!_currentAlertInfo) {
    _currentAlertInfo = alertInfo; // assign current alert info
    [self showAlert];
  }
  /// add all alerts to queue
  [self addAlertToQueue:alertInfo];
}

- (BOOL)canTerminate {
  return (_currentAlertInfo == nil);
}
#pragma mark - Private methods

- (BOOL)canShowAlert:(PAAlertInfo *)alertInfo {
  if (!alertInfo) {
    return false;
  }
  
  if ([_fsManager doesFileExist:[self contentHtmlPathForAlert:alertInfo]]) {
    return true;
  }
  return false;
}
- (NSString *)contentHtmlPathForAlert:(PAAlertInfo *)alertInfo {
  NSString *htmlPath = alertInfo.alertContentHtmlPath;
  htmlPath = [htmlPath stringByRemovingPercentEncoding];
  htmlPath = [_fsManager stringByExpandingPath:htmlPath];
  return htmlPath;
}

- (void)showNextAlert {
  [self removeAlertDismissTimer];
  [self removeCurrentAlert];
  _currentAlertInfo = [self nextAlertInQueue];
  if ([self canShowAlert:_currentAlertInfo] ) {
    [self showAlert];
  } else {
    [self terminateApp];
  }
}

- (void)terminateApp {
  [[NSNotificationCenter defaultCenter] postNotificationName:kPANotificationNameTerminateApp object:nil];
}

- (void)addAlertToQueue:(PAAlertInfo *)alertInfo {
  if ([self isAlertInQueue:alertInfo]) {
    return;
  }
  [_alertQueue addObject:alertInfo];
}

- (void)removeCurrentAlert {
  [_alertQueue removeObject:_currentAlertInfo];
}

- (PAAlertInfo *)nextAlertInQueue {
  if (_alertQueue.count) {
    return _alertQueue.firstObject;
  }
  return nil;
}

- (void)showAlert {
  _isUserDismissed = YES;
  [self showAlertWindow];
  [self removeAlertDismissTimer];
  [self addAlertDismissTimer];
}

- (void)showAlertWindow {
  _mainWindowController = [self mainWindowController];
  PAMainViewController *mainvc = [PAMainViewController mainViewController];
  
  mainvc.webPageLink = @"testpage/testpage";
  _mainWindowController.contentViewController = mainvc;
  
  NSRect screenRect = [[NSScreen mainScreen] visibleFrame];
  NSRect windowRect = _mainWindowController.window.frame;
  windowRect.size.width = (_currentAlertInfo.windowWidth >= 100 ? _currentAlertInfo.windowWidth : 100);
  windowRect.size.height = (_currentAlertInfo.windowHeight >= 100 ? _currentAlertInfo.windowHeight : 100);
  windowRect.origin.x = NSWidth(screenRect) - NSWidth(windowRect) + NSMinX(screenRect);
  windowRect.origin.y = screenRect.origin.y;
  
  [_mainWindowController.window setFrameOrigin:windowRect.origin];
  [_mainWindowController.window setFrame:windowRect display:YES];
  [[_mainWindowController.window standardWindowButton:NSWindowMiniaturizeButton] setHidden:YES];
  [[_mainWindowController.window standardWindowButton:NSWindowZoomButton] setHidden:YES];
  [_mainWindowController.window makeKeyAndOrderFront:self];
  [NSApp activateIgnoringOtherApps:YES];
  [_mainWindowController showWindow:self];/// to show the window
  
  NSString *filePath = [self contentHtmlPathForAlert:_currentAlertInfo];
  
  NSFileManager *fileManager = [NSFileManager defaultManager];
  NSString *sourcePath = [filePath stringByDeletingLastPathComponent];
  NSArray *sourceFiles = [fileManager contentsOfDirectoryAtPath:sourcePath error:NULL];
  NSError *copyError = nil;
  NSString *newFilePath = filePath;
  
  BOOL isDirectory;
  _tempFolderPath = [NSTemporaryDirectory() stringByAppendingPathComponent:(_currentAlertInfo.contentGuid)];
  [[PALogger sharedLoggerForClient:PALogClientAlertTray] debug:@"*******Temporary folder is [%@]", _tempFolderPath];
  
  if((![fileManager fileExistsAtPath:_tempFolderPath isDirectory:&isDirectory]) || isDirectory) {
    if([fileManager createDirectoryAtPath:_tempFolderPath withIntermediateDirectories:NO attributes:nil error:nil]) {
      [[PALogger sharedLoggerForClient:PALogClientAlertTray] debug:@"*******Folder created or already exist"];
      
      for (NSString *currentFile in sourceFiles)
      {
        [[PALogger sharedLoggerForClient:PALogClientAlertTray] debug:@"*******Copying file [%@]", currentFile];
        if ([fileManager fileExistsAtPath:[sourcePath stringByAppendingPathComponent:currentFile] isDirectory:&isDirectory] && !isDirectory)
        {
          bool success = [fileManager copyItemAtPath:[sourcePath stringByAppendingPathComponent:currentFile] toPath:[_tempFolderPath stringByAppendingPathComponent:currentFile] error:&copyError];
          [[PALogger sharedLoggerForClient:PALogClientAlertTray] debug:@"*******Copying file [%@] [%s]", currentFile, success?"success":"failure"];
        }
      }
      newFilePath = [_tempFolderPath stringByAppendingPathComponent:[filePath lastPathComponent]];
      [[PALogger sharedLoggerForClient:PALogClientAlertTray] debug:@"*******New file path [%@]", newFilePath];
      NSError *error;
      NSString *resourceImagesPath = [[NSBundle mainBundle] resourcePath];
      [[PALogger sharedLoggerForClient:PALogClientAlertTray] debug:@"*******Image files path [%@]", resourceImagesPath];
      resourceImagesPath = [resourceImagesPath stringByAppendingString:@"/images/"];
      [[PALogger sharedLoggerForClient:PALogClientAlertTray] debug:@"*******Image files path [%@]", resourceImagesPath];
      NSString *alertContent = [[NSString alloc] initWithContentsOfFile:newFilePath encoding:NSUTF8StringEncoding error:&error];
      [[PALogger sharedLoggerForClient:PALogClientAlertTray] debug:@"*******Alert Content before  [[[%@]]]", alertContent];
      alertContent = [alertContent stringByReplacingOccurrencesOfString:@"##CLIENT_PROV_ROOT##images/" withString:resourceImagesPath];
      alertContent = [alertContent stringByReplacingOccurrencesOfString:@"<head>" withString:@"<head><meta http-equiv='Content-Type' content='text/html;charset=utf-8'>"];
      [[PALogger sharedLoggerForClient:PALogClientAlertTray] debug:@"*******Alert Content after  [[[%@]]]", alertContent];
      [alertContent writeToFile:newFilePath atomically:YES encoding:NSUTF8StringEncoding error:&error];
    }
  }
  
  NSURL *url = [NSURL fileURLWithPath:newFilePath];
  NSURLRequest *request = [NSURLRequest requestWithURL:url];
  [mainvc loadWebPage:request];
  _mainWindowController.window.delegate = self;
  [self logAlertAction:_currentAlertInfo action:@"Delivered"];
  
  [PAEventRunListWriter markAlertAsRead:_currentAlertInfo];
}

- (NSWindowController *)mainWindowController {
  NSStoryboard *storyboard = [NSStoryboard mainAppStoryboard];
  NSWindowController *mainWindowVc = [storyboard instantiateControllerWithIdentifier:kPAMainWindowControllerId];
  return mainWindowVc;
}

- (void)addAlertDismissTimer {
  if (_currentAlertInfo.alertTimeInterval > 0) {
    _alertDismissTimer =  [NSTimer scheduledTimerWithTimeInterval:_currentAlertInfo.alertTimeInterval target:self selector:@selector(alertDismissTimerFired:) userInfo:nil repeats:NO];
  }
}

- (void)removeAlertDismissTimer {
  if (_alertDismissTimer.isValid) {
    [_alertDismissTimer invalidate];
  }
}

- (NSString*)getHexTimeStamp {
  
  NSDate *now = [NSDate date];
  
  unsigned long long offsetEpoch = 0x19db1ded53e8000L;
  
  NSTimeInterval secondsBetween = [now timeIntervalSince1970];
  
  unsigned long long windowsTime = (unsigned long long) secondsBetween;
  windowsTime = (windowsTime * 10000000L) + offsetEpoch;
  
  unsigned long lowTime = (unsigned long)(windowsTime & 0x00000000FFFFFFFF);
  unsigned long highTime = (unsigned long)((windowsTime & 0xFFFFFFFF00000000) >> 32);
  long offset = -([[NSTimeZone localTimeZone] secondsFromGMT]) / 60;
  if(offset == 0) {
    offset = -1;
  }
  NSString *hexTime = [NSString stringWithFormat:@"%08lx%08lx%03x", highTime,lowTime, offset];
  return hexTime;
}

- (NSString*)formatString:(PAAlertInfo *)info action:(NSString *) eventAction {
  NSDate* sourceDate = [NSDate date];
  
  NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
  NSTimeZone* destinationTimeZone = [NSTimeZone systemTimeZone];
  
  NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:sourceDate];
  NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:sourceDate];
  NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
  
  NSDate* destinationDate = [[NSDate alloc] initWithTimeInterval:interval sinceDate:sourceDate];
  NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
  NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"GMT"];
  [formatter setTimeZone:timeZone];
  [formatter setDateFormat:@"yyyy-MM-dd hh:mm:ss a"];
  NSString *stringFromDate = [formatter stringFromDate:destinationDate];
  
  NSString* instanceID = @"0";
  NSInteger eventType = 4;
  NSInteger gmt = (NSInteger) (interval/60) * -1;
  NSString* hexTime = [self getHexTimeStamp]; //@"01d34f367ff2cc70fffffeb6"
  NSString *uuid = [[NSUUID UUID] UUIDString];
  unsigned int randNum = arc4random_uniform(UINT_MAX);
  
  NSString* result = [NSString stringWithFormat:@"%u|%ld|%@|{%@}|%@|%@|%@|%@|%ld|%@|%@|\r\n", randNum, eventType, hexTime, uuid, eventAction, info.contentTitle, info.contentGuid, info.contentVersion, gmt, stringFromDate, instanceID];
  return result;
}

- (void)logAlertAction:(PAAlertInfo *)info action:(NSString*) eventAction {
  
  if(info.alertType != PAAlertTypeRealTime) {
    NSString *logData = [self formatString:info action:eventAction];
    [[PALogger sharedLoggerForClient:PALogClientEvent] writeRawText:logData];
  }
}

- (void)alertDismissTimerFired:(NSTimer *)timer {
  // close alert window
  _isUserDismissed = NO;
  [_mainWindowController.window close];
}

- (void)acknowledgeAlert {
  NSFileManager *fileManager = [NSFileManager defaultManager];
  NSError *error;
  if(_tempFolderPath != nil) {
    [fileManager removeItemAtPath:_tempFolderPath error:&error];
  }
  _tempFolderPath = nil;
  _mainWindowController.window.delegate = self;
  if(_isUserDismissed) {
    [self logAlertAction:_currentAlertInfo action:@"Recognized"];
  } else {
    [self logAlertAction:_currentAlertInfo action:@"Ignored"];
  }
  if (_currentAlertInfo.alertType != PAAlertTypeRealTime ||
      !_currentAlertInfo.isAcknowledgeRequired) {
    [self showNextAlert];
    return;
  }
  
  __block PANetworkServiceManager *serviceManager = [[PANetworkServiceManager alloc] init];
  PARealTimeAlertRequest *request = [[PARealTimeAlertRequest alloc]initWithRequestType:PARealTimeAlertRequestTypeMessageAcknowledgement guid:_currentAlertInfo.requestGuid clientName:[NSHost computerName] isIntended:nil];
  [_networkServiceManagers addObject:serviceManager];
  [serviceManager realTimeAlertAcknowledgement:request callback:^(NSError *error, PABaseResponse *response) {
    if (response.statusCode == kPAResponseSuccess) {
      [[PALogger sharedLoggerForClient:PALogClientAlertTray] debug:@"Real Time Alert acknowledgement request success"];
    } else {
      [[PALogger sharedLoggerForClient:PALogClientAlertTray] debug:@"Real Time Alert acknowledgement request failed; Error: %@",error];
    }
    [_networkServiceManagers removeObject:serviceManager];
    dispatch_async(dispatch_get_main_queue(), ^{
      [self showNextAlert];
    });
  }];
}

- (BOOL)isAlertInQueue:(PAAlertInfo *)info {
  if (_alertQueue.count) {
    NSArray *filtered = [_alertQueue filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(contentGuid == %@ && contentVersion == %@)", info.contentGuid, info.contentVersion]];
    if (filtered.count) {
      return true;
    }
  }
  
  return false;
}

#pragma mark NSWindowDelegate methods

- (void)windowWillClose:(NSNotification *)notification {
  dispatch_async(dispatch_get_main_queue(), ^{
    [self acknowledgeAlert];
  });
}

@end
