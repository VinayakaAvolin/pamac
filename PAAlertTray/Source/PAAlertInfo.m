//
//  PAAlertInfo.m
//  PAAlertTray
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <PACoreServices/PACoreServices.h>
#import "PAAlertInfo.h"
#import "NSDate+Additions.h"
#import "NSString+Additions.h"

NSString *const kPARTXmlExtension = @"xml";
NSString *const kPARTHtmlFileName = @"Alert Content.htm";

@implementation PAAlertInfo

- (instancetype)initWithContentGuid:(NSString *)guid
                            version:(NSString *)version
                          alertType:(PAAlertType)type {
  return [self initWithContentGuid:guid version:version requestGuid:nil windowWidth:0 windowHeight:0 acknowledgeRequired:false alertTimeInterval:0 expiryDate:nil alertType:type contentTitle:nil];
}

- (instancetype)initWithContentGuid:(NSString *)guid
                            version:(NSString *)version
                        requestGuid:(NSString *)requestGuid
                        windowWidth:(NSUInteger)width
                       windowHeight:(NSUInteger)height
                acknowledgeRequired:(BOOL)ackRequired
                  alertTimeInterval:(NSTimeInterval)timeInterval
                         expiryDate:(NSString *)expiryDateStr
                          alertType:(PAAlertType)type
                       contentTitle:(NSString *)title {
  self = [super init];
  if (self) {
    _contentGuid = guid;
    _contentVersion = version;
    _requestGuid =requestGuid;
    _windowWidth = width;
    _windowHeight = height;
    _isAcknowledgeRequired = ackRequired;
    _alertTimeInterval = timeInterval;
    _alertType = type;
    _contentTitle = title;
    _expiryTimeStr = expiryDateStr;
    if (![_expiryTimeStr isEmptyOrHasOnlyWhiteSpaces]) {
      _expiryDate = [NSDate dateFromString:_expiryTimeStr format:Format9];
    }
  }
  return self;
}
- (NSString *)contentFolderPath {
  PAFileSystemManager *fsManager = [[PAFileSystemManager alloc]init];
  NSString *alertsFolderPath = nil;
  switch (_alertType) {
    case PAAlertTypeRealTime:
      alertsFolderPath = fsManager.realTimeAlertFolderPath;
      break;
    case PAAlertTypeNormal:
      alertsFolderPath = fsManager.alertsFolderPath;
      break;
      
    default:
      break;
  }
  if (!alertsFolderPath) {
    return nil;
  }
  if (_contentGuid && _contentVersion) {
    NSString *contentFolderName = [NSString stringWithFormat:@"%@.%@",_contentGuid, _contentVersion];
    NSString *alertContentFolderPath = [alertsFolderPath stringByAppendingPathComponent:contentFolderName];
    return alertContentFolderPath;
  }
  return nil;
}

- (NSString *)contentXmlPath {
  if (!_contentGuid ||
      !_contentVersion) {
    return nil;
  }
  NSString *contentFolderName = [NSString stringWithFormat:@"%@.%@",_contentGuid, _contentVersion];
  NSString *alertContentFolderPath = [self contentFolderPath];
  NSString *contentXmlPath = [alertContentFolderPath stringByAppendingPathComponent:contentFolderName];
  contentXmlPath = [contentXmlPath stringByAppendingPathExtension:kPARTXmlExtension];
  return contentXmlPath;
}

- (NSString *)alertContentHtmlPath {
  NSString *htmlPath = [self contentFolderPath];
  htmlPath= [htmlPath stringByAppendingPathComponent:kPARTHtmlFileName];
  return htmlPath;
}

+ (PAAlertType)alertTypeEnumFor:(NSString *)alertType {
  PAAlertType type = PAAlertTypeNone;
  if (![alertType isEmptyOrHasOnlyWhiteSpaces]) {
    NSArray *typeArray = [[NSArray alloc] initWithObjects:kPAAlertTypeArray];
    NSUInteger index = [typeArray indexOfObject:alertType];
    if (NSNotFound != index) {
      type = (PAAlertType)index;
    }
  }
  return type;
}
@end
