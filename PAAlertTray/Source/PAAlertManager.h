//
//  PAAlertManager.h
//  PAAlertTray
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PAAlertInfo.h"

@interface PAAlertManager : NSObject

- (void)showRealTimeAlert:(PAAlertInfo *)alertInfo;

- (BOOL)canTerminate;

@end
