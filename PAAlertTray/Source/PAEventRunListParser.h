//
//  PAEventRunListParser.h
//  PAAlertTray
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <PACoreServices/PACoreServices.h>
#import "PAAlertInfo.h"
#import "PAAlertTrayConstants.h"

typedef void(^PAEventRunListParserCompletionBlock)(BOOL success, NSArray *alertInfoList);

@interface PAEventRunListParser : PAXmlParser

- (instancetype)initWithAlertType:(PAAlertType)alertType;

- (void)parseEventRunListWithCompletion:(PAEventRunListParserCompletionBlock)completionBlock;

@end
