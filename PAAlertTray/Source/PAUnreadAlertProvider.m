//
//  PAUnreadAlertProvider.m
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PAUnreadAlertProvider.h"
#import "PAEventRunListParser.h"

@interface PAUnreadAlertProvider () {
  NSString *_alertFolderPath;
  PAFileSystemManager *_fsManager;
  PAAlertType _alertType;
  NSArray *_readAlertList;
  PAUnreadAlertProviderCompletionBlock _completionBlock;
}

@end

@implementation PAUnreadAlertProvider

- (instancetype)initWithAlertType:(PAAlertType)alertType {
  self = [super init];
  if (self) {
    _alertType = alertType;
    _fsManager = [[PAFileSystemManager alloc]init];
    [self setupAlertsFolderPath];
    if (!_alertFolderPath ||
        ![_fsManager doesDirectoryExist:_alertFolderPath]) {
      self = nil;
    }
  }
  return self;
}

- (void)getUnreadAlertsWithCompletion:(PAUnreadAlertProviderCompletionBlock)completionBlock {
  _completionBlock = completionBlock;
  [self getReadAlertList];
}

- (void)getReadAlertList {
  PAEventRunListParser *parser = [[PAEventRunListParser alloc] initWithAlertType:_alertType];
  if (!parser) {
    [self getUnreadAlerts];
    return;
  }
  [parser parseEventRunListWithCompletion:^(BOOL success, NSArray *alertInfoList) {
    if (alertInfoList.count) {
      _readAlertList = [alertInfoList copy];
    }
    [self getUnreadAlerts];
  }];
}

- (void)getUnreadAlerts {
  NSMutableArray *alertsList = [[NSMutableArray alloc] init];
  NSArray *contentsAlertDir = [_fsManager contentsOfDirectory:_alertFolderPath];
  for (NSString *itemName in contentsAlertDir) {
    @autoreleasepool {
      NSString *contentGuid = [itemName stringByDeletingPathExtension];
      NSString *contentVersion = itemName.pathExtension;
      PAAlertInfo *alertInfo = [[PAAlertInfo alloc] initWithContentGuid:contentGuid version:contentVersion alertType:_alertType];
      /// check if alert exists in last run event list; if so do not consider that alert
      if (alertInfo && ![self doesAlert:alertInfo existInList:_readAlertList]) {
        [alertsList addObject:alertInfo];
      }
    }
  }
  _completionBlock((alertsList.count > 0), alertsList);
}

- (BOOL)doesAlert:(PAAlertInfo *)alertInfo existInList:(NSArray *)inputList {
  if (inputList.count) {
    NSArray *filtered = [inputList filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(contentGuid == %@ && contentVersion == %@)", alertInfo.contentGuid, alertInfo.contentVersion]];
    if (filtered.count) {
      return true;
    }
  }
  
  return false;
}

- (void)setupAlertsFolderPath {
  PAFileSystemManager *fsManager = [[PAFileSystemManager alloc]init];
  switch (_alertType) {
    case PAAlertTypeRealTime:
      _alertFolderPath = fsManager.realTimeAlertFolderPath;
      break;
    case PAAlertTypeNormal:
      _alertFolderPath = fsManager.alertsFolderPath;
      break;
      
    default:
      break;
  }
}


@end
