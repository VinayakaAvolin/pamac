//
//  PAAlertTrayConstants.h
//  PAAlertTray
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifndef PAAlertTrayConstants_h
#define PAAlertTrayConstants_h

typedef enum {
    PAAlertTypeNone,
    PAAlertTypeRealTime,
    PAAlertTypeNormal
} PAAlertType;
#define kPAAlertTypeArray @"unknown", @"-realTimeAlert", @"-alert", nil

// notification name constants
extern NSString *const kPANotificationNameTerminateApp;

extern NSString *const kPAATEventXmlRoot;
extern NSString *const kPAATEventInfoNodeName;
extern NSString *const kPAATEventGuidAttribute;
extern NSString *const kPAATEventVersionAttribute;
extern NSString *const kPAATEventGuidKey;
extern NSString *const kPAATEventVersionKey;

#endif /* PAAlertTrayConstants_h */
