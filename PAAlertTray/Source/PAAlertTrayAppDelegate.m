//
//  PAAlertTrayAppDelegate.m
//  PAAlertTray
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <PACoreServices/PACoreServices.h>
#import "PAAlertTrayAppDelegate.h"
#import "PAAlertManager.h"
#import "PAAlertInfo.h"
#import "PAAlertTrayConstants.h"
#import "PAInputConfigurationXmlParser.h"
#import "PAUnreadAlertProvider.h"

@interface PAAlertTrayAppDelegate () <NSUserNotificationCenterDelegate> {
  PAAlertManager *_alertManager;
}

@end

@implementation PAAlertTrayAppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
  
  // Insert code here to initialize your application
  [self addNotificationObservers];
  [self setup];
  //    NSMutableArray *args = [@[@"test", @"-realTimeAlert"] mutableCopy];
  //    NSMutableArray *args = [@[@"test", @"-alert"] mutableCopy];
  NSMutableArray *args = [[[NSProcessInfo processInfo] arguments] mutableCopy];
  if (args.count > 1) {
    [args removeObjectAtIndex:0];
  }
  [[PALogger sharedLoggerForClient:PALogClientAlertTray] debug:@"Alert tray app is launched with args %@",[[NSProcessInfo processInfo] arguments]];
  [self parseArguments:args];
}


- (void)applicationWillTerminate:(NSNotification *)aNotification {
  // Insert code here to tear down your application
  [self removeNotificationObservers];
}

#pragma mark - Private methods

- (void)setup {
  _alertManager = [[PAAlertManager alloc] init];
}

- (void)terminateApp {
  if ([_alertManager canTerminate]) {
    [NSApp performSelector:@selector(terminate:) withObject:nil afterDelay:0.0];
  }
}

- (void)parseArguments:(NSArray *)arguments {
  if (arguments.count < 1) {
    [self terminateApp];
    return;
  }
  // minimum 1 argument is must
  if (arguments.count >= 1) {
    NSString *alertType = arguments[0];
    NSArray *alertTypeArray = [[NSArray alloc] initWithObjects:kPAAlertTypeArray];
    
    if (!alertType ||
        ![alertTypeArray containsObject:alertType]) {
      [self terminateApp];
      return;
    }
    PAAlertType alertTypeEnum = [PAAlertInfo alertTypeEnumFor:alertType];
    NSString *contentGuid = nil;
    NSString *contentVersion = nil;
    /// check for content guid and version
    if (arguments.count >= 3) {
      contentGuid = arguments[1];
      contentVersion = arguments[2];;
      if (!contentGuid ||
          !contentVersion) {
        [self terminateApp];
        return;
      }
    }
    
    if (contentGuid && contentVersion) {
      PAAlertInfo *alertInfo = [[PAAlertInfo alloc] initWithContentGuid:contentGuid version:contentVersion alertType:alertTypeEnum];
      [self handleSingleAlert:alertInfo];
    } else {
      [self checkForNewAlertsOfType:alertTypeEnum];
    }
  }
}

- (void)checkForNewAlertsOfType:(PAAlertType)alertType {
  PAUnreadAlertProvider *unreadAlertProvider = [[PAUnreadAlertProvider alloc]initWithAlertType:alertType];
  [unreadAlertProvider getUnreadAlertsWithCompletion:^(BOOL success, NSArray *alertInfoList) {
    if (alertInfoList.count) {
      for (PAAlertInfo *alertInfo in alertInfoList) {
        [self handleSingleAlert:alertInfo];
      }
    }else {
      [self terminateApp];
      return;
    }
  }];
}
- (void)handleSingleAlert: (PAAlertInfo *)alertsInfo {
  PAInputConfigurationXmlParser *xmlParser = [[PAInputConfigurationXmlParser alloc]initWithAlertInfo:alertsInfo];
  [xmlParser parseInputConfigurationWithCompletion:^(BOOL success, PAAlertInfo *alertInfo) {
    [_alertManager showRealTimeAlert:alertInfo];
    [[PALogger sharedLoggerForClient:PALogClientAlertTray] debug:@"Real time alert info %d %d %d",alertInfo.windowWidth, alertInfo.windowHeight, alertInfo.alertTimeInterval];
  }];
  
}

#pragma mark -Notification related methods

- (void)addNotificationObservers {
  [[NSDistributedNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveNotification:) name:kPANotificationNameNewRealTimeAlert object:kPARealTimeAlertObjectName];
  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveNotification:) name:kPANotificationNameTerminateApp object:nil];
}
- (void)removeNotificationObservers {
  [[NSDistributedNotificationCenter defaultCenter] removeObserver:self];
  [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveNotification:(NSNotification *)notification {
  if ([notification.name isEqualToString:kPANotificationNameNewRealTimeAlert]) {
    NSArray *arguments = notification.userInfo[kPARealTimeAlertArgumentsKey];
    if ([arguments isKindOfClass:[NSArray class]]) {
      [[PALogger sharedLoggerForClient:PALogClientAlertTray] debug:@"Received kPANotificationNameNewRealTimeAlert notification with arguments %@",arguments];
      [self parseArguments:arguments];
    }
  } else if ([notification.name isEqualToString:kPANotificationNameTerminateApp]) {
    [self terminateApp];
  }
}


@end
