//
//  PAUnreadAlertProvider.h
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <PACoreServices/PACoreServices.h>
#import "PAAlertInfo.h"
#import "PAAlertTrayConstants.h"

typedef void(^PAUnreadAlertProviderCompletionBlock)(BOOL success, NSArray *alertInfoList);

@interface PAUnreadAlertProvider : NSObject

- (instancetype)initWithAlertType:(PAAlertType)alertType;

- (void)getUnreadAlertsWithCompletion:(PAUnreadAlertProviderCompletionBlock)completionBlock;

@end
