//
//  PAEventRunListWriter.h
//  PAAlertTray
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PAAlertInfo.h"

@interface PAEventRunListWriter : NSObject

+ (BOOL)markAlertAsRead:(PAAlertInfo *)alertInfo;

@end
