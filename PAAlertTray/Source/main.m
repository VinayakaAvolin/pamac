//
//  main.m
//  PAAlertTray
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
  return NSApplicationMain(argc, argv);
}
