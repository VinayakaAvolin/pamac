//
//  PAAlertTrayConstants.m
//  PAAlertTray
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PAAlertTrayConstants.h"

NSString *const kPANotificationNameTerminateApp = @"PANotificationNameTerminateApp";

NSString *const kPAATEventXmlRoot = @"sprt-events";
NSString *const kPAATEventInfoNodeName = @"sprt-event-info";
NSString *const kPAATEventGuidAttribute = @"event_guid";
NSString *const kPAATEventVersionAttribute = @"event_version";
NSString *const kPAATEventGuidKey = @"_event_guid";
NSString *const kPAATEventVersionKey = @"_event_version";
