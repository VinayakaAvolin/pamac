//
//  PAEventRunListWriter.m
//  PAAlertTray
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <PACoreServices/PACoreServices.h>
#import "PAEventRunListWriter.h"
#import "PAAlertTrayConstants.h"

@interface PAEventRunListWriter () {
  NSString *_eventListXmlPath;
  PAXmlWriter *_xmlWriter;
  PAFileSystemManager *_fsManager;
  PAAlertInfo *_alertInfo;
}

@end

@implementation PAEventRunListWriter

- (instancetype)initWithAlertInfo:(PAAlertInfo *)alertInfo
{
  self = [super init];
  if (self) {
    if (!alertInfo.contentGuid || !alertInfo.contentVersion) {
      self = nil;
    } else {
      _alertInfo = alertInfo;
      _fsManager = [[PAFileSystemManager alloc]init];
      [self setupEventListXmlPath];
      if (!_eventListXmlPath) {
        self = nil;
      } else {
        _xmlWriter = [[PAXmlWriter alloc]initWithXmlAtPath:_eventListXmlPath];
      }
    }
  }
  return self;
}

- (void)setupEventListXmlPath {
  switch (_alertInfo.alertType) {
    case PAAlertTypeRealTime:
      _eventListXmlPath = [_fsManager realTimeAlertRunListFilePath];
      break;
    case PAAlertTypeNormal:
      _eventListXmlPath = [_fsManager alertRunListFilePath];
      break;
    default:
      break;
  }
}

+ (BOOL)markAlertAsRead:(PAAlertInfo *)alertInfo {
  @autoreleasepool {
    PAEventRunListWriter *runListWriter = [[PAEventRunListWriter alloc] initWithAlertInfo:alertInfo];
    return [runListWriter saveAlertInfo];
  }
}

- (BOOL)saveAlertInfo {
  BOOL result = [self createScheduledOptimizerXml];
  
  if (!result) {
    return false;
  }
  PAXmlAttribute *guidAttribute =  [[PAXmlAttribute alloc] initWithName:kPAATEventGuidAttribute value:_alertInfo.contentGuid];
  PAXmlAttribute *versionAttribute =  [[PAXmlAttribute alloc] initWithName:kPAATEventVersionAttribute value:_alertInfo.contentVersion];
  result = [_xmlWriter addXmlElementWithName:kPAATEventInfoNodeName value:nil attributes:@[guidAttribute, versionAttribute] parentXPath:kPAATEventXmlRoot];
  return result;
}

- (BOOL)createScheduledOptimizerXml {
  if ([_fsManager doesFileExist:_eventListXmlPath]) {
    return true;
  }
  BOOL xmlCreated = false;
  
  PAXmlFileProperties *properties = [[PAXmlFileProperties alloc]initWithVersion:kPAXmlVersionValue encoding:nil standalone:nil root:kPAATEventXmlRoot];
  
  PAXmlServiceManager *manager = [[PAXmlServiceManager alloc]init];
  xmlCreated = [manager createXmlAtPath:_eventListXmlPath withProperties:properties];
  if (xmlCreated) {
    _xmlWriter = [[PAXmlWriter alloc]initWithXmlAtPath:_eventListXmlPath];
  }
  return xmlCreated;
}

@end
