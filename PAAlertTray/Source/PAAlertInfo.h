//
//  PAAlertInfo.h
//  PAAlertTray
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PAAlertTrayConstants.h"

@interface PAAlertInfo : NSObject

@property(nonatomic, readonly) NSString *alertContentHtmlPath;
@property(nonatomic, readonly) NSString *requestGuid;
@property(nonatomic, readonly) NSUInteger windowWidth;
@property(nonatomic, readonly) NSUInteger windowHeight;
@property(nonatomic, readonly) BOOL isAcknowledgeRequired;
@property(nonatomic, readonly) NSTimeInterval alertTimeInterval;
@property(nonatomic, readonly) NSString *contentGuid;
@property(nonatomic, readonly) NSString *contentVersion;
@property(nonatomic, readonly) PAAlertType alertType;
@property (nonatomic, readonly) NSString *expiryTimeStr; // alert expiry date & time
@property (nonatomic, readonly) NSDate *expiryDate; // alert expiry date & time

@property (nonatomic, readonly) NSString *contentFolderPath;
@property (nonatomic, readonly) NSString *contentXmlPath;
@property (nonatomic, readonly) NSString *contentTitle;

- (instancetype)initWithContentGuid:(NSString *)guid
                            version:(NSString *)version
                          alertType:(PAAlertType)type;

- (instancetype)initWithContentGuid:(NSString *)guid
                            version:(NSString *)version
                        requestGuid:(NSString *)requestGuid
                        windowWidth:(NSUInteger)width
                       windowHeight:(NSUInteger)height
                acknowledgeRequired:(BOOL)ackRequired
                  alertTimeInterval:(NSTimeInterval)timeInterval
                         expiryDate:(NSString *)expiryDateStr
                          alertType:(PAAlertType)type
                       contentTitle:(NSString *)title;


+ (PAAlertType)alertTypeEnumFor:(NSString *)alertType;

@end
