//
//  PAEventRunListParser.m
//  PAAlertTray
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PAEventRunListParser.h"

@interface PAEventRunListParser () {
  NSString *_eventListXmlPath;
  PAFileSystemManager *_fsManager;
  PAAlertType _alertType;
}

@end

@implementation PAEventRunListParser

- (instancetype)initWithAlertType:(PAAlertType)alertType {
  self = [super init];
  if (self) {
    _alertType = alertType;
    _fsManager = [[PAFileSystemManager alloc]init];
    [self setupEventListXmlPath];
    if (!_eventListXmlPath ||
        ![_fsManager doesFileExist:_eventListXmlPath]) {
      self = nil;
    }
  }
  return self;
}

- (void)setupEventListXmlPath {
  switch (_alertType) {
    case PAAlertTypeRealTime:
      _eventListXmlPath = [_fsManager realTimeAlertRunListFilePath];
      break;
    case PAAlertTypeNormal:
      _eventListXmlPath = [_fsManager alertRunListFilePath];
      break;
    default:
      break;
  }
}
/*
 Parse dictionary of below form:
 {
 "__name" = "sprt-events";
 "sprt-event-info" =     (
 {
 "_event_guid" = "6d3ca88c-adc1-42ff-a0cc-9020979614e6";
 "_event_version" = 1;
 },
 {
 "_event_guid" = "6d3ca88c-adc1-42ff-a0cc-9020979614e6";
 "_event_version" = 1;
 }
 );
 }*/
- (void)parseEventRunListWithCompletion:(PAEventRunListParserCompletionBlock)completionBlock {
  NSMutableArray *alertInfoList = [[NSMutableArray alloc] init];
  NSDictionary *xmlDict = [self dictionaryFromXmlAtPath:_eventListXmlPath];
  if (xmlDict) {
    id eventInfoValue = xmlDict[kPAATEventInfoNodeName];
    NSArray *eventInfoList = nil;
    if ([eventInfoValue isKindOfClass:[NSArray class]]) {
      eventInfoList = (NSArray *)eventInfoValue;
    } else if ([eventInfoValue isKindOfClass:[NSDictionary class]]) {
      eventInfoList = @[eventInfoValue];
    }
    
    for (NSDictionary *eventInfoDict in eventInfoList) {
      NSString *contentGuid = eventInfoDict[kPAATEventGuidKey];
      NSString *contentVersion = eventInfoDict[kPAATEventVersionKey];
      PAAlertInfo *alertInfo = [[PAAlertInfo alloc] initWithContentGuid:contentGuid version:contentVersion alertType:_alertType];
      /// check if alert exists in last run event list; if so do not consider that alert
      if (alertInfo) {
        [alertInfoList addObject:alertInfo];
      }
    }
  }
  
  completionBlock((alertInfoList.count > 0), [NSArray arrayWithArray:alertInfoList]);
}
@end
