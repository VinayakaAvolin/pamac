//
//  PASupportService.h
//  PASupportService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Cocoa/Cocoa.h>

//! Project version number for PASupportService.
FOUNDATION_EXPORT double PASupportServiceVersionNumber;

//! Project version string for PASupportService.
FOUNDATION_EXPORT const unsigned char PASupportServiceVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <PASupportService/PublicHeader.h>

#import <PASupportService/PAServiceInterface.h>
#import <PASupportService/PAServiceConstants.h>
#import <PASupportService/PAServiceInfoProtocol.h>
#import <PASupportService/PAServiceInfo.h>
