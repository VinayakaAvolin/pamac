//
//  PAServiceInterface.h
//  PASupportService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "PAServiceConstants.h"
#import "PAServiceInfoProtocol.h"


@interface PAServiceInterface : NSObject

+ (void)executeService:(PASupportService)service category:(PASupportServiceCategory)category info:(id<PAServiceInfoProtocol>)info callback:(PAServiceCallback)callback;

@end
