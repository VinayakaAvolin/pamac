//
//  PAServiceProvider.m
//  PASupportService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PAServiceProvider.h"

@implementation PAServiceProvider

- (BOOL)canExecuteService:(PASupportService)service category:(PASupportServiceCategory)category {
    return false;
}

- (void)executeService:(PASupportService)service category:(PASupportServiceCategory)category info:(id<PAServiceInfoProtocol>)info callback:(PAServiceCallback)callback {
    
}
@end
