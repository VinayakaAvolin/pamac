//
//  PAServiceInfo.h
//  PASupportService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PAServiceInfoProtocol.h"
#import "PAServiceConstants.h"

@interface PAServiceInfo : NSObject <PAServiceInfoProtocol, NSSecureCoding, NSCopying>

@property (nonatomic, assign) PASupportServiceCategory serviceCategory;
@property (nonatomic, assign) PASupportService service;
@property (nonatomic, assign) PAServiceMode serviceMode;

///for protection and download we will be receiving guid and version from input
@property (nonatomic) NSString *guid;
@property (nonatomic) NSString *version;

// for support solution service; input is either the script path or the script itself
@property (nonatomic) NSString *scriptFilePath;
@property (nonatomic) NSString *scriptString;

// for download offer service; additional inputs are  download url and type of installer (DMG/PKG)
@property (nonatomic) NSString *downloadUrl;
@property (nonatomic) NSString *downloadedItemPath; // path to the downloaded file
@property (nonatomic, readonly) PASupportServiceInstallFileType installerType;  // installer type enum
@property (nonatomic) NSString *installerKind; // installer type string
@property (nonatomic) BOOL doesRequireRestart;
@property (nonatomic) BOOL isSilentInstall;
@property (nonatomic) NSString *installVerificationPath; // after installation need to check if app is installed at this path

// for repair service; additional inputs are  files and app to repair
@property (nonatomic) NSString *applicationName;
@property (nonatomic) NSArray *filesToProtect;

// for restore protection, additional inputs are user selected protected item name (time stamp)
@property (nonatomic, assign) NSString *protectedItemName;

// for Messages service; additional inputs are support action, tell me link and message type (caption)
@property (nonatomic) NSString *messageTypeStr;
@property (nonatomic) BOOL hasSupportAction;
@property (nonatomic) NSString *tellMeLink;
@property (nonatomic, readonly) PASupportServiceMessageType messageType;  // message type enum

// for support action
@property (nonatomic) NSString *executionTypeStr;
@property (nonatomic, readonly) PASupportActionExecutionType executionType;
@property (nonatomic) NSDictionary *inputParameters;

@end
