//
//  PAServiceInfo.m
//  PASupportService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PAServiceInfo.h"
#import "NSString+Additions.h"

NSString *const kSFScriptPath = @"_scriptPath";
NSString *const kSFScriptString = @"_scriptString";
NSString *const kSFServiceCategory = @"_serviceCategory";
NSString *const kSFService = @"_service";
NSString *const kSFInstallerKind = @"_installerKind";
NSString *const kSFDownloadUrl = @"_downloadUrl";
NSString *const kSFDoesRequireRestart = @"_doesRequireRestart";
NSString *const kSFDownloadedItemPath = @"_downloadedItemPath";
NSString *const kSFIsSilentInstall = @"_isSilentInstall";
NSString *const kSFInstallVerificationPath = @"_installVerificationPath";
NSString *const kSFApplication = @"_applicationName";
NSString *const kSFFilesToProtect = @"_filesToProtect";
NSString *const kSFProtectedItemName = @"_protectedItemName";
NSString *const kSFGuid = @"_guid";
NSString *const kSFVersion = @"_version";
NSString *const kSFMessageType = @"_messageTypeStr";
NSString *const kSFHasSupportAction = @"_hasSupportAction";
NSString *const kSFTellMeLink = @"_tellMeLink";
NSString *const kSFExecutionType = @"_executionTypeStr";
NSString *const kSFInputParams = @"_inputParameters";

@interface PAServiceInfo () {
  PASupportServiceInstallFileType _installerTypeEnum;
  PASupportServiceMessageType _messageTypeEnum;
  PASupportActionExecutionType _executionTypeEnum;
}


@end
@implementation PAServiceInfo

+ (BOOL)supportsSecureCoding {
  return YES;
}

- (id)copyWithZone:(NSZone *)zone {
  PAServiceInfo *newService = [[[self class] allocWithZone:zone] init];
  newService.serviceCategory = self.serviceCategory;
  newService.service = self.service;
  newService.serviceMode = self.serviceMode;
  newService.guid = self.guid;
  newService.version = self.version;
  newService.scriptFilePath = self.scriptFilePath;
  newService.scriptString = self.scriptString;
  newService.downloadUrl = self.downloadUrl;
  newService.installerKind = self.installerKind;
  newService.isSilentInstall = self.isSilentInstall;
  newService.doesRequireRestart = self.doesRequireRestart;
  newService.installVerificationPath = self.installVerificationPath;
  newService.applicationName = self.applicationName;
  newService.filesToProtect = self.filesToProtect;
  newService.protectedItemName = self.protectedItemName;
  newService.downloadedItemPath = self.downloadedItemPath;
  newService.messageTypeStr = self.messageTypeStr;
  newService.hasSupportAction = self.hasSupportAction;
  newService.tellMeLink = self.tellMeLink;
  newService.executionTypeStr = self.executionTypeStr;
  newService.inputParameters = self.inputParameters;
  return newService;
}

- (id)initWithCoder:(NSCoder *)coder {
  if ((self = [super init])) {
    // Decode the property values by key, specifying the expected class
    _service = [coder decodeIntegerForKey:kSFService];
    _serviceCategory = [coder decodeIntegerForKey:kSFServiceCategory];
    
    _guid = [coder decodeObjectOfClass:[NSString class] forKey:kSFGuid];
    _version = [coder decodeObjectOfClass:[NSString class] forKey:kSFVersion];
    
    _scriptFilePath = [coder decodeObjectOfClass:[NSString class] forKey:kSFScriptPath];
    _scriptString = [coder decodeObjectOfClass:[NSString class] forKey:kSFScriptString];
    
    _filesToProtect = [coder decodeObjectOfClass:[NSArray class] forKey:kSFFilesToProtect];
    _protectedItemName = [coder decodeObjectOfClass:[NSString class] forKey:kSFProtectedItemName];
    _applicationName = [coder decodeObjectOfClass:[NSString class] forKey:kSFApplication];
    
    _installerKind = [coder decodeObjectOfClass:[NSString class] forKey:kSFInstallerKind];
    _downloadUrl = [coder decodeObjectOfClass:[NSString class] forKey:kSFDownloadUrl];
    _installVerificationPath = [coder decodeObjectOfClass:[NSString class] forKey:kSFInstallVerificationPath];
    _isSilentInstall = [coder decodeBoolForKey:kSFIsSilentInstall];
    _doesRequireRestart = [coder decodeBoolForKey:kSFDoesRequireRestart];
    _downloadedItemPath = [coder decodeObjectOfClass:[NSString class] forKey:kSFDownloadedItemPath];
    
    _hasSupportAction = [coder decodeBoolForKey:kSFHasSupportAction];
    _messageTypeStr = [coder decodeObjectOfClass:[NSString class] forKey:kSFMessageType];
    _tellMeLink = [coder decodeObjectOfClass:[NSString class] forKey:kSFTellMeLink];
    
    _executionTypeStr = [coder decodeObjectOfClass:[NSString class] forKey:kSFExecutionType];
    _inputParameters = [coder decodeObjectOfClass:[NSDictionary class] forKey:kSFInputParams];
  }
  return self;
}

- (void)encodeWithCoder:(NSCoder *)coder {
  // Encode our ivars using string keys as normal
  [coder encodeInteger:_service forKey:kSFService];
  [coder encodeInteger:_serviceCategory forKey:kSFServiceCategory];
  [coder encodeObject:_guid forKey:kSFGuid];
  [coder encodeObject:_version forKey:kSFVersion];
  [coder encodeObject:_scriptFilePath forKey:kSFScriptPath];
  [coder encodeObject:_scriptString forKey:kSFScriptString];
  [coder encodeObject:_filesToProtect forKey:kSFFilesToProtect];
  [coder encodeObject:_applicationName forKey:kSFApplication];
  [coder encodeObject:_protectedItemName forKey:kSFProtectedItemName];
  [coder encodeObject:_downloadUrl forKey:kSFDownloadUrl];
  [coder encodeObject:_installerKind forKey:kSFInstallerKind];
  [coder encodeObject:_installVerificationPath forKey:kSFInstallVerificationPath];
  [coder encodeObject:_downloadedItemPath forKey:kSFDownloadedItemPath];
  [coder encodeBool:_isSilentInstall forKey:kSFIsSilentInstall];
  [coder encodeBool:_doesRequireRestart forKey:kSFDoesRequireRestart];
  [coder encodeObject:_downloadedItemPath forKey:kSFDownloadedItemPath];
  [coder encodeBool:_hasSupportAction forKey:kSFHasSupportAction];
  [coder encodeObject:_messageTypeStr forKey:kSFMessageType];
  [coder encodeObject:_tellMeLink forKey:kSFTellMeLink];
  [coder encodeObject:_executionTypeStr forKey:kSFExecutionType];
  [coder encodeObject:_inputParameters forKey:kSFInputParams];
}

- (PASupportServiceInstallFileType)installerType {
  if (_installerTypeEnum == PASupportServiceInstallFileTypeNone) {
    _installerTypeEnum = PASupportServiceInstallFileTypeNone;
    if (![_installerKind isEmptyOrHasOnlyWhiteSpaces]) {
      NSArray *installerTypeArray = [[NSArray alloc] initWithObjects:kPASupportServiceInstallFileTypeArray];
      NSUInteger index = [installerTypeArray indexOfObject:_installerKind];
      if (NSNotFound != index) {
        _installerTypeEnum = (PASupportServiceInstallFileType)index;
      }
    }
  }
  return _installerTypeEnum;
}
- (PASupportServiceMessageType)messageType {
  if (_messageTypeEnum == PASupportServiceMessageTypeNone) {
    _messageTypeEnum = PASupportServiceMessageTypeNone;
    if (![_messageTypeStr isEmptyOrHasOnlyWhiteSpaces]) {
      NSArray *msgTypeArray = [[NSArray alloc] initWithObjects:kPASupportServiceMessageTypeArray];
      NSUInteger index = [msgTypeArray indexOfObject:_messageTypeStr];
      if (NSNotFound != index) {
        _messageTypeEnum = (PASupportServiceMessageType)index;
      }
    }
  }
  return _messageTypeEnum;
}

- (PASupportActionExecutionType)executionType {
  if (_executionTypeEnum == PASupportActionExecutionTypeNone) {
    _executionTypeEnum = PASupportActionExecutionTypeNone;
    if (![_executionTypeStr isEmptyOrHasOnlyWhiteSpaces]) {
      NSArray *execTypeArray = [[NSArray alloc] initWithObjects:kPASupportActionExecutionTypeArray];
      NSUInteger index = [execTypeArray indexOfObject:_executionTypeStr];
      if (NSNotFound != index) {
        _executionTypeEnum = (PASupportActionExecutionType)index;
      }
    }
  }
  return _executionTypeEnum;
}


@end
