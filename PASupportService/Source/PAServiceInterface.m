//
//  PAServiceInterface.m
//  PASupportService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <PACoreServices/PACoreServices.h>

#import "PAServiceInterface.h"
#import "PAServiceProvider.h"
#import "NSBundle+Additions.h"
#import "NSString+Additions.h"

@interface PAServiceInterface () {
  NSMutableDictionary *_serviceProviders;
  PAAppConfiguration *_appconfigurations;
  NSArray *_supportedSprockets;
}

@end

@implementation PAServiceInterface

+ (void)executeService:(PASupportService)service category:(PASupportServiceCategory)category info:(id<PAServiceInfoProtocol>)info callback:(PAServiceCallback)callback {
  
  PAServiceInterface *sharedServiceInterface = [PAServiceInterface sharedServiceInterface];
  /// get the appropriate service provider and execute service there
  PAServiceProvider *serviceProvider = [sharedServiceInterface serviceProviderForService:service category:category];
  if (serviceProvider) {
    [serviceProvider executeService:service category:category info:info callback:callback];
  } else {
    callback(false, nil, nil);
  }
}

+ (id)sharedServiceInterface {
  static PAServiceInterface *sharedServiceInterface = nil;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    sharedServiceInterface = [[self alloc] init];
  });
  return sharedServiceInterface;
}

- (instancetype)init
{
  self = [super init];
  if (self) {
    _serviceProviders = [[NSMutableDictionary alloc]init];
    [self readConfigurations];
    [self populateSupportedSprockets];
    [self registerServiceProviders];
  }
  return self;
}

- (void)readConfigurations {
  _appconfigurations = [PAConfigurationManager appConfigurations];
}

- (void)registerServiceProviders {
  // Clear all service providers
  [_serviceProviders removeAllObjects];
  
  PAServiceProvider *serviceProvider;
  NSString *pluginPath = _appconfigurations.mainAppPath;
  pluginPath = [pluginPath stringByAppendingPathComponent:kPASupportServicePluginPath];
  
  /// Register repair service provider
  pluginPath = [pluginPath stringByAppendingPathComponent:kPASupportServicePluginNameRepair];
  
  if (pluginPath && [self canLoadSprocket:kPASupportServiceNameRepair]) {
    serviceProvider = [NSBundle loadBundleWithPath:pluginPath];
    if (serviceProvider) {
      [_serviceProviders setValue:serviceProvider forKey:kPASupportServiceNameRepair];
    }
  }
  
  /// Register Download offers service providers
  pluginPath = [pluginPath stringByDeletingLastPathComponent];
  pluginPath = [pluginPath stringByAppendingPathComponent:kPASupportServicePluginNameDownloadOffers];
  
  if (pluginPath && [self canLoadSprocket:kPASupportServiceNameDownloadOffers]) {
    serviceProvider = [NSBundle loadBundleWithPath:pluginPath];
    if (serviceProvider) {
      [_serviceProviders setValue:serviceProvider forKey:kPASupportServiceNameDownloadOffers];
    }
  }
  
  /// Register Messages service provider
  pluginPath = [pluginPath stringByDeletingLastPathComponent];
  pluginPath = [pluginPath stringByAppendingPathComponent:kPASupportServicePluginNameMessages];
  
  if (pluginPath && [self canLoadSprocket:kPASupportServiceNameMessages]) {
    serviceProvider = [NSBundle loadBundleWithPath:pluginPath];
    if (serviceProvider) {
      [_serviceProviders setValue:serviceProvider forKey:kPASupportServiceNameMessages];
    }
  }
  
  /// Register Support action service provider
  pluginPath = [pluginPath stringByDeletingLastPathComponent];
  pluginPath = [pluginPath stringByAppendingPathComponent:kPASupportServicePluginNameSupportAction];
  
  if (pluginPath && [self canLoadSprocket:kPASupportServiceNameSupportAction]) {
    serviceProvider = [NSBundle loadBundleWithPath:pluginPath];
    if (serviceProvider) {
      [_serviceProviders setValue:serviceProvider forKey:kPASupportServiceNameSupportAction];
    }
  }
  /// Register other service providers
  //pluginPath = [pluginPath stringByDeletingLastPathComponent];
}

/// Returns the service provider matching the input service name
- (PAServiceProvider *)serviceProviderFor:(NSString *)serviceName {
  return [_serviceProviders valueForKey:serviceName];
}

/// Returns the service provider matching the input service category and service
- (PAServiceProvider *)serviceProviderForService:(PASupportService)service category:(PASupportServiceCategory)category {
  PAServiceProvider *serviceProvider;
  switch (category) {
    case PASupportServiceCategoryRepair:
      serviceProvider = [self serviceProviderFor:kPASupportServiceNameRepair];
      break;
    case PASupportServiceCategoryDownloadOffers:
      serviceProvider = [self serviceProviderFor:kPASupportServiceNameDownloadOffers];
      break;
    case PASupportServiceCategorySupportAction:
      serviceProvider = [self serviceProviderFor:kPASupportServiceNameSupportAction];
      break;
      
    default:
      break;
  }
  return serviceProvider;
}

- (void)populateSupportedSprockets {
  if (_appconfigurations.supportedSprockets.count) {
    _supportedSprockets = [NSArray arrayWithArray:_appconfigurations.supportedSprockets];
  }
}

- (BOOL)canLoadSprocket:(NSString *)sprocketName {
  return (![sprocketName isEmptyOrHasOnlyWhiteSpaces] && _supportedSprockets.count && [_supportedSprockets containsObject:sprocketName]);
}

@end
