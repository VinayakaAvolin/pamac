//
//  PAServiceConstants.h
//  PASupportService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifndef PAServiceConstants_h
#define PAServiceConstants_h

typedef enum : NSUInteger {
  PASupportServiceCategoryNone,
  PASupportServiceCategoryRepair, // for protection, restore, undo
  PASupportServiceCategoryMessage,
  PASupportServiceCategoryAlert,
  PASupportServiceCategoryDownloadOffers,
  PASupportServiceCategorySupportAction // for self service support like optimization, auto fix
} PASupportServiceCategory;

typedef enum : NSUInteger {
  PASupportServiceNone,
  
  // for app protection
  PASupportServiceProtect, // for protection, restore, undo
  PASupportServiceGetProtectedHistory, // to get list of protected items to display in the UI
  PASupportServiceRestore,
  PASupportServiceUndoRestore,
  PASupportServiceGetRestoredHistory, // to get list of restored items to display in the UI
  
  // for download and install offers
  PASupportServiceDownload,
  PASupportServiceInstall,
  // for alert, messages and notifications
  PASupportServiceMessages,
  // for optimization adn auto fix
  PASupportServiceOptimizationScan,
  PASupportServiceOptimizationFix,
  PASupportServiceSupportActionScan,
  PASupportServiceSupportActionFix,
  PASupportServiceSupportActionMessageFix,
} PASupportService;

/// The file types for PASupportServiceCategoryDownloadOffers
typedef enum : NSUInteger {
  PASupportServiceInstallFileTypeNone,
  PASupportServiceInstallFileTypeDmg,
  PASupportServiceInstallFileTypePackage
} PASupportServiceInstallFileType;
#define kPASupportServiceInstallFileTypeArray @"none", @"DMG", @"PKG", nil

typedef enum : NSUInteger {
  PAServiceModeNormal,
  PAServiceModeElevated
} PAServiceMode;

typedef enum : NSUInteger {
  PASupportServiceMessageTypeNone,
  PASupportServiceMessageTypeDoItForMe,
  PASupportServiceMessageTypeTellMe,
  PASupportServiceMessageTypeFix,
  PASupportServiceMessageTypeShowMe,
} PASupportServiceMessageType;

typedef enum : NSUInteger {
  PADownloadStatusNone,
  PADownloadStatusStarted,
  PADownloadStatusInProgress,
  PADownloadStatusComplete,
  PADownloadStatusFailed,
} PADownloadStatus;
#define kPASupportServiceMessageTypeArray @"none", @"Do it For Me", @"Tell Me", @"Fix", @"Show Me", nil

typedef enum : NSUInteger {
  PASupportActionExecutionTypeNone,
  PASupportActionExecutionTypeAutofix,
  PASupportActionExecutionTypeScanAndFix,
} PASupportActionExecutionType;
#define kPASupportActionExecutionTypeArray @"none", @"Auto_Fix", @"Scan_And_Fix", nil

/// service plugin constants
extern NSString *const kPASupportServicePluginPath;
extern NSString *const kPASupportServicePluginNameRepair;
extern NSString *const kPASupportServicePluginNameDownloadOffers;
extern NSString *const kPASupportServicePluginNameMessages;
extern NSString *const kPASupportServicePluginNameSupportAction;

/// service name constants
extern NSString *const kPASupportServiceNameRepair;
extern NSString *const kPASupportServiceNameDownloadOffers;
extern NSString *const kPASupportServiceNameMessages;
extern NSString *const kPASupportServiceNameSupportAction;

// download offer service constants
extern NSString *const kPASupportServiceResponseKeyGuid;
extern NSString *const kPASupportServiceResponseKeyVersion;
extern NSString *const kPASupportServiceResponseKeyDownloadPath;
extern NSString *const kPASupportServiceResponseKeyProgressValue;
extern NSString *const kPASupportServiceResponseKeyStatus;
extern NSString *const kPASupportServiceResponseKeyInstallerType;

extern NSString *const kPASupportServiceResponseKeyExitCode;
extern NSString *const kPASupportServiceResponseKeyOptimizationXml;

extern NSString *const kPASupportServiceDownloadStatusNotificationName;
extern NSString *const kPASupportServiceDownloadOfferNotificationObjectName;

extern NSString *const kPASupportServiceOptimizationNotificationObjectName;
extern NSString *const kPASupportServiceDownloadCancelNotificationName;
extern NSString *const kPASupportServiceStartOptimizerNotificationName;
extern NSString *const kPASupportServiceStopOptimizerNotificationName;
extern NSString *const kPASupportServiceStartOptimizationNotificationName;
extern NSString *const kPASupportServiceStopOptimizationNotificationName;

typedef void(^PAServiceCallback)(BOOL success, NSError *error, id result);

#endif /* PAServiceConstants_h */
