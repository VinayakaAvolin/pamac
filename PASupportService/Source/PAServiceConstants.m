//
//  PAServiceConstants.m
//  PASupportService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PAServiceConstants.h"

NSString *const kPASupportServicePluginPath = @"Contents/Frameworks/PASupportService.framework/PlugIns";
NSString *const kPASupportServicePluginNameRepair = @"PARepairService.plugin";
NSString *const kPASupportServicePluginNameDownloadOffers = @"PADownloadOffersService.plugin";
NSString *const kPASupportServicePluginNameMessages = @"PAMessageService.plugin";
NSString *const kPASupportServicePluginNameSupportAction = @"PASupportAction.plugin";

NSString *const kPASupportServiceNameRepair = @"PARepairService";
NSString *const kPASupportServiceNameDownloadOffers = @"PADownloadOffersService";
NSString *const kPASupportServiceNameMessages = @"PAMessageService";
NSString *const kPASupportServiceNameSupportAction = @"PASupportAction";

NSString *const kPASupportServiceResponseKeyGuid = @"guid";
NSString *const kPASupportServiceResponseKeyVersion = @"version";
NSString *const kPASupportServiceResponseKeyDownloadPath = @"downloadPath";
NSString *const kPASupportServiceResponseKeyProgressValue = @"progress";
NSString *const kPASupportServiceResponseKeyStatus = @"status";
NSString *const kPASupportServiceResponseKeyInstallerType = @"installerType";

NSString *const kPASupportServiceResponseKeyExitCode = @"exit_code";
NSString *const kPASupportServiceResponseKeyOptimizationXml = @"optimizationXml";

NSString *const kPASupportServiceDownloadStatusNotificationName = @"PADownloadStatusProgress";
NSString *const kPASupportServiceDownloadOfferNotificationObjectName = @"PADownloadOffer";
NSString *const kPASupportServiceDownloadCancelNotificationName = @"PADownloadCancel";

NSString *const kPASupportServiceOptimizationNotificationObjectName = @"PAOptimization";
NSString *const kPASupportServiceStartOptimizerNotificationName = @"PASupportServiceStartOptimizer";
NSString *const kPASupportServiceStopOptimizerNotificationName = @"PASupportServiceStopOptimizer";
NSString *const kPASupportServiceStartOptimizationNotificationName = @"PASupportServiceStartOptimization";
NSString *const kPASupportServiceStopOptimizationNotificationName = @"PASupportServiceStopOptimization";

