//
//  PAEventSubscriber.m
//  PAEventSubscriptionManager
//
//  Copyright © 2018 Avolin. All rights reserved.
//
#import <PACoreServices/PACoreServices.h>
#import "PAEventSubscriber.h"

@interface PAEventSubscriber()
@property (nonatomic) NSString *objectId;
@property (nonatomic) PASelfHealTriggerInfo *triggerInfo;
@end

@implementation PAEventSubscriber

- (instancetype)initWithTriggerInfo:(PASelfHealTriggerInfo *)triggerInfo
{
    self = [super init];
    if (self) {
        if (triggerInfo && triggerInfo.contentGuid) {
            _objectId = triggerInfo.contentGuid;
            _triggerInfo = triggerInfo;
        } else {
            return nil;
        }
    }
    return self;
}

- (NSString *)objectIdentifier {
    return self.objectId;
}

- (void)onEvent:(id<PAEventType>)event {
    if ([event.eventGuid isEqualToString:self.triggerInfo.contentGuid] && event.eventClass == self.triggerInfo.monitorEventClass) {
        NSString* runCommand  = self.triggerInfo.triggerCommand;
        runCommand = [runCommand stringByAppendingFormat:@" %@",((PASelfHealEvent *)event).eventData];
      
       /* NSArray *matchingApps = [NSRunningApplication runningApplicationsWithBundleIdentifier:@"com.avolin.PANotificationTray"];
        for(NSRunningApplication *app in matchingApps){
          NSString *bundlePath =
        }*/
      
        [PALaunchServiceManager runCommand:runCommand];
      
        //Testing...
//#ifdef DEBUG
//        NSURL * bURL = [[NSWorkspace sharedWorkspace] URLForApplicationWithBundleIdentifier:@"com.apple.calculator"];
//        [[NSWorkspace sharedWorkspace] launchApplicationAtURL:bURL options:NSWorkspaceLaunchDefault configuration:[NSDictionary new] error:nil];
//#endif
    }
}
@end
