//
//  PAEventSubscriber.h
//  PAEventSubscriptionManager
//
//  Copyright © 2018 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <PAEventBus/PAEventBus.h>
#import "PASelfHealTriggerInfo.h"

@interface PAEventSubscriber : NSObject<PAEventSubscribable>

- (instancetype)initWithTriggerInfo:(PASelfHealTriggerInfo *)triggerInfo;


@end
