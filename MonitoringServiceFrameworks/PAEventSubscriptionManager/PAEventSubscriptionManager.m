//
//  PAEventSubscriptionManager.m
//  PAEventSubscriptionManager
//
//  Copyright © 2018 Avolin. All rights reserved.
//
#import <PAEventBus/PAEventBus.h>
#import "PAEventSubscriptionManager.h"
#import "PASelfHealTriggerInfo.h"
#import "PAEventSubscriber.h"

@interface PAEventSubscriptionManager()

@property (atomic) NSMutableDictionary *subscribers;
@property (nonatomic) PAEventBus *eventBus;

@end

@implementation PAEventSubscriptionManager

+ (PAEventSubscriptionManager *)sharedInstance
{
    static PAEventSubscriptionManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        _subscribers = [NSMutableDictionary new];
        _eventBus = [PAEventBus sharedInstance];
    }
    return self;
}

-(BOOL)setupSubscriberForEventInfo:(PASelfHealTriggerInfo *)triggerInfo {
    PAEventSubscriber *subscriber = [[PAEventSubscriber alloc] initWithTriggerInfo:triggerInfo];
    if (subscriber) {
        self.subscribers[subscriber.objectIdentifier] = subscriber;
        [self.eventBus registerSubscriber:subscriber forEventClass:triggerInfo.monitorEventClass];
        return YES;
    }
    return NO;
}

-(BOOL)removeSubscriberForEvent:(NSString *)eventGuid {
    PAEventSubscriber *subscriber = self.subscribers[eventGuid];
    if (subscriber) {
        [self.eventBus unregisterSubscriber:subscriber];
        [self.subscribers removeObjectForKey:eventGuid];
        return YES;
    }
    return NO;
}

- (NSArray *)registeredSubscribers {
    return self.subscribers.allValues;
}

@end
