//
//  PAEventSubscriptionManager.h
//  PAEventSubscriptionManager
//
//  Copyright © 2018 Avolin. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <Foundation/Foundation.h>

//! Project version number for PAEventSubscriptionManager.
FOUNDATION_EXPORT double PAEventSubscriptionManagerVersionNumber;

//! Project version string for PAEventSubscriptionManager.
FOUNDATION_EXPORT const unsigned char PAEventSubscriptionManagerVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <PAEventSubscriptionManager/PublicHeader.h>\

@class PASelfHealTriggerInfo;

@interface PAEventSubscriptionManager : NSObject
+ (PAEventSubscriptionManager *)sharedInstance;
- (BOOL)setupSubscriberForEventInfo:(PASelfHealTriggerInfo *)triggerInfo;
- (BOOL)removeSubscriberForEvent:(NSString *)eventGuid;
- (NSArray *)registeredSubscribers;
@end
