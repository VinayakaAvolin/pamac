//
//  PAEventMonitorManager.h
//  PAEventMonitorManager
//
//  Copyright © 2018 Avolin. All rights reserved.
//

#import <Cocoa/Cocoa.h>

//! Project version number for PAEventMonitorManager.
FOUNDATION_EXPORT double PAEventMonitorManagerVersionNumber;

//! Project version string for PAEventMonitorManager.
FOUNDATION_EXPORT const unsigned char PAEventMonitorManagerVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <PAEventMonitorManager/PublicHeader.h>

#import "PAEventMonitorInterface.h"
