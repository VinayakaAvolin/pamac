//
//  PAEventMonitorInterface.h
//  ProactiveAssistAgent
//
//  Copyright © 2018 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <PAEventBus/PAEventBus.h>

typedef void(^PAMonitorServiceResult)(PASelfHealEvent *event);
typedef void(^PAMonitorServiceStopMonitorResult)(BOOL success);

@interface PAEventMonitorInterface : NSObject

+(instancetype) sharedInstance;

-(void) setupMonitoringServiceForEvent:(PASelfHealTriggerInfo*) triggerInfo withReply:(PAMonitorServiceResult)reply;

-(void) setupMonitoringServiceForEvent:(PASelfHealTriggerInfo*) triggerInfo;

-(void) startMonitoring:(id<PAEventType>) event;

-(void) startMonitoring:(NSString *)eventGuid withReply:(PAMonitorServiceResult)reply;

-(void) stopMonitoring:(NSString *)eventGuid withReply: (PAMonitorServiceStopMonitorResult)reply;

@end
