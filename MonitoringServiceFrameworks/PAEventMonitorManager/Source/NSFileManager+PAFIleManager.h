//
//  NSFileManager+PAFIleManager.h
//  PAEventMonitorManager
//
//  Copyright © 2018 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^FileEnumerationBlock)(NSURL *_Nonnull fileURL);

@interface NSFileManager (Extensions)

- (void)enumerateWithRootDirectoryURL:(nonnull NSURL *)rootURL
                          fileHandler:(FileEnumerationBlock _Nonnull)fileHandler
                                error:(NSError *_Nullable *_Nullable)error;

@end
