//
//  PAEventMonitorInterface.m
//  ProactiveAssistAgent
//
//  Copyright © 2018 Avolin. All rights reserved.
//

#import <PACoreServices/PACoreServices.h>
#import "PAEventMonitorInterface.h"
#import "NSBundle+Additions.h"
#import "NSFileManager+PAFIleManager.h"
#import "PAMonitorServiceProvider.h"
#import "PASelfHealTriggerInfo.h"
#import "PACodeSignValidator.h"


#define TEST_PRODUCTS_PATH @"Desktop/Products"

@interface PAEventMonitorInterface ()
{
    NSMutableDictionary * _monitorMap;
}
@property (nonatomic)     PASelfHealTriggerInfo * triggerInfo;
@end

@implementation PAEventMonitorInterface

+(instancetype) sharedInstance
{
    static PAEventMonitorInterface * sEventInterface = NULL;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sEventInterface = [[PAEventMonitorInterface alloc] init];
    });
    
    return sEventInterface;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        _monitorMap = [[NSMutableDictionary alloc] init];
    }
    return self;
}

-(void) setupMonitoringServiceForEvent:(PASelfHealTriggerInfo *) eventInfo
{
    self.triggerInfo = eventInfo;
    [self identifyCandidatePLugInforEvent:eventInfo];
    [self startMonitoring:nil];
}

-(void) setupMonitoringServiceForEvent:(PASelfHealTriggerInfo*) triggerInfo withReply:(PAMonitorServiceResult)reply {
    self.triggerInfo = triggerInfo;
    [self identifyCandidatePLugInforEvent:triggerInfo];
    [self startMonitoring:nil withReply:reply];
}

-(void) identifyCandidatePLugInforEvent:(PASelfHealTriggerInfo *) event
{
    NSArray * paths = [self selfHealPluginPaths];
    for (NSString * path in paths) {
        NSLog(@"%@", path);
        [self findOutPluginCapability:path];
    }
}

- (NSArray *)selfHealPluginPaths {
    PAFileSystemManager *fsManager = [[PAFileSystemManager alloc] init];
    NSArray *paths = fsManager.selfHealMonitorPluginPaths;
    NSMutableArray *pluginPaths = [NSMutableArray new];
    for (NSString* path in paths) {
        NSArray *plugins = [fsManager contentsOfDirectory: path];
        for (NSString *plugin in plugins) {
            [pluginPaths addObject:[NSString stringWithFormat:@"%@/%@",path, plugin]];
        }
    }
    return pluginPaths;
}

-(NSArray *) findPluginsAtPath:(NSString *) path
{
    NSMutableArray * finalArray = [NSMutableArray new];
    NSURL * filePath = [NSURL fileURLWithPath:[NSHomeDirectory()stringByAppendingPathComponent:path]];
    NSLog(@"%@", filePath);
    NSError * error;
    [[NSFileManager defaultManager] enumerateWithRootDirectoryURL:filePath fileHandler:^(NSURL * _Nonnull fileURL) {
        NSLog(@"%@", fileURL);
        NSString *extension = [[fileURL absoluteString] pathExtension];
        if ([extension isEqualToString:@"bundle"] || [extension isEqualToString:@"plugin"]) {
            [finalArray addObject:fileURL];
        }
    } error:&error];
    
    NSLog(@"%@", finalArray);
    
    return finalArray;
}
// true can handle and false can't handle
-(void) findOutPluginCapability:(NSString *) urlPath
{
    BOOL hasValidCodesign = [PACodeSignValidator validateCodeSigningOfItemAtPath:urlPath];
    if (hasValidCodesign) {
        [[PALogger sharedLoggerForClient:PALogClientSelfHeal] debug:@"Code sign verified for plugin at path [%@]----", urlPath];
        id bundle = [NSBundle loadBundleWithPath:urlPath];
        
        BOOL result = [(PAMonitorServiceProvider *)bundle canHandleCapability:self.triggerInfo.monitorEventClass forEvent:self.triggerInfo];
        
        NSLog(@"%d", result);
        if (result) {
            [_monitorMap setObject:bundle forKey:self.triggerInfo.contentGuid];
        }
    } else {
        [[PALogger sharedLoggerForClient:PALogClientSelfHeal] debug:@"*****Code sign did NOT match for plugin at path [%@]----", urlPath];
    }
}

-(void) startMonitoring:(id<PAEventType>) event
{
    if (_monitorMap && _monitorMap.allKeys.count > 0) {
        PAMonitorServiceProvider * provider = nil;
        if (event) {
            provider = _monitorMap[event.eventGuid];
        } else {
            provider = _monitorMap[self.triggerInfo.contentGuid];
        }
        if (provider) {
            [provider startMonitoringEvent:self.triggerInfo];
        }
    }
}

-(void) startMonitoring:(NSString *)eventGuid withReply:(PAMonitorServiceResult)reply {
    if (_monitorMap && _monitorMap.allKeys.count > 0) {
        PAMonitorServiceProvider * provider = nil;
        if (eventGuid) {
            provider = _monitorMap[eventGuid];
        } else {
            provider = _monitorMap[self.triggerInfo.contentGuid];
        }
        if (provider) {
            [provider startMonitoringEvent:self.triggerInfo withReply:reply];
        }
    }
}


-(void) stopMonitoring:(NSString *)eventGuid withReply: (PAMonitorServiceStopMonitorResult)reply
{
    @synchronized (self) {
        NSLog(@"Stopping monitoring...");
        NSBundle *provider = _monitorMap[eventGuid];
        BOOL result = NO;
        if (provider) {
            PASelfHealTriggerInfo *triggerInfo = [[PASelfHealTriggerInfo alloc] initWithContentGuid:eventGuid version:nil contentXmlPath:nil];
            [(PAMonitorServiceProvider *)provider
             stopMonitoringEvent:triggerInfo];
            [_monitorMap removeObjectForKey:eventGuid];
            provider = nil;
            result = YES;
        }
        reply(result);
    }
}

@end
