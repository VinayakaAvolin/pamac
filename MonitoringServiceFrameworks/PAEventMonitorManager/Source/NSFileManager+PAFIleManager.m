//
//  NSFileManager+PAFIleManager.h
//  PAEventMonitorManager
//
//  Copyright © 2018 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSFileManager+PAFIleManager.h"

@implementation NSFileManager (Extensions)

- (void)enumerateWithRootDirectoryURL:(NSURL *)rootURL
                          fileHandler:(FileEnumerationBlock)fileHandler
                                error:(NSError **)error {
    NSString *rootPath = rootURL.path;
    NSAssert(rootPath != nil, @"Invalid root URL %@ (nil path)", rootURL);
    
    NSArray *subs = [[NSFileManager defaultManager] subpathsOfDirectoryAtPath:rootPath
                                                                        error:error];
    
    if (!subs) {
        return;
    }
    
    for (NSString *sub in subs) {
        fileHandler([rootURL URLByAppendingPathComponent:sub]);
    }
}

@end
