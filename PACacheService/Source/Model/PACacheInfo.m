//
//  PACacheInfo.m
//  PACacheService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PACacheInfo.h"

@implementation PACacheInfo

- (void)addToCacheWithRealm:(RLMRealm *)realm {
  [realm beginWriteTransaction];
  [realm addObject:self];
  [realm commitWriteTransaction];
}

- (void)updateInCache:(id)oldObject
            withRealm:(RLMRealm *)realm {
  [realm beginWriteTransaction];
  if (oldObject) {
    [realm deleteObject:oldObject];
  }
  [realm addObject:self];
  [realm commitWriteTransaction];
}


- (void)removeFromCacheWithRealm:(RLMRealm *)realm {
  [realm beginWriteTransaction];
  [realm deleteObject:self];
  [realm commitWriteTransaction];
}

@end
