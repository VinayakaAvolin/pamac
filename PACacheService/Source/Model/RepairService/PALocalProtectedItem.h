//
//  PALocalProtectedItem.h
//  PACacheService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PACacheInfo.h"
#import "PAProtectedItem.h"

@interface PALocalProtectedItem : PACacheInfo

@property (nonatomic) NSString *guid;
@property (nonatomic) NSString *version;
@property (nonatomic) NSString *dateString;

- (instancetype)initWithProtectedItem:(PAProtectedItem *)item;

@end

// This protocol enables typed collections. i.e.:
RLM_ARRAY_TYPE(PALocalProtectedItem)
