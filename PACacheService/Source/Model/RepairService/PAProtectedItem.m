//
//  PAProtectedItem.m
//  PACacheService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PAProtectedItem.h"
#import "PACacheServiceConstants.h"

@implementation PAProtectedItem

- (instancetype)initWithGuid:(NSString *)guid
                     version:(NSString *)version
                        date:(NSString *)dateString {
  
  self = [super init];
  if (self) {
    if (!guid || !version || !dateString) {
      self = nil;
    } else {
      _guid = guid;
      _version = version;
      _dateString = dateString;
    }
  }
  return self;
}

- (NSDictionary *)dictionaryRepresentation {
  NSMutableDictionary *dict = [NSMutableDictionary dictionary];
  [dict setValue:_guid forKey:kPACSProtectGuidKey];
  [dict setValue:_version forKey:kPACSProtectVersionKey];
  [dict setValue:_dateString forKey:kPACSProtectDateKey];
  return [NSDictionary dictionaryWithDictionary:dict];
}

@end
