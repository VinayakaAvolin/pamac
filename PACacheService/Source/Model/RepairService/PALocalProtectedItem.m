//
//  PALocalProtectedItem.m
//  PACacheService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PALocalProtectedItem.h"

@implementation PALocalProtectedItem

- (instancetype)initWithProtectedItem:(PAProtectedItem *)item {
  self = [super init];
  if (self) {
    _guid = item.guid;
    _version = item.version;
    _dateString = item.dateString;
  }
  return self;
}

@end
