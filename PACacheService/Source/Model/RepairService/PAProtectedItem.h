//
//  PAProtectedItem.h
//  PACacheService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PACacheInfoProtocol.h"

@interface PAProtectedItem : NSObject <PACacheInfoProtocol>

@property (nonatomic) NSString *guid;
@property (nonatomic) NSString *version;
@property (nonatomic) NSString *dateString;

@property (nonatomic) NSDictionary *dictionaryRepresentation;

- (instancetype)initWithGuid:(NSString *)xmlPath
                     version:(NSString *)version
                        date:(NSString *)dateString;

@end
