//
//  RLMResults+Additions.h
//  PACacheService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>

@interface RLMResults (Additions)

- (NSArray *)toArray;

@end
