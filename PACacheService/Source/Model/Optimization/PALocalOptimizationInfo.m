//
//  PALocalOptimizationInfo.m
//  PACacheService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PALocalOptimizationInfo.h"

@implementation PALocalOptimizationInfo

// Specify default values for properties

//+ (NSDictionary *)defaultPropertyValues
//{
//    return @{};
//}

// Specify properties to ignore (Realm won't persist these)
//
//+ (NSArray *)ignoredProperties
//{
//    return @[];
//}

- (instancetype)initWithOptimizationInfo:(PAOptimizationInfo *)infoObj {
  self = [super init];
  if (self) {
    _frequency = @(infoObj.frequency);
    _hours = @(infoObj.hours);
    _minutes = @(infoObj.minutes);
    _identifier = infoObj.identifier;
  }
  return self;
}
@end
