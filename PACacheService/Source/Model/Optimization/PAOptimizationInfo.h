//
//  PAOptimizationInfo.h
//  PACacheService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PACacheInfoProtocol.h"

typedef enum : NSUInteger {
  PAOptimizationFrequencyNone,
  PAOptimizationFrequencyDaily,
  PAOptimizationFrequencyWeekly,
  PAOptimizationFrequencyMonthly
} PAOptimizationFrequency;


@interface PAOptimizationInfo : NSObject <PACacheInfoProtocol>

@property (nonatomic) PAOptimizationFrequency frequency;
@property (nonatomic) NSUInteger hours;
@property (nonatomic) NSUInteger minutes;
@property (nonatomic) NSString *identifier;

@end
