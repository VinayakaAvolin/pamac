//
//  PALocalOptimizationInfo.h
//  PACacheService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "PACacheInfo.h"
#import "PAOptimizationInfo.h"

@interface PALocalOptimizationInfo : PACacheInfo

@property (nonatomic) NSNumber<RLMInt> *frequency;
@property (nonatomic) NSNumber<RLMInt> *hours;
@property (nonatomic) NSNumber<RLMInt> *minutes;
@property (nonatomic) NSString *identifier;

- (instancetype)initWithOptimizationInfo:(PAOptimizationInfo *)infoObj;

@end

// This protocol enables typed collections. i.e.:
// RLMArray<PALocalOptimizationInfo>
RLM_ARRAY_TYPE(PALocalOptimizationInfo)
