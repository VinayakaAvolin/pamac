//
//  PACacheInfo.h
//  PACacheService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>

#import "PACacheInfoProtocol.h"

@interface PACacheInfo : RLMObject <PACacheInfoProtocol>

- (void)addToCacheWithRealm:(RLMRealm *)realm;
- (void)removeFromCacheWithRealm:(RLMRealm *)realm;
- (void)updateInCache:(id)oldObject withRealm:(RLMRealm *)realm;

@end
