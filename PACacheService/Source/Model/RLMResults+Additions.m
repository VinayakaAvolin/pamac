//
//  RLMResults+Additions.m
//  PACacheService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "RLMResults+Additions.h"

@implementation RLMResults (Additions)

- (NSArray *)toArray {
  NSMutableArray *array = [NSMutableArray new];
  for (RLMObject *object in self) {
    [array addObject:object];
  }
  return array;
}

@end
