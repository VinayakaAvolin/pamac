//
//  PACacheInfoFactory.h
//  PACacheService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>
#import "PACacheInfoProtocol.h"

@interface PACacheInfoFactory : NSObject

+ (id<PACacheInfoProtocol>)localObjectFrom:(id)inputInfo;
+ (id<PACacheInfoProtocol>)cachedObjectFor:(id)inputInfo ;
+ (id<PACacheInfoProtocol>)remoteObjectFrom:(id)inputInfo;
+ (NSArray *)remoteObjectsFrom:(NSArray *)inputInfoList;
+ (NSArray *)localObjectsFrom:(NSArray *)inputInfoList;
+ (NSArray *)allObjectsFor:(Class)realmClass inRealm:(RLMRealm*)realmRef;

@end
