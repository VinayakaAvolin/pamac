//
//  PACacheServiceConstants.m
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PACacheServiceConstants.h"

NSString *const kPACSProtectGuidKey = @"guid";
NSString *const kPACSProtectVersionKey = @"version";
NSString *const kPACSProtectDateKey = @"dateString";
