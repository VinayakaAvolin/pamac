//
//  PACacheInfoFactory.m
//  PACacheService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PACacheInfoFactory.h"
#import "PALocalOptimizationInfo.h"
#import "PAOptimizationInfo.h"
#import "PAProtectedItem.h"
#import "PALocalProtectedItem.h"
#import "RLMResults+Additions.h"

@implementation PACacheInfoFactory

+ (id<PACacheInfoProtocol>)localObjectFrom:(id)inputInfo {
  id<PACacheInfoProtocol> localInfoObj;
  if ([inputInfo isKindOfClass:PAOptimizationInfo.class]) {
    localInfoObj = [[PALocalOptimizationInfo alloc]initWithOptimizationInfo:(PAOptimizationInfo *)inputInfo];
  } else if ([inputInfo isKindOfClass:PAProtectedItem.class]) {
    localInfoObj = [[PALocalProtectedItem alloc]initWithProtectedItem:(PAProtectedItem *)inputInfo];
  } else {
    
  }
  return localInfoObj;
}

+ (id<PACacheInfoProtocol>)cachedObjectFor:(id)inputInfo {
  id<PACacheInfoProtocol> localInfoObj;
  if ([inputInfo isKindOfClass:PAOptimizationInfo.class]) {
    NSString *objId = ((PAOptimizationInfo *)inputInfo).identifier;
    localInfoObj = [[[PALocalOptimizationInfo objectsWhere:@"identifier = %@",objId] toArray] firstObject];
  } else if ([inputInfo isKindOfClass:PAProtectedItem.class]) {
    NSString *objId = ((PAProtectedItem *)inputInfo).dateString;
    localInfoObj = [[[PALocalProtectedItem objectsWhere:@"dateString = %@",objId] toArray] firstObject];
  } else {
    
  }
  return localInfoObj;
}


+ (id<PACacheInfoProtocol>)remoteObjectFrom:(id)inputInfo {
  id<PACacheInfoProtocol> remoteInfoObj;
  if ([inputInfo isKindOfClass:PALocalOptimizationInfo.class]) {
    PAOptimizationInfo *remoteInfo = [[PAOptimizationInfo alloc]init];
    PALocalOptimizationInfo *localInfo = (PALocalOptimizationInfo *)inputInfo;
    remoteInfo.frequency = localInfo.frequency.integerValue;
    remoteInfo.hours = localInfo.hours.integerValue;
    remoteInfo.minutes = localInfo.minutes.integerValue;
    remoteInfoObj = remoteInfo;
  }  else if ([inputInfo isKindOfClass:PALocalProtectedItem.class]) {
    PAProtectedItem *remoteInfo = [[PAProtectedItem alloc]init];
    PALocalProtectedItem *localInfo = (PALocalProtectedItem *)inputInfo;
    remoteInfo.guid = localInfo.guid;
    remoteInfo.version = localInfo.version;
    remoteInfo.dateString = localInfo.dateString;
    remoteInfoObj = remoteInfo;
  } else {
    
  }
  return remoteInfoObj;
}

+ (NSArray *)remoteObjectsFrom:(NSArray *)inputInfoList {
  NSMutableArray *infoList = [[NSMutableArray alloc]init];
  for (id<PACacheInfoProtocol> info in inputInfoList) {
    id<PACacheInfoProtocol> remoteInfoObj = [PACacheInfoFactory remoteObjectFrom:info];
    if (remoteInfoObj) {
      [infoList addObject:remoteInfoObj];
    }
  }
  if (infoList.count) {
    return [NSArray arrayWithArray:infoList];
  }
  return nil;
}

+ (NSArray *)localObjectsFrom:(NSArray *)inputInfoList {
  NSMutableArray *infoList = [[NSMutableArray alloc]init];
  for (id<PACacheInfoProtocol> info in inputInfoList) {
    id<PACacheInfoProtocol> localInfoObj = [PACacheInfoFactory localObjectFrom:info];
    if (localInfoObj) {
      [infoList addObject:localInfoObj];
    }
  }
  if (infoList.count) {
    return [NSArray arrayWithArray:infoList];
  }
  return nil;
}

+ (NSArray *)allObjectsFor:(Class)realmClass  inRealm:(RLMRealm*)realmRef {
  if (realmClass == [PAProtectedItem class]) {
    return [[PALocalProtectedItem allObjectsInRealm:realmRef] toArray];
  }
  else if (realmClass == [PAOptimizationInfo class]) {
    return [[PALocalOptimizationInfo allObjectsInRealm:realmRef] toArray];
  }
  return nil;
}

@end
