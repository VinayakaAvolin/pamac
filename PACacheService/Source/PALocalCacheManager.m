//
//  PALocalCacheManager.m
//  PACacheService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Realm/Realm.h>
#import "PALocalCacheManager.h"
#import "PACacheInfoFactory.h"
#import "PACacheInfo.h"
#import "RLMResults+Additions.h"

@interface PALocalCacheManager () {
  NSString *_databasePath;
}
@end

@implementation PALocalCacheManager

- (instancetype)initWithDatabase:(NSString *)dbPath {
  self = [super init];
  if (self) {
    if (dbPath) {
      _databasePath = dbPath;
    }
  }
  return self;
}
- (void)add:(id<PACacheInfoProtocol>)cacheInfo {
  [self printRealmDbPath];
  id<PACacheInfoProtocol> localInfoObj = [PACacheInfoFactory localObjectFrom:cacheInfo];
  [(PACacheInfo *)localInfoObj addToCacheWithRealm:[self realmObject]];
}

- (void)update:(id<PACacheInfoProtocol>)cacheInfo {
  id<PACacheInfoProtocol> oldLocalInfoObj = [PACacheInfoFactory cachedObjectFor:cacheInfo];
  id<PACacheInfoProtocol> newLocalInfoObj = [PACacheInfoFactory localObjectFrom:cacheInfo];
  [(PACacheInfo *)newLocalInfoObj updateInCache:oldLocalInfoObj withRealm:[self realmObject]];
}

/// Removes input info from local cache
- (void)remove:(id<PACacheInfoProtocol>)cacheInfo {
  id<PACacheInfoProtocol> localInfoObj = [PACacheInfoFactory localObjectFrom:cacheInfo];
  [(PACacheInfo *)localInfoObj removeFromCacheWithRealm:[self realmObject]];
}

/// returns cache object matching input objClass
- (id<PACacheInfoProtocol>)cachedObjectOfType:(Class)objClass {
  id<PACacheInfoProtocol> cachedObject = [[PACacheInfoFactory allObjectsFor:objClass inRealm:[self realmObject]] firstObject];
  id<PACacheInfoProtocol> remoteCachedObject = [PACacheInfoFactory remoteObjectFrom:cachedObject];
  return remoteCachedObject;
}

/// returns cache objects matching input objClass
- (NSArray *)cachedObjectsOfType:(Class)objClass {
  NSArray *cachedObjects = [PACacheInfoFactory allObjectsFor:objClass inRealm:[self realmObject]];
  NSArray *remoteCachedObject = [PACacheInfoFactory remoteObjectsFrom:cachedObjects];
  return remoteCachedObject;
}

- (void)clearCache {
  RLMRealm *realm = [self realmObject];
  [realm beginWriteTransaction];
  [realm deleteAllObjects];
  [realm commitWriteTransaction];
}

- (void)printRealmDbPath {
  RLMRealm *realm = [self realmObject];
}

- (RLMRealm *)realmObject {
  NSURL *fileUrl = [NSURL URLWithString:[_databasePath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] ;
  if (fileUrl) {
    RLMRealmConfiguration *configuration = [[RLMRealmConfiguration alloc]init];
    configuration.fileURL = fileUrl;
    configuration.schemaVersion = 1;
    NSError *error = nil;
    RLMRealm *realmRef = [RLMRealm realmWithConfiguration:configuration error:&error];
    return realmRef;
  }
  return nil;
}
@end
