//
//  PALocalCacheManager.h
//  PACacheService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PACacheInfoProtocol.h"

/**
 Manages local cache
 */
@interface PALocalCacheManager : NSObject

- (instancetype)initWithDatabase:(NSString *)dbPath;

/// Adds input info to local cache
- (void)add:(id<PACacheInfoProtocol>)cacheInfo;

/// Updates input info to local cache
- (void)update:(id<PACacheInfoProtocol>)cacheInfo;

/// Removes input info from local cache
- (void)remove:(id<PACacheInfoProtocol>)cacheInfo;

/// returns cache object matching input objClass
- (id<PACacheInfoProtocol>)cachedObjectOfType:(Class)objClass;

/// returns cache objects matching input objClass
- (NSArray *)cachedObjectsOfType:(Class)objClass;

-(void)clearCache; // to clear the entire local cache

@end
