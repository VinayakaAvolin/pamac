//
//  PACacheServiceConstants.h
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifndef PACacheServiceConstants_h
#define PACacheServiceConstants_h

extern NSString *const kPACSProtectGuidKey;
extern NSString *const kPACSProtectVersionKey;
extern NSString *const kPACSProtectDateKey;

#endif /* PACacheServiceConstants_h */
