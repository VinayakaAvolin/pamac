//
//  PACacheService.h
//  PACacheService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Cocoa/Cocoa.h>

//! Project version number for PACacheService.
FOUNDATION_EXPORT double PACacheServiceVersionNumber;

//! Project version string for PACacheService.
FOUNDATION_EXPORT const unsigned char PACacheServiceVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <PACacheService/PublicHeader.h>

#import <PACacheService/PALocalCacheManager.h>
#import <PACacheService/PAOptimizationInfo.h>
#import <PACacheService/PACacheInfoProtocol.h>
#import <PACacheService/PAProtectedItem.h>
#import <PACacheService/PACacheServiceConstants.h>
