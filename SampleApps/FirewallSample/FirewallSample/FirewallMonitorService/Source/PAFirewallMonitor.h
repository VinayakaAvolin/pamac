//
//  PAFirewallMonitor.h
//  PAFirewallEventMonitorService
//
//  Created by Ramachandra Naragund on 11/12/18.
//  Copyright © 2018 Aptean. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PAFirewallMonitorEventDelegate.h"
@class PASelfHealEvent;

@interface PAFirewallMonitor : NSObject

-(instancetype) initWithDelegate:(id<PAFirewallMonitorEventDelegate>) delegate forStatus:(PASelfHealEvent *) event;

-(void) startMonitoring;

-(void) stopMonitoring;

@end

