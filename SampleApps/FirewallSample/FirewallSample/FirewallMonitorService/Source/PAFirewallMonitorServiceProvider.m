//
//  PAFirewallMonitorServiceProvider.m
//  PAFirewallEventMonitorService
//
//  Created by Ramachandra Naragund on 11/12/18.
//  Copyright © 2018 Aptean. All rights reserved.
//

#import "PAFirewallMonitorServiceProvider.h"
#import "PAFirewallMonitor.h"

@interface PAFirewallMonitorServiceProvider ()
{
    PAFirewallMonitor * firewallMonitor;
}
@end

@implementation PAFirewallMonitorServiceProvider


- (BOOL)canHandleCapability:(SelfHealEventClass)capability forEvent:(PASelfHealEvent*)event
{
    return capability == BatteryLevelMonitoring;
}

- (void) startMonitoringEvent:(PASelfHealEvent*)event
{
    NSLog(@"Firewall monitoring started");
    //TODO: Identify the paths and data to be monitored.
    if (!firewallMonitor) {
        firewallMonitor = [[PAFirewallMonitor alloc] initWithDelegate:self forStatus:event];
    }
    [firewallMonitor startMonitoring];
    
}

- (void) stopMonitoringEvent:(PASelfHealEvent*)event
{
    NSLog(@"Firewall monitoring ended");
    [firewallMonitor stopMonitoring];
}

- (void) firewallMonitor:(PAFirewallMonitorServiceProvider *)batteryMonitor eventOccurred:(PASelfHealEvent *)event
{
    NSLog(@"Call the self heal trigger class methods");
}

@end
