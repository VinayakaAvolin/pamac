//
//  PAFirewallMonitorServiceProvider.h
//  PAFirewallEventMonitorService
//
//  Created by Ramachandra Naragund on 11/12/18.
//  Copyright © 2018 Aptean. All rights reserved.
//

#import "PAMonitorServiceProvider.h"
#import "PAFirewallMonitorEventDelegate.h"


@interface PAFirewallMonitorServiceProvider : PAMonitorServiceProvider <PAFirewallMonitorEventDelegate>

@end
