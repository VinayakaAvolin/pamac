//
//  PAFirewallMonitorEventDelegate.h
//  ProactiveAssist
//
//  Created by Ramachandra Naragund on 11/12/18.
//  Copyright © 2018 Aptean. All rights reserved.
//

#ifndef PAFirewallMonitorEventDelegate_h
#define PAFirewallMonitorEventDelegate_h

@class PAFirewallMonitorServiceProvider;
@class PASelfHealEvent;

@protocol PAFirewallMonitorEventDelegate
@required

- (void) firewallMonitor:(PAFirewallMonitorServiceProvider *)batteryMonitor eventOccurred:(PASelfHealEvent *)event;

@end

#endif /* PAFirewallMonitorEventDelegate_h */
