//
//  PAMonitorServiceProvider.h
//  PABaseMonitorService
//
//  Created by Vijayaraja H A on 24/11/18.
//  Copyright © 2018 Aptean. All rights reserved.
//

#import <Foundation/Foundation.h>

//#import "PAMonitorServiceConstants.h"
#import "PAMonitorServiceDelegate.h"
#import "PASelfHealEvent.h"

@class PAMonitorServiceInfo;

@interface PAMonitorServiceProvider : NSObject

@property(weak) id<PAMonitorServiceDelegate> delegate;

- (BOOL)canHandleCapability:(SelfHealEventClass)capability forEvent:(PASelfHealEvent*)event;

- (void) startMonitoringEvent:(PASelfHealEvent*)event;

- (void) stopMonitoringEvent:(PASelfHealEvent*)event;

@end
