//
//  PAFirewallMonitor.m
//  PAFirewallEventMonitorService
//
//  Created by Ramachandra Naragund on 11/12/18.
//  Copyright © 2018 Aptean. All rights reserved.
//

#import "PAFirewallMonitor.h"
#import "PAScriptRunner.h"
#import "PASelfHealEvent.h"

@interface PAFirewallMonitor ()

@property (nonatomic) id<PAFirewallMonitorEventDelegate> delegate;
@property (nonatomic) PASelfHealEvent * event;
@property (nonatomic) NSTimer * timer;
@end

@implementation PAFirewallMonitor

-(instancetype) initWithDelegate:(id<PAFirewallMonitorEventDelegate>) delegate forStatus:(PASelfHealEvent *) event
{
    if (self = [super init]) {
        self.delegate = delegate;
        self.event = event;
    }
    return self;
}

- (void)setupTimerAndPoll {
    self.timer = [NSTimer scheduledTimerWithTimeInterval:10 // event.interval
                                                 repeats:YES block:^(NSTimer * _Nonnull timer) {
                                                     PAFirewallMonitor * weakSelf = self;
                                                     [PAScriptRunner executeScript:@"/usr/libexec/ApplicationFirewall/socketfilterfw --getglobalstate"
                                                                            ofType:PAScriptTypeShell
                                                                          callBack:^(BOOL success, NSError *error, id result) {
                                                                              PAFirewallMonitor * strongSelf = weakSelf;
                                                                              
                                                                              NSLog(@"result = %@", result); // Firewall is enabled. (State = 1)
                                                                              if (!error && [result isKindOfClass:[NSString class]]) {
                                                                                  NSString * statusValue = (NSString *) result;
                                                                                  if (statusValue && statusValue.length) {
                                                                                      NSString * resultValue = [[statusValue componentsSeparatedByString:@"(State = "] lastObject];
                                                                                      BOOL state = [[resultValue substringToIndex:resultValue.length-2] boolValue];
                                                                                      //                                     NSString * string = (NSString *)[strongSelf.event.fieldSet objectForKey:kPASelfHealEventFirewallStatusKey];
                                                                                      //                                     FirewallStatus firewallStatus = [string integerValue];
                                                                                      FirewallStatus firewallStatus = Enabled;
                                                                                      if (firewallStatus == Enabled && state == true) {
                                                                                          [strongSelf.delegate firewallMonitor:nil eventOccurred:nil];
                                                                                      } else if (firewallStatus == Disabled && state == false) {
                                                                                          [strongSelf.delegate firewallMonitor:nil eventOccurred:nil];
                                                                                      }
                                                                                  }
                                                                              }
                                                                          }];
                                                 }];
    
}

-(void) startMonitoring
{
    [self setupTimerAndPoll];
}

-(void) stopMonitoring
{
    [self.timer invalidate];
    self.timer = nil;
    self.delegate = nil;
    self.event = nil;
}


@end
