//
//  AppDelegate.m
//  FirewallSample
//
//  Created by Ramachandra Naragund on 11/12/18.
//  Copyright © 2018 Ramachandra Naragund. All rights reserved.
//

#import "AppDelegate.h"
#import "PAFirewallMonitorServiceProvider.h"


@interface AppDelegate ()
@property (nonatomic) PAFirewallMonitorServiceProvider * fp;
@property (weak) IBOutlet NSWindow *window;
@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application
    self.fp = [PAFirewallMonitorServiceProvider new];
    [self.fp startMonitoringEvent:nil];
}


- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}

- (void)applicationWillUnhide:(NSNotification *)notification
{
    [self.fp stopMonitoringEvent:nil];
}

@end
