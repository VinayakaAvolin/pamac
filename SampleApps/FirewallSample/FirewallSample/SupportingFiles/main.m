//
//  main.m
//  FirewallSample
//
//  Created by Ramachandra Naragund on 11/12/18.
//  Copyright © 2018 Ramachandra Naragund. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
