//
//  PAScriptRunner.m
//  PAScriptRunnerService
//
//  Copyright © 2017 Aptean. All rights reserved.
//

//#import <PAFileSystemService/PAFileSystemService.h>
#import "PAScriptRunner.h"
#import "PAScriptProtocol.h"
#import "PAScriptFactory.h"
//#import "NSError+Additions.h"

NSString *const kPAAppleScriptExtension = @"scpt";
NSString *const kPAShellScriptExtension = @"sh";

@implementation PAScriptRunner

+ (void)executeScriptWithPath:(NSString *)path
                     callBack:(PAScriptCallback)callback {
  [PAScriptRunner executeScriptWithPath:path ofType:[PAScriptRunner scriptTypeFromPath:path] callBack:callback];
}
+ (void)executeScriptWithPath:(NSString *)path
                       ofType:(PAScriptType)type
                     callBack:(PAScriptCallback)callback {
  id<PAScriptProtocol> script = [PAScriptFactory scriptWithPath:path scriptType:type];
  [script runWithCallBack:callback];
  
}
+ (void)executeScript:(NSString *)scriptString
               ofType:(PAScriptType)type
             callBack:(PAScriptCallback)callback {
  NSString *scriptStr = scriptString;
  PAScriptType scriptType = type;
  /// shell commands cannot be executed as sring; we will have to execute the script string by prefixing "do shell script" via Apple script
  if (type == PAScriptTypeShell) {
    if (![scriptStr containsString:@"do shell script"]) {
      scriptStr = [NSString stringWithFormat:@"do shell script \"%@\"",scriptString];
    }
    scriptType = PAScriptTypeApple;
  }
  id<PAScriptProtocol> script = [PAScriptFactory scriptWithString:scriptStr scriptType:scriptType];
  [script runWithCallBack:callback];
}

//+ (void)executeScript:(NSString *)scriptString
//               ofType:(PAScriptType)type
//writeToFileBeforeExecute:(BOOL)writeToFile
//           scriptName:(NSString *)fileName
//             callBack:(PAScriptCallback)callback
//{
//  PAFileSystemManager *fsManager = [[PAFileSystemManager alloc]init];
//  NSString *tempScriptPath = [fsManager temporaryDownloadsFolderPath];
//  tempScriptPath = [tempScriptPath stringByAppendingPathComponent:fileName];
//  BOOL createFile = true;
//  switch (type) {
//    case PAScriptTypeApple:
//      tempScriptPath = [tempScriptPath stringByAppendingPathExtension:kPAAppleScriptExtension];
//      break;
//    case PAScriptTypeShell:
//      tempScriptPath = [tempScriptPath stringByAppendingPathExtension:kPAShellScriptExtension];
//      break;
//    default:
//      createFile = false;
//      break;
//  }
//  
//  if ([fsManager doesFileExist:tempScriptPath]) {
//    [fsManager deleteFile:tempScriptPath];
//  }
//  NSData *fileData = [scriptString dataUsingEncoding:NSASCIIStringEncoding];
//  if (createFile && fileData && [fsManager createFileAtPath:tempScriptPath attributes:nil error:nil] && [fsManager writeToFileAtPath:tempScriptPath data:fileData]) {
//    id<PAScriptProtocol> script = [PAScriptFactory scriptWithPath:tempScriptPath scriptType:type];
//    [script runWithCallBack:^(BOOL success, NSError *error, id result) {
//      [fsManager deleteFile:tempScriptPath];
//      callback(success, error, result);
//    }];
//  } else {
//    callback(false, [NSError errorWithMessage:nil code:-100], nil);
//  }
//}
/**
 Returns script type from file name extension
 
 @param path The path to the script
 @return Returns PAScriptTypeApple: if file extension is "scpt";
 PAScriptTypeShell: if file extension is "sh";
 PAScriptTypeNone: if no extension or unrecognized file extension
 */
+ (PAScriptType)scriptTypeFromPath:(NSString *)path {
  PAScriptType scriptType = PAScriptTypeNone;
  NSString *extension = [path pathExtension];
  if ([extension isEqualToString:kPAAppleScriptExtension]) {
    scriptType = PAScriptTypeApple;
  } else if ([extension isEqualToString:kPAShellScriptExtension]) {
    scriptType = PAScriptTypeShell;
  }
  return scriptType;
}

@end
