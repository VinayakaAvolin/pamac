//
//  PAAppleScript.h
//  PAScriptRunnerService
//
//  Copyright © 2017 Aptean. All rights reserved.
//

#import <AppKit/AppKit.h>
#import <Cocoa/Cocoa.h>
#import <Foundation/Foundation.h>
#import <Carbon/Carbon.h>
#import "PAScript.h"

@interface PAAppleScript : PAScript

- (instancetype)initWithPath:(NSString *)path
                     handler:(NSString *)handlerName
                  parameters:(id)firstParameter, ... ;

@end
