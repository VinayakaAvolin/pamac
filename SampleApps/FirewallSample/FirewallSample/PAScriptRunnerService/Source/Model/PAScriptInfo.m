//
//  PAScriptInfo.m
//  PrivilegedTool
//
//  Copyright © 2017 Aptean. All rights reserved.
//

#import "PAScriptInfo.h"

NSString *const kSIScriptPath = @"_scriptPath";
NSString *const kSIScriptString = @"_scriptString";
NSString *const kSIScriptType = @"_scriptType";
NSString *const kSIScriptMode = @"_scriptMode";

@implementation PAScriptInfo

+ (BOOL)supportsSecureCoding {
  return YES;
}

- (id)initWithCoder:(NSCoder *)coder {
  if ((self = [super init])) {
    // Decode the property values by key, specifying the expected class
    _scriptPath = [coder decodeObjectOfClass:[NSString class] forKey:kSIScriptPath];
    _scriptString = [coder decodeObjectOfClass:[NSString class] forKey:kSIScriptString];
    _scriptType = [coder decodeIntegerForKey:kSIScriptType];
    _scriptMode = [coder decodeIntegerForKey:kSIScriptMode];
  }
  return self;
}

- (void)encodeWithCoder:(NSCoder *)coder {
  // Encode our ivars using string keys as normal
  [coder encodeObject:_scriptPath forKey:kSIScriptPath];
  [coder encodeObject:_scriptString forKey:kSIScriptString];
  [coder encodeInteger:_scriptType forKey:kSIScriptType];
  [coder encodeInteger:_scriptMode forKey:kSIScriptMode];
}


- (instancetype)initWithPath:(NSString *)path
                        type:(PAScriptType)scriptType
                        mode:(PAScriptMode)scriptMode {
  self = [super init];
  if (self) {
    if (!path ||
        scriptType == PAScriptTypeNone) {
      self = nil;
    } else {
      _scriptPath = path;
      _scriptType = scriptType;
      _scriptMode = scriptMode;
    }
  }
  return self;
}

- (instancetype)initWithScript:(NSString *)scriptString
                          type:(PAScriptType)scriptType
                          mode:(PAScriptMode)scriptMode {
  self = [super init];
  if (self) {
    if (!scriptString ||
        scriptType == PAScriptTypeNone) {
      self = nil;
    } else {
      _scriptString = scriptString;
      _scriptType = scriptType;
      _scriptMode = scriptMode;
    }
  }
  return self;
}

@end
