//
//  PAScript.h
//  PAScriptRunnerService
//
//  Copyright © 2017 Aptean. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "PAScriptRunnerConstants.h"
#import "PAScriptProtocol.h"


@interface PAScript : NSObject <PAScriptProtocol>

@property (nonatomic) NSString *scriptPath;
@property (nonatomic) NSString *scriptString;

- (BOOL)setExecutablePermissionToScriptAtPath: (NSString*)scriptPath;

@end
