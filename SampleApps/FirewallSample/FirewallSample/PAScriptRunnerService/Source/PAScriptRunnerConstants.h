//
//  PAScriptRunnerConstants.h
//  PAScriptRunnerService
//
//  Copyright © 2017 Aptean. All rights reserved.
//


typedef enum : NSUInteger {
  PAScriptTypeNone,
  PAScriptTypeApple,
  PAScriptTypeShell
} PAScriptType;
/// order of elements in the kPAScriptTypeArray should match the PAScriptType enum order
#define kPAScriptTypeArray @"unknown", @"apple", @"shell", nil
#define kPAScriptTypeArray1 @"unknown", @"applescript", @"shellscript", nil

typedef enum : NSUInteger {
  PAScriptModeNone,
  PAScriptModeNormal,
  PAScriptModeElevated
} PAScriptMode;


typedef void(^PAScriptCallback)(BOOL success, NSError *error, id result);
