//
//  NSAppleEventDescriptor+Additions.m
//  PAScriptRunnerService
//
//  Copyright © 2017 Aptean. All rights reserved.
//


#import "NSAppleEventDescriptor+Additions.h"

@implementation NSAppleEventDescriptor (Additions)

/* Convert an AEDescList of strings to NSArray of strings preserving their ordering. */
- (NSArray*)arrayOfStrings {
  
  NSAppleEventDescriptor *listDescriptor = [self coerceToDescriptorType:typeAEList];
  NSArray *list = nil;
  if (!listDescriptor) {
    return nil;
  }
  /* count the number of items in the AEDescList */
  unsigned index, total = (unsigned)[self numberOfItems];
  
  /* create a local array */
  NSMutableArray* array = [NSMutableArray arrayWithCapacity:total];
  
  /* add all of the strings to the array */
  for (index=0; index<total; index++) {
    
    /* get the nth descriptor's content as a string */
    NSString* theString = [[self descriptorAtIndex:(index+1)] stringValue];
    
    /* append the object to the items in the array */
    [array addObject:theString];
  }
  if (array.count) {
    list = [NSArray arrayWithArray:array];
  }
  /* return the new array */
  return list;
}



/* Convert an AEDescList of strings to NSArray of strings sorted in alphabetical order. */
- (NSArray*)sortedArrayOfStrings {
  
  /* return a sorted version of the array returned by listOfStringsToArray */
  return [[self arrayOfStrings] sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
}


@end
