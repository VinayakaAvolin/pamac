//
//  main.m
//  HardwareMonitoring
//
//  Created by Ramachandra Naragund on 18/12/18.
//  Copyright © 2018 Ramachandra Naragund. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
