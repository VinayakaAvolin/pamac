//
//  AppDelegate.h
//  HardwareMonitoring
//
//  Created by Ramachandra Naragund on 18/12/18.
//  Copyright © 2018 Ramachandra Naragund. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>


@end

