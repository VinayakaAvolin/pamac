//
//  AppDelegate.m
//  HardwareMonitoring
//
//  Created by Ramachandra Naragund on 18/12/18.
//  Copyright © 2018 Ramachandra Naragund. All rights reserved.
//

#import "AppDelegate.h"
#import "PAHardwareMonitorServiceProvider.h"

@interface AppDelegate ()

@property (weak) IBOutlet NSWindow *window;
@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application
    
    PAHardwareMonitorServiceProvider * hp = [[PAHardwareMonitorServiceProvider alloc] init];
    
    [hp startMonitoringEvent:nil];
    
    
}


- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}


@end
