//
//  PAHardwareMonitor.h
//  ProactiveAssist
//
//  Created by Ramachandra Naragund on 18/12/18.
//  Copyright © 2018 Aptean. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PAHardwareMonitorEventDelegate.h"
#import "PASelfHealEvent.h"

@interface PAHardwareMonitor : NSObject

-(instancetype) initWithDelegate:(id<PAHardwareMonitorEventDelegate>) delegate forStatus:(PASelfHealEvent *) event;

-(void) startMonitoring;
-(void) stopMonitoring;
@end

