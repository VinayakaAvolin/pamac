//
//  PAHardwareMonitorEventDelegate.h
//  ProactiveAssist
//
//  Created by Ramachandra Naragund on 17/12/18.
//  Copyright © 2018 Aptean. All rights reserved.
//

#ifndef PAHardwareMonitorEventDelegate_h
#define PAHardwareMonitorEventDelegate_h

@class PAHardwareMonitorServiceProvider;
@class PASelfHealEvent;

@protocol PAHardwareMonitorEventDelegate
@required

- (void) hardwareMonitor:(PAHardwareMonitorServiceProvider *)hardwareMonitor eventOccurred:(PASelfHealEvent *)event;

@end


#endif /* PAHardwareMonitorEventDelegate_h */
