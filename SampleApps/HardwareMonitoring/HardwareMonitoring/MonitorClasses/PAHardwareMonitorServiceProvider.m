//
//  PAHardwareMonitorServiceProvider.m
//  PAHardwareEventMonitorService
//
//  Created by Ramachandra Naragund on 17/12/18.
//  Copyright © 2018 Aptean. All rights reserved.
//

#import "PAHardwareMonitorServiceProvider.h"
#import "PAHardwareMonitor.h"

@interface PAHardwareMonitorServiceProvider ()
{
    PAHardwareMonitor * hardwareMonitor;
}
@end

@implementation PAHardwareMonitorServiceProvider

- (BOOL)canHandleCapability:(SelfHealEventClass)capability forEvent:(PASelfHealEvent*)event
{
    return (capability == HardwareMonitoring);
}


- (void) startMonitoringEvent:(PASelfHealEvent*)event
{
    NSLog(@"Wireless monitoring started");
    //TODO: Identify the paths and data to be monitored.
    if (hardwareMonitor == nil) {
        hardwareMonitor = [[PAHardwareMonitor alloc] initWithDelegate:self forStatus:event];
    }
    [hardwareMonitor startMonitoring];
    
}

- (void) stopMonitoringEvent:(PASelfHealEvent*)event
{
    NSLog(@"Wireless monitoring ended");
    [hardwareMonitor stopMonitoring];
    hardwareMonitor = nil;
}

- (void) hardwareMonitor:(PAHardwareMonitorServiceProvider *)hardwareMonitor eventOccurred:(PASelfHealEvent *)event
{
    NSLog(@"Call the self heal trigger class methods");
}


@end
