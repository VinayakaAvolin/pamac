//
//  PASelfHealEvent.h
//  ProactiveAssistAgent
//
//  Created by Ramkumar HN on 05/12/18.
//  Copyright © 2018 Aptean. All rights reserved.
//

#import <Foundation/Foundation.h>

FOUNDATION_EXPORT NSString *const kPASelfHealEventMonitoringTypeKey;
FOUNDATION_EXPORT NSString *const kPASelfHealEventClassKey ;
FOUNDATION_EXPORT NSString *const kPASelfHealEventUserModeKey;
FOUNDATION_EXPORT NSString *const kPASelfHealEventFileMonitorTypeKey;
FOUNDATION_EXPORT NSString *const kPASelfHealEventNetworkMonitorTypeKey;
FOUNDATION_EXPORT NSString *const kPASelfHealEventFirewallStatusKey ;
FOUNDATION_EXPORT NSString *const kPASelfHealEventWirelessStatusKey;
FOUNDATION_EXPORT NSString *const kPASelfHealEventAppleScriptPathKey;
FOUNDATION_EXPORT NSString *const kPASelfHealEventTriggerIntervalKey;
FOUNDATION_EXPORT NSString *const kPASelfHealEventMonitoringTypeAttributesKey;
FOUNDATION_EXPORT NSString *const kPASelfHealEventHardwareEventFilePath;

typedef NS_ENUM(NSInteger, MonitoringType)
{
    NativeMonitoring = 0,
    AppleScriptMonitoring = 1
};
typedef NS_ENUM(NSInteger, SelfHealEventClass)
{
    FileMonitoring = 0,
    PlistMonitoring = 1,
    HardwareMonitoring = 2,
    BatteryLevelMonitoring = 63,
    StorageMonitoring = 4,
    AppiicationMonitoring = 5,
    NetworkMonitoring = 6
};

typedef NS_ENUM(NSInteger, UserMode )
{
    user = 0,
    admin = 1
};

typedef NS_ENUM(NSInteger, EventMonitorType)
{
    ItemCreated = 1,
    ItemModified = 2,
    ItemDeleted = 4
};

typedef NS_ENUM(NSInteger, NetworkMonitorType)
{
    Firewall = 1,
    Wireless = 2
};
typedef NS_ENUM(NSInteger, FirewallStatus)
{
    Enabled = 1,
    Disabled = 2
};
typedef NS_ENUM(NSInteger, WirelessStatus)
{
    Connected = 1,
    Disconnected = 2
};



@interface PASelfHealEvent : NSObject

@property (nonatomic, readonly) NSString *eventGUID;
@property (nonatomic, readonly) NSData *eventData;
@property (nonatomic, readonly) NSDictionary *fieldSet;

- (instancetype)initWithGuid: (NSString*)guid fieldSet: (NSDictionary *)attribs andData: (NSData *)data;

@end
