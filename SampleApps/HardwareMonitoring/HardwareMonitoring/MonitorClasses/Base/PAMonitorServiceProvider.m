//
//  PAMonitorServiceProvider.m
//  BatterySample
//
//  Created by Ramachandra Naragund on 11/12/18.
//  Copyright © 2018 Ramachandra Naragund. All rights reserved.
//

#import "PAMonitorServiceProvider.h"

@implementation PAMonitorServiceProvider

- (BOOL)canHandleCapability:(SelfHealEventClass)capability forEvent:(PASelfHealEvent*)event{
    return false;
}

- (void) startMonitoringEvent:(PASelfHealEvent*)event
{
    
}

- (void) stopMonitoringEvent:(PASelfHealEvent*)event
{
    
}

@end
