//
//  PASelfHealEvent.m
//  ProactiveAssistAgent
//
//  Created by Ramkumar HN on 05/12/18.
//  Copyright © 2018 Aptean. All rights reserved.
//

#import "PASelfHealEvent.h"

NSString *const kPASelfHealEventMonitoringTypeKey = @"MonitoringType";
NSString *const kPASelfHealEventClassKey = @"EventClass";
NSString *const kPASelfHealEventUserModeKey = @"UserMode";
NSString *const kPASelfHealEventFileMonitorTypeKey = @"FileMonitorType";
NSString *const kPASelfHealEventNetworkMonitorTypeKey = @"NetworkMonitorType";
NSString *const kPASelfHealEventFirewallStatusKey = @"FirewallStatus";
NSString *const kPASelfHealEventWirelessStatusKey = @"WirelessStatus";
NSString *const kPASelfHealEventAppleScriptPathKey = @"ScriptPath";
NSString *const kPASelfHealEventTriggerIntervalKey = @"TriggerInterval";
NSString *const kPASelfHealEventMonitoringTypeAttributesKey =  @"MonitoringTypeAttributes";
NSString *const kPASelfHealEventHardwareEventFilePath = @"event_file_path";


@implementation PASelfHealEvent

- (instancetype)initWithGuid:(NSString *)guid fieldSet:(NSDictionary *)attribs andData:(NSData *)data
{
    self = [super init];
    if (self) {
        _eventGUID = [guid copy];
        _fieldSet = [attribs copy];
        _eventData = [data copy];
    }
    return self;
}
@end
