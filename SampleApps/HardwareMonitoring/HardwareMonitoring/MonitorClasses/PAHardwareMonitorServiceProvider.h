//
//  PAHardwareMonitorServiceProvider.h
//  PAHardwareEventMonitorService
//
//  Created by Ramachandra Naragund on 17/12/18.
//  Copyright © 2018 Aptean. All rights reserved.
//

#import "PAMonitorServiceProvider.h"
#import "PAHardwareMonitorEventDelegate.h"


@interface PAHardwareMonitorServiceProvider : PAMonitorServiceProvider <PAHardwareMonitorEventDelegate>

@end
