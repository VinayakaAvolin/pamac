//
//  PAHardwareMonitor.m
//  ProactiveAssist
//
//  Created by Ramachandra Naragund on 18/12/18.
//  Copyright © 2018 Aptean. All rights reserved.
//

#import "PAHardwareMonitor.h"
#import <AppKit/AppKit.h>

#define NOTIFICATION_CENTER [[NSWorkspace sharedWorkspace] notificationCenter]


@interface PAHardwareMonitor ()
@property (nonatomic) id <PAHardwareMonitorEventDelegate> delegate;
@property (nonatomic) PASelfHealEvent * event;
@end
@implementation PAHardwareMonitor

-(instancetype) initWithDelegate:(id<PAHardwareMonitorEventDelegate>) delegate forStatus:(PASelfHealEvent *) event
{
    if (self = [super init]) {
        self.delegate = delegate;
        self.event = event;
    }
    return  self;
}


-(void) startMonitoring
{
    [NOTIFICATION_CENTER addObserver:self
                            selector: @selector(didVolumesUpdate:)
                                name:NSWorkspaceDidMountNotification
                              object: nil];
    
    [NOTIFICATION_CENTER addObserver:self
                            selector: @selector(didVolumesUpdate:)
                                name:NSWorkspaceDidUnmountNotification
                              object:nil];

}

-(void) didVolumesUpdate: (NSNotification*) notification
{
    NSString* path = [notification.userInfo[NSWorkspaceVolumeURLKey] path];
    NSString *eventPath =  @"/Volumes/Extra"; //self.event.fieldSet[kPASelfHealEventHardwareEventFilePath];
    if ([path isEqualToString:eventPath]) {
        [self.delegate hardwareMonitor:nil eventOccurred:self.event];
    }
}

-(void) stopMonitoring
{
    [NOTIFICATION_CENTER removeObserver:NSWorkspaceDidMountNotification];
    [NOTIFICATION_CENTER removeObserver:NSWorkspaceDidUnmountNotification];
    self.delegate = nil;
    self.event = nil;
}

@end
