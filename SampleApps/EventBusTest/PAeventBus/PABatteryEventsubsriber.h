//
//  PABatteryEventsubsriber.h
//  PAeventBus
//
//  Created by Ramkumar HN on 18/12/18.
//  Copyright © 2018 Ramkumar HN. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PAEventSubscribable.h"
@interface PABatteryEventsubsriber : NSObject<PAEventSubscribable>

@end
