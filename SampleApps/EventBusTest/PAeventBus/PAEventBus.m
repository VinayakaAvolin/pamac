//
//  PAEventBus.m
//  PAeventBus
//
//  Created by Ramkumar HN on 18/12/18.
//  Copyright © 2018 Ramkumar HN. All rights reserved.
//

#import "PAEventBus.h"
NSString* const kEventClassNameKey = @"event";
NSString* const kSubscriberObjectKey = @"subscriber";
@interface PAEventBus()

@property (atomic) NSMutableDictionary *subscribers;

@end

@implementation PAEventBus

+ (PAEventBus *)sharedInstance
{
    static PAEventBus *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        _subscribers = [NSMutableDictionary new];
    }
    return self;
}

- (void)registerSubscriber: (id<PAEventSubscribable>)subsciber forEventClass: (SelfHealEventClass)eventClass
{

    NSDictionary *subscriberInfo = @{
                                     kEventClassNameKey: @(eventClass),
                                     kSubscriberObjectKey: subsciber
    };
    self.subscribers[[subsciber objectIdentifier]] = subscriberInfo;
}

- (NSArray<id<PAEventSubscribable>> *)registeredSubscribers
{
    NSMutableArray *mapped = [NSMutableArray arrayWithCapacity:[[self.subscribers allValues] count]];
    [[self.subscribers allValues] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        id mapObj = obj[kSubscriberObjectKey];
        [mapped addObject:mapObj];
    }];
    return mapped;
}

- (void)unregisterSubscriber: (id<PAEventSubscribable>)subscriber
{
    return [self.subscribers removeObjectForKey:[subscriber objectIdentifier]];
}

- (void)postEvent: (id<PAEventType>)event
{
    @synchronized (self) {
        NSArray *allSubscribers = [self.subscribers allValues];
        for (NSDictionary* subscriberInfo in allSubscribers) {
            if ([subscriberInfo[kEventClassNameKey] intValue] == event.eventClass){
                id<PAEventSubscribable> subscriber = (id<PAEventSubscribable>)subscriberInfo[kSubscriberObjectKey];
                if ([subscriber respondsToSelector:@selector(eventNotificationQueue)])
                {
                    NSInvocationOperation *operation = [[NSInvocationOperation alloc]
                                                        initWithTarget:subscriber
                                                        selector:@selector(eventNotificationQueue)
                                                        object:event];
                    [[subscriber eventNotificationQueue] addOperation:operation];
                } else {
                    [subscriber onEvent: event];
                }
            }
        }
    }
}
@end
