//
//  main.m
//  PAeventBus
//
//  Created by Ramkumar HN on 13/12/18.
//  Copyright © 2018 Ramkumar HN. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PAEventBus.h"
#import "PABatteryEvent.h"
#import "PABatteryEventsubsriber.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        
        PAEventBus *eventBus = [PAEventBus sharedInstance];
        PABatteryEventsubsriber *sub1 = [[PABatteryEventsubsriber alloc] init];
        PABatteryEventsubsriber *sub2 = [[PABatteryEventsubsriber alloc] init];

        [eventBus registerSubscriber:sub1 forEventClass:BatteryLevelMonitoring];
        [eventBus registerSubscriber:sub2 forEventClass:BatteryLevelMonitoring];

        // after a while
        NSLog(@"===============Initial setup = %@================", [eventBus registeredSubscribers]);

        
        [eventBus postEvent: [[PABatteryEvent alloc] init]];
        
        [eventBus unregisterSubscriber:sub1];
        //
        NSLog(@"===============After unregistering first = %@================", [eventBus registeredSubscribers]);
        [eventBus postEvent:[[PABatteryEvent alloc] init]];
    
        [eventBus unregisterSubscriber:sub2];
        NSLog(@"===============After unregistering second = %@================", [eventBus registeredSubscribers]);
        [eventBus postEvent:[[PABatteryEvent alloc] init]];

    }
    return 0;
}
