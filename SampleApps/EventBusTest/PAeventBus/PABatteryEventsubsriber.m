//
//  PABatteryEventsubsriber.m
//  PAeventBus
//
//  Created by Ramkumar HN on 18/12/18.
//  Copyright © 2018 Ramkumar HN. All rights reserved.
//

#import "PABatteryEventsubsriber.h"
@interface PABatteryEventsubsriber()
@property (nonatomic) NSUUID *objectId;
@property (nonatomic) NSOperationQueue *queue;

@end

@implementation PABatteryEventsubsriber

- (instancetype)init
{
    self = [super init];
    if (self) {
        _objectId = [NSUUID new];
        _queue = [[NSOperationQueue alloc] init];
    }
    return self;
}

- (NSUUID *)objectIdentifier {
    return self.objectId;
}
- (void)onEvent:(id<PAEventType>)event {
    NSLog(@"Battery event triggered. I am handling it(@%@)", event.eventGuid);
}

@end
