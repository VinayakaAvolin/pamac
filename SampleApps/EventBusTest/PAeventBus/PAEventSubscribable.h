//
//  PAEventSubscribable.h
//  PAeventBus
//
//  Created by Ramkumar HN on 18/12/18.
//  Copyright © 2018 Ramkumar HN. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PAEventType.h"

@protocol PAEventSubscribable <NSObject>

@required
- (NSUUID *)objectIdentifier;
- (void)onEvent:(id<PAEventType>)event;
@optional
- (NSOperationQueue *)eventNotificationQueue;

@end

