//
//  PAEventBus.h
//  PAeventBus
//
//  Created by Ramkumar HN on 18/12/18.
//  Copyright © 2018 Ramkumar HN. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PAEventType.h"
#import "PAEventSubscribable.h"

@interface PAEventBus : NSObject

+ (PAEventBus *)sharedInstance;
- (void)registerSubscriber: (id<PAEventSubscribable>)subsciber forEventClass: (SelfHealEventClass)eventClass;
- (NSArray<id<PAEventSubscribable>> *)registeredSubscribers;
- (void)unregisterSubscriber: (id<PAEventSubscribable>)subscriber;
- (void)postEvent: (id<PAEventType>)event;

@end
