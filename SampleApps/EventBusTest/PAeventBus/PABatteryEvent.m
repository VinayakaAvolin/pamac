//
//  PABatteryEvent.m
//  PAeventBus
//
//  Created by Ramkumar HN on 18/12/18.
//  Copyright © 2018 Ramkumar HN. All rights reserved.
//

#import "PABatteryEvent.h"

@implementation PABatteryEvent

- (NSString *)eventGuid {
    return @"0a1a61dc-c42c-48eb-857f-825a3a887dc3";
}// Should be unique

- (SelfHealEventClass)eventClass{
    return BatteryLevelMonitoring;
}
@end
