//
//  PAEventType.h
//  PAeventBus
//
//  Created by Ramkumar HN on 18/12/18.
//  Copyright © 2018 Ramkumar HN. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef NS_ENUM(NSInteger, SelfHealEventClass)
{
    FileMonitoring = 0,
    PlistMonitoring = 1,
    HardwareMonitoring = 2,
    BatteryLevelMonitoring = 63,
    StorageMonitoring = 4,
    AppiicationMonitoring = 5,
    NetworkMonitoring = 6
};


@protocol PAEventType <NSObject>
@required
- (NSString *)eventGuid; // Should be unique
- (SelfHealEventClass)eventClass;
@end

