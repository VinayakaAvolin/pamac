//
//  PABatteryMonitorServiceProvide.m
//  PAFileMonitorService
//
//  Created by Ramachandra Naragund on 10/12/18.
//  Copyright © 2018 Aptean. All rights reserved.
//

#import "PABatteryMonitorServiceProvider.h"
#import "PABatteryMonitor.h"

@interface PABatteryMonitorServiceProvider ()
{
    PABatteryMonitor * batteryMonitor;
}
@end

@implementation PABatteryMonitorServiceProvider


- (BOOL)canHandleCapability:(SelfHealEventClass)capability forEvent:(PASelfHealEvent*)event
{
    return capability == BatteryLevelMonitoring;
}

- (void) startMonitoringEvent:(PASelfHealEvent*)event
{
    NSLog(@"Event monitoring started");
    //TODO: Identify the paths and data to be monitored.
    
    if (batteryMonitor == nil) {
        batteryMonitor = [[PABatteryMonitor alloc] initWithDelegate:self forBatteryLevelsTobeMonitored:@[@86,@30]];
    }
    [batteryMonitor startMonitoring];
    
}

- (void) stopMonitoringEvent:(PASelfHealEvent*)event
{
    NSLog(@"Event monitoring ended");
    if (batteryMonitor) {
        [batteryMonitor stopMonitoring];
    }
    batteryMonitor = nil;
}

- (void)batteryMonitor:(PABatteryMonitorServiceProvider *)batteryMonitor eventOccurred:(PASelfHealEvent *)event
{
    NSLog(@"Call the self heal trigger class methods");
}

@end
