//
//  PABatteryMonitor.h
//  PAFileMonitorService
//
//  Created by Ramachandra Naragund on 10/12/18.
//  Copyright © 2018 Aptean. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PABatteryMonitorEventDelegate.h"

@interface PABatteryMonitor : NSObject

-(instancetype) initWithDelegate:(id<PABatteryMonitorEventDelegate>) delegate forBatteryLevelsTobeMonitored:(NSArray *) batteryLevel;

-(void) startMonitoring;

-(void) stopMonitoring;
@end
