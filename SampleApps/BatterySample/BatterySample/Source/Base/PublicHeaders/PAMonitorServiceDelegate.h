//
//  PAMonitorServiceDelegate.h
//  PAMonitorService
//
//  Created by Vijayaraja H A on 25/09/18.
//  Copyright © 2018 Aptean. All rights reserved.
//

#ifndef PAMonitorServiceDelegate_h
#define PAMonitorServiceDelegate_h

@class PAMonitorServiceProvider;
@class PAMonitorEvent;
@protocol PAMonitorServiceDelegate
@required

- (void)serviceProvider:(PAMonitorServiceProvider *)serviceProvider eventOccurred:(PAMonitorEvent *)event;
@end
#endif /* PAMonitorServiceDelegate_h */
