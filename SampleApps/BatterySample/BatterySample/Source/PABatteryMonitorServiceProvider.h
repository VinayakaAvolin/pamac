//
//  PABatteryMonitorServiceProvide.h
//  PAFileMonitorService
//
//  Created by Ramachandra Naragund on 10/12/18.
//  Copyright © 2018 Aptean. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PAMonitorServiceProvider.h"
#import "PABatteryMonitorEventDelegate.h"

@interface PABatteryMonitorServiceProvider : PAMonitorServiceProvider <PABatteryMonitorEventDelegate>

@end

