//
//  PABatteryMonitorEventDelegate.h
//  ProactiveAssist
//
//  Created by Ramachandra Naragund on 10/12/18.
//  Copyright © 2018 Aptean. All rights reserved.
//

#ifndef PABatteryMonitorEventDelegate_h
#define PABatteryMonitorEventDelegate_h

@class PABatteryMonitorServiceProvider;
@class PASelfHealEvent;

@protocol PABatteryMonitorEventDelegate
@required

- (void)batteryMonitor:(PABatteryMonitorServiceProvider *)batteryMonitor eventOccurred:(PASelfHealEvent *)event;
@end

#endif /* PABatteryMonitorEventDelegate_h */
