//
//  PABatteryMonitor.m
//  PAFileMonitorService
//
//  Created by Ramachandra Naragund on 10/12/18.
//  Copyright © 2018 Aptean. All rights reserved.
//

#import "PABatteryMonitor.h"
#include <IOKit/ps/IOPowerSources.h>

static CFRunLoopSourceRef batteryRunLoopSrcRef = NULL;


@interface PABatteryMonitor ()
{
    id<PABatteryMonitorEventDelegate> _delegate;
    NSMutableArray * batteryLevelsToMonitor;
}
@end

@implementation PABatteryMonitor


static void powerSourceCallback(void * object)
{
    CFTypeRef    blob = IOPSCopyPowerSourcesInfo();
    CFArrayRef   batterySources = IOPSCopyPowerSourcesList(blob);
    unsigned    resourceCount = (unsigned) CFArrayGetCount(batterySources);
    unsigned int index;
    for (index = 0U; index < resourceCount; ++index)
    {
        CFTypeRef        powerSource;
        CFDictionaryRef description;
        powerSource = CFArrayGetValueAtIndex(batterySources, index);
        description = IOPSGetPowerSourceDescription(blob, powerSource);
        [(__bridge PABatteryMonitor *)object updateBatteryInfo:(__bridge NSDictionary *)description];
    }
    CFRelease(blob);
    CFRelease(batterySources);
}


-(instancetype) initWithDelegate:(id<PABatteryMonitorEventDelegate>) delegate forBatteryLevelsTobeMonitored:(NSArray *) batteryLevel
{
    if (self = [super init]) {
        _delegate = delegate;
        batteryLevelsToMonitor = [NSMutableArray arrayWithArray:batteryLevel];
    }
    return self;
}

-(void) startMonitoring
{
    [self subscribeToBatteryChange];
}

-(void) stopMonitoring
{
    CFRunLoopRemoveSource(CFRunLoopGetCurrent(), batteryRunLoopSrcRef, kCFRunLoopDefaultMode);
    CFRelease(batteryRunLoopSrcRef);
}

-(void) subscribeToBatteryChange {
    batteryRunLoopSrcRef = IOPSNotificationCreateRunLoopSource(powerSourceCallback,(__bridge void *)(self));
    if (batteryRunLoopSrcRef) {
        CFRunLoopAddSource(CFRunLoopGetCurrent(), batteryRunLoopSrcRef, kCFRunLoopDefaultMode);
    }
}


-(void) updateBatteryInfo:(NSDictionary *) batteryInfo {
    // parse the battery info and trigger the event based on the battery percentage dips on certain levels (batteryLevelsToMonitor)
    if (batteryInfo && batteryInfo.allKeys.count) {
        NSNumber * batteryCapacity = [batteryInfo objectForKey:@"Current Capacity"];
        if ([batteryLevelsToMonitor containsObject:batteryCapacity]) {
            [_delegate batteryMonitor:nil eventOccurred:nil];
        }
    }
    
}

@end
