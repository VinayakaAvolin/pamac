//
//  AppDelegate.m
//  BatterySample
//
//  Created by Ramachandra Naragund on 11/12/18.
//  Copyright © 2018 Ramachandra Naragund. All rights reserved.
//

#import "AppDelegate.h"
#import "PABatteryMonitorServiceProvider.h"

@interface AppDelegate ()

@property (weak) IBOutlet NSWindow *window;
@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application
    PABatteryMonitorServiceProvider * sp = [[PABatteryMonitorServiceProvider alloc] init];
    [sp startMonitoringEvent:nil];
}


- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}


@end
