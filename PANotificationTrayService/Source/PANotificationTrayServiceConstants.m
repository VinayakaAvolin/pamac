//
//  PANotificationTrayServiceConstants.m
//  PANotificationTrayService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PANotificationTrayServiceConstants.h"

NSString *const kPANotificationNameNewEvent = @"PANotificationNewEvent";

NSString *const kPANotificationObjectName = @"PANotificationTrayDistributedNotification";

NSString *const kPANotifTrayServiceEventTitleKey = @"event_title";
NSString *const kPANotifTrayServiceEventMessageKey = @"event_message";
NSString *const kPANotifTrayServiceEventDetailedMessageKey = @"event_detailedMessage";
NSString *const kPANotifTrayServiceEventIdKey = @"event_id";
