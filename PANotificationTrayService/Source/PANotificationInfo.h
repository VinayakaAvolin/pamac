//
//  PANotificationInfo.h
//  PANotificationTrayService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PANotificationInfo : NSObject

@property(nonatomic, readonly) NSString *identifier;
@property(nonatomic, readonly) NSString *title;
@property(nonatomic, readonly) NSString *message;
@property(nonatomic, readonly) NSString *detailedMessage;

/// Returns the dictionary constructed from class properties
@property(nonatomic, readonly) NSDictionary *dictionaryRepresentation;

- (instancetype)initWithTitle:(NSString *)title
                     messsage:(NSString *)message
             detailedMesssage:(NSString *)detailedMesssage;

@end
