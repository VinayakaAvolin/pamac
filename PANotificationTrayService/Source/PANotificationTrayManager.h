//
//  PANotificationTrayManager.h
//  PANotificationTrayService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PANotificationInfo.h"

@interface PANotificationTrayManager : NSObject

+ (void)notifyTrayAppWithInfo:(PANotificationInfo *)notificationInfo;

@end
