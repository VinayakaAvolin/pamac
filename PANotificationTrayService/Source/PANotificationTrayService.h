//
//  PANotificationTrayService.h
//  PANotificationTrayService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Cocoa/Cocoa.h>

//! Project version number for PANotificationTrayService.
FOUNDATION_EXPORT double PANotificationTrayServiceVersionNumber;

//! Project version string for PANotificationTrayService.
FOUNDATION_EXPORT const unsigned char PANotificationTrayServiceVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <PANotificationTrayService/PublicHeader.h>

#import <PANotificationTrayService/PANotificationTrayManager.h>
#import <PANotificationTrayService/PANotificationTrayServiceConstants.h>
