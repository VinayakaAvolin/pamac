//
//  PANotificationTrayManager.m
//  PANotificationTrayService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <PAScriptRunnerService/PAScriptRunnerService.h>
#import <PAFileSystemService/PAFileSystemService.h>
#import <PAConfigurationService/PAConfigurationService.h>
#import "PANotificationTrayManager.h"
#import "PANotificationTrayServiceConstants.h"

NSString *const kPANotifTrayServiceGetAllProcessesScript = @"do shell script \"ps -awwx\"";
NSString *const kPANotifTrayAppName = @"PANotificationTray.app";

typedef void(^PANotificationTrayManagerCallback)(BOOL success);

@implementation PANotificationTrayManager

+ (void)notifyTrayAppWithInfo:(PANotificationInfo *)notificationInfo {
  PANotificationTrayManager *manager = [[PANotificationTrayManager alloc]init];
  
  /// launch the app; if not active already
  [manager checkWhetherNotificationTrayAppLaunchedWithCallback:^(BOOL success) {
    /// app not launched
    if(!success) {
      [manager launchNotificationTrayApp];
    }
    // notify
    [[NSDistributedNotificationCenter defaultCenter] postNotificationName:kPANotificationNameNewEvent object:kPANotificationObjectName userInfo:notificationInfo.dictionaryRepresentation deliverImmediately:true];
  }];
}

- (void)checkWhetherNotificationTrayAppLaunchedWithCallback:(PANotificationTrayManagerCallback)callback {
  PAAppConfiguration *appConfiguration = [PAConfigurationManager appConfigurations];
  PAFileSystemManager *fileSystemManager = [[PAFileSystemManager alloc]init];
  PAAppleScript *script = [[PAAppleScript alloc]initWithScriptString:kPANotifTrayServiceGetAllProcessesScript];
  [script runWithCallBack:^(BOOL success, NSError *error, id result) {
    BOOL isAppLaunched = false;
    if (success && [result isKindOfClass:[NSString class]]) {
      isAppLaunched = [(NSString *)result containsString:[fileSystemManager stringByExpandingPath:appConfiguration.notificationTrayExecutablePath]];
    }
    callback(isAppLaunched);
  }];
}

- (BOOL)launchNotificationTrayApp {
  return [[NSWorkspace sharedWorkspace] launchApplication:kPANotifTrayAppName];
}

@end
