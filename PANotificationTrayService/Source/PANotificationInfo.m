//
//  PANotificationInfo.m
//  PANotificationTrayService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PANotificationInfo.h"
#import "NSString+Additions.h"
#import "PANotificationTrayServiceConstants.h"

@implementation PANotificationInfo

- (instancetype)initWithTitle:(NSString *)title
                     messsage:(NSString *)message
             detailedMesssage:(NSString *)detailedMesssage {
  
  self = [super init];
  if (self) {
    _identifier = [NSString uuid];
    _title = title;
    _message = message;
    _detailedMessage = detailedMesssage;
  }
  return self;
}

- (NSDictionary *)dictionaryRepresentation {
  NSMutableDictionary *dict = [NSMutableDictionary dictionary];
  [dict setValue:_title forKey:kPANotifTrayServiceEventTitleKey];
  [dict setValue:_message forKey:kPANotifTrayServiceEventMessageKey];
  [dict setValue:_detailedMessage forKey:kPANotifTrayServiceEventDetailedMessageKey];
  [dict setValue:_identifier forKey:kPANotifTrayServiceEventIdKey];
  return [NSDictionary dictionaryWithDictionary:dict];
}
@end
