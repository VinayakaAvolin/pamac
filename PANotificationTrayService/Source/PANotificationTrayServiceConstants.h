//
//  PANotificationTrayServiceConstants.h
//  PANotificationTrayService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifndef PANotificationTrayServiceConstants_h
#define PANotificationTrayServiceConstants_h

// notification name constants
extern NSString *const kPANotificationNameNewEvent;

// notification object name constants
extern NSString *const kPANotificationObjectName;

// notification info keys
extern NSString *const kPANotifTrayServiceEventIdKey;
extern NSString *const kPANotifTrayServiceEventTitleKey;
extern NSString *const kPANotifTrayServiceEventMessageKey;
extern NSString *const kPANotifTrayServiceEventDetailedMessageKey;

#endif /* PANotificationTrayServiceConstants_h */
