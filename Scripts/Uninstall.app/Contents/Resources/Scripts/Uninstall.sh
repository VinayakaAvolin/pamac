#! /bin/sh
# <codex>
# <abstract>Script to remove everything installed by the sample.</abstract>
# </codex>
# This uninstalls everything installed by the ProactiveAssist.

# Updated by Vinayaka Sathyanarayana


# Unload PA login agent "com.aptean.ProactiveAssistAgent"
UnloadLoginAgent()
{
  # Kill "S2C3RBBETE.com.aptean.supportsoft.app" process
  if [ -e "/Applications/ProactiveAssist.app/Contents/Library/LoginItems/S2C3RBBETE.com.aptean.supportsoft.app" ]; then
    users=$(who | cut -d' ' -f1 | sort | uniq | grep -v _mbsetupuser)

    for user in $users
    do
      sudo -H -u $user launchctl unload /Library/LaunchAgents/com.aptean.proactiveassistagent.plist
      echo "Unload 'S2C3RBBETE.com.aptean.supportsoft' process - Success"
    done
  fi

  # Clear "com.aptean.proactiveassistagent.plist" file
  if [ -e "/Library/LaunchAgents/com.aptean.proactiveassistagent.plist" ]; then
    sudo rm -f /Library/LaunchAgents/com.aptean.proactiveassistagent.plist
    echo "Removed 'com.aptean.proactiveassistagent.plist' file"
  fi

  # Clear "com.aptean.proactiveassistagent_shell.plist" file
  if [ -e "/Library/LaunchAgents/com.aptean.proactiveassistagent_shell.plist" ]; then
    sudo rm -f /Library/LaunchAgents/com.aptean.proactiveassistagent_shell.plist
    echo "Removed 'com.aptean.proactiveassistagent_shell.plist' file"
  fi
}

# Unload PA Privileged Tool "PAPrivilegedTool
UnloadPrivelegedTool()
{
  # Kill "PAPrivilegedTool" process
  if pgrep -x "PAPrivilegedTool" > /dev/null
  then
    sudo launchctl stop com.aptean.PrivilegedTool
    echo "Stop 'com.aptean.PrivilegedTool' process - Success"
    sudo launchctl unload /Library/LaunchDaemons/com.aptean.PrivilegedTool.plist
    echo "Unload 'com.aptean.PrivilegedTool' process - Success"
  fi

  # Clear "com.aptean.PrivilegedTool.plist" file
  if [ -e "/Library/LaunchDaemons/com.aptean.PrivilegedTool.plist" ]; then
    sudo rm -f /Library/LaunchDaemons/com.aptean.PrivilegedTool.plist
    echo "Removed 'com.aptean.PrivilegedTool.plist' file"
  fi
}

# Removes App directory from all user profiles
RemoveAppDirectoryFromAllUsers()
{
  users=$(ls -1t /users | grep -v Shared)

  for user in $users
  do
      sudo rm -rf /Users/$user/Library/Application\ Support/ProactiveAssist
      echo "Removed ProactiveAssist App directory from user : $user"
  done
}

# Kill "Proactive Assist" App
CheckAndKillProactiveAssistApp()
{
  if [ -d /Applications/ProactiveAssist.app ]; then
    # Removes ProactiveAssist package from MacOS Installer
    sudo rm -R /Applications/ProactiveAssist.app
    echo "Removed ProactiveAssist App application from Application directory"
    # pkgutil is a different utility used to query and manipulate macOS Installer packages and receipts.
    sudo pkgutil --forget com.aptean.pkg.ProactiveAssist
    echo "Removed ProactiveAssist App from pkgutil"
  fi

  if [ -d /Library/Application\ Support/ProactiveAssist ]; then
    sudo rm -R /Library/Application\ Support/ProactiveAssist
    echo "Removed ProactiveAssist App directory from system"
  fi
}

# Kill "ProactiveAssist" App - if launched
CheckAndKillProactiveAssistLaunched()
{
  proc_name='ProactiveAssist'
  if pgrep -x $proc_name > /dev/null
  then
    sudo pkill -9 $proc_name
    echo "Killed ProactiveAssist app"
  fi
}

# Kill "Notification Tray" App - if launched
CheckAndKillNotificationTrayLaunched()
{
  proc_name='PANotificationTray'
  if pgrep -x $proc_name > /dev/null
  then
    sudo pkill -9 $proc_name
    echo "Killed PANotificationTray app"
  fi
}

# Kill "Alert Tray" App - if launched
CheckAndKillAlertTrayLaunched()
{
  proc_name='PAAlertTray'
  if pgrep -x $proc_name > /dev/null
  then
    sudo pkill -9 $proc_name
    echo "Killed PAAlertTray app"
  fi
}

# Kill "Login Agent" App - if launched
CheckAndKillLoginAgentAndPrivelegedTool()
{
  proc_name='S2C3RBBETE.com.aptean.supportsoft'
  if pgrep -x $proc_name > /dev/null
  then
    sudo pkill -9 $proc_name
    echo "Killed S2C3RBBETE.com.aptean.supportsoft app"
  fi

  proc_name='PAPrivilegedTool'
  if pgrep -x $proc_name > /dev/null
  then
    sudo pkill -9 $proc_name
    echo "Killed PAPrivilegedTool app"
  fi
}


#Unloads Login Agent service and removes 'ProActiveAssist' user data folder from all users
UnloadLoginAgent

# Stop 'com.aptean.PrivilegedTool' daemon service and unload
UnloadPrivelegedTool

# Kill ProactiveAssist.app and Prepackaged directories
CheckAndKillProactiveAssistApp
RemoveAppDirectoryFromAllUsers

# Check ProactiveAssist, Notification Tray and Alert Tray. If Yes then kill the process.
CheckAndKillProactiveAssistLaunched
CheckAndKillNotificationTrayLaunched
CheckAndKillAlertTrayLaunched

# Check Login Agent process. If Yes then kill the process.
CheckAndKillLoginAgentAndPrivelegedTool
