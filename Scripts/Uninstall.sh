#! /bin/sh

# <codex>
# <abstract>Script to remove everything installed by the sample.</abstract>
# </codex>

# This uninstalls everything installed by the sample.  It's useful when testing to ensure that 
# you start from scratch.

UnloadLoginAgent()
{
    if [ -e "/Applications/ProactiveAssist.app/Contents/Library/LoginItems/S2C3RBBETE.com.aptean.supportsoft.app" ]; then

        users=$(who | cut -d' ' -f1 | sort | uniq | grep -v _mbsetupuser)

        for user in $users
        do
            sudo -H -u $user launchctl unload /Library/LaunchAgents/com.aptean.supportsoft.plist
        done
    fi
}
RemoveAppFoldersFromAllUsers()
{
    users=$(ls -1t /users | grep -v Shared)

    for user in $users
    do
        sudo rm -R /Users/$user/Library/Application\ Support/ProactiveAssist
    done
}

UnloadLoginAgent
RemoveAppFoldersFromAllUsers

sudo launchctl stop com.aptean.PrivilegedTool
sudo launchctl unload /Library/LaunchDaemons/com.aptean.PrivilegedTool.plist


#sudo launchctl stop com.aptean.PANotificationTray
#sudo launchctl stop com.aptean.PAAlertTray


sudo rm -f /Library/LaunchDaemons/com.aptean.PrivilegedTool.plist
sudo rm -f /Library/LaunchAgents/com.aptean.supportsoft.plist
sudo rm -R /Applications/ProactiveAssist.app
sudo rm -R /Library/Application\ Support/ProactiveAssist

sudo pkgutil --forget com.aptean.pkg.ProactiveAssist
