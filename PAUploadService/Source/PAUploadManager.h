//
//  PAUploadManager.h
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

typedef void(^PAUploadManagerCompletionBlock)(BOOL success, NSError *error, NSString *fileUploaded);

@interface PAUploadManager : NSObject

+(instancetype)uploadManagerGetInstance;

- (void)uploadFile:(NSString*)filePath targetFolder:(NSString*)folderName uploadURL:(NSString*)urlStr parameters:(NSDictionary*)parameters callback:(PAUploadManagerCompletionBlock)callback;
@end
