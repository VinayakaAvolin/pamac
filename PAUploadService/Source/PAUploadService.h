//
//  PAUploadService.h
//  PAUploadService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Cocoa/Cocoa.h>

//! Project version number for PAUploadService.
FOUNDATION_EXPORT double PAUploadServiceVersionNumber;

//! Project version string for PAUploadService.
FOUNDATION_EXPORT const unsigned char PAUploadServiceVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <PAUploadService/PublicHeader.h>

#import <PAUploadService/PAUploadManager.h>

