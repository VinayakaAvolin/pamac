//
//  PAUploadManager.m
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PAUploadManager.h"


@interface PAUploadManager ()  <NSURLSessionDelegate, NSURLSessionTaskDelegate>
{
  NSURLSession *_urlSession;
  //NSMutableArray *_tasks;
  NSMutableDictionary *_tasks;
  PAUploadManagerCompletionBlock _responseCallback;
  NSString *_fileToUpload;
}

@end

@implementation PAUploadManager


+(instancetype)uploadManagerGetInstance {
  static dispatch_once_t pred;
  static PAUploadManager *_uploadManagerInstance;
  dispatch_once(&pred, ^{
    _uploadManagerInstance = [[self alloc] init];
  });
  return _uploadManagerInstance;
}

- (instancetype)init
{
  self = [super init];
  if (self) {
    _tasks = [NSMutableDictionary new];
    NSURLSessionConfiguration *conf = [NSURLSessionConfiguration backgroundSessionConfigurationWithIdentifier:@"uploader"];
    conf.allowsCellularAccess = NO;
    _urlSession = [NSURLSession sessionWithConfiguration:conf delegate:self delegateQueue:[NSOperationQueue mainQueue]];
  }
  return self;
}


- (void)uploadFile:(NSString*)filePath
      targetFolder:(NSString*)folderName
         uploadURL:(NSString*)urlStr
        parameters:(NSDictionary*)parameters
          callback:(PAUploadManagerCompletionBlock)callback {
  _responseCallback = callback;
  _fileToUpload = filePath;
  
  NSString *boundary = [self generateBoundaryString];
  urlStr = [urlStr stringByAppendingString:@"?"];
  // configure the request
  if (parameters) {
    NSInteger count = 0;
    for (NSString* parameterKey in parameters.allKeys) {
      urlStr = [urlStr stringByAppendingString:[NSString stringWithFormat: @"%@=%@",parameterKey,parameters[parameterKey]]];
      if (count < parameters.allKeys.count -1) {
        urlStr = [urlStr stringByAppendingString:@"&"];
      }
      count++;
    }
    
  }
  
  
  NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlStr]];
  [request setHTTPMethod:@"POST"];
  
  // set content type
  
  NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
  [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
  
  // create body
  NSData *httpBody = [self createBodyWithBoundary:boundary folder:folderName path:filePath parameters:parameters];
  request.HTTPBody =httpBody;
  NSURLSessionTask *task = [_urlSession uploadTaskWithRequest:request fromData:nil];
  //[_tasks addObject:task];
  [_tasks setObject:filePath forKey:task];
  [task resume];
  
  
}

- (NSString *)generateBoundaryString {
  return [NSString stringWithFormat:@"Boundary-%@", [[NSUUID UUID] UUIDString]];
}

- (NSData *)createBodyWithBoundary:(NSString *)boundary
                            folder:(NSString *)folderName
                              path:(NSString *)path
                        parameters:(NSDictionary*)parameters{
  NSMutableData *httpBody = [NSMutableData data];
  
  NSData   *data      = [NSData dataWithContentsOfFile:path];
  
  [httpBody appendData:data];
  return httpBody;
}

#pragma mark -
- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task
didCompleteWithError:(NSError *)error
{
  //    NSLog(@"Finished uploading task %zu %@: %@ %@, HTTP %ld", (unsigned long)[task taskIdentifier], task.originalRequest.URL, error ?: @"Success", task.response, (long)[(id)task.response statusCode]);
  NSString *fileUploaded = [_tasks objectForKey:task];
  [_tasks removeObjectForKey:task];
  //[_tasks removeObject:task];
  if (error) {
    _responseCallback(false, error, _fileToUpload);
  } else {
    _responseCallback(true, nil, _fileToUpload);
  }
}

- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask
    didReceiveData:(NSData *)data
{
  //    NSLog(@"Response:: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
}

- (void)URLSession:(NSURLSession *)session didBecomeInvalidWithError:(NSError *)error
{
  //    NSLog(@"sadface :( %@", error);
  //_responseCallback(false, error, @"");
}


- (void)URLSessionDidFinishEventsForBackgroundURLSession:(NSURLSession *)session
{
  //    NSLog(@"finihed events for bg session");
}

@end
