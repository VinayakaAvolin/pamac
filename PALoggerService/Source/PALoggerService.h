//
//  PALoggerService.h
//  PALoggerService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Cocoa/Cocoa.h>

//! Project version number for PALoggerService.
FOUNDATION_EXPORT double PALoggerServiceVersionNumber;

//! Project version string for PALoggerService.
FOUNDATION_EXPORT const unsigned char PALoggerServiceVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <PALoggerService/PublicHeader.h>

#import <PALoggerService/PALogger.h>
#import <PALoggerService/PALogConstants.h>
#import <PALoggerService/PALogConfiguration.h>
#import <PALoggerService/PALogInfo.h>
