//
//  PALogConfiguration.m
//  PALoggerService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <PAConfigurationService/PAConfigurationService.h>
#import <PAFileSystemService/PAFileSystemService.h>

#import "PALogConfiguration.h"

#define MAX_FILES_COUNT 5

@interface PALogConfiguration () {
  PAFileSystemManager *_fsManager;
}

@end

@implementation PALogConfiguration

- (instancetype)init
{
  return [self initWithLogLevel:PALogLevelNone client:PALogClientNone];
}
- (instancetype)initWithLogLevel:(PALogLevel)logLevel
                          client:(PALogClient)client {
  
  self = [super init];
  if (self) {
    _logClient = client;
    if (client == PALogClientNone) {
      _logClient = PALogClientResidentAgent;
    }
    
    _logLevel = logLevel;
    // set default log level if log level not set
    [self setDefaultLogLevelIfRequired];
    
    
    _fsManager = [[PAFileSystemManager alloc]init];
  }
  return self;
}

- (void)setDefaultLogLevelIfRequired {
  PAAppConfiguration *appconfigurations = [PAConfigurationManager appConfigurations];
  NSUInteger logLevel = 0;
  switch (_logClient) {
    case PALogClientAppUi:
      logLevel = appconfigurations.paUiLogConfiguration.logLevel;
      break;
    case PALogClientResidentAgent:
      logLevel = appconfigurations.agentLogConfiguration.logLevel;
      break;
    default:
      break;
  }
  // if log level not set; read from config plist
  PALogLevel defaultLogLevel = (logLevel > 0) ? logLevel : PALogLevelDebug;
  if (_logLevel <= 0) {
    _logLevel = defaultLogLevel;
  }
}

- (NSString *)logFilePath {
  BOOL doesFileExist = false;
  NSString *logFileDirectory = [_fsManager logsFileDirectoryPath];
  NSString *logFilePath = [logFileDirectory stringByAppendingPathComponent:[self logFileName]];
  
  
  doesFileExist = [_fsManager doesFileExist:logFilePath];
  if(doesFileExist) {
    PAItemAttributes* attributes = [_fsManager attributesOfItemAtPath:logFilePath];
    if (attributes.size > 2000000) {
      NSArray* files = [_fsManager filesOfDirectory:[_fsManager logsFileDirectoryPath]];
      NSMutableArray* backupFiles = [NSMutableArray array];
      for (NSString* fileName in files) {
        if ([fileName hasPrefix:[NSString stringWithFormat:@"%@.",[self logName]]] && ![fileName isEqualToString:[self logFileName]]) {
          [backupFiles addObject:fileName];
        }
      }
      [backupFiles sortUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        return [obj1 compare:obj2 options:NSCaseInsensitiveSearch];
      }];
      if (backupFiles.count == 0) {
        NSString*  newName = [NSString stringWithFormat:@"%@.1%@",[logFileDirectory stringByAppendingPathComponent:[self logName]],@".log"];
        [_fsManager renameFile:logFilePath newFileName:[newName lastPathComponent]];
        [_fsManager createFileAtPath:logFilePath attributes:nil error:nil];
        return logFilePath;
      }else {
        NSString *nextFileName = @"";
        if(backupFiles.count == MAX_FILES_COUNT) {
          nextFileName = [logFileDirectory stringByAppendingPathComponent:backupFiles[backupFiles.count -1]];
          [_fsManager deleteFile:nextFileName];
          [backupFiles removeObject:backupFiles[backupFiles.count -1]];
        } else {
          nextFileName = [NSString stringWithFormat:@"%@.%@%@",[logFileDirectory stringByAppendingPathComponent:[self logName]],@(backupFiles.count + 1),@".log"];
        }
        for (NSInteger index = backupFiles.count -1; index >= 0 ; index--) {
          NSString* fileToRename = [logFileDirectory stringByAppendingPathComponent:backupFiles[index]];
          [_fsManager renameFile:fileToRename newFileName:[nextFileName lastPathComponent]];
          nextFileName = fileToRename;
        }
        [_fsManager renameFile:logFilePath newFileName:[nextFileName lastPathComponent]];
        [_fsManager createFileAtPath:logFilePath attributes:nil error:nil];
        return logFilePath;
        
      }
    }
  }
  if (!doesFileExist) {
    if ([_fsManager createFileAtPath:logFilePath attributes:nil error:nil]) {
      doesFileExist = true;
    }
  }
  if (doesFileExist) {
    return logFilePath;
  }
  return nil;
}

- (NSString *)logLevelString {
  NSString *loglevelString = nil;
  if (NSNotFound != _logLevel) {
    NSArray *cmdArray = [[NSArray alloc] initWithObjects:kPALogLevelStringArray];
    loglevelString = [cmdArray objectAtIndex:_logLevel];
  }
  
  return loglevelString;
}


- (NSString *)logFileName {
  NSString *logFileName = kPADefaultLogFileName;
  switch (_logClient) {
    case PALogClientAppUi:
      logFileName = @"app.log";
      break;
    case PALogClientResidentAgent:
      logFileName = @"agent.log";
      break;
    case PALogClientSyncManager:
      logFileName = @"sync.log";
      break;
    case PALogClientAlertTray:
      logFileName = @"alertTray.log";
      break;
    case PALogClientNotificationTray:
      logFileName = @"notificationTray.log";
      break;
    case PALogClientEvent:
      logFileName = @"event.log";
      break;
    case PALogClientSelfHeal:
      logFileName = @"selfHeal.log";
      break;
    default:
      break;
  }
  return logFileName;
}

- (NSString *)logName {
  NSString *logName = kPADefaultLogFileName;
  switch (_logClient) {
    case PALogClientAppUi:
      logName = @"app";
      break;
    case PALogClientResidentAgent:
      logName = @"agent";
      break;
    case PALogClientSyncManager:
      logName = @"sync";
      break;
    case PALogClientAlertTray:
      logName = @"alertTray";
      break;
    case PALogClientNotificationTray:
      logName = @"notificationTray";
      break;
    case PALogClientEvent:
      logName = @"event";
      break;
    case PALogClientSelfHeal:
      logName = @"selfHeal";
      break;
    default:
      break;
  }
  return logName;
}

@end
