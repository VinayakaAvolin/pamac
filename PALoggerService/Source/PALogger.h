//
//  PALogger.h
//  PALoggerService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PALogConfiguration.h"
#import "PALogInfo.h"

@interface PALogger : NSObject

+ (id)sharedLoggerForClient:(PALogClient)logClient;

- (void)setLogConfiguration:(PALogConfiguration *)configuration;

- (void)debugWithInfo:(PALogInfo *)logInfo andText:(NSString *)format, ...;
- (void)infoWithInfo:(PALogInfo *)logInfo andText:(NSString *)format, ...;
- (void)warningWithInfo:(PALogInfo *)logInfo andText:(NSString *)format, ...;
- (void)errorWithInfo:(PALogInfo *)logInfo andText:(NSString *)format, ...;

- (void)debug:(NSString *)format, ...;
- (void)info:(NSString *)format, ...;
- (void)warning:(NSString *)format, ...;
- (void)error:(NSString *)format, ...;

- (void)writeRawText:(NSString *)text;

@end
