//
//  PALogInfo.h
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PALogInfo : NSObject

@property (nonatomic, readonly) NSString *lineNumber;
@property (nonatomic, readonly) NSString *fileName;
@property (nonatomic, readonly) NSString *methodName;

- (instancetype)initWithLineNumber:(NSString *)lineNumber
                          fileName:(NSString *)fileName
                        methodName:(NSString *)methodName;


@property(nonatomic, readonly) NSDictionary *dictionaryRepresentation;

@end
