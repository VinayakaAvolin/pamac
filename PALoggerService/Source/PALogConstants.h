//
//  PALogConstants.h
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifndef PALogConstants_h
#define PALogConstants_h

typedef enum : NSUInteger {
  PALogClientNone,
  PALogClientResidentAgent,
  PALogClientSyncManager,
  PALogClientAppUi,
  PALogClientAlertTray,
  PALogClientNotificationTray,
  PALogClientEvent,
  PALogClientSelfHeal
} PALogClient;

typedef enum : NSUInteger {
  PALogLevelNone,
  PALogLevelDebug,
  PALogLevelInfo,
  PALogLevelWarning,
  PALogLevelError,
  PALogLevelOff,
} PALogLevel;
#define kPALogLevelStringArray @"none", @"DEBUG", @"INFO", @"WARNING", @"ERROR", @"OFF", nil

extern NSString *const kPALogFormatterLogLevelKey;
extern NSString *const kPALogFormatterLineNumberKey;
extern NSString *const kPALogFormatterFileNameKey;
extern NSString *const kPALogFormatterMethodNameKey;

extern NSString *const kPADefaultLogFileName;

#endif /* PALogConstants_h */
