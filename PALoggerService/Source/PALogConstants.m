//
//  PALogConstants.m
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PALogConstants.h"

NSString *const kPALogFormatterLogLevelKey = @"LogLevelKey";
NSString *const kPALogFormatterLineNumberKey = @"LineNumberKey";
NSString *const kPALogFormatterFileNameKey = @"FilenameKey";
NSString *const kPALogFormatterMethodNameKey = @"MethodnameKey";

// different log file name
NSString *const kPADefaultLogFileName = @"default.log";
