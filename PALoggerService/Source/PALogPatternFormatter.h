//
//  PALogPatternFormatter.h
//  PALoggerService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 The PatternFormatter will format the message according to a given pattern.
 The pattern is a regular string, with markers prefixed by '%'.
 
 Available markers are :
 * l: The name of the log level.
 * n: The name of the logger.
 * d: The date of the log. The format specifier is that NSDateFormatterMediumStyle function.
 * L: the number of the line where the log was issued
 * f: the name of the file where the log was issued without the full path
 * M: the name of the function in which the log was issued
 * m: the message
 *
 **Examples**
 "[%d] [%l] [%n] [%f] [%M] %m" -> "[20-Oct-2017, 4:10:01 PM] [Debug] [mylogger] [main.m] [main] log message"
 */
@interface PALogPatternFormatter : NSObject

@property (readonly) NSString *formatString;
@property (readonly) NSArray *specifiers;
@property (readonly) NSDictionary *info;
@property (readonly) NSString *identifier;

-(instancetype)initWithFormat: (NSString*)formatString
                     withInfo: (NSDictionary *)info
                andIdentifier: (NSString *)identifier;
-(NSString *)format: (NSString*)logMsg;

@end
