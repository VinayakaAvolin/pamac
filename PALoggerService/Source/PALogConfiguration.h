//
//  PALogConfiguration.h
//  PALoggerService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PALogConstants.h"

@interface PALogConfiguration : NSObject

@property (nonatomic, readonly) NSString *logFilePath;
@property (nonatomic, readonly) PALogLevel logLevel;
@property (nonatomic, readonly) NSString *logLevelString;
@property (nonatomic, readonly) PALogClient logClient;

- (instancetype)initWithLogLevel:(PALogLevel)logLevel
                          client:(PALogClient)client;

@end
