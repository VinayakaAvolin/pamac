//
//  PALogPatternFormatter.m
//  PALoggerService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PALogPatternFormatter.h"
#import "PALogConstants.h"
#import "NSDate+Additions.h"

@interface PALogPatternFormatter()

@property (strong) NSString *formatString;
@property (strong) NSArray *specifiers;
@property (strong) NSDictionary *info;
@property (strong) NSString *identifier;

@end

@implementation PALogPatternFormatter

-(instancetype)initWithFormat: (NSString*)formatString
                     withInfo: (NSDictionary *)info
                andIdentifier: (NSString *)identifier
{
  self = [super init];
  if (self) {
    NSArray *words = [formatString componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    _formatString = [words componentsJoinedByString:@""];
    _info = [info copy];
    _specifiers = [self tokenize];
    _identifier = identifier;
  }
  return self;
}

-(NSArray *)tokenize {
  NSMutableArray *tokens = [NSMutableArray new];
  NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"\\[(.*?)\\]" options:NSRegularExpressionCaseInsensitive error:nil];
  
  [regex enumerateMatchesInString:self.formatString options:0
                            range:NSMakeRange(0, [self.formatString length])
                       usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop)
   {
     for (int i = 1; i< [result numberOfRanges] ; i++) {
       [tokens addObject:[self.formatString substringWithRange:[result rangeAtIndex:i]]];
     }
   }];
  if([self.formatString containsString:@"%m"]) {
    [tokens addObject:@"%m"];
  }
  return [tokens copy];
}


-(NSString *)format: (NSString*)logMsg {
  
  NSMutableString *formattedLogMessage = [NSMutableString new];
  
  for (NSString *specifier in self.specifiers) {
    if([specifier isEqualToString:@"%d"]) {
      NSString *dateStr = [[NSDate systemDate] dateStringWithFormat:Format8];
      [formattedLogMessage appendFormat:@"[%@] ", dateStr];
    }
    else if ([specifier isEqualToString: @"%l"]) {
      NSString *logLevel = self.info[kPALogFormatterLogLevelKey];
      if (logLevel) {
        [formattedLogMessage appendFormat:@"[%@] ", logLevel];
      }
    }
    else if ([specifier isEqualToString: @"%L"]) {
      NSNumber *lineNum = self.info[kPALogFormatterLineNumberKey];
      if (lineNum) {
        [formattedLogMessage appendFormat:@"[%@] ", lineNum];
      }
    }
    else if([specifier isEqualToString: @"%n"]) {
      [formattedLogMessage appendFormat:@"[%@] ", self.identifier];
    }
    else if([specifier isEqualToString:@"%f"]) {
      NSString *fileName = self.info[kPALogFormatterFileNameKey];
      if (fileName) {
        [formattedLogMessage appendFormat:@"[%@] ", fileName];
      }
    }
    else if([specifier isEqualToString:@"%M"]) {
      NSString *methodName = self.info[kPALogFormatterMethodNameKey];
      if (methodName) {
        [formattedLogMessage appendFormat:@"[%@] ", methodName];
      }
    }
    else if([specifier isEqualToString:@"%m"]) {
      [formattedLogMessage appendFormat:@" %@", logMsg];
    }
  }
  return [formattedLogMessage copy];
}

@end
