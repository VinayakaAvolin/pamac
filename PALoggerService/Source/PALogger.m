//
//  PALogger.m
//  PALoggerService
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <PAFileSystemService/PAFileSystemService.h>
#import <PAConfigurationService/PAConfigurationService.h>
#import "PALogger.h"
#import "PALogPatternFormatter.h"
#import "NSString+Additions.h"

NSString *const kPALoggerId = @"PALogger";

@interface PALogger () {
  PAFileSystemManager *_fsManager;
  PALogPatternFormatter *_formatter;
  PALogInfo *_logInfo;
  NSString *_logFormat;
}

@property (nonatomic) PALogConfiguration *logConfiguration;
@property (nonatomic) NSMutableDictionary *logConfigurationMatrix;

@end

@implementation PALogger

+ (id)sharedLoggerForClient:(PALogClient)logClient {
  static PALogger *sharedLogger = nil;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    sharedLogger = [[self alloc] initWithLogConfiguration:nil];
    sharedLogger.logConfigurationMatrix = [[NSMutableDictionary alloc]init];
  });
  sharedLogger.logConfiguration = [sharedLogger logConfigurationForClient:logClient];
  return sharedLogger;
}

- (instancetype)initWithLogConfiguration:(PALogConfiguration *)logConfiguration {
  self = [super init];
  if (self) {
    _logConfiguration = logConfiguration;
    
    if (!logConfiguration) {
      _logConfiguration = [[PALogConfiguration alloc]init];
    }
    if (_logConfiguration) {
      NSString *clientKey = [self keyForClient:_logConfiguration.logClient];
      if (clientKey) {
        _logConfigurationMatrix[clientKey] = _logConfiguration;
      }
    }
    _fsManager = [[PAFileSystemManager alloc]init];
    [self setupLogFormat];
  }
  return self;
}
- (PALogConfiguration *)logConfigurationForClient:(PALogClient)logClient {
  NSString *clientKey = [self keyForClient:logClient];
  PALogConfiguration *config = nil;
  if (clientKey) {
    PALogConfiguration *tempConfig = _logConfigurationMatrix[clientKey];
    if (tempConfig) {
      config = tempConfig;
    }
  }
  if (config) {
    _logConfigurationMatrix[clientKey] = config;
  } else {
    config = [[PALogConfiguration alloc] initWithLogLevel:PALogLevelDebug client:logClient];
    _logConfigurationMatrix[clientKey] = config;
  }
  return config;
}
- (NSString *)keyForClient:(PALogClient)logClient {
  NSString *key = [NSString stringWithFormat:@"%ld",logClient];
  return key;
}
- (void)setupLogFormat {
  PAAppConfiguration *appconfigurations = [PAConfigurationManager appConfigurations];
  switch (_logConfiguration.logClient) {
    case PALogClientAppUi:
      _logFormat = appconfigurations.paUiLogConfiguration.logFormat;
      break;
    case PALogClientResidentAgent:
      _logFormat = appconfigurations.agentLogConfiguration.logFormat;
      break;
    case PALogClientEvent:
      _logFormat = @"%@";
      break;
    default:
      _logFormat = @"[%d] [%l] [%n] [%f] [%M] %m";
      break;
  }
  
}

- (void)setLogConfiguration:(PALogConfiguration *)configuration {
  if (configuration) {
    _logConfiguration = configuration;
  }
}

- (void)debugWithInfo:(PALogInfo *)logInfo andText:(NSString *)format, ... {
  va_list ap;
  va_start(ap, format);
  NSString *logText = [[NSString alloc] initWithFormat:format arguments:ap];
  va_end(ap);
  
  _logInfo = logInfo;
  [self logText:logText level:PALogLevelDebug];
}
- (void)infoWithInfo:(PALogInfo *)logInfo andText:(NSString *)format, ... {
  va_list ap;
  va_start(ap, format);
  NSString *logText = [[NSString alloc] initWithFormat:format arguments:ap];
  va_end(ap);
  
  _logInfo = logInfo;
  [self logText:logText level:PALogLevelInfo];
}
- (void)warningWithInfo:(PALogInfo *)logInfo andText:(NSString *)format, ... {
  va_list ap;
  va_start(ap, format);
  NSString *logText = [[NSString alloc] initWithFormat:format arguments:ap];
  va_end(ap);
  
  _logInfo = logInfo;
  [self logText:logText level:PALogLevelWarning];
}
- (void)errorWithInfo:(PALogInfo *)logInfo andText:(NSString *)format, ... {
  va_list ap;
  va_start(ap, format);
  NSString *logText = [[NSString alloc] initWithFormat:format arguments:ap];
  va_end(ap);
  
  _logInfo = logInfo;
  [self logText:logText level:PALogLevelError];
}

- (void)debug:(NSString *)format, ... {
  va_list ap;
  va_start(ap, format);
  NSString *logText = [[NSString alloc] initWithFormat:format arguments:ap];
  va_end(ap);
  
  _logInfo = nil;
  [self logText:logText level:PALogLevelDebug];
}
- (void)info:(NSString *)format, ... {
  va_list ap;
  va_start(ap, format);
  NSString *logText = [[NSString alloc] initWithFormat:format arguments:ap];
  va_end(ap);
  
  _logInfo = nil;
  [self logText:logText level:PALogLevelInfo];
}
- (void)warning:(NSString *)format, ... {
  va_list ap;
  va_start(ap, format);
  NSString *logText = [[NSString alloc] initWithFormat:format arguments:ap];
  va_end(ap);
  
  _logInfo = nil;
  [self logText:logText level:PALogLevelWarning];
}
- (void)error:(NSString *)format, ... {
  va_list ap;
  va_start(ap, format);
  NSString *logText = [[NSString alloc] initWithFormat:format arguments:ap];
  va_end(ap);
  
  _logInfo = nil;
  [self logText:logText level:PALogLevelError];
}
- (void)logText:(NSString *)text level:(PALogLevel)level {
  if (level >= _logConfiguration.logLevel) {
    [self writeLog:text];
  }
}

- (void)writeRawText:(NSString *)text {
  
  NSData *data = [text dataUsingEncoding:NSUTF8StringEncoding];
  NSString *logFilePath = _logConfiguration.logFilePath;
  if (logFilePath) {
    [_fsManager writeToFileAtPath:logFilePath data:data];
  }
}

- (void)writeLog:(NSString *)logText {
  if (![logText isEmptyOrHasOnlyWhiteSpaces]) {
    
    PALogPatternFormatter *formatter = [[PALogPatternFormatter alloc] initWithFormat:_logFormat
                                                                            withInfo: [self logInfoDictionary]
                                                                       andIdentifier:kPALoggerId];
    
    NSString *formattedLogText = [formatter format:logText];
    
    NSData *data = [[formattedLogText stringByAppendingString:@"\n\n"] dataUsingEncoding:NSUTF8StringEncoding];
    NSString *logFilePath = _logConfiguration.logFilePath;
    if (logFilePath) {
      [_fsManager writeToFileAtPath:logFilePath data:data];
    }
  }
}

- (NSDictionary *)logInfoDictionary {
  NSMutableDictionary *logInfoDict = [_logInfo.dictionaryRepresentation mutableCopy];
  if (!logInfoDict) {
    logInfoDict = [NSMutableDictionary dictionary];
  }
  [logInfoDict setValue:_logConfiguration.logLevelString forKey:kPALogFormatterLogLevelKey];
  return [NSDictionary dictionaryWithDictionary:logInfoDict];
}

@end
