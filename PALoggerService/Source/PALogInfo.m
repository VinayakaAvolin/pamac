//
//  PALogInfo.m
//  ProactiveAssist
//
//  Copyright © 2017 Avolin. All rights reserved.
//

#import "PALogInfo.h"
#import "PALogConstants.h"

@implementation PALogInfo

- (instancetype)initWithLineNumber:(NSString *)lineNumber
                          fileName:(NSString *)fileName
                        methodName:(NSString *)methodName {
  self = [super init];
  if (self) {
    _lineNumber = lineNumber;
    _fileName = fileName;
    _methodName = methodName;
  }
  return self;
}

- (NSDictionary *)dictionaryRepresentation {
  NSMutableDictionary *dict = [NSMutableDictionary dictionary];
  [dict setValue:_lineNumber forKey:kPALogFormatterLineNumberKey];
  [dict setValue:_fileName forKey:kPALogFormatterFileNameKey];
  [dict setValue:_methodName forKey:kPALogFormatterMethodNameKey];
  return [NSDictionary dictionaryWithDictionary:dict];
}
@end
