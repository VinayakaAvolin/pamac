{
  "cid"       : "61e3dc90-de46-402c-926b-f385e9a702b4",
  "version"   : "10",
  "title"     : "Problem with LotusNotes Logon",
  "description": "Seems like you have problem in logging into Lotus Notes application. Click on Fix button to reset or unlock your login.",
  "ctype"     : "sprt_msg",
  "fid"       : "288e1838-ed31-42e7-b402-fc2b113b374c",
  "language"  : "en",
  "category"  : []
}
