{
  "cid"       : "b9c9d191-b488-40e3-a5e2-329820e9e9df",
  "version"   : "12",
  "title"     : "Support Message - Do it For Me (Emergency)",
  "description": "Please click on “Do it For Me” button below to perform an urgent fix on Group Policy related issue.",
  "ctype"     : "sprt_msg",
  "fid"       : "bb29f816-727a-4b8c-bff1-43ec56652abd",
  "language"  : "en",
  "category"  : []
}
