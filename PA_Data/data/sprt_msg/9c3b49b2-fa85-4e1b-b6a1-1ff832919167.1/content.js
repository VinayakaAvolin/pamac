{
  "cid"       : "9c3b49b2-fa85-4e1b-b6a1-1ff832919167",
  "version"   : "1",
  "title"     : "Enable disabled WebEx Productivity Add-ins in Outlook",
  "description": "This solution enables disabled WebEx productivity Addin in Outlook",
  "ctype"     : "sprt_msg",
  "fid"       : "ea708038-c2b3-42f3-924b-4c686e97b13a",
  "language"  : "en",
  "category"  : []
}
