{
  "cid"       : "e61c5205-3c0e-45ee-aa0b-a69e54b7179a",
  "version"   : "5",
  "title"     : "Account Manager Notification Job",
  "description": "Account Manager notification for Registration and Reset of password.",
  "ctype"     : "sprt_job",
  "fid"       : "a05c907c-1f08-4a8a-b5be-0579183eaf16",
  "language"  : "en",
  "category"  : []
}
