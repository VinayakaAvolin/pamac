{
  "cid"       : "d43726f4-3363-443c-b3ad-8867891707e3",
  "version"   : "5",
  "title"     : "Prompt user to reboot if not rebooted in X days",
  "description": "This solution prompts the user to reboot the system. After prompting, the solution also allows the user to perform a reboot. The number of days after which a prompt is displayed can be configured during solution deployment.",
  "ctype"     : "sprt_job",
  "fid"       : "a05c907c-1f08-4a8a-b5be-0579183eaf16",
  "language"  : "en",
  "category"  : []
}
