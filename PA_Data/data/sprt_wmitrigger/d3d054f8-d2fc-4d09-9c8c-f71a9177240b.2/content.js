{
  "cid"       : "d3d054f8-d2fc-4d09-9c8c-f71a9177240b",
  "version"   : "2",
  "title"     : "WMITrigger for fixing ProactiveAssist sync issue",
  "description": "After the configured number of sync fails the solution executes and fixes the issue",
  "ctype"     : "sprt_wmitrigger",
  "fid"       : "f1c6bd81-b544-42e7-bbad-623006e062ec",
  "language"  : "en",
  "category"  : []
}
