{
  "cid"       : "231a2e9e-e847-4023-8d70-6477b0913557",
  "version"   : "2",
  "title"     : "WMITrigger for enabling &#39;Do Not Join Audio&#39; option - Lync 2013 or Skype for Business 2015",
  "description": "Enable the &#39;Do not Join Audio&#39; option in Lync or Skype for Business",
  "ctype"     : "sprt_wmitrigger",
  "fid"       : "f1c6bd81-b544-42e7-bbad-623006e062ec",
  "language"  : "en",
  "category"  : []
}
