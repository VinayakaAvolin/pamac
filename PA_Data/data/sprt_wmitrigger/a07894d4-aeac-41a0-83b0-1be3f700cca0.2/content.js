{
  "cid"       : "a07894d4-aeac-41a0-83b0-1be3f700cca0",
  "version"   : "2",
  "title"     : "WMITrigger for fixing connectivity issues - Lync 2013 or Skype for Business 2015",
  "description": "Connectivity issues may be due to Automatic Configuration not being enabled in Lync or Skype for Business.",
  "ctype"     : "sprt_wmitrigger",
  "fid"       : "f1c6bd81-b544-42e7-bbad-623006e062ec",
  "language"  : "en",
  "category"  : []
}
