{
  "cid"       : "f58a20d4-3e64-4944-b18d-44e8b5cafd71",
  "version"   : "2",
  "title"     : "WMITrigger to fix Issues with Opening Hyperlinks in Microsoft Outlook Emails",
  "description": "WMITrigger to fix Issues with Opening Hyperlinks in Microsoft Outlook Emails",
  "ctype"     : "sprt_wmitrigger",
  "fid"       : "f1c6bd81-b544-42e7-bbad-623006e062ec",
  "language"  : "en",
  "category"  : []
}
