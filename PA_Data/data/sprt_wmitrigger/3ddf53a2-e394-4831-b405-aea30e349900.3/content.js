{
  "cid"       : "3ddf53a2-e394-4831-b405-aea30e349900",
  "version"   : "3",
  "title"     : "WMITrigger for enabling &#39;Do Not Join Audio&#39; option - Skype for Business 2016",
  "description": "Enable the &#39;Do not Join Audio&#39; option in Lync or Skype for Business",
  "ctype"     : "sprt_wmitrigger",
  "fid"       : "f1c6bd81-b544-42e7-bbad-623006e062ec",
  "language"  : "en",
  "category"  : []
}
