{
  "cid"       : "8a7950ab-8947-4124-ba46-ca88930d4057",
  "version"   : "2",
  "title"     : "WMITrigger to fix Microsoft Outlook Read Emails Displayed as Unread",
  "description": "WMITrigger to fix Microsoft Outlook Read Emails Displayed as Unread - 2016",
  "ctype"     : "sprt_wmitrigger",
  "fid"       : "f1c6bd81-b544-42e7-bbad-623006e062ec",
  "language"  : "en",
  "category"  : []
}
