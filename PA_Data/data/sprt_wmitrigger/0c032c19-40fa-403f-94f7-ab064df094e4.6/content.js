{
  "cid"       : "0c032c19-40fa-403f-94f7-ab064df094e4",
  "version"   : "6",
  "title"     : "Critical Windows Service Stopped",
  "description": "This solution automatically starts windows firewall service should it accidentally stop",
  "ctype"     : "sprt_wmitrigger",
  "fid"       : "815d43bc-e154-4749-bafe-c61e19bcf1db",
  "language"  : "en",
  "category"  : []
}
