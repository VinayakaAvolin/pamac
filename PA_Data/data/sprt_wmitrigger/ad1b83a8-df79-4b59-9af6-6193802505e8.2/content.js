{
  "cid"       : "ad1b83a8-df79-4b59-9af6-6193802505e8",
  "version"   : "2",
  "title"     : "WMITrigger for system reboot",
  "description": "This solution notifies the user to reboot the system",
  "ctype"     : "sprt_wmitrigger",
  "fid"       : "f1c6bd81-b544-42e7-bbad-623006e062ec",
  "language"  : "en",
  "category"  : []
}
