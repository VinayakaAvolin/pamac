{
  "cid"       : "5e3e89e0-8f43-4742-abb6-826a7e71071f",
  "version"   : "2",
  "title"     : "WMITrigger for fixing connectivity issues - Skype for Business 2016",
  "description": "Connectivity issues may be due to Automatic Configuration not being enabled in Lync or Skype for Business.",
  "ctype"     : "sprt_wmitrigger",
  "fid"       : "f1c6bd81-b544-42e7-bbad-623006e062ec",
  "language"  : "en",
  "category"  : []
}
