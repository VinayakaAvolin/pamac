{
  "cid"       : "8fe6c211-d87b-434b-a97d-cfc3c53c43c2",
  "version"   : "2",
  "title"     : "WMITrigger to auto populate user logon ID - Skype for Business 2016",
  "description": "This solution obtains the default Microsoft Outlook email ID and auto populates the logon ID for Microsoft Lync or Skype for Business",
  "ctype"     : "sprt_wmitrigger",
  "fid"       : "f1c6bd81-b544-42e7-bbad-623006e062ec",
  "language"  : "en",
  "category"  : []
}
