{
  "cid"       : "24572eaf-3df5-44c9-850e-678ee4bdec34",
  "version"   : "2",
  "title"     : "WMITrigger to auto populate user logon ID - Lync 2013 or Skype for Business 2015",
  "description": "This solution obtains the default Microsoft Outlook email ID and auto populates the logon ID for Microsoft Lync or Skype for Business",
  "ctype"     : "sprt_wmitrigger",
  "fid"       : "f1c6bd81-b544-42e7-bbad-623006e062ec",
  "language"  : "en",
  "category"  : []
}
