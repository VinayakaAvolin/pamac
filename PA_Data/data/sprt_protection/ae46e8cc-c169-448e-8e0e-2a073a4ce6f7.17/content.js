{
  "cid"       : "ae46e8cc-c169-448e-8e0e-2a073a4ce6f7",
  "version"   : "17",
  "title"     : "Cisco AnyConnect Secure Mobility Client Profile Protection",
  "description": "This solution protects and restores the Cisco AnyConnect Secure Mobility Client profiles",
  "ctype"     : "sprt_protection",
  "fid"       : "69154b8b-ef92-4544-9d29-ff0bace5150f",
  "language"  : "en",
  "category"  : []
}
