{
  "cid"       : "217ccf3c-b205-4e00-ab12-83cb63aedd5e",
  "version"   : "5",
  "title"     : "Cisco AnyConnect Secure Mobility Client Protection",
  "description": "This protection restores the Cisco AnyConnect Secure Mobility Client settings",
  "ctype"     : "sprt_protection",
  "fid"       : "69154b8b-ef92-4544-9d29-ff0bace5150f",
  "language"  : "en",
  "category"  : []
}
