{
  "cid"       : "d2b0aea9-b8ce-4b9f-9b1a-47728baf395f",
  "version"   : "4",
  "title"     : "Fix connectivity issues on Lync or Skype for Business",
  "description": "Connectivity issues may be due to Automatic Configuration not being enabled in Lync or Skype for Business. This solution switches Manual configuration to Automatic Configuration in Advanced Connection Settings, if this is not enabled.",
  "ctype"     : "sprt_actionlight",
  "fid"       : "14d29e0c-344e-4b1d-933a-818516915b0f",
  "language"  : "en",
  "category"  : []
}
