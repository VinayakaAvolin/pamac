{
  "cid"       : "2471ef5f-d1ed-4a37-9864-1840d061f27c",
  "version"   : "5",
  "title"     : "Disable Script Debugging",
  "description": "If you are unable to open a new window/ tab from a hyperlink given on an existing webpage, then this solution will help you.",
  "ctype"     : "sprt_actionlight",
  "fid"       : "0685ad60-cb6f-4e50-b506-7d2400321c30",
  "language"  : "en",
  "category"  : ["featured"]
}
