{
  "cid"       : "1fab4704-4598-48c1-945c-14e07ceb5cf1",
  "version"   : "7",
  "title"     : "Clear Windows Temp Files",
  "description": "This solution clears all the temporary files under Windows &quot;Temp&quot; folder and provides free space for improving System Performance.",
  "ctype"     : "sprt_actionlight",
  "fid"       : "6269acb5-b92d-46c9-922d-3771679c7d28",
  "language"  : "en",
  "category"  : []
}
