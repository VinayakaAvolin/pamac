{
  "cid"       : "25de5dd2-0940-438f-854b-06896d481610",
  "version"   : "4",
  "title"     : "Clear Temporary Internet files (cache)",
  "description": "Temporary Internet Files folder contains a record of the websites visited. All those files stored in the cache take up space. You may want to clear those files occasionally to free up some space and significantly improve the speed and performance of your computer.",
  "ctype"     : "sprt_actionlight",
  "fid"       : "0685ad60-cb6f-4e50-b506-7d2400321c30",
  "language"  : "en",
  "category"  : []
}
