{
  "cid"       : "cbb85281-0fbb-44cc-a7db-3634ee5f1dfd",
  "version"   : "3",
  "title"     : "Enable Home button in Microsoft Edge Browser",
  "description": "The Home button in Microsoft Edge browser may not be visible due to setting changes. This solution enables the display of Home button in Microsoft Edge browser.",
  "ctype"     : "sprt_actionlight",
  "fid"       : "ef5a37b9-f7d8-42d2-a912-6565a17067a2",
  "language"  : "en",
  "category"  : []
}
