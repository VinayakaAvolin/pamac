{
  "cid"       : "5f4cc104-49f6-4856-bd87-e1fd981d63dd",
  "version"   : "4",
  "title"     : "System Management Tool",
  "description": "Currently, this solution has options to provide and remove group permissions to the user. The author or analyst can use this solution to grant or revoke group permissions. This solution will not be seen by the end user on the ProactiveAssit Client.",
  "ctype"     : "sprt_actionlight",
  "fid"       : "15205b17-bcb0-42cb-b9b2-f338918c6a9f",
  "language"  : "en",
  "category"  : []
}
