{
  "cid"       : "bedbcc9b-b583-4cc5-98ae-09ba0bcfe66a",
  "version"   : "4",
  "title"     : "Repair corrupted Java",
  "description": "This solution uninstalls all Java versions on the user machine and installs Java Run Environment 1.6, 32-bit. Note: Any open Internet Explorer browsers must be closed before solution execution.",
  "ctype"     : "sprt_actionlight",
  "fid"       : "c4c12636-594f-4232-b708-ea1fbd8eeb24",
  "language"  : "en",
  "category"  : []
}
