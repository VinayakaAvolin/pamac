{
  "cid"       : "75109183-7e7f-4477-a4f8-b9a53e909ab2",
  "version"   : "3",
  "title"     : "Enable Windows Spotlight",
  "description": "Windows Spotlight is an option for the lock screen background that displays different background images. The lock screen background will occasionally suggest Windows 10 features that the user has not tried. This solution enables Windows Spotlight (if available) on Windows 10 if Windows Spotlight is disabled.",
  "ctype"     : "sprt_actionlight",
  "fid"       : "382176c2-38d8-4526-9ecc-99794cb87109",
  "language"  : "en",
  "category"  : []
}
