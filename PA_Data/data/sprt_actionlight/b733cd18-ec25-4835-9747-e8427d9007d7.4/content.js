{
  "cid"       : "b733cd18-ec25-4835-9747-e8427d9007d7",
  "version"   : "4",
  "title"     : "Disk Cleanup",
  "description": "This solution helps you to free up space on your hard disk by clearing Temporary Files, Recycle Bin, and compressing old files. This is useful when the available disk space is low, which leads to slow disk and system performance, problems in launching applications.",
  "ctype"     : "sprt_actionlight",
  "fid"       : "6269acb5-b92d-46c9-922d-3771679c7d28",
  "language"  : "en",
  "category"  : []
}
