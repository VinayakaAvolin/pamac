{
  "cid"       : "681941cc-85e5-44e2-991b-b571e721bbb8",
  "version"   : "4",
  "title"     : "Enable the &#39;Do not Join Audio&#39; option in Lync or Skype for Business",
  "description": "The user may need to connect to a call using a phone or may want to connect to audio later. This solution enables the Do Not Join Audio option on Microsoft Lync or Skype for Business. After execution, the audio is disabled for the user.",
  "ctype"     : "sprt_actionlight",
  "fid"       : "14d29e0c-344e-4b1d-933a-818516915b0f",
  "language"  : "en",
  "category"  : []
}
