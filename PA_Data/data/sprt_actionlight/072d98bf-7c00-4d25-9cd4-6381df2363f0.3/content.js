{
  "cid"       : "072d98bf-7c00-4d25-9cd4-6381df2363f0",
  "version"   : "3",
  "title"     : "Enable macros in Microsoft Excel",
  "description": "Macros in Excel allow you to automate simple, repetitive tasks. This solution enables macros in Microsoft Excel if macros are disabled.",
  "ctype"     : "sprt_actionlight",
  "fid"       : "e5aa2571-fb7b-48e7-bb30-661dbdd5f2c9",
  "language"  : "en",
  "category"  : []
}
