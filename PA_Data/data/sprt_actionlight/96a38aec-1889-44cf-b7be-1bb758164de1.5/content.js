{
  "cid"       : "96a38aec-1889-44cf-b7be-1bb758164de1",
  "version"   : "5",
  "title"     : "Clear Cookies in Internet Explorer",
  "description": "Cookie is a short line of text that a web site puts on your computer&#39;s hard drive when you access websites. Websites use cookies to track the session and visitor information. This solution will clear the cookies to improve browser&#39;s performance.",
  "ctype"     : "sprt_actionlight",
  "fid"       : "0685ad60-cb6f-4e50-b506-7d2400321c30",
  "language"  : "en",
  "category"  : []
}
