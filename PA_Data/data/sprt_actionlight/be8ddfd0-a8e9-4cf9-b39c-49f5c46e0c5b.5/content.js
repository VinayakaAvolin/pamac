{
  "cid"       : "be8ddfd0-a8e9-4cf9-b39c-49f5c46e0c5b",
  "version"   : "5",
  "title"     : "Enable Inactive Add-Ins in Microsoft Office Application",
  "description": "Enable Inactive Add-Ins in Microsoft Office Application",
  "ctype"     : "sprt_actionlight",
  "fid"       : "14d29e0c-344e-4b1d-933a-818516915b0f",
  "language"  : "en",
  "category"  : []
}
