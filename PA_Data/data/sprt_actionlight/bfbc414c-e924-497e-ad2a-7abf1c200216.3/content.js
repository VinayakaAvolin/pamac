{
  "cid"       : "bfbc414c-e924-497e-ad2a-7abf1c200216",
  "version"   : "3",
  "title"     : "Enable Apps to be downloaded and installed from Enterprise App Store on Mac",
  "description": "Enable Apps to be downloaded and installed from Enterprise App Store on Mac",
  "ctype"     : "sprt_actionlight",
  "fid"       : "a94e1a52-ce71-45bf-9d00-7317fff36e68",
  "language"  : "en",
  "category"  : []
}
