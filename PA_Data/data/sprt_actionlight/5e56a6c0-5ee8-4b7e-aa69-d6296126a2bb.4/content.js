{
  "cid"       : "5e56a6c0-5ee8-4b7e-aa69-d6296126a2bb",
  "version"   : "4",
  "title"     : "Clear all Java add-ons (except Java 6) on Internet Explorer",
  "description": "This solution clears all the Java add-ons on Internet Explorer except for the add-ons for Java Runtime Environment 1.6.",
  "ctype"     : "sprt_actionlight",
  "fid"       : "c4c12636-594f-4232-b708-ea1fbd8eeb24",
  "language"  : "en",
  "category"  : []
}
