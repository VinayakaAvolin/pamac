{
  "cid"       : "25c4418c-e7e2-4f4c-8feb-f7df57b2f441",
  "version"   : "4",
  "title"     : "Clear TEMP files for all profiles",
  "description": "This solution clears all the Temp files for all the profiles, and provides free space for improving System Performance.",
  "ctype"     : "sprt_actionlight",
  "fid"       : "6269acb5-b92d-46c9-922d-3771679c7d28",
  "language"  : "en",
  "category"  : []
}
