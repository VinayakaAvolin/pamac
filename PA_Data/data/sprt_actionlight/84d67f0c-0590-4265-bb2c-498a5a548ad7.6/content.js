{
  "cid"       : "84d67f0c-0590-4265-bb2c-498a5a548ad7",
  "version"   : "6",
  "title"     : "Fix frequent pop-up to enter password issues in Microsoft Outlook",
  "description": "Fix frequent pop-up to enter password issues in Microsoft Outlook",
  "ctype"     : "sprt_actionlight",
  "fid"       : "14d29e0c-344e-4b1d-933a-818516915b0f",
  "language"  : "en",
  "category"  : []
}
