{
  "cid"       : "cc7aff4d-21de-4f5c-8ee2-bcc780450971",
  "version"   : "5",
  "title"     : "Fix send or receive email errors in Microsoft Outlook",
  "description": "Fix send or receive email errors in Microsoft Outlook",
  "ctype"     : "sprt_actionlight",
  "fid"       : "14d29e0c-344e-4b1d-933a-818516915b0f",
  "language"  : "en",
  "category"  : []
}
