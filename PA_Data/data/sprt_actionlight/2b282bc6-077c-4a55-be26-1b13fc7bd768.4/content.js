{
  "cid"       : "2b282bc6-077c-4a55-be26-1b13fc7bd768",
  "version"   : "4",
  "title"     : "Clear TEMP files for logged-in user profile",
  "description": "This solution clears all the temporary files under Logged in Temp Folder and provides free space for improving System Performance.",
  "ctype"     : "sprt_actionlight",
  "fid"       : "6269acb5-b92d-46c9-922d-3771679c7d28",
  "language"  : "en",
  "category"  : []
}
