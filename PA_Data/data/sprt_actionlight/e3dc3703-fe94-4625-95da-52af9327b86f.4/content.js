{
  "cid"       : "e3dc3703-fe94-4625-95da-52af9327b86f",
  "version"   : "4",
  "title"     : "Enable Save Conversation History settings in Lync or Skype for Business",
  "description": "The Microsoft Lync or Skype for Business conversations may not be getting saved in Outlook due to the applied settings. This solution enables the Save IM conversations in my email Conversation History folder option under Personal settings to resolve this issue.",
  "ctype"     : "sprt_actionlight",
  "fid"       : "14d29e0c-344e-4b1d-933a-818516915b0f",
  "language"  : "en",
  "category"  : []
}
