{
  "cid"       : "b6a26151-654d-47cd-bd25-4fb68c3cc6a2",
  "version"   : "3",
  "title"     : "Enable Formula Bar in Microsoft Excel",
  "description": "If the Formula Bar is not visible, you may have difficulties with editing formulas. This solution enables the display of the Formula bar in Microsoft Excel.",
  "ctype"     : "sprt_actionlight",
  "fid"       : "e5aa2571-fb7b-48e7-bb30-661dbdd5f2c9",
  "language"  : "en",
  "category"  : []
}
