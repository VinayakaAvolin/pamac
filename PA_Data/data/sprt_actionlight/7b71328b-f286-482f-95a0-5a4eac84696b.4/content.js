{
  "cid"       : "7b71328b-f286-482f-95a0-5a4eac84696b",
  "version"   : "4",
  "title"     : "Fix ProactiveAssist sync issue",
  "description": "This solution silently fixes ProactiveAssist sync issues. After the configured number of sync fails, the solution executes and fixes the issue.",
  "ctype"     : "sprt_actionlight",
  "fid"       : "15205b17-bcb0-42cb-b9b2-f338918c6a9f",
  "language"  : "en",
  "category"  : []
}
