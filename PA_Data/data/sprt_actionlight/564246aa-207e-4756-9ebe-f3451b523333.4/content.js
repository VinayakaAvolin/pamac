{
  "cid"       : "564246aa-207e-4756-9ebe-f3451b523333",
  "version"   : "4",
  "title"     : "Prevention of Java updates",
  "description": "This solution prevents updates for Java. The solution makes appropriate registry changes to prevent the Java updates.",
  "ctype"     : "sprt_actionlight",
  "fid"       : "c4c12636-594f-4232-b708-ea1fbd8eeb24",
  "language"  : "en",
  "category"  : []
}
