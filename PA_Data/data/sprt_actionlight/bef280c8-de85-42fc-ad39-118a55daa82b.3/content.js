{
  "cid"       : "bef280c8-de85-42fc-ad39-118a55daa82b",
  "version"   : "3",
  "title"     : "Disable Action Center in Windows",
  "description": "Action Center is a unified place for all system notifications and quick access to various settings on Windows 10. This solution disables Action Center on Windows 10 if Action Center is enabled.",
  "ctype"     : "sprt_actionlight",
  "fid"       : "382176c2-38d8-4526-9ecc-99794cb87109",
  "language"  : "en",
  "category"  : []
}
