{
  "cid"       : "cad3b6a6-96bb-4a78-81ba-8d7ec4f49b41",
  "version"   : "7",
  "title"     : "Auto populate user logon ID for Lync or Skype for Business",
  "description": "This solution obtains the default Microsoft Outlook email ID and auto populates the logon ID for Microsoft Lync or Skype for Business. Note: The Outlook profile must be created and configured for the solution to execute.",
  "ctype"     : "sprt_actionlight",
  "fid"       : "14d29e0c-344e-4b1d-933a-818516915b0f",
  "language"  : "en",
  "category"  : []
}
