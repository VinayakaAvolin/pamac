{
  "cid"       : "fbabc888-f59f-4e0f-91f4-67a94bd3bff3",
  "version"   : "3",
  "title"     : "Fix Microsoft Outlook read emails displayed as unread",
  "description": "Generally in Microsoft Outlook, emails appear as unread after they are opened. However, this may sometimes fail to happen due to setting changes. This solution resolves the issue by making the necessary setting changes.",
  "ctype"     : "sprt_actionlight",
  "fid"       : "e5aa2571-fb7b-48e7-bb30-661dbdd5f2c9",
  "language"  : "en",
  "category"  : []
}
