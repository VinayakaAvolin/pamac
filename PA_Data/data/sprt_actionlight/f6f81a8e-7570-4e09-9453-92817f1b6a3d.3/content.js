{
  "cid"       : "f6f81a8e-7570-4e09-9453-92817f1b6a3d",
  "version"   : "3",
  "title"     : "Enable Flash player in Microsoft Edge Browser",
  "description": "Without Adobe Flash player, you may have trouble playing videos on Edge browser. This solution enables Adobe Flash Player in Microsoft Edge browser if Adobe Flash Player is disabled.",
  "ctype"     : "sprt_actionlight",
  "fid"       : "ef5a37b9-f7d8-42d2-a912-6565a17067a2",
  "language"  : "en",
  "category"  : []
}
