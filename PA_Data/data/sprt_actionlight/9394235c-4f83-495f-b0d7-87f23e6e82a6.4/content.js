{
  "cid"       : "9394235c-4f83-495f-b0d7-87f23e6e82a6",
  "version"   : "4",
  "title"     : "Configure Virtual Memory",
  "description": "This Solution will configure the Virtual Memory based on the Physical Memory of the System. This will help in improving the System Performance.",
  "ctype"     : "sprt_actionlight",
  "fid"       : "6269acb5-b92d-46c9-922d-3771679c7d28",
  "language"  : "en",
  "category"  : []
}
