{
  "cid"       : "9a506f0f-6d74-4dbe-9713-33e3b8c143d7",
  "version"   : "6",
  "title"     : "Restore Cisco AnyConnect VPN profiles",
  "description": "This solution restores the cisco any connect vpn profiles if it is already protected",
  "ctype"     : "sprt_actionlight",
  "fid"       : "d5fe8789-35d8-4062-aab4-8a217ff39ece",
  "language"  : "en",
  "category"  : []
}
