{
  "cid"       : "a8587fa0-7745-4263-8a9f-2794c59cb7d4",
  "version"   : "3",
  "title"     : "System Management Tool",
  "description": "This solution allows the administrator to do the following.
- Provide or remove administrative privileges to the user.
- Allow or block recognition of USB devices on the user machine.",
  "ctype"     : "sprt_actionlight",
  "fid"       : "382176c2-38d8-4526-9ecc-99794cb87109",
  "language"  : "en",
  "category"  : []
}
