{
  "cid"       : "4924304f-cdab-4a82-b2da-0126ac07e3e3",
  "version"   : "3",
  "title"     : "Fix issues in opening Hyperlinks in Microsoft Outlook emails",
  "description": "Hyperlinks in Outlook emails may fail to work. This solution resolves this issue by making the appropriate setting changes.",
  "ctype"     : "sprt_actionlight",
  "fid"       : "e5aa2571-fb7b-48e7-bb30-661dbdd5f2c9",
  "language"  : "en",
  "category"  : []
}
