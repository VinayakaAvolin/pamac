
$ss.layout = $ss.layout ||
{};


$ss.layout.LayoutModel = MVC.Class.extend({
  init: function(sPageDefFile){
    this._pageDefFile = sPageDefFile;
    this._navTree = [];
    this.NAVLEVELMAX = 2;
    this._header = {};
    this._footer = {};
    this._menu = {};
    this._pageDefinition = {};
    this._urlPageMap = {};
    this._layoutGridCache = {};
    
    this._defaultPath ="";
    this._selectedFirstNav = "";
    this._selectedSecNav = "";
    this._currentPage = "";
    this._currentUrl = "";
    
    this._logger = $ss.agentcore.log.GetDefaultLogger("$ss.layout.LayoutModel");
    this._rootXPath = "//root/shell/body";
    this.Initialize();
  },
  Initialize: function(){
    var _xml = $ss.agentcore.dal.xml;
    var _const = $ss.agentcore.constants;
    var tempDom = _xml.LoadXML(this._pageDefFile, false);
    this.LoadHeader(tempDom);
    this.LoadFooter(tempDom);
    this.LoadMenu(tempDom);
    var contXPath = this._rootXPath + "/content";
    var oContentDom = _xml.GetNode(contXPath, "", tempDom);
    this.BuildNavigation(oContentDom, this._navTree, "/",null);
    
    //get the default path;
    var defaultPathUrl = "/";
    var defaultPathPage = this._pageDefinition[this._menu.deaultPage];
    if(defaultPathPage) {
        this._urlPageMap[defaultPathUrl] = this._urlPageMap[defaultPathPage.url];
        this._defaultPath = defaultPathPage.url;
    } 
    
  },
  LoadHeader: function(oDom){
    var _xml = $ss.agentcore.dal.xml;
    var headerXpath = this._rootXPath + "/header";
    var tempHeadNode = _xml.GetNode(headerXpath, "", oDom);
    var toLocation = _xml.GetAttribute(tempHeadNode, "markupId");
    var headerFile = _xml.GetAttribute(tempHeadNode, "contentUri");
    headerFile = $ss.agentcore.dal.config.ParseMacros(headerFile);
    this._header.to = toLocation;
    this._header.file = headerFile;
    tempHeadNode = null;
  },
  LoadFooter: function(oDom){
    var _xml = $ss.agentcore.dal.xml;
    var footerXpath = this._rootXPath + "/footer";
    var tempFooterNode = _xml.GetNode(footerXpath, "", oDom);
    var toLocation = _xml.GetAttribute(tempFooterNode, "markupId");
    var footerFile = _xml.GetAttribute(tempFooterNode, "contentUri");
    footerFile = $ss.agentcore.dal.config.ParseMacros(footerFile);
    this._footer.to = toLocation;
    this._footer.file = footerFile;
    tempFooterNode = null;
  },
  LoadMenu: function(oDom){
    var _xml = $ss.agentcore.dal.xml;
    var menuXpath = this._rootXPath + "/menu";
    var tempMenuNode = _xml.GetNode(menuXpath, "", oDom);
    var toLocation = _xml.GetAttribute(tempMenuNode, "markupId");
    var menuFile = _xml.GetAttribute(tempMenuNode, "contentUri");
    menuFile = $ss.agentcore.dal.config.ParseMacros(menuFile);
    var defaultPage = _xml.GetAttribute(tempMenuNode, "defaultpage");
    this._menu.to = toLocation;
    this._menu.file = menuFile;
    this._menu.deaultPage = defaultPage;
    tempMenuNode = null;
  },
  BuildNavigation: function(oDom, oNavLevel,sParentPath,parentObj){
    var _xml = $ss.agentcore.dal.xml;

    if (oDom) {
      for (var i = 0; i < oDom.childNodes.length; i++) {
        
        var tmpChildNode = oDom.childNodes[i];
        var tmpLevel = _xml.GetAttribute(tmpChildNode, "level");
        var tmpUrl = _xml.GetAttribute(tmpChildNode, "url");
        var tmpDefName = _xml.GetAttribute(tmpChildNode, "definitionLabel");
        var tmpTitle = $ss.agentcore.utils.GlobalXLate(_xml.GetAttribute(tmpChildNode, "title"));
        var tmpShowInMenu = _xml.GetAttribute(tmpChildNode, "showinmenu") == "true" ? true : false;
        var tmpMenuDisplayCssClass = _xml.GetAttribute(tmpChildNode, "menuclassname");
        var tmpNodeType = _xml.GetAttribute(tmpChildNode, "markupType");

        var config_section =  _xml.GetAttribute(tmpChildNode, "show_page_config_section");
        var config_key = _xml.GetAttribute(tmpChildNode, "show_page_config_key");
        var page_available = (_xml.GetAttribute(tmpChildNode, "page_available")===null) ? true:  (page_available === "true");
     
        config_section = config_section===null ? null: config_section ;
        config_key = config_key===null  ? null: config_key ;
        if(config_section && config_key ) {
          var overRideShowStatus = ($ss.agentcore.dal.config.GetConfigValue( config_section , config_key, "false")) == "true";
          //tmpShowInMenu = overRideShowStatus; // in minibcont it is always no show
          page_available = overRideShowStatus;
        }
        
        if(!page_available) continue; 


        oNavLevel[i] = {};
        oNavLevel[i].level = tmpLevel;
        oNavLevel[i].url = tmpUrl;
        oNavLevel[i].defName = tmpDefName;
        oNavLevel[i].title = tmpTitle;
        oNavLevel[i].isMenuItem = tmpShowInMenu;
        oNavLevel[i].MenuCss = tmpMenuDisplayCssClass;
        oNavLevel[i].parentUrl = sParentPath;
        this._urlPageMap[tmpUrl] = {};
        oNavLevel[i].parent =  parentObj ;      
        this._urlPageMap[tmpUrl] = oNavLevel[i];
        switch (tmpNodeType) {
          case 'Page':
            //load the page details
            oNavLevel[i].page = tmpDefName;
            oNavLevel[i].hasSubMenu = false;
            this.BuildPageDefinition(tmpChildNode, tmpDefName, tmpUrl, tmpTitle, tmpMenuDisplayCssClass);
            break;
          case 'Book':
            var tmpDefPage = _xml.GetAttribute(tmpChildNode, "defaultPage");
            oNavLevel[i].page = tmpDefPage;
            oNavLevel[i].hasSubMenu = true;
            oNavLevel[i].children = [];
            this.BuildNavigation(tmpChildNode,oNavLevel[i].children,tmpUrl,oNavLevel[i]);
            break;
          default:
            break;
        }
      }
    }
    
  },
  BuildPageDefinition: function(oDom, pageDef, pageUrl, pageTitle, pageNavClass){
    var _xml = $ss.agentcore.dal.xml;
    this._pageDefinition[pageDef] = {}
    this._pageDefinition[pageDef].url = pageUrl;
    this._pageDefinition[pageDef].title = pageTitle;
    this._pageDefinition[pageDef].cssClassName = pageNavClass;
    if (oDom && oDom.hasChildNodes()) {
      //grid defintion to have only one child node
      var tmpGridDom = oDom.childNodes[0];
      var layoutGrid = _xml.GetAttribute(tmpGridDom, "htmlLayoutUri");
      var layoutGridLocation = _xml.GetAttribute(tmpGridDom, "markupId");
      this._pageDefinition[pageDef].gridLayout = layoutGrid;
      this._pageDefinition[pageDef].gridLayoutTo = layoutGridLocation;
      this._pageDefinition[pageDef].placeholder = [];
      for (var i = 0; tmpGridDom &&   i < tmpGridDom.childNodes.length; i++) {
        var placeHolderDom = tmpGridDom.childNodes[i];
        var tPlaceHolderId = _xml.GetAttribute(placeHolderDom, "markupId");
        this._pageDefinition[pageDef].placeholder[i] = {};
        this._pageDefinition[pageDef].placeholder[i].to = tPlaceHolderId;
        this._pageDefinition[pageDef].placeholder[i].snapins = [];
        for (var j = 0; placeHolderDom  && j < placeHolderDom.childNodes.length; j++) {
          var snapinDoms = placeHolderDom.childNodes[j];
          var snapinName = _xml.GetAttribute(snapinDoms, "snapinName");
          var methodName = _xml.GetAttribute(snapinDoms, "methodName");
          if (!methodName) 
            methodName = "index";
          this._pageDefinition[pageDef].placeholder[i].snapins[j] = {};
          this._pageDefinition[pageDef].placeholder[i].snapins[j].name = snapinName;
          this._pageDefinition[pageDef].placeholder[i].snapins[j].method = methodName;
          this._pageDefinition[pageDef].placeholder[i].snapins[j].inputparam = [];
          for (var k = 0; snapinDoms && k < snapinDoms.childNodes.length; k++) {
            var inputDom = snapinDoms.childNodes[k];
            var varName = _xml.GetAttribute(inputDom, "variableName");
            //var varType = _xml.GetAttribute(inputDom, "vType");
            var varValue = _xml.GetNodeText(inputDom);
            this._pageDefinition[pageDef].placeholder[i].snapins[j].inputparam[k] = {};
            this._pageDefinition[pageDef].placeholder[i].snapins[j].inputparam[k].name = varName;
            //this._pageDefinition[pageDef].placeholder[i].snapins[j].inputparam[k].type = varType;
            this._pageDefinition[pageDef].placeholder[i].snapins[j].inputparam[k].value = varValue;
            inputDom = null;
          }
          snapinDoms = null;
        }
        placeHolderDom = null;
      }
      tmpGridDom = null;
    }
  },
  GetPageDetails: function(sPageDef) {
    if(this._pageDefinition[sPageDef]) return this._pageDefinition[sPageDef];
    return null;
  },
  GetLayoutGrid: function(sGridPath) {
    if(this._layoutGridCache[sGridPath]) return this._layoutGridCache[sGridPath];
    var sGridAbsPath = 'file://'+$ss.GetLayoutAbsolutePath()+ sGridPath;
    var grid_text = "";
    try {
      grid_text = MVC.request(sGridAbsPath);
      this._layoutGridCache[sGridPath] = grid_text;
      return  this._layoutGridCache[sGridPath];
    } catch (e) {
      grid_text = "";
      return null;
    }
  },
  SetCurrentPage: function(sPageDef,uNavUri) {
    this._currentUrl = uNavUri;
    this._currentPage = sPageDef;
  },
  GetCurrentPage: function() {
    return { navurl: this._currentUrl, pageDef: this._currentPage};
  },
  SetRequestedUrl: function(sUrl){
    this._currentUrl = sUrl;
  },
  GetCurrentUrl: function(sUrl){
    return this._currentUrl ;
  },
  GetHeader: function() {
    return this._header;
  },
  GetFooter: function() {
    return this._footer;
  },
  GetMenu: function() {
    return this._menu;
  },
  GetUrlDetails: function(sUrl) {
    if(this._urlPageMap[sUrl]) return this._urlPageMap[sUrl];
  },
  GetNavTree: function(sUrl) {
    sUrl = this.GetUrl(sUrl); 
    //set the current url
    //this._currentUrl = sUrl;
    var sPaths = sUrl.split("/");
    var levels = [];
    var navObjectToSearch = this._navTree;
    var tmpPath = "";
    var tmpSearchObject = [];
    for(var i=1; i<sPaths.length; i++) {
      levels[i-1] = [];
      var count = 0;
      tmpPath = tmpPath + "/"+sPaths[i];
      for(var j=0; j<navObjectToSearch.length; j++ ) {
        if(navObjectToSearch[j].isMenuItem) {
          levels[i-1][count] = {};
          levels[i-1][count].title = navObjectToSearch[j].title;
          levels[i-1][count].cssClass = navObjectToSearch[j].MenuCss;
          levels[i-1][count].url = navObjectToSearch[j].url;
          if(navObjectToSearch[j].url == tmpPath) {
            levels[i-1][count].selected = true;
          } else {
            levels[i-1][count].selected = false;
          }
          count = count +1;
        }
        if (navObjectToSearch[j].url == tmpPath) {
          if(navObjectToSearch[j].hasSubMenu) {
            tmpSearchObject = navObjectToSearch[j].children;
          }
        }
      }
      navObjectToSearch = tmpSearchObject;
      
    }
    
   return levels ;
  },
  GetUrl: function(sUrl) {
    //TODO remove trailing "/" if there is anything 
    //TODO if /sa/adsdasd is there should it return /sa/home instead pf /home (the default)
    if(!sUrl) sUrl = this._defaultPath;
    var sUrl = sUrl.split("?")[0];
    if(sUrl === "/") sUrl = this._defaultPath;
    if(!this._urlPageMap[sUrl]) sUrl = this._defaultPath; 
    var isBook = this._urlPageMap[sUrl].hasSubMenu;
    try {
      if (isBook) {
        var sTempPage = this._urlPageMap[sUrl].page;
        sUrl = this._pageDefinition[sTempPage].url;
      }
    } catch (ex) { }
    if(!sUrl) sUrl = this._defaultPath;
    return sUrl;
  }
});


