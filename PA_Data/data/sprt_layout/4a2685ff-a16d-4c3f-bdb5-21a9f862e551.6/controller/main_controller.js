MainController = MVC.Controller.extend('main',
{

   logger: $ss.agentcore.log.GetDefaultLogger("main_controller_mini_bcont"),
   init: function() {
     this._super();
   }
},
{
	load: function(params){
    try {
      this.Class.dispatch("navigation", "LoadInitPage", params);
    } 
    catch (ex) {
      this.Class.logger.error("Navigate Click: Error in Dispatching to Navigation Controller %1%", ex.message);
      //Unhandled Error while loading initial page...close the bcont..
      window.external.hide();
      $ss.agentcore.utils.ui.CloseBcont();
    }
  },

  "#idTitleCloseButton click": function(params) {
    var bCanClose = $ss.agentcore.utils.ui.CanCloseBcont();
    
    if (bCanClose) {
    this.Class.logger.info("Close Button Click: Clicked The Close Button");
    try {
      window.external.hide();
      params.event.kill();
    } 
    catch (ex) {
      this.Class.logger.error("Agentcore Does not have Minimize Function");
    }
    $ss.agentcore.utils.ui.CloseBcont();
    }
    
    params.event.kill();
   },
   ".navigate click": function(params) {
     var url = $(params.element).attr("url");
     if(url) {
        params.requesturl = url;
        params.requestPath = $ss.agentcore.utils.GetRootFromURL(params.requesturl);
        params.requestQS = $ss.agentcore.utils.GetQSFromURL(params.requesturl);
        params.requestArgs = $ss.agentcore.utils.GetQSObjectFromURL(params.requesturl);
        try {
          this.Class.dispatch("navigation","index",params);
        } catch (ex) {
          this.Class.logger.error("Navigate Click: Error in Dispatching to Navigation Controller %1%", ex.message);
      }
     }
   }
      
});

