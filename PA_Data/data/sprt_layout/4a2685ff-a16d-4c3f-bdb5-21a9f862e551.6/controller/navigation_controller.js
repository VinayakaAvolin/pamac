NavigationController = MVC.Controller.extend('navigation', //there must be one div id "navigation" in the layout template html file
{
  pageDefObj: null,
  logger: $ss.agentcore.log.GetDefaultLogger("mini_navigation_controller"),
  Initialize: function(layoutBasePath){
    try {
      if (this.pageDefObj) 
        return this.pageDefObj;
      var defPageDefFile = "agent.page.def.xml";
      var pageDefFile = $ss.agentcore.dal.config.GetConfigValue("package", "page_def_to_use", defPageDefFile);
      var sFilePath = layoutBasePath + "\\"+pageDefFile;
      if(!$ss.agentcore.dal.file.FileExists(sFilePath)) {
        var bDefFileMissing = true;
        if(defPageDefFile !== pageDefFile) {
          //use default page def
          sFilePath = layoutBasePath + "\\"+defPageDefFile;
          if($ss.agentcore.dal.file.FileExists(sFilePath)) {
            bDefFileMissing = false;
          }
        }
        if(bDefFileMissing) {
          this.logger.fatal("Initialize: Page def file is missing and bcont will be closed %1% ", pageDefFile);
          if (!bMac) {
            window.external.hide();
          }
          $ss.agentcore.utils.ui.CloseBcont();
        }
      } 
      this.pageDefObj = new $ss.layout.LayoutModel(sFilePath);
    } 
    catch (ex) {
      this.logger.error("Initialize: Error While while initializing %1% file msg: %2%", sFilePath, ex.message);
    }
  },
  sCurrentRequestUrl: "",
  sCurrentRequestPath: "",
  GetServerUrl: function() {
    var ServerURL = $ss.agentcore.dal.config.GetContextValue("Paths:DirActions");
    var arr = ServerURL.split('\\');
    return arr[0] + "//" + arr[2];
  }
}, {
  index: function (params) {
    var requestUrl = params.requesturl;
    //if the same page request has been made do nothing just return
    params.requestArgs = $ss.agentcore.utils.GetQSObjectFromURL(params.requesturl);
    var isRefresh = params.requestArgs["refresh"] === "true";
    if (!isRefresh) {
      if (requestUrl === this.Class.sCurrentRequestUrl)
        return false;
    }
    this.Class.sCurrentRequestUrl = requestUrl;
    var layoutBasePath = $ss.GetLayoutAbsolutePath();
    var requestPath = this.Class.pageDefObj.GetUrl(requestUrl);
    //TODO get the request cgi params in e.x. /ab/cd?ssd=sdaadsa&dsda=dsads
    //if request path is same no need to update the Navigation
    if (requestPath !== this.Class.sCurrentRequestPath) {
      try {
        var menu = this.Class.pageDefObj.GetMenu();
        var navTree = this.Class.pageDefObj.GetNavTree(requestPath);
        this.render({
          to: menu.to,
          absolute_url: 'file://' + layoutBasePath + menu.file,
          locals: {
            navdata: navTree
          }
        });
        this.Class.sCurrentRequestPath = requestPath;
      }
      catch (ex) {

      }
    }
    //load the layout grid and create the placeholders ...
    var oPageInfo = this.Class.pageDefObj.GetUrlDetails(requestPath)
    if (oPageInfo) {

      var sPageId = oPageInfo.page;
      if (sPageId) {
        var oPageDetails = this.Class.pageDefObj.GetPageDetails(sPageId);
        if (!oPageDetails) {
          // Force close Bcont- no page details was found... 
          this.Class.logger.error("index: Page details not found .. closing minibcont");
          $ss.agentcore.utils.ui.CloseBcont();
          return false; //TODO log error.
        }
        var sGridLocation = oPageDetails.gridLayoutTo;
        var sGridFile = oPageDetails.gridLayout;
        var oGridHTml = this.Class.pageDefObj.GetLayoutGrid(sGridFile);
        if (oGridHTml && $("#" + sGridLocation).length) {
          //paste the html
          $("#" + sGridLocation).html(oGridHTml);
          var aPlaceHolders = oPageDetails.placeholder;
          for (var i = 0; aPlaceHolders && i < aPlaceHolders.length; i++) {
            var sPlaceHolderId = aPlaceHolders[i].to;
            var ePlaceholder = $("#" + sGridLocation + " #" + sPlaceHolderId);
            if (ePlaceholder.length) {
              var aSnapins = aPlaceHolders[i].snapins;
              for (var j = 0; aSnapins && j < aSnapins.length; j++) {
                var oSnapin = aSnapins[j];
                var sSnapinDivId = sPlaceHolderId + "_" + "sinst_" + j;
                $("#" + sGridLocation + " #" + sPlaceHolderId).append("<div id='" + sSnapinDivId + "'></div>");
                var sSnapinToLocation = "#" + sGridLocation + " #" + sPlaceHolderId + " #" + sSnapinDivId;
                var oInputParam = oSnapin.inputparam;
                params.toLocation = sSnapinToLocation;
                params.inInstanceParam = null;
                if (oInputParam) {
                  params.inInstanceParam = oSnapin.inputparam;
                }
                try {
                  //see if the required snapin is there or not ...


                  //dispatch to the right right snapin
                  if (oSnapin.name && oSnapin.method) {
                    this.Class.dispatch(oSnapin.name, oSnapin.method, params);
                  }
                }
                catch (ex) {

                  $ss.agentcore.utils.ui.CloseBcont();
                  this.Class.logger.error("index: MiniBcont and closing bcont.. Dispatch returned error for snapin %1% , method  %2% and error message %3% ", oSnapin.name, oSnapin.method, ex);
                  //log the error               
                }
                
                if(!bMac)
                {
                params.toLocation = "";
                params.inInstanceParam = null;
                oSnapin = null;
              }
                
              }
              aSnapins = null;
            }
          }
          aPlaceHolders = null;
        }
        oPageDetails = null;
      }
    } else {
      //page details was not found close bcont
      this.Class.logger.error("index: Page Info not found .. closing minibcont");
      $ss.agentcore.utils.ui.CloseBcont();
    }
    oPageInfo = null;
  },

  LoadInitPage: function (params) {


// Customization Starts - to create the BRANDNAME macro
	var brandname = $ss.agentcore.dal.config.GetConfigValue("global", "brandname", "");
	$ss.agentcore.dal.config.DefineCustomMacro("%BRANDNAME%", brandname);
// Customization Ends

  //initalize the page definition
    var bForceFirstRun = ($ss.agentcore.dal.config.GetConfigValue("firstrun", "force_first_run", "true") === "true");
    var bFirstRunHasBeenSeen = ($ss.agentcore.dal.config.GetConfigValue("firstrun", "FirstRunHasBeenSeen") === "true");
    if (bForceFirstRun) {
      if (!bFirstRunHasBeenSeen) {
        // First Run has not beeen seen so close the bcont..
        $ss.agentcore.utils.ui.CloseBcont();
        return;
      }
    }

    var layoutBasePath = $ss.GetLayoutAbsolutePath();
    this.Class.Initialize(layoutBasePath);
    //render the template
    try {
      this.render({
        to: 'idMainWindow',
        absolute_url: 'file://' + layoutBasePath + '\\views\\layout_template.html'
      });
    }
    catch (ex) {
      //could not load the main layout itself...
    }
    //load the header 
    try {
      var header = this.Class.pageDefObj.GetHeader();
      if (header.to && header.file) {
        this.render({
          to: header.to,
          absolute_url: 'file://' + layoutBasePath + header.file
        });
      }
    }
    catch (ex) {

    }
    //load the footer
    try {
      var footer = this.Class.pageDefObj.GetFooter();
      if (footer.to && footer.file) {
        this.render({
          to: footer.to,
          absolute_url: 'file://' + layoutBasePath + footer.file
        });
      }
    }
    catch (ex) {

    }

    $ss.helper.InitializeConnectCheck();

    var objCmdLine = $ss.agentcore.utils.GetObjectFromCmdLine();

    if (objCmdLine.trigger) {
      // in trigger

      params.requesturl = "/trigger?type=" + objCmdLine.trigger;
      params.requestPath = "/trigger"
      params.requestQS = "type=" + objCmdLine.trigger;
      params.requestArgs = objCmdLine;

    }
    else {
      if (objCmdLine.path) {
        params.requesturl = objCmdLine.path;
      }
      else {
        params.requesturl = params.requesturl || "/";
      }

      if (params.requesturl.substring(0, 1) !== "/") {
        params.requesturl = "/" + params.requesturl;
      }

      //params.requestPath = $ss.agentcore.utils.GetRootFromURL(params.requesturl);
      //params.requestPath = "/";
      params.requestQS = $ss.agentcore.utils.GetQSFromURL(params.requesturl);
      params.requestArgs = $ss.agentcore.utils.GetQSObjectFromURL(params.requesturl);
    }

    if(!bMac)
    {
    if (!objCmdLine.instancename) {
      var requestPath = this.Class.pageDefObj.GetUrl(params.requesturl);
      // Get the new instance name
      var instanceName = $ss.agentcore.utils.GetMiniBcontSpawnName(requestPath);
      var bOnTop = false;
      if (objCmdLine.trigger) bOnTop = true;
      // delegate
      this.LaunchNewBcont(bOnTop, instanceName);
      return;
    }
    }

    //    //Get the Query String/Url from the command line and pass it to the next
    //    params.requesturl = "/";
    this.index(params);

        //Customization starts - Reading registry value to enable or disable default scheduling of PM Optimizers
        var sRegRoot = "HKCU";
        var provider = $ss.agentcore.dal.config.GetProviderID();
        var user = $ss.agentcore.dal.config.ExpandSysMacro("%USER%");
        var prodName = $ss.agentcore.dal.config.ParseMacros("%PRODUCT%");
        var regPath = "Software\\SupportSoft\\ProviderList\\" + provider + "\\" + prodName + "\\users\\" + user + "\\ss_config\\global\\";
        var isSchedulingEnabled = $ss.agentcore.dal.registry.GetRegValue(sRegRoot, regPath, "Enable_Scheduling");
        var isEnabled = false;
        if (bMac) {
          if (isSchedulingEnabled != undefined && isSchedulingEnabled.toLowerCase() == "true")
            isEnabled = true;
        }else {
         isEnabled = isSchedulingEnabled.toLowerCase() == "true"; 
        }
        if (isEnabled) 
        // Add registry entries to set scheduling for performance optimizers entry under HKCU
            $ss.pmschedulerhelper.AddPMOptimizerScheduling();
        else
            $ss.pmschedulerhelper.RemovePMOptimizerScheduling();
        //Customization Ends
  },

  GetUrlPath: function (sUrl) {
    return this.pageDefObj.GetUrl(sUrl);
  },

  LaunchNewBcont: function (bOnTop, instanceName) {
    var newCmdLine = $ss.agentcore.utils.GetObjInstance().GetCmdLine();
    if (bOnTop) newCmdLine += " /topmost";
    if (instanceName) newCmdLine += " /instancename " + "\"" + instanceName + "\" ";
    $ss.agentcore.utils.RunCommand(newCmdLine, $ss.agentcore.constants.BCONT_RUNCMD_ASYNC);
    $ss.agentcore.utils.CloseBcont();
    return;
  }

});
var bMac = $ss.agentcore.utils.IsRunningInMacMachine();
