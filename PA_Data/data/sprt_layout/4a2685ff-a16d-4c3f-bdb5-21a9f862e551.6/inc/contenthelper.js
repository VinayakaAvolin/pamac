$ss = $ss ||
{};
$ss.helper = $ss.helper ||
{};
var connectStat = "";

(function(){
  $.extend($ss.helper, {
    CanCloseBcont: function() {
      var bCanClose = true;
      try {
        if (_busy.IsAnyItemBusy()) {
          var aMsgs = _busy.GetBusyItems();
          var sDisp = _utils.GlobalXLate("ss_glb_Quit_Question_begin") + "\n";
          for (var i = 0; i < aMsgs.length; i++) {
            sDisp = sDisp + aMsgs[i].message + "\n";
          }
          sDisp = sDisp + _utils.GlobalXLate("ss_glb_Quit_Question_end");
          bCanClose = confirm(sDisp);
        }
      } catch (e) {
        bCanClose = true;
      }
      return bCanClose;
    },

    InitializeConnectCheck: function () {
      _sHostName = _parseUrlRegEx.exec(_nwConnectPrimaryUrl)[3];

      //Start with the assumption that n/w connection is available;
      _databag.SetValue(_connectConst.NW_LASTSTATE_DB_KEY, _connectConst.NW_CONNECTION_STATUS.ONLINE);

      var interValTime = 0;
      try {
          interValTime = parseInt(_nwConnectCheckInterval);
      }
      catch (ex) {}
      $(document).oneTime(1000, "initialcheck", _helper.CheckConnectivity);
    },

    IsConnectionAvailable: function () {
      if (_useDefaultConnectivityCheck && (!_nwConnectCheckEnabled)) {
        return _inet.BasicConnectionTest(true, false, "default");
      }
      else {
        return (_databag.GetValue(_connectConst.NW_LASTSTATE_DB_KEY) === _connectConst.NW_CONNECTION_STATUS.ONLINE)
      }
    },

    CheckConnectivity: function () {//by default conenctivity check has been disabled so do not proceed
      try {
        if (_nWConnectBrowser === _constants.INET_BROWSER_FIREFOX) {
          _fFoxProxSetting = null;
          if (_inet.IsFirefoxProxyEnabled())  _fFoxProxSetting = _inet.GetFirefoxProxySettings();
          if (_fFoxProxSetting) {
              _fIEProxSetting = _inet.GetCurrentProxySettings();
              _inet.SetProxySettings(_fFoxProxSetting);
          }
        }

        connectStat = _connectConst.NW_CONNECTION_STATUS.ONLINE;
        var doSpoofTest = false;
        if (navigator.onLine === false) connectStat = _connectConst.NW_CONNECTION_STATUS.OFFLINE;
        else {
          var statusCode = _helper.HTTPAuthTest();
          //Considering Authentication needed or Proxy credentials needed as online case
          if (_inetErrorCodes[statusCode]) {
            // Error In Connection
            connectStat = _connectConst.NW_CONNECTION_STATUS.OFFLINE;
          }
          else {
            //Found Connection
            connectStat = _connectConst.NW_CONNECTION_STATUS.ONLINE;
            doSpoofTest = _nwspooftestEnabled;
          }
        }
        if (statusCode === 0 || statusCode === 12510) {
          _origDnsTimeOut = _nDNSTimeout;
          _nDNSTimeout = 2000;
          doSpoofTest = true;
          _http.AbortRequest();
        }
        else {
          _nDNSTimeout = _origDnsTimeOut;
        }
        if (_nWConnectBrowser === _constants.INET_BROWSER_FIREFOX) {
          if (_fFoxProxSetting) {
            _inet.SetProxySettings(_fIEProxSetting);
          }
        }
        return;
      }
      catch (ex) {}
    },

    HTTPAuthTest: function () {
      var proxyuser = "";
      var proxypwd = "";

      if (_useProxy) {
        var EncryptProxypName = _registry.GetRegValue(_constants.REG_TREE, _regBase, "ProxyUsername");
        if (EncryptProxypName != "")  proxyuser = _utils.DecryptHexToStr(EncryptProxypName);
        var EncryptProxypwd = _registry.GetRegValue(_constants.REG_TREE, _regBase, "ProxyPassword");
        if (EncryptProxypwd != "")  proxypwd = _utils.DecryptHexToStr(EncryptProxypwd);
      }
      var sUrl = _nwConnectPrimaryUrl;
      var statusCode = _http.Authenticate(sUrl, _localFile, _nwConnectTimeOut, "", "", proxyuser, proxypwd);
      return statusCode;
    }
  })

  var _utils = $ss.agentcore.utils;
  var _busy = $ss.agentcore.state.busy;
  var _helper = $ss.helper;
  var _parseUrlRegEx = /^(?:([A-Za-z]+):)?(\/{0,3})([0-9.\-A-Za-z]+)(?::(\d+))?(?:\/([^?#]*))?(?:\?([^#]*))?(?:#(.*))?$/;
  var _nwConnectCheckInterval = _config.GetConfigValue("status_connectivity_check", "net_check_interval", "50000");
  var _serverbaseurl = _config.ExpandAllMacros("%SERVERBASEURL%");
  var _nwConnectPrimaryUrl = _serverbaseurl;
  var _sHostName = "";
  var _useDefaultConnectivityCheck = (_config.GetConfigValue("status_connectivity_check", "use_default_connectivity_for_status", "false") == "true");
  var _nwConnectCheckEnabled = (_config.GetConfigValue("status_connectivity_check", "net_check_enabled", "true") === "true");
  var _nWConnectBrowser = _config.GetConfigValue("status_connectivity_check", "browser_to_be_tested", "Default");
  var _databag = $ss.agentcore.dal.databag;
  var _connectConst = {
    NW_LASTSTATE_DB_KEY: "NW_LAST_STATE_DB",
    NW_CURRENTSTATE_DB_KEY: "NW_CURRENT_STATE_DB",
    NW_CONNECTION_STATUS: {
      INPROGRESS: 2,
      ONLINE: 1,
      OFFLINE: 0
    },
    NW_CONNECTION_PAUSE_DB_KEY: "NW_FOOTER_CONNECTION_PAUSE",
    NW_CONNECTION_HIDE_DB_KEY: "NW_FOOTER_CONNECTION_HIDE"
  };

  var _inetErrorCodes = {
    12002: "ERROR_INTERNET_TIMEOUT",
    12005: "Malformed URL",
    12007: "ERROR_INTERNET_NAME_NOT_RESOLVED",
    12029: "ERROR_INTERNET_CANNOT_CONNECT",
    12030: "ERROR_INTERNET_CONNECTION_ABORTED",
    12031: "ERROR_INTERNET_CONNECTION_RESET",
    12152: "ERROR_HTTP_INVALID_SERVER_RESPONSE",
    12164: "ERROR_INTERNET_SERVER_UNREACHABLE",
    12159: "ERROR_INTERNET_TCPIP_NOT_INSTALLED",
    12163: "ERROR_INTERNET_DISCONNECTED",
    12510: "Error Problem",
    302: "MOVED",
    502: "BAD GATEWAY",
    503: "SERVICE UNAVAILABLE",
    504: "GATEWAY TIMEOUT",
    505: "HTTP Version Not Supported",
    402: "Payment Required",
    403: "Forbidden",
    400: "Bad Request",
    408: "Request Timeout",
    500: "Internal Server Error",
    501: "Not Implemented",
    0: "timeout",
    2: "Unkwnown Error"
  };
  var _nDNSTimeout = _config.GetConfigValue("status_connectivity_check", "DNSTimeout", 1000);
  var _origDnsTimeOut = _nDNSTimeout;
  var _useProxy;

  var _fFoxProxSetting = null;
  var _fIEProxSetting = null;

})();
