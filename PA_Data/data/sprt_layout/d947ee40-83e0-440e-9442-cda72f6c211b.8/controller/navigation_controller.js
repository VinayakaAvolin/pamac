NavigationController = MVC.Controller.extend('navigation', //there must be one div id "navigation" in the layout template html file
{
pageDefObj: null,
logger: $ss.agentcore.log.GetDefaultLogger("navigation_controller"),
Initialize: function (layoutBasePath) {

  try {
    if (this.pageDefObj)
      return this.pageDefObj;
    var defPageDefFile = "agent.page.def.xml";
    var pageDefFile = $ss.agentcore.dal.config.GetConfigValue("package", "page_def_to_use", defPageDefFile);
    var sFilePath = layoutBasePath + "\\" + pageDefFile;
    if (!$ss.agentcore.dal.file.FileExists(sFilePath)) {
      var bDefFileMissing = true;
      if (defPageDefFile !== pageDefFile) {
        //use default page def
        sFilePath = layoutBasePath + "\\" + defPageDefFile;
        if ($ss.agentcore.dal.file.FileExists(sFilePath)) {
          bDefFileMissing = false;
        }
      }
      if (bDefFileMissing) {
        this.logger.fatal("Initialize: Page def file is missing and bcont will be closed %1% ", pageDefFile);
        window.external.hide();
        $ss.agentcore.utils.ui.CloseBcont();
      }
    }
    this.pageDefObj = new $ss.layout.LayoutModel(sFilePath);
  }
  catch (ex) {
    this.logger.error("Initialize: Error While while initializing %1% file msg: %2%", sFilePath, ex.message);
  }
},
sCurrentRequestUrl: "",
sCurrentRequestPath: "",
sLastGridLocation: "",
sContentLocation: "",
oDefaultPage: null,
//oBreadCrumbHtml: {},
sCurrentPageId: "",
disableNav: false,
InitBcontDisplayed: true,
GetBreadCrumbHtml: function (sPageId, oAddBreadCrumb) {
  this.logger.info("GetBreadCrumbHtml: Build The Bread Crumb");
  sPageId = sPageId || this.sCurrentPageId;
  var pageBreadCrumbHTML = "";
  var tBObj = this.GetBreadCrumbObject(sPageId).slice();
  if (oAddBreadCrumb) {
    tBObj.push(oAddBreadCrumb)
  };
  for (var x = 0; tBObj && x < tBObj.length; x++) {
    try {
      var url = tBObj[x].url;
      var defName = tBObj[x].defName;
      var text = tBObj[x].text;
      if (text == undefined && defName != undefined) {
                    if (bMac) {
                        if (this.pageDefObj.GetPageDetails(defName)) {
        text = this.pageDefObj.GetPageDetails(defName).title || "";
                        }else {
                            text = "";
                        }
                    }else{
                        text = this.pageDefObj.GetPageDetails(defName).title || "";
                    }
      }
      text = text || "";
      if (x == (tBObj.length - 1)) {
        pageBreadCrumbHTML += "<span hidefocus=true>" + text + "</span>&nbsp;";
      }
      else {
        pageBreadCrumbHTML += "<a href='#' hidefocus=true class='navigate' url='" + url + "'>" + text + "</a>";
      }
    }
    catch (ex) {
      this.logger.error("GetBreadCrumbHtml: Error While Creating Breadcrumb %1%", ex.message);
    }
  }
  tBObj = null;
  ;
  return pageBreadCrumbHTML;

},

GetBreadCrumbObject: function (sPageId) {
  this.logger.info("GetBreadCrumbObject: Get The BreadcrumbObject for the page %1% ", sPageId);
  sPageId = sPageId || this.sCurrentPageId;
  if (this.pageDefObj.GetPageDetails(sPageId)) {
    return this.pageDefObj.GetPageDetails(sPageId).breadCrumb;
  };

},
UpdateBreadCrumbHTMLElement: function (oAddBreadCrumb) {
  return this.GetBreadCrumbHtml(this.sCurrentPageId, oAddBreadCrumb);
},
IsUrlValid: function (sUrl) {
  return this.pageDefObj.DoesUrlExist(sUrl);
},

GetServerUrl: function() {
  var ServerURL = $ss.agentcore.dal.config.GetContextValue("Paths:DirActions");
  var arr = ServerURL.split('\\');
  return arr[0] + "//" + arr[2];
}

}, {
  index: function (params) {
    $ss.agentcore.events.SendByName("SNAPIN_DO_CLEAN");
    this.Class.logger.info("index: Navigation Controller is being called ");

    var requestUrl = params.requesturl;
    params.requestPath = params.requestPath || $ss.agentcore.utils.GetRootFromURL(params.requesturl);
    params.requestQS = params.requestQS || $ss.agentcore.utils.GetQSFromURL(params.requesturl);
    params.requestArgs = params.requestArgs || $ss.agentcore.utils.GetQSObjectFromURL(params.requesturl);
    //if((params.requestArgs["refresh"] !== "yes")) {
    //if the same page request has been made do nothing just return
    // if (requestUrl === this.Class.sCurrentRequestUrl) return false;
    //}
    this.Class.sCurrentRequestUrl = requestUrl;

    var requestPath = this.Class.pageDefObj.GetUrl(params.requestPath);

    this.ResetNavigationButtons();
    //if request path is same no need to update the Navigation
    if (requestPath !== this.Class.sCurrentRequestPath) {
      this.RenderMenu(requestPath);
      this.Class.sCurrentRequestPath = requestPath;
    }
    this.ResetFooter();

    //load the layout grid and create the placeholders ...
    if (this.Class.sContentLocation) {
      $(this.Class.sContentLocation).empty();
    }

    var oPageInfo = this.Class.pageDefObj.GetUrlDetails(requestPath);
    if (oPageInfo) {
      var sPageId = oPageInfo.page;
      if (sPageId) {
        //set the current page id to the member variable
        this.Class.sCurrentPageId = sPageId;
        var oPageDetails = this.Class.pageDefObj.GetPageDetails(sPageId);
        if (!oPageDetails) {
          this.Class.logger.error("index: Page Details Not Found For page %1% ", sPageId);
          return false;
        }

        var sGridLocation = oPageDetails.gridLayoutTo;
        var sGridFile = oPageDetails.gridLayout;
        var oGridHTml = this.Class.pageDefObj.GetLayoutGrid(sGridFile);

        if (oGridHTml && $("#" + sGridLocation).length) {
          //paste the html
          //document.getElementById(sGridLocation).innerHTML=oGridHTml;
          $("#" + sGridLocation).html("");
          $("#" + sGridLocation).html(oGridHTml);
          oGridHTml = null;
          //check for the breadcrumb and update the breadcrumb;
          var breadCrumbHTML = this.Class.GetBreadCrumbHtml();
          if (breadCrumbHTML) {
            $("#breadcrumb").html(breadCrumbHTML);
          }
          breadCrumbHTML = null;
          this.Class.sContentLocation = "#" + sGridLocation;
          if (this.Class.InitBcontDisplayed && this.Class.InitatedByTrigger) {
            this.ShowBcontForFirstTime();
          }

          var aPlaceHolders = oPageDetails.placeholder;
          for (var i = 0; aPlaceHolders && i < aPlaceHolders.length; i++) {
            var sPlaceHolderId = aPlaceHolders[i].to;
            if ($("#" + sGridLocation + " #" + sPlaceHolderId).length) {
              var aSnapins = aPlaceHolders[i].snapins;
              for (var j = 0; aSnapins && j < aSnapins.length; j++) {
                var oSnapin = aSnapins[j];
                var sSnapinDivId = sPlaceHolderId + "_" + "sinst_" + j;
                if (j == 0) {
                  $("#" + sGridLocation + " #" + sPlaceHolderId).html("<div id='" + sSnapinDivId + "'></div>");
                }
                else {
                  $("#" + sGridLocation + " #" + sPlaceHolderId).append("<div id='" + sSnapinDivId + "'></div>");
                }
                var sSnapinToLocation = "#" + sGridLocation + " #" + sPlaceHolderId + " #" + sSnapinDivId;
                var oInputParam = oSnapin.inputparam;
                params.toLocation = sSnapinToLocation;
                params.inInstanceParam = null;
                if (oInputParam) {
                  params.inInstanceParam = oSnapin.inputparam;
                }
                try {
                  //dispatch to the right right snapin
                  this.Class.logger.info("index: rendering snapin %1% , method  %2% and to location %3% ", oSnapin.name, oSnapin.method, params.toLocation);
                  if (oSnapin.name && oSnapin.method) {
                    this.Class.dispatch(oSnapin.name, oSnapin.method, params);
                  }
                }
                catch (ex) {
                  this.Class.logger.error("index: Class Dispatch returned error for snapin %1% , method  %2% and error message %3% ", oSnapin.name, oSnapin.method, ex);
                  //log the error               
                }
if(!bMac)
{
                params.toLocation = "";
                params.inInstanceParam = null;
                oSnapin = null;
              }
              }
              aSnapins = null;
            }
          }
          aPlaceHolders = null;
        }
        else {
          this.Class.logger.error("index: Grid Definition Could not be rendered for file %1% to %2% ", sGridFile, sGridLocation);
        }
        oPageDetails = null;
      }
    }
    delete oPageInfo;
    //FIX THE PNGS
    $ss.agentcore.utils.ui.FixPng();
  },
  RenderMenu: function (requestPath) {
    var layoutBasePath = $ss.GetLayoutAbsolutePath();
    try {
      var menu = this.Class.pageDefObj.GetMenu();
      var navTree = this.Class.pageDefObj.GetNavTree(requestPath);
      var headerItems = this.Class.pageDefObj.GetGroupDetails("header");
      this.Class.logger.info("RenderMenu: Rendering Menu for menu file %1% ", menu.file);
      if (menu.to && menu.file) {
        this.render({
          to: menu.to,
          absolute_url: 'file://' + layoutBasePath + menu.file,
          locals: {
            navdata: navTree,
            header: headerItems,
            cUrl: requestPath,
            disableNav: this.Class.disableNav
          }
        });
      }
      menu = null;
      navTree = null;
      headerItems = null;
    }
    catch (ex) {
      this.Class.logger.error("RenderMenu: Rendering Menu Throws Error for menu file %1% with msg %2%", menu.file, ex);
    }
  },
  LoadInitPage: function (params) {
    //initalize the page definition
    this.Class.logger.info("LoadInitPage: Entering Function From MainController");

        // Customization Starts - to create the BRANDNAME macro
        var brandname = $ss.agentcore.dal.config.GetConfigValue("global", "brandname", "");
        $ss.agentcore.dal.config.DefineCustomMacro("%BRANDNAME%", brandname);
        // Customization Ends

    var layoutBasePath = $ss.GetLayoutAbsolutePath();
    this.Class.Initialize(layoutBasePath);
    this.RenderLayoutTemplate();
    this.RenderHeader();
    this.RenderFooter();
    $ss.helper.InitializeConnectCheck();
    $ss.helper.InitializeNavigation();
    
    //updates user preffered language under HKLM
   //Vinayaka - Commented while merging to 9.3.1
    // $ss.helper.UpdateLangPref();
    //updates user preffered language under HKCU.
    $ss.helper.UpdateLocalUserPrefLang();
    
    var objCmdLine = $ss.agentcore.utils.GetObjectFromCmdLine();
    var hiddenConfig = $ss.agentcore.dal.ini.GetIniValue("", "SETUP", "starthidden", "0");
    var isBcontStartHidden = objCmdLine.starthidden || (hiddenConfig === "1");
    if (!isBcontStartHidden) {
      this.Class.InitBcontDisplayed = true;
      if (objCmdLine["entry"] === "Trigger Minibcont") {
        this.Class.InitatedByTrigger = true;
        //do not allow network check...
        $ss.agentcore.events.SendByName("DISABLE_HIDE_NW_CHECK");
      }
    }

    //Get the Query String/Url from the command line and pass it to the next
    var objCmdLine = $ss.agentcore.utils.GetObjectFromCmdLine();
    if (objCmdLine.path) {

      params.requesturl = objCmdLine.path;
      if (params.requesturl && params.requesturl.substring(0, 1) !== "/") {
        params.requesturl = "/" + params.requesturl;
      }
      params.requestQS = $ss.agentcore.utils.GetQSFromURL(params.requesturl);
      params.requestArgs = $ss.agentcore.utils.GetQSObjectFromURL(params.requesturl);
      this.Class.logger.info("LoadInitPage: Requested Page Path %1% Called From command fline", params.requesturl);
    }
    else {
      params.requesturl = params.requesturl || "/";
      this.Class.logger.info("LoadInitPage: Requested Page Path %1% Called From requested URL", params.requesturl);
    }

        //GS Customization Start  

        // Engineering bug - First time when we click on Download Manager Tab, it alerts an unnecessary message as below:
        //"click on OK to close the notification" and also pops up the mini-bcont
        //Fix - Add PopUpShown entry under "HKCU\Software\SupportSoft\ProviderList\<provider>\Download Manager\MODULE"
        $ss.helper.AddDownloadPopupShownEntryUnderHKCU();

        //Snapins/urls to excluded (used during multiple launch of FixOmatic)
        var aExcludedURLs = $ss.agentcore.dal.config.GetValues("multipleinstancebcont", "excluded_urls");
        var bLaunchAgent = true;
        for (var i = 0; i < aExcludedURLs.length; i++) {
            if (params.requesturl.indexOf(aExcludedURLs[i]) >= 0) {
                bLaunchAgent = false;
                break;
            }
        }

        try {
            var isIgnoreSingle = objCmdLine.ignoresingle || (isIgnoreSingle === "1");
            var isBcontStartHidden = objCmdLine.starthidden || (hiddenConfig === "1");
            if (isIgnoreSingle && isBcontStartHidden) {
                bLaunchAgent = false;
            }

        } catch (ex) {
        }
        if (bLaunchAgent) {
            //Allowing agent launching from multiple user login
           if(!bMac)
{
            $ss.helper.LaunchAgent();
        }
        }
        //GS Customization End
    var bForceFirstRun = ($ss.agentcore.dal.config.GetConfigValue("firstrun", "force_first_run", "true") === "true");
    var bFirstRunHasBeenSeen = ($ss.agentcore.dal.config.GetConfigValue("firstrun", "FirstRunHasBeenSeen") === "true");
    var startSprtCmd = false;
    var isFirstRunIncluded = $ss.GetSnapin("snapin_firstrun") ? true : false;

    if (bForceFirstRun && isFirstRunIncluded) {
      var bCompletedFirstRun = ($ss.agentcore.dal.config.GetConfigValue("firstrun", "FirstRunCompleted") === "true");
      if (!bFirstRunHasBeenSeen && bCompletedFirstRun) {
        //set the FirstRunHasBeenSeen to true
        $ss.agentcore.dal.config.SetMachineValue("firstrun", "FirstRunHasBeenSeen", true);
      }
      if (bFirstRunHasBeenSeen) {
        bCompletedFirstRun = true;
      }
      if (!bFirstRunHasBeenSeen && !bCompletedFirstRun && isBcontStartHidden)
        $ss.agentcore.utils.ui.CloseBcont();

      if (!bCompletedFirstRun) {
        params.requesturl = $ss.agentcore.dal.config.GetConfigValue("preconfigured_snapin_url", "dynamicagent_firstrun", "/firstrun");
      } else {
        var bCompletedAccountInfo = true;
        var bForceAcctInfo = ($ss.agentcore.dal.config.GetConfigValue("firstrun", "force_account_information") === "true");
        if (bForceAcctInfo) {
          var bAccountCollected = ($ss.agentcore.dal.config.GetConfigValue("firstrun", "AccountInformationCollected") === "true");
          if (!bAccountCollected) {
            bCompletedAccountInfo = false;
          }
        }
        var bForceAcctVer = ($ss.agentcore.dal.config.GetConfigValue("firstrun", "force_account_verification") === "true");
        if (bForceAcctVer) {
          var bAcctVerified = ($ss.agentcore.dal.config.GetConfigValue("firstrun", "AccountInformationVerified") === "true");
          if (!bAcctVerified) {
            bCompletedAccountInfo = false;
          }
        }

        if (!bCompletedAccountInfo) {
          params.requesturl = $ss.agentcore.dal.config.GetConfigValue("preconfigured_snapin_url", "dynamicagent_firstrun", "/firstrun");
        }
        else {
          startSprtCmd = true;
        }
      }
    } else {
      startSprtCmd = true;
      if (!bForceFirstRun && !bFirstRunHasBeenSeen) {
        //set the FirstRunHasBeenSeen to true
        $ss.agentcore.dal.config.SetMachineValue("firstrun", "FirstRunHasBeenSeen", true);
        //added to ensure DM sprocket (dmmonitor.dll) starts Silent DO's without waiting for First Run to be completed
        $ss.agentcore.dal.config.SetUserValue("firstrun", "FirstRunCompleted", "true");
      }
    }
    if (startSprtCmd) {
      //put a delay for this asynchronous call ..
      setTimeout("$ss.agentcore.utils.StartSprtCmd(true)", 2000);
    }

    params.requestPath = $ss.agentcore.utils.GetRootFromURL(params.requesturl);

if(!bMac)
{
    if (!this.Class.IsUrlValid(params.requestPath) && isBcontStartHidden) $ss.agentcore.utils.ui.CloseBcont();
}

    var that = this;
    that.params = params;
    // Using the setTimeout to reduce startup delay     
    setTimeout(function () {
      that.index(that.params);
      that.ShowBcontForFirstTime();
    }, 50);
    //this.index(params);
    //this.ShowBcontForFirstTime();
  },
  GetUrlPath: function (sUrl) {
    return this.Class.pageDefObj.GetUrl(sUrl);
  },
  ResetFooter: function () {
    if ($ss.helper && $ss.helper.ShowFooterConnect)
      $ss.helper.ShowFooterConnect();
  },
  RenderLayoutTemplate: function () {
    var layoutBasePath = $ss.GetLayoutAbsolutePath();
    //render the template
    try {
      this.render({
        to: 'idMainWindow',
        absolute_url: 'file://' + layoutBasePath + '\\views\\layout_template.html'
      });
    }
    catch (ex) {

      this.Class.logger.error("LoadInitPage: rendering layout problem msg %1% %2%", ex, ex.message);
      //could not load the main layout itself...
    }
  },
  ResetNavigationButtons: function () {

    if (this.Class.disableNav === true) {
      this.Class.disableNav = false;
      this.RenderHeader();
    }

  },
  DisableNavigation: function () {
    this.Class.disableNav = true;
    this.RenderHeader();
    this.RenderMenu("/somearbitarypath");
  },
  RenderHeader: function () {
    var layoutBasePath = $ss.GetLayoutAbsolutePath();
    //load the header 
    try {
      var header = this.Class.pageDefObj.GetHeader();
      if (header.to && header.file) {
        this.render({
          to: header.to,
          absolute_url: 'file://' + layoutBasePath + header.file,
          locals: {
            disableNav: this.Class.disableNav
          }
        });
      }
      header = null;
    }
    catch (ex) {
      this.Class.logger.error("LoadInitPage: rendering header %1% msg %2% %3%", header.file, ex, ex.message);
    }
  },
  RenderFooter: function () {
    var layoutBasePath = $ss.GetLayoutAbsolutePath();
    //load the footer
    try {
      var footer = this.Class.pageDefObj.GetFooter();
      if (footer.to && footer.file) {
        this.render({
          to: footer.to,
          absolute_url: 'file://' + layoutBasePath + footer.file,
          locals: {
            disableNav: this.Class.disableNav
          }
        });
      }
      footer = null;
    }
    catch (ex) {

      this.Class.logger.error("LoadInitPage: rendering footer %1% msg %2% %3%", footer.file, ex, ex.message);
    }
  },
  ShowBcontForFirstTime: function () {
    //allow this sleep to remove the splash screen.
    if (this.Class.InitBcontDisplayed) {
      $ss.agentcore.utils.ui.ShowDAgentFirstTimeIfDAHidden();
      this.Class.InitBcontDisplayed = true;
    }

  }
});
var bMac = $ss.agentcore.utils.IsRunningInMacMachine();