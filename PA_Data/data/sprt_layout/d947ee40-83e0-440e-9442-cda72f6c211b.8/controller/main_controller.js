MainController = MVC.Controller.extend('main', {
  logger: $ss.agentcore.log.GetDefaultLogger("main_controller"),
  urlCheckRegEx: /^(?:([A-Za-z]+):)?(\/{0,3})([0-9.\-A-Za-z]+)(?::(\d+))?(?:\/([^?#]*))?(?:\?([^#]*))?(?:#(.*))?$/,
  init: function () {
    this.logger.info("Init: Initalized the Controller");
    this._super();
  },
  checkConnectionOnClickLink: ($ss.agentcore.dal.config.GetConfigValue("status_connectivity_check", "do_check_connection_onclick_exlink", "true") === "true"),

  CheckEventType: new RegExp("(.*?)\\s?(click|dblclick|keydown|keyup|keypress|select|submit)$"),
  AllowedElements: new RegExp("(.*?)\\s?(idTitleCloseButton|idTitleMinimizeButton)$"),
  CheckPropagation: function (ev) {
    //This is called by the MVC dispatcher to see if the shell is busy. If the shell is busy, it will not allow
    //user to proceed further.
    var evType = ev.type;
    if (!$ss.agentcore.utils.ui.ShellBusy()) {
      return true;
    }
    var isAllowed = false;
    var parents_path = MVC.Delegator.node_path(ev.target);
    for (var x = 0; x < parents_path.length; x++) {
      if (this.AllowedElements.test(parents_path[x].id)) {
        isAllowed = true;
      }
    }
    return isAllowed;
  }
}, {
  load: function (params) {
    var that = this;
    this.Class.logger.info("Load: Loading the Main Controller");
    try {
      window.external.EnableSkinnedWindow();
    }
    catch (ex) {
      this.Class.logger.error("Error in Calling EnableSkinnedWindow %1%", ex.message);
    }
    try {
      this.Class.dispatch("navigation", "LoadInitPage", params);
    }
    catch (ex) {
      this.Class.logger.error("Navigate Click: Error in Dispatching to Navigation Controller %1%", ex.message);
    } finally {

    }

    // This kills the tab press or CTRL+O, CTRL+P and CTRL+L
    // The attachEvent is used to make sure that the onkeydown function is not overwritten
    try {
      document.attachEvent("onkeydown", function (e) {
        var e = e || event

        if (e.keyCode == 9 || 				       // TAB key code is 9, cancel event
           (e.ctrlKey && e.keyCode == 79) ||  // CTRL+O "letter O" key code is 79, cancel event
           (e.ctrlKey && e.keyCode == 76))   // CTRL+L "letter L" key code is 76, cancel event
        {
          e.keyCode = 0;
          e.cancelBubble = true;
          e.returnValue = false;
          return true;
        }
      });
    } catch (e) {
      if (!bMac) {
      alert(e.message);
    }
    }

    if(NavigationController.IsUrlValid("/search")){
      var localparam = {};
      localparam.searchTextArea = '#search-text';
      localparam.AutoComplete = true;
      this.Class.dispatch("snapin_search", "initSearch", localparam);
      localparam = null;
    }
    
    if(NavigationController.IsUrlValid("/chat")){
      $('#chatOverlay').popover({
        content : "<div id = 'chat_container'></div>",
        html: true
      });
      $('.chatWidget').on('bringToFront',function(){
          $(this).click();
      });
      // subscribing to chat click event here
      $("#chatOverlay").on("click", function(params){
        var activeOnChat = $(this).attr('activeOnChat');
        params.activeOnChat = activeOnChat;
        params.requesturl = "/chat";
        params.chatContainer ='#chatOverlay';
        params.toLocation ='#chat_container';

        if (!/true/i.test(activeOnChat)) {
            that.Class.dispatch("snapin_chat", "showChatPage", params);
        } else {
            that.Class.dispatch("snapin_chat", "hideChatPage", params);
        }
        var act = activeOnChat == "true" ? "false" : "true";
        $(this).attr('activeOnChat', act);
      });
      $("#chatOverlay").on("tryRecon", function(params){
        params.chatContainer ='#chatOverlay';
        params.toLocation ='#chat_container';
        params.requesturl = "/chat";
        that.Class.dispatch("snapin_chat", "reconnMe",params);
      });

      $("#chatOverlay").on("online", function(params) {
        $('#chatImg').attr('src',$ss.GetLayoutSkinPath()+'\\icons\\chat_online.png');
      });
      $("#chatOverlay").on("offline", function(params){
        $('#chatImg').attr('src',$ss.GetLayoutSkinPath()+'\\icons\\chat_offline.png');
      });
      $('#chatOverlay').on("notifyMe", function(){
        $('#chatImg').attr('src',$ss.GetLayoutSkinPath()+'\\icons\\chat_notify.png');
      });
      $('#chatOverlay').on("notify_new_msg", function(){
        if(/true/i.test($(this).attr('activeOnChat'))) return;
        $('#chatImg').attr('src',$ss.GetLayoutSkinPath()+'\\icons\\chat_notify.png');
      });
    }
    $ss.helper.GetViewableFolderGuidList(); 
  },

  "#idTitleCloseButton click": function (params) {
    var bCanClose = $ss.agentcore.utils.ui.CanCloseBcont();
    if (bCanClose) {
      this.Class.logger.info("Close Button Click: Clicked The Close Button");
      try {
        window.external.hide();
        params.event.kill();
      }
      catch (ex) {
        this.Class.logger.error("Agentcore Does not have Minimize Function");
      }
      $ss.agentcore.utils.ui.CloseBcont();
    }
  },

  "#idSideMenuToggle click": function(params) {
    var $wrapper = $("#wrapper");
    var $poweredBy = $('#poweredBy');
    var $buildSyncInfo = $('#buildSyncInfo');
    
    $wrapper.toggleClass("toggled-2");
    if ($wrapper[0].className === "toggled-2") {
      isSideBarCollapsed = true;
      $poweredBy.hide();
      $buildSyncInfo.hide();
    } else {
      isSideBarCollapsed = false;
      $poweredBy.show();
      $buildSyncInfo.show();
    }

    //$('#menu ul').hide();
    params.event.kill();
  },

  "#idTitleMinimizeButton click": function (params) {
    this.Class.logger.info("Minimize Button Click: Clicked The min Button");
    try {
      if (bMac) {
            var message = {'JSClassNameKey':'UIUtility','JSOperationNameKey':'MinimizeAgentWindow', 'JSArgumentsKey':[], 'JSISSyncMethodKey' : '0'}
            jsBridge.execute(message);
          }else {
      window.external.MinimizeAgentWindow();
    }
    }
    catch (ex) {
      this.Class.logger.error("Agentcore Does not have Minimize Function");
      try {
        var min = _utils.CreateObject("wscript.shell");
        min.SendKeys("% n");
      }
      catch (ex) {
      }
      min = null;
    }

  },
  ".navigate click": function (params) {
    var url = $(params.element).attr("url");
    this.Class.logger.info("Navigate Click: User has been redirected to the URL %1%", url);
    if (url) {
      params.requesturl = url;
      params.requestPath = $ss.agentcore.utils.GetRootFromURL(params.requesturl);
      params.requestQS = $ss.agentcore.utils.GetQSFromURL(params.requesturl);
      params.requestArgs = $ss.agentcore.utils.GetQSObjectFromURL(params.requesturl);
      params.requestCategory = $(params.element).attr("sCategory");
      
      try {
        this.Class.dispatch("navigation", "index", params);
      }
      catch (ex) {
        this.Class.logger.error("Navigate Click: Error in Dispatching to Navigation Controller %1%", ex.message);
      }

    }
  },
  "a click": function (params) {
    var aHref = params.element.href;
      
    //GS Customization start - Functionaliy ??      
    var className = params.element.className || "";
    var values = this.Class.urlCheckRegEx.exec(aHref);
    var protocol = (values && values.length > 2) ? values[1] : "file";
    if (protocol.toLowerCase() === "http" || protocol.toLowerCase() === "https" || protocol.toLowerCase() === "ftp") {
      var connectStatus = true;
      if (this.Class.checkConnectionOnClickLink) {
        connectStatus = $ss.agentcore.network.inet.GetConnectionStatus();
      }
      if (connectStatus) {
        var retVal = $ss.agentcore.utils.ui.RunDefaultBrowser(aHref);
        if (retVal == -1) {
          var alertText = $ss.agentcore.utils.GlobalXLate("glb_browser_disabled");
          if (alertText !== "glb_browser_disabled") {
            alert(alertText);
          }
        }
        params.event.kill();
        return false;
      }
      else {
        var alertText = $ss.agentcore.utils.GlobalXLate("glb_site_unreachable");
        if (alertText !== "glb_site_unreachable") {
          alert(alertText);
        }
        params.event.kill();
        return false;
      }
    }

  },

  "#search-text keydown":function(params){
    if (event.keyCode == 13) {
      $("#search-btn").focus().click();
    }
  },
  
  "#search-text focusout":function() {
    if($("#search-text").val().length > 0) {
      $("#idClearIcon").removeClass("hideClearIcon").addClass('showClearIcon');
    }
    else {
      $("#idClearIcon").removeClass("showClearIcon").addClass("hideClearIcon");
    }
  }
});
var isSideBarCollapsed = false; //default it is not collapsed

setTimeout(function(){
 $("#chatOverlay").trigger('tryRecon');
},2000);
var oContentcountDict = {};
var aViewablefolderguidlist= [];
var bMac = $ss.agentcore.utils.IsRunningInMacMachine();
var jsBridge = window.JSBridge;