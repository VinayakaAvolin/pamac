$ss = $ss ||
{};
$ss.helper = $ss.helper ||
{};
var connectStat = "";
var bstatus = false;
(function () {
    $.extend($ss.helper, {
      ReplaceResInContent: function (sContent) {
        if (!sContent)
          return "";
        var matches = sContent.match(_imgRegEx);
        var rPath = null;
        if (matches) {
          for (var x = 0; x < matches.length; x++) {
            rPath = null;
            var imagePath = matches[x];
            var rObj = _resRegEx.exec(imagePath);
            if (rObj) {
              var resType = rObj[1];
              var resGUID = rObj[2];
              if (resType === "sprt_resource") {
                rPath = $ss.agentcore.dal.content.GetResource(resGUID);
                sContent = sContent.replace(imagePath, rPath);
              }
            }
          }
        }
        return sContent;
      },
      UpdateLangPref: function(){
        var providerID = $ss.agentcore.dal.config.GetProviderID();
        var regPath = "SOFTWARE\\SupportSoft\\ProviderList\\"+ providerID +"\\SubAgent\\ss_config\\global\\";
        
        var lastLang = _utils.GetLanguage().substring(0,2);
        _registry.SetRegValue("HKLM", regPath, "language", lastLang);
      },
	  
      UpdateLocalUserPrefLang:function(){
        if(_config.GetUserValue("global", "language") === "undefined"){
          _config.SetUserValue("global", "language", "en");
        }
      },
      
      RefreshPageFromTrayIcon: function (oEvent) {
        //if shell busy get out of it
        try {
          if ($ss.agentcore.utils.ui.ShellBusy())
            return;
          var firstRunPath = $ss.agentcore.dal.config.GetConfigValue("preconfigured_snapin_url", "dynamicagent_firstrun", "/firstrun");
          var currentPath = NavigationController.sCurrentRequestPath;
          //if in firstRun Do not Allow
          if (firstRunPath === currentPath)
            return;
          var cmdLine = oEvent.description;
          //crude way of handling it
          var cmdLineRegEx = /\/\w+\s+"([^"]*)"/g;
          var aCmdLine = cmdLine.match(cmdLineRegEx);
          var itemRegEx = /(\/\w+)\s+"([^"]*)"/;
          var reQuestedPath = "";
          var isFromSysIcon = false;

          for (var iCnt = 0; iCnt < aCmdLine.length; iCnt++) {
            var oC1 = aCmdLine[iCnt];
            var oItems = itemRegEx.exec(oC1);
            if (oItems.length >= 3) {
              var p1 = oItems[1];
              var p2 = oItems[2];
              if (p1.substr(0, 1) === "/") {
                p1 = p1.replace("\/", "");
                switch (p1) {
                  case "path":
                    reQuestedPath = p2;
                    break;
                  case "entry":
                    if (p2 === "Tray Icon") {
                      isFromSysIcon = true;
                    }
                    break;
                }
              }
            }
          }
          //if not initiated from Tray Icon do not do anything
          if (!isFromSysIcon || reQuestedPath === "")
              return;
          var allowSwitch = $ss.agentcore.dal.config.GetConfigValue("shell", "allow_single_instance_snapinswitch", "true");
          if (allowSwitch !== "true")
              return false;
          var params = {};
          params.requesturl = reQuestedPath;
          params.requestPath = $ss.agentcore.utils.GetRootFromURL(reQuestedPath);
          params.requestQS = $ss.agentcore.utils.GetQSFromURL(reQuestedPath);
          params.requestArgs = $ss.agentcore.utils.GetQSObjectFromURL(reQuestedPath);
          MVC.Controller.dispatch("navigation", "index", params);
        }
        catch (ex) {}
      }
    ,
      DisableNavigationItem: function () {
          MVC.Controller.dispatch("navigation", "DisableNavigation", null);
      },
      InitializeNavigation: function () {
          $ss.agentcore.events.Subscribe("DISABLE_NAVIGATION", $ss.helper.DisableNavigationItem);
      },
      InitializeConnectCheck: function () {

          //GS Customization start- for Hostname
          _sHostName = _parseUrlRegEx.exec(_nwConnectPrimaryUrl)[3];
          //GS Customization end

          //Start with the assumption that n/w connection is available;
          _databag.SetValue(_connectConst.NW_LASTSTATE_DB_KEY, _connectConst.NW_CONNECTION_STATUS.ONLINE);
          if (_nwConnectCheckEnabled === false) {
              $("#footer_connect_status").hide();
              return false;
          }
          var interValTime = 0;
          try {
              interValTime = parseInt(_nwConnectCheckInterval);
          }
          catch (ex) {

          }
          if (interValTime > 0) {
              $(document).everyTime(interValTime, "nw_check_polling", _helper.CheckConnectivity);
          }
          $(document).oneTime(1000, "initialcheck", _helper.CheckConnectivity);
          //_helper.CheckConnectivity();
      },

      CheckSyncStatus: function () {
        return AmIOnlineAndLatest;
      },

      //GS Customization start: Get Sync details and display on the Navigation page
      CheckSync: function () {
          var syncInterValTime = 0;
          syncInterValTime = parseInt(_SyncCheckInterval);
        if (syncInterValTime > 0) $(document).everyTime(syncInterValTime, "sync_check", _helper.GetUpdate);
        $(document).oneTime(1000, "sync_check", _helper.GetUpdate);
      },
      
      GetUpdate: function() {
        //AmIOnlineAndLatest :
        //1 - User is Online and Sync is Done. -------ONLINE
        //2 - User is Online and Sync is not Done. ---ERROR
        //3 - User is Offline. -----------------------OFFLINE
      
        var bIsOnline = _helper.GetOnlineStatus();
        var bSyncStatus = _helper.GetSyncStatus();
        if (!bIsOnline) AmIOnlineAndLatest = "3";
        else AmIOnlineAndLatest = (bSyncStatus) ? "1" : "2";
        _helper.UpdateUI();
      },
      
      UpdateUI: function(){
      if(AmIOnlineAndLatest == "1") {
        // The User is Online and Sync is Successfull.
        $("#TabOnline").show();
        $("#TabOffline").hide();
        $("#TabError").hide();
      }
      if(AmIOnlineAndLatest == "2") {
        // The User is Online but Sync is not Successfull. - ERROR
        $("#TabOnline").hide();
        $("#TabOffline").hide();
        $("#TabError").show();
      }
      if(AmIOnlineAndLatest == "3") {
        // The User is Offline.
        $("#TabOnline").hide();
        $("#TabOffline").show();
        $("#TabError").hide();
          }
      },

    GetOnlineStatus: function () {
      try {
        //checking wheather the connection is available or not.
        _helper.CheckConnectivity();
        if (connectStat == 1) return true;
        return false;
      }
      catch (ex) {
        return false;
      }
    },

      GetSyncStatus: function () {
          try {
              var sRegRoot, sRegKey, sRegVal, providerID, prodName, sShowSyncInfo, sUser, sLastUpadteErr, sSyncBitDownloadState;
              sRegRoot = "HKLM";
              providerID = $ss.agentcore.dal.config.GetProviderID();
              prodName = $ss.agentcore.dal.config.ParseMacros("%PRODUCT%");
              sUser = $ss.agentcore.dal.config.ExpandSysMacro("%USER%");
              sRegKey = "Software\\SupportSoft\\ProviderList\\" + providerID + "\\SubAgent\\" + prodName;
              sShowSyncInfo = $ss.agentcore.dal.registry.GetRegValue(sRegRoot, sRegKey, "ShowSyncInfo");
              //if SyncInfo to be shown
              if (sShowSyncInfo.toLowerCase() === "yes") {
                  $("#SyncStatus").show();
                  sRegRoot = "HKCU";
                  sRegKey = "Software\\SupportSoft\\ProviderList\\" + providerID + "\\users\\" + sUser + "\\";
                  sLastUpadteErr = $ss.agentcore.dal.registry.GetRegValue(sRegRoot, sRegKey, "LastUpdateErrCode");
                  sSyncBitDownloadState = $ss.agentcore.dal.registry.GetRegValue(sRegRoot, sRegKey, "SprtSync:BitsDownloadState");
                  //We are checking wheather the sync is successful or not.
                if (sLastUpadteErr == "0" && sSyncBitDownloadState.length == 0) return true;
                return false;
                  }
                  else {
                $("#SyncStatus").hide(); //Hide the SyncInfo, if ShowSyncInfo is "no"
                return false;
              }
          }
          catch (ex) {
            return false;
          }
      },

      //Add a registry for Download Popup Shown in HKCU
      AddDownloadPopupShownEntryUnderHKCU: function () {
          try {
              var provider = $ss.agentcore.dal.config.GetProviderID();
              var regPath = "Software\\SupportSoft\\ProviderList\\" + provider + "\\";

              var keys = $ss.agentcore.dal.registry.EnumRegKey("HKCU", regPath, "|");
              if (keys.indexOf("Download Manager") >= 0) {
                  $ss.agentcore.dal.registry.SetRegValueByType("HKCU", regPath + "Download Manager\\MODULE\\", "PopUpShown", $ss.agentcore.constants.REG_SZ, "true");
              }
          }
          catch (ex) {
          }
      },

      // To check if PA agent is launched
      LaunchAgent: function () {
          var loggedInUser = $ss.agentcore.dal.config.ExpandSysMacro("%USER%");
          var providerID = $ss.agentcore.dal.config.GetProviderID().toLowerCase();
          var agentLaunchedUser;
          var count = 0;

          var eServiceList = new Enumerator(window.external.GetObject("winmgmts:{impersonationLevel=impersonate}!root/cimv2").ExecQuery("Select * From Win32_Process where name='bcont_nm.exe'"));
          for (; !eServiceList.atEnd() ; eServiceList.moveNext()) {
              agentLaunchedUser = eServiceList.item().ExecMethod_("GetOwner");
              if (agentLaunchedUser.ReturnValue == 0) {
                  if (agentLaunchedUser.User == loggedInUser) {
                      var cmdline = eServiceList.item().CommandLine;
                      if (cmdline.indexOf("minibcont") < 0)
                          count += 1;
                  }
                  if (count > 1) {
                      alert(_utils.GlobalXLate("launched_agent"));
                      $ss.agentcore.utils.ui.CloseBcont();
                  }
              }
          }
      },
      //GS Customization end

      //GS Customization start: Get Sync URL
      GetSyncURL: function () {
          var sSyncURL = "";
          var sServerURL = ""
          var sRegRoot, sRegKey, sRegVal, providerID;
          try {
              sRegRoot = "HKLM";
              providerID = $ss.agentcore.dal.config.GetProviderID();
              sRegKey = "Software\\SupportSoft\\ProviderList\\" + providerID + "\\users\\_default\\";
              sRegVal = providerID;

              sSyncURL = $ss.agentcore.dal.registry.GetRegValue(sRegRoot, sRegKey, sRegVal);
              sServerURL = sSyncURL;
              sServerURL = sServerURL.replace("global/", "");
              sServerURL = sServerURL.replace("global", "");
              sServerURL = sServerURL.replace("http://", "");
              sServerURL = sServerURL.replace("https://", "");
              sServerURL = sServerURL.replace("//", "");
              sServerURL = sServerURL.replace("/", "");
          }
          catch (ex)
          { }
          return sServerURL;
      },
      //GS Customization end

      CheckConnectivity: function () {//by default conenctivity check has been disabled so do not proceed
          try {
              if (_nwConnectCheckEnabled === false || _nwConnectCheckEnabled === "false") {
                  $("#footer_connect_status").hide();
                  return;
              }
              var doPause = _databag.GetValue(_connectConst.NW_CONNECTION_PAUSE_DB_KEY);
              //it has been paused do not check n/w connectivity
              if (doPause === true)
                  return;

              var currentConnectStatus = _databag.GetValue(_connectConst.NW_CURRENTSTATE_DB_KEY) || _connectConst.NW_CONNECTION_STATUS.ONLINE;
              //if already n/w conenctivity is being checked stop.. do not continue
              if (currentConnectStatus === _connectConst.NW_CONNECTION_STATUS.INPROGRESS) {
                  return;
              }
              var doHide = _databag.GetValue(_connectConst.NW_CONNECTION_HIDE_DB_KEY);
              if (_nwConnectShowVisual === "false" || doHide === true) {
                  $("#footer_connect_status").hide();
              }
              _helper.UpdatedFooterNWUI(_connectConst.NW_CONNECTION_STATUS.INPROGRESS, false);

              if (_nWConnectBrowser === _constants.INET_BROWSER_FIREFOX) {
                  _fFoxProxSetting = null;
                  if (_inet.IsFirefoxProxyEnabled()) {
                      _fFoxProxSetting = _inet.GetFirefoxProxySettings();
                  }

                  if (_fFoxProxSetting) {
                      _fIEProxSetting = _inet.GetCurrentProxySettings();
                      _inet.SetProxySettings(_fFoxProxSetting);
                  }
              }

              connectStat = _connectConst.NW_CONNECTION_STATUS.ONLINE;
              var doSpoofTest = false;
              if (navigator.onLine === false) {
                  connectStat = _connectConst.NW_CONNECTION_STATUS.OFFLINE;
              }
              else {
                  var statusCode = _helper.HTTPAuthTest();
                  //Considering Authentication needed or Proxy credentials needed as online case
                  if (_inetErrorCodes[statusCode]) {
                      // Error In Connection
                      connectStat = _connectConst.NW_CONNECTION_STATUS.OFFLINE;
                  }
                  else {
                      //Found Connection
                      connectStat = _connectConst.NW_CONNECTION_STATUS.ONLINE;
                      doSpoofTest = _nwspooftestEnabled;
                  }
              }
              if (statusCode === 0 || statusCode === 12510) {
                  _origDnsTimeOut = _nDNSTimeout;
                  _nDNSTimeout = 2000;
                  doSpoofTest = true;
                  _http.AbortRequest();
              }
              else {
                  _nDNSTimeout = _origDnsTimeOut;
              }
              if (_nWConnectBrowser === _constants.INET_BROWSER_FIREFOX) {
                  if (_fFoxProxSetting) {
                      _inet.SetProxySettings(_fIEProxSetting);
                  }
              }
              _helper.UpdatedFooterNWUI(connectStat, doSpoofTest);
              //_helper.RequestRemoteUrl(_nwConnectPrimaryUrl);
              return;
          }
          catch (ex) {
          }
      },
      HTTPAuthTest: function () {
          var proxyuser = "";
          var proxypwd = "";

          if (_useProxy) {
              var EncryptProxypName = _registry.GetRegValue(_constants.REG_TREE, _regBase, "ProxyUsername");
              if (EncryptProxypName != "")
                  proxyuser = _utils.DecryptHexToStr(EncryptProxypName);

              var EncryptProxypwd = _registry.GetRegValue(_constants.REG_TREE, _regBase, "ProxyPassword");
              if (EncryptProxypwd != "")
                  proxypwd = _utils.DecryptHexToStr(EncryptProxypwd);
          }
          var sUrl = _nwConnectPrimaryUrl;
          if (_nwConnectReplacedUrl) {
              // GS customization start- we are getting 407 error by using URL with IP address. so, commenting below line 
              //sUrl = _nwConnectReplacedUrl;
              // GS customization end
          }
          var statusCode = _http.Authenticate(sUrl, _localFile, _nwConnectTimeOut, "", "", proxyuser, proxypwd);
          return statusCode;
      },
      UpdatedFooterNWUI: function (sState, doSpoofTest) {
          doSpoofTest = (doSpoofTest === undefined) ? false : doSpoofTest;
          if (doSpoofTest) {
              if (_bCheckDnsSpoofing) {
                  //flush the DNS only once to make the performance better. Do not make it multiple times.
                  // it may break the SA leading to bcont crash.
                  if (!_dnsFlushDone) {

                      try {
                          _utils.StartShellBusy();
                          _utils.FlushDNS();
                      } catch (ex) {

                      } finally {
                          _dnsFlushDone = true;
                          _utils.EndShellBusy();
                      }

                  }
                  //NOTE: DO NOT SA ENABLED NAMECHECK.. If another instance of SA has been initiated, it may crash bcont
                  //Use very little 500 ms as a timeout.

                  var sDNSResult = _netcheck.ResolveHostname(null, _sHostName, _nDNSTimeout, false);
                  if (sDNSResult !== "") {
                      if (_sHostNameIP) { //already we know the valid IP address..
                          if (_sHostNameIP !== sDNSResult) {
                              sState = _connectConst.NW_CONNECTION_STATUS.OFFLINE;
                          }
                      }
                      else {
                          //NOTE: DO NOT SA ENABLED NAMECHECK.. If another instance of SA has been initiated, it may crash bcont
                          //Use very little 500 ms as a timeout.
                          var sFakeDNSResult = _netcheck.ResolveHostname(null, _sSpoofDNSTestHost, _nDNSTimeout, false);
                          var bSpoof2 = false;
                          try {
                              var sDNSResultSub = sDNSResult.split(".").slice(0, 3).join(".");
                              var sFakeDNSResultSub = sFakeDNSResult.split(".").slice(0, 3).join(".");
                              if (sDNSResultSub == sFakeDNSResultSub)
                                  bSpoof2 = true;
                          }
                          catch (e) {
                              bSpoof2 = false;
                          }

                          if (sFakeDNSResult == sDNSResult || bSpoof2) {
                              sState = _connectConst.NW_CONNECTION_STATUS.OFFLINE;
                          }
                          if ((sFakeDNSResult !== sDNSResult) && !bSpoof2 && !_nwConnectReplacedUrl) {
                              _nwConnectReplacedUrl = _nwConnectPrimaryUrl.replace(_sHostName, sDNSResult);
                              //_sHostNameIP = sDNSResult; // there is a problem it some time gives different host name..
                          }
                      }
                  }

              }
          }

          _databag.SetValue(_connectConst.NW_CURRENTSTATE_DB_KEY, sState);
          var lastState = _databag.GetValue(_connectConst.NW_LASTSTATE_DB_KEY);
          switch (sState) {
              case _connectConst.NW_CONNECTION_STATUS.ONLINE:
                  $(".footer_connect_status_each").hide();
                  $("#footer_connect_status_on").show();
                  _databag.SetValue(_connectConst.NW_LASTSTATE_DB_KEY, sState);
                  if (lastState !== sState) {
                      $ss.agentcore.events.SendByName("BROADCAST_CONNECT_STATUS");
                  }
                  break;
              case _connectConst.NW_CONNECTION_STATUS.INPROGRESS:
                  if (_nwConnectShowProgress != "false") {
                      $(".footer_connect_status_each").hide();
                      $("#footer_connect_status_check").show();
                  }
                  break;
              case _connectConst.NW_CONNECTION_STATUS.OFFLINE:
                  $(".footer_connect_status_each").hide();
                  $("#footer_connect_status_off").show();
                  _databag.SetValue(_connectConst.NW_LASTSTATE_DB_KEY, sState);
                  if (lastState !== sState) {
                      $ss.agentcore.events.SendByName("BROADCAST_CONNECT_STATUS");
                  }
                  break;
          }
      },

      HideFooterConnect: function (timerObj, bPause) {
          //by default pause will be always true;
          bPause = typeof (bPause) === "undefined" ? true : bPause;
          _databag.SetValue(_connectConst.NW_CONNECTION_PAUSE_DB_KEY, bPause);
          _databag.SetValue(_connectConst.NW_CONNECTION_HIDE_DB_KEY, true);
          $("#footer_connect_status").hide();
          var currentStatus = _databag.GetValue(_connectConst.NW_CURRENTSTATE_DB_KEY);
          if (currentStatus === _connectConst.NW_CONNECTION_STATUS.INPROGRESS) {
              _http.AbortRequest();
          }

      },
      ShowFooterConnect: function () {
          _databag.SetValue(_connectConst.NW_CONNECTION_PAUSE_DB_KEY, false);
          var hideStatus = _databag.GetValue(_connectConst.NW_CONNECTION_HIDE_DB_KEY);
          if (hideStatus === true) {
              _databag.SetValue(_connectConst.NW_CONNECTION_HIDE_DB_KEY, false);
              _hostDNSResult = "";
              $(document).oneTime(1000, "retest", $ss.helper.CheckConnectivity);
              $("#footer_connect_status").show();
          }
      },
      IsConnectionAvailable: function () {
          if (_useDefaultConnectivityCheck && (!_nwConnectCheckEnabled)) {
              return _inet.BasicConnectionTest(true, false, "default");
          }
          else {
              return (_databag.GetValue(_connectConst.NW_LASTSTATE_DB_KEY) === _connectConst.NW_CONNECTION_STATUS.ONLINE)
          }
      },
      GetNetworkConnectioCheckURL : function(){
          return(_serverbaseurl + _config.GetConfigValue("status_connectivity_check", "network_connection_check_url", "sdccommon/asp/blank.html"));
      },
      CanCloseBcont: function () {
          var bCanClose = true;
          try {
              if (_busy.IsAnyItemBusy()) {
                  var aMsgs = _busy.GetBusyItems();
                  var sDisp = _utils.GlobalXLate("ss_glb_Quit_Question_begin") + "\n";
                  for (var i = 0; i < aMsgs.length; i++) {
                      sDisp = sDisp + aMsgs[i].message + "\n";
                  }
                  sDisp = sDisp + _utils.GlobalXLate("ss_glb_Quit_Question_end");
                  bCanClose = confirm(sDisp);
              }
          } catch (e) {
              bCanClose = true;
          }
          return bCanClose;
      },

      GetSearchableContents: function (bFromDashboard) {
          var slang = $ss.agentcore.utils.GetLanguage();
          if (bFromDashboard!==true) {
              if (_RefreshCache) {
                  var oContentTypes = ["sprt_actionlight", "sprt_rawdoc", "sprt_articlefaq", "sprt_rawdoc_inline", "sprt_download", "sprt_msg", "sprt_optimize", "sprt_protection", "sprt_alertevents"];
              var existingDownloads = _content.GetContentsByAnyType(oContentTypes, slang);
                  var validDownloads = _content.GetValidContentsForDisplay(existingDownloads);
                  _cachedContent = validDownloads;
                  _RefreshCache = false;
              }
              return _cachedContent;
          } else {
              var oContentTypes = ["sprt_msg", "sprt_alertevents"];
            var existingDownloads = _content.GetContentsByAnyType(oContentTypes, slang);
              var validDownloads = _content.GetValidContentsForDisplay(existingDownloads);
              return validDownloads;
          }
      },

      GetViewableFolderGuidList: function () {
        if(!aViewablefolderguidlist.length){
          var sCurFolderId = _config.GetConfigValue("root_folder_id", "root_folder", "sprt_root_folder");
          $ss.helper.GetCurrentFolderDetails(sCurFolderId);        
        }        
      },
      
      GetCurrentFolderDetails:function(fid){
        if(fid === "") return;
        aViewablefolderguidlist.push(fid);
        var oFolders = _content.GetFolderDetails(fid);
        if(oFolders !== null && typeof(oFolders.subfolders) === "object"){
          for(var i=0;i< oFolders.subfolders.length;i++){
            var oCurrentFolder = oFolders.subfolders[i].fid;
            $ss.helper.GetCurrentFolderDetails(oCurrentFolder);
          }
        } 
      },

      /*Due to Pruning, SearchResult will always be filtered/verified against contents exist in data folder.
      /*Also silent Download contents and deleted Support Messages will be filtered out.*/
      GetFilteredResult: function (sresult, dresult) {
          oContentcountDict = [];
          var dResult = $ss.helper.VerifySearchResultOnDataFolder(sresult, dresult);
          var filteredResult = $ss.helper.FilterOutSilentDOsAndDelSMsgs(dResult);
          //Updating DOs count by removing mannedDOs
          var cSilentDOs = 0;
          if (arrMannedDO.length > 0) cSilentDOs = oContentcountDict["sprt_download"] - arrMannedDO.length;
          if(cSilentDOs > 0) oContentcountDict["sprt_download"] = oContentcountDict["sprt_download"] - cSilentDOs;
          return filteredResult;
      },
      
      MapSearchableContent: function (parseData) {
        var me=this;
        var mapsearchResult = $.map(parseData, function (v, index) {
              var res = { fid: v.fid || v.scc_folder_guid, cid: v.cid || v.scc_content_guid, ctype: v.ctype || v.scc_content_type_guid, version: v.version || v.scc_version, description: v.description || v.scc_description, title: v.title || v.scc_title, category: $.makeArray(v.category || v.scc_categories), featured: false };
          res.featured =  me.isFeaturedContent(res);
          return res;              
        });
        return mapsearchResult;
      },

      VerifySearchResultOnDataFolder: function (sresult, dresult) {
          var bContentExist;
          return $.grep(sresult, function (el, index) {
              bContentExist = false;
              if($ss.helper.isContentExistInsideViewableFolder(el.ctype, el.fid)){
                if (typeof el.category !== "undefined" && el.category.length !== 0) {
                    //Content with category defined only as "sys_saction" will be filtered out.
                    if (!$ss.helper.isCategorySysSaction(el.category)) {
                        bContentExist = $ss.helper.checkIfexists(dresult, el);
                    }
                } else {
                    bContentExist = $ss.helper.checkIfexists(dresult, el);
                }
              }
              return bContentExist;
          });
      },

      isContentExistInsideViewableFolder : function(ctype,fid){
        //Currently we check only for content types in checkContentList whether its corresponding folder guid exist in viewablefolder array list or not.
        //For other content types we blindly assume it is viewable by default.
        //returns true if content fid exists in aViewablefolderguidlist otherwise retun false.
        var bViewable = true;
        var checkContentList = ["sprt_actionlight", "sprt_rawdoc", "sprt_articlefaq", "sprt_rawdoc_inline"];
        if($.inArray(ctype, checkContentList) > -1 && $.inArray(fid, aViewablefolderguidlist) === -1) bViewable = false; 
        return bViewable;
      },

      isCategorySysSaction: function (sCat) {
          var issyssaction = false;
          var sCategory = $.isArray(sCat) ? sCat : sCat.split(",");
          $.each(sCategory, function (iIndex, sValue) {
              if (typeof sValue === "object") sValue = sValue.toString().trim();
              else if (typeof sValue === "string") sValue = sValue.trim();
              if (sValue === "sys_saction") issyssaction = true;
          });
          return issyssaction;
      },

      //MannedDOGuid List is used to filter out Silent DO contents from search Result and SSS page.
      GetMannedDOGuidList: function () {
          var localparams = {};
          try {
              MVC.Controller.dispatch("snapin_download_manager", "indexMannedDO", localparams);
              arrMannedDO = localparams.MannedDOList;
          } catch (ex) {
              this.Class.logger.error("Error in Dispatching to Download Manager Controller%1%", ex.message);
          }
      },

      //GetDeletedSMsgsList is used to filter out deleted Support Messages from search Result and SSS page.
      GetDeletedSMsgsList: function (params) {
          var localparams = { lastmodified: _gSilentDOXMLtStamp };
          try {
              MVC.Controller.dispatch("snapin_messageviewer", "indexMessage", localparams);
              if (localparams.SDelMsgslist) arrDelSupportMsgs = localparams.SDelMsgslist;
              _gSilentDOXMLtStamp = localparams.lastmodified;
              localparams = null;
          } catch (ex) {
              this.Class.logger.error("Error in Dispatching to Message Viewer Controller %1%", ex.message);
          }
      },

      FilterOutSilentDOsAndDelSMsgs: function (fresult) {
          if(NavigationController.IsUrlValid("/dm"))	this.GetMannedDOGuidList();
          if(NavigationController.IsUrlValid("/sa/messagesviewer"))	this.GetDeletedSMsgsList();
          return $.grep(fresult, function (el, index) {
              retval = false;
              switch (el.ctype) {
                  case "sprt_download": if (!$ss.helper.checkIsSilentDO(el.cid)) retval = true;
                      break;
                  case "sprt_msg": retval = ($.inArray(el.cid, arrDelSupportMsgs) === -1) ? true : false;
                      break;
                  default: retval = true;
                      break;
              }
              return retval;
          });
      },

      checkIsSilentDO: function (contentguid) {
          if (arrMannedDO.length > 0) {
              if ($.inArray(contentguid, arrMannedDO) === -1) return true;
          }
          return false;
      },

      checkIfexists: function (source, elem) {
          var bContentFound = false;
          $.each(source, function (k, v) {
            if (v.cid == elem.cid && v.version == elem.version) {
              bContentFound = true;
              if (oContentcountDict[v.ctype]) {
                oContentcountDict[v.ctype] = oContentcountDict[v.ctype] + 1;
              } else {
                oContentcountDict[v.ctype] = 1;
              }
            }
          });
          return bContentFound;
      },
        
      contentHasCategories:function(c){
        if(!c) return false;
        return(c.category && $.isArray(c.category) && c.category.length>0);
      },
      
      isFeaturedContent:function(c){
        if(!c) return false;
          return(c.featured===true || (this.contentHasCategories(c) && c.category.containsMatch('featured','gi')));
    },

    getStringLengthForTruncation: function (sSnapinName, sPreferedConfigValue) {
      var iMaxLength = _config.GetConfigValue(sSnapinName, sPreferedConfigValue);
      if (iMaxLength !== undefined) {
        return iMaxLength;
      }
      return _config.GetConfigValue("layout_layout1", "maxStringLenToDisp_" + _utils.GetLanguage(), "15");
    }
    })

    var _connectConst = {
        NW_LASTSTATE_DB_KEY: "NW_LAST_STATE_DB",
        NW_CURRENTSTATE_DB_KEY: "NW_CURRENT_STATE_DB",
        NW_CONNECTION_STATUS: {
            INPROGRESS: 2,
            ONLINE: 1,
            OFFLINE: 0
        },
        NW_CONNECTION_PAUSE_DB_KEY: "NW_FOOTER_CONNECTION_PAUSE",
        NW_CONNECTION_HIDE_DB_KEY: "NW_FOOTER_CONNECTION_HIDE"
    };

    // GS customization - removed : 401: "Unauthorized" and 407: "Proxy Authentication Required"
    var _inetErrorCodes = {
        12002: "ERROR_INTERNET_TIMEOUT",
        12005: "Malformed URL",
        12007: "ERROR_INTERNET_NAME_NOT_RESOLVED",
        12029: "ERROR_INTERNET_CANNOT_CONNECT",
        12030: "ERROR_INTERNET_CONNECTION_ABORTED",
        12031: "ERROR_INTERNET_CONNECTION_RESET",
        12152: "ERROR_HTTP_INVALID_SERVER_RESPONSE",
        12164: "ERROR_INTERNET_SERVER_UNREACHABLE",
        12159: "ERROR_INTERNET_TCPIP_NOT_INSTALLED",
        12163: "ERROR_INTERNET_DISCONNECTED",
        12510: "Error Problem",
        302: "MOVED",
        502: "BAD GATEWAY",
        503: "SERVICE UNAVAILABLE",
        504: "GATEWAY TIMEOUT",
        505: "HTTP Version Not Supported",
        402: "Payment Required",
        403: "Forbidden",
        400: "Bad Request",
        408: "Request Timeout",
        500: "Internal Server Error",
        501: "Not Implemented",
        0: "timeout",
        2: "Unkwnown Error"

    };

    var _constants = $ss.agentcore.constants;
    var _utils = $ss.agentcore.utils;
    var _inet = $ss.agentcore.network.inet;
    var _netcheck = $ss.agentcore.network.netcheck;
    var _helper = $ss.helper;
    var _registry = $ss.agentcore.dal.registry;
    var _config = $ss.agentcore.dal.config;
    var _http = $ss.agentcore.dal.http;
    var _databag = $ss.agentcore.dal.databag;
    var _busy = $ss.agentcore.state.busy;

    var _imgRegEx = /[A-Za-z]{0,4}:\/{0,3}[0-9.\-A-Za-z:]+\/[^?#]*((docview\.asp)|(docs))\?[^\s\"\']*/g;
    var _resRegEx = /sprt_ct=([0-9_\-A-Za-z]*)[\S]*sprt_cid=([0-9\-A-Za-z]*)/i;
    var _parseUrlRegEx = /^(?:([A-Za-z]+):)?(\/{0,3})([0-9.\-A-Za-z]+)(?::(\d+))?(?:\/([^?#]*))?(?:\?([^#]*))?(?:#(.*))?$/;
    var _serverbaseurl = _config.ExpandAllMacros("%SERVERBASEURL%");
    var _nwConnectPrimaryUrl = _helper.GetNetworkConnectioCheckURL();
    var _sHostName = "" //GS : This value is set later in the page
    var _nwConnectReplacedUrl;
    var _hadRedirectCode = false;
    var _nwConnectCheckEnabled = (_config.GetConfigValue("status_connectivity_check", "net_check_enabled", "true") === "true");
    var _useDefaultConnectivityCheck = (_config.GetConfigValue("status_connectivity_check", "use_default_connectivity_for_status", "false") == "true");
    var _nwConnectTimeOut = _config.GetConfigValue("status_connectivity_check", "net_check_timeout", "30000");
    var _nwConnectCheckInterval = _config.GetConfigValue("status_connectivity_check", "net_check_interval", "50000");
    var _nwConnectShowVisual = _config.GetConfigValue("status_connectivity_check", "show_visual", "true");
    var _nwConnectShowProgress = _config.GetConfigValue("status_connectivity_check", "show_progress_status", "true");
    var _CheckDnsSpoofing = _config.GetConfigValue("status_connectivity_check", "HomeRunFailsOnSpoofedDNS", "true");
    var _bCheckDnsSpoofing = (_CheckDnsSpoofing.toLowerCase() != "false" && _CheckDnsSpoofing != "1");
    var _sSpoofDNSTestHost = _config.GetConfigValue("status_connectivity_check", "HomeRunFakeHostname", "www.supportsoft.com"); //GS Customization- to Fetch the URL from config file
    var _nWConnectBrowser = _config.GetConfigValue("status_connectivity_check", "browser_to_be_tested", "Default");

    //GS Customization start: Get Sync Check Interval from Config
    var _SyncCheckInterval = _config.GetConfigValue("status_connectivity_check", "sync_check_interval", "10000");
    //GS Customization end

    if (_nWConnectBrowser == null || typeof (_nWConnectBrowser) === "undefined" || _nWConnectBrowser == "")
        _nWConnectBrowser = _constants.INET_BROWSER_IE;
    if (_nWConnectBrowser == "Default")
        _nWConnectBrowser = _inet.GetDefaultBrowser();

    var _nDNSTimeout = _config.GetConfigValue("status_connectivity_check", "DNSTimeout", 1000);

    var _fakeDNSResult = "";
    var _hostDNSResult = "";
    var _fFoxProxSetting = null;
    var _fIEProxSetting = null;

    //var _regBase = $ss.agentcore.dal.config.GetRegRoot();
    var _regBase = $ss.agentcore.constants.REG_SPRT + "ProviderList\\" + $ss.agentcore.dal.config.GetProviderID();
    var _localFile = _config.ExpandSysMacro("%TEMP%clientuiping.test");
    var _sHostNameIP;
    var _origDnsTimeOut = _nDNSTimeout;
    var _useProxy;
    var _dnsFlushDone = false;

    //subscribe to the event on BroadCase On Refresh for Tray Icon External Call
    $ss.agentcore.events.Subscribe("BROADCAST_ON_EXTERNALREFRESH", $ss.helper.RefreshPageFromTrayIcon);
    $ss.agentcore.events.Subscribe("DISABLE_HIDE_NW_CHECK", $ss.helper.HideFooterConnect);
    var _cachedContent = null;
    var _RefreshCache = true;
    var _content = $ss.agentcore.dal.content;
    var _gSilentDOXMLtStamp = null;
    var arrMannedDO = [];
    var arrDelSupportMsgs = [];
    var _nwspooftestEnabled = (_config.GetConfigValue("status_connectivity_check", "do_spoof_test", "false") === "true"); //GS Customization- to Fetch the do_spoof_test value from config file
    var AmIOnlineAndLatest = "1";
})();
