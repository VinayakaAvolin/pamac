
$ss.layout = $ss.layout ||
{};


$ss.layout.LayoutModel = MVC.Class.extend({
  init: function (sPageDefFile) {
    this._pageDefFile = sPageDefFile;
    this._navTree = [];
    this.NAVLEVELMAX = 2;
    this._header = {};
    this._footer = {};
    this._menu = {};
    this._pageDefinition = {};
    this._urlPageMap = {};
    this._layoutGridCache = {};
    this._groupMap = {};

    this._defaultPath = "";
    this._selectedFirstNav = "";
    this._selectedSecNav = "";
    this._currentPage = "";
    this._currentUrl = "";
    this._defaultPageId = "";

    this._logger = $ss.agentcore.log.GetDefaultLogger("$ss.layout.LayoutModel");
    this._rootXPath = "//root/shell/body";
    this.Initialize();
    
    this._currentHighlightedMenu;
  },
  Initialize: function () {
    var _xml = $ss.agentcore.dal.xml;
    var _const = $ss.agentcore.constants;
    var tempDom = _xml.LoadXML(this._pageDefFile, false);
    this.LoadHeader(tempDom);
    this.LoadFooter(tempDom);
    this.LoadMenu(tempDom);
    var contXPath = this._rootXPath + "/content";
    var oContentDom = _xml.GetNode(contXPath, "", tempDom);
    var initBreadCrumb = [{
      url: "/",
      defName: this._menu.deaultPage
    }];

    this.BuildNavigation(oContentDom, this._navTree, "/", null, initBreadCrumb, this._menu.deaultPage);

    //get the default path;
    var defaultPathUrl = "/";
    var defaultPathPage = this._pageDefinition[this._menu.deaultPage];
    if (defaultPathPage) {
      this._urlPageMap[defaultPathUrl] = this._urlPageMap[defaultPathPage.url];
      this._defaultPath = defaultPathPage.url;
    }

  },
  LoadHeader: function (oDom) {
    var _xml = $ss.agentcore.dal.xml;
    var headerXpath = this._rootXPath + "/header";
    var tempHeadNode = _xml.GetNode(headerXpath, "", oDom);
    var toLocation = _xml.GetAttribute(tempHeadNode, "markupId");
    var headerFile = _xml.GetAttribute(tempHeadNode, "contentUri");
    headerFile = $ss.agentcore.dal.config.ParseMacros(headerFile);
    this._header.to = toLocation;
    this._header.file = headerFile;
    tempHeadNode = null;
  },
  LoadFooter: function (oDom) {
    var _xml = $ss.agentcore.dal.xml;
    var footerXpath = this._rootXPath + "/footer";
    var tempFooterNode = _xml.GetNode(footerXpath, "", oDom);
    var toLocation = _xml.GetAttribute(tempFooterNode, "markupId");
    var footerFile = _xml.GetAttribute(tempFooterNode, "contentUri");
    footerFile = $ss.agentcore.dal.config.ParseMacros(footerFile);
    this._footer.to = toLocation;
    this._footer.file = footerFile;
    tempFooterNode = null;
  },
  LoadMenu: function (oDom) {
    var _xml = $ss.agentcore.dal.xml;
    var menuXpath = this._rootXPath + "/menu";
    var tempMenuNode = _xml.GetNode(menuXpath, "", oDom);
    var toLocation = _xml.GetAttribute(tempMenuNode, "markupId");
    var menuFile = _xml.GetAttribute(tempMenuNode, "contentUri");
    menuFile = $ss.agentcore.dal.config.ParseMacros(menuFile);
    var defaultPage = _xml.GetAttribute(tempMenuNode, "defaultpage");
    this._menu.to = toLocation;
    this._menu.file = menuFile;
    this._menu.deaultPage = defaultPage;
    this._defaultPageId = defaultPage;
    tempMenuNode = null;
  },
  BuildNavigation: function (oDom, oNavLevel, sParentPath, parentObj, oBreadCrumb, sDefaultPage) {
    var _xml = $ss.agentcore.dal.xml;

    if (oDom) {
      for (var i = 0, iChildLen = 0; iChildLen < oDom.childNodes.length; i++) {

        var tmpChildNode = oDom.childNodes[iChildLen++];
        var tmpLevel = _xml.GetAttribute(tmpChildNode, "level");
        var tmpUrl = _xml.GetAttribute(tmpChildNode, "url");
        var tmpDefName = _xml.GetAttribute(tmpChildNode, "definitionLabel");
        var tmpTitle = $ss.agentcore.utils.GlobalXLate(_xml.GetAttribute(tmpChildNode, "title"));
        var tmpShowInMenu = _xml.GetAttribute(tmpChildNode, "showinmenu") == "true" ? true : false;
        var tmpMenuDisplayCssClass = _xml.GetAttribute(tmpChildNode, "menuclassname");
        var tmpNodeType = _xml.GetAttribute(tmpChildNode, "markupType");
        var tmpNodeId = _xml.GetAttribute(tmpChildNode, "markupId");
        var tmpGroup = _xml.GetAttribute(tmpChildNode, "group");
        var config_section = _xml.GetAttribute(tmpChildNode, "show_page_config_section");
        var config_key = _xml.GetAttribute(tmpChildNode, "show_page_config_key");
        var page_available = (_xml.GetAttribute(tmpChildNode, "page_available") === null) ? true : (page_available === "true");

        config_section = config_section === null ? null : config_section;
        config_key = config_key === null ? null : config_key;
        if (config_section && config_key) {
          var overRideShowStatus = ($ss.agentcore.dal.config.GetConfigValue(config_section, config_key, "false")) == "true";
          tmpShowInMenu = overRideShowStatus;
          page_available = overRideShowStatus;
        }

        if (!page_available) continue;

        oNavLevel[i] = {};
        oNavLevel[i].level = tmpLevel;
        oNavLevel[i].url = tmpUrl;
        oNavLevel[i].defName = tmpDefName;
        oNavLevel[i].title = tmpTitle;
        oNavLevel[i].isMenuItem = tmpShowInMenu;
        oNavLevel[i].MenuCss = tmpMenuDisplayCssClass;
        oNavLevel[i].parentUrl = sParentPath;
        oNavLevel[i].nodeId = tmpNodeId;
        oNavLevel[i].group = tmpGroup;

        oNavLevel[i].isPageAvailable = page_available;


        this._urlPageMap[tmpUrl] = {};

        oNavLevel[i].parent = parentObj;
        this._groupMap[tmpGroup] = this._groupMap[tmpGroup] || {};
        this._groupMap[tmpGroup][tmpUrl] = oNavLevel[i];

        this._urlPageMap[tmpUrl] = oNavLevel[i];
        switch (tmpNodeType) {
          case 'Page':
            //load the page details
            oNavLevel[i].page = tmpDefName;
            oNavLevel[i].hasSubMenu = false;
            this.BuildPageDefinition(tmpChildNode, tmpDefName, tmpUrl, tmpTitle, tmpMenuDisplayCssClass, tmpNodeId, sParentPath, oBreadCrumb, sDefaultPage);
            if (this._pageDefinition[tmpDefName] == undefined) {
              this.RemoveNavElem(oNavLevel, i);
              this.RemoveUrl(tmpUrl);
              delete this._groupMap[tmpGroup][tmpUrl];
              i--;
            }
            break;
          case 'Book':
            var tmpDefaultPage = _xml.GetAttribute(tmpChildNode, "defaultPage");
            oNavLevel[i].page = tmpDefaultPage;
            oNavLevel[i].defaultPage = tmpDefaultPage;
            oNavLevel[i].hasSubMenu = true;
            oNavLevel[i].children = [];
            oBreadCrumb = oBreadCrumb || [];
            oBreadCrumb.push({ url: oNavLevel[i].url, defName: oNavLevel[i].defaultPage });
            this.BuildNavigation(tmpChildNode, oNavLevel[i].children, tmpUrl, oNavLevel[i], oBreadCrumb, tmpDefaultPage);
            var bRemove = true;
            var toProcess = oNavLevel[i].children;
            var aUrlCache = [];
            for (var j = 0; j < toProcess.length && bRemove; j++) {
              if (!toProcess[j]) continue;
              if (!this._pageDefinition[toProcess[j].page]) continue;
              if (tmpDefaultPage === toProcess[j].page) {
                aUrlCache.push(toProcess[j].url);
                continue;
              }
              aUrlCache = []; //Clear URL cache
              bRemove = false;
            }
            if (bRemove) {
              for (var j = 0; j < aUrlCache.length; j++)
                this.RemoveUrl(aUrlCache.pop()); //Removing Child Menu Items
              this.RemoveNavElem(oNavLevel, i);
              this.RemoveUrl(tmpUrl);
              delete this._pageDefinition[tmpDefaultPage];
            }
            oBreadCrumb.pop();
            break;
          default:
            break;
        }
      }
    }

  },
  BuildPageDefinition: function (oDom, pageDef, pageUrl, pageTitle, pageNavClass, nodeId, parentUrl, oBreadCrumb, sDefaultPage) {
    var _xml = $ss.agentcore.dal.xml;
    this._pageDefinition[pageDef] = {}
    this._pageDefinition[pageDef].url = pageUrl;
    this._pageDefinition[pageDef].title = pageTitle;
    this._pageDefinition[pageDef].cssClassName = pageNavClass;
    this._pageDefinition[pageDef].nodeId = nodeId;
    this._pageDefinition[pageDef].parentUrl = parentUrl;
    oBreadCrumb = oBreadCrumb || [];
    this._pageDefinition[pageDef].breadCrumb = oBreadCrumb.slice();
    if (pageDef !== sDefaultPage) {
      this._pageDefinition[pageDef].breadCrumb.push({
        url: pageUrl,
        defName: pageDef
      });
    }

    if (oDom && oDom.hasChildNodes()) {
      //grid defintion to have only one child node
      var tmpGridDom = oDom.childNodes[0];
      var layoutGrid = _xml.GetAttribute(tmpGridDom, "htmlLayoutUri");
      var layoutGridLocation = _xml.GetAttribute(tmpGridDom, "markupId");
      this._pageDefinition[pageDef].gridLayout = layoutGrid;
      this._pageDefinition[pageDef].gridLayoutTo = layoutGridLocation;
      this._pageDefinition[pageDef].placeholder = [];
      for (var i = 0, iGridChilds = 0; tmpGridDom && iGridChilds < tmpGridDom.childNodes.length; i++) {
        var placeHolderDom = tmpGridDom.childNodes[iGridChilds++];
        var tPlaceHolderId = _xml.GetAttribute(placeHolderDom, "markupId");
        var bMandatory = _xml.GetAttribute(placeHolderDom, "ignoreIntelDisplay") === "true" ? true : false;
        this._pageDefinition[pageDef].placeholder[i] = {};
        this._pageDefinition[pageDef].placeholder[i].to = tPlaceHolderId;
        this._pageDefinition[pageDef].placeholder[i].snapins = [];
        for (var j = 0, iPlaceHolderChilds = 0; placeHolderDom && iPlaceHolderChilds < placeHolderDom.childNodes.length; j++) {
          var snapinDoms = placeHolderDom.childNodes[iPlaceHolderChilds++];
          var snapinName = _xml.GetAttribute(snapinDoms, "snapinName");
          if ($ss.GetSnapin(snapinName) === null && !bMandatory) {
            snapinDoms = null;
            j--;
            continue;
          }
          var methodName = _xml.GetAttribute(snapinDoms, "methodName");
          if (!methodName)
            methodName = "index";
          this._pageDefinition[pageDef].placeholder[i].snapins[j] = {};
          this._pageDefinition[pageDef].placeholder[i].snapins[j].name = snapinName;
          this._pageDefinition[pageDef].placeholder[i].snapins[j].method = methodName;
          if (snapinDoms && snapinDoms.hasChildNodes()) {
            this._pageDefinition[pageDef].placeholder[i].snapins[j].inputparam = {};
            for (var k = 0; snapinDoms && k < snapinDoms.childNodes.length; k++) {
              var inputDom = snapinDoms.childNodes[k];
              var varName = _xml.GetAttribute(inputDom, "variableName");
              var varValue = _xml.GetNodeText(inputDom);
              this._pageDefinition[pageDef].placeholder[i].snapins[j].inputparam[varName] = varValue;
              inputDom = null;
            }
          }
          snapinDoms = null;
        }
        if (this._pageDefinition[pageDef].placeholder[i].snapins[0] === undefined) {
          delete this._pageDefinition[pageDef].placeholder[i];
          this._pageDefinition[pageDef].placeholder.splice(i, 1);
          i--;
        }
        placeHolderDom = null;
      }
      if (this._pageDefinition[pageDef].placeholder[0] === undefined) {
        delete this._pageDefinition[pageDef];
      }
      tmpGridDom = null;
    }
  },
  GetPageDetails: function (sPageDef) {
    if (this._pageDefinition[sPageDef]) return this._pageDefinition[sPageDef];
    return null;
  },
  GetLayoutGrid: function (sGridPath) {
    if (this._layoutGridCache[sGridPath]) return this._layoutGridCache[sGridPath];
    var sGridAbsPath = 'file://' + $ss.GetLayoutAbsolutePath() + sGridPath;
    var grid_text = "";
    try {
      grid_text = MVC.request(sGridAbsPath);
      this._layoutGridCache[sGridPath] = grid_text;
      return this._layoutGridCache[sGridPath];
    } catch (e) {
      grid_text = "";
      return null;
    }
  },
  SetCurrentPage: function (sPageDef, uNavUri) {
    this._currentUrl = uNavUri;
    this._currentPage = sPageDef;
  },
  GetCurrentPage: function () {
    return { navurl: this._currentUrl, pageDef: this._currentPage };
  },
  SetRequestedUrl: function (sUrl) {
    this._currentUrl = sUrl;
  },
  GetCurrentUrl: function (sUrl) {
    return this._currentUrl;
  },
  GetHeader: function () {
    return this._header;
  },
  GetFooter: function () {
    return this._footer;
  },
  GetMenu: function () {
    return this._menu;
  },
  GetUrlDetails: function (sUrl) {
    if (this._urlPageMap[sUrl]) return this._urlPageMap[sUrl];
  },
  GetGroupDetails: function (sGroup) {
    return this._groupMap[sGroup];
  },
  GetNavTree: function (sUrl) {
    sUrl = this.GetUrl(sUrl);
    //set the current url
    //this._currentUrl = sUrl;
    var sPaths = sUrl.split("/");
    var levels = [];
    var navObjectToSearch = this._navTree;
    var tmpPath = "";
    var tmpSearchObject = [];
    for (var i = 1; i < sPaths.length; i++) {
      levels[i - 1] = [];
      var count = 0;
      tmpPath = tmpPath + "/" + sPaths[i];
      for (var j1 = 0; j1 < navObjectToSearch.length; j1++) {
        if (!navObjectToSearch[j1]) continue;
        if (navObjectToSearch[j1].isMenuItem) {
          if(navObjectToSearch[j1].url == tmpPath){ 
            this._currentHighlightedMenu = navObjectToSearch[j1].url;
          }
        }
      }
      for (var j = 0; j < navObjectToSearch.length; j++) {
        if (!navObjectToSearch[j]) continue;
        if (navObjectToSearch[j].isMenuItem) {
          levels[i - 1][count] = {};
          levels[i - 1][count].title = navObjectToSearch[j].title;
          levels[i - 1][count].cssClass = navObjectToSearch[j].MenuCss;
          levels[i - 1][count].url = navObjectToSearch[j].url;
          levels[i - 1][count].id = navObjectToSearch[j].nodeId;
          levels[i - 1][count].isMenuItem = navObjectToSearch[j].isMenuItem;

          if (navObjectToSearch[j].url == this._currentHighlightedMenu) {
            levels[i - 1][count].selected = true;
          } else {
            levels[i - 1][count].selected = false;
          }
         
          count = count + 1;
        }
        if (navObjectToSearch[j].url == tmpPath) {
          if (navObjectToSearch[j].hasSubMenu) {
            tmpSearchObject = navObjectToSearch[j].children;
          }
        }
      }
      navObjectToSearch = tmpSearchObject;

    }

    return levels;
  },
  GetUrl: function (sUrl) {
    //TODO remove trailing "/" if there is anything 
    //TODO if /sa/adsdasd is there should it return /sa/home instead pf /home (the default)
    if (!sUrl) sUrl = this._defaultPath;
    var sUrl = sUrl.split("?")[0];
    if (sUrl === "/") sUrl = this._defaultPath;
    if (!this._urlPageMap[sUrl]) sUrl = this._defaultPath;
    if (this._urlPageMap[sUrl])
      var isBook = this._urlPageMap[sUrl].hasSubMenu;
    try {
      if (isBook) {
        var sTempPage = this._urlPageMap[sUrl].page;
        sUrl = this._pageDefinition[sTempPage].url;
      }
    } catch (ex) { }
    if (!sUrl) sUrl = this._defaultPath;
    return sUrl;
  },
  DoesUrlExist: function (sUrl) {
    if (this._urlPageMap[sUrl]) return true;
    return false;
  },
  RemoveUrl: function (sUrl) {
    delete this._urlPageMap[sUrl];
  },
  RemoveNavElem: function (oNav, index) {
    delete oNav[index];
    oNav.splice(index, 1);
  }
});
