{
  "cid"       : "98157662-79cd-446e-b5cf-c9360d2950b7",
  "version"   : "8",
  "title"     : "Clear Firefox History",
  "description": "Clear Firefox History",
  "ctype"     : "sprt_optimize",
  "fid"       : "d2de879e-58b3-43a3-9e63-600d9b38d674",
  "language"  : "en",
  "category"  : ["opt_privacy"]
}
