{
  "cid"       : "53dc6a66-5a82-43ef-929c-4fb2a9d6e77e",
  "version"   : "8",
  "title"     : "Clear DNS Cache",
  "description": "Clear DNS Cache",
  "ctype"     : "sprt_optimize",
  "fid"       : "d2de879e-58b3-43a3-9e63-600d9b38d674",
  "language"  : "en",
  "category"  : ["opt_browser_perf"]
}
