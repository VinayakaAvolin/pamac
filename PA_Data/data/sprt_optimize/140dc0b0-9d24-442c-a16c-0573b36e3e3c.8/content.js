{
  "cid"       : "140dc0b0-9d24-442c-a16c-0573b36e3e3c",
  "version"   : "8",
  "title"     : "Delete Cookies Stored by Internet Explorer",
  "description": "Delete Cookies Stored by Internet Explorer",
  "ctype"     : "sprt_optimize",
  "fid"       : "d2de879e-58b3-43a3-9e63-600d9b38d674",
  "language"  : "en",
  "category"  : ["opt_privacy"]
}
