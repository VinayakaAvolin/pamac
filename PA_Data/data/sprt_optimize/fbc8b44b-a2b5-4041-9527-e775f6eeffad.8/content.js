{
  "cid"       : "fbc8b44b-a2b5-4041-9527-e775f6eeffad",
  "version"   : "8",
  "title"     : "Disable Script Debugger",
  "description": "Disable Script Debugger",
  "ctype"     : "sprt_optimize",
  "fid"       : "d2de879e-58b3-43a3-9e63-600d9b38d674",
  "language"  : "en",
  "category"  : ["opt_browser_perf"]
}
