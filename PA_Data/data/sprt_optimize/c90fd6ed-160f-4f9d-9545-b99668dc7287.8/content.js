{
  "cid"       : "c90fd6ed-160f-4f9d-9545-b99668dc7287",
  "version"   : "8",
  "title"     : "Eliminate the Aero Peek Delay",
  "description": "Eliminate the Aero Peek Delay",
  "ctype"     : "sprt_optimize",
  "fid"       : "d2de879e-58b3-43a3-9e63-600d9b38d674",
  "language"  : "en",
  "category"  : ["opt_system_perf"]
}
