{
  "cid"       : "48c706d2-a681-44b4-8d73-fe742988f29a",
  "version"   : "8",
  "title"     : "Configure simultaneous downloads in Internet Explorer",
  "description": "Configure simultaneous downloads in Internet Explorer",
  "ctype"     : "sprt_optimize",
  "fid"       : "d2de879e-58b3-43a3-9e63-600d9b38d674",
  "language"  : "en",
  "category"  : ["opt_browser_perf"]
}
