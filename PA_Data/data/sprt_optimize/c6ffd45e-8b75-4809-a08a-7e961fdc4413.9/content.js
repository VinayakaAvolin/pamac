{
  "cid"       : "c6ffd45e-8b75-4809-a08a-7e961fdc4413",
  "version"   : "9",
  "title"     : "Delete Cookies Stored by Firefox",
  "description": "Delete Cookies Stored by Firefox",
  "ctype"     : "sprt_optimize",
  "fid"       : "d2de879e-58b3-43a3-9e63-600d9b38d674",
  "language"  : "en",
  "category"  : ["opt_privacy"]
}
