{
  "cid"       : "04362ab0-d7e8-4e4c-b6bc-4c6e9ee656e8",
  "version"   : "9",
  "title"     : "Set DNS Cache Size",
  "description": "Set DNS Cache Size",
  "ctype"     : "sprt_optimize",
  "fid"       : "d2de879e-58b3-43a3-9e63-600d9b38d674",
  "language"  : "en",
  "category"  : ["opt_browser_perf"]
}
