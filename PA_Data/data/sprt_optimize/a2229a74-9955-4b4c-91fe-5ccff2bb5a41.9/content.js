{
  "cid"       : "a2229a74-9955-4b4c-91fe-5ccff2bb5a41",
  "version"   : "9",
  "title"     : "Remove Temporary Files From User Context",
  "description": "Remove Temporary Files From User Context",
  "ctype"     : "sprt_optimize",
  "fid"       : "d2de879e-58b3-43a3-9e63-600d9b38d674",
  "language"  : "en",
  "category"  : ["opt_system_clean"]
}
