{
  "cid"       : "d56f67e0-589c-4fa7-9315-0edd2d6e6056",
  "version"   : "2",
  "title"     : "Clear Safari Browsing History Optimization on Mac",
  "description": "Clear Safari Browsing History Optimization on Mac",
  "ctype"     : "sprt_optimize",
  "fid"       : "d2de879e-58b3-43a3-9e63-600d9b38d674",
  "language"  : "en",
  "category"  : ["opt_privacy"]
}
