{
  "cid"       : "f5e58a6a-a203-4328-9484-f85dc6f52d44",
  "version"   : "2",
  "title"     : "Clear Firefox Browsing History Optimization on Mac",
  "description": "Clear Firefox Browsing History Optimization on Mac",
  "ctype"     : "sprt_optimize",
  "fid"       : "d2de879e-58b3-43a3-9e63-600d9b38d674",
  "language"  : "en",
  "category"  : ["opt_privacy"]
}
