{
  "cid"       : "0024a3d6-021f-4634-a055-7ea81785ffc6",
  "version"   : "9",
  "title"     : "Erase Recently Used Document List",
  "description": "Erase Recently Used Document List",
  "ctype"     : "sprt_optimize",
  "fid"       : "d2de879e-58b3-43a3-9e63-600d9b38d674",
  "language"  : "en",
  "category"  : ["opt_privacy"]
}
