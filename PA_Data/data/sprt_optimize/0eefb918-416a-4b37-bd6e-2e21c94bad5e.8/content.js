{
  "cid"       : "0eefb918-416a-4b37-bd6e-2e21c94bad5e",
  "version"   : "8",
  "title"     : "Cleanup Invalid Desktop Shortcuts",
  "description": "Cleanup Invalid Desktop Shortcuts",
  "ctype"     : "sprt_optimize",
  "fid"       : "d2de879e-58b3-43a3-9e63-600d9b38d674",
  "language"  : "en",
  "category"  : ["opt_desktop_clean"]
}
