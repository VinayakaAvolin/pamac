$ss.snapin = $ss.snapin || {};
$ss.snapin.accmgr = $ss.snapin.accmgr || {};
$ss.snapin.accmgr.helper = $ss.snapin.accmgr.helper || {};

(function () {

  SnapinAccountManagerController = SnapinBaseController.extend('snapin_account_manager', {
    model: null,
    location: null,
    selectedCat: null
  },

  {
    index: function (params, bFlag) {
      try {
        if (!this.Class.location) {
          this.Class.location = params.toLocation;
        }
        var viewCat = _helper.ParseIdFrom(params.requesturl);
        if (/account_manager_nav_summary/i.test(viewCat)) {
          this.renderSnapinView("snapin_account_manager", params.toLocation, "\\views\\am_welcome.html", { "ammodel": null });
          $ss.agentcore.utils.Sleep(100);
          that = this;
        }
        if(params.fromSnapin && params.fromSnapin == "Dashboard"){
          that.Class.dispatch("snapin_account_manager",".button_widget click",params);
        }
        if (bFlag){
          var view2Render = _helper.ParseIdFrom(params.requesturl);
          that.RenderAccmanView(view2Render, viewCat);
        }

      } catch(e) {}


      // This kills the tab press or CTRL (anything) but specifically CTRL+O that opens a new window.
      // The attachEvent is used to make sure that the onkeydown function is not overwritten
      try {
        document.attachEvent("onkeydown", function (e) {
          var e = e || event

          if (e.keyCode == 9 || e.keyCode == 17) // TAB key code is 9, cancel event // CTRL key code is 17, cancel event
          {
            e.keyCode = 0;
            e.cancelBubble = true;
            e.returnValue = false;
            return true;
          }
        });
      } catch (e) {
      }
    },

    indexActionWidget: function(params){
      try {
        var that= this;
        params.instanceParam = params.inInstanceParam || {};
        
        var params1 = $.extend({},params);
        //var view2Render = /accountwidget/i.test(templateid) ? "\\views\\accountWidget.html" : "\\views\\am_widget.html";

        that.renderSnapinView("snapin_account_manager", params1.toLocation, "\\views\\progress.html");

         that.GetUserModel(true)
        .done(function (model) {
          //Set language cookie for AM page to load in user selected language
          
          
          if(/(INACTIVE|UNKNOWN)/i.test(model.sStatus)){
            params.msg = $ss.agentcore.utils.LocalXLate("snapin_account_manager","snp_am_widget_register_link_txt");
            params.model = model;
          }else if (/(ACTIVE)/i.test(model.sStatus)){
            var daysLeftForExpiration = model.dDaysToExpire;
            var expirationPeriod = _config.GetConfigValue("snapin_acm_notification", "no_of_days_of_expiration", "10");
            
            //already expired
            if (parseInt(daysLeftForExpiration) < 0) {
              params.msg = $ss.agentcore.utils.LocalXLate("snapin_account_manager","snp_am_widget_pass_expired_txt"); /*convert double to single string*/
              params.model = model;
            }else if(parseInt(daysLeftForExpiration) == 0){
              params.msg = $ss.agentcore.utils.LocalXLate("snapin_account_manager","snp_am_widget_pass_expiry_today_txt"); /*convert double to single string*/
              params.model = model;
            }
            // days to expire password is less than the config value (expirationPeriod)
            else if ((parseInt(daysLeftForExpiration) <= parseInt(expirationPeriod)) && (parseInt(daysLeftForExpiration) > 0)) {
              params.msg = daysLeftForExpiration + $ss.agentcore.utils.LocalXLate("snapin_account_manager","snp_am_widget_pass_expiry_txt");
              params.model = model;
            }
            else {
              params.msg="";
              params.model = model;
            }
          }
          if(NavigationController.IsUrlValid("/paction"))  that.Class.dispatch("snapin_pendingactionwidget","indexAccMngr",params);
        })
        .fail(function (model){
          //alert(model.sErrMessage);
        })
        
      } catch(e) {}
    },
    
    GetUserModel: function (forceReload) {
      var that = this;
      if (!that.Class.model || forceReload === true) {
        var $d = _helper.GetModel();
        $d.then(function (model) {
          that.Class.model = model;
        });
        return $d;
      } else {
        return $.Deferred().resolve(that.Class.model).promise()
      }
    },
    
    RenderWelcomeView: function () {
      that.renderSnapinView("snapin_account_manager", that.Class.location, "\\views\\am_welcome.html", { "ammodel": this.Class.model });
    },
    
    RenderSummaryView: function () {
      var model = this.Class.model;
      if (model.bError) {
        that.RenderWelcomeView();
      }
      else if (model.bExtUrl) {
        that.RenderExternalView();
      }
      else {
        that.RenderInternalView();
      }
    },
    
    RenderRegistrationView: function (model) {
      if (model.bExtUrl)  that.RenderExternalView();
      else  that.RenderInternalView();
    },

    RenderInternalView: function () {
      that.renderSnapinView("snapin_account_manager", that.Class.location, "\\views\\am_internal.html", { "ammodel": this.Class.model });
    },

    RenderExternalView: function () {
      that.renderSnapinView("snapin_account_manager", that.Class.location, "\\views\\am_external.html", { "ammodel": this.Class.model });
    },

    RenderHistoryView: function () {
      that.renderSnapinView("snapin_account_manager", "#col_account_manager_content", "\\views\\am_history.html", { "ammodel": this.Class.model });
    },

    RenderAccmanView: function (model, selectedCat) {
      if (model.bInitialized === false) return;
      if (selectedCat === "account_manager_nav_history")  that.RenderHistoryView();
      else  that.RenderSummaryView();
    },

    InitHistory: function () {
      this.RenderHistoryView();
    },

    ".pmnavlinks click": function (params) {
      if (params.element.id) {
        params.requesturl = params.element.id;
        $(params.element).addClass("active");
        $('#account_manager_nav_summary').removeClass("active");
        this.index(params, true);
      }
    },

    ".pmnavlinksCK click": function (params) {
      if (params.element.id) {
        params.requesturl = params.element.id;
        $(params.element).addClass("active");
        $('#account_manager_nav_history').removeClass("active");
        this.index(params, true);
      }
    },

    ".btnNext click": function (params) {
      $(".loadingDetails").css("display", "block");
      $(".contentinfo").css("display", "none");
      var that = this;
      this.GetUserModel().done(function (model) {
        that.RenderRegistrationView(model);
      })
      .fail(function(model){
        alert(model.sErrMessage);
        that.RenderWelcomeView();
      });
    },

    ".btnrefresh click": function (params) {
      $(".loadingDetails").css("display", "block");
      $(".historycontent").css("display", "none");
      var that = this;
      this.GetUserModel(true).done(function (model) {
        that.RenderHistoryView();
      }).fail(function(model){
        that.RenderWelcomeView();
      });
    },
    
    ".btnRedirect click": function (params) {
      var sUrl = $(params.element).attr("url");
      this.renderSnapinView("snapin_account_manager", "#col_account_manager_content", "\\views\\am_iframe.html", { "url": sUrl });
    },

    ".button_widget click": function (params) {
    if(params.fromSnapin){
      var sUrl = params.subUrl;
    }else{
      var sUrl = $(params.element).attr("subUrl");
    }
      this.renderSnapinView("snapin_account_manager", "#col_account_manager_content", "\\views\\am_iframe.html", { "url": sUrl });
    }
  });

  //Private
  var _helper = $ss.snapin.accmgr.helper;
  var that;

})();
