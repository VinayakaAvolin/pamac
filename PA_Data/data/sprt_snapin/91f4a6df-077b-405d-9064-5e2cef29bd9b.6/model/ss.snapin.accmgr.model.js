/**
 * @author Shishiram
 */
$ss.snapin = $ss.snapin ||
{};
$ss.snapin.accmgr = $ss.snapin.accmgr ||
{};
$ss.snapin.accmgr.model = $ss.snapin.accmgr.model ||
{};

(function() {
$ss.snapin.accmgr.model.user = MVC.Model.extend({
  init: function (oControl) {
    try {
      this.sName = "",
      this.sDomain = "",
      this.sStatus = "",
      this.bExtUrl = false,
      this.sExtUrl = "";
      this.sRegUrl = "",
      this.sResetUrl = "",
      this.history = "",
      this.dLastReset = "",
      this.dLastUnlock = "",
      this.bError = false,
      this.sErrMessage = "",
      this.bInitialized = false,
      this.sServerUrl = "",
      this.ControllerCallBack = oControl
    }
    catch (e) {
      throw {
        name: 'TypeError',
        message: _const.PR_CONSTRUCTOR_FAILED
      }
    }
  }
});

  var _helper = $ss.snapin.accmgr.helper;
  var _utils = $ss.agentcore.utils;

  jQuery.support.cors = true;
}) ()