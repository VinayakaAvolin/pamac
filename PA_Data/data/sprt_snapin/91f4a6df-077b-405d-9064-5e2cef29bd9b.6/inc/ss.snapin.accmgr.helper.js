/** @namespace Provides System Information functionality */

$ss.snapin = $ss.snapin || {};
$ss.snapin.accmgr = $ss.snapin.accmgr || {};
$ss.snapin.accmgr.helper = $ss.snapin.accmgr.helper ||{};

(function() {

  $.extend($ss.snapin.accmgr.helper, {

  Initialize: function(sParams) {
  },

    //This function currently not being used
	  IsRunningInSysAccount: function () {
	    //Used for checking Bcont launched from user or system account 
	     var scriptPath = _config.ExpandAllMacros("%CONTENTPATH%");
	     var scriptFolder = scriptPath.split('\\')[0] + "\\" + scriptPath.split('\\')[1] + scriptPath.split('\\')[2];
	     var osName = _utils.system.GetOS();
	     if (osName.search("XP") > -1) {
	       if (scriptFolder.search("AllUsers") > -1) {
	         $ss.agentcore.events.SendByName("DISABLE_NAVIGATION");
	         $ss.agentcore.events.SendByName("DISABLE_HIDE_NW_CHECK");
	         return true;
	       }
	     }
	     else {
	       if (scriptFolder.search("ProgramData") > -1) {
	         $ss.agentcore.events.SendByName("DISABLE_NAVIGATION");
	         $ss.agentcore.events.SendByName("DISABLE_HIDE_NW_CHECK");
	         return true;
	       }
	     }
	    return false;
	  },

    ParseIdFrom : function(sURL) {
      var sSelectedCat = sURL;
      if (sSelectedCat == undefined || sSelectedCat == null)  return;
      if (sSelectedCat.indexOf("?") > -1) {
        var oUrl = _utils.GetObjectFromQueryString(sURL);
        if (oUrl != false && oUrl.id !== undefined) {
          sSelectedCat = oUrl.id;
        }
      }
      return sSelectedCat;
    },
    
    GetModel: function () {
      var model = new $ss.snapin.accmgr.model.user();
      var $pDef = $.Deferred();
      try {
        var sUrl = NavigationController.GetServerUrl() + "/sdcxuser/accmgr/asp/amdefault.asp";
        $.ajax({
          type: "GET",
          url: sUrl,
          dataType: 'json',
          data: { amOpType: "getjson", agent: "pa" }
        })
        .then(function (data) {
          model = $.extend(model, data);
          ExtractURLData(model);
          UpdateUserInfo(model);
          GetUserInfo(model.sName, model.sDomain)
          .then(function (historyData) {
            ExtractHistoryStatus(model, historyData);
            $pDef.resolve(model);
          })
          .fail(function () {
            model.sErrMessage = _utils.LocalXLate("snapin_account_manager", "snp_accman_not_contact") + _utils.LocalXLate("snapin_account_manager", "snp_accman_contact_help");
            $pDef.reject(model);
          });
          return $pDef;
        })
        .fail(function () {
          if (!model.sErrMessage) {
            model.sErrMessage = _utils.LocalXLate("snapin_account_manager", "snp_accman_not_contact") + " " + _utils.LocalXLate("snapin_account_manager", "snp_accman_contact_help");
          }
          $pDef.reject(model);
        });
        return $pDef.promise();
      } catch (e) {
          model.sErrMessage = _utils.LocalXLate("snapin_account_manager", "snp_accman_not_contact") + " " + _utils.LocalXLate("snapin_account_manager", "snp_accman_contact_help");
          return $pDef.reject(model).promise();
      }

      function GetUserInfo(user, domain) {
        var sUrl = NavigationController.GetServerUrl() + "/sdcxuser/accmgr/AMSnapin/GetUserInfo";
        return $.ajax({
          type: "POST",
          url: sUrl,
          data: { DomainName: domain, User: user, Snapin: true }
        });
      }


    }
  });
  function ExtractURLData(oExt) {
    if (oExt.isexternal !== "False") {
      oExt.bExtUrl = true;
      oExt.sExtUrl = decodeURIComponent(oExt.exturl);
    } else {
      oExt.bExtUrl = false;
      oExt.sRegUrl = decodeURIComponent(oExt.intregurl);
      oExt.sResetUrl = decodeURIComponent(oExt.intreseturl);
      oExt.sServerUrl = decodeURIComponent(oExt.serverurl);
    }
  }

  function UpdateUserInfo(oUser) {
    try {
      var _si = $ss.agentcore.diagnostics.smartissue;
      var absPath = $ss.getSnapinAbsolutePath("snapin_account_manager");
      // Create Issue
      var issueID = _si.CreateIssue("SystemInfo", true, absPath + "\\smartissue\\default.xml", false, false, null);
      var sIssuePath = $ss.getAttribute("startingpoint").replace("\\data", "\\state\\Issues\\");
      var sSmartIssueXml = _xml.LoadXML(sIssuePath + issueID + ".xml", false, null);
      var xpathNode = "//PROPERTY[@*]";
      var nodes = _xml.GetNodes(xpathNode, "", sSmartIssueXml);

      oUser.sDomain = _xml.GetNodeText(nodes[0], 'VALUE');
      oUser.sName = _xml.GetNodeText(nodes[2], 'VALUE').replace(oUser.sDomain + "\\", "");

      _si.DeleteIssue(issueID);
    } catch (e) { }
  }
  function ExtractHistoryStatus(oUser, json) {
    //Store the User history info and current status
    oUser.bInitialized = true;
    oUser.sStatus = json.Status || "UNKNOWN";
    oUser.sFirstName = json.FirstName;
    oUser.sLastName = json.LastName;
    oUser.dLastReset = (typeof (json.History) != 'undefined') ? GetDateToDisplay(json.History.LastResetDate) : "";
    oUser.dLastUnlock = (typeof (json.History) != 'undefined') ? GetDateToDisplay(json.History.LastUnlockDate) : "";
    oUser.dDaysToExpire = (typeof (json.History) != 'undefined') ? json.History.DaysToExpire : "";
  }

  function GetDateToDisplay(sDate) {
    if (sDate === "") {
      return _utils.LocalXLate("snapin_account_manager", "snp_accman_not_avail", "Not Available");
    }
    //sDate += " UTC";
    try {
      var date = new Date(sDate);
      date.toString();
      var dd = date.getDate()   ;    
      var mm = date.getMonth() ;
      var yy = date.getFullYear();
      var dateOnly = new Date(yy,mm,dd);
      return dateOnly;
    }
    catch (ex) { }
    return sDate;
  }
  //Private Region
  //Required Registry Paths
   var _xml = $ss.agentcore.dal.xml;
   var _utils = $ss.agentcore.utils;
   var _config = $ss.agentcore.dal.config;
})();
