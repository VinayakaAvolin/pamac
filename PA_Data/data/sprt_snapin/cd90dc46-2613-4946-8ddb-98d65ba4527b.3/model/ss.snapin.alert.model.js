/**
 * @author Shishiram
 */
$ss.snapin = $ss.snapin ||
{};
$ss.snapin.alert = $ss.snapin.alert ||
{};
$ss.snapin.alert.model = $ss.snapin.alert.model ||
{};

/*
 * This class will hold the information existing Alerts
 */
(function() {

  $ss.snapin.alert.model.AlertFieldInfo = Class.extend({

    init: function() {

      try {
        this._alertPath = "";
        this._existingAlerts = [];
        this._numOfExistingAlerts = 0;
        this._validAlerts = [];
        this._numOfAlertsToList = {};
        this._iNumOfAlerts = 0;
        this._exception = $ss.agentcore.exceptions;
        this.GetAlerts();
      }
      catch (ex) {
        this._exception.HandleException(ex, '$ss.snapin.contactus.model.AlertFieldInfo', 'init', arguments);
      }
    },

    GetAlerts: function() {

      try {

        var dirPath = _config.ExpandAllMacros("%CONTENTPATH%");
        this._alertPath = dirPath + "\\sprt_alertevents";

        this._validAlerts = this.GetValidAlerts();

        //sort the alerts
        this._validAlerts.sort(this.SortLogic);

        if (this._validAlerts) {
          this._iNumOfAlerts = this._validAlerts.length;
          this._numOfAlertsToList["num_alerts_per_page_detailpage"] = _config.GetConfigValue("snapin_alert", "num_alerts_per_page_detailpage", "");
          this._numOfAlertsToList["num_alerts_per_widgetpage"] = _config.GetConfigValue("snapin_alert", "num_alerts_per_widgetpage", "");
        }
      }
      catch (e) {
        this._exception.HandleException(ex, '$ss.snapin.contactus.model.AlertFieldInfo', 'GetAlerts', arguments);
        return false;
      }
      return true;
    },

    SortLogic: function(lhs, rhs) {
      var lm = lhs.receivedDate.valueOf();
      var rm = rhs.receivedDate.valueOf();
      // descending
      if (lm > rm)
        return -1;
      if (lm < rm)
        return 1;
      return 0;
    },

    GetValidAlerts: function(sAlertGuid) {
      this._existingAlerts = _content.GetContentsByAnyType(["sprt_alertevents",,"sprt_realtimealert"]);
      this._validAlerts = _content.GetValidContentsForDisplay(this._existingAlerts);
      return this.UpdateAlertListWithDate();
    },

    GetNumOfAlertsPerPage: function(sPageType) {
      return this._numOfAlertsToList[sPageType];
    },

    GetValidAlert: function() {
      return this._validAlerts;
    },

	GetValidAlertByCid: function(alertId) {	  
			return $.grep(this._validAlerts, function(e){ return e.cid == alertId; });			
    },
	
    GetNumOfValidAlerts: function() {
      return this._iNumOfAlerts;
    },

    GetAlertTitle: function(alertId) {
      for (i = 0; i < this._iNumOfAlerts; i++) {
        if (this._validAlerts[i]["cid"] == alertId)
          return this._validAlerts[i]["title"];
      }
    },

    UpdateAlertListWithDate: function() {
     
         var bMac = $ss.agentcore.utils.IsRunningInMacMachine();
  var _file = $ss.agentcore.dal.file;
        if (bMac) {
//
          var path = "";
          for (var i = 0; i < this._validAlerts.length; i++) {
            path = this.GetAlertContentPath(this._validAlerts[i]["ctype"], this._validAlerts[i]["cid"], this._validAlerts[i]["version"]);
            path = path + this._validAlerts[i]["cid"] + "." + this._validAlerts[i]["version"] + ".xml";
            var recDate = new Date(_file.GetCreatedDate(path));
            this._validAlerts[i]["receivedDate"] = recDate;
            this._validAlerts[i]["received"] = recDate.toLocaleDateString();
          }
        }else{
      try {
        var oActiveX = $ss.agentcore.utils.activex.GetObjInstance();
        var oFso = oActiveX.CreateInternalObject("filesystem"); // to be replaced by use agentcore api
        var oFile = null;
        var path = "";
        for (var i = 0; i < this._validAlerts.length; i++) {
          path = this.GetAlertContentPath(this._validAlerts[i]["ctype"], this._validAlerts[i]["cid"], this._validAlerts[i]["version"]);
          path = path + this._validAlerts[i]["cid"] + "." + this._validAlerts[i]["version"] + ".xml";
          oFile = oFso.GetFile(path);
          var recDate = new Date(oFile.DateCreated);
          this._validAlerts[i]["receivedDate"] = recDate;
          this._validAlerts[i]["received"] = recDate.toLocaleDateString();
          oFile = null;
        }
        oFso = null;
        oActiveX = null;
        }catch (ex) {
        oFso = null;
        oActiveX = null;
      }
        }
      
      return this._validAlerts;
    },

    GetAlertHTMLPath: function(sContentType, contentID, version) {

      var path = this.GetAlertContentPath(sContentType, contentID, version);
      path = path + "Alert Content.htm";

      return path;
    },

    GetAlertContentPath: function(sContentType, sContentGuid, sContentVersion) {
      var alertPath = $ss.GetAttribute('startingpoint') + "\\" +
      sContentType +
      "\\" +
      sContentGuid +
      "." +
      sContentVersion +
      "\\";
      return alertPath;
    }

  });

  var _config = $ss.agentcore.dal.config;
  var _content = $ss.agentcore.dal.content;
  var _xml = $ss.agentcore.dal.xml;
  var _file = $ss.agentcore.dal.file;

})();
