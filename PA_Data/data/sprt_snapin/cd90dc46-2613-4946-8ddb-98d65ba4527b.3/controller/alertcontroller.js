SnapinAlertController = SnapinBaseController.extend('snapin_alert', {
  oAlertModel: "",
  exception: $ss.agentcore.exceptions,
  oViewAll: "",
  InitializeAlertModel: function() {
    if (this.oAlertModel === "") {
      this.oAlertModel = new $ss.snapin.alert.model.AlertFieldInfo();
    }
    $ss.agentcore.events.Subscribe("BROADCAST_ON_SYNCNEWDATA", SnapinAlertController.RefreshAlerts);
  },
  templates: {
    detail: {
    page: "\\views\\alert_template.html",
    toLocation: ""
  },
    widget: {
      page: "\\views\\alertWidget.html",
      toLocation: ""
    }
  },
  RefreshAlerts: function() {
    SnapinAlertController.oAlertModel = "";
  }

}, {

  index: function(params) {
    try {
      this.Class.InitializeAlertModel();
      var templateid = "detail";
      //get the instanceparam if available.
      params.instanceParam = params.inInstanceParam || {};
      templateid = params.snpalert_templateid || params.instanceParam.snpalert_templateid || "detail";
      if (!this.Class.templates[templateid]) {
        templateid = "detail";
      }
      //get the toLocation where the snapin to be displayed.
      if (params.toLocation) {
        this.Class.templates[templateid].toLocation = params.toLocation;
      }
      if (templateid === "detail") {
        this.DisplayDetailTemplatePage(params);
      }
      if (templateid === "widget") {
        this.DisplayWidgetTemplatePage(params);
      }
    }
    catch (ex) {
      this.Class.exception.HandleException(ex, 'snapin_alertcontroller', 'index', params);
    }
  },

  DisplayDetailTemplatePage: function(params) {
    //This is responsible to render template id "0"
    var pageId = 1;
    var templateId = "detail";
    //Get the Page Id;
    try {
      if (params.snp_alert_pageid) {
        pageId = params.snp_alert_pageid;
      }
      if(SnapinAlertController.oViewAll=="yes"){
        var data = this.UpdateDisplayList(pageId);
      }
      else{
        var data = this.UpdateDisplayFilteredList(pageId,params.element.id);
      }
      var alertIndex = 0;

      //If control is transferred from summary
      if (params.element.id) {
        alertIndex = this.GetAlertIndex(params.element.id, data);
        data["currentPage"] = parseInt((alertIndex / data["itemsPerPage"]) + 1);
      }

      this.renderSnapinView("snapin_alert", this.Class.templates[templateId].toLocation, this.Class.templates[templateId].page, {
        "alerts": data
      });

      if (data.length) {
        // for alert click from home page
        if (params.element.id) {
          this.DisplayAlert(data[alertIndex]);
        }
        else {
          this.DisplayAlert(data[((pageId - 1) * data["itemsPerPage"])]);
        }
      }
    }
    catch (ex) {
      this.Class.exception.HandleException(ex, 'snapin_alertcontroller', 'DisplayDetailTemplatePage', params);
    }
  },

  DisplayWidgetTemplatePage: function(params) {
    //This is responsible to render widget
    var templateId = "";
    templateId = params.snpalert_templateid || params.instanceParam.snpalert_templateid;

    var data;
    if (templateId === "widget") {
      data = this.UpdateWidgetDisplayList();
    }
    //params.alertList = data;  
    this.renderSnapinView("snapin_alert", this.Class.templates[templateId].toLocation, this.Class.templates[templateId].page, {
       "alerts": data
    });

  },

  UpdateWidgetDisplayList: function() {
    var validAlert = this.Class.oAlertModel.GetValidAlert();
    validAlert["itemsPerWidgetPage"] = this.Class.oAlertModel.GetNumOfAlertsPerPage("num_alerts_per_widgetpage");
    return validAlert;
  },

  GetMaxSetToDisplay: function(length, iPage) {
    var numAlertInList = iPage;
    var numOfSets = 0;
    var maxSetToDisplay = 1;
    numOfSets = parseInt(length / numAlertInList);
    var remainingAlerts = length - (numAlertInList * numOfSets);
    if (remainingAlerts > 0) {
      maxSetToDisplay = numOfSets + 1;
    }
    else {
      maxSetToDisplay = numOfSets;
    }
    return maxSetToDisplay;
  },

  UpdateDisplayList: function(pageId) {
    var validAlert = this.Class.oAlertModel.GetValidAlert();
    validAlert["itemsPerPage"] = parseInt(this.Class.oAlertModel.GetNumOfAlertsPerPage("num_alerts_per_page_detailpage"));
    validAlert["currentPage"] = pageId;
    validAlert["totalPages"] = this.GetMaxSetToDisplay(validAlert.length, validAlert["itemsPerPage"]);
    return validAlert;
  },

  UpdateDisplayFilteredList: function(pageId,alertGuid) {
    var validAlert = this.Class.oAlertModel.GetValidAlertByCid(alertGuid);
	
    validAlert["itemsPerPage"] = parseInt(this.Class.oAlertModel.GetNumOfAlertsPerPage("num_alerts_per_page_detailpage"));
    validAlert["currentPage"] = pageId;
    validAlert["totalPages"] = this.GetMaxSetToDisplay(validAlert.length, validAlert["itemsPerPage"]);
    return validAlert;
  },
  
  GetAlertIndex: function(alertGuid, validAlert) {
    var i = 0;
    for (i = 0; i < validAlert.length; i++) {
      if (validAlert[i]["cid"] == alertGuid)
        return i;
    }

    return 0;
  },

  DisplayAlert: function(defAlert) {
    if (defAlert) {
      var iframeSrc = this.Class.oAlertModel.GetAlertHTMLPath("sprt_alertevents", defAlert["cid"], defAlert["version"]);
      if (defAlert["ctype"] == "sprt_realtimealert")
		{
			iframeSrc = this.Class.oAlertModel.GetAlertHTMLPath("sprt_realtimealert", defAlert["cid"], defAlert["version"]);
		}
      $("#panelContent_" + defAlert["cid"]).addClass("in");
      $("#snapin_alerts_content_" + defAlert["cid"]).attr("src",iframeSrc);
      //Reoprt logging for each alert viewed
      $ss.agentcore.reporting.Reports.LogContentStart("snapin_alert", defAlert["cid"], defAlert["ctype"], defAlert["version"]);
    }
  },

  TruncateExtraChars: function(sAlertTitle, maxLength) {
    if (sAlertTitle) {
      var iAlertLength = sAlertTitle.length;
      if (iAlertLength > maxLength) {
        sAlertTitle = sAlertTitle.substring(0, maxLength);
        sAlertTitle = sAlertTitle + "...";
      }
    }
    return sAlertTitle;
  },

  ".alertWidgetRefresh click": function(params) {
    params.snpalert_templateid = "widget";
    this.index(params);
  },

  ".alertPagination click": function(params) {
    var pageId = $(params.element).attr("pageid");
    var templateId = 0;
    params.snpalert_templateid = templateId;
    params.snp_alert_pageid = parseInt(pageId);
    this.index(params);
  },

  ".snpAlertClass click": function(params) {
    var sAlertId = params.element.id;
    var sAlertVersion = $(params.element).attr("verid");
    var iframeSrc = this.Class.oAlertModel.GetAlertHTMLPath("sprt_alertevents", sAlertId, sAlertVersion);
    if ($(params.element).attr("ctype") == "sprt_realtimealert")
		{
			iframeSrc = this.Class.oAlertModel.GetAlertHTMLPath("sprt_realtimealert", sAlertId, sAlertVersion);
		}
    $("#snapin_alerts_content_" + sAlertId).attr("src",iframeSrc);

    //Report logging for each alert viewed
   if ($(params.element).attr("ctype") == "sprt_realtimealert")
		{
			$ss.agentcore.reporting.Reports.LogContentStart("snapin_alert", sAlertId, "sprt_realtimealert", sAlertVersion);
		}
    else
    {
      $ss.agentcore.reporting.Reports.LogContentStart("snapin_alert", sAlertId, "sprt_alertevents", sAlertVersion);
    }
  },

  "#scriptActionViewAllAlert click": function(params){	
	SnapinAlertController.oViewAll="yes";
	this.Class.dispatch("snapin_alert", "index", params);
	SnapinAlertController.oViewAll="";
	$('#scriptActionViewAllAlert').attr('disabled',true);	
  }

});
