$ss.snapins = $ss.snapins || {};
$ss.snapins.selfservicesearch = $ss.snapins.selfservicesearch || {};
$ss.snapins.selfservicesearch.summary = $ss.snapins.selfservicesearch.summary || {};

(function() {
  $.extend($ss.snapins.selfservicesearch.summary,
  {
    TruncateExtraChars: function(sText, maxLength) {
      if (sText) {
        var iStrLen = sText.length;
        if (iStrLen > maxLength) {
          sText = sText.substring(0, maxLength);
          sText = sText + "...";
        }
      }
      return sText;
    },

    RenderCellTemplate :function(oContent){
      var regex = new RegExp('^Featured$','i');
      var bCheckBox = false;
      var tTooltip = "";
      var isAdvancedOptContent = false;
      switch(oContent.ctype){
        case "sprt_actionlight" : //autofix
          tTooltip = _utils.LocalXLate("snapin_selfservicesearch","snp_search_filter_autofix_tooltip");
          break;
        case "sprt_protection" :  //repair
          tTooltip = _utils.LocalXLate("snapin_selfservicesearch","snp_search_filter_repair_tooltip");
          break;
        case "sprt_optimize":  //optimize
          if(typeof oContent.category === "undefined") oContent.category = [""];
          var arrCategory = $.isArray(oContent.category) ? oContent.category : oContent.category.split(",");
          $.each(arrCategory, function (iIndex, sValue) {
            if(typeof sValue === "object") sValue = sValue.toString().trim();
            else if(typeof sValue === "string") sValue = sValue.trim();
            if(!regex.test(sValue)){
              if(sValue == "opt_advanced" || sValue == "Advanced Commands"){
                isAdvancedOptContent = true;
                tTooltip = _utils.LocalXLate("snapin_selfservicesearch","snp_search_filter_advancedoptimize_tooltip"); 
              }else{
                bCheckBox = !(bAllSolutions);
                tTooltip = _utils.LocalXLate("snapin_selfservicesearch","snp_search_filter_optimize_tooltip");
              }
            }
          });
          break;
        case "sprt_download" :  //install
          bCheckBox = !(bAllSolutions);
          tTooltip = _utils.LocalXLate("snapin_selfservicesearch","snp_search_filter_install_tooltip");
          break;
        case "sprt_articlefaq":  //articles
        case "sprt_rawdoc_inline":
        case "sprt_rawdoc":
          if(oContent.ctype === "sprt_rawdoc") tTooltip = _utils.LocalXLate("snapin_selfservicesearch","snp_search_filter_rawdoc_tooltip");
          else if(oContent.ctype === "sprt_rawdoc_inline") tTooltip = _utils.LocalXLate("snapin_selfservicesearch","snp_search_filter_inlinerawdoc_tooltip");
          else tTooltip = _utils.LocalXLate("snapin_selfservicesearch","snp_search_filter_articles_tooltip");
          break;
        case "sprt_alertevents" :  //alerts
          tTooltip = _utils.LocalXLate("snapin_selfservicesearch","snp_search_filter_alert_tooltip");
          break;
        case "sprt_msg" :  //sprtmsg
          tTooltip = _utils.LocalXLate("snapin_selfservicesearch","snp_search_filter_messages_tooltip");
          break;
        default : break;
      }  
      var sCheckboxProp = "none";
      var sFeaturedProp = "none";
      
      if(bCheckBox) sCheckboxProp = "block";
      if(oContent.featured) sFeaturedProp = "";

      var sTitle = oContent.title || "";
      var sDescription = oContent.description || "";
      sDescription = _utils.ui.EncodeHtml($.trim(sDescription));
      var aCategory = oContent.category || [""];
      var sCtype = oContent.ctype || "";
      
      sTruncatedTitle = this.TruncateExtraChars(sTitle, _config.GetConfigValue("snapin_search", "max_title_len", "90"));
      sTruncatedDescription = this.TruncateExtraChars(sDescription, _config.GetConfigValue("snapin_search", "max_desc_len", "90"));
      
      
      if(!$.isArray(aCategory)) {
        aCategory = aCategory.split(",");
      }  
      var sCategories = "";
      var _that = this;
      var sCategoriesDelimiter = ", ";

      $.each(aCategory, function(iIndex, sCategory) {
        if(typeof sCategory === "object") sCategory = sCategory.toString().trim();
        else if(typeof sCategory === "string") sCategory = sCategory.trim();
        if(!regex.test(sCategory)){
          var sLocalizedCategories = _utils.LocalXLate("snapin_selfservicesearch", sCategory);
          var sCatVal = sLocalizedCategories;
          if (sCatVal != "")
            sCategories += "#"+ '<span class="displaycategory" title="' + _utils.LocalXLate("snapin_selfservicesearch", "snp_search_sortby_category_txt") + ': ' + _utils.LocalXLate("snapin_selfservicesearch", sLocalizedCategories) + '">' + sCatVal + '</b></span>' + sCategoriesDelimiter;
        }
      });

      sCategories = sCategories.slice(0 , sCategories.length - sCategoriesDelimiter.length);
  
      _that = null;
  
      var template = '<div class="cell-inner"><div class="cell-left"><div class="cell-left-contents"><input type="checkbox" class="filterChkbox" style="float:left;display:' 
                      + sCheckboxProp + '"  id="chkbox_'+ oContent.cid  
                      + '" guid="'+ oContent.cid +'" version="'+ oContent.version 
                      +'" ctype="'+ oContent.ctype 
                      + '" name="cbFilters"/><div><i class="fa fa-star featuredsoln" data-toggle="tooltip" ' 
                      + 'title="' + _utils.LocalXLate("snapin_selfservicesearch","snp_search_filter_featured") 
                      + '" style="display:' + sFeaturedProp + '"></i></div><div id="imageIcon" class="content-' + sCtype +'" data-toggle="tooltip" '
                      +'title="' + tTooltip + '"></div></div></div><div class="cell-main" id="cellmain_'+ oContent.cid  
                      + '" guid="'+ oContent.cid +'" version="'+ oContent.version 
                      +'" ctype="'+ oContent.ctype + '" isAdvanced="'+ isAdvancedOptContent 
                      +'"><div><span title="' + sTitle + '"><b>' + sTruncatedTitle + '</b></span><br/><span title="' + sDescription + '">' 
                      + sTruncatedDescription + '</span></div><div class="cell-main-contents">' + sCategories +'</div></div></div></div>'
      return template;
    }   
})
var _config = $ss.agentcore.dal.config;
var _utils = $ss.agentcore.utils;
var _file = $ss.agentcore.dal.file;
})();
