$ss.snapins = $ss.snapins || {};
$ss.snapins.selfservicesearch = $ss.snapins.selfservicesearch || {};
$ss.snapins.selfservicesearch.summary = $ss.snapins.selfservicesearch.summary || {};


function renderCell(row, cell, value, columnDef, dataContext) {
  return $ss.snapins.selfservicesearch.summary.RenderCellTemplate(dataContext);
}

var selectedRowIds;
var Gridresult;
var renderedresult;

SnapinSelfservicesearchController = SnapinBaseController.extend('snapin_selfservicesearch', {
  searchGrid: null,
  controller: null,
  searchCriteria: {filter:{}, sort:{}},
  init: function () {
    controller = this;
  },
  index: function (params) {
    if (!params.backToSearch || params.backToSearch ==false)  this.searchCriteria = {filter:{}, sort:{}};
    selectedRowIds=[];
    selectedCat = "";
    bAllSolutions = true;
    renderedresult = [];
    if(this.Class.searchGrid) delete this.Class.searchGrid;

    this.mapLocalizedCategories();
    if(params.toLocation) snapinloc = params.toLocation;
    this.renderSnapinView("snapin_selfservicesearch", snapinloc, "\\views\\selfservicesearch.html");
    if(params.SearchResult){
      fromSnapin = "Search";
      if($.isArray(params.SearchResult) && params.SearchResult.length>0 ){
        renderedresult = params.SearchResult;
      }else{
          oContentcountDict =[0];
          controller.updateDataToRender([]);
      }
    }else{
        renderedresult = $ss.helper.GetSearchableContents();
    }
    if (typeof(this.searchCriteria.filter.by) !== "undefined") {
      bAllSolutions = (this.searchCriteria.filter.by === "allsolutions"); //to enable checkbox when navigating from Back button
    }
    controller.renderData(renderedresult);
    controller.setUIforFilter("#allsolutions");

    //Retain the UI onclick of back
    if (typeof(this.searchCriteria.sort.by) !== "undefined") this.setUIforSort(this.searchCriteria.sort.by);
    if (typeof(this.searchCriteria.filter.by) !== "undefined") this.setUIforFilter("#" + this.searchCriteria.filter.by);
  },

  setUIforSort: function(cat){
    var $el = $('#' + cat);
    var strSorted = _utils.LocalXLate("snapin_selfservicesearch", "snp_search_sortedby_txt") 
                    + _utils.LocalXLate("snapin_selfservicesearch", "snp_search_sortby_"+cat.toLowerCase()+"_txt");
    $el.parents(".dropdown").find('#groupsel').html(_utils.GetTruncatedString(strSorted,maxStrLen) + '<span class="caret"></span>');
  },

  setUIforFilter: function(cat){
    this.applyDefaultStyle();
    this.applyNewStyle(cat);
  },

  "#sprt_sortby_filter li a click": function(params){
    var selectedCat = params.element.id;
    
    //Setting default order to ascending
    if(this.searchCriteria.sort.by!==selectedCat) this.searchCriteria.sort.asc = "";
    //Clearing the arrows/UI from previous sort filter
    $('#' + this.searchCriteria.sort.by).removeClass();

    this.searchCriteria.sort.by = selectedCat;
    this.searchCriteria.sort.asc = !this.searchCriteria.sort.asc;
    if (selectedCat) {
      var me = this;
      this.setUIforSort(selectedCat);
      var $el = $('#' + selectedCat);
      controller.updateDataToRender(Gridresult, {}, { sortBy: selectedCat, asc: this.searchCriteria.sort.asc })
      .then(function(){
        var $parent = $el.parents(".dropdown").find('#groupsel').addClass("sortBy");
        $el.addClass(me.searchCriteria.sort.asc !==true ? 'fa fa-arrow-down' : 'fa fa-arrow-up');
      });
    }
  },

  ".cell-main click": function(params){
    var $id = $('#' + params.element.id);
    var ctype = $id.attr("ctype");
    var cid = $id.attr("guid");
    var version = $id.attr("version");
    switch(ctype){
      case "sprt_alertevents":
        params.element.id = cid;
        params.requestPath = "/sa/alert";
        params.requesturl = "/sa/alert?snp_alertguid=" + cid;
      if($.inArray(cid ,selectedRowIds) === -1) selectedRowIds.push(cid);
        params.action = ".navigate click";
        try {
          this.Class.dispatch("navigation", "index", params);
        }catch (ex) {
          this.Class.logger.error("Alert View Click: Error in Dispatching to Navigation Controller %1%", ex.message);
        }
        break;
      case "sprt_articlefaq" :  //articles
      case "sprt_rawdoc" :
      case "sprt_rawdoc_inline" :
        if(NavigationController.IsUrlValid("/sa/soln")){
          if (!initialized) {
            params.controller = "snapin_solutions";
            params.action = "SolutionFaq click";
            params.event = params.event || {};
            params.element = params.element || {};
            this.Class.dispatch("snapin_solutions", "index", params);
            initialized = true;
          }
          $ss.snapin.solutions.helper.RenderSolutionView(ctype, cid, version);
        }
        break;
      case "sprt_msg":
        params.requesturl = "/sa/messagesviewer";
        params.requestPath = "/sa/messagesviewer";
        params.SelectedId = ([{ "id": cid}]);
        params.action = ".navigate click";
        try {
          this.Class.dispatch("navigation", "index", params);
        }catch (ex) {
          this.Class.logger.error("Message View Click:Error in Dispatching to Navigation Controller %1%", ex.message);
        }
        break;
      case "sprt_protection" :
        params.requesturl = "/sa/protect";
        params.requestPath = "/sa/protect";
        params.SelectedId = ([{ "id": cid}]);
        params.element.id = "lnkRestore";
        params.action = ".navigate click";
        try {
          this.Class.dispatch("navigation", "index", params);
        }catch (ex) {
          this.Class.logger.error("Repair click:Error in Dispatching to Navigation Controller %1%", ex.message);
        }
        break;
      case "sprt_optimize" :
        isAdvancedcontent = $id.attr("isAdvanced");
        params.requesturl = (isAdvancedcontent === "true") ? "/perfman?id=perfman_nav_advanced&isadvanced=perfman_nav_advanced_userselect" : "/perfman?id=perfman_nav_quick&idselect=perfman_nav_quick_userselect";
        params.requestPath = "/perfman";
        params.SelectedId = ([{ "id": cid}]);

      if($.inArray(cid ,selectedRowIds) === -1) selectedRowIds.push(cid);
        params.action = ".navigate click";
        try {
          this.Class.dispatch("navigation", "index", params);
        }catch (ex) {
          this.Class.logger.error("Optimize click:Error in Dispatching to Navigation Controller %1%", ex.message);
        }
        break;
      case "sprt_download":
        params.requesturl = "/dm";
        params.requestPath = "/dm";
        params.SelectedId = ([{ "id": cid}]);
        try {
          this.Class.dispatch("navigation", "index", params);
        }catch (ex) {
          this.Class.logger.error("Install click:Error in Dispatching to Navigation Controller %1%", ex.message);
        }
        break;
      case "sprt_actionlight":
        params.action = ".SolutionAction click";
        params.controller = "snapin_solutions";
        params.callerSnapin = "Search_Snapin";
        params.requesturl = '/sa/soln?snpsoln_template=SolutionView&snpsolution_cid=' + cid + '&snpsolution_version=' + version + '&snpsolution_ctype='+ctype +'&snpsoln_doneurl=/sa/soln';
        params.requestPath ='/sa/soln?snpsoln_template=SolutionView&snpsolution_cid=' + cid + '&snpsolution_version=' + version + '&snpsolution_ctype='+ctype +'&snpsoln_doneurl=/sa/soln';
        try {
          this.Class.dispatch("navigation", "index", params);
        }catch (ex) {
          this.Class.logger.error("Auto-Fix click: Error in Dispatching to Navigation Controller %1%", ex.message);
        }
    }

  },

  updateSelectedRowsonGrid : function(Griddata){
    $(Griddata).each(function(k,v){
      v.checked = ($.inArray(v.cid , selectedRowIds) > -1 )? true : false;
      if (v.checked) $('#chkbox_' + v.cid).prop("checked", "checked");
      else $('#chkbox_' + v.cid).prop("checked", false);
    });
    Gridresult = Griddata;
  },

  updateGridDataonEvent : function(){
    var allRows = controller.Class.searchGrid.getData();
    controller.updateSelectedRowsonGrid(allRows);
  },

  mapLocalizedCategories: function () {
    aCategory["opt_system_clean"] = _utils.LocalXLate("snapin_selfservicesearch", "opt_system_clean");
    aCategory["opt_browser_clean"] = _utils.LocalXLate("snapin_selfservicesearch", "opt_browser_clean");
    aCategory["opt_system_security"] = _utils.LocalXLate("snapin_selfservicesearch", "opt_system_security");
    aCategory["opt_privacy"] = _utils.LocalXLate("snapin_selfservicesearch", "opt_privacy");
    aCategory["opt_system_perf"] = _utils.LocalXLate("snapin_selfservicesearch", "opt_system_perf");
    aCategory["opt_desktop_clean"] = _utils.LocalXLate("snapin_selfservicesearch", "opt_desktop_clean");
    aCategory["opt_browser_perf"] = _utils.LocalXLate("snapin_selfservicesearch", "opt_browser_perf");
    aCategory["opt_advanced"] = _utils.LocalXLate("snapin_selfservicesearch", "opt_advanced");
    aCategory["email"] = _utils.LocalXLate("snapin_selfservicesearch", "email");
    aCategory["system"] = _utils.LocalXLate("snapin_selfservicesearch", "system");
    aCategory["browser"] = _utils.LocalXLate("snapin_selfservicesearch", "browser");
    aCategory["sys_saction"] = _utils.LocalXLate("snapin_selfservicesearch", "sys_saction");
    aCategory["network"] = _utils.LocalXLate("snapin_selfservicesearch", "network");
    aCategory["featured"] = _utils.LocalXLate("snapin_selfservicesearch", "featured");
  },

  renderData: function (searchData) {
    var dataToShow=[];
    try{
      if (searchData.length > 0) {
        $("#myGrid").append('<img class="gridloadingnow" src="' + $ss.GetLayoutSkinPath() + "\\icons\\loader.gif" + '">');
        var mapsearchresult = $ss.helper.MapSearchableContent(searchData);
        var sAllSearchableData = $ss.helper.GetSearchableContents();
        dataToShow = $ss.helper.GetFilteredResult(mapsearchresult,sAllSearchableData);
        mapsearchresult = null;
        dataToShow = this.fixCategory(dataToShow);
      }
    }catch(e){
    }
    //slickGrid requires a valid container so load myGrid before rendering
    this.updateDataToRender(dataToShow,
                      {filterBy: this.searchCriteria.filter.by},
                      {sortBy:this.searchCriteria.sort.by,asc:this.searchCriteria.sort.asc}
                    ).then(function(){
    });
    renderedresult = dataToShow;  //assigning slick grid data (UI data) to global variable so that we can re-use it for other filters
    dataToShow = null;
  },

  fixCategory:function(contents){
    return $.map(contents, function (content, index) {
        if(content.category && $.isArray(content.category)){
          var fcats= $.map(content.category,function(cat,i){
              return aCategory[cat];
          });
          content.category = fcats;
        }
        return content;
      });
  },

  updateDataToRender:function(renderedresult, filterCriteria, sortCriteria){
    var data2Render = renderedresult;
    if(!$.isArray(renderedresult)) return $.Deferred.reject('invalid data').promise();
    if (filterCriteria){
      data2Render = this.applyFilters(data2Render, filterCriteria)
    }
    if(sortCriteria){
      data2Render = this.applySorting(data2Render, sortCriteria)
    }
    return $.when(
      controller.renderToGrid(data2Render)
    );
  },

  applyFilters: function(curSearchResult, criteria){
    if(!curSearchResult || curSearchResult.length==0
        || !criteria ||!criteria.filterBy) return curSearchResult;
    var filterresult = curSearchResult;
    switch (criteria.filterBy) {
      case "autofix":
        filterresult = $.grep(curSearchResult, function(v, index){
          return (v.ctype == "sprt_actionlight");
        });
        break;
      case "repair":
        filterresult = $.grep(curSearchResult, function(v, index){
          return (v.ctype == "sprt_protection");
        });
        break;
      case "optimize":
        filterresult = $.grep(curSearchResult, function(v, index){
          return (v.ctype == "sprt_optimize");
        });
        break;
      case "install":
        filterresult = $.grep(curSearchResult, function(v, index){
          return (v.ctype == "sprt_download");
        });
        break;
      case "articles":
        filterresult = $.grep(curSearchResult, function(v, index){
          return (v.ctype == "sprt_articlefaq" || v.ctype == "sprt_rawdoc" || v.ctype == "sprt_rawdoc_inline");
        });
        break;
      case "alerts":
        filterresult = $.grep(curSearchResult, function(v, index){
          return (v.ctype == "sprt_alertevents");
        });
        break;
      case "sprtmsg":
        filterresult = $.grep(curSearchResult, function(v, index){
          return (v.ctype == "sprt_msg");
        });
        break;
    }
    return filterresult;
  },

  applySorting: function(curSearchResult, criteria){
    if(!curSearchResult || curSearchResult.length==0
        || !criteria ||!criteria.sortBy) return curSearchResult;
    var sortResult = curSearchResult;
    switch(criteria.sortBy.toLowerCase()){
      case "category":
        sortResult = sortResult.sort(this.sortByCategory("category", criteria.asc==true));
        break;
      case "featured":
        sortResult.sort(this.sortByFeatured("category" ,criteria.asc==true));
        break;
      default:
        sortResult.sort(this.sortNormal(criteria.sortBy.toLowerCase(), criteria.asc==true));
        break;
    }
    return sortResult;
  },

  sortNormal: function (property, asc) {
    return function (a, b) {
      var aName = String(a[property]).toLowerCase();
      var bName = String(b[property]).toLowerCase();
      if(asc!==true) return ((aName < bName) ? 1 : ((aName > bName) ? -1 : 0));
      return ((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));
    }
  },

  sortByCategory: function (property, asc) {
    return function (a, b) {
      if(asc===true){
        if(!$ss.helper.contentHasCategories(b) && $ss.helper.contentHasCategories(a)) return -2;
        if (!$ss.helper.isFeaturedContent(a) && $ss.helper.isFeaturedContent(b)) return -3;
      }else{
        if(!$ss.helper.contentHasCategories(a) && $ss.helper.contentHasCategories(b)) return 2;
        if (!$ss.helper.isFeaturedContent(b) && $ss.helper.isFeaturedContent(a)) return 3;
      }
      var aName = String(a[property][0] || "").toLowerCase();
      var bName = String(b[property][0] || "").toLowerCase();
      if(asc!==true) return ((aName < bName) ? 1 : ((aName > bName) ? -1 : 0));
      return ((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));

    }
  },

  sortByFeatured: function (property, asc) {
    return function (a, b) {
      return ($ss.helper.isFeaturedContent(a) && !$ss.helper.isFeaturedContent(b) ? -1 : (asc==true ? 0 : -2));
    }
  },

  ".filterChkbox click": function (params) {
    var chkElements = $("input[name=cbFilters]");
    var count = 0;
    for (var i = 0; i < chkElements.length; i++) {
      var id  = $(chkElements)[i].id;
      var cid = $("#"+id).attr("guid");
      if(chkElements[i].checked){
        count = count + 1;
        if($.inArray(cid ,selectedRowIds) === -1) selectedRowIds.push(cid);
      }else{
        if($.inArray(cid ,selectedRowIds) > -1) selectedRowIds.splice($.inArray(cid ,selectedRowIds),1);
      }
    }
    if (count < 1) $("#buttn_Main").prop("disabled", true);
    else $("#buttn_Main").removeAttr("disabled");
  },

  "#buttn_Main click": function (params) {
    if ($("#buttn_Main").attr("disabled") == "disabled") return;
    if ($("#buttn_Main").attr("disabled") != "undefined") {
      var cid = [];
      var chkElements = $("input[name=cbFilters]");
      var contenttype = chkElements.attr("ctype");
      var allRows = controller.Class.searchGrid.getData();
      $(allRows).each(function(k,v){
         if($.inArray(v.cid , selectedRowIds) > -1 ) cid.push({ "id": v.cid });
      });
      switch (contenttype) {
        case "sprt_download": params.requesturl = "/dm"; break;
        case "sprt_optimize": params.requesturl = "/perfman?id=perfman_nav_quick&idselect=perfman_nav_quick_userselect"; break;
        default: break;
      }
      params.action = ".navigate click";
      params.SelectedId = cid;
      try {
        this.Class.dispatch("navigation", "index", params);
      }catch (ex) {
        this.Class.logger.error("In SearchController: Error in Dispatching to Navigation Controller %1%", ex.message);
      }
    }
  },

  "li a.searchfilter click": function (params) {
    fromSnapin ="";
    bAllSolutions = false;
    this.applyDefaultStyle();
    var sElementId = params.element.id;

    var sButtonText = "";
    var filterresult = this.applyFilters(renderedresult,{filterBy: sElementId});
    switch (sElementId) {
      case "allsolutions":
        bAllSolutions = true;
        break;
      case "optimize":
        if(filterresult.length > 0)
          sButtonText = _utils.LocalXLate("snapin_selfservicesearch", "snp_search_filter_optimize_main_bttn_txt");
        break;
      case "install":
        if(filterresult.length > 0)
          sButtonText = _utils.LocalXLate("snapin_selfservicesearch", "snp_search_filter_install_main_bttn_txt");
        break;
    }
    this.updateDataToRender(filterresult, {}, { sortBy:this.searchCriteria.sort.by, asc: this.searchCriteria.sort.asc});
    if (sButtonText) this.addMainButtonToUI(sButtonText);
    //Hide scroll bar when grid result is empty.
    if(filterresult.length == 0) $("#myGrid .slick-viewport").css("overflow-y","hidden");
    else {
      //Storing the filterBy value globally
      this.searchCriteria.filter.by = sElementId;
      $("#myGrid .slick-viewport").css("overflow-y","auto");
    }
    this.applyNewStyle("#" + sElementId);
  },

  GetcontentCount :function(ocontents){
    var totalcount = 0;
    if(ocontents){
      Object.keys(ocontents).forEach(function(key){
          totalcount = totalcount + ocontents[key];
      });
    }
    return totalcount;
  },

  getCountforArticles: function(oContentcountDict){
    var cArticles = 0;
    if(oContentcountDict){
      if(oContentcountDict["sprt_articlefaq"])  cArticles = cArticles + oContentcountDict["sprt_articlefaq"];
      if(oContentcountDict["sprt_rawdoc"])  cArticles = cArticles + oContentcountDict["sprt_rawdoc"];
      if(oContentcountDict["sprt_rawdoc_inline"])  cArticles = cArticles + oContentcountDict["sprt_rawdoc_inline"];
    }
    return cArticles;
  },

  updateCountforfilters :function(){
    if (oContentcountDict) {
      if(oContentcountDict["sprt_actionlight"]){
         $("#autofix").html(_utils.GetTruncatedString(cFiltAutoFix, maxStrLen) + '('  + oContentcountDict["sprt_actionlight"] +')');
      }else{
          $("#liautofix").remove();
      }
      if(oContentcountDict["sprt_alertevents"]){
         $("#alerts").html(_utils.GetTruncatedString(cFiltAlert, maxStrLen) + '('  + oContentcountDict["sprt_alertevents"] +')');
      }else{
         $("#lialerts").remove();
      }
       if(oContentcountDict["sprt_articlefaq"] || oContentcountDict["sprt_rawdoc"] || oContentcountDict["sprt_rawdoc_inline"]){
         $("#articles").html(_utils.GetTruncatedString(cFiltArticle, maxStrLen) + '('  + this.getCountforArticles(oContentcountDict) +')');
      }else{
           $("#liarticles").remove();
      }
       if(oContentcountDict["sprt_download"]){
         $("#install").html(_utils.GetTruncatedString(cFiltInstall, maxStrLen) + '('  + oContentcountDict["sprt_download"] +')');
      }else{
           $("#liinstall").remove();
      }
      if(oContentcountDict["sprt_msg"]){
        var localparams = {};
        if(NavigationController.IsUrlValid("/sa/messagesviewer")) MVC.Controller.dispatch("snapin_messageviewer", "indexWidget", localparams);
        if(localparams.DeletedMsgs && localparams.TotalMsg){
          oContentcountDict["sprt_msg"] = localparams.TotalMsg - localparams.DeletedMsgs;
        }
         $("#sprtmsg").html(_utils.GetTruncatedString(cFiltMsg, maxStrLen) + '('  + oContentcountDict["sprt_msg"] +')');
      }else{
          $("#lisprtmsg").remove();
      }
      if(oContentcountDict["sprt_optimize"]){
         $("#optimize").html(_utils.GetTruncatedString(cFiltOtimize, maxStrLen) + '('  + oContentcountDict["sprt_optimize"] +')');
      }else{
          $("#lioptimize").remove();
      }
      if(oContentcountDict["sprt_protection"]){
         $("#repair").html(_utils.GetTruncatedString(cFiltRepair, maxStrLen) + '('  + oContentcountDict["sprt_protection"] +')');
      }else{
          $("#lirepair").remove();
      }
      //getting totalcount at the end
      var totalcount = this.GetcontentCount(oContentcountDict);
      if (totalcount > 0) $("#allsolutions").html(_utils.GetTruncatedString(cFiltAll, maxStrLen) + '(' + totalcount + ')');
    }
  },

  renderToGrid: function (searchResults) {
    this.updateCountforfilters(); //update each content type count for filters.

    try {
      var showfeatured = $.grep(searchResults, function (el, index) {
        if (el.featured==true) return 1;
      }).length>0;

      //To display total number of Search Results per filter.
      $('#searchresults').html(_utils.LocalXLate("snapin_selfservicesearch","snp_search_total_results_txt",[searchResults.length.toString()]));
      $('#featured').html(_utils.LocalXLate("snapin_selfservicesearch",showfeatured==true ? "snp_search_featured_txt":""));

      if (!this.Class.searchGrid) {
        var columns = [{ id: "title", name: "", field: "title", formatter: renderCell}],
        options = {
          rowHeight: 75,
          editable: false,
          enableAddRow: false,
          enableCellNavigation: false,
          forceFitColumns: true,
          forceSyncScrolling: true,
          enableColumnReorder: false
        };
        this.Class.searchGrid = new Slick.Grid("#myGrid", [], columns, options);
        if (typeof this.Class.searchGrid === "object") {
          this.Class.searchGrid.onViewportChanged.subscribe(controller.updateGridDataonEvent);
          this.Class.searchGrid.onScroll.subscribe(controller.updateGridDataonEvent);
        }
      }
    }catch (e) {
      //alert(e);
      return;
    }

    try {
      Gridresult = (typeof searchResults == "string") ? JSON.parse(searchResults) : searchResults;
      if (!Gridresult.length) {
        this.Class.searchGrid.invalidateAllRows();
        if(fromSnapin === "Search"){
          $('.grid-canvas').html(_utils.LocalXLate("snapin_selfservicesearch","snp_selfservicesearch_noresult"));
        }else{
            $('.grid-canvas').html(_utils.LocalXLate("snapin_selfservicesearch","snp_selfservicesearch_nofilterresult"));
        }
        return;
      }
      if (this.Class.searchGrid) {
        this.Class.searchGrid.invalidateAllRows();
        this.Class.searchGrid.setData(Gridresult, true);
        this.Class.searchGrid.render();
        controller.updateSelectedRowsonGrid(Gridresult);
      }
    }catch (e) {
      //alert(e);
    }
  },

  "#clickedlink click": function (params) {
    params.event.preventDefault();
    params.requesturl = params.url;
    this.Class.dispatch("snapin_selfservicesearch", "index", params);
  },

  applyDefaultStyle: function(){
    $("#buttn_Main").css('display', 'none');
    $("#buttn_Main").attr('value', '');
    $("#buttn_Main").prop("disabled", true);
    $('#search_filters li a').removeClass('tab-menu-filters-selected');
    $('#search_filters li a').removeClass('dropdown-filters-selected');
    $('#search_filters li.filter-li a').toggleClass('tab-menu-filters',true);
  },

  applyNewStyle: function(elemID){
    $(elemID).removeClass("tab-menu-filters");
    $(elemID).toggleClass('tab-menu-filters-selected',true);
    $(elemID).css('padding-bottom', '12px');

    if(elemID === "#sprtmsg" || elemID === "#optimize" || elemID === "#repair") {
     $("#filterDropdown").addClass("dropdown-filters-selected");
    }
  },

  addMainButtonToUI: function (bttnTxt) {
    $("#buttn_Main").css('display', 'block');
    $("#buttn_Main").css('height', '20px');
    $("#buttn_Main").css('padding-top', '0px');
    $("#buttn_Main").attr('value', bttnTxt);
  },

  "#groupsel click":function(){
    $("#groupsel").addClass("dropdown-filters-selected");
  }
});
var bAllSolutions;
var aCategory = [];
var initialized = false;
var fromSnapin = "";
var _logger = $ss.agentcore.log.GetDefaultLogger("selfservicesearch_controller");
var _utils = $ss.agentcore.utils;

//strings for filters
var cFiltAll = _utils.LocalXLate("snapin_selfservicesearch", "snp_search_filters_all"),
  cFiltAlert = _utils.LocalXLate("snapin_selfservicesearch", "snp_search_filters_alerts"),
  cFiltArticle = _utils.LocalXLate("snapin_selfservicesearch", "snp_search_filters_articles"),
  cFiltAutoFix = _utils.LocalXLate("snapin_selfservicesearch", "snp_search_filters_autofix"),
  cFiltInstall = _utils.LocalXLate("snapin_selfservicesearch", "snp_search_filters_install"),
  cFiltMsg = _utils.LocalXLate("snapin_selfservicesearch", "snp_search_filters_messages"),
  cFiltOtimize = _utils.LocalXLate("snapin_selfservicesearch", "snp_search_filters_optimize"),
  cFiltRepair = _utils.LocalXLate("snapin_selfservicesearch", "snp_search_filters_repair");
  var maxStrLen = $ss.helper.getStringLengthForTruncation("snapin_selfservicesearch", "maxStringLenToDispForFilters_"+ _utils.GetLanguage());