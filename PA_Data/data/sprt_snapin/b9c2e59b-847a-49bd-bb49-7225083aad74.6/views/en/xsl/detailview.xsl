<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output  method="html"
              omit-xml-declaration="yes"/>
  <xsl:include href="common.xsl"/>

  <xsl:template match="/">
    <div id="messagesarea">
      <div class="cell-header"><span class="tab-menu-header">Messages</span></div><br/>
      <div class="panel-parent-header-ss-d">
      <i>View important messages from your support organization and follow instructions, as applicable.</i><br/><br/>
      </div>

      <form name="messageslist"  method="get" action="#">
        <div id="scrollmessagesarea" class="panel-heading detailmsgview panel-widget-body-ss">
          <table cellpadding="4" cellspacing="0" border="0" width="97%">
            <xsl:call-template name="pagerCount" />
            <xsl:for-each select="sprt/msgs/msg[@guid!='']">
              <xsl:for-each select=".">
            <xsl:call-template name="messagedetails" />
              </xsl:for-each>
            </xsl:for-each>
          </table>
        </div>
        <div id="messagebuttonsarea">
          <div id="pagenumbersarea">
            <div class="pages">
              <a href="#" tabindex="2" class="next" id="idMsgNext"></a>
              <span id="btnNavigate3" class="numbers">3</span>
              <span id="btnNavigate2" class="numbers">2</span>
              <span id="btnNavigate1" class="numbersCK">1</span>
             <a class="previous" id="idMsgPrev" tabindex="1"></a>
            </div>
          </div>
          <div id="butoncolumnright">
            <xsl:for-each select="sprt/msgs/msg[@guid!='']">
              <xsl:for-each select=".">
            <xsl:call-template name="messagedetailsbtns" />
              </xsl:for-each>
            </xsl:for-each>
      </div>
        </div>
      </form>
    </div>
    <xsl:call-template name="displayOrder" />
  </xsl:template>

  <xsl:template name="messagedetails">
      <tr class="clsMsgMarker">
        <xsl:attribute name="guid">
          <xsl:value-of select="@guid" />
        </xsl:attribute>
        <xsl:attribute name="version">
          <xsl:value-of select="@ver" />
        </xsl:attribute>
        <td valign="top" width="1%">

          <xsl:choose>
            <xsl:when test="p[@name='severity']='1'">
              <span width="35" height="35"/>
            </xsl:when>
            <xsl:otherwise>
              <span width="35" height="35" />
            </xsl:otherwise>
          </xsl:choose>

        </td>
        <td valign="top" width="99%">

         <h2 id="sa_msg_title">
        <xsl:value-of select="p[@name='title']" disable-output-escaping="no"/>
          </h2><br/>
          <span id="sa_msg_detail"></span>
        </td>
      </tr>
  </xsl:template>

  <xsl:template name="messagedetailsbtns">
    <span>
      <xsl:attribute name="id">
        <xsl:text>status</xsl:text>
        <xsl:value-of select="@guid" />
      </xsl:attribute>
      <xsl:choose>
        <xsl:when test="p[@name='payload_status']='S'">
          <xsl:choose>
            <xsl:when test="p[@name='payload']='U'">
              <span id="sa_msg_status_completed" class="label label-default status"><i class="fa fa-check"></i>Completed</span>
            </xsl:when>
            <xsl:when test="p[@name='payload']='V'">
              <span id="sa_msg_status_completed" class="label label-default status"><i class="fa fa-check"></i>Completed</span>
            </xsl:when>
            <xsl:otherwise>
              <span id="sa_msg_status_success" class="label label-default status"><i class="fa fa-check"></i>Issue Resolved</span>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:when>
        <xsl:when test="p[@name='payload_status']='U'">
          <span id="sa_msg_status_fail"  class="label label-default statuserror"><i class="fa fa-times"></i>Action Failed. Please try again.</span>
        </xsl:when>
        <xsl:otherwise>
          <span id="sa_msg_status_na" class="label label-default statusrun"><i class="fa fa-flag"></i>Action not yet run</span>
        </xsl:otherwise>
      </xsl:choose>
    </span>

      <xsl:variable name="caption">
        <xsl:choose>
          <xsl:when test="p[@name='payload_status']='S'">
            <xsl:value-of select="p[@name='completed_caption']" />
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="p[@name='incomplete_caption']" />
          </xsl:otherwise>
        </xsl:choose>
      </xsl:variable>
<br/><br/>
      
      <span class="btn btn-primary-ss btn-sm sa_msg_ViewAllMsgdetail">
        <xsl:attribute name="id">View All</xsl:attribute>
        View All
      </span>
      <span>&#160;&#160;</span>
      <span class="btn btn-primary-ss btn-sm button">
        <xsl:choose>
          <xsl:when test="p[@name='payload']='S'">
              <xsl:attribute name="msgguid">
                  <xsl:value-of select="@guid" />
              </xsl:attribute>
              <xsl:attribute name="msgver">
                  <xsl:value-of select="@ver" />
              </xsl:attribute>
              <xsl:attribute name="msgtype">
                  <xsl:text>0</xsl:text>
              </xsl:attribute>
            <xsl:if test="p[@name='payload_status']='S'">
              <xsl:attribute name="disabled">true</xsl:attribute>
            </xsl:if>
            <xsl:attribute name="elevation">
              <xsl:value-of select="p[@name='elevation']" />
            </xsl:attribute>
          </xsl:when>
          <xsl:when test="p[@name='payload']='U'">
              <xsl:attribute name="msgguid">
                  <xsl:value-of select="@guid" />
              </xsl:attribute>
              <xsl:attribute name="msgver">
                  <xsl:value-of select="@ver" />
              </xsl:attribute>
              <xsl:attribute name="msgtype">
                  <xsl:text>3</xsl:text>
              </xsl:attribute>
          </xsl:when>
          <xsl:when test="p[@name='payload']='V'">
              <xsl:attribute name="msgguid">
                  <xsl:value-of select="@guid" />
              </xsl:attribute>
              <xsl:attribute name="msgver">
                  <xsl:value-of select="@ver" />
              </xsl:attribute>
              <xsl:attribute name="msgtype">
                  <xsl:text>1</xsl:text>
              </xsl:attribute>
          </xsl:when>
          <xsl:when test="p[@name='payload']='VS'">
              <xsl:attribute name="msgguid">
                  <xsl:value-of select="@guid" />
              </xsl:attribute>
              <xsl:attribute name="msgver">
                  <xsl:value-of select="@ver" />
              </xsl:attribute>
              <xsl:attribute name="msgtype">
                  <xsl:text>2</xsl:text>
              </xsl:attribute>
            <xsl:if test="p[@name='payload_status']='S'">
              <xsl:attribute name="disabled">true</xsl:attribute>
            </xsl:if>
            <xsl:attribute name="elevation">
              <xsl:value-of select="p[@name='elevation']" />
            </xsl:attribute>
          </xsl:when>
        </xsl:choose>

        <xsl:choose>
          <xsl:when test="p[@name='payload_status']='U'">
              Try Again
          </xsl:when>
          <xsl:otherwise>
          <xsl:value-of select="substring($caption, 1, 15)" />
          </xsl:otherwise>
        </xsl:choose>

      </span>
      <span>&#160;&#160;</span>
      <span class="btn btn-primary-ss btn-sm buttondel">
        <xsl:attribute name="id">Delete</xsl:attribute>
        Delete
      </span>

  </xsl:template>

  <xsl:template name="pagerCount">
    <tr>
      <th colspan="2" id="Th1">
        <span class="clsMsgCounter" id="idMsgCounter">
      <span id="idMsg"><![CDATA[Messages ]]></span>
      <span id="idMsgCounter1">1</span>
      <xsl:text>&#xa0;</xsl:text>
      <span id="cl_msg_Of"><![CDATA[ of ]]></span>
      <xsl:text>&#xa0;</xsl:text>
      <span id="idMsgCounter2"></span>
        </span>
      </th>
    </tr>
  </xsl:template>  
</xsl:stylesheet>