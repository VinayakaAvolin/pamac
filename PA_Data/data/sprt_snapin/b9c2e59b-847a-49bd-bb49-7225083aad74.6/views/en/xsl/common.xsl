<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" omit-xml-declaration="yes"/>

  <xsl:template name="displayOrder">
    
    <!-- 1a. Real Time alerts -->
    <xsl:apply-templates select="/sprt/msgs/msg[@top='1']" />

    <!-- 1a. Unexecuted and UnViewed Critical alerts -->
    <xsl:apply-templates select="/sprt/msgs/msg[p[@name='severity']='1' and p[@name='shown_dt']='' and (@top!='1' or not(@top))]" />

    <!-- 1b. Unexecuted and Viewed Critical alerts -->    
    <xsl:apply-templates select="/sprt/msgs/msg[p[@name='severity']='1' and p[@name='shown_dt']!='' and p[@name='payload_status']='N' and (@top!='1' or not(@top))]" />
    
    <!-- 1c. Executed Critical alerts -->
    <xsl:apply-templates select="/sprt/msgs/msg[p[@name='severity']='1' and p[@name='shown_dt']!='' and p[@name='payload_status']!='N' and (@top!='1' or not(@top))]" />

    <!-- 2.1 Unexecuted and Unviewed -->
    <xsl:apply-templates select="/sprt/msgs/msg[p[@name='severity']!='1' and p[@name='shown_dt']='' and (@top!='1' or not(@top))]" >
      <xsl:sort select="p[@name='severity']" data-type="number"/>
      <xsl:sort select="p[@name='create_dt']"/>
    </xsl:apply-templates>     

    <!-- 2.2 Unexecuted and Viewed  -->
    <xsl:apply-templates select="/sprt/msgs/msg[p[@name='severity']!='1' and p[@name='shown_dt']!='' and p[@name='payload_status']='N' and (@top!='1' or not(@top))]" >
      <xsl:sort select="p[@name='severity']" data-type="number"/>      
      <xsl:sort select="p[@name='create_dt']"/>
    </xsl:apply-templates>

    <!-- 2.3 Executed -->
    <xsl:apply-templates select="/sprt/msgs/msg[p[@name='severity']!='1' and p[@name='shown_dt']!='' and p[@name='payload_status']!='N' and (@top!='1' or not(@top))]" >
      <xsl:sort select="p[@name='severity']" data-type="number"/>
      <xsl:sort select="p[@name='create_dt']"/>
    </xsl:apply-templates>
      
  </xsl:template>

</xsl:stylesheet>