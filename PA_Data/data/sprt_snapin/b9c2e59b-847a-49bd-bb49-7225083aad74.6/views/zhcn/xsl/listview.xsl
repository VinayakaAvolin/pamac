<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output  method="html"
              omit-xml-declaration="yes"/>
  <xsl:include href="common.xsl"/>

  <xsl:template match="/">
    <div id="messagesarea">
      <div class="cell-header"><span class="tab-menu-header">消息</span></div><br/>
      <div class="panel-parent-header-ss-d">
        <i>从您的支持组织中查看重要信息，并按照说明使用。</i><br/><br/>
      </div>

      <form name="messageslist" method="get" action="#">
        <div id="scrollmessagesarea" class="panel-widget-body-ss">
          <div id="messageCounter" class="table-heading">
            <table cellpadding="4" cellspacing="0" border="0" width="97%">
              <xsl:call-template name="pagerCount" />
            </table>
          </div>
          <div id="messagelistdisplay" class="panel-heading" >
            <table cellpadding="4" cellspacing="0" border="0" width="97%">
              <xsl:call-template name="displayOrder" />
            </table>
          </div>
        </div>
        <div id="messagebuttonsarea">
          <div id="pagenumbersarea">
            <div class="pages">
            </div>
          </div>
          <div id="butoncolumnright">
            <span id="sa_msg_offline_text" class="statusoffline error-msg" style="display:none">电脑离线</span>
            <br/><a href="#" id="sa_msg_ViewAllMsg" class="btn btn-primary-ss btn-sm"><xsl-text>查看全部</xsl-text></a>
            <span>&#160;&#160;</span>
            <a href="#" class="btn btn-primary-ss btn-sm"><xsl:text>刷新</xsl:text></a>
            <span>&#160;&#160;</span>
            <a href="#" id="sa_msg_DeleteMsg" disabled="true" class="btn btn-primary-ss btn-sm"><xsl-text>删除</xsl-text></a>
          </div>
        </div>
      </form>
    </div>
  </xsl:template>

  <xsl:template match="msg">
    <xsl:for-each select=".">
    <tr class="clsMsgMarker FilterMsgs">
      <xsl:attribute name="id">
        <xsl:text>id</xsl:text>
        <xsl:value-of select="@guid" />
      </xsl:attribute>
      <xsl:attribute name="guid">
        <xsl:value-of select="@guid" />
      </xsl:attribute>
      <xsl:attribute name="version">
        <xsl:value-of select="@ver" />
      </xsl:attribute>

      <xsl:variable name="chkID">
        <xsl:value-of select="@guid" />
      </xsl:variable>
      <td valign="top" width="1%">
        <input name="" type="checkbox" class="checkboxclass">
          <xsl:attribute name="value">
            <xsl:value-of select="@guid" />
          </xsl:attribute>
        <xsl:attribute name="id">
          <xsl:text>chkDeleteMsg</xsl:text><xsl:value-of select="$chkID" /></xsl:attribute>
        </input>
      </td>
      <td valign="top" width="1%">
        <xsl:choose>
          <xsl:when test="p[@name='severity']='1'">
            <span class="fa fa-exclamation-triangle fa-2x"  style="color:#CC0033;" width="35" height="35" alt="" border="0"/>
          </xsl:when>
          <xsl:otherwise>
            <span class="fa fa-info-circle fa-2x"  style="color:#1284c7;" width="35" height="35" alt="" border="0"/>
          </xsl:otherwise>
        </xsl:choose>
      </td>
      <td valign="top" width="70%">
        <p>
          <a href="#" class="navigate" url="/sa/messagesviewer?pagetoLoad=detail">
            <xsl:attribute name="onclick">$ss.snapin.msgViewer.Helper.cl_msg_go_to_detail('<xsl:value-of select="@guid" />');</xsl:attribute>
              <xsl-text>
                <xsl:attribute name="id">
                  <xsl:text>title</xsl:text>
                  <xsl:value-of select="@guid" />
                </xsl:attribute>
                <xsl:if test="p[@name='shown_dt']=''">
                  <xsl:attribute name="style">font-weight:bold;</xsl:attribute>
                </xsl:if>
                <xsl:value-of select="p[@name='title']" disable-output-escaping="no"/>
            </xsl-text>
          </a>
        </p>
      </td>
      <td valign="top" width="28%">
        <a href="#"><span style="text-align:center;color:#fff !important;" class="col-xs-12 btn btn-primary-ss btn-sm button panel-widget-body-ss">
          <xsl:variable name="caption">
            <xsl:choose>
              <xsl:when test="p[@name='payload_status']='S'">
                <xsl:value-of select="substring(p[@name='completed_caption'],1,15)" />
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="substring(p[@name='incomplete_caption'],1,15)" />
              </xsl:otherwise>
            </xsl:choose>
          </xsl:variable>
          <xsl:attribute name="id">
            <xsl:text>操作</xsl:text>
            <xsl:value-of select="@guid" />
          </xsl:attribute>
          <xsl:choose>
            <xsl:when test="p[@name='payload']='S'">
                <xsl:attribute name="msglistguid">
                    <xsl:value-of select="@guid" />
                </xsl:attribute>
                <xsl:attribute name="msglistver">
                    <xsl:value-of select="@ver" />
                </xsl:attribute>
                <xsl:attribute name="msglisttype">
                    <xsl:text>2</xsl:text>
                </xsl:attribute>
              <xsl:if test="p[@name='payload_status']='S'">
                <xsl:attribute name="disabled">true</xsl:attribute>
              </xsl:if>
              <xsl:attribute name="elevation">
                <xsl:value-of select="p[@name='elevation']" />
              </xsl:attribute>
            </xsl:when>
            <xsl:when test="p[@name='payload']='U'">
                <xsl:attribute name="msglistguid">
                    <xsl:value-of select="@guid" />
                </xsl:attribute>
                <xsl:attribute name="msglistver">
                    <xsl:value-of select="@ver" />
                </xsl:attribute>
                <xsl:attribute name="msglisttype">
                    <xsl:text>3</xsl:text>
                </xsl:attribute>
            </xsl:when>
            <xsl:when test="p[@name='payload']='V'">
                <xsl:attribute name="msglistguid">
                    <xsl:value-of select="@guid" />
                </xsl:attribute>
                <xsl:attribute name="msglistver">
                    <xsl:value-of select="@ver" />
                </xsl:attribute>
                <xsl:attribute name="msglisttype">
                    <xsl:text>1</xsl:text>
                </xsl:attribute>
            </xsl:when>
            <xsl:when test="p[@name='payload']='VS'">
                <xsl:attribute name="msglistguid">
                    <xsl:value-of select="@guid" />
                </xsl:attribute>
                <xsl:attribute name="msglistver">
                    <xsl:value-of select="@ver" />
                </xsl:attribute>
                <xsl:attribute name="msglisttype">
                    <xsl:text>2</xsl:text>
                </xsl:attribute>
              <xsl:if test="p[@name='payload_status']='S'">
                <xsl:attribute name="disabled">true</xsl:attribute>
              </xsl:if>
              <xsl:attribute name="elevation">
                <xsl:value-of select="p[@name='elevation']" />
              </xsl:attribute>
            </xsl:when>
          </xsl:choose>
          <xsl:choose>
            <xsl:when test="p[@name='payload_status']='U'">
              再试一次
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="substring($caption, 1, 15)" />
            </xsl:otherwise>
          </xsl:choose>
        </span></a><br/><br/>
      </td>
      <td valign="top" width="30%">
        <span>
          <xsl:attribute name="id">
            <xsl:text>status</xsl:text>
            <xsl:value-of select="@guid" />
          </xsl:attribute>
          <xsl:choose>
            <xsl:when test="p[@name='payload_status']='S'">
              <xsl:choose>
                <xsl:when test="p[@name='payload']='U'">
                  <div id="sa_msg_status_completed" class="label label-default status"><i class="fa fa-check"></i>完成</div>
                </xsl:when>
                <xsl:when test="p[@name='payload']='V'">
                  <div id="sa_msg_status_completed" class="label label-default status"><i class="fa fa-check"></i>完成</div>
                </xsl:when>
                <xsl:otherwise>
                  <div id="sa_msg_status_success" class="label label-default status"><i class="fa fa-check"></i>解决问题</div>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:when>
            <xsl:when test="p[@name='payload_status']='U'">
              <div id="sa_msg_status_fail"  class="label label-default statuserror"><i class="fa fa-times"></i>操作失败，请重试或联系您的管理员。</div>
            </xsl:when>
            <xsl:otherwise>
              <div id="sa_msg_status_na"   class="label label-default statusrun"><i class="fa fa-flag"></i>`</div>
            </xsl:otherwise>
          </xsl:choose>
        </span>
      </td>
      
    </tr>
    </xsl:for-each>
  </xsl:template>

  <xsl:template name="pagerCount">
    <tr class="clsMsgHeadCounter">
    <th width="3%"></th>
      <th width="3.7%">
        <input name="" type="checkbox" id="chkDeleteAll" value="">
        </input>
      </th>
      <th colspan="3" id="idMsgCounter">
        <xsl:value-of select="count(/sprt/msgs/msg[p[@name='deleted_dt']=''])" /><xsl-text> 消息</xsl-text>
      </th>
    </tr>
  </xsl:template>
</xsl:stylesheet>