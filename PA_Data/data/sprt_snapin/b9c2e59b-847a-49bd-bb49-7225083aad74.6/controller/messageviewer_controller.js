SnapinMessageviewerController = SnapinBaseController.extend('snapin_messageviewer',
{
 //Private Access Area
 index:function(params, msgguid)
   {
      $ss.agentcore.dal.databag.SetValue("SNAPIN_MESSAGE_VIEWED", true);
      var sArgs = params.requestArgs;
      var bDetailed = false;
      snapLoc = params.toLocation;
      fromSnapin = false;
      if(params.UnreadMsgguids){
        arrUnreadMsgGuids = [];
        for(var i = 0;i<params.UnreadMsgguids.length;i++){
          arrUnreadMsgGuids.push(params.UnreadMsgguids[i].cid);
        }
        fromSnapin = true;
      }
      if(sArgs)
        if(params.requestArgs.pagetoLoad == "detail")
          bDetailed = true;
      if(params.SelectedId) {
        this.renderSnapinView("snapin_messageviewer", params.toLocation, "\\views\\msg_messagedetail.htm");
        $ss.snapin.msgViewer.Helper.RenderViewDetail(params.SelectedId[0].id);
      }
      else{
        if(bDetailed)
        {
          this.renderSnapinView("snapin_messageviewer", params.toLocation, "\\views\\msg_messagedetail.htm");
          $('#idMessages').css("display", "block");
          $ss.snapin.msgViewer.Helper.AfterRenderDetail();
          $ss.agentcore.utils.ui.FixPng();
        }
        else
        {          
          if(params.requestArgs.pagetoLoad == "emptypage")
          {
            this.renderSnapinView("snapin_messageviewer", params.toLocation, "\\views\\msg_messagelist.htm");
            $ss.agentcore.utils.ui.FixPng();
            $ss.snapin.msgViewer.Helper.checkEmpty();
          }
          else if(params.requestArgs.pagetoLoad == "refreshlist") {
            this.renderSnapinView("snapin_messageviewer", params.toLocation, "\\views\\msg_messagelist.htm");
            $ss.snapin.msgViewer.Helper.refreshList();
            $ss.agentcore.utils.ui.FixPng();
          }
          else{
            $ss.snapin.msgViewer.Helper.BeforeRenderList();
            this.renderSnapinView("snapin_messageviewer", params.toLocation, "\\views\\msg_messagelist.htm");
            $ss.agentcore.utils.ui.FixPng();
            $ss.snapin.msgViewer.Helper.AfterRenderList();
          }
        }
     }
   },

   CheckFileExists :function(){
    var _xml =  $ss.agentcore.dal.xml;
    var dom  = _xml.LoadXML(_msgprocessor.cl_msg_getMessageXMLFilePath(), false);
    if (bMac) {
     if (dom == null) return false;
    }else {
    if(dom.xml == "") return false;
    }
    return true;
   },
   
   indexActionWidget: function(params){
      arrUnreadMsgGuids = [];
      var bexists = this.CheckFileExists();
      if(!bexists){
        $ss.snapin.msgViewer.Helper.BeforeRenderList();            
        $ss.agentcore.utils.ui.FixPng();
        $ss.snapin.msgViewer.Helper.AfterRenderList();
      }
      var _xml =  $ss.agentcore.dal.xml;
      var dom  = _xml.LoadXML(_msgprocessor.cl_msg_getMessageXMLFilePath(), false);
      var nodes = null;
      if (bMac) {
        nodes = _xml.GetNodes('/sprt/msgs/msg[@t!="1" and p[@name="payload_status"]!= "S"]','',dom);     
      }
      else{
        nodes = _xml.GetNodes('//msgs/msg[@t!="1" and p[@name="payload_status"]!= "S"]','',dom);     
      }
      var toBeReadMsgs = 0;
      if (nodes) {      
      for(var i = 0;i< nodes.length;i++){
        var obj = {};
        var cid = nodes[i].getAttribute("guid");
        obj["cid"] = cid;
        arrUnreadMsgGuids.push(obj);
      }
        toBeReadMsgs = nodes.length;
      }
      
      params = params || {};
      params.toBeReadMsgs = toBeReadMsgs;
      params.msgList = arrUnreadMsgGuids;        
    },
  
   indexMessage: function (params) {
      var _fso = $ss.agentcore.dal.file.GetFileSystemObject();
      var path = _msgprocessor.cl_msg_getMessageXMLFilePath();
      if(!_fso.FileExists(path)) return;
      var dateModified  =null;
      if (bMac) {
        dateModified = new Date(_fso.GetModifiedDate(path));
      }else {
        dateModified = new Date(_fso.GetFile(path).DateLastModified);
      }
      if(dateModified <= params.lastmodified) return;
      var arrGuid=[];
      var _xml = $ss.agentcore.dal.xml;
      var dom = _xml.LoadXML(path, false);

      if (dom) {
        if (bMac) {
          var nodes = _xml.GetNodes("/sprt/msgs/msg[p[@name='deleted_dt']!='']", '', dom);
          if (nodes) {
            for(var i=0;i < nodes.length;i++){
              arrGuid.push(nodes[i].getAttribute("guid"));
            }
        }
        }
        else {
      var nodes = _xml.GetNodes("//msgs/msg[p[@name='deleted_dt']!='']", '', dom);
      for(var i=0;i < nodes.length;i++){
        arrGuid.push(nodes[i].getAttributeNode("guid").text);
      }
        }
      }
      params = params ||{};
      params.SDelMsgslist = arrGuid;
      params.lastmodified = dateModified;
   },

   indexWidget:function(params)
   {
    var _xml =  $ss.agentcore.dal.xml;
    var dom  = _xml.LoadXML(_msgprocessor.cl_msg_getMessageXMLFilePath(), false);
    //Status of triggers in message.xml:
    // t=0: Non-Trigger Msg's
    // t=1: Trigger Msg's which are RUN/Excute
    // t=2: Trigger Msg's closed from MINIBCONT without performing any action

    var nodes = null;
    var TotalMsg = 0;
    var DeletedMsgs = 0;
    var ShownMsgs = 0 ;
    if (bMac) {
      nodes = _xml.GetNodes('/sprt//msgs/msg[@t!="1"]','',dom);
      if (nodes) {
        TotalMsg = nodes.length;
      }
      
      nodes = _xml.GetNodes("/sprt//msgs/msg[p[@name='deleted_dt']!='']",'',dom);
      if (nodes) {
        DeletedMsgs = nodes.length;
      }
      nodes = _xml.GetNodes('/sprt//msgs/msg[@t!="1" and p[@name="payload_status"]!="N"]','',dom);
      if (nodes) {
        ShownMsgs = nodes.length;
      }

    }
    else{
      nodes = _xml.GetNodes('//msgs/msg[@t!="1"]','',dom);
      TotalMsg = nodes.length;
    nodes = _xml.GetNodes("//msgs/msg[p[@name='deleted_dt']!='']",'',dom);
      DeletedMsgs = nodes.length;
    nodes = _xml.GetNodes('//msgs/msg[@t!="1" and p[@name="payload_status"]!="N"]','',dom);
      ShownMsgs = nodes.length;
    }

    params = params || {};
    params.TotalMsg = TotalMsg;
    params.DeletedMsgs = DeletedMsgs;
    params.ShownMsgs = ShownMsgs;
    //Currently this function is getting used by SSS snapin.Below render call is not required.
    //this.renderSnapinView("snapin_messageviewer", params.toLocation, "\\views\\%LANGCODE%\\msgWidget.htm", {msgs:params} );
   },
   
   executeMessages:function(params){
     params.fromSnapin = true;
     this.Class.dispatch("snapin_messageviewer","#messagesarea  .button click",params);
     
   },
  "#sa_msg_refresh click" : function(params)
    {
    //Content API always returns the cahed value if it is in the same session
    //So Clearing content cache before getting the updated support messages list
     $ss.agentcore.dal.content.ClearContentCache();
     $ss.snapin.msgViewer.Helper.refreshList();
    },

  "#chkDeleteAll click" : function(params)
   {
      if(params.element.checked){
      $("#butoncolumnright #sa_msg_DeleteMsg").attr("disabled", false);
      }
      else{
        $("#butoncolumnright #sa_msg_DeleteMsg").attr("disabled", true);}
    var chkElements = $("tr.clsMsgMarker input:checkbox");
    for(var i=0;i<chkElements.length;i++) {
      if(params.element.checked)
        chkElements[i].checked = true;// =  true;
      else
        chkElements[i].checked = false;//("checked");// =  false;
    }
  },
  "#sa_msg_DeleteMsg click":function(params)
  {
    $ss.snapin.msgViewer.Helper.cl_msg_List_DeleteMsgs();
  },

  /*To view all available Messages*/
  "#sa_msg_ViewAllMsg click" : function(params){
    if ($("#sa_msg_ViewAllMsg").attr("disabled") == "disabled") {
        return false;
    }
    if(fromSnapin) fromSnapin=false;
    $ss.snapin.msgViewer.Helper.BeforeRenderList();            
    this.renderSnapinView("snapin_messageviewer", params.toLocation, "\\views\\msg_messagelist.htm");
    $ss.agentcore.utils.ui.FixPng();
    $ss.snapin.msgViewer.Helper.AfterRenderList();
    $("#sa_msg_ViewAllMsg").attr("disabled","disabled");
  },

  ".sa_msg_ViewAllMsgdetail click" :function(params){
    params.requestPath = "/sa/messagesviewer";
    params.requesturl = "/sa/messagesviewer";
    params.requestArgs = {};
    params.toLocation = snapLoc;
    this.Class.dispatch("snapin_messageviewer", "index", params);
    $(".sa_msg_ViewAllMsgdetail").attr("disabled", "disabled");
    $("#sa_msg_ViewAllMsg").attr("disabled", "disabled");//We need to disable this button as well, coz we are displaying this View All button for the second time.
  },

  "#chkDeleteAll  click":function(params){
    $ss.snapin.msgViewer.Helper.cl_msg_EnableDelete('all', params.element);
  } ,
  ".checkboxclass click":function(params)
  {
    $ss.snapin.msgViewer.Helper.cl_msg_EnableDelete("", params.element );
  },
  "#messagesarea #messagebuttonsarea #pagenumbersarea .pages .next click":function(params)
  {
     $ss.snapin.msgViewer.Helper.cl_msg_detail_nextMessage();
  },
  "#messagesarea #messagebuttonsarea #pagenumbersarea .pages .next keypress":function(params)
  {
     $ss.snapin.msgViewer.Helper.cl_msg_checkKey(params.element);
  },

  "#messagesarea #messagebuttonsarea #pagenumbersarea .pages .previous click":function(params)
  {
    $ss.snapin.msgViewer.Helper.cl_msg_detail_prevMessage();
  },
  "#messagesarea #messagebuttonsarea #pagenumbersarea .pages .previous keypress":function(params)
  {
    $ss.snapin.msgViewer.Helper.cl_msg_checkKey(params.element);
  },
  "#messagesarea  .buttondel click":function(params)
  {
    $ss.snapin.msgViewer.Helper.cl_msg_detail_delete();
  },
   "#messagesarea  .button click":function(params)
   {
      fromListPage = true;
      try
      {
    if($(($(params)[0].element.outerHTML)).attr("msglisttype")!=undefined)
    {
      //click event from Listing page
      $ss.snapin.msgViewer.Helper.cl_msg_ExecPayloadDetail($(($(params)[0].element.outerHTML)).attr("msglistguid"),$(($(params)[0].element.outerHTML)).attr("msglistver"),parseInt($(($(params)[0].element.outerHTML)).attr("msglisttype")),params.element);
    }
    else
    {
      fromListPage = false;
      //click event from detail page
    $ss.snapin.msgViewer.Helper.cl_msg_ExecPayloadDetail($(".button").attr("msgguid"),$(".button").attr("msgver"),parseInt($(".button").attr("msgtype")),params.element);
  }
      }
      catch(ex)
      {
        $ss.agentcore.log.GetDefaultLogger("$ss.snapin.messageviewer.controller").error("#messagesarea  .button click",ex.message);
      }

  }



});
var arrUnreadMsgGuids = [];
var fromSnapin = false;
var fromListPage = false;
var snapLoc="";
var bMac = $ss.agentcore.utils.IsRunningInMacMachine();
var sUserPrefLangcode = $ss.agentcore.utils.GetLanguage();
var _msgprocessor = $ss.snapin.msgprocessor.helper;