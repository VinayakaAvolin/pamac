
$ss.snapin = $ss.snapin || {};
$ss.snapin.msgViewer =$ss.snapin.msgViewer || {};
$ss.snapin.msgViewer.Helper = $ss.snapin.msgViewer.Helper || {};

(function()
{
    
  $.extend($ss.snapin.msgViewer.Helper, 
  {
   /********************************************************************************/
   //All Public functions
   /********************************************************************************/  
       
     RenderViewDetail: function (sGuid) {
       $ss.snapin.msgViewer.Helper.BeforeRenderList();
       $ss.snapin.msgViewer.Helper.AfterRenderList();
       this.cl_msg_go_to_detail(sGuid, true);
       //TODO:Can be Removed once Registry related Support Message is filtered from Search page.
       if($('#idMessages')[0].innerHTML.length == 28){
          $('#idMessages').html('<div><b>No detailed view available for this content</b></div>');
       }
       $('#idMessages').css("display", "block");
       $ss.agentcore.utils.ui.FixPng();
    },

    /* cl_msg_messagelist.htm functions START */
    BeforeRenderList: function()
    {    
      try
      {
      // It is set to true if a function caller is cl_msg_MessageList(), otherwise it is set to false.
      _databag.SetValue("db_msg_MessageList", false);
      g_isViewingAlertPage = true;
      var bForceFirstRun = (_config.GetConfigValue("firstrun", "force_first_run", "true") === "true");
      var firstRunCompleted = "true";
      if (bForceFirstRun) {
        firstRunCompleted = _config.GetUserValue("firstrun", "FirstRunCompleted");
      }
      if(firstRunCompleted == "true") 
      {
        _events.Subscribe("BROADCAST_ON_EXTERNALREFRESH", this.cl_msg_OnMultiInstance);
        _events.Subscribe("SS_EVT_SNAPIN_VP_FOCUS", this.cl_msg_pageFocus);
        _events.Subscribe("SS_EVT_BCONT_REFRESH", this.cl_msg_pageRefresh);
          //_events.Subscribe("SS_EVT_SNAPIN_VP_BLUR", this.cl_msg_updateSystrayThreaded );  // When navigating out of this page
        _events.Subscribe("BROADCAST_ON_BCONTCLOSE", this.cl_msg_updateSystrayAndClose); // When bcont closes 
      }
      
      }
      catch(ex) 
      {
        g_Logger.error("MessageViewerSnapin:$ss.snapin.msgViewer.Helper.BeforeRenderList()",ex.message);
      }
    },  
    cl_msg_OnMultiInstance: function(oEvent)
    {
      try 
      {
        $ss.agentcore.dal.content.ClearContentCache();
        $ss.agentcore.events.SendByName("SS_EVT_SNAPIN_VP_FOCUS"); 
        // If not on the alerts page, then save the refresh request for later, if user visits the alert list viewer page
        if (!g_isViewingAlertPage) {
          g_isRefreshNeeded = true;
          return;
        }
       
        var cmdline = oEvent.description;
       
        if(cmdline.indexOf(_const.BCONT_MINI_RUNNING) > -1)
        {       
          if (!g_bDialogShowing)
          {
            g_bDialogShowing = true;
            var sSnapinUrl = _config.GetConfigValue("preconfigured_snapin_url", "snapin_messagespopup", "messages");
            var sMiniBcontInstance = _utils.GetMiniBcontSpawnName(sSnapinUrl);
            if(_utils.IsMiniBcontRunning(sMiniBcontInstance)) {
              if(g_bCloseAlertNeeded) alert(_utils.LocalXLate("snapin_messageviewer", "cl_msg_popup_close_info"));
              _msgprocessor.cl_msg_CloseMiniBcont();
            }
            _utils_ui.FocusBcont();
            var strSnapinMsgUrl=_config.GetConfigValue("preconfigured_snapin_url", "supportmessage_list")+"?pagetoLoad=detail"
            if(NavigationController.sCurrentRequestUrl == strSnapinMsgUrl) {
              var params = {};
              params.requesturl = _config.GetConfigValue("preconfigured_snapin_url", "supportmessage_list")+"?pagetoLoad=refreshlist"; 
              MVC.Controller.dispatch("navigation","index",params);
            }
            else
              $ss.snapin.msgViewer.Helper.refreshList();
            
            g_bDialogShowing = false;
          }            
        }         
      } catch(ex) {
        g_Logger.error("MessageViewerSnapin:$ss.snapin.msgViewer.Helper.BeforeRenderList()",ex.message);
      }      
    },
    refreshList: function()
    {      
      try
      {    
        this.updateSynchAlert(_msgprocessor.isSyncEnabled());
        $('#sa_msg_refresh').attr("disabled", true); 
        $("#sa_msg_refresh").addClass("buttondisabled");
        _utils_ui.StartShellBusy();

        $("tr.clsMsgMarker").css("display", "none");
        $("#messagebuttonsarea").css("display", "none");
        $("#noMsgTable").css("display", "none");
        $("#idMessages").html(sDefaultHTM);
        _system.Sleep(20);

        g_showAll = true;          
        this.cl_msg_MessageList();  
          
      } 
      catch (e) 
      { 
        g_Logger.error("MessageViewerSnapin:$ss.snapin.msgViewer.Helper.refreshList()",e.message);
      } 
      finally 
      {
        $('#sa_msg_refresh').attr("disabled", false); 
        $("#sa_msg_refresh").removeClass("buttondisabled");
        $("#sa_msg_refresh").addClass("button");
        _utils_ui.EndShellBusy();  
        this.checkEmpty();
      }      
    },
    updateSynchAlert: function(bEnable) 
    {

      // Update alerts if there is one for Update Synch
      try 
      {
        var sMessagesXml = _msgprocessor.cl_msg_getMessageXMLFilePath();
   
        if (_file.FileExists(sMessagesXml)) 
        {
   
          var oXml = $ss.agentcore.dal.xml.LoadXML(sMessagesXml);
          var oNodes = null;
          if (bMac) {
             oNodes = _Xml.SelectNodes(oXml.responseXML, "/sprt/msgs/msg");
          }else {
             oNodes = oXml.selectNodes("/sprt/msgs/msg");
        }
          var updateNeeded = false;

          for (nI=0;nI<oNodes.length;nI++) 
          {
            var text = "";
            if (bMac) {
            text = _Xml.GetNodeText(oNodes[nI]);
          }
          else{
            text = oNodes[nI].text;
          }
            if (text.indexOf("enableSync.cab") > -1) {        
              var oChildren = oNodes[nI].childNodes;
              for (nJ=0;nJ<oChildren.length;nJ++) {
                if (oChildren[nJ].getAttribute("name") == "payload_status") {
                  if (bEnable && _Xml.GetNodeText(oChildren[nJ]) != 'S') {
                    _Xml.SetNodeElementText(oChildren[nJ], 'S');
                    updateNeeded = true;
                  }

                  if (!bEnable && _Xml.GetNodeText(oNodes[nI]) == 'S') {
                    if (bMac) {
                    _Xml.SetNodeElementText(oChildren[nJ], 'X');
                  }
                  else
                    oChildren[nJ].text = 'X';
                    updateNeeded = true;
                  }
                  break;
                }
              }
            }           
          }          
     
          if (updateNeeded) {
            if (bMac) {
              var serializer = new XMLSerializer();
              var writetofile = serializer.serializeToString(oXml.responseXML);
              _file.WriteNewFile(sMessagesXml,writetofile);
            }else
            _file.WriteNewFile(sMessagesXml, oXml.xml);
          }
        }
      }
      catch(ex) 
      {
         g_Logger.error("MessageViewerSnapin:$ss.snapin.msgViewer.Helper.updateSynchAlert()",ex.message);
      }           
    },

    cl_msg_MessageList: function()
    {
      try
      {
      _databag.SetValue("db_msg_MessageList", true);
      // sync contents of messages.xml to filesystem - cl_msg_getMessagesXml() returns messages.xml XMLDocument
      var current_msgs_xml = _msgprocessor.cl_msg_getMessagesXml();
      var updated_msgs_xml = _msgprocessor.cl_msg_updateMessagesXml(current_msgs_xml, true);
      _databag.SetValue("db_msg_MessageList", false);
      if(updated_msgs_xml != null){
      //show messages list
      this.cl_msg_displayMsgs(EVT_USEREVENT);
      }
      }
      catch(ex) 
      {
        g_Logger.error("MessageViewerSnapin:$ss.snapin.msgViewer.Helper.cl_msg_MessageList()",ex.message);
      }
    },
     // Subscribe to the minibcont event only if the user is in the message listing or message detail page
    cl_msg_pageFocus: function(oEvent) 
    {
      try 
      {  
      // Status to keep on whether user is viewing alert page.  If refresh is needed then refresh list
        if(NavigationController.sCurrentRequestUrl.indexOf(_config.GetConfigValue("preconfigured_snapin_url", "supportmessage_list")) != -1) 
        {
        g_isViewingAlertPage = true;
        //fix for alert prob - commenting closeminibcont - check
         //alert(_utils.LocalXLate("snapin_messageviewer", "sa_msg_NewMesssage"));
        //_msgprocessor.cl_msg_CloseMiniBcont();
          if (g_isRefreshNeeded) 
          {
          setTimeout(function(){$ss.snapin.msgViewer.Helper.refreshList();},50);
          g_isRefreshNeeded = false;
        }
          } 
          else 
          {
        g_isViewingAlertPage = false;
      }
      }
      catch(ex) 
      {
          g_Logger.error("MessageViewerSnapin:$ss.snapin.msgViewer.Helper.cl_msg_pageFocus()",ex.message);
       }      
    },
    // On a refresh event, give a popup and refresh list.  This is to support the message detail page
    // look at cl_msg_messagedetail.js where this event is issued
    cl_msg_pageRefresh: function(oEvent) 
    {
      try
      { 
        //alert(_utils.LocalXLate("snapin_messageviewer", "sa_msg_no_longer_available"));
      _utils_ui.FocusBcont();
      $ss.agentcore.events.SendByName("SS_EVT_SNAPIN_VP_FOCUS");
      $ss.snapin.msgViewer.Helper.refreshList();
      }
      catch(ex) 
      {
         g_Logger.error("MessageViewerSnapin:$ss.snapin.msgViewer.Helper.cl_msg_pageRefresh()",ex.message);
       }
    },
    cl_msg_updateSystrayThreaded: function() 
    {
      try 
      {
      // If not navigating out of alerts page, there is no need to update systray 
        if (NavigationController.sCurrentRequestUrl != _config.GetConfigValue("preconfigured_snapin_url", "supportmessage_list")) 
        {
        return;
      }
      _msgprocessor.cl_msg_updateSysTray(false);
      }
      catch(ex) 
      {
         g_Logger.error("MessageViewerSnapin:$ss.snapin.msgViewer.Helper.cl_msg_updateSystrayThreaded()",ex.message);
       }
    },
    cl_msg_updateSystrayAndClose: function() 
    {
      try 
      {
      _msgprocessor.cl_msg_updateSysTray(false);
      // If not closing from alerts page, there is no need to update systray and google gadget
        if (!g_isViewingAlertPage) 
        {
        return;
      }      
      return true;
      }
      catch(ex) 
      {
         g_Logger.error("MessageViewerSnapin:$ss.snapin.msgViewer.Helper.cl_msg_updateSystrayAndClose()",ex.message);
       }
    },
    AfterRenderList: function()
    {
      try 
      {
        $ss.agentcore.utils.ui.FixPng();
        var bForceFirstRun = (_config.GetConfigValue("firstrun", "force_first_run", "true") === "true");
        var firstRunCompleted = "true";
        if (bForceFirstRun) {
          firstRunCompleted = _config.GetUserValue("firstrun", "FirstRunCompleted");
        }
        if(firstRunCompleted == "true")
        {
          _events.Subscribe("BROADCAST_ON_EXTERNALCLOSE", this.cl_msg_OnClose);
          g_isViewingAlertPage = true;

          var sSnapinUrl = _config.GetConfigValue("preconfigured_snapin_url", "snapin_messagespopup", "messages");
          var sMiniBcontInstance = _utils.GetMiniBcontSpawnName(sSnapinUrl);
          if(_utils.IsMiniBcontRunning(sMiniBcontInstance)) {
            var oCmd = _utils.GetObjectFromCmdLine();
            var args = _utils.GetObjectFromQueryString(oCmd.path);
            if(args.caller !== "messages" && g_bCloseAlertNeeded) alert(_utils.LocalXLate("snapin_messageviewer", "cl_msg_popup_close_info"));
            _msgprocessor.cl_msg_CloseMiniBcont();
          }
          this.refreshList ();        
          // If a msgguid was passed, then forward user to detail page
          // Also check for param already set in session.  This is set in ss_shellinclude.js when
          // DSC is already open and user clicks on view detail
          //Show the detail only once when navigated from mini-bcont
          if (!_databag.GetValue("ss_msg_db_SelectedDetail")) 
          {
            //this.cl_msg_go_to_detail(_databag.GetValue("ss_msg_db_SelectedDetail"));
            //If its navigated from other snapins (if databag value is already set) do not show the details again
            oCmdLine = _utils_ui.GetObjectFromCmdLine();        
            if (oCmdLine["msgguid"]) 
            {
              this.cl_msg_go_to_detail(oCmdLine["msgguid"], true);
            } 
          }
        }
        else {
          $(".msgview_wait_message").css("display", "none");
          $(".msgview_restart_message").css("display", "block");
        }
      }
      catch(ex) {
        g_Logger.error("MessageViewerSnapin:$ss.snapin.msgViewer.Helper.AfterRenderList()",ex.message);
      }      
    },         
    cl_msg_OnClose: function()
    {
      try 
      {
        var oPendingMsgs = _msgprocessor.cl_msg_PeekMessages();
        var regRoot = _config.GetRegRoot();
        var user=_config.ExpandSysMacro("%USER%");
        var bSystray=_registry.GetRegValue($ss.agentcore.constants.REG_HKCU, regRoot+"\\users\\"+user+"\\ss_config\\Message", "systray");
        if(bSystray=="True")
          _msgprocessor.cl_msg_notifyTray(oPendingMsgs,true);
        else
          _msgprocessor.cl_msg_notifyTray(oPendingMsgs,false);      } 
      catch(ex) 
      {
          g_Logger.error("MessagePopupSnapin:$ss.snapin.msgViewer.Helper.cl_msg_OnClose()",ex.message);
      }
    },
    cl_msg_List_Delete: function(guid) 
    {
      try 
      { 
      // Delete from messages.xml and from UI
      _msgprocessor.cl_msg_IgnoreIt(guid);
      
      // Delete from session array
      if (_databag.GetValue("ss_msg_db_guids") != "") 
      {
        var storedguids = _databag.GetValue("ss_msg_db_guids");
        storedguids = storedguids.replace("," + guid,"");
        storedguids = storedguids.replace(guid + ",","");
        storedguids = storedguids.replace(guid,"");
        _databag.SetValue("ss_msg_db_guids", storedguids);
      }
      
      this.checkEmpty();  
      }
      catch(ex) 
      {
         g_Logger.error("MessageViewerSnapin:$ss.snapin.msgViewer.Helper.cl_msg_List_Delete()",ex.message);
       }
    },
    cl_msg_List_DeleteMsgs: function() 
    { 
      try 
      {
      var chkElements = $("tr.clsMsgMarker input:checkbox");
      var delAll = $("#scrollmessagesarea #chkDeleteAll");
      var sGuid;
      if(delAll.attr("checked"))
      {
        for(var i=0;i<chkElements.length;i++) {
          if(chkElements[i].id == "chkDeleteAll") {}
          else{
            sGuid = chkElements[i].value;
            if(sGuid != "" && sGuid != null && sGuid != "undefined")
            // Delete from messages.xml and from UI
            this.cl_msg_IgnoreIt(sGuid);
              // Delete from session array
              if (_databag.GetValue("ss_msg_db_guids") != "") {
                var storedguids = _databag.GetValue("ss_msg_db_guids");
                storedguids = storedguids.replace("," + sGuid,"");
                storedguids = storedguids.replace(sGuid + ",","");
                storedguids = storedguids.replace(sGuid,"");
                _databag.SetValue("ss_msg_db_guids", storedguids);
              }
          }
        }
         
      }
      else
      {
          for(var i=0;i<chkElements.length;i++) 
          {
          if(chkElements[i].id == "chkDeleteAll") {}
            else
            {
              if(chkElements[i].checked) 
              {
              sGuid = chkElements[i].value;
              if(sGuid != "" && sGuid != null && sGuid != "undefined")
              // Delete from messages.xml and from UI
              this.cl_msg_IgnoreIt(sGuid);
      // Delete from session array
                if (_databag.GetValue("ss_msg_db_guids") != "") 
                {
        var storedguids = _databag.GetValue("ss_msg_db_guids");
        storedguids = storedguids.replace("," + sGuid,"");
        storedguids = storedguids.replace(sGuid + ",","");
        storedguids = storedguids.replace(sGuid,"");
        _databag.SetValue("ss_msg_db_guids", storedguids);
      }
              }
            }
          }
          
        }
        
      this.checkEmpty();  
      // fix for alert during single message delete.
      this.cl_msg_EnableDelete("","");
      }
      catch(ex) 
      {
         g_Logger.error("MessageViewerSnapin:$ss.snapin.msgViewer.Helper.cl_msg_List_DeleteMsgs()",ex.message);
       }
    },
    checkEmpty: function() 
    {
        if(fromSnapin) var messageCount = $("#idMessages tr.shownMessages").length;
        else var messageCount = $("#idMessages tr.clsMsgMarker").length;
      if (messageCount != 1) 
      {
        $("#idMsgCounter").html(messageCount + " " + _utils.LocalXLate("snapin_messageviewer", "sa_msg_msgs"));
      } 
      else 
      {
        $("#idMsgCounter").html(messageCount + " " + _utils.LocalXLate("snapin_messageviewer", "sa_msg_msg"));
      }
   
      if (messageCount == 0) {
        $("tr.clsMsgMarker").css("display", "none");
        $("#messagebuttonsarea").css("display", "none");
        $("#idMessages").css("display", "block");
        $("#idMessages tr.clsMsgHeadCounter").css("display", "none");
       // $("#noMsgTable").css("display", "block");
        $("#scrollmessagesarea").html($("#noMsgTable").html());
        
      } else {
        $("#noMsgTable").css("display", "none");
        $("#idMessages").css("display", "block");

      }
    },
   
    cl_msg_getTmpXmlObj: function()
    {
		_databag.SetValue("db_msg_MessageList", true);
      // sync contents of messages.xml to filesystem - cl_msg_getMessagesXml() returns messages.xml XMLDocument
      var current_msgs_xml = _msgprocessor.cl_msg_getMessagesXml();
      var updated_msgs_xml = _msgprocessor.cl_msg_updateMessagesXml(current_msgs_xml, true);
      _databag.SetValue("db_msg_MessageList", false);
       return(updated_msgs_xml);
      // return(document.getElementById('idTmp').XMLDocument);
    },
    
    /*******************************************************************************
    ** Name:        cl_msg_displayMsgs
    **		
    ** Purpose:     handles display of messages:  gets details/url of message,
    **		transforms to ui, and sets initial view (by setting current index)
    **		
    ** Parameter:   none
    **              
    ** Return:      none
    *******************************************************************************/
   
    cl_msg_displayMsgs: function(pnCEvent, guid)
    {  
      //load idTmp data island - this is where we have cached the latest data
      var oTmp = this.cl_msg_getTmpXmlObj();
      
      
      // KL (7/17/07): if there's an trigger event, then remove all other non-triggered nodes EXCEPT the
      //               one which got triggered  
      if (pnCEvent == EVT_TRIGGEREVENT && guid)
      {        
        if (bMac) {
          var nodes = _Xml.SelectNodes(oTmp.responseXML, XPATH_MSG_STRING + "[@t='1' and @guid != '" + guid + "']");
          if (nodes) {
            for(var i = 0;  i< nodes.length ; i++) {
              var aNode = nodes[i];
              aNode.parentNode.removeChild(aNode);
            }
          }
        }
        else{
        oTmp.selectNodes(XPATH_MSG_STRING + "[@t='1' and @guid != '" + guid + "']").removeAll();
        }
        

        // set the top attribute to 1 so it will show on top
        var oNode = null;
        if (bMac) {
          oNode = _Xml.SelectSingleNode(this.cl_msg_getTmpXmlObj().responseXML, _msgprocessor.cl_msg_getXPathForMsgGuid(guid));
        }else {
          oNode = this.cl_msg_getTmpXmlObj().selectSingleNode(_msgprocessor.cl_msg_getXPathForMsgGuid(guid));
        }
        if (oNode)
        {
          oNode.setAttribute("top", "1");
        }
      }
      else
      {
        // for all other types, remove all non-triggered nodes as they don't belong here
        if (bMac) {
          var nodes = _Xml.SelectNodes(oTmp.responseXML, XPATH_NONTRIGGERED_STRING);
          if (nodes) {
            for(var i = 0;  i< nodes.length ; i++) {
              var aNode = nodes[i];
              aNode.parentNode.removeChild(aNode);
            }
          }

          nodes = _Xml.SelectNodes(oTmp.responseXML, XPATH_OUTDATED_STRING);
          if (nodes) {
            for(var i = 0;  i< nodes.length ; i++) {
              var aNode = nodes[i];
              aNode.parentNode.removeChild(aNode);
            }
      }
        }
        else{
          oTmp.selectNodes(XPATH_NONTRIGGERED_STRING).removeAll();  
      
      // remove all outdated or non-active nodes
      oTmp.selectNodes(XPATH_OUTDATED_STRING).removeAll();
        }
      }
      
     
      var oNodes = null;
      //get the collection of msg nodes

      if (bMac) {
      if(fromSnapin)  oNodes = _Xml.SelectNodes(oTmp.responseXML, XPATH_UNREAD_MSG_STRING);
      else  oNodes = _Xml.SelectNodes(oTmp.responseXML, XPATH_MSG_STRING);  
      }
      else{
         if(fromSnapin)  oNodes = oTmp.selectNodes(XPATH_UNREAD_MSG_STRING);
      else  oNodes = oTmp.selectNodes(XPATH_MSG_STRING);  
      }
         
      
      //store counter, and msg nodes length
      var nI,nMi = oNodes.length;
      
      //for each msg in idTmp data island
      for (nI=0;nI<nMi;nI++)
      {  
        _system.Sleep(10);
        //also, get the msg's url (if there is one) and add to msg node in idTmp
        if (bMac) {
        oNodes[nI].appendChild(_msgprocessor.cl_msg_createNewPropertyCDataNode(oTmp.responseXML,"url",_msgprocessor.cl_msg_getMessageUrl(oNodes[nI].getAttribute('guid'),oNodes[nI].getAttribute('ver'))));
        }else
        oNodes[nI].appendChild(_msgprocessor.cl_msg_createNewPropertyCDataNode(oTmp,"url",_msgprocessor.cl_msg_getMessageUrl(oNodes[nI].getAttribute('guid'),oNodes[nI].getAttribute('ver'))));
      }
      
      //**uncomment the following line of code in case if you view the "slow ui updation"
      //idMessages is where we innerHTML the UI (transformed xml)
      //first, turn off the display so that user only sees finished product
      //$('#idMessages').css("display", 'none');
      
      //transform idTmp data island with what ever idXsl is set to  - currently, popview.xsl 
      sDefaultHTM = $("#idMessages").html();
      if (bMac) {
        var xsltProcessor = new XSLTProcessor();
        var doc = document.getElementById('idXsl');
        var path = doc.getAttribute('src');
        var xsl = _Xml.LoadXML(path,false);
        xsl = this.xslResolveIncludes(xsl.responseXML);
        xsltProcessor.importStylesheet(xsl);
        var tr = xsltProcessor.transformToFragment(oTmp.responseXML, document);
        $('#idMessages').html(tr);
  
      }else{
      $('#idMessages').html(oTmp.transformNode(document.getElementById('idXsl')));
      }
      /*Hide rows if messages are already read and show only unread messages row in view*/
      if(fromSnapin){
        $('#idMessages').css("display","none");
        var msgCount = $(".FilterMsgs").length;
        for(var i=0;i< msgCount;i++){
          var id = $(".FilterMsgs")[i].id;
          var msgguid =$(".FilterMsgs")[i].getAttribute("guid");
          if(($.inArray(msgguid,arrUnreadMsgGuids) > -1)){
            $("#" + id).css("display","block"); 
            $("#" + id).addClass("shownMessages");
          }
          else{
            $("#" + id).css("display","none");
            $("#" + id).removeClass("clsMsgMarker");            
          }
        }
        $('#idMessages').css("display","block");
      }
      //now, perform localization on the new output (which is still hidden)
      $ss.agentcore.utils.ui.FixPng();
      if (pnCEvent == EVT_TRIGGEREVENT && guid)
      {
        // after the xsl transform,
        // update messages.xml to indicate this message was "triggered"
        _msgprocessor.updateXMLAttribute(guid, "t", "2");
      }    

      //we need to update the pagecounter so count all "message" tags to see what the total count is
      //"message" tag is special and just used by us and has no display...it makes it easier to track
      //messages in ui for user    

      if(fromSnapin) var oMsgs = $("#idMessages tr.shownMessages");    
      else  var oMsgs = $("#idMessages tr.clsMsgMarker");
      
      //store index counter, and count of messages  
      var nX=0,nMx=oMsgs.length;
      
      var guidArray = new Array();  
      
      //for each message
      for(nX=0;nX<nMx;nX++)
      {  
        _system.Sleep(10);
        //set the attr idx of that "message" to index counter (later we use this to determine
        //which message user is currently on)
        oMsgs[nX].setAttribute('idx',nX+1);
        
        //since we don't know total message counts in the UI (because of the way the xsl is ran)
        //we have to tell each message element what the total count is now since we have that info.
        //we could use this information later but currently we don't
        oMsgs[nX].setAttribute('of',nMx);
        
        guidArray[nX] = oMsgs[nX].getAttribute('guid'); 
      }
      if(fromSnapin) var oMsgsDoForMe = $("#idMessages tr.shownMessages span.button:disabled");    
      else var oMsgsDoForMe = $("#idMessages tr.clsMsgMarker span.button:disabled");
      var len = oMsgsDoForMe.length;
      for(var k=0;k<len;k++)
      {
        oMsgsDoForMe[k].className = "btn btn-primary-ss btn-sm disabled";
      }
      _databag.SetValue("ss_msg_db_guids", guidArray.toString());
      
      //if we have any messages in UI
      if (oMsgs.length)
      {    
        //set the global index pointer to the 1st one
        g_nCurrentMessage = 1;
        
        //and cl_msg_toggleDisplayOf() which will turn off the display of all messages except
        //for g_nCurrentMessage
        this.cl_msg_toggleDisplayOf();
        
        //**end hiding of ui
        //all done, now turn on UI so user can see messages 

        if(fromSnapin) $('#idMessages tr.shownMessages').show();
        else $('#idMessages tr.clsMsgMarker').show();
        
        //show bcont if hidden    
        _msgprocessor.cl_msg_show();    
      }
      else
      {   
        $('#idMessages').html(sDefaultHTM);
        //no messages to show so let's just close
        //ref:ss_container.js:ss_con_Close();
        this.cl_msg_close();
      }
    },

    xslResolveIncludes: function( xsl )
    {
      var xslns = "http://www.w3.org/1999/XSL/Transform";
      var includes = xsl.getElementsByTagNameNS( xslns, "include" ); // NodeList

      for ( var i = includes.length -1; i >= 0; i-- )
      {
        var n = includes.item(i);
        var path = _file.BuildPath(xsl.documentURI + "/../",n.getAttribute('href'));
        var d = _Xml.LoadXML(path,false);

        this.replaceNode( n, d.responseXML.documentElement.childNodes );
      }
      return xsl;
    },
    
    replaceNode: function( orig, replacement )
    {
      var tmp;
      //if (typeof replacement == "
      if ( replacement instanceof NodeList)
      {
        var frag = document.createDocumentFragment();
        for ( var i = 0; i < replacement.length; i ++ )
          frag.appendChild( document.importNode( replacement.item(i), true ) );
        replacement = frag;
      }
      orig.parentNode.replaceChild( tmp=document.importNode( replacement, true ), orig );
      return tmp;
    },
    /* cl_msg_messagelist.htm functions END */
    /* cl_msg_messagedetail.htm functions START */
    AfterRenderDetail: function()
    {
      try 
      {
        var sInstanceName=$ss.agentcore.dal.ini.GetIniValue("", "PROVIDERINFO", "instancename", "");
        if(sInstanceName=="minibcont") 
          _msgprocessor.cl_msg_CloseMiniBcont();
      
        // Assign session variable to local array
        msgArray = _databag.GetValue("ss_msg_db_guids").split(",");          
        
        // Read messages.xml
        current_msgs_xml = _msgprocessor.cl_msg_getMessagesXml();
        
        // Look for GUID to show
         for (i=0; i<msgArray.length; i++) 
         {
            if (_databag.GetValue("ss_msg_db_SelectedDetail") == msgArray[i]) 
            {
            g_nCurrentMessageGUID = _databag.GetValue("ss_msg_db_SelectedDetail");
            g_nCurrentMessage = i+1;
            this.cl_msg_detail_show(); 
            //_databag.SetValue("ss_msg_db_SelectedDetail","");//Retain the value so that detail is shown only once
            break;
          }
        }
          $ss.agentcore.utils.ui.FixPng();
        } 
        catch (ex) 
        {
        g_Logger.error("MessageViewerSnapin:$ss.snapin.msgViewer.Helper.AfterRenderDetail()",ex.message);
      }
    },  
    /*******************************************************************************
    ** Name:        cl_msg_detail_show
    **  
    ** Purpose:     Show a specific detail page set in a global variable called g_nCurrentMessageGUID
    **  
    ** Parameter:   none
    **              
    ** Return:      none   
    *******************************************************************************/ 
    cl_msg_detail_show: function() {
      var thisNode;
      try {
        if (bMac) {
          thisNode = _Xml.SelectSingleNode(current_msgs_xml.responseXML, _msgprocessor.cl_msg_getXPathForMsgGuid(g_nCurrentMessageGUID));  
        }
        else{
        thisNode = current_msgs_xml.selectSingleNode(_msgprocessor.cl_msg_getXPathForMsgGuid(g_nCurrentMessageGUID));  
        }
      } catch (e) {g_Logger.error("MessageViewerSnapin:$ss.snapin.msgViewer.Helper.cl_msg_detail_show()",e.message);}
     
      // If alert is not in messages.xml or file folder does not exist or alert is deleted, then restart message listing
      // Which will refresh it too. 
      if (bMac) {
        if (thisNode != null) {
          var nodeVal = '';
          var node = _Xml.SelectSingleNodeFromNode(thisNode, 'name', 'deleted_dt');
          if (node) {
            nodeVal = _Xml.GetNodeText(node);
            nodeVal = nodeVal == null ? '' : nodeVal;
          }
          if (typeof(thisNode) == 'undefined' 
                || !_file.FolderExists(_file.BuildPath(_msgprocessor.cl_msg_getMessagesPath(),g_nCurrentMessageGUID+"."+thisNode.getAttribute("ver"))) 
                || nodeVal != '') {
              _events.Send(_events.Create(SS_EVT_BCONT_REFRESH)); 
              return;
            }
        }
      }
      else {
      if (thisNode != null) {
          if (typeof(thisNode) == 'undefined' 
              || !_file.FolderExists(_file.BuildPath(_msgprocessor.cl_msg_getMessagesPath(),g_nCurrentMessageGUID+"."+thisNode.getAttribute("ver"))) 
              || thisNode.selectSingleNode("p[@name='deleted_dt']").text != '') {
            _events.Send(_events.Create(SS_EVT_BCONT_REFRESH)); 
            return;
          }
      }
    }
     
     if (bMac) {
      var serializer = new XMLSerializer();
      var contents = serializer.serializeToString(thisNode);
      var oXml = _Xml.LoadXMLFromString("<?xml version=\"1.0\"?><sprt><msgs>" + contents + "</msgs></sprt>", true);
     }else{
      var oXml = _Xml.LoadXMLFromString("<?xml version=\"1.0\"?><sprt><msgs>" + thisNode.xml + "</msgs></sprt>", true);
    }
    try {
      var serializer = new XMLSerializer();
      var contents = serializer.serializeToString(oXml.responseXML);
      if (bMac) {
      // document.getElementById('idTmp').innerHTML(contents);
      }else{
      document.getElementById('idTmp').load(oXml);
    }
    }
    catch(ex) {
      var serializer = new XMLSerializer();

    }
       if (bMac) {
        var xsltProcessor = new XSLTProcessor();
        var doc = document.getElementById('idXsl');
        var path = doc.getAttribute('src');
        var xsl = _Xml.LoadXML(path,false);
        xsl = this.xslResolveIncludes(xsl.responseXML);
        xsltProcessor.importStylesheet(xsl);
        var tr = xsltProcessor.transformToFragment(oXml.responseXML, document);
        $('#idMessages').html(tr);

      }else{
      $('#idMessages').html(oXml.transformNode(document.getElementById('idXsl')));
      }
	  var $abc = $('#messagesarea');
	  $abc = '<div id="messagesarea">' + $abc.html() + '</div>'
	  $('#idMessages').html($abc);
      $ss.agentcore.utils.ui.FixPng();
      this.cl_msg_getMessageDetails(g_nCurrentMessageGUID,thisNode.getAttribute("ver"));

      $('#cl_msg_Of').html(_utils.LocalXLate("snapin_messageviewer", "cl_msg_Of"));
      $('#idMsgCounter2').html(msgArray.length);
      $('#idMsgCounter1').html(g_nCurrentMessage);   
      var oMsgs = $("#idMessages tr.clsMsgMarker");
  
      var nCurX = g_nCurrentMessage - 1;            // current message index
      if (nCurX < 0) nCurX = 0;   

      var sGuid = oMsgs[0].getAttribute('guid');  
      var i = Number(g_nCurrentMessage);
      var j = i+1;
      var k = j+1;

      $("#btnNavigate1").html(i);
      $("#btnNavigate2").html(j);
      $("#btnNavigate3").html(k);

      // check the current message index status and set the buttons
      if (g_nCurrentMessage == msgArray.length){
        $("#btnNavigate2")[0].className="numbersdisabled";
        $("#btnNavigate3")[0].className="numbersdisabled";
        this.cl_msg_ChangeButtonState($("#idMsgNext"), false);
      } else {
       this.cl_msg_ChangeButtonState($("#idMsgNext"), true);
      }
      if(g_nCurrentMessage == msgArray.length-1){
        $("#btnNavigate3")[0].className="numbersdisabled";
      }
      //if the current message index <  1 then reset index to msgs.length (so we loop through)
      if (g_nCurrentMessage == 1){
        this.cl_msg_ChangeButtonState($("#idMsgPrev"), false);
      } else {
        this.cl_msg_ChangeButtonState($("#idMsgPrev"), true);
      }
      var oMsgsDoForMe = $("#idMessages #messagebuttonsarea span.button:disabled");
      var len = oMsgsDoForMe.length;
      for(var k=0;k<len;k++)
      {
        oMsgsDoForMe[k].className = "btn btn-primary-ss btn-sm disabled";
      }
      var sVer = thisNode.getAttribute("ver");
     
      // Set viewed messages in messages.xml
      this.UpdateDetailViewHelper(g_nCurrentMessageGUID, sVer);
      var pXML = _msgprocessor.updateShownTs(g_nCurrentMessageGUID,'V',sVer);  
      if(pXML)
      {
        document.getElementById('idTmp').load(pXML);
      }

      _rl_MsgShownDetail(g_nCurrentMessageGUID, sVer, false);
     
    },
    /*******************************************************************************
    ** Name:        cl_msg_ChangeButtonState
    **		
    ** Purpose:     for next arrow to be grayed when no more messages are there
    **		
    ** Parameter:   poThis, object, span element containing background image of arrow
    **              bFlag : 0 : disable
    **                      1 : enable
    **              
    ** Return:      none
    *******************************************************************************/
    cl_msg_ChangeButtonState: function(poThis, bFlag)
    {
      try {
        if(!poThis) return;
        var elemBtn = $("#" + poThis);
        if(poThis.hasClass("previous")) { 
          if (bFlag) {
            poThis.removeClass("previousdisabled");
            poThis.attr("disabled", false);
          } else {
            poThis.removeClass("previous");
            poThis.addClass("previousdisabled");
            poThis.attr("disabled", true);
          }  
        } else if (poThis.hasClass("next")) {
          if (bFlag) {
            poThis.removeClass("nextdisabled");
            poThis.attr("disabled", false);
          } else {
            poThis.removeClass("next");
            poThis.addClass("nextdisabled");
            poThis.attr("disabled", true);
          }
        }  
      } catch (ex) {
        try {
          g_Logger.error("cl_msg_dropHover", "Unexpected Error " + ex.message);
        } catch (ex2) {}
      }
    },
    /*******************************************************************************
    ** Name:        cl_msg_ExecPayloadDetail
    **  
    ** Purpose:     After a message is executed in the detail page.  This function updates
    **              the same information in the list page.
    **  
    ** Parameter:   none
    **              
    ** Return:      none   
    *******************************************************************************/
    cl_msg_ExecPayloadDetail: function(psGuid,psVersion,pCType,buttonElement) {
      try {
      var bSuccess;
      
      bSuccess = this.cl_msg_ExecPayload(psGuid,psVersion,pCType,buttonElement);     
      }catch(ex) {
        g_Logger.error("MessageViewerSnapin:$ss.snapin.msgViewer.Helper.cl_msg_ExecPayloadDetail()",ex.message);
      }   

    },
    /*******************************************************************************
    ** Name:        cl_msg_getMessageDetails
    **  
    ** Purpose:     gets message details from TO_MESSAGES_DETAILS file for a particular
    **              message
    **  
    ** Parameter:   psGuid, string, guid of message
    **              psVersion, string, version of message
    **              
    ** Return:      contents of TO_MESSAGE_DETAILS for that particular message (string)
    *******************************************************************************/
    cl_msg_getMessageDetails: function(psGuid,psVersion)
    {
      //build path to file using psGuid.psVersion
      var messageFile = _msgprocessor.cl_msg_getBuildPath(_msgprocessor.cl_msg_getBuildPath(_msgprocessor.cl_msg_getMessagesPath(),psGuid+"."+psVersion),TO_MESSAGES_DETAILS); 

      // Use XMLHTTP for proper language encoding
      var oXmlHttp = null;
      var contents_detail = "";
      if(window.ActiveXObject) {
        oXmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
        if(!oXmlHttp) {
          oXmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
         oXmlHttp.open("GET", messageFile, true);
      oXmlHttp.send(null);
      contents_detail = oXmlHttp.responseText;
      }else {
        var xml  =_Xml.LoadXML(messageFile,false);
        if (xml) {
          try {
             var serializer = new XMLSerializer();
             contents_detail = serializer.serializeToString(xml.responseXML);
          }catch(ex){
            
          }
        }
      }
   


      // Apply macros
      contents_detail = _utils.LangParseMacros(contents_detail, "snapin_messageviewer"); 

      //Assign to a hidden div
      $('#sa_msg_detail').html(contents_detail);
    
      // Add _blank as the target for all a href
      var aTags = $("#sa_msg_detail a"); 
      
      for (i=0;i<aTags.length;i++) { 
        // Do no add target if a javascript opens it up on a different window anyways
        if (aTags[i].href.indexOf("window.open") == -1) {
          aTags[i].setAttribute("target","_blank");
        }
      }      
    },
    /*******************************************************************************
    ** Name:        cl_msg_getMessageProperty
    **		
    ** Purpose:     Retrieves the message property value.
    **		
    ** Parameter:   Guid of the content, Version # of the content, name of property
    **              
   ** Return:      Content property value
    *******************************************************************************/
    cl_msg_getMessageProperty: function(psGuid,psVersion,psProperty)
    {
      try {
        return _content.GetContentProperty("sprt_msg", psGuid, psVersion, psProperty);
      } catch(ex) {}
    },
    /*******************************************************************************
    ** Name:        cl_msg_toggleDisplayOf
    **		
    ** Purpose:     updates the ui to display current message only.
    **		
    ** Parameter:   none
    **              
    ** Return:      none
    *******************************************************************************/
    cl_msg_toggleDisplayOf: function()
    {  
      if (g_showAll) return;
      
      //get collection of messages elements - this tell us how many messages in current ui
      var oMsgs = $("#idMessages tr.clsMsgMarker");
      
      //var counter and  msgs.length
      var nX=0,nMx=oMsgs.length;
      
      // check the current message index status and set the buttons
      if (g_nCurrentMessage == nMx){
        this.cl_msg_ChangeButtonState($("#idMsgNext"), false);
      } else if (g_nCurrentMessage == (nMx-1)) {
        this.cl_msg_ChangeButtonState($("#idMsgNext"), true);
      }
      
      //if the current message index <  1 then reset index to msgs.length (so we loop through)
      if (g_nCurrentMessage == 1){
        this.cl_msg_ChangeButtonState($("#idMsgPrev"), false);
      } else if (g_nCurrentMessage == 2) {
        this.cl_msg_ChangeButtonState($("#idMsgPrev"), true);
      }
  
      //store guid of message that we are currently on
      var sGuid = '';
      
      //update the X of N counter on top of UI
      $('#idMsgCounter1').html(g_nCurrentMessage);
      $('#idMsgCounter2').html(nMx);  
  
      var nCurX = g_nCurrentMessage - 1;            // current message index
      if (nCurX < 0) nCurX = 0;                     // check for lower-bound
      
      sGuid = oMsgs[nCurX].getAttribute('guid');    // get the msg guid from the element  
      g_nCurrentMessageGUID = sGuid;                // set the global GUID to the currently select GUID      
      $("#" + sGuid).css("display", 'block');     // set display to block
          
      var nPrevX = g_nCurrentMessage - 2;           // index of prev message
      var nNextX = g_nCurrentMessage;               // index of next message

       // schen - display current msg guid

      var sVer = oMsgs[nCurX].getAttribute('version');
      var tv = oMsgs[nCurX].getAttribute('tv');

      if (tv != '1')
      {
        oMsgs[nCurX].setAttribute('tv', '1'); 
        _msgprocessor.updateXMLAttribute(sGuid, 'tv', '1'); 
        _msgprocessor.cl_msg_updateTitleViewReport(sGuid, sVer)  
      }  
     
      // hide prev message
      if (nPrevX >= 0)
      {    
        sGuid = oMsgs[nPrevX].getAttribute('guid');      
        $("#" + sGuid).css("display", 'none');  
      }
      
      // hide next message
      if (nNextX < nMx)
      {
        sGuid = oMsgs[nNextX].getAttribute('guid');
        $("#" + sGuid).css("display", 'none');    
      }  
    },
    cl_msg_go_to_detail: function(guid, bLaunchFromMini) {
      try {
      // Save the selected guid into the session first
      _databag.SetValue("ss_msg_db_SelectedDetail", guid);
if (bLaunchFromMini == undefined) {
  bLaunchFromMini = true;
}
      if(bLaunchFromMini) {
        var params = {};

        params.requesturl = _config.GetConfigValue("preconfigured_snapin_url", "supportmessage_list")+"?pagetoLoad=detail"; //add additional parameters.
        MVC.Controller.dispatch("navigation","index",params);
      }
      }catch(ex) {
        g_Logger.error("MessageViewerSnapin:$ss.snapin.msgViewer.Helper.cl_msg_go_to_detail()",ex.message);
      }
    },
    cl_msg_IgnoreIt: function(psGuid)
    {
      //load messages.xml
      var oXml = _msgprocessor.cl_msg_getMessagesXml();
      
      //load msg node for the one we want to ignore
      var oNode = null;
      if (bMac) {
        oNode = _Xml.SelectSingleNode(oXml.responseXML, _msgprocessor.cl_msg_getXPathForMsgGuid(psGuid));
      }else{
        oNode = oXml.selectSingleNode(_msgprocessor.cl_msg_getXPathForMsgGuid(psGuid));

      }
      
      // Gets the version of this message
      var oVersion = oNode.getAttribute('ver');
      
      //let's check just in case
      if(oNode)
      {    
        //set delete_dt property to timestamp 
        if (bMac) {
          var node = _Xml.SelectSingleNodeFromNode(oNode,"name", "deleted_dt");
          if (node) {
            //node.appendChild(document.createTextNode(_msgprocessor.cl_msg_getTs()));
              node.textContent = _msgprocessor.cl_msg_getTs(); // Ramkumar
          }
          var nodes = oNode.childNodes;
          if (nodes) {
            for(var i = 0;  i< nodes.length ; i++) {
              var aNode = nodes[i];
              if (aNode.getAttribute('name') != 'deleted_dt') { // Ramkumar
                i--;
               aNode.parentNode.removeChild(aNode);
              }
            }
          }
        }else{
        oNode.selectSingleNode("p[@name='deleted_dt']").text = _msgprocessor.cl_msg_getTs();
        
        //remove all unecessary p nodes (everything except for the one that had deleted_dt)
        oNode.selectNodes("p[@name!='deleted_dt']").removeAll();
        
        }
          
        //save the new message.xml
        if (bMac) {
          var serializer = new XMLSerializer();
          var writetofile = serializer.serializeToString(oXml.responseXML);
          _msgprocessor.cl_msg_saveToFile(writetofile, _msgprocessor.cl_msg_getMessageXMLFilePath());
        }else{
        _msgprocessor.cl_msg_saveToFile(oXml.xml, _msgprocessor.cl_msg_getMessageXMLFilePath());
        }
        
        // decrement current message if it is the last message
       // var oMsgs = ss_ui_html_getElementsByClassName("div", "clsMsgMarker", ss_utl_Id("idMessages"));    
		   var oMsgs =$("#idMessages tr.clsMsgMarker");    

        if (g_nCurrentMessage == oMsgs.length) g_nCurrentMessage--;
      }

      //wipe out the node for this message  
      if (bMac) {
        var node = document.getElementById('id'+psGuid);
        node.parentNode.removeChild(node);
      } 
      else {
      document.getElementById('id'+psGuid).removeNode(true);
    }
      
      
      //Adding this event for reporting
      _msgprocessor.cl_msg_updateDeleteReport(psGuid,oVersion);
       
      this.cl_msg_resynchTmpMessages();  
    },

    /*******************************************************************************
    ** Name:        cl_msg_close
    **            
    ** Purpose:     Closes the window conditionally based on whether it is a show all
    **            
    ** Parameter:   none
    **              
    ** Return:      none   
    *******************************************************************************/
    cl_msg_close: function()
    {
      try {
      if (g_showAll) return;
      
      // start fading away
      _msgprocessor.AnimateHideWindow();
       
        _utils_ui.HideBcont();
      // close it for real
      _utils_ui.CloseBcont(); 
      }catch(ex) {
        g_Logger.error("MessageViewerSnapin:$ss.snapin.msgViewer.Helper.cl_msg_close()",ex.message);
      }

    },
    /*******************************************************************************
    ** Name:        cl_msg_checkKey
    **		
    ** Purpose:     For accessiblity purposes, trap enter key and send click event. 
    ** 		Used in tabstops
    **		
    ** Parameter:   element to send click event to
    **              
    ** Return:      none   
    *******************************************************************************/ 
    cl_msg_checkKey: function(el)
    {  
      try {
      if (event.keyCode==13)
        el.click();
      }catch(ex) {
        g_Logger.error("MessageViewerSnapin:$ss.snapin.msgViewer.Helper.cl_msg_checkKey()",ex.message);
      }
    },

/*******************************************************************************
    ** Name:        cl_msg_dropClick
    **		
    ** Purpose:     for arrows on display, this returns arrows back to normal (ie,
    **		called by onmouseout event).  calls from ui - look in xsl (pagercount)
    **		
    ** Parameter:   poThis, object, span element containing background image of arrow
    **              
    ** Return:      none
    *******************************************************************************/
    cl_msg_dropClick: function(poThis)
    {
      // do nothing
    },
    /*******************************************************************************
    ** Name:        cl_msg_dropHover
    **		
    ** Purpose:     for arrows on display, this drops arrows down to hover slide (ie,
    **		called by onhover event).  calls from ui - look in xsl (pagercount)
    **		
    ** Parameter:   poThis, object, span element containing background image of arrow
    **              
    ** Return:      none    
    *******************************************************************************/
    cl_msg_dropHover: function(poThis)
    {
      try {
        if(!poThis) return;
        if(poThis.hasClass("clsMsgPrev")) {
          poThis.addClass("clsMsgPrevOver");
        } else if (poThis.hasClass("clsMsgNext")) {
          poThis.addClass("clsMsgNextOver");
        }  
      } catch (ex) {
        try {
          g_Logger.error("cl_msg_dropHover", "Unexpected Error " + ex.message);
        } catch (ex2) {}
      }
    },
    /*******************************************************************************
    ** Name:        cl_msg_dropBack
    **		
    ** Purpose:     for arrows on display, this returns arrows back to normal (ie,
    **		called by onmouseout event).  calls from ui - look in xsl (pagercount)
    **		
    ** Parameter:   poThis, object, span element containing background image of arrow
    **              
    ** Return:      none
    *******************************************************************************/
    cl_msg_dropBack: function(poThis)
    {
      try {
        if(!poThis) return;
        poThis.removeClass("clsMsgPrevOver");
        poThis.removeClass("clsMsgNextOver");
      } catch (ex) {
        try {
          g_Logger.error("cl_msg_dropBack", "Unexpected Error " + ex.message);
        } catch (ex2) {}
      }
    },

    /*******************************************************************************
    ** Name:        cl_msg_ExecPayload
    **		
    ** Purpose:     for Executing any payload type - run SA, Viewlet, both, or Tell me URL
    **		
    ** Parameter:   psGuid, string, guid of message that contains SA to run
    **		psVersion, string, version of message that contains SA to run
    **		pCType, integer/Constant, corresponds with message type:
    **		0 = TYPE_SUPPORTACTIONONLY - SA only
    **		1 = TYPE_VIEWLETONLY - viewlet only
    **		2 = TYPE_VIEWLETACTION - viewlet and SA
    **    3 = TYPE_TELLME - Tell me URl
    **              
    ** Return:      none
    *******************************************************************************/
    cl_msg_ExecPayload: function(psGuid,psVersion,pCType,buttonElement)
    {
      // do nothing if buttonElement is disabled
      if (buttonElement.disabled) return;
      
      //store local
      var sAction,sViewlet;
      var bSuccess = true;
  
      // If message folder is no longer available, i.e. pruned, then send event for a refresh
      if (!_file.FolderExists(_msgprocessor.cl_msg_getBuildPath(_msgprocessor.cl_msg_getMessagesPath(),psGuid+"."+psVersion))) {
        _events.Send(_events.Create(SS_EVT_BCONT_REFRESH)); 
        return;
      }
      
      // gray out and disable link/button so user can't click on it twice
      // it will be re-enabled if payload did not get executed successfully  
      buttonElement.disabled = true;
      if(fromListPage){
        buttonElement.className = "col-xs-12 btn btn-primary-ss btn-sm panel-widget-body-ss";
      }else{
        buttonElement.className = "btn btn-primary-ss btn-sm";
      }

      
      var bNeedsElevation = (buttonElement.elevation == "1");
        payloadObject = new Object();
        payloadObject.psGuid = psGuid;
        payloadObject.psVersion = psVersion;
        payloadObject.pCType = pCType;
        payloadObject.buttonElement = buttonElement;
      if (!bMac) {
      bSuccess = _msgprocessor.cl_msg_ExecPayloadHelper(psGuid,psVersion,pCType);
      // when running supportactions, 0 means success
        payloadObject.bSuccess = bSuccess;
        this.cl_msg_Update_after_execution(payloadObject);
      }
      else {
        _msgprocessor.cl_msg_ExecPayloadHelperMac(this, psGuid,psVersion,pCType);
      }
      
    },
 
    cl_msg_ExecPayloadHelperCompleted: function(result) {
        payloadObject.bSuccess = result;
        this.cl_msg_Update_after_execution(payloadObject);
      },

    cl_msg_Update_after_execution: function(result) {
      var bSuccess = result.bSuccess;
      var psGuid = result.psGuid;
      var psVersion = result.psVersion;
      var pCType = result.pCType;
      var buttonElement = result.buttonElement;
     try {
      var pXML;
      // When user performs an action, it should change to "viewed"
      if (bSuccess) {
        this.cl_msg_updateExecutedReport(psGuid,'S',psVersion,pCType);
        // Try to update the title font weight
        try {
          var titleElement = $('#title' + psGuid);   
          titleElement.css("fontWeight", "normal");
        } catch (e) {}    
        
      } else {
        this.cl_msg_updateExecutedReport(psGuid,'U',psVersion,pCType);        
      }
    
      // Update XML file
      pXML = _msgprocessor.updateXMLValue(psGuid, 'payload_status', bSuccess ? 'S' : 'U');
      if(pXML)
      {
          if (bMac) {
          var serializer = new XMLSerializer();
          var contents = serializer.serializeToString(pXML.responseXML);
          // document.getElementById('idTmp').textContent = contents;
          // document.getElementById('idTmp').innerHTML = (contents);
        }else{
       document.getElementById('idTmp').load(pXML);
      } 
      } 
      // Re-enable button/link if payload did not get executed successfully
      // Exceptions are "Tell me" and "Viewlet Only" type, we always want to re-enable them
      if (!bSuccess || (pCType == TYPE_TELLME) || (pCType == TYPE_VIEWLETONLY))
      {
        buttonElement.disabled = false;
        if(fromListPage){
          buttonElement.className = "col-xs-12 btn btn-primary-ss btn-sm  button panel-widget-body-ss";
        }else{
          buttonElement.className = "btn btn-primary-ss btn-sm button";
        }
          
      }
    
      // set button/link caption to "completed" if payload was executed successfully
      if (bSuccess)
      {
        // Set the button value after it has been clicked
        buttonElement.innerHTML = _msgprocessor.cl_msg_getValueFrom(psGuid,psVersion,XPATH_COMPLETEDCAPTION_STRING);
      } else {
        buttonElement.innerHTML = _utils.LocalXLate("snapin_messageviewer", "cl_msg_TryAgain");
      }
      }catch(ex) {
        g_Logger.error("MessageViewerSnapin:$ss.snapin.msgViewer.Helper.cl_msg_ExecPayload()",ex.message);
      }
      // Try to update the status field with the latest status
      try {
        var statusElement = $('#status' + psGuid);
        if (bSuccess) {
          if (pCType == TYPE_TELLME || pCType == TYPE_VIEWLETONLY) {
            statusElement[0].innerHTML = "<i class=\"fa fa-check\"></i>" + _utils.LocalXLate("snapin_messageviewer", "sa_msg_status_completed");
            statusElement[0].className = "label label-default status";
          } else {
            statusElement[0].innerHTML = "<i class=\"fa fa-check\"></i>" + _utils.LocalXLate("snapin_messageviewer", "sa_msg_status_success");
            statusElement[0].className = "label label-default status";
          }
        } else {
            if(_databag.GetValue("INTERNET_FAILED") == true)
              $("#sa_msg_offline_text").show(); 
          statusElement[0].innerHTML = "<i class=\"fa fa-times\"></i>" + _utils.LocalXLate("snapin_messageviewer", "sa_msg_status_fail");
          statusElement[0].className = "label label-default statuserror";
        }
        // Fix for EE22086
        this.cl_msg_Update_executed_status(psGuid, bSuccess);
      } catch (e) {g_Logger.error("MessageViewerSnapin:$ss.snapin.msgViewer.Helper.cl_msg_ExecPayload()",e.message);}   
        
      return bSuccess;
    },
 
    cl_msg_Update_executed_status: function(psGuid, bSuccess) {
      try {
        if (current_msgs_xml) {
          var oNode = current_msgs_xml.selectSingleNode(_msgprocessor.cl_msg_getXPathForMsgGuid(psGuid));
          oNode.selectSingleNode("p[@name='" + "payload_status" + "']").text = bSuccess ? 'S' : 'F';
        }
      }
      catch (ex) {
      }
    },

    cl_msg_updateExecutedReport: function(psGuid, psStatus, iVersion, iActionType)
    {
       // Capture only once.  If new version of alert is downloaded and detail is viewed, no need to count again.
      var logShownTs = true;

      //load idTmp data island - this is where we have cached the latest data 
      var oTmp = this.cl_msg_getTmpXmlObj();

      //get msg node for psGuid
      var oNode = null;
      if (bMac) {
        oNode = _Xml.SelectSingleNode(oTmp.responseXML, _msgprocessor.cl_msg_getXPathForMsgGuid(psGuid));
        if (this.getNodeValueHelperMac(oNode,"name","shown_dt")!="") logShownTs = false;
      }
      else {
        oNode = oTmp.selectSingleNode(_msgprocessor.cl_msg_getXPathForMsgGuid(psGuid));
      if (this.getNodeValueHelper(oNode,"p[@name='shown_dt']")!="") logShownTs = false;
      }
        var pXML=_msgprocessor.updateXMLValue(psGuid, "shown_dt", _msgprocessor.cl_msg_getTs());
      
      if(pXML) {
        if (bMac) {
          var serializer = new XMLSerializer();
          var contents = serializer.serializeToString(pXML.responseXML);
          document.getElementById('idTmp').nodeValue = (contents);
        }else{
         document.getElementById('idTmp').load(pXML);
        }
      }
       if ((psStatus  != undefined) && logShownTs)
       {
         var actionstring = "";
  
         switch(iActionType)
         {
          case 0: actionstring = "_do"; break;
          case 1: 
          case 2: actionstring = "_show"; break;
          case 3: actionstring = "_tell"; break;
         }
         
         if (psStatus == 'S')
           var oLogEntry = $ss.agentcore.reporting.Reports.CreateLogEntry(psGuid,iVersion,"msg_execute" , 1,"","","","", $ss.agentcore.constants.RESULT_SUCCESS);
         else if (psStatus == 'U')
           var oLogEntry = $ss.agentcore.reporting.Reports.CreateLogEntry(psGuid,iVersion,"msg_execute" , 1,"","","","", $ss.agentcore.constants.RESULT_FAILED);
         
         _msgprocessor.cl_msg_ContentUsage(oLogEntry,false);
      }   
        
    }, 
    
    getNodeValueHelperMac: function(poXml,sAttrName, sAttrValue) {
      var sReturn = '';
      var node = _Xml.SelectSingleNodeFromNode(poXml,sAttrName, sAttrValue);
      if (node) {
        sReturn = node.getAttribute(sAttrValue);
      }
      return sReturn;
    },

    getNodeValueHelper:function (poXml,psXPath)
    {
      //store return
      var sReturn = '';
      try
      {
        //get node and store text property
        if (bMac) {
          sReturn = _Xml.GetNodeText(this.getNodeHelper(poXml,psXPath));
        }else {
        sReturn = this.getNodeHelper(poXml,psXPath).text;
      }
      }
      catch(oErr)
      {
        //on err, return empty
        sReturn='';
      }
      return(sReturn);
    },
    getNodeHelper: function(poXml,psXPath)
    {
      //store return
      var vReturn;
      try
      {
       
        vReturn = poXml.selectSingleNode(psXPath);
      
      }
      catch(oErr)
      {
        //on err, return false
        vReturn = false;
      }
      return(vReturn);
    },
    /*******************************************************************************
    ** Name:        cl_msg_resynchTmpMessages
    **		
    ** Purpose:     counts the "message" tags and sets the idx/of of the each element
    **		which will be read by cl_msg_cl_msg_toggleDisplayOf() 
    **		
    ** Parameter:   none
    **              
    ** Return:      none
    *******************************************************************************/
    cl_msg_resynchTmpMessages: function()
    {  
      //get collection of "message" tags  
      var oMsgs = $("#idMessages tr.clsMsgMarker");
      
      //get length and set nX=0
      var nX=0,nMx=oMsgs.length;
  
      //for each one
      for(nX=0;nX<nMx;nX++)
      {    
        //set idx attr - later this will tell us what msg we are currently reading, etc..
        oMsgs[nX].setAttribute('idx',nX+1);
        
        //set of attr - total msg counts which we will need later for X of <N> logic
        oMsgs[nX].setAttribute('of',nMx);
      }
      
      //if there are messages this go display the next one
      if(nMx)
      {    
        //will handle display and count (X of <N>)    
        this.cl_msg_toggleDisplayOf();
      }
      else
      {    
        //no more msgs so let's just exit here.
        this.cl_msg_close();
      }
    },
    /*******************************************************************************
    ** Name:        cl_msg_detail_delete
    **  
    ** Purpose:     Deletes a message in the detail page
    **  
    ** Parameter:   none
    **              
    ** Return:      none   
    *******************************************************************************/
    cl_msg_detail_delete: function() {
      try {
        var deletedMessageGUID = g_nCurrentMessageGUID;

        // Delete message from array and reset the counter
        msgArray.splice(g_nCurrentMessage-1,1);
        _databag.SetValue("ss_msg_db_guids", msgArray.toString());
        $('#idMsgCounter2').html(msgArray.length);

      
        // Delete message from the messages.xmls
        var oXml = _msgprocessor.cl_msg_getMessagesXml();
        //load msg node for the one we want to ignore
        var oNode  = null;
        if (bMac) {
          oNode = _Xml.SelectSingleNode(oXml.responseXML,_msgprocessor.cl_msg_getXPathForMsgGuid(deletedMessageGUID));
        }
        else {
          oNode = oXml.selectSingleNode(_msgprocessor.cl_msg_getXPathForMsgGuid(deletedMessageGUID));
        }
        // Gets the version of this message
        var oVersion = "";

        if(oNode)
          {    
            oVersion = oNode.getAttribute('ver');
            //set delete_dt property to timestamp 
            if (bMac) {
              var node = _Xml.SelectSingleNodeFromNode(oNode,"name", "deleted_dt");
              if (node) {
                //node.appendChild(document.createTextNode(_msgprocessor.cl_msg_getTs()));
                node.textContent = _msgprocessor.cl_msg_getTs(); // Ramkumar
              }
              var nodes = oNode.childNodes;
              if (nodes) {
                for(var i = 0;  i< nodes.length ; i++) {
                  var aNode = nodes[i];
                  if (aNode.getAttribute('name') != 'deleted_dt') { // Ramkumar
                    aNode.parentNode.removeChild(aNode);
                  }
                }
              }

              var serializer = new XMLSerializer();
              var writetofile = serializer.serializeToString(oXml.responseXML);
              _msgprocessor.cl_msg_saveToFile(writetofile, _msgprocessor.cl_msg_getMessageXMLFilePath());
  
            }else{
              oNode.selectSingleNode("p[@name='deleted_dt']").text = _msgprocessor.cl_msg_getTs();

              //remove all unecessary p nodes (everything except for the one that had deleted_dt)
              oNode.selectNodes("p[@name!='deleted_dt']").removeAll();
      
              //save the new message.xml
              _msgprocessor.cl_msg_saveToFile(oXml.xml, _msgprocessor.cl_msg_getMessageXMLFilePath());
            }
          }

          // If no more message, then take user to a fresh message listing page
          if (msgArray.length == 0) {
            var params = {};
            params.requesturl = _config.GetConfigValue("preconfigured_snapin_url", "supportmessage_list")+"?pagetoLoad=emptypage"; //add additional parameters.
            MVC.Controller.dispatch("navigation","index",params);
            // If on the last message then show last message
          } else if (g_nCurrentMessage > msgArray.length) {
            g_nCurrentMessage = msgArray.length;
            g_nCurrentMessageGUID = msgArray[g_nCurrentMessage-1];
            this.cl_msg_detail_show();        
            // Else just show the next message
          } else {
            g_nCurrentMessageGUID = msgArray[g_nCurrentMessage-1];
            this.cl_msg_detail_show();
          }
          //Adding this event for reporting
          _msgprocessor.cl_msg_updateDeleteReport(deletedMessageGUID,oVersion);
        }catch(ex) {
            g_Logger.error("MessageViewerSnapin:$ss.snapin.msgViewer.Helper.cl_msg_detail_delete()",ex.message);
      }
    },
    /*******************************************************************************
    ** Name:        cl_msg_detail_nextMessage
    **  
    ** Purpose:     Move to the next message in the detail page
    **  
    ** Parameter:   none
    **              
    ** Return:      none   
    *******************************************************************************/ 
    cl_msg_detail_nextMessage: function() {
      try {
      if($("#idMsgNext").attr("disabled")) return;
      g_nCurrentMessage ++;
      g_nCurrentMessageGUID = msgArray[g_nCurrentMessage-1];
      this.cl_msg_detail_show();
      }catch(ex) {
         g_Logger.error("MessageViewerSnapin:$ss.snapin.msgViewer.Helper.cl_msg_detail_nextMessage()",ex.message);
      }
    },
    /*******************************************************************************
    ** Name:        cl_msg_detail_prevMessage
    **  
    ** Purpose:     Move to the previous message in the detail page
    **  
    ** Parameter:   none
    **              
    ** Return:      none   
    *******************************************************************************/ 
    cl_msg_detail_prevMessage: function() {
      try{
      if($("#idMsgPrev").attr("disabled")) return;
      g_nCurrentMessage --;
      g_nCurrentMessageGUID = msgArray[g_nCurrentMessage-1];
      this.cl_msg_detail_show();
      }catch(ex) {
         g_Logger.error("MessageViewerSnapin:$ss.snapin.msgViewer.Helper.cl_msg_detail_prevMessage()",ex.message);
      }
    },
    cl_msg_EnableDelete: function(sDel, params)
    {
      try {
        if(params.checked == false) {
          $("#chkDeleteAll")[0].checked = false;  
        } 
      if(sDel == "all")
      {
        if($("#chkDeleteAll")[0].checked)
    {
      $("#butoncolumnright #sa_msg_DeleteMsg").attr("disabled", false);
          $("#butoncolumnright #sa_msg_DeleteMsg")[0].className = "btn btn-primary-ss btn-sm";
        }
        else
        {
          $("#butoncolumnright #sa_msg_DeleteMsg").attr("disabled", true);
          $("#butoncolumnright #sa_msg_DeleteMsg")[0].className = "btn btn-primary-ss btn-sm";
        }
      }
      else {
        var elemCheck = $("tr.clsMsgMarker .checkboxclass");
        for(var j=0; j<elemCheck.length;  j++)
        {
          if(elemCheck[j].checked)
          {
            $("#butoncolumnright #sa_msg_DeleteMsg").attr("disabled", false);
            $("#butoncolumnright #sa_msg_DeleteMsg")[0].className = "btn btn-primary-ss btn-sm";
            break;
          }
          else {
            $("#chkDeleteAll")[0].checked = false;
            $("#butoncolumnright #sa_msg_DeleteMsg").attr("disabled", true);
            $("#butoncolumnright #sa_msg_DeleteMsg")[0].className = "btn btn-primary-ss btn-sm";
          }
        }
      }
      }
      catch(ex) {
         g_Logger.error("MessageViewerSnapin:$ss.snapin.msgViewer.Helper.cl_msg_EnableDelete()",ex.message);
      }
    },
    UpdateDetailViewHelper: function(psGuid, iVersion) 
    {
      // Capture only once.  If new version of alert is downloaded and detail is viewed, no need to count again.
      var logShownTs = true;

      //load idTmp data island - this is where we have cached the latest data 
      var oTmp = this.cl_msg_getTmpXmlObj();

      //get msg node for psGuid
      if (bMac) {
        var oNode = _Xml.SelectSingleNode(oTmp.responseXML,_msgprocessor.cl_msg_getXPathForMsgGuid(psGuid));
      }else {
      var oNode = oTmp.selectSingleNode(_msgprocessor.cl_msg_getXPathForMsgGuid(psGuid));
      }

      if (bMac) {
      if (this.getNodeValueHelperMac(oNode,"name", "shown_dt")!="") logShownTs = false;
      }
      else{
      if (this.getNodeValueHelper(oNode,"p[@name='shown_dt']")!="") logShownTs = false;
      }
      
      return logShownTs; 
      
    }

  });//end of extend
    
  /********************************************************************************/
  //Private Area 
  /********************************************************************************/
  var _content = $ss.agentcore.dal.content;
  var _databag = $ss.agentcore.dal.databag;    
  var _utils =$ss.agentcore.utils;
  var _utils_ui =$ss.agentcore.utils.ui;
  var _file =$ss.agentcore.dal.file;
  var _Xml =$ss.agentcore.dal.xml;
  var _const = $ss.agentcore.constants;
  var _system = $ss.agentcore.utils.system;
  var _config = $ss.agentcore.dal.config;
  var _registry = $ss.agentcore.dal.registry;
  var _shell = $ss.agentcore.shell;
  var g_Logger = $ss.agentcore.log.GetDefaultLogger("$ss.snapin.msgViewer.Helper");  
    var _events = $ss.agentcore.events ; 
  var _msgprocessor=$ss.snapin.msgprocessor.helper;    
  var _rlogging = $ss.agentcore.reporting.Reports ;
  var sDefaultHTM;
  var g_isRefreshNeeded = false;
  var g_isViewingAlertPage = false;
  var g_bDialogShowing = false;
  var g_notificationType;
  var msgArray;
  var current_msgs_xml;
  var SS_EVT_BCONT_REFRESH          = "SS_EVT_BCONT_REFRESH";
  var SS_EVT_BCONT_CLOSE_DIALOG     = "SS_EVT_BCONT_CLOSE_DIALOG";
  //index to current message to show in ui
  var g_nCurrentMessage              = 0;

  //current message guid
  var g_nCurrentMessageGUID;
  
  //used by big bcont to indicate showing all
  var g_showAll;
  var g_oFso = null;

  var bMac = $ss.agentcore.utils.IsRunningInMacMachine();
    var payloadObject;

  try {
    var g_bCloseAlertNeeded = (_config.GetConfigValue("sprtmsg", "need_mini_bcont_close_alert", "False").toLowerCase() === "false")? false: true; 
  } catch(e) {}

  var g_BlinkCookie = 0;
  var g_NumOfBlinks = 0;
  var g_MAXBLINKS = 3;
  //action types
  var TYPE_SUPPORTACTIONONLY         = 0;
  var TYPE_VIEWLETONLY               = 1;
  var TYPE_VIEWLETACTION             = 2;
  var TYPE_TELLME                    = 3; 
  
  //xpaths for the <guid>.<ver>.xml's
  var XPATH_CONTENTSET_STRING        = "/sprt/content-set/content";
  var XPATH_COMPLETEDCAPTION_STRING  = XPATH_CONTENTSET_STRING + "/field-set/field[@name='Completed Link Caption']/value";
  var XPATH_INCOMPLETECAPTION_STRING = XPATH_CONTENTSET_STRING + "/field-set/field[@name='Incomplete Link Caption']/value";
  var XPATH_PRIORITY_STRING          = XPATH_CONTENTSET_STRING + "/field-set/field[@name='Priority']/value";
  var XPATH_TITLE_STRING             = XPATH_CONTENTSET_STRING + "/field-set/field[@name='Title']/value";
  var XPATH_TELLME_STRING            = XPATH_CONTENTSET_STRING + "/field-set/field[@name='Tell Me']/value";
  var XPATH_VIEWLET_STRING           = XPATH_CONTENTSET_STRING + "/field-set/field[@name='Viewlet']/value[@name='sccfb_field_value_blob']";
  var XPATH_SUPPORTACTION_STRING     = XPATH_CONTENTSET_STRING + "/field-set/field[@name='SupportAction']/value[@name='sccfb_field_value_blob']";
  var XPATH_SA_REQUIRE_ADMIN_STRING  = XPATH_CONTENTSET_STRING + "/field-set/field[@name='SupportAction Admin Privileges']/value[@name='sccf_field_value_int']";
  var XPATH_MOREDETAILS_STRING       = XPATH_CONTENTSET_STRING + "/field-set/field[@name='Detail']/value[@name='sccfc_field_value_clob']";

  var XPATH_EXPIREDATE_STRING        = XPATH_CONTENTSET_STRING + "/property-set/property[@name='scc_expire_date']";
  var XPATH_ACTIVEDATE_STRING        = XPATH_CONTENTSET_STRING + "/property-set/property[@name='scc_active_date']";
  var XPATH_LASTMODDATE_STRING       = XPATH_CONTENTSET_STRING + "/property-set/property[@name='scc_last_mod']";
  var XPATH_TELLMEURL_STRING         = XPATH_CONTENTSET_STRING + "/field-set/field[@name='Tell Me']/value[@name='sccf_field_value_char']";
  var XPATH_ISTRIGGER_STRING         = XPATH_CONTENTSET_STRING + "/field-set/field[starts-with(@name,'TriggerType')]"; 
    
  //event types (or, why were we called)
  EVT_NONE                               = 0;
  EVT_TIMEREVENT                         = 1;
  EVT_SYNCEVENT                          = 2;
  EVT_TRIGGEREVENT                       = 3;
  EVT_USEREVENT                          = 4;  //no arguments or msglist
  EVT_VIEWDETAILSEVENT                   = 5;  //user select to view details

  //path to a msg's detail.htm file (the message) relative from TO_MESSAGES_PATH\<guid>.<version>\dir
  var TO_MESSAGES_DETAILS            = 'Detail.htm';

  //message.xml xpaths
  var XPATH_MSG_STRING               = "/sprt/msgs/msg";
  var XPATH_UNREAD_MSG_STRING        = "sprt/msgs/msg[@t!='1' and p[@name='payload_status']!= 'S']";

  // non-viewed trigger messages (triggered messages will be @t='2')
  var XPATH_NONTRIGGERED_STRING      = XPATH_MSG_STRING + "[@t='1']";

  // out-dated messages
  var XPATH_OUTDATED_STRING          = XPATH_MSG_STRING + "[p[@name='deleted_dt']!='' or @exp='1' or @act='0']";
  
  function _rl_MsgShownDetail(sFlowId, iVersion,bDoSurvey )
  {
    var oLogEntry = _rlogging.CreateLogEntry(sFlowId,    iVersion,       "msg_detail", 1,    "",       "", "",    "",  "");
    return _msgprocessor.cl_msg_ContentUsage(oLogEntry, bDoSurvey);
  }
  

})();
