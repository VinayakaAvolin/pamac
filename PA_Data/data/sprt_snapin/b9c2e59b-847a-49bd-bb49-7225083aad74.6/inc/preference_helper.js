$ss.snapin = $ss.snapin ||{};
$ss.snapin.preference = $ss.snapin.preference ||{};
$ss.snapin.preference.helper = $ss.snapin.preference.helper ||{};
$ss.snapin.preference.helper.messages = $ss.snapin.preference.helper.messages ||{};

(function(){
  $.extend($ss.snapin.preference.helper.messages, {
    RenderPreferenceDetail: function(oObj, sSnapinName, sPagePath){ 
      var sSnapinPath = $ss.getSnapinAbsolutePath(sSnapinName);
      try {
        return oObj.render({
          partial: sSnapinPath + sPagePath,
          absolute_url: sSnapinPath + sPagePath,
          cache: false
        });
      } 
      catch (ex) {
        return false;
      }
    }
  });
})();


$ss.snapin.preference.message = $ss.snapin.preference.message ||{};

(function(){
  $.extend($ss.snapin.preference.message, {
  
    GetNotificationGroup: function(){
      return $("#snap_pref_Notification_frequency")[0];
    },
    
    EnableNotificationMethodGroup: function(){
      $("#snp_pref_enable_disable")[0].disabled = false;
      $("#snap_pref_Notification_frequency")[0].disabled = false;
    },
    
    DisableNotificationMethodGroup: function(){
      $("#snp_pref_enable_disable")[0].disabled = true;
      $("#snap_pref_Notification_frequency")[0].disabled = true;
    },
    
    GetEnableState: function(){
      var enableOption = $("#notification_settings_enable_radio")[0];
      if (enableOption) {
        if (!enableOption.checked) {
          return false;
        }
      }
      return true;
    },
    
    HandleNotificationGroup: function(){      
      if (_mph.GetEnableState()) {
        _mph.EnableNotificationMethodGroup();
      }
      else {
        _mph.DisableNotificationMethodGroup();
      }
    },
    
    AfterRender: function(){
      var PathforFreq="Software\\SupportSoft\\ProviderList\\"; 
      var user=$ss.agentcore.dal.config.ExpandSysMacro("%USER%");
      var ntf_freq=$ss.agentcore.dal.registry.GetRegValue($ss.agentcore.constants.REG_HKCU, PathforFreq+$ss.agentcore.dal.config.GetProviderID()+"\\users\\"+user+"\\Message", "LowFreqHours");
      ntf_freq=parseInt(ntf_freq,16)  ;
      if(ntf_freq!="")
        $("#snap_pref_Notification_frequency").prop("value",ntf_freq);
      _mph.HandleNotificationGroup();
    },
    BeforeSave: function(dataToSave){ 
       var bSetSuccess=true;
       var objReturn = {
        "RetVal": true,
        "ErrorMessage": ""
      };
      
      try{
        
        for(i=0;i<dataToSave.length;i++)
        {
          if(dataToSave[i].name=="snap_pref_Notification_frequency") 
            bSetSuccess=$ss.agentcore.dal.registry.SetRegValueByType($ss.agentcore.constants.REG_HKCU,"Software\\SupportSoft\\ProviderList\\"+$ss.agentcore.dal.config.GetProviderID()+"\\users\\"+$ss.agentcore.dal.config.ExpandSysMacro("%USER%")+"\\Message", "LowFreqHours", $ss.agentcore.constants.REG_DWORD, dataToSave[i].value) 
        }
        if(!bSetSuccess){
          objReturn.RetVal=false;  
          objReturn.ErrorMessage = $ss.agentcore.utils.LocalXLate("snapin_preference","Error");
          $("#lbl_notification_freq_error").html(objReturn.ErrorMessage);
          $("#lbl_notification_freq_error").css("display","block");
        }    
                   
      }
      catch(e){
        objReturn.ErrorMessage = $ss.agentcore.utils.LocalXLate("snapin_preference","Error");
        $("#lbl_notification_freq_error").html(objReturn.ErrorMessage);
        $("#lbl_notification_freq_error").css("display","block");
      }  
     
     return objReturn;
          
    }
    
  });
  
  //private handlers
  
  var _mph = $ss.snapin.preference.message;

  
})();


 var prefController = typeof(SnapinPreferenceController) !== "undefined" ? SnapinPreferenceController : SnapinBaseController;
 SnapinPreferenceController = prefController.extend("snapin_preference",{
      "#notification_settings_disable_radio click": function(params){
          $ss.snapin.preference.message.DisableNotificationMethodGroup();
      },
      "#notification_settings_enable_radio click": function(params){
        $ss.snapin.preference.message.EnableNotificationMethodGroup();
      }
    }
  )
  



