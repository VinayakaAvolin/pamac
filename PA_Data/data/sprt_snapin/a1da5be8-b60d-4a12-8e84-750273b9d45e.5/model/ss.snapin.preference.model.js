/**
 * @author Dinesh Venugopalan
 */
$ss.snapin = $ss.snapin ||
{};
$ss.snapin.preference = $ss.snapin.preference ||
{};
$ss.snapin.preference.model = $ss.snapin.preference.model ||
{};

(function () {

    $.extend($ss.agentcore.constants, {
        PR_KEY_NOT_PRESENT: "NURTSRIF",
        PR_CONSTRUCTOR_FAILED: "Constrcutor Failed",
        PR_SCOPE_PERSISTANCE: "persistance",
        PR_SCOPE_SESSION: "session",
        PR_SNAPIN_NAME: "snapin_preference",
        PR_CONFIG_FILE: "config.xml",
        PR_CANT_SAVE: "Save Failed",
        PR_NOT_FOUND: -1,
        PR_MIN_LENGTH_FOR_FUNCTION_NAME: 2,
        PR_DELIMITER: "@##@",
        PR_EXCEPTION: "Internal Error",
        PR_PROXY_WORKING_WITHOUT_USERNAME: "Your connection is working correctly without proxy credential input",
        PR_RE_ENTER_USERNAME: "Please re-enter user name and password"
    });
})();

/*
 * This class will hold the information about the individual preference fields
 */
$ss.snapin.preference.model.PreferenceFieldInfo = Class.extend({
    init: function (regPath) {
        var _const = $ss.agentcore.constants;

        try {
            this._id = "";
            this._scope = "";
            this._cache = "";
            this._container = "";
            this._regFolderName = "";
            this._keyName = "";
            this._type = "";
            this._encrypt = "";
            this._onSaveErrorDisplayID = "";
            this._registryValue = _const.PR_KEY_NOT_PRESENT;
            this._changedValue = _const.PR_KEY_NOT_PRESENT;
            this._regPath = regPath;
            this._containerColl = null;
        }
        catch (e) {
            throw {
                name: 'TypeError',
                message: _const.PR_CONSTRUCTOR_FAILED
            }
        }
    },

    /*
     * This function will return the id of the field.
     * Input . None
     * OutPut . Id of the field.
     */
    GetID: function () {
        return this._id;
    },

    /*
     *  This function will return the value that is saved in the registry or databag
     *  Input : void
     *  Output: Returns the stored value, in case the value was never saved then it returns NURTSRIF
     */
    GetSavedValue: function () {
        return this._registryValue;
    },

    /*
     This function will update the changed value of the field
     Input : value to be updated
     Output: true if success else false
     */
    SetChangedValue: function (value) {
        this._changedValue = value;
        return true;
    },

    /*
     This function will update the changed value of the field
     Input : value to be updated
     Output: true if success else false
     */
    GetErrorFieldId: function () {
        return this._onSaveErrorDisplayID;
    },
    /*
     This function will read the xml node attributes for preference field and update itself.
     Input : preference node in the xml
     Output: true if success else false
     */
    UpdateFromXML: function (preferenceNodeAttributes) {
        if (!preferenceNodeAttributes) {
            return false;
        }

        try {
            this._id = preferenceNodeAttributes.getNamedItem("id").nodeValue;
            this._scope = preferenceNodeAttributes.getNamedItem("scope").nodeValue;
            this._cache = preferenceNodeAttributes.getNamedItem("cache").nodeValue;
            this._container = preferenceNodeAttributes.getNamedItem("container").nodeValue;
            this._regFolderName = preferenceNodeAttributes.getNamedItem("regFolderName").nodeValue;
            this._keyName = preferenceNodeAttributes.getNamedItem("keyName").nodeValue;
            this._type = preferenceNodeAttributes.getNamedItem("type").nodeValue;
            this._encrypt = preferenceNodeAttributes.getNamedItem("encrypt").nodeValue;
            this._onSaveErrorDisplayID = preferenceNodeAttributes.getNamedItem("onSaveErrorDisplayID").nodeValue;
        }
        catch (e) {
            return false;
        }

        return this.ValidatePreferenceFieldInfo();
    },

    /*
     * This function will validate the values of the field
     * Input . None
     * OutPut . true in case of success else false.
     */
    ValidatePreferenceFieldInfo: function () {
        var _const = $ss.agentcore.constants;

        //container could be an array so split it
        if ((_const.PR_SCOPE_PERSISTANCE == this._scope)) {

            //validate the type
            if (!this.ValidateType()) {
                return false;
            }

            //validate the container
            if ((this._container) && (3 < this._container.length)) {
                this._regPath += this._regFolderName;
                this._containerColl = this._container.split(";");

                if (!this.UpdateValueFromRegistry()) {
                    return false;
                }
            }
        }


        return true;
    },

    /*
     This function will validate the type for this field.
     Input : None
     Output: true if success else false
     */
    ValidateType: function () {
        var _const = $ss.agentcore.constants;
        switch (this._type) {
            case 'REG_SZ':
                this._type = _const.REG_SZ;
                break;
            case 'REG_BINARY':
                this._type = _const.REG_BINARY;
                break;
            case 'REG_DWORD':
                this._type = _const.REG_DWORD;
                break;
            case 'REG_MULTI_SZ':
                this._type = _const.REG_MULTI_SZ;
                break;
            default:
                return false;
        }

        return true;
    },

    /*
     This function will update the value, after it reads it from the registry.
     Input : None
     Output: true if success else false
     */
    UpdateValueFromRegistry: function () {

        for (var i = 0; i < this._containerColl.length; i++) {
            var fieldValue = null;
            var regValue = null;
            try {
                //check whether the value is present in the registry or not
                if ($ss.agentcore.dal.registry.RegValueExists(this._containerColl[i], this._regPath, this._keyName)) {
                    regValue = $ss.agentcore.dal.registry.GetRegValue(this._containerColl[i], this._regPath, this._keyName);
                    if (regValue) {
                        fieldValue = regValue;
                        if ("true" == this._encrypt) {
                            fieldValue = $ss.agentcore.utils.DecryptHexToStr(regValue);
                        }
                        this._registryValue = fieldValue;
                        this._changedValue = fieldValue;
                        return true;
                    }
                }
            }
            catch (e) {
                return false;
            }
        }
        return true;
    },

    /*
     * This function will save the values of the field
     * Input . None
     * OutPut . true in case of success else false.
     */
    Save: function () {
        var _const = $ss.agentcore.constants;

        if (_const.PR_SCOPE_PERSISTANCE == this._scope) {
            //Save it to the registry only if the this._registryValue and this._changedValue are not same
            if (this._changedValue != this._registryValue) {

                if (!this.SaveInfoInRegistry()) {
                    return false;
                }
            }
        }
        else {
            //Save it to the databag
            return this.SaveInfoInDataBag();
        }

        return true;
    },

    /*
     * This function will write the information into the databag.
     * Input . None
     * OutPut . true in case of success else false.
     */
    SaveInfoInDataBag: function () {
        var key = "preference_" + this._regFolderName + "_" + this._keyName + this._id;
        return $ss.agentcore.dal.databag.SetValue(key, this._changedValue, true);
    },

    /*
     * This function will write the information into the registry.
     * Input . None
     * OutPut . true in case of success else false.
     */
    SaveInfoInRegistry: function () {
        var regValue = "";
        for (var i = 0; i < this._containerColl.length; i++) {
            try {
                regValue = this._changedValue;
                //if encrytion is on then we need to encrypt it
                if ("true" == this._encrypt) {
                    regValue = $ss.agentcore.utils.EncryptStrToHex(this._changedValue);
                }
                if (!$ss.agentcore.dal.registry.SetRegValueByType(this._containerColl[i], this._regPath, this._keyName, this._type, regValue)) {
                    return false;
                }

                this._registryValue = this._changedValue;
            }
            catch (e) {
                return false;
            }
        }
        return true;
    }

});

/*
 * This class holds a collection of PreferenceFieldInfo
 */
$ss.snapin.preference.model.PreferenceField = Class.extend({
    init: function () {
        var _const = $ss.agentcore.constants;

        try {
            this._preferenceFieldColl = new Array();
            //     
            this._regPath = this.GetRegPath();
            this._error = {
                id: "",
                message: ""
            };
        }
        catch (e) {
            throw {
                name: 'TypeError',
                message: _const.PR_CONSTRUCTOR_FAILED
            }
        }
    },

    GetRegPath: function () {
        var regRoot = $ss.agentcore.dal.config.GetRegRoot();
        var user = $ss.agentcore.dal.config.ExpandSysMacro("%USER%");
        var path = null;

        if (regRoot) {
            path = regRoot + "\\users\\" + user + "\\ss_config\\";
        }

        return path
    },

    /*
     * This function will return the error value for save.
     * Input . None
     * OutPut. Erro object.
     */
    GetErrorValue: function () {
        return this._error;
    },

    /*
     * This function will read the xml and update the field info.
     * Input . xml node pointing to tabinfo
     * OutPut. true in case of success else false.
     */
    UpdateFromXML: function (tabInfoNode) {

        var iterator = 0;
        var preferenceFieldInfo = null;

        //sanity check
        if ((!tabInfoNode) || (!tabInfoNode.childNodes[1]) || (!tabInfoNode.childNodes[1].childNodes)) {
            return false;
        }

        for (iterator = 0; iterator < tabInfoNode.childNodes[1].childNodes.length; iterator++) {
            try {
                preferenceFieldInfo = new $ss.snapin.preference.model.PreferenceFieldInfo(this._regPath);
            }
            catch (e) {
                return false;
            }

            if (!preferenceFieldInfo.UpdateFromXML(tabInfoNode.childNodes[1].childNodes[iterator].attributes)) {
                return false;
            }

            this._preferenceFieldColl.push(preferenceFieldInfo);
        }

        return true;
    },

    /*
     * This function will save each field info.
     * Input . xml node pointing to tabinfo
     * OutPut. true in case of success else false.
     */
    Save: function () {
        this._error.id = "";
        this._error.message = "";

        var _const = $ss.agentcore.constants;

        for (var iterator = 0; iterator < this._preferenceFieldColl.length; iterator++) {
            if (!this._preferenceFieldColl[iterator].Save()) {
                this._error.id = this._preferenceFieldColl[iterator].GetErrorFieldId();
                this._error.message = _const.PR_CANT_SAVE;

                return false;
            }
        }

        return true;
    },

    /*
     * This function will update the fieldinfo whose id is matching.
     * Input . fieldId. Id of the fieldinfo
     * 			fieldValue. Value that needs to be updated.
     * OutPut. true in case of success else false.
     */
    UpdateFieldInfo: function (fieldId, fieldValue) {
        var _const = $ss.agentcore.constants;

        var iterator = this.GetFieldWithMatchingId(fieldId);
        if (_const.PR_NOT_FOUND != iterator) {
            this._preferenceFieldColl[iterator].SetChangedValue(fieldValue);
            return true;
        }
        return false
    },

    /*
     * This function will find the fieldinfo whose id is matching.
     * Input . fieldId. Id of the fieldinfo	 *
     * OutPut. Iterator of the fieldInfo in case of success else error.
     */
    GetFieldWithMatchingId: function (fieldId) {
        var _const = $ss.agentcore.constants;

        for (var iterator = 0; iterator < this._preferenceFieldColl.length; iterator++) {
            if (fieldId == this._preferenceFieldColl[iterator].GetID()) {
                return iterator;
            }
        }

        return _const.PR_NOT_FOUND;
    },

    /*
     * This function will get the fieldinfo.
     * Input : tabName. Name of the tab in which this field is present.
     * 		   fieldId. Id of the field.
     * Output: value in case of success else null
     */
    GetFieldInfo: function (fieldId, fieldValue) {
        var _const = $ss.agentcore.constants;

        var iterator = this.GetFieldWithMatchingId(fieldId);
        if (_const.PR_NOT_FOUND != iterator) {
            return this._preferenceFieldColl[iterator].GetSavedValue();
        }

        return null;
    },

    /*
     * This function will all the values present in filedinfo.
     * Input : None.
     * Output: an array of the form id PR_DELIMITER value, else null
     */
    GetFieldInfoData: function () {
        var _const = $ss.agentcore.constants;
        var data = {};

        for (var iterator = 0; iterator < this._preferenceFieldColl.length; iterator++) {
            if (this._preferenceFieldColl[iterator].GetSavedValue() !== _const.PR_KEY_NOT_PRESENT) {
                data[this._preferenceFieldColl[iterator].GetID()] = this._preferenceFieldColl[iterator].GetSavedValue();
            }
        }

        return data;
    },

    /*
     * This function will get all the ids present in filedinfo.
     * Input : None.
     * Output: an array of the form id PR_DELIMITER error id, else null
     */
    GetFieldIds: function () {
        var _const = $ss.agentcore.constants;
        var data = null;
        try {
            data = new Array();
        }
        catch (e) {
            return null;
        }

        for (var iterator = 0; iterator < this._preferenceFieldColl.length; iterator++) {
            data.push(this._preferenceFieldColl[iterator].GetID() + _const.PR_DELIMITER + this._preferenceFieldColl[iterator].GetErrorFieldId());
        }

        return data;
    }
});

/*
 * This class holds the information about a particular tab
 */
$ss.snapin.preference.model.PreferenceFieldTabInfo = Class.extend({
    init: function () {
        var _const = $ss.agentcore.constants;
        try {
            this._name = "";
            this._viewpage = "\\"; //snapinPath ;
            this._displaypriority = 0;
            this._tabimage = "";
            this._tabDisplayText = "";
            this._beforeRender = "";
            this._afterRender = "";
            this._beforeSave = "";
            this._afterSave = "";
            this._preferenceField = new $ss.snapin.preference.model.PreferenceField();
            this._selected = false;
            this._snapin_name = "";
        }
        catch (e) {
            throw {
                name: 'TypeError',
                message: _const.PR_CONSTRUCTOR_FAILED
            }
        }
    },

    GetName: function () {
        return this._name;
    },

    GetViewPage: function () {
        return this._viewpage;
    },

    GetSnapinNameForTab: function () {
        return this._snapin_name;
    },

    GetDisplaypriority: function () {
        return this._displaypriority;
    },

    GetTabimage: function () {
        return this._tabimage;
    },

    GetTabDisplayText: function () {
        return this._tabDisplayText;
    },

    GetBeforeRender: function () {
        return this._beforeRender;
    },

    GetAfterRender: function () {
        return this._afterRender;
    },

    GetPreferenceField: function () {
        return this._preferenceField
    },

    SetSelected: function (value) {
        this._selected = value;
    },

    UpdateFromXML: function (tabInfoNode) {
        try {
            var tabInfoAttributes = tabInfoNode.childNodes[0].childNodes[0].attributes;
            this._name = tabInfoAttributes.getNamedItem("name").nodeValue;
            this._snapin_name = tabInfoAttributes.getNamedItem("snapin_name").nodeValue;
            this._viewpage += $ss.agentcore.dal.config.ParseMacros(tabInfoAttributes.getNamedItem("viewpage").nodeValue);
            this._displaypriority = tabInfoAttributes.getNamedItem("displaypriority").nodeValue;
            this._tabimage = tabInfoAttributes.getNamedItem("tabimage").nodeValue;
            this._tabDisplayText = $ss.agentcore.utils.LocalXLate("snapin_preference", tabInfoAttributes.getNamedItem("tabDisplayText").nodeValue);
            this._beforeRender = tabInfoAttributes.getNamedItem("beforeRender").nodeValue;
            this._afterRender = tabInfoAttributes.getNamedItem("afterRender").nodeValue;
            this._beforeSave = tabInfoAttributes.getNamedItem("beforeSave").nodeValue;
            this._afterSave = tabInfoAttributes.getNamedItem("afterSave").nodeValue;

            //load the field information for this tab
            if (!this._preferenceField.UpdateFromXML(tabInfoNode)) {
                return false;
            }
        }
        catch (e) {
            return false;
        }

        return true;
    },

    Save: function () {

        return this._preferenceField.Save();

        return true;
    },

    GetErrorValue: function () {
        return this._preferenceField.GetErrorValue();
    },

    CallBeforeRenderFunction: function () {
        this.CallRenderFunction(this._beforeRender);
    },

    CallAfterRenderFunction: function () {
        this.CallRenderFunction(this._afterRender);
    },

    CallRenderFunction: function (functionName) {
        var _const = $ss.agentcore.constants;

        if ((functionName) && (_const.PR_MIN_LENGTH_FOR_FUNCTION_NAME < functionName.length)) {
            try {
                eval(functionName)();
            }
            catch (e) {
            }
        }
    },

    UpdateFieldInfo: function (fieldId, fieldValue) {
        return this._preferenceField.UpdateFieldInfo(fieldId, fieldValue);
    },

    /*
     * This function will get the fieldinfo.
     * Input : tabName. Name of the tab in which this field is present.
     * 		   fieldId. Id of the field.
     * Output: value in case of success else null
     */
    GetFieldInfo: function (fieldId, fieldValue) {
        return this._preferenceField.GetFieldInfo(fieldId, fieldValue);
    },

    /*
     * This function will all the values present in filedinfo.
     * Input : None.
     * Output: an array of the form id PR_DELIMITER value, else null
     */
    GetFieldInfoData: function () {
        return this._preferenceField.GetFieldInfoData();
    },

    /*
     * This function will get all the ids present in filedinfo.
     * Input : None.
     * Output: an array of the form id PR_DELIMITER value, else null
     */
    GetFieldIds: function (tabName) {
        return this._preferenceField.GetFieldIds();
    },

    CallBeforeSaveFunction: function (dataToSave) {
        return this.ExecuteFunction(this._beforeSave, dataToSave);
    },

    CallAfterSaveFunction: function (dataToSave) {
        return this.ExecuteFunction(this._afterSave, dataToSave);
    },

    /*
   * This function will call function present in the variable.
   * Input . None
   * OutPut . true in case of success else false.
   */
    ExecuteFunction: function (functionName, dataToSave) {
        var _const = $ss.agentcore.constants;
        var objReturn = {
            "RetVal": true,
            "ErrorMessage": $ss.agentcore.utils.LocalXLate("snapin_preference", "Error")
        };

        if ((functionName) && (_const.PR_MIN_LENGTH_FOR_FUNCTION_NAME < functionName.length)) {
            try {
                var returnVal = eval(functionName + "(dataToSave)");
                if ("object" === typeof (returnVal)) {
                    if ("boolean" === typeof (returnVal.RetVal)) {
                        objReturn.RetVal = returnVal.RetVal
                    }
                    else {
                        objReturn.RetVal = returnVal;
                    }
                    objReturn.ErrorMessage = returnVal.ErrorMessage || returnVal;
                }
            }
            catch (e) {
                objReturn.RetVal = false;
            }
        }

        return objReturn;
    }
});

$ss.snapin.preference.model.PreferenceFieldTab = Class.extend({
    init: function () {
        var _const = $ss.agentcore.constants;

        try {
            this._preferenceTabInfoColl = new Array();
            this._error = {
                id: "",
                message: ""
            };
            this._currentSelectedTabIterator = 0;
        }
        catch (e) {
            throw {
                name: 'TypeError',
                message: _const.PR_CONSTRUCTOR_FAILED
            }
        }
    },

    GetPreferenceTabInfoCollection: function () {
        if (0 == this._preferenceTabInfoColl.length) {
            return null;
        }
        return this._preferenceTabInfoColl;
    },

    UpdateFromXML: function (preferenceNodes) {
        var iterator = 0; // for iterating the nodes
        var preferenceFieldTabInfo = null; // object for storing tab properties
        var preferenceFieldTabAttribues = null; // Stores attributes for a tab
        //sanity check
        if (!preferenceNodes) {
            return false;
        }

        var sRegRoot, providerID, prodName;
        sRegRoot = "HKLM";
        providerID = $ss.agentcore.dal.config.GetProviderID();
        prodName = $ss.agentcore.dal.config.ParseMacros("%PRODUCT%");
        sRegKey = "Software\\SupportSoft\\ProviderList\\" + providerID + "\\SubAgent\\" + prodName;
        sShowBuildInfo = ($ss.agentcore.dal.registry.GetRegValue(sRegRoot, sRegKey, "ShowBuildInfo")).toLowerCase();
        sShowSyncInfo = ($ss.agentcore.dal.registry.GetRegValue(sRegRoot, sRegKey, "ShowSyncInfo")).toLowerCase();
        if (sShowBuildInfo == "")
            sShowBuildInfo = "no";
        if (sShowSyncInfo == "")
            sShowSyncInfo = "no";

        //iterate the nodes collection and for each node create an object
        for (var iterator = 0; iterator < preferenceNodes.length; iterator++) {
            var showtabinfo = true;  // Variable to decide whether to show n0de/tab in the snapin
            try {
                preferenceFieldTabInfo = new $ss.snapin.preference.model.PreferenceFieldTabInfo();
            }
            catch (e) {
                return false;
            }

            if (!preferenceFieldTabInfo.UpdateFromXML(preferenceNodes[iterator])) {
                return false;
            }

            //For Mac disable General and Notification tab
             
            
            if (bMac) {
                if (preferenceFieldTabInfo._name == "tab_snapin_pref_general") {
                    showtabinfo = false;
                }
                if (preferenceFieldTabInfo._name == "tab_snapin_pref_message_notification") {
                    showtabinfo = false;
                }
            }
            // Loading sync status and build information htmls based on registry value*/
            if ((preferenceFieldTabInfo._name == "tab_snapin_sync_status" && sShowSyncInfo == "no") || (preferenceFieldTabInfo._name == "tab_snapin_build_Info" && sShowBuildInfo == "no")) {
                showtabinfo = false;
            }
            if (showtabinfo == true) {
                this._preferenceTabInfoColl.push(preferenceFieldTabInfo);
            }
        }

        this.SortTabCollection();
        return true;
    },

    /*
     This function is used to sort the tabs present in the collection based on display priority
     Input : tabCollection.Collection of tab inf0
     Output: Returns true in case of success else false
     */
    SortTabCollection: function () {
        this._preferenceTabInfoColl.sort(this.DisplayOrder);

        return true;
    },

    /*
     This function is used to compare which tab should be given more priority
     Input : firstTabInfo. Reference to first tab.
     secondTabinfo. Reference to second tab.
     Output: Returns negative,0 or positive values
     */
    DisplayOrder: function (firstTabInfo, secondTabinfo) {
        if ((!firstTabInfo) || (!secondTabinfo)) {
            return 0;
        }
        return (firstTabInfo.GetDisplaypriority() - secondTabinfo.GetDisplaypriority())
    },

    Save: function () {
        this._error.id = "";
        this._error.message = "";
        for (var iterator = 0; iterator < this._preferenceTabInfoColl.length; iterator++) {
            if (!this._preferenceTabInfoColl[iterator].Save()) {
                this._error = this._preferenceTabInfoColl[iterator].GetErrorValue();
                return false;
            }
        }

        return true;
    },

    GetErrorValue: function () {
        return this._error;
    },

    SaveTabInfo: function (tabName) {
        var _const = $ss.agentcore.constants;

        var iterator = this.GetTabWithMatchingName(tabName);
        if (_const.PR_NOT_FOUND != iterator) {
            if (this._preferenceTabInfoColl[iterator].Save()) {
                return true;
            }
            else {
                this._error = this._preferenceTabInfoColl[iterator].GetErrorValue();
            }
        }

        return false;
    },

    UpdateFieldInfo: function (tabName, fieldId, fieldValue) {
        var _const = $ss.agentcore.constants;

        var iterator = this.GetTabWithMatchingName(tabName);
        if (_const.PR_NOT_FOUND != iterator) {
            return this._preferenceTabInfoColl[iterator].UpdateFieldInfo(fieldId, fieldValue);
        }

        return false;
    },

    /*
     * This function will get the fieldinfo.
     * Input : tabName. Name of the tab in which this field is present.
     * 		   fieldId. Id of the field.
     * Output: value in case of success else null
     */
    GetFieldInfo: function (tabName, fieldId, fieldValue) {
        var _const = $ss.agentcore.constants;

        var iterator = this.GetTabWithMatchingName(tabName);
        if (_const.PR_NOT_FOUND != iterator) {
            return this._preferenceTabInfoColl[iterator].GetFieldInfo(fieldId, fieldValue);
        }
        return null;
    },

    GetTabWithMatchingName: function (tabName) {
        var _const = $ss.agentcore.constants;

        for (var iterator = 0; iterator < this._preferenceTabInfoColl.length; iterator++) {
            if (this._preferenceTabInfoColl[iterator].GetName() == tabName) {
                return iterator;
            }
        }

        return _const.PR_NOT_FOUND;
    },

    /*
     * This function will call the function that needs to be called before this tab is rendered.
     * Input : tabName. Name of the tab in which this field is present.
     * 		   fieldId. Id of the field.
     * Output: true in case of success else false
     */
    CallBeforeRenderTabCustomMethod: function (tabName) {
        var _const = $ss.agentcore.constants;
        var iterator = this.GetTabWithMatchingName(tabName);
        if (_const.PR_NOT_FOUND != iterator) {
            return this._preferenceTabInfoColl[iterator].CallBeforeRenderFunction();
        }
        return false;
    },

    /*
     * This function will call the function that needs to be called after this tab has been  rendered.
     * Input : tabName. Name of the tab in which this field is present.
     * 		   fieldId. Id of the field.
     * Output: true in case of success else false
     */
    CallAfterRenderTabCustomMethod: function (tabName) {
        var _const = $ss.agentcore.constants;
        var iterator = this.GetTabWithMatchingName(tabName);
        if (_const.PR_NOT_FOUND != iterator) {
            return this._preferenceTabInfoColl[iterator].CallAfterRenderFunction();
        }
        return false;
    },

    /*
     * This function will set the state of the current tab as selected.
     * Input : tabName. Name of the tab in which this field is present.
     * 		   fieldId. Id of the field.
     * Output: true in case of success else false
     */
    SelectTab: function (tabName) {
        var _const = $ss.agentcore.constants;

        if (_const.PR_NOT_FOUND != this._currentSelectedTabIterator) {
            this._preferenceTabInfoColl[this._currentSelectedTabIterator].SetSelected(false);
        }

        this._currentSelectedTabIterator = this.GetTabWithMatchingName(tabName);
        if (_const.PR_NOT_FOUND != this._currentSelectedTabIterator) {
            this._preferenceTabInfoColl[this._currentSelectedTabIterator].SetSelected(true);
            return true;
        }
        return false;
    },

    /*
     * This function will all the values present in filedinfo.
     * Input : None.
     * Output: an array of the form id PR_DELIMITER value, else null
     */
    GetFieldInfoData: function (tabName) {
        var _const = $ss.agentcore.constants;
        var iterator = this.GetTabWithMatchingName(tabName);

        if (_const.PR_NOT_FOUND != iterator) {
            return this._preferenceTabInfoColl[iterator].GetFieldInfoData();
        }

        return null;
    },

    /*
     * This function will get all the ids present in filedinfo.
     * Input : None.
     * Output: an array of the form id PR_DELIMITER value, else null
     */
    GetFieldIds: function (tabName) {
        var _const = $ss.agentcore.constants;
        var iterator = this.GetTabWithMatchingName(tabName);

        if (_const.PR_NOT_FOUND != iterator) {
            return this._preferenceTabInfoColl[iterator].GetFieldIds();
        }

        return null;
    },

    /*
     * This function will get the id of the first that that needs to be displayed.
     * Input : None.
     * Output: tab id or null
     */
    GetFirstTabId: function () {
        return this._preferenceTabInfoColl[0].GetName();
    },

    /*
     * This function will get the view page for this tab.
     * Input : None.
     * Output: tab id or null
     */
    GetViewPageForTab: function (tabName) {
        var _const = $ss.agentcore.constants;
        var iterator = this.GetTabWithMatchingName(tabName);

        if (_const.PR_NOT_FOUND != iterator) {
            return this._preferenceTabInfoColl[iterator].GetViewPage();
        }

        return null;
    },

    GetSnapinNameForTab: function (tabName) {
        var _const = $ss.agentcore.constants;
        var iterator = this.GetTabWithMatchingName(tabName);

        if (_const.PR_NOT_FOUND != iterator) {
            return this._preferenceTabInfoColl[iterator].GetSnapinNameForTab();
        }

        return null;
    },

    /*
	 * This function will return an array which will contain the information required
	 * by the html to render the screen
	 */
    GetPreferenceFieldTabInfo: function () {
        var _const = $ss.agentcore.constants;
        var infoColl = null;
        var tabInfo = null;

        try {
            infoColl = [];

            for (var iterator = 0; iterator < this._preferenceTabInfoColl.length; iterator++) {
                tabInfo = {
                    "DisplayText": this._preferenceTabInfoColl[iterator]._tabDisplayText,
                    "Name": this._preferenceTabInfoColl[iterator]._name,
                    "SnapinName": this._preferenceTabInfoColl[iterator]._snapin_name,
                    "Selected": this._preferenceTabInfoColl[iterator]._selected,
                    "Image": this._preferenceTabInfoColl[iterator]._tabimage,
                    "ViewPage": this._preferenceTabInfoColl[iterator]._viewpage
                };

                infoColl.push(tabInfo);
            }
        }
        catch (ex) {
            return infoColl = null;
        }

        return infoColl;
    },

    CallBeforeSaveFunction: function (tabName, dataToSave) {
        var _const = $ss.agentcore.constants;
        var iterator = this.GetTabWithMatchingName(tabName);

        if (_const.PR_NOT_FOUND != iterator) {
            return this._preferenceTabInfoColl[iterator].CallBeforeSaveFunction(dataToSave);
        }

        return null;
    },

    CallAfterSaveFunction: function (tabName, dataToSave) {
        var _const = $ss.agentcore.constants;
        var iterator = this.GetTabWithMatchingName(tabName);

        if (_const.PR_NOT_FOUND != iterator) {
            return this._preferenceTabInfoColl[iterator].CallAfterSaveFunction(dataToSave);
        }

        return null;
    }

});

$ss.snapin.preference.model.Multitab = MVC.Model.extend({
    init: function () {
        var _const = $ss.agentcore.constants;

        //this._domForXml = null;

        this._preferenceFieldTab = null;
    },

    GetPreferenceFieldTab: function () {
        //return this._preferenceFieldTab; 
        if (!this._preferenceFieldTab) {
            return null;
        }

        return this._preferenceFieldTab.GetPreferenceFieldTabInfo();
    },

    /*
     This function will load the xml and populate the data structure
     Input : void
     Output: Returns created data structure or null
     */
    LoadDataFromConfig: function () {
        var _const = $ss.agentcore.constants;

        //if the xml has already been loaded and the tab collection is not null, then return the existing object
        //    if ((this._domForXml) && (this._preferenceFieldTab)) {
        //      return true;
        //    }

        if (this._preferenceFieldTab) {
            return true;
        }

        var preferenceNodes = null; //nodes collection for preference
        //Load the section for preference from the config file
        preferenceNodes = this.LoadPreferenceSection();
        if (null == preferenceNodes) {
            return false;
        }

        try {
            this._preferenceFieldTab = new $ss.snapin.preference.model.PreferenceFieldTab()
        }
        catch (e) {
            return false;
        }

        return this._preferenceFieldTab.UpdateFromXML(preferenceNodes);
    },

    /*
     This function will load the config file and load the preference section nodes.
     Input : file name with full path
     Output: Returns loaded nodes or null
     */
    LoadPreferenceSection: function () {

        // load the config file
        //this._domForXml = this.LoadConfigFile(xmlFileName);
        var domForXml = $ss.agentcore.dal.config.GetConfigDOM();
        if (!domForXml) {
            return null;
        }

        // search for the node specific to preference snapins
        var preferenceNodes = this.LoadPrefereneNode(domForXml)
        if (!preferenceNodes) {
            return null;
        }

        return preferenceNodes;
    },

    /*
     This function will load the config file into the memory.
     Input : file name with full path
     Output: Returns loaded dom object or null
     */
    LoadConfigFile: function (xmlFileName) {
        if (!xmlFileName) {
            return null;
        }

        var domForXml = null;
        try {
            domForXml = $ss.agentcore.dal.xml.LoadXML(xmlFileName, false);
        }
        catch (e) {
        }

        return domForXml;
    },


    /*
     This function will load the config file into the memory.
     Input : file name with full path
     Output: Returns loaded dom object or null
     */
    LoadPrefereneNode: function (domForXml) {
        if (!domForXml) {
            return null;
        }
        var preferenceNodes = null;
        var xpath = "//sprt/configuration[@name='clientui']/config-section[@type='preference']"

        try {
            preferenceNodes = $ss.agentcore.dal.xml.GetNodes(xpath, "", domForXml);
        }
        catch (e) {
        }

        return preferenceNodes;
    },

    /*
     This function will save the tabs data into the registry or into the databag
     Input  :
     OutPut : Error object
     */
    Save: function () {
        if (this._preferenceFieldTab) {
            return this._preferenceFieldTab.Save();
        }

        return false;
    },

    /*
     * This function will save the tab based on the tab name
     * Input : Tab name
     * Output: True in case of success, else false
     */
    SaveTabInfo: function (tabName) {
        if (this._preferenceFieldTab) {
            return this._preferenceFieldTab.SaveTabInfo(tabName);
        }

        return false;
    },

    /*
     * This function will return the error object after a save call.
     * Input : none
     * Output: error object in case of error
     */
    GetErrorValue: function () {
        if (this._preferenceFieldTab) {
            return this._preferenceFieldTab.GetErrorValue();
        }

        return null;
    },

    /*
     * This function will update the fieldinfo.
     * Input : tabName. Name of the tab in which this field is present.
     * 		   fieldId. Id of the field.
     * Output: true in case of success else false
     */
    UpdateFieldInfo: function (tabName, fieldId, fieldValue) {
        if (this._preferenceFieldTab) {
            return this._preferenceFieldTab.UpdateFieldInfo(tabName, fieldId, fieldValue);
        }
        return false;
    },

    /*
     * This function will call the function that needs to be called before this tab is rendered.
     * Input : tabName. Name of the tab in which this field is present.
     * 		   fieldId. Id of the field.
     * Output: true in case of success else false
     */
    CallBeforeRenderTabCustomMethod: function (tabName) {
        if (this._preferenceFieldTab) {
            return this._preferenceFieldTab.CallBeforeRenderTabCustomMethod(tabName);
        }
        return false;
    },

    /*
     * This function will call the function that needs to be called after this tab has been  rendered.
     * Input : tabName. Name of the tab in which this field is present.
     * 		   fieldId. Id of the field.
     * Output: true in case of success else false
     */
    CallAfterRenderTabCustomMethod: function (tabName) {
        if (this._preferenceFieldTab) {
            return this._preferenceFieldTab.CallAfterRenderTabCustomMethod(tabName);
        }
        return false;
    },

    /*
     * This function will set the state of the current tab as selected.
     * Input : tabName. Name of the tab in which this field is present.
     * 		   fieldId. Id of the field.
     * Output: true in case of success else false
     */
    SelectTab: function (tabName) {
        if (this._preferenceFieldTab) {
            return this._preferenceFieldTab.SelectTab(tabName);
        }
        return false;
    },

    /*
     * This function will get the fieldinfo.
     * Input : tabName. Name of the tab in which this field is present.
     * 		   fieldId. Id of the field.
     * Output: value in case of success else null
     */
    GetFieldInfo: function (tabName, fieldId, fieldValue) {
        if (this._preferenceFieldTab) {
            return this._preferenceFieldTab.GetFieldInfo(tabName, fieldId, fieldValue);
        }
        return null;
    },

    /*
     * This function will get all the values present in filedinfo.
     * Input : None.
     * Output: an array of the form id PR_DELIMITER value, else null
     */
    GetFieldInfoData: function (tabName) {
        if (this._preferenceFieldTab) {
            return this._preferenceFieldTab.GetFieldInfoData(tabName);
        }
        return null;
    },

    /*
     * This function will get all the ids present in filedinfo.
     * Input : None.
     * Output: an array of the form id PR_DELIMITER value, else null
     */
    GetFieldIds: function (tabName) {
        if (this._preferenceFieldTab) {
            return this._preferenceFieldTab.GetFieldIds(tabName);
        }
        return null;
    },

    /*
     * This function will get the id of the first that that needs to be displayed.
     * Input : None.
     * Output: tab id or null
     */
    GetFirstTabId: function () {
        if (this._preferenceFieldTab) {
            return this._preferenceFieldTab.GetFirstTabId();
        }
        return null;
    },

    /*
     * This function will get the view page for this tab.
     * Input : None.
     * Output: tab id or null
     */
    GetViewPageForTab: function (tabName) {
        if (this._preferenceFieldTab) {
            return this._preferenceFieldTab.GetViewPageForTab(tabName);
        }
        return null;
    },

    GetSnapinNameForTab: function (tabName) {
        if (this._preferenceFieldTab) {
            return this._preferenceFieldTab.GetSnapinNameForTab(tabName);
        }
        return null;
    },

    CallBeforeSaveFunction: function (tabName, dataToSave) {
        if (this._preferenceFieldTab) {
            return this._preferenceFieldTab.CallBeforeSaveFunction(tabName, dataToSave);
        }
        return null;
    },

    CallAftereSaveFunction: function (tabName, dataToSave) {
        if (this._preferenceFieldTab) {
            return this._preferenceFieldTab.CallAfterSaveFunction(tabName, dataToSave);
        }
        return null;
    }
});

var bMac = $ss.agentcore.utils.IsRunningInMacMachine();
