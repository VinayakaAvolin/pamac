/**
 * @author Dinesh Venugopalan
 */
$ss.snapin = $ss.snapin ||
{};

$ss.snapin.preference = $ss.snapin.preference ||
{};

$ss.snapin.preference.helper = $ss.snapin.preference.helper ||
{};

(function(){
  $.extend($ss.snapin.preference.helper, {
    RenderPreferenceDetail: function(oObj, sSnapinName, sPagePath){
      var sSnapinPath = $ss.getSnapinAbsolutePath(sSnapinName);
      try {
        return oObj.render({
          partial: sSnapinPath + sPagePath,
          absolute_url: sSnapinPath + sPagePath,
          cache: false
        });
      }
      catch (ex) {
        return false;
      }
    }
  });
})();


//Stating the helper files for "general" preference
$ss.snapin.preference.general = $ss.snapin.preference.general ||
{};

(function() {
  $.extend($ss.snapin.preference.general, {
    BeforeSave: function(dataToSave) {
      $("#savingwait").show();
      var bReturn = true;

      for (var i = 0; i < dataToSave.length; i++) {
        /*
        if ("chk_startup" === dataToSave[i].name) {
        var key = $ss.agentcore.dal.config.GetContextValue("SdcContext:ProviderId");
        var value = '"' + $ss.agentcore.dal.config.GetProviderBinPath() + 'sprtcmd.exe"' + ' /P ' + $ss.agentcore.dal.config.GetContextValue('SdcContext:ProviderId');

        if (dataToSave[i].value) {
        bReturn = $ss.agentcore.dal.registry.SetRegValueByType("HKEY_LOCAL_MACHINE", "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", key, 1, value);
        }
        else {
        bReturn = $ss.agentcore.dal.registry.DeleteRegVal("HKEY_LOCAL_MACHINE", "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", key);
        }

        if (!bReturn) {
        return bReturn;
        }
        }
        */

        if ("chk_welcome" === dataToSave[i].name) {
          var regRoot = $ss.agentcore.dal.config.GetRegRoot();
          var user = $ss.agentcore.dal.config.ExpandSysMacro("%USER%");
          var path = null;
          var firstRunValue = "false";
          if (dataToSave[i].value) {
            firstRunValue = "true";
          }

          if (regRoot) {
            path = regRoot + "\\users\\" + user + "\\ss_config\\firstrun\\";
            bReturn = $ss.agentcore.dal.registry.SetRegValueByType("HKCU", path, "FirstRunCompleted", $ss.agentcore.constants.REG_SZ, firstRunValue);
            var pathHKLM = regRoot + "\\ss_config\\firstrun\\";
            bReturn = $ss.agentcore.dal.registry.SetRegValueByType("HKLM", pathHKLM, "FirstRunHasBeenSeen", $ss.agentcore.constants.REG_SZ, firstRunValue);
          }

          if (!bReturn) {
            return bReturn;
          }
        }

        if ("chk_trigger" === dataToSave[i].name) {
          var regRoot = $ss.agentcore.dal.config.GetRegRoot();
          var user = $ss.agentcore.dal.config.ExpandSysMacro("%USER%");
          var path = null;
          var triggerSelectAll = "false";
          if (dataToSave[i].value) {
            triggerSelectAll = "true";
          }

          if (regRoot) {
            path = regRoot + "\\users\\" + user + "\\ss_config\\general\\";
            bReturn = $ss.agentcore.dal.registry.SetRegValueByType("HKCU", path, "TriggerSelectAll", $ss.agentcore.constants.REG_SZ, triggerSelectAll);
          }

          if (!bReturn) {
            return bReturn;
          }
        }
      }
      if ($('.clsTriggers').length > 0) {
        $('.clsTriggers').each(function() {
          $ss.snapin.preference.general.PersistTrigger(this);
        });
        this.RestartAgent();
      }
      $("#savingwait").hide();
      return true;
    },

    AfterRender: function() {
      var regRoot = $ss.agentcore.dal.config.GetRegRoot();
      var user = $ss.agentcore.dal.config.ExpandSysMacro("%USER%");
      var path = null;

      if (regRoot) {
        path = regRoot + "\\users\\" + user + "\\ss_config\\general\\";
        var triggerSelectAll = $ss.agentcore.dal.registry.GetRegValue("HKCU", path, "TriggerSelectAll");
        triggerSelectAll = triggerSelectAll || "true";
        if (triggerSelectAll === "true") {
          $('#chk_trigger').attr("checked", "checked");
        }
        else {
          $('#chk_trigger').removeAttr("checked");
        }
      }

      if ($('.clsTriggers').length > 0) {
        $('.clsTriggers').each(
              function() {
                $ss.snapin.preference.general.ReadTriggerData(this);
              }
          );
      }

      //check the value under firstrun for FirstRunCompleted and FirstRunhasbeenseen, if they are
      //true then set general as true and update the ui
      try {
        if (regRoot) {
          var bForceFirstRun = ($ss.agentcore.dal.config.GetConfigValue("firstrun", "force_first_run", "true") === "true");
          var pathFirstRun = regRoot + "\\users\\" + user + "\\ss_config\\firstrun\\";
          var pathHKLM = regRoot + "\\ss_config\\firstrun\\";
          var firstRunCompleted_fr = $ss.agentcore.dal.registry.GetRegValue("HKCU", pathFirstRun, "FirstRunCompleted");
          var firstRunHasBeenSeen_fr = $ss.agentcore.dal.registry.GetRegValue("HKLM", pathHKLM, "FirstRunHasBeenSeen");

          if (("true" === firstRunCompleted_fr) && ("true" === firstRunHasBeenSeen_fr)) {
            $ss.agentcore.dal.registry.SetRegValueByType("HKCU", path, "FirstRunCompleted", $ss.agentcore.constants.REG_SZ, "True");
            $('#chk_welcome').attr("checked", "checked");
          }
          if(bForceFirstRun == false){
        $('#general_userexperiance').hide();
      }

        }
      }
      catch (ex) {
      }
    },

    ReadTriggerData: function(item) {
      var _config = $ss.agentcore.dal.config;
      var _registry = $ss.agentcore.dal.registry;
      var _constants = $ss.agentcore.constants;

      var pathTriggerPersist = "SOFTWARE\\SupportSoft\\ProviderList\\" +
                                _config.GetContextValue("SdcContext:ProviderId") +
                                "\\users\\" +
                                _config.GetContextValue('SdcContext:UserName') +
                                "\\SupportTriggers\\Persistent";

      pathTriggerPersist += "\\" + item.id;

      var disabled = _registry.GetRegValue("HKCU", pathTriggerPersist, "Disabled");
      disabled = disabled || "0";
      item.checked = disabled === "0" ? true : false;
    },

    PersistTrigger: function(item) {
      var _config = $ss.agentcore.dal.config;
      var _registry = $ss.agentcore.dal.registry;
      var _constants = $ss.agentcore.constants;

      var pathTriggerPersist = "SOFTWARE\\SupportSoft\\ProviderList\\" +
      _config.GetContextValue("SdcContext:ProviderId") +
      "\\users\\" +
      _config.GetContextValue('SdcContext:UserName') +
      "\\SupportTriggers\\Persistent";

      pathTriggerPersist += "\\" + item.id;

      return _registry.SetRegValueByType("HKCU", pathTriggerPersist, "Disabled", _constants.REG_DWORD, item.checked ? 0 : 1);

    },

  //Display Sync details from the registry
  CreateSyncInfoHTML: function() {
    var str;
    str = $ss.snapin.preference.general.GetSyncInfoFromRegistry();
    return '<div>' + str + '</div>';
    },
  
  GetSyncInfoFromRegistry: function() {
            var sRegRoot, sRegKey, sRegVal, providerID, prodName, user, val, message;
            var displayname, registryname;
            try {
                sRegRoot = "HKCU";
                providerID = $ss.agentcore.dal.config.GetProviderID();
                prodName = $ss.agentcore.dal.config.ParseMacros("%PRODUCT%");
                user = $ss.agentcore.dal.config.ExpandSysMacro("%USER%");
                sRegKey = "Software\\SupportSoft\\ProviderList\\" + providerID + "\\users\\" + user + "\\";
                
                //Sync - Update Info
                displayname = $ss.snapin.preference.general.GetArray("syncdata","download_disp_name");
                registryname = $ss.snapin.preference.general.GetArray("syncdata","download_reg_name");
                message = "<table>"
                message += "<tr> <td colspan='2'> <b> <u> " + _utils.LocalXLate("snapin_preference","sync_dwnlStatus") + " </u> </b> </td> </tr>";                
                for (var i=0; i<registryname.length; i++) {
                    val = _registry.GetRegValue(sRegRoot, sRegKey, registryname[i]);
                    message += "<tr> <td style='padding-right: 15px;'>" + _utils.LocalXLate("snapin_preference",displayname[i]) + "</td> <td>" + val + "</td> </tr>";
                }
                message += "<tr><td> &nbsp; </td></tr>";
                 
                //Sync - Upload Info                
                displayname = $ss.snapin.preference.general.GetArray("syncdata","upload_disp_name");
                registryname = $ss.snapin.preference.general.GetArray("syncdata","upload_reg_name");
                message += "<tr> <td colspan='2'> <b> <u> " + _utils.LocalXLate("snapin_preference","sync_uploadStatus") + " </u> </b> </td> </tr>";                
                for (var i=0; i<registryname.length; i++) {
                    val = _registry.GetRegValue(sRegRoot, sRegKey, registryname[i]);
                    message += "<tr> <td style='padding-right: 15px;'>" + _utils.LocalXLate("snapin_preference",displayname[i]) + "</td> <td>" + val + "</td> </tr>";
                }
                message += "<tr><td> &nbsp; </td></tr>";

                //Sync - Other Info
                displayname = $ss.snapin.preference.general.GetArray("syncdata","others_disp_name");
                registryname = $ss.snapin.preference.general.GetArray("syncdata","others_reg_name");
                message += "<tr> <td colspan='2'> <b> <u> " + _utils.LocalXLate("snapin_preference","sync_othData") + " </u> </b> </td> </tr>";                
                for (var i=0; i<registryname.length; i++) {
                    val = _registry.GetRegValue(sRegRoot, sRegKey, registryname[i]);
                    message += "<tr> <td style='padding-right: 15px;'>" + _utils.LocalXLate("snapin_preference",displayname[i]) + "</td> <td>" + val + "</td> </tr>";
                }
                message += "<tr><td> </td></tr>";
                message += "</table>";
                return message;
            }
            catch(ex) {
            }
        },
    
    GetArray: function(section, key) {
      var folders = _config.GetValues(section, key);
      if (folders.length > 0) {
          var folderslist = new Array();
          for (var i = 0; i < folders.length; i++) {
            folderslist[i] = folders[i];
          }
          return folderslist;
      }
    },

    RestartAgent: function() {
      var cnt = 0;
      $ss.agentcore.utils.StartSprtCmd(false);
      while ($ss.agentcore.utils.IsSprtCmdRunning() && cnt <= 10) {
        $ss.agentcore.utils.Sleep(2000);
        cnt++;
        continue;
      }
      $ss.agentcore.utils.StartSprtCmd(true);
    },

    LangCodeToLangLong: function(Lang){
      var translate_code = "cl_ls_lang_code_" + Lang;
      return $ss.agentcore.dal.config.GetConfigValue("language_list", translate_code);
    }
  });
})();

//Stating the helper files for "proxy" preference
$ss.snapin.preference.proxy = $ss.snapin.preference.proxy ||
{};

(function(){
  $.extend($ss.snapin.preference.proxy, {
    BeforeSave: function(dataToSave){
      var userName = null;
      var password = null;

      if (2 != dataToSave.length) {
        return false;
      }

      userName = dataToSave[0].value;
      password = dataToSave[1].value;

      return _mph.TestProxySettings(userName, password);
    },

    TestProxySettings: function(userName, password){
      var objReturn = {
        "RetVal": true,
        "ErrorMessage": ""
      };
      try {
        var bOnline = _mph.BasicAuthConnectionTest("", "");
        if (bOnline) {
          //display to the user that there is no need to connect
          objReturn.RetVal = false;
          objReturn.ErrorMessage = $ss.agentcore.utils.LocalXLate("snapin_preference","PR_PROXY_WORKING");
        }
        else {
          bOnline = _mph.BasicAuthConnectionTest(userName, password);
          if (!bOnline) {
            objReturn.RetVal = false;
            objReturn.ErrorMessage = $ss.agentcore.utils.LocalXLate("snapin_preference","PR_PROXY_REENTER");
          }
          else {
            _mph.SaveCredential(userName, password);
          }
        }
      }
      catch (ex) {
        objReturn.RetVal = false;
        objReturn.ErrorMessage = "Internal Error";
      }

      return objReturn;
    },

    SaveCredential: function(userName, password){
      var encryptName = "";
      var encryptPwd = "";

      encryptName = $ss.agentcore.utils.EncryptStrToHex(userName);

      if (password != "") {
        encryptPwd = $ss.agentcore.utils.EncryptStrToHex(password);
      }

      var sBase = $ss.agentcore.constants.REG_SPRT + "ProviderList\\" + $ss.agentcore.dal.config.GetContextValue("SdcContext:ProviderId");
      $ss.agentcore.dal.registry.SetRegValue($ss.agentcore.constants.REG_TREE, sBase, "proxyusername", encryptName);
      $ss.agentcore.dal.registry.SetRegValue($ss.agentcore.constants.REG_TREE, sBase, "proxypassword", encryptPwd);
    },

    BasicAuthConnectionTest: function(sUserame, sPassword){

      var sHost = _config.ExpandAllMacros("%SERVERBASEURL%");
      //var sHTTPMethod = $ss.agentcore.dal.config.GetConfigValue("getconnect", "HomeRunCheckMethod", "GET");
      var SwapIPForHostname = $ss.agentcore.dal.config.GetConfigValue("getconnect", "SwapDNSInHomeRunURL", "true");
      var bSwapIPForHostname = (SwapIPForHostname.toLowerCase() != "false" && SwapIPForHostname != "1");
      var CheckDnsSpoofing = $ss.agentcore.dal.config.GetConfigValue("getconnect", "HomeRunFailsOnSpoofedDNS", "true");
      var bCheckDnsSpoofing = (CheckDnsSpoofing.toLowerCase() != "false" && CheckDnsSpoofing != "1");
      //var sSocketTestAddress = $ss.agentcore.dal.config.GetConfigValue("getconnect", "SocketConnect", null);
      var nDNSTimeout = $ss.agentcore.dal.config.GetConfigValue("getconnect", "DNSTimeout", 5000);
      var nHTTPTimeout = $ss.agentcore.dal.config.GetConfigValue("getconnect", "HTTPTimeout", 5000);
      var nDNSIterations = $ss.agentcore.dal.config.GetConfigValue("getconnect", "DNSIterations", 2);
      //var nHTTPIterations = $ss.agentcore.dal.config.GetConfigValue("getconnect", "HTTPIterations", 2);
      //var sBrowser = "Default"; // Todo $ss.agentcore.network.inet.INET_BROWSER_DEFAULT;


      // if we're doing any DNS tests, we need to bump up the DNS timeout.  Othwerwise,
      // we need to bump up the HTTP timeout
      if (bSwapIPForHostname || bCheckDnsSpoofing) {
        if (nDNSTimeout < SA_FR_FIRST_NETWORK_HIT_TIMEOUT)
          nDNSTimeout = SA_FR_FIRST_NETWORK_HIT_TIMEOUT;
      }
      else {
        if (nHTTPTimeout < SA_FR_FIRST_NETWORK_HIT_TIMEOUT)
          nHTTPTimeout = SA_FR_FIRST_NETWORK_HIT_TIMEOUT;
      }
      var sHomeRunURL = "";
      var g_bConnectStatus = false;

      var bPerformDNS = bSwapIPForHostname || bCheckDnsSpoofing;
      var sDNSResult = "";
      var bSuccess = true;

      // Perform DNS Lookup
      if (bPerformDNS) {
        sDNSResult = $ss.agentcore.network.inet.DNSLookup(sHost, nDNSTimeout, nDNSIterations);
        if (sDNSResult == "")
          bSuccess = false; // All Iterations Exhausted, DNS Resolution Failed
      }

      if (bSwapIPForHostname && bSuccess) sHomeRunURL = sDNSResult;
      else  sHomeRunURL = sHost;

      var sHTTPLocalFile = "%TEMP%clientui.test";
      sHTTPLocalFile = $ss.agentcore.dal.config.ExpandSysMacro(sHTTPLocalFile);
      var returnCode;
      //g_Logger.Debug("cl_pre_BasicAuthConnectionTest()", "ss_con_HttpRequestEx HTTP Test with username " + sUserame + " password " + sPassword);
      returnCode = $ss.agentcore.dal.http.Authenticate(sHomeRunURL, sHTTPLocalFile, nHTTPTimeout, "", //username for web site
 "", //password for web site
 sUserame, sPassword);

      //Todo remove the hardcoding $ss.agentcore.network.inet.HTTP_STATUS_OK
      if (returnCode == 200)
        g_bConnectStatus = true;

      //Todo change the hard coding SS_INT_DB_CONNTEST_RESULT ,
      $ss.agentcore.dal.databag.SetValue("INT_DB_CONNTEST_RESULT", returnCode);

      return g_bConnectStatus;
    }
  });

  //private handlers

  var _mph = $ss.snapin.preference.proxy;
  var SA_FR_FIRST_NETWORK_HIT_TIMEOUT = 45000; // ms
  var HTTP_STATUS_PROXY_AUTH_REQ = 407; // proxy authentication required
})();

$ss.snapin.preference.triggers = $ss.snapin.preference.triggers ||
{};

(function(){
  $.extend($ss.snapin.preference.triggers, {

    GetTriggerAppList: function(){
      //var issueID = $ss.agentcore.diagnostics.smartissue.CreateIssue("WirelessConfig", true, "C:\\Program Files\\SPRTI-QA117\\agent\\common\\smartissue\\default.xml", true, false, null);
      var triggers = $ss.agentcore.dal.content.GetContentsByAnyType(["sprt_trigger"]);

      var triggerPersistentPrefs = "SOFTWARE\\SupportSoft\\ProviderList\\" +
      _config.GetContextValue("SdcContext:ProviderId") +
      "\\users\\" +
      _config.GetContextValue("SdcContext:UserName") +
      "\\SupportTriggers\\Persistent";


      var appList = {};

      for (var id in triggers) {
        //var contTitle = triggers[id].title;
        var contGuid = triggers[id].cid;
        var contVer = triggers[id].version;
        var contExe = _GetTriggerPath("sprt_trigger", contGuid, contVer);

        if (!contExe)
          continue;

        var bAppDisabled = 0;
        bAppDisabled = _registry.GetRegValue("HKCU", triggerPersistentPrefs + "\\" + contExe, "Disabled");
        appList[contExe] = bAppDisabled;
      }

      return appList;
    }
  });

  var _config = $ss.agentcore.dal.config;
  var _registry = $ss.agentcore.dal.registry;

  function _GetTriggerPath(contType, contGuid, contVer){
    var contentXML = $ss.agentcore.dal.content.GetContentDetailsAsXML(contType, contGuid, contVer);

    var dom = $ss.agentcore.dal.xml.LoadXML(contentXML);

    var node = $ss.agentcore.dal.xml.GetNode("//sprt/content-set/content/field-set/field[@name='Path']/value[@name='sccf_field_value_char']", "", dom);

    if (node) {
      return node.text;
    }

    return null;
  }

})()
