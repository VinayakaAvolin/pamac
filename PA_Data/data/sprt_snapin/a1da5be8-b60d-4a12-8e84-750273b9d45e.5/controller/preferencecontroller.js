var prefController = typeof(SnapinPreferenceController) !== "undefined" ? SnapinPreferenceController : SnapinBaseController;

SnapinPreferenceController = prefController.extend('snapin_preference', {
  tabModel: null,
  currentTabId: "id",
  errorObject: {
    id: "",
    message: ""
  },
  errorCount: 0,
  toLocation: null,
  preferenceInfoColl: null,
  prefValidateData: null,
  arrExcludePrefTabs: []
}, {
//Restrict special characters in telephone field-values
    "#countrycode keypress": function (e) {        
        if (event.keyCode < 48 || event.keyCode > 57) {
            event.returnValue = false;
        }
    },
    "#areacode keypress": function (e) {
        if (event.keyCode < 48 || event.keyCode > 57) {
            event.returnValue = false;
        }
    },
    "#phonenumber keypress": function (e) {
        if (event.keyCode < 48 || event.keyCode > 57) {
            event.returnValue = false;
        }
    },
  //When the user clicks in ths save button
  "#snapin_preference_save click": function(params){
    this.SaveTab();
    this.SaveLanguage();
    params.event.kill();
    if (isReloadRequired) window.external.ResetUrl(); //reload bcont
  },

    //When the user clicks on the Refresh button
    "#snapin_preference_refresh click": function () {
        if ($("#snapin_preference_refresh").attr("class") != "brand_button_disabled") {
            $(".scrollarea").css("display", "none");
            $(".waiting").css("display", "block");
            $ss.agentcore.utils.Sleep(1000);
            var sTabName = this.Class.currentTabId;
            this.RenderPreferencePage(sTabName);
            $('#tab_snapin_sync_status .tab-menu-filters').toggleClass('tab-menu-filters-selected', true);//This line is added to Select the "Update Status" menu item in the Header after refresh.
        }
    },


    //When the user clicks on the ForceSync button
    "#snapin_preference_forcesync click": function () {
        $(".waitindicator").css("display", "block");
        $("#snapin_preference_refresh").removeClass("btn btn-primary-ss btn-sm");
        $("#snapin_preference_refresh").addClass("btn btn-primary-ss btn-sm disabled");
        try {
            this.DoForceSync();
            $(".waitindicator").css("display", "none");
            $("#success").css("display", "block");
            $("#snapin_preference_refresh").removeClass("btn btn-primary-ss btn-sm disabled");
            $("#snapin_preference_refresh").addClass("btn btn-primary-ss btn-sm");
        }
        catch (ex) {
        }
    },

    DoForceSync: function () {
        var sRegRoot, sRegKey, sRegVal, providerID, prodName, user, val1, val2, message, sInstallPath;
        try {
            //Delere Registry entries
            sRegRoot = "HKCU";
            providerID = $ss.agentcore.dal.config.GetProviderID();
            prodName = $ss.agentcore.dal.config.ParseMacros("%BRANDNAME%");
            user = $ss.agentcore.dal.config.ExpandSysMacro("%USER%");
            sRegKey = "Software\\SupportSoft\\ProviderList\\" + providerID + "\\users\\" + user + "\\";

            _registry.DeleteRegVal(sRegRoot, sRegKey, "NextUpdateTime");
            _registry.DeleteRegVal(sRegRoot, sRegKey, "NextUpdateTimeHex");

            if (bMac) {
              $ss.agentcore.utils.Sleep(2000);
              var jsBridge = window.JSBridge;
              var message = {'JSClassNameKey':'Sync','JSOperationNameKey':'ForceSync', 'JSArgumentsKey':[], 'JSISSyncMethodKey' : '0'};
              var result = jsBridge.execute(message);
            }else{
            //Restart the sprtcmd.exe
            var eServiceList = new Enumerator(window.external.GetObject("winmgmts:{impersonationLevel=impersonate}!root/cimv2").ExecQuery("Select * From Win32_Process where name='sprtcmd.exe'"));
            sRegRoot = "HKLM";
            sRegKey = "Software\\SupportSoft\\ProviderList\\" + providerID + "\\InstallPaths";
            sInstallPath = $ss.agentcore.dal.registry.GetRegValue(sRegRoot, sRegKey, "ProgramRoot");

            for (; !eServiceList.atEnd(); eServiceList.moveNext()) {
                var strKillCmd = '"' + sInstallPath + '\\bin\\sprtcmd.exe" /p ' + providerID + ' /kill "';
                var strStartCmd = '"' + sInstallPath + '\\bin\\sprtcmd.exe" /p ' + providerID + ' /start "';
                try {
                    var state = $ss.agentcore.utils.RunCommand(strKillCmd, 4);
                    $ss.agentcore.utils.Sleep(2000);
                    if (state == 0) {
                        $ss.agentcore.utils.RunCommand(strStartCmd, 4);
                        $ss.agentcore.utils.Sleep(25000);
                    }
                }
                catch (ex) {
                    alert("Error occured while force syncing. Please try later.");
                }
            }
        }
        }
        catch (ex) {
        }
    },
  
  //when the user clicks on the reset button
  "#snapin_preference_reset click": function(params){
    document.snp_preference_form.reset();
    this.UpdateFormWithInitData(this.Class.currentTabId);
		this.Class.tabModel.CallAfterRenderTabCustomMethod(this.Class.currentTabId);
    params.event.kill();
  },


  //when any link with snapinPrefSelTab as class name is clicked
  ".snapinPrefSelTab click": function(params){
    this.applyDefaultStyle();
    var tabname = params.element.id;
    if (tabname) {
      if (tabname != this.Class.currentTabId) {
        this.RenderPreferencePage(tabname);
      }
    }
    this.applyNewStyle("#" + tabname);
  },

  applyDefaultStyle: function(){
    $('.tab-menu-filters').toggleClass('tab-menu-filters-selected',false);
    $('.tab-menu-filters').toggleClass('tab-menu-filters',true);
  },

  applyNewStyle: function(elemID){
    $(elemID +' .tab-menu-filters').toggleClass('tab-menu-filters-selected',true);
  },

  SaveLanguage: function(){
    var strVal = $("#ss_ls_LangSel").val();
    var providerID = $ss.agentcore.dal.config.GetProviderID();
    var regPath = "SOFTWARE\\SupportSoft\\ProviderList\\"+ providerID +"\\SubAgent\\ss_config\\global\\";
	
    if (strVal) {
      var lastLang = _utils.GetLanguage();
      _registry.SetRegValue("HKLM", regPath, "language", strVal);
      if (lastLang === strVal)  return;
      _config.SetUserValue("global", "language", strVal);
      isReloadRequired = true;
    }
  },

  //This function will save all the controls present inside the tab
  SaveTab: function(){
    $("#snp_preference_form .error").hide();
    $("#snp_preference_form .success").hide();

    $("#snp_preference_form .accountnumber").hide();
    $("#snp_preference_form .username").hide();
    $("#snp_preference_form .firstname").hide();
    $("#snp_preference_form .lastname").hide();
    $("#snp_preference_form .passworddonotmatch").hide();
    var aDataToSave = $("#snp_preference_form").formToArray();
    var isValid = true;
    //get the array of the values
    if (this.Class.prefValidateData) {
      var validateStatus = $("#snp_preference_form").validate(this.Class.prefValidateData);

      validateStatus.settings.highlight = false;
      validateStatus.settings.errorClass = "myerror"

      if ($("#snp_preference_form #password").val() != $("#snp_preference_form #cpassword").val()) {
          $("#snp_preference_form .passworddonotmatch").show();
          return;
      }

      for (var x = 0; x < aDataToSave.length; x++) {
        try {
          if (aDataToSave[x].value == "") {
                  if (aDataToSave[x].name == "accountnumber") {
                      $("#snp_preference_form .accountnumber").show();
                      return;
                  }
                  else if (aDataToSave[x].name == "username") {
                      $("#snp_preference_form .username").show();
                      return;
                  }
                  else if (aDataToSave[x].name == "firstname") {
                      $("#snp_preference_form .firstname").show();
                      return;
                  }
                  else if (aDataToSave[x].name == "lastname") {
                      $("#snp_preference_form .lastname").show();
                      return;
                  }
                  else if (aDataToSave[x].name == "pass") {
                      $("#snp_preference_form .lastname").show();
                      return;
                  }
              }
          if (!validateStatus.element("#" + aDataToSave[x].name)) {
            isValid = false;
            $("#snp_preference_form .error").show();
            $("#snapin_preference .scrollarea").scrollTo(100);
          }
        }
        catch (ex) {
        }
      }

    }

    if (!isValid){
      return false;
    }

    //call before save function
    var objReturn = this.Class.tabModel.CallBeforeSaveFunction(this.Class.currentTabId,aDataToSave);
    if( !objReturn ){
      this.DisplayError($ss.agentcore.utils.LocalXLate("snapin_preference","Error"));
      return false;
    }

    objReturn.RetVal = objReturn.RetVal.RetVal || objReturn.RetVal;
    objReturn.ErrorMessage = objReturn.ErrorMessage.RetVal || objReturn.ErrorMessage;

    if( objReturn.RetVal ){
      for (var x = 0; x < aDataToSave.length; x++) {
        var key = aDataToSave[x].name;
        var value = aDataToSave[x].value;
        var retMsg = this.Class.tabModel.UpdateFieldInfo(this.Class.currentTabId, key, value);
      }

      var status = this.Class.tabModel.SaveTabInfo(this.Class.currentTabId);
      if (status) {
        $("#snp_preference_form .success").show();
				$("#snp_preference_form .scrollarea").scrollTo(100);
      }
      else{
        this.DisplayError($ss.agentcore.utils.LocalXLate("snapin_preference","Error"));
      }

      //call after save info
      var objReturn = this.Class.tabModel.CallAftereSaveFunction(this.Class.currentTabId,aDataToSave);
      if( objReturn ){
        return false;
      }
    }
    else{
      //get the error code and display the error
      this.DisplayError(objReturn.ErrorMessage);
    }
  },

  DisplayError:function(errorMessage){
    $("#snp_preference_form .error").html(errorMessage);
    $("#snp_preference_form .error").show();
  },

  //This function will update the view with the values present in the model
  UpdateFormWithInitData: function(tabName){

    var _const = $ss.agentcore.constants;
    var data = this.Class.tabModel.GetFieldInfoData(tabName);
    if (!data) {
      return false;
    }
    $("#snp_preference_form").autoFill(data);
    return true;
  },

  index: function(params){

	//this.renderSnapinView("snapin_preference", params.toLocation, "\\views\\en\\mytest.html");
	//return ;
  var sTabName;
  try {
      if (!this.Class.tabModel) {
        this.Class.tabModel = new $ss.snapin.preference.model.Multitab();
      }

      if (this.Class.tabModel.LoadDataFromConfig()) {
        this.Class.preferenceInfoColl = this.Class.tabModel.GetPreferenceFieldTab();
		if (!params.requestCategory)
        var sTabName = this.Class.tabModel.GetFirstTabId();
        else
           sTabName = params.requestCategory;
        if (params.toLocation) {
          this.Class.toLocation = params.toLocation;
          this.RenderPreferencePage(sTabName);
        }
        this.applyNewStyle("#" + sTabName);
      }
    }
    catch (ex) {
    }
  },

  RenderPreferencePage: function(sTabName){

    //check the to Location, this is the place where the preference should be rendered.
    try {
      if (!this.Class.toLocation) {
        return false;
      }

      //set the current tab
      this.Class.currentTabId = sTabName;
      //is there preference data retrieved from XML, if not no need to display the page
      if (!this.Class.preferenceInfoColl) {
        return false;
      }

      // identify the selected tab
      for (var x = 0; x < this.Class.preferenceInfoColl.length; x++) {
        if (this.Class.preferenceInfoColl[x].Name == sTabName) {
          this.Class.preferenceInfoColl[x].Selected = true;
        }
        else {
          this.Class.preferenceInfoColl[x].Selected = false;
        }
      }

      var optInput = this.Class.tabModel.CallBeforeRenderTabCustomMethod(sTabName);

      var data = {
        preferenceInfoColl: this.Class.preferenceInfoColl,
        optData: optInput
      }
      this.Class.prefValidateData = null;
      this.renderSnapinView("snapin_preference", this.Class.toLocation, "\\views\\preference_index.html", data);

	//Exclude tabs by fetching from the config entry to exclude "Save" and "Reset" button.
	arrExcludePrefTabs = $ss.agentcore.dal.config.GetValues("snapin_preference", "snp_pref_Tabs_excludeButtons", "");
	for (var k = 0; k < arrExcludePrefTabs.length; k++) {
		if (arrExcludePrefTabs[k] == sTabName) {
			$("#btnTable").css("display", "none");
			break;
		}
	}
      //update the html
      this.UpdateFormWithInitData(sTabName);

      //call after render
      this.Class.tabModel.CallAfterRenderTabCustomMethod(sTabName);
    }
    catch (ex) {
      return false;
    }

    return true;
  },

  "#chk_trigger click" : function(params){
    if(params.element.checked)
      $('.clsTriggers').prop("checked",true);
    else
      $('.clsTriggers').prop("checked",false);
  }
});
var isReloadRequired = false;
var bMac = $ss.agentcore.utils.IsRunningInMacMachine();