/** @namespace Provides Firewall Snapin functionality */
$ss.snapin = $ss.snapin || {};
$ss.snapin.personalfirewall = $ss.snapin.personalfirewall || {};
$ss.snapin.personalfirewall.helper = $ss.snapin.personalfirewall.helper || {};
$ss.snapin.personalfirewall.renderhelper = $ss.snapin.personalfirewall.renderhelper || {};

(function(){

 
  
  $.extend($ss.snapin.personalfirewall.helper, {
 	
	
	/**
	     *  @name getInstalledFirewalls
	     *  @memberOf $ss.snapin.personalfirewall.helper
	     *  @function
	     *  @description Gets a list of  Installed Firewalls from the local system
	     *  @returns array of Installed Firewalls 
	     */  
	getInstalledFirewalls: function() {
	
		var installedFirewalls = [];	// this will be the array returned to the controller

		try
		{
			// get the path to supportedfirewalls.xml
			var spath = 
				$ss.getSnapinAbsolutePath("snapin_personal_firewall") + "\\xml\\supportedfirewalls.xml";
			
			var xml = $ss.agentcore.dal.xml;
			var dom = xml.LoadXML(spath, false, null);

			// use SPRT.SprtFWMgr to get list of INSTALLED Firewalls
			var objActivex = $ss.agentcore.utils;
	        var fwMgr = objActivex.CreateObject("SPRT.SprtFWMgr",false);
			var fwlist = fwMgr.GetALLFWs();
			
			var fwEnum = new Enumerator(fwlist);
			
	        for(; ! fwEnum.atEnd(); fwEnum.moveNext()) 
			{
				var firewall = {};
				firewall.vendor = fwEnum.item().GetProductVendor();
				firewall.id = fwEnum.item().GetProductID();
				firewall.version = fwEnum.item().GetProductVersion();
				firewall.description = fwEnum.item().GetProductDescription();
				// Not all firewall will have this property, i.e. Norton 2009
				try {
				  firewall.enabled = fwEnum.item().IsFirewallEnabled();
				} catch (e) {
				  firewall.enabled = false;
				}

				// check the XML - see if this is a "supported" firewall (i.e.: do we have an instruction page for it)
				// do this by comparing the vendor and description
				var xpath = "//firewall[@vendor='" + firewall.vendor + "'][.='" + firewall.description + "']";
				var nodes = xml.GetNodes(xpath, "", dom.documentElement);
				// if there is a matching node, add it to the installed firewalls list
				if (nodes != null && nodes.length > 0)
				{
					firewall.id = xml.GetAttribute(nodes[0], "id");
					if (xml.GetAttribute(nodes[0], "displayName") != null) {
					  firewall.description = xml.GetAttribute(nodes[0], "displayName");
					}
					installedFirewalls.push(firewall);
				}
			}
		}
		catch(ex)
		{
		
		}
				
		return installedFirewalls;
	
	},
	
	getSupportedFirewalls: function() {
	
		var supportedFirewalls = [];
		
		try
		{
	
			// the current (6.5) firewall snap-in retrieves the list of "supported" firewalls 
			// and returns the last one in the list for each vendor 
			
			var spath = 
				$ss.getSnapinAbsolutePath("snapin_personal_firewall") + "\\xml\\supportedfirewalls.xml";
			
			var xml = $ss.agentcore.dal.xml;
			var dom = xml.LoadXML(spath, false, null);
			var xpath = "//firewall";
			var nodes = xml.GetNodes(xpath, "", dom.documentElement);
			var lastVendor = xml.GetAttribute(nodes[0], "vendor");
			
			
			for (var i=0;i<nodes.length;i++)
			{
				var vendor = xml.GetAttribute(nodes[i], "vendor");
				if (vendor != lastVendor)
				{
					lastVendor = vendor;
					var firewall = {};
					firewall.vendor = xml.GetAttribute(nodes[(i-1)], "vendor");
					firewall.id = xml.GetAttribute(nodes[(i-1)], "id");
					firewall.version = xml.GetAttribute(nodes[(i-1)], "version");
					firewall.description = xml.GetNodeText(nodes[(i-1)], "vendor");
					firewall.enabled = false;
					supportedFirewalls.push (firewall);
				}
			}
			
			
		}
		catch(ex)
		{
		}
		return supportedFirewalls;
	},
	
	fwSelected: function (){
		var selectedValue = "";
		$(":radio").each(function() { if (this.checked) selectedValue = this.value;});
		$ss.agentcore.dal.databag.SetValue("personalfirewall", selectedValue, true);
	},
	
	highlightMenu: function(tagName) {
    $("#frcolumnone .frlinksCK").removeClass('frlinksCK').addClass('frlinks');
    $("#frcolumnone .frlinkslastCK").removeClass('frlinkslastCK').addClass('frlinkslast');    
	  
	  $("#frcolumnone div[@name='" + tagName + "']").removeClass('frlinks');
	  $("#frcolumnone div[@name='" + tagName + "']").addClass('frlinksCK');
	},
	
	ButtonsEnable: function(bEnable) {
	    try {
	      if (bEnable) {
	         $("#nextbutton")[0].src = $("#nextbutton")[0].src.replace("btn_next_disabled.gif","btn_next.gif");
	         $("#backbutton")[0].src = $("#backbutton")[0].src.replace("btn_back_disabled.gif","btn_back.gif");
	         
	         $("#nextbutton")[0].disabled = false;
	         $("#backbutton")[0].disabled = false;
	      } else {
	         $("#nextbutton")[0].src = $("#nextbutton")[0].src.replace("btn_next.gif","btn_next_disabled.gif");
	         $("#backbutton")[0].src = $("#backbutton")[0].src.replace("btn_back.gif","btn_back_disabled.gif");
	         $("#nextbutton")[0].disabled = true;
	         $("#backbutton")[0].disabled = true;
	      }
	    } catch(ex) {}
	}

	
	
        
  });
  


})();



//start of render helper function
(function(){
  $.extend($ss.snapin.personalfirewall.renderhelper, {
    fws_intro: {
      before: function(params, obj){
        $ss.snapin.personalfirewall.helper.highlightMenu("fwintro");
      },
      after: function(params,obj) {
        $("input.nextbutton:last").focus();  
      }
    },
    fws_pick: {
      before: function(params, obj){
        $ss.snapin.personalfirewall.helper.highlightMenu("fwinfo");
      },
	  after: function(params, obj){
                       $ss.snapin.personalfirewall.helper.ButtonsEnable(false);
                       
	               $ss.agentcore.utils.Sleep(0);
			var defaultSelected = false;
			var foundSupportedFirewalls = true;
			var installedFirewalls = $ss.snapin.personalfirewall.helper.getInstalledFirewalls();
			
			// clear the "waiting message"
			$("#cl_pfw_Content").html("");
			
			if (installedFirewalls == null || installedFirewalls.length == 0)
			{
				// we didn't find any installed "supported" firewalls, so get the "default" list from the xml
				installedFirewalls = $ss.snapin.personalfirewall.helper.getSupportedFirewalls();
				foundSupportedFirewalls = false;
				$("#cl_pfw_Description").html($ss.agentcore.utils.LocalXLate("snapin_personal_firewall", "cl_pfw_pick_DescriptionChose"));
				
				
			}
			else
			{
				$("#cl_pfw_Description").html($ss.agentcore.utils.LocalXLate("snapin_personal_firewall", "cl_pfw_pick_DescriptionDetected"));
			}
			
			for (var i=0;i<installedFirewalls.length; i++)
			{
				var id = "fw_" + i;
				var rstring = "<input type=\"radio\" id=\"" + id 
									+ "\" name=\"selectedfirewall\" value=\"" 
									+ installedFirewalls[i].id + "\">" +
									installedFirewalls[i].description + "<br>";
									
				$( "#cl_pfw_Content" ).append(rstring);

				
				if (installedFirewalls[i].enabled && !defaultSelected)
				{
					// if the firewall is enabled, select it (but only one)
					$("#" + id)[0].checked = true;
					defaultSelected = true;
					$ss.agentcore.dal.databag.SetValue("personalfirewall", installedFirewalls[i].id, true);
				}
				
			}
			
				if (!foundSupportedFirewalls)
				{
					// add the "none of these" option
					var rstring = "<input type=\"radio\" id=\"" + "fw_noneofthese" 
										+ "\" name=\"selectedfirewall\" value=\"" 
										+ "noneofthese" + "\">" 
										+ $ss.agentcore.utils.LocalXLate("snapin_personal_firewall", "fw_noneofthese") + "<br>";
					$( "#cl_pfw_Content" ).append(rstring);
				}
			
				// add the "no firewall" option //fw_none
				var rstring = "<input type=\"radio\" name=\"selectedfirewall\" id=\"fw_none\" value=\"none\">" + $ss.agentcore.utils.LocalXLate("snapin_personal_firewall", "fw_none") + "<br>";
				$( "#cl_pfw_Content").append(rstring);
				
				// if no firewalls are already selected, select the "no firewall" option
				if (!defaultSelected)
				{
					$("#fw_none")[0].checked = true;
					$ss.agentcore.dal.databag.SetValue("personalfirewall", "none", true);
				}
			 
				//$(":radio").bind("click", $ss.snapin.personalfirewall.helper.fwSelected);
				$(":radio").bind("click", $ss.snapin.personalfirewall.helper.fwSelected);
				$("#cl_pfw_instruction")[0].style.display = "block";
				$ss.snapin.personalfirewall.helper.ButtonsEnable(true);
				$("input.nextbutton:last").focus();
			 
		return {
          installedFirewalls : installedFirewalls
        };
	  }
    },
    fws_selected: {
      before: function(params, obj){
        $ss.snapin.personalfirewall.helper.highlightMenu("fwinfo");
      },
      after: function(params,obj) {
        $("input.nextbutton:last").focus();  
      }
    },
	
	post_start: {
      before: function(params, obj){
                 
                $ss.snapin.personalfirewall.helper.highlightMenu("sprtcmd");
                $ss.snapin.personalfirewall.helper.ButtonsEnable(false);
                
		//store the selected firewall type in the user config
		$ss.agentcore.dal.config.SetUserValue("firstrun", "personalfirewall", $ss.agentcore.dal.databag.GetValue("personalfirewall"));
		
		//get the path to sprtcmd.exe
		var sprtCmdPath = "\"" + $ss.agentcore.dal.config.GetProviderBinPath() + "sprtcmd.exe\"" + " /P " + $ss.agentcore.dal.config.GetProviderID(); 
		
		$ss.agentcore.utils.system.RunCommand(sprtCmdPath, $ss.agentcore.constants.BCONT_RUNCMD_ASYNC);
		setTimeout(
            function () {
              $ss.snapin.personalfirewall.helper.ButtonsEnable(true);
              var headerHTML = $ss.agentcore.utils.LocalXLate("snapin_personal_firewall", "headerHTML")
              $("#subheader").html(headerHTML);
              var contentHTML = $ss.agentcore.utils.LocalXLate("snapin_personal_firewall", "contentHTML")
              $("#cl_pfw_Content").append(contentHTML);
              $("input.nextbutton:last").focus();
            }
          , 2000);
        return {
          
        };
      }
    }
  
  })
})();

