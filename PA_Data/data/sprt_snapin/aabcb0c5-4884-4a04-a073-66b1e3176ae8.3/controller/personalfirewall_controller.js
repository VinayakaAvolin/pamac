/** @namespace Provides Firewall Snapin functionality */
$ss.snapin = $ss.snapin || {};
$ss.snapin.personalfirewall = $ss.snapin.personalfirewall || {};
$ss.snapin.personalfirewall.helper = $ss.snapin.personalfirewall.helper || {};

SnapinPersonalFirewallController = SteplistBaseController.extend('snapin_personal_firewall', {

	

  InitParam: {
    aNavigationItems: ["#snapin_personal_firewall #nextbutton","#snapin_personal_firewall #backbutton" ],
    aContentPlaceHolderDIV: "snapin_personal_firewall_content",
    sBackToPageURL: "/home",
    sForwardToPageURL: null

  }
  
}, { 

  /**
   *  @function
   *  @description Entry point for this snapin.  Initializes variables and display view.
   *  @param params Optional MVC Params object
   *  @returns none
   */
  index: function(params){

    this.renderSnapinView("snapin_personal_firewall", params.toLocation, "\\views\\sa_layout.htm");
    this._super(params,"snapin_personal_firewall");
    var flowInfo = this.Class.InitiateStep(params);
    if(flowInfo) {
      this.ProcessRender(params,flowInfo);
    } 
  },
  
  ProcessNext: function(params){
    var flowInfo = this._super(this,params);
    if(flowInfo) {
      this.ProcessRender(params,flowInfo);
    }
  },
  ProcessPrevious: function(params){
    var flowInfo = this._super(this,params);
    if(flowInfo) {
      this.ProcessRender(params,flowInfo);
    }
    
  },  
  
  ProcessRender: function(params,flowInfo) {
    var renderHelper = $ss.snapin.personalfirewall.renderhelper || {};
	var fwhelper = $ss.snapin.personalfirewall.helper || {};
    var stepId = flowInfo.stepid;
    var retData = {};
    var opt = {};
    if (stepId) {
      try {
        if(renderHelper[stepId] && renderHelper[stepId].before) {
          retData = renderHelper[stepId].before(params,this);  
        }
        opt.oLocal = retData;
      } catch(ex) {
        //failed to execute the renderhelper function
      }
      
    }
    this.ProcessFlow(this,params,flowInfo,opt);
    // start the post processing
    if (stepId) {
      try {
        var afterStatus = {};
        if(renderHelper[stepId] && renderHelper[stepId].after) {
          afterStatus = renderHelper[stepId].after(params,this);  
        }
        switch (afterStatus.move) {
          case "forward":
            return this.ProcessNext(params);
            break;
          case "back":
            return this.ProcessPrevious(params);
            break;
          default:
            break;
        }
      } catch(ex) {
        //failed to execute the renderhelper after function
      }
    }  
  },
  
  "#nextbutton click": function(params){
     this.ProcessNext(params);
  },
  
  "#backbutton click": function(params){
    this.ProcessPrevious(params);
  }

});
