$ss.snapin = $ss.snapin || {};
$ss.snapin.userinfo = $ss.snapin.userinfo || {};
$ss.snapin.userinfo.helper = $ss.snapin.userinfo.helper || {};

(function() {
  SnapinUserInformationController = SnapinBaseController.extend('snapin_user_information', {
    _registry : $ss.agentcore.dal.registry,
    _config : $ss.agentcore.dal.config,
    userInfoRegPath : "Software\\SupportSoft\\ProviderList\\" + _config.GetProviderID() + "\\" + _config.ParseMacros("%PRODUCT%") + "\\users\\" +  _config.ExpandSysMacro("%USER%") + "\\ss_config\\UserInfo",
    localImagePath : _config.GetContextValue('SdcContext:DirUserServer') + "state\\userinfo\\",
    localImageFullPath : "",
    arrExtensions : [],
    objLatestUserInfo : {
      uLastUserInfoUpload : "", uImage : "", uFullName : "", uLoginID : "", uDomain : "", uPswrdExpiry : "", uOS : "", uDNSName : "", uIPAddress : "", uMachineType : "", uMACId : "", uTotalPhysicalMem : "", uHostName : "", uAMResetURL : ""
    },
    objCachedUserInfo : {
      uLastUserInfoUpload : "", uImage : "", uFullName : "", uLoginID : "", uDomain : "", uIPAddress : "", uPswrdExpiry : "", uOS : "", uDNSName : "", uIPAddress : "", uMachineType : "", uMACId : "", uTotalPhysicalMem : "", uHostName : "", uAMResetURL : ""
    },
    templateLocation : ""
  }, {
    index: function(params){
      if (params.toLocation)  this.Class.templateLocation = params.toLocation;
      if (params.requestArgs)  this.Class.templateReqArgs = params.requestArgs;
      var that = this;
      that.GetUserInfoFromRegistry();
      var reload = params.snpuserinfo_refresh || isStale();
      that.renderSnapinView("snapin_user_information", this.Class.templateLocation, "\\views\\progress.html");

      if (!reload){
        try{
          getImageFullPath();
          that.RenderSnapinView(that.Class.objCachedUserInfo, this.Class.templateLocation);
          that.RenderFileUpload(that);
          return;
        }catch(e){
          logger.error("Invalid local data");
          reload = true;
        }
      }
      
      $(document).oneTime(10000,
                      "UserInfo Data",
                      function() {
                        getData(reload, that,that.Class.templateReqArgs)
                          .done(function (data) {
                            that.SetImageToShow(data);
                            that.RenderSnapinView(data, that.Class.templateLocation);
                            that.SetUserInfoInRegistry();
                            that.RenderFileUpload(that);
                          })
                          .fail(function (reason){
                            //alert(reason);
                          });
                      });
    },

    RenderFileUpload : function(that){
      $(".file-upload").on('change', function(){
        that.DoFileUpload();
      });
    },
    
    RenderSnapinView : function(data, locParams){
      this.renderSnapinView("snapin_user_information", locParams, "\\views\\userinfo.html", {objCachedUserInfo : data});
    },

    SetImageToShow : function(data){
      getImageFullPath();
      data["uImage"] = this.Class.objCachedUserInfo["uImage"];
    },

    ".upload-button click" : function(){
      if (bMac) {
       //   setTimeout(function () {
       //      var message = {'JSClassNameKey':'File','JSOperationNameKey':'ShowBrowseFolderDialog', 'JSCallbackMethodNameKey':"SelectedImagePathWrapper", 'JSISSyncMethodKey' : '0'}
       // _jsBridge.execute(message);
       //  }, 50);
        SelectImage(this);
      }else {
      $(".file-upload").click();
    }
    },

    ".userInfoRefresh click": function(params) {
      params.snpuserinfo_refresh = true;
      this.index(params);
    },
  
    ".userInfoReset click": function (params) {
      var sUrl = $(params.element).attr("subUrl");
      this.renderSnapinView("snapin_account_manager", "#col_account_manager_content", "\\views\\am_iframe.html", { "url": sUrl });
    },
    
    SelectedImagePath: function(result) {
        var parsedJSONObject = JSON.parse(result);
              // logger.info("SelectedImagePath Not able to fetch latest data");
        this.DoFileUpload(parsedJSONObject.Data);
    },
    DoFileUpload: function(inputFilePath){
      // var inputFilePath = document.querySelector('input[type=file]').value;
      var fileName = inputFilePath.replace(/^.*[\\\/]/, '');
      var fso = _file.GetFileSystemObject();
      var obj = this.Class;

      //Get supported File Extentions & check if uploaded image is valid
      this.GetSupportedExtensions();
      if(obj.arrExtensions.indexOf(fileName.toLowerCase().split('.').pop()) <=-1){
        alert("Please upload valid image");
        return;
      }

      //create folder in state folder if it doesn't exists
      if(!fso.FolderExists(obj.localImagePath)) _file.CreateDir(obj.localImagePath);
      obj.localImageFullPath = obj.localImagePath + fileName;

      //Copy user image to state/userinfo folder
      //TODO: Delete older uploaded images in state/userinfo
      if (!fso.FileExists(obj.localImageFullPath)) fso.CopyFile(inputFilePath, obj.localImageFullPath);

      //set user image name to registry
      obj._registry.SetRegValue("HKCU", obj.userInfoRegPath, "uImage", obj.localImageFullPath);

      //set profile picture src
      if (inputFilePath)  $('.profile-pic').attr('src', inputFilePath);
    },

    GetSupportedExtensions: function() {
      var obj = this.Class;
      if (obj.arrExtensions && obj.arrExtensions.length > 0) {
        return obj.arrExtensions;
      }
      obj.arrExtensions = obj._config.GetValues("snapin_user_information", "supportedExtensions", "");
      return obj.arrExtensions;
    },

    GetUserInfoFromRegistry : function() {
      var obj = this.Class.objCachedUserInfo;
      for (var key in obj){
        if(obj[key] != null)  obj[key] = this.Class._registry.GetRegValue("HKCU", this.Class.userInfoRegPath, key);
      }
    },

    SetUserInfoInRegistry : function() {
      var obj = this.Class.objLatestUserInfo;
      for (var key in obj){
        if(obj[key] != null)  this.Class._registry.SetRegValue("HKCU", this.Class.userInfoRegPath, key, obj[key]);
      }
    },
    RefreshUI: function() {
      this.RenderSnapinView(this.Class.objLatestUserInfo, this.Class.templateLocation);
      this.SetUserInfoInRegistry();
    }
  });
})();

  var _file = $ss.agentcore.dal.file;
  var _utils = $ss.agentcore.utils;
  var _helper = $ss.snapin.userinfo.helper;

  var _data_load_freq = parseInt(_config.GetConfigValue("snapin_user_information", "frequency_for_load_days"));

  var cObjCachedUserInfo = SnapinUserInformationController.objCachedUserInfo;
  var cobjLatestUserInfo = SnapinUserInformationController.objLatestUserInfo;
  var bMac = $ss.agentcore.utils.IsRunningInMacMachine();
  var _jsBridge = window.JSBridge;
var thisObj = null;
function SelectImage(scope) {
  thisObj = scope;
var message = {'JSClassNameKey':'File','JSOperationNameKey':'ShowBrowseFolderDialog', 'JSCallbackMethodNameKey':"SelectedImagePathWrapper", 'JSISSyncMethodKey' : '0'}
       _jsBridge.execute(message);
}
 function SelectedImagePathWrapper(result) {
  if (thisObj) {
        thisObj.SelectedImagePath(result);
  }else{
   var aa = new SnapinUserInformationController();
        aa.SelectedImagePath(result);
  }
}
  function getData(reload, scope, reqParams){
    var $def = $.Deferred();
    var fetchFromServer = reload === true;
    var contentData;
    try {
      if(!bMac){
      _helper.GetUserLogDetails(scope);
      _helper.GetMachineTypeDetails(scope);
    }
      _helper.GetUserImg(scope);
      _helper.LoadDefaultSysInfo(scope);
      cobjLatestUserInfo["uLastUserInfoUpload"] = new Date().toUTCString();
      contentData = cobjLatestUserInfo;
      $def.resolve(contentData);
    }catch(e){
      logger.error("Not able to fetch latest data");
      contentData = cObjCachedUserInfo; //show cached data
    }
    return $def.promise();
  }

  function getImageFullPath(){
    var defaultImg = $ss.getSnapinAbsolutePath("snapin_user_information") + "\\skins\\default\\images\\default.png";
    if (cObjCachedUserInfo["uImage"].indexOf("default.png") > 0 || cObjCachedUserInfo["uImage"].length == 0) cObjCachedUserInfo["uImage"] = defaultImg;
  }

  //Refresh only once in a day & show the cached value
  function isStale() {
    try {
      if (_data_load_freq == 0) return true;
      var currTime = new Date().toUTCString();
      var lastUploadTime = cObjCachedUserInfo["uLastUserInfoUpload"];
      if(lastUploadTime){
        var diff = (new Date(Date.parse(currTime)) - new Date(Date.parse(lastUploadTime))) / (1000 * 24 * 3600);
        return (diff >= _data_load_freq);
      }
    } catch (e) {
      logger.error('failed to detemine state from local file [' + sProblemSolvedFilePath + '].Reason=' + e.message);
    }
    return true;
  }