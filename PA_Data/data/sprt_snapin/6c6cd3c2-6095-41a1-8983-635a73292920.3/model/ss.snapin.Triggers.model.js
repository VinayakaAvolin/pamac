 
$ss.snapin = $ss.snapin || {}; 
$ss.snapin.triggers = $ss.snapin.triggers || {};
$ss.snapin.triggers.model = $ss.snapin.triggers.model || {};

(function()
{

  $ss.snapin.triggers.model.Trigger = Class.extend(
  {
    _triggerType : null,
    _view : null,
    _cbDiagnose : null,
    _cbIgnore : null,
    _cbSavePreferences : null,
    _cbBeforeRender : null,
    _cbAfterRender : null,
    _timeIgnore : 0
  },
  {
    init: function(triggerType)
    {
      if(triggerType)
      {
        this.Class._triggerType = triggerType.toString().toLowerCase();
        
        this.ReadTriggerData();
      }    
    },
    
    GetResource:function(idRes)
    {
      return $ss.agentcore.utils.LocalXLate("snapin_trigger",idRes);
    },
    
    GetTriggerType:function()
    {
      return this.Class._triggerType;
    },
    
    ReadTriggerData:function()
    {
      var objConfig = $ss.agentcore.dal.config;
      this.Class._view = objConfig.GetConfigValue("snapin_trigger_" + this.Class._triggerType,"LandingPage","");
      this.Class._timeIgnore = objConfig.GetConfigValue("snapin_trigger_" + this.Class._triggerType,"UITimeOut","0");
      this.GetCallbacks();
    },
    
    GetIgnoreTime:function()
    {
      return this.Class._timeIgnore;
    },
    
    GetView:function()
    {
      return this.Class._view;
    },
    
    GetCallbacks : function()
    {
      var objConfig = $ss.agentcore.dal.config;
      this.Class._cbDiagnose = objConfig.GetAttribute("snapin_trigger_" + this.Class._triggerType,"Callbacks","cbDiagnose","");
      this.Class._cbIgnore = objConfig.GetAttribute("snapin_trigger_" + this.Class._triggerType,"Callbacks","cbIgnore","");
      this.Class._cbSavePreferences = objConfig.GetAttribute("snapin_trigger_" + this.Class._triggerType,"Callbacks","cbSavePreferences","");
      this.Class._cbBeforeRender = objConfig.GetAttribute("snapin_trigger_" + this.Class._triggerType,"Callbacks","cbBeforeRender","");
      this.Class._cbAfterRender = objConfig.GetAttribute("snapin_trigger_" + this.Class._triggerType,"Callbacks","cbAfterRender","");
    },
    
    Diagnose:function()
    {
      eval(this.Class._cbDiagnose + "()");        
    },
    
    Ignore:function()
    {
      eval(this.Class._cbIgnore + "()");        
    },
    
    SavePreferences:function(disableTime)
    {
      return eval(this.Class._cbSavePreferences + "(" + disableTime + ")");        
    },
    
    BeforeRender:function()
    {
      eval(this.Class._cbBeforeRender + "()");        
    },
    
    AfterRender:function()
    {
      eval(this.Class._cbAfterRender + "()");        
    },
    
    /****************************************************************************************
    ** Name:        ss_rl_LogTriggerInfo()
    **
    ** Purpose:     This function will log the usage of a trigger 
    **
    ** Parameter:   none
    **
    ** Return:      none
    *****************************************************************************************/
    LogTriggerReportInfo:function()
    {
      var sCmdLine = $ss.agentcore.utils.activex.GetObjInstance().GetCmdLine().toString();
      var sEntry = ($ss.agentcore.utils.GetObjectFromCmdLine())["entry"]
      var triggers = $ss.agentcore.dal.content.GetContentsByAnyType(["sprt_trigger"]);

      var sContGuid = '';
      var sContVersion = '0';
      var sContTitle = '';
      
      if (sEntry)
      {
        for( var i = 0;triggers && i < triggers.length;i++)
        {
          sContGuid = triggers[i].cid;
          sContVersion = triggers[i].version;
          sContTitle = triggers[i].title;
          
          var oContentDom = $ss.agentcore.dal.content.GetContentDetailsAsXML("sprt_trigger", sContGuid, sContVersion);
          var sXPath = "//field-set/field";
          var _xml = $ss.agentcore.dal.xml;
          var fields = _xml.GetNodes(sXPath, "", oContentDom);
          
          for( var n = 0;n < fields.length;n++)
          {
            var sFieldValue = ""
            var valueNode = _xml.GetNode("value[@name='sccf_field_value_char']", "", fields[n]);
            if(valueNode)
            {
              sFieldValue = valueNode.nodeTypedValue;
              sFieldValue = sFieldValue.toString().toLowerCase();
            }
                
            if( sFieldValue.indexOf(sEntry.toLowerCase())>-1 )
            {
              var options = 
              {
               sContGuid : sContGuid,
               iVersion : sContVersion,
               sTitle : sContTitle,
               sCommandLine : sCmdLine,
               snapinName : "snapin_trigger"
              }
              $ss.agentcore.reporting.Reports.LogTriggerStart(options);
              break;
            }
          }
        }
      }
    }
    
  });
  
})()