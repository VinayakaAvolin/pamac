SnapinTriggerController = SnapinBaseController.extend('snapin_trigger',
{
    _model : null,
        
    UpdateIgnoreButton : function(ignoreTime)
    {
      if($('.closecountdown').length == 0)
      {
        return;
      }
      var ignoreText = $('.closecountdown').html();
      var counter = ignoreTime;
      var prevTime = new Date().getTime();
      
      var startTime=new Date();
      var duration = ignoreTime;
      var remTime = ignoreTime;
      $(document).everyTime(1000, "triggerwindow_closecountdown", function() {
          if($("#triggerwindow .closecountdown").length == 0) {
            $(document).stopTime("triggerwindow_closecountdown"); 
            return;
          }
          if(remTime > 0) {
            //var curTime = new Date().getTime();
           // while((curTime - prevTime) < 1000)
	         // {
	        //    curTime = new Date().getTime();
	        //  }
	        //  prevTime = curTime;
	          remTime = $ss.agentcore.utils.ui.GetRemainingTime(startTime,duration);       
            $("#triggerwindow .closecountdown").html(ignoreText + " (" + remTime.toString() + ")");
            counter--;
	         // $ss.agentcore.utils.Sleep(0);
	          
          } else {
            $ss.agentcore.utils.CloseBcont();
          }
        },
        ignoreTime + 1,true);
    },
    
    DisplayBusyMsg:function()
    {
      $('#divDiag').css("display","none");
      $('#divBusy').css("display","block");
    }
},
{
  "#btnDiagnose click": function(params) {
    this.Class._model.Diagnose();
  },

  ".clsIgnore click": function(params) {
    $ss.agentcore.utils.CloseBcont();
  },

  "#btnSave click": function(params) {
//alert("#btnSave click, didn't seem to close the mini bcont after fix");
    var disableTime = $('#selectDisableNotifications').val(); //Get the preferences data
    var retVal = this.Class._model.SavePreferences(disableTime);

    var sDisplayMsgId = (retVal) ? "statusSaveSuccess" : "statusSaveFail";
    var sDisplayMsg = $ss.agentcore.utils.LocalXLate("snapin_trigger", sDisplayMsgId);
    $('#divSaveMsg').html(sDisplayMsg); //this div doesn't exists in View
  },
  
  GetTriggerType: function() {
    return this.Class._model.GetTriggerType();
  },

  index:function(params)
  {
    try
    {
      //check if first run flow is enabled and has been seen, if not do not show the trigger
      var bForceFirstRun = ($ss.agentcore.dal.config.GetConfigValue("firstrun", "force_first_run", "true") === "true");
      if (bForceFirstRun) {
        var firstRunHasBeenSeen = ($ss.agentcore.dal.config.GetConfigValue("firstrun", "FirstRunHasBeenSeen") === "true");
        if(!firstRunHasBeenSeen) {
          $ss.agentcore.utils.CloseBcont();
          return;
        }
      }

    var typeTrigger = $ss.agentcore.utils.GetQSObjectFromURL(params.requesturl).type;

    this.Class._model = new $ss.snapin.triggers.model.Trigger(typeTrigger);
    if(this.Class._model)
    {
      // Call the before load function(if any)
      this.Class._model.BeforeRender();
        
      if(params.toLocation) {
      // render view
      
        this.renderSnapinView("snapin_trigger",params.toLocation,"\\" + this.Class._model.GetView());
      }
      // Call the After load function(if any)
      this.Class._model.AfterRender();
      
      this.Class._model.LogTriggerReportInfo();
//          ss_evt_Send({name: SS_EVT_CACHEDLOG_FLUSH, bStopCaching: true});
      }
    }
    catch(ex)
    {
      $ss.agentcore.utils.CloseBcont();
    }
  },
  
  GetResource:function(idRes)
  {
    if(this.Class._model)
    {
      return this.Class._model.GetResource(idRes);
    }
    return "";
  }
  
  
});

