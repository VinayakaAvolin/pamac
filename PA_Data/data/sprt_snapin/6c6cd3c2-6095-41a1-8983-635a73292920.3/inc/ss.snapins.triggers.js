$ss.snapin = $ss.snapin || {};
$ss.snapin.triggers = $ss.snapin.triggers || {};
$ss.snapin.triggers.firefox = $ss.snapin.triggers.firefox || {};
$ss.snapin.triggers.DialUp = $ss.snapin.triggers.DialUp || {};
$ss.snapin.triggers.OutlookExpress = $ss.snapin.triggers.OutlookExpress || {};

(function()
{
  $.extend($ss.snapin.triggers,
  {
    GetErrors:function(fnAcctProvider) 
    {
      var providerId = _config.GetContextValue('SdcContext:ProviderId');
      var username = _config.GetContextValue("SdcContext:UserName");
      var shell = _objContainer.CreateInternalObject("shell");
      var contextId = shell.ExpandEnvironmentStrings("%SPRT_CONTEXT_ID%");
      var accounts = $ss.snapin.email.utility.GetAllEmailAccounts(true);
      var bRet = false;
      var regKey = "SOFTWARE\\SupportSoft\\ProviderList\\" + providerId + "\\users\\" +
                        username + "\\SupportTriggers\\State\\" + contextId;
      var errCnt = _registry.GetRegValue("HKCU", regKey, "Count");
      _registry.SetRegValueByType("HKCU", regKey, "Count", $ss.agentcore.constants.REG_DWORD, 0);
      //
      // Index the accounts which have problems by finding 'account name' in each error string.
      //
      var hCurrentProblemList = {};
      var bWLMInstalled = $ss.snapin.email.utility.IsWinLiveMailInstalled()
      //
      // Since each account can be listed more than once we use a hash to index them by name.
      // This avoids having to "keep track" of which accounts have been diagnosed within fix_email.
      //
      for (var iCount = 1; iCount <= errCnt; iCount++) {
        var errStr = _registry.GetRegValue("HKCU", regKey, "" + iCount);
        _registry.SetRegValue("HKCU", regKey, "" + iCount, "");
        for (var i = 0; i < accounts.length; i++) {
          if (errStr.toLowerCase().indexOf(accounts[i].Name.toLowerCase()) != -1) {
            // outlook 2000 format
            bRet = true;
            hCurrentProblemList[i] = true;
            break;
          } else if (errStr.toLowerCase().indexOf("'"+accounts[i].Name.toLowerCase()+" -") != -1) { 
            // exchange "Task 'account - <taskname>'" format
            bRet = true;
            hCurrentProblemList[i] = true;
            break;
          } else if (bWLMInstalled) {
            // Windows Live Mail error handling
            var oRegEx;
            if (typeof (oRegEx) === "undefined")
              oRegEx = new RegExp($ss.snapin.email.utility.GetWinLiveMailConfig("error_regex", "0x8"));

            if (errStr.search(oRegEx) != -1) {
              bRet = true;
              hCurrentProblemList[i] = true;
              break;
            }
          }
        }
      }
      //
      // Generate an array of the indexed account names
      //
      var aCurrentProblemList = [];
      for (var i in hCurrentProblemList)
        aCurrentProblemList.push(i);
      //
      // Warm the databag with the list of accounts to operate on.
      //
      _databag.SetValue("TriggerAccountIdList",   aCurrentProblemList.join(','));
      _databag.SetValue("TriggerAccountProvider", fnAcctProvider);
      return bRet;
    },
    
    Diagnose : function()
    {
      var url = _config.GetConfigValue("preconfigured_snapin_url","subagent_getconnect","/sa/connect/getconnect")                        
      _LaunchBcont(url);
    },

    Ignore : function()
    {
      $ss.agentcore.utils.CloseBcont();
    },
        
    BeforeRender : function()
    {
    },
    
    AfterRender : function()
    {
      try
      {
      if(this.IsWithinDisabledPeriod())
      {
        $ss.agentcore.utils.CloseBcont();
      }
      else
      {
        if (_inet.BasicConnectionTest(false, false, $ss.agentcore.constants.INET_BROWSER_IE)) { 
           // found connection.. exit silently..
        	 $ss.agentcore.utils.CloseBcont();
            
        }
        else
        {
            var time = _config.GetConfigValue("triggers","ui_timeout_secs","30") * 1;
            if(time > 0)
              SnapinTriggerController.UpdateIgnoreButton(time);
           
            $ss.agentcore.utils.ShowBcont();   
            $ss.agentcore.utils.FocusBcont();

        }
      }
      }
      catch(ex)
    {
        $ss.agentcore.utils.CloseBcont();
      }
    },
    
    SavePreferences:function(timeInMinutes)
    { 
      
      var bSuccess = false;
      
      if(timeInMinutes)
      {
        var appName = _GetTriggeredAppName();
        if(appName)
        {
          var keyPath = _triggerPersistentPath + "\\" + appName;     
          var dtCurrent = new Date();
          var disabledUntil = dtCurrent.valueOf() + (timeInMinutes * MILLISECONDS_PER_MINUTE);
          var dtDisabledUntil = new Date(disabledUntil);
          
          bSuccess = _registry.SetRegValueByType("HKCU", keyPath, "DisabledUntil", 1, disabledUntil);
          bSuccess = bSuccess && _registry.SetRegValueByType("HKCU", keyPath, "DisabledUntilReadable", 1, dtDisabledUntil);
        }   
      }
      
        return bSuccess;
    },
    
    IsWithinDisabledPeriod:function()
    {
      var appName = _GetTriggeredAppName();

      if (appName)
      {
        var keyPath = _triggerPersistentPath + "\\" + appName;      
        var dtCurrent = new Date();
        var disabledUntil = parseInt(_registry.GetRegValue("HKCU", keyPath, "DisabledUntil"));
     
        if(disabledUntil)
        {  
          return (dtCurrent.valueOf() <= disabledUntil);
        }
      }
      
      return false;
    }

  });
  
  $.extend($ss.snapin.triggers.firefox,
  {
    checkFirefoxProxyConnection : function()
    {
      var bRetVal = true;
      
      if (_inet.IsFirefoxProxyEnabled())
      {
        var objPassCtl = $ss.agentcore.utils.activex.CreateObject("sdcuser.tgpassctl");
        
        if (objPassCtl)
        {
          var currentProxySettings = _inet.GetFirefoxProxySettings();
          var sProxyServer = currentProxySettings.server;

          var bSocketOpen = objPassCtl.openSocket(sProxyServer);

          if (bSocketOpen)
          {
            bRetVal = true;
            objPassCtl.close();
          } else {
            bRetVal = false;          
          }
          objPassCtl = null;
        }
      }
      
      return bRetVal;
    },
   
    IsGoodFirefoxConnection:function()
    { 
      var isGoodFirefoxConnection= false;

      var sHTTPMethod        = _config.GetConfigValue("getconnect", "HomeRunCheckMethod", "GET");
      var nHTTPTimeout       = _config.GetConfigValue("getconnect", "HTTPTimeout", 5000);
      var hostURL            = _config.ExpandAllMacros("%SERVERBASEURL%");
      
      if (! this.checkFirefoxProxyConnection())
        return isGoodFirefoxConnection;
      
      isGoodFirefoxConnection = _inet.FirefoxHttpRequest(hostURL, sHTTPMethod, "%TEMP%clientui.test", nHTTPTimeout);

      return isGoodFirefoxConnection;
    },
    
    Diagnose : function()
    {
      var url = _config.GetConfigValue("preconfigured_snapin_url","subagent_getconnect","/sa/connect/getconnect")                        
      _LaunchBcont(url + "?browser=firefox");
    },
    
    AfterRender:function()
    {
      try
      {
        if($ss.snapin.triggers.IsWithinDisabledPeriod())
        {
          $ss.agentcore.utils.CloseBcont();
        }
        else
        {
         if (this.IsGoodFirefoxConnection()) {
               
            $ss.agentcore.utils.CloseBcont(); 
          }
          else {
              var time = _config.GetConfigValue("triggers","ui_timeout_secs","30") * 1;
              if(time > 0)
                SnapinTriggerController.UpdateIgnoreButton(time);
                
              $ss.agentcore.utils.FocusBcont();

          }
        }       
      }
      catch(e)
      {
        $ss.agentcore.utils.CloseBcont();
      }
    }
    
  });
  
  $.extend($ss.snapin.triggers.DialUp,
  {
    AfterRender:function()
    {
      try
      {
        if($ss.snapin.triggers.IsWithinDisabledPeriod() || !(_utils.GetObjectFromCmdLine())["code"]) // if RAS Code is missing from command line, bail
        {
          $ss.agentcore.utils.CloseBcont();
        }
        else
        {        
            var time = _config.GetConfigValue("triggers","ui_timeout_secs","30") * 1;
            if(time > 0)
              SnapinTriggerController.UpdateIgnoreButton(time);
              
            $ss.agentcore.utils.FocusBcont();
        }       
      }
      catch(e)
      {
        $ss.agentcore.utils.CloseBcont();
      }
    }
  });
  
  $.extend($ss.snapin.triggers.OutlookExpress,
  {
     BeforeRender:function()  
     {
     },

    AfterRender:function()
    {
      if (_databag.GetValue("AllEmailFixed")) 
      {
        $ss.agentcore.utils.CloseBcont();
      }
      _databag.SetValue("FromTriggers", true);
      try
      {
        var bHasNoErrors = !$ss.snapin.triggers.GetErrors("smail_OE_GetEmailAccounts");
        
        if($ss.snapin.triggers.IsWithinDisabledPeriod() || bHasNoErrors)
        {
          $ss.agentcore.utils.CloseBcont();
        }
        else
        {
            var time = _config.GetConfigValue("triggers","ui_timeout_secs","30") * 1;
            if(time > 0)
              SnapinTriggerController.UpdateIgnoreButton(time);
              
            $ss.agentcore.utils.FocusBcont();        
        }       
      }
      catch(e)
      {
        // not putting anything here in case the statements inside the catch
        // block cause an exception themselves thus preventing the container
        // to exit properly
        $ss.agentcore.utils.CloseBcont();
      }
    },

    Diagnose:function ()
    {
      var url = _config.GetConfigValue("preconfigured_snapin_url","subagent_email","/sa/email")                        
      _LaunchBcont(url + "?type=troubleshoot");
    }
  });
  
  var _databag = $ss.agentcore.dal.databag;
  var _registry = $ss.agentcore.dal.registry;
  var _config = $ss.agentcore.dal.config;
  var _objContainer = $ss.agentcore.utils.activex.GetObjInstance();
  var _utils = $ss.agentcore.utils;
  var _txtTime = 0;
  var _intervalID = 0;
  var _inet = $ss.agentcore.network.inet;
  
  var _triggerPersistentPath = "SOFTWARE\\SupportSoft\\ProviderList\\" +
                                _config.GetContextValue("SdcContext:ProviderId") +
                                "\\users\\" +
                                _config.GetContextValue("SdcContext:UserName") +
                                "\\SupportTriggers\\Persistent";
  
  var MILLISECONDS_PER_MINUTE = 60000;

  function _GetTriggeredAppName()
  {
    var shell   = _objContainer.CreateInternalObject("shell");
    var appName = shell.ExpandEnvironmentStrings("%SPRT_CONTEXT_EXE%");
    if (appName == "%SPRT_CONTEXT_EXE%")
    {
      var cmdAppName = (_utils.GetObjectFromCmdLine())["exe"];
      if (cmdAppName)
      {
        appName = cmdAppName;
      }
    }
    return (appName != "%SPRT_CONTEXT_EXE%") ? appName : "";
  }


  function _LaunchBcont(path)
  {
    if(_utils.CheckInstance(_GetBCONTMutexPrefix() + "bigbcont"))
    {
      //alert("One more instance is running");
      SnapinTriggerController.DisplayBusyMsg();
    }
    else
    {
      var strRunCommand = "\"" + _utils.GetBCONTPath() + "\" /ini \""+ _config.GetProviderRootPath() + 
                "\\agent\\bin\\bcont_nm.ini \" /entry \"Trigger Minibcont\" /p " + _config.GetProviderID() + " /path \"" +path + "\"";
      _utils.RunCommand(strRunCommand,
                        $ss.agentcore.constants.BCONT_RUNCMD_ASYNC); 
      
      _utils.CloseBcont();
    }
  }
  
  function _GetBCONTMutexPrefix()
  {
    var mutexPrefix = "__SDC_BCONT_MUTEX__" + _config.GetProviderID();
    return mutexPrefix.toUpperCase();
  }
  
})()
