// Namespaces
$ss.snapin = $ss.snapin || {};
$ss.snapin.mdetect = $ss.snapin.mdetect || {};
$ss.snapin.mdetect.dec = $ss.snapin.mdetect.dec || {};
$ss.snapin.mdetect.navhandler = $ss.snapin.mdetect.navhandler || {};
$ss.snapin.mdetect.renderhelper = $ss.snapin.mdetect.renderhelper || {};


//start of decision functions 
(function(){
  $.extend($ss.snapin.mdetect.dec, {
    fr_BranchDetectModem: function() {
      var choice = $ss.agentcore.dal.databag.GetValue("detectmodem");

      if (choice == "nonefound" && ($ss.agentcore.dal.databag.GetValue("modemredetect") === "true" || $ss.agentcore.dal.databag.GetValue("modemredetect") === "false")) {
        choice = "noneretry";
      }

      return choice;
    },
    GetModemConnectionType: function () {
      var retConn;
      try {
        if (this.IsNullModem()) // if current modem code is set to null_modem, then look up from last detected value
        {
           retConn = _config.GetConfigValue(_const.SMAPI_MODEM, _const.SMAPI_DB_LASTDETECTED_MODEMCONNTYPE, "");   
        }
        else                     // if current modem code is available, then look up the class from modem xml
        {
          retConn = _objSI.ReadPersistInfo(_oSIConst.MODEM_CLASS, _oSIConst.CONN_TYPE);  
        }
      }
      catch(ex){
      }
      if (!retConn) retConn = "ethernet";

      if ((retConn != "usb") && (retConn != "ethernet") && (retConn != "wireless")) {
       retConn = "ethernet";
      }
      return retConn;
    },
    GetModemCode: function() {
      var retCode;
      try {
        if (this.IsNullModem()) // if current modem code is set to null_modem, then look up from last detected value
        {
          retCode = _config.GetConfigValue(_const.SMAPI_MODEM, _const.SMAPI_DB_LASTDETECTED_MODEMCODE, "");
        }
        else                     // if current modem code is available, then look up the class from modem xml
        {
          retCode = _objSI.ReadPersistInfo(_oSIConst.MODEM_CLASS, _oSIConst.MODEM_CODE);   
        }
      }
      catch(ex){
      }
      if (!retCode) retCode = "common";
      return retCode;
    },
    DetectModem: function(stepId,sessionId) {
      // Need to start this as first thing to avoid any delays
      // in button disablement
      //Busy jQuery
      var modem = "";
      var sModemRedetect = $ss.agentcore.dal.databag.GetValue("modemredetect") || "false";
      if (_sAutodetect === "0" && (sModemRedetect === "false" || sModemRedetect === "null")) {
        //Get last detected modem
        modem = $ss.agentcore.smapi.methods.GetInstanceWithDetection(false, false); 
        if (modem.sCode)
          modem = $ss.agentcore.smapi.methods.GetInstanceWithDetection(true, true, [modem.sCode]); //Detect if the last detected modem is active

        //if in case the modem is not detected to maintain the state and to display appropriate screen.
        $ss.agentcore.dal.databag.SetValue("modemredetect", "null");
      } else {
        //Delete modem entry so we always check for modem.
        $ss.agentcore.diagnostics.smartissue.DeletePersistInfo("ModemInfo","code");
        modem = $ss.agentcore.smapi.methods.GetInstanceWithDetection(true, true);
      }
      $("#nextbutton")[0].src = $("#nextbutton")[0].src.replace("btn_next_disabled.gif","btn_next.gif");
      $("#nextbutton").removeAttr("disabled");
      $("#prevbutton")[0].src = $("#prevbutton")[0].src.replace("btn_back_disabled.gif","btn_back.gif");
      $("#prevbutton").removeAttr("disabled");
      if (modem.GetLastError() == "SMAPI_NO_AUTH") {
        $('#cl_md_globalWorkingAnimation').hide('fast')
        $('#cl_md_detect_msg').hide('fast')
        $('#cl_md_Content').hide('fast')
        $('#cl_md_admin_creds').show('fast')
        var subheadertext = $ss.agentcore.utils.LocalXLate("snapin_mdetect","sub_header_text_nocreds")
        $('#subcontent').html(subheadertext);
        var subheader = $ss.agentcore.utils.LocalXLate("snapin_mdetect","header_text_nocreds")
        $('#subheader').html(subheader);
        var layoutSkinPath = $ss.GetLayoutSkinPath();
        if ($("#snapin_mdetect_buttons #retrybutton").length) {
			$("#snapin_mdetect_buttons #retrybutton").show();
			} else {
			var html = "<input type='image' alt='" + $ss.agentcore.utils.LocalXLate("snapin_mdetect", "cl_md_retry_button") + "' src='" + layoutSkinPath + "/buttons/" + $ss.GetShellLang() + "/btn_retry.gif' name='retrybutton' id='retrybutton'>&nbsp;";
			$("#snapin_mdetect_buttons").prepend(html);
		}
		$("#snapin_mdetect_buttons #retrybutton").attr("stepSessionId", sessionId).attr("stepId", stepId);
        //ss_evt_SendByName(SS_EVT_BTTNBAR_ENABLE_RETEST);
        //ss_evt_Subscribe(SS_EVT_BTTNBAR_CLICK_RETEST, onRetest);

        //No Auth means autodetect found protocol but couldn't get detail.
        //Delete SI record autodetect will re-detect.
        //Set nonfound in case user skips detection.
        $ss.agentcore.diagnostics.smartissue.DeletePersistInfo("ModemInfo","code");
        $ss.agentcore.dal.databag.SetValue("detectmodem", "skipmodem");

        return;
      } 

      modem.Name();
      var sModem = modem.mObjResponse["NewName"];

      if(!$ss.snapin.mdetect.dec.cl_md_isValidModemName(sModem)) {
        var html = $ss.agentcore.utils.LocalXLate("snapin_mdetect","unable_to_detect_modem")
        $('#cl_md_header_text').html(html);
        var modemredetect = $ss.agentcore.dal.databag.GetValue("modemredetect");
        if(modemredetect == "true") {
          var htmlbody = $ss.agentcore.utils.LocalXLate("snapin_mdetect","unable_to_detect_modem_again")
          $('#cl_md_detect_msg').html(htmlbody);
          if (_sAutodetect === "0") $ss.agentcore.dal.databag.SetValue("modemredetect", "false");
          //ss_evt_SendByName(SS_EVT_BTTNBAR_ENABLE_RETEST);
        } else {
          var htmlbody = $ss.agentcore.utils.LocalXLate("snapin_mdetect","unable_to_detect_modem_body")
          $('#cl_md_detect_msg').html(htmlbody);
          $('#cl_md_globalWorkingAnimation').hide('fast')
          $ss.agentcore.dal.databag.SetValue("detectmodem", "nonefound");
          SnapinMdetectController.ProcessNextThisSession();
          //setTimeout("ss_evt_SendByName(SS_EVT_BTTNBAR_CLICK_NEXT)",10);
        }
        $ss.agentcore.dal.databag.SetValue("detectmodem", "nonefound");
      } else {
       
        var html = $ss.agentcore.utils.LocalXLate("snapin_mdetect","found_modem")
        $('#cl_md_header_text').html(html);
        var htmlbody = $ss.agentcore.utils.LocalXLate("snapin_mdetect","found_modem_body") + ' <b>' + sModem + '</b>.</p>'
        $ss.agentcore.dal.config.SetUserValue("firstrun", "modemdetected", sModem);
        $('#cl_md_detect_msg').html(htmlbody);      
        $ss.agentcore.dal.databag.SetValue("detectmodem", "foundone");
        if (_sAutodetect === "0") $ss.agentcore.dal.databag.SetValue("modemredetect", "false");
        //ss_evt_SendByName(SS_EVT_BTTNBAR_HIDE_RETEST);
      }
      $('#cl_md_globalWorkingAnimation').hide('fast')
      //ss_shi_BusyEnd("", "cl_md_modemdetect busy end");
    },
    cl_md_isNullModem: function() {
      var bRet = false;
      var sModemCode = $ss.agentcore.diagnostics.smartissue.ReadPersistInfo("ModemInfo", "code");
      sModemCode     = sModemCode.toLowerCase();  
      if(sModemCode.substr(0,10)=="null_modem") bRet = true;
      return bRet;
    },
    cl_md_isValidModemName: function(sModem) {
      var bRet = true;
      if(sModem == "" || sModem == undefined || $ss.snapin.mdetect.dec.cl_md_isNullModem()) bRet = false;
      return bRet;
    },
    GenerateModemList: function() {
      var aModemList = $ss.snapin.mdetect.dec.cl_md_GetModemList();
      if (!aModemList || aModemList.lentgh < 0) {
        //g_debug.Error("Unable to load modem DOM -- no modem detection possible");
        return "";
      }

      // -- iterate through the modem list
      var number = 0;
      var sOut = new String();
      sOut =  '<div id="cl_md_none_framelist">';
      sOut += '<table id="cl_md_none_tablelist" class="clsModemNicTable" cellpadding="0" cellspacing="0">';
      sOut += '<tr>';
      for (var ndx = 0; ndx < aModemList.length; ndx++) {
        number++;
        var oNode = aModemList[ndx];
        var imgsrc = $ss.getSnapinAbsolutePath("snapin_modem") + "\\" + $ss.agentcore.dal.config.ParseMacros(oNode.image);
        sOut += '<td>';
        sOut += '<div><img alt="" src="' + imgsrc + '"></div>';
        sOut += '<label class="clsModemNicName">'+ oNode.name + '</label>';
        sOut += '</td>';
        if ((number / 3) == Math.round(number / 3)) {
           sOut += '</tr><tr>';
         }

      }
      sOut += '</tr>';
      sOut += '</table><div>';
      
      $("#cl_md_none_list").html(sOut);
      
      return sOut;
    },
    cl_md_GetModemList: function(bGetNullModems, bGetHiddenModems) {
      var aModemList = new Array();
 
      bGetNullModems   = (bGetNullModems==true);
      bGetHiddenModems = (bGetHiddenModems==true);
  
      var oModemXML   = $ss.agentcore.smapi.utils._GetModemDOM(); 
      var sCfgType    = $ss.agentcore.dal.config.GetConfigValue("modems", "modem_type");

      if(oModemXML==null) return aFoundModems;
  
      var oModemNode = $ss.agentcore.dal.xml.GetNodes('//sprt_modemlist/modem',"",oModemXML);
      for (var i=0; i<oModemNode.length; i++) {
        if($ss.agentcore.dal.xml.GetAttribute(oModemNode[i], "hide")=="1" && !bGetHiddenModems) continue;
        var oCommonNode=$ss.agentcore.dal.xml.GetNode('./instance[@id="common"]',"",oModemNode[i]);          

        // check type is matching the modem type defined in config.xml
        var oNodeForType = $ss.agentcore.dal.xml.GetNode("type", "", oCommonNode);
        var sType = $ss.agentcore.dal.xml.GetNodeText(oNodeForType);
        //var sType = $ss.agentcore.dal.xml.GetNodeText(oCommonNode,"type");
        if(sType != sCfgType) continue;
        var oNodeForClass = $ss.agentcore.dal.xml.GetNode("class", "", oCommonNode);
        if( $ss.agentcore.dal.xml.GetNodeText(oNodeForClass) =="$ss.snapin.modem.nullmodem.wrapper" && !bGetNullModems) continue;

        var oModem = new Object();
        oModem = $ss.agentcore.dal.xml.XmlToJs(oCommonNode, oModem, true);
        oModem["code"]=$ss.agentcore.dal.xml.GetAttribute(oModemNode[i], "code");
                
        aModemList.push(oModem);
      }
      return aModemList;
    }
  })

  var _sAutodetect = _config.GetConfigValue("modems", "autodetect", "0");
})();


//start of render helper function
(function(){
  $.extend($ss.snapin.mdetect.renderhelper, {
    md_modemdetect: {
      before: function(params,obj){
        $("#nextbutton")[0].src = $("#nextbutton")[0].src.replace("btn_next.gif","btn_next_disabled.gif");
        $("#nextbutton").attr("disabled", "true");
        $("#prevbutton")[0].src = $("#prevbutton")[0].src.replace("btn_back.gif","btn_back_disabled.gif");
        $("#prevbutton").attr("disabled", "true");
        $("#frcolumnone .frlinksCK").removeClass('frlinksCK').addClass('frlinks');
        $("#frcolumnone .frlinkslastCK").removeClass('frlinkslastCK').addClass('frlinkslast');
        $("#frcolumnone div[@name='modemdetect']").removeClass('frlinks').addClass('frlinksCK');
      },
      after: function(params,obj) {
    $("input.nextbutton:last").focus();
		var stepId = params.element.stepId ;
		var sessionId = params.element.stepSessionId;

		    $("#nextbutton")[0].src = $("#nextbutton")[0].src.replace("btn_next.gif","btn_next_disabled.gif");
        $("#nextbutton").attr("disabled", "true");
        $("#frcolumnone div[@name='complete']").removeClass('frlinkslastCK').addClass('frlinkslast');
        $('#ss_glb_resetpwd').hide('fast')
        $('#ss_glb_pboption1').hide('fast')
        $('#ss_glb_pboption2').hide('fast')
        $('#ss_glb_pboption3').hide('fast')
        
        //override the getassistence message.
        $('#ss_glb_getassistance').html('Click <b>Next</b> to skip modem detection.');
        setTimeout($ss.snapin.mdetect.dec.DetectModem(stepId,sessionId), 1000);
      }
    },
    md_nonefound: {
      before: function(params,obj){
        $("#frcolumnone .frlinksCK").removeClass('frlinksCK').addClass('frlinks');
        $("#frcolumnone .frlinkslastCK").removeClass('frlinkslastCK').addClass('frlinkslast');
        $("#frcolumnone div[@name='modemdetect']").removeClass('frlinks').addClass('frlinksCK');
        $("#nextbutton")[0].src = $("#nextbutton")[0].src.replace("btn_next.gif","btn_next_disabled.gif");
        $("#nextbutton").attr("disabled", "true");
      },
      after: function(params,obj) {
        $("input.nextbutton:last").focus();
        //setTimeout($ss.snapin.mdetect.dec.DetectModem(), 1000);
        //setTimeout($ss.snapin.mdetect.dec.GenerateModemList(), 1000);
        $ss.snapin.mdetect.dec.GenerateModemList();
      }
    }
  
  })
})();