// Namespaces
$ss.snapin = $ss.snapin || {};
$ss.snapin.mdetect = $ss.snapin.mdetect || {};
$ss.snapin.mdetect.dec = $ss.snapin.mdetect.dec || {};
$ss.snapin.mdetect.navhandler = $ss.snapin.mdetect.navhandler || {};
$ss.snapin.mdetect.renderhelper = $ss.snapin.mdetect.renderhelper || {};

// Define snapin, buttons and content holders
SnapinMdetectController = SteplistBaseController.extend("snapin_mdetect", {
  InitParam: {
    aNavigationItems: ["#snapin_mdetect #nextbutton","#snapin_mdetect #prevbutton" ],
    aContentPlaceHolderDIV: "snapin_mdetect_content",
    sBackToPageURL: "/home",
    sForwardToPageURL: null
  }
  
}, 

// Sub functions
{
  index: function(params){
    var instanceParam = params.inInstanceParam || {};
    if(instanceParam["standalone"]) {
      this.RenderStandAlone(params);
    }
    this.renderSnapinView("snapin_mdetect", params.toLocation, "\\views\\md_layout.htm");
    this._super(params,"snapin_mdetect");
    var flowInfo = this.Class.InitiateStep(params);
    if(flowInfo) {
      this.ProcessRender(params,flowInfo);
    }
  },
  ProcessNext: function(params){
    var flowInfo = this._super(this,params);
    if(flowInfo) {
      this.ProcessRender(params,flowInfo);
    }
  },

  ProcessPrevious: function(params){
    var flowInfo = this._super(this,params);
    if(flowInfo) {
      this.ProcessRender(params,flowInfo);
    }
    
  },
  ProcessRetry: function(params){
	var renderHelper = $ss.snapin.mdetect.renderhelper || {};
    //renderHelper.hideCloseBtn();
    var flowInfo = this._super(this,params);
    if(flowInfo) {
      this.ProcessRender(params,flowInfo);
    }    
  },
  ProcessRender: function(params,flowInfo) {
       var renderHelper = $ss.snapin.mdetect.renderhelper || {};
       var stepId = flowInfo.stepid;
       var retData = {};
       var opt = {};
       if (stepId) {
          try {
            if(renderHelper[stepId] && renderHelper[stepId].before) {
              retData = renderHelper[stepId].before(params,this);  
            }
            opt.oLocal = retData;
          } catch(ex) {
            //failed to execute the renderhelper function
          }
      
        }
      this.ProcessFlow(this,params,flowInfo,opt);
      // start the post processing
      if (stepId) {
          try {
            var afterStatus = {};
            if (renderHelper[stepId] && renderHelper[stepId].Retest) {
                  afterStatus = renderHelper[stepId].Retest(params, this);
            }
            if(renderHelper[stepId] && renderHelper[stepId].after) {
                 afterStatus = renderHelper[stepId].after(params,this);  
            }
            switch (afterStatus.move) {
              case "forward":
                return this.ProcessNext(params);
                break;
              case "back":
                return this.ProcessPrevious(params);
                break;
              default:
                break;
            }
          } catch(ex) {
              //failed to execute the renderhelper after function
          }
        }  
  },
  RenderStandAlone: function(params) {
    var instanceParam = params.inInstanceParam || {};
    params.stepListSession = {};           
    params.stepListSession.sBackToPageURL = instanceParam["BackToPageURL"]; 
    params.stepListSession.sForwardToPageURL = instanceParam["ForwardToPageURL"]; 
    params.stepListSession.sErrorUrl = instanceParam["ErrorUrl"];
    delete params.inInstanceParam;
		//Commented the next line to avoid loading modem detect snapin twice
    //this.index(params);

  },

 
// Button Clicks and Registry saves
  "#nextbutton click": function(params){
    this.ProcessNext(params);
  },
  "#prevbutton click": function(params){
    this.ProcessPrevious(params);
  },
  "#popupfirstrun #closeclick click": function(params){
    $("#closeclick").parents("p").remove();
    $("#popupfirstrun").hide('fast');
  },
  "#snapin_mdetect_buttons #retrybutton click": function(params){


    var sAdminName = $('#adminname').val();
    var sAdminPass = $('#adminpass').val();
    if (sAdminName == "") {
      $("#closeclick").parents("p").remove();
      $("#popupfirstrun").hide('fast');      
      $("#popupfirstrun").show('fast');
      var layoutSkinPath = $ss.GetLayoutSkinPath();
      var html = "<p class='close' align='center'><img src='" + layoutSkinPath + "/buttons/"+ $ss.GetShellLang()+"/btn_close.gif' id='closeclick' name='closeclick'  alt='" + $ss.agentcore.utils.LocalXLate("snapin_mdetect", "cl_md_close_button") + "'  border='0'/></p>";
      $("#popupfirstrun").append(html);
    } else {
  	  $("#snapin_mdetect_buttons #retrybutton").hide(); 
  	  $("#nextbutton")[0].src = $("#nextbutton")[0].src.replace("btn_next.gif","btn_next_disabled.gif");
        $("#nextbutton").attr("disabled", "true");
        $("#prevbutton")[0].src = $("#prevbutton")[0].src.replace("btn_back.gif","btn_back_disabled.gif");
        $("#prevbutton").attr("disabled", "true");     
  	  var modem = $ss.agentcore.smapi.methods.GetInstance(true);
  	  modem.UseAdminCreds(sAdminName,sAdminPass);
        this.ProcessRetry(params);
  	  return;
    }
  },
  "#cl_md_none_radio1 click": function(params){
    $("#nextbutton")[0].src = $("#nextbutton")[0].src.replace("btn_next_disabled.gif","btn_next.gif");
    $("#nextbutton").removeAttr("disabled");
    $ss.agentcore.dal.databag.SetValue("modemredetect","true");
  },
  "#cl_md_none_radio2 click": function(params){
    $("#nextbutton")[0].src = $("#nextbutton")[0].src.replace("btn_next_disabled.gif","btn_next.gif");
    $("#nextbutton").removeAttr("disabled");
    $ss.agentcore.dal.databag.SetValue("modemredetect","true");
  },
  "#cl_md_none_radio3 click": function(params){
    $("#nextbutton")[0].src = $("#nextbutton")[0].src.replace("btn_next_disabled.gif","btn_next.gif");
    $("#nextbutton").removeAttr("disabled");
    $ss.agentcore.dal.databag.SetValue("modemredetect","null");
  }
});