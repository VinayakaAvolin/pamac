$ss.snapins = $ss.snapins || {};
$ss.snapins.dm = $ss.snapins.dm || {};
$ss.snapins.dm.summary = $ss.snapins.dm.summary || {};

(
function() {
  $.extend($ss.snapins.dm.summary,
  {
    GetDownloadContent: function() {
      if (_RefreshCache) {
        var existingDownloads = _content.GetContentsByAnyType(["sprt_download"]);
        var validDownloads = _content.GetValidContentsForDisplay(existingDownloads);
        var sSortBy = $ss.agentcore.dal.config.GetConfigValue("snapin_download_manager", "sort_download_offer_by", "alphabet");
        validDownloads = ($ss.snapins.dm.SortTheContent(validDownloads));
        validDownloads = this.GetAttributesForDisplay(validDownloads);
        _cachedContent = validDownloads;
        _RefreshCache = false;
      }
      return _cachedContent;
    },
    RefreshCachedData: function() {
      _RefreshCache = true;
    },
    GetAttributesForDisplay: function(arrContent) {
      var length = (maxDisplayOfOffers < arrContent.length) ? maxDisplayOfOffers : arrContent.length;
      var arrDisplayContent = new Array;
      for (var i = 0; i < length; i++) {
        var sNew = "";
        if ($ss.snapins.dm.IsContentFormatNew(arrContent[i])) {
          sNew = ":::1";
        }
        var unManned = this.GetContentField(arrContent[i], "sdc_sd_silent" + sNew, "sccf_field_value_char");
        if (unManned === "yes") {
          arrContent.splice(i, 1);
          i--;
          length = (maxDisplayOfOffers < arrContent.length) ? maxDisplayOfOffers : arrContent.length;
          continue;
        }
        arrContent[i]["description"] = this.TruncateExtraChars(_content.GetContentProperty("sprt_download", arrContent[i].cid, arrContent[i].version, "scc_description"), maxDescriptionLength);
        var iconName = this.GetContentField(arrContent[i], "sdc_sd_Icon" + sNew, "sccfb_field_value_blob");
        if (iconName === "") iconName = this.GetContentField(arrContent[i], "sdc_sd_Icon", "sccfb_field_value_blob");//to fix the issue of blob, where image is not getting updates to :::1
        arrContent[i]["iconPath"] = _config.ExpandAllMacros("%CONTENTPATH%") + "\\sprt_download\\" + arrContent[i].cid + "." + arrContent[i].version + "\\" + iconName;
        arrContent[i]["size"] = this.GetContentField(arrContent[i], "sdc_sd_size" + sNew, "sccf_field_value_char");
        arrContent[i].title = this.TruncateExtraChars(arrContent[i].title, maxTitleLength);
        arrDisplayContent[i] = arrContent[i];
      }
      return arrDisplayContent;
    },

    TruncateExtraChars: function(sText, maxLength) {
      if (sText) {
        var iAlertLength = sText.length;
        if (iAlertLength > maxLength) {
          sText = sText.substring(0, maxLength);
          sText = sText + "...";
        }
      }
      return sText;
    },

    GetContentField: function(oJson, attributeName, attributeValue) {
      var returnValue = "";
      try {
        var xmlPath = _config.ExpandAllMacros("%CONTENTPATH%") + "\\sprt_download\\" + oJson.cid + "." + oJson.version + "\\" + oJson.cid + "." + oJson.version + ".xml";
        var domXml = $ss.agentcore.dal.xml.LoadXML(xmlPath, true);
        var sXPath = "/sprt//content-set//content//field-set";
        if (attributeName != undefined) {
          sXPath = sXPath + "//field[@name='" + attributeName + "']";

          if (attributeValue != undefined) {
            sXPath = sXPath + "//value[@name='" + attributeValue + "']";
          }
        }
        var iconNode = $ss.agentcore.dal.xml.GetNodes(sXPath, "", domXml);
        if (iconNode.length > 0) returnValue = iconNode[0].text;
      } catch (ex) { }
      return returnValue;
    }

  });

  var _content = $ss.agentcore.dal.content;
  var _config = $ss.agentcore.dal.config;
  var _cachedContent = null;
  var _RefreshCache = true;
  var maxTitleLength = _config.GetConfigValue("snapin_download_manager", "chars_in_summary_title", "15");
  var maxDescriptionLength = _config.GetConfigValue("snapin_download_manager", "chars_in_summary_description", "165");
  var maxDisplayOfOffers = _config.GetConfigValue("snapin_download_manager", "number_of_offers_in_summary", "2");
  $ss.agentcore.events.Subscribe("BROADCAST_ON_PRUNEDONE", $ss.snapins.dm.summary.RefreshCachedData);
})();
