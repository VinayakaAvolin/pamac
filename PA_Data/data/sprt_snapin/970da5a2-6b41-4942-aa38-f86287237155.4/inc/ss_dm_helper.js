$ss.snapins = $ss.snapins || {};
$ss.snapins.dm = $ss.snapins.dm || {};
(function() {
  $.extend($ss.snapins.dm,
  {

    CancellDownload: function() {
      //commenting this function call as the snapins is to be reloaded and hence not required
      //$("#snapin_download_manager #idProgressFooter").hide();
      //refresh the UI--
      $ss.agentcore.utils.Sleep(0);
      StopJob();
      CompleteJob();
      $ss.snapins.dm.afterJobUpdate(false, true);
      //commenting this function call as the snapins is to be reloaded and hence not required
      //toggleProgressBar(false);
      clearTimeout(_timeOutId);
      $ss.snapins.dm.reload();
    },


    APPLICATIONS: null,
    g_bShowing: false,
    g_sError: "",
    g_sNoShowReason: "",
    g_bSelections: false,

    GetCurrentState: function() {
      return (GetState());
    },

    checkState: function() {
      if (g_oClient) {
        if (this.IsRebootPending()) {
          $ss.snapins.dm.HideErrorBar();
          toggleProgressBar(false);
          return;
        }
        var sState = GetState();
        if (sState != "0") {
          switch (sState) {
            case "3": //STATUS_DOWNLOADING
            case "4": //STATUS_DOWNLOADED
            case "5": //STATUS_INSTALLING
            case "6": //STATUS_INSTALLED
            case "1": //STATUS_FINISHED
              $ss.snapins.dm.HideErrorBar();
              toggleProgressBar(true);
              _timeOutId = setTimeout("$ss.snapins.dm.MonitorInstall()", 500);
              break;
            case "8":  //STATUS_MANDATORY_REBOOT
              $ss.snapins.dm.HideErrorBar();
              toggleProgressBar(false);
              break;
            case "9":  //STATUS_MANDATORY_REBOOT
              $ss.snapins.dm.HideErrorBar();
              toggleProgressBar(false);
              _timeOutId = setTimeout("$ss.snapins.dm.MonitorInstall()", 500);
              break;
            case "2": //STATUS_ERROR
              StopJob();
              CompleteJob();
              $ss.snapins.dm.afterJobUpdate();
              showError();
              break;
            case "100":
              break;
            case "200":
              $ss.snapins.dm.afterJobUpdate();
              break;
          }
        }
      }
    },

    toggleDetails: function(pArgs) {
      var sMore = $ss.agentcore.utils.LocalXLate("snapin_download_manager","ss_dm_res_moretxt");
      var sLess = $ss.agentcore.utils.LocalXLate("snapin_download_manager","ss_dm_res_lesstxt");
      try {
        var oEl = pArgs.element;
        var sGuid, sClassName, oSpan;
        if (oEl) {
          sGuid = $(oEl).attr("guid");
          oSpan = $("#idTextOf_" + sGuid);
          if (oSpan.length) {
            sClassName = oSpan.attr("class");
            oSpan.toggleClass("clShort").toggleClass("clLong");
            $(oEl).removeClass().addClass(sClassName == "clShort" ? "LinkDetailClose small-link-Font" : "LinkDetailShow small-link-Font");
            $(oEl).attr("title", sClassName == "clShort" ? sLess : sMore);
            $(oEl).html(sClassName == "clShort" ? "<i class='fa fa-angle-double-left'></i> " + sLess : sMore + " <i class='fa fa-angle-double-right'></i>");
          }
        }
      } catch (oErr) {
      }
    },

    runThis: function(paArgs) {
      var oEl, sGuid;
      oEl = paArgs.element;
      if (oEl) {
        sGuid = $(oEl).attr("guid");
        if (sGuid) {

          //This is capture number of times installation attempts  
          var vCount = parseInt(GetFormElementVal(sGuid, "0", "INVOKED"));
          if (vCount == "" || vCount < 0) {
            vCount = 1;
          } else {
            vCount++;
          }

          SetFormElementVal(sGuid, "0", "INVOKED", vCount);
          _registry.SetRegValueByType(REG_HKCU, REG_THIS + "\\" + sGuid, "INVOKED", 1, GetFormElementVal(sGuid, "0", "INVOKED"));

          //Read the Installer Command Line
          sRunCmd = expandMacro(GetFormElementVal(sGuid, "0", "sdc_sd_CommandToRun"));

          //Get actual path from commandline
          sRunCmd = CmdTokenToRawToken(sRunCmd);

          if (fileExists(sRunCmd)) {
            sRunCmd = RawTokenToCmdToken(sRunCmd);

            //To Do : Append Command Line Aruments to run command

            $ss.agentcore.utils.system.RunCommand(sRunCmd, 4);
            // Commented as it is not required for this release          
            //logThis(sGuid, VERB_INVOKED);
          }
        }
      }
    },

    ss_dm_translate: function(psID, psDefault) {
      var sRet = psDefault;
      if (g_oTextCache[psID]) {
        return (g_oTextCache[psID]);
      }
      try {
        sRet = $ss.agentcore.utils.LocalXLate("snapin_download_manager", psID);
        if (sRet == "" || sRet == psID) {
          sRet = psDefault;
        }
      } catch (oErr) {
        sRet = psDefault;
      }
      g_oTextCache[psID] = sRet;
      return (sRet);
    },

    reload: function() {
      SnapinDownloadManagerController.dispatch('snapin_download_manager', 'index');
    },

    SetReqRegValues: function(sDOID, sPreReqSeq) {
      //When the DOs start installing newly remove the previous error state
      var sRegPath = REG_THIS + "\\" + sDOID;
      if (sPreReqSeq > 0) sRegPath = sRegPath + "\\" + sDOID + "_" + (sPreReqSeq + 1);
      _registry.DeleteRegVal("HKCU", sRegPath, "exit_code");
      SetFormElementVal(sDOID, sPreReqSeq, "INSTALLED", "1");

      //Set current DO as working state and save the data in the registry
      SetFormElementVal(sDOID, sPreReqSeq, "WORKING", "1");
      SaveDOStateToReg(sDOID, sPreReqSeq);
    },

    startInstallationsForMac: function(pArgs) {
      try{
        var bRet = true;
        var nCount = 0;
        var unManned = false;
        var bJobFlag = true;
        pArgs = pArgs || {};
        pArgs.requestArgs = pArgs.requestArgs || {};
        if(pArgs.requestArgs["unmanned"] == "true") {
          unManned = true;
          DeleteOldSilentInvokeJob();
        }
        if($ss.snapins.dm.IsRebootPending() && !unManned) {
          $ss.snapins.dm.reload(); //This case may occur in case of Silent shows mandatory reboot and big bcont is open
          return;
        }
        var addStartRegistry = true;
        var bMannedCondition, bUnMannedCondition, installationRequired = false;
        for(var sDOID in $ss.snapins.dm.APPLICATIONS) {
          if((GetFormElementVal(sDOID, "0", "SELECTED") == "1" || unManned) && GetFormElementVal(sDOID, "0", "ContentValid")) {
            bInstalled = isInstalled(sDOID, "0"); //($ss.snapins.dm.APPLICATIONS[sDOID]["0"]["INSTALLED"] == "1" && $ss.snapins.dm.APPLICATIONS[sDOID]["0"]["VERIFIED"] == "1");
            if(unManned) {
              if(parseInt(GetFormElementVal(sDOID, "0", "RetryAttempts")) > _iSilentReattempt)
                continue;
            }
            bMannedCondition = GetFormElementVal(sDOID, "0", "SELECTED") == "1" && !bInstalled && !IsDoSilent(sDOID);
            bUnMannedCondition = !bInstalled && unManned;
            if(bMannedCondition || bUnMannedCondition) {
              // if(bJobFlag) {
              //   var bOk = createJob(pArgs);
              //   if (!bOk) break;
              //   bJobFlag = false;
              // }
              for(var sPreReqSeq in $ss.snapins.dm.APPLICATIONS[sDOID]) {
                if(sPreReqSeq != "0") {
                  bInstalled = isInstalled(sDOID, sPreReqSeq);
                  bMannedCondition = !bInstalled && !IsDoSilent(sDOID);
                  bUnMannedCondition = !bInstalled && unManned;
                  if(bMannedCondition || bUnMannedCondition) {
                    nCount++;
                    $ss.snapins.dm.SetReqRegValues(sDOID, sPreReqSeq);
                    AddFile(sDOID, sPreReqSeq);
                  }
                }
              }
              nCount++;
              $ss.snapins.dm.SetReqRegValues(sDOID, "0");
              AddFile(sDOID, "0");
            }
          }
        }
        if(nCount > 0) {
          ResetProgressChachedCounters();
          if(g_oClient) {
            var oPropDict = NewDictionary();
            oPropDict = UpdateMandatoryDictValues(oPropDict);
            if(unManned) {
              oPropDict = SetUnmannedPriority(oPropDict);
              oPropDict = SetUnmannedTrayDisplay(oPropDict);
            }
            oPropDict = SetFailureRetry(oPropDict);
            oPropDict = SetTrayDispTime(oPropDict);
            SetResourceValue(oPropDict);
            var status = StartJobForMac();
            if(unManned == false && status == "2") {
              $ss.snapins.dm.reload();
              return;
            }
            $ss.snapins.dm.HideErrorBar();
            _timeOutId = setTimeout(function() { return $ss.snapins.dm.MonitorInstallForMac(unManned) }, 200);
          }
        }
      }
      catch(ex) {
      }
    },

    startInstallations: function(pArgs) {

      $ss.snapins.dm.startInstallationsForMac(pArgs);
      return;
      try {
        var bRet = true;
        var nCount = 0;
        var unManned = false;
        var bJobFlag = true;
        pArgs = pArgs || {};
        pArgs.requestArgs = pArgs.requestArgs || {};
        if (pArgs.requestArgs["unmanned"] == "true") {
          unManned = true;
          DeleteOldSilentInvokeJob();
        }
        if ($ss.snapins.dm.IsRebootPending() && !unManned) {
          $ss.snapins.dm.reload(); //This case may occur in case of Silent shows mandatory reboot and big bcont is open
          return;
        }

        var addStartRegistry = true;
        var bMannedCondition, bUnMannedCondition, installationRequired = false;
        for (var sDOID in $ss.snapins.dm.APPLICATIONS) {
          if ((GetFormElementVal(sDOID, "0", "SELECTED") == "1" || unManned) && GetFormElementVal(sDOID, "0", "ContentValid")) {
            bInstalled = isInstalled(sDOID, "0"); //($ss.snapins.dm.APPLICATIONS[sDOID]["0"]["INSTALLED"] == "1" && $ss.snapins.dm.APPLICATIONS[sDOID]["0"]["VERIFIED"] == "1");
            if (unManned) {
              if (parseInt(GetFormElementVal(sDOID, "0", "RetryAttempts")) > _iSilentReattempt)
                continue;
            }
            bMannedCondition = GetFormElementVal(sDOID, "0", "SELECTED") == "1" && !bInstalled && !IsDoSilent(sDOID);
            bUnMannedCondition = !bInstalled && unManned;
            if (bMannedCondition || bUnMannedCondition) {
              if (bJobFlag) {
                var bOk = createJob(pArgs);
                if (!bOk) break;
                bJobFlag = false;
              }
              for (var sPreReqSeq in $ss.snapins.dm.APPLICATIONS[sDOID]) {
                if (sPreReqSeq != "0") {
                  bInstalled = isInstalled(sDOID, sPreReqSeq);
                  bMannedCondition = !bInstalled && !IsDoSilent(sDOID);
                  bUnMannedCondition = !bInstalled && unManned;
                  if (bMannedCondition || bUnMannedCondition) {
                    nCount++;
                    $ss.snapins.dm.SetReqRegValues(sDOID, sPreReqSeq);
                    AddFile(sDOID, sPreReqSeq);
                  }
                }
              }
              nCount++;
              $ss.snapins.dm.SetReqRegValues(sDOID, "0");
              AddFile(sDOID, "0");
            }
          }
        }
        if (nCount > 0) {
          ResetProgressChachedCounters();
          if (g_oClient) {
            var oPropDict = NewDictionary();
            oPropDict = UpdateMandatoryDictValues(oPropDict);
            if (unManned) {
              oPropDict = SetUnmannedPriority(oPropDict);
              oPropDict = SetUnmannedTrayDisplay(oPropDict);
            }
            oPropDict = SetFailureRetry(oPropDict);
            oPropDict = SetTrayDispTime(oPropDict);
            SetResourceValue(oPropDict);
            var status = StartJob();
            if (unManned == false && status == "2") {
              $ss.snapins.dm.reload();
              return;
            }
            $ss.snapins.dm.HideErrorBar();
            _timeOutId = setTimeout(function() { return $ss.snapins.dm.MonitorInstall(unManned) }, 2000);
          }
        }
      } catch (ex) { }
    },

    selectThis: function(pArgs) {
      var oEl = pArgs.element;
      var sGuid = oEl.getAttribute("guid");
      if (sGuid != "") {
        var oCheckEl = $("#idCheck_" + sGuid);
        if (oCheckEl.length && !oCheckEl[0].disabled) {
          var bChecked = oCheckEl[0].checked;
          var bHidden = oCheckEl.css("display") === "none";
          if (!bHidden) {

            $("#idCheckAll").attr("checked", false);
            if (bChecked && GetFormElementVal(sGuid, 0, "sdc_sd_DefaultState") != "2") {
              SetFormElementVal(sGuid, "0", "SELECTED", "0");
            } else {
              SetFormElementVal(sGuid, "0", "SELECTED", "1");
            }
            oCheckEl.attr("checked", (GetFormElementVal(sGuid, "0", "SELECTED") == "1"));
          }
        }
        SaveDOStateToReg(sGuid, "0");
      }
      SetInstallBtnState();
    },

    selectThisApplication: function(pArgs) {
      $("#idCheckAll")[0].checked = false;
      var oEl = pArgs.element;
      var sGuid = $(oEl).attr("guid");
      if (sGuid != "") {
        var oCheckEl = $("#idCheck_" + sGuid);
        var bChecked = oCheckEl[0].checked;
        $ss.snapins.dm.selectThisDownloadApplication(sGuid,bChecked);
      }

        //This code is to check whether all the check boxes are checked and check the "Select All" check box accordingly.
      var tAllChecked = 1;
      for (var sDOID in $ss.snapins.dm.APPLICATIONS) {//We are looping all the entries.
          oEl = $("#idCheck_" + sDOID);
          if (oEl.length) {
              if (!oEl[0].disabled) {
                  if (!oEl[0].checked && oEl.length) {//Verifying whether they are checked or not.
                      tAllChecked = 0;//If an entry is not checked, then we are setting the flag to 0 and break out of the loop.
                      break;
                  }
              }
          }
      }
        //Based on the flag status, we are checking/unchecking the "Select All" checkbox accordingly.
      if (tAllChecked == 0) {
          $("#idCheckAll")[0].checked = false;
      }
      else {
          $("#idCheckAll")[0].checked = true;
      }
    },
    
    selectThisDownloadApplication : function(sGuid, bChecked){
       var sSelectedVal = "1";
       if (!bChecked && $ss.snapins.dm.APPLICATIONS[sGuid]["sdc_sd_DefaultState"] != "2") {
         sSelectedVal = "0";
      }
      SetFormElementVal(sGuid, "0", "SELECTED", sSelectedVal);
      SaveDOStateToReg(sGuid, "0");
      SetInstallBtnState();
    },

    selectAllApplications: function(pArgs) {
      var oEl, bChecked = pArgs.element.checked;
      for (var sDOID in $ss.snapins.dm.APPLICATIONS) {

        oEl = $("#idCheck_" + sDOID);

        if (oEl.length) {
          if (!oEl[0].disabled) {
            if (bChecked) {
              if (!oEl[0].checked && oEl.length) {
                oEl[0].checked = true;
                SetFormElementVal(sDOID, "0", "SELECTED", "1");
              }
            }
            else if (!bChecked) {
              if (oEl[0].checked && oEl[0]) {
                if (GetFormElementVal(sDOID, "0", "sdc_sd_DefaultState") != "2") {
                  oEl[0].checked = false;
                  SetFormElementVal(sDOID, "0", "SELECTED", "0");
                }
              }
            }
          }
        }
      }
      SaveSelectionChanges();
      SetInstallBtnState();
    },

    GetUnmannedFromList: function(oJson) {
      for (var i = 0; i < oJson.length; i++) {
        var sUnmanned = GetContentField(oJson[i], "sdc_sd_silent:::1", "sccf_field_value_char");
        if (sUnmanned === "no" || sUnmanned == false) {
          var temp = oJson.splice(i, 1);
          i--;
        }
      }
      return oJson;
    },

    RemoveUnmannedFromList: function(oJson) {
      var ni = 0, temp;
      var arrTemp = new Array();
      for (var i = 0; i < oJson.length; i++) {
        var sUnmanned = GetContentField(oJson[i], "sdc_sd_silent:::1", "sccf_field_value_char");
        if (sUnmanned === "no" || sUnmanned == false) {
          temp = oJson.splice(i, 1);
          arrTemp[ni] = temp[0];
          ni++;
          i--; //Since a element is removed, index needs to be reduced
        }
      }
      return arrTemp;
    },

    ProcessBeforeCallBack: function(sDldKey) {
      var bCallFlag = false;
      sDldList = $ss.agentcore.dal.registry.GetRegValue("HKCU", REG_THIS, sDldKey);
      if (sDldList) {
        sDldList = sDldList.split("#");
        for (var i = 0; i < sDldList.length; i++) {
          if (isInstalled(sDldList[i], "0")) {
            if (_sSilentInstallCompNeeded === "yes") {
              bCallFlag = true;
              break;
            } else {
              if (GetFormElementVal(sDldList[i], "0", "sdc_sd_requiresRestart") === "yes") {
                bCallFlag = true;
                break;
              }
            }
          }
        } //End of for
      }
      if (bCallFlag) CallMiniBcontForSilent(false, sDldKey);
      else $ss.agentcore.dal.registry.DeleteRegVal(REG_HKCU, REG_THIS, sDldKey);
      return bCallFlag;
    },

    loadApplicationData: function(pArgs) {
      var unManned = false, bSilentTest = false, bSilentInstalled = false;
      try {
        if (_refreshContent) {
          CreateApplicationObject();
          if (folderExists(_DOWNLOADS_)) {

            pArgs = pArgs || {};
            pArgs.requestArgs = pArgs.requestArgs || {};
            if (pArgs.requestArgs["unmanned"] == "true") {
              unManned = true;
              if (pArgs.requestArgs["test"] == "true") bSilentTest = true;
              ChangeJobNameToUnmanned();
            }
            BuildContentForm();
            LogInvalidContentAndSetFlags();
            // Logs the data for content display/listed and deploy and sets corresponding flag
            LogDeployDisplayAndSetFlags(unManned);
            HandleUndeployedDOContents();
            _refreshContent = false; // setting cache as false...
          }
        }

        ModifyStateAndSaveDataToReg(unManned, bSilentTest);


        if (unManned) {
          RemoveMannedDOs();
          if (pArgs.requestArgs["DldList"]) {
            //Call back the mini bcont only if there is any DO installed
            bSilentInstalled = $ss.snapins.dm.ProcessBeforeCallBack(pArgs.requestArgs["DldList"]);
          }
          RemoveSilentDOIfInvalid();
        }
        else {
          RemoveUnMannedDOs();
        }

        if (unManned && !bSilentTest) {
          RemoveSilentDOIfPrevInstalled();
          this.startInstallations(pArgs);
          return;
        }

        if ((_current_silent_failed_key !== null) && unManned) {
          if (bSilentInstalled) $ss.agentcore.utils.Sleep(1000);
          CallMiniBcontForSilent(true);
          return;
        }

        $ss.snapins.dm.g_bSelections = false;

        for (var sDOID in $ss.snapins.dm.APPLICATIONS) {
          if (GetFormElementVal(sDOID, "0", "INSTALLED") !== "1" || (GetFormElementVal(sDOID, "0", "INSTALLED") === "1" && GetFormElementVal(sDOID, "0", "VERIFIED") == "0")) {
            $ss.snapins.dm.g_bSelections = true;
            break;
          }
        }

        checkComponents();
      } catch (ex) { }
    },

    SortTheContent: function(oContents) {
      oContents = GetRequiredAttributesToSort(oContents);
      if (oContents && oContents.length > 0) {
        switch (_sPrimarySortBy) {
          case "sdc_sd_DisplayOrder":
            oContents.sort(SortLogicByDisplayOrderField); // in case of display order based on index use this one
            break;
          case "scc_last_mod":
            oContents.sort(SortLogicByDate);
            break;
          case "scc_title":
          default:
            oContents.sort(SortLogicByTitle);
            break;
        }
      }
      return oContents;
    },

    IsContentFormatNew: function(oContent) {
      var sRes = GetContentField(oContent, "sdc_sd_DownloadURL:::1");
      if (sRes === "") return false;
      return true;
    },

    IsNewlyDeployedContent: function(sDOID) {
      var regPath = REG_THIS + "\\" + sDOID;
      // if the entry for the content GUID already exists in registry
      if (!(getRegValue(REG_HKCU, regPath, "scc_content_guid") == sDOID)) {
        return true;
      }

      var iContentVerReg = parseInt(getRegValue(REG_HKCU, regPath, "scc_version"));
      var iContentVerFileSys = parseInt(GetFormElementVal(sDOID, "0", "scc_version"));

      // Later version of a known content
      if (iContentVerFileSys > iContentVerReg && (!isNaN(iContentVerReg)))
        return true;
      else
        return false;
    },

    GetCurrentUnmannedState: function() {
      ChangeJobNameToUnmanned();
      if (IsUnmannedInProgress())
        return "running";
      else
        return "stopped";
    },

    RefreshCachedData: function() {
      _refreshContent = true;
    },

    ScrolltoFirstErroredDO: function(bDoClickedFrmHomeSnapin) {
      var iScrollLen = 0;
      if ($ss.snapins.dm.g_sError !== "") {  // if there is an error
        if (!bDoClickedFrmHomeSnapin) { // and user has not clicked on a download offer link on the HOME snapin
          // scroll to the DO that has error
          iScrollLen = ($ss.snapins.dm.g_sError - 1) * 50;
        }
        $(".itemsscrollarea").scrollTo(iScrollLen + "px");
        $(".errorbararea").css("display", "block");
      }
    },

    HideErrorBar: function() {
      $(".errorbararea").css("display", "none");
    },

    HideRestartBar: function() {
      $(".restartbararea").css("display", "none");
    },

    MonitorInstallForMac:function(unManned) {
      var aProgress, sProgress;
      var bMonitor = false;
      if (!$("#downmanagerbg").html()) {
        bMonitor = false;
        return;
      }
      if (this.IsRebootPending()) {
        bMonitor = false;
        this.reload();
        return;
      }
      if(g_oClient) {
        var iCallback = 50;
        var mapReturned = g_oClient.GetJobState();
        sJobState = mapReturned["$$JOBSTATUS$$"];
        if (sJobState !== undefined) {
          switch (sJobState) {
            case "7": //STATUS_DELETING            
              bMonitor = false;
              break;
            case "3": //STATUS_DOWNLOADING
            case "4": //STATUS_DOWNLOADED
            case "5": //STATUS_INSTALLING
            case "6": //STATUS_INSTALLED
            case "10": //MOUNTING_DISK_IMAGE
            case "11": //MOUNTED_DISK IMAGE
              if ($("#idProgressFooter").css("display") === "none") {
                $ss.snapins.dm.HideErrorBar();
                $ss.snapins.dm.HideRestartBar();
                toggleProgressBar(true);
              }
              UpdateProgressForMac(mapReturned);
              bMonitor = true;
              break;
            case "1": //STATUS_FINISHED
              bMonitor = false;
              StopJob();
              CompleteJob();
              $ss.snapins.dm.afterJobUpdate();
              toggleProgressBar(false);
              if (!unManned) $ss.snapins.dm.reload();
              break;
            case "2": //STATUS_ERROR
              this.HandleErrorStatus();
              bMonitor = false;
              break;
            case "8": //STATUS_MANDATORY_REBOOT
              bMonitor = false;
              if (!unManned) $ss.snapins.dm.reload();
              break;
            case "9": //STATUS_WAITING
              bMonitor = true;
              iCallback = 2000; // In waiting state monitor every 2 seconds
              break;
            default:
              bMonitor = true; //added as the sync data was coming as a problematic data
              break;
          }
        } else {
          this.HandleErrorStatus();
          bMonitor = false;
        }
      }
      else {
        this.HandleErrorStatus();
        bMonitor = false;
      }
      if (bMonitor) {
        _timeOutId = setTimeout(function() { return $ss.snapins.dm.MonitorInstallForMac(unManned) }, iCallback);
      }
    },

    MonitorInstall: function(unManned) {
      var aProgress, sProgress;
      var bMonitor = false;
      if (!$("#downmanagerbg").html()) {
        bMonitor = false;
        return;
      }
      if (this.IsRebootPending()) {
        bMonitor = false;
        this.reload();
        return;
      }
      var tempMacro = g_oClient.GetMacroValues(g_oDictionary.Keys(), g_oDictionary.Items());
      var arrMacro = new VBArray(tempMacro);
      var arrTemp = arrMacro.toArray();
      var mapReturned = CreateMap(arrTemp);
      var iCallback = 500;
      if (g_oClient) {
        var sJobState = mapReturned["$$JOBSTATUS$$"];
        if (sJobState !== undefined) {
          switch (sJobState) {
            case "7": //STATUS_DELETING            
              bMonitor = false;
              break;
            case "3": //STATUS_DOWNLOADING
            case "4": //STATUS_DOWNLOADED
            case "5": //STATUS_INSTALLING
            case "6": //STATUS_INSTALLED
              if ($("#idProgressFooter").css("display") === "none") {
                $ss.snapins.dm.HideErrorBar();
                $ss.snapins.dm.HideRestartBar();
                toggleProgressBar(true);
              }
              UpdateProgress(mapReturned);
              bMonitor = true;
              break;
            case "1": //STATUS_FINISHED
              bMonitor = false;
              StopJob();
              CompleteJob();
              $ss.snapins.dm.afterJobUpdate();
              toggleProgressBar(false);
              if (!unManned) $ss.snapins.dm.reload();
              break;
            case "2": //STATUS_ERROR
              this.HandleErrorStatus();
              bMonitor = false;
              break;
            case "8": //STATUS_MANDATORY_REBOOT
              bMonitor = false;
              if (!unManned) $ss.snapins.dm.reload();
              break;
            case "9": //STATUS_WAITING
              bMonitor = true;
              iCallback = 2000; // In waiting state monitor every 2 seconds
              break;
            default:
              bMonitor = true; //added as the sync data was coming as a problematic data
              break;
          }
        } else {
          this.HandleErrorStatus();
          bMonitor = false;
        }
      } else {
        this.HandleErrorStatus();
        bMonitor = false;
        // showError();
      }
      if (bMonitor) {
        _timeOutId = setTimeout(function() { return $ss.snapins.dm.MonitorInstall(unManned) }, iCallback);
      }

    },

    HandleErrorStatus: function() {
      StopJob();
      CompleteJob();
      $ss.snapins.dm.afterJobUpdate();
      showError();
    },

    MonitorNetworkStatus: function() {
      isConnected = $ss.agentcore.network.inet.GetConnectionStatus();
      if (isConnected) {
        SetInstallBtnState();
        $("#lblNoConnectivity").css("display", "none");
        $("#lblNoConnectivityAllDOInstalled").css("display", "none");
      }
      else {
        $("#idInstallButton").css("display", "none");
        $("#idDisabledInstallButton").css("display", "none");

        $("#lblNoConnectivity").css("display", "none");
        $("#lblNoConnectivityAllDOInstalled").css("display", "block");

        for (var sDOID in $ss.snapins.dm.APPLICATIONS) {
          if (!isInstalled(sDOID, "0")) {
            $("#lblNoConnectivityAllDOInstalled").css("display", "none");
            $("#lblNoConnectivity").css("display", "block");
            break;
          }
        }
      }
    },

    IsRebootPending: function() {
      var bReturn = false;
      var sRegValue = getRegValue(REG_HKCU, REG_THIS, "RebootState");
      if (sRegValue === "1" || sRegValue === "2") bReturn = true;
      return bReturn;
    },

    SetRebootFlag: function(bDelete) {
      //When Comp rebooted delete the Reboot state value else See if any reboot pending
      //Set corresponding Reboot value (1 - Postpone reboot, 2 - Mandatory reboot)
      var bValSet = false;
      var bRebootSet = $ss.snapins.dm.IsRebootPending();
      if (bDelete) {
        _registry.DeleteRegVal(REG_HKCU, REG_THIS, "RebootState");
      } else if (!bRebootSet) {
        if ($ss.snapins.dm.IsMandatoryReebootPending()) {
          bValSet = true;
          _registry.SetRegValueByType(REG_HKCU, REG_THIS, "RebootState", $ss.agentcore.constants.REG_SZ, "2");
        }
        if ($ss.snapins.dm.IsPostponeRebootPending()) {
          bValSet = true;
          _registry.SetRegValueByType(REG_HKCU, REG_THIS, "RebootState", $ss.agentcore.constants.REG_SZ, "1");
        }
      }

      if (bValSet) {
        var sRegPath = "Software\\Microsoft\\Windows\\CurrentVersion\\RunOnce";
        var sRegValue = APP_COMPANY + "_RebootCallBack";
        var sRegData = _config.ExpandAllMacros("\"%ProgramFiles%\Common Files\\supportsoft\\bin\\bcont_nm.exe\" /starthidden /p \"" + APP_PROVIDER + "\" /entry \"Desktop Shortcut\" /path \"/dm?rebooted=true\" /ini \"%ProgramFiles%\\" + APP_PROVIDER + "\\agent\\bin\\bcont_nm.ini\"");
        _registry.SetRegValueByType(REG_HKCU, sRegPath, sRegValue, $ss.agentcore.constants.REG_SZ, sRegData);
      }
    },

    IsMandatoryReebootPending: function() {
      var bReturn = false;
      if (!rebootVerifier) InitializeRebootVerifier();
      if (!rebootVerifier) return bReturn;
      var sCmpVal = "\\??\\CONSONA DM MANDATORY REBOOT";
      return rebootVerifier.IsRebootPending(sCmpVal, APP_PROVIDER);
    },

    IsPostponeRebootPending: function() {
      var bReturn = false;
      if (!rebootVerifier) InitializeRebootVerifier();
      if (!rebootVerifier) return bReturn;
      var sCmpVal = "\\??\\CONSONA DM REBOOT";
      return rebootVerifier.IsRebootPending(sCmpVal, APP_PROVIDER);
    },

    ProcessDOPopup: function() {
      var res = _registry.GetRegValue(REG_HKCU, REG_THIS, "PopUpShown");
      if (res === "true") return;
      else {
        var popUrl = $ss.agentcore.dal.config.GetConfigValue("preconfigured_snapin_url", "supportmessage_pop", "/messages");
        var iniPath = " /ini \"" + $ss.agentcore.dal.config.GetProviderRootPath() + "agent\\bin\\minibcont.ini" + "\" /instancename \"minibcont_messages\" ";
        var sPath = _config.ExpandAllMacros("\"%ProgramFiles%\Common Files\\supportsoft\\bin\\bcont_nm.exe\" ") + getStartPath(popUrl, false, false) + " /syncevent " + iniPath;
        $ss.agentcore.utils.system.RunCommand(sPath, $ss.agentcore.constants.BCONT_RUNCMD_ASYNC);
        _registry.SetRegValueByType(REG_HKCU, REG_THIS, "PopUpShown", $ss.agentcore.constants.REG_SZ, "true");
      }
    },

    afterJobUpdate: function(unManned, bManualCancel) {
      for (var sDOID in $ss.snapins.dm.APPLICATIONS) {
        for (var sPreReqSeq in $ss.snapins.dm.APPLICATIONS[sDOID]) {
          if ((GetFormElementVal(sDOID, sPreReqSeq, "SELECTED") === "1" && GetFormElementVal(sDOID, sPreReqSeq, "WORKING") === "1") || (GetFormElementVal(sDOID, sPreReqSeq, "WORKING") === "1" && unManned === true)) {
            SetFormElementVal(sDOID, sPreReqSeq, "WORKING", "0");
            if (isInstalled(sDOID, sPreReqSeq)) {
              SetFormElementVal(sDOID, sPreReqSeq, "VERIFIED", "1");
            } else {
              SetFormElementVal(sDOID, sPreReqSeq, "VERIFIED", "0");
              //If its a Manual cancel do not show error
              if (bManualCancel)
                SetFormElementVal(sDOID, sPreReqSeq, "INSTALLED", "0");
            }
            SaveDOStateToReg(sDOID, sPreReqSeq);
          }
        }
      }
    },

    GetShortTitle: function(sTitle) {
      var sShortTitle = sTitle;
      if (sTitle.length > parseInt(_sCharsInTitle)) {
        sShortTitle = sTitle.substr(0, (parseInt(_sCharsInTitle) - 3)) + "...";
      }
      return sShortTitle;
    },

    GetDisplayInfo: function(sDOID, iCounter, sState) {
      var arrRet = new Array();
      var bVerified = isInstalled(sDOID, iCounter);
      var sTitle = "";
      var sDescription = "";
      var bHasApplications = false;
      var iErrorType = 0;

      var sRegPath = REG_THIS + "\\" + sDOID;
      if (iCounter > 0) sRegPath = sRegPath + "\\" + sDOID + "_" + (iCounter + 1);
      var iErrorCode = parseInt(getRegValue("HKCU", sRegPath, "exit_code"));
      if (isNaN(iErrorCode)) iErrorCode = 0;

      //When its a Main Download, test for the exit codes of the prerequiste so that error needs
      //to be displayed for the Main download in case if the Prerequiste has failed
      var bContentValid = true;
      if (iCounter === 0)
        bContentValid = GetFormElementVal(sDOID, iCounter, "ContentValid");
      for (var iPreCounter in $ss.snapins.dm.APPLICATIONS[sDOID])
        if (iPreCounter !== "0") {
          iPreCounter = parseInt(iPreCounter);
          var sRegPathTemp = REG_THIS + "\\" + sDOID + "\\" + sDOID + "_" + (iPreCounter + 1);
          var iErrorCodeTemp = parseInt(getRegValue("HKCU", sRegPathTemp, "exit_code"));
          if (!bContentValid)
            iErrorType = DO_ERROR_INVALIDCONTENT;
          else if ((!isNaN(iErrorCodeTemp) && iErrorCodeTemp !== 0) || ($ss.snapins.dm.APPLICATIONS[sDOID][iCounter]["INSTALLED"] === "1" && sState === "200" && !bVerified))
            iErrorType = DO_ERROR_INSTALLATION;
      }

      if (!bContentValid || (iErrorType === DO_ERROR_INVALIDCONTENT)) {
        if (iCounter == "0") {
          sTitle = GetFormElementVal(sDOID, iCounter, "scc_title");
          sDescription = GetFormElementVal(sDOID, iCounter, "scc_description");
        } else {
          sTitle = GetFormElementVal(sDOID, iCounter, "sdc_sd_prerequisite_title");
          sDescription = GetFormElementVal(sDOID, iCounter, "sdc_sd_prerequisite_description");
        }
        bHasApplications = true;
        iErrorType = DO_ERROR_INVALIDCONTENT;
      } else if (bVerified && bContentValid) {
        sTitle = GetFormElementVal(sDOID, iCounter, "sdc_sd_InstalledTitle");
        sDescription = GetFormElementVal(sDOID, iCounter, "sdc_sd_InstalledDescription");
        SetFormElementVal(sDOID, iCounter, "INSTALLED", "0");
        SetFormElementVal(sDOID, iCounter, "SELECTED", "0");
        SaveDOStateToReg(sDOID, iCounter);
        bHasApplications = false;
      } else if (iErrorCode !== 0 || iErrorType === DO_ERROR_INSTALLATION || (GetFormElementVal(sDOID, iCounter, "INSTALLED") === "1" && (sState === "200" || sState === "9") && !bVerified)) {
        bHasApplications = true;
        if (GetFormElementVal(sDOID, iCounter, "WORKING") === "1")
        {
          sTitle = GetFormElementVal(sDOID, iCounter, "scc_title");
          sDescription = GetFormElementVal(sDOID, iCounter, "scc_description");                    
        }
        else
        {
          sTitle = GetFormElementVal(sDOID, iCounter, "sdc_sd_ErrorTitle");
          sDescription = GetFormElementVal(sDOID, iCounter, "sdc_sd_ErrorDescription");
          iErrorType = DO_ERROR_INSTALLATION
        }
      } else {
        if (iCounter == 0) {
          sTitle = GetFormElementVal(sDOID, iCounter, "scc_title");
          sDescription = GetFormElementVal(sDOID, iCounter, "scc_description");
        }
        else {
          sTitle = GetFormElementVal(sDOID, iCounter, "sdc_sd_prerequisite_title");
          sDescription = GetFormElementVal(sDOID, iCounter, "sdc_sd_prerequisite_description");
        }
        bHasApplications = true;
      }
      arrRet[0] = sTitle;
      arrRet[1] = sDescription;
      arrRet[2] = bHasApplications;
      arrRet[3] = bVerified;
      arrRet[4] = iErrorType;
      return arrRet;
    },

    CloseMiniBcont: function(sCaller) {
      var sSnapinUrl = _config.GetConfigValue("preconfigured_snapin_url", "snapin_messagespopup", "messages");
      var sMiniBcontInstance = _utils.GetMiniBcontSpawnName(sSnapinUrl);
      if (_utils.IsMiniBcontRunning(sMiniBcontInstance)) {
        if (sCaller !== "messages" && _bCloseAlertNeeded) {
          alert(_utils.LocalXLate("snapin_download_manager", "ss_dm_res_popup_close_info"));
        }
        $ss.agentcore.utils.CloseMiniBcont(sMiniBcontInstance);
      }
    }

  });

  //Global Variables
  var g_oClient = false;
  var g_oDictionary = false;
  var isConnected = false;
  var g_oTextCache = new Object();

  //Common Application Variables
  var APP_NAME = $ss.snapins.dm.ss_dm_translate("ss_dm_res_appname", "Download Manager") + " (" + $ss.agentcore.dal.config.ExpandSysMacro("%USER%") + ")";
  
  //GS Customization Starts
  //Instead of setting APP_COMPANY by reading from the config.xml, it set set by using the MACRO %BRANDNAME% (Fasnet harcoded in config.xml - Engineering bug)
  //var APP_COMPANY = $ss.snapins.dm.ss_dm_translate("ss_dm_res_appcompany", "Fastnet");
  var prodName = $ss.agentcore.dal.config.ParseMacros("%BRANDNAME%");
  var APP_COMPANY = prodName;
  // GS Customization Ends

  var PRODUCT_NAME = $ss.snapins.dm.ss_dm_translate("ss_dm_res_appname", "Download Manager");
  var COMP_PRODUCT_NAME = APP_COMPANY + " " + PRODUCT_NAME;
  var JOB_NAME = APP_COMPANY + " " + APP_NAME;
  var APP_PROVIDER = $ss.agentcore.dal.config.GetProviderID();

  //Required Registry Paths
  var REG_HKCU = "HKCU";
  var REG_HKLM = "HKLM";
  var REG_SPRT = "Software\\SupportSoft\\ProviderList\\";
  var REG_APPLICATION = "Download Manager\\MODULE";
  var REG_THIS = REG_SPRT + APP_PROVIDER + "\\" + REG_APPLICATION;
  var REG_DMSPROCKET = "sprtcmd\\Sprockets\\DM";

  //Required Static Paths
  var _CACHE_ = buildPathString(expandMacro("%TEMP%"), "cache");
  var _DOWNLOADS_ = buildPathString(expandMacro("%CONTENTPATH%"), "sprt_download");
  var IMAGE_PATH = $ss.getSnapinAbsolutePath('snapin_download_manager') + "\\skins\\" + $ss.agentcore.dal.config.ParseMacros("%SKINNAME%") + "\\" + $ss.agentcore.dal.config.ParseMacros("%LANGCODE%");

  //State Variables
  var ENABLED = "1";
  var DISABLED = "0";
  var NONE = "-1";

  //Scheduler Related Variables
  var scheduleJobName = "sprtInvokeSilentInstall";
  var schedulerClient = false;
  var rebootVerifier = false;

  //Job related variable
  var _current_job_list_key = null;
  var _prev_working_do_guid = "";
  var _prev_working_do_state = "";
  var _prev_working_do_count = "";

  //Refresh flag
  var _refreshContent = true;

  //Agentcore variables
  var _config = $ss.agentcore.dal.config;
  var _registry = $ss.agentcore.dal.registry;
  var _utils = $ss.agentcore.utils;

  //Do Error Enums
  var DO_ERROR_INSTALLATION = 1;
  var DO_ERROR_INVALIDCONTENT = 2;


  //Log Related Variables
  var VERB_LISTED = "dm_do_listed";
  var VERB_DEPLOYED = "dm_do_deployed";
  var VERB_ERROR_CONTENT = "dm_do_content_err";
  var VERB_INVOKED = "dm_do_invoked";
  var VERB_UNINSTALLED = "dm_do_uninstalled";


  //private variables
  var _sCharsInTitle = _config.GetConfigValue("snapin_download_manager", "chars_in_do_list_title", "65");
  var _sPrimarySortBy = _config.GetConfigValue("snapin_download_manager", "sort_download_offer_by", "scc_tile");
  var _sSecondarySortBy = _config.GetConfigValue("snapin_download_manager", "secondary_display_order", "");
  var _bDisplayUndeployed = (_config.GetConfigValue("snapin_download_manager", "display_undeployed_but_installed_do", "") === "true");
  var _sViewType = _config.GetConfigValue("snapin_download_manager", "prerequisite_view_type", "detail");
  var _sMaxCharsProgTitle = parseInt(_config.GetConfigValue("snapin_download_manager", "chars_in_progress_title", "50"));
  var _sNumFailRetry = _config.GetConfigValue("snapin_download_manager", "number_of_failure_retries", "2");
  var _sTrayDispTime = _config.GetConfigValue("snapin_download_manager", "tray_icon_display_time", "5000");
  var _bCloseAlertNeeded = (_config.GetConfigValue("snapin_download_manager", "need_mini_bcont_close_alert", "False").toLowerCase() === "false")? false: true; 

  //Silent Configs
  var _sSilentIcon = _config.GetConfigValue("snapin_download_manager_silent", "tray_icon_needed_for_silent", "yes");
  var _sSilentJobPriority = _config.GetConfigValue("snapin_download_manager_silent", "job_priority_for_silent", "3");
  var iTempSilentReattempt = parseInt(_config.GetConfigValue("snapin_download_manager_silent", "reattempt_count_for_silent", "2"));
  var _iSilentReattempt = isNaN(iTempSilentReattempt) ? 2 : iTempSilentReattempt; //incrementing by 1 as num of Download tries (Reattempt + 1)
  var _sSilentScheduleTime = _config.GetConfigValue("snapin_download_manager_silent", "reattempt_schedule_for_silent", "60");
  var _sSilentInstallCompNeeded = _config.GetConfigValue("snapin_download_manager_silent", "install_complete_required_for_silent", "no");
  var _current_silent_failed_key = null;
  var _bSilentScheduled = false;
  var _timeOutId = false;

  function buildPathString(psTrunk, psBranch) {
    return ($ss.agentcore.dal.file.BuildPath(psTrunk, psBranch));
  }

  function count(poObj) {
    var nC = 0;
    for (var nI in poObj) {
      nC++;
    }
    return (nC);
  }

  function GetCurrentDldGuids(unManned) {
    var dldGuids = [];

    for (var sDOID in $ss.snapins.dm.APPLICATIONS) {
      if (GetFormElementVal(sDOID, "0", "SELECTED") === "1" && GetFormElementVal(sDOID, "0", "VERIFIED") !== "1" && !unManned) {
        dldGuids.push(sDOID);
      } else if (unManned && !isInstalled(sDOID, "0") && (parseInt(GetFormElementVal(sDOID, "0", "RetryAttempts")) <= _iSilentReattempt)) {
        dldGuids.push(sDOID);
      }
    }
    return dldGuids.join("#");
  }

  function CreateMap(sArray) {
    var arrMacros = new Array();
    var bKey = true;
    for (var counter in sArray) {
      if (bKey) {
        if ("string" == typeof (sArray[counter])) {
          var key = sArray[counter];
          var iCounter = parseInt(counter) + 1;
          var value = sArray[iCounter];
          arrMacros[key] = value;
          bKey = false;
        }
      }
      else {
        bKey = true;
      }
    }
    return arrMacros;
  }

  /*Install Button related function*/
  function CheckAndFindInstallBtnState() {
    for (var sDOID in $ss.snapins.dm.APPLICATIONS) {
      if (IsDownloadOfferState("SELECTED", sDOID) && GetFormElementVal(sDOID, "0", "ContentValid")) {
        if (!IsDownloadOfferState("INSTALLED", sDOID)) {
          return ENABLED;
        }
        else {
          if (!IsDownloadOfferState("VERIFIED", sDOID)) {
            return ENABLED;
          }
        }
      }
    }
    return DISABLED;
  }

  function IsDownloadOfferState(doState, sDOID) {
    if (getRegValue(REG_HKCU, REG_THIS + "\\" + sDOID, doState) == "1") {
      return true;
    }
    return false;
  }

  function SetInstallBtnState() {
    var btnState = NONE;
    if (isConnected) {
      if ($ss.snapins.dm.g_bShowing) {
        btnState = CheckAndFindInstallBtnState();
      }
      SetInstallBtnToState(btnState)
    }
  }

  function SetInstallBtnToState(sEnabled) {
    if (sEnabled == 1) {
      $("#idDisabledInstallButton").css("display", "none");
      $("#idInstallButton").css("display", "block");
      $("#idInstallButton").css("cursor", "hand");
    }
    else if (sEnabled == 0) {
      $("#idInstallButton").css("display", "none");
      $("#idDisabledInstallButton").css("display", "block");
      $("#idDisabledInstallButton").css("cursor", "text");
    }
    else if (sEnabled == -1) {
      $("#idInstallButton").css("display", "none");
      $("#idDisabledInstallButton").css("display", "none");
    }
  }
  /*End Of - Install Button related function*/

  function CallMiniBcontForSilent(bFailure, sKey) {
    var dmUrl = $ss.agentcore.dal.config.GetConfigValue("preconfigured_snapin_url", "downloadmanager_messagesinfo", "/messagesinfo");
    if (bFailure)
      dmUrl = dmUrl + "?callback=true&silentFailure=true&FailList=" + _current_silent_failed_key;
    else
      dmUrl = dmUrl + "?callback=true&DldList=" + sKey;

    var iniPath = " /ini \"" + $ss.agentcore.dal.config.GetProviderRootPath() + "agent\\bin\\minibcont.ini" + "\"";
    var startPath = " /starthidden /p " + APP_PROVIDER + " /instancename \"minibcont_messagesinfo_silent\" /path \"" + dmUrl + "\"" + iniPath;
    var sPath = $ss.agentcore.dal.ini.GetPath("EXE_PATH");
    sPath = "\"" + sPath + "\"" + startPath;
    $ss.agentcore.utils.system.RunCommand(sPath, $ss.agentcore.constants.BCONT_RUNCMD_ASYNC);
  }

  /*Get Different Command Lines*/
  function getMainBcontArgumentForSilentTest() {
    var dmUrl = $ss.agentcore.dal.config.GetConfigValue("preconfigured_snapin_url", "downloadmanager_home", "/dm");
    dmUrl = dmUrl + "?unmanned=true&test=true&DldList=" + _current_job_list_key;
    var iniPath = " /ini \"" + $ss.agentcore.dal.config.GetProviderRootPath() + "agent\\bin\\bcont_nm.ini" + "\"";
    var startPath = " /starthidden /p " + APP_PROVIDER + " /instancename \"" + APP_PROVIDER + "_silent\" /path \"" + dmUrl + "\"" + iniPath;
    return startPath;
  }

  function getMiniBcontStartArgument() {
    var dmUrl = $ss.agentcore.dal.config.GetConfigValue("preconfigured_snapin_url", "downloadmanager_messagesinfo", "/messagesinfo");
    dmUrl = dmUrl + "?callback=true&DldList=" + _current_job_list_key;
    var iniPath = " /ini \"" + $ss.agentcore.dal.config.GetProviderRootPath() + "agent\\bin\\minibcont.ini" + "\"";
    var startPath = getStartPath(dmUrl, "minibcont_messagesinfo", true) + iniPath;
    return startPath;
  }

  function getMiniBcontArgumentForMandatoryReboot() {
    var dmUrl = $ss.agentcore.dal.config.GetConfigValue("preconfigured_snapin_url", "downloadmanager_messagesinfo", "/messagesinfo");
    dmUrl = dmUrl + "?callback=true&mandatoryreeboot=true";
    var iniPath = " /ini \"" + $ss.agentcore.dal.config.GetProviderRootPath() + "agent\\bin\\minibcont.ini" + "\"";
    var startPath = getStartPath(dmUrl, "minibcont_messagesinfo", true) + iniPath;
    return startPath;
  }

  function getMainBcontStartArgument() {
    var dmUrl = $ss.agentcore.dal.config.GetConfigValue("preconfigured_snapin_url", "downloadmanager_home", "/dm");
    var iniPath = " /ini \"" + $ss.agentcore.dal.config.GetProviderRootPath() + "agent\\bin\\bcont_nm.ini" + "\"";
    var startPath = getStartPath(dmUrl, false, false) + "\"" + iniPath;
    return startPath;
  }

  function getStartPath(sUrl, sInstance, bHidden) {
    var sStartPath = " /p " + APP_PROVIDER;
    if (bHidden)
      sStartPath += " /starthidden "
    if (sInstance)
      sStartPath += " /instancename \"" + sInstance + "\"";
    if (sUrl)
      sStartPath += " /path \"" + sUrl + "\"";

    return sStartPath;
  }
  /*End Of - Get Different Command Lines*/

  /*DM Job Related Functions*/
  function createJob(pArgs) {
    if (!g_oClient) InitializeGlobalDMClient();
    if (g_oClient) {
      var unManned = false;
      pArgs = pArgs || {};
      pArgs.requestArgs = pArgs.requestArgs || {};
      if (pArgs.requestArgs["unmanned"] == "true") unManned = true;

      if (!g_oDictionary) CreateGlobalDictionary();
      var ret = g_oClient.CreateDMJob(g_oDictionary.Keys(), g_oDictionary.Items());
      var sa = new VBArray(ret);
      var arrTemp = sa.toArray();
      var mapReturned = CreateMap(arrTemp);

      //if (g_oClient.CreateJob(JOB_NAME, APP_PROVIDER) != "0") {
      if (mapReturned["RETURN_VALUE"] == "0" || mapReturned["RETURN_VALUE"] == "1") {
        try {
          var sPath = $ss.agentcore.dal.ini.GetPath("EXE_PATH");
          _current_job_list_key = new Date().getTime();
          var dlList = GetCurrentDldGuids(unManned);
          _registry.SetRegValueByType(REG_HKCU, REG_THIS, _current_job_list_key, $ss.agentcore.constants.REG_MULTI_SZ, dlList);
          var oDictionary = false;
          oDictionary = NewDictionary();

          var sMandateRebootArg = getMiniBcontArgumentForMandatoryReboot();
          oDictionary = UpdateDictWithCallBackArgs(oDictionary, sPath);
          oDictionary.add("CALLBACK_ARGS", sMandateRebootArg);
          oDictionary.add("CALLBACK_ID", "3"); //Mandatory Reboot call back
          ret = g_oClient.UICallBack(oDictionary.Keys(), oDictionary.Items());
          oDictionary.RemoveAll();

          if (unManned) {
            var sSilentTestArg = getMainBcontArgumentForSilentTest();
            oDictionary = UpdateDictWithCallBackArgs(oDictionary, sPath);
            oDictionary.add("CALLBACK_ARGS", sSilentTestArg);
            oDictionary.add("CALLBACK_ID", "2"); //Job Completion test call b ack for silent (if silent jobs are not installed we will schedule a job)
            ret = g_oClient.UICallBack(oDictionary.Keys(), oDictionary.Items());
            oDictionary.RemoveAll();

            return true;
          }

          var onCompleteStartArgument = getMiniBcontStartArgument();
          oDictionary = UpdateDictWithCallBackArgs(oDictionary, sPath);
          oDictionary.add("CALLBACK_ARGS", onCompleteStartArgument);
          oDictionary.add("CALLBACK_ID", "2"); //Job Completion callback
          ret = g_oClient.UICallBack(oDictionary.Keys(), oDictionary.Items());
          oDictionary.RemoveAll();

          //g_oClient.SetCallback(JOB_NAME, APP_PROVIDER, sPath, onCompleteStartArgument);

          var onSysIconArgument = getMainBcontStartArgument() + " /entry \"Tray Icon\"";
          if (!oDictionary) oDictionary = NewDictionary();
          oDictionary = UpdateDictWithCallBackArgs(oDictionary, sPath);
          oDictionary.add("CALLBACK_ARGS", onSysIconArgument);
          oDictionary.add("CALLBACK_ID", "1"); //Tray Click call back

          ret = g_oClient.UICallBack(oDictionary.Keys(), oDictionary.Items());
          oDictionary.RemoveAll();

          //g_oClient.SetTrayClickCallBack(JOB_NAME, APP_PROVIDER, sPath, onSysIconArgument);
        } catch (ex) { }
        return (true);
      } else {
        g_oClient = false;
      }
    }

    return (false);
  }

  function AddFile(psId, sPreReqSeq) {
    if (!g_oClient) {
      createJob();
    }
    if (g_oClient) {
      //var sAddArg = GetAddFileArgStr(psId, sPreReqSeq);
      var oDictionary = false;
      oDictionary = NewDictionary();
      oDictionary = UpdateDictWithArgs(psId, sPreReqSeq, oDictionary);
      if (bMac) {
        g_oClient.AddDMFile(oDictionary);
      }else {
      var sRet = g_oClient.AddDMFile(oDictionary.Keys(), oDictionary.Items());
      }
      return true;

    }
    return (false);
  }

  function StartJob() {
    if (g_oClient) {
      toggleProgressBar(true);
      SetLoggerArgs();
      SetToolTipValues();
      if (!g_oDictionary) CreateGlobalDictionary();
      var ret = g_oClient.StartDMJob(g_oDictionary.Keys(), g_oDictionary.Items());
      var sa = new VBArray(ret);
      var saArr = sa.toArray();
      var myMap = CreateMap(saArr);
      return myMap["RETURN_VALUE"];
    }
  }

  function StartJobForMac() {
    if (g_oClient) {
      toggleProgressBar(true);
      SetLoggerArgs();
      SetToolTipValues();
      if (!g_oDictionary) CreateGlobalDictionary();
      var ret = g_oClient.StartDMJob();
    }
  }

  function ChangeJobNameToUnmanned() {
    JOB_NAME = APP_COMPANY + " " + $ss.snapins.dm.ss_dm_translate("ss_dm_res_appname", "Download Manager") + " Silent";
  }

  function StopJob() {
    if (g_oClient) {
      if (!g_oDictionary) CreateGlobalDictionary();
      var ret = g_oClient.StopDMJob(g_oDictionary.Keys(), g_oDictionary.Items());
    }
  }

  function CompleteJob() {
    if (g_oClient) {
      if (!g_oDictionary) CreateGlobalDictionary();
      var ret = g_oClient.CompleteDMJob(g_oDictionary.Keys(), g_oDictionary.Items());
      g_oClient = false;
    }
  }

  function SetLoggerArgs() {
    var oDict = NewDictionary();
    oDict = UpdateDictWithLoggerArgs(oDict);
    if (g_oClient) {
      g_oClient.SetMacroValues(oDict.Keys(), oDict.Items());
    }
  }

  function SetResourceValue(oDict) {
    if (g_oClient) {
      var ret = g_oClient.SetDMProperties(oDict.Keys(), oDict.Items());
      //g_oClient.SetResourceStrings(JOB_NAME, APP_PROVIDER, sResourceArg);
    }
  }
  /*Enf Of - DM Job Related Functions*/

  /*Safe Array Filling functions*/
  function NewDictionary() {
    if (bMac) {
      oTemp = new Dictionary();
    }
    else {
    oTemp = new ActiveXObject("Scripting.Dictionary");
  }
    return oTemp;
  }

  function UpdateMandatoryDictValues(oParam) {
    oParam.add("JOB_NAME", JOB_NAME);
    oParam.add("PROVIDER_ID", APP_PROVIDER);

    return oParam;
  }

  function CreateGlobalDictionary() {
    if (!g_oDictionary)
      g_oDictionary = NewDictionary();
    else
      g_oDictionary.RemoveAll();

    g_oDictionary = UpdateMandatoryDictValues(g_oDictionary);
  }

  function UpdateDictWithCallBackArgs(oDictionary, sPath) {
    oDictionary.add("JOB_NAME", JOB_NAME);
    oDictionary.add("PROVIDER_ID", APP_PROVIDER);
    oDictionary.add("CALLBACK_APP", sPath);

    return oDictionary;
  }

  function UpdateDictWithArgs(psId, sPreReqSeq, oDictionary) {
    var sRestart = "0";
    var sFileGuid = GetFormElementVal(psId, sPreReqSeq, "scc_content_guid");
    var sFileVersion = GetFormElementVal(psId, sPreReqSeq, "scc_version");
    var sPrerqFile = "0";
    var sTitle = GetFormElementVal(psId, "0", "scc_title");
    if (sPreReqSeq !== "0") {
      sFileGuid += "_" + GetFormElementVal(psId, sPreReqSeq, "PriorityOrder"); //File Guid for Prerequisite file
      sPrerqFile = "1";
      if (_sViewType === "detail") {
        sTitle = GetFormElementVal(psId, sPreReqSeq, "sdc_sd_prerequisite_title");
      }
      oDictionary.add("DO_FILENAME", GetFormElementVal(psId, "0", "scc_content_guid"));
    }
    if (GetFormElementVal(psId, sPreReqSeq, "sdc_sd_requiresRestart") === "" || GetFormElementVal(psId, sPreReqSeq, "sdc_sd_requiresRestart") === "no") {
      sRestart = "0";
    } else if (GetFormElementVal(psId, sPreReqSeq, "sdc_sd_requiresRestart") === "yes") {
      sRestart = "1";
    } else {
      sRestart = "2";
    }

    oDictionary.add("JOB_NAME", JOB_NAME);
    oDictionary.add("PROVIDER_ID", APP_PROVIDER);
    oDictionary.add("FILE_TITLE", sTitle + " ");
    oDictionary.add("FILE_URL", GetFormElementVal(psId, sPreReqSeq, "sdc_sd_DownloadURL"));
    oDictionary.add("FILE_SAVEAS", buildPathString(_CACHE_, expandMacro(GetFormElementVal(psId, sPreReqSeq, "sdc_sd_InstallationCmd"))));
    oDictionary.add("PREREQUISITE_FILE", sPrerqFile);
    oDictionary.add("INSTAL_ARGUMENTS", expandMacro(GetFormElementVal(psId, sPreReqSeq, "sdc_sd_InstallationArguments")));
    oDictionary.add("FILE_GUID", sFileGuid);
    oDictionary.add("FILE_CHECKSUM", GetFormElementVal(psId, sPreReqSeq, "sdc_sd_checksum"));
    oDictionary.add("INSTALL_LOCKDOWN", GetFormElementVal(psId, "0", "sdc_sd_InstallOptions"));
    oDictionary.add("ERRORSOURCE_ID", GetFormElementVal(psId, sPreReqSeq, "sdc_sd_ErrorSrcID"));
    oDictionary.add("REBOOT_VALUE", sRestart);
    oDictionary.add("HALT_INSTAL_IF_ERROR", "1"); //Hard Coded the value as complex config values need not be exposed to author
    oDictionary.add("DELETE_FILE_AFTER_INSTALL", "0");
    oDictionary.add("FILE_64_BIT", "0");
    oDictionary.add("FILE_VERSION", sFileVersion);

    return oDictionary;
  }

  function UpdateDictWithLoggerArgs(oDictionary) {
    oDictionary = UpdateMandatoryDictValues(oDictionary);
    var sSessionId = $ss.agentcore.utils.GetSessionId();
    var sIPAddress = "";
    oDictionary.add("$$BCONTSESSIONID$$", sSessionId);
    oDictionary.add("$$CLIENTIPADDRESS$$", sIPAddress);
    return oDictionary;
  }

  function SetToolTipValues() {
    var oDict = NewDictionary();
    oDict = UpdateMandatoryDictValues(oDict);
    var sDownloading = _utils.LocalXLate("snapin_download_manager", "ss_dm_macro_tooltip_downloading", [COMP_PRODUCT_NAME]);
    var sDownloaded = _utils.LocalXLate("snapin_download_manager", "ss_dm_macro_tooltip_downloaded", [COMP_PRODUCT_NAME]);
    var sInstalling = _utils.LocalXLate("snapin_download_manager", "ss_dm_macro_tooltip_installing", [COMP_PRODUCT_NAME]);
    var sInstalled = _utils.LocalXLate("snapin_download_manager", "ss_dm_macro_tooltip_installed", [COMP_PRODUCT_NAME]);
    var sMandateReboot = _utils.LocalXLate("snapin_download_manager", "ss_dm_macro_tooltip_mandatory_reeboot", [COMP_PRODUCT_NAME]);
    var sFinishedJob = _utils.LocalXLate("snapin_download_manager", "ss_dm_macro_tooltip_finished_job", [COMP_PRODUCT_NAME]);
    var sErrorToolTip = _utils.LocalXLate("snapin_download_manager", "ss_dm_macro_tooltip_error_job", [COMP_PRODUCT_NAME]);

    oDict.add("1", sFinishedJob);
    oDict.add("2", sErrorToolTip);
    oDict.add("3", sDownloading);
    oDict.add("4", sDownloaded);
    oDict.add("5", sInstalling);
    oDict.add("6", sInstalled);
    oDict.add("8", sMandateReboot);
    if (g_oClient) {
      g_oClient.SetToolTipStrings(oDict.Keys(), oDict.Items());
    }
  }

  function SetUnmannedPriority(oDict) {
    if (oDict) {
      oDict.add("JOBPRIORITY", _sSilentJobPriority);
    }
    return oDict;
  }

  function SetUnmannedTrayDisplay(oDict) {
    if (oDict) {
      var iState = 0;

      if (_sSilentIcon === "yes") iState = 31;
      else iState = 0;
      iState = iState + ''; //convert state to string
      oDict.add("TRAYDISPLAY", iState);
    }
    return oDict;
  }

  function SetFailureRetry(oDict) {
    if (oDict) {
      oDict.add("FAILURE_RETRY", _sNumFailRetry);
    }
    return oDict;
  }

  function SetTrayDispTime(oDict) {
    if (oDict) {
      oDict.add("INSTALLED_ICON_TIME", _sTrayDispTime);
    }
    return oDict;
  }
  /*End Of - Safe Array Filling functions*/

  /*Progress Bar Related Functions*/
  function GetCountToDisplay(mMacros, sReplace) {
    var sReturn = GetPrevWorkingDOCount();
    var sCurGuid = mMacros["$$DOGUID$$"];
    var sPrevGuid = GetPrevWorkingDOGuid();
    if (sPrevGuid !== sCurGuid) {
      var iCurrDoCnt = parseInt(sReplace);
      var iTotalDoCnt = parseInt(mMacros["$$TOTALDOCOUNT$$"]);

      if ((iCurrDoCnt + 1) <= iTotalDoCnt)
        sReturn = parseInt(sReplace) + 1;
    }
    SetPrevWorkingDOCount(sReturn);
    SetPrevWorkingDOGuid(sCurGuid);
    return sReturn;
  }

  function replaceMacroInString(sStr, mMacros, paProgress) {
    var rRegEx, sReplace, sAlt, sCompMacro;
    var retArr = new Array();
    if (paProgress === "5" || paProgress === "6") sCompMacro = "$$INSTALLEDDOCOUNT$$";
    else sCompMacro = "$$DOWNLOADEDDOCOUNT$$";
    for (var mMacroKey in mMacros) {
      sReplace = mMacros[mMacroKey];
      if (mMacroKey === sCompMacro) {
        sReplace = GetCountToDisplay(mMacros, sReplace);
        if (bMac) {
          sReplace = mMacros[mMacroKey];
        }
      }
      mMacroKey = mMacroKey.replace(/\$/gi, "\\$");
      rRegEx = new RegExp(mMacroKey, "gi");
      sStr = sStr.replace(rRegEx, sReplace);
    }
    sAlt = sStr.replace(/\$\$ELLIPSESLEFT\$\$/gi, "");
    var iLen = sStr.indexOf("$$ELLIPSESLEFT$$");
    if (iLen > _sMaxCharsProgTitle) {
      var arrStr = sStr.split("$$ELLIPSESLEFT$$");
      arrStr[0] = arrStr[0].substring(0, _sMaxCharsProgTitle - 3);
      arrStr[0] += "...";
      sStr = arrStr[0] + arrStr[1];
    } else {
      sStr = sStr.replace(/\$\$ELLIPSESLEFT\$\$/gi, "");
    }
    retArr[0] = sStr;
    retArr[1] = sAlt;
    return retArr;
  }

  function ResetProgressChachedCounters() {
    ResetPrevWorkingDOGuid();
    ResetPrevWorkingDOState();
    ResetPrevWorkingDOCount();
  }

  function ResetPrevWorkingDOGuid() {
    _prev_working_do_guid = "";
  }

  function SetPrevWorkingDOGuid(sDOGuid) {
    _prev_working_do_guid = sDOGuid;
  }

  function GetPrevWorkingDOGuid() {
    return _prev_working_do_guid;
  }

  function ResetPrevWorkingDOState() {
    _prev_working_do_state = "";
  }

  function SetPrevWorkingDOState(sState) {
    _prev_working_do_state = sState;
  }

  function GetPrevWorkingDOState() {
    return _prev_working_do_state;
  }

  function ResetPrevWorkingDOCount() {
    _prev_working_do_count = "";
  }

  function SetPrevWorkingDOCount(sCount) {
    _prev_working_do_count = sCount;
  }

  function GetPrevWorkingDOCount() {
    return _prev_working_do_count;
  }

  function UpdateProgressForMac(mapReturned) {
    var sState, nPercent, sDownloadInstallCount, arrDisplay;
    var paProgress = mapReturned["$$FILESTATE$$"] > 0 ? mapReturned["$$FILESTATE$$"] : 1;
    try {
      var sTitle = mapReturned["$$FILENAME$$"];
      var sAltText = "";
      if (sTitle === "" || sTitle == undefined) {
        sState = $ss.snapins.dm.ss_dm_translate("ss_dm_res_processing", "Processing...");
        sAltText = sState;
        updateLinks($ss.snapins.dm.ss_dm_translate("ss_dm_res_processing", "Processing..."), "Downloading");
        $("#snapin_download_manager #idCancelDownloadButton").show();
      }else {
        if (GetPrevWorkingDOState() !== paProgress) ResetProgressChachedCounters();
        SetPrevWorkingDOState(paProgress);

        if (paProgress === "3" || paProgress === "4") {
          if (paProgress === "3") sState = $ss.snapins.dm.ss_dm_translate("ss_dm_macro_ui_downloading", "Downloading ");
          else sState = $ss.snapins.dm.ss_dm_translate("ss_dm_macro_ui_downloaded", "Downloaded ");

          arrDisplay = replaceMacroInString(sState, mapReturned, paProgress);
          sState = arrDisplay[0];
          sAltText = arrDisplay[1];
          nPercent = mapReturned["$$PERCENTAGEDLD$$"];
          updateLinks($ss.snapins.dm.ss_dm_translate("ss_dm_res_downloading", "Downloading... "), "Downloading");
          $("#snapin_download_manager #idCancelDownloadButton").show();
        }
        else if (paProgress === "5" || paProgress === "6") {
          if (paProgress === "5") sState = $ss.snapins.dm.ss_dm_translate("ss_dm_macro_ui_installing", "Installing ");
          else sState = $ss.snapins.dm.ss_dm_translate("ss_dm_macro_ui_installed", "Installed ");

          arrDisplay = replaceMacroInString(sState, mapReturned, paProgress);
          sState = arrDisplay[0];
          sAltText = arrDisplay[1];
          nPercent = mapReturned["$$PERCENTAGEINSTALLPREQ$$"];
          updateLinks($ss.snapins.dm.ss_dm_translate("ss_dm_res_installing", "Installing... "), "Downloading");
          //added to hide the cancel download button
          $("#snapin_download_manager #idCancelDownloadButton").hide();
        }else if(paProgress === "10" || paProgress === "11") {
        if (paProgress === "10") sState = $ss.snapins.dm.ss_dm_translate("ss_dm_macro_ui_mounting", "Mounting Disk Image ");
          else sState = $ss.snapins.dm.ss_dm_translate("ss_dm_macro_ui_mounted", "Mounted Disk Image ");

          arrDisplay = replaceMacroInString(sState, mapReturned, paProgress);
          sState = arrDisplay[0];
          sAltText = arrDisplay[1];
          nPercent = mapReturned["$$PERCENTAGEDLD$$"];
          updateLinks($ss.snapins.dm.ss_dm_translate("ss_dm_res_installing", "Installing... "), "Downloading");
          //added to hide the cancel download button
          $("#snapin_download_manager #idCancelDownloadButton").hide();
        }
      }
      DisplayProgress(sState, sAltText, nPercent);
    }
    catch(ex) {

    }
  }

  function UpdateProgress(mapReturned) {
    var sState, nPercent, sDownloadInstallCount, arrDisplay;
    var paProgress = mapReturned["$$FILESTATE$$"] > 0 ? mapReturned["$$FILESTATE$$"] : 1;
    try {
      var sTitle = mapReturned["$$FILENAME$$"];
      var sAltText = "";
      if (sTitle === "" || sTitle == undefined) {
        sState = $ss.snapins.dm.ss_dm_translate("ss_dm_res_processing", "Processing...");
        sAltText = sState;
        updateLinks($ss.snapins.dm.ss_dm_translate("ss_dm_res_processing", "Processing..."), "Downloading");
        $("#snapin_download_manager #idCancelDownloadButton").show();
      } else {
        if (GetPrevWorkingDOState() !== paProgress) ResetProgressChachedCounters();
        SetPrevWorkingDOState(paProgress);

        if (paProgress === "3" || paProgress === "4") {
          if (paProgress === "3") sState = $ss.snapins.dm.ss_dm_translate("ss_dm_macro_ui_downloading", "Downloading ");
          else sState = $ss.snapins.dm.ss_dm_translate("ss_dm_macro_ui_downloaded", "Downloaded ");

          arrDisplay = replaceMacroInString(sState, mapReturned, paProgress);
          sState = arrDisplay[0];
          sAltText = arrDisplay[1];
          nPercent = mapReturned["$$PERCENTAGEDLD$$"];
          updateLinks($ss.snapins.dm.ss_dm_translate("ss_dm_res_downloading", "Downloading... "), "Downloading");
          $("#snapin_download_manager #idCancelDownloadButton").show();
        } else if (paProgress === "5" || paProgress === "6") {
          if (paProgress === "5") sState = $ss.snapins.dm.ss_dm_translate("ss_dm_macro_ui_installing", "Installing ");
          else sState = $ss.snapins.dm.ss_dm_translate("ss_dm_macro_ui_installed", "Installed ");

          arrDisplay = replaceMacroInString(sState, mapReturned, paProgress);
          sState = arrDisplay[0];
          sAltText = arrDisplay[1];
          nPercent = mapReturned["$$PERCENTAGEINSTALLPREQ$$"];
          updateLinks($ss.snapins.dm.ss_dm_translate("ss_dm_res_installing", "Installing... "), "Downloading");
          //added to hide the cancel download button
          $("#snapin_download_manager #idCancelDownloadButton").hide();
        }
      }

      DisplayProgress(sState, sAltText, nPercent);
    } catch (oErr) {
      //do nothing...
    }
  }

  function DisplayProgress(sState, sAltText, nPercent) {
      if ($("#snapin_download_manager").length) {
          //document.getElementById("idDMStatusTxt").innerText = sState;
          $("#idDMStatusTxt").text(sState);
          $("#idDMStatusTxt").attr("title", sAltText);
          if (!isNaN(nPercent) && nPercent != undefined) {
              if (nPercent != 0) {
                  $("#idDMProgressBar").width(nPercent + "%");
              }
              else {
                  $("#idDMProgressBar").width("0%");
              }
              $("#idDMProgressTxt").text(nPercent + "%");
          }
      }
  }

  function toggleProgressBar(bShow) {
      if ($("#snapin_download_manager").length) {
          $("#idButtonFooter").css("display", bShow ? "none" : "block");
          $("#idProgressFooter").css("display", bShow ? "block" : "none");
          $("#idCheckAll").attr("disabled", true);
          $("#idDMStatusTxt").text($ss.snapins.dm.ss_dm_translate("ss_dm_res_processing", "Processing..."));
          $("#idDMProgressTxt").text("0%");
          $("#snapin_download_manager #idCancelDownloadButton").hide();
          $("#snapin_download_manager .itemselect input").css("display", bShow ? "none" : "block");
      }
  }

  function updateLinks(psInnerText, psClassName) {
    for (var sDOID in $ss.snapins.dm.APPLICATIONS) {
      if (GetFormElementVal(sDOID, "0", "SELECTED") === "1" && GetFormElementVal(sDOID, "0", "VERIFIED") !== "1" && GetFormElementVal(sDOID, "0", "WORKING") === "1") {
        $("#idStatusLink_" + sDOID).text(psInnerText).removeClass().addClass(psClassName);
      }
    }
  }
  /*End Of - Progress Bar Related Functions*/

  /*Logging related functions*/
  function getReportingState(psID) {
    var sState = GetFormElementVal(psID, "0", "SELECTED") === "0" ? GetFormElementVal(psID, "0", "sdc_sd_DefaultState") : "3";
    if (GetFormElementVal(psID, "0", "INSTALLED") === "1" && GetFormElementVal(psID, "0", "VERIFIED") === "1") {
      sState = "7";
    }
    if (GetFormElementVal(psID, "0", "VERIFIED") === "1" && GetFormElementVal(psID, "0", "VERIFIED") === "0") {
      sState = "6";
    }
    return (sState);
  }

  function logThis(psID, psVerb) {
    logThisEx(psID, 0, psVerb)
  }

  function logThisEx(psID, sPreReqSeq, psVerb) {
    var oEntry = "";
    var sData, sComment;
    try {
      switch (psVerb) {
        case VERB_LISTED:
          sData = getReportingState(psID);
          sComment = "0";
          break;
        case VERB_DEPLOYED:
          sData = 0;
          sComment = "0";
          break;
        case VERB_ERROR_CONTENT:
          sData = GetFormElementVal(psID, sPreReqSeq, "sdc_sd_VerificationType") + ":" + GetFormElementVal(psID, sPreReqSeq, "sdc_sd_VerificationPath");
          sComment = (isInstalled(psID, sPreReqSeq) ? "1" : "0");
          break;
        case VERB_INVOKED:
          sData = $ss.snapins.dm.APPLICATIONS[psID]["INVOKED"];
          sComment = "";
          break;
        case VERB_UNINSTALLED:
          sData = " ";
          sComment = " ";
          break;
      }
      oEntry = createLogEntry(psID, psVerb, sData, sComment);
      return ($ss.agentcore.reporting.Reports.Log(oEntry, "snapin_download_manager"));
    } catch (oErr) {
    }
    return (false);
  }

  function createLogEntry(psID, psVerb, psData, psComment) {
    try {
      var oEntry = $ss.agentcore.reporting.Reports.CreateLogEntry(psID, GetFormElementVal(psID, "0", "scc_version"), psVerb, "0", "0", psComment, psData, "0", "0");
      return (oEntry);
    } catch (oErr) {
    }
    return (false);
  }

  function LogInvalidContentAndSetFlags() {
    for (var sCid in $ss.snapins.dm.APPLICATIONS) {
      for (var sPreReqSeq in $ss.snapins.dm.APPLICATIONS[sCid]) {
        SetFormElementVal(sCid, sPreReqSeq, "ContentValid", true);
        if (!isContentValid(sCid, sPreReqSeq)) {
          SetFormElementVal(sCid, sPreReqSeq, "ContentValid", false);
          // Commented as it is not required for this release          
          //logThisEx(sCid, sPreReqSeq, VERB_ERROR_CONTENT); 
          if (sPreReqSeq !== "0" && (GetFormElementVal(sCid, "0", "ContentValid") === true)) {
            //Set main download also as invalid
            SetFormElementVal(sCid, "0", "ContentValid", false);
            //logThisEx(sCid, "0", VERB_ERROR_CONTENT);
          }
        }
      }
    }
  }

  function LogDeployDisplayAndSetFlags(unManned) {
    var bHasUserSeen;
    var regPath;
    for (var sDOID in $ss.snapins.dm.APPLICATIONS) {
      regPath = REG_THIS + "\\" + sDOID;

      bHasUserSeen = true;
      if ($ss.snapins.dm.IsNewlyDeployedContent(sDOID)) {
        logThis(sDOID, VERB_DEPLOYED);

        // If snapin is invoked by user and DO is Manned          
        if (!unManned && !IsDoSilent(sDOID)) {
          bHasUserSeen = false;
        }
        else {
          //Setting to 'No' so that logging for display happens for the new version
          SetFormElementVal(sDOID, "0", "ShownToUser", "no");
        }
      }
      else {
        SetFormElementVal(sDOID, "0", "ShownToUser", getRegValue(REG_HKCU, regPath, "ShownToUser"));
        if (!HasUserSeenDO(sDOID) && !unManned && !IsDoSilent(sDOID))
          bHasUserSeen = false;
      }
      if (!bHasUserSeen) {
        logThis(sDOID, VERB_LISTED);
        SetFormElementVal(sDOID, "0", "ShownToUser", "yes");
      }
    }
  }
  /*End Of - Logging related functions*/


  function SaveSelectionChanges() {
    var pOrder;
    for (var sDOID in $ss.snapins.dm.APPLICATIONS) {
      for (var sPreReqSeq in $ss.snapins.dm.APPLICATIONS[sDOID]) {
        SaveDOStateToReg(sDOID, sPreReqSeq);
      }
    }
  }

  function getRegValue(psRoot, psKey, psValue) {
    var sReturn = "";
    try {
      sReturn = _registry.GetRegValue(psRoot, psKey, psValue);
    } catch (oErr) {
      sReturn = "";
    }
    return (sReturn);
  }

  function RemoveMannedDOs() {
    for (var sDOID in $ss.snapins.dm.APPLICATIONS) {
      if (!IsDoSilent(sDOID)) {
        delete $ss.snapins.dm.APPLICATIONS[sDOID];
      }
    }
  }

  /*Silent Flow related Private functions*/
  function RemoveUnMannedDOs() {
    for (var sDOID in $ss.snapins.dm.APPLICATIONS) {
      if (IsDoSilent(sDOID)) {
        delete $ss.snapins.dm.APPLICATIONS[sDOID];
      }
    }
  }

  function RemoveSilentDOIfPrevInstalled() {
    var bDelete = false;
    for (var sDOID in $ss.snapins.dm.APPLICATIONS) {
      regPath = REG_THIS + "\\" + sDOID;
      var iExitCode = parseInt(getRegValue(REG_HKCU, regPath, "exit_code"));
      if ((!(isNaN(iExitCode)) && iExitCode === 0)) {
        var iRegVer = parseInt(getRegValue(REG_HKCU, regPath, "scc_version"));
        var iContentVer = parseInt(GetFormElementVal(sDOID, "0", "scc_version"));
        if (iContentVer <= iRegVer || isNaN(iRegVer)) {
          delete $ss.snapins.dm.APPLICATIONS[sDOID];
        }
      }
    }
  }

  function RemoveSilentDOIfInvalid() {
    var bDelete = false;
    for (var sDOID in $ss.snapins.dm.APPLICATIONS) {
      for (var iPreCounter in $ss.snapins.dm.APPLICATIONS[sDOID]) {
        if (iPreCounter !== "0") {
          var bPreReqValid = GetFormElementVal(sDOID, iPreCounter, "ContentValid");
          if (!bPreReqValid) {
            bDelete = true;
            break;
          }
        }
        else {
          if (!GetFormElementVal(sDOID, "0", "ContentValid")) {
            bDelete = true;
            break;
          }
        }
      }
      if (bDelete) {
        delete $ss.snapins.dm.APPLICATIONS[sDOID];
        bDelete = false;
      }
    }
  }

  function IsUnmannedInProgress() {
    if (!g_oClient) {
      InitializeGlobalDMClient();
      if (!g_oDictionary) CreateGlobalDictionary();
    }
    var state = GetState();
    switch (state) {
      case "3": //STATUS_DOWNLOADING
      case "4": //STATUS_DOWNLOADED
      case "5": //STATUS_INSTALLIN
      case "6": //STATUS_INSTALLED
      case "1": //STATUS_FINISHED
      case "100": //JOB EXISTS
      case "0":
        return true;
      case "200":
        return false;
      default:
        return true;
    }
    return false;
  }

  function GetSilentModeArguments() {
    var regKey = "UMArguments";
    var regPath = REG_SPRT + APP_PROVIDER + "\\" + REG_DMSPROCKET;
    var silentModeArguments = getRegValue(REG_HKLM, regPath, regKey);
    return silentModeArguments;
  }
  /*Enf of - Silent Flow related Private functions*/

  /*Retry attempt related function for Silent*/
  function ProcessReattemptAndFailure(sDOID, sPreReqSeq) {
    var iRetryAttemptsCount = parseInt(GetFormElementVal(sDOID, sPreReqSeq, "RetryAttempts"));
    if (iRetryAttemptsCount < _iSilentReattempt) {
      iRetryAttemptsCount = iRetryAttemptsCount + 1;
      SetFormElementVal(sDOID, sPreReqSeq, "RetryAttempts", (iRetryAttemptsCount + ''));
      if (!_bSilentScheduled) {
        _bSilentScheduled = true;
        ScheduleSilentInvokeJob();
      }
    } else {
      var sRegFailVal = _registry.GetRegValue(REG_HKCU, REG_THIS + "\\" + sDOID, "FailureDisplayed");
      if (sRegFailVal !== "true") {
        if (_current_silent_failed_key == null) _current_silent_failed_key = new Date().getTime();
        var sSeparator = "#";
        var sRegval = _registry.GetRegValue(REG_HKCU, REG_THIS, _current_silent_failed_key);
        if (!sRegval) {
          sSeparator = "";
          sRegval = "";
        }
        sRegval = sRegval + sSeparator + sDOID;
        _registry.SetRegValueByType(REG_HKCU, REG_THIS, _current_silent_failed_key, $ss.agentcore.constants.REG_MULTI_SZ, sRegval);
        SetFormElementVal(sDOID, sPreReqSeq, "RetryAttempts", ((iRetryAttemptsCount + 1) + ""));
      }
    }
  }

  function AddRetryAttemptToSilentDOIndex(sDOID) {
    var regPath = GetRegPathForContent(sDOID, "0");
    if ($ss.snapins.dm.IsNewlyDeployedContent(sDOID)) {
      if (IsDoSilent(sDOID)) {
        SetFormElementVal(sDOID, "0", "RetryAttempts", "0");
        if (_registry.RegValueExists(REG_HKCU, regPath, "FailureDisplayed")) {
          _registry.DeleteRegVal(REG_HKCU, regPath, "FailureDisplayed");
        }
      }
    }
  }
  /*Enf of - Retry attempt related function for Silent*/

  /*DO Saving to or reading or remvoving from registry, related functions*/
  function SaveApplicationDataToReg(psId, sPreReqSeq) {
    var regPath = GetRegPathForContent(psId, sPreReqSeq);
    var iRegVer = parseInt(getRegValue(REG_HKCU, regPath, "scc_version"));
    var iContentVer = parseInt(GetFormElementVal(psId, sPreReqSeq, "scc_version"));
    if (iContentVer > iRegVer || isNaN(iRegVer)) {
      SaveDOStaticInfoToReg(psId, sPreReqSeq);
      ResetDOStatesInForm(psId, sPreReqSeq);
    }
    SaveDOStateToReg(psId, sPreReqSeq);
    if (sPreReqSeq === "0") {
      _registry.SetRegValueByType(REG_HKCU, regPath, "ShownToUser", 1, GetFormElementVal(psId, "0", "ShownToUser"));
    }
    if (IsDoSilent(psId)) {
      _registry.SetRegValueByType(REG_HKCU, regPath, "RetryAttempts", 1, GetFormElementVal(psId, "0", "RetryAttempts"));
    }
  }

  function GetRegPathForContent(psId, sPreReqSeq) {
    var pOrder = GetFormElementVal(psId, sPreReqSeq, "PriorityOrder");
    var regPath = "";
    if (pOrder === "1" || pOrder === undefined)
      regPath = REG_THIS + "\\" + psId;
    else
      regPath = REG_THIS + "\\" + psId + "\\" + psId + "_" + pOrder;

    return regPath;
  }

  function SaveDOStateToReg(sDOID, sPreReqSeq) {
    var regPath = GetRegPathForContent(sDOID, sPreReqSeq);
    _registry.SetRegValueByType(REG_HKCU, regPath, "SELECTED", 1, GetFormElementVal(sDOID, sPreReqSeq, "SELECTED"));
    _registry.SetRegValueByType(REG_HKCU, regPath, "VERIFIED", 1, GetFormElementVal(sDOID, sPreReqSeq, "VERIFIED"));
    _registry.SetRegValueByType(REG_HKCU, regPath, "INSTALLED", 1, GetFormElementVal(sDOID, sPreReqSeq, "INSTALLED"));
    _registry.SetRegValueByType(REG_HKCU, regPath, "INVOKED", 1, GetFormElementVal(sDOID, sPreReqSeq, "INVOKED"));
    _registry.SetRegValueByType(REG_HKCU, regPath, "WORKING", 1, GetFormElementVal(sDOID, sPreReqSeq, "WORKING"));
  }

  function ResetDOStatesInForm(sDOID, sPreReqSeq) {
    SetFormElementVal(sDOID, sPreReqSeq, "INSTALLED", "0");
    SetFormElementVal(sDOID, sPreReqSeq, "SELECTED", GetFormElementVal(sDOID, sPreReqSeq, "sdc_sd_DefaultState") === "0" ? "0" : "1");
  }

  function SaveDOStaticInfoToReg(sDOID, sPreReqSeq) {
    var regPath = GetRegPathForContent(sDOID, sPreReqSeq);
    //these are static registry entries, which only changes on change of version
    if (sPreReqSeq >= 1) {
      _registry.SetRegValueByType(REG_HKCU, regPath, "sdc_sd_prerequisite_title", 1, GetFormElementVal(sDOID, sPreReqSeq, "sdc_sd_prerequisite_title"));
      _registry.SetRegValueByType(REG_HKCU, regPath, "sdc_sd_prerequisite_description", 1, GetFormElementVal(sDOID, sPreReqSeq, "sdc_sd_prerequisite_description"));
    }
    else {
      _registry.SetRegValueByType(REG_HKCU, regPath, "scc_title", 1, GetFormElementVal(sDOID, "0", "scc_title"));
      _registry.SetRegValueByType(REG_HKCU, regPath, "scc_description", 1, GetFormElementVal(sDOID, "0", "scc_description"));
    }
    _registry.DeleteRegVal("HKCU", regPath, "exit_code");
    _registry.SetRegValueByType(REG_HKCU, regPath, "scc_content_guid", 1, GetFormElementVal(sDOID, sPreReqSeq, "scc_content_guid"));
    _registry.SetRegValueByType(REG_HKCU, regPath, "scc_version", 1, GetFormElementVal(sDOID, sPreReqSeq, "scc_version"));
    _registry.SetRegValueByType(REG_HKCU, regPath, "sdc_sd_Icon", 1, GetFormElementVal(sDOID, sPreReqSeq, "sdc_sd_Icon"));
    _registry.SetRegValueByType(REG_HKCU, regPath, "sdc_sd_InstalledTitle", 1, GetFormElementVal(sDOID, sPreReqSeq, "sdc_sd_InstalledTitle"));
    _registry.SetRegValueByType(REG_HKCU, regPath, "sdc_sd_InstalledDescription", 1, GetFormElementVal(sDOID, sPreReqSeq, "sdc_sd_InstalledDescription"));
    _registry.SetRegValueByType(REG_HKCU, regPath, "sdc_sd_ErrorTitle", 1, GetFormElementVal(sDOID, sPreReqSeq, "sdc_sd_ErrorTitle"));
    _registry.SetRegValueByType(REG_HKCU, regPath, "sdc_sd_ErrorDescription", 1, GetFormElementVal(sDOID, sPreReqSeq, "sdc_sd_ErrorDescription"));
    _registry.SetRegValueByType(REG_HKCU, regPath, "sdc_sd_DefaultState", 1, GetFormElementVal(sDOID, sPreReqSeq, "sdc_sd_DefaultState"));
    _registry.SetRegValueByType(REG_HKCU, regPath, "sdc_sd_DownloadURL", 1, GetFormElementVal(sDOID, sPreReqSeq, "sdc_sd_DownloadURL"));
    _registry.SetRegValueByType(REG_HKCU, regPath, "sdc_sd_InstallationCmd", 1, GetFormElementVal(sDOID, sPreReqSeq, "sdc_sd_InstallationCmd"));
    _registry.SetRegValueByType(REG_HKCU, regPath, "sdc_sd_InstallationArguments", 1, GetFormElementVal(sDOID, sPreReqSeq, "sdc_sd_InstallationArguments"));
    _registry.SetRegValueByType(REG_HKCU, regPath, "sdc_sd_VerificationType", 1, GetFormElementVal(sDOID, sPreReqSeq, "sdc_sd_VerificationType"));
    _registry.SetRegValueByType(REG_HKCU, regPath, "sdc_sd_VerificationPath", 1, GetFormElementVal(sDOID, sPreReqSeq, "sdc_sd_VerificationPath"));
    _registry.SetRegValueByType(REG_HKCU, regPath, "sdc_sd_CommandToRun", 1, GetFormElementVal(sDOID, sPreReqSeq, "sdc_sd_CommandToRun"));
    _registry.SetRegValueByType(REG_HKCU, regPath, "sdc_sd_size", 1, GetFormElementVal(sDOID, sPreReqSeq, "sdc_sd_size"));
    _registry.SetRegValueByType(REG_HKCU, regPath, "sdc_sd_requiresRestart", 1, GetFormElementVal(sDOID, sPreReqSeq, "sdc_sd_requiresRestart"));
    _registry.SetRegValueByType(REG_HKCU, regPath, "sdc_sd_checksum", 1, GetFormElementVal(sDOID, sPreReqSeq, "sdc_sd_checksum"));
    _registry.SetRegValueByType(REG_HKCU, regPath, "sdc_sd_InstallOptions", 1, GetFormElementVal(sDOID, sPreReqSeq, "sdc_sd_InstallOptions"));
    _registry.SetRegValueByType(REG_HKCU, regPath, "sdc_sd_silent", 1, GetFormElementVal(sDOID, sPreReqSeq, "sdc_sd_silent"));
    _registry.SetRegValueByType(REG_HKCU, regPath, "sdc_sd_ErrorSrcID", 1, GetFormElementVal(sDOID, sPreReqSeq, "sdc_sd_ErrorSrcID"));
    //Constant for a version of the content
    _registry.SetRegValueByType(REG_HKCU, regPath, "ContentValid", 1, GetFormElementVal(sDOID, sPreReqSeq, "ContentValid"));
  }

  function ModifyStateAndSaveDataToReg(unManned, bSilentTest) {
    var iState = parseInt(GetState());
    for (var sDOID in $ss.snapins.dm.APPLICATIONS) {
      for (var sPreReqSeq in $ss.snapins.dm.APPLICATIONS[sDOID]) {
        var regPath = GetRegPathForContent(sDOID, sPreReqSeq);
        var bInstalled;
        // if the DO GUID entry exists in the registry
        if (getRegValue(REG_HKCU, regPath, "scc_content_guid") == sDOID) {
          bInstalled = isInstalled(sDOID, sPreReqSeq);
          if (GetFormElementVal(sDOID, sPreReqSeq, "sdc_sd_DefaultState") != "2") {
            SetFormElementVal(sDOID, sPreReqSeq, "SELECTED", getRegValue(REG_HKCU, regPath, "SELECTED"));
          } else {
            SetFormElementVal(sDOID, sPreReqSeq, "SELECTED", "1");
          }
          ReadDOStateFromReg(sDOID, sPreReqSeq, regPath);
          SetFormElementVal(sDOID, sPreReqSeq, "RetryAttempts", getRegValue(REG_HKCU, regPath, "RetryAttempts"));

          // Commented as it is not required for this release as per PM 
          /*if ($ss.snapins.dm.APPLICATIONS[sDOID][sPreReqSeq]["VERIFIED"] == 1 && !bInstalled) {
          logThis(sDOID, VERB_UNINSTALLED);
          }*/

          if (GetFormElementVal(sDOID, sPreReqSeq, "INSTALLED") == "1" && bInstalled) {
            SetFormElementVal(sDOID, sPreReqSeq, "VERIFIED", "1");
          }
          else if (bInstalled) {
            SetFormElementVal(sDOID, sPreReqSeq, "VERIFIED", "1");
            SetFormElementVal(sDOID, sPreReqSeq, "WORKING", "0");
          }
          else {
            SetFormElementVal(sDOID, sPreReqSeq, "VERIFIED", "0");
            if (!(iState >= 3 && iState <= 9))
              SetFormElementVal(sDOID, sPreReqSeq, "WORKING", "0");
          }

        } else {
          bInstalled = isInstalled(sDOID, sPreReqSeq);
          if (bInstalled) {
            SetFormElementVal(sDOID, sPreReqSeq, "VERIFIED", "1");
          }
        }
        AddRetryAttemptToSilentDOIndex(sDOID);
        if (unManned && sPreReqSeq === "0" && (IsDoSilent(sDOID)) && !bInstalled && bSilentTest) {
          ProcessReattemptAndFailure(sDOID, sPreReqSeq);
        }
        SaveApplicationDataToReg(sDOID, sPreReqSeq);
      } //end of for - sPreReqSeq
    }
  }

  function HandleUndeployedDOContents() {
    var aKeys, sKey, sKeys, regPath;
    sKeys = _registry.EnumRegKey(REG_HKCU, REG_THIS, ",");

    //Handle Different cases for undeployed contents
    if (sKeys != "") {
      aKeys = sKeys.split(",");
      var nI, nMi;
      nMi = aKeys.length;
      for (nI = 0; nI < nMi; nI++) {
        sKey = aKeys[nI];
        if (!$ss.snapins.dm.APPLICATIONS[sKey] && sKey != "") {
          //Enters only sKey is undeployed
          CreateDOObject(sKey);
          $ss.snapins.dm.APPLICATIONS[sKey]["0"] = new Object();
          regPath = REG_THIS + "\\" + sKey;
          SetFormElementVal(sKey, "0", "sdc_sd_VerificationType", getRegValue(REG_HKCU, regPath, "sdc_sd_VerificationType"));
          SetFormElementVal(sKey, "0", "sdc_sd_VerificationPath", getRegValue(REG_HKCU, regPath, "sdc_sd_VerificationPath"));
          var sWorking = getRegValue(REG_HKCU, regPath, "WORKING");
          var sSilent = getRegValue(REG_HKCU, regPath, "sdc_sd_silent");

          //Different cases for undeployed
          if ((!isInstalled(sKey, "0") && (sWorking !== "1"))) {
            //Only if the Application is (not working and notinstalled) or (its silent) remove registry entry
            _registry.DeleteRegKey(REG_HKCU, regPath);
            delete $ss.snapins.dm.APPLICATIONS[sKey];
          } else if (!isInstalled(sKey, "0") && (sWorking === "1")) {
            ReadDoFromReg(sKey);  // If its not installed but it is slected for download and intall, display on UI
          } else if (isInstalled(sKey, "0")) {
            if (_bDisplayUndeployed)
              ReadDoFromReg(sKey); // Creates the DO Object for Display
            else
              delete $ss.snapins.dm.APPLICATIONS[sKey]; // Do not display on UI
          }
          //End of Different cases for undeployed

        }
      }
    } //End of If undeployed
  }

  function ReadDoFromReg(sKey) {
    $ss.snapins.dm.APPLICATIONS[sKey] = new Object();
    var childKeys = _registry.EnumRegKey(REG_HKCU, REG_THIS + "\\" + sKey, ",");
    if (childKeys != "") {
      var sPreReqSeq;
      var arrPriority = new Array();
      var arrChildKey = new Array();
      arrChildKey = childKeys.split(",");
      for (sPreReqSeq = 0; sPreReqSeq < arrChildKey.length; sPreReqSeq++) {
        var key = arrChildKey[sPreReqSeq];
        if (key != "") {
          var keyArr = key.split("_");
          arrPriority[sPreReqSeq] = keyArr[1];
          regPath = REG_THIS + "\\" + sKey + "\\" + key;
          if (!$ss.snapins.dm.APPLICATIONS[sKey][sPreReqSeq + 1] && sKey != "")
            CopyDeletedApplicationData(sKey, sPreReqSeq + 1, arrPriority[sPreReqSeq], regPath);
        }
      }
    } //Finished Handling Prerequisites
    regPath = REG_THIS + "\\" + sKey;

    CopyDeletedApplicationData(sKey, "0", "1", regPath);
  }

  function compare(a, b) {
    return (b - a);
  }

  function ReadDOStateFromReg(sDOID, sPreReqSeq, regPath) {
    SetFormElementVal(sDOID, sPreReqSeq, "INSTALLED", getRegValue(REG_HKCU, regPath, "INSTALLED"));
    SetFormElementVal(sDOID, sPreReqSeq, "SELECTED", getRegValue(REG_HKCU, regPath, "SELECTED"));
    SetFormElementVal(sDOID, sPreReqSeq, "VERIFIED", getRegValue(REG_HKCU, regPath, "VERIFIED"));
    SetFormElementVal(sDOID, sPreReqSeq, "INVOKED", getRegValue(REG_HKCU, regPath, "INVOKED"));
    SetFormElementVal(sDOID, sPreReqSeq, "WORKING", getRegValue(REG_HKCU, regPath, "WORKING"));
  }

  //Copy the registry values if the application is installed display it else remove the regestry entry
  function CopyDeletedApplicationData(sKey, sPreReqSeq, pOrder, regPath) {
    $ss.snapins.dm.APPLICATIONS[sKey][sPreReqSeq] = new Object();
    SetFormElementVal(sKey, sPreReqSeq, "PriorityOrder", pOrder);
    if (sPreReqSeq >= 1) {
      SetFormElementVal(sKey, sPreReqSeq, "sdc_sd_prerequisite_title", getRegValue(REG_HKCU, regPath, "sdc_sd_prerequisite_title"));
      SetFormElementVal(sKey, sPreReqSeq, "sdc_sd_prerequisite_description", getRegValue(REG_HKCU, regPath, "sdc_sd_prerequisite_description"));
    }
    SetFormElementVal(sKey, sPreReqSeq, "scc_content_guid", getRegValue(REG_HKCU, regPath, "scc_content_guid"));
    SetFormElementVal(sKey, sPreReqSeq, "scc_version", getRegValue(REG_HKCU, regPath, "scc_version"));
    SetFormElementVal(sKey, sPreReqSeq, "scc_description", getRegValue(REG_HKCU, regPath, "scc_description"));
    SetFormElementVal(sKey, sPreReqSeq, "scc_title", getRegValue(REG_HKCU, regPath, "scc_title"));
    SetFormElementVal(sKey, sPreReqSeq, "sdc_sd_Icon", getRegValue(REG_HKCU, regPath, "sdc_sd_Icon"));
    SetFormElementVal(sKey, sPreReqSeq, "sdc_sd_InstalledTitle", getRegValue(REG_HKCU, regPath, "sdc_sd_InstalledTitle"));
    SetFormElementVal(sKey, sPreReqSeq, "sdc_sd_InstalledDescription", getRegValue(REG_HKCU, regPath, "sdc_sd_InstalledDescription"));
    SetFormElementVal(sKey, sPreReqSeq, "sdc_sd_ErrorTitle", getRegValue(REG_HKCU, regPath, "sdc_sd_ErrorTitle"));
    SetFormElementVal(sKey, sPreReqSeq, "sdc_sd_ErrorDescription", getRegValue(REG_HKCU, regPath, "sdc_sd_ErrorDescription"));
    SetFormElementVal(sKey, sPreReqSeq, "sdc_sd_DefaultState", getRegValue(REG_HKCU, regPath, "sdc_sd_DefaultState"));
    SetFormElementVal(sKey, sPreReqSeq, "sdc_sd_DownloadURL", getRegValue(REG_HKCU, regPath, "sdc_sd_DownloadURL"));
    SetFormElementVal(sKey, sPreReqSeq, "sdc_sd_InstallationCmd", getRegValue(REG_HKCU, regPath, "sdc_sd_InstallationCmd"));
    SetFormElementVal(sKey, sPreReqSeq, "sdc_sd_InstallationArguments", getRegValue(REG_HKCU, regPath, "sdc_sd_InstallationArguments"));
    SetFormElementVal(sKey, sPreReqSeq, "sdc_sd_VerificationType", getRegValue(REG_HKCU, regPath, "sdc_sd_VerificationType"));
    SetFormElementVal(sKey, sPreReqSeq, "sdc_sd_VerificationPath", getRegValue(REG_HKCU, regPath, "sdc_sd_VerificationPath"));
    SetFormElementVal(sKey, sPreReqSeq, "sdc_sd_CommandToRun", getRegValue(REG_HKCU, regPath, "sdc_sd_CommandToRun"));
    SetFormElementVal(sKey, sPreReqSeq, "sdc_sd_size", getRegValue(REG_HKCU, regPath, "sdc_sd_size"));
    SetFormElementVal(sKey, sPreReqSeq, "sdc_sd_requiresRestart", getRegValue(REG_HKCU, regPath, "sdc_sd_requiresRestart"));
    SetFormElementVal(sKey, sPreReqSeq, "sdc_sd_checksum", getRegValue(REG_HKCU, regPath, "sdc_sd_checksum"));
    SetFormElementVal(sKey, sPreReqSeq, "sdc_sd_InstallOptions", getRegValue(REG_HKCU, regPath, "sdc_sd_InstallOptions"));
    SetFormElementVal(sKey, sPreReqSeq, "sdc_sd_silent", getRegValue(REG_HKCU, regPath, "sdc_sd_silent"));
    SetFormElementVal(sKey, sPreReqSeq, "sdc_sd_ErrorSrcID", getRegValue(REG_HKCU, regPath, "sdc_sd_ErrorSrcID"));
    SetFormElementVal(sKey, sPreReqSeq, "ContentValid", getRegValue(REG_HKCU, regPath, "ContentValid"));
    SetFormElementVal(sKey, sPreReqSeq, "ShownToUser", getRegValue(REG_HKCU, regPath, "ShownToUser"));

    ReadDOStateFromReg(sKey, sPreReqSeq, regPath);
  }
  /*Enf Of - DO Saving to or reading or remvoving from registry, related functions*/

  /*Sceduler related Functionality*/
  function CreateDictArgs() {
    var odict = null;
    if (bMac) {
    oDict = new Dictionary();
    }else {
       odict = $ss.agentcore.utils.ui.CreateObject("Scripting.Dictionary", false);
    }
    odict.RemoveAll();
    if (odict) {
      odict.add("SCHEDULE_NAME", scheduleJobName);
      odict.add("SCHEDULE_START_TIME", "NOW");
      odict.add("SCHEDULE_INTERVAL", _sSilentScheduleTime);
      odict.add("SCHEDULE_RECURRENCE", "1");
      odict.add("SCHEDULE_FIRST_TRIGGER", "false");
      odict.add("SCHEDULE_APPLICATION", $ss.agentcore.dal.ini.GetPath("EXE_PATH"));
      odict.add("SCHEDULE_ARGUMENTS", GetSilentModeArguments());
      odict.add("SCHEDULE_FORCE_STOP_ON_REBOOT", "false");
      odict.add("PROVIDER_ID", APP_PROVIDER);
      return odict;
    }
    else {
      return false;
    }
  }

  function InitializeSchedulerClient() {
    try {
      schedulerClient = $ss.agentcore.utils.ui.CreateObject("Sprttaskclient.Scheduler", false);
    }
    catch (oErr) {
      schedulerClient = false;
    }
  }

  function InitializeRebootVerifier() {
    if (bMac) {
      rebootVerifier = false;
      return;
    }
    try {
      rebootVerifier = $ss.agentcore.utils.ui.CreateObject("Sprttaskclient.RegistryOp", false);
    }
    catch (oErr) {
      rebootVerifier = false;
    }
  }

  function DeleteOldSilentInvokeJob() {
    if (!schedulerClient) InitializeSchedulerClient();
    var dictArgs = CreateDictArgs();
    if ((!schedulerClient) || (!dictArgs)) return;
    var ret = schedulerClient.DeleteScheduleJob(dictArgs.keys(), dictArgs.Items());
  }

  function ScheduleSilentInvokeJob() {
    if (!schedulerClient) InitializeSchedulerClient();
    var dictArgs = CreateDictArgs();
    if ((!schedulerClient) || (!dictArgs)) return;
    var ret = schedulerClient.AddScheduleJob(dictArgs.keys(), dictArgs.Items());
  }
  /*Enf of - Sceduler related Functionality*/

  /*DO Content XML Parsing functions*/
  function GetContentField(oJson, attributeName, attributeValue) {
    var returnValue = "";
    try {
      var xmlPath = _config.ExpandAllMacros("%CONTENTPATH%") + "\\sprt_download\\" + oJson.cid + "." + oJson.version + "\\" + oJson.cid + "." + oJson.version + ".xml";
      var domXml = null;
      if (bMac) {
        domXml = domXml = $ss.agentcore.dal.xml.LoadXML(xmlPath, false);
      }
      else {
        domXml = domXml = $ss.agentcore.dal.xml.LoadXML(xmlPath, true);
      }
      var sXPath = "/sprt//content-set//content//field-set";
      if (attributeName != undefined) {
        sXPath = sXPath + "//field[@name='" + attributeName + "']";
        if (attributeValue != undefined) {
          sXPath = sXPath + "//value[@name='" + attributeValue + "']";
        }
      }
      var iconNode = $ss.agentcore.dal.xml.GetNodes(sXPath, "", domXml);
      if (iconNode.length > 0) {
        returnValue = $ss.agentcore.dal.xml.GetNodeText(iconNode[0].childNodes[0]);
      }
    } catch (ex) {

    }
    return returnValue;
  }

  function getPrerequisiteArr(filePath) {
    var arrDelimitor = "##";
    var xmlDoc = $ss.agentcore.dal.xml;
    var tempDOM = xmlDoc.LoadXML(filePath, false, null);
    var xpathNode = "//sprt/content-set/content/field-set/field[@name='hiddenprerequisite_order']/value[@name='sccf_field_value_char']";
    var node = xmlDoc.GetNode(xpathNode, "", tempDOM);
    if (node === null) return null;
    var nodeValue = node.nodeTypedValue.toString();
    if (nodeValue === "") return null;
    var preRequisiteArr = nodeValue.split(arrDelimitor);
    return preRequisiteArr
  }
  /*End Of - DO Content XML Parsing functions*/

  /*Content XMl parsing and List building related functions*/
  function getFieldFromPropertySet(oJson, pnDx) {
    if (oJson) {
      try {
        var oObj = oJson.content_set[0].content[0].property_set[0].property[pnDx];
        return (oObj);
      } catch (oErr) {

      }
    }
    return (false);
  }

  function getFieldFromFieldSet(oJson, pnDx) {
    if (oJson) {
      try {
        var oObj = oJson.content_set[0].content[0].field_set[0].field[pnDx];
        return (oObj);
      } catch (oErr) {

      }
    }
    return (false);
  }

  function getFieldText(oNode) {
    if (oNode) {
      try {
        if (oNode.Text) {
          sRet = oNode.Text;
          return (sRet);
        }
      } catch (oErr) {
      }
    }
    return ("");
  }

  function getValueFromField(poObj, psNameOfField) {
    if (poObj && psNameOfField != "") {
      try {
        var oNode = poObj.value.getNodeByAttribute("name", psNameOfField);
        var sRet = getFieldText(oNode);
        return (sRet);
      } catch (oErr) {

      }
    }
    return ("");
  }

  function getFieldName(poObj, psDefaultName) {
    var sDefault = psDefaultName != "" ? psDefaultName : "UNKNOWN";
    var sRet = sDefault;
    if (poObj) {
      try {
        var sTmp = poObj.name;
        if (sTmp != undefined) {
          sRet = sTmp;
        }
      } catch (oErr) {
        sRet = sDefault;
      }
    }
    return (sRet);
  }

  function cDownloadListing(oJson, psParentPath, pOrder, bType) {
    try {
      if (oJson) {
        var nI = -1;
        var oTmp;
        var sTmpName;
        while (oTmp = getFieldFromPropertySet(oJson, ++nI)) {
          sTmpName = getFieldName(oTmp, "UNKNOWN");
          this[sTmpName] = getFieldText(oTmp);
        }

        nI = -1;
        this["PriorityOrder"] = pOrder;
        while (oTmp = getFieldFromFieldSet(oJson, ++nI)) {
          sTmpName = getFieldName(oTmp, "UNKNOWN");
          if (bType) {
            if (sTmpName.match(":::" + pOrder) === null) {
              if(sTmpName !== "sdc_sd_Icon" && sTmpName !== "Configuration") {//to fix the issue of blob, where image is not getting updates to :::1 on server
                continue;
              }
            }
            sTmpName = sTmpName.replace(":::" + pOrder, "");
          }
          if (sTmpName == "sdc_sd_Icon") {
            this[sTmpName] = buildPathString(psParentPath, getValueFromField(oTmp, "sccfb_field_value_blob"));
          } else {
            if(sTmpName !== "Configuration"){
              this[sTmpName] = getValueFromField(oTmp, "sccf_field_value_char");
            }else{
              this[sTmpName] = getValueFromField(oTmp, "sccfb_field_value_blob");
            }
          }
        }
        if (this["sdc_sd_DefaultState"]) {
          this["SELECTED"] = this["sdc_sd_DefaultState"] == "1" || this["sdc_sd_DefaultState"] == "2" ? "1" : "0";
        } else {
          this["SELECTED"] = "0";
        }

        this["sdc_sd_InstalledTitle"] = this["sdc_sd_InstalledTitle"] ? this["sdc_sd_InstalledTitle"] : this["scc_title"];
        this["sdc_sd_InstalledDescription"] = this["sdc_sd_InstalledDescription"] ? this["sdc_sd_InstalledDescription"] : this["scc_description"];
        this["sdc_sd_ErrorTitle"] = this["sdc_sd_ErrorTitle"] ? this["sdc_sd_ErrorTitle"] : this["scc_title"];
        this["sdc_sd_ErrorDescription"] = this["sdc_sd_ErrorDescription"] ? this["sdc_sd_ErrorDescription"] : this["scc_description"];
        this["VERIFIED"] = "0";
        this["INSTALLED"] = "0";
        this["INVOKED"] = "0";
        this["WORKING"] = "0";
        this["RetryAttempts"] = "0";
        this["ContentValid"] = "0";
        if (pOrder === "1") {
          this["ShownToUser"] = "no";
        }


        if (this["sdc_sd_checksum"] === undefined || this["sdc_sd_checksum"] === "")
          this["sdc_sd_checksum"] = "-1";

        if ((this["sdc_sd_InstallOptions"] === undefined) || this["sdc_sd_InstallOptions"] !== "1")
          this["sdc_sd_InstallOptions"] = "0";
      }
      if(this["Configuration"] != undefined) {
        var sConfigXML = this["Configuration"];
        var sConfigXMLPath = buildPathString(psParentPath,sConfigXML);
        var _file = $ss.agentcore.dal.file;
        if (_file.FileExists(sConfigXMLPath)){
         oXMLObj = _xml.LoadXML(sConfigXMLPath, false);
         UpdateFieldsFromXML(oXMLObj, this);
        }
      }
      
    } catch (oErr) {

    }
  }
  /*Enf Of - Content XMl parsing and List building related functions*/

  function UpdateFieldsFromXML(oXMLObj, oContentObj){
    if (typeof (oXMLObj) === "undefined")  return;
    var sUserPrefLangCode,sXPath
    var sTitleId = "'title'" , sDescriptionId = "'description'" , sInstalledTitleId = "'InstalledTitle'" , sInstalledDescId = "'InstalledDescription'" , sErrorTitleId = "'ErrorTitle'" , sErrorDescId = "'ErrorDescription'";
    sUserPrefLangCode = $ss.agentcore.utils.GetLanguage();
    sXPath = "/sprt/resources/lang[@code='" + sUserPrefLangCode + "']";

    if(oXMLObj.selectNodes(sXPath).length == 0){
      sXPath = "/sprt/resources/lang[@code='en']";
      if (oXMLObj.selectNodes(sXPath).length == 0) return false;
    }
    sXPath += "/res";
    
    oContentObj["scc_title"] = updateFieldValue(oXMLObj, sXPath, "id", sTitleId, oContentObj["scc_title"]);
    oContentObj["scc_description"] = updateFieldValue(oXMLObj, sXPath, "id", sDescriptionId, oContentObj["scc_description"]);
    oContentObj["sdc_sd_InstalledTitle"] = updateFieldValue(oXMLObj, sXPath, "id", sInstalledTitleId ,oContentObj["sdc_sd_InstalledTitle"]);
    oContentObj["sdc_sd_InstalledDescription"] = updateFieldValue(oXMLObj, sXPath, "id", sInstalledDescId, oContentObj["sdc_sd_InstalledDescription"]);
    oContentObj["sdc_sd_ErrorTitle"] = updateFieldValue(oXMLObj, sXPath, "id", sErrorTitleId ,oContentObj["sdc_sd_ErrorTitle"]);
    oContentObj["sdc_sd_ErrorDescription"] = updateFieldValue(oXMLObj, sXPath, "id", sErrorDescId ,oContentObj["sdc_sd_ErrorDescription"]);
  }

  function updateFieldValue(oXml, xPath, sAttribName, sAttribVal, sDefaultVal){
    var retVal = sDefaultVal;
    var oXPath = oXml.selectNodes(xPath + "[@" + sAttribName + "=" + sAttribVal + "]");
    if (oXPath.length != 0)
      retVal = oXPath[0].text.trim();
    return retVal;
  }

  /*Install verification*/
  function isInstalled(psId, pOrder) {
    var result = false
    if (GetFormElementVal(psId, pOrder, "sdc_sd_VerificationPath") && GetFormElementVal(psId, pOrder, "sdc_sd_VerificationPath") != "") {
      try {
        result = verifyIfInstalledAlready(GetFormElementVal(psId, pOrder, "sdc_sd_VerificationType"), GetFormElementVal(psId, pOrder, "sdc_sd_VerificationPath"));
      }
      catch (err) {
        result = verifyInstallation(psId, pOrder);
      }
    }
    return result;
  }

  function verifyInstallation(psId, pOrder) {
    try {
      if (GetFormElementVal(psId, pOrder, "sdc_sd_VerificationPath") && GetFormElementVal(psId, pOrder, "sdc_sd_VerificationPath") != "") {
        switch (GetFormElementVal(psId, pOrder, "sdc_sd_VerificationType")) {
          case "0":
            var filePath = expandMacro(GetFormElementVal(psId, pOrder, "sdc_sd_VerificationPath"))
            filePath = CmdTokenToRawToken(filePath);
            return (fileExists(filePath));
            break;
          case "1":
            var aPath = GetFormElementVal(psId, pOrder, "sdc_sd_VerificationPath").split("\\");
            if (aPath.length == 1) {
              aPath = psPath.split("/");
            }
            var sRoot = aPath[0];
            var sValue = aPath[aPath.length - 1];
            var sPath = aPath.slice(1, aPath.length - 1).join("\\");
            return (regValueExists(sRoot, sPath, sValue));
            break;
        }
        return (true);
      } else {
        return (true);
      }
    } catch (err) { }
    return false;
  }

  function verifyIfInstalledAlready(locationType, dataString) {
    var location;
    if (locationType === "0")
      location = "FileSystem";
    else
      location = "Registry";
    var wildCardChar = "*";
    //var dataString = "HKLM\\Software\\Symantec\\Common Client##Version##104.1.13.20";
    //dataString = "%WinDir%\\digest.dll##Version##.##6.*";
    var dataArr = dataString.split(/\#\#/);
    var splitOperator = ".";
    var requiredVersion = "";
    var arrReqVer, availableVersion, arrAvailVer;

    if (dataArr.length != 3) throw "Error: Invalid verification string";
    requiredVersion = dataArr[2];
    arrReqVer = requiredVersion.split(new RegExp("\\" + splitOperator));
    if (location === "Registry") {
      var dalObj = $ss.agentcore.dal.registry;
      var regPath = dataArr[0];
      var regPathArr = regPath.split("\\");
      var regTree = regPathArr[0];
      regPath = regPathArr.slice(1, regPathArr.length).join("\\");
      var regKey = dataArr[1];
      if (!dalObj.RegValueExists(regTree, regPath, regKey)) return false;
      availableVersion = dalObj.GetRegValue(regTree, regPath, regKey);
    }
    else {
      var fso = $ss.agentcore.dal.file.GetFileSystemObject();
      var filePath = expandMacro(dataArr[0]);
      filePath = CmdTokenToRawToken(filePath);
      if (!fso.FileExists(filePath)) return false;
      switch (dataArr[1].toLowerCase()) {
        case "version":
        case "file version":
        case "fileversion":
          availableVersion = fso.GetFileVersion(filePath);
          break;
        default:
          return false;
      }
    }
    if (availableVersion.match(new RegExp("\\" + splitOperator)) == -1) return false;
    arrAvailVer = availableVersion.split(new RegExp("\\" + splitOperator));

    //Append wild card character if the version strings are of unequal length
    var iAvailLen = arrAvailVer.length, iReqLen = arrReqVer.length;
    if (iAvailLen > iReqLen && arrReqVer[iReqLen - 1] !== wildCardChar) arrReqVer[iReqLen] = wildCardChar;
    else if (iReqLen > iAvailLen && arrAvailVer[iAvailLen - 1] !== wildCardChar) arrAvailVer[iAvailLen] = wildCardChar;

    return CompareValues(arrReqVer, arrAvailVer, wildCardChar);
  }

  function CompareValues(arrReqVer, arrAvailVer, wildCardChar) {
    var result = true;
    for (var i = 0; i < arrReqVer.length; i++) {
      if (arrReqVer[i] === wildCardChar) break;
      if (arrAvailVer[i] === wildCardChar) break;
      if (parseInt(arrReqVer[i]) < parseInt(arrAvailVer[i])) break;
      if (parseInt(arrReqVer[i]) > parseInt(arrAvailVer[i])) {
        result = false;
        break;
      }
    }
    return result;
  }
  /*End Of - Install verification*/


  function checkComponents() {
    $ss.snapins.dm.g_bShowing = true;
    $ss.snapins.dm.g_sNoShowReason = "";
    var sMessage, sReason;

    if (count($ss.snapins.dm.APPLICATIONS[0]) > 5 || count($ss.snapins.dm.APPLICATIONS) == 0) {
      sMessage = $ss.snapins.dm.ss_dm_translate("ss_dm_res_errnocontent_txt", "There are currently no downloads available.  Please check back at a later time.");
      sReason = $ss.snapins.dm.ss_dm_translate("ss_dm_res_errnocontent_rsn", "no content");
      $ss.snapins.dm.g_bShowing = false;
    }

    try {
      if (!g_oClient) InitializeGlobalDMClient();
      if (!g_oDictionary) CreateGlobalDictionary();
    } catch (oErr) {
      g_oClient = false;
    }

    if (!g_oClient) {
      $ss.snapins.dm.g_bShowing = false;
      sMessage = $ss.snapins.dm.ss_dm_translate("ss_dm_res_errnoclient_txt", "The Download Manager is unable to run due to an error.  Please check back at a later time.");
      sReason = $ss.snapins.dm.ss_dm_translate("ss_dm_res_errnoclient_rsn", "Error: DMClient unavailable");
    }

    try {

      try {
        if (!$ss.agentcore.utils.IsSprtCmdRunning())
          $ss.agentcore.utils.StartSprtCmd(true);
      }
      catch (oErr) {
        // BCONT does use STA model & does not allow incoming calls if it does have placed outgoing call.
        // hence, it does not allow a call used for checking if sprtCmd is runnin or not, when mini bcont is displayed
        // Mini bcont anyway wont be displayed if sprtcmd is not running. Hence could ignore the exception -2147417843

        if (oErr.number != -2147417843)
          throw (oErr);
      }

      var attempt = 0;

      //CheckService would 
      //returns "0" if service is busy, even after waiting for 5 secs.
      //returns "1" if service is available.
      //returns "2" if service is not available.
      //It will be waiting only if the service is not yet available.

      while (g_oClient.CheckService(APP_PROVIDER) == "2" && attempt < 6) {
        attempt++;
        $ss.agentcore.utils.Sleep(500);
        // give some time to establish sprockets, wait 'UPTO' 500*6=3000 milisecs
      }

      if (!(g_oClient.CheckService(APP_PROVIDER) == "1")) {
        $ss.snapins.dm.g_bShowing = false;
        sMessage = $ss.snapins.dm.ss_dm_translate("ss_dm_res_errnomonitor_txt", "The Download Manager is unable to run due to an error.  Please check back at a later time.");
        sReason = $ss.snapins.dm.ss_dm_translate("ss_dm_res_errnomonitor_rsn", "Error: DMMonitor unavailable");
      }
    } catch (oErr) {
      $ss.snapins.dm.g_bShowing = false;
      sMessage = $ss.snapins.dm.ss_dm_translate("ss_dm_res_errrecgmonitor_txt", "The Download Manager is unable to run due to an error.  Please check back at a later time.");
      sReason = $ss.snapins.dm.ss_dm_translate("ss_dm_res_errrecgmonitor_rsn", "Error: DMMonitor not recognized");
    }

    if (!$ss.snapins.dm.g_bShowing) {
      $ss.snapins.dm.g_sNoShowReason = "<div class=\"clErr\">" + sMessage + "</div><div class=\"clErrReason\">[" + sReason + "]</div>";
      return (false);
    }

    return (true);
  }

  /*Common DM Functions*/
  function IsDoSilent(sDOID) {
    if (GetFormElementVal(sDOID, "0", "sdc_sd_silent") === "yes") {
      return true;
    }
    else
      return false;
  }

  function HasUserSeenDO(sDOID) {
    var regPath;
    regPath = REG_THIS + "\\" + sDOID;

    if (getRegValue(REG_HKCU, regPath, "ShownToUser") === "yes")
      return true;
    else
      return false;
  }

  function isContentValid(psId, pOrder) {
    var dmApp = $ss.snapins.dm.APPLICATIONS[psId][pOrder];
    if ((dmApp["sdc_sd_VerificationType"] === "1") && !_utils.Is64BitOS()) {
      if ((dmApp["sdc_sd_VerificationPath"].search(/__%%REG_VIEW_64%%/i)) !== -1) {
        return false;
      }
    }
    return true;
  }

  function showError() {
    SnapinDownloadManagerController.dispatch('snapin_download_manager', 'error');
  }

  function InitializeGlobalDMClient() {
    if (bMac) {
      try {
        g_oClient = new $ss.snapin.dm.DMClient();
      } catch (e) {
        g_oClient = false;
      }
    }
    else {
    try {
      g_oClient = $ss.agentcore.utils.ui.CreateObject("SupportSoftDM.client", false);
    } catch (e) {
      g_oClient = false;
    }
  }
  }

  function GetState() {
    var sRet = "0";
    if (!g_oClient) InitializeGlobalDMClient();
    if (g_oClient) {
      try {
        var oDictionary = null;
        if (bMac) {
          oDictionary = NewDictionary();
        }else {
          oDictionary = new ActiveXObject("Scripting.Dictionary");
        }
        oDictionary.add("JOB_NAME", JOB_NAME);
        oDictionary.add("PROVIDER_ID", APP_PROVIDER);
        var ret = g_oClient.GetDMState(oDictionary.Keys(), oDictionary.Items());
        if (bMac) {
          return ret;
        }
        var sa = new VBArray(ret);
        var test = sa.toArray();
        var myMap = CreateMap(test);
        oDictionary.RemoveAll();
        switch (myMap["RETURN_VALUE"]) {
          case "0":
            sRet = myMap["STATE_VALUE"];
            break;
          case "1":  //Job already exists.
            sRet = "100";
            break;
          case "-1":
            sRet = "200";
            break;
          default:
            break;
        }
        //sRet = g_oClient.GetState(JOB_NAME, APP_PROVIDER);
      } catch (oErr) {
        sRet = "0";
      }
    }
    return (sRet);
  }

  function GetFileLocationForDO(sCid, sVer) {
    return buildPathString(_DOWNLOADS_, sCid + "." + sVer);
  }
  /*End Of - Common DM Functions*/

  /*Content Form fill up functions*/
  function BuildContentForm() {
    var oContents = $ss.agentcore.dal.content.GetContentsByAnyType(["sprt_download"]);
    oContents = $ss.snapins.dm.SortTheContent(oContents);

    for (var i = 0; i < oContents.length; i++) {
      var sCid = oContents[i]["cid"];
      var sVer = oContents[i]["version"];
      var bDOType = $ss.snapins.dm.IsContentFormatNew(oContents[i]);
      BuildDOObject(sCid, sVer, "0", "1", bDOType);
      //$ss.snapins.dm.APPLICATIONS[sCid]["0"] = new cDownloadListing(oObj, fileLocation, "1", bDOType);
      BuildPrerequisites(sCid, sVer, i);
    }
  }

  function BuildPrerequisites(sCid, sVer, i) {
    var filePath = GetFileLocationForDO(sCid, sVer) + "\\" + sCid + "." + sVer + ".xml";
    var arrPreRequisite = getPrerequisiteArr(filePath);
    if (arrPreRequisite !== null) {
      for (var sPreReqSeq = 0; sPreReqSeq < arrPreRequisite.length; sPreReqSeq++) {
        var doPreqIndex = sPreReqSeq + 1;
        BuildDOObject(sCid, sVer, doPreqIndex, arrPreRequisite[sPreReqSeq], true);
        //$ss.snapins.dm.APPLICATIONS[sCid][doPreqIndex] = new cDownloadListing(oObj, fileLocation, arrPreRequisite[sPreReqSeq], true);
      }
    }
  }

  function BuildDOObject(sDOID, sVer, sPreReqSeq, sPreReqXml, bDOType) {
    var oJson = $ss.agentcore.dal.content.GetContentDetailsAsJSON("sprt_download", sDOID, sVer);
    var fileLocation = GetFileLocationForDO(sDOID, sVer);

    if (!$ss.snapins.dm.APPLICATIONS) CreateApplicationObject();
    if (!$ss.snapins.dm.APPLICATIONS[sDOID]) CreateDOObject(sDOID);
    $ss.snapins.dm.APPLICATIONS[sDOID][sPreReqSeq] = new cDownloadListing(oJson, fileLocation, sPreReqXml, bDOType);
  }

  function CreateDOObject(sDOID) {
    if (!$ss.snapins.dm.APPLICATIONS) CreateApplicationObject();
    $ss.snapins.dm.APPLICATIONS[sDOID] = new Object();
  }

  function CreateApplicationObject() {
    $ss.snapins.dm.APPLICATIONS = new Object();
  }

  function GetFormElementVal(sDOID, sPreReqSeq, sEle) {
    if ($ss.snapins.dm.APPLICATIONS[sDOID][sPreReqSeq])
      return $ss.snapins.dm.APPLICATIONS[sDOID][sPreReqSeq][sEle];
    return "";
  }

  function SetFormElementVal(sDOID, sPreReqSeq, sEle, sVal) {
    if ($ss.snapins.dm.APPLICATIONS[sDOID][sPreReqSeq])
      $ss.snapins.dm.APPLICATIONS[sDOID][sPreReqSeq][sEle] = sVal;
  }

  function GetFormObject() {
    return $ss.snapins.dm.APPLICATIONS;
  }
  /*Enf Of - Content Form fill up functions*/

  /*Sort Related Functionality*/
  function SortLogicByTitle(oLJson, oRJson) {
    var oStringLStr = new String(oLJson.title);
    var oStringRStr = new String(oRJson.title);
    var res = oStringLStr.localeCompare(oStringRStr);
    return res;
  }

  function SortLogicByDate(oLJson, oRJson) {
    if (oLJson.scc_last_mod > oRJson.scc_last_mod) return -1; //Decreasing Order
    if (oLJson.scc_last_mod < oRJson.scc_last_mod) return 1;
    return 0;
  }

  function SortLogicByDisplayOrderField(oLJson, oRJson) {
    var returnValue = 0;
    try {
      if (isNaN(oLJson.sdc_sd_DisplayOrder) || isNaN(oRJson.sdc_sd_DisplayOrder)) {
        returnValue = 1;
        if (isNaN(oLJson.sdc_sd_DisplayOrder) && isNaN(oRJson.sdc_sd_DisplayOrder)) returnValue = 0;
        if (!(isNaN(oLJson.sdc_sd_DisplayOrder)) && isNaN(oRJson.sdc_sd_DisplayOrder)) returnValue = -1;
      }
      else {
        returnValue = (oLJson.sdc_sd_DisplayOrder - oRJson.sdc_sd_DisplayOrder); //Increasing Order - values having 10, 40, 30 will result in 10,30,40 
      }
    } catch (ex) {
      returnValue = 0;
    }
    if (returnValue == 0) {
      switch (_sSecondarySortBy) {
        case "scc_last_mod":
          return SortLogicByDate(oLJson, oRJson);
          break;
        case "scc_title":
          return SortLogicByTitle(oLJson, oRJson);
          break;
        default:
          return returnValue;
          break;
      }
    } else {
      return returnValue;
    }
  }

  function GetRequiredAttributesToSort(oContents) {
    for (var i = 0; i < oContents.length; i++) {
      if ($ss.snapins.dm.IsContentFormatNew(oContents[i])) {
        oContents[i]["sdc_sd_DisplayOrder"] = parseInt(GetContentField(oContents[i], "sdc_sd_DisplayOrder:::1"));
      } else {
        oContents[i]["sdc_sd_DisplayOrder"] = parseInt(GetContentField(oContents[i], "sdc_sd_DisplayOrder"));
      }
      oContents[i]["scc_last_mod"] = GetDateFromJson(oContents[i], "scc_last_mod");
    }
    return oContents;
  }
  /*End Of - Sort Related Functionality*/

  /*Common functions*/
  function GetDateFromJson(oJson, fieldName) {
    var oDate = new Date();
    try {
      if (oJson) {
        var sDate = $ss.agentcore.dal.content.GetContentProperty("sprt_download", oJson.cid, oJson.version, fieldName) || null;
        if (sDate) {
          if (sDate.indexOf("T") > -1) {
            sDate = sDate.split("T");             //Splits sDate to sDate[0] = Date and sDate[1] = Time
            if (sDate.length && sDate[0].indexOf("-") > -1) {
              sDate[0] = sDate[0].split("-");     //Splits sDate to sDate[0][0] = Year, sDate[0][1] = Month and sDate[0][2] = Date
              sDate[0][1] = sDate[0][1] * 1 - 1;  //Reduce month by 1 digit since Jan = 0
              sDate[1] = sDate[1].split(":");     //Splits sDate to sDate[1][0] = Hour, sDate[1][1] = Minutes, sDate[1][2] = seconds
              oDate = new Date(sDate[0][0], sDate[0][1], sDate[0][2], sDate[1][0], sDate[1][1], sDate[1][2]); //Get Date in Standard Format
              return oDate;
            }
          }
        }
      }
    } catch (ex) {

    }
  }

  function regValueExists(psRoot, psKey, psValue) {
    var sVal = "";
    try {
      sVal = _registry.RegValueExists(psRoot, psKey, psValue);
      return ((sVal != ""));
    } catch (oErr) {

    }
    return (false);
  }

  function fileExists(psPath) {
    try {
      if ($ss.agentcore.dal.file.FileExists(psPath) == true) {
        return (true);
      }
    } catch (oErr) {

    }
    return (false);
  }

  function folderExists(psPath) {
    try {
      if ($ss.agentcore.dal.file.FolderExists(psPath) == true) {
        return (true);
      }
    } catch (oErr) {

    }
    return (false);
  }

  String.prototype.trimDQuote = function() {
    return this.replace(/^\"|\"$/g, "");
  }

  //Use this function to obtain Raw CommandToken from commandline token
  //Alogrithm
  //path = Trim path
  //path = Removes Surrounding double quote one at start and one at the end and if both are present 
  //path = Trim path
  function CmdTokenToRawToken(cmdLineToken) {
    cmdLineToken = cmdLineToken.trim();
    if (cmdLineToken.charAt(0) == "\"" && cmdLineToken.charAt(cmdLineToken.length - 1) == "\"") {
      cmdLineToken = cmdLineToken.trimDQuote().trim();
    }
    return cmdLineToken;
  }

  function RawTokenToCmdToken(rawToken) {
    rawToken = rawToken.trim();
    return (rawToken.search(/\s+/) > -1) ? ("\"" + rawToken + "\"") : rawToken;
  }

  function expandMacro(psStringToExpand) {
    try {
      var sVal = $ss.agentcore.dal.config.ExpandAllMacros(psStringToExpand);
      if (sVal == "") {
        return (psStringToExpand);
      } else {
        return (sVal);
      }
    } catch (oErr) {

    }
    return (psStringToExpand);
  }
  /*End Of - Common functions*/

})();

var bMac = $ss.agentcore.utils.IsRunningInMacMachine();

