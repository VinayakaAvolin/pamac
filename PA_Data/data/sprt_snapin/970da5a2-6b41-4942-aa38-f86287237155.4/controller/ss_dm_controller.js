SnapinDownloadManagerController = SnapinBaseController.extend('snapin_download_manager',
{
    isUnManned: false,
    canRun: true,
    loading: false,
    init: function () {
        this._super();
        $ss.agentcore.events.Subscribe("BROADCAST_ON_EXTERNALREFRESH", function (oEvent) { return SnapinDownloadManagerController.OnExternalRefresh(oEvent) });
        $ss.agentcore.events.Subscribe("BROADCAST_CONNECT_STATUS", function (oEvent) { return SnapinDownloadManagerController.OnExternalRefresh(oEvent) });
        $ss.agentcore.events.Subscribe("BROADCAST_ON_PRUNEDONE", function (oEvent) { return SnapinDownloadManagerController.OnExternalRefresh(oEvent) });
        $ss.agentcore.events.Subscribe("BROADCAST_ON_SYNCNEWDATA", function (oEvent) { return SnapinDownloadManagerController.OnExternalRefresh(oEvent); });
    },
    OnExternalRefresh: function (oEvent) {
        if (this.canRun && !this.loading) {
            this.canRun = false;
            if (oEvent.name === "BROADCAST_ON_SYNCNEWDATA" || oEvent.name === "BROADCAST_ON_PRUNEDONE") {
                $ss.snapins.dm.RefreshCachedData();
                $ss.agentcore.dal.content.ClearContentCache();
            }
            if ($("#downmanagerbg").html()) {
                oEvent = oEvent || {};
                oEvent.toLocation = "#ssl_content_area #sprt_gen_nosubmenu_grid #sprt_gen_nosubmenu_grid_sinst_0";
                SnapinDownloadManagerController.dispatch('snapin_download_manager', 'index', oEvent);
            }
            this.canRun = true;
        }
        else
            return;
    }

},
{
    "#idCheckAll click": $ss.snapins.dm.selectAllApplications,
    ".itemselect input click": $ss.snapins.dm.selectThisApplication,
    "#idInstallButton click": $ss.snapins.dm.startInstallations,
    ".LinkDetailShow click": $ss.snapins.dm.toggleDetails,
    ".LinkDetailClose click": $ss.snapins.dm.toggleDetails,
    ".clRunThis click": $ss.snapins.dm.runThis,
    ".clSelectThis click": $ss.snapins.dm.selectThis,
    "#idCancelDownloadButton click": function (params) {
        //show the dialog
        var x = confirm($ss.snapins.dm.ss_dm_translate("ss_dm_res_cancelprompt", "Do you want to cancel the download?"));
        if (x) {
            $ss.snapins.dm.CancellDownload();
        }
    },
    index: function (pArgs) {
        this.Class.loading = true;
        var bUnmannedShouldLoad = true;
        var bClkFrmHome = false;
        var bDelRebootFlag = false;
        fromSnapin = false;
        pArgs = pArgs || {};
        pArgs.requestArgs = pArgs.requestArgs || {};
        pArgs.SelectedId = pArgs.SelectedId || {};
         if(pArgs.SelectedId.length > 0){
          // pArgs.SelectedId = this.CheckisArray(pArgs.SelectedId);
          fromSnapin = true;
        }
        if (pArgs && pArgs.toLocation) {
            this.Class.viewDivContainer = pArgs.toLocation;
        }
        var oArgReq = pArgs.requestArgs;

        try {
            if (oArgReq["unmanned"] == "true") {
                this.Class.isUnManned = true;
                bUnmannedShouldLoad = ($ss.snapins.dm.GetCurrentUnmannedState() === "stopped");
            } else if (oArgReq["rebooted"] == "true") {
                bDelRebootFlag = true;
            } else if (oArgReq["summaryclick"] == "true") {
                bClkFrmHome = true;
            }

            var bDisplayUI = !this.Class.isUnManned && !bDelRebootFlag;
            $ss.snapins.dm.ProcessDOPopup();

            if ((false == bDelRebootFlag) && (!$ss.snapins.dm.IsMandatoryReebootPending()) && (!$ss.snapins.dm.IsPostponeRebootPending())) {
                // This condition occurs when Bcont didnt receive "rebooted = true" as argument after system has rebooted.
                // This can happen if RunOnce Key entry for Bcont is missing. So we check if PendingFileRenameOperation has DM reboot related entry
                // to decide if "RebootState" key should be deleted or not. Presence of "RebootType" entry and absence of PendingFileRenameOperation entry for DM 
                // indicates that DM is in inconsistant state. If "RebootState" key is not deleted, DM will never allow user to use DM snapin as everything will be disabled.  
                bDelRebootFlag = true;
            }
            $ss.snapins.dm.SetRebootFlag(bDelRebootFlag);
            if (bDisplayUI) {
                $ss.snapins.dm.CloseMiniBcont(oArgReq["caller"]);
                $ss.agentcore.utils.ui.StartShellBusy();
                this.renderSnapinView("snapin_download_manager", this.Class.viewDivContainer, "\\views\\Processing.ejs");
                $ss.agentcore.utils.Sleep(500);
            }
        } catch (e) { $ss.agentcore.utils.CloseBcont(); }

        try {
            if (bUnmannedShouldLoad) {
                $ss.snapins.dm.loadApplicationData(pArgs);
            }
        } catch (ex) {
            $ss.agentcore.utils.ui.EndShellBusy();
        }

        if (bDisplayUI) {
            this.renderSnapinView("snapin_download_manager", this.Class.viewDivContainer, "\\views\\DownloadManager.ejs", { "guidlist": pArgs.SelectedId });
            $ss.snapins.dm.ScrolltoFirstErroredDO(bClkFrmHome);
            $ss.snapins.dm.MonitorNetworkStatus();
            $ss.agentcore.utils.ui.EndShellBusy();

            if ($ss.agentcore.network.inet.GetConnectionStatus())
                $ss.snapins.dm.checkState();
            this.Class.loading = false;
        } else {
            // Close the bcont after some delay.. use setTimeout...
            $ss.agentcore.utils.CloseBcont();
        }
    },
    
    indexMannedDO: function (pArgs) {
      var arrMannedDO = [];
      $ss.snapins.dm.loadApplicationData(pArgs);
      if($ss.snapins.dm.APPLICATIONS){
        for(var sDOID in $ss.snapins.dm.APPLICATIONS){
          arrMannedDO.push(sDOID);
        }
      }
      pArgs = pArgs ||{};
      pArgs.MannedDOList = arrMannedDO;
    },

    CheckisArray:function(SelectedId){
      if(SelectedId){
        var j = 0;
        for(var i =0;i< SelectedId.length;i++){
          SelectedId[i] =  $.isArray(SelectedId[i].id) ? SelectedId[i].id.toString() : SelectedId[i].id.split(",").toString();
        }
      }
      return SelectedId;
    },

    indexActionWidget: function (pArgs) {
        $ss.snapins.dm.loadApplicationData(pArgs);
        var sState = $ss.snapins.dm.GetCurrentState();
        var aContentstoInstall = [];
        for (var NDX in $ss.snapins.dm.APPLICATIONS) {
          var aDisInfo = $ss.snapins.dm.GetDisplayInfo(NDX, 0, sState);
          if (!aDisInfo[3]) {
            //aContentstoInstall.push({"id" :$ss.snapins.dm.APPLICATIONS[NDX][0]});
            aContentstoInstall.push($ss.snapins.dm.APPLICATIONS[NDX][0]);
          }
        }
  
        //pArgs.guidListToInstall = ([{ "sprt_download": aContentstoInstall}]);
        pArgs.guidListToInstall = aContentstoInstall;
    },

    error: function (pArgs) {
        pArgs = pArgs || {};
        this.Class.viewDivContainer = pArgs.toLocation || this.Class.viewDivContainer;
        this.renderSnapinView("snapin_download_manager", this.Class.viewDivContainer, "\\views\\Error.ejs");
    },

    "#idRestartButton click": function () {
        $ss.agentcore.utils.system.Reboot();
    },
    
    "#idInstallButton click": function(params) {
        $ss.snapins.dm.startInstallations(params);
    },

    ".clRunThis click" : function(params) {
        $ss.snapins.dm.runThis(params);
    },

    ".clSelectThis click": function(params) { 
        $ss.snapins.dm.selectThis(params)
    },

    "#idCheckAll click": function(params) {
        $ss.snapins.dm.selectAllApplications(params);
    },

    ".itemselect input click": function(params) {
         $ss.snapins.dm.selectThisApplication(params);
    },

    ".LinkDetailShow click": function(params) {
        $ss.snapins.dm.toggleDetails(params);
    },

    ".LinkDetailClose click": function(params) {
        $ss.snapins.dm.toggleDetails(params);
    },

    "#idViewAllButton click":function(params){
       if ($("#idViewAllButton").attr("disabled") == "disabled") {
        return false;
    }
       this.Class.dispatch("snapin_download_manager","index",params);
       $("#idViewAllButton").attr("disabled","disabled");
      
    }

});
