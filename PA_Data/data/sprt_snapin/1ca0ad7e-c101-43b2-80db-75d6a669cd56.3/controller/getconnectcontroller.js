GcknowmorerightController = SnapinBaseController.extend('gcknowmoreright',{
 '#checkcables #show_filter mouseover': function(){
		try {				
  		$("#gcrsimg_filter").css("display", "inline");
      $("#gcrsimg_modem").css("display", "none");
      $("#gcrsimg_pc").css("display", "none");
	  } catch(err){$ss.agentcore.log.GetDefaultLogger("$ss.snapin.getconnect.controller").error("Mouse Over on Animated Image",err.message); }
  },
'#checkcables #show_modem mouseover': function(){
		try {				
     
  		$("#gcrsimg_filter").css("display", "none");
      $("#gcrsimg_modem").css("display", "inline");
      $("#gcrsimg_pc").css("display", "none");
	  } catch(err){$ss.agentcore.log.GetDefaultLogger("$ss.snapin.getconnect.controller").error("Mouse Over on Animated Image",err.message); }
  },
'#checkcables #show_pc mouseover': function(){
		try {				
  		$("#gcrsimg_filter").css("display", "none");
      $("#gcrsimg_modem").css("display", "none");
      $("#gcrsimg_pc").css("display", "inline");
	  } catch(err){$ss.agentcore.log.GetDefaultLogger("$ss.snapin.getconnect.controller").error("Mouse Over on Animated Image",err.message); }
  },
'#show_filter_usb mouseover': function(){
		try {				
  		$("#gcrsimg_filter-usb").css("display", "inline");
      $("#gcrsimg_modem-usb").css("display", "none");
      $("#gcrsimg_pc-usb").css("display", "none");
	  } catch(err){$ss.agentcore.log.GetDefaultLogger("$ss.snapin.getconnect.controller").error("Mouse Over on Animated Image",err.message); }
  },
'#show_modem_usb mouseover': function(){
		try {				
     
  		$("#gcrsimg_filter-usb").css("display", "none");
      $("#gcrsimg_modem-usb").css("display", "inline");
      $("#gcrsimg_pc-usb").css("display", "none");
	  } catch(err){$ss.agentcore.log.GetDefaultLogger("$ss.snapin.getconnect.controller").error("Mouse Over on Animated Image",err.message); }
  },
'#show_pc_usb mouseover': function(){
		try {				
  		$("#gcrsimg_filter-usb").css("display", "none");
      $("#gcrsimg_modem-usb").css("display", "none");
      $("#gcrsimg_pc-usb").css("display", "inline");
	  } catch(err){$ss.agentcore.log.GetDefaultLogger("$ss.snapin.getconnect.controller").error("Mouse Over on Animated Image",err.message); }
  }

});
SnapinGetconnectController = SteplistBaseController.extend('snapin_getconnect',{
  InitParam: {
    aNavigationItems: ["#snapin_getconnect #nextbutton","#snapin_getconnect #prevbutton", "#snapin_getconnect #closebutton", "#snapin_getconnect #bubble_yes", "#snapin_getconnect #retrybutton", "#ss_gc_dns_Content #ss_gc_dns_openHomeURL","#snapin_getconnect #bubble_no"],
    aContentPlaceHolderDIV: "gccontentarea",
    sBackToPageURL: _config.GetConfigValue("getconnect_params", "BackHome_URL"),
    sForwardToPageURL: _config.GetConfigValue("getconnect_params", "Success_URL"),//snapin completion as well as success url 
    sErrorPageURL: _config.GetConfigValue("getconnect_params", "Failure_URL") //snapin complete failure
  }	
},
{
  index: function(params){
    $ss.agentcore.events.SendByName("DISABLE_HIDE_NW_CHECK");
    params.stepListSession = params.stepListSession || {};
    params.element = params.element || {};
    var sessionId = params.stepListSession.sessionId || params.element.stepSessionId ;
    var bFirstRun = params.stepListSession.mode==="start";
    params.requestArgs = params.requestArgs || {};
    sessionId = sessionId || params.requestArgs["sl_session"] || bFirstRun;
    if(!sessionId) {
        _layout.gc_ClearDBParams();
    }
    var strBrowser = false;
    if (params.requesturl) {
      strBrowser = $ss.agentcore.utils.GetQSObjectFromURL(params.requesturl).browser;
    }
    
    if (strBrowser) {
      if (strBrowser === "firefox")
        strBrowser = _const.INET_BROWSER_FIREFOX;
      _databag.SetValue("ss_gc_db_Browser",strBrowser);
    }
    
    //this is the place to render the layout...    
    this.renderSnapinView("snapin_getconnect", params.toLocation, "\\views\\gc_home.htm");
    //initialize the snapin...
    this._super(params,"snapin_getconnect");
    //Clear all databag values before starting the getconnect, as these DB values will affect the flow.
    //initialize the step .. however params will have some value 
	
	var that = this;
	that.params = params;
	setTimeout( function() {
					var flowInfo = that.Class.InitiateStep(that.params);
    if(flowInfo) {
						that.ProcessRender(that.params,flowInfo);
    }
				}, 100);
	
	//var flowInfo = this.Class.InitiateStep(params);
	//this.ProcessRender(params,flowInfo);
  },
  
  ProcessNext: function(params){
    var renderHelper = $ss.snapin.getconnect.renderHelper || {};
    if(params.element.id == "closebutton") {}
    else {
    renderHelper.hideRetryBtn();
    renderHelper.hideCloseBtn();
    renderHelper.enableNextBtn();
    }
    var flowInfo = this._super(this,params);
    if(flowInfo) {
      this.ProcessRender(params,flowInfo);
    }
  },

  ProcessPrevious: function(params){
    var renderHelper = $ss.snapin.getconnect.renderHelper || {};
    renderHelper.hideRetryBtn();
    renderHelper.enableNextBtn();
    renderHelper.hideCloseBtn();
    var flowInfo = this._super(this,params);
    if(flowInfo) {
      this.ProcessRender(params,flowInfo);
    }    
  },
  ProcessRetry: function(params){
    var renderHelper = $ss.snapin.getconnect.renderHelper || {};
    renderHelper.hideCloseBtn();
    var flowInfo = this._super(this,params);
    if(flowInfo) {
      this.ProcessRender(params,flowInfo);
    }    
  },
  ProcessRender: function(params,flowInfo) {
    _layout.AutoUpdateVisualFlowState();
    _layout.EnableLayoutBtns();
    var renderHelper = $ss.snapin.getconnect.renderHelper || {};
    renderHelper.hideRetryBtn();
    renderHelper.hideCloseBtn();
    stepId = flowInfo.stepid;
    var retData = {};
    var opt = {};
    //start pre render/before load function 
    if (stepId) {
      try {
        if(renderHelper[stepId] && renderHelper[stepId].BeforeRender) {
          retData = renderHelper[stepId].BeforeRender(params,this);  
        }
        opt.oLocal = retData;
      } catch(ex) {
        //failed to execute the renderhelper function
        $ss.agentcore.log.GetDefaultLogger("$ss.snapin.getconnect.controller").error("Before ProcessRender:",ex.message); 
      }
    }
    //make sure it is being prcoessed...
    this.ProcessFlow(this,params,flowInfo,opt);
    //Know more section views update
    if(flowInfo.type == "url")
    {
      var sFileArr = [];
      sFileName = flowInfo.file;
      sFileArr = sFileName.split("%/");
   
      var bDefContent = $ss.agentcore.dal.config.GetConfigValue("getconnect_KM","ShowDefContent") ;

      sKnowmoreFile = $ss.getSnapinAbsolutePath("snapin_getconnect")+ _config.ParseMacros("\\views\\helper\\%LANGCODE%\\") + sFileArr[1];
      if(_file.FileExists(sKnowmoreFile))
      {
        this.render({
		       	to: 'ConnectivityFlowHelper', 
		        partial: sKnowmoreFile ,
		        absolute_url: sKnowmoreFile 
        });
      }
      else if(bDefContent=="true")
      {
        var defaultFileName = _config.GetConfigValue("getconnect_KM","DefaultFileName");
        var defaultFilePath = $ss.getSnapinAbsolutePath("snapin_getconnect") + _config.ParseMacros("\\views\\helper\\%LANGCODE%\\") + defaultFileName;
        
        if(_file.FileExists(defaultFilePath))
          this.render({
		       	to: 'ConnectivityFlowHelper', 
		        partial: defaultFilePath ,
		        absolute_url: defaultFilePath 
          });          
      }    
      else
      {
        $("#ConnectivityFlowHelper").html("");
      }

    }
    var that = this;
    that.stepId = stepId; 
    that.params = params;

    // start the post processing
    if (stepId) {
        // Using settimeouts to reduce the start delay when getconnect is invoked
        // thru trigger or a reboot from restore network settings snapin(thru getconnect)
        setTimeout(
        	function() {
                  try {
                    var afterStatus = {};
                    
                    if(renderHelper[stepId] && renderHelper[stepId].AfterRender) {
          	            afterStatus = renderHelper[that.stepId].AfterRender(that.params,that);  
                    }
                    switch (afterStatus.move) {
                      case "forward":
                        return that.ProcessNext(that.params);
                        break;
                      case "back":
                        return that.ProcessPrevious(that.params);
                        break;
                      default:
                        break;
                    }
                            
                  } catch(ex) {
                          //failed to execute the renderhelper after function
                    $ss.agentcore.log.GetDefaultLogger("$ss.snapin.getconnect.controller").error("After ProcessRender:",ex.message); 
                  }  //end of catch
             },500);
    }  //end of post process
  },
   
  "#nextbutton click": function(params){
    if (!$("#nextbutton").hasClass("btndisabled")) { //Control whether to proceed with the onClick event on the navigation images (buttons)
    var stepId = params.element.stepId;
    var navHandler = $ss.snapin.getconnect.renderHelper || {};
    var bMove = true;
    // try one next button 
    try {
      if (stepId) {
        if (navHandler[stepId] && navHandler[stepId].NavNext) {
          bMove = navHandler[stepId].NavNext(params, this);
        }
      }
    } catch(ex) {
        $ss.agentcore.log.GetDefaultLogger("$ss.snapin.getconnect.controller").error("Next Button Click",ex.message); 
    }
    if(bMove != false)
      this.ProcessNext(params);
	}
	else return false;
  },
  "#closebutton click": function(params) {
    try
    {
      this.ProcessNext(params);
    }
    catch(ex)
    {
      $ss.agentcore.log.GetDefaultLogger("$ss.snapin.getconnect.controller").error("Done Button Click",ex.message);   
    }
  },

  "#retrybutton click": function(params){
    if (!$("#retrybutton").hasClass("btndisabled")) { //Control whether to proceed with the onClick event on the navigation images (buttons)
    var stepId = params.element.stepId;
    var renderHelper = $ss.snapin.getconnect.renderHelper || {};
    
    try {
      if (stepId) {
        if (renderHelper[stepId] && renderHelper[stepId].Retest) {
          afterStatus = renderHelper[stepId].Retest(params, this);
          switch (afterStatus.move) {
            case "forward":
              return this.ProcessNext(params);
              break;
            case "back":
              return this.ProcessPrevious(params);
              break;
            default:
              break;
          }
        }
      }
    } catch(ex) {
        $ss.agentcore.log.GetDefaultLogger("$ss.snapin.getconnect.controller").error("Retry Button Click:",ex.message); 
    }
    this.ProcessRetry(params);
	}
	else return false;
  },
  "#prevbutton click": function(params){
    if (!$("#prevbutton").hasClass("btndisabled")) { //Control whether to proceed with the onClick event on the navigation images (buttons)
    this.ProcessPrevious(params);
	}
	else return false;
  },
 
  '#bubble_yes click': function(params) { 
    var stepId = params.element.stepId;
    var _gcHelper = $ss.snapin.getconnect.renderHelper || {};

    var retdata = {};
		try {
			if (_gcHelper[stepId]){	
				if (_gcHelper[stepId].ClickYes){ 
					retdata = _gcHelper[stepId].ClickYes();  
				}	
			}
	  } catch(err) {$ss.agentcore.log.GetDefaultLogger("$ss.snapin.getconnect.controller").error("Radio Yes Click",err.message); }
  },
  
  '#bubble_no click': function(params) {
    var stepId = params.element.stepId;
    var _gcHelper = $ss.snapin.getconnect.renderHelper || {};

    var retdata = {};
		try {
			if (_gcHelper[stepId]){	
				if (_gcHelper[stepId].ClickNo){ 
					retdata = _gcHelper[stepId].ClickNo();
				}	
			}
	  } catch(err) {$ss.agentcore.log.GetDefaultLogger("$ss.snapin.getconnect.controller").error("Radio No Click",err.message); }
  },
  
  "#ss_gc_dns_Content #ss_gc_dns_openHomeURL click": function(params) {  
    var stepId = params.element.stepId;
    var _gcHelper = $ss.snapin.getconnect.renderHelper || {};

    if(_gcHelper[stepId])
	    if(_gcHelper[stepId].OpenHomeRunURL)  
        _gcHelper[stepId].OpenHomeRunURL();
  }
});
  
var _config  = $ss.agentcore.dal.config;
var _uiutils = $ss.agentcore.utils.ui;
var _layout  = $ss.snapin.getconnect.layoutHelper;
var _file    = $ss.agentcore.dal.file;
var _const   = $ss.agentcore.constants;
var _databag = $ss.agentcore.dal.databag;