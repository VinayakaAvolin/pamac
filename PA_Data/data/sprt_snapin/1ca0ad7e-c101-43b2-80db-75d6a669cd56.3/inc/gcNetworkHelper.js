﻿/**
 * @author Vishnu Ratheesh
 */
$ss.snapin = $ss.snapin ||{}; 
$ss.snapin.getconnect = $ss.snapin.getconnect || {};
$ss.snapin.getconnect.renderHelper = $ss.snapin.getconnect.renderHelper || {};

(function(){
	$.extend($ss.snapin.getconnect.renderHelper,{  
    hideRetryBtn: function()  {
      _layout.updatelayoutbuttons("HIDE_RETRY");
    },
    hideCloseBtn: function()  {
      _layout.updatelayoutbuttons("HIDE_CLOSE");
    },
    enableNextBtn: function()  {
      _layout.updatelayoutbuttons("ENABLE_NEXT");
    },
    /* ss_gc_releaserenew.htm functions - START */  
    sgC_ss_gc_releaserenew : {
      
      BeforeRender: function() {
        _layout.DisableLayoutBtns();
      },
			
      AfterRender: function(){
        try {
        _uiutils.StartShellBusy();
				_system.Sleep(0); // let UI refresh itself to draw page

				var dbResult =  (_system.ReleaseIpConfig() && _system.RenewIpConfig());
        
				_databag.SetValue("ss_gc_db_releaserenew", dbResult);        
				_databag.SetValue("ss_gc_db_SolutionOk", dbResult);
        
				// DB: Nothing takes affect very fast on Windows 98 and ME hence
				if (dbResult && (_system.GetOSGroup() == "9x")) {
					_system.Sleep(30000);
				}  
        _uiutils.EndShellBusy();   
				// do auto-next
				return {move:"forward"};
        } catch(ex){
            _logger.error("sgC_ss_gc_releaserenew: AfterRender()", ex.message);
        }
        finally {_uiutils.EndShellBusy();}
      }
    },
    
    sgC_ss_gc_releaserenew2 : {
		  AfterRender: function(){
        return _gcHelper["sgC_ss_gc_releaserenew"].AfterRender();
      },
		  BeforeRender: function(){
        return _gcHelper["sgC_ss_gc_releaserenew"].BeforeRender();
      }
    },
    
    ss_gc_ras_XXX_releaserenew : {
			AfterRender: function(){
        return _gcHelper["sgC_ss_gc_releaserenew"].AfterRender();
      },
		  BeforeRender: function(){
        return _gcHelper["sgC_ss_gc_releaserenew"].BeforeRender();
      }
    },
    sgC_ss_gc_releaserenewprompt : {
      BeforeRender: function() {
        _layout.EnableLayoutBtns();
      }
    },
    /* ss_gc_releaserenew.htm functions - END */
    /* ss_gc_heal_network_prompt.htm functions - START */    
    sgC_ss_gc_NetworkHealPrompt: {
			BeforeRender: function(){
				if (!_databag.GetValue('ss_gc_db_restore_network')) 
			    return {restore : false};		
		    else
          return {restore : true};
			},
			NavNext: function() {
				if($('#Restore').attr("checked")) {
					_databag.SetValue("ss_gc_db_restore_network", true);
				} else {
					_databag.SetValue("ss_gc_db_restore_network", false);
				}
				return true;
			}				
    },
    
    sgC_ss_gc_NetworkHealPrompt1 : {
      BeforeRender: function(){
        return _gcHelper["sgC_ss_gc_NetworkHealPrompt"].BeforeRender();
      },
      NavNext: function(){
        return _gcHelper["sgC_ss_gc_NetworkHealPrompt"].NavNext();
      }
    },
    /* ss_gc_heal_network_prompt.htm functions - END */
    /* ss_gc_heal_network.htm functions - START */    
    sgC_ss_gc_NetworkHeal: {

      BeforeRender: function() {
        _layout.DisableLayoutBtns();
      },

			AfterRender: function(){
        try {
        _uiutils.StartShellBusy();
				_system.Sleep(0); // let UI refresh itself to draw page
				var dbResult = "false";
				//Use include at snapin/host level where all proper .js files are included
				if(_gcutils.HealNetwork()) {
					dbResult = "true";
				}
				_databag.SetValue("ss_gc_db_SolutionOk", dbResult);
        _uiutils.EndShellBusy();
				// do auto-next
				return {move:"forward"};
        } catch(ex){
            _logger.error("sgC_ss_gc_NetworkHeal: AfterRender()", ex.message);
        }
        finally {_uiutils.EndShellBusy();}
			}
		},
    
    sgC_ss_gc_NetworkHeal1 : {
      AfterRender: function(){
        return _gcHelper["sgC_ss_gc_NetworkHeal"].AfterRender();
      },
      BeforeRender: function(){
        return _gcHelper["sgC_ss_gc_NetworkHeal"].BeforeRender();
      }
    },
    /* ss_gc_heal_network.htm functions - END */
    /* ss_gc_heal_wireless_prompt.htm functions - START */
    sgC_ss_gc_WirelessHealPrompt : {
			BeforeRender: function(){
		    if (!_databag.GetValue('ss_gc_db_restore_wireless')) 
			    return {restore : false};		
		    else
          return {restore : true};
			},
			NavNext: function() {
				if($('#Restore').attr("checked")) {
					_databag.SetValue("ss_gc_db_restore_wireless", true);
				} else {
					_databag.SetValue("ss_gc_db_restore_wireless", false);
				}
				return true;
			}
    },
    /* ss_gc_heal_wireless_prompt.htm functions - END */
    /* ss_gc_heal_wireless.htm functions - START */
    sgC_ss_gc_WirelessHeal: {
      
      BeforeRender: function() {
        _layout.DisableLayoutBtns();
      },
			
      AfterRender: function(){
        try {
        _uiutils.StartShellBusy();
				_system.Sleep(0); // let UI refresh itself to draw page
				var dbResult = "false";
				//Use include at snapin/host level where all proper .js files are included
				if(_gcutils.HealWireless()) {
					dbResult = "true";
				}
				_databag.SetValue("ss_gc_db_SolutionOk", dbResult);
        _uiutils.EndShellBusy();
				// do auto-next
				return {move:"forward"};
        }catch(ex){
           _logger.error("sgC_ss_gc_WirelessHeal: AfterRender()", ex.message);
        }
        finally {_uiutils.EndShellBusy();}
      }
    },
    /* ss_gc_heal_wireless.htm functions - END */
    /* ss_gc_connectiondiagram.htm functions - START */
    ss_gc_cmr_checkcables:{
			AfterRender: function(){
				try {
					_gcutils.CreateModemSpecificDiagram("ss_gc_cdm_");
					_layout.updatelayoutbuttons("DISABLE_NEXT");
					
				} catch(ex) {
					_logger.error("AfterRender()", ex.message);
				}
			},
			ClickYes: function(){				
				_databag.SetValue('ss_gc_db_connectdiagram', 'true');
				_databag.SetValue('ss_gc_db_OptYesNo', 'yes');	
				_layout.updatelayoutbuttons("ENABLE_NEXT");				
			},
			ClickNo: function(){				
				_databag.SetValue('ss_gc_db_connectdiagram', 'false');
				_databag.SetValue('ss_gc_db_OptYesNo', 'no');
				_layout.updatelayoutbuttons("ENABLE_NEXT");			
			}
    },
    /* ss_gc_connectiondiagram.htm functions - END */
    /* ss_gc_heal_dun_prompt.htm functions - START */ 
    ss_gc_cmr_DunPrompt: {
			BeforeRender: function(){
        if (!_databag.GetValue('ss_gc_db_restore_dun')) 
			    return {restore : false};		
		    else
          return {restore : true};        
			},
			NavNext: function() {
				if($('#Restore').attr("checked")) {
					_databag.SetValue("ss_gc_db_restore_dun", true);
				} else {
					_databag.SetValue("ss_gc_db_restore_dun", false);
				}
				return true;
			}
    },
    /* ss_gc_heal_dun_prompt.htm functions - END */ 
    /* ss_gc_heal_dun.htm functions - START */ 
    ss_gc_cmr_HealDUN: {
	    AfterRender: function(){		
        try {
        _uiutils.StartShellBusy();		
			  _system.Sleep(0); // let UI refresh itself to draw page
				var dbResult = "false";
				//Use include at snapin/host level where all proper .js files are included
				if(_gcutils.HealDun()) {
					dbResult = "true";
				}
				_databag.SetValue("ss_gc_db_SolutionOk", dbResult);
        _uiutils.EndShellBusy();
				// do auto-next
				return {move:"forward"};
        }catch(ex){
           _logger.error("ss_gc_cmr_HealDUN: AfterRender()", ex.message);
        }
        finally {_uiutils.EndShellBusy();}
      }
    },
    /* ss_gc_heal_dun.htm functions - END */   
    /* ss_gc_heal_dun_drivers_prompt.htm functions - START */   
    ss_gc_cmr_DriversPrompt: {
			BeforeRender: function(){
        if (!_databag.GetValue('ss_gc_db_restore_dundrivers')) 
          return {restore : false};		
		    else
          return {restore : true};
			},
			NavNext: function() {
				if($('#Restore').attr("checked")) {
					_databag.SetValue("ss_gc_db_restore_dundrivers", true);
				} else {
					_databag.SetValue("ss_gc_db_restore_dundrivers", false);
				}
				return true;
			}
    },
    /* ss_gc_heal_dun_drivers_prompt.htm functions - END */    
    /* ss_gc_heal_dun_drivers.htm functions - START */
    ss_gc_cmr_DriversHeal: {
			AfterRender: function(){
        try {
        _uiutils.StartShellBusy();
				_system.Sleep(0); // let UI refresh itself to draw page
				var dbResult = "false";
				//Use include at snapin/host level where all proper .js files are included
				if(_gcutils.HealDunDrivers()) {
					dbResult = "true";
				}
				_databag.SetValue("ss_gc_db_SolutionOk", dbResult);
        _uiutils.EndShellBusy();
				// do auto-next
				return {move: "forward"};
        }catch(ex){
           _logger.error("ss_gc_cmr_DriversHeal: AfterRender()", ex.message);
        }
        finally {_uiutils.EndShellBusy();}
      }
    },
    /* ss_gc_heal_dun_drivers.htm functions - END */
    /* ss_gc_restartwirelessprompt.htm functions - START */
    sgC_ss_gc_restartwirelessprompt: {
			BeforeRender: function(){

				var nicDisplayName = "";

				var oNics = _gcutils.GetNICList();
				for(sAdapter in oNics) {
					//since we check all, skip adapters that dont need a fixin'
					if(_netcheck.IsAdapterWireless(null, sAdapter)) {
						var displayName = _netcheck.GetAdapterDisplayName(null, sAdapter);
						if (displayName != null && displayName != "")
							nicDisplayName += displayName + " ";
					}
				}
				return {dispStr:nicDisplayName};					
			}				
    },
    /* ss_gc_restartwirelessprompt.htm functions - END */
    /* ss_gc_restartwireless.htm functions - START */
    sgC_ss_gc_restartwireless: {
      
      BeforeRender: function() {
        _layout.DisableLayoutBtns();
      },
			
      AfterRender: function(){
        try {
        _uiutils.StartShellBusy();
				_system.Sleep(0); // let UI refresh itself to draw page			
			
        var oNics = _gcutils.GetNICList();
        for(var sAdapter in oNics) {
          //since we check all, skip adapters that dont need a fixin'
          if(_netcheck.IsAdapterWireless(null, sAdapter)) {
            _netcheck.DisableAdapter(null, sAdapter);
            _system.Sleep(2000);
            _netcheck.EnableAdapter(null, sAdapter);
          }
        }

        _databag.SetValue("ss_gc_db_SolutionOk", true);
        _uiutils.EndShellBusy();
        // do auto-next
        return {move:"forward"};
        }catch(ex){
           _logger.error("sgC_ss_gc_restartwireless: AfterRender()", ex.message);
        }
        finally {_uiutils.EndShellBusy();}
      }
    },
    /* ss_gc_restartwireless.htm functions - END */
    /* ss_gc_wireless_profile_prompt.htm functions - END */
    sgC_ss_gc_wirelessprofileprompt : {
			BeforeRender: function(){
		    if (!_databag.GetValue('ss_gc_db_create_wireless_profile')) 
			    return {create : false};		
		    else
          return {create : true};
			},
			NavNext: function() {
				if($('#Create').attr("checked")) {
					_databag.SetValue("ss_gc_db_create_wireless_profile", true);
				} else {
					_databag.SetValue("ss_gc_db_create_wireless_profile", false);
				}
				return true;
			}
    },
    /* ss_gc_wireless_profile_prompt.htm functions - END */
    /* ss_gc_reboot_pc.htm functions - START */
    sgC_ss_gc_NetworkHealReboot: {
			NavNext: function(params) {				
        try{
          var sessionid = params.element.stepSessionId;
          _uiutils.RestartWithSave(_gcSnapinHomeURL + "?sl_resume=true&sl_session=" + sessionid, true);

        }
        catch(ex)
        {
          _logger.error("Reboot Machine failed!", ex.message);
        }
        return false;   //preventing the app to navigate to the next page while rebooting is in progress
			}
    },
    
    sgC_ss_gc_NetworkHealReboot1 : {
      NavNext: function(params) {
				return _gcHelper["sgC_ss_gc_NetworkHealReboot"].NavNext(params);
			}
    },
    
    /* ss_gc_pingtest.htm functions START*/
    ss_gc_pingcheck2:
    {
      BeforeRender: function() {
        _layout.DisableLayoutBtns();
      },

      AfterRender: function() {
        $ss.snapin.getconnect.decisions.CanPingExternalsite();
        return { move: "forward" };
      }
    },
    
    ss_gc_pingcheck3:
    {
      BeforeRender: function() {
        return _gcHelper["ss_gc_pingcheck2"].BeforeRender();
      },

      AfterRender: function() {
        return _gcHelper["ss_gc_pingcheck2"].AfterRender();
      }
    },

    ss_gc_pingcheck3a:
    {
      BeforeRender: function() {
        return _gcHelper["ss_gc_pingcheck2"].BeforeRender();
      },

      AfterRender: function() {
        return _gcHelper["ss_gc_pingcheck2"].AfterRender();
      }

    },

    ss_gc_pingcheck4:
    {
      BeforeRender: function() {
        return _gcHelper["ss_gc_pingcheck2"].BeforeRender();
      },

      AfterRender: function() {
        return _gcHelper["ss_gc_pingcheck2"].AfterRender();
      }

    },

    ss_gc_pingcheck5:
    {
      BeforeRender: function() {
        return _gcHelper["ss_gc_pingcheck2"].BeforeRender();
      },

      AfterRender: function() {
        return _gcHelper["ss_gc_pingcheck2"].AfterRender();
      }

    },

    ss_gc_pingcheck6:
    {
      BeforeRender: function() {
        return _gcHelper["ss_gc_pingcheck2"].BeforeRender();
      },

      AfterRender: function() {
        return _gcHelper["ss_gc_pingcheck2"].AfterRender();
      }

    },

    ss_gc_pingcheck5_1:
    {
      BeforeRender: function() {
        return _gcHelper["ss_gc_pingcheck2"].BeforeRender();
      },

      AfterRender: function() {
        return _gcHelper["ss_gc_pingcheck2"].AfterRender();
      }

    },

    ss_gc_pingcheck5_2:
    {
      BeforeRender: function() {
        return _gcHelper["ss_gc_pingcheck2"].BeforeRender();
      },

      AfterRender: function() {
        return _gcHelper["ss_gc_pingcheck2"].AfterRender();
      }
    },

    ss_gc_pingcheck7:
    {
      BeforeRender: function() {
        return _gcHelper["ss_gc_pingcheck2"].BeforeRender();
      },

      AfterRender: function() {
        return _gcHelper["ss_gc_pingcheck2"].AfterRender();
      }

    },

    ss_gc_pingcheck8:
    {
      BeforeRender: function() {
        return _gcHelper["ss_gc_pingcheck2"].BeforeRender();
      },

      AfterRender: function() {
        return _gcHelper["ss_gc_pingcheck2"].AfterRender();
      }
    },
    
    ss_gc_pingcheck9:
    {
      BeforeRender: function() {
        return _gcHelper["ss_gc_pingcheck2"].BeforeRender();
      },

      AfterRender: function() {
        return _gcHelper["ss_gc_pingcheck2"].AfterRender();
      }
    },

    ss_gc_pingcheck10:
    {
      BeforeRender: function() {
        return _gcHelper["ss_gc_pingcheck2"].BeforeRender();
      },

      AfterRender: function() {
      return _gcHelper["ss_gc_pingcheck2"].AfterRender();
      }
    },

    ss_gc_pingcheck1:
    {
      BeforeRender: function() {
        return _gcHelper["ss_gc_pingcheck2"].BeforeRender();
      },

      AfterRender: function() {
        return _gcHelper["ss_gc_pingcheck2"].AfterRender();
      }
    },

    ss_gc_pingcheck11:
    {
      BeforeRender: function() {
        return _gcHelper["ss_gc_pingcheck2"].BeforeRender();
      },

      AfterRender: function() {
        return _gcHelper["ss_gc_pingcheck2"].AfterRender();
      }

    },
    
    ss_gc_pingcheck12:
    {
      BeforeRender: function() {
        return _gcHelper["ss_gc_pingcheck2"].BeforeRender();
      },

      AfterRender: function() {
        return _gcHelper["ss_gc_pingcheck2"].AfterRender();
      }

    },
    /* ss_gc_pingtest.htm functions END*/

    /* ss_gc_reboot_pc.htm functions - END */
    /* ss_gc_testconnect.htm functions - END */   
    ssgcConn_ss_gc_testconnect : {
      
      BeforeRender: function() {
        _layout.DisableLayoutBtns();
      },
			
      AfterRender: function(){
				try { return this.Init(); } catch (ex) {
          _layout.EnableLayoutBtns();
					_logger.error("AfterRender", ex.message );
				}
        finally {_uiutils.EndShellBusy();}
			},
			Init: function(){							
				//Set Visual-Flow animation parameters
        if(_databag.GetValue("ss_gc_db_InitialTest")!= true) {
				_databag.SetValue("ss_gc_db_vflink3_animation", true);
				_layout.AutoUpdateVisualFlowState();
        }
        else
        {
          _layout.VFLinkComputerToModemAnimate();
          _layout.VFLinkModemToWalledGardenAnimate();
          _layout.VFLinkWalledGardenToInternetAnimate();  
        }
		    _uiutils.StartShellBusy();
				if (_databag.GetValue("ss_gc_db_RunTestFirst") || _databag.GetValue("ss_gc_db_DoTestInternet")) 
				{            
					// Check if we need to set creds
					if( _databag.GetValue("ss_gc_db_SetCreds")=="true")
					{
						_logger.debug("Init", "going to set user credentials");
						this.execSetUserCreds("ss_gc_tc_");			
					} 
          
					var bConnected = this.execTestConnect("ss_gc_tc_");
					if (bConnected) 
					{
            if(_databag.GetValue("ss_gc_db_InitialTest") == true) {
				      _databag.SetValue("ss_gc_db_vflink3_animation", true);
				      _layout.AutoUpdateVisualFlowState();          
            }
						_databag.SetValue("ss_gc_db_InternetConnected", "true");
						_logger.debug("Init", "basic connection test passed");
						if(_databag.GetValue("ss_gc_db_SetCreds")=="true")
						{
							// we got connected due to good creds setting 
							// hence persist them for later usage
							var sName     = _databag.GetValue("GC_SMAPI_DB_USERNAME");
							var sPassword = _databag.GetValue("GC_SMAPI_DB_USERPASS");
							_registry.SetRegValue("HKLM", _config.GetRegRoot(), "username", sName);
							_registry.SetRegValue("HKLM", _config.GetRegRoot(), "userpass", sPassword);
        
							// change flag
							_databag.SetValue("ss_gc_db_SetCreds", "false");
       
						}
            _uiutils.EndShellBusy();
						// do auto next
						return {move:"forward"};        
					} 
					else 
					{
						_databag.SetValue("ss_gc_db_InternetConnected", "false");
						_logger.debug("Init", "basic connection test failed");
						// Try to set creds only if its a GC initiated flow
						// and we haven't tried setting creds before
						if ( _databag.GetValue("ss_gc_db_FromGC") && (!_gcutils.IsNullModem()) && (!_gcutils.IsModemRas()) &&
								(!(_databag.GetValue("ss_gc_db_SetCreds")=="true"))
                && ($ss.snapin.getconnect.decisions.IsModemFlowNeeded() == "true")) { 
							this.SetCreds();
						} else {
							// change flag
							_databag.SetValue("ss_gc_db_SetCreds", "false");
              _uiutils.EndShellBusy();
							// do auto next
							return {move:"forward"};      
						}  
					}  
				}
				else
				{
					// show "Fixing connection to Internet..."						
					$("#ss_gc_tc_fixing").css("display", "block");

					// set databag value to indicate that we've been through here once (so we can actually 
					// do the test the next time around)
					_databag.SetValue("ss_gc_db_DoTestInternet", "true");
          _uiutils.EndShellBusy();
					return {move:"forward"};     
				}		
        _uiutils.EndShellBusy();			
			},
      execSetUserCreds: function(sDivPrefix) {          
        var _oModem = null;
        if(!_oModem) _oModem = _sdcDSLModem.GetInstance();
          var timeOut = Number(_oModem.GetInstanceValue("pinginternet_timeout", "60"));
          var duration = timeOut;
          var sTime = new Date();
          var timer = timeOut;
          timeOut = timeOut * 1000;
          var time;
          var opt = 
          {
            fnPtr    : function(){return _gcutils.SetUserCreds(sDivPrefix);},
            timeout  : timeOut,
            sleepTime: 3000,
            callback : function()
                       {
                         if($('#' + sDivPrefix + "testing").length > 0 )
                         {
                          $('#' + sDivPrefix + "testing").css("display", "block");
                          var remTime = $ss.agentcore.utils.ui.GetRemainingTime(sTime,duration);
                          //time = timer--;
                          //if(time<0) time = 0;
                          //time = time.toString();
                          var uitext = _utils.LocalXLate("snapin_getconnect", sDivPrefix + "testing", [remTime]);
                          $('#' + sDivPrefix + "testing").html(uitext);
                         }
                       }
          } ;
          var result = _uiutils.UpdateUI(opt);   
          return result;  
      },
      execTestConnect: function(sDivPrefix) {          
          
          var timeOut = Number(_config.GetConfigValue("getconnect_params", "InternetTestTimeout", "60"));
          var duration = timeOut;
          var sTime = new Date();
          var timer = timeOut;
          timeOut = timeOut * 1000;
          var time;
          var opt = 
          {
            fnPtr    : function(){return _gcutils.TestConnect(sDivPrefix);},
            timeout  : timeOut,
            sleepTime: 3000,
            callback : function()
                       {
                         if($('#' + sDivPrefix + "testing").length > 0 )
                         {
                          $('#' + sDivPrefix + "testing").css("display", "block");
                          var remTime = $ss.agentcore.utils.ui.GetRemainingTime(sTime,duration);
                          //time = timer--;
                          //if(time<0) time = 0;
                          //time = time.toString();
                          var uitext = _utils.LocalXLate("snapin_getconnect", sDivPrefix + "testing", [remTime]);
                          $('#' + sDivPrefix + "testing").html(uitext);
                         }
                       }
          } ;
          var result = _uiutils.UpdateUI(opt);   
          return result;  
        },
			SetCreds: function(){
				// Check if creds are in the common location
				var oCreds = this.CheckReg();
				if (!oCreds) {
					// Ask user for it
					this.GetCredsFromUser();  
				} else {
					// Push the creds to databag and set it up for credential set up
					_databag.SetValue("GC_SMAPI_DB_USERNAME", oCreds.sName);
					_databag.SetValue("GC_SMAPI_DB_USERPASS", oCreds.sPassword);
          
					_databag.SetValue("ss_gc_db_SetCreds", "true");
					this.Init();    
				}
			},
			CheckReg: function(){
				var oRet = null;
				// When SmartAccess saves user creds in this common area 
				// GetConnected could pick it up
				var sRoot = _config.GetRegRoot();
				
				var sProduct = _config.ParseMacros("%PRODUCT%");	
				var re = new RegExp("\\"+sProduct);
				sRoot = sRoot.replace(re, "");
        
				// Get unique username /password to be set
				var sName     = _registry.GetRegValue("HKLM", sRoot, "username");
				var sPassword = _registry.GetRegValue("HKLM", sRoot, "userpass");
				if(sName && sPassword) {
					oRet.sName = sName;
					oRet.sPassword = _utils.DecryptHexToStr(sPassword);
				}
    
				return oRet;
      },
			GetCredsFromUser: function(){
				var sDivPref = "ss_gc_tc_";
        if ($("#" + sDivPref + "Content").length != 0) {
          $("#" + sDivPref + "Content").css("display", "none");
        }
        $("#" + sDivPref + "usercreds").css("display", "block"); 
        _layout.EnableLayoutBtns(); 
        _layout.updatelayoutbuttons("ENABLE_RETRY");
			},
			Retest: function(){
			  // Get creds 
				var sName     = $("#username").val();
				var sPassword = $("#userpass").val();
        
				// Push the creds to databag and set it up for credential set up
				_databag.SetValue("GC_SMAPI_DB_USERNAME", sName);
				_databag.SetValue("GC_SMAPI_DB_USERPASS", sPassword);
          
				_databag.SetValue("ss_gc_db_SetCreds", "true");	        
			}
    },
    sta_ss_gc_testconnect : {
      BeforeRender: function(){
        return _gcHelper["ssgcConn_ss_gc_testconnect"].BeforeRender();
      },
			AfterRender: function(){
        return _gcHelper["ssgcConn_ss_gc_testconnect"].AfterRender();
      },
      Retest: function(){
        return _gcHelper["ssgcConn_ss_gc_testconnect"].Retest();
      }
    },
    ssgcConn_ss_gc_testconnect2 : {
      BeforeRender: function(){
        return _gcHelper["ssgcConn_ss_gc_testconnect"].BeforeRender();
      },
			AfterRender: function(){
        return _gcHelper["ssgcConn_ss_gc_testconnect"].AfterRender();
      },
      Retest: function(){
        return _gcHelper["ssgcConn_ss_gc_testconnect"].Retest();
      }
    },
    
    ssgcConn_ss_gc_testconnect3 : {
      BeforeRender: function(){
        return _gcHelper["ssgcConn_ss_gc_testconnect"].BeforeRender();
      },
			AfterRender: function(){
        return _gcHelper["ssgcConn_ss_gc_testconnect"].AfterRender();
      },
      Retest: function(){
        return _gcHelper["ssgcConn_ss_gc_testconnect"].Retest();
      }
    },
    
    ssgcConn_ss_gc_testconnect4 : {
      BeforeRender: function(){
        return _gcHelper["ssgcConn_ss_gc_testconnect"].BeforeRender();
      },
			AfterRender: function(){
        return _gcHelper["ssgcConn_ss_gc_testconnect"].AfterRender();
      },
      Retest: function(){
        return _gcHelper["ssgcConn_ss_gc_testconnect"].Retest();
      }
    },
    
    ssgcConn_ss_gc_testconnect5 : {
			BeforeRender: function(){
        return _gcHelper["ssgcConn_ss_gc_testconnect"].BeforeRender();
      },
      AfterRender: function(){
        return _gcHelper["ssgcConn_ss_gc_testconnect"].AfterRender();
      },
      Retest: function(){
        return _gcHelper["ssgcConn_ss_gc_testconnect"].Retest();
      }
    },
    
    ssgcConn_ss_gc_testconnect5a : {
      BeforeRender: function(){
        return _gcHelper["ssgcConn_ss_gc_testconnect"].BeforeRender();
      },
			AfterRender: function(){
        return _gcHelper["ssgcConn_ss_gc_testconnect"].AfterRender();
      },
      Retest: function(){
        return _gcHelper["ssgcConn_ss_gc_testconnect"].Retest();
      }
    },
    
    ssgcConn_ss_gc_testconnect6 : {
      BeforeRender: function(){
        return _gcHelper["ssgcConn_ss_gc_testconnect"].BeforeRender();
      },
			AfterRender: function(){
        return _gcHelper["ssgcConn_ss_gc_testconnect"].AfterRender();
      },
      Retest: function(){
        return _gcHelper["ssgcConn_ss_gc_testconnect"].Retest();
      }
    },
    
    ssgcConn_ss_gc_testconnect7 : {
      BeforeRender: function(){
        return _gcHelper["ssgcConn_ss_gc_testconnect"].BeforeRender();
      },
			AfterRender: function(){
        return _gcHelper["ssgcConn_ss_gc_testconnect"].AfterRender();
      },
      Retest: function(){
        return _gcHelper["ssgcConn_ss_gc_testconnect"].Retest();
      }
    },
    
    ssgcrdci_ss_gc_testconnect1 : {
      BeforeRender: function(){
        return _gcHelper["ssgcConn_ss_gc_testconnect"].BeforeRender();
      },
			AfterRender: function(){
        return _gcHelper["ssgcConn_ss_gc_testconnect"].AfterRender();
      },
      Retest: function(){
        return _gcHelper["ssgcConn_ss_gc_testconnect"].Retest();
      }
    },
    
    ssgcrdci_ss_gc_testconnect2 : {
      BeforeRender: function(){
        return _gcHelper["ssgcConn_ss_gc_testconnect"].BeforeRender();
      },
			AfterRender: function(){
        return _gcHelper["ssgcConn_ss_gc_testconnect"].AfterRender();
      },
      Retest: function(){
        return _gcHelper["ssgcConn_ss_gc_testconnect"].Retest();
      }
    },
    /* ss_gc_testconnect.htm functions - END */
    /* ss_gc_powercycle.htm functions - START */
    
    sgC_ss_gc_powercycle: {
			AfterRender: function(){
				_layout.updatelayoutbuttons("DISABLE_NEXT");
			},
			ClickYes: function(){				
				_databag.SetValue('ss_gc_db_powercycle', 'true');
				_databag.SetValue('ss_gc_db_OptYesNo', 'yes');
				_layout.updatelayoutbuttons("ENABLE_NEXT");			
			},
			ClickNo: function(){				
				_databag.SetValue('ss_gc_db_powercycle', 'false');
				_databag.SetValue('ss_gc_db_OptYesNo', 'no');
				_layout.updatelayoutbuttons("ENABLE_NEXT");			
			}
    },
    
    sgG_ss_gc_powercycle : {
			AfterRender: function(){
        return _gcHelper["sgC_ss_gc_powercycle"].AfterRender();     
      },
      ClickYes: function(){	
        return _gcHelper["sgC_ss_gc_powercycle"].ClickYes();     
      },
      ClickNo: function(){	  
        return _gcHelper["sgC_ss_gc_powercycle"].ClickYes();     
      }
    },
    /* ss_gc_powercycle.htm functions - END */
    /* ss_gc_retryconnectmodem.htm functions - START */
    ssgcCo_ss_gc_retryconnectmodem : {
			AfterRender: function(){				
				_layout.updatelayoutbuttons("ENABLE_RETRY");					
			},
			NavNext: function(){				
				_databag.SetValue("ss_gc_db_RetryConnectModem", "false", "false");
			},
			Retest: function(){		
				_databag.SetValue("ss_gc_db_RetryConnectModem", "true", "false");
        _layout.updatelayoutbuttons("DISABLE_NEXT");		
				return {move:"forward"};
			}
    },
    /* ss_gc_retryconnectmodem.htm functions - END */
    /* ss_gc_retryconnectwg.htm functions - START */
    ssgcConne_ss_gc_retryconnectwg : {
			AfterRender: function(){			
				_layout.updatelayoutbuttons("ENABLE_RETRY");
			},
			NavNext: function(){	
				_databag.SetValue("ss_gc_db_RetryConnectWG", "false", "false");
			},
			Retest: function(){						
				_databag.SetValue("ss_gc_db_RetryConnectWG", "true", "false");
        _layout.updatelayoutbuttons("DISABLE_NEXT");		
				return {move:"forward"};
			}
    },
    /* ss_gc_retryconnectwg.htm functions - END */
    /* ss_gc_retrygetsync.htm functions - START */
    ssgcGe_ss_gc_retrygetsync : {
			AfterRender: function(){		
        _layout.updatelayoutbuttons("ENABLE_RETRY");
			},
			NavNext: function(){
				_databag.SetValue("ss_gc_db_RetryGetSync", "false", "false");
			},
			Retest: function(){						
				_databag.SetValue("ss_gc_db_RetryGetSync", "true", "false");
        _layout.updatelayoutbuttons("DISABLE_NEXT");		
				return {move:"forward"};

			}
    },
    /* ss_gc_retrygetsync.htm functions - END */
    /* ss_gc_testsync.htm functions - START */
    sgG_ss_gc_testsync : {								
					
      BeforeRender: function() {
        _layout.DisableLayoutBtns();
      },							
		  
      AfterRender: function(){			
				//Set Visual-Flow animation parameter
				_databag.SetValue("ss_gc_db_vflink2_animation", true);
				_layout.AutoUpdateVisualFlowState();
			  try {
					if (_databag.GetValue("ss_gc_db_RunTestFirst") ||
							_databag.GetValue("ss_gc_db_DoTestSync")) 
					{
						
						var sdcDSLModem = _sdcDSLModem.GetInstance();
						var bRet = this.execTestSync("ss_gc_tsy_");
						if(bRet) 
						{
							_databag.SetValue("ss_gc_db_SyncOk", "true");
							// do an auto next when things are OK
							return {move:"forward"};
						} 
						else if (sdcDSLModem.GetLastError() == _const.SMAPI_NO_AUTH)   
						{
              _gcutils.OnNoAuth("ss_gc_tsy_");              
							_databag.SetValue("ss_gc_db_SyncOk", "no_auth");
              _layout.EnableLayoutBtns(); 
              _layout.updatelayoutbuttons("ENABLE_RETRY");
						}
						else if (sdcDSLModem.GetLastError() == _const.SMAPI_DO_REBOOT)   
						{ 
							_databag.SetValue("ss_gc_db_SyncOk", "do_reboot");
							// do an auto next on reboot as well
							return {move:"forward"};
						} 
						else 
						{
							_databag.SetValue("ss_gc_db_SyncOk", "false");  
							// do an auto next on all other sync failures
							return {move:"forward"};
						}
					}
					else
					{
						// show "Fixing connection to network..."
						$("#ss_gc_tsy_fixing").css("display", "block");

						// set databag value to indicate that we've been through here once (so we can actually 
						// do the test the next time around)
						_databag.SetValue("ss_gc_db_DoTestSync", "true");
            return {move:"forward"};
					}
				} catch (ex) {
          _layout.EnableLayoutBtns();
					_logger.error("AfterRender()", ex.message);
				}  
			},
      execTestSync: function(sDivPrefix) {          
        
        var timeOut = Number(_config.GetConfigValue("getconnect_params", "SyncTestTimeout", "60"));
        var duration = timeOut;
        var sTime = new Date();
        var timer = timeOut;
        timeOut = timeOut * 1000;
        var time;
        var opt = 
        {
          fnPtr    : function(){return _gcutils.TestSync(sDivPrefix);},
          timeout  : timeOut,
          sleepTime: 3000,
          callback : function()
                     {
                       if($('#' + sDivPrefix + "testing").length > 0 )
                       {
                        $('#' + sDivPrefix + "testing").css("display", "block");
                        //time = timer--;
                        //if(time<0) time = 0;
                        //time = time.toString();
                        var remTime = $ss.agentcore.utils.ui.GetRemainingTime(sTime,duration);
                        var uitext = _utils.LocalXLate("snapin_getconnect", sDivPrefix + "testing", [remTime]);
                        $('#' + sDivPrefix + "testing").html(uitext);
                       }
                     }
        } ;
        var result = _uiutils.UpdateUI(opt);   
        return result;  
      },
			Retest: function(){
				var modem = _sdcDSLModem.GetInstance();
				if(modem.GetLastError() == _const.SMAPI_NO_AUTH) {
					var sAdminName = $("#adminname").val();
					var sAdminPass = $("#adminpass").val();
					modem.UseAdminCreds(sAdminName,sAdminPass);
				}

			}
		},
    
    ss_gc_gsr_testsync : {				
      BeforeRender: function(){	
        return _gcHelper["sgG_ss_gc_testsync"].BeforeRender();
      },				
		  AfterRender: function(){	
        return _gcHelper["sgG_ss_gc_testsync"].AfterRender();
      },
      Retest: function(){
        return _gcHelper["sgG_ss_gc_testsync"].Retest();
      }
    },
    
    ss_gc_gsr_testsync1 : {		
      BeforeRender: function(){	
        return _gcHelper["sgG_ss_gc_testsync"].BeforeRender();
      },						
		  AfterRender: function(){	
        return _gcHelper["sgG_ss_gc_testsync"].AfterRender();
      },
      Retest: function(){
        return _gcHelper["sgG_ss_gc_testsync"].Retest();
      }
    },
    
    ss_gc_gsr_testsync2 : {								
      BeforeRender: function(){	
        return _gcHelper["sgG_ss_gc_testsync"].BeforeRender();
      },
		  AfterRender: function(){	
        return _gcHelper["sgG_ss_gc_testsync"].AfterRender();
      },
      Retest: function(){
        return _gcHelper["sgG_ss_gc_testsync"].Retest();
      }
    },
    
    ss_gc_gsr_testsync3 : {								
      BeforeRender: function(){	
        return _gcHelper["sgG_ss_gc_testsync"].BeforeRender();
      },
		  AfterRender: function(){	
        return _gcHelper["sgG_ss_gc_testsync"].AfterRender();
      },
      Retest: function(){
        return _gcHelper["sgG_ss_gc_testsync"].Retest();
      }
    },
    
    ss_gc_gsr_testsync4: {								
      BeforeRender: function(){	
        return _gcHelper["sgG_ss_gc_testsync"].BeforeRender();
      },
		  AfterRender: function(){	
        return _gcHelper["sgG_ss_gc_testsync"].AfterRender();
      },
      Retest: function(){
        return _gcHelper["sgG_ss_gc_testsync"].Retest();
      }
    },
    /* ss_gc_testsync.htm functions - END */
		/* ss_gc_BindAdapter.htm functions - START */   
    sgC_ss_gc_bindadapter : {

      BeforeRender: function() {
        _layout.DisableLayoutBtns();
      },

      AfterRender: function() {
        try {
        // if 9x, skip this work since the previous page walks the user through the
        // manual process (ss_net_BindTcpipToAdapter doesn't work on 9x)   
        var sOS = _system.GetOS();
        if (sOS != "9x") 
        {
          _uiutils.StartShellBusy();
          _system.Sleep(0) ;         //TOREPLACE 

          var sTCPIPVersion = _config.GetConfigValue("getconnect_params", "TCPIPVersion", "4");
          
          var dbResult = "false";
          var oNics = _gcutils.GetNICList();
          for(var sAdapter in oNics) {
            //since we check all, skip adapters that dont need a fixin'
            if(_gcutils.IsNICBoundToTCPIP(sAdapter, sTCPIPVersion)) continue;

            if (this.BindTcpipToAdapterEx(sAdapter, sTCPIPVersion))
            {
              if (this.IsAdapterBound(sAdapter))
              {
                dbResult = "true";                   
              }
            }
          }
        }
        _databag.SetValue("ss_gc_db_SolutionOk", dbResult); 
        _uiutils.EndShellBusy();
         // do auto-next 
        return {move:"forward"};
        } catch(ex){
            _logger.error("sgC_ss_gc_bindadapter: AfterRender()", ex.message);
        }
        finally {_uiutils.EndShellBusy();}
      },
  
      BindTcpipToAdapterEx: function (sAdapter, sTCPIPVersion)
      {
        var result = null;          

        if (sTCPIPVersion == "all")
        {
          result = _netcheck.BindTcpipToAdapter(null, sAdapter);
          result = result && _netcheck.EnableComponentForAdapter(null, sAdapter, "ms_tcpip6", true);
        }
        else if (sTCPIPVersion == "6")
        {
          result = _netcheck.EnableComponentForAdapter(null, sAdapter, "ms_tcpip6", true);
        }
        else
        {
          result = _netcheck.BindTcpipToAdapter(null, sAdapter);
        }
        return result;      
      },
      ena_RetestAdapterBound: function (sAdapter)
      {
        return _netcheck.IsTCPIPboundToAdapter(null, sAdapter);
      },
      IsAdapterBound: function(sAdapter) {          
        
        var timeOut = Number(_config.GetConfigValue("getconnect_params", "RetryFuncTimeout", "45"));

        var timer = timeOut;
        timeOut = timeOut * 1000;
        var time;
        var opt = 
        {
          fnPtr    : function(){return _gcHelper["sgC_ss_gc_bindadapter"].ena_RetestAdapterBound(sAdapter);},
          timeout  : timeOut,
          sleepTime: 1000,
          callback : function()
                     {                      
                     }
        } ;
        var result = _uiutils.UpdateUI(opt);   
        return result;  
      }
    },      
    
    /* ss_gc_BindAdapter.htm functions - END */
    
    /* ss_gc_BindAdapterprompt.htm functions - START */     
    sgC_ss_gc_bindadapterprompt : {        
      BeforeRender: function()
      {
        //Removing the OS check, as 9x is not supported by dynamic agent
        //Also restart functionality is handled differently in dynamic agent
      }
    },
    /* ss_gc_BindAdapterprompt.htm functions - END */
    /* ss_gc_BindAdaptertoeac.htm functions - START */
    sgC_ss_gc_bindadaptertoeac : {      
               
      AfterRender: function()
      {
        try {
        _uiutils.StartShellBusy();
        _system.Sleep(0) ;    // let UI refresh itself to draw page //TOREPLACE 

        var dbResult = "false";
        var oNics = _gcutils.GetNICList();
        for(var sAdapter in oNics) {
          //since we check all, skip adapters that dont need a fixin'
          if(_netcheck.IsComponentBoundToAdapter(null, sAdapter, "nt_eacfilt", 3)) continue;

          if (_netcheck.EnableComponentForAdapter(null, sAdapter, "nt_eacfilt", true))
          {
            if (this.IsAdapterBoundToEAC(sAdapter))
            {
              dbResult = "true";
            }
          }
        }

        _databag.SetValue("ss_gc_db_SolutionOk", dbResult);
        _uiutils.EndShellBusy();
        // do auto-next
        return {move:"forward"};
        } catch(ex){
            _logger.error("sgC_ss_gc_bindadaptertoeac: AfterRender()", ex.message);
        }
        finally {_uiutils.EndShellBusy();}
      },
      eac_RetestAdapterBound: function(sAdapter)
      {
        return _netcheck.IsComponentBoundToAdapter(null, sAdapter, "nt_eacfilt", 3);
      },
      IsAdapterBoundToEAC: function(sAdapter) {     
               
         var timeOut = Number(_config.GetConfigValue("getconnect_params", "RetryFuncTimeout", "45"));

         var timer = timeOut;
         timeOut = timeOut * 1000;
         var time;
         var opt = 
         {
           fnPtr    : function(){return _gcHelper["sgC_ss_gc_bindadaptertoeac"].eac_RetestAdapterBound(sAdapter);},
           timeout  : timeOut,
           sleepTime: 1000,
           callback : function()
                     {                      
                     }
         } ;
         var result = _uiutils.UpdateUI(opt);   
         return result;  
      }
    },
         
    /* ss_gc_BindAdaptertoeac.htm functions - END */
    
    /* ss_gc_checkactivationdate.htm functions - START */  
    ss_gc_gsr_checkactivationdate: {
      AfterRender: function() {
        _layout.updatelayoutbuttons("DISABLE_NEXT");
      },
      ClickYes: function()
      {        
        _databag.SetValue('ss_gc_db_activationdate', 'true');        
        var sTryLater = _utils.LocalXLate("snapin_getconnect","ss_gc_trylater");
        this.UpdateUI(sTryLater);           
        _layout.updatelayoutbuttons("ENABLE_NEXT"); 
      },     
      ClickNo: function()
      {
        _databag.SetValue('ss_gc_db_activationdate', 'false');
        _layout.updatelayoutbuttons("ENABLE_NEXT");			
      },   
      UpdateUI: function(strWarn) {
        $("#ss_gc_trylater").html(strWarn);
        $("#ss_gc_trylater").css("display", "block");
      }       
     },
     
     sgG_ss_gc_checkactivationdate : {
      AfterRender: function() {
        return _gcHelper["ss_gc_gsr_checkactivationdate"].AfterRender();
      },
      ClickYes: function()
      { 
        return _gcHelper["ss_gc_gsr_checkactivationdate"].ClickYes();          
      },     
      ClickNo: function()
      {
        return _gcHelper["ss_gc_gsr_checkactivationdate"].ClickNo();
      }     
     },
     /* ss_gc_checkactivationdate.htm functions - END */
  
     /* ss_gc_setdhcpforadapter.htm functions - START */
     sgC_ss_gc_setdhcpforadapter : {
      
      BeforeRender: function() {
        _layout.DisableLayoutBtns();
      },        
      AfterRender: function()
      {
        try {
        _uiutils.StartShellBusy();
        _system.Sleep(0) ;
        var dbResult = "false";
        var oNics = _gcutils.GetNICList();
        var didEnable = false;
        var sAdapter;

        for(var sAdapter in oNics) {
          //since we check all, skip adapters that dont need a fixin'
          if(_netcheck.IsDHCPEnabled(null, sAdapter) && !(_netcheck.IsConfiguredForStaticDNS(null,sAdapter))) continue;
          
          if (_netcheck.EnableDHCP(null, sAdapter))
          {
            didEnable = true;
            /*
                To address issue with win8 static to dynamic to take effect.
              */
              
              if(_system.GetOS() == "WIN8" || _system.GetOS() == "WIN8_1")
              {
                _netcheck.DisableAdapter(null, sAdapter);
                _system.Sleep(2000);
                _netcheck.EnableAdapter(null, sAdapter);
              }
          else
              /**/
            _system.RefreshDHCP();

            if (this.IsDHCPSetOnAdapter(sAdapter))
            {
              dbResult = "true";
              
            }
          }
        }
        _databag.SetValue("ss_gc_db_SolutionOk", dbResult);
        _uiutils.EndShellBusy();
        //9x machines require a restart for static->dynamic to take effect
        if (didEnable && (_system.GetOSGroup() == "9x"))
        {
          _databag.SetValue("SS_GC_DB_9x_RESTART", "true");
          $("#ss_gc_restartNetAdapter_reboot").css("display", "block");
          $("#ss_gc_dhc_progress_wrap").css("display", "none");          
          _layout.EnableLayoutBtns();
          return;
        }
         // do auto-next
        return {move:"forward"};
        }catch(ex){
           _logger.error("sgC_ss_gc_setdhcpforadapter: AfterRender()", ex.message);
        }
        finally {_uiutils.EndShellBusy();}
      },
      NavNext: function(params) {
          try{
          if(_databag.GetValue("SS_GC_DB_9x_RESTART") == "true") {
            var sessionid = params.element.stepSessionId;
            _uiutils.RestartWithSave(_gcSnapinHomeURL + "?sl_resume=true&sl_session=" + sessionid, true);            
          }
        }
          catch(ex)
          {
            _logger.error("Reboot Machine failed!", ex.message);
          }
          return false;   //preventing the app to navigate to the next page while rebooting is in progress
      },
      dhc_RetestAdapterDHCP: function(sAdapter)
      {
        var bAdapterDynamic = _netcheck.IsDHCPEnabled(null, sAdapter);
        var bAdapterStaticDns = _netcheck.IsConfiguredForStaticDNS(null, sAdapter);
        return (bAdapterDynamic && !bAdapterStaticDns);
      },
      IsDHCPSetOnAdapter: function(sAdapter) {     
               
         var timeOut = Number(_config.GetConfigValue("getconnect_params", "RetryFuncTimeout", "45"));

         var timer = timeOut;
         timeOut = timeOut * 1000;
         var time;
         var opt = 
         {
           fnPtr    : function(){return _gcHelper["sgC_ss_gc_setdhcpforadapter"].dhc_RetestAdapterDHCP(sAdapter);},
           timeout  : timeOut,
           sleepTime: 1000,
           callback : function()
                     {                      
                     }
         } ;
         var result = _uiutils.UpdateUI(opt);   
         return result;  
      }        
     },
     
     ssgcConn_ss_gc_setdhcpforadapter : {
       AfterRender: function()
       {
         return _gcHelper["sgC_ss_gc_setdhcpforadapter"].AfterRender();
       },
       BeforeRender: function()
       {
         return _gcHelper["sgC_ss_gc_setdhcpforadapter"].BeforeRender();
       },
       NavNext: function(params)
       {
         return _gcHelper["sgC_ss_gc_setdhcpforadapter"].NavNext(params);
       }
     },
     /* ss_gc_setdhcpforadapter.htm functions - END */
     /* ss_gc_enableadapter.htm functions - START */
     sgC_ss_gc_enableadapter : {
      
      BeforeRender: function() {
        _layout.DisableLayoutBtns();
      },
       AfterRender: function()
       {
         try {
         // if 9x, skip this work since the previous page walks the user through the
         // manual process (ss_net_EnableAdapter doesn't work on 9x)
         if (_system.GetOS() != "9x") 
         {
           _uiutils.StartShellBusy();
           _system.Sleep(0) //TOREPLACE // let UI refresh itself to draw page

           var dbResult = "false";

           // Some RAS modems do not get categorized as "NetworkCards" under
           // (HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\NetworkCards)          
           // However, in those cases, we should still add those modems to the "nic list" that we are
           // going to enable
           var oNics = _gcutils.GetNICListEx();  // check all nics and ras modems                         
          
           for(var sAdapter in oNics) {
             //since we check all, skip adapters that dont need a fixin'
             if(_netcheck.IsAdapterEnabled(null, sAdapter)) continue;
            
             if (_netcheck.EnableAdapter(null, sAdapter))
             {
               if (this.IsAdapterEnabled(sAdapter))
               {
                 dbResult = "true";
               }
             }
           }
         }
        _databag.SetValue("ss_gc_db_SolutionOk", dbResult);
        _uiutils.EndShellBusy();
         
         // do auto-next
         return {move:"forward"};
         }catch(ex){
            _logger.error("sgC_ss_gc_enableadapter: AfterRender()", ex.message);
         }
         finally {_uiutils.EndShellBusy();}
       },

       ena_RetestAdapterEnabled: function(sAdapter)
       {
         return _netcheck.IsAdapterEnabled(null, sAdapter);
       },
       IsAdapterEnabled: function(sAdapter) {     
               
         var timeOut = Number(_config.GetConfigValue("getconnect_params", "RetryFuncTimeout", "45"));

         var timer = timeOut;
         timeOut = timeOut * 1000;
         var time;
         var opt = 
         {
           fnPtr    : function(){return _gcHelper["sgC_ss_gc_enableadapter"].ena_RetestAdapterEnabled(sAdapter);},
           timeout  : timeOut,
           sleepTime: 1000,
           callback : function()
                     {                      
                     }
         } ;
         var result = _uiutils.UpdateUI(opt);   
         return result;  
      }
      },
      
      ss_gc_gsr_enableadapter : {
        AfterRender: function()
        {
          _gcHelper["sgC_ss_gc_enableadapter"].AfterRender();
        },
        BeforeRender: function()
        {
          _gcHelper["sgC_ss_gc_enableadapter"].BeforeRender();
        }
      },
      
      ss_gc_ras_XXX_enableadapter : {
        AfterRender: function()
        {
          _gcHelper["sgC_ss_gc_enableadapter"].AfterRender();
        },
        BeforeRender: function()
        {
          _gcHelper["sgC_ss_gc_enableadapter"].BeforeRender();
        }
      },
     /* ss_gc_enableadapter.htm functions - END */
     /* ss_gc_enableadapterprompt.htm functions - END */     
     sgC_ss_gc_enableadapterprompt : {
       BeforeRender: function()
       {
        //Removing the OS check, as 9x is not supported by dynamic agent
        //Also restart functionality is handled differently in dynamic agent
        /* if (_system.GetOS() == "9x") 
         {
           _gcutils.ConfigureForWin9xManualReboot();  
           retobj = {OS:"9x"};
           return retobj;
         }
         else
         {
           retobj = {OS:"others"};
           return retobj;
         }*/
       }
      },
      
      ss_gc_gsr_enableadapterprompt : {
        BeforeRender: function()
        {
          _gcHelper["sgC_ss_gc_enableadapterprompt"].BeforeRender();
        }
      },
      
      ss_gc_ras_XXX_enableadapterprompt : {
        BeforeRender: function()
        {
          _gcHelper["sgC_ss_gc_enableadapterprompt"].BeforeRender();
        }
      },
     /* ss_gc_enableadapterprompt.htm functions - END */      
      
      /* ss_gc_checkcables.htm functions - START */
    sgC_ss_gc_checkcables_Modem: {
      AfterRender: function() {
        _layout.updatelayoutbuttons("DISABLE_NEXT");
      },

      ClickYes: function() {      
        _databag.SetValue('ss_gc_db_checkcables', 'true');
        _databag.SetValue('ss_gc_db_OptYesNo', 'yes');
        _layout.updatelayoutbuttons("ENABLE_NEXT");
      },

      ClickNo: function() {      
        _databag.SetValue('ss_gc_db_checkcables', 'false');
        _databag.SetValue('ss_gc_db_OptYesNo', 'no');
        _layout.updatelayoutbuttons("ENABLE_NEXT");
      }
    },
    
    /* ss_gc_checkcables_NoModem.htm functions - START */
    sgC_ss_gc_checkcables_NoModem: {
      AfterRender: function() {
          _layout.updatelayoutbuttons("DISABLE_NEXT");
        },

        ClickYes: function()
        {
          _databag.SetValue('ss_gc_db_checkcables', 'true');
          _databag.SetValue('ss_gc_db_OptYesNo', 'yes');
           _layout.updatelayoutbuttons("ENABLE_NEXT");			
        },

        ClickNo: function()
        {
          _databag.SetValue('ss_gc_db_checkcables', 'false');
          _databag.SetValue('ss_gc_db_OptYesNo', 'no');
          _layout.updatelayoutbuttons("ENABLE_NEXT");			
        }
      },     
      /* ss_gc_checkcables.htm functions - END */
      /* ss_gc_checkfilters.htm functions - START */
      ss_gc_gsr_checkfilters : {
        AfterRender: function()
        {
          _gcu.CreateModemSpecificDiagram("ss_gc_cfl_");          
          _layout.updatelayoutbuttons("DISABLE_NEXT");
        },

        ClickYes: function()
        {
          _databag.SetValue('ss_gc_db_checkfilters', 'true');        
          _databag.SetValue('ss_gc_db_OptYesNo', 'yes');
          _layout.updatelayoutbuttons("ENABLE_NEXT");			
        },

        ClickNo: function()
        {
          _databag.SetValue('ss_gc_db_checkfilters', 'false');        
          _databag.SetValue('ss_gc_db_OptYesNo', 'no');
          _layout.updatelayoutbuttons("ENABLE_NEXT");			
        }
      },
      
      sgG_ss_gc_checkfilters : {
        AfterRender: function() {
          return _gcHelper["ss_gc_gsr_checkfilters"].AfterRender();
        },
        ClickYes: function()
        { 
          return _gcHelper["ss_gc_gsr_checkfilters"].ClickYes();          
        },     
        ClickNo: function()
        {
          return _gcHelper["ss_gc_gsr_checkfilters"].ClickNo();
        }     
      },
      
      ss_gc_ras_680_checkfilters : {
        AfterRender: function() {
          return _gcHelper["ss_gc_gsr_checkfilters"].AfterRender();
        },
        ClickYes: function()
        { 
          return _gcHelper["ss_gc_gsr_checkfilters"].ClickYes();          
        },     
        ClickNo: function()
        {
          return _gcHelper["ss_gc_gsr_checkfilters"].ClickNo();
        }     
      },
      /* ss_gc_checkfilters.htm functions - START */
      /* ss_gc_checkfirewalls.htm functions - START */
      sgC_ss_gc_checkfirewalls : {
        AfterRender: function()
        {
          _layout.updatelayoutbuttons("DISABLE_NEXT");
        },

        ClickYes: function()
        {
          _databag.SetValue('ss_gc_db_firewall', 'true');
          _databag.SetValue('ss_gc_db_OptYesNo', 'yes');
          _layout.updatelayoutbuttons("ENABLE_NEXT");			
        },

        ClickNo: function()
        {
          _databag.SetValue('ss_gc_db_firewall', 'false');
          _databag.SetValue('ss_gc_db_OptYesNo', 'no');
          _layout.updatelayoutbuttons("ENABLE_NEXT");			
        }
      },
      /* ss_gc_checkfirewalls.htm functions - START */
      /* ss_gc_checkmissingadapters.htm functions - START */
      sgC_ss_gc_checkmissingadapters : {
        
        BeforeRender: function() {       
          var missingNics = [];
          missingNics = _gcu.GetMissingNICs();
          _layout.updatelayoutbuttons("DISABLE_NEXT");
          return {missingNics : missingNics};   
        },

        ClickYes: function()
        {
          _databag.SetValue('ss_gc_db_firewall', 'true');
          _databag.SetValue('ss_gc_db_OptYesNo', 'yes');
          _layout.updatelayoutbuttons("ENABLE_NEXT");			
        },

        ClickNo: function()
        {
          _databag.SetValue('ss_gc_db_firewall', 'false');
          _databag.SetValue('ss_gc_db_OptYesNo', 'no');
          _layout.updatelayoutbuttons("ENABLE_NEXT");			
        }
        
      },
      /* ss_gc_checkmissingadapters.htm functions - END */
      /* ss_gc_checksignal.htm functions - START */
      sgC_ss_gc_checksignal : {
      
        AfterRender: function()
        {
          _layout.updatelayoutbuttons("DISABLE_NEXT");
        },

        ClickYes: function()
        {
          _databag.SetValue('ss_gc_db_OptYesNo', 'yes');
          _layout.updatelayoutbuttons("ENABLE_NEXT");			
        },

        ClickNo: function()
        {
          _databag.SetValue('ss_gc_db_OptYesNo', 'no');
          _layout.updatelayoutbuttons("ENABLE_NEXT");			
        }
      },
      /* ss_gc_checksignal.htm functions - END */
      /* ss_gc_checkphoneline.htm functions - START*/
      ss_gc_gsr_checkphoneline : {
        AfterRender: function()
        {	      
          _gcu.CreateModemSpecificDiagram("ss_gc_cph_");          
          _layout.updatelayoutbuttons("DISABLE_NEXT");
        },

        ClickYes: function()
        {
          _databag.SetValue('ss_gc_db_phonecable', 'true');        
          _databag.SetValue('ss_gc_db_OptYesNo', 'yes');
          _layout.updatelayoutbuttons("ENABLE_NEXT");			
        },

        ClickNo: function()
        {
          _databag.SetValue('ss_gc_db_phonecable', 'false'); 
          _databag.SetValue('ss_gc_db_OptYesNo', 'no');
          _layout.updatelayoutbuttons("ENABLE_NEXT");			
        }
      },
      
      sgG_ss_gc_checkphoneline : {
        AfterRender: function() {
          return _gcHelper["ss_gc_gsr_checkphoneline"].AfterRender();
        },
        ClickYes: function()
        { 
          return _gcHelper["ss_gc_gsr_checkphoneline"].ClickYes();          
        },     
        ClickNo: function()
        {
          return _gcHelper["ss_gc_gsr_checkphoneline"].ClickNo();
        } 
      },
      
      ss_gc_ras_680_checkphoneline : {
        AfterRender: function() {
          return _gcHelper["ss_gc_gsr_checkphoneline"].AfterRender();
        },
        ClickYes: function()
        { 
          return _gcHelper["ss_gc_gsr_checkphoneline"].ClickYes();          
        },     
        ClickNo: function()
        {
          return _gcHelper["ss_gc_gsr_checkphoneline"].ClickNo();
        } 
      },
      /* ss_gc_checkphoneline.htm functions - END */
      
      /* ss_gc_checkrouters.htm functions - START */      
      sgC_ss_gc_checkrouters : {
      
        AfterRender: function()
        {         
          _layout.updatelayoutbuttons("DISABLE_NEXT");
        },

        ClickYes: function()
        {
          _databag.SetValue('ss_gc_db_checkrouter', 'true');        
          _databag.SetValue('ss_gc_db_OptYesNo', 'yes');
          _layout.updatelayoutbuttons("ENABLE_NEXT");			
        },

        ClickNo: function()
        {
          _databag.SetValue('ss_gc_db_checkrouter', 'false');
          _databag.SetValue('ss_gc_db_OptYesNo', 'no');
          _layout.updatelayoutbuttons("ENABLE_NEXT");			
        }
      },
      /* ss_gc_checkrouters.htm functions - END */
      /* ss_gc_success.htm functions - START */
      ssgc_SuccessPage : {
        BeforeRender: function() {
          _layout.VFLinkComputerToModemPass();
          _layout.VFLinkModemToWalledGardenPass();
          _layout.VFLinkWalledGardenToInternetPass();  
          _layout.updatelayoutbuttons("ENABLE_CLOSE");
          _layout.updatelayoutbuttons("HIDE_NEXT");
          _layout.updatelayoutbuttons("HIDE_BACK");
        }
      },
      /* ss_gc_success.htm functions - END */
      sgC_ss_gc_WirelessCheckPrompt : {
        NavNext: function()
        {
          if($('#Wireless').attr("checked")) {
            _databag.SetValue("ss_gc_db_repair_wireless_connection", true);
          } else {
            _databag.SetValue("ss_gc_db_repair_wireless_connection", false);
          }
          return true;
        }
      }     
   });
  var _gcutils = $ss.snapin.getconnect.utils;
  var _registry = $ss.agentcore.dal.registry;
  var _utils = $ss.agentcore.utils;
  var _uiutils = $ss.agentcore.utils.ui;
  var _const = $ss.agentcore.constants;
  var _system = $ss.agentcore.utils.system;
  var _config = $ss.agentcore.dal.config;       
  var _gcu = $ss.snapin.getconnect.utils;               
	var _databag = $ss.agentcore.dal.databag;    
	var _netcheck = $ss.agentcore.network.netcheck;
	var _logger = $ss.agentcore.log.GetDefaultLogger("$ss.snapin.getconnect.renderHelper");   
  var _gcHelper = $ss.snapin.getconnect.renderHelper;  
  var _sdcDSLModem = $ss.agentcore.smapi.methods;
  var _layout = $ss.snapin.getconnect.layoutHelper; 
  var _objIni = $ss.agentcore.dal.ini;
  var _gcSnapinHomeURL = _config.GetConfigValue("preconfigured_snapin_url", "subagent_getconnect", "sa/connect/getconnect");
})();



