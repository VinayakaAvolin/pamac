/**
 * @author Vishnu Ratheesh
 */

$ss.snapin = $ss.snapin || {};

$ss.snapin.getconnect = $ss.snapin.getconnect || {};
  
$ss.snapin.getconnect.decisions = $ss.snapin.getconnect.decisions || {};

(function(){
  $.extend($ss.snapin.getconnect.decisions,
  {
    AdapterDisabled: function()
    {
      //For SubAgent we need to check all adapters.  If any of them are disabled
      //return true and try to re-enable.
      var oNics = _gcu.GetNICListEx();  // check all nics and ras modems
      // if no adapter selected, skip this step
      if (oNics == null || oNics == "") return false;
      var bRet = false;
      for(var sAdapter in oNics) {
        if(!_netcheck.IsAdapterEnabled(null,sAdapter)) {
          bRet=true;
          break;
        }
      }      
      return bRet;
    },

    AdapterUnboundToTcpIp: function()
    {
      //For SubAgent we need to check all adapters.  If any of them are disabled
      //return true and try to re-enable.
      var oNics = _gcu.GetNICList();
      // if no adapter selected, skip this step
      if (oNics == null || oNics == "") return false;

      var sTCPIPVersion = _config.GetConfigValue("getconnect_params", "TCPIPVersion", "4");
      
      var bRet = false;
      for(var sAdapter in oNics) {  
        if(!_gcu.IsNICBoundToTCPIP(sAdapter, sTCPIPVersion)) {
          bRet=true;
          break;
        }
      }
      return bRet;
    },

    AdapterUnboundToEACFilter: function()
    {
      // if GC not configured to check for EAC Filter binding, then skip check
      var sCheckEAC = _config.GetConfigValue("getconnect_params", "CheckEACFilt", "false");
      if (sCheckEAC != "true") return false;

      // if 9x, skip this step
      if (_system.GetOS() == "9x") return false;

      // if Nortel not installed, skip the step
      
      var keyString = _registry.EnumRegKey("HKLM", "SYSTEM\\CurrentControlSet\\Enum\\ROOT", "|");
      var keys = keyString.split(",");
      var exists = false;
      for (var i = 0; i < keys.length; i++) {
        if (keys[i] == "NT_EACFILTMP") {
          exists = true;
          break;
        }
      }
      //if the user does not have Nortel adapters, then none need to be bound
      if (!exists) return false;

      //For SubAgent we need to check all adapters.  If any of them are unbound
      //return true and try to bind them.
      var oNics = _gcu.GetNICList();
      // if no adapter selected, skip this step
      if (oNics == null || oNics == "") return false;
      var bRet = false;
      for(var sAdapter in oNics) {
        if(!_netcheck.IsComponentBoundToAdapter(null,sAdapter,"nt_eacfilt",3)) {
          bRet=true;
          break;
        }
      }
      return bRet;
    },

    ShouldFixStaticAdapter: function()
    {
      // if config entry turned off, return false  
      var sForceDHCP = _config.GetConfigValue("getconnect_params", "forceDHCP", "true");
      if (sForceDHCP == "false") return false;

      //For SubAgent we need to check all adapters.  If any of them are disabled
      //return true and try to re-enable.
      var oNics = _gcu.GetNICList();
      // if no adapter selected, skip this step
      if (oNics == null || oNics == "") return false;
      var bRet = false;
      for(var sAdapter in oNics) {
        if(!_netcheck.IsDHCPEnabled(null,sAdapter) || _netcheck.IsConfiguredForStaticDNS(null,sAdapter)) {
          bRet=true;
          break;
        }
      }
      return bRet;
    },

    ShouldHealNetworkWithNoDHCP: function()
    {
      // if config entry turned on, return false (only want to heal static configurations)
      var sForceDHCP = _config.GetConfigValue("getconnect_params", "forceDHCP", "true");
      if (sForceDHCP == "true") return false;

      return this.NetworkHealAvailable();
    },

    ShouldReleaseRenew: function()
    {
      //For SubAgent we need to check all adapters.  If any of them are disabled
      //return true and try to re-enable.
      var oNics = _gcu.GetNICList();
      // if no adapter selected, skip this step
      if (oNics == null || oNics == "") return false;
      var bRet = false;
      for(var sAdapter in oNics) {
        // only try release/renew if DHCP enabled
        if(_netcheck.IsDHCPEnabled(null,sAdapter)) {
          bRet=true;
          break;
        }
      }
      return bRet;
    },

    ShouldRebootAfterHealDUN: function()
    {
      return (_system.GetOS() == "9x");
    },


    NetworkHealAvailable: function()
    {
      return _HealAvailable(_gcu.GetNetworkGuid());
    },

    WirelessHealAvailable: function()
    {
      return _HealAvailable(_gcu.GetWirelessGuid());
    },

    ShouldHealFirefox: function()
    {   
      return ((_gcu.DetectCurrentBrowser() == _const.INET_BROWSER_FIREFOX) && _HealAvailable(_gcu.GetFirefoxGuid()));
    },

    ShouldHealIE: function()
    {
      return ((_gcu.DetectCurrentBrowser() == _const.INET_BROWSER_IE) && _HealAvailable(_gcu.GetIEGuid()));
    },

    ShouldHealDrivers: function()
    {
      return _HealAvailable(_gcu.GetDunDriversGuid());
    },

    ShouldHealDun: function()
    {
      return _HealAvailable(_gcu.GetDunGuid());
    },

    IsIEOffline: function()
    {
      return (_inet.IsIEOffline());
    },

    ShouldDisableProxy: function()
    {
      var bDisableProxy = false;
      
      if (_inet.IsProxyEnabled())
      {
        // temporarily disable proxy and see if it fixes connectivity
        var proxySettings = _inet.GetCurrentProxySettings();
        _inet.DisableProxy();
        bDisableProxy = _inet.BasicConnectionTest(false, false, _const.INET_BROWSER_IE);
        _inet.SetProxySettings(proxySettings);
      }

      return bDisableProxy;
    },
    ShouldDisableProxySettings: function() {
      return (_inet.IsFirefoxProxyEnabled() || _inet.IsProxyEnabled());  
    },

    CheckDNSSpoof: function()
    {
      var sHost       = _config.GetConfigValue("getconnect", "HomeRunHostname", "www.supportsoft.com");
      var sFakeHost   = _config.GetConfigValue("getconnect", "HomeRunFakeHostname", "www.google.com");
      
      var dnsHost     = _inet.DNSLookup(sHost); 
      var dnsFakeHost = _inet.DNSLookup(sFakeHost);

      if (dnsHost == "" || dnsFakeHost == "") return false;  // DNS lookup failed, but at least we're not spoofing
      return (dnsHost == dnsFakeHost);
    },

    GetConnectionType: function()
    {
      // if db value not already set, try to detect it
      var connectType = _databag.GetValue("ss_gc_db_ConnectionType");

      if (connectType == null || connectType == "")
      {
        connectType = "ethernet";

        //For SubAgent we need to check all adapters and if wireless present
        //then set wireless flag?
        var oNics = _gcu.GetNICList();
        if (oNics != null && oNics != "")
        {
          for(var sAdapter in oNics)
            if (_netcheck.IsAdapterWireless(null, sAdapter)) connectType = "wireless";
        }

        // HT:TODO: usb detection??
        _databag.SetValue("ss_gc_db_ConnectionType", connectType);
      }
      return connectType;
    },

    //Use function in getconnectutilhelper.js
    IsNullModem: function() {
      return _gcu.IsNullModem();
    },

    // Return if using Null Modem and configured to use HTTP to test detect
    IsNullModemHttp: function() {
      // TODO:  hook up configuration to use HTTP instead of Ping
      return false;
    },

    NullModemDetectResult: function()
    {
      if (!this.IsNullModem()) return "NormalModem";
      else if (this.IsNullModemHttp())
      {
        if (_databag.GetValue("ss_gc_db_ConnectModemStatus") == "success") return "NullHttpSuccess";
        else return "NullHttpFail";
      }
      else if (_databag.GetValue("ss_gc_db_ConnectModemStatus") == "success") return "NullPingSuccess";
      else return "NullPingFail";
    },

    //Use function in getconnectutilshelper.js
    IsLastModemRas: function()
    {
      return _gcu.IsLastModemRas();
    },

    //Use function in getconnectutilshelper.js
    IsModemRas: function()
    {
      return _gcu.IsModemRas();
    },

    MissingAdapters: function()
    {
      var missingNics = _gcu.GetMissingNICs();
      return (missingNics.length > 0);
    },

    HaveActiveRasConnection: function()
    {
      return _gcu.HaveActiveRasConnection();
    },

    HasSelectedAConn: function()
    {
      if(_databag.GetValue("ss_gc_db_SelectedRASConnection")!=null && _databag.GetValue("ss_gc_db_SelectedRASConnection")!="")
      {
        return "true";
      }
      else
      {
        return "false";
      }
    },


    FindBestRasEntry: function()
    {
      var sNextAction = "create";

      try
      {
        // test if user has previously selected the best entry already
        var sPrevEntry = _databag.GetValue("ss_gc_db_SelectedRASConnection");
        if (sPrevEntry != null && typeof(sPrevEntry) != "undefined" && sPrevEntry != "")
        {
          return "dial";
        }

        var deviceName = _gcu.GetModemDevice();
        var arrEntries = _netcheck.GetRasEntries(null, deviceName);
        
        var ispEntries = _gcu.GetModemRasEntries();
        var RasEntryCountOnSystem = arrEntries.length;
        var RasEntryMatches = 0;
        var CurrentRasEntry = "";

        /*
          Check to verify if the any of the RAS entries defined for the
          Modem is found on the system
        */
        for (var i = 0; i < arrEntries.length; i++)
        {
          for (var j = 0; j < ispEntries.length; j++)
          {
            if (ispEntries[j].toLowerCase() == arrEntries[i].toLowerCase())
            {
              CurrentRasEntry = ispEntries[j];
              RasEntryMatches++;
            }
          }
        }

        // if none of the ras entries are found then create a default ras entry
        if (RasEntryMatches==0 && RasEntryCountOnSystem==0)
        {
          sNextAction = "create";
        }
        else if  (RasEntryMatches == 0 && RasEntryCountOnSystem > 0)
        {
          // now the user has to make a choice
          sNextAction = "prompt";
        }
        else if (RasEntryMatches == 1)
        {
          // found a single ras entry that can be used to dial, why ask?
          _databag.SetValue("ss_gc_db_SelectedRASConnection", CurrentRasEntry);
          sNextAction = "dial";
        }
        else if(RasEntryMatches > 1)
        {
          // found more than one ras entry that can be used to dial. Ask the user to make a choice
          sNextAction = "prompt";
        }
      }
      catch (e)
      {
        _logger.debug("FindBestRasEntry()", e.message);
      }
      return sNextAction;
    },

    GetRasErrCode: function()
    {
      var rasErrorCode = _databag.GetValue("ss_gc_db_RasErrCode");
      if (rasErrorCode == null || typeof(rasErrorCode) == "undefined" || rasErrorCode == "")
      {
        return "no_error";
      }
      return rasErrorCode;
    },

    SaveProxySettings: function()
    {
      var currentIEProxySettings = _inet.GetCurrentProxySettings();

      //Temporarily store the proxy settings in registry
      var sUserName = new String(_config.GetContextValue("SdcContext:UserName"));
      var regKey = "Software\\SupportSoft\\ProviderList\\" + _config.GetContextValue("SdcContext:ProviderId") + "\\" + _config.ParseMacros("%PRODUCT%")+ "\\users\\" + sUserName + "\\ss_config\\ProxySettings\\";

      if (currentIEProxySettings.flags) _registry.SetRegValueByType("HKCU", regKey, "flags", 1, currentIEProxySettings.flags );
      if (currentIEProxySettings.server) _registry.SetRegValueByType("HKCU", regKey, "server", 1, currentIEProxySettings.server );
      if (currentIEProxySettings.bypass) _registry.SetRegValueByType("HKCU", regKey, "bypass", 1, currentIEProxySettings.bypass );
      if (currentIEProxySettings.autoproxy) _registry.SetRegValueByType("HKCU", regKey, "autoproxy", 1, currentIEProxySettings.autoproxy );

      if (_inet.IsFirefoxProxyEnabled())
      {
        var intFirefoxProxyType = _utils.GetFirefoxUserPref("network.proxy.type");
        _registry.SetRegValueByType("HKCU", regKey, "firefoxProxy", 1,  intFirefoxProxyType);
      }

    },

    RestoreProxySettings: function()
    {
      var sUserName = new String(_config.GetContextValue("SdcContext:UserName"));
      var regKey = "Software\\SupportSoft\\ProviderList\\" + _config.GetContextValue("SdcContext:ProviderId") + "\\" + _config.ParseMacros("%PRODUCT%") + "\\users\\" + sUserName + "\\ss_config\\ProxySettings\\";
      var IEProxySettings = {
        flags:      "",
        server:     "", 
        bypass:     "", 
        autoproxy:  "" 
      }; 
      
      if (_registry.RegValueExists("HKCU", regKey, "flags"))
      {
        IEProxySettings.flags = _registry.GetRegValue("HKCU", regKey, "flags");
        _registry.DeleteRegVal("HKCU", regKey, "flags");
      }
      if (_registry.RegValueExists("HKCU", regKey, "server"))
      {
        IEProxySettings.server = _registry.GetRegValue("HKCU", regKey, "server");
        _registry.DeleteRegVal("HKCU", regKey, "server");
      }
      if (_registry.RegValueExists("HKCU", regKey, "bypass"))
      {
        IEProxySettings.bypass = _registry.GetRegValue("HKCU", regKey, "bypass");
        _registry.DeleteRegVal("HKCU", regKey, "bypass");
      }
      if (_registry.RegValueExists("HKCU", regKey, "autoproxy"))
      {
        IEProxySettings.autoproxy = _registry.GetRegValue("HKCU", regKey, "autoproxy");
        _registry.DeleteRegVal("HKCU", regKey, "autoproxy");
      }
      //Commenting out this code for now, as this makes some proxy settings in IE and causes internet failure issue
     // _inet.SetProxySettings(IEProxySettings);
      
      if (_registry.RegValueExists("HKCU", regKey, "firefoxProxy"))
      {
        var FirefoxProxy = _registry.GetRegValue("HKCU", regKey, "firefoxProxy");
        _inet.SetFirefoxUserPref("network.proxy.type", FirefoxProxy, 2);
        _registry.DeleteRegVal("HKCU", regKey, "firefoxProxy"); 
      }

    },
    IsBroswerOffline: function() {
      return (_inet.IsFirefoxOffline()|| _inet.IsIEOffline());
    },

    ResetBrowserSettings: function()
    {
      this.SaveProxySettings();
      _inet.DisableProxy();
      _inet.DisableIEOffline();
      if (_inet.IsFirefoxOffline())
        _inet.DisableFirefoxOffline();
      if (_inet.IsFirefoxProxyEnabled())
        _inet.DisableFirefoxProxy();        
    },
    IsFirefoxCurrentBrowser: function()
    {
      return (_gcu.DetectCurrentBrowser() == _const.INET_BROWSER_FIREFOX);
    },
    ShouldDisableFirefoxProxy: function()
    {
      var bDisableProxy = false;

      if (_inet.IsFirefoxProxyEnabled())
      {
        // temporarily disable proxy and see if it fixes connectivity

        var proxySettings = _inet.GetCurrentProxySettings();
        _inet.DisableFirefoxProxy();
        bDisableProxy = _inet.BasicConnectionTest(false, false, _const.INET_BROWSER_IE);
        _inet.SetProxySettings(proxySettings);
    }

      return bDisableProxy;
    },
    
    IsModemFlowNeeded: function() {      
    
      var isModemFlowRequired = _databag.GetValue("ss_gc_modem_flow_required");
      
      if(isModemFlowRequired == null || isModemFlowRequired == "" || isModemFlowRequired == "undefined")
      {    
        isModemFlowRequired = $ss.agentcore.dal.config.GetConfigValue("modems", "modem_flow_required", "true");
        _databag.SetValue("ss_gc_modem_flow_required", isModemFlowRequired);
      }
      return isModemFlowRequired;
    },

    CanPingExternalsite: function() {
      _databag.SetValue("ss_gc_db_pingSuccess", false);
      $ss.snapin.getconnect.layoutHelper.DisableLayoutBtns();
      var bRet = false;
      try {
        var sites = _config.GetValues("modems", "internet_pingsite");
        for (var cnt = 0; cnt < sites.length; cnt++) {
          _utils.Sleep(1500);
          if (_netCheck.Ping(sites[cnt])) {
            bRet = true;
            break;
          }
          _utils.Sleep(1500);
        }
      } catch (ex) {
        _logger.error("Pinging the websites: ", ex.message);
      }      
      _databag.SetValue("ss_gc_db_pingSuccess", bRet);

      return bRet;
    }

}); 
  
var _utils = $ss.agentcore.utils;
var _system = $ss.agentcore.utils.system;
var _config = $ss.agentcore.dal.config;
var _databag = $ss.agentcore.dal.databag;
var _registry = $ss.agentcore.dal.registry;
var _network = $ss.agentcore.network.network;
var _netcheck = $ss.agentcore.network.netcheck;
var _inet = $ss.agentcore.network.inet;
var _const = $ss.agentcore.constants;
var _logger = $ss.agentcore.log.GetDefaultLogger("$ss.snapin.getconnect.decisions");  

var _gcu = $ss.snapin.getconnect.utils;
var _gcd = $ss.snapin.getconnect.decisions;


//---------------------------------------------------------- PRIVATE FUNCTIONS ----------------------------------------------

function _HealAvailable(sGuid)
{
  try{
    if (sGuid == "") return false;

    //Get the DNA Probe object to check for protections...
    var oMyProbe = _gcu.GetProbe();
    if(!oMyProbe) return false;

    return oMyProbe.IsProtectedByGuid(sGuid);
  } catch(Error){
  }
}

})();