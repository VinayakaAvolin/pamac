﻿/**
 * @author Vishnu Ratheesh
 */
$ss.snapin = $ss.snapin ||{}; 
$ss.snapin.getconnect = $ss.snapin.getconnect || {};
$ss.snapin.getconnect.renderHelper = $ss.snapin.getconnect.renderHelper || {};

(function(){
	$.extend($ss.snapin.getconnect.renderHelper,{
    /* ss_gc_detectmodem.htm functions - START */
    
    sgC_ss_gc_detectmodem: {
      BeforeRender: function() {
        _layout.DisableLayoutBtns();
      },
      AfterRender: function() {
				//Set Visual-Flow animation parameters
				_databag.SetValue("ss_gc_db_vflink1_animation", true);
				_layout.AutoUpdateVisualFlowState();
	      	      
        // Start always with clean slate if we are not here because of
        // authentication failures
        if (_databag.GetValue("ss_gc_db_ModemDetected") != "no_auth") {
          _databag.GetValue("ss_gc_db_ModemDetected", "false");
        }
        try{
          if (_databag.GetValue("ss_gc_db_RunTestFirst") || _databag.GetValue("ss_gc_db_DoDetectModem")) 
          {
            _uiutils.StartShellBusy();
            var bRet = this.execDetectModem("ss_gc_dm_");
            if(bRet) 
            {
              _databag.SetValue("ss_gc_db_ModemDetected", "true");
              // do an auto next when things are OK
              return {move: "forward"};		
            } 
            else if (_sdcDSLModem.GetLastError() == _constants.SMAPI_NO_AUTH)   
            {
              _gcUtils.OnNoAuth("ss_gc_dm_");
              _databag.SetValue("ss_gc_db_ModemDetected", "no_auth");
              _layout.AutoUpdateVisualFlowState();
              _layout.EnableLayoutBtns();
              _layout.updatelayoutbuttons("ENABLE_RETRY");
            }
            else if (_sdcDSLModem.GetLastError() == _constants.SMAPI_DO_REBOOT)   
            { 
              _databag.SetValue("ss_gc_db_ModemDetected", "do_reboot");
              // do an auto next on reboot as well
              return {move: "forward"};		
            } 
            else 
            {
              _databag.SetValue("ss_gc_db_ModemDetected", "false");  
              // do an auto next on all other sync failures              
              return {move: "forward"};		
            }
          }
          _uiutils.EndShellBusy();
          }catch(ex){
            _logger.error("ss_gc_detectmodem: AfterRender()", ex.message);
          }
          finally {_uiutils.EndShellBusy();}
        },
        execDetectModem: function(sDivPrefix) {          

          // DB: Whenever trying to detect modem we should force a redetect
          // Cases where a previous run of detect chose "null modem" need not be valid.
          // Only in cases of "no auth" we can rely on valid modem detection
          // FZ: also the case that "autodetect" is off so we will not delete the SI_MODEM_CODE for smartaccess.
          // SI_MODEM_CODE for smartaccess should always be the choosing modem name, not null_modem_dsl or an empty key.
          
          var timeOut = Number(_config.GetConfigValue("getconnect_params", "ModemDetectTimeout", "60"));
          var duration = timeOut;
          var sTime = new Date();
          var timer = timeOut;
          timeOut = timeOut * 1000;
          var time;
          var opt = 
          {
            fnPtr    : function(){return _gcUtils.DetectModem(sDivPrefix);},
            timeout  : timeOut,
            sleepTime: 3000,
            callback : function()
                       {
                         if($('#' + sDivPrefix + "testing").length > 0 )
                         {
                          $('#' + sDivPrefix + "testing").css("display", "block");
                          var remTime = $ss.agentcore.utils.ui.GetRemainingTime(sTime,duration);
                          //time = timer--;
                          //if(time<0) time = 0;
                          //time = time.toString();
                          var uitext = _utils.LocalXLate("snapin_getconnect", sDivPrefix + "testing", [remTime]);
                          $('#' + sDivPrefix + "testing").html(uitext);
                         }
                       }
          } ;
          var result = _uiutils.UpdateUI(opt);   
          return result;  
        },
        
        Retest: function() {
          var modem  = _sdcDSLModem.GetInstance();
          if(modem.GetLastError() == _constants.SMAPI_NO_AUTH) {
            var sAdminName = document.getElementById("adminname").value;
            var sAdminPass = document.getElementById("adminpass").value;
            modem.UseAdminCreds(sAdminName,sAdminPass);
          }  
        }
    },
    ss_gc_cmr_detectmodem_ras : {
      AfterRender: function()
      {
        return _gcHelper["sgC_ss_gc_detectmodem"].AfterRender();
      },
      BeforeRender: function() { 
        return _gcHelper["sgC_ss_gc_detectmodem"].BeforeRender();
      },
      Retest: function() {
        return _gcHelper["sgC_ss_gc_detectmodem"].Retest();
      }
    },
    
    ss_gc_cmr_detectmodem_ras2 : {
      AfterRender: function()
      {
        return _gcHelper["sgC_ss_gc_detectmodem"].AfterRender();
      },
      BeforeRender: function() { 
        return _gcHelper["sgC_ss_gc_detectmodem"].BeforeRender();
      },
      Retest: function() {
        return _gcHelper["sgC_ss_gc_detectmodem"].Retest();
      }
    },
    
    ss_gc_cmr_detectmodem_ras3 : {
      AfterRender: function()
      {
        return _gcHelper["sgC_ss_gc_detectmodem"].AfterRender();
      },
      BeforeRender: function() { 
        return _gcHelper["sgC_ss_gc_detectmodem"].BeforeRender();
      },
      Retest: function() {
        return _gcHelper["sgC_ss_gc_detectmodem"].Retest();
      }
    },
    
    sgC_ss_gc_detectmodem2 : {
      AfterRender: function()
      {
        return _gcHelper["sgC_ss_gc_detectmodem"].AfterRender();
      },
      BeforeRender: function() { 
        return _gcHelper["sgC_ss_gc_detectmodem"].BeforeRender();
      },
      Retest: function() {
        return _gcHelper["sgC_ss_gc_detectmodem"].Retest();
      }
    },
    
    sgC_ss_gc_detectmodem3 : {
      AfterRender: function()
      {
        return _gcHelper["sgC_ss_gc_detectmodem"].AfterRender();
      },
      BeforeRender: function() { 
        return _gcHelper["sgC_ss_gc_detectmodem"].BeforeRender();
      },
      Retest: function() {
        return _gcHelper["sgC_ss_gc_detectmodem"].Retest();
      }
    },
    
    sgC_ss_gc_detectmodem3a : {
      AfterRender: function()
      {
        return _gcHelper["sgC_ss_gc_detectmodem"].AfterRender();
      },
      BeforeRender: function() { 
        return _gcHelper["sgC_ss_gc_detectmodem"].BeforeRender();
      },
      Retest: function() {
        return _gcHelper["sgC_ss_gc_detectmodem"].Retest();
      }
    },
    
    sgC_ss_gc_detectmodem4 : {
      AfterRender: function()
      {
        return _gcHelper["sgC_ss_gc_detectmodem"].AfterRender();
      },
      BeforeRender: function() { 
        return _gcHelper["sgC_ss_gc_detectmodem"].BeforeRender();
      },
      Retest: function() {
        return _gcHelper["sgC_ss_gc_detectmodem"].Retest();
      }
    },
    
    sgC_ss_gc_detectmodem5 : {
      AfterRender: function()
      {
        return _gcHelper["sgC_ss_gc_detectmodem"].AfterRender();
      },
      BeforeRender: function() { 
        return _gcHelper["sgC_ss_gc_detectmodem"].BeforeRender();
      },
      Retest: function() {
        return _gcHelper["sgC_ss_gc_detectmodem"].Retest();
      }
    },
    
    sgC_ss_gc_detectmodem6 : {
      AfterRender: function()
      {
        return _gcHelper["sgC_ss_gc_detectmodem"].AfterRender();
      },
      BeforeRender: function() { 
        return _gcHelper["sgC_ss_gc_detectmodem"].BeforeRender();
      },
      Retest: function() {
        return _gcHelper["sgC_ss_gc_detectmodem"].Retest();
      }
    },
    
    sgC_ss_gc_detectmodem5_1 : {
      AfterRender: function()
      {
        return _gcHelper["sgC_ss_gc_detectmodem"].AfterRender();
      },
      BeforeRender: function() { 
        return _gcHelper["sgC_ss_gc_detectmodem"].BeforeRender();
      },
      Retest: function() {
        return _gcHelper["sgC_ss_gc_detectmodem"].Retest();
      }
    },
    
    sgC_ss_gc_detectmodem5_2 : {
      AfterRender: function()
      {
        return _gcHelper["sgC_ss_gc_detectmodem"].AfterRender();
      },
      BeforeRender: function() { 
        return _gcHelper["sgC_ss_gc_detectmodem"].BeforeRender();
      },
      Retest: function() {
        return _gcHelper["sgC_ss_gc_detectmodem"].Retest();
      }
    },
    
    sgC_ss_gc_detectmodem7 : {
      AfterRender: function()
      {
        return _gcHelper["sgC_ss_gc_detectmodem"].AfterRender();
      },
      BeforeRender: function() { 
        return _gcHelper["sgC_ss_gc_detectmodem"].BeforeRender();
      },
      Retest: function() {
        return _gcHelper["sgC_ss_gc_detectmodem"].Retest();
      }
    },
    
    sgC_ss_gc_detectmodem8 : {
      AfterRender: function()
      {
        return _gcHelper["sgC_ss_gc_detectmodem"].AfterRender();
      },
      BeforeRender: function() { 
        return _gcHelper["sgC_ss_gc_detectmodem"].BeforeRender();
      },
      Retest: function() {
        return _gcHelper["sgC_ss_gc_detectmodem"].Retest();
      }
    },
    
    sgC_ss_gc_detectmodem9 : {
      AfterRender: function()
      {
        return _gcHelper["sgC_ss_gc_detectmodem"].AfterRender();
      },
      BeforeRender: function() { 
        return _gcHelper["sgC_ss_gc_detectmodem"].BeforeRender();
      },
      Retest: function() {
        return _gcHelper["sgC_ss_gc_detectmodem"].Retest();
      }
    },
    
    sgC_ss_gc_detectmodem10 : {
      AfterRender: function()
      {
        return _gcHelper["sgC_ss_gc_detectmodem"].AfterRender();
      },
      BeforeRender: function() { 
        return _gcHelper["sgC_ss_gc_detectmodem"].BeforeRender();
      },
      Retest: function() {
        return _gcHelper["sgC_ss_gc_detectmodem"].Retest();
      }
    },
    
    sgC_ss_gc_detectmodem1 : {
      AfterRender: function()
      {
        return _gcHelper["sgC_ss_gc_detectmodem"].AfterRender();
      },
      BeforeRender: function() { 
        return _gcHelper["sgC_ss_gc_detectmodem"].BeforeRender();
      },
      Retest: function() {
        return _gcHelper["sgC_ss_gc_detectmodem"].Retest();
      }
    },
    
    sgC_ss_gc_detectmodem12 : {
      AfterRender: function()
      {
        return _gcHelper["sgC_ss_gc_detectmodem"].AfterRender();
      },
      BeforeRender: function() { 
        return _gcHelper["sgC_ss_gc_detectmodem"].BeforeRender();
      },
      Retest: function() {
        return _gcHelper["sgC_ss_gc_detectmodem"].Retest();
      }
    },
    
    sgC_ss_gc_detectmodem12 : {
      AfterRender: function()
      {
        return _gcHelper["sgC_ss_gc_detectmodem"].AfterRender();
      },
      BeforeRender: function() { 
        return _gcHelper["sgC_ss_gc_detectmodem"].BeforeRender();
      },
      Retest: function() {
        return _gcHelper["sgC_ss_gc_detectmodem"].Retest();
      }
    },
    
    sgC_ss_gc_detectmodem11 : {
      AfterRender: function()
      {
        return _gcHelper["sgC_ss_gc_detectmodem"].AfterRender();
      },
      BeforeRender: function() { 
        return _gcHelper["sgC_ss_gc_detectmodem"].BeforeRender();
      },
      Retest: function() {
        return _gcHelper["sgC_ss_gc_detectmodem"].Retest();
      }
    },
    /* ss_gc_detectmodem.htm functions - END */
    
    /* ss_gc_reboot_modem.htm functions - START */    
    sgG_ss_gc_rebootmodem : {
      
      BeforeRender: function() {
        _layout.DisableLayoutBtns();
      },
			AfterRender: function(){
        
        try {
          _uiutils.StartShellBusy();
          _system.Sleep(0); // let UI refresh itself to draw page
          var dbResult = this.execDoReboot("ss_gc_rm_");
          
          // set databag and do auto-next
          _databag.SetValue("ss_gc_db_SolutionOk", dbResult);
          _uiutils.EndShellBusy();
          _layout.EnableLayoutBtns();
          return {move:"forward"};
          
        } catch (ex) {
          _logger.error("ss_gc_reboot_modem: AfterRender()", ex.message);
        }          
        finally {_uiutils.EndShellBusy();}
      },
      execDoReboot: function(sDivPrefix) {          
        
        var timeOut = Number(_config.GetConfigValue("getconnect_params", "ModemRebootTimeout", "60"));
        var duration = timeOut;
        var sTime = new Date();
        var timer = timeOut;
        timeOut = timeOut * 1000;
        var time;
        var opt = 
        {
          fnPtr    : function(){return _gcUtils.DoReboot();},
          timeout  : timeOut,
          sleepTime: 3000,
          callback : function()
                     {
                       if($('#' + sDivPrefix + "Progress").length > 0 )
                       {
                        $('#' + sDivPrefix + "Progress").css("display", "block");
                        var remTime = $ss.agentcore.utils.ui.GetRemainingTime(sTime,duration);
                        //time = timer--;
                        //if(time<0) time = 0;
                        //time = time.toString();
                        var uitext = _utils.LocalXLate("snapin_getconnect", sDivPrefix + "Progress", [remTime]);
                        $('#' + sDivPrefix + "Progress").html(uitext);
                       }
                     }
        } ;
        var result = _uiutils.UpdateUI(opt);   
        return result;  
      }
    },
    
    sgC_ss_gc_rebootmodem : {
      BeforeRender: function(){
        return _gcHelper["sgG_ss_gc_rebootmodem"].BeforeRender();
      },
			AfterRender: function(){
        return _gcHelper["sgG_ss_gc_rebootmodem"].AfterRender();
      }
    },
    /* ss_gc_reboot_modem.htm functions - END */
    
    /* ss_gc_ras_checkrasmodem.htm functions - START */    
    ss_gc_ras_checkLastRasModem : {
      BeforeRender: function()
      {
        //no necessary if-it-is-ras check needed because this page only can be reached if it is ras modem.
        var LastModemCode = _config.GetConfigValue(_constants.SMAPI_MODEM, _constants.SMAPI_DB_LASTDETECTED_MODEMNAME, "");
        
        var modemStr = $ss.agentcore.utils.LocalXLate("snapin_getconnect","ss_gc_crm_Content2", [LastModemCode]); 
        return {modemStr: modemStr};
      },
      AfterRender: function()
      {
        _layout.updatelayoutbuttons("DISABLE_NEXT");
      },
      ClickYes: function()
      {
        _databag.SetValue('ss_gc_db_checkrasmodem', 'true');
        _databag.SetValue('ss_gc_db_OptYesNo', 'yes');
        _layout.updatelayoutbuttons("ENABLE_NEXT");	 
      },

      ClickNo: function()
      {
        _databag.SetValue('ss_gc_db_checkrasmodem', 'false');
        _databag.SetValue('ss_gc_db_OptYesNo', 'no');
        _layout.updatelayoutbuttons("ENABLE_NEXT");
      }
    },      
    /* ss_gc_ras_checkrasmodem.htm functions - END */
    
    /* ss_gc_ras_createrasentry.htm functions - START */
    ssgcrdci_ss_gc_ras_CreateRasEntry : {
      BeforeRender: function()
      {
        if (_databag.GetValue("ss_gc_db_ras_UpdateCreds"))
        {
          retobj = {updatecreds:"true"};
          return retobj;
        }
        else
        {
          retobj = {updatecreds:"false"};
          return retobj;
        }            
      },
      
      AfterRender: function() {
        var oRet = this.rcras_PopulateCreds();
        if(oRet) this.UpdateUserInputs(oRet);
      },             
      UpdateUserInputs: function(oCreds)
      {
        if(oCreds)
        {
          $("#ss_gc_rcras_username_input").val(oCreds.sName);
          $("#ss_gc_rcras_userpwd_input").val(oCreds.sPassword);
        }
      }, 
      //-----------------------------------------------------------------------
      rcras_PopulateCreds: function()
      {
        // Check if creds are in the common location
        var oCreds = this.rcras_CheckReg();
        if (oCreds) 
          return oCreds;
      },
      
      //-----------------------------------------------------------------------
      rcras_CheckReg: function()
      {
        var oRet = new Object();
        // When SmartAccess saves user creds in this common area 
        // GetConnected could pick it up
        var sRoot = _config.GetRegRoot();
        //var sRoot = "SOFTWARE\\SupportSoft\\ProviderList\\demoagent"
        var sProduct = _config.ParseMacros("%PRODUCT%");
        //var sProduct = "demoagent";
        var re = new RegExp("\\"+sProduct);
        sRoot = sRoot.replace(re, "");
        
        // Get unique username /password to be set
         var sName     = _registry.GetRegValue(_constants.REG_TREE, sRoot, "username");
        var sPassword = _registry.GetRegValue(_constants.REG_TREE, sRoot, "userpass");
        if(sName && sPassword) {
          oRet.sName = sName;
          oRet.sPassword = _utils.DecryptHexToStr(sPassword);
        }
        return oRet;
      },   
      
      NavNext: function()
      {
        if(this.ValidateInputs() == false)    
          return false;

        try 
        {
          var connName = "";
          
          var username = $("#ss_gc_rcras_username_input").val();
          var password = $("#ss_gc_rcras_userpwd_input").val();
          
          // use existing connection name if it was specified
          if(_databag.GetValue("ss_gc_db_SelectedRASConnection")!=null && _databag.GetValue("ss_gc_db_SelectedRASConnection")!="")
          {
            connName = _databag.GetValue("ss_gc_db_SelectedRASConnection");
          }
          else
          {
            connName = _gcUtils.GetModemDefaultRasEntry(); 
          }
          var sCreate = "";
          this.UpdateUI(connName, "progress");
          // if we're updating the creds on an existing connection, we need to delete the existing one and then do a create w/new creds
          var result = this.rcras_CreateRasEntry(connName, username, password);
          if (result && _gcUtils.HaveRasEntry(connName))
          {          
            _databag.SetValue("ss_gc_db_SolutionOk", "true");
            this.UpdateUI(connName, "success");          
            _databag.SetValue("ss_gc_db_SelectedRASConnection", connName);
          }
          else
          {
            _databag.SetValue("ss_gc_db_SolutionOk", "false");
            this.UpdateUI(connName, "fail");  
          }
          
        }
        catch (ex)
        {
          _logger.error("NavNext()", ex.message);
        }
        
        return true;
      },  
      ValidateInputs: function() {
        var username = $("#ss_gc_rcras_username_input").val();
        var password = $("#ss_gc_rcras_userpwd_input").val();
       
        if (username.length == 0 || password.length == 0)
        {
          var sWarning = $ss.agentcore.utils.LocalXLate("snapin_getconnect","ss_gc_rcras_NeedUserAndPw"); 
          $("#ss_gc_rcras_input_error").html(sWarning);
          return false;
        }
        return true;
      },       
      UpdateUI: function(connName,state) {
  
        switch (state)
        {
          case "progress":          
            $("#ss_gc_rcras_Prompt").css("display", "none");
            $("#ss_gc_rcras_user1").css("display", "none");       
            $("#ss_gc_rcras_userupdate").css("display", "none");              
            $("#ss_gc_rcras_Working").css("display", "block");
            $("#ss_gc_rcras_input_error").css("display", "none");
            $("#ss_gc_rcras_creating").html($ss.agentcore.utils.LocalXLate("snapin_getconnect","ss_gc_rcras_creating", [connName]));
            break;
          case "success":
            $("#ss_gc_rcras_creating").append($ss.agentcore.utils.LocalXLate("snapin_getconnect", "ss_gc_rcras_creating_success"));
            break;
          case "fail":
           $("#ss_gc_rcras_creating").html($ss.agentcore.utils.LocalXLate("snapin_getconnect", "ss_gc_rcras_creating_failed"));
            break;        
         }
       },           

      //-----------------------------------------------------------------------
      rcras_CreateRasEntry: function(connName, username, password)
      {
        // Create Phonebook entry
        var deviceName = _gcUtils.GetModemDevice();
        var dsl_link_type = _gcUtils.GetModemInstanceValue("dsl_link_type", "PPPoA");                        
        return this.rcras_ConfigureUser(dsl_link_type, connName, username, password, deviceName);
      },
      
      //-----------------------------------------------------------------------
      rcras_ConfigureUser: function(dsl_link_type, connectionname, username, password, deviceName)
      {
        var result = false;        
        try 
        {
          // 1. Disconnect the active connection
          _logger.debug("rcras_ConfigureUser", "Disconnecting : " + connectionname);
          _netCheck.RasDisconnect(null, connectionname);
          
          // 2. Delete the existing connection
          _logger.debug("rcras_ConfigureUser", "Deleting : " + connectionname);
          _netCheck.DelRasEntry(null, "", connectionname);
           
          // 3. Create new connection
          _logger.debug("rcras_ConfigureUser", "Creating connection : [" + connectionname + "] username : [" + username + "] password : [" + password + "]");
          if (dsl_link_type.toLowerCase() == "PPPoA".toLowerCase())
          {
            result = _netCheck.CreatePPPoARasEntry(null, connectionname, "0", "0", username, password, "nodialrules", deviceName);      
          }
          else if (dsl_link_type.toLowerCase() == "PPPoE".toLowerCase())
          {
            result = _netCheck.CreatePPPoERasEntry(null, connectionname, "0", "0", username, password, "nodialrules");
          }          
          _logger.debug("rcras_ConfigureUser", "Connection create: " + result);
        }
        catch(e)
        {
          _logger.debug("rcras_ConfigureUser", "Connection create failed - exception:" + e.message);
        }
        return result;
      }
     },
     ss_gc_ras_691_UpdateCreds : {
       AfterRender: function(){
         return _gcHelper["ssgcrdci_ss_gc_ras_CreateRasEntry"].AfterRender();     
       },
       BeforeRender: function(){
         return _gcHelper["ssgcrdci_ss_gc_ras_CreateRasEntry"].BeforeRender();     
       },
       NavNext: function(){
         return _gcHelper["ssgcrdci_ss_gc_ras_CreateRasEntry"].NavNext();     
       }
     },
     /* ss_gc_ras_createrasentry.htm functions - END */
     /* ss_gc_ras_dialrasentry.htm functions - START */
     ssgcrdci_ss_gc_ras_DialRasEntry : {        
    
       BeforeRender: function() 
       {
         var connName = _databag.GetValue("ss_gc_db_SelectedRASConnection");          
         var dialingStr = $ss.agentcore.utils.LocalXLate("snapin_getconnect","ss_gc_rdras_dial_ras_connection", [connName]);
		     _layout.DisableLayoutBtns();
         return {SelectedRAS:dialingStr};
       },

       //-----------------------------------------------------------------------
       AfterRender: function()
       {
         try {
         _uiutils.StartShellBusy();
         var nTimeoutSecs = Number(_config.GetConfigValue("getconnect_params", "ras_dialTimeout", "25"));
         var nMaxRetries = Number(_config.GetConfigValue("getconnect_params", "ras_retryCount", "1"));
         var nRetryPauseSecs = Number(_config.GetConfigValue("getconnect_params", "ras_retryPauseTime", "5"));
         
         var connName = _databag.GetValue("ss_gc_db_SelectedRASConnection"); 
         _system.Sleep(1000);
         var bSuccess = this.DialLoop(connName, this.OnDialComplete, nTimeoutSecs, nMaxRetries, nRetryPauseSecs);
         _system.Sleep(1000);
         _uiutils.EndShellBusy();
         if (!bSuccess)
         {
           this.UpdateUI();
           _layout.EnableLayoutBtns();
           _layout.updatelayoutbuttons("ENABLE_RETRY");
         }
         else
         {
           // do auto-next
           return {move:"forward"};
         }
         } catch(ex) {_logger.error("ssgcrdci_ss_gc_ras_DialRasEntry:AfterRender",ex.message); }
         finally {_uiutils.EndShellBusy();}
       },
       UpdateUI: function(state)
       {
         switch(state)
         {
          case "redial":
            $("#ss_gc_rdras_dialing_placeholder").append($ss.agentcore.utils.LocalXLate("snapin_getconnect","ss_gc_rdras_redial"));
            break;
          case "success":
            $("#ss_gc_rdras_dialing_placeholder").append($ss.agentcore.utils.LocalXLate("snapin_getconnect","ss_gc_rdras_dial_success"));
            break;
          case "fail":        
            $("#ss_gc_rdras_dialing_placeholder").append( $ss.agentcore.utils.LocalXLate("snapin_getconnect","ss_gc_rdras_dial_error"));
            break;
          case "timeout":
            $("#ss_gc_rdras_dialing_placeholder").append($ss.agentcore.utils.LocalXLate("snapin_getconnect","ss_gc_rdras_dial_timedout"));
            break;
          default:
            $("#ss_gc_rdras_dial_status").css("display", "block");
            break;
         }
       },

       //-----------------------------------------------------------------------
       OnDialComplete: function(oRet)
       {
         var bContinue = true;

         if (!oRet.bSuccess)
         {
           if (oRet.nLastError == -1)  // request timed out
           {
             _gcHelper["ssgcrdci_ss_gc_ras_DialRasEntry"].UpdateUI("timeout");
             _databag.SetValue("ss_gc_db_RasErrCode", "638");  // 638 is "The request has timed out"
           }
           else
           {
             var rasErrCode = oRet.nLastError;
             _gcHelper["ssgcrdci_ss_gc_ras_DialRasEntry"].UpdateUI("fail");
             _databag.SetValue("ss_gc_db_RasErrCode", rasErrCode);

             // retry wouldn't help in these 2 situations
             if (rasErrCode == 619 && rasErrCode == 691)
             {
               bContinue = false;
             }
           }
         }
         else
         {
           bContinue = false;  // success
           _databag.SetValue("ss_gc_db_RasErrCode", "");  // clear ras error code
           _gcHelper["ssgcrdci_ss_gc_ras_DialRasEntry"].UpdateUI("success");
         }

         return bContinue;
       },

       //-----------------------------------------------------------------------
       DialLoop: function(sConnName, fnOnDialComplete, nTimeoutSecs, nMaxRetries, nRetryPauseSecs)
       {
         var bContinue = true;
         var nTries = 1;
         do
         {
           // sleep before retrying
           if (nTries > 1)
           {
             if (bContinue)  this.UpdateUI("redial");
             _system.Sleep(nRetryPauseSecs * 1000);
           }

           var oRet = _netcheck.RasDialEntry(null, sConnName, nTimeoutSecs);
           bContinue = fnOnDialComplete(oRet);
         } while (bContinue && (nTries++ <= nMaxRetries));

         return oRet.bSuccess;
       }
      },
      /* ss_gc_ras_dialrasentry.htm functions - END */
      /* ss_gc_ras_opendialer.htm functions - START */
      ss_gc_ras_691_OpenDialer1 : {
        
        AfterRender: function()
        {
          try 
          {
            if(_databag.GetValue("ss_gc_db_AlreadyOpenedDial") == "true")
            { 
              this.UpdateUI("opened");      
            }
            else
            {
              var prevLogin1 = "";
              prevLogin1 = _registry.GetRegValue("HKLM","Software\\support.com\\autoprov","userid");
              if (prevLogin1 == "undefined" || prevLogin1 == "") 
              {
                prevLogin1 = _registry.GetRegValue("HKCU","Software\\Alcatel\\SpeedTouchUSB\\STDialup\\BelgacomADSL","User");
              }
          
              if (prevLogin1 == "undefined" || prevLogin1 == "") 
              {
                this.UpdateUI("noprevlogin"); 
              }
              else 
              { 
                this.UpdateUI("prevlogin", prevLogin1); 
              }
            }               
          }
          catch(e)
          {
            _logger.error("AfterRender()", e.message);
          }
        },      
        UpdateUI: function(state, data){  
  
          switch(state)
          {
            case "opened":
              $("#ss_gc_dun_odl_getcredentials").css("display","block");
              $("#question").css("display", "none");        
              break;
            case "noprevlogin":
              $("#lblPrevLogin").css("display", "none");
              break;
            case "prevlogin":       
	            $("#ss_gc_dun_odl_previouslogin").css("display", "block");
              $("#ss_gc_dun_odl_spacer").css("display", "block");
              $("#lblPrevLogin").html("<b>" + data + "</b>"); 
              break;
          }
        },  

        NavNext: function()
        { 
          if( $("#Know").attr("checked"))
          {
            _databag.SetValue("ss_gc_db_KnowsPassword", "true");
          }
          else
          {
            _databag.SetValue("ss_gc_db_KnowsPassword", "false");
          }
          
          return true;
        }
      },
      /* ss_gc_ras_opendialer.htm functions - END */
      
      /* ss_gc_ras_reboot.htm functions - START */
      ss_gc_cmr_Reboot1 : {
        NavNext: function(params) {				
          try{
            var sessionid = params.element.stepSessionId;
            _uiutils.RestartWithSave(_gcSnapinHomeURL + "?sl_resume=true&sl_session=" + sessionid, true);            
          }
          catch(ex)
          {
            _logger.error("Reboot Machine failed!", ex.message);
          }
          return false; //preventing the app to navigate to the next page while rebooting is in progress
        }
      }, 
      
      ss_gc_cmr_Reboot2 : {
        NavNext: function(params)
        {  
          return _gcHelper["ss_gc_cmr_Reboot1"].NavNext(params);   
        }
      },
      
      ss_gc_gsr_Reboot : {
        NavNext: function(params)
        {    
          return _gcHelper["ss_gc_cmr_Reboot1"].NavNext(params);   
        }
      },
      
      ss_gc_ras_619_Reboot : {
        NavNext: function(params)
        {    
          return _gcHelper["ss_gc_cmr_Reboot1"].NavNext(params);      
        }
      },
      
      ss_gc_ras_721_Reboot : {
        NavNext: function(params)
        {    
          return _gcHelper["ss_gc_cmr_Reboot1"].NavNext(params);   
        }
      },
      /* ss_gc_ras_reboot.htm functions - END */
  
      /* ss_gc_ras_showrasentries.htm functions - START */
      ssgcrdci_ss_gc_ras_ShowRasEntries : {
        BeforeRender: function()
        {
          try
          {
            var deviceName = _gcUtils.GetModemDevice();   
            var entryList = _netcheck.GetRasEntries(null, deviceName);
            var prevEntry = _databag.GetValue("ss_gc_db_SelectedRASConnection");
            var locals= {
                      deviceName:  deviceName,
                      entryList: entryList,
                      prevEntry: prevEntry
                }
            return locals;
          }       
          catch(ex)
          {
            _logger.error("BeforeRender()", ex.message);
          }       
        },
        
        NavNext: function()
        {
          _databag.SetValue("ss_gc_db_SelectedRASConnection", $("#ss_gc_rsre_selectras_rasentry").val());
        }
      },
      
      ss_gc_ras_691_ShowRasEntries : {
        BeforeRender: function()
        {
          return _gcHelper["ssgcrdci_ss_gc_ras_ShowRasEntries"].BeforeRender(); 
        },
        NavNext: function()
        {
          return _gcHelper["ssgcrdci_ss_gc_ras_ShowRasEntries"].NavNext(); 
        }
      }
      /* ss_gc_ras_showrasentries.htm functions - END */
    });
    var _config = $ss.agentcore.dal.config;
    var _databag = $ss.agentcore.dal.databag;
    var _system = $ss.agentcore.utils.system;
    var _constants = $ss.agentcore.constants;
    var _sdcDSLModem = $ss.agentcore.smapi.methods;
    var _gcUtils = $ss.snapin.getconnect.utils;
    var _utils = $ss.agentcore.utils;                     
    var _databag = $ss.agentcore.dal.databag;    
    var _netcheck = $ss.agentcore.network.netcheck;
    var _registry = $ss.agentcore.dal.registry;  
    var _logger = $ss.agentcore.log.GetDefaultLogger("$ss.snapin.getconnect.renderHelper");  
    var _gcHelper = $ss.snapin.getconnect.renderHelper;
    var _layout = $ss.snapin.getconnect.layoutHelper; 
    var _uiutils = $ss.agentcore.utils.ui;
    var _gcSnapinHomeURL = _config.GetConfigValue("preconfigured_snapin_url", "subagent_getconnect", "sa/connect/getconnect");
})();

