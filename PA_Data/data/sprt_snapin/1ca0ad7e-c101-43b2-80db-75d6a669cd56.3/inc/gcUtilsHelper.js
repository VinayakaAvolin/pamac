$ss.snapin = $ss.snapin || {}; 
$ss.snapin.getconnect = $ss.snapin.getconnect || {};
$ss.snapin.getconnect.utils = $ss.snapin.getconnect.utils || {};
(function()
{
 $.extend($ss.agentcore.constants,{ 
    SS_CUI_RESUMEMODE_AUTO : "auto",
    SS_CUI_RESUMEMODE : "ResumeMode"
 });
})();
(function()
{
 $.extend($ss.snapin.getconnect.utils,
  {   
    /*                       Heal functions                         */
    GetProbe : function () 
    {
      if(_probe == null) {
        try {
          _probe = new $ss.agentcore.dna.protectrestore.Probe;       
        }catch(e) { return null; }
      }
      return _probe;
    },

    GetWirelessGuid : function () 
    {
      return _config.GetConfigValue("protections","wzcsvc_settings_protection_guid", "");
    },
    GetNetworkGuid : function ()
    {
      return _config.GetConfigValue("protections","network_settings_protection_guid", "");
    },
    GetFirefoxGuid : function () 
    {
      return _config.GetConfigValue("protections","firefox_settings_protection_guid", "");
    },
    GetIEGuid : function () 
    {
      return _config.GetConfigValue("protections","browser_settings_protection_guid", "");
    },
    GetDunGuid : function () 
    {
      return _config.GetConfigValue("protections","dun_settings_protection_guid", "");
    },
    GetDunDriversGuid : function () 
    {
      return this.GetModemInstanceValue("protection_guid","");     
    },

    HealWireless : function () 
    {
      try {
        var oMyProbe = this.GetProbe();
        var sGuid = this.GetWirelessGuid();
        if(oMyProbe == null || sGuid == "") return false;
        
        _objService.StartWin32Service(_system.IsWinVST(true) ? gutil_WLANSVCNAME : gutil_WZCSVCNAME,false);  // stop wzcsvc 
        oMyProbe.guids[sGuid].Heal();
        _objService.StartWin32Service(_system.IsWinVST(true) ? gutil_WLANSVCNAME : gutil_WZCSVCNAME,true);   // start wzcsvc 

        _objUtil.Sleep(10000);
        _objUtil.ReleaseIpConfig();
        _objUtil.RenewIpConfig();

        return true;
      } catch(e) {
        return false;
      }
    },

    HealNetwork : function () 
    {
      return this.HealProtection(this.GetNetworkGuid());
    },
    
    HealFirefox: function() 
    {
      return this.HealProtection(this.GetFirefoxGuid());
    },
    
    HealIE : function () {
      return this.HealProtection(this.GetIEGuid());
    },
    
    HealDun : function () 
    {
      return this.HealProtection(this.GetDunGuid());
    },
    
    HealDunDrivers : function () 
    {
      return this.HealProtection(this.GetDunDriversGuid());
    },
    
    /*                       End Heal functions                     */
    /*--------------------------------------------------------------*/

    CheckAllAdapters : function () 
    {
      if(_oCheckAllAdapters == null) {
        _oCheckAllAdapters = _databag.GetValue("ss_gc_db_CheckAllAdapters");    
      }
      return _oCheckAllAdapters;
    },

    GetNICListEx : function () 
    {
      var oNics = this.GetNICList();
      if (this.CheckAllAdapters())
      {
        // Some RAS modems do not get categorized as "NetworkCards" under 9x and Vista,
        // so we need to manually add them to the list
        var oRasModems = this.GetModemDeviceGuid();                             
        for (var oDev in oRasModems)
        {
          oNics[oDev] = oRasModems[oDev];
        }
      }
      return oNics;
    },


    //For SubAgent we check/set all adapters, SmartAccess check/sets a specific adapter.
    //So if checkAllAdapters set, get 'em all, otherwise set to selected, if that
    //is null than return null.
    GetNICList : function () 
    {
      if(_oAdapterList == null) {
        if(this.CheckAllAdapters()) {
          //                                       oNet, filterVirtual, filterBridge, filterDisabled,  filterWireless
          _oAdapterList = _objnetCheck.GetPresentAdapters(null, true,          true,         false,           false);    
        } else {
          var nic = _databag.GetValue("ss_gc_db_SelectedAdapter");   
          if (nic == null || nic == "") return null;
          _oAdapterList = {};
          _oAdapterList[nic]=nic;
        }
      }
      return _oAdapterList;
    },

    GetMissingNICs : function ()
    {
      var allNics = _objnetCheck.GetEthernetCards(null);                    
      var presentNics = _objnetCheck.GetPresentAdapters(null, true, true, false, false); 
      var missingNics = [];

      for (var nic in allNics)
      {
        if (!presentNics[nic] &&
            !_objnetCheck.IsBridgeAdapter(nic) && !_objnetCheck.IsAdapterInBridge(nic) &&
            _objnetCheck.ValidNIC(nic)) 
        {
          missingNics.push(allNics[nic]);
        }
      }
      return missingNics;
    },

    /*
       sdcDSLModem.GetInstance() will autodect and default to null_modem if none found.
    */
    IsNullModem : function () 
    {
      var sModemCode = _objSI.ReadPersistInfo(_oSIConst.MODEM_CLASS, _oSIConst.MODEM_CODE);     
      sModemCode = sModemCode.toLowerCase();
      if(sModemCode.substr(0,10)=="null_modem") {
        return true;
      } else {
        return false;
      }
    },

    IsLastModemRas : function ()
    {
      var modemClass = "";
      if (this.IsNullModem()) // if current modem code is set to null_modem, then look up from last detected value
      {
        modemClass = _config.GetConfigValue(_const.SMAPI_MODEM, _const.SMAPI_DB_LASTDETECTED_MODEMCLASS, "");
        _logger.debug("IsLastModemRas()", "Last Detected Modem Class: " + modemClass);
      }
      else                     // if current modem code is available, then look up the class from modem xml
      {
        modemClass = this.GetModemInstanceValue(_const.SMAPI_XMLNODE_CLASS);   
        _logger.debug("IsLastModemRas()", "Current Modem Class: " + modemClass);
      }

      var bRet = (modemClass == "$ss.snapin.modem.ras.wrapper");
      _logger.debug("IsLastModemRas()", "Returns: " + bRet);

      return bRet;
    },

    IsModemRas : function ()
    {
      var _oModem = null;
      var bRet = false;
      var modemClass = "";
      
      var modemCode = _objSI.ReadPersistInfo(_oSIConst.MODEM_CLASS, _oSIConst.MODEM_CODE);   
      
      if (modemCode != "")
      {
       _oModem = _sdcDSLModem.GetInstanceByCode(modemCode);   
         
       modemClass = _oModem.GetInstanceValue(_const.SMAPI_XMLNODE_CLASS);  
      }
        
      _logger.debug("IsModemRas()", "Current Modem Class: " + modemClass);

      bRet = (modemClass == "$ss.snapin.modem.ras.wrapper");
      _logger.debug("IsModemRas()", "Returns: " + bRet);
      return bRet;
    },

    //------------------------------------------------------------------------------
    
    DetectModem: function(sDivPrefix)
    {
      var _oModem = null;
      var bRet = false;
      var createNewModemInstance = false;  // initialise the var - not to create a new instance of the modem
      //check config to see if autodetect is turned on. autodetect is turned off when nAD == 0
      var nAD = Number(_config.GetConfigValue("modems", "autodetect", "0"));

      if ((_databag.GetValue("ss_gc_db_ModemDetected") != "no_auth") && (nAD != 0)){   
        //_objSI.DeletePersistInfo(_oSIConst.MODEM_CLASS,_oSIConst.MODEM_CODE);   
        createNewModemInstance = true;  // we have identified that autodetect is ON. So create a new instance of the modem.
        _smapiUtils.SetModemCodeToEmpty();
      }

      _logger.info("DetectModem");

      // show "Testing connection to modem..."
      $("#" + sDivPrefix + "testing").css("display", "block"); 

      if(!_oModem) _oModem = _sdcDSLModem.GetInstance(createNewModemInstance); 

      try
      {
        _logger.debug("DetectModem()", "About to Detect");
        _oModem.Detect();
        if((_oModem.GetLastError() == _const.SMAPI_SUCCESS) &&
          (_oModem.mObjResponse["NewStatus"] == 1)) {
          bRet = true;
        } else if(_oModem.GetLastError() == _const.SMAPI_NO_AUTH ||
                  _oModem.GetLastError() == _const.SMAPI_DO_REBOOT) {
          bRet = false;
        }
      } catch (ex) {
        _logger.error("DetectModem()", ex.message);
      }
      return bRet;
    },
      

    //------------------------------------------------------------------------------
    TestSync : function ()
    {
      var bRet = false;
      var _oModem = null;

      if(!_oModem) _oModem = _sdcDSLModem.GetInstance();


      try {
        _logger.debug("TestSync()");
          _oModem.IsSync();
        if((_oModem.GetLastError() == _const.SMAPI_SUCCESS) &&
          (_oModem.mObjResponse["NewStatus"] == 1)) {
          bRet = true;
          
        } else if(_oModem.GetLastError() == _const.SMAPI_NO_AUTH ||
                  _oModem.GetLastError() == _const.SMAPI_DO_REBOOT) {
            bRet = false;
        }
          
      } catch (ex) {
        _logger.error("TestSync()", ex.message);
      }
      return bRet;
    },

    //------------------------------------------------------------------------------
    DoReboot : function ()
    {
      var bRet = false;
      var bDetected = false;
      var _oModem = null;

      try
      {
        if(!_oModem) _oModem = _sdcDSLModem.GetInstance();
        _logger.info("DoReboot()");

        // Skip null modems, can't reboot.
        if (_oModem.GetLastError() == _const.SMAPI_SUCCESS && (!this.IsNullModem())) 
        {
          // Don't bother rebooting if we can't detect modem.
          if (this.Detect())
          {
            _logger.debug("DoReboot()", "Doing Reboot");
            _oModem.Reboot();
            if(_oModem.GetLastError() == _const.SMAPI_SUCCESS) {
              bRet = true;
            }
            else
              bRet = false;
          } // pvtDetect
        } // check for null modem
      } catch (ex) {
        _logger.error("DoReboot()", ex.message);
      }
      return bRet;
    },
   
    DetectCurrentBrowser : function ()
    {
      var sBrowser = _databag.GetValue("ss_gc_db_Browser");
      if (sBrowser == null || typeof(sBrowser) == undefined || sBrowser == "" || sBrowser == INET_BROWSER_DEFAULT)
      {
        sBrowser = _inet.GetDefaultBrowser();  
      }
      return sBrowser;
    },

    //-----------------------------------------------------------------------
    TestConnect : function ()
    {
    
      var sBrowser = this.DetectCurrentBrowser();
      var bTest = _inet.BasicConnectionTest(false, true, sBrowser);    
      

      // DB: Many modem changes do not take affect immediately in some cases
      // hence we need a loop of test connect before deciding a fail/success
      if(!this.IsNullModem())
      {
        try {
          _logger.info("TestConnect()");
          
          bTest = _inet.BasicConnectionTest(false, true, sBrowser);    
          
        } catch (ex) {
          _logger.error("TestConnect()", ex.message);
        }
      }
      return bTest;
    },

    //------------------------------------------------------------------------------

    SetUserCreds : function (sDivPrefix)
    {
      var _oModem = null;
      var bSetCreds = false;  //tracks failure in setting the creds
      var bDetected = false;  //tracks failure in detecting modem after successfull creds update
      
      if(!_oModem) _oModem = _sdcDSLModem.GetInstance();

      try {
        _logger.info("SetUserCreds()");

        // Get unique username /password to be set
        var sName     = _databag.GetValue("GC_SMAPI_DB_USERNAME"); 
        var sPassword = _databag.GetValue("GC_SMAPI_DB_USERPASS"); 

        _oModem.SetUserCreds(sName, sPassword);
        bSetCreds = (_oModem.GetLastError() == _const.SMAPI_SUCCESS);

        if(bSetCreds)
        {
          _logger.debug("SetUserCreds()", "in while loop");
          // There could be a possible reboot in modem specific
          // implementations because of which we detect again
          bDetected = this.Detect();
        }
      } catch (ex) {
        _logger.error("SetUserCreds()", ex.message);
      }
      return (bSetCreds && bDetected);
    },

    //------------------------------------------------------------------------------
    OnNoAuth : function (sDivPref)
    {
      if ($("#" + sDivPref + "Content") != null) {  
        $("#" + sDivPref + "Content").css("display", "none");
      }
      $("#" + sDivPref + "admincreds").css("display", "block");
    },


    //------------------------------------------------------------------------------
    IsNICBoundToTCPIP : function (sNIC, sTCPIPVersion)
    {
      // if version set to "all", return true only if both TCPIPv4 and TCPIPv6 bound
      // otherwise, check binding of corresponding version of TCPIP
      // default to TCPIPv4

      if (sTCPIPVersion == "all")
      {
        return (_objnetCheck.IsComponentBoundToAdapter(null, sNIC, "ms_tcpip6", 3) &&
               _objnetCheck.IsTCPIPboundToAdapter(null, sNIC));
      }
      else if (sTCPIPVersion == "6")
      {
        return (_objnetCheck.IsComponentBoundToAdapter(null, sNIC, "ms_tcpip6", 3));
      }
      else
      {
        return (_objnetCheck.IsTCPIPboundToAdapter(null, sNIC));
      }
    },

    //------------------------------------------------------------------------------
    GetModemInstanceValue : function (valName, defValue)
    {
      try{
        var modemValue = "";
        var _oModem = null;
        if(!_oModem) _oModem = _sdcDSLModem.GetInstance();
        if (_oModem.GetLastError() == _const.SMAPI_SUCCESS)
        {
          modemValue = _oModem.GetInstanceValue(valName, defValue);
        }
      }
      catch(e){
      
      }
      return modemValue;
    },

    //------------------------------------------------------------------------------
    GetModemRasEntries : function ()
    {
      var modemRasEntries = [];
      var _oModem = null;
      if(!_oModem) _oModem = _sdcDSLModem.GetInstance();
      if (_oModem.GetLastError() == _const.SMAPI_SUCCESS)
      {
        modemRasEntries = _oModem.GetInstanceArray("rasentry_name", _const.SMAPI_EMPTY_STR);
      }
      return modemRasEntries;
    },

    //------------------------------------------------------------------------------
    GetModemDefaultRasEntry : function ()
    {
      var modemDefaultRasEntry = "";
      var _oModem = null;

      if(!_oModem) _oModem = _sdcDSLModem.GetInstance();
      if (_oModem.GetLastError() == _const.SMAPI_SUCCESS)
      {
        var expr = "rasentry_name/item[@default=\"true\"]";
        modemDefaultRasEntry = _oModem.GetInstanceValue(expr, _const.SMAPI_EMPTY_STR);
      }
      return modemDefaultRasEntry;
    },

    //------------------------------------------------------------------------------
    GetModemDevice : function ()
    {
      var sModemDevice = "";
      var _oModem = null;

      if (_objUtil.GetOSGroup() != "9x")
      { 
        if(!_oModem) _oModem = _sdcDSLModem.GetInstance();
        if (_oModem.GetLastError() == _const.SMAPI_SUCCESS)
        {
          // enumerate modem_device list specified in modem xml and return the one that's
          // currently present. If none of them is present, return empty string
          var arValues = _oModem.GetInstanceArray("modem_device", _const.SMAPI_EMPTY_STR);
          sModemDevice = _objnetCheck.FindIfDevicePresent(null, arValues);
        }
      }
      _logger.debug("GetModemDevice", sModemDevice);
      return sModemDevice;
    },

    //------------------------------------------------------------------------------
    GetModemDeviceGuid : function ()
    {
      var oFoundDevices = {};
      var _oModem = null;

      try
      {
        if (!_oModem) _oModem = _sdcDSLModem.GetInstance();
        if (_oModem.GetLastError() == _const.SMAPI_SUCCESS)
        {
          var arValues = _oModem.GetInstanceArray("modem_device", _const.SMAPI_EMPTY_STR);      
          oFoundDevices = _objnetCheck.GetPresentNetDeviceList(null, arValues);
        }
        _logger.debug("GetModemDeviceGuid", _strutils.ConvertJSObjectToString(oFoundDevices));
      }
      catch (ex)
      {
        _logger.debug("GetModemDeviceGuid", ex.message);
      }
      return oFoundDevices;  
    },

    //------------------------------------------------------------------------------
    HaveRasEntry : function (rasEntry)
    {
      var bRet = false;
      var deviceName = this.GetModemDevice();
      var arrRasEntries = _objnetCheck.GetRasEntries(null, deviceName);

      var bFilter = ((rasEntry != null) && (rasEntry != "") && typeof(rasEntry) != "undefined");

      if (bFilter)
      {
        for (var i = 0; i < arrRasEntries.length; i++)
        {
          if (rasEntry.toLowerCase() == arrRasEntries[i].toLowerCase())
          {
            bRet = true;
            break;
          }
        }
      }
      else
      {
        bRet = arrRasEntries.length > 0;
      }

      return bRet;
    },

    //------------------------------------------------------------------------------
   HaveActiveRasConnection : function ()
    {
      var bIsConnected = false;
      var _oModem = null;

      if(!_oModem) _oModem = _sdcDSLModem.GetInstance();
      if (_oModem.GetLastError() == _const.SMAPI_SUCCESS)
      {
        _oModem.IsConnected();
        bIsConnected = ((_oModem.GetLastError() == _const.SMAPI_SUCCESS) && (_oModem.mObjResponse.NewStatus == 1));
      }
      return bIsConnected;
    },

    //--------------------------------------------------------------------------------
    // This returns the modem type to show for the diagram (westell 6100, etc)
    GetModemCodeForDiagram : function ()
    {
      var retCode;
      try {
        if (this.IsNullModem()) // if current modem code is set to null_modem, then look up from last detected value
        {
          retCode = _config.GetConfigValue(_const.SMAPI_MODEM, _const.SMAPI_DB_LASTDETECTED_MODEMCODE, "");
        }
        else                     // if current modem code is available, then look up the class from modem xml
        {
          retCode = _objSI.ReadPersistInfo(_oSIConst.MODEM_CLASS, _oSIConst.MODEM_CODE);   
        }
      }
      catch(ex){
      }
      if (!retCode) retCode = "common";

      return retCode;
    },

    //--------------------------------------------------------------------------------
    // This returns the connection type to show for the diagram (usb, ethernet, etc)
    GetModemConnectionForDiagram : function ()
    {
      var retConn;
      try {
        if (this.IsNullModem()) // if current modem code is set to null_modem, then look up from last detected value
        {
           retConn = _config.GetConfigValue(_const.SMAPI_MODEM, _const.SMAPI_DB_LASTDETECTED_MODEMCONNTYPE, "");   
        }
        else                     // if current modem code is available, then look up the class from modem xml
        {
          retConn = _objSI.ReadPersistInfo(_oSIConst.MODEM_CLASS, _oSIConst.CONN_TYPE);  
        }
      }
      catch(ex){
      }
      if (!retConn) retConn = "ethernet";

      if((retConn != "usb") && (retConn != "ethernet") && (retConn != "wireless"))
      {
       retConn = "ethernet";
      }

      return retConn;
    },

    //--------------------------------------------------------------------------------
    // This will create a div for a modem specific diagram as well as a div for 
    // a common connection diagram, using the div Prefix specified.
    // Put this in onBeforeLangLoad() to create the Divs prior to resource translation
    // Use this in conjunction with ShowModemSpecificDiagram.
    //
    // Parameters: divPrefix - Prefix to use when creating the connection diagram divs
    CreateModemSpecificDiagram : function (divPrefix)
    {
      var modemCode = this.GetModemCodeForDiagram();
      var connType  = this.GetModemConnectionForDiagram();

      //make the agreed id based on the values collected
      var str  = ""; // '<div id="' + divPrefix + modemCode + '_' + connType + '"><\/div>';
      //str     += '<div id="' + divPrefix + 'common'  + '_' + connType + '"><\/div>';
      
      var modemSpecificImg = divPrefix + modemCode + '_' + connType;
      var modemCommonImg = divPrefix + 'common_' + connType;
      
      var idTouse = "";
      
      if (modemSpecificImg == modemCommonImg){
        idTouse = modemCommonImg;
				modemCommonImg = $('#' + modemCommonImg);
				modemCommonImg = modemCommonImg.html();
				str = modemCommonImg;
				 
      } else {
				idTouse = modemSpecificImg;
				modemSpecificImg = $('#' + modemSpecificImg);
				modemSpecificImg = modemSpecificImg.html();      
				str =  modemSpecificImg;
      }
             
      var sModemDiagDiv = $('#' + divPrefix + 'Figure');
      if(sModemDiagDiv != null)
      {
        sModemDiagDiv.append(str);            
         
      } 
      return idTouse;
    },
    HealProtection: function(sGuid) 
    {
      try {
        var oMyProbe = this.GetProbe();
        if(oMyProbe == null || sGuid == "") return false;

        oMyProbe.guids[sGuid].Heal();

        return true;
      } catch(e) {
        return false;
      }
    },


    /*
     * Used in conjunction with ss_gc_ConfigureForWin9xManualReboot() to remove any
     * reboot configuration that was added.
     */
    OnPageChange: function()
    {
      // This code is similar to the code in ss_shi_RunOnce_Set() that sets the Run key
      // in the registry.
      var sValue = _config.GetConfigValue("global", "product", "") + " AutoStart";
      _objReg.DeleteRegVal("HKLM", "Software\\Microsoft\\Windows\\CurrentVersion\\Run\\", sValue);
    },

    //------------------------------------------------------------------------------
    Detect: function()
    {
      _logger.info("_Detect()");
      var _oModem = null;
      if(!_oModem) _oModem = _sdcDSLModem.GetInstance();
      _oModem.Detect();
      if((_oModem.GetLastError() == _const.SMAPI_SUCCESS) &&
         (_oModem.mObjResponse["NewStatus"] == 1)) {
        return true;
      }
      return false;
    }

  });
  
    var _objUtil = $ss.agentcore.utils;
    var _objService = $ss.agentcore.dal.service;
    var _strutils = $ss.agentcore.utils.string;
    var _logger = $ss.agentcore.log.GetDefaultLogger("$ss.snapin.getconnect.utils");  
    var _config = $ss.agentcore.dal.config;
    var _databag = $ss.agentcore.dal.databag;
    var _objReg = $ss.agentcore.dal.registry;
    var _history = $ss.agentcore.dal.history;
    var _file = $ss.agentcore.dal.file;
    var _objnetCheck = $ss.agentcore.network.netcheck;
    var _system = $ss.agentcore.utils.system;
    var _events = $ss.agentcore.events;
    var _inet = $ss.agentcore.network.inet;
    var _objSI = $ss.agentcore.diagnostics.smartissue;
    var _oSIConst = $ss.agentcore.constants.smartissue;
    var _const = $ss.agentcore.constants;
    var _sdcDSLModem = $ss.agentcore.smapi.methods;
    var _oCheckAllAdapters  = null;
    var _oAdapterList = null;
    var _probe;
    var _smapiUtils = $ss.agentcore.smapi.utils;
    var gutil_WZCSVCNAME  = "wzcsvc";
    var gutil_WLANSVCNAME = "wlansvc";
    
   /*******************************************************************************
    **    Constants
    *******************************************************************************/
    var _bInTimer = false;
    
    var _TIMER_DETECT = "DETECT";
    var _TIMER_SYNC = "SYNC";
    var _TIMER_REBOOT = "REBOOT";
    var _TIMER_PING_INT = "PING_INT";
    var _TIMER_TEST_CONNECT = "TEST_CONNECT";
    var INET_BROWSER_DEFAULT = "Default";
    /*--------------------------------------------------------------*/
    ////////// Private functions /////////////////////////////////////////////////
    
    
 
 })();