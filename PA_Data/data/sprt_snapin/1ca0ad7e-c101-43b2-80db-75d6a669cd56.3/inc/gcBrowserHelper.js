/**
 * @author Vishnu Ratheesh & Santhi Dhanuskodi
 */
$ss.snapin = $ss.snapin ||{}; 
$ss.snapin.getconnect = $ss.snapin.getconnect || {};
$ss.snapin.getconnect.renderHelper = $ss.snapin.getconnect.renderHelper || {};

(function(){
	$.extend($ss.snapin.getconnect.renderHelper,{
    /* ss_gc_heal_ie_prompt.htm functions - START */

    sgC_ss_gc_IEHealPrompt : {
      BeforeRender: function() {
				if (!_databag.GetValue('ss_gc_db_restore_ie')) 
			    return {restore : false};		
		    else
          return {restore : true};
      },
      NavNext: function() {
				if( $('#Restore').attr("checked")) {
					_databag.SetValue("ss_gc_db_restore_ie", true);
				} else {
					_databag.SetValue("ss_gc_db_restore_ie", false);
				}
				return true;
      }
    },

    ss_gc_cwr_IEHealPrompt : {
      BeforeRender: function(){
        return _gcHelper["sgC_ss_gc_IEHealPrompt"].BeforeRender();
      },
      NavNext: function(){
        return _gcHelper["sgC_ss_gc_IEHealPrompt"].NavNext();
      }
    },
    
    ssgcConn_ss_gc_IEHealPrompt : {
      BeforeRender: function(){
        return _gcHelper["sgC_ss_gc_IEHealPrompt"].BeforeRender();
      },
      NavNext: function(){
        return _gcHelper["sgC_ss_gc_IEHealPrompt"].NavNext();
      }
    },
    /* ss_gc_heal_ie_prompt.htm functions - START */
    /* ss_gc_heal_ie.htm functions - START */
    sgC_ss_gc_IEHeal : {
      
      BeforeRender: function() {
        _layout.DisableLayoutBtns();
      },

			AfterRender: function(){
        try {
        _uiutils.StartShellBusy();
				_system.Sleep(0); // let UI refresh itself to draw page
				var dbResult = "false";
				//Use include at snapin/host level where all proper .js files are included
				if(_gcutils.HealIE()) {
					dbResult = "true";
				}
				_databag.SetValue("ss_gc_db_SolutionOk", dbResult);
        _uiutils.EndShellBusy();
				// do auto-next
        return {move:"forward"};				
        }  catch(ex) {_logger.error("sgC_ss_gc_IEHeal:AfterRender",ex.message); }
        finally {_uiutils.EndShellBusy();}
      }
    },
    
    ss_gc_cwr_IEHeal : {
      AfterRender: function(){
        return _gcHelper["sgC_ss_gc_IEHeal"].AfterRender();
      },
      BeforeRender: function(){
        return _gcHelper["sgC_ss_gc_IEHeal"].BeforeRender();
      }
    },
    
    
    ssgcConn_ss_gc_IEHeal : {
      AfterRender: function(){
        return _gcHelper["sgC_ss_gc_IEHeal"].AfterRender();
      },
      BeforeRender: function(){
        return _gcHelper["sgC_ss_gc_IEHeal"].BeforeRender();
      }
    },
    /* ss_gc_heal_ie.htm functions - END */
    /* ss_gc_heal_firefox_prompt.htm functions - START */
    
    ss_gc_cwr_FirefoxHealPrompt : {
			BeforeRender: function(){
		    if (!_databag.GetValue('ss_gc_db_restore_firefox')) 
			    return {restore : false};		
		    else
          return {restore : true};
			},
			NavNext: function() {
				if( $('#Restore').attr("checked")) {
					_databag.SetValue("ss_gc_db_restore_firefox", true);
				} else {
					_databag.SetValue("ss_gc_db_restore_firefox", false);
				}
				return true;
			}				
    },
    
    sgC_ss_gc_FirefoxHealPrompt : {
      BeforeRender: function(){
        return _gcHelper["ss_gc_cwr_FirefoxHealPrompt"].BeforeRender();
      },
			NavNext: function() {
        return _gcHelper["ss_gc_cwr_FirefoxHealPrompt"].NavNext();
      }
    },
    /* ss_gc_heal_firefox_prompt.htm functions - END */
    /* ss_gc_heal_firefox.htm functions - START */
    ss_gc_cwr_FirefoxHeal : {      
      
      BeforeRender: function() {
        _layout.DisableLayoutBtns();
      },

			AfterRender: function(){
        try {
        _uiutils.StartShellBusy();
				_system.Sleep(0); // let UI refresh itself to draw page
				var dbResult = "false";

				$("#ss_gc_hff_CloseFirefox").css("display", "inline");					

				_system.Sleep(500); // let FF completely shutdown

				//Use include at snapin/host level where all proper .js files are included
				if(_gcutils.HealFirefox()) {
					dbResult = "true";
				}
				_databag.SetValue("ss_gc_db_SolutionOk", dbResult);
        _uiutils.EndShellBusy();
				// do auto-next
				return {move:"forward"};
        }  catch(ex) {_logger.error("ss_gc_cwr_FirefoxHeal:AfterRender",ex.message); }
        finally {_uiutils.EndShellBusy();}
			}
    },
    
    sgC_ss_gc_FirefoxHeal : {
      AfterRender: function()
      {
        return _gcHelper["ss_gc_cwr_FirefoxHeal"].AfterRender();
      },
      BeforeRender: function()
      {
        return _gcHelper["ss_gc_cwr_FirefoxHeal"].BeforeRender();
      }
    },
    /* ss_gc_cwr_FirefoxHeal.htm functions - END */
    /* ss_gc_disable_ie_offline.htm functions - START */
    sgC_ss_gc_disable_ie_offline : {
      NavNext: function()
      {
        _inet.DisableIEOffline();
        _databag.SetValue("ss_gc_db_SolutionOk", true);
        return true;
      }
    },
    
    ss_gc_cwr_disable_ie_offline : {
      NavNext: function()
      {
        return _gcHelper["sgC_ss_gc_disable_ie_offline"].NavNext();
      }
    },
    
    ssgcConn_ss_gc_disable_ie_offline : {
      NavNext: function()
      {
        return _gcHelper["sgC_ss_gc_disable_ie_offline"].NavNext();
      }
    },
    sgr_ss_gc_disable_offline : {
      NavNext: function()
      {
        // To kill firefox process
        if (_utils.IsProcessRunning("firefox.exe")) {
          alert(_utils.LocalXLate("snapin_getconnect", "ss_gc_firefox_close"));
          var sCmdPath = '"' + _config.GetProviderBinPath() + 'sdckillw.exe" ';
          sCmdPath = sCmdPath + " /name firefox.exe";
          try {
            _system.RunCommand(sCmdPath, _const.BCONT_RUNCMD_HIDDEN);  // run in hidden mode
          } catch (e) {
            _logger.error("sgr_ss_gc_disable_offline:NavNext", e.message); 
          }
        }
        _inet.DisableFirefoxOffline();
        _inet.DisableIEOffline();
        _databag.SetValue("ss_gc_db_SolutionOk", true);
        return true;
      }
    },
    /* ss_gc_disable_ie_offline.htm functions - END */
    /* ss_gc_disableproxy.htm functions - START */    
    sgC_ss_gc_disableproxy : {
      NavNext: function()
      {
          _inet.DisableProxy();
          _databag.SetValue("ss_gc_db_SolutionOk", true);
          return true;
      }
    },
    
    ss_gc_cwr_disableproxy : {
      NavNext: function()
      {
        return _gcHelper["sgC_ss_gc_disableproxy"].NavNext();
      }
    },
    
    ssgcConn_ss_gc_disableproxy : {
      NavNext: function()
      {
        return _gcHelper["sgC_ss_gc_disableproxy"].NavNext();
      }
    },
    sgr_ss_gc_disableproxy : {
      NavNext: function()
      { 
        // To kill firefox process
	      var sCmdPath = '"' + _config.GetProviderBinPath() + 'sdckillw.exe" ';
        sCmdPath = sCmdPath + " /name firefox.exe";                
        _system.RunCommand(sCmdPath, _const.BCONT_RUNCMD_HIDDEN);  // run in hidden mode
        _decisions.SaveProxySettings();
        _inet.DisableFirefoxProxy();  
        _inet.DisableProxy();        
        _databag.SetValue("ss_gc_db_SolutionOk", true);
        return true;
      }
    },
    ssgcConn_ss_gc_disablefirefoxproxy : {
      NavNext: function()
      {
        _system.Sleep(500); // let FF completely shutdown
        
        // To kill firefox process
	var sCmdPath = '"' + _config.GetProviderBinPath() + 'sdckillw.exe" ';
        sCmdPath = sCmdPath + " /name firefox.exe";                
        _system.RunCommand(sCmdPath, _const.BCONT_RUNCMD_HIDDEN);  // run in hidden mode

        _inet.DisableFirefoxProxy();
        _databag.SetValue("ss_gc_db_SolutionOk", true);
        return true;
      }
    },   
    ss_gc_cwr_disablefirefoxproxy : {
      NavNext: function()
      {
        return _gcHelper["ssgcConn_ss_gc_disablefirefoxproxy"].NavNext();
      }
    },
    /* ss_gc_disableproxy.htm functions - END */
    /* ss_gc_dnsspoof.htm functions - START */
    ssgcConn_ss_gc_dnsspoof : {
      OpenHomeRunURL: function()
      {
        var sHost         = _config.GetConfigValue("getconnect", "HomeRunHostname", "www.supportsoft.com");
        var sUrlTemplate  = _config.GetConfigValue("getconnect", "HomeRunCheckURLMacro", "http://%NetCheck_HomeRunHostname%");
        var sHomeRunURL   = sUrlTemplate.replace(/\%NetCheck\_HomeRunHostname\%/, sHost);
        _ui.OpenWindow(sHomeRunURL, "_new", "");
        _databag.SetValue("ss_gc_db_SolutionOk", true);
      }
    }
     /* ss_gc_dnsspoof.htm functions - END */
  });
	var _gcutils = $ss.snapin.getconnect.utils;
  var _decisions = $ss.snapin.getconnect.decisions;
	var _system = $ss.agentcore.utils.system;
	var _ui = $ss.agentcore.utils.ui;
	var _config = $ss.agentcore.dal.config;                  
	var _databag = $ss.agentcore.dal.databag;    
	var _inet = $ss.agentcore.network.inet;
	var _logger = $ss.agentcore.log.GetDefaultLogger("$ss.snapin.getconnect.renderHelper");   
  var _gcHelper = $ss.snapin.getconnect.renderHelper;   
  var _layout = $ss.snapin.getconnect.layoutHelper; 
  var _const = $ss.agentcore.constants;
})();


