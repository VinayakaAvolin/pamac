﻿/**
 * @author Santhi Dhanuskodi
 */
 
$ss.snapin = $ss.snapin || {}; 
$ss.snapin.getconnect = $ss.snapin.getconnect || {};
$ss.snapin.getconnect.layoutHelper = $ss.snapin.getconnect.layoutHelper || {};

(function(){
    
    $.extend($ss.snapin.getconnect.layoutHelper, {

      updatelayoutbuttons: function(Status) {

        switch (Status) {
          case "ENABLE_NEXT":
            this.EnableButton("#nextbutton"); 
            break;      
          case "ENABLE_BACK":
            this.EnableButton("#prevbutton"); 
            break;
          case "DISABLE_NEXT":
            this.DisableButton("#nextbutton"); 
            break;      
          case "DISABLE_BACK":
            this.DisableButton("#prevbutton"); 
            break;   
          case "SHOW_RETRY":
            this.ShowButton("#retrybutton");
            break;
          case "ENABLE_RETRY":
            this.EnableButton("#retrybutton");
            break;
          case "HIDE_RETRY":
            this.HideButton("#retrybutton");
            break; 
          case "HIDE_CLOSE":
            this.HideButton("#closebutton");
            break;
          case "HIDE_BACK":
            this.HideButton("#prevbutton");
            break;
          case "HIDE_NEXT":
            this.HideButton("#nextbutton");
            break;
          case "ENABLE_CLOSE":
            this.EnableButton("#closebutton");
            break;                   
          default:
            break;
        }
      },
      ShowButton: function(sBtnId) {
	      $(sBtnId).show();
      },
      HideButton: function(sBtnId) {
	      $(sBtnId).hide();
      },
      DisableLayoutBtns: function() {	  
	      this.DisableButton("#nextbutton");
	      this.DisableButton("#prevbutton");
    	  this.DisableButton("#retrybutton");
      },
      EnableLayoutBtns: function() {
		    this.EnableButton("#nextbutton");
		    this.EnableButton("#prevbutton");
      },
      EnableButton: function(sBtnId) {
        var sImgsrc = "";
        var sEvt = "";
        switch(sBtnId)
        {
          case "#nextbutton":
            sImgsrc  = "btn_next.gif";
            break;
          case "#prevbutton":
            sImgsrc  = "btn_back.gif";
            break;
          case "#retrybutton":
            sImgsrc  = "btn_retry.gif";
            break;
          case "#closebutton":
            sImgsrc  = "btn_done.gif";
            break;
          default:
            break;
        }

        $(sBtnId+"div").css("display","inline");	
        $(sBtnId).css("display","inline");	
	      //Remove the class "btnDisabled" to controll the click event of the button. 
        $(sBtnId).removeClass("btndisabled");
        var sNewImg =$ss.GetLayoutAbsolutePath() + "\\skins\\" + _config.ParseMacros("%SKINNAME%") + "\\buttons\\" + _config.ParseMacros("%LANGCODE%")+"\\" + sImgsrc;
        $(sBtnId).attr("src",sNewImg);	
      },
      DisableButton: function(sBtnId) {
        var sImgsrc = "";
        var sEvt = "";
        switch(sBtnId)
        {
          case "#nextbutton":
            sImgsrc  = "btn_next_disabled.gif";
		        $(sBtnId+"div").css("display","inline");	
            break;
          case "#prevbutton":
            sImgsrc  = "btn_back_disabled.gif";
            $(sBtnId+"div").css("display","inline");	
            break;
          case "#retrybutton":
            sImgsrc  = "btn_retry_disabled.gif";
            $(sBtnId+"div").css("display","none");	
            break;
          default:
            break;
        }
			  //Remove the class "btnDisabled" to controll the click event of the button. 
        $(sBtnId).addClass("btndisabled");
	      var sNewImg = $ss.GetLayoutAbsolutePath() + "\\skins\\" + _config.ParseMacros("%SKINNAME%") + "\\buttons\\" + _config.ParseMacros("%LANGCODE%")+"\\" + sImgsrc;
	      $(sBtnId).attr("src",sNewImg);	
	    },
	    ShowVisualFlow: function() {
				$("#gc_vflowdiv").css("display", "block");					
      },
      HideVisualFlow: function() {
        $("#gc_vflowdiv").css("display", "none");
      },
      
      InitializeVisualFlow: function() {
        _databag.SetValue("ss_gc_db_vflink1_animation", false);
        _databag.SetValue("ss_gc_db_vflink2_animation", false);
        _databag.SetValue("ss_gc_db_vflink3_animation", false);
      },
      AutoUpdateVisualFlowState: function(){
			
				//Comp to Modem
				var compToModem = _databag.GetValue("ss_gc_db_ModemDetected", "false");
				if(compToModem=="true" 
				    || _databag.GetValue("ss_gc_db_vflink2_animation", false)==true 
				    || _databag.GetValue("ss_gc_db_vflink3_animation", false)==true){
					this.VFLinkComputerToModemPass();		
					//Link 1 need not animate at all once passed the modem detect
					_databag.SetValue("ss_gc_db_vflink1_animation", null);
				}else if ((compToModem=="false") 
				  || (compToModem=="no_auth") 
				  || (compToModem=="do_reboot")){
					this.VFLinkComputerToModemFail();
				}
									
				//Comp to Modem Link Animation check					
				var compToModemLink = _databag.GetValue("ss_gc_db_vflink1_animation", false);
				if(compToModemLink==true){
					this.VFLinkComputerToModemAnimate();
					//Link 1 animation stopped, till set again
					_databag.SetValue("ss_gc_db_vflink1_animation", null);
				}else if (compToModemLink==false){
					this.VFLinkComputerToModemNoResult();
				}					
														
				//Modem to WG					
				var modemToWG = _databag.GetValue("ss_gc_db_SyncOk", "false");
				if(modemToWG=="true" 
				  || _databag.GetValue("ss_gc_db_vflink3_animation", false)==true){
					this.VFLinkModemToWalledGardenPass();		
					//Link 2 need not animate at all once passed the WG Check
					_databag.SetValue("ss_gc_db_vflink2_animation", null);
				}else if ((modemToWG=="false") 
				  || (modemToWG=="no_auth") 
				  || (modemToWG=="do_reboot")){
					this.VFLinkModemToWalledGardenFail();
				}
										
				//Modem to WG Link Animation check
				var modemToWGLink = _databag.GetValue("ss_gc_db_vflink2_animation", false);
				if(modemToWGLink==true){
					this.VFLinkModemToWalledGardenAnimate();
					//Link 2 animation stopped, till set again
					_databag.SetValue("ss_gc_db_vflink2_animation", null);
				}else if(modemToWGLink==false){
					this.VFLinkModemToWalledGardenNoResult();
				}
				
				//WG to Internet					
				var wGToInternet = _databag.GetValue("ss_gc_db_InternetConnected", "false");
				if(wGToInternet=="true"){
					this.VFLinkWalledGardenToInternetPass();		
					//Link 3 need not animate at all once passed the Internet Check
					_databag.SetValue("ss_gc_db_vflink3_animation", null);
				}else if (wGToInternet=="false"){
					this.VFLinkWalledGardenToInternetFail();						
        }
											
				//WG to Internet Link Animation check
				var wGToInternetLink = _databag.GetValue("ss_gc_db_vflink3_animation", false);
				if(wGToInternetLink){
					this.VFLinkWalledGardenToInternetAnimate();
					//Link 3 animation stopped, till set again
					_databag.SetValue("ss_gc_db_vflink3_animation", null);
				}else if(wGToInternetLink==false){
					this.VFLinkWalledGardenToInternetNoResult();
				}
      },
      AnimateVFComputer: function(){
				
      },
      VFLinkComputerToModemNoResult: function(){
				if(!$("#gc_vflow_link1").hasClass("cls_gc_vflowlink1_nr")){
					$("#gc_vflow_link1").removeClass();
					$("#gc_vflow_link1").addClass("cls_gc_vflowlink1_nr");					
				}					
      },
      VFLinkComputerToModemAnimate: function(){
				if(!$("#gc_vflow_link1").hasClass("cls_gc_vflowlink1_anim")){
					$("#gc_vflow_link1").removeClass();
					$("#gc_vflow_link1").addClass("cls_gc_vflowlink1_anim");					
				}					
      },	      
      VFLinkComputerToModemPass: function(){
				if(!$("#gc_vflow_link1").hasClass("cls_gc_vflowlink1_pass")){
					$("#gc_vflow_link1").removeClass();
					$("#gc_vflow_link1").addClass("cls_gc_vflowlink1_pass");
				}
      },
      VFLinkComputerToModemFail: function(){
				if(!$("#gc_vflow_link1").hasClass("cls_gc_vflowlink1_fail")){
					$("#gc_vflow_link1").removeClass();
					$("#gc_vflow_link1").addClass("cls_gc_vflowlink1_fail");
				}
      },
      VFLinkModemToWalledGardenNoResult: function(){
				if(!$("#gc_vflow_link2").hasClass("cls_gc_vflowlink2_nr")){
					$("#gc_vflow_link2").removeClass();
					$("#gc_vflow_link2").addClass("cls_gc_vflowlink2_nr");
				}
      },
      VFLinkModemToWalledGardenAnimate: function(){
				if(!$("#gc_vflow_link2").hasClass("cls_gc_vflowlink2_anim")){
					$("#gc_vflow_link2").removeClass();
					$("#gc_vflow_link2").addClass("cls_gc_vflowlink2_anim");
				}
      },
      VFLinkModemToWalledGardenPass: function(){
				if(!$("#gc_vflow_link2").hasClass("cls_gc_vflowlink2_pass")){
					$("#gc_vflow_link2").removeClass();
					$("#gc_vflow_link2").addClass("cls_gc_vflowlink2_pass");
				}
      },
      VFLinkModemToWalledGardenFail: function(){
				if(!$("#gc_vflow_link2").hasClass("cls_gc_vflowlink2_fail")){
					$("#gc_vflow_link2").removeClass();
					$("#gc_vflow_link2").addClass("cls_gc_vflowlink2_fail");
				}
      },
      VFLinkWalledGardenToInternetNoResult: function(){
				if(!$("#gc_vflow_link3").hasClass("cls_gc_vflowlink3_nr")){
					$("#gc_vflow_link3").removeClass();
					$("#gc_vflow_link3").addClass("cls_gc_vflowlink3_nr");
				}
      },
      VFLinkWalledGardenToInternetAnimate: function(){
				if(!$("#gc_vflow_link3").hasClass("cls_gc_vflowlink3_anim")){
					$("#gc_vflow_link3").removeClass();
					$("#gc_vflow_link3").addClass("cls_gc_vflowlink3_anim");
				}
      },
      VFLinkWalledGardenToInternetPass: function(){
				if(!$("#gc_vflow_link3").hasClass("cls_gc_vflowlink3_pass")){
					$("#gc_vflow_link3").removeClass();
					$("#gc_vflow_link3").addClass("cls_gc_vflowlink3_pass");					
				}
      },
      VFLinkWalledGardenToInternetFail: function(){
				if(!$("#gc_vflow_link3").hasClass("cls_gc_vflowlink3_fail")){
					$("#gc_vflow_link3").removeClass();
					$("#gc_vflow_link3").addClass("cls_gc_vflowlink3_fail");
				}
      },
      gc_ClearDBParams: function() {
        try{
          _databag.RemoveValue("ss_gc_db_restore_ie");
          _databag.RemoveValue("ss_gc_db_SolutionOk");
          _databag.RemoveValue("ss_gc_db_restore_firefox");        
          _databag.RemoveValue("ss_gc_db_ConnectionType");
          _databag.RemoveValue("ss_gc_db_SelectedRASConnection");
          _databag.RemoveValue("ss_gc_db_vflink1_animation");
          _databag.RemoveValue("ss_gc_db_vflink2_animation");
          _databag.RemoveValue("ss_gc_db_vflink3_animation");        
          _databag.RemoveValue("ss_gc_db_ModemDetected");
          _databag.RemoveValue('ss_gc_db_checkrasmodem');
          _databag.RemoveValue('ss_gc_db_OptYesNo');
          _databag.RemoveValue("ss_gc_db_RasErrCode");  
          _databag.RemoveValue("ss_gc_db_KnowsPassword");
          _databag.RemoveValue("ss_gc_db_releaserenew");    
          _databag.RemoveValue("ss_gc_db_restore_network");        
          _databag.RemoveValue("ss_gc_db_restore_wireless");        
          _databag.RemoveValue('ss_gc_db_connectdiagram');        
          _databag.RemoveValue("ss_gc_db_restore_dun");
          _databag.RemoveValue("ss_gc_db_restore_dundrivers");
          _databag.RemoveValue("ss_gc_db_create_wireless_profile");        
          _databag.RemoveValue("ss_gc_db_InternetConnected");
          _databag.RemoveValue("ss_gc_db_SetCreds");
          _databag.RemoveValue("ss_gc_db_DoTestInternet");
          _databag.RemoveValue("GC_SMAPI_DB_USERNAME");
          _databag.RemoveValue("GC_SMAPI_DB_USERPASS");  
          _databag.RemoveValue('ss_gc_db_powercycle');        
          _databag.RemoveValue("ss_gc_db_RetryConnectModem");
          _databag.RemoveValue("ss_gc_db_RetryConnectWG");
          _databag.RemoveValue("ss_gc_db_RetryGetSync");
          _databag.RemoveValue("ss_gc_db_SyncOk");
          _databag.RemoveValue("ss_gc_db_DoTestSync");       
          _databag.RemoveValue('ss_gc_db_activationdate');          
          _databag.RemoveValue("SS_GC_DB_9x_RESTART");        
          _databag.RemoveValue('ss_gc_db_checkcables');           
          _databag.RemoveValue('ss_gc_db_checkfilters');         
          _databag.RemoveValue('ss_gc_db_firewall');
          _databag.RemoveValue('ss_gc_db_phonecable'); 
          _databag.RemoveValue('ss_gc_db_checkrouter');
          //steplist xml's DB values
          _databag.RemoveValue('ss_gc_db_HaveRebooted');
          _databag.RemoveValue('ss_wrl_db_EntryPoint');
          _databag.RemoveValue('ss_gc_db_ConnectModemStatus');
          _databag.RemoveValue('ss_gc_db_CheckSyncEntry');
          _databag.RemoveValue('ss_gc_db_GetSyncStatus');
          _databag.RemoveValue('ss_gc_db_RasErrCode');
          _databag.RemoveValue('ss_gc_db_ras_CheckDialEntry');
          _databag.RemoveValue('ss_gc_db_FromGC');
          _databag.RemoveValue('ss_gc_db_CheckAllAdapters');
          _databag.RemoveValue('ss_gc_db_DoDetectModem');
          _databag.RemoveValue('ss_gc_db_DoTestSync');
          _databag.RemoveValue('ss_gc_db_DoTestInternet');
          _databag.RemoveValue('ss_gc_db_Browser');
          _databag.RemoveValue('ss_gc_db_HandledRas680');
          _databag.RemoveValue('ss_gc_db_HandledRas619');
          _databag.RemoveValue('ss_gc_db_HandledRas691');                
          _databag.RemoveValue('ss_gc_db_ras_UpdateCreds');
          _databag.RemoveValue('ss_gc_db_HandledRas721');
          _databag.RemoveValue('ss_gc_db_HandledRasXXX');
          _databag.RemoveValue('ss_gc_db_InitialTest');
        }
        catch(Err) { }
      }	      
	      
    });
    
    var _config = $ss.agentcore.dal.config;               
    var _databag = $ss.agentcore.dal.databag;    
})();
