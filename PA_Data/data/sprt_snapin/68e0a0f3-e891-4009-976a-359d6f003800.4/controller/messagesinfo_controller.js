SnapinMessagesinfoController = SnapinBaseController.extend('snapin_messagesinfo', {
  hasinitialized: false,
  Initialize: function() {
    $ss.agentcore.events.Subscribe("BROADCAST_ON_EXTERNALREFRESH", function(oEvent) { return SnapinMessagesinfoController.OnExternalRefresh(oEvent) });
    $ss.agentcore.events.Subscribe("BROADCAST_ON_EXTERNALCLOSE", function(oEvent) { return SnapinMessagesinfoController.OnExternalClose(oEvent) });
    this.hasinitialized = true;
  },
  scheduleJobName: "sprtRestartSchedule",
  scheduleJobInterval: $ss.agentcore.dal.config.GetConfigValue("snapin_messagesinfo", "restart_message_interval", "600"), //Interval is set in seconds
  scheduleApplication: $ss.agentcore.dal.ini.GetPath("EXE_PATH"),
  schedulerClient: false,
  curPopupType: 0,
  //0 - install complete(disabled), 1 - reboot required, 2 - reboot mandatory, 3 - failure (disabled)

  InitializeSchedulerClient: function() {
    try {
      this.schedulerClient = $ss.agentcore.utils.ui.CreateObject("Sprttaskclient.Scheduler", false);
    }
    catch (oErr) {
      this.schedulerClient = false;
    }
  },

  AddJob: function() {
    if (!this.schedulerClient) this.InitializeSchedulerClient();
    var dictArgs = this.CreateDictArgs();
    if ((!this.schedulerClient) || (!dictArgs)) {
      return;
    }
    if ($("#remindMeLater")[0].checked === true) {
      var ret = this.schedulerClient.AddScheduleJob(dictArgs.keys(), dictArgs.Items());
    }
  },

  GetArguments: function() {
    //"/p NARAIN-2K3 /instancename \"minibcont_messagesinfo\" /path \"/messagesinfo?callback=true&mandatoryreeboot=true\" /ini \"c:\\program files\\NARAIN-2K3\\agent\\bin\\minibcont.ini\""			      
    var instanceName = " /instancename \"minibcont_messagesinfo\"";
    var APP_PROVIDER = $ss.agentcore.dal.config.GetProviderID();
    var callBackArgument = " /path \"/messagesinfo?callback=true&postponereeboot=true\"";
    if (this.curPopupType === 2)
      var callBackArgument = " /path \"/messagesinfo?callback=true&mandatoryreeboot=true\"";

    var iniPath = " /ini \"" + $ss.agentcore.dal.config.GetProviderRootPath() + "agent\\bin\\minibcont.ini" + "\"";
    var startPath = "/p " + APP_PROVIDER + instanceName + callBackArgument + iniPath;
    return startPath;
  },

  CreateDictArgs: function() {
    var odict = $ss.agentcore.utils.ui.CreateObject("Scripting.Dictionary", false);
    if (odict) {
      odict.add("SCHEDULE_NAME", this.scheduleJobName);
      odict.add("SCHEDULE_START_TIME", "NOW"); 	//"11/09/2009 23:07:30"				
      odict.add("SCHEDULE_INTERVAL", this.scheduleJobInterval);
      odict.add("SCHEDULE_RECURRENCE", "1");
      odict.add("SCHEDULE_FIRST_TRIGGER", "false");
      odict.add("SCHEDULE_APPLICATION", this.scheduleApplication);
      odict.add("SCHEDULE_ARGUMENTS", this.GetArguments());
      odict.add("SCHEDULE_FORCE_STOP_ON_REBOOT", "true");
      odict.add("PROVIDER_ID", $ss.agentcore.dal.config.GetProviderID());
      return odict;
    }
    else {
      return false;
    }
  },

  GetRegRoot: function() {
    var REG_SPRT = "Software\\SupportSoft\\ProviderList\\";
    var REG_APPLICATION = "Download Manager\\MODULE";
    var REG_THIS = REG_SPRT + $ss.agentcore.dal.config.GetProviderID() + "\\" + REG_APPLICATION;
    return REG_THIS;
  },

  OnExternalClose: function(oEvent) {
    this.AddJob();
  },

  OnExternalRefresh: function(oEvent) {

    if (!oEvent.description) return false;
    var REG_THIS = this.GetRegRoot();
    var cmdLine = oEvent.description;
    //crude way of handling it
    var cmdLineRegEx = /\/\w+\s+"([^"]*)"/g;
    var aCmdLine = cmdLine.match(cmdLineRegEx);
    var itemRegEx = /(\/\w+)\s+"([^"]*)"/;
    var path = "";


    for (var iCnt = 0; iCnt < aCmdLine.length; iCnt++) {
      var oC1 = aCmdLine[iCnt];
      var oItems = itemRegEx.exec(oC1);
      if (oItems.length >= 3) {
        var p1 = oItems[1];
        var p2 = oItems[2];
        if (p1.substr(0, 1) === "/") {
          p1 = p1.replace("\/", "");
          switch (p1) {
            case "path":
              if (p2.indexOf("messagesinfo") > -1) {
                path = p2;
              }
              break;
          }
        }
      }
    }
    if (path === "") return false;
    //this is for onExternal Refresh
    var params = params || {};
    //read the command line .... 
    params.requesturl = path + "&refresh=true";
    params.requestQS = $ss.agentcore.utils.GetQSFromURL(params.requesturl);
    params.requestArgs = $ss.agentcore.utils.GetQSObjectFromURL(params.requesturl);
    if (params.requestArgs["DldList"]) {
      var dldListKey = params.requestArgs["DldList"];
      var data = this.ProcessCallBack(dldListKey);
      if (data.requiresRestart || params.requestArgs["postponereeboot"]) {
        this.curPopupType = 1;
        MVC.Controller.dispatch("navigation", "index", params);
      }
      else {
        $ss.agentcore.dal.registry.DeleteRegVal("HKCU", REG_THIS, dldListKey);
        return false;
      }
    } else if (params.requestArgs["mandatoryreeboot"]) {
      //If popup is for mandatory (no evaluation) call comes from binary and from scheduler
      this.curPopupType = 2;
      MVC.Controller.dispatch("navigation", "index", params);
    } else if (params.requestArgs["silentFailure"]) {
      var sFailKey = params.requestArgs["FailList"];
      var data = this.ProcessFailure(sFailKey);
    }
    return false;
  },

  IsInstalled: function(psId) {
    var result = false
    var REG_THIS = this.GetRegRoot();
    var type = "";
    var path = "";
    path = $ss.agentcore.dal.registry.GetRegValue("HKCU", REG_THIS + "\\" + psId, "sdc_sd_VerificationPath");
    if (path !== "") {
      type = $ss.agentcore.dal.registry.GetRegValue("HKCU", REG_THIS + "\\" + psId, "sdc_sd_VerificationType");
      try {
        result = this.VerifyInstallationRequired(type, path);
      }
      catch (err) {
        result = this.IsInstalledOld(type, path);
      }
    }
    return result;
  },

  CmdTokenToRawToken: function(cmdLineToken) {
    cmdLineToken = cmdLineToken.trim();
    if (cmdLineToken.charAt(0) == "\"" && cmdLineToken.charAt(cmdLineToken.length - 1) == "\"") {
      cmdLineToken = cmdLineToken.trimDQuote().trim();
    }
    return cmdLineToken;
  },

  VerifyInstallationRequired: function(locationType, dataString) {
    var location;
    if (locationType === "0")
      location = "FileSystem";
    else
      location = "Registry";
    var wildCardChar = "*";
    //var dataString = "HKLM\\Software\\Symantec\\Common Client##Version##104.1.13.20";
    //dataString = "%WinDir%\\digest.dll##Version##.##6.*";
    var dataArr = dataString.split(/\#\#/);
    var splitOperator = ".";
    var requiredVersion = "";
    var arrReqVer, availableVersion, arrAvailVer;

    if (dataArr.length != 3) throw "Error: Invalid verification string";
    requiredVersion = dataArr[2];
    arrReqVer = requiredVersion.split(new RegExp("\\" + splitOperator));
    if (location === "Registry") {
      var dalObj = $ss.agentcore.dal.registry;
      var regPath = dataArr[0];
      var regPathArr = regPath.split("\\");
      var regTree = regPathArr[0];
      var regPath = regPathArr.slice(1, regPathArr.length).join("\\");
      var regKey = dataArr[1];
      if (!dalObj.RegValueExists(regTree, regPath, regKey)) return false;
      availableVersion = dalObj.GetRegValue(regTree, regPath, regKey);
    }
    else {
      var fso = $ss.agentcore.dal.file.GetFileSystemObject();
      var filePath = $ss.agentcore.dal.config.ExpandAllMacros(dataArr[0]);
      filePath = this.CmdTokenToRawToken(filePath);
      if (!fso.FileExists(filePath)) return false;
      switch (dataArr[1].toLowerCase()) {
        case "version":
        case "file version":
        case "fileversion":
          availableVersion = fso.GetFileVersion(filePath);
          break;
        default:
          return false;
      }
    }
    if (availableVersion.match(new RegExp("\\" + splitOperator)) == -1) return false;
    arrAvailVer = availableVersion.split(new RegExp("\\" + splitOperator));

    //Append wild card character if the version strings are of unequal length
    var iAvailLen = arrAvailVer.length, iReqLen = arrReqVer.length;
    if (iAvailLen > iReqLen && arrReqVer[iReqLen - 1] !== wildCardChar) arrReqVer[iReqLen] = wildCardChar;
    else if (iReqLen > iAvailLen && arrAvailVer[iAvailLen - 1] !== wildCardChar) arrAvailVer[iAvailLen] = wildCardChar;

    return this.CompareValues(arrReqVer, arrAvailVer, wildCardChar);
  },

  CompareValues: function(arrReqVer, arrAvailVer, wildCardChar) {
    var result = true;
    for (var i = 0; i < arrReqVer.length; i++) {
      if (arrReqVer[i] === wildCardChar) break;
      if (arrAvailVer[i] === wildCardChar) break;
      if (parseInt(arrReqVer[i]) < parseInt(arrAvailVer[i])) break;
      if (parseInt(arrReqVer[i]) > parseInt(arrAvailVer[i])) {
        result = false;
        break;
      }
    }
    return result;
  },

  IsInstalledOld: function(type, path) {
    switch (type) {
      case "0":
        return ($ss.agentcore.dal.file.FileExists($ss.agentcore.dal.config.ExpandAllMacros(path)));
        break;
      case "1":
        var aPath = path.split("\\");
        if (aPath.length == 1) {
          aPath = psPath.split("/");
        }
        var sRoot = aPath[0];
        var sValue = aPath[aPath.length - 1];
        var sPath = aPath.slice(1, aPath.length - 1).join("\\");
        return (this.RegValueExists(sRoot, sPath, sValue));
        break;
    }
    return (true);
  },

  RegValueExists: function(psRoot, psKey, psValue) {
    var sVal = "";
    try {
      sVal = $ss.agentcore.dal.registry.RegValueExists(psRoot, psKey, psValue);
      return ((sVal != ""));
    }
    catch (oErr) { }
    return (false);
  },


  IsReebootPending: function() {
    var sProvider = $ss.agentcore.dal.config.GetProviderID();
    var sRegPath = "SYSTEM\\CurrentControlSet\\Control\\Session Manager";
    var sRegKey = "PendingFileRenameOperations";
    var sRegValue = $ss.agentcore.dal.registry.GetRegValue("HKLM", sRegPath, sRegKey);
    var sCmpVal = "\\??\\CONSONA DM REBOOT_" + sProvider;
    if (sRegValue.toUpperCase() === sCmpVal.toUpperCase()) return true;
    return false;
  },

  ProcessCallBack: function(dldList) {
    var dldKey = dldList;
    var data = {};
    data.requiresRestart = false;
    data.show = false;
    var REG_THIS = this.GetRegRoot();

    dldList = $ss.agentcore.dal.registry.GetRegValue("HKCU", REG_THIS, dldKey);
    if (dldList == "") return data;
    dldList = dldList.split("#");
    var show = false;
    var restart = false;
    var oConfig = $ss.agentcore.dal.config;
    var oUtils = $ss.agentcore.utils;
    data.title = $ss.agentcore.utils.LocalXLate("snapin_messagesinfo", "dm_snp_msginfo_title");
    data.description = $ss.agentcore.utils.LocalXLate("snapin_messagesinfo", "dm_snp_msginfo_description");
    data.dispImage = "fa-info-circle";

    for (var i = 0; i < dldList.length; i++) {
      // verify the path or registry

      if (this.IsInstalled(dldList[i])) {
        this.curPopupType = 0;
        show = false; //Disabled for Install Complete popup
        data.show = show;
        if ($ss.agentcore.dal.registry.GetRegValue("HKCU", REG_THIS + "\\" + dldList[i], "sdc_sd_requiresRestart") === "yes") {
          this.curPopupType = 1;
          data.show = true;
          restart = true;
          data.requiresRestart = true;
          data.title = $ss.agentcore.utils.LocalXLate("snapin_messagesinfo", "dm_snp_msginfo_restart_title");
          data.description = $ss.agentcore.utils.LocalXLate("snapin_messagesinfo", "dm_snp_msginfo_restart_description");
          break;
        }
      }
    }

    return data;

  },


  ProcessFailure: function(sFailKey) {
    REG_THIS = this.GetRegRoot();
    var data = {};
    sFailList = $ss.agentcore.dal.registry.GetRegValue("HKCU", REG_THIS, sFailKey);
    if (sFailList != "")
      sFailList = sFailList.split("#");
    if (sFailList.length > 1) {
      for (var i = 0; i < sFailList.length; i++)
        $ss.agentcore.dal.registry.SetRegValue("HKCU", REG_THIS + "\\" + sFailList[i], "FailureDisplayed", "true");

      data.title = $ss.agentcore.utils.LocalXLate("snapin_messagesinfo", "dm_snp_silent_fail_title");
      data.description = $ss.agentcore.utils.LocalXLate("snapin_messagesinfo", "dm_snp_silent_fail_description");
    } else {
      var sFailGuid = sFailList[0];
      $ss.agentcore.dal.registry.SetRegValue("HKCU", REG_THIS + "\\" + sFailGuid, "FailureDisplayed", "true");
      var sTitle = $ss.agentcore.dal.registry.GetRegValue("HKCU", REG_THIS + "\\" + sFailGuid, "sdc_sd_ErrorTitle");
      var sDescription = $ss.agentcore.dal.registry.GetRegValue("HKCU", REG_THIS + "\\" + sFailGuid, "sdc_sd_ErrorDescription");
      data.title = sTitle;
      data.description = sDescription;
    }
    data.dispImage = "fa-exclamation-triangle";
    data.requiresRestart = false;
    $ss.agentcore.dal.registry.DeleteRegVal("HKCU", REG_THIS, sFailKey);
    return data;
  }

},

{
  //Private Access Area
  index: function(params) {
    var REG_THIS = this.Class.GetRegRoot();

    if (!this.Class.hasinitialized) {
      this.Class.Initialize();
    }
    var hasDataToShow = false;
    var bFailure = false;
    params = params || {};
    params.requestArgs = params.requestArgs || {};
    //params.requestArgs.restart = params.requestArgs.restart || "false";
    try {
      if (params.requestArgs["callback"] === "true") {
        if (params.requestArgs["DldList"]) {
          var dldList = params.requestArgs["DldList"];
          var data = this.Class.ProcessCallBack(dldList);

          if (data.show) {
            hasDataToShow = true;
          }
          $ss.agentcore.dal.registry.DeleteRegVal("HKCU", REG_THIS, dldList);
        } else if (params.requestArgs["mandatoryreeboot"]) {
          this.Class.curPopupType = 2;
          var data = {};
          data.show = true;
          data.mandatory = true;
          data.requiresRestart = true;
          data.title = $ss.agentcore.utils.LocalXLate("snapin_messagesinfo", "dm_snp_msginfo_mandatory_reeboot_title");
          data.description = $ss.agentcore.utils.LocalXLate("snapin_messagesinfo", "dm_snp_msginfo_mandatory_reeboot_description");
          data.dispImage = "fa-exclamation-triangle";
          hasDataToShow = true;
        } else if (params.requestArgs["postponereeboot"]) {
          //if the call is from scheduler handle it
          this.Class.curPopupType = 1;
          var data = {};
          data.show = true;
          data.requiresRestart = true;
          data.title = $ss.agentcore.utils.LocalXLate("snapin_messagesinfo", "dm_snp_msginfo_restart_title");
          data.description = $ss.agentcore.utils.LocalXLate("snapin_messagesinfo", "dm_snp_msginfo_restart_description");
          data.dispImage = "fa-info-circle";
          hasDataToShow = true;
        } else if (params.requestArgs["silentFailure"]) {
          var sFailKey = params.requestArgs["FailList"];
          var data = this.Class.ProcessFailure(sFailKey);
          this.Class.curPopupType = 3;
          hasDataToShow = false; //disabled as of now
          bFailure = true;
        }
      }
      if (hasDataToShow) {
        //has data -- so show the stuff...
        this.renderSnapinView("snapin_messagesinfo", params.toLocation, "\\views\\messagesinfo.html", { "info": data });
        $ss.agentcore.utils.ShowBcont();
        $ss.agentcore.utils.ui.FixPng();
      } else {
        //does not have any data .. close bcont
        $ss.agentcore.utils.CloseBcont();
      }
    } catch (ex) {
      $ss.agentcore.utils.CloseBcont();
    }

  },


  "#restartYes click": function() {
    $ss.agentcore.utils.system.Reboot();
  },

  "#restartNo click": function() {
    $ss.agentcore.utils.ui.CloseBcont();
  },

  "#remindMeLater click": function() {
  }

});
