SnapinPendingactionwidgetController = SnapinBaseController.extend('snapin_pendingactionwidget',
{
  //Private Access Area
  index: function (params) {
      totalActions = 0;
      if(NavigationController.IsUrlValid("/sa/protect"))  this.Class.dispatch("snapin_protectrestore", "indexActionWidget", params);
      if(NavigationController.IsUrlValid("/dm"))  this.Class.dispatch("snapin_download_manager", "indexActionWidget", params);
      if(NavigationController.IsUrlValid("/sa/messagesviewer"))  this.Class.dispatch("snapin_messageviewer","indexActionWidget" ,params);
      if(NavigationController.IsUrlValid("/Accountmanager"))  this.Class.dispatch("snapin_account_manager", "indexActionWidget", params);
      globparams = params;
      this.renderSnapinView("snapin_pendingactionwidget", params.toLocation, "\\views\\pendingactionwidget.html",{"oContents": params});
    },
    
    RenderSnapin: function(params) {
      this.renderSnapinView("snapin_pendingactionwidget", params.toLocation, "\\views\\pendingactionwidget.html",{"oContents": params});
    },

    ".protectlink click" :function(params){
      var arr=[];
      if(globparams.guidListToProtect.length > 0){
        for(var i =0;i< globparams.guidListToProtect.length;i++){
          arr.push(globparams.guidListToProtect[i].guid);
        }
      }

      params.requesturl = "/sa/protect";
      params.requestPath = "/sa/protect";
      params.SelectedId = arr;
      params.action = ".navigate click";
      try {
        this.Class.dispatch("navigation", "index", params);
      }catch (ex) {
        this.Class.logger.error("Action protect click:Error in Dispatching to Navigation Controller %1%", ex.message);
      }
    },
    
    ".installlink click" :function(params){
      var arr=[];
      if(globparams.guidListToInstall.length > 0){
        for(var i =0;i< globparams.guidListToInstall.length;i++){
          arr.push({"id" :globparams.guidListToInstall[i].scc_content_guid});
        }
      }
      params.requesturl = "/dm";
      params.requestPath = "/dm";
      params.SelectedId = arr;
      params.action = ".navigate click";
      try {
        this.Class.dispatch("navigation", "index", params);
      }catch (ex) {
        this.Class.logger.error("Action install click:Error in Dispatching to Navigation Controller %1%", ex.message);
      }
    },
    
    ".msgslink click" :function(params){
      var arr=[];
      if(globparams.msgList.length > 0){
        arr = globparams.msgList;
      }
      params.requesturl = "/sa/messagesviewer";
      params.requestPath = "/sa/messagesviewer";
      params.UnreadMsgguids = arr;
      params.action = ".navigate click";
      try {
        this.Class.dispatch("navigation", "index", params);
      }catch (ex) {
        this.Class.logger.error("Action Msg click:Error in Dispatching to Navigation Controller %1%", ex.message);
      }
    },
    
    indexAccMngr :function(params){
      var userInfoRegPath = "Software\\SupportSoft\\ProviderList\\" + _config.GetProviderID() + "\\" + _config.ParseMacros("%PRODUCT%") + "\\users\\" +  _config.ExpandSysMacro("%USER%") + "\\ss_config\\UserInfo";
      var dispmsg = params.msg ||{};
      var amResetURL = params.model.sResetUrl || {};
      $(".accupdate").attr('title',dispmsg);
      $(".accupdate").html(_utils.GetTruncatedString(dispmsg, maxStrLen));
      if(params.msg != ""){
        $(".amrow").css("display","block");
        totalActions = totalActions + 1;
      }
      $(".totalcount").html(_utils.LocalXLate("snapin_pendingactionwidget","sa_msg_All_UpdatedActions",[totalActions]));
      $ss.agentcore.dal.registry.SetRegValue("HKCU", userInfoRegPath, "uAMResetURL",amResetURL);
      if (totalActions == 0) $(".WidgetNoContentDiv").css("display","block");
    },
    
    ".accupdate click":function(params){
      var amModel = globparams.model;
      params.requesturl = "/Accountmanager?id=account_manager_nav_summary";
      if(globparams.msg == $ss.agentcore.utils.LocalXLate("snapin_pendingactionwidget","sa_msg_widget_register_link_txt")){
        params.subUrl = amModel.sRegUrl;
      }else{
        params.subUrl = amModel.sResetUrl;
      }
      params.fromSnapin = "Dashboard";
      params.action = ".navigate click";
      try {
        this.Class.dispatch("navigation", "index", params);
      }catch (ex) {
        this.Class.logger.error("Repair click:Error in Dispatching to Navigation Controller %1%", ex.message);
      }
    }
    
    
});

var globparams ;
var totalActions = 0;
var _config = $ss.agentcore.dal.config;
var _utils = $ss.agentcore.utils;
var maxStrLen = _config.GetConfigValue("snapin_pendingactionwidget", "maxStringLenToDisp");