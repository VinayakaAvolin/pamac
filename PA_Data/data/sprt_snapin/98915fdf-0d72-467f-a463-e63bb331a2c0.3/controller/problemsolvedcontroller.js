
$ss.snapin = $ss.snapin || {};
$ss.snapin.problemsolved = $ss.snapin.problemsolved || {};
(function () {
  SnapinProblemsolvedController = SnapinBaseController.extend('snapin_problemsolved', {
    templateLocation : ""
  }, {
    index: function (params) {
      var that = this;
      that.Class.templateLocation = params.toLocation;
      var reload = params.snpprobsolved_refresh || isStale();
      that.renderSnapinView("snapin_problemsolved", that.Class.templateLocation, "\\views\\progress.html");
      var oContent = {};

      getData(reload)
      .done(function (data) {
        oContent = ProcessData(data);
        that.renderSnapinView("snapin_problemsolved", that.Class.templateLocation, "\\views\\problemsolved.html", { oContent: oContent });
        
        //Doughnut Charts
        var dataPS = [{
          value: oContent.iSelfServiceInPercentage,
          color: "#1284C7"
        },{
          value: oContent.iSelfHealInPercentage,
          color: "#87B840"
        }]
        var options = { animation: true, percentageInnerCutout: 70 };
        //Error Handling
        if($("#myChart") && $("#myChart").get(0)){
          myNewChart = new Chart($("#myChart").get(0).getContext("2d")).Doughnut(dataPS, options);
        }
        
      }).fail(function (reason) {
        oContent.error_code = reason;
        that.renderSnapinView("snapin_problemsolved", that.Class.templateLocation, "\\views\\problemsolved.html", { oContent: oContent });
      });
    },
    
    ".problemSolvedRefresh click": function(params) {
      params.snpprobsolved_refresh = true;
      this.index(params);
    }
  });

  function getData(reload) {
    var $def = $.Deferred();
    var fetchFromServer = reload === true || !_file.FileExists(sProblemSolvedFilePath) ;
    if (!fetchFromServer) {
      try {
        var contentData = $.parseJSON(_file.GetFileContents(sProblemSolvedFilePath));
        $def.resolve(contentData);
      } catch (e) {
        logger.error("Invalid local data");
        fetchFromServer = true;
      }
    }
    if (fetchFromServer) {
      var  hostname = Gethostname();
      if(!hostname || !_username) {
        $def.reject('snp_fail_to_retreive_local_info');
        $def.promise();
      }
      $.support.cors = true;
      $.ajax({
        url: geturl(),
        method: 'POST',
        data: { user: _username, hostname: hostname },
        dataType: 'json',
        cache: false
      }).then(function (c) {
        if(c.error) {
          $def.reject(c.error);
          return;
        }
        $def.resolve(c);  //mark as resolved even if saved fails eventually.
        try {
          _file.WriteNewFile(sProblemSolvedFilePath, JSON.stringify(c));
        } catch (e) {
          logger.error("Unable to save data to machine");
        }
      }).fail(function (reason) {
        $def.reject('snp_fail');  //TODO: do we need to show each error
      });
    }
    return $def.promise();
  }

  function isStale() {
    try {
      if (_json_load_freq == 0) return true;
      if (_file.FileExists(sProblemSolvedFilePath)) {
        var contentData = $.parseJSON(_file.GetFileContents(sProblemSolvedFilePath));
        var time_now = new Date().toUTCString();
        if (IsValidDateTime(contentData.time)) {
          var diff = (new Date(Date.parse(time_now)) - new Date(Date.parse(contentData.time))) / (1000 * 24 * 3600);
          return (diff >= _json_load_freq);
        }
      }
    } catch (e) {
      logger.error('failed to detemine state from local file [' + sProblemSolvedFilePath + '].Reason=' + e.message);
    }
    return true;
  }

  function geturl() {
    return NavigationController.GetServerUrl() + "/sdcxuser/asp/problemsolved.asp";
  }

  function Gethostname() {
    var hostname;
    try {
      var _smartissue = $ss.agentcore.diagnostics.smartissue;
      var issueID = _smartissue.CreateIssue("Machine_Info", true, siXMLPath, false, false, null);
      hostname = _smartissue.GetIssueData(issueID, "SDC_Connectivity", "ConnectionData", "HostName");
      _smartissue.DeleteIssue(issueID);
    }
    catch (e) {
      logger.error('Unable to fetch machine hostname ' + e.message);
    }
    return hostname;    
  }

  function IsValidDateTime(data) {
    if ((new Date(Date.parse(data)).toString().match(/(i?)Invalid Date/)) === null)
      return true;
    return false;
  }
  
  function formatDateTime(DateString){
    var date = new Date(Date.parse(DateString));
    return date.toLocaleDateString() + " " + date.toLocaleTimeString();
  }
  
  function ProcessData(contentData) {
    
    var oContent = {};
    
    if(contentData == null){
      oContent.error_code = "snp_fail";
      return oContent;
    }
    var oSelfService = contentData["SS"];
    var oSelfHeal = contentData["SH"];
    var iTotalSuccess;

    oContent.bShowLastUpdateTime = _config.GetConfigValue("snapin_problemsolved", "show_timestamp").match(/(i?)true/) !== null ? true : false;
    if (oContent.bShowLastUpdateTime === true) {
      oContent.time = formatDateTime(contentData.time); // conversion to local timezone occurs
    }
    oContent.iSelfServiceTotalExecution = oContent.iSelfHealTotalExecution = oContent.iSelfServiceInPercentage = oContent.iSelfHealInPercentage = oContent.iSelfHealTotalSucceeded = oContent.iSelfServiceTotalSucceeded = 0;

    $.each(oSelfService, function (sContentType, aExecStatus) {
      oContent.iSelfServiceTotalExecution = oContent.iSelfServiceTotalExecution + aExecStatus.executions
      oContent.iSelfServiceTotalSucceeded = oContent.iSelfServiceTotalSucceeded + aExecStatus.Success
    });
    $.each(oSelfHeal, function (sContentType, aExecStatus) {
      oContent.iSelfHealTotalExecution = oContent.iSelfHealTotalExecution + aExecStatus.executions
      oContent.iSelfHealTotalSucceeded = oContent.iSelfHealTotalSucceeded + aExecStatus.Success
    });

    oContent.totalSuceeded = oContent.iSelfServiceTotalSucceeded + oContent.iSelfHealTotalSucceeded;
    
    if (oContent.totalSuceeded > 0) {
      oContent.iSelfServiceInPercentage =  Math.round((oContent.iSelfServiceTotalSucceeded * 100) / oContent.totalSuceeded);
      oContent.iSelfHealInPercentage = 100 - oContent.iSelfServiceInPercentage;
      oContent.success = true;
    }
    else if(oContent.totalSuceeded == 0){
      oContent.success = true;
      oContent.iSelfHealInPercentage = oContent.iSelfHealInPercentage = 0;
    }
    else {
      oContent.error_code = "snp_fail";
    }
    return oContent;
  }

  /************ Private data *****************/
  var logger = $ss.agentcore.log.GetDefaultLogger('snapin_problemsolved');
  var _file = $ss.agentcore.dal.file;
  var _config = $ss.agentcore.dal.config;
  var _username = _config.ExpandSysMacro("%USER%");
  var sProblemSolvedFilePath = $ss.getSnapinAbsolutePath("snapin_problemsolved") + '\\ProblemSolvedData.json';
  var _json_load_freq = parseInt(_config.GetConfigValue("snapin_problemsolved", "frequency_for_json_load_days"));
  var siXMLPath = $ss.getSnapinAbsolutePath("snapin_problemsolved") + "\\smartissue\\default.xml";

})();