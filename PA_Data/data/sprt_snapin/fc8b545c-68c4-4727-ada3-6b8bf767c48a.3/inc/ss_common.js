// Namespaces
$ss.snapin = $ss.snapin || {};
$ss.snapin.firstrun = $ss.snapin.firstrun || {};
$ss.snapin.firstrun.dec = $ss.snapin.firstrun.dec || {};
$ss.snapin.firstrun.navhandler = $ss.snapin.firstrun.navhandler || {};
$ss.snapin.firstrun.renderhelper = $ss.snapin.firstrun.renderhelper || {};
$ss.snapin.firstrun.common = $ss.snapin.firstrun.common || {};

//start of common functions 
(function(){
  $.extend($ss.snapin.firstrun.common, {
    DisableButton: function(sButtonName){
      if (sButtonName == "next") {
        $("#nextbutton")[0].src = $("#nextbutton")[0].src.replace("btn_next.gif","btn_next_disabled.gif");
        $("#nextbutton").attr("disabled", "true");
      } else if (sButtonName == "previous") {
        $("#prevbutton")[0].src = $("#prevbutton")[0].src.replace("btn_back.gif","btn_back_disabled.gif");
        $("#prevbutton").attr("disabled", "true");
      }
    },
    PrintWindow: function(sCurrentTabHTML, oTempPrintFrame){
      oTempPrintFrame.document.open();
      oTempPrintFrame.document.write("<html><body>" + sCurrentTabHTML + "</body></html>");
      oTempPrintFrame.document.close();
      oTempPrintFrame.focus();
      oTempPrintFrame.print();
    },
    EnableButton: function(sButtonName){
      if (sButtonName == "next") {
        $("#nextbutton")[0].src = $("#nextbutton")[0].src.replace("btn_next_disabled.gif","btn_next.gif");
        $("#nextbutton").removeAttr("disabled");
      } else if (sButtonName == "previous") {
        $("#prevbutton")[0].src = $("#prevbutton")[0].src.replace("btn_back_disabled.gif","btn_back.gif");
        $("#prevbutton").removeAttr("disabled");
      }
    },
    ToggleLinkClass: function(ClassName, Enabled){
      $("#frcolumnone .frlinksCK").removeClass('frlinksCK').addClass('frlinks');
      $("#frcolumnone .frlinkslastCK").removeClass('frlinkslastCK').addClass('frlinkslast');
      
      if (Enabled){
        if (ClassName == "complete"){
          $("#frcolumnone div[@name='" + ClassName + "']").removeClass('frlinkslast').addClass('frlinkslastCK');
          //$("#frcolumnone div[@name='" + ClassName + "']").addClass('frlinkslastCK');
        } else {
          $("#frcolumnone div[@name='" + ClassName + "']").removeClass('frlinks').addClass('frlinksCK');
          //$("#frcolumnone div[@name='" + ClassName + "']").addClass('frlinksCK');
        }   
      } else {
        if (ClassName == "complete"){
          $("#frcolumnone div[@name='" + ClassName + "']").removeClass('frlinkslastCK').addClass('frlinkslast');
          //$("#frcolumnone div[@name='" + ClassName + "']").addClass('frlinkslast');
        } else {
          $("#frcolumnone div[@name='" + ClassName + "']").removeClass('frlinksCK').addClass('frlinks');
          //$("#frcolumnone div[@name='" + ClassName + "']").addClass('frlinks');        
        }
      }
    },
    SaveFormData: function(){
      $ss.agentcore.dal.config.SetUserValue("UserInfo","user_account_no", $("#accountnumber").val());
      $ss.agentcore.dal.config.SetUserValue("UserInfo","birth_day", $("#BirthDay").val());
      $ss.agentcore.dal.config.SetUserValue("UserInfo","birth_month", $("#BirthMonth").val());
      $ss.agentcore.dal.config.SetUserValue("UserInfo","birth_year", $("#BirthYear").val());
      $ss.agentcore.dal.config.SetUserValue("UserInfo","first_name", $("#firstname").val());
      $ss.agentcore.dal.config.SetUserValue("UserInfo","last_name", $("#lastname").val());
      $ss.agentcore.dal.config.SetUserValue("UserInfo","phone_country_id", $("#countrycode").val());
      $ss.agentcore.dal.config.SetUserValue("UserInfo","phone_area_code", $("#areacode").val());
      $ss.agentcore.dal.config.SetUserValue("UserInfo","user_phone_no", $("#phonenumber").val());
      $ss.agentcore.dal.config.SetUserValue("UserInfo","secret_question", $("#question").val());
      $ss.agentcore.dal.config.SetUserValue("UserInfo","secret_question_answer", $("#answers").val());
      $ss.agentcore.dal.config.SetUserValue("UserInfo","user_id", $("#username").val());
      $ss.agentcore.dal.config.SetUserValue("UserInfo","user_password", $ss.agentcore.utils.EncryptStrToHex($("#password").val()));
      $ss.agentcore.dal.config.SetUserValue("UserInfo","user_password_confirm", $ss.agentcore.utils.EncryptStrToHex($("#cpassword").val()));
      $ss.agentcore.dal.config.SetUserValue("firstrun", "AccountInformationCollected", "true");

      if($ss.agentcore.dal.config.GetConfigValue("firstrun", "force_account_verification") === "true")
        $ss.agentcore.dal.config.SetUserValue("firstrun", "AccountInformationVerified", "true");
      else
        $ss.agentcore.dal.config.SetUserValue("firstrun", "AccountInformationVerified", "false");
    },
    GetFormData: function(){
      var data = {};
      
      var regRoot = $ss.agentcore.dal.config.GetProviderID();
      var user = $ss.agentcore.dal.config.ExpandSysMacro("%USER%");
      var prodName = $ss.agentcore.dal.config.ParseMacros("%PRODUCT%");      
            
      var regPath = "Software\\SupportSoft\\ProviderList\\" + regRoot + "\\"+prodName+"\\users\\" + user + "\\ss_config\\UserInfo\\";
     
      data["accountnumber"] = $ss.agentcore.dal.registry.GetRegValue("HKCU", regPath, "user_account_no");
      data["BirthDay"] = $ss.agentcore.dal.registry.GetRegValue("HKCU", regPath, "birth_day");
      data["BirthMonth"] = $ss.agentcore.dal.registry.GetRegValue("HKCU", regPath, "birth_month");
      data["BirthYear"] = $ss.agentcore.dal.registry.GetRegValue("HKCU", regPath, "birth_year");
      data["firstname"] = $ss.agentcore.dal.registry.GetRegValue("HKCU", regPath, "first_name");
      data["lastname"] = $ss.agentcore.dal.registry.GetRegValue("HKCU", regPath, "last_name");
      data["countrycode"] = $ss.agentcore.dal.registry.GetRegValue("HKCU", regPath, "phone_country_id");
      data["areacode"] = $ss.agentcore.dal.registry.GetRegValue("HKCU", regPath, "phone_area_code");
      data["phonenumber"] = $ss.agentcore.dal.registry.GetRegValue("HKCU", regPath, "user_phone_no");
      data["question"] = $ss.agentcore.dal.registry.GetRegValue("HKCU", regPath, "secret_question");
      data["answers"] = $ss.agentcore.dal.registry.GetRegValue("HKCU", regPath, "secret_question_answer");
      data["username"] = $ss.agentcore.dal.registry.GetRegValue("HKCU", regPath, "user_id");
      data["password"] = $ss.agentcore.utils.DecryptHexToStr($ss.agentcore.dal.registry.GetRegValue("HKCU", regPath, "user_password"));
      data["cpassword"] = $ss.agentcore.utils.DecryptHexToStr($ss.agentcore.dal.registry.GetRegValue("HKCU", regPath, "user_password_confirm"));
      
      return data
    }   
  })
})();