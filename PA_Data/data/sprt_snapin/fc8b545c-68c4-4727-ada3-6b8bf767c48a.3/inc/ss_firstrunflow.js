// Namespaces
$ss.snapin = $ss.snapin || {};
$ss.snapin.firstrun = $ss.snapin.firstrun || {};
$ss.snapin.firstrun.dec = $ss.snapin.firstrun.dec || {} 
$ss.snapin.firstrun.navhandler = $ss.snapin.firstrun.navhandler || {};
$ss.snapin.firstrun.renderhelper = $ss.snapin.firstrun.renderhelper || {};
$ss.snapin.firstrun.common = $ss.snapin.firstrun.common || {};

//start of decision functions 
(function(){
  $.extend($ss.snapin.firstrun.dec, {
    sa_fr_FirstRunComplete: function(){
      var regRoot = $ss.agentcore.dal.config.GetProviderID();
      var user = $ss.agentcore.dal.config.ExpandSysMacro("%USER%");
      var prodName = $ss.agentcore.dal.config.ParseMacros("%PRODUCT%");
      var regPath = "Software\\SupportSoft\\ProviderList\\" + regRoot + "\\" + prodName + "\\users\\" + user + "\\ss_config\\firstrun\\";
      var HasBeenSeen = $ss.agentcore.dal.config.GetConfigValue("firstrun", "FirstRunHasBeenSeen")
      if (HasBeenSeen != "true") 
        return false;
      
      var bCompletedAccountInfo = true;
      var bForceAcctInfo = ($ss.agentcore.dal.config.GetConfigValue("firstrun", "force_account_information") === "true");
      if (bForceAcctInfo) {
        var bAccountCollected = ($ss.agentcore.dal.config.GetConfigValue("firstrun", "AccountInformationCollected") === "true");
        if (!bAccountCollected) {
          bCompletedAccountInfo = false;
        }
      }
      var bForceAcctVer = ($ss.agentcore.dal.config.GetConfigValue("firstrun", "force_account_verification") === "true");
      if (bForceAcctVer) {
        var bAcctVerified = ($ss.agentcore.dal.config.GetConfigValue("firstrun", "AccountInformationVerified") === "true");
        if (!bAcctVerified) {
          bCompletedAccountInfo = false;
        }
      }
      
      if (bCompletedAccountInfo) {
        //If the First Run Flow has been seen we skip it and show the home page
        return true;
      }
      else {
        return false;
      }
    },
    fr_LangSelected: function(){
    //This is a carry forward from the 6.5 agent
      return ($ss.agentcore.dal.config.GetConfigValue("firstrun", "allow_skip_language_select_if_already_selected") == "true");
    },
	
    fr_ModemFlowRequired: function(){    
    // Check if Modem flow is required or not
      return $ss.agentcore.dal.config.GetConfigValue("modems", "modem_flow_required", "true");
    },
	
    fr_HaveFlash: function(){
    //Detect flash and direct to the proper landing page
      var iMinimumVersion = $ss.agentcore.dal.config.GetConfigValue("firstrun","flash_minimum_version")*1;
      var version = 0;
      var max_version = iMinimumVersion<20 ? 20 : iMinimumVersion;
      var objFlash;
      for (var i=max_version; i>=0; i--)
        {
        try
          {
            objFlash = new ActiveXObject("ShockwaveFlash.ShockwaveFlash."+i);
            if (objFlash && i >= iMinimumVersion) {
              return true;
            }
           } catch(e) {
             if (i <= iMinimumVersion) {
              return false;
            }
          }
        }
    return false;
    },
    fr_SkipEula: function(){
    //Another carry forward from the 6.5 agent
      if ($ss.agentcore.dal.config.GetConfigValue("firstrun", "allow_skip_eula_if_already_accepted") === "false")
        return false;
      return ($ss.agentcore.dal.config.GetConfigValue("firstrun", "eulaaccepted") === "true");
    },
    
    fr_DiagnoseConnection: function(){
      var ret = $ss.agentcore.dal.databag.GetValue("sa_fr_diagnose_connection");
      return ret;
    },
	fr_AccountInformationStage:function(){
	  return ($ss.agentcore.dal.config.GetConfigValue("firstrun", "force_account_information") == "true");	
	},
    fr_AccountVerificationStage: function(){
      var bAccountInformationVerified = ($ss.agentcore.dal.config.GetConfigValue("firstrun", "AccountInformationVerified") === "true");
      var bForceAccountVerification = ($ss.agentcore.dal.config.GetConfigValue("firstrun", "force_account_verification") === "true");
      var bAllowSkipAccountInfoIfAlreadyVerified = ($ss.agentcore.dal.config.GetConfigValue("firstrun", "allow_skip_account_info_if_already_verified") === "true");

      if (bAllowSkipAccountInfoIfAlreadyVerified && bAccountInformationVerified) {
        bForceAccountVerification = false;
      }

      var bOnline = $ss.agentcore.network.inet.GetConnectionStatus(); //$ss.agentcore.dal.databag.GetValue("sa_fr_connected");
	  switch (true) {

        
		case ( bForceAccountVerification && ! bOnline ) :
          return "mustbeonline";

        case ( ! bOnline ) :
          return "performlater";

        case ( bAccountInformationVerified && bAllowSkipAccountInfoIfAlreadyVerified ) :
          return "verifiedcanskip";

        case ( ! bAccountInformationVerified ) :
        default:
          return "unverified";
      }
    },
    sa_fr_StartSprtCMD:function(){
      return false;
    }
  })
})();


//start of render helper function
(function() {
  $.extend($ss.snapin.firstrun.renderhelper, {
    sta_intro: {
      before: function(params, obj) {
        $ss.snapin.firstrun.common.DisableButton("previous");
      },
      after: function(params, obj) {
        $("input.nextbutton:last").focus();
        $ss.snapin.firstrun.common.EnableButton("next");
        $ss.snapin.firstrun.common.ToggleLinkClass("welcome", true);
        _databag.SetValue("ss_fl_db_onresume_selected_section", "welcome");  // this databag is used to determine which left one will be selected if it starts on reume because of get connect flow

      }
    },
    sta_flashintro: {
      before: function(params, obj) {
        return {
          FlashPath: $ss.getSnapinAbsolutePath("snapin_firstrun")
        };
      },
      after: function(params, obj) {
        $ss.snapin.firstrun.common.ToggleLinkClass("information", true);
        $ss.snapin.firstrun.common.EnableButton("next");
        _databag.SetValue("ss_fl_db_onresume_selected_section", "information");
        $("input.nextbutton:last").focus();
      }
    },
    sta_overview: {
      before: function(params, obj) {
        return {
          AppName: $ss.agentcore.dal.config.GetProviderID()
        };
      },
      after: function(params, obj) {
        $ss.snapin.firstrun.common.ToggleLinkClass("information", true);
        _databag.SetValue("ss_fl_db_onresume_selected_section", "information");
        $ss.snapin.firstrun.common.EnableButton("next");
        $("#savebutton").remove();
        $("#printbutton").remove();
        $("input.nextbutton:last").focus();
      }
    },
    sta_eula: {
      before: function(params, obj) {
        $ss.snapin.firstrun.common.DisableButton("next");
        return {
          SaveimgPath: $ss.getSnapinAbsolutePath("snapin_firstrun")
        };
      },
      after: function(params, obj) {
        $ss.snapin.firstrun.common.ToggleLinkClass("eula", true);
        _databag.SetValue("ss_fl_db_onresume_selected_section", "eula");

        var layoutSkinPath = $ss.GetLayoutSkinPath();
        var html = "<input type='image' alt='" + $ss.agentcore.utils.LocalXLate("snapin_firstrun", "cl_fr_save_button") + "' src='" + layoutSkinPath + "/buttons/" + $ss.GetShellLang() + "/btn_save.gif' name='savebutton' id='savebutton' style='margin-right:3px;'>";
        $("#snapin_firstrun_buttons").prepend(html);
        var html2 = "<input type='image' alt='" + $ss.agentcore.utils.LocalXLate("snapin_firstrun", "cl_fr_print_button") + "' src='" + layoutSkinPath + "/buttons/" + $ss.GetShellLang() + "/btn_print.gif' name='printbutton' id='printbutton' style='margin-right:3px;'>";
        $("#snapin_firstrun_buttons").prepend(html2);
        $("input.nextbutton:last").focus();
      }
    },
    sta_getconnected: {
      after: function(params, obj) {
        var _utils = $ss.agentcore.utils;
        $("input.nextbutton:last").focus();
        $("#savebutton").remove()
        $ss.snapin.firstrun.common.ToggleLinkClass("getconnect", true);
        _databag.SetValue("ss_fl_db_onresume_selected_section", "getconnect");
        $ss.snapin.firstrun.common.DisableButton("next");
        $ss.snapin.firstrun.common.DisableButton("previous");
        var sHost = $ss.agentcore.dal.config.GetConfigValue("getconnect", "HomeRunHostname", "www.supportsoft.com");
        var objNet = new $ss.agentcore.network.network();
        var ret = objNet.BasicConnectionTest(false, sHost, "http://%NetCheck_HomeRunHostname%", "verify.modem.is.not.spoofing.dns.sprt", "NONE", "GET",
                               "%TEMP%\clientui.test", true, true, true, true, true, true, "5000", "5000", "1000", "5", "5", "3");
        if (ret) {
          $('#cl_fr_gc_Header').html(_utils.LocalXLate("snapin_firstrun", "cl_fr_verify_connect_success"));
          $('#cl_fr_gc_Content').html(_utils.LocalXLate("snapin_firstrun", "cl_fr_verify_connect_success_dsc"));
          $ss.agentcore.dal.databag.SetValue("sa_fr_diagnose_connection", "false");
          $ss.agentcore.dal.databag.SetValue("sa_fr_connected", "true");
          $ss.snapin.firstrun.common.EnableButton("next");
          $ss.snapin.firstrun.common.EnableButton("previous");
        } else {
          $('#cl_fr_gc_Header').html(_utils.LocalXLate("snapin_firstrun", "cl_fr_verify_connect_fail"));
          $('#cl_fr_gc_Content').html(_utils.LocalXLate("snapin_firstrun", "cl_fr_verify_connect_fail_dsc"));
          var bAllowFirstRunCompleteWithoutConnection = ($ss.agentcore.dal.config.GetConfigValue("firstrun", "allow_first_run_complete_without_connection") == "true");
          $ss.snapin.firstrun.common.EnableButton("previous");
          if (bAllowFirstRunCompleteWithoutConnection) {
            $('#cl_fr_gc_offlineoption').show('fast');
            $('#cl_fr_gc_pressnext').show('fast');
          } else {
            $('#cl_fr_gc_onlyofflineoption').show('fast');
          }
          $ss.agentcore.dal.databag.SetValue("sa_fr_diagnose_connection", "true");
          $ss.agentcore.dal.databag.SetValue("sa_fr_connected", "false");
        }
      }
    },
    sta_accountinfo: {
      after: function(params, obj) {
        $("input.nextbutton:last").focus();
        $ss.snapin.firstrun.common.ToggleLinkClass("accountinfo", true);
        _databag.SetValue("ss_fl_db_onresume_selected_section", "accountinfo");
        var data = $ss.snapin.firstrun.common.GetFormData()
        if (data) {
          $("#ContactForm").autoFill(data);
        }
        $(document).ready(function() {
          $("#ContactForm").validate(this.Class.prefValidateData);
        });
      }
    },
    home_page_landing: {
      after: function(params, obj) {
        $("input.nextbutton:last").focus();
        $ss.snapin.firstrun.common.ToggleLinkClass("complete", true);
        _databag.RemoveValue("ss_fl_db_onresume_selected_section");
        $ss.agentcore.dal.config.SetUserValue("firstrun", "FirstRunCompleted", "true");
        $ss.agentcore.dal.config.SetMachineValue("firstrun", "FirstRunHasBeenSeen", "true");
      }
    },
    trylater: {
      after: function(params, obj) {
        $("input.nextbutton:last").focus();
        $ss.snapin.firstrun.common.ToggleLinkClass("getconnect", true);
        var layoutSkinPath = $ss.GetLayoutSkinPath();
        var html = "<input type='image' alt='" + $ss.agentcore.utils.LocalXLate("snapin_firstrun", "cl_fr_close_button") + "' src='" + layoutSkinPath + "/buttons/" + $ss.GetShellLang() + "/btn_close.gif' name='closebutton' id='closebutton'>&nbsp;";
        $("#snapin_firstrun_buttons").html(html);
      }
    },
    displayLeftNavigation: {
      prev: function() {
        var currentFRunSection = _databag.GetValue("ss_fl_db_onresume_selected_section");
        switch (currentFRunSection) {
          case "information":
            if (!$ss.snapin.firstrun.dec.fr_LangSelected()) {
              $ss.snapin.firstrun.common.ToggleLinkClass("language", true);
              _databag.SetValue("ss_fl_db_onresume_selected_section", "language");
            }
            return;
          case "accountinfo":
            $ss.snapin.firstrun.common.ToggleLinkClass("getconnect", true);
            _databag.SetValue("ss_fl_db_onresume_selected_section", "getconnect");
            return;
          default:
            return;
        }
      },
      next: function() {
        var currentFRunSection = _databag.GetValue("ss_fl_db_onresume_selected_section");
        switch (currentFRunSection) {
          case "welcome":
            $ss.snapin.firstrun.common.ToggleLinkClass("language", true);
            _databag.SetValue("ss_fl_db_onresume_selected_section", "language");
            return;
          case "sprtcmd":
            $ss.snapin.firstrun.common.ToggleLinkClass("getconnect", true);
            _databag.SetValue("ss_fl_db_onresume_selected_section", "getconnect");
            return;
          default:
            return;
        }
      }
    }
  })

  var _databag = $ss.agentcore.dal.databag;
}
)

();