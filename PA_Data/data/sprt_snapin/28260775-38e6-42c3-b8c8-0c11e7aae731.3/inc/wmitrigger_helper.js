$ss.snapin = $ss.snapin || {};
$ss.snapin.wmitrigger = $ss.snapin.wmitrigger || {};
$ss.snapin.wmitrigger.helper = $ss.snapin.wmitrigger.helper || {};

(function () {
  $.extend($ss.snapin.wmitrigger.helper,
    {
      FetchandParseGUIDXML : function(sCntType, sGuid) {
        try {
          var oXMLDoc, oResXMLDoc, oContent;
          var sContentVersion, sContentPath, sGuidXML, sResXML;

          sContentVersion = this.GetContentVersion(sCntType, sGuid);
          oDictionary.ContentTitle = _content.GetContentProperty(sCntType, sGuid, sContentVersion, "scc_title");
          sContentPath = this.GetContentFolderPath(sCntType, sGuid, sContentVersion);
          sGuidXML = sGuid + "." + sContentVersion + ".xml";

          //Load the GUID xml
          oXMLDoc = this.FetchXML(sContentPath, sGuidXML);
          if (typeof(oXMLDoc) != "object")
            return false;
          oXMLDoc.setProperty("SelectionLanguage","XPath");

          //Parse the GUID xml for solution details
          this.ParseGUIDXML(oXMLDoc, sContentPath);
          return true;
        }
        catch (ex) {
          return false;
        }
      },

      // Get the latest version of the content
      GetContentVersion : function(sCntType, sGuid) {
        try {
          var oContent = _content.GetContentByCid(sCntType, sGuid);
          return oContent.version;
        }
        catch(ex) {
        }
      },

      //Get the folder path of the content
      GetContentFolderPath : function(sCntType, sGuid, sCntVersion) {
        try {
          sContentPath = $ss.GetAttribute('startingpoint') + sCntType + "\\" + sGuid + "." + sCntVersion;
          return sContentPath;
        }
        catch(ex) {
        }
      },

      FetchXML : function(sContentPath, sXML) {
        try {
          var oXML = "";
          var sXMLContentPath;

          sXMLContentPath = sContentPath + "\\" + sXML;
          if (_file.FileExists(sXMLContentPath))
            oXML = _xml.LoadXML(sXMLContentPath, false);
          return oXML;
        }
        catch (ex) {
        }
      },

      ParseGUIDXML : function (oXMLDoc, sContentPath) {
        try {
          var sXPath;
          oDictionary.oSolnDetails = [];
          sXPath = "/sprt/content-set/content/field-set/field";
          var parent = this;

          GetSolutionsListFromXML();
          GetFixTypeFromXML();
          GetSuccessCriteriaFromXML();
          sResXML = GetPayloadDetailsFromXML();
          this.GetMessagesFromXML(oXMLDoc, sContentPath, sResXML);

          //Get the list of solutions to be executed from the solution guid xml
          function GetSolutionsListFromXML () {
            var oAttrSoln = "";
            var arrSolnList = [];
            oAttrSoln = oXMLDoc.selectNodes(sXPath + "[starts-with(@name, 'Solution:::')]");
            if(oAttrSoln.length != 0) {
              for (var iCount=0; iCount < oAttrSoln.length; iCount++)
                arrSolnList[iCount] = oAttrSoln[iCount].text.trim();
                GetSolutionDetails(arrSolnList);
            }
          }

          //Get all the necessary details for the solution and load it in an object
          function GetSolutionDetails (arrSolnList) {
            for(var iCount=0; iCount<arrSolnList.length; iCount++) {
              var solnDetails = [];
              var iLength = oDictionary.oSolnDetails.length;
              var aSolnDetails = arrSolnList[iCount].split("||");
              var sSolnGuid = aSolnDetails[2].trim();
              if (sSolnGuid.length != 36) continue; //not a valid GUID [not configured too], hence skip

              oDictionary.oSolnDetails[iLength] = [];
              var sSolnType = _config.GetValues("wmitrigger", "content_type", aSolnDetails[0]);
              oDictionary.oSolnDetails[iLength].SolutionType = sSolnType;
              oDictionary.oSolnDetails[iLength].ExitCodeLibraryType = aSolnDetails[1];
              oDictionary.oSolnDetails[iLength].SolutionGuid = sSolnGuid;
              oDictionary.oSolnDetails[iLength].KeyValuePair = aSolnDetails[3];

              var sSolnVersion = parent.GetContentVersion(sSolnType, sSolnGuid);
              var sSolnPath = parent.GetContentFolderPath(sSolnType, sSolnGuid, sSolnVersion);
              var sCabPath = sSolnPath + "\\composite.cab";
              var bSolutionExists = _file.FileExists(sCabPath);

              oDictionary.oSolnDetails[iLength].SolutionPath = sSolnPath;
              oDictionary.oSolnDetails[iLength].SolutionCabPath = sCabPath;
              oDictionary.oSolnDetails[iLength].TestResult = null;
              oDictionary.oSolnDetails[iLength].MainResult = null;
              oDictionary.oSolnDetails[iLength].ContentFound = bSolutionExists;
              oDictionary.oSolnDetails[iLength].SolutionVersion = (bSolutionExists) ? sSolnVersion : "0";
              oDictionary.oSolnDetails[iLength].ExecStatus = (bSolutionExists) ? SOLN_RESULT_PROBLEM_NOT_IDENTIFIED : SOLN_RESULT_CONTENT_MISSING;
            }
          }

          function GetFixTypeFromXML() {
            oDictionary.sFixType = GetDataFromXML(sXPath + "[@name='Fix_Type']/value[@name='sccf_field_value_char']");
          }

          function GetPayloadDetailsFromXML() {
            return GetDataFromXML(sXPath + "[@name='Configuration']/value[@name='sccfb_field_value_blob']");
          }

          function GetSuccessCriteriaFromXML() {
            oDictionary.sSuccessCriteria = GetDataFromXML(sXPath + "[@name='SuccessCriteria']/value[@name='sccf_field_value_char']");
          }

          function GetDataFromXML(xPath) {
            var sRetVal = "";
            var oXPathOutPut = oXMLDoc.selectNodes(xPath);
            if(oXPathOutPut.length != 0)
              sRetVal = oXPathOutPut[0].text.trim();
            return sRetVal;
          }
        }
        catch (ex) {
        }
      }, //End of ParseGUIDXML function

      //Get messages from the xml
      GetMessagesFromXML : function(oXML, sContentPath, sResXML) {
        try {
          var sLangCode, sXPath;
          var oResXMLDoc;

          var sNotifyMsg = "", sSussessMsg = "", sFailureMsg = "", sContentTitle = "";
          var notify_msg = "'Notification_Msg'", success_msg = "'Success_Msg'", failure_msg = "'Failure_Msg'", sTitleId = "'title'";

          if (!GetLocalizedMessages(this))
            GetDefaultMessagesFromGuidXML();

          //Get localized messages from resource file
          function GetLocalizedMessages(param) {
            oResXMLDoc = param.FetchXML(sContentPath, sResXML);
            if (typeof(oResXMLDoc) == "object") {
              oResXMLDoc.setProperty("SelectionLanguage","XPath");
              sLangCode = _utils.GetLanguage();
              sXPath = "/sprt/resources/lang[@code='" + sLangCode + "']";

              if (oResXMLDoc.selectNodes(sXPath).length == 0) {
                sXPath = "/sprt/resources/lang[@code='en']";
                if (oResXMLDoc.selectNodes(sXPath).length == 0)
                  return false;
              }
              sXPath += "/res";

              sContentTitle = GetMsgValue(oResXMLDoc, sXPath, "id", sTitleId);      //Get Title for that Language
              sNotifyMsg = GetMsgValue(oResXMLDoc, sXPath, "id", notify_msg);       //Get Notification Message
              sSussessMsg = GetMsgValue(oResXMLDoc, sXPath, "id", success_msg);     //Get Success Message
              sFailureMsg = GetMsgValue(oResXMLDoc, sXPath, "id", failure_msg);     //Get Failure Message

              return (sNotifyMsg != "" && sSussessMsg != "" && sFailureMsg != "")
            }
            return false;
          }

          oDictionary.Messages = [];
          oDictionary.Messages.notify_msg = sNotifyMsg;
          oDictionary.Messages.success_msg = sSussessMsg;
          oDictionary.Messages.failure_msg = sFailureMsg;
          if (sContentTitle.length > 0)
            oDictionary.ContentTitle = sContentTitle;

          //Get default (English) messages from content XML if the localized messages are not available
          function GetDefaultMessagesFromGuidXML() {
            sXPath = "/sprt/content-set/content/field-set/field";
            var sValue = "/value[@name='sccf_field_value_char']";

            sNotifyMsg = GetMsgValue(oXML, sXPath, "name", notify_msg, sValue);       //Get Notification Message
            sSussessMsg = GetMsgValue(oXML, sXPath, "name", success_msg, sValue);     //Get Success Message
            sFailureMsg = GetMsgValue(oXML, sXPath, "name", failure_msg, sValue);     //Get Failure Message
          }

          function GetMsgValue(oXml, xPath, sAttribName, sAttribVal, sSubAttribNode) {
            var retVal = "";
            if(typeof(sSubAttribNode) != "string")
              sSubAttribNode = "";

            var oXPath = oXml.selectNodes(xPath + "[@" + sAttribName + "=" + sAttribVal + "]" + sSubAttribNode);
            if (oXPath.length != 0)
              retVal = oXPath[0].text.trim();
            return retVal;
          }
        }
        catch(ex) {
        }
      }, // GetMessagesFromXML - End

      //Set the script Runner Control
      SetSrControl : function(sGuid, sCabPath, sKeyvaluepair) {
        var providerID = _config.GetContextValue('SdcContext:ProviderId');
        SrCtl = new $ss.agentcore.supportaction(sGuid, providerID, sCabPath);

        var sParams = sKeyvaluepair.split("~");

        for (var iCount = 0; iCount < sParams.length; iCount++) {
            var paname = sParams[iCount].split(":");
            SrCtl.SetParameter(paname[0], paname[1]);
        }
        return SrCtl;
      },

      CheckIfFixIsRequired: function(oSolution) {
        try {
          if (!oSolution.ContentFound) {
            return;
          }
          var sParams, sKeyvaluepair;
          var bIsFixReqd = false;
          var sExitCodeType = oSolution.ExitCodeLibraryType;
          var oSrCtl = this.SetSrControl(oSolution.SolutionGuid, oSolution.SolutionCabPath, oSolution.KeyValuePair);

          oSolution.TestResult = oSrCtl.EvaluateTest();
          if (sExitCodeType == SOLN_LIB_TYPE_ASK) {
            bIsFixReqd = (oSolution.TestResult === ASK_SA_FIX_REQUIRED);
          } else if (sExitCodeType == SOLN_LIB_TYPE_PM || oSolution.SolutionType == SOLN_TYPE_OPTIMIZATION) {
            bIsFixReqd = (oSolution.TestResult === PM_FIX_REQUIRED);
          } else { //SOLN_LIB_TYPE_SA and default SupportAction without any Library type selected
            bIsFixReqd = true;
          }
          if (bIsFixReqd) {
            oSolution.ExecStatus = SOLN_RESULT_NO_ACTION_TAKEN; //reset to no action taken (in case if Fix is not run)
            oFixRequiredSolns[oFixRequiredSolns.length] = oSolution;
          }
        } catch (ex) {
        }
      },

      ExecuteSolutionToFix: function(oFixRequiredSolns) {
        var retValue
        var bSolnExecStatus = null;
        var bStatus = false;

        var bConsiderAllSolns = this.GetExecStatusFromConfig();

        for(var iCounter1=0; iCounter1<oFixRequiredSolns.length; iCounter1++) {
          if (!oFixRequiredSolns[iCounter1].ContentFound) {
            continue; //let other solutions execute, leave overall status untouched
          }
          var sSolnType, sSolnGuid, sCabPath, sKeyvaluepair, sExitCodeType;

          sSolnGuid = oFixRequiredSolns[iCounter1].SolutionGuid;
          sSolnType = oFixRequiredSolns[iCounter1].SolutionType;
          sExitCodeType = oFixRequiredSolns[iCounter1].ExitCodeLibraryType;
          sKeyvaluepair = oFixRequiredSolns[iCounter1].KeyValuePair;
          sCabPath = oFixRequiredSolns[iCounter1].SolutionCabPath;

          var oSrCtl = this.SetSrControl(sSolnGuid, sCabPath, sKeyvaluepair);

          if (sSolnType == SOLN_TYPE_SA && (sExitCodeType == SOLN_LIB_TYPE_SA || sExitCodeType == ""))
            retValue = oSrCtl.srCtl.EvaluateTestAndRun(sCabPath);
          else
            retValue = oSrCtl.Evaluate();
          oFixRequiredSolns[iCounter1].MainResult = retValue;
          var oExit = $ss.agentcore.exitcodes.GetExitCodeDetails(sExitCodeType, retValue);

          switch (sExitCodeType || String(sSolnType)) {
            case SOLN_LIB_TYPE_ASK: if (oExit != null) {
                                        bStatus = (oExit.exittype == "0");
                                      }
                                      else if (ASK_SA_FIX_SUCCESS.indexOf(retValue + ",") != -1)
                                        bStatus = true;
                                      break;
            case SOLN_TYPE_OPTIMIZATION:
            case SOLN_LIB_TYPE_PM:    if (oExit != null) {
                                        bStatus = (oExit.exittype == "0" || oExit.exittype == "2" || oExit.exittype == "3");
                                      }
                                      else if (PM_EXEC_SUCCESS.indexOf(retValue + ",") != -1)
                                        bStatus = true;
                                      break;
            case SOLN_LIB_TYPE_SA:
            default               :   if (oExit != null) {
                                        bStatus = (oExit.exittype == "0");
                                      }
                                      else if(SA_FIX_SUCCESS.indexOf(retValue) != -1)
                                        bStatus = true;
                                      break;
          }
          if (bSolnExecStatus == null)
            bSolnExecStatus = bStatus; //first time load with current status
          oFixRequiredSolns[iCounter1].ExecStatus = bStatus ? SOLN_RESULT_SUCCESS : SOLN_RESULT_FAILURE;
          bSolnExecStatus = (bConsiderAllSolns)?(bSolnExecStatus && bStatus):(bSolnExecStatus || bStatus);
        }
        oDictionary.OverAllStatus = (bSolnExecStatus) ? TRIG_RESULT_SUCCESS : TRIG_RESULT_FAILURE;
        return bSolnExecStatus;
      },

      GetExecStatusFromConfig: function() {
        return (oDictionary.sSuccessCriteria == ALL_SOLUTIONS_SHOULD_SUCCEED);
      },

      GetOverallResultForTrigger: function () {
        return oDictionary.OverAllStatus;
      },

      LogHelperForWmiTrigger: function(oContent, bTrigStart) {
        if (!_repLogger) return;
        var sTrigStatus = (bTrigStart) ? WMI_TRIGGER_START : WMI_TRIGGER_END;
        var sResult = (bTrigStart) ? "" : this.GetOverallResultForTrigger();
        var oLogEntry = _repLogger.CreateLogEntry(oContent.cid, oContent.version, sTrigStatus, 1, "", oContent.title, "", "", sResult);
        _repLogger.Log(oLogEntry, "snapin_wmitrigger");
      },

      LogHelperForSolution: function(oSolution) {
        if (!oSolution) return;
        var oLogEntry = this.CreateLoggerObjectForSolution(oSolution);
        if (!oLogEntry) return;
        _repLogger.Log(oLogEntry, "snapin_wmitrigger");
      },

      GetDataForSolution: function(oSoln) {
        if (!oSoln) return "";
        var retVal = "Scan:<>-->"; //Content missing
        if (oSoln.ContentFound) {
          if (oSoln.TestResult != null) { //Scan done
            retVal = "Scan:<" + oSoln.TestResult + ">-->";
            if (oSoln.MainResult != null) { //Fix done, only if scan done
              retVal += "Fix:<" + oSoln.MainResult + ">";
            }
          }
        }
        return retVal;
      },

      GetResultForSolution: function(oSoln) {
        if (oSoln.SolutionType == SOLN_TYPE_OPTIMIZATION) {
          return (oSoln.MainResult == null) ? oSoln.TestResult : oSoln.MainResult;
        }
          return oSoln.ExecStatus;
      },

      CreateLoggerObjectForSolution: function (oSoln) {
        if (!oSoln) return false;
        var oLogEntry = "";
        try {
          var sSolved = ""; //Survey related entry, don't update
          var sRating = ""; //Survey related entry, don't update
          var sViewed = "1";
          var sVerb = (oSoln.SolutionType == SOLN_TYPE_SA) ? "View" : "optimizationSchedule";
          var sComment = CONTENT_TYPE;
          var sSolnResult = this.GetResultForSolution(oSoln);
          var sData = this.GetDataForSolution(oSoln);
                                              //guidContent, version, verb, viewed, solved, comment, data, rating, result
          oLogEntry = _repLogger.CreateLogEntry(oSoln.SolutionGuid, oSoln.SolutionVersion, sVerb, sViewed, sSolved, sComment, sData, sRating, sSolnResult);
          return oLogEntry;
        } catch (e){
        }
        return false;
      }

    });
}) ();