$ss.snapin = $ss.snapin ||
{};
$ss.snapin.wmitrigger = $ss.snapin.wmitrigger ||
{};
SnapinWmitriggerController = SnapinBaseController.extend('snapin_wmitrigger', {
  LogSolutionsAndTrigEnd: function (oEvent) {
    var arrSolutionList = oDictionary.oSolnDetails;
    for (var i=0; i<arrSolutionList.length; i++) { //log for each solution
      _wmihelper.LogHelperForSolution(arrSolutionList[i]);
    }
    _wmihelper.LogHelperForWmiTrigger(oDictionary.oContentDetails, false); //Log Trigger End
  }
},

{
  index: function (params) {
    try {
      //Subscribe for BCont close
      $ss.agentcore.events.Subscribe("BROADCAST_ON_EXTERNALCLOSE", function(oEvent) { return SnapinWmitriggerController.LogSolutionsAndTrigEnd(oEvent) });
      var oCmdLine;
      var sContentType, sContentGuid, sFixType, sSolutionType, sSolutionGuid, sKeyValuePair;
      var arrSolutionList;
      var bSolnStatus, bContClose = false;

      oCmdLine = _utils.GetObjectFromCmdLine(); //Reading the values from Command line
      sContentType = CONTENT_TYPE;
      sContentGuid = (oCmdLine.guid) ? oCmdLine.guid : "";

      oDictionary.oContentDetails = _content.GetContentByCid(sContentType, sContentGuid);
      _wmihelper.LogHelperForWmiTrigger(oDictionary.oContentDetails, true); //Log Trigger start

      if (sContentGuid == "") {
        bContClose = true;
        _logger.error("WMI trigger solution GUID is not provided");
      } else if (!_wmihelper.FetchandParseGUIDXML(sContentType, sContentGuid)) { //Read GUID.xml to load Solution, Messages & Config data
        bContClose = true;
        _logger.error("WMI trigger: Failed while parsing content xml file");
      } else {
        oDictionary.OverAllStatus = TRIG_RESULT_CONTENTS_MISSING; //all contents might be missing
        arrSolutionList = oDictionary.oSolnDetails;
        sFixType = oDictionary.sFixType;
        for (var iCount=0; iCount<arrSolutionList.length; iCount++) {
          _wmihelper.CheckIfFixIsRequired(arrSolutionList[iCount]);
        }
        if (oFixRequiredSolns.length == 0) {
          _logger.info("Fix is not required. Status Optimal");
          for (var iCount=0; iCount<arrSolutionList.length; iCount++) {
            if (arrSolutionList[iCount].ContentFound) { //atleast one solution found but fix is not required
              oDictionary.OverAllStatus = TRIG_RESULT_PROBLEM_NOT_FOUND; //No Fix required for any of associated solutions
            }
          }
          bContClose = true;
        } else {
          oDictionary.OverAllStatus = TRIG_RESULT_NO_ACTION_TAKEN; //if user closes without running fix
          // noftify the user before executing the solution and display status, User will close the Bcont
          if (sFixType === NOTIFY_TO_FIX) {
            this.renderSnapinView("snapin_wmitrigger", params.toLocation, "\\views\\wmitrigger.html", { oDictionary: oDictionary });
            _utils.ui.ShowBcont();
            $("#idMessage")[0].innerHTML = oDictionary.Messages.notify_msg;
          } else {
            // execute the solution silently
            if (sFixType === FIX_SILENTLY) {
              bSolnStatus = _wmihelper.ExecuteSolutionToFix(oFixRequiredSolns);
              bContClose = true;
            } else {
              bSolnStatus = _wmihelper.ExecuteSolutionToFix(oFixRequiredSolns);
              this.renderSnapinView("snapin_wmitrigger", params.toLocation, "\\views\\wmitrigger.html", { oDictionary: oDictionary });
              $("#idMessage")[0].innerHTML = (bSolnStatus) ? oDictionary.Messages.success_msg : oDictionary.Messages.failure_msg;

              // Show bCont if solution execution is successful; Also Show bCont if the notification type is 'Notify on failure'
              if (bSolnStatus || sFixType === NOTIFY_FIX_FAILURE)
                _utils.ui.ShowBcont();
              else
                bContClose = true;
            }
            _logger.info("Solutions executed.");
          }
        }
      }
    } catch (ex) {
      bContClose = true;
      _logger.error("WMI Trigger:Index - Error No: " + ex.number + " Message: " + ex.message);
    }
    if (bContClose)
      _utils.ui.CloseBcont();
  },

  "#idFixBtn click": function (params) {
    try {
      $('#idLoading').css("display", "block");
      $('#idMessage').css("display", "none");
      $("#idFixBtn").css("display", "none");

      var bSolnStatus = _wmihelper.ExecuteSolutionToFix(oFixRequiredSolns);
      _utils.Sleep(2000);

      $('#idLoading').css("display", "none");
      $('#idMessage').css("display", "block");
      $("#idMessage")[0].innerHTML = (bSolnStatus) ? oDictionary.Messages.success_msg : oDictionary.Messages.failure_msg;
    }
    catch (ex) {
      _utils.ui.CloseBcont();
    }
  }
});

var CONTENT_TYPE_SPRT_WMITRIGGER = "sprt_wmitrigger";

var _config = $ss.agentcore.dal.config;
var _content = $ss.agentcore.dal.content
var _utils = $ss.agentcore.utils;
var _xml = $ss.agentcore.dal.xml;
var _file = $ss.agentcore.dal.file;
var _objContainer = $ss.agentcore.utils.activex.GetObjInstance();
var _wmihelper = $ss.snapin.wmitrigger.helper;
var _logger = $ss.agentcore.log.GetDefaultLogger("snapin_wmitrigger");
var _repLogger = $ss.agentcore.reporting.Reports;

var oDictionary = {};
var oFixRequiredSolns = [];
var sSolutionPath = "";

var SUCCESS_CODE = 0;
var CONTENT_TYPE = $ss.agentcore.dal.config.GetValues("wmitrigger", "content_type", CONTENT_TYPE_SPRT_WMITRIGGER);

var ALL_SOLUTIONS_SHOULD_SUCCEED = "2";

var NOTIFY_TO_FIX = "1";
var FIX_SILENTLY = "2";
var NOTIFY_FIX_SUCCESSFUL = "3";
var NOTIFY_FIX_FAILURE = "4";

var SOLN_TYPE_SA = "sprt_actionlight";
var SOLN_TYPE_OPTIMIZATION = "sprt_optimize";

var SOLN_LIB_TYPE_ASK = "ASK_SA";
var SOLN_LIB_TYPE_SR = "SR";
var SOLN_LIB_TYPE_PM = "PM";
var SOLN_LIB_TYPE_SA = "SA";

var SOLN_RESULT_SUCCESS = 1;
var SOLN_RESULT_FAILURE = 0;
var SOLN_RESULT_NO_ACTION_TAKEN = -990; //User have closed agent without clicking on fix
var SOLN_RESULT_CONTENT_MISSING = -992; //Solution is missing
var SOLN_RESULT_PROBLEM_NOT_IDENTIFIED = -993; //Fix not required

var TRIG_RESULT_SUCCESS = 1;
var TRIG_RESULT_FAILURE = 0;
var TRIG_RESULT_NO_ACTION_TAKEN = -990; //User have closed agent without clicking on fix
var TRIG_RESULT_PROBLEM_NOT_FOUND = -991; //No fix is required for any of associated solutions
var TRIG_RESULT_CONTENTS_MISSING = -992; //all contents of given trigger were missing

var WMI_TRIGGER_START = "wmitrigger_start";
var WMI_TRIGGER_END = "wmitrigger_end";

//Logesh: if we use exit_code.xml hope these constants will be removed
var ASK_SA_FIX_REQUIRED = 0;
var ASK_SA_FIX_ERROR = "-1,-2,-3,-4,-4,-10,-11,-12,-13,-14,-100,-110,-111,-120,-121,1,4,999,";
var ASK_SA_FIX_SUCCESS = "0,2,3,5,";

var SA_FIX_ERROR = "1,999,-1,";
var SA_FIX_SUCCESS = "0";

var PM_FIX_REQUIRED = 0;
var PM_EXEC_ERROR = "-4,-2,-1,";
var PM_EXEC_SUCCESS = "0,2,3,";