SnapinActionwidgetController = SnapinBaseController.extend('snapin_actionwidget',{
  _xml :  $ss.agentcore.dal.xml,
  _snapins:[],
  init: function () {
    var that = this;
var bMac=true;
    that._super();
    var _file = $ss.agentcore.dal.file;
    var filePath  = _file.BuildPath($ss.getSnapinAbsolutePath('snapin_actionwidget'),'\\views\\actionCenterConfig.xml');
    if (_file.FileExists(filePath)){
      var dom  = _xml.LoadXML(filePath,false);
      if (bMac)
{
      var nodes = _xml.GetNodes('/root/snapinInstance','',dom);
}
else
{
      var nodes = _xml.GetNodes('//snapinInstance','',dom);
}
      $.each(nodes,function(index){
          var sname= _xml.GetAttribute(this,'snapinName');
          var snapin = $ss.GetSnapin(sname);
            if(snapin){
              snapin.methodName = _xml.GetAttribute(this,'methodName');
              var params={};
              $.each(this.childNodes, function(){
                  params[_xml.GetAttribute(this,'variableName')]=_xml.GetNodeText(this);
              });
              snapin.params = params;
              that._snapins.push(snapin);
            }

      });
    }
  }
}, {

  index: function (params) {
    this.renderSnapinView("snapin_actionwidget", params.toLocation, "\\views\\actionwidget.html");
    var that = this;
    $.each(that.Class._snapins, function(index){
      var snapin = this;
      snapin.params.toLocation = '#' + snapin.name;
      try{
        that.Class.dispatch(snapin.name,snapin.methodName || 'index',snapin.params)
      }catch(e){ }
    });
  },
  
  ".actionWidgetRefresh click": function(params) {
    this.Class.dispatch('snapin_actionwidget', 'index', params);
  }
});

