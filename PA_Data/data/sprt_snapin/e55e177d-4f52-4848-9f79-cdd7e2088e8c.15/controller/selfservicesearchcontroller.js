$ss.snapins = $ss.snapins || {};
$ss.snapins.selfservicesearch = $ss.snapins.selfservicesearch || {};
$ss.snapins.selfservicesearch.summary = $ss.snapins.selfservicesearch.summary || {};


function renderCell(row, cell, value, columnDef, dataContext) {
  return $ss.snapins.selfservicesearch.summary.RenderCellTemplate(dataContext);
}

var filterresult;
var selectedRowIds;
var Gridresult;
var renderedresult;

SnapinSelfservicesearchController = SnapinBaseController.extend('snapin_selfservicesearch', {
  searchGrid: null,
  controller: null,
  init: function () {
    controller = this;
  },
  
  index: function (params) {
    selectedRowIds=[];
    selectedCat = "";
    bAllSolutions = true;
    renderedresult = [];
    showfeatured = false;
    if(this.Class.searchGrid) delete this.Class.searchGrid;

    this.mapLocalizedCategories();
    if(params.toLocation){
      snapinloc = params.toLocation;
    }
    this.renderSnapinView("snapin_selfservicesearch", snapinloc, "\\views\\selfservicesearch.html");
    if (params.updateNoResult) {
      fromSnapin = "Search";
      oContentcountDict =[0];
      controller.renderToGrid([]);
    }else{
      if(params.SearchResult){
        fromSnapin = "Search";
        renderedresult = params.SearchResult;
      }else{
        renderedresult = NavigationController.GetSearchableContent();
      }
      controller.renderData(renderedresult);
    }

    controller.applyDefaultStyle();
    controller.applyNewStyle("#sFilter_allsolutions");

    $("#sprt_sortby_filter li a").click(function(params){
      selectedCat = params.target.id;
      if (selectedCat) {
        $(this).parents(".dropdown").find('#groupsel').html(_utils.LocalXLate("snapin_selfservicesearch", "snp_search_sortedby_txt", [selectedCat]));
        controller.updateGridData(selectedCat);
        $(this).parents(".dropdown").find('#groupsel_filter').removeClass("filteredBy");
        $(this).parents(".dropdown").find('#groupsel').addClass("sortBy");
      }
    });
  },
  
  ".cell-main click" :function(params){
    var $id = $('#' + params.element.id);
    var ctype = $id.attr("ctype");
    var cid = $id.attr("guid");
    var version = $id.attr("version");
    switch(ctype){
      case "sprt_alertevents":
        params.element.id = cid;
        params.requestPath = "/sa/alert";
        params.requesturl = "/sa/alert?snp_alertguid=" + cid;
	if($.inArray(cid ,selectedRowIds) === -1) selectedRowIds.push(cid);
        params.action = ".navigate click";
        try {
          this.Class.dispatch("navigation", "index", params);
        }catch (ex) {
          this.Class.logger.error("Alert View Click: Error in Dispatching to Navigation Controller %1%", ex.message);
        }
        break;
      case "sprt_articlefaq" :  //articles
      case "sprt_rawdoc" :
      case "sprt_rawdoc_inline" :
        if (!initialized) {
          params.controller = "snapin_solutions";
          params.action = "SolutionFaq click";
          params.event = params.event || {};
          params.element = params.element || {};
          this.Class.dispatch("snapin_solutions", "index", params);
          initialized = true;
        }
        $ss.snapin.solutions.helper.RenderSolutionView(ctype, cid, version);
        break;
      case "sprt_msg":
        params.requesturl = "/sa/messagesviewer";
        params.requestPath = "/sa/messagesviewer";
        params.SelectedId = ([{ "id": cid}]);
        params.action = ".navigate click";
        try {
          this.Class.dispatch("navigation", "index", params);
        }catch (ex) {
          this.Class.logger.error("Message View Click:Error in Dispatching to Navigation Controller %1%", ex.message);
        }
        break;
      case "sprt_protection" :
        params.requesturl = "/sa/protect";
        params.requestPath = "/sa/protect";
        params.SelectedId = ([{ "id": cid}]);
        params.element.id = "lnkRestore";
        params.action = ".navigate click";
        try {
          this.Class.dispatch("navigation", "index", params);
        }catch (ex) {
          this.Class.logger.error("Repair click:Error in Dispatching to Navigation Controller %1%", ex.message);
        }
        break;
      case "sprt_optimize" :
        isAdvancedcontent = $id.attr("isAdvanced");
        params.requesturl = (isAdvancedcontent === "true") ? "/perfman?id=perfman_nav_advanced&isadvanced=perfman_nav_advanced_userselect" : "/perfman?id=perfman_nav_quick&idselect=perfman_nav_quick_userselect";
        params.requestPath = "/perfman";
        params.SelectedId = ([{ "id": cid}]);

	if($.inArray(cid ,selectedRowIds) === -1) selectedRowIds.push(cid);
        params.action = ".navigate click";
        try {
          this.Class.dispatch("navigation", "index", params);
        }catch (ex) {
          this.Class.logger.error("Optimize click:Error in Dispatching to Navigation Controller %1%", ex.message);
        }
        break;
      case "sprt_download":
        params.requesturl = "/dm";
        params.requestPath = "/dm";
        params.SelectedId = ([{ "id": cid}]);
        try {
          this.Class.dispatch("navigation", "index", params);
        }catch (ex) {
          this.Class.logger.error("Install click:Error in Dispatching to Navigation Controller %1%", ex.message);
        }
        break;
      case "sprt_actionlight":
        params.action = ".SolutionAction click";
        params.controller = "snapin_solutions";
        params.callerSnapin = "Search_Snapin";
        params.requesturl = '/sa/soln?snpsoln_template=SolutionView&snpsolution_cid=' + cid + '&snpsolution_version=' + version + '&snpsolution_ctype='+ctype +'&snpsoln_doneurl=/sa/soln';
        params.requestPath ='/sa/soln?snpsoln_template=SolutionView&snpsolution_cid=' + cid + '&snpsolution_version=' + version + '&snpsolution_ctype='+ctype +'&snpsoln_doneurl=/sa/soln';
        try {
          this.Class.dispatch("navigation", "index", params);
        }catch (ex) {
          this.Class.logger.error("Auto-Fix click: Error in Dispatching to Navigation Controller %1%", ex.message);
        }
    }
    
  },
  
  updateSelectedRowsonGrid : function(Griddata){
    $(Griddata).each(function(k,v){
      v.checked = ($.inArray(v.cid , selectedRowIds) > -1 )? true : false;
      if (v.checked) $('#chkbox_' + v.cid).prop("checked", "checked");
      else $('#chkbox_' + v.cid).prop("checked", false);
    });
    Gridresult = Griddata;
  },

  updateGridDataonEvent : function(){
    var allRows = controller.Class.searchGrid.getData();
    controller.updateSelectedRowsonGrid(allRows); 
  },
  
  updateGridData: function (sSortByProperty) {
    var arrfilterCategory;
    var regex = new RegExp('^' + sFeaturestring + '$', 'i');

    var mapsearchresult = $.map(Gridresult, function (v, index) {
      if(typeof v.category === "undefined") v.category = [""]; //if undefined throws error during slice.
      v.localcategory  = v.category.slice();
      var arrCategory = $.isArray(v.localcategory) ? v.localcategory : v.localcategory.split(",");
      arrfilterCategory = [""];
      //remove "featured" category if exists.
      arrfilterCategory = $.grep(arrCategory, function(cat, index){
        if(!regex.test(cat)) return cat; 
      });
      var sValue = arrfilterCategory[0] || ""; //if category is undefined initialize to ""
      arrfilterCategory[0] = aCategory[sValue] || sValue;
      v.localcategory = arrfilterCategory[0]; //only first value is needed for sorting.
      return v;
    });
    Gridresult = mapsearchresult;
    mapsearchresult = null;
    if(sSortByProperty === "Category") sSortByProperty = "localcategory";
    if (Gridresult) Gridresult.sort(this.sortOn(sSortByProperty.toLowerCase()));
    if(regex.test(sSortByProperty)) Gridresult.reverse(); //To display featured contents on Top in Grid
    controller.renderToGrid(Gridresult);
  },

  sortOn: function (property) {
    return function (a, b) {
      var aName = String(a[property]).toLowerCase();
      var bName = String(b[property]).toLowerCase();
      return ((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));
    }
  },

  mapLocalizedCategories: function () {
    aCategory["opt_system_clean"] = _utils.LocalXLate("snapin_selfservicesearch", "opt_system_clean");
    aCategory["opt_browser_clean"] = _utils.LocalXLate("snapin_selfservicesearch", "opt_browser_clean");
    aCategory["opt_system_security"] = _utils.LocalXLate("snapin_selfservicesearch", "opt_system_security");
    aCategory["opt_privacy"] = _utils.LocalXLate("snapin_selfservicesearch", "opt_privacy");
    aCategory["opt_system_perf"] = _utils.LocalXLate("snapin_selfservicesearch", "opt_system_perf");
    aCategory["opt_desktop_clean"] = _utils.LocalXLate("snapin_selfservicesearch", "opt_desktop_clean");
    aCategory["opt_browser_perf"] = _utils.LocalXLate("snapin_selfservicesearch", "opt_browser_perf");
    aCategory["opt_advanced"] = _utils.LocalXLate("snapin_selfservicesearch", "opt_advanced");
    aCategory["email"] = _utils.LocalXLate("snapin_selfservicesearch", "email");
    aCategory["system"] = _utils.LocalXLate("snapin_selfservicesearch", "system");
    aCategory["browser"] = _utils.LocalXLate("snapin_selfservicesearch", "browser");
    aCategory["sys_saction"] = _utils.LocalXLate("snapin_selfservicesearch", "sys_saction");
    aCategory["network"] = _utils.LocalXLate("snapin_selfservicesearch", "network");
    aCategory["featured"] = _utils.LocalXLate("snapin_selfservicesearch", "featured");
  },
  
  renderData: function (data) {
    var filtersearchresult = [];
    var mapsearchresult = [];
    searchData = data;
    if (searchData.length > 0) {
      mapsearchresult = NavigationController.MapSearchableContent(searchData);
      var sAllSearchableData = NavigationController.GetSearchableContent();
      filtersearchresult = $ss.helper.GetFilteredResult(mapsearchresult,sAllSearchableData);
      mapsearchresult = null;
    }
    renderedresult = filtersearchresult;
    filtersearchresult = null;
    //slickGrid requires a valid container so load myGrid before rendering
    controller.renderToGrid(renderedresult);
    //onpageload highlight "All" filter
    controller.applyNewStyle("#sFilter_allsnolutions"); 
  },
  
  ".filterChkbox click": function (params) {
    var chkElements = $("input[name=cbFilters]");
    var count = 0;
    for (var i = 0; i < chkElements.length; i++) {
      var id  = $(chkElements)[i].id;
      var cid = $("#"+id).attr("guid");
      if(chkElements[i].checked){
        count = count + 1;
        if($.inArray(cid ,selectedRowIds) === -1) selectedRowIds.push(cid);
      }else{
        if($.inArray(cid ,selectedRowIds) > -1) selectedRowIds.splice($.inArray(cid ,selectedRowIds),1);
      }
    }
    if (count < 1) $("#buttn_Main").prop("disabled", true);
    else $("#buttn_Main").removeAttr("disabled");
  },

  "#buttn_Main click": function (params) {
    if ($("#buttn_Main").attr("disabled") == "disabled") return;
    if ($("#buttn_Main").attr("disabled") != "undefined") {
      var cid = [];
      var chkElements = $("input[name=cbFilters]");
      var contenttype = chkElements.attr("ctype");
      var allRows = controller.Class.searchGrid.getData();
      $(allRows).each(function(k,v){
         if($.inArray(v.cid , selectedRowIds) > -1 ) cid.push({ "id": v.cid });
      });
      switch (contenttype) {
        case "sprt_download": params.requesturl = "/dm"; break;
        case "sprt_optimize": params.requesturl = "/perfman?id=perfman_nav_quick&idselect=perfman_nav_quick_userselect"; break;
        default: break;
      }
      params.action = ".navigate click";
      params.SelectedId = cid;
      try {
        this.Class.dispatch("navigation", "index", params);
      }catch (ex) {
        this.Class.logger.error("In SearchController: Error in Dispatching to Navigation Controller %1%", ex.message);
      }
    }
  },

  //".aFilter click": function (params) {
   //;
  //".dropdown-menu li span.all_ss_sprt click": function (params) {
  ".dropdown-menu li a.searchfilter click": function (params) {
    fromSnapin ="";
    bAllSolutions = false;
    this.applyDefaultStyle();
    var sElementId = params.element.id;
    var curSearchResult = renderedresult
    var sButtonText = "";
    switch (sElementId) {
      case "allsolutions":
        bAllSolutions = true;
        filterresult = curSearchResult;
        break;
      case "autofix":
        filterresult = $.grep(curSearchResult, function(v, index){
          return (v.ctype == "sprt_actionlight");
        });
        break;
      case "repair":
        filterresult = $.grep(curSearchResult, function(v, index){
          return (v.ctype == "sprt_protection");
        });
        break;
      case "optimize":
        filterresult = $.grep(curSearchResult, function(v, index){
          return (v.ctype == "sprt_optimize");
        });
        if(filterresult.length > 0)
          sButtonText = _utils.LocalXLate("snapin_selfservicesearch", "snp_search_filter_optimize_main_bttn_txt");
        break;
      case "install":
        filterresult = $.grep(curSearchResult, function(v, index){
          return (v.ctype == "sprt_download");
        });
        if(filterresult.length > 0)
          sButtonText = _utils.LocalXLate("snapin_selfservicesearch", "snp_search_filter_install_main_bttn_txt");
        break;
      case "articles":
        filterresult = $.grep(curSearchResult, function(v, index){
          return (v.ctype == "sprt_articlefaq" || v.ctype == "sprt_rawdoc" || v.ctype == "sprt_rawdoc_inline");
        });
        break;
      case "alerts":
        filterresult = $.grep(curSearchResult, function(v, index){
          return (v.ctype == "sprt_alertevents");
        });
        break;
      case "sprtmsg":
        filterresult = $.grep(curSearchResult, function(v, index){
          return (v.ctype == "sprt_msg");
        });
        break;
    }
    //Hide scroll bar when grid result is empty.
    if(filterresult.length == 0) $("#myGrid .slick-viewport").css("overflow-y","hidden");
    else $("#myGrid .slick-viewport").css("overflow-y","auto");
    this.applyNewStyle("#sFilter_" + sElementId);
    if (sButtonText) this.addMainButtonToUI(sButtonText);
    Gridresult = filterresult;
    filterresult = null;
    if (typeof selectedCat != "undefined") controller.updateGridData(selectedCat);
    else this.renderToGrid(Gridresult);

    var dictfilter={
		allsolutions:"All",
		autofix:"Auto-fix",
		repair:"Repair",
		optimize:"Optimize",
		install:"Install",
		articles:"Articles",
		alerts:"Alerts",
		sprtmsg:"Messages"
    };
	
    $('#groupsel_filter').html(_utils.LocalXLate("snapin_selfservicesearch", "snp_search_filteredby_txt", dictfilter[[sElementId]]));
    $('#groupsel').removeClass("sortBy");
    $('#groupsel_filter').addClass("filteredBy");
  },
  
  GetcontentCount :function(ocontents){
    var totalcount = 0;
    if(ocontents){
      Object.keys(ocontents).forEach(function(key){
          totalcount = totalcount + ocontents[key];
      });
    }
    return totalcount;
  },
  
  updateCountforfilters :function(){       
    if (oContentcountDict) {     
      if(oContentcountDict["sprt_actionlight"]){
         $("#autofix").html(_utils.LocalXLate("snapin_selfservicesearch", "snp_search_filters_autofix")+'('  + oContentcountDict["sprt_actionlight"] +')');
      }else{
          $("#liautofix").remove();
      }
      if(oContentcountDict["sprt_alertevents"]){
         $("#alerts").html(_utils.LocalXLate("snapin_selfservicesearch", "snp_search_filters_alerts")+'('  + oContentcountDict["sprt_alertevents"] +')');
      }else{
         $("#lialerts").remove();
      }
       if(oContentcountDict["sprt_articlefaq"]){
         $("#articles").html(_utils.LocalXLate("snapin_selfservicesearch", "snp_search_filters_articles")+'('  + oContentcountDict["sprt_articlefaq"] +')');
      }else{
           $("#liarticles").remove();
      }
       if(oContentcountDict["sprt_download"]){
         $("#install").html(_utils.LocalXLate("snapin_selfservicesearch", "snp_search_filters_install")+'('  + oContentcountDict["sprt_download"] +')');
      }else{
           $("#liinstall").remove();
      }
      if(oContentcountDict["sprt_msg"]){
        var localparams = {};
        MVC.Controller.dispatch("snapin_messageviewer", "indexWidget", localparams);
        if(localparams.DeletedMsgs && localparams.TotalMsg){
          oContentcountDict["sprt_msg"] = localparams.TotalMsg - localparams.DeletedMsgs;           
        } 
         $("#sprtmsg").html(_utils.LocalXLate("snapin_selfservicesearch", "snp_search_filters_messages")+'('  + oContentcountDict["sprt_msg"] +')');
      }else{
          $("#lisprtmsg").remove();
      }
      if(oContentcountDict["sprt_optimize"]){
         $("#optimize").html(_utils.LocalXLate("snapin_selfservicesearch", "snp_search_filters_optimize")+'('  + oContentcountDict["sprt_optimize"] +')');
      }else{
          $("#lioptimize").remove();
      }
      if(oContentcountDict["sprt_protection"]){
         $("#repair").html(_utils.LocalXLate("snapin_selfservicesearch", "snp_search_filters_repair")+'('  + oContentcountDict["sprt_protection"] +')');
      }else{
          $("#lirepair").remove();
      }
      //getting totalcount at the end
      var totalcount = this.GetcontentCount(oContentcountDict);
      $("#allsolutions").html(_utils.LocalXLate("snapin_selfservicesearch", "snp_search_filters_all") + '(' + totalcount + ')');     
      
    }
  },

  renderToGrid: function (searchResults) {
    this.updateCountforfilters(); //update each content type count for filters.
    
    try {
    //To display total number of Search Results per filter.
      $('#searchresults').html(_utils.LocalXLate("snapin_selfservicesearch","snp_search_total_results_txt",[searchResults.length.toString()]));
      if(showfeatured)  $('#featured').html(_utils.LocalXLate("snapin_selfservicesearch","snp_search_featured_txt"));
      
      if (!this.Class.searchGrid) {
        var columns = [{ id: "title", name: "", field: "title", formatter: renderCell}],
        options = {
          rowHeight: 75,
          editable: false,
          enableAddRow: false,
          enableCellNavigation: false,
          forceFitColumns: true,
          forceSyncScrolling: true,
          enableColumnReorder: false
        };
        this.Class.searchGrid = new Slick.Grid("#myGrid", [], columns, options);
        if (typeof this.Class.searchGrid === "object") {
          this.Class.searchGrid.onViewportChanged.subscribe(controller.updateGridDataonEvent); 
          this.Class.searchGrid.onScroll.subscribe(controller.updateGridDataonEvent);
        }
      }
    }catch (e) {
      alert(e);
      return;
    }

    try {
      Gridresult = (typeof searchResults == "string") ? JSON.parse(searchResults) : searchResults;
      if (!Gridresult.length) {
        this.Class.searchGrid.invalidateAllRows();
        if(fromSnapin === "Search"){
          $('.grid-canvas').html(_utils.LocalXLate("snapin_selfservicesearch","snp_selfservicesearch_noresult"));
        }else{
            $('.grid-canvas').html(_utils.LocalXLate("snapin_selfservicesearch","snp_selfservicesearch_nofilterresult"));
        }
        return;
      }
      if (this.Class.searchGrid) {
        this.Class.searchGrid.invalidateAllRows();
        this.Class.searchGrid.setData(Gridresult, true);
        this.Class.searchGrid.render();
        controller.updateSelectedRowsonGrid(Gridresult);
      }
    }catch (e) {
      alert(e);
    }
  },

  "#clickedlink click": function (params) {
    params.event.preventDefault();
    params.requesturl = params.url;
    this.Class.dispatch("snapin_selfservicesearch", "index", params);
  },

  applyDefaultStyle: function(){
    $("#buttn_Main").css('display', 'none');
    $("#buttn_Main").attr('value', '');
    $("#buttn_Main").prop("disabled", true);
    $('.tab-menu-item').toggleClass('tab-menu-item-selected',false);
    $('.tab-menu-item').toggleClass('tab-menu-item',true);
  },
  
  applyNewStyle: function(elemID){
    $(elemID +' .tab-menu-item').toggleClass('tab-menu-item-selected',true);
  },

  addMainButtonToUI: function (bttnTxt) {
    $("#buttn_Main").css('display', 'block');
    $("#buttn_Main").css('height', '20px');
    $("#buttn_Main").css('padding-top', '0px');
    $("#buttn_Main").attr('value', bttnTxt);
  },
  
  "#groupsel_filter click":function(){
    $("#groupsel").css('background','transparent');//removing the other dropdown property
    $("#groupsel").css('color','#1284C7'); //
    $("#groupsel_filter").css('background','#064963');
    $("#groupsel_filter").css('color','white');
  },
  
  "#groupsel click":function(){
    $("#groupsel_filter").css('background','transparent');
    $("#groupsel_filter").css('color','#1284C7');
    $("#groupsel").css('background','#064963');
    $("#groupsel").css('color','white');
  }
});
var bAllSolutions;
var aCategory = [];
var initialized = false;
var fromSnapin = "";
var _logger = $ss.agentcore.log.GetDefaultLogger("selfservicesearch_controller");
var _utils = $ss.agentcore.utils;