/** @namespace Provides System Information functionality */
$ss.snapin = $ss.snapin || {};
$ss.snapin.sysinfo = $ss.snapin.sysinfo || {};
$ss.snapin.sysinfo.helper = $ss.snapin.sysinfo.helper || {};

SnapinSystemInformationController = SnapinBaseController.extend('snapin_system_information', {

  // Class Variables
  oInitialParams: null
  
}, { 

  /**
   *  @function
   *  @description Entry point for this snapin.  Initializes variables and display view.
   *  @param params Optional MVC Params object
   *  @returns none
   */
  index: function(params,sCat){

    var _helper = $ss.snapin.sysinfo.helper;
    //reset the selected category whenever the user visits this page from some other page. In case
    //the user refreshes the page the seleceted category to be honored.
    _helper.SetSelectedCat(sCat);
                                            
    this.Class.oInitialParams = params; //use this.Class to access any member variables/ methods
    _helper.Initialize(params.requestArgs);
    
    //Getting the categories from the helper and passing to the template as local variables
    this.renderSnapinView("snapin_system_information", params.toLocation, "\\views\\sysinfo.html", {
      oCategories: _helper.GetCategoryList()
    });
    $ss.agentcore.utils.ui.FixPng();
    _helper.LoadDefaultSysInfo(this);

    
  },
  
  Load: function() {
    var _helper = $ss.snapin.sysinfo.helper;
    this.LoadSelectedCat($("#snapin_system_information #" + _helper.GetSelectedCat())[0]);
  },
    
  
  indexTools : function(params){
    this.renderSnapinView("snapin_system_information", params.toLocation, "\\views\\sysinfo_tools.html");
  },
  
  /**
   *  @function
   *  @description Handles user click on print button.  Prints scrollarea.
   *  @param params MVC Params object, including button element
   *  @returns none
   */
  "#snapin_sysinfo_print click": function(params){
    var _helper = $ss.snapin.sysinfo.helper;
                 
    _helper.PrintWindow(_helper.getStyleForMedia(), $("#snapin_system_information #scrollarea")[0].innerHTML,
                            $("#snapin_system_information #print_frame")[0].contentWindow);
                            
    $ss.agentcore.utils.ui.BringBcontToFront();                            
  },
  
  /**
   *  @function
   *  @description Handles user click on save button.  Saves stylesheet and scrollarea to html.
   *  @param params MVC Params object, including button element
   *  @returns none
   */
  "#snapin_sysinfo_save click": function(params){
  
    var _helper = $ss.snapin.sysinfo.helper;    
    var sSaveResult = _helper.SaveToDesktop(_helper.getStyleForMedia(),$("#snapin_system_information #scrollarea")[0].innerHTML);
    
    $('.modal-header').html('<b>' + $ss.agentcore.utils.LocalXLate("snapin_system_information","snp_sysinfo") + '</b>' + '<button type="button" class="close" data-dismiss="modal">&times;</button>');
    $(".modal-body").html(sSaveResult); 
  },
  
  /**
   *  @function
   *  @description Handles user click on refresh button
   *  @param params MVC Params object, including button element
   *  @returns none
   */
  "#snapin_sysinfo_refresh click": function(params){
    var _helper = $ss.snapin.sysinfo.helper;
    var sSelectedCat = _helper.GetSelectedCat();
    this.index(this.Class.oInitialParams,sSelectedCat);
  },
  
  /**
   *  @function
   *  @description Closes/hides the popup on the container
   *  @param params MVC Params object, including button element
   *  @returns none
   */
  "#snapin_sysinfo_close click": function(params){
     $("#snapin_system_information #popupaction").css("display","none");
  },
  
  /**
   *  @function
   *  @description Handles user click on a particular category from the left pane
   *  @param params MVC Params object, including the <a> element clicked
   *  @returns none
   */
  ".sinavlinks click": function(params){
    this.LoadSelectedCat(params.element);
  },
  
    applyDefaultStyle: function(){
      $('#sysinfo_filters li a').removeClass('tab-menu-filters-selected');
      $('#sysinfo_filters li a').removeClass('dropdown-filters-selected');
      $('#sysinfo_filters li.filter-li a').toggleClass('tab-menu-filters',true);
    },
 
    applyNewStyle: function(elemID){
      $(elemID +' .tab-menu-filters').toggleClass('tab-menu-filters-selected',true);
      
      if(elemID === "#environmentvariables" || elemID === "#runningprograms" || elemID === "#printerlist") {
       $("#filterDropdown").addClass("dropdown-filters-selected");
      }
    },
  
  LoadSelectedCat:function(SelectElement) {
    $ss.agentcore.utils.ui.StartShellBusy();
    var _helper = $ss.snapin.sysinfo.helper;
    var sSelectedCat = _helper.GetSelectedCat();
    
    this.applyDefaultStyle();
    this.applyNewStyle("#"+ SelectElement.id);

    _helper.CheckAndLoadAdditionalTemplates(SelectElement.id);
    
    $("#snapin_system_information #scrollarea")[0].innerHTML = _helper.getSysInfoByCat(SelectElement.id);    
    $("#snapin_system_information #" + sSelectedCat)[0].className = "sinavlinks";
    SelectElement.className = "sinavlinksCK";
    
    _helper.SetSelectedCat(SelectElement.id);
    //$("#snapin_system_information #catnamedisplay")[0].innerHTML = _helper.GetCategoryName(_helper.GetSelectedCat());
    
    $("#snapin_system_information #scrollarea")[0].scrollTop = 0;
    $ss.agentcore.utils.ui.EndShellBusy();
  }
  
  
});
