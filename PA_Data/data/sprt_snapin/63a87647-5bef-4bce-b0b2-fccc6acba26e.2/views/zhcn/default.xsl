﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
<div>
  <div id="systemsummary" name="系统概要" filename="System Summary" class="alternatelisttable">
    <table border="0" margin="0" width="100%">
     <tr>
      <td class="table-heading">我的电脑和事件摘要</td> 
      <td class="table-heading"></td>
     </tr>
     <tr>
      <td class="odd-row">智能问题账号</td>
      <td class="odd-row">{<xsl:value-of select="//UPLOADDATA[1]/@USERNAME"/>}</td>
     </tr>
     <tr>
      <td class="even-row">机器名</td>
      <td class="even-row"><xsl:value-of select="//INSTANCE[@CLASSNAME='SPRT_Connectivity']/PROPERTY[@NAME='MacID']/VALUE"/></td>
     </tr>
     <tr>
      <td class="odd-row">DNS 名</td>
      <td class="odd-row"><xsl:value-of select="//INSTANCE[@CLASSNAME='SPRT_Connectivity']/PROPERTY[@NAME='HostName']/VALUE"/></td>
     </tr>
     <tr>
      <td class="even-row">域或工作组</td>
      <td class="even-row"><xsl:value-of select="//INSTANCE[@CLASSNAME='WMIComputerSystem']/PROPERTY[@NAME='Domain']/VALUE"/></td>
     </tr>
     <tr>
      <td class="odd-row">操作系统</td>
      <td class="odd-row"><xsl:variable name="displayOS">
            <xsl:value-of select="//INSTANCE[@CLASSNAME='WMIOperatingSystem']/PROPERTY[@NAME='Caption']/VALUE"/>
            <xsl:variable name="CSDVersion" select="//INSTANCE[@CLASSNAME='WMIOperatingSystem']/PROPERTY[@NAME='CSDVersion']/VALUE"/>
            <xsl:if test="normalize-space($CSDVersion) != ''">
              - <xsl:value-of select="$CSDVersion"/>
            </xsl:if>
          </xsl:variable>
          <xsl:value-of select="$displayOS"/>
     </td>
     </tr>
     <tr>
      <td class="even-row">操作系统版本</td>
      <td class="even-row"><xsl:value-of select="//INSTANCE[@CLASSNAME='WMIOperatingSystem']/PROPERTY[@NAME='Version']/VALUE"/></td>
     </tr>
     <tr>
      <td class="odd-row">现场</td>
      <td class="odd-row">
        <xsl:variable name="localeName">
          <xsl:call-template name="getLocaleName">
            <xsl:with-param name="localeID"><xsl:value-of select="//INSTANCE[@CLASSNAME='OperatingSystemInfo']/PROPERTY[@NAME='Locale']/VALUE"/></xsl:with-param>
          </xsl:call-template>
        </xsl:variable>
        <xsl:value-of select="$localeName"/>
      </td>
     </tr>
     <tr>
      <td class="even-row">总物理内存</td>
      <td class="even-row"><xsl:variable name="totalMem" select="//INSTANCE[@CLASSNAME='Win32_LogicalMemoryConfig']/PROPERTY[@NAME='TotalPhysicalMemory']/VALUE"/>
          <xsl:value-of select="$totalMem"/> MB
      </td>
     </tr>
     <tr>
      <td class="odd-row">剩余内存</td>
      <td class="odd-row"><xsl:variable name="percentLoad" select="//INSTANCE[@CLASSNAME='SPRT_AdditionalSysInfo']/PROPERTY[@NAME='MemoryLoad']/VALUE * 0.01"/>
          <xsl:value-of select="round($totalMem - ($percentLoad * $totalMem))"/> MB
      </td>
     </tr>
     <tr>
      <td class="even-row">默认浏览器</td>
      <td class="even-row">
        <xsl:variable name="browser_command" select="//INSTANCE[@CLASSNAME='DefaultBrowser']/PROPERTY[@NAME='BrowserOpenCommand']/VALUE"/>
        <xsl:choose>
          <xsl:when test="contains($browser_command, 'chrome')">谷歌浏览器</xsl:when>
          <xsl:when test="contains($browser_command, 'firefox')">火狐</xsl:when>
          <xsl:when test="contains($browser_command, 'safari')">苹果浏览器</xsl:when>
          <xsl:when test="contains($browser_command, 'opera')">欧朋浏览器</xsl:when>
          <xsl:when test="contains($browser_command, 'appxq0fevzme2pys62n3e0fbqa7peapykr8v')">Microsoft Edge</xsl:when>
          <xsl:when test="contains($browser_command, 'Unknown')">未知</xsl:when>
          <xsl:otherwise>因特网浏览器</xsl:otherwise>
        </xsl:choose>
      </td>
     </tr>
     <tr>
      <td class="odd-row">默认浏览器版本</td>
      <td class="odd-row"><xsl:value-of select="//INSTANCE[@CLASSNAME='DefaultBrowser']/PROPERTY[@NAME='DefaultBrowserVersion']/VALUE"/></td>
     </tr>     
     <tr>
      <td class="even-row">默认邮件客户端</td>
      <td class="even-row">
        <xsl:variable name="Def_HKCU" select="//INSTANCE[@CLASSNAME='Software']/PROPERTY[@NAME='DefaultEmailClientUser']/VALUE" />
        <xsl:choose>
          <xsl:when test="not($Def_HKCU)">
            <xsl:call-template name="check-value">
              <xsl:with-param name="value">
                <xsl:value-of select="//INSTANCE[@CLASSNAME='Software']/PROPERTY[@NAME='DefaultEmailClient']/VALUE"/>
              </xsl:with-param>
            </xsl:call-template>
          </xsl:when>
          <xsl:otherwise>
            <xsl:call-template name="check-value">
              <xsl:with-param name="value"><xsl:value-of select="//INSTANCE[@CLASSNAME='Software']/PROPERTY[@NAME='DefaultEmailClientUser']/VALUE"/></xsl:with-param>
            </xsl:call-template>
          </xsl:otherwise>
        </xsl:choose>
      </td>
     </tr> 
     <tr>
      <td class="odd-row">TCP/IP 地址</td>
      <td class="odd-row">         
        <xsl:variable name="tcpAddress" select="//INSTANCE[@CLASSNAME='SPRT_Connectivity']/PROPERTY[@NAME='TCPIP_Address']/VALUE"/>
        <xsl:choose>
          <xsl:when test ="normalize-space($tcpAddress) != '127.0.0.1'">
         <xsl:call-template name="output-tokens">
              <xsl:with-param name="list">
                <xsl:value-of select="normalize-space($tcpAddress)" />
              </xsl:with-param>
           <xsl:with-param name="delimiter">;</xsl:with-param>
         </xsl:call-template>
          </xsl:when>
          <xsl:otherwise>
            -
          </xsl:otherwise>
        </xsl:choose>
      </td>
     </tr> 
     <tr>
      <td class="even-row">物理地址</td>
      <td class="even-row">
        <xsl:call-template name="output-tokens">
          <xsl:with-param name="list"><xsl:value-of select="//INSTANCE[@CLASSNAME='EthernetInfo']/PROPERTY[@NAME='MACIDList']/VALUE"/></xsl:with-param>
          <xsl:with-param name="delimiter">;</xsl:with-param>
        </xsl:call-template>
      </td>
     </tr>
     <tr>
      <td class="odd-row">产品名称</td>
      <td class="odd-row"><xsl:value-of select="//INSTANCE[@CLASSNAME='ProductInfo']/PROPERTY[@NAME='ProductName']/VALUE"/></td>
     </tr>
     <tr>
      <td class="even-row"><xsl:value-of select="//INSTANCE[@CLASSNAME='ProductInfo']/PROPERTY[@NAME='ProductName']/VALUE"/> 版本</td>
      <td class="even-row"><xsl:value-of select="//INSTANCE[@CLASSNAME='ClientVersion']/PROPERTY[@NAME='ClientBuildVersion']/VALUE"/></td>
     </tr>
    </table>

  </div>
  
  
  
  <div id="windows" name="窗口目录" filename="Windows">
    <table border="0" margin="0" width="100%" class="alternatelisttable">
     <tr>
      <td class="table-heading">我的操作系统</td>
      <td class="table-heading"></td>
     </tr>
     <tr>
      <td class="odd-row">操作系统</td>
      <td class="odd-row"><xsl:value-of select="$displayOS"/></td>
     </tr>
     <tr>
      <td class="even-row">操作系统类型</td>
      <td class="even-row"><xsl:value-of select="//INSTANCE[@CLASSNAME='OperatingSystemInfo']/PROPERTY[@NAME='OSType']/VALUE"/></td>
     </tr>
     <tr>
      <td class="odd-row">服务包</td>
      <td class="odd-row">
        <xsl:call-template name="check-value">
          <xsl:with-param name="value"><xsl:value-of select="//INSTANCE[@CLASSNAME='OperatingSystemInfo']/PROPERTY[@NAME='SvcPack']/VALUE"/></xsl:with-param>
        </xsl:call-template>
      </td>
     </tr>
     <tr>
      <td class="even-row">操作系统版本</td>
      <td class="even-row"><xsl:value-of select="//INSTANCE[@CLASSNAME='WMIOperatingSystem']/PROPERTY[@NAME='Version']/VALUE"/></td>
     </tr>
     <tr>
      <td class="odd-row">交换文件信息</td>
      <td class="odd-row">
        <xsl:variable name="virtualMemFilename" select="normalize-space(//INSTANCE[@CLASSNAME='VirtualMemorySetting']/PROPERTY[@NAME='Filename']/VALUE)"/>
        <xsl:choose>
          <xsl:when test="$virtualMemFilename = ''">分页已禁用</xsl:when>
          <xsl:otherwise><xsl:value-of select="$virtualMemFilename"/> 
                         <xsl:text disable-output-escaping='yes'> </xsl:text>
                         <xsl:value-of select="//INSTANCE[@CLASSNAME='SPRT_AdditionalSysInfo']/PROPERTY[@NAME='PageFileAvailable']/VALUE"/>兆 空
          </xsl:otherwise>
	</xsl:choose>
      </td>
     </tr>
     <tr>
      <td class="even-row">窗口目录</td>
      <td class="even-row"><xsl:value-of select="//INSTANCE[@CLASSNAME='PCH_Sysinfo']/PROPERTY[@NAME='WindowsDirectory']/VALUE"/></td>
     </tr>     
     <tr>
      <td class="odd-row">现场</td>
      <td class="odd-row"><xsl:value-of select="$localeName"/></td>
     </tr>
   </table>
  </div>
  
  
  <div id="myhardware" name="我的硬件" filename="My Hardware" class="alternatelisttable">
    <table border="0" margin="0" width="100%">
     <tr>
      <td class="table-heading">我的中央处理器</td>
      <td class="table-heading"></td>
     </tr>
     <tr>
      <td class="odd-row" width="30%">处理器数量</td>
      <td class="odd-row"><xsl:value-of select="//INSTANCE[@CLASSNAME='SPRT_AdditionalSysInfo']/PROPERTY[@NAME='NumberOfProcessors']/VALUE"/></td>
     </tr>
     <tr>
      <td class="even-row">处理器体系结构</td>
      <td class="even-row"><xsl:value-of select="//INSTANCE[@CLASSNAME='SPRT_AdditionalSysInfo']/PROPERTY[@NAME='ProcessorArchitecture']/VALUE"/></td>
     </tr>
     <tr>
      <td class="odd-row">处理器类型</td>
      <td class="odd-row"><xsl:value-of select="//INSTANCE[@CLASSNAME='SPRT_AdditionalSysInfo']/PROPERTY[@NAME='ProcessorType']/VALUE"/></td>
     </tr>
     <tr>
      <td class="even-row">处理器级别</td>
      <td class="even-row"><xsl:value-of select="//INSTANCE[@CLASSNAME='SPRT_AdditionalSysInfo']/PROPERTY[@NAME='ProcessorLevel']/VALUE"/></td>
     </tr>
     <tr>
      <td class="odd-row">处理器版本</td>
      <td class="odd-row"><xsl:value-of select="//INSTANCE[@CLASSNAME='WMIProcessor']/PROPERTY[@NAME='Version']/VALUE"/></td>
     </tr>
     <tr>
      <td class="even-row">完整的处理器名称</td>
      <td class="even-row"><xsl:value-of select="//INSTANCE[@CLASSNAME='WMIProcessor']/PROPERTY[@NAME='Name']/VALUE"/></td>
     </tr>
    </table>

    <table border="0" margin="0" width="100%" class="alternatelisttable">
     <tr>
      <td class="table-heading">我的内存</td>
      <td class="table-heading"></td>
     </tr>     
     <tr>
      <td class="odd-row" width="30%">总物理内存</td>
      <td class="odd-row"><xsl:value-of select="$totalMem"/> MB</td>
     </tr>
     <tr>
      <td class="even-row">剩余内存</td>
      <td class="even-row"><xsl:value-of select="round($totalMem - ($percentLoad * $totalMem))"/> MB</td>
     </tr>
     <tr>
      <td class="odd-row">最大页面文件大小</td>
      <td class="odd-row">
        <xsl:variable name="virtualMemMaxSize" select="normalize-space(//INSTANCE[@CLASSNAME='VirtualMemorySetting']/PROPERTY[@NAME='MaxSize']/VALUE)"/>
        <xsl:choose>
          <xsl:when test="$virtualMemMaxSize = '0'">系统管理</xsl:when>
          <xsl:when test="$virtualMemMaxSize = ''">分页已禁用</xsl:when>
          <xsl:otherwise><xsl:value-of select="$virtualMemMaxSize"/> MB</xsl:otherwise>
	</xsl:choose>
      </td>
     </tr>
     <tr>
      <td class="even-row">初始页面文件大小</td>
      <td class="even-row">
        <xsl:variable name="virtualMemInitSize" select="normalize-space(//INSTANCE[@CLASSNAME='VirtualMemorySetting']/PROPERTY[@NAME='InitSize']/VALUE)"/>
        <xsl:choose>
          <xsl:when test="$virtualMemInitSize = 0">系统管理</xsl:when>
          <xsl:when test="$virtualMemInitSize = ''">分页已禁用</xsl:when>
          <xsl:otherwise><xsl:value-of select="$virtualMemInitSize"/> MB</xsl:otherwise>
	</xsl:choose>      
      </td>
     </tr>
     <tr>
      <td class="odd-row">可用虚拟内存</td>
      <td class="odd-row"><xsl:value-of select="round(//INSTANCE[@CLASSNAME='WMIOperatingSystem']/PROPERTY[@NAME='FreeVirtualMemory']/VALUE div 1024)"/> MB</td>
     </tr>
     <tr>
      <td class="even-row">总虚拟内存</td>
      <td class="even-row"><xsl:value-of select="round(//INSTANCE[@CLASSNAME='WMIOperatingSystem']/PROPERTY[@NAME='TotalVirtualMemorySize']/VALUE div 1024)"/> MB</td>
     </tr> 
     <tr>
      <td class="odd-row">Windows目录</td>
      <td class="odd-row"><xsl:value-of select="//INSTANCE[@CLASSNAME='PCH_Sysinfo']/PROPERTY[@NAME='WindowsDirectory']/VALUE"/></td>
     </tr> 
    </table>   

    <table border="0" margin="0" width="100%" class="alternatelisttable">
     <tr>
      <td class="table-heading">我的磁盘驱动</td>
      <td class="table-heading"></td>
      <td class="table-heading"></td>
     </tr>    
     <tr>
      <td class="table-heading">硬盘</td>
      <td class="table-heading">硬盘大小</td>
      <td class="table-heading">可用的</td>
     </tr>
      <xsl:for-each select="//INSTANCE[@CLASSNAME='LogicalDiskInfo']">
        <tr>
        
        <td>
          <xsl:if test="position() mod 2 = 1"><xsl:attribute name="class">odd-row</xsl:attribute></xsl:if>
          <xsl:if test="position() mod 2 = 0"><xsl:attribute name="class">even-row</xsl:attribute></xsl:if>
          <xsl:value-of select="PROPERTY[@NAME='DriveName']/VALUE"/>
        </td>
        <xsl:variable name="driveSize" select="PROPERTY[@NAME='TotalCapacity']/VALUE"/>
        <td>
          <xsl:if test="position() mod 2 = 1"><xsl:attribute name="class">odd-row</xsl:attribute></xsl:if>
          <xsl:if test="position() mod 2 = 0"><xsl:attribute name="class">even-row</xsl:attribute></xsl:if>
          <xsl:choose>
            <xsl:when test="$driveSize &gt; 1048576">
              <xsl:value-of select="round($driveSize div 1048576)"/> GB
            </xsl:when>
            <xsl:when test="$driveSize &gt; 1024">
              <xsl:value-of select="round($driveSize div 1024)"/> MB
            </xsl:when>        
            <xsl:otherwise>
              <xsl:value-of select="round($driveSize)"/> KB
            </xsl:otherwise>
          </xsl:choose>
        </td>
        <td>
          <xsl:if test="position() mod 2 = 1"><xsl:attribute name="class">odd-row</xsl:attribute></xsl:if>
          <xsl:if test="position() mod 2 = 0"><xsl:attribute name="class">even-row</xsl:attribute></xsl:if>
          <xsl:value-of select="round(PROPERTY[@NAME='TotalFreeSpace']/VALUE div $driveSize * 100)"/>%
        </td>
        </tr>
      </xsl:for-each>     
     </table>


  </div>


      <div id="network" name="网络信息" filename="Network Information" class="alternatelisttable">
        <table border="0" margin="0" width="100%">
          <tr>
            <td class="table-heading">我的网络</td>
            <td class="table-heading"></td>
          </tr>
          <tr>
            <td class="odd-row" width="40%">计算机名</td>
            <td class="odd-row">
              <xsl:value-of select="//INSTANCE[@CLASSNAME='SPRT_Connectivity']/PROPERTY[@NAME='NetBIOSName']/VALUE" />
            </td>
          </tr>
          <tr>
            <td class="even-row">DNS 名</td>
            <td class="even-row">
              <xsl:value-of select="//INSTANCE[@CLASSNAME='SPRT_Connectivity']/PROPERTY[@NAME='HostName']/VALUE" />
            </td>
          </tr>
          <tr>
            <td class="odd-row">域名</td>
            <td class="odd-row">
              <xsl:variable name="domainRole" select="//INSTANCE[@CLASSNAME='WMIComputerSystem']/PROPERTY[@NAME='DomainRole']/VALUE"/>
              <xsl:choose>
                <xsl:when test="$domainRole != 0">
                    <xsl:value-of select="//INSTANCE[@CLASSNAME='WMIComputerSystem']/PROPERTY[@NAME='Domain']/VALUE" />
                </xsl:when>
                <xsl:otherwise>-</xsl:otherwise>
              </xsl:choose>
            </td>
          </tr>
          <tr>
            <td class="even-row">工作组</td>
            <td class="even-row">
              <xsl:choose>
                <xsl:when test="$domainRole = 0">
                    <xsl:value-of select="//INSTANCE[@CLASSNAME='WMIComputerSystem']/PROPERTY[@NAME='Domain']/VALUE" />
                </xsl:when>
                <xsl:otherwise>-</xsl:otherwise>
              </xsl:choose>             
            </td>
          </tr>
          <tr>
            <td class="odd-row">用户名</td>
            <td class="odd-row">
              <xsl:value-of select="//INSTANCE[@CLASSNAME='SPRT_Connectivity']/PROPERTY[@NAME='UserName']/VALUE" />
            </td>
          </tr>
          <tr>
            <td class="even-row">DNS服务器</td>
            <td class="even-row">
              <xsl:call-template name="output-tokens">
                <xsl:with-param name="list"><xsl:value-of select="//INSTANCE[@CLASSNAME='EthernetInfo']/PROPERTY[@NAME='DnsUniqueServerList']/VALUE" /></xsl:with-param>
                <xsl:with-param name="delimiter">;</xsl:with-param>
              </xsl:call-template>            
            </td>
          </tr>
          <tr>
            <td class="odd-row">TCP/IP 地址</td>
            <td class="odd-row">
                <xsl:choose>
                  <xsl:when test ="normalize-space($tcpAddress) != '127.0.0.1'">
                    <xsl:call-template name="output-tokens">
                    <xsl:with-param name="list"><xsl:value-of select="normalize-space($tcpAddress)" /></xsl:with-param>
                      <xsl:with-param name="delimiter">;</xsl:with-param>
                    </xsl:call-template>
                  </xsl:when>
                  <xsl:otherwise>
                    -
                  </xsl:otherwise>
                </xsl:choose>
            </td>
          </tr>

          <tr>
            <td class="table-heading">代理配置</td>
            <td class="table-heading"></td>
          </tr>
          <tr>
            <td class="odd-row" width="40%">代理启用</td>
            <td class="odd-row">
              <xsl:variable name="ProxyEnabled" select="//INSTANCE[@CLASSNAME='ProxyInfo']/PROPERTY[@NAME='ProxyEnabled']/VALUE" />
              <xsl:choose>
	        <xsl:when test="$ProxyEnabled = 1">
            正确
          </xsl:when>
	        <xsl:otherwise>
            错误
          </xsl:otherwise>
	      </xsl:choose>
            </td>
          </tr>
          <tr>
            <td class="even-row">代理地址</td>
            <td class="even-row">
              <xsl:call-template name="check-value">
                <xsl:with-param name="value"><xsl:value-of select="//INSTANCE[@CLASSNAME='ProxyInfo']/PROPERTY[@NAME='ProxyAddress']/VALUE" /></xsl:with-param>
              </xsl:call-template>
            </td>
          </tr>
          <tr>
            <td class="odd-row">代理自动配置地址</td>
            <td class="odd-row">
              <xsl:call-template name="check-value">
                <xsl:with-param name="value"><xsl:value-of select="//INSTANCE[@CLASSNAME='ProxyInfo']/PROPERTY[@NAME='ProxyAutoConfigAddress']/VALUE" /></xsl:with-param>
              </xsl:call-template>
            </td>
          </tr>
          <tr>
            <td class="even-row">绕过代理本地地址</td>
            <td class="even-row">
              <xsl:call-template name="check-value">
                <xsl:with-param name="value"><xsl:value-of select="//INSTANCE[@CLASSNAME='ProxyInfo']/PROPERTY[@NAME='ProxyBypassLocal']/VALUE" /></xsl:with-param>
              </xsl:call-template>
            </td>
          </tr>
          <tr>
            <td class="odd-row">本地地址绕过</td>
            <td class="odd-row">
              <xsl:call-template name="output-tokens">
                <xsl:with-param name="list"><xsl:value-of select="//INSTANCE[@CLASSNAME='ProxyInfo']/PROPERTY[@NAME='ProxyBypassLocalAddresses']/VALUE" /></xsl:with-param>
                <xsl:with-param name="delimiter">;</xsl:with-param>
              </xsl:call-template> 
            </td>
          </tr>
        </table>      
      
        <table border="0" margin="0" width="100%" class="alternatelisttable">
          <tr>
            <td class="table-heading">网络适配器</td>
            <td class="table-heading"></td>
          </tr>
          <xsl:for-each select="//INSTANCE[@CLASSNAME='EthernetInfo']/PROPERTY[starts-with(@NAME,'Counter')]">
            <xsl:variable name="counter" select="./VALUE" />
            
            <tr>
              <td colspan="2">
                <strong><xsl:value-of select="../PROPERTY[@NAME=concat('Description',$counter)]" /></strong>
              </td>
            </tr>
            <tr>
              <td class="odd-row" width="40%">类型</td>
              <td class="odd-row">
                <xsl:call-template name="check-value">
                  <xsl:with-param name="value"><xsl:value-of select="../PROPERTY[@NAME=concat('Type',$counter)]" /></xsl:with-param>
                </xsl:call-template>
              </td>
            </tr>
            <tr>
              <td class="even-row">连接状态</td>
              <td class="even-row">
                <xsl:choose>
	          <xsl:when test="normalize-space(../PROPERTY[@NAME=concat('ConnectStatus',$counter)])='Connected'">
              已连接
            </xsl:when>
	          <xsl:otherwise>
              已断开
            s</xsl:otherwise>
	        </xsl:choose>
              </td>
            </tr>            
            <tr>
              <td class="odd-row">DHCP已启用</td>
              <td class="odd-row">
                <xsl:call-template name="check-value">
                  <xsl:with-param name="value"><xsl:value-of select="../PROPERTY[@NAME=concat('DhcpEnabled',$counter)]" /></xsl:with-param>
                </xsl:call-template>
              </td>
            </tr>            
            <tr>
              <td class="even-row">IP地址</td>
              <td class="even-row">
                <xsl:call-template name="check-value">
                  <xsl:with-param name="value"><xsl:value-of select="../PROPERTY[@NAME=concat('IPAddress',$counter)]" /></xsl:with-param>
                </xsl:call-template>
              </td>
            </tr>
            <tr>
              <td class="odd-row">自动配置已启用</td>
              <td class="odd-row">
                <xsl:call-template name="check-value">
                  <xsl:with-param name="value"><xsl:value-of select="../PROPERTY[@NAME=concat('AutoConfigEnabled',$counter)]" /></xsl:with-param>
                </xsl:call-template>
              </td>
            </tr>
            <tr>
              <td class="even-row">子网掩码</td>
              <td class="even-row">
                <xsl:call-template name="check-value">
                  <xsl:with-param name="value"><xsl:value-of select="../PROPERTY[@NAME=concat('SubnetMask',$counter)]" /></xsl:with-param>
                </xsl:call-template>
              </td>
            </tr>
            <tr>
              <td class="odd-row">默认网关</td>
              <td class="odd-row">
                <xsl:call-template name="check-value">
                  <xsl:with-param name="value"><xsl:value-of select="../PROPERTY[@NAME=concat('DefaultGateway',$counter)]" /></xsl:with-param>
                </xsl:call-template>
              </td>
            </tr>
            <tr>
              <td class="even-row">DNS服务器</td>
              <td class="even-row">
                <xsl:call-template name="output-tokens">
                  <xsl:with-param name="list"><xsl:value-of select="../PROPERTY[@NAME=concat('DnsServerList',$counter)]" /></xsl:with-param>
                  <xsl:with-param name="delimiter">;</xsl:with-param>
                </xsl:call-template>
              </td>
            </tr>
            <tr>
              <td class="odd-row">WINS已启用</td>
              <td class="odd-row">
                <xsl:call-template name="check-value">
                  <xsl:with-param name="value"><xsl:value-of select="../PROPERTY[@NAME=concat('HaveWins',$counter)]" /></xsl:with-param>
                </xsl:call-template>
              </td>
            </tr>
            <tr>
              <td class="even-row">WINS服务器</td>
              <td class="even-row">
                <xsl:call-template name="check-value">
                  <xsl:with-param name="value"><xsl:value-of select="../PROPERTY[@NAME=concat('PrimaryWinsServer',$counter)]" /></xsl:with-param>
                </xsl:call-template>
                
                <xsl:if test="normalize-space(../PROPERTY[@NAME=concat('SecondaryWinsServer',$counter)]) != ''">
                  <br/><xsl:value-of select="../PROPERTY[@NAME=concat('SecondaryWinsServer',$counter)]" />
                </xsl:if>
                
              </td>
            </tr>
            <tr>
              <td class="odd-row">广播地址</td>
              <td class="odd-row">
                <xsl:call-template name="check-value">
                  <xsl:with-param name="value"><xsl:value-of select="../PROPERTY[@NAME=concat('BroadcastAddress',$counter)]" /></xsl:with-param>
                </xsl:call-template>
              </td>
            </tr>
            <tr>
              <td class="even-row">DHCP服务器</td>
              <td class="even-row">
                <xsl:call-template name="check-value">
                  <xsl:with-param name="value"><xsl:value-of select="../PROPERTY[@NAME=concat('DhcpServer',$counter)]"/></xsl:with-param>
                </xsl:call-template>
              </td>
            </tr>
            <tr>
              <td class="odd-row">子网</td>
              <td class="odd-row">
                <xsl:call-template name="check-value">
                  <xsl:with-param name="value"><xsl:value-of select="../PROPERTY[@NAME=concat('Subnet',$counter)]" /></xsl:with-param>
                </xsl:call-template>
              </td>
            </tr>
            <tr>
              <td class="even-row">MAC地址</td>
              <td class="even-row">
                <xsl:call-template name="check-value">
                  <xsl:with-param name="value"><xsl:value-of select="../PROPERTY[@NAME=concat('PhysicalAddress',$counter)]" /></xsl:with-param>
                </xsl:call-template>
              </td>
            </tr>
            <tr>
              <td class="odd-row">服务名称</td>
              <td class="odd-row">
                <xsl:call-template name="check-value">
                  <xsl:with-param name="value"><xsl:value-of select="../PROPERTY[@NAME=concat('ServiceName',$counter)]" /></xsl:with-param>
                </xsl:call-template>
              </td>
            </tr>
          </xsl:for-each>
        </table>
        
        
        <table border="0" margin="0" width="100%" class="alternatelisttable">
          <tr>
            <td class="table-heading">无线信息</td>
            <td class="table-heading"></td>
          </tr>
          <tr>
            <td class="odd-row" width="40%">无线连接名称</td>
            <td class="odd-row">
              <xsl:call-template name="check-value">
                <xsl:with-param name="value"><xsl:value-of select="//INSTANCE[@CLASSNAME='EthernetInfo']/PROPERTY[@NAME='WirelessConnectionName']/VALUE" /></xsl:with-param>
              </xsl:call-template>
            </td>
          </tr>
          <tr>
            <td class="even-row">连接状态</td>
            <td class="even-row">
              <xsl:choose>
	        <xsl:when test="normalize-space(//INSTANCE[@CLASSNAME='EthernetInfo']/PROPERTY[@NAME='WirelessConnectionStatus']/VALUE)='Connected'">
            已连接
          </xsl:when>
	        <xsl:otherwise>
            已断开
          </xsl:otherwise>
	      </xsl:choose>            
            </td>
          </tr>
          <tr>
            <td class="odd-row">SSID</td>
            <td class="odd-row">
              <xsl:call-template name="check-value">
                <xsl:with-param name="value"><xsl:value-of select="//INSTANCE[@CLASSNAME='EthernetInfo']/PROPERTY[@NAME='WirelessSSID']/VALUE" /></xsl:with-param>
              </xsl:call-template>
            </td>
          </tr>
          <tr>
            <td class="even-row">BSSID</td>
            <td class="even-row">
              <xsl:call-template name="check-value">              
                <xsl:with-param name="value"><xsl:value-of select="//INSTANCE[@CLASSNAME='EthernetInfo']/PROPERTY[@NAME='WirelessBSSID']/VALUE" /></xsl:with-param>
              </xsl:call-template>              
            </td>
          </tr>
          <tr>
            <td class="odd-row">身份验证模式</td>
            <td class="odd-row">
              <xsl:variable name="WirelessAuthMode" select="normalize-space(//INSTANCE[@CLASSNAME='EthernetInfo']/PROPERTY[@NAME='WirelessAuthMode']/VALUE)" />
              <xsl:choose>
                <xsl:when test="$WirelessAuthMode = ''">-</xsl:when>
	        <xsl:when test="$WirelessAuthMode = 0">打开</xsl:when>
	        <xsl:when test="$WirelessAuthMode = 1">共享</xsl:when>
	        <xsl:when test="$WirelessAuthMode = 2">自动切换</xsl:when>
	        <xsl:when test="$WirelessAuthMode = 3">无线网络安全接入</xsl:when>
	        <xsl:when test="$WirelessAuthMode = 4">无线网络安全接入-预共享密钥</xsl:when>
	        <xsl:when test="$WirelessAuthMode = 5">无线网络安全接入-无</xsl:when>
	        <xsl:when test="$WirelessAuthMode = 6">无线网络安全接入2</xsl:when>
	        <xsl:when test="$WirelessAuthMode = 7">无线网络安全接入2-预共享密钥</xsl:when>
	        <xsl:otherwise>未知或不适用</xsl:otherwise>
	      </xsl:choose>              
              
            </td>
          </tr>
          <tr>
            <td class="even-row">加密类型</td>
            <td class="even-row">
              <xsl:variable name="WirelessEncType" select="normalize-space(//INSTANCE[@CLASSNAME='EthernetInfo']/PROPERTY[@NAME='WirelessEncryptType']/VALUE)" />
              <xsl:choose>
                <xsl:when test="$WirelessEncType = ''">-</xsl:when>
	        <xsl:when test="$WirelessEncType = 0">有线等效私密性</xsl:when>
	        <xsl:when test="$WirelessEncType = 1">禁用</xsl:when>
	        <xsl:when test="$WirelessEncType = 2">禁用</xsl:when>
	        <xsl:when test="$WirelessEncType = 3">不支持</xsl:when>
	        <xsl:when test="$WirelessEncType = 4">临时密钥完整性协议</xsl:when>
	        <xsl:when test="$WirelessEncType = 5">临时密钥完整性协议</xsl:when>
	        <xsl:when test="$WirelessEncType = 6">AES</xsl:when>
	        <xsl:when test="$WirelessEncType = 7">AES</xsl:when>
	        <xsl:otherwise>未知或不适用</xsl:otherwise>
	      </xsl:choose>              
            </td>
          </tr>
          <tr>
            <td class="odd-row">频道</td>
            <td class="odd-row">
              <xsl:call-template name="check-value">
                <xsl:with-param name="value"><xsl:value-of select="//INSTANCE[@CLASSNAME='EthernetInfo']/PROPERTY[@NAME='WirelessChannel']/VALUE" /></xsl:with-param>
              </xsl:call-template>
            </td>
          </tr>
          <tr>
            <td class="even-row">信号强度</td>
            <td class="even-row">
              <xsl:variable name="WirelessSignalStrength" select="normalize-space(//INSTANCE[@CLASSNAME='EthernetInfo']/PROPERTY[@NAME='WirelessSignalStrength']/VALUE)" />
              <xsl:choose>
                <xsl:when test="$WirelessSignalStrength = ''">-</xsl:when>
	        <xsl:when test="$WirelessSignalStrength &lt; -90">无信号</xsl:when>
	        <xsl:when test="$WirelessSignalStrength &lt; -81">非常低</xsl:when>
	        <xsl:when test="$WirelessSignalStrength &lt; -71">低</xsl:when>
	        <xsl:when test="$WirelessSignalStrength &lt; -67">好</xsl:when>
	        <xsl:when test="$WirelessSignalStrength &lt; -57">非常低</xsl:when>
	        <xsl:when test="$WirelessSignalStrength &lt; 0">极佳</xsl:when>
	        <xsl:otherwise>未知或不适用</xsl:otherwise>
	      </xsl:choose>              
            </td>
          </tr>
          <tr>
            <td class="odd-row">连接速度</td>
            <td class="odd-row">
              <xsl:call-template name="check-value">
                <xsl:with-param name="value"><xsl:value-of select="//INSTANCE[@CLASSNAME='EthernetInfo']/PROPERTY[@NAME='WirelessLinkSpeed']/VALUE" /></xsl:with-param>
                <xsl:with-param name="postfix"> Mbit/s</xsl:with-param>
              </xsl:call-template>
            </td>
          </tr>
          <tr>
            <td class="even-row"> 支持速率</td>
            <td class="even-row">
              <xsl:call-template name="check-value">
                <xsl:with-param name="value"><xsl:value-of select="//INSTANCE[@CLASSNAME='EthernetInfo']/PROPERTY[@NAME='WirelessSupportedRates']/VALUE" /></xsl:with-param>
                <xsl:with-param name="postfix"> Mbit/s</xsl:with-param>
              </xsl:call-template>
            </td>
          </tr>
          <tr>
            <td class="odd-row">基础建设模式</td>
            <td class="odd-row">
              <xsl:variable name="WirelessInfrastructure" select="normalize-space(//INSTANCE[@CLASSNAME='EthernetInfo']/PROPERTY[@NAME='WirelessInfrastructure']/VALUE)" />
              <xsl:choose>
                <xsl:when test="$WirelessInfrastructure = ''">-</xsl:when>
	        <xsl:when test="$WirelessInfrastructure = 0">自组网</xsl:when>
	        <xsl:when test="$WirelessInfrastructure = 1">基础设施</xsl:when>
	        <xsl:when test="$WirelessInfrastructure = 2">自动</xsl:when>
	        <xsl:otherwise>未知或不适用</xsl:otherwise>
	      </xsl:choose>              
            </td>
          </tr>          
        </table>        

        <table border="0" margin="0" width="100%" class="alternatelisttable">
          <tr>
            <td class="table-heading">调制解调器信息</td>
            <td class="table-heading"></td>
          </tr>
          <tr>
            <td class="odd-row" width="40%">名称</td>
            <td class="odd-row">
              <xsl:call-template name="check-value">
                <xsl:with-param name="value"><xsl:value-of select="//INSTANCE[@CLASSNAME='ModemInfo']/PROPERTY[@NAME='modem_name']/VALUE" /></xsl:with-param>
              </xsl:call-template>
            </td>
          </tr>
          <tr>
            <td class="even-row">代码</td>
            <td class="even-row">
              <xsl:call-template name="check-value">
                <xsl:with-param name="value">
                  <xsl:if test="not(starts-with(//INSTANCE[@CLASSNAME='ModemInfo']/PROPERTY[@NAME='modem_code']/VALUE,'null_modem'))" >
                  <xsl:value-of select="//INSTANCE[@CLASSNAME='ModemInfo']/PROPERTY[@NAME='modem_code']/VALUE" />
                  </xsl:if>
                </xsl:with-param>
              </xsl:call-template>
            </td>
          </tr>
          <tr>
            <td class="odd-row">调制解调器类型</td>
            <td class="odd-row">
              <xsl:call-template name="check-value">
                <xsl:with-param name="value"><xsl:value-of select="//INSTANCE[@CLASSNAME='ModemInfo']/PROPERTY[@NAME='modem_type']/VALUE" /></xsl:with-param>
              </xsl:call-template>
            </td>
          </tr>
          <tr>
            <td class="even-row">连接类型</td>
            <td class="even-row">
              <xsl:call-template name="check-value">
                <xsl:with-param name="value"><xsl:value-of select="//INSTANCE[@CLASSNAME='ModemInfo']/PROPERTY[@NAME='modem_conn_type']/VALUE" /></xsl:with-param>
              </xsl:call-template>
            </td>
          </tr>
        </table>
        
      </div>      
      
      
  <div id="environmentvariables" name="环境变量" filename="Environment Variables">
    <xsl:variable name="vLowercaseChars_CONST" select="'abcdefghijklmnopqrstuvwxyz'"/> 
    <xsl:variable name="vUppercaseChars_CONST" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'"/> 
    <table border="0" margin="0" width="100%" class="alternatelisttable">
    <tr>
      <td class="table-heading">环境变量</td>
      <td class="table-heading">值</td>
     </tr>
     <xsl:for-each select="//INSTANCE[@CLASSNAME='SPRT_Environments']">
       <tr>
         <td>
           <xsl:if test="position() mod 2 = 1"><xsl:attribute name="class">odd-row</xsl:attribute></xsl:if>
           <xsl:if test="position() mod 2 = 0"><xsl:attribute name="class">even-row</xsl:attribute></xsl:if>
           <xsl:value-of select="translate(./PROPERTY[@NAME='EnvVar']/VALUE, $vLowercaseChars_CONST , $vUppercaseChars_CONST)"/> 
         </td>
         <td>
           <xsl:if test="position() mod 2 = 1"><xsl:attribute name="class">odd-row</xsl:attribute></xsl:if>
           <xsl:if test="position() mod 2 = 0"><xsl:attribute name="class">even-row</xsl:attribute></xsl:if>
           <xsl:value-of select="./PROPERTY[@NAME='Value']/VALUE" />
         </td>
       </tr>
     </xsl:for-each>
   </table>
  </div>
      

  <div id="printerlist" name="打印机列表" filename="Printer List">
    <table border="0" margin="0" width="100%" class="alternatelisttable">
    <tr>
      <td class="table-heading">打印机</td>
      <td class="table-heading">服务器名</td>
      <td class="table-heading">类型</td>
     </tr>
     
     <xsl:if test="count(//INSTANCE[@CLASSNAME='SPRT_Printers']) &lt; 1">
       <tr><td colspan="3"><br/><center>找不到打印机</center></td></tr>
     </xsl:if>
     
     <xsl:for-each select="//INSTANCE[@CLASSNAME='SPRT_Printers']">
       <tr>
         <td>
           <xsl:if test="position() mod 2 = 1"><xsl:attribute name="class">odd-row</xsl:attribute></xsl:if>
           <xsl:if test="position() mod 2 = 0"><xsl:attribute name="class">even-row</xsl:attribute></xsl:if>
           <xsl:value-of select="./PROPERTY[@NAME='Printer']/VALUE" />
         </td>
         <td>
            <xsl:if test="position() mod 2 = 1"><xsl:attribute name="class">odd-row</xsl:attribute></xsl:if>
            <xsl:if test="position() mod 2 = 0"><xsl:attribute name="class">even-row</xsl:attribute></xsl:if>
            <xsl:call-template name="check-value">
              <xsl:with-param name="value"><xsl:value-of select="./PROPERTY[@NAME='Server']/VALUE"/></xsl:with-param>
              <xsl:with-param name="center-on-blank">正确</xsl:with-param>
            </xsl:call-template>
         </td>
         <td>
           <xsl:if test="position() mod 2 = 1"><xsl:attribute name="class">odd-row</xsl:attribute></xsl:if>
           <xsl:if test="position() mod 2 = 0"><xsl:attribute name="class">even-row</xsl:attribute></xsl:if>
           <xsl:value-of select="./PROPERTY[@NAME='Type']/VALUE" />
         </td>
       </tr>
     </xsl:for-each>
   </table>
  </div>
      
      
  <div id="runningprograms" name="运行程序" filename="Running Programs">
    <table border="0" margin="0" width="100%" class="alternatelisttable">
    <tr>
      <td class="table-heading">程序</td>
      <td class="table-heading">PID</td>
      <td class="table-heading">产品名称</td>
      <td class="table-heading">版本</td>
     </tr>
     <xsl:for-each select="//INSTANCE[@CLASSNAME='SPRT_Applications']">
       <tr>
         <td>
	  <xsl:variable name="ProgName">
            <xsl:call-template name="substring-before-last">
              <xsl:with-param name="input"><xsl:value-of select="./PROPERTY[@NAME='Application']/VALUE"/>
              </xsl:with-param>
              <xsl:with-param name="substr">(</xsl:with-param>
            </xsl:call-template>
          </xsl:variable> 
           <xsl:if test="position() mod 2 = 1"><xsl:attribute name="class">odd-row</xsl:attribute></xsl:if>
           <xsl:if test="position() mod 2 = 0"><xsl:attribute name="class">even-row</xsl:attribute></xsl:if>
	   <xsl:choose>
	     <xsl:when test="starts-with($ProgName,'\??\')"><xsl:value-of select="substring-after($ProgName,'\??\')" /></xsl:when>
	     <xsl:otherwise><xsl:value-of select="$ProgName" /></xsl:otherwise>
	   </xsl:choose>
         </td>
         <td>
           <xsl:if test="position() mod 2 = 1"><xsl:attribute name="class">odd-row</xsl:attribute></xsl:if>
           <xsl:if test="position() mod 2 = 0"><xsl:attribute name="class">even-row</xsl:attribute></xsl:if>
           <xsl:value-of select="./PROPERTY[@NAME='PID']/VALUE" />
         </td>
         <td>
           <xsl:if test="position() mod 2 = 1"><xsl:attribute name="class">odd-row</xsl:attribute></xsl:if>
           <xsl:if test="position() mod 2 = 0"><xsl:attribute name="class">even-row</xsl:attribute></xsl:if>
           <xsl:call-template name="check-value">
             <xsl:with-param name="value"><xsl:value-of select="./PROPERTY[@NAME='ProductName']/VALUE" /></xsl:with-param>
             <xsl:with-param name="center-on-blank">正确</xsl:with-param>
           </xsl:call-template>
         </td>
         <td>
           <xsl:if test="position() mod 2 = 1"><xsl:attribute name="class">odd-row</xsl:attribute></xsl:if>
           <xsl:if test="position() mod 2 = 0"><xsl:attribute name="class">even-row</xsl:attribute></xsl:if>
           <xsl:call-template name="check-value">
             <xsl:with-param name="value"><xsl:value-of select="./PROPERTY[@NAME='ProductVersion']/VALUE" /></xsl:with-param>
             <xsl:with-param name="center-on-blank">正确</xsl:with-param>
           </xsl:call-template>
         </td>
       </tr>
     </xsl:for-each>
   </table>
  </div>
  
  
</div>
</xsl:template>


<xsl:template name="check-value">
  <xsl:param name="value" />
  <xsl:param name="center-on-blank" />
  <xsl:param name="postfix" />
  <xsl:choose>
    <xsl:when test="normalize-space($value)='' and $center-on-blank='true'">
      <center>-</center>
    </xsl:when>
    <xsl:when test="normalize-space($value)=''">
      -
    </xsl:when>    
    <xsl:otherwise>
      <xsl:value-of select="$value" /> <xsl:value-of select="$postfix" />
    </xsl:otherwise>
  </xsl:choose> 
</xsl:template>    


<xsl:template name="output-tokens">
  <xsl:param name="list" />
  <xsl:param name="delimiter" />
  <xsl:variable name="newlist">
    <xsl:choose>
      <xsl:when test="contains($list, $delimiter)"><xsl:value-of select="normalize-space($list)" /></xsl:when>
      <xsl:otherwise><xsl:value-of select="concat(normalize-space($list), $delimiter)"/></xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  <xsl:variable name="first" select="substring-before($newlist, $delimiter)" />
  <xsl:variable name="remaining" select="substring-after($newlist, $delimiter)" />
  <xsl:choose>
    <xsl:when test="normalize-space($first)=''">
      -
    </xsl:when>
    <xsl:otherwise>
      <xsl:value-of select="$first" /><br/>
    </xsl:otherwise>
  </xsl:choose>
  <xsl:if test="$remaining">
    <xsl:call-template name="output-tokens">
      <xsl:with-param name="list" select="$remaining" />
      <xsl:with-param name="delimiter"><xsl:value-of select="$delimiter"/></xsl:with-param>
    </xsl:call-template>
  </xsl:if>
</xsl:template>

<xsl:template name="substring-before-last">
  <xsl:param name="input" />
  <xsl:param name="substr" />
  <xsl:if test="$substr and contains($input, $substr)">
    <xsl:variable name="temp" select="substring-after($input, $substr)" />
    <xsl:value-of select="substring-before($input, $substr)" />
    <xsl:if test="contains($temp, $substr)">
      <xsl:value-of select="$substr" />
      <xsl:call-template name="substring-before-last">
        <xsl:with-param name="input" select="$temp" />
        <xsl:with-param name="substr" select="$substr" />
      </xsl:call-template>
    </xsl:if>
  </xsl:if>
</xsl:template>


<xsl:template name="getLocaleName">
  <xsl:param name="localeID" />
  <xsl:choose>
    <xsl:when test="$localeID='00000036'">南非荷兰语</xsl:when>
    <xsl:when test="$localeID='00000436'">布尔语（南非)</xsl:when>
    <xsl:when test="$localeID='0000001C'">阿尔巴尼亚</xsl:when>
    <xsl:when test="$localeID='0000041C'">阿尔巴尼亚（阿尔巴尼亚）</xsl:when>
    <xsl:when test="$localeID='00000484'">阿尔萨斯（法国）</xsl:when>
    <xsl:when test="$localeID='0000045E'">阿姆哈拉语（埃塞俄比亚）</xsl:when>
    <xsl:when test="$localeID='00000001'">阿拉伯语</xsl:when>
    <xsl:when test="$localeID='00001401'">阿拉伯语（阿尔及利亚）</xsl:when>
    <xsl:when test="$localeID='00003C01'">阿拉伯语（巴林）</xsl:when>
    <xsl:when test="$localeID='00000C01'">阿拉伯语（埃及）</xsl:when>
    <xsl:when test="$localeID='00000801'">阿拉伯语（伊拉克）</xsl:when>
    <xsl:when test="$localeID='00002C01'">阿拉伯语（约旦）</xsl:when>
    <xsl:when test="$localeID='00003401'">阿拉伯语（科威特）</xsl:when>
    <xsl:when test="$localeID='00003001'">阿拉伯语（黎巴嫩）</xsl:when>
    <xsl:when test="$localeID='00001001'">阿拉伯语（利比亚）</xsl:when>
    <xsl:when test="$localeID='00001801'">阿拉伯语（摩洛哥）</xsl:when>
    <xsl:when test="$localeID='00002001'">阿拉伯语（阿曼）</xsl:when>
    <xsl:when test="$localeID='00004001'">阿拉伯语（卡塔尔）</xsl:when>
    <xsl:when test="$localeID='00000401'">阿拉伯语（沙特阿拉伯）</xsl:when>
    <xsl:when test="$localeID='00002801'">阿拉伯语（叙利亚）</xsl:when>
    <xsl:when test="$localeID='00001C01'">阿拉伯语（突尼斯）</xsl:when>
    <xsl:when test="$localeID='00003801'">阿拉伯语（阿联酋）</xsl:when>
    <xsl:when test="$localeID='00002401'">阿拉伯语（也门）</xsl:when>
    <xsl:when test="$localeID='0000002B'">亚美尼亚</xsl:when>
    <xsl:when test="$localeID='0000042B'">亚美尼亚（亚美尼亚）</xsl:when>
    <xsl:when test="$localeID='0000044D'">阿萨姆语（印度）</xsl:when>
    <xsl:when test="$localeID='0000002C'">阿塞拜疆语</xsl:when>
    <xsl:when test="$localeID='0000082C'">阿塞拜疆语（西里尔,阿塞拜疆）</xsl:when>
    <xsl:when test="$localeID='0000042C'">阿塞拜疆语（拉丁文,阿塞拜疆）</xsl:when>
    <xsl:when test="$localeID='0000046D'">巴什基尔语（俄罗斯）</xsl:when>
    <xsl:when test="$localeID='0000002D'">巴斯克语</xsl:when>
    <xsl:when test="$localeID='0000042D'">巴斯克语（巴斯克语）</xsl:when>
    <xsl:when test="$localeID='00000023'">白俄罗斯语</xsl:when>
    <xsl:when test="$localeID='00000423'">白俄罗斯语（白俄罗斯）</xsl:when>
    <xsl:when test="$localeID='00000845'">孟加拉语（孟加拉）</xsl:when>
    <xsl:when test="$localeID='00000445'">孟加拉语（印度）</xsl:when>
    <xsl:when test="$localeID='0000201A'">波斯尼亚语（西里尔,波斯尼亚和黑塞哥维那）</xsl:when>
    <xsl:when test="$localeID='0000141A'">波斯尼亚语（拉丁，波斯尼亚和黑塞哥维那）</xsl:when>
    <xsl:when test="$localeID='0000047E'">布列塔尼人（法国）</xsl:when>
    <xsl:when test="$localeID='00000002'">保加利亚</xsl:when>
    <xsl:when test="$localeID='00000402'">保加利亚（保加利亚）</xsl:when>
    <xsl:when test="$localeID='00000003'">加泰罗尼亚语</xsl:when>
    <xsl:when test="$localeID='00000403'">加泰罗尼亚（加泰罗尼亚）</xsl:when>
    <xsl:when test="$localeID='00000C04'">中文（香港特别行政区）</xsl:when>
    <xsl:when test="$localeID='00001404'">中文（澳门特别行政区）</xsl:when>
    <xsl:when test="$localeID='00000804'">中文（中华人民共和国）</xsl:when>
    <xsl:when test="$localeID='00000004'">中文（简体）</xsl:when>
    <xsl:when test="$localeID='00001004'">中文（新加坡）</xsl:when>
    <xsl:when test="$localeID='00000404'">中文（台湾）</xsl:when>
    <xsl:when test="$localeID='00007C04'">中文（繁体）</xsl:when>
    <xsl:when test="$localeID='00000483'">科西嘉语（法国）</xsl:when>
    <xsl:when test="$localeID='0000001A'">科西嘉语</xsl:when>
    <xsl:when test="$localeID='0000041A'">科西嘉语（科西嘉）</xsl:when>
    <xsl:when test="$localeID='0000101A'">科西嘉语（拉丁，波斯尼亚和黑塞哥维那）</xsl:when>
    <xsl:when test="$localeID='00000005'">捷克</xsl:when>
    <xsl:when test="$localeID='00000405'">捷克（捷克共和国）</xsl:when>
    <xsl:when test="$localeID='00000006'">丹麦</xsl:when>
    <xsl:when test="$localeID='00000406'">丹麦（丹麦）</xsl:when>
    <xsl:when test="$localeID='0000048C'">达里（阿富汗）</xsl:when>
    <xsl:when test="$localeID='00000065'">迪维西语</xsl:when>
    <xsl:when test="$localeID='00000465'">迪维西语（马尔代夫）</xsl:when>
    <xsl:when test="$localeID='00000013'">荷兰</xsl:when>
    <xsl:when test="$localeID='00000813'">荷兰（比利时）</xsl:when>
    <xsl:when test="$localeID='00000413'">荷兰（荷兰）</xsl:when>
    <xsl:when test="$localeID='00000009'">英语</xsl:when>
    <xsl:when test="$localeID='00000C09'">英语（澳大利亚）</xsl:when>
    <xsl:when test="$localeID='00002809'">英语（伯利兹）</xsl:when>
    <xsl:when test="$localeID='00001009'">英语（加拿大）</xsl:when>
    <xsl:when test="$localeID='00002409'">英语（加勒比）</xsl:when>
    <xsl:when test="$localeID='00004009'">英语（印度）</xsl:when>
    <xsl:when test="$localeID='00001809'">英语（爱尔兰）</xsl:when>
    <xsl:when test="$localeID='00002009'">英语（牙买加）</xsl:when>
    <xsl:when test="$localeID='00004409'">英语（马来西亚）</xsl:when>
    <xsl:when test="$localeID='00001409'">英语（新西兰）</xsl:when>
    <xsl:when test="$localeID='00003409'">英语（菲律宾共和国）</xsl:when>
    <xsl:when test="$localeID='00004809'">英语（新加坡）</xsl:when>
    <xsl:when test="$localeID='00001C09'">英语（南非）</xsl:when>
    <xsl:when test="$localeID='00002C09'">英语（特立尼达和多巴哥）</xsl:when>
    <xsl:when test="$localeID='00000809'">英语（英国）</xsl:when>
    <xsl:when test="$localeID='00000409'">英语（美国）</xsl:when>
    <xsl:when test="$localeID='00003009'">英语（津巴布韦）</xsl:when>
    <xsl:when test="$localeID='00000025'">爱沙尼亚</xsl:when>
    <xsl:when test="$localeID='00000425'">爱沙尼亚（爱沙尼亚）</xsl:when>
    <xsl:when test="$localeID='00000038'">法罗人</xsl:when>
    <xsl:when test="$localeID='00000438'">法罗人（法罗群岛）</xsl:when>
    <xsl:when test="$localeID='00000464'">菲律宾语（菲律宾）</xsl:when>
    <xsl:when test="$localeID='0000000B'">芬兰</xsl:when>
    <xsl:when test="$localeID='0000040B'">芬兰（芬兰）</xsl:when>
    <xsl:when test="$localeID='0000000C'">法国</xsl:when>
    <xsl:when test="$localeID='0000080C'">法语（比利时）</xsl:when>
    <xsl:when test="$localeID='00000C0C'">法语（加拿大）</xsl:when>
    <xsl:when test="$localeID='0000040C'">法语（法国）</xsl:when>
    <xsl:when test="$localeID='0000140C'">法语（卢森堡）</xsl:when>
    <xsl:when test="$localeID='0000180C'">法语（摩纳哥公国）</xsl:when>
    <xsl:when test="$localeID='0000100C'">法语（瑞士）</xsl:when>
    <xsl:when test="$localeID='00000462'">弗里斯兰语（荷兰）</xsl:when>
    <xsl:when test="$localeID='00000056'">加利西亚语</xsl:when>
    <xsl:when test="$localeID='00000456'">加利西亚语（保加利亚）</xsl:when>
    <xsl:when test="$localeID='00000037'">格鲁吉亚</xsl:when>
    <xsl:when test="$localeID='00000437'">格鲁吉亚（乔治亚州）</xsl:when>
    <xsl:when test="$localeID='00000007'">德语</xsl:when>
    <xsl:when test="$localeID='00000C07'">德语（奥地利）</xsl:when>
    <xsl:when test="$localeID='00000407'">德语（德国）</xsl:when>
    <xsl:when test="$localeID='00001407'">德语（列支敦士登）</xsl:when>
    <xsl:when test="$localeID='00001007'">德语（卢森堡）</xsl:when>
    <xsl:when test="$localeID='00000807'">德语（瑞士）</xsl:when>
    <xsl:when test="$localeID='00000008'">希腊</xsl:when>
    <xsl:when test="$localeID='00000408'">希腊（希腊）</xsl:when>
    <xsl:when test="$localeID='0000046F'">格陵兰（格陵兰岛）</xsl:when>
    <xsl:when test="$localeID='00000047'">古吉拉特语</xsl:when>
    <xsl:when test="$localeID='00000447'">古吉拉特语（印度）</xsl:when>
    <xsl:when test="$localeID='00000468'">豪萨族（拉丁文、尼日利亚）</xsl:when>
    <xsl:when test="$localeID='0000000D'">希伯来语</xsl:when>
    <xsl:when test="$localeID='0000040D'">希伯来语（以色列）</xsl:when>
    <xsl:when test="$localeID='00000039'">北印度语</xsl:when>
    <xsl:when test="$localeID='00000439'">印地语（印度）</xsl:when>
    <xsl:when test="$localeID='0000000E'">匈牙利</xsl:when>
    <xsl:when test="$localeID='0000040E'">匈牙利（匈牙利）</xsl:when>
    <xsl:when test="$localeID='0000000F'">冰岛</xsl:when>
    <xsl:when test="$localeID='0000040F'">冰岛（冰岛）</xsl:when>
    <xsl:when test="$localeID='00000470'">伊博（尼日利亚）</xsl:when>
    <xsl:when test="$localeID='00000021'">印尼</xsl:when>
    <xsl:when test="$localeID='00000421'">印尼（印度尼西亚）</xsl:when>
    <xsl:when test="$localeID='0000085D'">因纽特语（拉丁，加拿大）</xsl:when>
    <xsl:when test="$localeID='0000045D'">因纽特语（加拿大原住民，加拿大）</xsl:when>
    <xsl:when test="$localeID='0000083C'">爱尔兰语（爱尔兰）</xsl:when>
    <xsl:when test="$localeID='00000434'">科萨语（南非）</xsl:when>
    <xsl:when test="$localeID='00000435'">祖鲁语（南非）</xsl:when>
    <xsl:when test="$localeID='00000010'">意大利语</xsl:when>
    <xsl:when test="$localeID='00000410'">意大利语（意大利）</xsl:when>
    <xsl:when test="$localeID='00000810'">意大利（瑞士）</xsl:when>
    <xsl:when test="$localeID='00000011'">日语</xsl:when>
    <xsl:when test="$localeID='00000411'">日语（日本）</xsl:when>
    <xsl:when test="$localeID='0000004B'">坎那达语</xsl:when>
    <xsl:when test="$localeID='0000044B'">坎那达语（印度）</xsl:when>
    <xsl:when test="$localeID='0000003F'">哈萨克语</xsl:when>
    <xsl:when test="$localeID='0000043F'">哈萨克语（哈萨克斯坦）</xsl:when>
    <xsl:when test="$localeID='00000453'">高棉语（柬埔寨）</xsl:when>
    <xsl:when test="$localeID='00000486'">基切语（危地马拉）</xsl:when>
    <xsl:when test="$localeID='00000487'">基尼亚卢旺达语（卢旺达）</xsl:when>
    <xsl:when test="$localeID='00000041'">斯瓦希里语</xsl:when>
    <xsl:when test="$localeID='00000441'">斯瓦希里语（肯尼亚）</xsl:when>
    <xsl:when test="$localeID='00000057'">贡根语</xsl:when>
    <xsl:when test="$localeID='00000457'">贡根语（印度）</xsl:when>
    <xsl:when test="$localeID='00000012'">朝鲜语</xsl:when>
    <xsl:when test="$localeID='00000412'">朝鲜语（韩国）</xsl:when>
    <xsl:when test="$localeID='00000040'">吉尔吉斯坦语</xsl:when>
    <xsl:when test="$localeID='00000440'">吉尔吉斯坦语（吉尔吉斯斯坦）</xsl:when>
    <xsl:when test="$localeID='00000454'">老挝语（老挝人民共和国）</xsl:when>
    <xsl:when test="$localeID='00000026'">拉脱维亚语</xsl:when>
    <xsl:when test="$localeID='00000426'">拉脱维亚语（拉脱维亚）</xsl:when>
    <xsl:when test="$localeID='00000027'">立陶宛语</xsl:when>
    <xsl:when test="$localeID='00000427'">立陶宛语（立陶宛）</xsl:when>
    <xsl:when test="$localeID='0000082E'">下索布语（德国）</xsl:when>
    <xsl:when test="$localeID='0000046E'">卢森堡语（卢森堡）</xsl:when>
    <xsl:when test="$localeID='0000002F'"> 马其顿语</xsl:when>
    <xsl:when test="$localeID='0000042F'">马其顿语（前南斯拉夫马其顿共和国）</xsl:when>
    <xsl:when test="$localeID='0000003E'">马来语</xsl:when>
    <xsl:when test="$localeID='0000083E'">马来语（文莱达鲁萨兰国）</xsl:when>
    <xsl:when test="$localeID='0000043E'">马来语（马来西亚）</xsl:when>
    <xsl:when test="$localeID='0000044C'">马拉雅拉姆语（印度）</xsl:when>
    <xsl:when test="$localeID='0000043A'">马耳他语（马耳他）</xsl:when>
    <xsl:when test="$localeID='00000481'">毛利语（新西兰）</xsl:when>
    <xsl:when test="$localeID='0000047A'">马普切语（智利）</xsl:when>
    <xsl:when test="$localeID='0000004E'">马拉地语</xsl:when>
    <xsl:when test="$localeID='0000044E'">马拉地语（印度）</xsl:when>
    <xsl:when test="$localeID='0000047C'">莫霍克（莫霍克）</xsl:when>
    <xsl:when test="$localeID='00000050'">蒙古语</xsl:when>
    <xsl:when test="$localeID='00000450'">蒙古语（西里尔、蒙古）</xsl:when>
    <xsl:when test="$localeID='00000850'">蒙古语（传统的蒙古人,中华人民共和国）</xsl:when>
    <xsl:when test="$localeID='00000461'">尼泊尔语（尼泊尔）</xsl:when>
    <xsl:when test="$localeID='00000014'">挪威语</xsl:when>
    <xsl:when test="$localeID='00000414'">Norwegian, Bokm�l (Norway)</xsl:when>
    <xsl:when test="$localeID='00000814'">挪威,尼诺斯克语（挪威）</xsl:when>
    <xsl:when test="$localeID='00000482'">奥克语的（法国）</xsl:when>
    <xsl:when test="$localeID='00000448'">奥里语（印度）</xsl:when>
    <xsl:when test="$localeID='00000463'">普什图语（阿富汗）</xsl:when>
    <xsl:when test="$localeID='00000029'">波斯语</xsl:when>
    <xsl:when test="$localeID='00000429'">波斯语</xsl:when>
    <xsl:when test="$localeID='00000015'">波兰语</xsl:when>
    <xsl:when test="$localeID='00000415'">波兰语（波兰）</xsl:when>
    <xsl:when test="$localeID='00000016'">葡萄牙语</xsl:when>
    <xsl:when test="$localeID='00000416'">葡萄牙语（巴西）</xsl:when>
    <xsl:when test="$localeID='00000816'">葡萄牙语（葡萄牙）</xsl:when>
    <xsl:when test="$localeID='00000046'">旁遮普语</xsl:when>
    <xsl:when test="$localeID='00000446'">旁遮普语（印度）</xsl:when>
    <xsl:when test="$localeID='0000046B'">盖丘亚语（玻利维亚）</xsl:when>
    <xsl:when test="$localeID='0000086B'">盖丘亚语（厄瓜多尔）</xsl:when>
    <xsl:when test="$localeID='00000C6B'">盖丘亚语（秘鲁）</xsl:when>
    <xsl:when test="$localeID='00000018'">罗马尼亚语</xsl:when>
    <xsl:when test="$localeID='00000418'">罗马尼亚语（罗马尼亚）</xsl:when>
    <xsl:when test="$localeID='00000417'">罗曼什语（瑞士）</xsl:when>
    <xsl:when test="$localeID='00000019'">俄语</xsl:when>
    <xsl:when test="$localeID='00000419'">俄语（俄罗斯）</xsl:when>
    <xsl:when test="$localeID='0000243B'">萨米语,伊纳里（芬兰）</xsl:when>
    <xsl:when test="$localeID='0000103B'">萨米语,吕勒奥（挪威）</xsl:when>
    <xsl:when test="$localeID='0000143B'">萨米语,吕勒奥（瑞典）</xsl:when>
    <xsl:when test="$localeID='00000C3B'">萨米,北部（芬兰）</xsl:when>
    <xsl:when test="$localeID='0000043B'">萨米,北部（挪威）</xsl:when>
    <xsl:when test="$localeID='0000083B'">萨米,北部（瑞典）</xsl:when>
    <xsl:when test="$localeID='0000203B'">斯科尔特萨米语、（芬兰）</xsl:when>
    <xsl:when test="$localeID='0000183B'">萨米人,南部（挪威）</xsl:when>
    <xsl:when test="$localeID='00001C3B'">萨米人,南部（瑞典）</xsl:when>
    <xsl:when test="$localeID='0000004F'">梵文</xsl:when>
    <xsl:when test="$localeID='0000044F'">梵文（印度）</xsl:when>
    <xsl:when test="$localeID='00007C1A'">塞尔维亚语</xsl:when>
    <xsl:when test="$localeID='00001C1A'">塞尔维亚语（西里尔,波斯尼亚和黑塞哥维那）</xsl:when>
    <xsl:when test="$localeID='00000C1A'">塞尔维亚语（西里尔,塞尔维亚）</xsl:when>
    <xsl:when test="$localeID='0000181A'">塞尔维亚语（拉丁,波斯尼亚和黑塞哥维那）</xsl:when>
    <xsl:when test="$localeID='0000081A'">塞尔维亚语（拉丁,塞尔维亚）</xsl:when>
    <xsl:when test="$localeID='0000046C'">塞索托语北梭托语（南非）</xsl:when>
    <xsl:when test="$localeID='00000432'">塞茨瓦纳语（南非）</xsl:when>
    <xsl:when test="$localeID='0000045B'">僧伽罗语（斯里兰卡）</xsl:when>
    <xsl:when test="$localeID='0000001B'">斯洛伐克语</xsl:when>
    <xsl:when test="$localeID='0000041B'">斯洛伐克语（斯洛伐克）</xsl:when>
    <xsl:when test="$localeID='00000024'">斯洛维尼亚语</xsl:when>
    <xsl:when test="$localeID='00000424'">斯洛文尼亚语（斯洛文尼亚）</xsl:when>
    <xsl:when test="$localeID='0000000A'">西班牙语</xsl:when>
    <xsl:when test="$localeID='00002C0A'">西班牙语（阿根廷）</xsl:when>
    <xsl:when test="$localeID='0000400A'">西班牙语（玻利维亚）</xsl:when>
    <xsl:when test="$localeID='0000340A'">西班牙语（智利）</xsl:when>
    <xsl:when test="$localeID='0000240A'">西班牙语（哥伦比亚）</xsl:when>
    <xsl:when test="$localeID='0000140A'">西班牙语（哥斯达黎加）</xsl:when>
    <xsl:when test="$localeID='00001C0A'">西班牙语（多米尼加共和国）</xsl:when>
    <xsl:when test="$localeID='0000300A'">西班牙语（厄瓜多尔）</xsl:when>
    <xsl:when test="$localeID='0000440A'">西班牙语（萨尔瓦多）</xsl:when>
    <xsl:when test="$localeID='0000100A'">西班牙语（危地马拉）</xsl:when>
    <xsl:when test="$localeID='0000480A'">西班牙语（洪都拉斯）</xsl:when>
    <xsl:when test="$localeID='0000080A'">西班牙语（墨西哥）</xsl:when>
    <xsl:when test="$localeID='00004C0A'">西班牙语（尼加拉瓜）</xsl:when>
    <xsl:when test="$localeID='0000180A'">西班牙语（巴拿马）</xsl:when>
    <xsl:when test="$localeID='00003C0A'">西班牙语（巴拉圭）</xsl:when>
    <xsl:when test="$localeID='0000280A'">西班牙语（秘鲁）</xsl:when>
    <xsl:when test="$localeID='0000500A'">西班牙语（波多黎各）</xsl:when>
    <xsl:when test="$localeID='00000C0A'">西班牙语（西班牙）</xsl:when>
    <xsl:when test="$localeID='0000540A'">西班牙语（美国）</xsl:when>
    <xsl:when test="$localeID='0000380A'">西班牙语（乌拉圭）</xsl:when>
    <xsl:when test="$localeID='0000200A'">西班牙语（委内瑞拉）</xsl:when>
    <xsl:when test="$localeID='0000001D'"> 瑞典语</xsl:when>
    <xsl:when test="$localeID='0000081D'">瑞典语（芬兰）</xsl:when>
    <xsl:when test="$localeID='0000041D'">瑞典语（瑞典）</xsl:when>
    <xsl:when test="$localeID='0000005A'">叙利亚语</xsl:when>
    <xsl:when test="$localeID='0000045A'">叙利亚语</xsl:when>
    <xsl:when test="$localeID='00000428'">塔吉克语（西里尔，塔吉克斯坦）</xsl:when>
    <xsl:when test="$localeID='0000085F'">塔马塞特语(拉丁文,阿尔及利亚)</xsl:when>
    <xsl:when test="$localeID='00000049'">泰米尔语</xsl:when>
    <xsl:when test="$localeID='00000449'">泰米尔语（印度）</xsl:when>
    <xsl:when test="$localeID='00000044'"> 鞑靼语</xsl:when>
    <xsl:when test="$localeID='00000444'"> 鞑靼语（俄罗斯）</xsl:when>
    <xsl:when test="$localeID='0000004A'">泰卢固语</xsl:when>
    <xsl:when test="$localeID='0000044A'">泰卢固语（印度）</xsl:when>
    <xsl:when test="$localeID='0000001E'">泰语</xsl:when>
    <xsl:when test="$localeID='0000041E'">泰语（泰国）</xsl:when>
    <xsl:when test="$localeID='00000451'">藏语（中国）</xsl:when>
    <xsl:when test="$localeID='0000001F'">土耳其语</xsl:when>
    <xsl:when test="$localeID='0000041F'">土耳其语（土耳其）</xsl:when>
    <xsl:when test="$localeID='00000442'">土库曼语（土库曼斯坦）</xsl:when>
    <xsl:when test="$localeID='00000480'">维吾尔语（中国）</xsl:when>
    <xsl:when test="$localeID='00000022'">乌克兰语</xsl:when>
    <xsl:when test="$localeID='00000422'">乌克兰语（乌克兰）</xsl:when>
    <xsl:when test="$localeID='0000042E'">上索布语（德国）</xsl:when>
    <xsl:when test="$localeID='00000020'">乌克兰语（乌克兰）</xsl:when>
    <xsl:when test="$localeID='00000420'">乌尔都语（巴基斯坦伊斯兰共和国）</xsl:when>
    <xsl:when test="$localeID='00000043'">乌兹别克</xsl:when>
    <xsl:when test="$localeID='00000843'">乌兹别克斯坦（西里尔,乌兹别克斯坦）</xsl:when>
    <xsl:when test="$localeID='00000443'">乌兹别克斯坦（拉丁,乌兹别克斯坦）</xsl:when>
    <xsl:when test="$localeID='0000002A'">越南语</xsl:when>
    <xsl:when test="$localeID='0000042A'">越南语（越南）</xsl:when>
    <xsl:when test="$localeID='00000452'">威尔士语（英国）</xsl:when>
    <xsl:when test="$localeID='00000488'">沃洛夫语（塞内加尔）</xsl:when>
    <xsl:when test="$localeID='00000485'">雅库特语（俄罗斯）</xsl:when>
    <xsl:when test="$localeID='00000478'">彝语（中国）</xsl:when>
    <xsl:when test="$localeID='0000046A'">约鲁巴语（尼日利亚）</xsl:when>
  </xsl:choose>
</xsl:template>

</xsl:stylesheet> 
