<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
<div>
  <div id="systemsummary" name="System Summary" filename="System Summary" class="alternatelisttable">
    <table border="0" margin="0" width="100%">
     <tr>
      <td class="table-heading">My Computer and Incident Summary</td>
      <td class="table-heading"></td>
     </tr>
     <tr>
      <td class="odd-row">SmartIssue ID</td>
      <td class="odd-row">{<xsl:value-of select="//UPLOADDATA[1]/@USERNAME"/>}</td>
     </tr>
     <tr>
      <td class="even-row">Machine ID</td>
      <td class="even-row"><xsl:value-of select="//INSTANCE[@CLASSNAME='SPRT_Connectivity']/PROPERTY[@NAME='MacID']/VALUE"/></td>
     </tr>
     <tr>
      <td class="odd-row">DNS Name</td>
      <td class="odd-row"><xsl:value-of select="//INSTANCE[@CLASSNAME='SPRT_Connectivity']/PROPERTY[@NAME='HostName']/VALUE"/></td>
     </tr>
     <tr>
      <td class="even-row">Domain or Workgroup</td>
      <td class="even-row"><xsl:value-of select="//INSTANCE[@CLASSNAME='WMIComputerSystem']/PROPERTY[@NAME='Domain']/VALUE"/></td>
     </tr>
     <tr>
      <td class="odd-row">Operating System</td>
      <td class="odd-row"><xsl:variable name="displayOS">
            <xsl:value-of select="//INSTANCE[@CLASSNAME='WMIOperatingSystem']/PROPERTY[@NAME='Caption']/VALUE"/>
            <xsl:variable name="CSDVersion" select="//INSTANCE[@CLASSNAME='WMIOperatingSystem']/PROPERTY[@NAME='CSDVersion']/VALUE"/>
            <xsl:if test="normalize-space($CSDVersion) != ''">
              - <xsl:value-of select="$CSDVersion"/>
            </xsl:if>
          </xsl:variable>
          <xsl:value-of select="$displayOS"/>
     </td>
     </tr>
     <tr>
      <td class="even-row">Operating System Version</td>
      <td class="even-row"><xsl:value-of select="//INSTANCE[@CLASSNAME='WMIOperatingSystem']/PROPERTY[@NAME='Version']/VALUE"/></td>
     </tr>
     <tr>
      <td class="odd-row">Locale</td>
      <td class="odd-row">
        <xsl:variable name="localeName">
          <xsl:call-template name="getLocaleName">
            <xsl:with-param name="localeID"><xsl:value-of select="//INSTANCE[@CLASSNAME='OperatingSystemInfo']/PROPERTY[@NAME='Locale']/VALUE"/></xsl:with-param>
          </xsl:call-template>
        </xsl:variable>
        <xsl:value-of select="$localeName"/>
      </td>
     </tr>
     <tr>
      <td class="even-row">Total Physical Memory</td>
      <td class="even-row"><xsl:variable name="totalMem" select="//INSTANCE[@CLASSNAME='Win32_LogicalMemoryConfig']/PROPERTY[@NAME='TotalPhysicalMemory']/VALUE"/>
          <xsl:value-of select="$totalMem"/> MB
      </td>
     </tr>
     <tr>
      <td class="odd-row">Memory Free</td>
      <td class="odd-row"><xsl:variable name="percentLoad" select="//INSTANCE[@CLASSNAME='SPRT_AdditionalSysInfo']/PROPERTY[@NAME='MemoryLoad']/VALUE * 0.01"/>
          <xsl:value-of select="round($totalMem - ($percentLoad * $totalMem))"/> MB
      </td>
     </tr>
     <tr>
      <td class="even-row">Default Browser</td>
      <td class="even-row">
        <xsl:variable name="browser_command" select="//INSTANCE[@CLASSNAME='DefaultBrowser']/PROPERTY[@NAME='BrowserOpenCommand']/VALUE"/>
        <xsl:choose>
          <xsl:when test="contains($browser_command, 'chrome')">Google Chrome</xsl:when>
          <xsl:when test="contains($browser_command, 'firefox')">Firefox</xsl:when>
          <xsl:when test="contains($browser_command, 'safari')">Safari</xsl:when>
          <xsl:when test="contains($browser_command, 'opera')">Opera</xsl:when>
          <xsl:when test="contains($browser_command, 'appxq0fevzme2pys62n3e0fbqa7peapykr8v')">Microsoft Edge</xsl:when>
          <xsl:when test="contains($browser_command, 'Unknown')">Unknown</xsl:when>
          <xsl:otherwise>Internet Explorer</xsl:otherwise>
        </xsl:choose>
      </td>
     </tr>
     <tr>
      <td class="odd-row">Default Browser Version</td>
      <td class="odd-row"><xsl:value-of select="//INSTANCE[@CLASSNAME='DefaultBrowser']/PROPERTY[@NAME='DefaultBrowserVersion']/VALUE"/></td>
     </tr>
     <tr>
      <td class="even-row">Default Email Client</td>
      <td class="even-row">
        <xsl:variable name="Def_HKCU" select="//INSTANCE[@CLASSNAME='Software']/PROPERTY[@NAME='DefaultEmailClientUser']/VALUE" />
        <xsl:choose>
          <xsl:when test="not($Def_HKCU)">
            <xsl:call-template name="check-value">
              <xsl:with-param name="value">
                <xsl:value-of select="//INSTANCE[@CLASSNAME='Software']/PROPERTY[@NAME='DefaultEmailClient']/VALUE"/>
              </xsl:with-param>
            </xsl:call-template>
          </xsl:when>
          <xsl:otherwise>
            <xsl:call-template name="check-value">
              <xsl:with-param name="value"><xsl:value-of select="//INSTANCE[@CLASSNAME='Software']/PROPERTY[@NAME='DefaultEmailClientUser']/VALUE"/></xsl:with-param>
            </xsl:call-template>
          </xsl:otherwise>
        </xsl:choose>
      </td>
     </tr>
     <tr>
      <td class="odd-row">TCP/IP Addresses</td>
      <td class="odd-row">
        <xsl:variable name="tcpAddress" select="//INSTANCE[@CLASSNAME='SPRT_Connectivity']/PROPERTY[@NAME='TCPIP_Address']/VALUE"/>
        <xsl:choose>
          <xsl:when test ="normalize-space($tcpAddress) != '127.0.0.1'">
         <xsl:call-template name="output-tokens">
              <xsl:with-param name="list">
                <xsl:value-of select="normalize-space($tcpAddress)" />
              </xsl:with-param>
           <xsl:with-param name="delimiter">;</xsl:with-param>
         </xsl:call-template>
          </xsl:when>
          <xsl:otherwise>
            -
          </xsl:otherwise>
        </xsl:choose>
      </td>
     </tr>
     <tr>
      <td class="even-row">MAC IDs</td>
      <td class="even-row">
        <xsl:call-template name="output-tokens">
          <xsl:with-param name="list"><xsl:value-of select="//INSTANCE[@CLASSNAME='EthernetInfo']/PROPERTY[@NAME='MACIDList']/VALUE"/></xsl:with-param>
          <xsl:with-param name="delimiter">;</xsl:with-param>
        </xsl:call-template>
      </td>
     </tr>
     <tr>
      <td class="odd-row">Product Name</td>
      <td class="odd-row"><xsl:value-of select="//INSTANCE[@CLASSNAME='ProductInfo']/PROPERTY[@NAME='ProductName']/VALUE"/></td>
     </tr>
     <tr>
      <td class="even-row"><xsl:value-of select="//INSTANCE[@CLASSNAME='ProductInfo']/PROPERTY[@NAME='ProductName']/VALUE"/> Version</td>
      <td class="even-row"><xsl:value-of select="//INSTANCE[@CLASSNAME='ClientVersion']/PROPERTY[@NAME='ClientBuildVersion']/VALUE"/></td>
     </tr>
    </table>

  </div>



  <div id="windows" name="Windows" filename="Windows">
    <table border="0" margin="0" width="100%" class="alternatelisttable">
     <tr>
      <td class="table-heading">My Operating System</td>
      <td class="table-heading"></td>
     </tr>
     <tr>
      <td class="odd-row">Operating System</td>
      <td class="odd-row"><xsl:value-of select="$displayOS"/></td>
     </tr>
     <tr>
      <td class="even-row">Operating System Type</td>
      <td class="even-row"><xsl:value-of select="//INSTANCE[@CLASSNAME='OperatingSystemInfo']/PROPERTY[@NAME='OSType']/VALUE"/></td>
     </tr>
     <tr>
      <td class="odd-row">Service Pack</td>
      <td class="odd-row">
        <xsl:call-template name="check-value">
          <xsl:with-param name="value"><xsl:value-of select="//INSTANCE[@CLASSNAME='OperatingSystemInfo']/PROPERTY[@NAME='SvcPack']/VALUE"/></xsl:with-param>
        </xsl:call-template>
      </td>
     </tr>
     <tr>
      <td class="even-row">Operating System Version</td>
      <td class="even-row"><xsl:value-of select="//INSTANCE[@CLASSNAME='WMIOperatingSystem']/PROPERTY[@NAME='Version']/VALUE"/></td>
     </tr>
     <tr>
      <td class="odd-row">Swap File Information</td>
      <td class="odd-row">
        <xsl:variable name="virtualMemFilename" select="normalize-space(//INSTANCE[@CLASSNAME='VirtualMemorySetting']/PROPERTY[@NAME='Filename']/VALUE)"/>
        <xsl:choose>
          <xsl:when test="$virtualMemFilename = ''">Paging Disabled</xsl:when>
          <xsl:otherwise><xsl:value-of select="$virtualMemFilename"/>
                         <xsl:text disable-output-escaping='yes'> </xsl:text>
                         <xsl:value-of select="//INSTANCE[@CLASSNAME='SPRT_AdditionalSysInfo']/PROPERTY[@NAME='PageFileAvailable']/VALUE"/> MB Free
          </xsl:otherwise>
  </xsl:choose>
      </td>
     </tr>
     <tr>
      <td class="even-row">Windows Directory</td>
      <td class="even-row"><xsl:value-of select="//INSTANCE[@CLASSNAME='PCH_Sysinfo']/PROPERTY[@NAME='WindowsDirectory']/VALUE"/></td>
     </tr>
     <tr>
      <td class="odd-row">Locale</td>
      <td class="odd-row"><xsl:value-of select="$localeName"/></td>
     </tr>
   </table>
  </div>


  <div id="myhardware" name="My Hardware" filename="My Hardware" class="alternatelisttable">
    <table border="0" margin="0" width="100%">
     <tr>
      <td class="table-heading">My CPU</td>
      <td class="table-heading"></td>
     </tr>
     <tr>
      <td class="odd-row" width="30%">Number of Processors</td>
      <td class="odd-row"><xsl:value-of select="//INSTANCE[@CLASSNAME='SPRT_AdditionalSysInfo']/PROPERTY[@NAME='NumberOfProcessors']/VALUE"/></td>
     </tr>
     <tr>
      <td class="even-row">Processor Architecture</td>
      <td class="even-row"><xsl:value-of select="//INSTANCE[@CLASSNAME='SPRT_AdditionalSysInfo']/PROPERTY[@NAME='ProcessorArchitecture']/VALUE"/></td>
     </tr>
     <tr>
      <td class="odd-row">Processor Type</td>
      <td class="odd-row"><xsl:value-of select="//INSTANCE[@CLASSNAME='SPRT_AdditionalSysInfo']/PROPERTY[@NAME='ProcessorType']/VALUE"/></td>
     </tr>
     <tr>
      <td class="even-row">Processor Level</td>
      <td class="even-row"><xsl:value-of select="//INSTANCE[@CLASSNAME='SPRT_AdditionalSysInfo']/PROPERTY[@NAME='ProcessorLevel']/VALUE"/></td>
     </tr>
     <tr>
      <td class="odd-row">Processor Version</td>
      <td class="odd-row"><xsl:value-of select="//INSTANCE[@CLASSNAME='WMIProcessor']/PROPERTY[@NAME='Version']/VALUE"/></td>
     </tr>
     <tr>
      <td class="even-row">Full Processor Name</td>
      <td class="even-row"><xsl:value-of select="//INSTANCE[@CLASSNAME='WMIProcessor']/PROPERTY[@NAME='Name']/VALUE"/></td>
     </tr>
    </table>

    <table border="0" margin="0" width="100%" class="alternatelisttable">
     <tr>
      <td class="table-heading">My Memory</td>
      <td class="table-heading"></td>
     </tr>
     <tr>
      <td class="odd-row" width="30%">Total Physical Memory</td>
      <td class="odd-row"><xsl:value-of select="$totalMem"/> MB</td>
     </tr>
     <tr>
      <td class="even-row">Memory Free</td>
      <td class="even-row"><xsl:value-of select="round($totalMem - ($percentLoad * $totalMem))"/> MB</td>
     </tr>
     <tr>
      <td class="odd-row">Maximum Page File Size</td>
      <td class="odd-row">
        <xsl:variable name="virtualMemMaxSize" select="normalize-space(//INSTANCE[@CLASSNAME='VirtualMemorySetting']/PROPERTY[@NAME='MaxSize']/VALUE)"/>
        <xsl:choose>
          <xsl:when test="$virtualMemMaxSize = '0'">System Managed</xsl:when>
          <xsl:when test="$virtualMemMaxSize = ''">Paging Disabled</xsl:when>
          <xsl:otherwise><xsl:value-of select="$virtualMemMaxSize"/> MB</xsl:otherwise>
  </xsl:choose>
      </td>
     </tr>
     <tr>
      <td class="even-row">Initial Page File Size</td>
      <td class="even-row">
        <xsl:variable name="virtualMemInitSize" select="normalize-space(//INSTANCE[@CLASSNAME='VirtualMemorySetting']/PROPERTY[@NAME='InitSize']/VALUE)"/>
        <xsl:choose>
          <xsl:when test="$virtualMemInitSize = 0">System Managed</xsl:when>
          <xsl:when test="$virtualMemInitSize = ''">Paging Disabled</xsl:when>
          <xsl:otherwise><xsl:value-of select="$virtualMemInitSize"/> MB</xsl:otherwise>
  </xsl:choose>
      </td>
     </tr>
     <tr>
      <td class="odd-row">Available Virtual Memory</td>
      <td class="odd-row"><xsl:value-of select="round(//INSTANCE[@CLASSNAME='WMIOperatingSystem']/PROPERTY[@NAME='FreeVirtualMemory']/VALUE div 1024)"/> MB</td>
     </tr>
     <tr>
      <td class="even-row">Total Virtual Memory</td>
      <td class="even-row"><xsl:value-of select="round(//INSTANCE[@CLASSNAME='WMIOperatingSystem']/PROPERTY[@NAME='TotalVirtualMemorySize']/VALUE div 1024)"/> MB</td>
     </tr>
     <tr>
      <td class="odd-row">Windows Directory</td>
      <td class="odd-row"><xsl:value-of select="//INSTANCE[@CLASSNAME='PCH_Sysinfo']/PROPERTY[@NAME='WindowsDirectory']/VALUE"/></td>
     </tr>
    </table>

    <table border="0" margin="0" width="100%" class="alternatelisttable">
     <tr>
      <td class="table-heading">My Disk Drives</td>
      <td class="table-heading"></td>
      <td class="table-heading"></td>
     </tr>
     <tr>
      <td class="table-heading">Drive</td>
      <td class="table-heading">Drive Size</td>
      <td class="table-heading">Percent Free</td>
     </tr>
      <xsl:for-each select="//INSTANCE[@CLASSNAME='LogicalDiskInfo']">
        <tr>

        <td>
          <xsl:if test="position() mod 2 = 1"><xsl:attribute name="class">odd-row</xsl:attribute></xsl:if>
          <xsl:if test="position() mod 2 = 0"><xsl:attribute name="class">even-row</xsl:attribute></xsl:if>
          <xsl:value-of select="PROPERTY[@NAME='DriveName']/VALUE"/>
        </td>
        <xsl:variable name="driveSize" select="PROPERTY[@NAME='TotalCapacity']/VALUE"/>
        <td>
          <xsl:if test="position() mod 2 = 1"><xsl:attribute name="class">odd-row</xsl:attribute></xsl:if>
          <xsl:if test="position() mod 2 = 0"><xsl:attribute name="class">even-row</xsl:attribute></xsl:if>
          <xsl:choose>
            <xsl:when test="$driveSize &gt; 1048576">
              <xsl:value-of select="round($driveSize div 1048576)"/> GB
            </xsl:when>
            <xsl:when test="$driveSize &gt; 1024">
              <xsl:value-of select="round($driveSize div 1024)"/> MB
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="round($driveSize)"/> KB
            </xsl:otherwise>
          </xsl:choose>
        </td>
        <td>
          <xsl:if test="position() mod 2 = 1"><xsl:attribute name="class">odd-row</xsl:attribute></xsl:if>
          <xsl:if test="position() mod 2 = 0"><xsl:attribute name="class">even-row</xsl:attribute></xsl:if>
          <xsl:value-of select="round(PROPERTY[@NAME='TotalFreeSpace']/VALUE div $driveSize * 100)"/>%
        </td>
        </tr>
      </xsl:for-each>
     </table>


  </div>


      <div id="network" name="Network Information" filename="Network Information" class="alternatelisttable">
        <table border="0" margin="0" width="100%">
          <tr>
            <td class="table-heading">My Network</td>
            <td class="table-heading"></td>
          </tr>
          <tr>
            <td class="odd-row" width="40%">Computer Name</td>
            <td class="odd-row">
              <xsl:value-of select="//INSTANCE[@CLASSNAME='SPRT_Connectivity']/PROPERTY[@NAME='NetBIOSName']/VALUE" />
            </td>
          </tr>
          <tr>
            <td class="even-row">DNS Name</td>
            <td class="even-row">
              <xsl:value-of select="//INSTANCE[@CLASSNAME='SPRT_Connectivity']/PROPERTY[@NAME='HostName']/VALUE" />
            </td>
          </tr>
          <tr>
            <td class="odd-row">Domain Name</td>
            <td class="odd-row">
              <xsl:variable name="domainRole" select="//INSTANCE[@CLASSNAME='WMIComputerSystem']/PROPERTY[@NAME='DomainRole']/VALUE"/>
              <xsl:choose>
                <xsl:when test="$domainRole != 0">
                    <xsl:value-of select="//INSTANCE[@CLASSNAME='WMIComputerSystem']/PROPERTY[@NAME='Domain']/VALUE" />
                </xsl:when>
                <xsl:otherwise>-</xsl:otherwise>
              </xsl:choose>
            </td>
          </tr>
          <tr>
            <td class="even-row">Workgroup</td>
            <td class="even-row">
              <xsl:choose>
                <xsl:when test="$domainRole = 0">
                    <xsl:value-of select="//INSTANCE[@CLASSNAME='WMIComputerSystem']/PROPERTY[@NAME='Domain']/VALUE" />
                </xsl:when>
                <xsl:otherwise>-</xsl:otherwise>
              </xsl:choose>
            </td>
          </tr>
          <tr>
            <td class="odd-row">User Name</td>
            <td class="odd-row">
              <xsl:value-of select="//INSTANCE[@CLASSNAME='SPRT_Connectivity']/PROPERTY[@NAME='UserName']/VALUE" />
            </td>
          </tr>
          <tr>
            <td class="even-row">DNS Servers</td>
            <td class="even-row">
              <xsl:call-template name="output-tokens">
                <xsl:with-param name="list"><xsl:value-of select="//INSTANCE[@CLASSNAME='EthernetInfo']/PROPERTY[@NAME='DnsUniqueServerList']/VALUE" /></xsl:with-param>
                <xsl:with-param name="delimiter">;</xsl:with-param>
              </xsl:call-template>
            </td>
          </tr>
          <tr>
            <td class="odd-row">TCP/IP Addresses</td>
            <td class="odd-row">
                <xsl:choose>
                  <xsl:when test ="normalize-space($tcpAddress) != '127.0.0.1'">
                    <xsl:call-template name="output-tokens">
                    <xsl:with-param name="list"><xsl:value-of select="normalize-space($tcpAddress)" /></xsl:with-param>
                      <xsl:with-param name="delimiter">;</xsl:with-param>
                    </xsl:call-template>
                  </xsl:when>
                  <xsl:otherwise>
                    -
                  </xsl:otherwise>
                </xsl:choose>
            </td>
          </tr>

          <tr>
            <td class="table-heading">Proxy Configuration</td>
            <td class="table-heading"></td>
          </tr>
          <tr>
            <td class="odd-row" width="40%">Proxy Enabled</td>
            <td class="odd-row">
              <xsl:variable name="ProxyEnabled" select="//INSTANCE[@CLASSNAME='ProxyInfo']/PROPERTY[@NAME='ProxyEnabled']/VALUE" />
              <xsl:choose>
          <xsl:when test="$ProxyEnabled = 1">
            True
          </xsl:when>
          <xsl:otherwise>
            False
          </xsl:otherwise>
        </xsl:choose>
            </td>
          </tr>
          <tr>
            <td class="even-row">Proxy Address</td>
            <td class="even-row">
              <xsl:call-template name="check-value">
                <xsl:with-param name="value"><xsl:value-of select="//INSTANCE[@CLASSNAME='ProxyInfo']/PROPERTY[@NAME='ProxyAddress']/VALUE" /></xsl:with-param>
              </xsl:call-template>
            </td>
          </tr>
          <tr>
            <td class="odd-row">Proxy Automatic Configuration Address</td>
            <td class="odd-row">
              <xsl:call-template name="check-value">
                <xsl:with-param name="value"><xsl:value-of select="//INSTANCE[@CLASSNAME='ProxyInfo']/PROPERTY[@NAME='ProxyAutoConfigAddress']/VALUE" /></xsl:with-param>
              </xsl:call-template>
            </td>
          </tr>
          <tr>
            <td class="even-row">Bypass Proxy for Local Addresses</td>
            <td class="even-row">
              <xsl:call-template name="check-value">
                <xsl:with-param name="value"><xsl:value-of select="//INSTANCE[@CLASSNAME='ProxyInfo']/PROPERTY[@NAME='ProxyBypassLocal']/VALUE" /></xsl:with-param>
              </xsl:call-template>
            </td>
          </tr>
          <tr>
            <td class="odd-row">Local Addresses to Bypass</td>
            <td class="odd-row">
              <xsl:call-template name="output-tokens">
                <xsl:with-param name="list"><xsl:value-of select="//INSTANCE[@CLASSNAME='ProxyInfo']/PROPERTY[@NAME='ProxyBypassLocalAddresses']/VALUE" /></xsl:with-param>
                <xsl:with-param name="delimiter">;</xsl:with-param>
              </xsl:call-template>
            </td>
          </tr>
        </table>

        <table border="0" margin="0" width="100%" class="alternatelisttable">
          <tr>
            <td class="table-heading">Network Adapter</td>
            <td class="table-heading"></td>
          </tr>
          <xsl:for-each select="//INSTANCE[@CLASSNAME='EthernetInfo']/PROPERTY[starts-with(@NAME,'Counter')]">
            <xsl:variable name="counter" select="./VALUE" />

            <tr>
              <td colspan="2">
                <strong><xsl:value-of select="../PROPERTY[@NAME=concat('Description',$counter)]" /></strong>
              </td>
            </tr>
            <tr>
              <td class="odd-row" width="40%">Type</td>
              <td class="odd-row">
                <xsl:call-template name="check-value">
                  <xsl:with-param name="value"><xsl:value-of select="../PROPERTY[@NAME=concat('Type',$counter)]" /></xsl:with-param>
                </xsl:call-template>
              </td>
            </tr>
            <tr>
              <td class="even-row">Connection Status</td>
              <td class="even-row">
                <xsl:choose>
            <xsl:when test="normalize-space(../PROPERTY[@NAME=concat('ConnectStatus',$counter)])='Connected'">
              Connected
            </xsl:when>
            <xsl:otherwise>
              Disconnected
            </xsl:otherwise>
          </xsl:choose>
              </td>
            </tr>
            <tr>
              <td class="odd-row">DHCP Enabled</td>
              <td class="odd-row">
                <xsl:call-template name="check-value">
                  <xsl:with-param name="value"><xsl:value-of select="../PROPERTY[@NAME=concat('DhcpEnabled',$counter)]" /></xsl:with-param>
                </xsl:call-template>
              </td>
            </tr>
            <tr>
              <td class="even-row">IP Addresses</td>
              <td class="even-row">
                <xsl:call-template name="check-value">
                  <xsl:with-param name="value"><xsl:value-of select="../PROPERTY[@NAME=concat('IPAddress',$counter)]" /></xsl:with-param>
                </xsl:call-template>
              </td>
            </tr>
            <tr>
              <td class="odd-row">Autoconfiguration Enabled</td>
              <td class="odd-row">
                <xsl:call-template name="check-value">
                  <xsl:with-param name="value"><xsl:value-of select="../PROPERTY[@NAME=concat('AutoConfigEnabled',$counter)]" /></xsl:with-param>
                </xsl:call-template>
              </td>
            </tr>
            <tr>
              <td class="even-row">Subnet Masks</td>
              <td class="even-row">
                <xsl:call-template name="check-value">
                  <xsl:with-param name="value"><xsl:value-of select="../PROPERTY[@NAME=concat('SubnetMask',$counter)]" /></xsl:with-param>
                </xsl:call-template>
              </td>
            </tr>
            <tr>
              <td class="odd-row">Default Gateway</td>
              <td class="odd-row">
                <xsl:call-template name="check-value">
                  <xsl:with-param name="value"><xsl:value-of select="../PROPERTY[@NAME=concat('DefaultGateway',$counter)]" /></xsl:with-param>
                </xsl:call-template>
              </td>
            </tr>
            <tr>
              <td class="even-row">DNS Servers</td>
              <td class="even-row">
                <xsl:call-template name="output-tokens">
                  <xsl:with-param name="list"><xsl:value-of select="../PROPERTY[@NAME=concat('DnsServerList',$counter)]" /></xsl:with-param>
                  <xsl:with-param name="delimiter">;</xsl:with-param>
                </xsl:call-template>
              </td>
            </tr>
            <tr>
              <td class="odd-row">WINS Enabled</td>
              <td class="odd-row">
                <xsl:call-template name="check-value">
                  <xsl:with-param name="value"><xsl:value-of select="../PROPERTY[@NAME=concat('HaveWins',$counter)]" /></xsl:with-param>
                </xsl:call-template>
              </td>
            </tr>
            <tr>
              <td class="even-row">WINS Servers</td>
              <td class="even-row">
                <xsl:call-template name="check-value">
                  <xsl:with-param name="value"><xsl:value-of select="../PROPERTY[@NAME=concat('PrimaryWinsServer',$counter)]" /></xsl:with-param>
                </xsl:call-template>

                <xsl:if test="normalize-space(../PROPERTY[@NAME=concat('SecondaryWinsServer',$counter)]) != ''">
                  <br/><xsl:value-of select="../PROPERTY[@NAME=concat('SecondaryWinsServer',$counter)]" />
                </xsl:if>

              </td>
            </tr>
            <tr>
              <td class="odd-row">Broadcast Address</td>
              <td class="odd-row">
                <xsl:call-template name="check-value">
                  <xsl:with-param name="value"><xsl:value-of select="../PROPERTY[@NAME=concat('BroadcastAddress',$counter)]" /></xsl:with-param>
                </xsl:call-template>
              </td>
            </tr>
            <tr>
              <td class="even-row">DHCP Server</td>
              <td class="even-row">
                <xsl:call-template name="check-value">
                  <xsl:with-param name="value"><xsl:value-of select="../PROPERTY[@NAME=concat('DhcpServer',$counter)]"/></xsl:with-param>
                </xsl:call-template>
              </td>
            </tr>
            <tr>
              <td class="odd-row">Subnet</td>
              <td class="odd-row">
                <xsl:call-template name="check-value">
                  <xsl:with-param name="value"><xsl:value-of select="../PROPERTY[@NAME=concat('Subnet',$counter)]" /></xsl:with-param>
                </xsl:call-template>
              </td>
            </tr>
            <tr>
              <td class="even-row">MAC Address</td>
              <td class="even-row">
                <xsl:call-template name="check-value">
                  <xsl:with-param name="value"><xsl:value-of select="../PROPERTY[@NAME=concat('PhysicalAddress',$counter)]" /></xsl:with-param>
                </xsl:call-template>
              </td>
            </tr>
            <tr>
              <td class="odd-row">Service Name</td>
              <td class="odd-row">
                <xsl:call-template name="check-value">
                  <xsl:with-param name="value"><xsl:value-of select="../PROPERTY[@NAME=concat('ServiceName',$counter)]" /></xsl:with-param>
                </xsl:call-template>
              </td>
            </tr>
          </xsl:for-each>
        </table>


        <table border="0" margin="0" width="100%" class="alternatelisttable">
          <tr>
            <td class="table-heading">Wireless Information</td>
            <td class="table-heading"></td>
          </tr>
          <tr>
            <td class="odd-row" width="40%">Wireless Connection Name</td>
            <td class="odd-row">
              <xsl:call-template name="check-value">
                <xsl:with-param name="value"><xsl:value-of select="//INSTANCE[@CLASSNAME='EthernetInfo']/PROPERTY[@NAME='WirelessConnectionName']/VALUE" /></xsl:with-param>
              </xsl:call-template>
            </td>
          </tr>
          <tr>
            <td class="even-row">Connection Status</td>
            <td class="even-row">
              <xsl:choose>
          <xsl:when test="normalize-space(//INSTANCE[@CLASSNAME='EthernetInfo']/PROPERTY[@NAME='WirelessConnectionStatus']/VALUE)='Connected'">
            Connected
          </xsl:when>
          <xsl:otherwise>
            Disconnected
          </xsl:otherwise>
        </xsl:choose>
            </td>
          </tr>
          <tr>
            <td class="odd-row">SSID</td>
            <td class="odd-row">
              <xsl:call-template name="check-value">
                <xsl:with-param name="value"><xsl:value-of select="//INSTANCE[@CLASSNAME='EthernetInfo']/PROPERTY[@NAME='WirelessSSID']/VALUE" /></xsl:with-param>
              </xsl:call-template>
            </td>
          </tr>
          <tr>
            <td class="even-row">BSSID</td>
            <td class="even-row">
              <xsl:call-template name="check-value">
                <xsl:with-param name="value"><xsl:value-of select="//INSTANCE[@CLASSNAME='EthernetInfo']/PROPERTY[@NAME='WirelessBSSID']/VALUE" /></xsl:with-param>
              </xsl:call-template>
            </td>
          </tr>
          <tr>
            <td class="odd-row">Authentication Mode</td>
            <td class="odd-row">
              <xsl:variable name="WirelessAuthMode" select="normalize-space(//INSTANCE[@CLASSNAME='EthernetInfo']/PROPERTY[@NAME='WirelessAuthMode']/VALUE)" />
              <xsl:choose>
                <xsl:when test="$WirelessAuthMode = ''">-</xsl:when>
          <xsl:when test="$WirelessAuthMode = 0">Open</xsl:when>
          <xsl:when test="$WirelessAuthMode = 1">Shared</xsl:when>
          <xsl:when test="$WirelessAuthMode = 2">Auto-Switch</xsl:when>
          <xsl:when test="$WirelessAuthMode = 3">WPA</xsl:when>
          <xsl:when test="$WirelessAuthMode = 4">WPA-PSK</xsl:when>
          <xsl:when test="$WirelessAuthMode = 5">WPANone</xsl:when>
          <xsl:when test="$WirelessAuthMode = 6">WPA2</xsl:when>
          <xsl:when test="$WirelessAuthMode = 7">WPA2-PSK</xsl:when>
          <xsl:otherwise>Unknown or Not Applicable</xsl:otherwise>
        </xsl:choose>

            </td>
          </tr>
          <tr>
            <td class="even-row">Encryption Type</td>
            <td class="even-row">
              <xsl:variable name="WirelessEncType" select="normalize-space(//INSTANCE[@CLASSNAME='EthernetInfo']/PROPERTY[@NAME='WirelessEncryptType']/VALUE)" />
              <xsl:choose>
                <xsl:when test="$WirelessEncType = ''">-</xsl:when>
          <xsl:when test="$WirelessEncType = 0">WEP</xsl:when>
          <xsl:when test="$WirelessEncType = 1">Disabled</xsl:when>
          <xsl:when test="$WirelessEncType = 2">Disabled</xsl:when>
          <xsl:when test="$WirelessEncType = 3">Not Supported</xsl:when>
          <xsl:when test="$WirelessEncType = 4">TKIP</xsl:when>
          <xsl:when test="$WirelessEncType = 5">TKIP</xsl:when>
          <xsl:when test="$WirelessEncType = 6">AES</xsl:when>
          <xsl:when test="$WirelessEncType = 7">AES</xsl:when>
          <xsl:otherwise>Unknown or Not Applicable</xsl:otherwise>
        </xsl:choose>
            </td>
          </tr>
          <tr>
            <td class="odd-row">Channel</td>
            <td class="odd-row">
              <xsl:call-template name="check-value">
                <xsl:with-param name="value"><xsl:value-of select="//INSTANCE[@CLASSNAME='EthernetInfo']/PROPERTY[@NAME='WirelessChannel']/VALUE" /></xsl:with-param>
              </xsl:call-template>
            </td>
          </tr>
          <tr>
            <td class="even-row">Signal Strength</td>
            <td class="even-row">
              <xsl:variable name="WirelessSignalStrength" select="normalize-space(//INSTANCE[@CLASSNAME='EthernetInfo']/PROPERTY[@NAME='WirelessSignalStrength']/VALUE)" />
              <xsl:choose>
                <xsl:when test="$WirelessSignalStrength = ''">-</xsl:when>
          <xsl:when test="$WirelessSignalStrength &lt; -90">No Signal</xsl:when>
          <xsl:when test="$WirelessSignalStrength &lt; -81">Very Low</xsl:when>
          <xsl:when test="$WirelessSignalStrength &lt; -71">Low</xsl:when>
          <xsl:when test="$WirelessSignalStrength &lt; -67">Good</xsl:when>
          <xsl:when test="$WirelessSignalStrength &lt; -57">Very Good</xsl:when>
          <xsl:when test="$WirelessSignalStrength &lt; 0">Excellent</xsl:when>
          <xsl:otherwise>Unknown or Not Applicable</xsl:otherwise>
        </xsl:choose>
            </td>
          </tr>
          <tr>
            <td class="odd-row">Link Speed</td>
            <td class="odd-row">
              <xsl:call-template name="check-value">
                <xsl:with-param name="value"><xsl:value-of select="//INSTANCE[@CLASSNAME='EthernetInfo']/PROPERTY[@NAME='WirelessLinkSpeed']/VALUE" /></xsl:with-param>
                <xsl:with-param name="postfix"> Mbit/s</xsl:with-param>
              </xsl:call-template>
            </td>
          </tr>
          <tr>
            <td class="even-row">Supported Rates</td>
            <td class="even-row">
              <xsl:call-template name="check-value">
                <xsl:with-param name="value"><xsl:value-of select="//INSTANCE[@CLASSNAME='EthernetInfo']/PROPERTY[@NAME='WirelessSupportedRates']/VALUE" /></xsl:with-param>
                <xsl:with-param name="postfix"> Mbit/s</xsl:with-param>
              </xsl:call-template>
            </td>
          </tr>
          <tr>
            <td class="odd-row">Infrastructure Mode</td>
            <td class="odd-row">
              <xsl:variable name="WirelessInfrastructure" select="normalize-space(//INSTANCE[@CLASSNAME='EthernetInfo']/PROPERTY[@NAME='WirelessInfrastructure']/VALUE)" />
              <xsl:choose>
                <xsl:when test="$WirelessInfrastructure = ''">-</xsl:when>
                <xsl:when test="$WirelessInfrastructure = 0">Ad-hoc</xsl:when>
                <xsl:when test="$WirelessInfrastructure = 1">Infrastructure</xsl:when>
                <xsl:when test="$WirelessInfrastructure = 2">Auto</xsl:when>
                <xsl:otherwise>Unknown or Not Applicable</xsl:otherwise>
              </xsl:choose>
            </td>
          </tr>
        </table>
      </div>


  <div id="environmentvariables" name="Environment Variables" filename="Environment Variables">
    <xsl:variable name="vLowercaseChars_CONST" select="'abcdefghijklmnopqrstuvwxyz'"/>
    <xsl:variable name="vUppercaseChars_CONST" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'"/>
    <table border="0" margin="0" width="100%" class="alternatelisttable">
    <tr>
      <td class="table-heading">Environment Variable</td>
      <td class="table-heading">Value</td>
     </tr>
     <xsl:for-each select="//INSTANCE[@CLASSNAME='SPRT_Environments']">
       <tr>
         <td>
           <xsl:if test="position() mod 2 = 1"><xsl:attribute name="class">odd-row</xsl:attribute></xsl:if>
           <xsl:if test="position() mod 2 = 0"><xsl:attribute name="class">even-row</xsl:attribute></xsl:if>
           <xsl:value-of select="translate(./PROPERTY[@NAME='EnvVar']/VALUE, $vLowercaseChars_CONST , $vUppercaseChars_CONST)"/>
         </td>
         <td>
           <xsl:if test="position() mod 2 = 1"><xsl:attribute name="class">odd-row</xsl:attribute></xsl:if>
           <xsl:if test="position() mod 2 = 0"><xsl:attribute name="class">even-row</xsl:attribute></xsl:if>
           <xsl:value-of select="./PROPERTY[@NAME='Value']/VALUE" />
         </td>
       </tr>
     </xsl:for-each>
   </table>
  </div>


  <div id="printerlist" name="Printer List" filename="Printer List">
    <table border="0" margin="0" width="100%" class="alternatelisttable">
    <tr>
      <td class="table-heading">Printer</td>
      <td class="table-heading">Server Name</td>
      <td class="table-heading">Type</td>
     </tr>

     <xsl:if test="count(//INSTANCE[@CLASSNAME='SPRT_Printers']) &lt; 1">
       <tr><td colspan="3"><br/><center>No Printer Found</center></td></tr>
     </xsl:if>

     <xsl:for-each select="//INSTANCE[@CLASSNAME='SPRT_Printers']">
       <tr>
         <td>
           <xsl:if test="position() mod 2 = 1"><xsl:attribute name="class">odd-row</xsl:attribute></xsl:if>
           <xsl:if test="position() mod 2 = 0"><xsl:attribute name="class">even-row</xsl:attribute></xsl:if>
           <xsl:value-of select="./PROPERTY[@NAME='Printer']/VALUE" />
         </td>
         <td>
            <xsl:if test="position() mod 2 = 1"><xsl:attribute name="class">odd-row</xsl:attribute></xsl:if>
            <xsl:if test="position() mod 2 = 0"><xsl:attribute name="class">even-row</xsl:attribute></xsl:if>
            <xsl:call-template name="check-value">
              <xsl:with-param name="value"><xsl:value-of select="./PROPERTY[@NAME='Server']/VALUE"/></xsl:with-param>
              <xsl:with-param name="center-on-blank">true</xsl:with-param>
            </xsl:call-template>
         </td>
         <td>
           <xsl:if test="position() mod 2 = 1"><xsl:attribute name="class">odd-row</xsl:attribute></xsl:if>
           <xsl:if test="position() mod 2 = 0"><xsl:attribute name="class">even-row</xsl:attribute></xsl:if>
           <xsl:value-of select="./PROPERTY[@NAME='Type']/VALUE" />
         </td>
       </tr>
     </xsl:for-each>
   </table>
  </div>


  <div id="runningprograms" name="Running Programs" filename="Running Programs">
    <table border="0" margin="0" width="100%" class="alternatelisttable">
    <tr>
      <td class="table-heading">Program</td>
      <td class="table-heading">PID</td>
      <td class="table-heading">Product Name</td>
      <td class="table-heading">Version</td>
     </tr>
     <xsl:for-each select="//INSTANCE[@CLASSNAME='SPRT_Applications']">
       <tr>
         <td>
    <xsl:variable name="ProgName">
            <xsl:call-template name="substring-before-last">
              <xsl:with-param name="input"><xsl:value-of select="./PROPERTY[@NAME='Application']/VALUE"/>
              </xsl:with-param>
              <xsl:with-param name="substr">(</xsl:with-param>
            </xsl:call-template>
          </xsl:variable>
           <xsl:if test="position() mod 2 = 1"><xsl:attribute name="class">odd-row</xsl:attribute></xsl:if>
           <xsl:if test="position() mod 2 = 0"><xsl:attribute name="class">even-row</xsl:attribute></xsl:if>
     <xsl:choose>
       <xsl:when test="starts-with($ProgName,'\??\')"><xsl:value-of select="substring-after($ProgName,'\??\')" /></xsl:when>
       <xsl:otherwise><xsl:value-of select="$ProgName" /></xsl:otherwise>
     </xsl:choose>
         </td>
         <td>
           <xsl:if test="position() mod 2 = 1"><xsl:attribute name="class">odd-row</xsl:attribute></xsl:if>
           <xsl:if test="position() mod 2 = 0"><xsl:attribute name="class">even-row</xsl:attribute></xsl:if>
           <xsl:value-of select="./PROPERTY[@NAME='PID']/VALUE" />
         </td>
         <td>
           <xsl:if test="position() mod 2 = 1"><xsl:attribute name="class">odd-row</xsl:attribute></xsl:if>
           <xsl:if test="position() mod 2 = 0"><xsl:attribute name="class">even-row</xsl:attribute></xsl:if>
           <xsl:call-template name="check-value">
             <xsl:with-param name="value"><xsl:value-of select="./PROPERTY[@NAME='ProductName']/VALUE" /></xsl:with-param>
             <xsl:with-param name="center-on-blank">true</xsl:with-param>
           </xsl:call-template>
         </td>
         <td>
           <xsl:if test="position() mod 2 = 1"><xsl:attribute name="class">odd-row</xsl:attribute></xsl:if>
           <xsl:if test="position() mod 2 = 0"><xsl:attribute name="class">even-row</xsl:attribute></xsl:if>
           <xsl:call-template name="check-value">
             <xsl:with-param name="value"><xsl:value-of select="./PROPERTY[@NAME='ProductVersion']/VALUE" /></xsl:with-param>
             <xsl:with-param name="center-on-blank">true</xsl:with-param>
           </xsl:call-template>
         </td>
       </tr>
     </xsl:for-each>
   </table>
  </div>


</div>
</xsl:template>


<xsl:template name="check-value">
  <xsl:param name="value" />
  <xsl:param name="center-on-blank" />
  <xsl:param name="postfix" />
  <xsl:choose>
    <xsl:when test="normalize-space($value)='' and $center-on-blank='true'">
      <center>-</center>
    </xsl:when>
    <xsl:when test="normalize-space($value)=''">
      -
    </xsl:when>
    <xsl:otherwise>
      <xsl:value-of select="$value" /> <xsl:value-of select="$postfix" />
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>


<xsl:template name="output-tokens">
  <xsl:param name="list" />
  <xsl:param name="delimiter" />
  <xsl:variable name="newlist">
    <xsl:choose>
      <xsl:when test="contains($list, $delimiter)"><xsl:value-of select="normalize-space($list)" /></xsl:when>
      <xsl:otherwise><xsl:value-of select="concat(normalize-space($list), $delimiter)"/></xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  <xsl:variable name="first" select="substring-before($newlist, $delimiter)" />
  <xsl:variable name="remaining" select="substring-after($newlist, $delimiter)" />
  <xsl:choose>
    <xsl:when test="normalize-space($first)=''">
      -
    </xsl:when>
    <xsl:otherwise>
      <xsl:value-of select="$first" /><br/>
    </xsl:otherwise>
  </xsl:choose>
  <xsl:if test="$remaining">
    <xsl:call-template name="output-tokens">
      <xsl:with-param name="list" select="$remaining" />
      <xsl:with-param name="delimiter"><xsl:value-of select="$delimiter"/></xsl:with-param>
    </xsl:call-template>
  </xsl:if>
</xsl:template>

<xsl:template name="substring-before-last">
  <xsl:param name="input" />
  <xsl:param name="substr" />
  <xsl:if test="$substr and contains($input, $substr)">
    <xsl:variable name="temp" select="substring-after($input, $substr)" />
    <xsl:value-of select="substring-before($input, $substr)" />
    <xsl:if test="contains($temp, $substr)">
      <xsl:value-of select="$substr" />
      <xsl:call-template name="substring-before-last">
        <xsl:with-param name="input" select="$temp" />
        <xsl:with-param name="substr" select="$substr" />
      </xsl:call-template>
    </xsl:if>
  </xsl:if>
</xsl:template>


<xsl:template name="getLocaleName">
  <xsl:param name="localeID" />
  <xsl:choose>
    <xsl:when test="$localeID='00000036'">Afrikaans</xsl:when>
    <xsl:when test="$localeID='00000436'">Afrikaans (South Africa)</xsl:when>
    <xsl:when test="$localeID='0000001C'">Albanian</xsl:when>
    <xsl:when test="$localeID='0000041C'">Albanian (Albania)</xsl:when>
    <xsl:when test="$localeID='00000484'">Alsatian (France)</xsl:when>
    <xsl:when test="$localeID='0000045E'">Amharic (Ethiopia)</xsl:when>
    <xsl:when test="$localeID='00000001'">Arabic</xsl:when>
    <xsl:when test="$localeID='00001401'">Arabic (Algeria)</xsl:when>
    <xsl:when test="$localeID='00003C01'">Arabic (Bahrain)</xsl:when>
    <xsl:when test="$localeID='00000C01'">Arabic (Egypt)</xsl:when>
    <xsl:when test="$localeID='00000801'">Arabic (Iraq)</xsl:when>
    <xsl:when test="$localeID='00002C01'">Arabic (Jordan)</xsl:when>
    <xsl:when test="$localeID='00003401'">Arabic (Kuwait)</xsl:when>
    <xsl:when test="$localeID='00003001'">Arabic (Lebanon)</xsl:when>
    <xsl:when test="$localeID='00001001'">Arabic (Libya)</xsl:when>
    <xsl:when test="$localeID='00001801'">Arabic (Morocco)</xsl:when>
    <xsl:when test="$localeID='00002001'">Arabic (Oman)</xsl:when>
    <xsl:when test="$localeID='00004001'">Arabic (Qatar)</xsl:when>
    <xsl:when test="$localeID='00000401'">Arabic (Saudi Arabia)</xsl:when>
    <xsl:when test="$localeID='00002801'">Arabic (Syria)</xsl:when>
    <xsl:when test="$localeID='00001C01'">Arabic (Tunisia)</xsl:when>
    <xsl:when test="$localeID='00003801'">Arabic (U.A.E.)</xsl:when>
    <xsl:when test="$localeID='00002401'">Arabic (Yemen)</xsl:when>
    <xsl:when test="$localeID='0000002B'">Armenian</xsl:when>
    <xsl:when test="$localeID='0000042B'">Armenian (Armenia)</xsl:when>
    <xsl:when test="$localeID='0000044D'">Assamese (India)</xsl:when>
    <xsl:when test="$localeID='0000002C'">Azeri</xsl:when>
    <xsl:when test="$localeID='0000082C'">Azeri (Cyrillic, Azerbaijan)</xsl:when>
    <xsl:when test="$localeID='0000042C'">Azeri (Latin, Azerbaijan)</xsl:when>
    <xsl:when test="$localeID='0000046D'">Bashkir (Russia)</xsl:when>
    <xsl:when test="$localeID='0000002D'">Basque</xsl:when>
    <xsl:when test="$localeID='0000042D'">Basque (Basque)</xsl:when>
    <xsl:when test="$localeID='00000023'">Belarusian</xsl:when>
    <xsl:when test="$localeID='00000423'">Belarusian (Belarus)</xsl:when>
    <xsl:when test="$localeID='00000845'">Bengali (Bangladesh)</xsl:when>
    <xsl:when test="$localeID='00000445'">Bengali (India)</xsl:when>
    <xsl:when test="$localeID='0000201A'">Bosnian (Cyrillic, Bosnia and Herzegovina)</xsl:when>
    <xsl:when test="$localeID='0000141A'">Bosnian (Latin, Bosnia and Herzegovina)</xsl:when>
    <xsl:when test="$localeID='0000047E'">Breton (France)</xsl:when>
    <xsl:when test="$localeID='00000002'">Bulgarian</xsl:when>
    <xsl:when test="$localeID='00000402'">Bulgarian (Bulgaria)</xsl:when>
    <xsl:when test="$localeID='00000003'">Catalan</xsl:when>
    <xsl:when test="$localeID='00000403'">Catalan (Catalan)</xsl:when>
    <xsl:when test="$localeID='00000C04'">Chinese (Hong Kong S.A.R.)</xsl:when>
    <xsl:when test="$localeID='00001404'">Chinese (Macao S.A.R.)</xsl:when>
    <xsl:when test="$localeID='00000804'">Chinese (People's Republic of China)</xsl:when>
    <xsl:when test="$localeID='00000004'">Chinese (Simplified)</xsl:when>
    <xsl:when test="$localeID='00001004'">Chinese (Singapore)</xsl:when>
    <xsl:when test="$localeID='00000404'">Chinese (Taiwan)</xsl:when>
    <xsl:when test="$localeID='00007C04'">Chinese (Traditional)</xsl:when>
    <xsl:when test="$localeID='00000483'">Corsican (France)</xsl:when>
    <xsl:when test="$localeID='0000001A'">Croatian</xsl:when>
    <xsl:when test="$localeID='0000041A'">Croatian (Croatia)</xsl:when>
    <xsl:when test="$localeID='0000101A'">Croatian (Latin, Bosnia and Herzegovina)</xsl:when>
    <xsl:when test="$localeID='00000005'">Czech</xsl:when>
    <xsl:when test="$localeID='00000405'">Czech (Czech Republic)</xsl:when>
    <xsl:when test="$localeID='00000006'">Danish</xsl:when>
    <xsl:when test="$localeID='00000406'">Danish (Denmark)</xsl:when>
    <xsl:when test="$localeID='0000048C'">Dari (Afghanistan)</xsl:when>
    <xsl:when test="$localeID='00000065'">Divehi</xsl:when>
    <xsl:when test="$localeID='00000465'">Divehi (Maldives)</xsl:when>
    <xsl:when test="$localeID='00000013'">Dutch</xsl:when>
    <xsl:when test="$localeID='00000813'">Dutch (Belgium)</xsl:when>
    <xsl:when test="$localeID='00000413'">Dutch (Netherlands)</xsl:when>
    <xsl:when test="$localeID='00000009'">English</xsl:when>
    <xsl:when test="$localeID='00000C09'">English (Australia)</xsl:when>
    <xsl:when test="$localeID='00002809'">English (Belize)</xsl:when>
    <xsl:when test="$localeID='00001009'">English (Canada)</xsl:when>
    <xsl:when test="$localeID='00002409'">English (Caribbean)</xsl:when>
    <xsl:when test="$localeID='00004009'">English (India)</xsl:when>
    <xsl:when test="$localeID='00001809'">English (Ireland)</xsl:when>
    <xsl:when test="$localeID='00002009'">English (Jamaica)</xsl:when>
    <xsl:when test="$localeID='00004409'">English (Malaysia)</xsl:when>
    <xsl:when test="$localeID='00001409'">English (New Zealand)</xsl:when>
    <xsl:when test="$localeID='00003409'">English (Republic of the Philippines)</xsl:when>
    <xsl:when test="$localeID='00004809'">English (Singapore)</xsl:when>
    <xsl:when test="$localeID='00001C09'">English (South Africa)</xsl:when>
    <xsl:when test="$localeID='00002C09'">English (Trinidad and Tobago)</xsl:when>
    <xsl:when test="$localeID='00000809'">English (United Kingdom)</xsl:when>
    <xsl:when test="$localeID='00000409'">English (United States)</xsl:when>
    <xsl:when test="$localeID='00003009'">English (Zimbabwe)</xsl:when>
    <xsl:when test="$localeID='00000025'">Estonian</xsl:when>
    <xsl:when test="$localeID='00000425'">Estonian (Estonia)</xsl:when>
    <xsl:when test="$localeID='00000038'">Faroese</xsl:when>
    <xsl:when test="$localeID='00000438'">Faroese (Faroe Islands)</xsl:when>
    <xsl:when test="$localeID='00000464'">Filipino (Philippines)</xsl:when>
    <xsl:when test="$localeID='0000000B'">Finnish</xsl:when>
    <xsl:when test="$localeID='0000040B'">Finnish (Finland)</xsl:when>
    <xsl:when test="$localeID='0000000C'">French</xsl:when>
    <xsl:when test="$localeID='0000080C'">French (Belgium)</xsl:when>
    <xsl:when test="$localeID='00000C0C'">French (Canada)</xsl:when>
    <xsl:when test="$localeID='0000040C'">French (France)</xsl:when>
    <xsl:when test="$localeID='0000140C'">French (Luxembourg)</xsl:when>
    <xsl:when test="$localeID='0000180C'">French (Principality of Monaco)</xsl:when>
    <xsl:when test="$localeID='0000100C'">French (Switzerland)</xsl:when>
    <xsl:when test="$localeID='00000462'">Frisian (Netherlands)</xsl:when>
    <xsl:when test="$localeID='00000056'">Galician</xsl:when>
    <xsl:when test="$localeID='00000456'">Galician (Galician)</xsl:when>
    <xsl:when test="$localeID='00000037'">Georgian</xsl:when>
    <xsl:when test="$localeID='00000437'">Georgian (Georgia)</xsl:when>
    <xsl:when test="$localeID='00000007'">German</xsl:when>
    <xsl:when test="$localeID='00000C07'">German (Austria)</xsl:when>
    <xsl:when test="$localeID='00000407'">German (Germany)</xsl:when>
    <xsl:when test="$localeID='00001407'">German (Liechtenstein)</xsl:when>
    <xsl:when test="$localeID='00001007'">German (Luxembourg)</xsl:when>
    <xsl:when test="$localeID='00000807'">German (Switzerland)</xsl:when>
    <xsl:when test="$localeID='00000008'">Greek</xsl:when>
    <xsl:when test="$localeID='00000408'">Greek (Greece)</xsl:when>
    <xsl:when test="$localeID='0000046F'">Greenlandic (Greenland)</xsl:when>
    <xsl:when test="$localeID='00000047'">Gujarati</xsl:when>
    <xsl:when test="$localeID='00000447'">Gujarati (India)</xsl:when>
    <xsl:when test="$localeID='00000468'">Hausa (Latin, Nigeria)</xsl:when>
    <xsl:when test="$localeID='0000000D'">Hebrew</xsl:when>
    <xsl:when test="$localeID='0000040D'">Hebrew (Israel)</xsl:when>
    <xsl:when test="$localeID='00000039'">Hindi</xsl:when>
    <xsl:when test="$localeID='00000439'">Hindi (India)</xsl:when>
    <xsl:when test="$localeID='0000000E'">Hungarian</xsl:when>
    <xsl:when test="$localeID='0000040E'">Hungarian (Hungary)</xsl:when>
    <xsl:when test="$localeID='0000000F'">Icelandic</xsl:when>
    <xsl:when test="$localeID='0000040F'">Icelandic (Iceland)</xsl:when>
    <xsl:when test="$localeID='00000470'">Igbo (Nigeria)</xsl:when>
    <xsl:when test="$localeID='00000021'">Indonesian</xsl:when>
    <xsl:when test="$localeID='00000421'">Indonesian (Indonesia)</xsl:when>
    <xsl:when test="$localeID='0000085D'">Inuktitut (Latin, Canada)</xsl:when>
    <xsl:when test="$localeID='0000045D'">Inuktitut (Syllabics, Canada)</xsl:when>
    <xsl:when test="$localeID='0000083C'">Irish (Ireland)</xsl:when>
    <xsl:when test="$localeID='00000434'">isiXhosa (South Africa)</xsl:when>
    <xsl:when test="$localeID='00000435'">isiZulu (South Africa)</xsl:when>
    <xsl:when test="$localeID='00000010'">Italian</xsl:when>
    <xsl:when test="$localeID='00000410'">Italian (Italy)</xsl:when>
    <xsl:when test="$localeID='00000810'">Italian (Switzerland)</xsl:when>
    <xsl:when test="$localeID='00000011'">Japanese</xsl:when>
    <xsl:when test="$localeID='00000411'">Japanese (Japan)</xsl:when>
    <xsl:when test="$localeID='0000004B'">Kannada</xsl:when>
    <xsl:when test="$localeID='0000044B'">Kannada (India)</xsl:when>
    <xsl:when test="$localeID='0000003F'">Kazakh</xsl:when>
    <xsl:when test="$localeID='0000043F'">Kazakh (Kazakhstan)</xsl:when>
    <xsl:when test="$localeID='00000453'">Khmer (Cambodia)</xsl:when>
    <xsl:when test="$localeID='00000486'">K'iche (Guatemala)</xsl:when>
    <xsl:when test="$localeID='00000487'">Kinyarwanda (Rwanda)</xsl:when>
    <xsl:when test="$localeID='00000041'">Kiswahili</xsl:when>
    <xsl:when test="$localeID='00000441'">Kiswahili (Kenya)</xsl:when>
    <xsl:when test="$localeID='00000057'">Konkani</xsl:when>
    <xsl:when test="$localeID='00000457'">Konkani (India)</xsl:when>
    <xsl:when test="$localeID='00000012'">Korean</xsl:when>
    <xsl:when test="$localeID='00000412'">Korean (Korea)</xsl:when>
    <xsl:when test="$localeID='00000040'">Kyrgyz</xsl:when>
    <xsl:when test="$localeID='00000440'">Kyrgyz (Kyrgyzstan)</xsl:when>
    <xsl:when test="$localeID='00000454'">Lao (Lao P.D.R.)</xsl:when>
    <xsl:when test="$localeID='00000026'">Latvian</xsl:when>
    <xsl:when test="$localeID='00000426'">Latvian (Latvia)</xsl:when>
    <xsl:when test="$localeID='00000027'">Lithuanian</xsl:when>
    <xsl:when test="$localeID='00000427'">Lithuanian (Lithuania)</xsl:when>
    <xsl:when test="$localeID='0000082E'">Lower Sorbian (Germany)</xsl:when>
    <xsl:when test="$localeID='0000046E'">Luxembourgish (Luxembourg)</xsl:when>
    <xsl:when test="$localeID='0000002F'">Macedonian</xsl:when>
    <xsl:when test="$localeID='0000042F'">Macedonian (Former Yugoslav Republic of Macedonia)</xsl:when>
    <xsl:when test="$localeID='0000003E'">Malay</xsl:when>
    <xsl:when test="$localeID='0000083E'">Malay (Brunei Darussalam)</xsl:when>
    <xsl:when test="$localeID='0000043E'">Malay (Malaysia)</xsl:when>
    <xsl:when test="$localeID='0000044C'">Malayalam (India)</xsl:when>
    <xsl:when test="$localeID='0000043A'">Maltese (Malta)</xsl:when>
    <xsl:when test="$localeID='00000481'">Maori (New Zealand)</xsl:when>
    <xsl:when test="$localeID='0000047A'">Mapudungun (Chile)</xsl:when>
    <xsl:when test="$localeID='0000004E'">Marathi</xsl:when>
    <xsl:when test="$localeID='0000044E'">Marathi (India)</xsl:when>
    <xsl:when test="$localeID='0000047C'">Mohawk (Mohawk)</xsl:when>
    <xsl:when test="$localeID='00000050'">Mongolian</xsl:when>
    <xsl:when test="$localeID='00000450'">Mongolian (Cyrillic, Mongolia)</xsl:when>
    <xsl:when test="$localeID='00000850'">Mongolian (Traditional Mongolian, PRC)</xsl:when>
    <xsl:when test="$localeID='00000461'">Nepali (Nepal)</xsl:when>
    <xsl:when test="$localeID='00000014'">Norwegian</xsl:when>
    <xsl:when test="$localeID='00000414'">Norwegian, Bokm�l (Norway)</xsl:when>
    <xsl:when test="$localeID='00000814'">Norwegian, Nynorsk (Norway)</xsl:when>
    <xsl:when test="$localeID='00000482'">Occitan (France)</xsl:when>
    <xsl:when test="$localeID='00000448'">Oriya (India)</xsl:when>
    <xsl:when test="$localeID='00000463'">Pashto (Afghanistan)</xsl:when>
    <xsl:when test="$localeID='00000029'">Persian</xsl:when>
    <xsl:when test="$localeID='00000429'">Persian</xsl:when>
    <xsl:when test="$localeID='00000015'">Polish</xsl:when>
    <xsl:when test="$localeID='00000415'">Polish (Poland)</xsl:when>
    <xsl:when test="$localeID='00000016'">Portuguese</xsl:when>
    <xsl:when test="$localeID='00000416'">Portuguese (Brazil)</xsl:when>
    <xsl:when test="$localeID='00000816'">Portuguese (Portugal)</xsl:when>
    <xsl:when test="$localeID='00000046'">Punjabi</xsl:when>
    <xsl:when test="$localeID='00000446'">Punjabi (India)</xsl:when>
    <xsl:when test="$localeID='0000046B'">Quechua (Bolivia)</xsl:when>
    <xsl:when test="$localeID='0000086B'">Quechua (Ecuador)</xsl:when>
    <xsl:when test="$localeID='00000C6B'">Quechua (Peru)</xsl:when>
    <xsl:when test="$localeID='00000018'">Romanian</xsl:when>
    <xsl:when test="$localeID='00000418'">Romanian (Romania)</xsl:when>
    <xsl:when test="$localeID='00000417'">Romansh (Switzerland)</xsl:when>
    <xsl:when test="$localeID='00000019'">Russian</xsl:when>
    <xsl:when test="$localeID='00000419'">Russian (Russia)</xsl:when>
    <xsl:when test="$localeID='0000243B'">Sami, Inari (Finland)</xsl:when>
    <xsl:when test="$localeID='0000103B'">Sami, Lule (Norway)</xsl:when>
    <xsl:when test="$localeID='0000143B'">Sami, Lule (Sweden)</xsl:when>
    <xsl:when test="$localeID='00000C3B'">Sami, Northern (Finland)</xsl:when>
    <xsl:when test="$localeID='0000043B'">Sami, Northern (Norway)</xsl:when>
    <xsl:when test="$localeID='0000083B'">Sami, Northern (Sweden)</xsl:when>
    <xsl:when test="$localeID='0000203B'">Sami, Skolt (Finland)</xsl:when>
    <xsl:when test="$localeID='0000183B'">Sami, Southern (Norway)</xsl:when>
    <xsl:when test="$localeID='00001C3B'">Sami, Southern (Sweden)</xsl:when>
    <xsl:when test="$localeID='0000004F'">Sanskrit</xsl:when>
    <xsl:when test="$localeID='0000044F'">Sanskrit (India)</xsl:when>
    <xsl:when test="$localeID='00007C1A'">Serbian</xsl:when>
    <xsl:when test="$localeID='00001C1A'">Serbian (Cyrillic, Bosnia and Herzegovina)</xsl:when>
    <xsl:when test="$localeID='00000C1A'">Serbian (Cyrillic, Serbia)</xsl:when>
    <xsl:when test="$localeID='0000181A'">Serbian (Latin, Bosnia and Herzegovina)</xsl:when>
    <xsl:when test="$localeID='0000081A'">Serbian (Latin, Serbia)</xsl:when>
    <xsl:when test="$localeID='0000046C'">Sesotho sa Leboa (South Africa)</xsl:when>
    <xsl:when test="$localeID='00000432'">Setswana (South Africa)</xsl:when>
    <xsl:when test="$localeID='0000045B'">Sinhala (Sri Lanka)</xsl:when>
    <xsl:when test="$localeID='0000001B'">Slovak</xsl:when>
    <xsl:when test="$localeID='0000041B'">Slovak (Slovakia)</xsl:when>
    <xsl:when test="$localeID='00000024'">Slovenian</xsl:when>
    <xsl:when test="$localeID='00000424'">Slovenian (Slovenia)</xsl:when>
    <xsl:when test="$localeID='0000000A'">Spanish</xsl:when>
    <xsl:when test="$localeID='00002C0A'">Spanish (Argentina)</xsl:when>
    <xsl:when test="$localeID='0000400A'">Spanish (Bolivia)</xsl:when>
    <xsl:when test="$localeID='0000340A'">Spanish (Chile)</xsl:when>
    <xsl:when test="$localeID='0000240A'">Spanish (Colombia)</xsl:when>
    <xsl:when test="$localeID='0000140A'">Spanish (Costa Rica)</xsl:when>
    <xsl:when test="$localeID='00001C0A'">Spanish (Dominican Republic)</xsl:when>
    <xsl:when test="$localeID='0000300A'">Spanish (Ecuador)</xsl:when>
    <xsl:when test="$localeID='0000440A'">Spanish (El Salvador)</xsl:when>
    <xsl:when test="$localeID='0000100A'">Spanish (Guatemala)</xsl:when>
    <xsl:when test="$localeID='0000480A'">Spanish (Honduras)</xsl:when>
    <xsl:when test="$localeID='0000080A'">Spanish (Mexico)</xsl:when>
    <xsl:when test="$localeID='00004C0A'">Spanish (Nicaragua)</xsl:when>
    <xsl:when test="$localeID='0000180A'">Spanish (Panama)</xsl:when>
    <xsl:when test="$localeID='00003C0A'">Spanish (Paraguay)</xsl:when>
    <xsl:when test="$localeID='0000280A'">Spanish (Peru)</xsl:when>
    <xsl:when test="$localeID='0000500A'">Spanish (Puerto Rico)</xsl:when>
    <xsl:when test="$localeID='00000C0A'">Spanish (Spain)</xsl:when>
    <xsl:when test="$localeID='0000540A'">Spanish (United States)</xsl:when>
    <xsl:when test="$localeID='0000380A'">Spanish (Uruguay)</xsl:when>
    <xsl:when test="$localeID='0000200A'">Spanish (Venezuela)</xsl:when>
    <xsl:when test="$localeID='0000001D'">Swedish</xsl:when>
    <xsl:when test="$localeID='0000081D'">Swedish (Finland)</xsl:when>
    <xsl:when test="$localeID='0000041D'">Swedish (Sweden)</xsl:when>
    <xsl:when test="$localeID='0000005A'">Syriac</xsl:when>
    <xsl:when test="$localeID='0000045A'">Syriac (Syria)</xsl:when>
    <xsl:when test="$localeID='00000428'">Tajik (Cyrillic, Tajikistan)</xsl:when>
    <xsl:when test="$localeID='0000085F'">Tamazight (Latin, Algeria)</xsl:when>
    <xsl:when test="$localeID='00000049'">Tamil</xsl:when>
    <xsl:when test="$localeID='00000449'">Tamil (India)</xsl:when>
    <xsl:when test="$localeID='00000044'">Tatar</xsl:when>
    <xsl:when test="$localeID='00000444'">Tatar (Russia)</xsl:when>
    <xsl:when test="$localeID='0000004A'">Telugu</xsl:when>
    <xsl:when test="$localeID='0000044A'">Telugu (India)</xsl:when>
    <xsl:when test="$localeID='0000001E'">Thai</xsl:when>
    <xsl:when test="$localeID='0000041E'">Thai (Thailand)</xsl:when>
    <xsl:when test="$localeID='00000451'">Tibetan (PRC)</xsl:when>
    <xsl:when test="$localeID='0000001F'">Turkish</xsl:when>
    <xsl:when test="$localeID='0000041F'">Turkish (Turkey)</xsl:when>
    <xsl:when test="$localeID='00000442'">Turkmen (Turkmenistan)</xsl:when>
    <xsl:when test="$localeID='00000480'">Uighur (PRC)</xsl:when>
    <xsl:when test="$localeID='00000022'">Ukrainian</xsl:when>
    <xsl:when test="$localeID='00000422'">Ukrainian (Ukraine)</xsl:when>
    <xsl:when test="$localeID='0000042E'">Upper Sorbian (Germany)</xsl:when>
    <xsl:when test="$localeID='00000020'">Urdu</xsl:when>
    <xsl:when test="$localeID='00000420'">Urdu (Islamic Republic of Pakistan)</xsl:when>
    <xsl:when test="$localeID='00000043'">Uzbek</xsl:when>
    <xsl:when test="$localeID='00000843'">Uzbek (Cyrillic, Uzbekistan)</xsl:when>
    <xsl:when test="$localeID='00000443'">Uzbek (Latin, Uzbekistan)</xsl:when>
    <xsl:when test="$localeID='0000002A'">Vietnamese</xsl:when>
    <xsl:when test="$localeID='0000042A'">Vietnamese (Vietnam)</xsl:when>
    <xsl:when test="$localeID='00000452'">Welsh (United Kingdom)</xsl:when>
    <xsl:when test="$localeID='00000488'">Wolof (Senegal)</xsl:when>
    <xsl:when test="$localeID='00000485'">Yakut (Russia)</xsl:when>
    <xsl:when test="$localeID='00000478'">Yi (PRC)</xsl:when>
    <xsl:when test="$localeID='0000046A'">Yoruba (Nigeria)</xsl:when>
  </xsl:choose>
</xsl:template>

</xsl:stylesheet>
