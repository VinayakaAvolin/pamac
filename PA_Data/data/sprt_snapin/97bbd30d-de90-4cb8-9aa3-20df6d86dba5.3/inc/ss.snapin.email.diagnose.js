////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//  cl_em_diagnose.js                                                         //
//  -----------------                                                         //
//                                                                            //
//  Copyright (C) 2008, SupportSoft Inc., All Rights Reserved                 //
//                                                                            //
//  This file contains functions for the email troubleshooter to diagnose     //
//  accounts                                                                  //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//  Function : GetAccounts                                              //
//                                                                            //
//  Access   : Public                                                         //
//                                                                            //
//  Arguments: None                                                           //
//                                                                            //
//  Returned : an array of accounts to manage                                 //
//                                                                            //
//  Get the filtered accounts and associate them with a settings template     //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

$ss.snapin = $ss.snapin ||
{};

$ss.snapin.email = $ss.snapin.email ||
{};

$ss.snapin.email.diagnose = $ss.snapin.email.diagnose ||
{};

(function() {
  $.extend($ss.snapin.email.diagnose, {

    GetAccounts: function(arrTemplates) {
      try {
        var arrAccounts, objRegExp1, objRegExp2, strEmail, bFound, L1, L2;

        // Get the email templates

        arrTemplates = $ss.snapin.email.template.GetTemplates();
        if (arrTemplates.length == 0)
          throw ("GetAccounts: No e-mail settings templates defined");

        // Get the filtered accounts

        var arrAccounts = $ss.snapin.email.utility.GetAllEmailAccounts(true);

        // Associate each account with a template. We give precedence to an email
        // address match over a SMTP or POP3 server match

        for (L1 = 0; L1 < arrAccounts.length; L1++) {

          bFound = false;

          // Check the e-mail address

          strEmail = (arrAccounts[L1].Email.split("@"))[1];
          if (typeof (strEmail) != "undefined") {

            for (L2 = 0; L2 < arrTemplates.length; L2++) {
              objRegExp1 = arrTemplates[L2].GetRegExp("Domain");
              if (strEmail.match(objRegExp1) != null) {
                bFound = true;
                break;
              }
            }
          }

          // If no match was found, check the SMTP and POP3 servers

          if (bFound == false) {

            for (L2 = 0; L2 < arrTemplates.length; L2++) {

              objRegExp1 = arrTemplates[L2].GetRegExp("SmtpServer");
              objRegExp2 = arrTemplates[L2].GetRegExp("Pop3Server");

              if (arrAccounts[L1].SmtpServer.match(objRegExp1) != null ||
              arrAccounts[L1].Pop3Server.match(objRegExp2) != null) {
                bFound = true;
                break;
              }
            }
          }

          // Add the template to the account if a match was found, otherwise 
          // delete the account

          if (bFound == true)
            arrAccounts[L1].m_objTemplate = arrTemplates[L2];
          else
            delete arrAccounts[L1];
        }

        // Return the accounts

        $ss.snapin.email.utility.PvtCleanArray(arrAccounts);
      }
      catch (ex) {
        arrAccounts = null;
      }
      return (arrAccounts);
    },

    ////////////////////////////////////////////////////////////////////////////////
    //                                                                            //
    //  Function : DiagnoseAccounts                                         //
    //                                                                            //
    //  Access   : Public                                                         //
    //                                                                            //
    //  Arguments: None                                                           //
    //                                                                            //
    //  Diagnose e-mail accounts                                                  //
    //                                                                            //
    ////////////////////////////////////////////////////////////////////////////////

    DiagnoseAccounts: function(accounts) {
      var nCase, nStatus, L1;

      // Assume all accounts are valid

      nCase = 1;

      for (L1 = 0; L1 < accounts.length; L1++) {

        // Diagnose the account

        nStatus = this.DiagnoseAccount(accounts[L1], false);

        // If we have an account with some problem then do not display anything
        // in the status field. This is done after a repair has been attempted

        if (nStatus != 2)
          nCase = -1;

        // Introduce an artificial delay so that the user can see the diagnosis

        // if (L1 != accounts.length) ss_con_Sleep (500); Todo handle asynronous Dinesh
      }

      // Show the status of the diagnosis

      //cm_em_ui_ShowDiagnosisStatus (nCase); Todo handle UI later
    },

    ////////////////////////////////////////////////////////////////////////////////
    //                                                                            //
    //  Function : DiagnoseAccount                                          //
    //                                                                            //
    //  Access   : Public                                                         //
    //                                                                            //
    //  Arguments: [IN] objAccount - account                                  //
    //             [IN] bShowErrorText - true if error text is to be shown        //
    //                                                                            //
    //  Returned : a status number indicating the overall condition of the        //
    //             account                                                        //
    //                                                                            //
    //  Diagnose an e-mail account                                                //
    //                                                                            //
    ////////////////////////////////////////////////////////////////////////////////

    DiagnoseAccount: function(objAccount, bShowErrorText) {
      var bSettings, nConnectivity, nAuthenticated, nStatus;

      if (!objAccount) {
        return -1;
      }

      if (typeof (objAccount).m_objResults == "undefined")
        objAccount.m_objResult = {};

      // Create the account table

      //cl_em_ui_CreateAccountTable (nIndex,objAccount.Email); Todo Handle UI Dinesh

      // Diagnose the settings


      bSettings = this.DiagnoseSettings(objAccount);

      objAccount.bSettings = bSettings;


      // Diagnose the connectivity to the e-mail servers

      nConnectivity = this.DiagnoseConnectivity(objAccount);
      if (1 == nConnectivity) {
        objAccount.bConnectivity = true;
      }
      else {
        objAccount.bConnectivity = false;
      }

      // Check the user authentication

      nAuthenticated = this.DiagnoseAuthentication(objAccount);
      if (1 == nAuthenticated) {
        objAccount.bAuthenticated = true;
      }
      else {
        objAccount.bAuthenticated = false;
      }

      // Determine the status of the diagnosis

      nStatus = (bSettings == true) ? 16 : 0;
      nStatus += (nConnectivity * 4) + nAuthenticated;

      if (nStatus == 16 || nStatus == 18 || nStatus == 24 || nStatus == 25)
        nStatus = 3;
      else
        if (nStatus == 20)
        nStatus = 4;
      else
        if (nStatus == 21)
        nStatus = 2;
      else
        nStatus = -1;

      objAccount.ShowRepairButton = (bSettings == false || nAuthenticated == 0);

      objAccount.RepairButtonErrorText = "";
      if (bShowErrorText) {
        objAccount.RepairButtonErrorText = "Your credentials cannot be verified. Check that your username and password are correct."
      }

      // Display the action bar

      //            cl_em_ui_DisplayActionBar (nIndex,(bSettings == false || nAuthenticated == 0),
      //                                       (bShowErrorText == true) ? nStatus : -1); Todo Handle UI Later Dinesh

      // Return the status

      return (nStatus);
    },

    ////////////////////////////////////////////////////////////////////////////////
    //                                                                            //
    //  Function : DiagnoseSettings                                         //
    //                                                                            //
    //  Access   : Public                                                         //
    //                                                                            //
    //  Arguments: [IN] objAccount - account                                     //
    //                                                                            //
    //  Returned : true if the accounts settings are correct, else false          //
    //                                                                            //
    //  Diagnose an e-mail account settings                                       //
    //                                                                            //
    ////////////////////////////////////////////////////////////////////////////////

    DiagnoseSettings: function(objAccount) {
      var objTemplate, strEmail, strRegKey, bStatus, strStatus;
      if (!objAccount) {
        return false;
      }

      //Create the settings objects
      try {
        if (typeof (objAccount).Settings == "undefined") {
          objAccount.Settings = new Object();
        }
      }
      catch (ex) {
        return false;
      }

      // Get the account and the settings template

      //objAccount  = g_arrAccounts[nIndex];
      objTemplate = objAccount.m_objTemplate;

      // Check the email account. For Telia the email address is something we can
      // fix automatically if the account information is held in the registry


      strRegKey = "mailinformation-" + objAccount.Pop3User.toLowerCase();
      strEmail = $ss.agentcore.dal.config.GetConfigFromHKLM(strRegKey, "user_email");

      if (strEmail != "" && strEmail != "undefined") {
        bStatus = (objAccount.Email.toLowerCase() == strEmail.toLowerCase());
        objAccount.m_objResult.Email = bStatus;
      }
      else {
        bStatus = this.CheckMailFormat(objAccount, "Email");
      }

      //cl_em_ui_LogResult (nIndex,"Settings","emailaddress",bStatus); 
      this.UpdateResult(objAccount.Settings, "Email", bStatus);
      objAccount.m_objResult["Email"] = bStatus;

      // Check the user name if a suffix is required

      if (objTemplate.GetProperty("Pop3UserSuffix") != "") {

        bResult = this.CheckMailFormat(objAccount, "Pop3User");

        //cl_em_ui_LogResult (nIndex,"Settings","username",bResult);
        this.UpdateResult(objAccount.Settings, "username", bResult);
        bStatus &= bResult;
      }

      // Check the simpler settings

      try {
        bStatus &= this.CheckSetting(objAccount, "SmtpServer", "Settings");
        bStatus &= this.CheckSetting(objAccount, "SmtpPort", "Settings");
        bStatus &= this.CheckSetting(objAccount, "SmtpSSL", "Settings");
        bStatus &= this.CheckSetting(objAccount, "SmtpAuth", "Settings");
        bStatus &= this.CheckSetting(objAccount, "Pop3Server", "Settings");
        bStatus &= this.CheckSetting(objAccount, "Pop3Port", "Settings");
        bStatus &= this.CheckSetting(objAccount, "Pop3SSL", "Settings");
        bStatus &= this.CheckSetting(objAccount, "Pop3Auth", "Settings");
      }
      catch (ex) {
        bStatus = false;
      }

      // Update the status text and icon

      strStatus = (bStatus == true) ? "pass" : "fail";
      //cl_em_ui_UpdateStatus (nIndex,"Settings",strStatus); Todo : Handle UI later

      // Return the status

      return (bStatus);
    },

    ////////////////////////////////////////////////////////////////////////////////
    //                                                                            //
    //  Function : DiagnoseConnectivity                                     //
    //                                                                            //
    //  Access   : Public                                                         //
    //                                                                            //
    //  Arguments: [IN] objAccount - account                                     //
    //                                                                            //
    //  Returned : 0 - connectivity failed                                        //
    //             1 - connectivity succeeded                                     //
    //             2 - connectivity skipped                                       //
    //                                                                            //
    //  Diagnose connectivity to e-mail servers                                   //
    //                                                                            //
    ////////////////////////////////////////////////////////////////////////////////

    DiagnoseConnectivity: function(objAccount) {
      var nStatus, strStatus;


      // Test connectivity to the e-mail servers      

      nStatus = this.TestConnection(objAccount, "SmtpServer", "SmtpPort");
      nStatus |= this.TestConnection(objAccount, "Pop3Server", "Pop3Port");
      if (nStatus == 3)
        nStatus = 2;

      // Update the status text and icon

      strStatus = (nStatus == 1) ? "pass" : "fail";

      // Return the status

      return (nStatus);
    },

    ////////////////////////////////////////////////////////////////////////////////
    //                                                                            //
    //  Function : DiagnoseAuthentication                                   //
    //                                                                            //
    //  Access   : Public                                                         //
    //                                                                            //
    //  Arguments: [IN] objAccount - account                                    //
    //                                                                            //
    //  Returned : 0 - authentication failed                                      //
    //             1 - authentication succeeded                                   //
    //             2 - authentication skipped                                     //
    //                                                                            //
    //  Diagnose authentication of an e-mail account                              //
    //                                                                            //
    ////////////////////////////////////////////////////////////////////////////////

    DiagnoseAuthentication: function(objAccount) {
      var objResult, objEmailClient, nStatus, bStatus, strStatus;

      if (!objAccount) {
        return 0;
      }

      try {
        if (typeof (objAccount).Authentication == "undefined") {
          objAccount.Authentication = new Object();
        }
      }
      catch (ex) {
        return 0;
      }


      objResult = objAccount.m_objResult;

      // Update the status text and icon

      //cl_em_ui_UpdateStatus (nIndex,"Authentication","testing"); Todo Handle UI Later Dinesh

      // Skip the authentication if the POP3 connectivity check failed

      if (objResult.Pop3ServerConnectivity == false) {

        //cl_em_ui_LogResult (nIndex,"Authentication","Pop3AuthSkipped",false); 
        this.UpdateResult(objAccount.Authentication, "Pop3AuthSkipped", false);
        nStatus = 2;

      }
      else {

        // Check the authentication using the client settings

        objEmailClient = $ss.snapin.email.utility.GetEmailClient(objAccount.MailClient, $ss.snapin.email.utility.GetEmailObject(objAccount));

        objEmailClient.sAccountId = objAccount.AccountId;
        objEmailClient.sProfile = objAccount.Profile;

        if (objEmailClient.IsPasswordSaved() == true) {

          // A password exists for this account in the mail client, so
          // authenticate the credentials
          // Use the mail object
          bStatus = objEmailClient.AuthenticatePassword();
          //cl_em_ui_LogResult (nIndex,"Authentication","Pop3Auth",bStatus); 
          this.UpdateResult(objAccount.Authentication, "Aut_Pop3Auth", bStatus);

          nStatus = (bStatus == true) ? 1 : 0;

        }
        else {

          // No password exists for this account in the e-mail client, so 
          // fail as a password must be defined

          //cl_em_ui_LogResult (nIndex,"Authentication","Pop3AuthNoPassword",false);
          this.UpdateResult(objAccount.Authentication, "Pop3AuthNoPassword", false);
          nStatus = 0;
        }
      }

      //if its a valid account then remove the status regarding password
      if (1 === nStatus) {
        try {
          delete objAccount.Authentication.Pop3AuthNoPassword;
        }
        catch (ex) {
        }
      }

      objResult.Pop3AuthTest = (nStatus == 1);

      // Update the status text and icon

      strStatus = (nStatus == 1) ? "pass" : "fail";
      //cl_em_ui_UpdateStatus (nIndex,"Authentication",strStatus); Todo Handle UI Later

      // Return the status

      return (nStatus);
    },

    ////////////////////////////////////////////////////////////////////////////////
    //                                                                            //
    //  Function : CheckMailFormat                                      //
    //                                                                            //
    //  Access   : Private                                                        //
    //                                                                            //
    //  Arguments: [IN] strValue   - the value to test                            //
    //             [IN] strSetting - the correct suffix setting                   //
    //                                                                            //
    //  Returned : true if the mail format is correct, else false                 //
    //                                                                            //
    //  Check a value has the correct format for an e-mail address                //
    //                                                                            //
    ////////////////////////////////////////////////////////////////////////////////

    CheckMailFormat: function(objAccount, strSetting) {
      var nPos, strEmail, strSuffix, objRegExp, bRet;

      // The email must contain at least one @ sign and not at the beginning

      nPos = objAccount[strSetting].indexOf("@");
      if (nPos <= 0)
        return (false);

      // Check the format of the email suffix

      strEmail = (objAccount[strSetting].split("@"))[1];
      strSuffix = strSetting + "Suffix";
      objRegExp = objAccount.m_objTemplate.GetRegExp(strSuffix);

      bRet = (strEmail.match(objRegExp) != null);

      return bRet;
    },

    ////////////////////////////////////////////////////////////////////////////////
    //                                                                            //
    //  Function : CheckSetting                                         //
    //                                                                            //
    //  Access   : Private                                                        //
    //                                                                            //
    //  Arguments: [IN] objAccount - account                                 //
    //             [IN] strSetting - the setting                                  //
    //             [IN] strSection - the HTML section identifier                  //
    //                                                                            //
    //  Returned : true if the setting is correct, else false                     //
    //                                                                            //
    //  Check a setting                                                           //
    //                                                                            //
    ////////////////////////////////////////////////////////////////////////////////

    CheckSetting: function(objAccount, strSetting, strSection) {
      var strValue, strCorrect, bResult;

      if ((!objAccount) || (!strSetting) || (!strSetting)) {
        return false;
      }

      // Get the setting in the account and the correct setting from the template

      strValue = objAccount[strSetting];
      if (typeof (strValue) == "undefined")
        throw ("CheckSetting: Unknown setting: " + strSetting);

      strCorrect = objAccount.m_objTemplate.GetProperty(strSetting);
      if (typeof (strCorrect) == "undefined")
        throw ("CheckSetting: Unknown template setting: " + strSetting);

      // Do a case insensitive test

      bResult = (strValue.toLowerCase() == strCorrect.toLowerCase());
      objAccount.m_objResult[strSetting] = bResult;

      // Log the result to the appropriate section

      if (typeof (strSection) != "undefined")
      //cl_em_ui_LogResult (nIndex,strSection,strSetting,bResult); 
        this.UpdateResult(objAccount.Settings, strSetting, bResult);

      // Return the result

      return (bResult);
    },

    ////////////////////////////////////////////////////////////////////////////////
    //                                                                            //
    //  Function : TestConnection                                       //
    //                                                                            //
    //  Access   : Private                                                        //
    //                                                                            //
    //  Arguments: [IN] objAccount    - account                              //
    //             [IN] strServerName - the server resource                       //
    //             [IN] strPort       - the port resource                         //
    //                                                                            //
    //  Returned : 0 - connectivity failed                                        //
    //             1 - connectivity succeeded                                     //
    //             2 - connectivity skipped                                       //
    //                                                                            //
    //  Test a connection to an e-mail server                                     //
    //                                                                            //
    ////////////////////////////////////////////////////////////////////////////////

    TestConnection: function(objAccount, strServerName, strPort) {
      var objResult, objNetCheck, strServer, nPort, bResult;

      if (!objAccount) {
        return 0;
      }

      try {
        if (typeof (objAccount).Connectivity == "undefined") {
          objAccount.Connectivity = new Object();
        }
      }
      catch (ex) {
        return 0;
      }

      objResult = objAccount.m_objResult;

      // Fail the test if the server settings are known to be wrong in the account

      if (objResult[strServerName] == false || objResult[strPort] == false) {
        objResult[strServerName + "Connectivity"] = false;
        //cl_em_ui_LogResult (nIndex,"Connectivity",strServerName + "skipped",false); 
        this.UpdateResult(objAccount.Connectivity, "Con_" + strServerName + "skipped", false);
        return (2);
      }

      // Get the server connection information

      strServer = objAccount[strServerName];
      nPort = parseInt(objAccount[strPort]);

      // Test the connection to the server


      //objNetCheck = $ss.agentcore.utils.activex.CreateObject("SPRT.SdcNetCheck");
      //bResult = objNetCheck.TestConnect(strServer, nPort, 5000);
      //delete objNetCheck;
      if ($ss.agentcore.utils.IsWinVST())
        bResult = $ss.agentcore.network.netcheck.TestConnection(null, strServer, nPort, 5000, false);
      else
        bResult = $ss.agentcore.network.netcheck.TestConnection(null, strServer, nPort, 5000);

      objResult[strServerName + "Connectivity"] = bResult;
      //cl_em_ui_LogResult (nIndex,"Connectivity",strServerName,bResult);
      this.UpdateResult(objAccount.Connectivity, "Con_" + strServerName, bResult);

      // Return the result

      return ((bResult == true) ? 1 : 0);
    },

    ////////////////////////////////////////////////////////////////////////////////
    //                                                                            //
    //  Function : AuthenticateAccount                                  //
    //                                                                            //
    //  Access   : Private                                                        //
    //                                                                            //
    //  Arguments: [IN] objAccount   - account                                  //
    //             [IN] objEmail - the email client object                        //
    //                                                                            //
    //  Returned : true if the authentication succeeded, else false               //
    //                                                                            //
    //  Authenticate an account with an e-mail server using SSL                   //
    //                                                                            //
    ////////////////////////////////////////////////////////////////////////////////

    AuthenticateAccount: function(objAccount, objEmail) {
      var strMailClient, strCommand, nStatus, bStatus;

      // Get the client version and account information
      strMailClient = objEmail.GetVersion();
      strAccountId = objAccount.AccountId;

      if (objAccount.MailClient == EM_OUTLOOK_EXPRESS ||
      objAccount.MailClient == EM_WINDOWS_MAIL) {
        strMailClient = objAccount.MailClient;
        strAccountId = objAccount.Name;
      }

      // Run the authentication executable

      //strCommand = "\"" + ss_con_GetProviderBinPath () + "telia_authenticator.exe\" ";
      //strCommand += "\"" + strMailClient + "\" \"" + strAccountId + "\"";

      //nStatus = $ss.agentcore.utils.system.RunCommand(strCommand,BCONT_RUNCMD_HIDDEN);
      bStatus = (nStatus == 0);

      // Log the result

      switch (nStatus) {

        case 0:
        case 2:

          //cl_em_ui_LogResult (nIndex,"Authentication","Pop3Auth",bStatus);
          this.UpdateResult(objAccount.Authentication, "Aut_Pop3Auth", bStatus);
          break;

        case 1:

          //cl_em_ui_LogResult (nIndex,"Authentication","ServerConn",false);
          this.UpdateResult(objAccount.Authentication, "ServerConn", false);
          break;
      }

      // Return the result

      return (bStatus);
    },

    UpdateResult: function(object, key, value) {

      var data = value;
      switch (key) {
        case "Email":
          if (true == value) {
            data = $ss.agentcore.utils.LocalXLate("snapin_email", "DG:emailaddress Valid")
          }
          else {
            data = $ss.agentcore.utils.LocalXLate("snapin_email", "DG:emailaddress Invalid")
          }
          break;
        case "SmtpServer":
          if (true == value) {
            data = $ss.agentcore.utils.LocalXLate("snapin_email", "DG:SmtpServer Valid");
          }
          else {
            data = $ss.agentcore.utils.LocalXLate("snapin_email", "DG:SmtpServer InValid");
          }
          break;
        case "SmtpPort":
          if (true == value) {
            data = $ss.agentcore.utils.LocalXLate("snapin_email", "DG:SmtpPort Valid");
          }
          else {
            data = $ss.agentcore.utils.LocalXLate("snapin_email", "DG:SmtpPort InValid");
          }
          break;
        case "SmtpSSL":
          if (true == value) {
            data = $ss.agentcore.utils.LocalXLate("snapin_email", "DG:SmtpSSL Valid");
          }
          else {
            data = $ss.agentcore.utils.LocalXLate("snapin_email", "DG:SmtpSSL InValid");
          }
          break;
        case "SmtpAuth":
          if (true == value) {
            data = $ss.agentcore.utils.LocalXLate("snapin_email", "DG:SmtpAuth Valid");
          }
          else {
            data = $ss.agentcore.utils.LocalXLate("snapin_email", "DG:SmtpAuth InValid");
          }
          break;
        case "Pop3Server":
          if (true == value) {
            data = $ss.agentcore.utils.LocalXLate("snapin_email", "DG:Pop3Server Valid");
          }
          else {
            data = $ss.agentcore.utils.LocalXLate("snapin_email", "DG:Pop3Server InValid");
          }
          break;
        case "Pop3Port":
          if (true == value) {
            data = $ss.agentcore.utils.LocalXLate("snapin_email", "DG:Pop3Port Valid");
          }
          else {
            data = $ss.agentcore.utils.LocalXLate("snapin_email", "DG:Pop3Port InValid");
          }
          break;
        case "Pop3SSL":
          if (true == value) {
            data = $ss.agentcore.utils.LocalXLate("snapin_email", "DG:Pop3SSL Valid");
          }
          else {
            data = $ss.agentcore.utils.LocalXLate("snapin_email", "DG:Pop3SSL InValid");
          }
          break;
        case "Pop3Auth":
          if (true == value) {
            data = $ss.agentcore.utils.LocalXLate("snapin_email", "DG:Pop3Auth Valid");
          }
          else {
            data = $ss.agentcore.utils.LocalXLate("snapin_email", "DG:Pop3Auth InValid");
          }
          break;
        case "Con_SmtpServerskipped":
          key = "SmtpServer";
          data = $ss.agentcore.utils.LocalXLate("snapin_email", "DG:Con_SmtpServerskipped InValid");
          break;
        case "Con_SmtpServer":
          if (true == value) {
            data = $ss.agentcore.utils.LocalXLate("snapin_email", "DG:Con_SmtpServer Valid");
          }
          else {
            data = $ss.agentcore.utils.LocalXLate("snapin_email", "DG:Con_SmtpServer InValid");
          }
          key = "SmtpServer";
          break;
        case "Con_Pop3Serverskipped":
          key = "Pop3Server";
          data = $ss.agentcore.utils.LocalXLate("snapin_email", "DG:Con_Pop3Serverskipped InValid");
          break;
        case "Con_Pop3Server":
          if (true == value) {
            data = $ss.agentcore.utils.LocalXLate("snapin_email", "DG:Con_Pop3Server Valid");
          }
          else {
            data = $ss.agentcore.utils.LocalXLate("snapin_email", "DG:Con_Pop3Server InValid");
          }
          key = "Pop3Server";
          break;
        case "Pop3AuthSkipped":
          data = $ss.agentcore.utils.LocalXLate("snapin_email", "DG:Pop3AuthSkipped InValid");
          key = "Pop3Auth";
          break;
        case "Con_Pop3Auth":
          if (true == value) {
            data = $ss.agentcore.utils.LocalXLate("snapin_email", "DG:Con_Pop3Auth Valid");
          }
          else {
            data = $ss.agentcore.utils.LocalXLate("snapin_email", "DG:Con_Pop3Auth InValid");
          }
          break;
        case "ServerConn":
          if (true == value) {
            data = $ss.agentcore.utils.LocalXLate("snapin_email", "DG:ServerConn Valid");
          }
          else {
            data = $ss.agentcore.utils.LocalXLate("snapin_email", "DG:ServerConn InValid");
          }
          key = "Pop3Auth";
          break;

        case "Aut_Pop3Auth":
          key = "Pop3Auth";
          if (value) {
            data = $ss.agentcore.utils.LocalXLate("snapin_email", "DG:Aut_Pop3Auth Valid");
          }
          else {
            data = $ss.agentcore.utils.LocalXLate("snapin_email", "DG:Aut_Pop3Auth InValid");
          }
          break;
        case "Pop3AuthNoPassword":
          if (true == value) {
            data = $ss.agentcore.utils.LocalXLate("snapin_email", "DG:Password Present")
          }
          else {
            data = $ss.agentcore.utils.LocalXLate("snapin_email", "DG:No Password")
          }
          break;

      }

      try {
        object[key] = data;
      }
      catch (ex) {
        return false;
      }

      return true;
    }

  });

})();



