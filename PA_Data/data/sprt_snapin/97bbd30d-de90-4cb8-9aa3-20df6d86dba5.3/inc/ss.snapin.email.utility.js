/*******************************************************************************
**
**  File Name: ss_snapin.email.utility.js
**
**  Summary: SupportSoft Client Common Email Functionality
**
**  Description: This javascript contains general purpose functions for 
**               providing ways to set various versions of 
**               Microsoft Outlook 
**               Outlook Express and
**               Windows Mail clients
**           
**  Copyright SupportSoft Inc. 2005, All rights reserved.
*******************************************************************************/


$ss.snapin = $ss.snapin ||
{};

$ss.snapin.email = $ss.snapin.email ||
{};

$ss.snapin.email.utility = $ss.snapin.email.utility ||
{};

/*******************************************************************************
**      G L O B A L    C O N S T A N T S 
********************************************************************************/
var EM_WINDOWS_MAIL         = "Windows Mail"
var EM_WINDOWS_LIVE_MAIL    = "Windows Live Mail";

// Microsoft Outlook and its various versions
var EM_MS_OUTLOOK      = "Microsoft Outlook";
var EM_MS_EXCHANGE     = "Exchange";
var EM_OLK_VER1997     = "Outlook 97";
var EM_OLK_VER2000     = "Outlook 2000";
var EM_OLK_VER2002     = "Outlook 2002";
var EM_OLK_VER2003     = "Outlook 2003";
var EM_OLK_VER2007     = "Outlook 2007";
var EM_OLK_VER2010     = "Outlook 2010";
var EM_OLK_VER2013     = "Outlook 2013";


var EM_OUTLOOK_EXPRESS = "Outlook Express"  

// Flags to show various client selections on UI
var EM_SHW_WINDOWS_MAIL    = 0x00000001;
var EM_SHW_MS_OUTLOOK      = 0x00000002;
var EM_SHW_OUTLOOK_EXPRESS = 0x00000004;
var EM_SHW_OPTION_NONE     = 0x00000008; 

// Global return constants
var EM_RET_OK              = 0;
var EM_RET_INCORRECT_STATE = 1;
var EM_RET_OTHER_ERROR     = 2;
var EM_RET_NOT_IMPLEMENTED = 3;

// Config Values
var EM_CFG_SMTP = 0x00000001;
var EM_CFG_POP3 = 0x00000002;

var EM_YES = "1";
var EM_NO = "0";
var EM_CLIENT_DELIMETER = "#@$#";
var EM_DOMAIN_DELIMETER = "#@$#";


/*******************************************************************************
**      C L A S S  : ssCBaseEmail 
**      B A S E    I N T E R F A C E  
********************************************************************************/
function ssCBaseEmail(oEmail) {
  this.oEmail = oEmail;      // for storing user input
  this.sVersion = "";        // for tracking email version
  this.sAccountId = "";      // for tracking unique account id
  this._logger = $ss.agentcore.log.GetDefaultLogger("$ss.snapin.email.ssCBaseEmail");
  this._exception = $ss.agentcore.exceptions;   
}
// base class prototypes
ssCBaseEmail.prototype.CreateAccount = function _ssCBaseEmail_CreateAccount() 
{ 
  this._logger.info("CreateAccount", "Not Implemented in " + this.toString()); 
}
ssCBaseEmail.prototype.GetVersion = function _ssCBaseEmail_GetVersion() 
{ 
  this._logger.info("GetVersion", "Not Implemented in " + this.toString()); 
}
ssCBaseEmail.prototype.GenerateAccountId = function _ssCBaseEmail_GenerateAccountId() 
{ 
  this._logger.info("GenerateAccountId", "Not Implemented in " + this.toString()); 
}
ssCBaseEmail.prototype.SetAccountProfile = function _ssCBaseEmail_SetAccountProfile(sRegKey)
{
  this._logger.info("SetAccountProfile", "Not Implemented in " + this.toString()); 
}
ssCBaseEmail.prototype.SetAccountPassword = function _ssCBaseEmail_SetAccountPassword()
{
  this._logger.info("SetAccountPassword", "Not Implemented in " + this.toString()); 
}
ssCBaseEmail.prototype.AuthenticatePassword = function _ssCBaseEmail_AuthenticatePassword()
{
  this._logger.info("AuthenticatePassword", "Not Implemented in " + this.toString()); 
}
ssCBaseEmail.prototype.IsPasswordSaved = function _ssCBaseEmail_IsPasswordSaved()
{
  this._logger.info("IsPasswordSaved", "Not Implemented in " + this.toString()); 
}
ssCBaseEmail.prototype.toString = function _ssCBaseEmail_ToString() 
{ 
  return "{ssCBaseEmail}"; 
}
ssCBaseEmail.prototype.DeleteAccount = function _ssCBaseEmail_DeleteAccount(sProfile, sServiceId, sAccountId) 
{ 
  this._logger.info("DeleteAccount", "Not Implemented in " + this.toString()); 
}

ssCBaseEmail.prototype.SetAsDefault = function _ssCBaseEmail_SetAsDefault(sProfile, sServiceId, sAccountId) 
{ 
  this._logger.info("SetAsDefault", "Not Implemented in " + this.toString()); 
}

ssCBaseEmail.prototype.GetUniversalReg = function _ssCBaseEmail_GetUniversalReg(RegRoot, RegKey, RegVal) {
  try {
    var theString = $ss.agentcore.dal.registry.GetRegUnicodeStringValue(RegRoot, RegKey, RegVal);
    if (theString.match(/[a-zA-Z0-9][a-zA-Z0-9]/) == null) {
      return $ss.agentcore.dal.registry.GetRegValue(RegRoot, RegKey, RegVal);
    }
  }
  catch (ex) {
    delete theString;
  }
  return theString;
}

ssCBaseEmail.prototype.GetUniversalRegWithNewline = function _GetUniversalRegWithNewline(RegRoot, RegKey, RegVal) {
  var theString = $ss.agentcore.dal.registry.GetRegUnicodeStringValueWithNewline(RegRoot, RegKey, RegVal);
  return theString;
}

/*******************************************************************************
**      C L A S S  : ssCEmail 
**      H E L P E R   C L A S S 
********************************************************************************/
function ssCEmail(bIsISPSpecific)
{
  this.bIsISPSpecific = bIsISPSpecific; // boolean to track if ISP values are 
                                        // to be consodered while creating object
  this.bInit = false;
  
  this._logger = $ss.agentcore.log.GetDefaultLogger("$ss.snapin.email.ssCEmail");   
  this._exception = $ss.agentcore.exceptions; 
}

/*******************************************************************************
** Name:         _ssCEmail_Init
**
** Purpose:      Used to initialize mail object. Creates all new properties
**               for the object that are passed in AddOns
**               
**
** Parameter:    oAddOns        : Object which helps adding more properties
**               In general contains the following properties:
**               sAccountName       sEmailAddress   sPOPPort
**               sUserId            sSMTPServer
**               sDisplayName       sSMTPPort
**               sPassword          sPOPServer
**
** Return:       none; sets bInit to true if everything goes fine.
********************************************************************************/
ssCEmail.prototype.Init = function _ssCEmail_Init(oAddOns) {
  try {

    for (var sProp in oAddOns) {
      this[sProp] = oAddOns[sProp];
    }

    if (this.bIsISPSpecific) {
      var sISP = $ss.agentcore.diagnostics.smartissue($ss.agentcore.constants.smartissue.GEN_CLASS, $ss.agentcore.constants.smartissue.ISP_NAME, "isp1");
      this.sSMTPServer = $ss.agentcore.dal.config.GetValues("email","smtp_" + sISP, "");
      this.sSMTPPort   = $ss.agentcore.dal.config.GetValues("email","smtpport_" + sISP, ""); 
      this.sSMTPSSL    = $ss.agentcore.dal.config.GetValues("email","smtpssl_" + sISP, ""); 
      this.sSMTPAuth   = $ss.agentcore.dal.config.GetValues("email","smtpauth_" + sISP, ""); 
      this.sPOPServer  = $ss.agentcore.dal.config.GetValues("email","pop_" + sISP, "");
      this.sPOPPort    = $ss.agentcore.dal.config.GetValues("email","popport_" + sISP, ""); 
      this.sPOPSSL     = $ss.agentcore.dal.config.GetValues("email","popssl_" + sISP, ""); 
      this.sPOPAuth    = $ss.agentcore.dal.config.GetValues("email","popauth_" + sISP, ""); 
      this.sIsDefault  = false; 
    } else {
      //TODO: any specifics for ssmail.js work
    }


    this.bInit = true;
  } catch (ex) {
    this._exception.HandleException(ex,'$ss.snapin.email.utility', 'ssCEmail.prototype.Init',arguments);
    this.bInit = false;
  }
}

/*******************************************************************************
** Name:         _ssCEmail_GetUniqueName
**
** Purpose:      Generate a unique account name by appending digits to the current
**               display name. Provide a base key under which it needs to
**               search for creating unique name. 
**
** Parameter:    sBaseKey:  Identity RegKey  
**
** Return:       Empty or Unique Name
********************************************************************************/
ssCEmail.prototype.GetUniqueName = function _ssCEmail_GetUniqueName(sBaseKey) {
  this._logger.info("_ssCEmail_GetUniqueName");
  var sUName = "";
  try {
    if (this.bInit)
      sUName = this.sAccountName;
    else
      return (this.sAccountName = "");

    var arExistingAccounts = $ss.agentcore.dal.registry.EnumRegKey("HKCU", sBaseKey, ",");
    if (arExistingAccounts.length == 0)         // no existing accounts
      return sUName;

    var nNumber = parseInt(sUName);
    if (isNaN(nNumber))
      nNumber = 0;

    oReg = new RegExp("\\d+", "g");
    var sBase = sUName.replace(oReg, "");

    var accounts = arExistingAccounts.split(',');
    // Go through accounts, looking for match
    var bMatch = false;
    do {
      bMatch = false;
      for (var ndx = 0; ndx < accounts.length; ndx++) {
        var sRegName = $ss.agentcore.dal.registry.GetRegValue("HKCU", sBaseKey + accounts[ndx], "Account Name");
        if (sUName == sRegName) {
          // it matched existing account so need to try new account
          bMatch = true;
          sUName = sBase + " " + nNumber++;
          break;
        }
      }
    } while (bMatch);
  } catch (ex) {
    this._exception.HandleException(ex, '$ss.snapin.email.utility', 'ssCEmail.prototype.GetUniqueName', arguments);
  }
  return (this.sAccountName = sUName);
}

/*******************************************************************************
**    D E R I V E D  C L A S S E S  -  AND THEIR IMPLEMENTATIONS
********************************************************************************/

//GS_CUS : Windows LIVE Mail

/*******************************************************************************
**      C L A S S  : ssCWindowsLiveMail
**      Class for supporting Microsoft Windows LIVE Mail
********************************************************************************/

function ssCWindowsLiveMail(oEmail)
{
  ssCBaseEmail.call(this, oEmail);      // ssCWindowsLiveMail inherits from ssCBaseEmail
  this.sMailFolder = "";                // tracks the mail folder on Vista for current user
  this.sLocalFolder = "";               // tracks the local folder on Vista for current user 
  this.sCurrentMailFolder = "";         // tracks the current mail folder 
  this.sAccountFileName = "";           // The full account file name path
   this.sMacros   = "%ACCOUNT_NAME%|%DISPLAY_NAME%|%EMAIL_ADDRESS%|" +
                   "%USERNAME%|%POP_PASSWORD%|" +
                   "%POP_SERVER%|%POP_PORT%|%POP_Sicily%|%POP_SSL%|" +
                   "%SMTP_SERVER%|%SMTP_PORT%|%SMTP_Sicily%|%SMTP_SSL%|" +
                   "%POP_SSL%|%SMTP_Sicily%";
}

ssCWindowsLiveMail.prototype = new ssCBaseEmail(); // initialize prototype object of derived with base 

/*******************************************************************************
** Name:         _ssCWindowsLiveMail_GetMailFolder
********************************************************************************/
ssCWindowsLiveMail.prototype.GetMailFolder = function _ssCWindowsLiveMail_GetMailFolder() {
  if (this.sMailFolder.length > 0) return this.sMailFolder;
  try {
    var sFolder = $ss.agentcore.dal.config.ExpandSysMacro("%SdcProfile%");
    if (sFolder.length <= 0)
      return (this.sMailFolder = "");
    //sMailFolder for Windows Live Mail is C:\Users\foo\AppData\Local\Microsoft\Windows Live Mail
    // Look up for last occurance of supportsoft and replace it
    sFolder = sFolder.replace(/\//g, "\\");
    this.sMailFolder = sFolder.replace(/(.*)\\SupportSoft/i, "$1\\Microsoft\\Windows Live Mail");
    this.SetMailFolder(this.sMailFolder);
  }
  catch (ex) {
    this._exception.HandleException(ex, '$ss.snapin.email.utility', "_ssCWindowsLiveMail_GetMailFolder", "Failed to get mail folder");
  }
  return this.sMailFolder;
} 


/*******************************************************************************
** Name:         _ssCWindowsLiveMail_SetMailFolder
********************************************************************************/
ssCWindowsLiveMail.prototype.SetMailFolder = function _ssCWindowsLiveMail_SetMailFolder(sFolder) {
  this.sMailFolder = sFolder;
  // Add trailing backslash if not present
  if (this.sMailFolder.charAt(this.sMailFolder.length - 1) != "\\")
    this.sMailFolder += "\\";

  // Update local folders as well
  this.sLocalFolder = this.sMailFolder //+ "Local Folders\\";
}


/*******************************************************************************
** Name:         _ssCWindowsLiveMail_MacroReplace
********************************************************************************/
ssCWindowsLiveMail.prototype.MacroReplace = function _ssCWindowsLiveMail_MacroReplace(sText) {
  var sRet = sText;
  try {
    var sRegExp = new RegExp(this.sMacros);
    sRegExp.global = true;
    var arMatched = sText.match(sRegExp);
    for (var i = 0; i < arMatched.length; i++) {
      switch (arMatched[i]) {
        case "%ACCOUNT_NAME%":
          sRet = sRet.replace(/%ACCOUNT_NAME%/g, this.oEmail.sAccountName);
          break;
        case "%POP_SERVER%":
          sRet = sRet.replace(/%POP_SERVER%/g, this.oEmail.sPOPServer);
          break;
        case "%USERNAME%":
          sRet = sRet.replace(/%USERNAME%/g, this.oEmail.sUserName);
          break;
        case "%POP_PASSWORD%":
          // Set the password incase of repair flow (troubleshoot)
          // Set same value which is currently set in mail client
          if (this.sCurrentMailFolder != "" && this.sCurrentMailFolder != null) {
            sRet = sRet.replace(/%POP_PASSWORD%/g, this.oEmail.sPassword);
          } else {
            //else encrypt the password and set it
            var sEncPassword = this.GetAccountPassword(this.oEmail.sPassword);
            sRet = sRet.replace(/%POP_PASSWORD%/g, sEncPassword);
          }
          break;
        case "%POP_PORT%":
          sRet = sRet.replace(/%POP_PORT%/g, this.oEmail.sPOPPort);
          break;
        case "%SMTP_SERVER%":
          sRet = sRet.replace(/%SMTP_SERVER%/g, this.oEmail.sSMTPServer);
          break;
        case "%SMTP_PORT%":
          sRet = sRet.replace(/%SMTP_PORT%/g, this.oEmail.sSMTPPort);
          break;
        case "%DISPLAY_NAME%":
          sRet = sRet.replace(/%DISPLAY_NAME%/g, this.oEmail.sDisplayName);
          break;
        case "%EMAIL_ADDRESS%":
          sRet = sRet.replace(/%EMAIL_ADDRESS%/g, this.oEmail.sEmailAddress);
          break;
        case "%POP_Sicily%":
          var temp = 0;
          if ("true" === this.oEmail.sPOPAuth) {
            temp = 1;
          }
          sRet = sRet.replace(/%POP_Sicily%/g, $ss.snapin.email.utility.NormalizeString(temp.toString(), 8));
          break;
        case "%SMTP_Sicily%":
          var temp = 0;
          if ("true" === this.oEmail.sSMTPAuth) {
            temp = 2;
          }
          sRet = sRet.replace(/%SMTP_Sicily%/g, $ss.snapin.email.utility.NormalizeString(temp.toString(), 8));
          break;
        case "%POP_SSL%":
          var temp = 0;
          if ("true" === this.oEmail.sPOPSSL) {
            temp = 1;
          }
          sRet = sRet.replace(/%POP_SSL%/g, $ss.snapin.email.utility.NormalizeString(temp.toString(), 8));
          break;
        case "%SMTP_SSL%":
          var temp = 0;
          if ("true" === this.oEmail.sSMTPSSL) {
            temp = 1;
          }
          sRet = sRet.replace(/%SMTP_SSL%/g, $ss.snapin.email.utility.NormalizeString(temp.toString(), 8));
          break;
        default:
          this._logger.info("_ssCWindowsLiveMail_MacroReplace", arMatched[i] + ": unknown macro");
          break;
      }
    }
  } catch (ex) {
    var sErr = "Failed for text: " + sText + " : Error:" + ex.message;
    g_logger.Error("_ssCWindowsLiveMail_MacroReplace", sErr);
  }
  return sRet;
}

/*******************************************************************************
** Name:         _ssCWindowsLiveMail_CreateAccount
**
** Purpose:      Create an email account using the default parameters for 
**               Microsoft Windows Mail Vista Client
**
** Parameter:    none       uses oEmail object of class
**
** Return:       int 0=OK, 1=incorrect_state (run wizard), 2=Other bad error
********************************************************************************/
ssCWindowsLiveMail.prototype.CreateAccount = function _ssCWindowsLiveMail_CreateAccount() {
  try {
    // Make sure that we have a mail folder set
    var sFolder = this.GetMailFolder();
    if (sFolder.length <= 0)
      return EM_RET_INCORRECT_STATE;

    // Generate AccountId
    this.GenerateAccountId();
    if (this.sAccountId.length <= 0) return EM_RET_INCORRECT_STATE;

    // TODO: Check if account to be created is unqiue
    // callers can do that better by not allowing in first place

    // Set Account Profile 
    if (this.SetAccountProfile())
      return EM_RET_OK;
    else
      return EM_RET_OTHER_ERROR;
  }
  catch (ex) {
    this._exception.HandleException(ex, "_ssCWindowsLiveMail_CreateAccount", ex.message);
  }
  return EM_RET_OTHER_ERROR;
}

/*******************************************************************************
** Name:         _ssCWindowsLiveMail_GetAccounts
**
** Purpose:      Get all accounts within the user's mail folder for
**               Microsoft Windows Mail 
**
** Parameter:    aAccounts  : array of account objects to use
**
** Return:       aAccounts  : updated array is returned
********************************************************************************/
ssCWindowsLiveMail.prototype.GetAccounts = function _ssCWindowsLiveMail_GetAccounts(aAccounts) {

  //Unlike in Windows Mail all the oeaccount files are hidden with in the sub folders. we will have to loop though each of these subfolders inorder to get the account infomation.

  if (typeof (aAccounts) == "undefined" || aAccounts == null)
    var aAccounts = new Array();

  try {
    // Make sure that we have a mail folder set
    var sFolder = this.GetMailFolder();
    if (sFolder.length <= 0) return aAccounts;

    // From the folder enumerate files of type account{*}.oeaccount
    var aFiles = $ss.agentcore.dal.file.GetFilesList(this.sLocalFolder, /account\{.*\}\.oeaccount/i);
    for (var i = 0; i < aFiles.length; i++) {
      var oAccount = this.GetAccountInfo(aFiles[i], this.sLocalFolder);
      if (oAccount != null && oAccount != "") {
        oAccount.sNewAccount = false;
        aAccounts.push(oAccount);
      }
    }

    var g_oFso = $ss.agentcore.dal.file.GetFileSystemObject();
    var oFoldObj = g_oFso.GetFolder(sFolder);

    var Collection = new Enumerator(oFoldObj.SubFolders);

    var oCollectionObj;

    for (Collection.moveFirst(); !Collection.atEnd(); Collection.moveNext()) {
      oCollectionObj = Collection.item();
      var sSubFolderPath = sFolder + oCollectionObj.Name + "\\"
      var aFiles = $ss.agentcore.dal.file.GetFilesList(sSubFolderPath, /account\{.*\}\.oeaccount/i);
      for (var i = 0; i < aFiles.length; i++) {
        var oAccount = this.GetAccountInfo(aFiles[i], sSubFolderPath);
        // Filter LDAP, NNTP accounts they would be found only at the mail folder level
        if (oAccount) {
          oAccount.sNewAccount = true;
          aAccounts.push(oAccount);
        }
      }
    }


  } catch (ex) {
    this._exception.HandleException(ex, "_ssCWindowsLiveMail_GetAccounts", ex.message);
  }
  return aAccounts;
}

/*******************************************************************************
** Name:         _ssCWindowsLiveMail_GetVersion
********************************************************************************/
ssCWindowsLiveMail.prototype.GetVersion = function _ssCWindowsLiveMail_GetVersion() {
  if (this.sVersion.length > 0)
    return this.sVersion;

  var sVersion = $ss.agentcore.dal.registry.GetRegValue("HKCU", "Software\\Microsoft\\Windows Live Mail", "VerStamp");
  if (sVersion.indexOf(".") == -1)
    sVersion += ".0";

  return (this.sVersion = sVersion);
}

/*******************************************************************************
** Name:         _ssCWindowsLiveMail_GenerateAccountId
********************************************************************************/
ssCWindowsLiveMail.prototype.GenerateAccountId = function _ssCWindowsLiveMail_GenerateAccountId() {
  if (this.sAccountId.length > 0 || this.sMailFolder == "") {
    this._logger.info("_ssCWindowsLiveMail_GenerateAccountId", "AccountId already generated or cannot be generated")
    return;
  }

  var bExisting = false; // track uniqueness of the account id
  do {
    this.sAccountId = "account" + $ss.agentcore.utils.GenGuid(true) + ".oeaccount";
    // Check mail folder
    bExisting = $ss.agentcore.dal.file.FileExists(this.sMailFolder + this.sAccountId);
    if (!bExisting) {
      // Check Local Folder as well
      bExisting = $ss.agentcore.dal.file.FileExists(this.sLocalFolder + this.sAccountId);
    }
  } while (bExisting);
}

/*******************************************************************************
** Name:         _ssCWindowsLiveMail_GetAccountInfo
**
** Purpose       Prepares account object with all values for current passed 
**               account id.
**
** Parameter:    sFileName : Account File Name that needs to be looked up
**
** Return:       oAccount : Account Object
********************************************************************************/
ssCWindowsLiveMail.prototype.GetAccountInfo = function _ssCWindowsLiveMail_GetAccountInfo(sFileName, sPath) {
  this._logger.info("_ssCWindowsLiveMail_GetAccountInfo");
  var oAccount = new Object;
  try {
    var oDOM = $ss.agentcore.dal.xml.LoadXML(sPath + sFileName);
    if (oDOM) {
      oAccount.MailClient = EM_WINDOWS_LIVE_MAIL;
      oAccount.Profile = ""; //TBD:
      oAccount.ServiceId = ""; //TBD:
      oAccount.AccountId = sFileName.replace(/account(\{.*\})\.oeaccount/i, "$1");
      oAccount.AccountFileName = sPath + sFileName;

      oAccount.Type = $ss.agentcore.dal.xml.GetNodeText(oDOM.selectSingleNode("//Connection_Type"));
      oAccount.Name = $ss.agentcore.dal.xml.GetNodeText(oDOM.selectSingleNode("//Account_Name"));
      oAccount.DisplayName = $ss.agentcore.dal.xml.GetNodeText(oDOM.selectSingleNode("//SMTP_Display_Name"));
      oAccount.Email = $ss.agentcore.dal.xml.GetNodeText(oDOM.selectSingleNode("//SMTP_Email_Address"));
      oAccount.Pop3User = $ss.agentcore.dal.xml.GetNodeText(oDOM.selectSingleNode("//POP3_User_Name"));
      oAccount.Pop3Server = $ss.agentcore.dal.xml.GetNodeText(oDOM.selectSingleNode("//POP3_Server"));
      oAccount.SmtpServer = $ss.agentcore.dal.xml.GetNodeText(oDOM.selectSingleNode("//SMTP_Server"));
      // pending : need to get other values Pop3Auth, Pop3SSL e.t.c
      oAccount.Password = $ss.agentcore.dal.xml.GetNodeText(oDOM.selectSingleNode("//POP3_Password2"));
      oAccount.Pop3Port = $ss.agentcore.dal.xml.GetNodeText(oDOM.selectSingleNode("//POP3_Port"));
      oAccount.Pop3SSL = ($ss.agentcore.dal.xml.GetNodeText(oDOM.selectSingleNode("//POP3_Secure_Connection")) == "00000001").toString();
      oAccount.Pop3Auth = ($ss.agentcore.dal.xml.GetNodeText(oDOM.selectSingleNode("//POP3_Use_Sicily")) == "00000001").toString();
      oAccount.SmtpPort = $ss.agentcore.dal.xml.GetNodeText(oDOM.selectSingleNode("//SMTP_Port"));
      oAccount.SmtpSSL = ($ss.agentcore.dal.xml.GetNodeText(oDOM.selectSingleNode("//SMTP_Secure_Connection")) == "00000001").toString();
      oAccount.SmtpAuth = ($ss.agentcore.dal.xml.GetNodeText(oDOM.selectSingleNode("//SMTP_Use_Sicily")) == "00000002").toString();

      if (oAccount.Pop3Port == "")
        oAccount.Pop3Port = "110";
      else
        oAccount.Pop3Port = parseInt(oAccount.Pop3Port, 16).toString();

      if (oAccount.SmtpPort == "")
        oAccount.SmtpPort = "25";
      else
        oAccount.SmtpPort = parseInt(oAccount.SmtpPort, 16).toString();

      if ("account" + oAccount.AccountId + ".oeaccount" == $ss.agentcore.dal.registry.GetRegValue("HKCU", "Software\\Microsoft\\Windows Live Mail", "Default Mail Account"))
        oAccount.IsDefault = "true";
      else
        oAccount.IsDefault = "false";
    }
  } catch (ex) {
    this._exception.HandleException(ex, "_ssCWindowsLiveMail_GetAccountInfo", ex.message);
  }
  return ((oAccount.Email && oAccount.DisplayName) ? oAccount : null);
}

/*******************************************************************************
** Name:         _ssCWindowsLiveMail_SetAccountProfile
********************************************************************************/
ssCWindowsLiveMail.prototype.SetAccountProfile = function _ssCWindowsLiveMail_SetAccountProfile(existingAccountPath, accountExtension, accountType) {
  var bRet = false;
  try {
    var fnTemplate = $ss.getSnapinAbsolutePath("snapin_email") + "\\resource\\windows_live_mail.xml";

    if (fnTemplate.length == 0) return bRet;

    var oDOM = $ss.agentcore.dal.xml.LoadXML(fnTemplate);
    if (oDOM) {
      var oNodes = $ss.agentcore.dal.xml.GetNodes("/MessageAccount/*", "", oDOM.documentElement);
      if (oNodes == null) return bRet;

      // Normalize email object before looping nodes
      var nTmp = parseInt(this.oEmail.sPOPPort);
      this.oEmail.sPOPPort = $ss.snapin.email.utility.NormalizeString(nTmp.toString(16), 8);
      nTmp = parseInt(this.oEmail.sSMTPPort);
      this.oEmail.sSMTPPort = $ss.snapin.email.utility.NormalizeString(nTmp.toString(16), 8);

      for (var i = 0; i < oNodes.length; i++) {
        var sTxt = $ss.agentcore.dal.xml.GetNodeText(oNodes[i]);
        if (sTxt.match(this.sMacros) != null)
          oNodes[i].text = this.MacroReplace(sTxt);
      }

      // Create the mail folders if necessary
      if (!$ss.agentcore.dal.file.FolderExists(this.sMailFolder))
        $ss.agentcore.dal.file.CreateDir(this.sMailFolder);

      if (!$ss.agentcore.dal.file.FolderExists(this.sLocalFolder))
        $ss.agentcore.dal.file.CreateDir(this.sLocalFolder);

      // write oDOM to file
      if (accountType) { //accountType is true if it is a new account
        this.sAccountFileName = this.sMailFolder + "\\account" + this.sAccountId + accountExtension;
      } else {
        if (existingAccountPath) {
          var accID = this.sAccountId;
          if (accID.match(/account{/i) == null)
            accID = "account" + this.sAccountId + ".oeaccount";

          this.sAccountFileName = this.sCurrentMailFolder + accID;
        } else {
          this.sAccountFileName = this.sMailFolder + "\\" + this.sAccountId;
        }
      }
      try {

        // Check that the mail folder actually exists

        var oFile = $ss.agentcore.dal.file.CreateTextFile(this.sAccountFileName, true, true);
        bRet = $ss.agentcore.dal.file.WriteFile(this.sAccountFileName, oDOM.xml, $ss.agentcore.constants.FILE_MODE.WRITE);
        //oFile.Close();
        //bRet = true; 

        // Make this default mail account
        if (this.oEmail.sIsDefault === true) {
          this.SetAsDefault("", "", this.sAccountId);
        }
      } catch (ex) {
        var sErr = "Error saving file " + this.sAccountFileName + "! " + ex.message;
        this._exception.HandleException(ex, '$ss.snapin.email.utility', '_ssCWindowsLiveMail_SetAccountProfile', arguments);
      }
    } else {
      var sErr = "Error loading DOM, " + fnTemplate;
      this._logger.info("_ssCWindowsLiveMail_SetAccountProfile", sErr);
    }
  } catch (ex) {
    this._exception.HandleException(ex, '$ss.snapin.email.utility', '_ssCWindowsLiveMail_SetAccountProfile', arguments);
  }
  return bRet;
}


/*******************************************************************************
** Name:         _ssCWindowsLiveMail_GetMailConfigValue
** Desc:         Get Config values specific to Windows Live Mail
********************************************************************************/
ssCWindowsLiveMail.prototype.GetMailConfigValue = function _ssCWindowsLiveMail_GetMailConfigValue(sName, sDefault) {
  try {
    return $ss.snapin.email.utility.GetWinLiveMailConfig(sName, sDefault);
  } catch (e) {
  }
  return "";
}

/*******************************************************************************
** Name:         _ssCWindowsLiveMail_GetAccountPassword
********************************************************************************/
ssCWindowsLiveMail.prototype.GetAccountPassword = function _ssCWindowsLiveMail_GetAccountPassword(sPassword)
{
  var sEncPassword = "";
  var sSalt = "";
  var hTemp;
  try {
    // Guard against empty password
    if (typeof this.oEmail.sPassword != "undefined" && this.oEmail.sPassword.length > 0) {
      //Get/Set Salt value
      if($ss.agentcore.dal.registry.RegValueExists("HKCU", "Software\\Microsoft\\Windows Live Mail", "Salt")) {
        sSalt = $ss.agentcore.dal.registry.GetRegValue("HKCU", "Software\\Microsoft\\Windows Live Mail", "Salt");
      } else {      
        for(var i=0; i<16; i++) {
          hTemp = Math.round((255)*Math.random()).toString(16);
          if (hTemp.length === 2) sSalt+= hTemp;
          else i--;//if 2 digits are not genarated, regenerate
        }
        if(!$ss.agentcore.dal.registry.SetRegValueByTypeEx("HKCU", "Software\\Microsoft\\Windows Live Mail", "Salt", $ss.agentcore.constants.REG_BINARY, sSalt))
          throw("Registry Write Failed","HKCU", "Software\\Microsoft\\Windows Live Mail", "Salt", $ss.agentcore.constants.REG_BINARY, sSalt);
      }
      var oSSMail = $ss.agentcore.utils.activex.CreateObject("SdcUser.SdcMailCtl");
      sEncPassword = oSSMail.EncryptMailPassword(sPassword, sSalt);      
      sEncPassword = sEncPassword.replace(/\s+/g,"");
      delete oSSMail;
    }
  } catch(ex) {    
    this._exception.HandleException(ex,'$ss.snapin.email.utility','_ssCWindowsLiveMail_GetAccountPassword',arguments);
  }
  return sEncPassword;
}
/*******************************************************************************
** Name:         _ssCWindowsLiveMail_SetAccountPassword
********************************************************************************/
ssCWindowsLiveMail.prototype.SetAccountPassword = function _ssCWindowsLiveMail_SetAccountPassword() {
  var bRet = false;
  try {
    var sAccPath = this.GetCurrentMailFolder(this.oEmail.sAccountId) + "\\account" + this.oEmail.sAccountId + ".oeaccount";
    if ($ss.agentcore.dal.file.FileExists(sAccPath)) {
      var oDOM = $ss.agentcore.dal.xml.LoadXML(sAccPath);
      if (oDOM) {
        var oNodes = $ss.agentcore.dal.xml.GetNodes("/MessageAccount/POP3_Password2", "", oDOM.documentElement);
        if (oNodes == null) return bRet;
        for (var i = 0; i < oNodes.length; i++) {
          oNodes[i].text = this.GetAccountPassword(this.oEmail.sPassword);
        }
      }
    }
    bRet = $ss.agentcore.dal.file.WriteFile(sAccPath, oDOM.xml, $ss.agentcore.constants.FILE_MODE.WRITE);
  } catch (ex) {
    this._exception.HandleException(ex, '$ss.snapin.email.utility', '_ssCWindowsLiveMail_SetAccountPassword', arguments);
  }
  return bRet;
}  

/*******************************************************************************
** Name:         _ssCWindowsLiveMail_AuthenticatePassword
**
** Purpose:      Get the sProfile variable, if its empty then tries to get 
**               default profile or first profile 
**               profiles are used : 2002 onwards  
**               
** Parameter:    none       And no need to have class instantiated with valid
**                          oEmail object
**
** Return:       this.sProfile : string having profile name or empty incase of 
**                          failures
********************************************************************************/
ssCWindowsLiveMail.prototype.AuthenticatePassword = function _ssCWindowsLiveMail_AuthenticatePassword() {
  this._logger.info("_ssCWindowsLiveMail_AuthenticatePassword");
  var bAccountAuthenticatedOK = false;
  if (!this.oEmail.sAccountName) return false;
  try {
    var mailobj = new $ss.agentcore.utils.activex.CreateObject("SdcUser.SdcMailCtl");
    bAccountAuthenticatedOK = mailobj.AuthenticateWLMPop(this.GetCurrentMailFolder(this.oEmail.sAccountId) + "\\account" + this.oEmail.sAccountId + ".oeaccount");
    delete mailobj;
  } catch (e) { }
  return bAccountAuthenticatedOK;
}

/*******************************************************************************
** Name:         _ssCWindowsLiveMail_IsPasswordSaved
**
** Purpose:      Get the sProfile variable, if its empty then tries to get 
**               default profile or first profile 
**               profiles are used : 2002 onwards  
**               
** Parameter:    none       And no need to have class instantiated with valid
**                          oEmail object
**
** Return:       this.sProfile : string having profile name or empty incase of 
**                          failures
********************************************************************************/
ssCWindowsLiveMail.prototype.IsPasswordSaved = function _ssCWindowsLiveMail_IsPasswordSaved()
{  

  // The Windows Mail function fails for this, so we just say it is saved
  
  this._logger.info("_ssCWindowsLiveMail_IsPasswordSaved");
  return (true);
}


/*******************************************************************************
** Name:         _DeleteAccount_DeleteAccount
**
** Purpose:      Delete the specifed Windows Live Mail email account
**               
** Parameter:    sProfile - Not used
**               sServiceId - Not used
**               sAccountId - Account Id (AccountId property of account object)
**
** Return:       1 - Success, 0 - Failure
**               
********************************************************************************/
ssCWindowsLiveMail.prototype.DeleteAccount = function _ssCWindowsLiveMail_DeleteAccount(sProfile, sServiceId, sAccountId)
{
  this._logger.info("_ssCWindowsMail_DeleteAccount");

  if(sAccountId.match(/account{/i) == null)
    sAccountId = "account" + sAccountId + ".oeaccount"
  
  var sAccountPath = this.GetMailFolder() + sAccountId;
  var bExisting = $ss.agentcore.dal.file.FileExists(sAccountPath); 
  // Account file may be in base folder (if user not opend the mail client after configuring through agent)
  if(bExisting)
    bExisting = $ss.agentcore.dal.file.DeleteFile(sAccountPath, true);
 
  // Account file may be in some folder (if user opens the mail client after configuring through agent)
  // Delete whole folder if account file exists in folder
   var sFolder = this.GetMailFolder();   
   var g_oFso = $ss.agentcore.dal.file.GetFileSystemObject();
   var oFoldObj = g_oFso.GetFolder(sFolder);       
   var Collection = new Enumerator(oFoldObj.SubFolders);
   var oCollectionObj;
    for(Collection.moveFirst();!Collection.atEnd();Collection.moveNext())
    {
      oCollectionObj = Collection.item();
      var sSubFolderPath = sFolder + oCollectionObj.Name + "\\"
      var aFiles = $ss.agentcore.dal.file.GetFilesList(sSubFolderPath, /account\{.*\}\.oeaccount/i);    
      for (var i=0; i<aFiles.length; i++) 
      {     
        if(aFiles[i] == sAccountId)
        {
          bExisting = $ss.agentcore.dal.file.DeleteDir(sSubFolderPath,true)
          break;
        }
      }  
    } 
    
  return bExisting;
}

/*******************************************************************************
** Name:         ssCWindowsLiveMail_SetAsDefault
**
** Purpose:      Set specifed Win Live Mail email account as default
**               
** Parameter:    sProfile - Not used
**               sServiceId - Not Used
**               sAccountId - Account Id (AccountId property of account object)
**
** Return:       1 - Success, 0 - Failure
**               
********************************************************************************/
ssCWindowsLiveMail.prototype.SetAsDefault = function _ssCWindowsLiveMail_SetAsDefault(sProfile, sServiceId, sAccountId)
{
  this._logger.info("ssCWindowsLiveMail_SetAsDefault");
  if(sAccountId.match(/account{/i) == null)
    sAccountId = "account" + sAccountId + ".oeaccount";
  
  $ss.agentcore.dal.registry.SetRegValueByType("HKCU", "Software\\Microsoft\\Windows Live Mail", "Default Mail Account", 1, sAccountId);
  return 1;
}

/*******************************************************************************
** Name:         _GetCurrentMailFolder_GetCurrentMailFolder
**
** Purpose:      Get current mail folder of Windows live mail account
**               
** Parameter:   sAccountId - Account Id (AccountId property of account object)
**
** Return:       folderpath if exists or empty
**               
********************************************************************************/
ssCWindowsLiveMail.prototype.GetCurrentMailFolder = function _ssCWindowsLiveMail_GetCurrentMailFolder(sAccountId)
{
  this._logger.info("_ssCWindowsLiveMail_GetCurrentMailFolder");
  
  var folderPath  = "" ;
  if(!this.GetMailFolder())
    return folderPath;
  if(sAccountId.match(/account{/i) == null)
    sAccountId = "account" + sAccountId + ".oeaccount"
  
  var sAccountPath = this.GetMailFolder() + sAccountId;  
  // Account file may be in base folder (if user not opend the mail client after configuring through agent)
  if($ss.agentcore.dal.file.FileExists(sAccountPath))
    return this.GetMailFolder();
 
  // Account file may be in some folder (if user opens the mail client after configuring through agent)  
   var sFolder = this.GetMailFolder();   
   var g_oFso = $ss.agentcore.dal.file.GetFileSystemObject();
   var oFoldObj = g_oFso.GetFolder(sFolder);       
   var Collection = new Enumerator(oFoldObj.SubFolders);
   var oCollectionObj;
    for(Collection.moveFirst();!Collection.atEnd();Collection.moveNext())
    {
      oCollectionObj = Collection.item();
      var sSubFolderPath = sFolder + oCollectionObj.Name + "\\"
      var aFiles = $ss.agentcore.dal.file.GetFilesList(sSubFolderPath, /account\{.*\}\.oeaccount/i);    
      for (var i=0; i<aFiles.length; i++) 
      {     
        if(aFiles[i] == sAccountId)
        {
          folderPath = sSubFolderPath;
          break;
        }
      }  
      if(folderPath != "")
      break;
    } 
    
  return folderPath;
}



/*******************************************************************************
**      C L A S S  : ssCOutlook 
**      Class for supporting Microsoft Outlook 97 and 2000
********************************************************************************/
function ssCOutlook(oEmail)
{
  ssCBaseEmail.call(this, oEmail);  // ssCoutlook inherits from ssCBaseEmail
  this.sProfileRegKey = ssCOutlook.PROFILE_REG_KEY;    // registry key to be used for profile
  this.sProfile = "";               // tracks default or first profile in profiles

  this._logger = $ss.agentcore.log.GetDefaultLogger("$ss.snapin.email.ssCOutlook");
}

ssCOutlook.prototype = new ssCBaseEmail(); // initialize prototype object of derived with base 
ssCOutlook.ACCT_MGR_KEY              = "Software\\Microsoft\\Office\\Outlook\\OMI Account Manager\\Accounts\\";
ssCOutlook.PROFILE_REG_KEY           = "SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Windows Messaging Subsystem\\Profiles\\";
ssCOutlook.PROFILE_REG_KEY_98ME      = "SOFTWARE\\Microsoft\\Windows Messaging Subsystem\\Profiles\\";
ssCOutlook.POP3_SMTP_SERVICE_SUBKEY  = "\\9375CFF0413111d3B88A00104B2A6676\\";
ssCOutlook.PROFILE_REG_KEY_WIN8      = "Software\\Microsoft\\Office\\";

ssCOutlook.POP3_SMTP_SERVICE_ID      = "9375CFF0413111d3B88A00104B2A6676";
ssCOutlook.EXCH_SERVICE_ID           = "13dbb0c8aa05101a9bb000aa002fc45a";
ssCOutlook.EXCH_ACCT_MGR_KEY         = "9207f3e0a3b11019908b08002b2a56c2";
ssCOutlook.EXCH_KEY_VAL              = "001e6608";                  
ssCOutlook.ACCT_INDEX_VALUE          = "{ED475418-B0D6-11D2-8C3B-00104B2A6676}";
ssCOutlook.SERVICE_CLSID             = "{ED475411-B0D6-11D2-8C3B-00104B2A6676}";

ssCOutlook.INTERNET_EMAIL_2K_ID      = "{8B8853C0-37BD-11D2-A4E2-006008AF820E}";

ssCOutlook.ACCT_TYPE_KEY             = "SOFTWARE\\Microsoft\\Office\\9.0\\Outlook\\Setup\\";
ssCOutlook.ACCT_TYPE_EXCHANGE        = "{ED475414-B0D6-11D2-8C3B-00104B2A6676}";
ssCOutlook.ACCT_TYPE_POP             = "{ED475411-B0D6-11D2-8C3B-00104B2A6676}";
ssCOutlook.ACCT_TYPE_PIMONLY         = 1;
ssCOutlook.ACCT_TYPE_NOMAILSUPPORT   = 2;
ssCOutlook.ACCT_TYPE_MAILSUPPORT     = 3;
//Used by Outlook to check email with the profile we created from scratch
ssCOutlook.REG_CRYPT_PROFILE_KEY     = "0102300b";
ssCOutlook.REG_CRYPT_PROFILE_VALUE   = "bb6580ca5d82464295b62b3b50979eef";
ssCOutlook.REG_CRYPT_PROFILE_SEED    = "8503020000000000c000000000000046";
/*******************************************************************************
** Name:         _ssCOutlook_GetProfile
**
** Purpose:      Get the sProfile variable, if its empty then tries to get 
**               default profile or first profile 
**               profiles are used : 2002 onwards  
**               
** Parameter:    none       And no need to have class instantiated with valid
**                          oEmail object
**
** Return:       this.sProfile : string having profile name or empty incase of 
**                          failures
********************************************************************************/
ssCOutlook.prototype.GetProfile = function _ssCOutlook_GetProfile() {
  this._logger.info("_ssCOutlook_GetProfile");
  if (this.sProfile != "") return this.sProfile;
  
  if (this.sProfileRegKey != null && this.sProfileRegKey.length < 1) {
    this.sProfileRegKey = $ss.agentcore.utils.GenGuid(true);
  }
  var sDefault = $ss.agentcore.dal.registry.GetRegValue("HKCU", this.sProfileRegKey, "DefaultProfile");
  if (sDefault != null && sDefault.length > 0) {
    return (this.sProfile = sDefault);
  } else {
    sDefault = "Outlook";
    var defaultProfileReg = this.sProfileRegKey.substr(0, this.sProfileRegKey.length -1);
    $ss.agentcore.dal.registry.SetRegValueByType("HKCU", this.sProfileRegKey, "DefaultProfile", 1, sDefault);
    var sCryptProfileRegKey = this.sProfileRegKey + sDefault + "\\" + ssCOutlook.REG_CRYPT_PROFILE_SEED;
    $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sCryptProfileRegKey, ssCOutlook.REG_CRYPT_PROFILE_KEY, 3, ssCOutlook.REG_CRYPT_PROFILE_VALUE);
    return (this.sProfile = sDefault);
  }
  
  // if no default profile explicitly specified, use first profile found
  var subKeys = $ss.agentcore.dal.registry.EnumRegKey("HKCU", this.sProfileRegKey,",");
  if (subKeys != null && subKeys.length > 0) {
    var arSubKeys = subKeys.split(",");
    return (this.sProfile = arSubKeys[0]);
  }

  return (this.sProfile = "");
}

/*******************************************************************************
** Name:         _ssCOutlook_SetProfile
**
** Purpose:      Set the sProfile variable with passed value
**               profiles are used : 2002 onwards  
**               
** Parameter:    none       And no need to have class instantiated with valid
**                          oEmail object
**
** Return:       none
********************************************************************************/
ssCOutlook.prototype.SetProfile = function _ssCOutlook_SetProfile(sProfile)
{
  this.sProfile = sProfile;
}

/*******************************************************************************
** Name:         _ssCOutlook_CreateAccount
**
** Purpose:      Create an email account using the default parameters for 
**               Microsoft Outlook client basically 97 and 2000
**
** Parameter:    none       uses oEmail object of class
**
** Return:       int 0=OK, 1=incorrect_state (run wizard), 2=Other bad error
********************************************************************************/
ssCOutlook.prototype.CreateAccount = function _ssCOutlook_CreateAccount()
{
  // If Internet mail not configured return
  if(!HasOutlookInternet()) return EM_RET_INCORRECT_STATE;
  
  // Get existing accounts
  var arExistingAccounts = $ss.agentcore.dal.registry.EnumRegKey("HKCU", ssCOutlook.ACCT_MGR_KEY,",");
  if (arExistingAccounts.length == 0 ) {
    this.sAccountId = "00000001";
  } else {
    var accounts = arExistingAccounts.split(',');
    accounts.sort();
    var highKey = parseInt(accounts[accounts.length-1]);
    this.sAccountId = "0000000" + ++highKey;
    this.sAccountId = this.sAccountId.substr( this.sAccountId.length - 8);
  }
  
  // Generate Unique Account Name
  // TO DO - change this.sAccountName
  this.oEmail.GetUniqueName(ssCOutlook.ACCT_MGR_KEY + "\\");    
  
  // Set values in registry
  var sRegKey = ssCOutlook.ACCT_MGR_KEY + this.sAccountId;
  this.SetAccountProfile(sRegKey);
  
  // Set password
  this.SetAccountPassword();
  
  // Set as default
  if(this.oEmail.sIsDefault === true)
    this.SetAsDefault(this.sProfile, ssCOutlook.POP3_SMTP_SERVICE_SUBKEY, this.sAccountId);
  
  return 0;
}

/*******************************************************************************
** Name:         _ssCOutlook_GetAccounts
**
** Purpose:      Get an array of Account Objects for the set profile.
**              
**               Enumerate HKEY_CURRENT_USER\Software\Microsoft\Windows NT\
**               CurrentVersion\Windows Messaging Subsystem\Profiles\<PROFILE>
**               Index each service by the 001e661f value.
**
**               Enumerate HKEY_CURRENT_USER\Software\Microsoft\Office\Outlook\
**               OMI Account Manager\Accounts
**               Index each Account by the Account Name value.
**
**               Cross Reference -- If a key exists in both lists this account is 
**               tied to this service, and therefore this Profile.
**
**
** Parameter:    none       uses oEmail object of class
**
** Return:       array of account objects
********************************************************************************/
ssCOutlook.prototype.GetAccounts = function _ssCOutlook_GetAccounts(aAccounts)
{
  this._logger.info("_ssCOutlook_GetAccounts");
  if (typeof aAccounts == "undefined" || aAccounts == null)
    aAccounts = new Array();

  // Make sure we have a profile, even if caller has not set
  this.GetProfile();
  
  // Get Account Listing Behavior Type
  var iAccountBehaviorType = this.GetAccountTypeBehavior();

  if (iAccountBehaviorType == ssCOutlook.ACCT_TYPE_PIMONLY || 
      iAccountBehaviorType == ssCOutlook.ACCT_TYPE_NOMAILSUPPORT) {
    
    // Handle POP / Internet Accounts
    var hAccountNameIndex = this.GetOfficeOMIAccountNameIndex();
    for (var h in hAccountNameIndex ) {
      aAccounts.push( this.GetAccountInfo( null, hAccountNameIndex[h] ) ); 
    }

  } else {

    // Handle Exchange
    var sProfileServiceRegKey = this.sProfileRegKey + this.sProfile ;
    var sProfileServiceKeys   = $ss.agentcore.dal.registry.EnumRegKey("HKCU", sProfileServiceRegKey,",");
    var aProfileServiceKeys, sAccountRegKey, sServerAddress;

    if (sProfileServiceKeys != null && sProfileServiceKeys != "") {
      aProfileServiceKeys = sProfileServiceKeys.split(",");
      for (var i=0; i<aProfileServiceKeys.length; i++) {
        sAccountRegKey = sProfileServiceRegKey + "\\" + aProfileServiceKeys[i];
        sServerAddress = $ss.agentcore.dal.registry.GetRegValue("HKCU", sAccountRegKey, ssCOutlook.EXCH_KEY_VAL);  // EXCHANGE
        if (sServerAddress != "" && sServerAddress != null) {
          aAccounts.push( this.GetAccountInfo( aProfileServiceKeys[i], null ) );
        }
      }
    }

    // TODO: Seems necessary in 2k2 onwards
    // Handle POP / Internet Accounts
    var hAccountNameIndex = this.GetOfficeOMIAccountNameIndex();
    var hDefaultProfileAccountNameIndex = this.GetProfileAccountNameIndex();

    for (var i in hDefaultProfileAccountNameIndex) {
      if (hAccountNameIndex[i]) { // Internet Email Based Accounts
        aAccounts.push( this.GetAccountInfo( hDefaultProfileAccountNameIndex[i], hAccountNameIndex[i] ) );
      }
    }
  }

  return aAccounts;
}

/*******************************************************************************
** Name:         _ssCOutlook_GetAccountType
** TODO: can be deleted now
** Purpose:      Returns the account type to determine if its an exchange or 
**               pop account
**
** Parameter:    sServiceId :
**               sAccountId : 
**
** Return:       Account type : Exchange or POP account
********************************************************************************/
ssCOutlook.prototype.GetAccountType = function _ssCOutlook_GetAccountType(sAccountId, sServiceId) 
{
  this._logger.info("_ssCOutlook_GetAccountType");
  var sAccountType = "";
  try
  {
    sAccountType = (sAccountId == null && sServiceId != null) ? ssCOutlook.ACCT_TYPE_EXCHANGE : ssCOutlook.ACCT_TYPE_POP;
  }
  catch (ex)
  {
    var sMsg = "Failed in getting account type for AccountId: " + sAccountId;
    sMsg += ", ServiceId: " + sServiceId + " Error: " + ex.message;
    this._logger.info("_ssCOutlook_GetAccountType", sMsg);
    
    exception.HandleException(ex,'$ss.snapin.email.utility','ssCOutlook.prototype.GetAccountType',arguments);
  }
  return acctType;
}

/*******************************************************************************
** Name:         _ssCOutlook_GetAccountTypeBehavior
********************************************************************************/
ssCOutlook.prototype.GetAccountTypeBehavior = function _ssCOutlook_GetAccountTypeBehavior() 
{
  this._logger.info("_ssCOutlook_GetAccountTypeBehavior");
  switch (this.GetVersion()) 
  {
    default : 
      return EM_RET_OTHER_ERROR; 
    
    case EM_OLK_VER1997:  
    case EM_OLK_VER2002:
    case EM_OLK_VER2003:
      return 0;
      
    case EM_OLK_VER2000:
      var sPIMOnly = "" + $ss.agentcore.dal.registry.GetRegValue("HKLM", ssCOutlook.ACCT_TYPE_KEY, "PIMOnly");
      if (sPIMOnly != "") {
        var iPIMOnly = parseInt(sPIMOnly);
        if (iPIMOnly == 1) {
          return ssCOutlook.ACCT_TYPE_PIMONLY;
        } else {
          var sMailSupport = "" + $ss.agentcore.dal.registry.GetRegValue("HKLM", ssCOutlook.ACCT_TYPE_KEY, "MailSupport");
          if (sMailSupport != "") {
            var iMailSupport = parseInt(sMailSupport);
            switch (iMailSupport) {
              case 1 : return ssCOutlook.ACCT_TYPE_MAILSUPPORT;
              case 0 : return ssCOutlook.ACCT_TYPE_NOMAILSUPPORT;
            }
          }
        }
      }
      return 0;
  }
  return 0;
}

/*******************************************************************************
** Name:         _ssCOutlook_GetAccountInfo
********************************************************************************/
ssCOutlook.prototype.GetAccountInfo = function _ssCOutlook_GetAccountInfo(sServiceId, sAccountId ) 
{
  this._logger.info("_ssCOutlook_GetAccountInfo");
  // if no profile specified, then we'll use inbuilt default or first profile
  var sProfile = this.GetProfile();
  //if (sProfile.length == 0) return "";
  
  var oAccount = new Object;
  oAccount.MailClient     = EM_MS_OUTLOOK;
  oAccount.Profile        = sProfile;
  oAccount.AccountId      = sAccountId;
  oAccount.ServiceId      = sServiceId;
  oAccount.Type           = (sAccountId == null && sServiceId != null) ? ssCOutlook.ACCT_TYPE_EXCHANGE : ssCOutlook.ACCT_TYPE_POP;
  oAccount.ExchangeUser   = "";
  oAccount.ExchangeServer = "";
  oAccount.Pop3Server     = "";
  oAccount.SmtpServer     = "";
  oAccount.Pop3User       = "";

  var sRegKey = "";
  if (oAccount.Type == ssCOutlook.ACCT_TYPE_EXCHANGE) 
  {
    if (sServiceId == null) sServiceId = ssCOutlook.EXCH_SERVICE_ID;
    //var sServiceKey = this.sProfileRegKey + sProfile + "\\" + sServiceId + "\\";
    sRegKey =  this.sProfileRegKey + sProfile + "\\" + sServiceId + "\\";
    
    oAccount.ExchangeServer = $ss.agentcore.dal.registry.GetRegValue("HKCU", sRegKey, ssCOutlook.EXCH_KEY_VAL);
    oAccount.Name           = $ss.agentcore.dal.registry.GetRegValue("HKCU", sRegKey, "001e3001");
    oAccount.DisplayName    = $ss.agentcore.dal.registry.GetRegValue("HKCU", sRegKey, "Display Name");
    oAccount.Email          = $ss.agentcore.dal.registry.GetRegValue("HKCU", sRegKey, "Email");
    /* Check specific reg areas (unicode encoded)  in registry for unset values */
    if(oAccount.ExchangeUser=="") oAccount.ExchangeUser = this.GetUniversalReg("HKCU", sRegKey, "001f6607");    
    if(oAccount.DisplayName=="")  oAccount.DisplayName = this.GetUniversalReg("HKCU", sRegKey, "001f6620");    
    if(oAccount.Name=="")         oAccount.Name = this.GetUniversalReg("HKCU", sRegKey, "001f3001");    

    if(oAccount.Email=="") {
      try {
        var sExEmails = new String(this.GetUniversalRegWithNewline("HKCU", sRegKey, "101f6637"));
        var aExEmails = sExEmails.split(/\n/);
        for(var i=0; i<aExEmails.length; i++) {
          if(aExEmails[i].match(/smtp:/i)) {
            oAccount.Email += aExEmails[i].replace(/smtp:/i,"");
            //don't grab all email accounts.
            break;
          }
        }
      }catch(ex) {}
    }    
  }
  else if (oAccount.Type == ssCOutlook.ACCT_TYPE_POP) 
  {
    sRegKey = ssCOutlook.ACCT_MGR_KEY + sAccountId;
    
    oAccount.Name           = $ss.agentcore.dal.registry.GetRegValue("HKCU", sRegKey, "Account Name");
    oAccount.DisplayName    = $ss.agentcore.dal.registry.GetRegValue("HKCU", sRegKey, "Account Name");
    oAccount.Email          = $ss.agentcore.dal.registry.GetRegValue("HKCU", sRegKey, "SMTP Email Address");
    oAccount.Pop3User       = $ss.agentcore.dal.registry.GetRegValue("HKCU", sRegKey, "POP3 User Name");
    oAccount.Pop3Server     = $ss.agentcore.dal.registry.GetRegValue("HKCU", sRegKey, "POP3 Server");
    oAccount.Pop3SSL        = ($ss.agentcore.dal.registry.GetRegValue("HKCU", sRegKey, "POP3 Secure Connection") == "1").toString ();
    oAccount.Pop3Auth       = ($ss.agentcore.dal.registry.GetRegValue("HKCU", sRegKey, "POP3 Use Sicily") == "1").toString ();
    oAccount.SmtpServer     = $ss.agentcore.dal.registry.GetRegValue("HKCU", sRegKey, "SMTP Server");
    oAccount.SmtpSSL        = ($ss.agentcore.dal.registry.GetRegValue("HKCU", sRegKey, "SMTP Secure Connection") == "1").toString ();
    oAccount.SmtpAuth       = ($ss.agentcore.dal.registry.GetRegValue("HKCU", sRegKey, "SMTP Use Sicily") == "2").toString ();

    oAccount.Pop3Port       = $ss.agentcore.dal.registry.GetRegValue("HKCU", sRegKey, "POP3 Port");
    if (oAccount.Pop3Port == "")
       oAccount.Pop3Port = "110";
    else
       oAccount.Pop3Port = parseInt (oAccount.Pop3Port,16).toString ();

    oAccount.SmtpPort    = $ss.agentcore.dal.registry.GetRegValue("HKCU", sRegKey, "SMTP Port");
    if (oAccount.SmtpPort == "")
       oAccount.SmtpPort = "25";
    else
       oAccount.SmtpPort = parseInt (oAccount.SmtpPort,16).toString ();
  }

  var sRegKey = ssCOutlook.ACCT_MGR_KEY.replace(/Accounts\\/,"");
  if($ss.agentcore.dal.registry.GetRegValue("HKCU", sRegKey, "Default Mail Account") == sAccountId)
    oAccount.IsDefault = "true";
  else
    oAccount.IsDefault = "false";      

  return oAccount;
}

/*******************************************************************************
** Name:         _ssCOutlook_GetOfficeOMIAccountNameIndex
********************************************************************************/
ssCOutlook.prototype.GetOfficeOMIAccountNameIndex = function _ssCOutlook_GetOfficeOMIAccountNameIndex() 
{
  this._logger.info("_ssCOutlook_GetOfficeOMIAccountNameIndex");
  var sAccountList = $ss.agentcore.dal.registry.EnumRegKey("HKCU", ssCOutlook.ACCT_MGR_KEY,",");
  if (sAccountList != null && sAccountList != "")
    var aAccountList = sAccountList.split(",");
  else
    return new Object;

  var hAccountNameIndex = new Object;
  var sAccountName   = "";
  var sAccountRegKey = "";

  for (var i=0; i<aAccountList.length; i++) {
    sAccountRegKey = ssCOutlook.ACCT_MGR_KEY + aAccountList[i];
    sAccountName = $ss.agentcore.dal.registry.GetRegValue("HKCU", sAccountRegKey, "Account Name");
    if (sAccountName != null && sAccountName != "") {
      hAccountNameIndex[sAccountName] = aAccountList[i];
    }
  }

  return hAccountNameIndex;
}

/*******************************************************************************
** Name:         _ssCOutlook_GetProfileAccountNameIndex
********************************************************************************/
ssCOutlook.prototype.GetProfileAccountNameIndex = function _ssCOutlook_GetProfileAccountNameIndex() 
{
  this._logger.info("_ssCOutlook_GetProfileAccountNameIndex");
  var sProfileServiceRegKey = this.sProfileRegKey + this.GetProfile() ;
  var sProfileServiceKeys   = $ss.agentcore.dal.registry.EnumRegKey("HKCU", sProfileServiceRegKey,",");

  if (sProfileServiceKeys != null && sProfileServiceKeys != "")
    var aProfileServiceKeys = sProfileServiceKeys.split(",");
  else
    return new Object;

  var hServiceNameIndex = new Object;
  var sAccountName   = "";
  var sAccountRegKey = "";

  for (var i=0; i<aProfileServiceKeys.length; i++) {
    sAccountRegKey = sProfileServiceRegKey + "\\" + aProfileServiceKeys[i];
    sAccountName = $ss.agentcore.dal.registry.GetRegValue("HKCU", sAccountRegKey, "001e661f"); // POP
    if (sAccountName != null && sAccountName != "") {
      hServiceNameIndex[sAccountName] = aProfileServiceKeys[i];
    }
  }

  return hServiceNameIndex;
}


/*******************************************************************************
** Name:         _ssCOutlook_GetVersion
********************************************************************************/
ssCOutlook.prototype.GetVersion = function _ssCOutlook_GetVersion()
{
  this._logger.info("_ssCOutlook_GetVersion");
  if(this.sVersion == "")
    this.sVersion = GetOutlookVersion();
    
  return this.sVersion;  
}

/*******************************************************************************
** Name:         _ssCOutlook_SetAccountProfile
********************************************************************************/
ssCOutlook.prototype.SetAccountProfile = function _ssCOutlook_SetAccountProfile(sRegKey)
{
    if (sRegKey == "" || typeof sRegKey == "undefined" || sRegKey == null)
      sRegKey = this.sACCT_MGR_KEY + "\\" + this.sAccountId;

    try {
    $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "Account Name", 1, this.oEmail.sAccountName);
    $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "Connection Type" , 4, 0); 
    $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "Connection Flags" , 4, 0); 
    $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "Leave Mail On Server", 4,0); 
    $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "POP3 Port", 4, parseInt(this.oEmail.sPOPPort));
    $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "POP3 Prompt for Password", 4, 0); 
    $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "POP3 Server" , 1, this.oEmail.sPOPServer );   
    $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "POP3 Skip Account", 4, 0);   
    $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "POP3 Timeout" , 4, 60  );   
    $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "POP3 User Name", 1, this.oEmail.sUserName );
    $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "SMTP Display Name" , 1, this.oEmail.sDisplayName ); 
    $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "SMTP Email Address" , 1, this.oEmail.sEmailAddress); 
    $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "SMTP Port" ,4 ,parseInt(this.oEmail.sSMTPPort)); 
    $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "SMTP Server" , 1, this.oEmail.sSMTPServer ); 
    $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "SMTP Split Messages", 4,0);     
    $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "SMTP Timeout" , 4, 60 );
    if (this.oEmail.sSMTPSSL == "true")
      $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "SMTP Secure Connection", 4, 1);
    else
      $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "SMTP Secure Connection", 4, 0);

    if (this.oEmail.sSMTPAuth == "true")
      $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "SMTP Use Sicily", 4, 2);
    else
      $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "SMTP Use Sicily", 4, 0);
  
    if (this.oEmail.sPOPSSL == "true")
      $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "POP3 Secure Connection", 4, 1);
    else
      $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "POP3 Secure Connection", 4, 0);

    if (this.oEmail.sPOPAuth == "true")
      $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "POP3 Use Sicily", 4, 1);
    else
      $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "POP3 Use Sicily", 4, 0);
  }  catch (ex) {
  }
  
}

/*******************************************************************************
** Name:         _ssCOutlook_SetAccountPassword
********************************************************************************/
ssCOutlook.prototype.SetAccountPassword = function _ssCOutlook_SetAccountPassword()
{
  this._logger.info("_ssCOutlook_SetAccountPassword");
  var oSSMail = $ss.agentcore.utils.activex.CreateObject("SdcUser.SdcMailCtl");
  oSSMail.ResetOLPassword(this.sAccountId, this.oEmail.sPassword);
  delete oSSMail;
}  

/*******************************************************************************
** Name:         _ssCOutlook_AuthenticatePassword
**
** Purpose:      Get the sProfile variable, if its empty then tries to get 
**               default profile or first profile 
**               profiles are used : 2002 onwards  
**               
** Parameter:    none       And no need to have class instantiated with valid
**                          oEmail object
**
** Return:       this.sProfile : string having profile name or empty incase of 
**                          failures
********************************************************************************/
ssCOutlook.prototype.AuthenticatePassword = function _ssCOutlook_AuthenticatePassword()
{
  this._logger.info("_ssCOutlook_AuthenticatePassword");
  if (! this.sAccountId)
    return false;
  var bAccountAuthenticatedOK = false;
  try {
    var mailobj = $ss.agentcore.utils.activex.CreateObject("SdcUser.SdcMailCtl");
    bAccountAuthenticatedOK = mailobj.AuthenticateOL2000Pop(this.sAccountId);
    delete mailobj;
  } catch(ex) {}
  return bAccountAuthenticatedOK;
}

/*******************************************************************************
** Name:         _ssCOutlook_IsPasswordSaved
**
** Purpose:      Get the sProfile variable, if its empty then tries to get 
**               default profile or first profile 
**               profiles are used : 2002 onwards  
**               
** Parameter:    none       And no need to have class instantiated with valid
**                          oEmail object
**
** Return:       this.sProfile : string having profile name or empty incase of 
**                          failures
********************************************************************************/
ssCOutlook.prototype.IsPasswordSaved = function _ssCOutlook_IsPasswordSaved()
{
  this._logger.info("_ssCOutlook_IsPasswordSaved");
  try {
    if (! this.sAccountId )
      return false;
    var bPasswordSaved = false;
    var mailobj = $ss.agentcore.utils.activex.CreateObject("SdcUser.SdcMailCtl");
    bPasswordSaved = mailobj.IsOL2000PasswordSaved(this.sAccountId);
    delete mailobj;
  } catch(ex) {
  }
  return bPasswordSaved;
}

/*******************************************************************************
** Name:         _ssCOutlook_DeleteAccount
**
** Purpose:      Delete the specifed Outlook email account
**               
** Parameter:    sProfile - Identity value (Profile property of account object)
**               sServiceId - for outlook only, blank for OE (SerciceId property of account object)
**               sAccountId - Account Id (AccountId property of account object)
**
** Return:       1 - Success, 0 - Failure
**               
********************************************************************************/
ssCOutlook.prototype.DeleteAccount = function _ssCOutlook_DeleteAccount(sProfile, sServiceId, sAccountId)
{
  var sRegKey = ssCOutlook.ACCT_MGR_KEY + sAccountId;

  // Get Account Listing Behavior Type
  var iAccountBehaviorType = this.GetAccountTypeBehavior();

  if (iAccountBehaviorType == ssCOutlook.ACCT_TYPE_PIMONLY || 
      iAccountBehaviorType == ssCOutlook.ACCT_TYPE_NOMAILSUPPORT) {
    if (!$ss.agentcore.dal.registry.DeleteRegKey("HKCU", sRegKey)) {
      return 0;
    }
  }
  else {
    var sProfile = this.GetProfile();        
    var sDelAccName = $ss.agentcore.dal.registry.GetRegValue("HKCU", sRegKey, "Account Name");

    var sProfileServiceRegKey = this.sProfileRegKey + sProfile ;
    var sProfileServiceKeys   = $ss.agentcore.dal.registry.EnumRegKey("HKCU", sProfileServiceRegKey,",");

    if (sProfileServiceKeys != null && sProfileServiceKeys != "")
      var aProfileServiceKeys = sProfileServiceKeys.split(",");
    else
      return;

    var hServiceNameIndex = new Object;
    var sAccountName   = "";
    var sAccountRegKey = "";

    for (var i=0; i<aProfileServiceKeys.length; i++){ 
      sAccountRegKey = sProfileServiceRegKey + "\\" + aProfileServiceKeys[i];
      sAccountName = $ss.agentcore.dal.registry.GetRegValue("HKCU", sAccountRegKey, "001e661f"); // POP
      if (sAccountName == sDelAccName) {  
        $ss.agentcore.dal.registry.DeleteRegKey("HKCU", sAccountRegKey)
      }
    }	
    if (!sAccountRegKey("HKCU",regkey,"")) {
      this._logger.info("_ssCOutlook_DeleteAccount - Unable to delete Outlook account");
      return 0;
    }
  }
  this.RemoveAccountFromOrderedList(sProfile, sServiceId, sAccountId);
}

/*******************************************************************************
** Name:         _ssCOutlook_SetAsDefault
**
** Purpose:      Set specifed Outlook email account as default
**               
** Parameter:    sProfile - Not used
**               sServiceId - Not used
**               sAccountId - Account Id (AccountId property of account object)
**
** Return:       1 - Success, 0 - Failure
**               
********************************************************************************/

ssCOutlook.prototype.SetAsDefault = function _ssCOutlook_SetAsDefault(sProfile, sServiceId, sAccountId)
{
  this._logger.info("_ssCOutlook_SetAsDefault");
  var sRegKey = ssCOutlook.ACCT_MGR_KEY.replace(/Accounts\\/,"");
  $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "Default Mail Account", 1, sAccountId);
}

//*******************************************************************************************
// Name:         _ssCOutlook2003_RemoveAccountFromOrderedList
// Purpose:      Remove the specified Outlook account from the ordered list of accounts
// Parameter:    sProfile - the profile to use
//               sAccountId - the account number
//*******************************************************************************************
ssCOutlook.prototype.RemoveAccountFromOrderedList = 
  function _ssCOutlook_RemoveAccountFromOrderedList(sProfile, sServiceId, sAccountId)
{
  var strMailSvcKey, arrAccounts, strAccount;

  // Get the service key
  var sRegKey = ssCOutlook.PROFILE_REG_KEY + sProfile + "\\" + sServiceId + "\\";

  // Normalize the supplied account
  sAccountId = "00000000" + sAccountId;
  sAccountId = sAccountId.substr (sAccountId.length - 8);

  // Remove the specified account
  arrAccounts = $ss.agentcore.dal.registry.GetRegValue("HKCU", sRegKey, ssCOutlook.ACCT_INDEX_VALUE);

  for (var L1 = 0; L1 < arrAccounts.length; L1 += 8) {
    strAccount = arrAccounts.charAt(L1 + 6) + arrAccounts.charAt(L1 + 7) +
    arrAccounts.charAt(L1 + 4) + arrAccounts.charAt(L1 + 5) +
    arrAccounts.charAt(L1 + 2) + arrAccounts.charAt(L1 + 3) +
    arrAccounts.charAt(L1 + 0) + arrAccounts.charAt(L1 + 1);

    if (strAccount == sAccountId) {
      // Remove the account
      arrAccounts = arrAccounts.substr (0,L1) + arrAccounts.substr (L1 + 8);
      // Set the account list
      $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, ssCOutlook.ACCT_INDEX_VALUE, $ss.agentcore.constants.REG_BINARY, "0x" + arrAccounts); 
      break;
    }
  }
}

/*******************************************************************************
**      C L A S S  : ssCOutlook2002 
**      Class for supporting Microsoft Outlook 2002
********************************************************************************/
function ssCOutlook2002(oEmail)
{
  ssCOutlook.call(this, oEmail);             // ssCoutlook2002 inherits from ssCOutlook
  this.sVersion = EM_OLK_VER2002;         // track version

  this._logger = $ss.agentcore.log.GetDefaultLogger("$ss.snapin.email.ssCOutlook2002");
}

ssCOutlook2002.prototype = new ssCOutlook(); // initialize prototype object of derived with base 

/*******************************************************************************
** Name:         _ssCOutlook2002_DoAccountBookKeeping
********************************************************************************/
ssCOutlook2002.prototype.DoAccountBookKeeping = function _ssCOutlook2002_DoAccountBookKeeping(sImailSvcKey, acctNum, normalizedAcctStr)
{
  this._logger.info("_ssCOutlook2002_DoAccountBookKeeping");
  // increment next acct entry
  var  nextAcct = acctNum + 1;
  $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sImailSvcKey, "NextAccountID", 4, "0x" + nextAcct.toString(16));

  // increment LastChangeVer (which is a 64-bit large num - stored in a binary value)
  var  lastVer = $ss.agentcore.dal.registry.GetRegValue("HKCU", sImailSvcKey, "LastChangeVer");
  var  carry = 0;
  var  i = 0;
  do
  {
    // process a byte at a time
    var byteStr = lastVer.charAt(i) + lastVer.charAt(i+1);  // string representation of current byte
    var byteVal = parseInt(byteStr, 16);                    // numerical value of current byte
    byteVal++;
    if (byteVal >= 256)
    {
      byteVal = 0;
      carry = 1;
    }
    else carry = 0;

    byteStr = byteVal.toString(16);
    lastVer = lastVer.substring(0, i) + byteStr + lastVer.substring(i+2, lastVer.length);

    i += 2;
    
  } while (carry > 0 && (i+1) < lastVer.length);

  $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sImailSvcKey, "LastChangeVer", 3, "0x" + lastVer.toString(16));

  // now add account to pop3/smtp array (stored as binary value)
  var  imailAcctArray = $ss.agentcore.dal.registry.GetRegValue("HKCU", sImailSvcKey, "{ED475418-B0D6-11D2-8C3B-00104B2A6676}");
  imailAcctArray += (normalizedAcctStr.charAt(6) + normalizedAcctStr.charAt(7) +
                     normalizedAcctStr.charAt(4) + normalizedAcctStr.charAt(5) +
                     normalizedAcctStr.charAt(2) + normalizedAcctStr.charAt(3) +
                     normalizedAcctStr.charAt(0) + normalizedAcctStr.charAt(1));
  $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sImailSvcKey, "{ED475418-B0D6-11D2-8C3B-00104B2A6676}", 3, "0x" + imailAcctArray);

  // write mandatory values under new account key
  //   clsid = {ED475411-B0D6-11D2-8C3B-00104B2A6676}
  //   Mini UID = <random DWORD value>
  var acctKey = sImailSvcKey + normalizedAcctStr;
  $ss.agentcore.dal.registry.SetRegValueByType("HKCU", acctKey, "clsid", 1, "{ED475411-B0D6-11D2-8C3B-00104B2A6676}");

  var miniUidStr = this.GenUIDStr();
  $ss.agentcore.dal.registry.SetRegValueByType("HKCU", acctKey, "Mini UID", 4, "0x" + miniUidStr);
}

/*******************************************************************************
** Name:         _ssCOutlook2002_GenUIDStr
********************************************************************************/
ssCOutlook2002.prototype.GenUIDStr = function _ssCOutlook2002_GenUIDStr()
{
  // Generate a nibble (4-bit) at a time
  var  uidStr = "";
  for (var i = 0; i < 8; i++) {
    var  nibbleVal = Math.floor(Math.random() * 16);
    uidStr += nibbleVal.toString(16);
  }
  return uidStr;
}

/*******************************************************************************
** Name:         _ssCOutlook2002_CreateAccount
**
** Purpose:      Create an email account using the default parameters for 
**               Microsoft Outlook client 20002
**
** Parameter:    none       uses object of ssCEmail class
**
** Return:       int 0=OK, 1=incorrect_state (run wizard), 2=Other bad error
********************************************************************************/
ssCOutlook2002.prototype.CreateAccount = function _ssCOutlook2002_CreateAccount()
{
  // if no profile specified, try to find default profile
  this.GetProfile(); 
  if(this.sProfile.length == 0) {
      //this is normal if they haven't configured Outlook - 
    this._logger.info("_ssCOutlook2002_CreateAccount", "Failed to obtain a profile");
    return EM_RET_INCORRECT_STATE;
  }
  
  // Generate Account Id
  this.GenerateAccountId();
  if (this.sAccountId.length <= 0) 
    return EM_RET_INCORRECT_STATE;
    
  // Set Account Profile in registry
  this.SetAccountProfile();
  
  // Set Account password
  if(this.oEmail.sPassword.length > 0)
    this.SetAccountPassword();
    
   // Set as default
  if(this.oEmail.sIsDefault === true)
    this.SetAsDefault(this.sProfile, ssCOutlook.POP3_SMTP_SERVICE_SUBKEY, this.sAccountId);

  return 0;
}

/*******************************************************************************
** Name:         _ssCOutlook2002_GetAccounts
********************************************************************************/
ssCOutlook2002.prototype.GetAccounts = function _ssCOutlook2002_GetAccounts(aAccounts)
{
  this._logger.info("_ssCOutlook2002_GetAccounts");
  if (typeof aAccounts == "undefined" || aAccounts == null)
    aAccounts = new Array();
  
  try {
    // Ensure that we have a profile to work with
    var sProfile = this.GetProfile();
    if (sProfile.length == 0) {
        //this is normal if they haven't configured Outlook - 
      this._logger.info("_ssCOutlook2002_GetAccounts", "Failed to obtain a profile");
      return aAccounts;
    }

    var sServiceKey = this.sProfileRegKey + sProfile + ssCOutlook.POP3_SMTP_SERVICE_SUBKEY;
    var aAcctArray  = $ss.agentcore.dal.registry.GetRegValue("HKCU", sServiceKey, ssCOutlook.ACCT_INDEX_VALUE);
    var i = 0;
    while (i + 7 < aAcctArray.length)
    {
      var sAccountId = aAcctArray.charAt(i+6) + aAcctArray.charAt(i+7) +
                       aAcctArray.charAt(i+4) + aAcctArray.charAt(i+5) +
                       aAcctArray.charAt(i+2) + aAcctArray.charAt(i+3) +
                       aAcctArray.charAt(i+0) + aAcctArray.charAt(i+1);
      var oAccount = this.GetAccountInfo(null, sAccountId);
      aAccounts.push(oAccount);
      i += 8;
    }
  } catch (ex)  {
    exception.HandleException(ex,'$ss.snapin.email.utility','ssCOutlook2002.prototype.GetAccounts',arguments);
  }
  return aAccounts;
}

/*******************************************************************************
** Name:         _ssCOutlook2002_TestAccountId
********************************************************************************/
ssCOutlook2002.prototype.TestAccountId = function _ssCOutlook2002_TestAccountId(nextAcctStr, sImailSvcKey) {
  // Normalize string to 8 chars
 
  var acctStrNormalized = $ss.snapin.email.utility.NormalizeString(nextAcctStr, 8);
  // Create key
  var newAcctKey = sImailSvcKey + acctStrNormalized; 
  if ($ss.agentcore.dal.registry.SetRegValueByType("HKCU", newAcctKey, "", 1, "")) {       
    // Successfully created new accout key hence do some book keeping for new account
    this.DoAccountBookKeeping(sImailSvcKey, parseInt(nextAcctStr, 16), acctStrNormalized);
    this.sAccountId = acctStrNormalized;
  }
}

/*******************************************************************************
** Name:         _ssCOutlook2002_GenerateAccountId
********************************************************************************/
ssCOutlook2002.prototype.GenerateAccountId = function _ssCOutlook2002_GenerateAccountId() {
  this._logger.info("_ssCOutlook2002_GenerateAccountId");
  var sImailSvcKey = this.sProfileRegKey + this.sProfile + ssCOutlook.POP3_SMTP_SERVICE_SUBKEY;
  var nextAcctStr = $ss.agentcore.dal.registry.GetRegValue("HKCU", sImailSvcKey, "NextAccountID");
  
  if (nextAcctStr == null || nextAcctStr.length == 0) {
    nextAcctInt = 1;
    for (;;) {
      nextAcctInt += Math.floor(Math.random() * 10);
      if (nextAcctInt > 10000000) {
        this._logger.info("_ssCOutlook2002_GenerateAccountId", "AccountIdInt has exceeded bounds");
        return;
      }
      nextAcctStr = $ss.agentcore.utils.ConvertStringToMinLen(nextAcctInt, "0", 8);
      this.TestAccountId(nextAcctStr, sImailSvcKey);
      var sRegKey = sImailSvcKey + this.sAccountId;
      var existingAccount = $ss.agentcore.dal.registry.GetRegValue("HKCU", sRegKey, "Account Name", 1, this.oEmail.sAccountName);
      if (existingAccount == null || existingAccount.length < 1) return;
    }
  }  
  this.TestAccountId(nextAcctStr, sImailSvcKey);
}

/*******************************************************************************
** Name:         _ssCOutlook2002_SetAccountProfile
********************************************************************************/
ssCOutlook2002.prototype.SetAccountProfile = function _ssCOutlook2002_SetAccountProfile()
{
  try {
    var sImailSvcKey = this.sProfileRegKey + this.sProfile + ssCOutlook.POP3_SMTP_SERVICE_SUBKEY;
    var sRegKey = sImailSvcKey + this.sAccountId;

    $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "Account Name", 1, this.oEmail.sAccountName);
    $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "Display Name", 1, this.oEmail.sDisplayName);
    $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "Email", 1, this.oEmail.sEmailAddress);
    $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "POP3 User", 1, this.oEmail.sUserName);
    $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "POP3 Server", 1, this.oEmail.sPOPServer);
    $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "POP3 Prompt for Password", 4, 1); 
    $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "SMTP Server", 1, this.oEmail.sSMTPServer);
    $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "SMTP Port", 4, parseInt(this.oEmail.sSMTPPort));
    $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "POP3 Port", 4, parseInt(this.oEmail.sPOPPort));
  
    if (this.oEmail.sSMTPSSL == "true")    
      $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "SMTP Use SSL", 4, 1);
    else    
      $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "SMTP Use SSL", 4, 0);
      
    if (this.oEmail.sSMTPAuth == "true")
      $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "SMTP Use Auth", 4, 1);
    else
      $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "SMTP Use Auth", 4, 0);

    $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "SMTP Auth Method", 4, 0);

    if (this.oEmail.sPOPSSL == "true")
      $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "POP3 Use SSL", 4, 1);
    else
      $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "POP3 Use SSL", 4, 0);
  
    if (this.oEmail.sPOPAuth == "true")
      $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "POP3 Use SPA", 4, 1);
    else
      $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "POP3 Use SPA", 4, 0);

  } catch (ex) {
    throw(ex);
  }
}

/*******************************************************************************
** Name:         _ssCOutlook2002_SetAccountPassword
********************************************************************************/
ssCOutlook2002.prototype.SetAccountPassword = function _ssCOutlook2002_SetAccountPassword() {
  this._logger.info("_ssCOutlook2002_SetAccountPassword");
  var oSSMail = $ss.agentcore.utils.activex.CreateObject("SdcUser.SdcMailCtl");
  oSSMail.ResetOL2002Password(this.sProfile, this.sAccountId, this.oEmail.sPassword);
  delete oSSMail;
}


/*******************************************************************************
** Name:         _ssCOutlook_GetAccountType
** TODO: can be deleted now
**
** Purpose:      Returns the account type to determine if its an exchange or 
**               pop account
**
** Parameter:    sAccountId : 
**
** Return:       Account type : Exchange or POP account
********************************************************************************/
ssCOutlook2002.prototype.GetAccountType = function _ssCOutlook2002_GetAccountType(sAccountId) {
  this._logger.info("_ssCOutlook2002_GetAccountType");
  var sAccountType = "";
  try {
    var sRegKey = this.sProfileRegKey + this.sProfile + ssCOutlook.POP3_SMTP_SERVICE_SUBKEY + sAccountId;
    sAccountType = $ss.agentcore.dal.registry.GetRegValue("HKCU", sRegKey, "clsid");
  }
  catch (ex) {
    var sMsg = "Failed in getting account type for Id: " + sAccountId;
    sMsg += " Error: " + ex.message;
    this._exception.HandleException(ex, '$ss.snapin.email.utility', 'ssCOutlook2002.prototype.GetAccountType', arguments);
  }
  return sAccountType;
}

/*******************************************************************************
** Name:         _ssCOutlook2002_GetAccountInfo
**
** Purpose       Prepares account object with all values for current set profile
**               and account id.
********************************************************************************/
ssCOutlook2002.prototype.GetAccountInfo = function _ssCOutlook2002_GetAccountInfo(sServiceId, sAccountId) {
  this._logger.info("_ssCOutlook2002_GetAccountInfo");
  var oAccount = new Object;
  try {
    // Ensure that we have a profile to work with
    var sProfile = this.GetProfile();
    if (sProfile.length == 0) return oAccount;

    var sRegKey = this.sProfileRegKey + sProfile + ssCOutlook.POP3_SMTP_SERVICE_SUBKEY + sAccountId;

    if (sServiceId == null) {
      if ($ss.agentcore.dal.registry.GetRegValue("HKCU", sRegKey, "POP3 Server"))
        sServiceId = ssCOutlook.POP3_SMTP_SERVICE_ID;
      else
        sServiceId = ssCOutlook.EXCH_SERVICE_ID;
    }

    oAccount.MailClient  = EM_MS_OUTLOOK;
    oAccount.Profile     = sProfile;
    oAccount.AccountId   = sAccountId;
    oAccount.ServiceId   = sServiceId;
    oAccount.Type        = $ss.agentcore.dal.registry.GetRegValue("HKCU", sRegKey, "clsid");
    oAccount.Name        = this.GetUniversalReg("HKCU", sRegKey, "Account Name");
    oAccount.DisplayName = this.GetUniversalReg("HKCU", sRegKey, "Display Name");
    oAccount.Email       = this.GetUniversalReg("HKCU", sRegKey, "Email");
    oAccount.ExchangeUser   = "";
    oAccount.ExchangeServer = "";
    oAccount.Pop3Server     = "";
    oAccount.Pop3User       = "";
    oAccount.Pop3Port       = "110";
    oAccount.Pop3SSL        = "false";
    oAccount.Pop3Auth       = "false";
    oAccount.SmtpServer     = "";
    oAccount.SmtpPort       = "25";
    oAccount.SmtpSSL        = "false";
    oAccount.SmtpAuth       = "false";

    if (oAccount.Type == ssCOutlook.ACCT_TYPE_EXCHANGE) {
      var sServiceKey = this.sProfileRegKey + sProfile + "\\" + sServiceId;
      oAccount.ExchangeServer = $ss.agentcore.dal.registry.GetRegValue("HKCU", sServiceKey, "001e6602");
      oAccount.ExchangeUser = this.GetUniversalReg("HKCU", sServiceKey, "001f6607");

      if (oAccount.DisplayName == "")
        oAccount.DisplayName = this.GetUniversalReg("HKCU", sServiceKey, "001f6620");
      //if(oAccount.Name=="")         oAccount.Name = this.GetUniversalReg"HKCU", sServiceKey, "001f3001");    

      if (oAccount.Email == "") {
        try {
          var sExEmails = new String(this.GetUniversalRegWithNewline("HKCU", sServiceKey, "101f6637"));
          var aExEmails = sExEmails.split(/\n/);
          for (var i = 0; i < aExEmails.length; i++) {
            if (aExEmails[i].match(/smtp:/i)) {
              oAccount.Email += aExEmails[i].replace(/smtp:/i, "");
              //don't grab all email accounts.
              break;
            }
          }
        } catch (ex) { }
      }
    }
    else if (oAccount.Type == ssCOutlook.ACCT_TYPE_POP) {
      oAccount.Pop3User   = this.GetUniversalReg("HKCU", sRegKey, "POP3 User");
      oAccount.Pop3Server = this.GetUniversalReg("HKCU", sRegKey, "POP3 Server");
      oAccount.Pop3Port    = this.GetUniversalReg("HKCU", sRegKey, "POP3 Port");
      oAccount.Pop3SSL    = (this.GetUniversalReg("HKCU", sRegKey, "POP3 Use SSL") == "1").toString ();
      oAccount.Pop3Auth   = (this.GetUniversalReg("HKCU", sRegKey, "POP3 Use SPA") == "1").toString ();
      oAccount.SmtpServer = this.GetUniversalReg("HKCU", sRegKey, "SMTP Server");
      oAccount.SmtpPort   = this.GetUniversalReg("HKCU", sRegKey, "SMTP Port");
      oAccount.SmtpSSL    = (this.GetUniversalReg("HKCU", sRegKey, "SMTP Use SSL") == "1").toString ();
      oAccount.SmtpAuth   = (this.GetUniversalReg("HKCU", sRegKey, "SMTP Use Auth") == "1" &&
                             (this.GetUniversalReg("HKCU", sRegKey, "SMTP Auth Method") == "" ||
                              this.GetUniversalReg("HKCU", sRegKey, "SMTP Auth Method") == "0")).toString ();

      if (oAccount.Pop3Port == "")
        oAccount.Pop3Port = "110";
      else
        oAccount.Pop3Port = parseInt(oAccount.Pop3Port, 16).toString();

      if (oAccount.SmtpPort == "")
        oAccount.SmtpPort = "25";
      else
        oAccount.SmtpPort = parseInt(oAccount.SmtpPort, 16).toString();
    }
    oAccount.IsDefault = this.IsDefault(sProfile, ssCOutlook.POP3_SMTP_SERVICE_SUBKEY, sAccountId);
  } catch (ex) {
    this._exception.HandleException(ex, '$ss.snapin.email.utility', 'ssCOutlook2002.prototype.GetAccountInfo', arguments);
  }
  return oAccount;
}

/*******************************************************************************
** Name:         _ssCOutlook2002_IsPasswordSaved
**
** Purpose:      Get the sProfile variable, if its empty then tries to get 
**               default profile or first profile 
**               profiles are used : 2002 onwards  
**               
** Parameter:    none       And no need to have class instantiated with valid
**                          oEmail object
**
** Return:       this.sProfile : string having profile name or empty incase of 
**                          failures
********************************************************************************/
ssCOutlook2002.prototype.IsPasswordSaved = function _ssCOutlook2002_IsPasswordSaved() {
  this._logger.info("_ssCOutlook2002_IsPasswordSaved");
  try {
    if (!this.sAccountId)
      return false;
    var bPasswordSaved = false;
    var mailobj = $ss.agentcore.utils.activex.CreateObject("SdcUser.SdcMailCtl");
    bPasswordSaved = mailobj.IsOL2002PasswordSaved(this.sProfile, this.sAccountId);
    delete mailobj;
  } catch (ex) {
  }
  return bPasswordSaved;
}

/*******************************************************************************
** Name:         _ssCOutlook2002_AuthenticatePassword
**
** Purpose:      Get the sProfile variable, if its empty then tries to get 
**               default profile or first profile 
**               profiles are used : 2002 onwards  
**               
** Parameter:    none       And no need to have class instantiated with valid
**                          oEmail object
**
** Return:       this.sProfile : string having profile name or empty incase of 
**                          failures
********************************************************************************/
ssCOutlook2002.prototype.AuthenticatePassword = function _ssCOutlook2002_AuthenticatePassword() {
  this._logger.info("_ssCOutlook2002_AuthenticatePassword");
  if (!this.sAccountId)
    return false;
  var bAccountAuthenticatedOK = false;
  try {
    var mailobj = $ss.agentcore.utils.activex.CreateObject("SdcUser.SdcMailCtl");
    bAccountAuthenticatedOK = mailobj.AuthenticateOL2002Pop(this.sProfile, this.sAccountId);
    delete mailobj;
  } catch (ex) { }
  return bAccountAuthenticatedOK;
}

/*******************************************************************************
** Name:         _ssCOutlook2002_DeleteAccount
**
** Purpose:      Delete the specifed Outlook 2002, 2003 & mostly 2007 email account
**               
** Parameter:    sProfile - Identity value (Profile property of account object)
**               sAccountId - Account Id (AccountID property of account object)
**
** Return:       1 - Success, 0 - Failure
**               
********************************************************************************/
ssCOutlook2002.prototype.DeleteAccount = function _ssCOutlook2002_DeleteAccount(sProfile, sServiceId, sAccountId) {
  this._logger.info("_ssCOutlook2002_DeleteAccount");

  var sRegKey = ssCOutlook.PROFILE_REG_KEY + sProfile + "\\" + sServiceId + "\\" + sAccountId;

  if (!$ss.agentcore.dal.registry.DeleteRegKey("HKCU", sRegKey)) {
    this._logger.info("_ssCOutlook2002_DeleteAccount - Unable to delete Outlook 2002 account");
    return 0;
  }

  this.RemoveAccountFromOrderedList(sProfile, sServiceId, sAccountId);

  return 1;
}

/*******************************************************************************
** Name:         _ssCOutlook2002_SetAsDefault
**
** Purpose:      Set specifed Outlook (2000/2003/2007) email account as default
**               
** Parameter:    sProfile - Identity value (Profile property of account object)
**               sServiceId - for outlook only, blank for OE (SerciceId property of account object)
**               sAccountId - Account Id (AccountId property of account object)
**
** Return:       1 - Success, 0 - Failure
**               
********************************************************************************/
ssCOutlook2002.prototype.SetAsDefault = function _ssCOutlook2002_SetAsDefault(sProfile, sServiceId, sAccountId) {

  this._logger.info("_ssCOutlook2002_SetAsDefault");
  var strMailSvcKey, arrAccounts, strAccount;
  // Get the service key
  var sRegKey = this.sProfileRegKey + sProfile + "\\" + sServiceId + "\\";

  // Normalize the supplied account
  sAccountId = "00000000" + sAccountId;
  sAccountId = sAccountId.substr(sAccountId.length - 8);

  // Remove the specified account
  arrAccounts = $ss.agentcore.dal.registry.GetRegValue("HKCU", sRegKey, ssCOutlook.ACCT_INDEX_VALUE);

  for (var L1 = 0; L1 < arrAccounts.length; L1 += 8) {
    strAccount = arrAccounts.charAt(L1 + 6) + arrAccounts.charAt(L1 + 7) +
    arrAccounts.charAt(L1 + 4) + arrAccounts.charAt(L1 + 5) +
    arrAccounts.charAt(L1 + 2) + arrAccounts.charAt(L1 + 3) +
    arrAccounts.charAt(L1 + 0) + arrAccounts.charAt(L1 + 1);

    if (strAccount == sAccountId) {
      // pust account id to begining
      arrAccounts = arrAccounts.substr(L1, L1 + 8) + arrAccounts.substr(0, L1) + arrAccounts.substr(L1 + 8);
      // Set the account list
      $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, ssCOutlook.ACCT_INDEX_VALUE, $ss.agentcore.constants.REG_BINARY, "0x" + arrAccounts);
      break;
    }
  }
}

/*******************************************************************************
** Name:         _ssCOutlook2002_IsDefault
**
** Purpose:      Checks if specified Outlook (2000/2003/2007) account is set as default or not 
**               
** Parameter:    sProfile - Identity value (Profile property of account object)
**               sServiceId - for outlook only, blank for OE (SerciceId property of account object)
**               sAccountId - Account Id (AccountId property of account object)
**
** Return:       true - default , false - not default
**               
********************************************************************************/
ssCOutlook2002.prototype.IsDefault = function _ssCOutlook2002_IsDefault(sProfile, sServiceId, sAccountId) {
  this._logger.info("_ssCOutlook2002_SetAsDefault");
  var strMailSvcKey, arrAccounts, strAccount;
  // Get the service key
  var sRegKey = this.sProfileRegKey + sProfile + "\\" + sServiceId + "\\";

  // Normalize the supplied account
  sAccountId = "00000000" + sAccountId;
  sAccountId = sAccountId.substr(sAccountId.length - 8);

  // Remove the specified account
  arrAccounts = $ss.agentcore.dal.registry.GetRegValue("HKCU", sRegKey, ssCOutlook.ACCT_INDEX_VALUE);

  for (var L1 = 0; L1 < arrAccounts.length; L1 += 8) {
    strAccount = arrAccounts.charAt(L1 + 6) + arrAccounts.charAt(L1 + 7) +
    arrAccounts.charAt(L1 + 4) + arrAccounts.charAt(L1 + 5) +
    arrAccounts.charAt(L1 + 2) + arrAccounts.charAt(L1 + 3) +
    arrAccounts.charAt(L1 + 0) + arrAccounts.charAt(L1 + 1);

    // return true if first account matches
    if (strAccount == sAccountId)
      return "true";
    else
      return "false";
  }
}

/*******************************************************************************
**      C L A S S  : ssCOutlook2003 
**      Class for supporting Microsoft Outlook 2003
********************************************************************************/
function ssCOutlook2003(oEmail)
{
  ssCOutlook2002.call(this, oEmail);  // ssCoutlook2003 inherits from ssCOutlook2002
  this.sVersion = EM_OLK_VER2003;

  this._logger = $ss.agentcore.log.GetDefaultLogger("$ss.snapin.email.ssCOutlook2003");
  this.sLogClassName = "ssCOutlook2003";
}
ssCOutlook2003.prototype = new ssCOutlook2002(); // initialize prototype object of derived with base 

/*******************************************************************************
** Name:         _ssCOutlook2003_SetAccountPassword
********************************************************************************/
ssCOutlook2003.prototype.SetAccountPassword = function _ssCOutlook2003_SetAccountPassword() {
  var oSSMail = $ss.agentcore.utils.activex.CreateObject("SdcUser.SdcMailCtl");
  oSSMail.ResetOL2003Password(this.sProfile, this.sAccountId, this.oEmail.sPassword);
  delete oSSMail;
}

/*******************************************************************************
** Name:         _ssCOutlook2003_IsPasswordSaved
**
** Purpose:      Get the sProfile variable, if its empty then tries to get 
**               default profile or first profile 
**               profiles are used : 2002 onwards  
**               
** Parameter:    none       And no need to have class instantiated with valid
**                          oEmail object
**
** Return:       this.sProfile : string having profile name or empty incase of 
**                          failures
********************************************************************************/
ssCOutlook2003.prototype.IsPasswordSaved = function _ssCOutlook2003_IsPasswordSaved() {
  this._logger.info("_" + this.sLogClassName + "_IsPasswordSaved");
  try {
    if (!this.sAccountId)
      return false;
    var bPasswordSaved = false;
    var mailobj = $ss.agentcore.utils.activex.CreateObject("SdcUser.SdcMailCtl");
    bPasswordSaved = mailobj.IsOL2003PasswordSaved(this.sProfile, this.sAccountId);
    delete mailobj;
  } catch (ex) {
    this._exception.HandleException(ex, '$ss.snapin.email.utility', 'ssCOutlook2003.prototype.IsPasswordSavedt', arguments);
  }
  return bPasswordSaved;
}

/*******************************************************************************
** Name:         _ssCOutlook2003_AuthenticatePassword
**
** Purpose:      Get the sProfile variable, if its empty then tries to get 
**               default profile or first profile 
**               profiles are used : 2002 onwards  
**               
** Parameter:    none       And no need to have class instantiated with valid
**                          oEmail object
**
** Return:       this.sProfile : string having profile name or empty incase of 
**                          failures
********************************************************************************/
ssCOutlook2003.prototype.AuthenticatePassword = function _ssCOutlook2003_AuthenticatePassword() {
  this._logger.info("_" + this.sLogClassName + "_AuthenticatePassword");
  if (!this.sAccountId)
    return false;
  var bAccountAuthenticatedOK = false;
  try {
    var mailobj = $ss.agentcore.utils.activex.CreateObject("SdcUser.SdcMailCtl");
    bAccountAuthenticatedOK = mailobj.AuthenticateOL2003Pop(this.sProfile, this.sAccountId);
    delete mailobj;
  } catch (ex) { }
  return bAccountAuthenticatedOK;
}

/*******************************************************************************
**      C L A S S  : ssCOutlook2007
**      Class for supporting Microsoft Outlook 2007
********************************************************************************/
function ssCOutlook2007(oEmail)
{

  ssCOutlook2003.call(this, oEmail);  // ssCoutlook2007 inherits from ssCOutlook2003
  this.sVersion = EM_OLK_VER2007;

  this._logger = $ss.agentcore.log.GetDefaultLogger("$ss.snapin.email.ssCOutlook2007");
  this.sLogClassName = "ssCOutlook2007";
}
ssCOutlook2007.prototype = new ssCOutlook2003(); // initialize prototype object of derived with base


/*******************************************************************************
** Name:         _ssCOutlook2007_SetAccountProfile
********************************************************************************/
ssCOutlook2007.prototype.SetAccountProfile = function _ssCOutlook2007_SetAccountProfile()
{
  try {
    var sImailSvcKey = this.sProfileRegKey + this.sProfile + ssCOutlook.POP3_SMTP_SERVICE_SUBKEY;
    var sRegKey = sImailSvcKey + this.sAccountId;

    $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "Account Name", 1, this.oEmail.sAccountName);
    $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "Display Name", 1, this.oEmail.sDisplayName);
    $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "Email", 1, this.oEmail.sEmailAddress);
    $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "POP3 User", 1, this.oEmail.sUserName);
    $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "POP3 Server", 1, this.oEmail.sPOPServer);
    $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "POP3 Prompt for Password", 4, 1); 
    $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "SMTP Server", 1, this.oEmail.sSMTPServer);
    $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "SMTP Port", 4, parseInt(this.oEmail.sSMTPPort));
    $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "POP3 Port", 4, parseInt(this.oEmail.sPOPPort));
  
    if (this.oEmail.sSMTPSSL == "true")
    {
      $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "SMTP Use SSL", 4, 1);
      $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "SMTP Secure Connection", 4, 1);
    }
    else
    {
      $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "SMTP Use SSL", 4, 0);
      $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "SMTP Secure Connection", 4, 0);
    }
    
    if (this.oEmail.sSMTPAuth == "true")
      $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "SMTP Use Auth", 4, 1);
    else
      $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "SMTP Use Auth", 4, 0);

    $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "SMTP Auth Method", 4, 0);

    if (this.oEmail.sPOPSSL == "true")
      $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "POP3 Use SSL", 4, 1);
    else
      $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "POP3 Use SSL", 4, 0);
  
    if (this.oEmail.sPOPAuth == "true")
      $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "POP3 Use SPA", 4, 1);
    else
      $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "POP3 Use SPA", 4, 0);

  } catch (ex) {
    throw(ex);
  }
}

/*******************************************************************************
** Name:         _ssCOutlook2007_GetAccountInfo
**
** Purpose       Prepares account object with all values for current set profile
**               and account id.
********************************************************************************/
ssCOutlook2007.prototype.GetAccountInfo = function _ssCOutlook2007_GetAccountInfo(sServiceId, sAccountId) {
  this._logger.info("_ssCOutlook2007_GetAccountInfo");
  var oAccount = new Object;
  try {
    // Ensure that we have a profile to work with
    var sProfile = this.GetProfile();
    if (sProfile.length == 0) return oAccount;

    var sRegKey = this.sProfileRegKey + sProfile + ssCOutlook.POP3_SMTP_SERVICE_SUBKEY + sAccountId;

    if (sServiceId == null) {
      if ($ss.agentcore.dal.registry.GetRegValue("HKCU", sRegKey, "POP3 Server"))
        sServiceId = ssCOutlook.POP3_SMTP_SERVICE_ID;
      else
        sServiceId = ssCOutlook.EXCH_SERVICE_ID;
    }

    oAccount.MailClient  = EM_MS_OUTLOOK;
    oAccount.Profile     = sProfile;
    oAccount.AccountId   = sAccountId;
    oAccount.ServiceId   = sServiceId;
    oAccount.Type        = $ss.agentcore.dal.registry.GetRegValue("HKCU", sRegKey, "clsid");
    oAccount.Name        = this.GetUniversalReg("HKCU", sRegKey, "Account Name");
    oAccount.DisplayName = this.GetUniversalReg("HKCU", sRegKey, "Display Name");
    oAccount.Email       = this.GetUniversalReg("HKCU", sRegKey, "Email");
    oAccount.ExchangeUser   = "";
    oAccount.ExchangeServer = "";
    oAccount.Pop3Server     = "";
    oAccount.Pop3User       = "";
    oAccount.Pop3Port       = "110";
    oAccount.Pop3SSL        = "false";
    oAccount.Pop3Auth       = "false";
    oAccount.SmtpServer     = "";
    oAccount.SmtpPort       = "25";
    oAccount.SmtpSSL        = "false";
    oAccount.SmtpAuth       = "false";

    if (oAccount.Type == ssCOutlook.ACCT_TYPE_EXCHANGE) {
      var sServiceKey = this.sProfileRegKey + sProfile + "\\" + sServiceId;
      oAccount.ExchangeServer = $ss.agentcore.dal.registry.GetRegValue("HKCU", sServiceKey, "001e6602");
      oAccount.ExchangeUser = this.GetUniversalReg("HKCU", sServiceKey, "001f6607");

      if (oAccount.DisplayName == "")
        oAccount.DisplayName = this.GetUniversalReg("HKCU", sServiceKey, "001f6620");
      //if(oAccount.Name=="")         oAccount.Name = this.GetUniversalReg"HKCU", sServiceKey, "001f3001");    

      if (oAccount.Email == "") {
        try {
          var sExEmails = new String(this.GetUniversalRegWithNewline("HKCU", sServiceKey, "101f6637"));
          var aExEmails = sExEmails.split(/\n/);
          for (var i = 0; i < aExEmails.length; i++) {
            if (aExEmails[i].match(/smtp:/i)) {
              oAccount.Email += aExEmails[i].replace(/smtp:/i, "");
              //don't grab all email accounts.
              break;
            }
          }
        } catch (ex) { }
      }
    }
    else if (oAccount.Type == ssCOutlook.ACCT_TYPE_POP) {
      oAccount.Pop3User   = this.GetUniversalReg("HKCU", sRegKey, "POP3 User");
      oAccount.Pop3Server = this.GetUniversalReg("HKCU", sRegKey, "POP3 Server");
      oAccount.Pop3Port    = this.GetUniversalReg("HKCU", sRegKey, "POP3 Port");
      oAccount.Pop3SSL    = (this.GetUniversalReg("HKCU", sRegKey, "POP3 Use SSL") == "1").toString ();
      oAccount.Pop3Auth   = (this.GetUniversalReg("HKCU", sRegKey, "POP3 Use SPA") == "1").toString ();
      oAccount.SmtpServer = this.GetUniversalReg("HKCU", sRegKey, "SMTP Server");
      oAccount.SmtpPort   = this.GetUniversalReg("HKCU", sRegKey, "SMTP Port");
      oAccount.SmtpSSL    = ((this.GetUniversalReg("HKCU", sRegKey, "SMTP Secure Connection") == "1")&&
                             (this.GetUniversalReg("HKCU", sRegKey, "SMTP Use SSL") == "1")).toString ();
      oAccount.SmtpAuth   = (this.GetUniversalReg("HKCU", sRegKey, "SMTP Use Auth") == "1" &&
                             (this.GetUniversalReg("HKCU", sRegKey, "SMTP Auth Method") == "" ||
                              this.GetUniversalReg("HKCU", sRegKey, "SMTP Auth Method") == "0")).toString ();

      if (oAccount.Pop3Port == "")
        oAccount.Pop3Port = "110";
      else
        oAccount.Pop3Port = parseInt(oAccount.Pop3Port, 16).toString();

      if (oAccount.SmtpPort == "")
        oAccount.SmtpPort = "25";
      else
        oAccount.SmtpPort = parseInt(oAccount.SmtpPort, 16).toString();
    }
    oAccount.IsDefault = this.IsDefault(sProfile, ssCOutlook.POP3_SMTP_SERVICE_SUBKEY, sAccountId);
  } catch (ex) {
    this._exception.HandleException(ex, '$ss.snapin.email.utility', 'ssCOutlook2007.prototype.GetAccountInfo', arguments);
  }
  return oAccount;
}

/*******************************************************************************
**      C L A S S  : ssCOutlook2010
**      Class for supporting Microsoft Outlook 2010
********************************************************************************/
function ssCOutlook2010(oEmail) {
  ssCOutlook2007.call(this, oEmail);  // ssCoutlook2010 inherits from ssCOutlook2007
  this.sVersion = EM_OLK_VER2010;

  this._logger = $ss.agentcore.log.GetDefaultLogger("$ss.snapin.email.ssCOutlook2010");
  this.sLogClassName = "ssCOutlook2010";
}
ssCOutlook2010.prototype = new ssCOutlook2007(); // initialize prototype object of derived with base 


/*******************************************************************************
**      C L A S S  : ssCOutlook2013
**      Class for supporting Microsoft Outlook 2013
********************************************************************************/
function ssCOutlook2013(oEmail) {

  ssCOutlook2010.call(this, oEmail);  // ssCoutlook2013 inherits from ssCOutlook2010
  this.sVersion = EM_OLK_VER2013;

  this._logger = $ss.agentcore.log.GetDefaultLogger("$ss.snapin.email.ssCOutlook2013");
  this.sLogClassName = "ssCOutlook2013";
  this.sProfileRegKey = ssCOutlook.PROFILE_REG_KEY_WIN8;
  this.sProfileRegKey += $ss.snapin.email.utility.GetOutlookCurVersion();          
  this.sProfileRegKey += ".0\\Outlook\\Profiles\\";
}

ssCOutlook2013.prototype = new ssCOutlook2010(); // initialize prototype object of derived with base 


ssCOutlook2013.prototype.SetAccountPassword = function _ssCOutlook2013_SetAccountPassword() {
  this._logger.info("_" + this.sLogClassName + "_SetAccountPassword");
  var oSSMail = $ss.agentcore.utils.activex.CreateObject("SdcUser.SdcMailCtl");
  oSSMail.ResetOL2013Password(this.sProfile, this.sAccountId, this.oEmail.sPassword, this.sProfileRegKey);
  delete oSSMail;
}

ssCOutlook2013.prototype.IsPasswordSaved = function _ssCOutlook2013_IsPasswordSaved() {
  this._logger.info("_" + this.sLogClassName + "_IsPasswordSaved");
  try {
    if (!this.sAccountId)
      return false;
    var bPasswordSaved = false;
    var mailobj = $ss.agentcore.utils.activex.CreateObject("SdcUser.SdcMailCtl");
    bPasswordSaved = mailobj.IsOL2013PasswordSaved(this.sProfile, this.sAccountId, this.sProfileRegKey);
    delete mailobj;
  } catch (ex) {
    this._exception.HandleException(ex, '$ss.snapin.email.utility', 'ssCOutlook2013.prototype.IsPasswordSavedt', arguments);
  }
  return bPasswordSaved;
}

ssCOutlook2013.prototype.AuthenticatePassword = function _ssCOutlook2013_AuthenticatePassword() {
  this._logger.info("_" + this.sLogClassName + "_AuthenticatePassword");
  if (!this.sAccountId)
    return false;
  var bAccountAuthenticatedOK = false;
  try {
    var mailobj = $ss.agentcore.utils.activex.CreateObject("SdcUser.SdcMailCtl");
    bAccountAuthenticatedOK = mailobj.AuthenticateOL2013Pop(this.sProfile, this.sAccountId, this.sProfileRegKey);
    delete mailobj;
  } catch (ex) { }
  return bAccountAuthenticatedOK;
}

/*******************************************************************************
**      C L A S S  : ssCOExpress 
**      Class for supporting Microsoft Outlook 2002
********************************************************************************/
function ssCOExpress(oEmail)
{
  ssCBaseEmail.call(this, oEmail);      // ssCOExpress inherits from ssCBaseEmail
  
  this.sIdentityRegKey = "";            // tracks reg key for the main identity
                                        // or the chosen one 
  this.sIdentity       = "";            // GUID/name of Identity
  this.bIsMainIdentity = false;         // tracks if chosen identty is main       
  
  this._logger = $ss.agentcore.log.GetDefaultLogger("$ss.snapin.email.ssCOExpress");
}

ssCOExpress.prototype = new ssCBaseEmail(); // initialize prototype object of derived with base 
ssCOExpress.VERSION_KEY = "SOFTWARE\\Microsoft\\Outlook Express\\Version Info";
ssCOExpress.MAIN_IDENTITY_REG_KEY = "SOFTWARE\\Microsoft\\Internet Account Manager\\Accounts\\";


/*******************************************************************************
** Name:         _ssCOExpress_GetIdentity
**
** Purpose:      Checks identities and if there are more than one then pick the 
**               default identity.
**
** Parameter:    none : 
**
** Return:       this.sIdentity, updates with main/default identity if required
********************************************************************************/
ssCOExpress.prototype.GetIdentity = function _ssCOExpress_GetIdentity() {
  if (this.sIdentity.length > 0) return this.sIdentity;

  // Check Identities and if more than one then pick the Main Identity
  var sIdentities = $ss.agentcore.dal.registry.EnumRegKey("HKCU", "Identities", ",");
  var aIdentities = sIdentities.split(",");
  var nIdentities = aIdentities.length;

  switch (nIdentities) {
    case 0:
      // no identities is an error in which case log
      this._logger.info("_ssCOExpress_GetIdentity", "No identity was found on the system");
      return (this.sIdentity = "");

    case 1:
      this.SetIdentity(aIdentities[0]);
      return this.sIdentity;

    default:
      // Default to main identity
      for (var i = 0; i < nIdentities; i++) {
        if (this.IsMainIdentity(aIdentities[i])) {
          this.sIdentity = aIdentities[i];
          this.bIsMainIdentity = true;
          this.sIdentityRegKey = ssCOExpress.MAIN_IDENTITY_REG_KEY;
          break;
        }
      }
      return this.sIdentity;
  }
}

/*******************************************************************************
** Name:         _ssCOExpress_SetIdentity
**
** Purpose:      Sets sIdentity member of the class with the passed value
**               Updates bIsMainidentity member variable as well.
**
** Parameter:    sIdentity : identity to set 
**
** Return:       sets sIdentity
********************************************************************************/
ssCOExpress.prototype.SetIdentity = function _ssCOExpress_SetIdentity(sIdentity) {
  this.sIdentity = sIdentity;
  this.bIsMainIdentity = this.IsMainIdentity(sIdentity);
  if (this.bIsMainIdentity)
    this.sIdentityRegKey = ssCOExpress.MAIN_IDENTITY_REG_KEY;
  else
    this.sIdentityRegKey = "Identities\\" + this.sIdentity + "\\Software\\Microsoft\\Internet Account Manager\\Accounts\\";
}

/*******************************************************************************
** Name:         _ssCOExpress_IsMainIdentity
**
** Purpose:      Checks if the passed identity is Main Identity
**
** Parameter:    sId : Identity to be checked 
**
** Return:       true/false
********************************************************************************/
ssCOExpress.prototype.IsMainIdentity = function _ssCOExpress_IsMainIdentity(sId) {
  var sRegKey = "Identities\\" + sId;
  var sDisplayName = $ss.agentcore.dal.registry.GetRegValue("HKCU", sRegKey, "Username");
  sDisplayName = sDisplayName.toLowerCase();

  return (sDisplayName == "main identity" ||
          sDisplayName == "huvudidentitet");
}

/*******************************************************************************
** Name:         _ssCOExpress_GenerateAccountList
**
** Purpose:      Get array list of accounts
**
** Parameter:    nIgnoreNamed : (optional) default value is false 
**                            :  true value will filter out accounts like BigFoot etc.
**               
** Return:       aRawAccounts : Raw account names
********************************************************************************/
ssCOExpress.prototype.GenerateAccountList = function _ssCOExpress_GenerateAccountList(bIgnoreNamed) {
  var aAccounts = [];
  try {
    var sRawAccounts = $ss.agentcore.dal.registry.EnumRegKey("HKCU", this.sIdentityRegKey, ",");
    if (sRawAccounts.length == 0)            // no existing accounts
      return aAccounts;

    var aRawAccounts = sRawAccounts.split(",");
    if (typeof bIgnoreNamed != "undefined" && bIgnoreNamed) {
      for (var i = 0; i < aRawAccounts.length; i++) {
        if (aRawAccounts[i].length != 8 || aRawAccounts[i].indexOf("0") != 0)
          continue; // Is a "named" account 
        aAccounts.push(aRawAccounts[i]);
      }
    } else {
      aAccounts = aRawAccounts;
    }
  }
  catch (ex) {
    this._exception.HandleException(ex, '$$ss.snapin.email.utility', 'ssCOExpress.prototype.GenerateAccountList', arguments);
  }
  return aAccounts;
}

/*******************************************************************************
** Name:         _ssCOExpress_IsAccountDuplicate
**
** Purpose:      Check for duplicates by seeing if any for the sa,e display
**               name if there is the same email address
**
** Parameter:    none : uses identity reg key, display name and email address
**
** Return:       true/false
********************************************************************************/
ssCOExpress.prototype.IsAccountDuplicate = function _ssCOExpress_IsAccountDuplicate()
{
  var arExistingAccounts = $ss.agentcore.dal.registry.EnumRegKey("HKCU", this.sIdentityRegKey,",");

  if (arExistingAccounts.length == 0 )            // no existing accounts
    return false;
  
  var accounts = arExistingAccounts.split(',');   // go through accounts, 
  var sDisplayName = "";
  var sEmailAddr = "";
  
  for (var ndx=0; ndx<accounts.length; ndx++) 
  { 
    sDisplayName = $ss.agentcore.dal.registry.GetRegValue("HKCU", this.sIdentityRegKey + accounts[ndx], "SMTP Display Name");
    sEmailAddr   = $ss.agentcore.dal.registry.GetRegValue("HKCU", this.sIdentityRegKey + accounts[ndx], "SMTP Email Address")
    if ( sDisplayName == this.oEmail.sDisplayName &&  
         sEmailAddr == this.oEmail.sEmailAddress) 
    {
      return true;    // matched hence duplicate 
    }
  }
  return false; // no match, return string
}

/*******************************************************************************
** Name:         _ssCOExpress_CreateAccount
**
** Purpose:      Create an email account using the default parameters for 
**               Microsoft Outlook Express
**
** Parameter:    none       uses object of ssCEmail class
**
** Return:       int 0=OK, 1=incorrect_state (run wizard), 2=Other bad error
********************************************************************************/
ssCOExpress.prototype.CreateAccount = function _ssCOExpress_CreateAccount() {
  try {
    // Check Identities and if more than one then pick the main Identity
    this.GetIdentity();
    if (this.sIdentity.length <= 0) return EM_RET_INCORRECT_STATE;

    // Generate AccountId
    this.GenerateAccountId();
    if (this.sAccountId.length <= 0) return EM_RET_INCORRECT_STATE;

    // Get Unique Account Name
    this.oEmail.GetUniqueName(this.sIdentityRegKey);

    // If duplicate no need to create another one
    if (this.IsAccountDuplicate()) return EM_RET_OK;

    // Set Account Profile in registry
    this.SetAccountProfile();

    // Set Account password
    if (this.oEmail.sPassword.length > 0)
      this.SetAccountPassword();

    // Make this default mail account
    if (this.oEmail.sIsDefault === true) {
      $ss.agentcore.dal.registry.SetRegValueByType("HKCU", "Software\\Microsoft\\Internet Account Manager", "Default Mail Account", 1, this.sAccountId);
    }

    $ss.agentcore.diagnostics.smartissue.PersistInfo($ss.agentcore.constants.smartissue.EMAIL_CLASS, $ss.agentcore.constants.smartissue.EMAIL_DISPLAYNAME, this.oEmail.sDisplayName);

    return EM_RET_OK;
  } catch (ex) {
    this._exception.HandleException(ex, '$ss.snapin.email.utility', 'ssCOExpress.prototype.CreateAccount', arguments);
    return EM_RET_OTHER_ERROR;
  }

}

/*******************************************************************************
** Name:         _ssCOExpress_GetAccounts
**
** Purpose:      Get all accounts associated with the set Identity for 
**               Microsoft Outlook Express
**
** Parameter:    aAccounts  : array of account objects to use
**
** Return:       aAccounts  : updated array is returned
********************************************************************************/
ssCOExpress.prototype.GetAccounts = function _ssCOExpress_GetAccounts(aAccounts) {
  if (typeof (aAccounts) == "undefined" || aAccounts == null)
    var aAccounts = new Array();

  // Make sure that we have an identity to workl with
  var identity = this.GetIdentity();

  var aAccountKeys = this.GenerateAccountList(true);
  var sAccountId = "";

  for (var i = 0; i < aAccountKeys.length; i++) {
    var sRegKey = this.sIdentityRegKey + aAccountKeys[i];
    var sAccountName = $ss.agentcore.dal.registry.GetRegValue("HKCU", sRegKey, "Account Name");
    if (sAccountName != null && sAccountName != "") {
      var oAccount = this.GetAccountInfo(aAccountKeys[i]);
      aAccounts.push(oAccount);
    }
  }

  return aAccounts;
}

/*******************************************************************************
** Name:         _ssCOExpress_GetVersion
**
** Purpose:      Gets the version of outlook express if not already set
**
** Parameter:    none
**
** Return:       sets this.sVersion
********************************************************************************/
ssCOExpress.prototype.GetVersion = function _ssCOExpress_GetVersion()
{
  if(this.sVersion == "")
    this.sVersion = $ss.agentcore.dal.registry.GetRegValue("HKLM", ssCOExpress.VERSION_KEY, "Current");
    
  return this.sVersion;  
}

/*******************************************************************************
** Name:         _ssCOExpress_GetAccountInfo
**
** Purpose       Prepares account object with all values for current set identity
**               and passed account id.
**
** Parameter:    sAccountId : Account Id to be used
**
** Return:       oAccount : Account Object
********************************************************************************/
ssCOExpress.prototype.GetAccountInfo = function _ssCOExpress_GetAccountInfo(sAccountId) {
  this._logger.info("_ssCOExpress_GetAccountInfo");
  var oAccount = new Object;
  
  try  
  {
    var sRegKey          = this.sIdentityRegKey + sAccountId;

    oAccount.MailClient  = EM_OUTLOOK_EXPRESS;
    oAccount.Profile     = this.sIdentity;
    oAccount.ServiceId   = "";
    oAccount.AccountId   = sAccountId;
    oAccount.Type        = ssCOutlook.ACCT_TYPE_POP;
    oAccount.Name        = $ss.agentcore.dal.registry.GetRegValue("HKCU", sRegKey, "Account Name");
    oAccount.DisplayName = $ss.agentcore.dal.registry.GetRegValue("HKCU", sRegKey, "SMTP Display Name");
    oAccount.Email       = $ss.agentcore.dal.registry.GetRegValue("HKCU", sRegKey, "SMTP Email Address");
    oAccount.Pop3User    = $ss.agentcore.dal.registry.GetRegValue("HKCU", sRegKey, "POP3 User Name");
    oAccount.Pop3Server  = $ss.agentcore.dal.registry.GetRegValue("HKCU", sRegKey, "POP3 Server");
    oAccount.Pop3SSL     = ($ss.agentcore.dal.registry.GetRegValue("HKCU", sRegKey, "POP3 Secure Connection") == "1").toString ();
    oAccount.Pop3Auth    = ($ss.agentcore.dal.registry.GetRegValue("HKCU", sRegKey, "POP3 Use Sicily") == "1").toString ();
    oAccount.SmtpServer  = $ss.agentcore.dal.registry.GetRegValue("HKCU", sRegKey, "SMTP Server");
    oAccount.SmtpSSL     = ($ss.agentcore.dal.registry.GetRegValue("HKCU", sRegKey, "SMTP Secure Connection") == "1").toString ();
    oAccount.SmtpAuth    = ($ss.agentcore.dal.registry.GetRegValue("HKCU", sRegKey, "SMTP Use Sicily") == "2").toString ();

    oAccount.Pop3Port    = $ss.agentcore.dal.registry.GetRegValue("HKCU", sRegKey, "POP3 Port");
    if (oAccount.Pop3Port == "")
      oAccount.Pop3Port = "110";
    else
      oAccount.Pop3Port = parseInt(oAccount.Pop3Port, 16).toString();

    oAccount.SmtpPort    = $ss.agentcore.dal.registry.GetRegValue("HKCU", sRegKey, "SMTP Port");
    if (oAccount.SmtpPort == "")
      oAccount.SmtpPort = "25";
    else
      oAccount.SmtpPort = parseInt(oAccount.SmtpPort, 16).toString();

    //check if the account is default
    var sDefaultAcc = $ss.agentcore.dal.registry.GetRegValue("HKCU", this.sIdentityRegKey.replace(/Accounts\\/, ""), "Default Mail Account");
    if (sDefaultAcc == sAccountId)
      oAccount.IsDefault = "true";
    else
      oAccount.IsDefault = "false";      
  }  
  catch (ex) 
  {
     this._exception.HandleException(ex,'$ss.snapin.email.utility','ssCOExpress.prototype.GetAccountInfo',arguments);
  }
  return oAccount;
}

/*******************************************************************************
** Name:         _ssCOExpress_GenerateAccountId
**
** Purpose:      Checks if the passed identity is Main Identity
**
** Parameter:    sId : Identity to be checked 
**
** Return:       true/false
********************************************************************************/
ssCOExpress.prototype.GenerateAccountId = function _ssCOExpress_GenerateAccountId()
{
  var acctNum = 1;
  while(acctNum < 100) {
    // Convert number to 8 char hex string (example: "00000001")
    var strAcctNum = acctNum.toString(16);
    var len = 8 - strAcctNum.length;

    for(var i = 0; i < len; i++) {
      strAcctNum = "0" + strAcctNum;
    }
    
    // Check registry if accout number already exists
    var sRegKey   = this.sIdentityRegKey + strAcctNum;
    var acctName = $ss.agentcore.dal.registry.GetRegValue("HKCU", sRegKey, "Account Name");
    if (acctName == null || acctName == "") 
      return (this.sAccountId = strAcctNum);

    // Account exists, increment account number
    acctNum++;
  }
  return (this.sAccountId = "");
}

/*******************************************************************************
** Name:         _ssCOExpress_SetAccountProfile
********************************************************************************/
ssCOExpress.prototype.SetAccountProfile = function _ssCOExpress_SetAccountProfile()
{
  try {
    var sRegKey = this.sIdentityRegKey + this.sAccountId;

    $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "Account Name", 1, this.oEmail.sAccountName);
    $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "Connection Type", 4, "3");
    $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "POP3 Prompt for Password", 4, "0");
    $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "POP3 Server", 1, this.oEmail.sPOPServer);
    $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "POP3 Port", 4, parseInt(this.oEmail.sPOPPort));
    $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "POP3 User Name", 1, this.oEmail.sUserName);
    $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "SMTP Display Name", 1, this.oEmail.sDisplayName);
    $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "SMTP Email Address", 1, this.oEmail.sEmailAddress);
    $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "SMTP Server", 1, this.oEmail.sSMTPServer);
    $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "SMTP Port", 4, parseInt(this.oEmail.sSMTPPort));
  
    if (this.oEmail.sSMTPSSL == "true")
       $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "SMTP Secure Connection", 4, 1);
    else
       $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "SMTP Secure Connection", 4, 0);
  
    if (this.oEmail.sSMTPAuth == "true")
       $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "SMTP Use Sicily", 4, 2);
    else
       $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "SMTP Use Sicily", 4, 0);

    if (this.oEmail.sPOPSSL == "true")
       $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "POP3 Secure Connection", 4, 1);
    else
       $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "POP3 Secure Connection", 4, 0);
  
    if (this.oEmail.sPOPAuth == "true")
       $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "POP3 Use Sicily", 4, 1);
    else
       $ss.agentcore.dal.registry.SetRegValueByType("HKCU", sRegKey, "POP3 Use Sicily", 4, 0);

  } catch (ex) {
    throw(ex);
  }
}

/*******************************************************************************
** Name:         _ssCOExpress_SetAccountPassword
********************************************************************************/
ssCOExpress.prototype.SetAccountPassword = function _ssCOExpress_SetAccountPassword() {
  var oSSMail = $ss.agentcore.utils.activex.CreateObject("SdcUser.SdcMailCtl");
  if (oSSMail) {
    oSSMail.ResetOEPassword(this.sAccountId, this.oEmail.sPassword);
    delete oSSMail;
  }
}

/*******************************************************************************
** Name:         _ssCOExpress_AuthenticatePassword
**
** Purpose:      Get the sProfile variable, if its empty then tries to get 
**               default profile or first profile 
**               profiles are used : 2002 onwards  
**               
** Parameter:    none       And no need to have class instantiated with valid
**                          oEmail object
**
** Return:       this.sProfile : string having profile name or empty incase of 
**                          failures
********************************************************************************/
ssCOExpress.prototype.AuthenticatePassword = function _ssCOExpress_AuthenticatePassword() {
  this._logger.info("_ssCOExpress_AuthenticatePassword");
  var bAccountAuthenticatedOK = false;
  if (!this.sAccountId) return false;
  try {
    var mailobj = $ss.agentcore.utils.activex.CreateObject("SdcUser.SdcMailCtl");
    bAccountAuthenticatedOK = mailobj.AuthenticateOEPop(this.sAccountId);
    delete mailobj;
  } catch (ex) { }
  return bAccountAuthenticatedOK;
}

/*******************************************************************************
** Name:         _ssCOExpress_IsPasswordSaved
**
** Purpose:      Get the sProfile variable, if its empty then tries to get 
**               default profile or first profile 
**               profiles are used : 2002 onwards  
**               
** Parameter:    none       And no need to have class instantiated with valid
**                          oEmail object
**
** Return:       this.sProfile : string having profile name or empty incase of 
**                          failures
********************************************************************************/
ssCOExpress.prototype.IsPasswordSaved = function _ssCOExpress_IsPasswordSaved()
{
  this._logger.info("_ssCOExpress_IsPasswordSaved");
  var bAccountAuthenticatedOK = false;
  if (! this.sAccountId) return false;
  try {
    var mailobj = $ss.agentcore.utils.activex.CreateObject("SdcUser.SdcMailCtl");
    bAccountAuthenticatedOK = mailobj.IsOEPasswordSaved(this.sAccountId);
    delete mailobj;
  } catch(ex) {}
  return bAccountAuthenticatedOK;
}

/*******************************************************************************
** Name:         _ssCOExpress_DeleteAccount
**
** Purpose:      Delete the specifed Outlook Express email account
**               
** Parameter:    sProfile - Identity value (Profile property of account object)
**               sAccountId - Account Id (AccountID property of account object)
**
** Return:       1 - Success, 0 - Failure
**               
********************************************************************************/
ssCOExpress.prototype.DeleteAccount = function _ssCOExpress_DeleteAccount(sProfile, sServiceId, sAccountId)
{
  this._logger.info("_ssCOExpress_DeleteAccount");

  var sRegKey = "";
  if (sProfile == this.GetIdentity())
    sRegKey = "Software\\Microsoft\\Internet Account Manager\\Accounts\\" + sAccountId;
  else
    sRegKey = "Identities\\" + sProfile + "\\Software\\Microsoft\\Internet Account Manager\\Accounts\\" + sAccountId;

  if (!$ss.agentcore.dal.registry.DeleteRegKey("HKCU", sRegKey)) {
    this._logger.info("_ssCOExpress_DeleteAccount - Unable to delete OE account");
    return 0;
  }

  return 1;
}

/*******************************************************************************
** Name:         _ssCOExpress_SetAsDefault
**
** Purpose:      Set specifed Outlook Express email account as default
**               
** Parameter:    sProfile - Not used
**               sServiceId - Not used
**               sAccountId - Account Id (AccountId property of account object)
**
** Return:       1 - Success, 0 - Failure
**               
********************************************************************************/
ssCOExpress.prototype.SetAsDefault = function _ssCOExpress_SetAsDefault(sProfile, sServiceId, sAccountId)
{
  this._logger.info("_ssCOExpress_SetAsDefault");
  $ss.agentcore.dal.registry.SetRegValueByType("HKCU", "Software\\Microsoft\\Internet Account Manager", "Default Mail Account", 1, sAccountId);
  return 1;
}

/*******************************************************************************
**      C L A S S  : ssCWindowsMail 
**      Class for supporting Microsoft Windows Mail for Vista
********************************************************************************/
function ssCWindowsMail(oEmail)
{
  ssCBaseEmail.call(this, oEmail);      // ssCWindowsMail inherits from ssCBaseEmail
  this.sMailFolder = "";                // tracks the mail folder on Vista for current user
  this.sLocalFolder = "";               // tracks the local folder on Vista for current user 
  this.sMacros   = "%ACCOUNT_NAME%|%DISPLAY_NAME%|%EMAIL_ADDRESS%|" +
                   "%USERNAME%|%POP_PASSWORD%|" +
                   "%POP_SERVER%|%POP_PORT%|%POP_Sicily%|%POP_SSL%|" +
                   "%SMTP_SERVER%|%SMTP_PORT%|%SMTP_Sicily%|%SMTP_SSL%|" +
                   "%POP_SSL%|%SMTP_Sicily%";
									 
  this._logger = $ss.agentcore.log.GetDefaultLogger("$ss.snapin.email.ssCWindowsMail");   
}

ssCWindowsMail.prototype = new ssCBaseEmail(); // initialize prototype object of derived with base 

/*******************************************************************************
** Name:         _ssCWindowsMail_GetMailFolder
********************************************************************************/
ssCWindowsMail.prototype.GetMailFolder = function _ssCWindowsMail_GetMailFolder()
{
  if(this.sMailFolder.length > 0) return this.sMailFolder;
  try
  {
    var sFolder = $ss.agentcore.dal.config.ExpandSysMacro("%SdcProfile%");
    if(sFolder.length <= 0)
      return (this.sMailFolder = "");
    //sMailFolder for Windows Mail is C:\Users\foo\AppData\Local\Microsoft\Windows Mail
    // Look up for last occurance of supportsoft and replace it
    sFolder = sFolder.replace(/\//g, "\\");
    this.sMailFolder = sFolder.replace(/(.*)\\SupportSoft/i, "$1\\Microsoft\\Windows Mail");
    this.SetMailFolder(this.sMailFolder);
  }
  catch(ex)
  {
    this._exception.HandleException(ex,'$ss.snapin.email.utility','ssCWindowsMail.prototype.GetMailFolder',arguments);
  }  
  return this.sMailFolder;
}  
  
/*******************************************************************************
** Name:         _ssCWindowsMail_SetMailFolder
********************************************************************************/
ssCWindowsMail.prototype.SetMailFolder = function _ssCWindowsMail_SetMailFolder(sFolder)
{
  this.sMailFolder = sFolder;
  // Add trailing backslash if not present
  if(this.sMailFolder.charAt(this.sMailFolder.length -1) != "\\")
    this.sMailFolder += "\\";

  // Update local folders as well
  this.sLocalFolder = this.sMailFolder + "Local Folders (1)\\";
  if (!$ss.agentcore.dal.file.FolderExists(this.sLocalFolder))
    this.sLocalFolder = this.sMailFolder + "Local Folders\\";
}

/*******************************************************************************
** Name:         _ssCWindowsMail_MacroReplace
********************************************************************************/
ssCWindowsMail.prototype.MacroReplace = function _ssCWindowsMail_MacroReplace(sText) {
  var sRet = sText;
  try {
    var sRegExp = new RegExp(this.sMacros);
    sRegExp.global = true;
    var arMatched = sText.match(sRegExp);
    for (var i = 0; i < arMatched.length; i++) {
      switch (arMatched[i]) {
        case "%ACCOUNT_NAME%":
          sRet = sRet.replace(/%ACCOUNT_NAME%/g, this.oEmail.sAccountName);
          break;
        case "%POP_SERVER%":
          sRet = sRet.replace(/%POP_SERVER%/g, this.oEmail.sPOPServer);
          break;
        case "%USERNAME%":
          sRet = sRet.replace(/%USERNAME%/g, this.oEmail.sUserName);
          break;
        case "%POP_PASSWORD%":
          //    password is now set by a MS API.  Do not try to set here.
          if ((!this.oEmail.sNewaAccount) && (100 < this.oEmail.sPassword.length)) {
            sRet = sRet.replace(/%POP_PASSWORD%/g, this.oEmail.sPassword);
            break;
          }
        case "%POP_PORT%":
          sRet = sRet.replace(/%POP_PORT%/g, this.oEmail.sPOPPort);
          break;
        case "%SMTP_SERVER%":
          sRet = sRet.replace(/%SMTP_SERVER%/g, this.oEmail.sSMTPServer);
          break;
        case "%SMTP_PORT%":
          sRet = sRet.replace(/%SMTP_PORT%/g, this.oEmail.sSMTPPort);
          break;
        case "%DISPLAY_NAME%":
          sRet = sRet.replace(/%DISPLAY_NAME%/g, this.oEmail.sDisplayName);
          break;
        case "%EMAIL_ADDRESS%":
          sRet = sRet.replace(/%EMAIL_ADDRESS%/g, this.oEmail.sEmailAddress);
          break;
        case "%POP_Sicily%":
          var temp = 0;
          if ("true" === this.oEmail.sPOPAuth) {
            temp = 1;
          }
          sRet = sRet.replace(/%POP_Sicily%/g, $ss.snapin.email.utility.NormalizeString(temp.toString(), 8));
          break;
        case "%SMTP_Sicily%":
          var temp = 0;
          if ("true" === this.oEmail.sSMTPAuth) {
            temp = 2;
          }
          sRet = sRet.replace(/%SMTP_Sicily%/g, $ss.snapin.email.utility.NormalizeString(temp.toString(), 8));
          break;
        case "%POP_SSL%":
          var temp = 0;
          if ("true" === this.oEmail.sPOPSSL) {
            temp = 1;
          }
          sRet = sRet.replace(/%POP_SSL%/g, $ss.snapin.email.utility.NormalizeString(temp.toString(), 8));
          break;
        case "%SMTP_SSL%":
          var temp = 0;
          if ("true" === this.oEmail.sSMTPSSL) {
            temp = 1;
          }
          sRet = sRet.replace(/%SMTP_SSL%/g, $ss.snapin.email.utility.NormalizeString(temp.toString(), 8));
          break;
        default:
          this._logger.info("_ssCWindowsMail_MacroReplace", arMatched[i] + ": unknown macro");
          break;
      }
    }
  } catch (ex) {
    this._exception.HandleException(ex, '$ss.snapin.email.utility', 'ssCWindowsMail.prototype.MacroReplace', arguments);
  }
  return sRet;
}

/*******************************************************************************
** Name:         _ssCWindowsMail_CreateAccount
**
** Purpose:      Create an email account using the default parameters for 
**               Microsoft Windows Mail Vista Client
**
** Parameter:    none       uses oEmail object of class
**
** Return:       int 0=OK, 1=incorrect_state (run wizard), 2=Other bad error
********************************************************************************/
ssCWindowsMail.prototype.CreateAccount = function _ssCWindowsMail_CreateAccount() {
  try {
    // Make sure that we have a mail folder set
    var sFolder = this.GetMailFolder();
    if (sFolder.length <= 0) {
      return EM_RET_INCORRECT_STATE;
    }

    // Generate AccountId
    this.GenerateAccountId();
    if (this.sAccountId.length <= 0) return EM_RET_INCORRECT_STATE;

    // Set Account Profile 
    if (this.SetAccountProfile()) {
      if (this.SetAccountPassword()) {

        if (this.oEmail.sIsDefault === true) {
          this.SetAsDefault("", "", this.sAccountId);
        }

        return EM_RET_OK;

      }
      else {
        return EM_RET_OTHER_ERROR;
      }
    }
    else {
      return EM_RET_OTHER_ERROR;
    }
  }
  catch (ex) {
    this._exception.HandleException(ex, '$$ss.snapin.email.utility', 'ssCWindowsMail.prototype.CreateAccount', arguments);
  }
  return EM_RET_OTHER_ERROR;
}

/*******************************************************************************
** Name:         _ssCWindowsMail_GetAccounts
**
** Purpose:      Get all accounts within the user's mail folder for
**               Microsoft Windows Mail 
**
** Parameter:    aAccounts  : array of account objects to use
**
** Return:       aAccounts  : updated array is returned
********************************************************************************/
ssCWindowsMail.prototype.GetAccounts = function _ssCWindowsMail_GetAccounts(aAccounts) {
  if (typeof (aAccounts) == "undefined" || aAccounts == null)
    var aAccounts = new Array();

  try {
    // Make sure that we have a mail folder set
    var sFolder = this.GetMailFolder();
    if (sFolder.length <= 0) return aAccounts;

    // From the folder enumerate files of type account{*}.oeaccount
    var aFiles = $ss.agentcore.dal.file.GetFilesList(this.sLocalFolder, /account\{.*\}\.oeaccount/i);
    for (var i = 0; i < aFiles.length; i++) {
      var oAccount = this.GetAccountInfo(aFiles[i], this.sLocalFolder);
      oAccount.sNewAccount = false;
      aAccounts.push(oAccount);
    }
    var aFiles = $ss.agentcore.dal.file.GetFilesList(this.sMailFolder, /account\{.*\}\.oeaccount/i);
    for (var i = 0; i < aFiles.length; i++) {
      var oAccount = this.GetAccountInfo(aFiles[i], this.sMailFolder);
      // Filter LDAP, NNTP accounts they would be found only at the mail folder level
      if (oAccount) {
        oAccount.sNewAccount = true;
        aAccounts.push(oAccount);
      }
    }
  } catch (ex) {
    this._exception.HandleException(ex, '$ss.snapin.email.utility', 'ssCWindowsMail.prototype.GetAccounts', arguments);
  }
  return aAccounts;
}

/*******************************************************************************
** Name:         _ssCWindowsMail_GetVersion
********************************************************************************/
ssCWindowsMail.prototype.GetVersion = function _ssCWindowsMail_GetVersion() {
  if (this.sVersion.length > 0)
    return this.sVersion;

  var sVersion = $ss.agentcore.dal.registry.GetRegValue("HKCU", "Software\\Microsoft\\Windows Mail", "VerStamp");
  if (sVersion.indexOf(".") == -1)
    sVersion += ".0";

  return (this.sVersion = sVersion);
}

/*******************************************************************************
** Name:         _ssCWindowsMail_GenerateAccountId
********************************************************************************/
ssCWindowsMail.prototype.GenerateAccountId = function _ssCWindowsMail_GenerateAccountId() {
  if (this.sAccountId.length > 0 || this.sMailFolder == "") {
    this._logger.info("_ssCWindowsMail_GenerateAccountId", "AccountId already generated or cannot be generated")
    return;
  }

  var bExisting = false; // track uniqueness of the account id
  do {
    this.sAccountId = "account" + $ss.agentcore.utils.GenGuid(true) + ".oeaccount";
    // Check mail folder
    bExisting = $ss.agentcore.dal.file.FileExists(this.sMailFolder + this.sAccountId);
    if (!bExisting) {
      // Check Local Folder as well
      bExisting = $ss.agentcore.dal.file.FileExists(this.sLocalFolder + this.sAccountId);
    }
  } while (bExisting);
}


/*******************************************************************************
** Name:         _ssCWindowsMail_GetAccountInfo
**
** Purpose       Prepares account object with all values for current passed 
**               account id.
**
** Parameter:    sFileName : Account File Name that needs to be looked up
**
** Return:       oAccount : Account Object
********************************************************************************/
ssCWindowsMail.prototype.GetAccountInfo = function _ssCWindowsMail_GetAccountInfo(sFileName, sPath) {
  this._logger.info("_ssCWindowsMail_GetAccountInfo");
  var oAccount = new Object;
  try {
    var oDOM = $ss.agentcore.dal.xml.LoadXML(sPath + sFileName);
    if (oDOM) {
      oAccount.MailClient = EM_WINDOWS_MAIL;
      oAccount.Profile = ""; //TBD:
      oAccount.ServiceId = ""; //TBD:
      oAccount.AccountId = sFileName.replace(/account(\{.*\})\.oeaccount/i, "$1");


      oAccount.Type = $ss.agentcore.dal.xml.GetNodeText(oDOM.selectSingleNode("//Connection_Type"));
      oAccount.Name = $ss.agentcore.dal.xml.GetNodeText(oDOM.selectSingleNode("//Account_Name"));
      oAccount.DisplayName = $ss.agentcore.dal.xml.GetNodeText(oDOM.selectSingleNode("//SMTP_Display_Name"));
      oAccount.Email = $ss.agentcore.dal.xml.GetNodeText(oDOM.selectSingleNode("//SMTP_Email_Address"));
      oAccount.Pop3User = $ss.agentcore.dal.xml.GetNodeText(oDOM.selectSingleNode("//POP3_User_Name"));
      oAccount.Pop3Server = $ss.agentcore.dal.xml.GetNodeText(oDOM.selectSingleNode("//POP3_Server"));
      oAccount.SmtpServer = $ss.agentcore.dal.xml.GetNodeText(oDOM.selectSingleNode("//SMTP_Server"));

      oAccount.Password = $ss.agentcore.dal.xml.GetNodeText(oDOM.selectSingleNode("//POP3_Password2"));
      oAccount.Pop3Port = $ss.agentcore.dal.xml.GetNodeText(oDOM.selectSingleNode("//POP3_Port"));
      oAccount.Pop3SSL = ($ss.agentcore.dal.xml.GetNodeText(oDOM.selectSingleNode("//POP3_Secure_Connection")) == "00000001").toString();
      oAccount.Pop3Auth = ($ss.agentcore.dal.xml.GetNodeText(oDOM.selectSingleNode("//POP3_Use_Sicily")) == "00000001").toString();
      oAccount.SmtpPort = $ss.agentcore.dal.xml.GetNodeText(oDOM.selectSingleNode("//SMTP_Port"));
      oAccount.SmtpSSL = ($ss.agentcore.dal.xml.GetNodeText(oDOM.selectSingleNode("//SMTP_Secure_Connection")) == "00000001").toString();
      oAccount.SmtpAuth = ($ss.agentcore.dal.xml.GetNodeText(oDOM.selectSingleNode("//SMTP_Use_Sicily")) == "00000002").toString();

      if (oAccount.Pop3Port == "")
        oAccount.Pop3Port = "110";
      else
        oAccount.Pop3Port = parseInt(oAccount.Pop3Port, 16).toString();

      if (oAccount.SmtpPort == "")
        oAccount.SmtpPort = "25";
      else
        oAccount.SmtpPort = parseInt(oAccount.SmtpPort, 16).toString();

      if ("account" + oAccount.AccountId + ".oeaccount" == $ss.agentcore.dal.registry.GetRegValue("HKCU", "Software\\Microsoft\\IAM", "Default Mail Account"))
        oAccount.IsDefault = "true";
      else
        oAccount.IsDefault = "false";
    }
  } catch (ex) {
    this._exception.HandleException(ex, '$ss.snapin.email.utility', 'ssCWindowsMail.prototype.GetAccountInfo', arguments);
  }
  return ((oAccount.Email && oAccount.DisplayName) ? oAccount : null);
}

/*******************************************************************************
** Name:         _ssCWindowsMail_SetAccountProfile
********************************************************************************/
ssCWindowsMail.prototype.SetAccountProfile = function _ssCWindowsMail_SetAccountProfile(existingAccountPath, accountExtension, accountType)
{
  var bRet = false;
  try {
    //var fnTemplate = ss_cfg_GetValue("email", "email_template", "");
		var fnTemplate = $ss.getSnapinAbsolutePath("snapin_email") + "\\resource\\windows_mail.xml";
    if(fnTemplate.length == 0) return bRet;
    
    var oDOM = $ss.agentcore.dal.xml.LoadXML(fnTemplate);
    if(oDOM) {
      var oNodes = $ss.agentcore.dal.xml.GetNodes("/MessageAccount/*", "", oDOM.documentElement);
      if(oNodes == null) return bRet;
      
      // Normalize email object before looping nodes
      var nTmp = parseInt(this.oEmail.sPOPPort);
      this.oEmail.sPOPPort = $ss.snapin.email.utility.NormalizeString(nTmp.toString(16), 8);
      nTmp = parseInt(this.oEmail.sSMTPPort);
      this.oEmail.sSMTPPort = $ss.snapin.email.utility.NormalizeString(nTmp.toString(16), 8);
      
      for (var i=0; i<oNodes.length; i++) {
        var sTxt = $ss.agentcore.dal.xml.GetNodeText(oNodes[i]);
        if(sTxt.match(this.sMacros) != null) 
          oNodes[i].text = this.MacroReplace(sTxt);
      } 
      if (!$ss.agentcore.dal.file.FolderExists(this.sMailFolder)) {
				$ss.agentcore.dal.file.CreateDir(this.sMailFolder);
			}
      if (!$ss.agentcore.dal.file.FolderExists(this.sLocalFolder)) {
				  $ss.agentcore.dal.file.CreateDir(this.sLocalFolder);
		  }
      // write oDOM to file
      if (accountType) { //accountType is true if it is a new account
        var sFileName = this.sLocalFolder + "account" + this.sAccountId + accountExtension;
      } else {
        if (existingAccountPath) {
         //trouble shooting windows email was creating a new account in local folder , rather than in local folder (1)
          var sFileName = this.sLocalFolder + "account" + this.sAccountId + accountExtension;
        } else {
          var sFileName = this.sLocalFolder + this.sAccountId;
        }
      }
      try {
      
        var oFile = $ss.agentcore.dal.file.CreateTextFile(sFileName, true, true);
        $ss.agentcore.dal.file.WriteFile(sFileName, oDOM.xml, $ss.agentcore.constants.FILE_MODE.WRITE);
        //oFile.Close();
        bRet = true; 
      } catch(ex) {
        var sErr = "Error saving file " + sFileName + "! " + ex.message;
        this._exception.HandleException(ex,'$ss.snapin.email.utility','ssCWindowsMail.prototype.SetAccountProfile',arguments);
      }
    } else {
      var sErr = "Error loading DOM, " + fnTemplate;
      this._logger.info("_ssCWindowsMail_SetAccountProfile", sErr);   
    }  
  }  catch (ex) {
    this._exception.HandleException(ex,'$ss.snapin.email.utility','ssCWindowsMail.prototype.SetAccountProfile',arguments);
  }
  return bRet;
}

/*******************************************************************************
** Name:         _ssCWindowsMail_SetAccountPassword
********************************************************************************/
ssCWindowsMail.prototype.SetAccountPassword = function _ssCWindowsMail_SetAccountPassword() {
  var bRet = false;
  try {
    // Guard against empty password
    if (typeof this.oEmail.sPassword !== "undefined") {
      if (this.oEmail.sPassword.length > 0) {
        var oSSMail = $ss.agentcore.utils.activex.CreateObject("SdcUser.SdcMailCtl");
        if (oSSMail) {
          oSSMail.SetWindowsMailPassword(this.oEmail.sAccountName, this.oEmail.sPassword);
          delete oSSMail;
          bRet = true;
        }
      }
    }
  } catch (ex) {
    this._exception.HandleException(ex, '$ss.snapin.email.utility', 'ssCWindowsMail.prototype.SetAccountPassword', arguments);
  }
  return bRet;
}  

/*******************************************************************************
** Name:         _ssCWindowsMail_AuthenticatePassword
**
** Purpose:      Get the sProfile variable, if its empty then tries to get 
**               default profile or first profile 
**               profiles are used : 2002 onwards  
**               
** Parameter:    none       And no need to have class instantiated with valid
**                          oEmail object
**
** Return:       this.sProfile : string having profile name or empty incase of 
**                          failures
********************************************************************************/
ssCWindowsMail.prototype.AuthenticatePassword = function _ssCWindowsMail_AuthenticatePassword() {
  this._logger.info("_ssCWindowsMail_AuthenticatePassword");
  var bAccountAuthenticatedOK = false;
  if (!this.oEmail.sAccountName) return false;
  try {
    var mailobj = new $ss.agentcore.utils.activex.CreateObject("SdcUser.SdcMailCtl");
    if (mailobj) {
      bAccountAuthenticatedOK = mailobj.AuthenticateWindowsMailPop(this.oEmail.sAccountName);
      delete mailobj;
    }
  } catch (e) { }
  return bAccountAuthenticatedOK;
}

/*******************************************************************************
** Name:         _ssCWindowsMail_IsPasswordSaved
**
** Purpose:      Get the sProfile variable, if its empty then tries to get 
**               default profile or first profile 
**               profiles are used : 2002 onwards  
**               
** Parameter:    none       And no need to have class instantiated with valid
**                          oEmail object
**
** Return:       this.sProfile : string having profile name or empty incase of 
**                          failures
********************************************************************************/
ssCWindowsMail.prototype.IsPasswordSaved = function _ssCWindowsMail_IsPasswordSaved() {
  this._logger.info("_ssCWindowsMail_IsPasswordSaved");
  var bAccountAuthenticatedOK = false;
  if (!this.oEmail.sAccountName) return false;
  try {
    var mailobj = new $ss.agentcore.utils.activex.CreateObject("SdcUser.SdcMailCtl");
    if (mailobj) {
      bAccountAuthenticatedOK = mailobj.IsWindowsMailPasswordSaved(this.oEmail.sAccountName);
      delete mailobj;
    }
  } catch (e) { }
  return bAccountAuthenticatedOK;
}

/*******************************************************************************
** Name:         _DeleteAccount_DeleteAccount
**
** Purpose:      Delete the specifed Win Mail email account
**               
** Parameter:    sProfile - Not used
**               sServiceId - Not used
**               sAccountId - Account Id (AccountId property of account object)
**
** Return:       1 - Success, 0 - Failure
**               
********************************************************************************/
ssCWindowsMail.prototype.DeleteAccount = function _ssCWindowsMail_DeleteAccount(sProfile, sServiceId, sAccountId) {
  this._logger.info("_ssCWindowsMail_DeleteAccount");

  if (sAccountId.match(/account{/i) == null)
    sAccountId = "account" + sAccountId + ".oeaccount"

  var sAccountPath = this.GetMailFolder() + sAccountId;
  var bExisting = $ss.agentcore.dal.file.FileExists(sAccountPath);

  if (bExisting)
    bExisting = $ss.agentcore.dal.file.DeleteFile(sAccountPath, true);

  sAccountPath = this.sLocalFolder + sAccountId;
  bExisting = $ss.agentcore.dal.file.FileExists(sAccountPath);

  if (bExisting)
    bExisting = $ss.agentcore.dal.file.DeleteFile(sAccountPath, true);

  return bExisting;
}

/*******************************************************************************
** Name:         _ssCWindowsMail_SetAsDefault
**
** Purpose:      Set specifed Win Mail email account as default
**               
** Parameter:    sProfile - Not used
**               sServiceId - Not Used
**               sAccountId - Account Id (AccountId property of account object)
**
** Return:       1 - Success, 0 - Failure
**               
********************************************************************************/
ssCWindowsMail.prototype.SetAsDefault = function _ssCWindowsMail_SetAsDefault(sProfile, sServiceId, sAccountId) {
  this._logger.info("_ssCOExpress_SetAsDefault");
  if (sAccountId.match(/account{/i) == null)
    sAccountId = "account" + sAccountId + ".oeaccount";

  $ss.agentcore.dal.registry.SetRegValueByType("HKCU", "Software\\Microsoft\\IAM", "Default Mail Account", 1, sAccountId);
  return 1;
}


//===========================================================================================================
//    UI Related Functionality
//       - The following functions are used in the UI functionality for sma_configemail.htm
//    TODO: Move from here to sma specific location
//===========================================================================================================
//this function is not used.
function ss_em_CreateEmail()   
{     
  if (g_bEmailCreated) 
  {              // We have created the account, bye    
    $ss.agentcore.diagnostics.smartissue.PersistInfo($ss.agentcore.constants.smartissue.EMAIL_CLASS, $ss.agentcore.constants.smartissue.EMAIL_CONFIGURED, "true");
    return true;
  }
  
  var sType = getInput();
  if (sType.length == 0) 
  {
   //$ss.agentcore.utils.ui.ShowDialog(1, ss_loc_XLate("makeasel"));
   $ss.agentcore.utils.ui.ShowDialog(1, "makeasel");
   $ss.agentcore.diagnostics.smartissue.PersistInfo($ss.agentcore.constants.smartissue.EMAIL_CLASS, $ss.agentcore.constants.smartissue.EMAIL_CONFIGURED, "false");
   return false;
  }
  
  //---  Did they give a display  and account name ? -------
  if (document.all.displayname.value.length==0) 
  {
   //$ss.agentcore.utils.ui.ShowDialog(1, ss_loc_XLate("makeadn"));
   $ss.agentcore.utils.ui.ShowDialog(1, "makeadn");
   $ss.agentcore.diagnostics.smartissue.PersistInfo($ss.agentcore.constants.smartissue.EMAIL_CLASS, $ss.agentcore.constants.smartissue.EMAIL_CONFIGURED, "false");
   return false;
  }
    
  if (document.all.accountname.value.length==0) 
  {
    //$ss.agentcore.utils.ui.ShowDialog(1, ss_loc_XLate("makean"));
    $ss.agentcore.utils.ui.ShowDialog(1, "makean");
    $ss.agentcore.diagnostics.smartissue.PersistInfo($ss.agentcore.constants.smartissue.EMAIL_CLASS, $ss.agentcore.constants.smartissue.EMAIL_CONFIGURED, "false");
    return false;
  }
  
  var emailAddress = GetEmailAccount();
  var sUserId = emailAddress;
  
  var useFullDomainInEmail = $ss.agentcore.dal.config.GetConfigValue("email", "useFullDomainUserId", "false");
  if (useFullDomainInEmail != "true")
  {
    sUserId = emailAddress.indexOf("@")!=-1?emailAddress.substr(0,emailAddress.indexOf("@")):emailAddress;
  }
  
  // TODO: look databag instead - Send password from caller 
  var sPassword = $ss.agentcore.dal.registry.GetRegValue(REG_TREE, $ss.agentcore.dal.config.GetRegRoot(), "userpass");
    
  if(sPassword != "") 
    sPassword = $ss.agentcore.utils.DecryptHexToStr(sPassword);
    
  var oAddOns = {
    "sAccountName"  : document.emailinfo.accountname.value,
    "sUserName"       : sUserId,
    "sPassword"     : sPassword,
    "sDisplayName"  : document.emailinfo.displayname.value,
    "sEmailAddress" : emailAddress
  };
  var oEmail = new ssCEmail(true);
  oEmail.Init(oAddOns);
  
  $ss.agentcore.diagnostics.smartissue.PersistInfo($ss.agentcore.constants.smartissue.EMAIL_CLASS, $ss.agentcore.constants.smartissue.EMAIL_ACCOUNTNAME, sUserId);
  $ss.agentcore.diagnostics.smartissue.PersistInfo($ss.agentcore.constants.smartissue.EMAIL_CLASS, $ss.agentcore.constants.smartissue.EMAIL_CLIENT, sType);
  
  var nResult = 1;  // default to failure
  var oEmailClient = GetEmailClient(sType, oEmail);
  
  if(oEmailClient != null) {
    
    $ss.agentcore.diagnostics.smartissue.PersistInfo($ss.agentcore.constants.smartissue.EMAIL_CLASS, $ss.agentcore.constants.smartissue.EMAIL_CLIENTVERSION, oEmailClient.GetVersion());
    nResult = oEmailClient.CreateAccount();
    
    if(nResult == 1) {
    
      $ss.agentcore.diagnostics.smartissue.PersistInfo($ss.agentcore.constants.smartissue.EMAIL_CLASS, $ss.agentcore.constants.smartissue.EMAIL_CONFIGURED, "false");
      switch(sType) {
        case EM_OUTLOOK_EXPRESS:
          //$ss.agentcore.utils.ui.ShowDialog(1, ss_loc_XLate("oemsg1"));
          $ss.agentcore.utils.ui.ShowDialog(1, "oemsg1");
          return false;
        case EM_MS_OUTLOOK:
          //$ss.agentcore.utils.ui.ShowDialog(1, ss_loc_XLate("omsg1"));
          $ss.agentcore.utils.ui.ShowDialog(1, "omsg1");
          return false; 
        case EM_WINDOWS_MAIL:
      
          return false;
      }    
    
    } else if(nResult == 0) {
      
      $ss.agentcore.diagnostics.smartissue.PersistInfo($ss.agentcore.constants.smartissue.EMAIL_CLASS, $ss.agentcore.constants.smartissue.EMAIL_CONFIGURED, "true");
      switch(sType) {
        case EM_OUTLOOK_EXPRESS:
          $ss.agentcore.dal.registry.SetRegValue(REG_TREE, $ss.agentcore.dal.config.GetRegRoot(),"emailStatus","createdoe");
          break;
        case EM_MS_OUTLOOK:
          $ss.agentcore.dal.registry.SetRegValue(REG_TREE, $ss.agentcore.dal.config.GetRegRoot(),"emailStatus","createdo");
          break;
        case EM_WINDOWS_MAIL:
          $ss.agentcore.dal.registry.SetRegValue(REG_TREE, $ss.agentcore.dal.config.GetRegRoot(), "emailStatus", "createdwm");
          break;
      }
      return true;
    }
  }
  
  try {
    document.emailinfo.displayname.disabled = true;
  } catch(ex) {
    //ignore
  }
  $ss.agentcore.diagnostics.smartissue.PersistInfo($ss.agentcore.constants.smartissue.EMAIL_CLASS, $ss.agentcore.constants.smartissue.EMAIL_CONFIGURED, "false");
  return false;
}
  
/*******************************************************************************
** Name:         getInput
**
** Purpose:      
**
** Return:       
*******************************************************************************/
//this function is not used
function getInput()   
{
  var radioGrp = document.all.REM;
  if (radioGrp.length==null)
  {
    return radioGrp.id;
  } 
  else 
  {
    for (var i=0; i<radioGrp.length; i++) 
    {
      if (radioGrp[i].checked) 
      {
        $ss.agentcore.dal.registry.SetRegValue(REG_TREE, $ss.agentcore.dal.config.GetRegRoot(), "defaultEMail",i);
        return radioGrp[i].id;
      }
    }
  }
  return "";
}

/*******************************************************************************
** Name:         showInstruction
**
** Purpose:      
**
** Return:       
*******************************************************************************/
//this function is not used
function showInstruction(showHide)
{
  try {document.emailinfo.displayname.disabled = (showHide);} catch(ex) {}
}        
               
/*******************************************************************************
** Name:         ss_em_SetRadio
**
** Purpose:      
**
** Return:       none
*******************************************************************************/
//this function is not used
function ss_em_SetRadio() 
{
  if (document.all.REM && $ss.agentcore.dal.registry.GetRegValue(REG_TREE, $ss.agentcore.dal.config.GetRegRoot(), "defaultEMail") ) 
  {
    try {
      document.all.REM[$ss.agentcore.dal.registry.GetRegValue(REG_TREE, $ss.agentcore.dal.config.GetRegRoot(), "defaultEMail")].checked = true;
    } catch(ex) {
    this._exception.HandleException(ex, '$ss.snapin.email.utility', 'ss_em_SetRadio', arguments);
    }
  }
}

/*******************************************************************************
** Name:         ss_em_pvt_GetHTML
**
** Purpose:      
**
** Return:       sOutHTML : Well formed HTML for the radio box selection
*******************************************************************************/
//this function is not used
function ss_em_pvt_GetHTML(type, checked) 
{
  var msg = type;
  var out = '<tr><td width="8">&nbsp;</td>';
  out += '<td class="clsPageText">';
  out += '<input type="radio" class="clsRadioButton" ';
  if (checked)
    out += 'checked ';

  if (type=="OutlookSorry") // Outlook, but no Internet Mail
  {                
    out += ' disabled ';
    msg = "Exchange without Internet Email";
  }
  var bIsNone = (type=="None");
  if (bIsNone) // then they will do it themselves
  {                    
    //msg = ss_loc_XLate("notsup");
    msg = "notsup";
  }
  
  out += ' onclick=showInstruction('+bIsNone+');'
  out += ' name="REM" id="' + type + '"><label for="' + type + '" id="' + type + 'label" class="clsRadioButton">' + msg;
  if (checked)
  {
    //out = out + " " + ss_loc_XLate('worddefault') + "</label></td>";
    out = out + " " + "worddefault" + "</label></td>";
  }
  return out + '</tr>';
}
 
/*******************************************************************************
** Name:         ss_em_GenerateEMailSelection
**
** Purpose:      
**
** Return:       sOutHTML : Well formed HTML for the radio box selection
*******************************************************************************/
//this function is not used
function ss_em_GenerateEMailSelection(ulFlag) 
{
  var defaultClient = GetDefaultEmailClient();
  var sOutlookVer   = GetOutlookVersion(); 
  var sOutHTML = ""; 
  var bInput   = false;   

  //-----------  Outlook    ---------------    
  if (  $ss.agentcore.utils.HasMask(ulFlag, EM_SHW_MS_OUTLOOK) && 
        (sOutlookVer == EM_OLK_VER2002 || sOutlookVer == EM_OLK_VER2003) &&
        HasOutlookInternet())
  {
    bInput = (defaultClient == EM_MS_OUTLOOK || defaultClient == EM_MS_EXCHANGE);
    sOutHTML += ss_em_pvt_GetHTML(EM_MS_OUTLOOK, bInput);
  }
  
  //-----------  Windows Mail    ----------------    
  if ( $ss.agentcore.utils.HasMask(ulFlag, EM_SHW_WINDOWS_MAIL) && 
       $ss.agentcore.utils.system.IsWinVST(true)) 
  {
    sOutHTML += ss_em_pvt_GetHTML(EM_WINDOWS_MAIL, (defaultClient == EM_WINDOWS_MAIL));
  } 
  //-----------  Outlook Express ---------------
  else if ($ss.agentcore.utils.HasMask(ulFlag, EM_SHW_OUTLOOK_EXPRESS) && IsOutlookExpressInstalled())
  {        
    bInput = (defaultClient.indexOf(EM_OUTLOOK_EXPRESS)>=0);
    sOutHTML += ss_em_pvt_GetHTML(EM_OUTLOOK_EXPRESS, bInput);
  }        

  //----------- Allow No Clients to be configured ---------------
  if ($ss.agentcore.utils.HasMask(ulFlag, EM_SHW_OPTION_NONE))
  {
    sOutHTML += ss_em_pvt_GetHTML("None", false);
  }
  return sOutHTML;
}

(function() {
  $.extend($ss.snapin.email.utility, {


    /*******************************************************************************
    **      F A C T O R Y    M E T H O D S 
    ********************************************************************************/

    UpdateAccount: function(acctObj) {

      if (!acctObj) {
        return false;
      }

      var oEmail = this.GetEmailObject(acctObj);
      if (!oEmail) {
        return false;
      }

      var clientToUpdate = this.GetEmailClient(acctObj.MailClient, oEmail);
      if (!clientToUpdate) {
        return false;
      }

      if (acctObj.MailClient == EM_OUTLOOK_EXPRESS) {
        clientToUpdate.SetIdentity(acctObj.Profile);
      } else if (acctObj.MailClient == EM_MS_OUTLOOK) {
        clientToUpdate.SetProfile(acctObj.Profile);
        clientToUpdate.sACCT_MGR_KEY = ssCOutlook.ACCT_MGR_KEY;
      }

      clientToUpdate.sAccountId = acctObj.AccountId;

      if (acctObj.MailClient == EM_WINDOWS_MAIL) {
        clientToUpdate.sMailFolder = clientToUpdate.GetMailFolder();
        clientToUpdate.SetAccountProfile("Local Folders\\account", ".oeaccount", oEmail.sNewAccount);
        if (acctObj.BinaryPassword == false) clientToUpdate.SetAccountPassword();
      }
      else if (acctObj.MailClient == EM_WINDOWS_LIVE_MAIL) {
        clientToUpdate.sMailFolder = clientToUpdate.GetMailFolder();
        clientToUpdate.sCurrentMailFolder = clientToUpdate.GetCurrentMailFolder(oEmail.sAccountId);
        clientToUpdate.SetAccountProfile(true, ".oeaccount", false);
        if (acctObj.BinaryPassword == false) clientToUpdate.SetAccountPassword();
      }
      else
        clientToUpdate.SetAccountProfile();

      if (oEmail.sPassword && oEmail.sPassword.length > 0) {
        if (acctObj.MailClient !== EM_WINDOWS_MAIL && acctObj.MailClient !== EM_WINDOWS_LIVE_MAIL) {
          clientToUpdate.SetAccountPassword();
        }
      }

      return true;
    },

    GetEmailObject: function(acctObj) {
      if (!acctObj) {
        return null;
      }

      var accountSettings = {
        "sAccountName": acctObj.Name,
        "sProfile": acctObj.Profile,
        "sDisplayName": acctObj.DisplayName,
        "sAccountId": acctObj.AccountId,
        "sUserName": acctObj.Pop3User,
        "sPassword": acctObj.Password,
        "sEmailAddress": acctObj.Email,
        "sSMTPServer": acctObj.SmtpServer,
        "sPOPServer": acctObj.Pop3Server,
        "sNewAccount": acctObj.sNewAccount,
        "sPOPPort": acctObj.Pop3Port,
        "sPOPSSL": acctObj.Pop3SSL,
        "sPOPAuth": acctObj.Pop3Auth,
        "sSMTPPort": acctObj.SmtpPort,
        "sSMTPSSL": acctObj.SmtpSSL,
        "sSMTPAuth": acctObj.SmtpAuth,
        "sIsDefault": acctObj.IsDefault
      };

      var oEmail = new ssCEmail(true);
      if (!oEmail) {
        return false;
      }

      oEmail.Init(accountSettings);
      return oEmail;
    },

    GetEmailClient: function(sType, oEmail) // by Type
    {
      var ob = null;
      try {
        //if(!oEmail.bInit) return ob;

        switch (sType) {
          case EM_WINDOWS_MAIL:
            ob = new ssCWindowsMail(oEmail);
            break;

          case EM_WINDOWS_LIVE_MAIL:
            ob = new ssCWindowsLiveMail(oEmail);
            break;

          case EM_MS_OUTLOOK:
          case EM_MS_EXCHANGE:
            var sVer = this.GetOutlookVersion();
            switch (sVer) {
              case EM_OLK_VER2002:
                ob = new ssCOutlook2002(oEmail);
                break;

              case EM_OLK_VER2003:
                ob = new ssCOutlook2003(oEmail);
                break;

              case EM_OLK_VER2007:
                ob = new ssCOutlook2007(oEmail);
                break;
              case EM_OLK_VER2010:
                ob = new ssCOutlook2010(oEmail);
                break;
                
              case EM_OLK_VER2013:
                ob = new ssCOutlook2013(oEmail);
                break;

              case EM_OLK_VER1997:
              case EM_OLK_VER2000:
              default:
                ob = new ssCOutlook(oEmail);
                break;
            }
            break;

          case EM_OUTLOOK_EXPRESS:
            ob = new ssCOExpress(oEmail);
            break;

          default: // do nothing
            this._logger.info("GetEmailClient", "Invalid Email type");
            break;
        }

      } catch (ex) {
        _exception.HandleException(ex, '$ss.snapin.email.utility', 'GetEmailClient', arguments);
      }
      return ob;
    },


    /*******************************************************************************
    **      G E N E R A L   M A I L   C L I E N T   F U N C T I O N S
    ********************************************************************************/

    /*******************************************************************************
    ** Name:         ValidateEmailObject
    **
    ** Purpose:      Validates all the properties of the email object before configuring
    **               email client.
    **
    ** Return:       1: if all the feilds are valid
    **               0: if one or more feild is empty
    **               -1: invalid email
    **               -2: invalid name
    **               -3: invalid username
    ********************************************************************************/
    ValidateEmailObject: function(acctObj) {
      var intRetVal = 1;

      if (!acctObj) {
        return 0;
      }

      // Check for any missing values
      if (acctObj.sDisplayName == "" || acctObj.sAccountName == "" || acctObj.sEmailAccount == "" || acctObj.sEmailAddress == "" || acctObj.sPOPServer == "" || acctObj.sSmtpServer == "" || acctObj.sUserName == "" || acctObj.sPassword == "") {
        intRetVal = 0;
      } else {
        var addressPattern = /^[a-z]\w*((-|\.)\w+)*\@[a-z][a-z0-9]*((\.|-)[a-z0-9]+)*\.[a-z0-9]+$/i;
        var namePattern = /^[a-z][a-z \.]*$/i;
        //var usernamePattern = /^[a-z]\w*((-|\.)\w+)*$/i;
        var usernamePattern = /\b[A-Z0-9._%+-]/i;

        if (!addressPattern.test(acctObj.sEmailAddress)) {
          intRetVal = -1;
        } else if (!namePattern.test(acctObj.sDisplayName)) {
          intRetVal = -2;
        } else if (!usernamePattern.test(acctObj.sUserName)) {//Domain name is appended to the username, hence do validation for address
          intRetVal = -3;
        }
      }

      return intRetVal;
    },

    /*******************************************************************************
    ** Name:         GetDefaultEmailClient
    **
    ** Purpose:      Determine the system's default email client
    **
    ** Return:       string containing the name of the email client
    **               (should be either 'Microsoft Outlook' or 'Outlook Express')
    ********************************************************************************/
    GetDefaultEmailClient: function() {
      var defMailClient = $ss.agentcore.dal.registry.GetRegValue("HKCU", "Software\\Clients\\Mail\\", "").toString();

      if (defMailClient == "") {
        defMailClient = $ss.agentcore.dal.registry.GetRegValue("HKLM", "Software\\Clients\\Mail\\", "").toString();
      }

      return defMailClient;
    },


    /*******************************************************************************
    ** Name:         IsOutlookInstalled
    **
    ** Purpose:      Returns true if Outlook Is installed
    **
    ** Return:       true if installed else false
    ********************************************************************************/
    IsOutlookInstalled: function() {
      try {
        var regVal = $ss.agentcore.dal.registry.GetRegValue("HKLM", "Software\\Clients\\Mail\\Microsoft Outlook\\", "");

        //support upto Outlook 2010 (version 14)
        var sOutlookVer = this.GetOutlookCurVersion();
        if (regVal == null || regVal == "" || sOutlookVer > 15)
          return false;
        else
          return true;
      }
      catch (ex) {
        _exception.HandleException(ex, '$ss.snapin.email.utility', 'IsOutlookInstalled', arguments);
      }

      return false;
    },

    /*******************************************************************************
    ** Name:         GetOutlookCurVersion
    **
    ** Purpose:      Get the outlook version installed on the machine
    **
    ** Return:       version of outlook if installed else infinity
    ********************************************************************************/
    GetOutlookCurVersion: function() {
      try {
        var sId = this.GetRegOutlookCurVer();
        if (!(typeof (sId) === "undefined" || sId == "")) {
          var arrOL = sId.split(".");
          if (! (typeof (arrOL[2]) === "undefined"))
            return arrOL[2];
        }
      } catch (ex) {
        _exception.HandleException(ex, '$ss.snapin.email.utility', 'GetOutlookCurVersion', arguments);
      }
      return Infinity;
    },

    GetRegOutlookCurVer: function() {
      return $ss.agentcore.dal.registry.GetRegValue("HKCR", "Outlook.application\\CurVer", "");
    },

    /*******************************************************************************
    ** Name:         HasOutlookInternet
    **
    ** Purpose:      Returns true if Outlook has Internet Mail configured
    **
    ** Return:       bool
    ********************************************************************************/
    HasOutlookInternet: function() {
      var products = $ss.agentcore.dal.registry.EnumRegKey("HKCU", "Software\\Microsoft\\Office", ",");
      try {
        //formulate a reg path based on current OL version
        var sId = this.GetRegOutlookCurVer().split(".");
        var ol_version = sId[2] + ".0";
        var ol_present_cu = $ss.agentcore.dal.registry.EnumRegKey("HKCU", "Software\\Microsoft\\Office\\" + ol_version, ",");
        var ol_present_lm = $ss.agentcore.dal.registry.EnumRegKey("HKLM", "Software\\Microsoft\\Office\\" + ol_version, ",");
        return (products.indexOf("Outlook") != -1 || ol_present_cu.indexOf("Outlook") != -1 || ol_present_lm.indexOf("Outlook") != -1);
      } catch (ex) {
        return false;
      }
    },

    /*******************************************************************************
    ** Name:         IsOutlookExpressInstalled
    **
    ** Purpose:      Returns true if Outlook Express is installed
    **
    ** Return:       true if installed else false
    ********************************************************************************/
    IsOutlookExpressInstalled: function() {
      try {
        var oeRegVal = $ss.agentcore.dal.registry.GetRegValue("HKLM", "Software\\Clients\\Mail\\Outlook Express\\", "");
        if (oeRegVal == null || oeRegVal == "") {
          // secondary check in control panel list of installed components (in case the mail client regkey was missing)
          oeRegVal = $ss.agentcore.dal.registry.GetRegValue("HKLM", "Software\\Microsoft\\Active Setup\\Installed Components\\{44BBA840-CC51-11CF-AAFA-00AA00B6015C}", "");
          if (oeRegVal != null && oeRegVal != "") {
            if (oeRegVal.indexOf("Outlook Express") != -1)
              return true;
          }
          return false;
        } else {
          return true;
        }
      } catch (ex) {
        _exception.HandleException(ex, '$ss.snapin.email.utility', 'IsOutlookExpressInstalled', arguments);
      }
      return false;
    },

    /*******************************************************************************
    ** Name:         GetOutlookVersion
    **
    ** Purpose:      Look for outlook version in registry
    **
    ** Return:       Return outlook version or empty string if no outlook
    ** Note:         Certain Anti-Virus applications trigger on Outlook DOT Application
    **               To work around this issue the two portions of the object ProgID
    **               are concatenated together.  It is NOT the actual instantiation
    **               of the object that triggers these Anti-Virus applications.
    *******************************************************************************/
    GetOutlookVersion: function() {
      var sVersion = "";
      try {
        var sId = this.GetRegOutlookCurVer();
        if (sId.length > 0) {
          switch (sId.toLowerCase()) {
            case "outlook" + ".application.15": sVersion = EM_OLK_VER2013; break;            
            case "outlook" + ".application.14": sVersion = EM_OLK_VER2010; break;
            case "outlook" + ".application.12": sVersion = EM_OLK_VER2007; break;
            case "outlook" + ".application.11": sVersion = EM_OLK_VER2003; break;
            case "outlook" + ".application.10": sVersion = EM_OLK_VER2002; break;
            case "outlook" + ".application.9": sVersion = EM_OLK_VER2000; break;
            case "outlook" + ".application.8": sVersion = EM_OLK_VER1997; break;
            default: sVersion = sId; break;
          }
        }
      } catch (ex) {
        _exception.HandleException(ex, '$ss.snapin.email.utility', 'GetOutlookVersion', arguments);
      }
      return sVersion;
    },

    /*******************************************************************************
    ** Name:         GetConfigArray
    **
    ** Purpose:      Returns an aggregate list of config values for smtp and pop 
    **               servers. Function is used to map the descrepancies in config
    **               layouts for subagent, repair manager, smartaccess
    **
    **               typical usage: 
    **               var a = GetConfigArray(EM_CFG_SMTP, false);
    **               
    ** Parameters:   ulFlag - EM_CFG_SMTP|EM_CFG_POP3 specifying which all
    **                        values to get
    **               bIsISPSpecific - whether to look for isp specific values
    **                                true: smartaccess type
    **                                false: subagent/repair manager type 
    **
    ** Return:       aRet : array of config values
    *******************************************************************************/
    GetConfigArray: function(ulFlag, bIsISPSpecific) {
      var aRet = [];
      try {
        if (bIsISPSpecific) {
          var sISP = $ss.agentcore.diagnostics.smartissue.PersistInfo($ss.agentcore.constants.smartissue.GEN_CLASS, $ss.agentcore.constants.smartissue.ISP_NAME, "isp1");
          if ($ss.agentcore.utils.HasMask(ulFlag, EM_CFG_SMTP))
            aRet.push($ss.agentcore.dal.config.GetConfigValue("email", "smtp_" + sISP, ""));
          if ($ss.agentcore.utils.HasMask(ulFlag, EM_CFG_POP3))
            aRet.push($ss.agentcore.dal.config.GetConfigValue("email", "pop_" + sISP, ""));
        }
        else {
          if ($ss.agentcore.utils.HasMask(ulFlag, EM_CFG_SMTP))
            aRet = $ss.agentcore.dal.config.GetValues("email", "smtp_servers");
          if ($ss.agentcore.utils.HasMask(ulFlag, EM_CFG_POP3)) {
            if (aRet.length > 0)
              aRet = aRet.concat($ss.agentcore.dal.config.GetValues("email", "pop3_servers"));
            else
              aRet = $ss.agentcore.dal.config.GetValues("email", "pop3_servers");
          }
        }
      } catch (ex) {
        _exception.HandleException(ex, '$ss.snapin.email.utility', 'GetConfigArray', arguments);
      }
      return aRet;
    },

    //*******************************************************************************************
    // Name:         PvtCleanArray
    // Purpose:      Clean an array after items are deleted from it.
    // Parameter:
    // Return:
    //*******************************************************************************************
    PvtCleanArray: function(list, iStart) {
      if (list) {
        iStart = iStart || 0;
        var i2;
        while (iStart < list.length) {
          if (list[iStart] == undefined) {
            i2 = iStart + 1;
            while (i2 < list.length) {
              if (list[i2] != undefined) {
                list[iStart] = list[i2];
                iStart++;
              }
              i2++;
            }
            list.length = iStart;
          }
          iStart++;
        }
      }
    },

    /*******************************************************************************
    ** Name:         FilterUnsupportedAccounts
    **
    ** Purpose:      Removes unwanted accounts which don't match
    our list
    **               
    **
    **               
    **
    ** Return:       nothing; modifies arrAccounts structure
    *******************************************************************************/
    FilterUnsupportedAccounts: function(arrAccounts) {
      if (!arrAccounts) {
        return null;
      }

      var AccountObject, bEmailOK, bPop3OK, bSMTPOK, bExchangeOK;
      for (var i = 0; i < arrAccounts.length; i++) {
        AccountObject = arrAccounts[i];
        switch (AccountObject.Type) {
          case ssCOutlook.ACCT_TYPE_POP:
            bPop3OK = this.IsServerInPOP3List(AccountObject.Pop3Server);
            bSMTPOK = this.IsServerInSMTPList(AccountObject.SmtpServer);
            bEmailOK = this.IsEmailInDomainList(AccountObject.Email);
            if (!bEmailOK && !bPop3OK && !bSMTPOK)
              delete (arrAccounts[i]);
            break;
          case ssCOutlook.ACCT_TYPE_EXCHANGE:
            bExchangeOK = this.IsServerInExchangeList(AccountObject.ExchangeServer);
            bEmailOK = AccountObject.Email != "" && this.IsEmailInDomainList(AccountObject.Email); // skip exchange accounts
            if (!bExchangeOK && !bEmailOK)
              delete (arrAccounts[i]);
            break;
          default:
            bPop3OK = this.IsServerInPOP3List(AccountObject.Pop3Server);
            bSMTPOK = this.IsServerInSMTPList(AccountObject.SmtpServer);
            bEmailOK = this.IsEmailInDomainList(AccountObject.Email);
            if (!bEmailOK && !bPop3OK && !bSMTPOK)
              delete (arrAccounts[i]);
            break;
        }
      }
      this.PvtCleanArray(arrAccounts);
      return arrAccounts;
    },

    /*******************************************************************************
    ** Name:         GetAllEmailAccounts
    **
    ** Purpose:      Returns an aggregate list of accounts accross all supported 
    **               mail clients, Outlook, OE and Windows Mail.
    **
    **               Future Expansion: One can pass specific profile or identity
    **               to get accounts only under those. Function goes for defaults 
    **               when not specified.
    **
    ** Return:       aAccounts : An array of accounts
    *******************************************************************************/
    GetAllEmailAccounts: function(bFilter, sOLProfile, sOEIdentity) {
      var aAccounts = new Array();
      try {
        var oEmail = new ssCEmail();
        var oMailClient = null;

        // check all mail client for existance and if found query them for all accounts
        if (this.IsOutlookInstalled()) {
          oMailClient = this.GetEmailClient(EM_MS_OUTLOOK, oEmail);
          if (oMailClient != null) {
            if (typeof sOLProfile != "undefined" && sOLProfile != null && sOLProfile.length > 0)
              oMailClient.SetProfile(sOLProfile); // use this profile for getting accounts
            aAccounts = oMailClient.GetAccounts(aAccounts);
          }
        }

        if (this.IsOutlookExpressInstalled()) {
          oMailClient = this.GetEmailClient(EM_OUTLOOK_EXPRESS, oEmail);
          if (oMailClient != null) {
            if (typeof sOEIdentity != "undefined" && sOEIdentity != null && sOEIdentity.length > 0)
              oMailClient.SetIdentity(sOEIdentity); // use this identity for getting accounts
            aAccounts = oMailClient.GetAccounts(aAccounts);
          }
        } else {
          // look up Windows mail
          oMailClient = this.GetEmailClient(EM_WINDOWS_MAIL, oEmail);
          if (oMailClient != null)
            aAccounts = oMailClient.GetAccounts(aAccounts);
        }

        if (this.IsWinLiveMailInstalled()) {
          oMailClient = this.GetEmailClient(EM_WINDOWS_LIVE_MAIL, oEmail);
          if (oMailClient != null)
            aAccounts = oMailClient.GetAccounts(aAccounts);
        }

        if (bFilter) {
          this.FilterUnsupportedAccounts(aAccounts);
        }
      } catch (ex) {
        _exception.HandleException(ex, '$ss.snapin.email.utility', 'GetAllEmailAccounts', arguments);
      }
      return aAccounts;
    },

    /*******************************************************************************
    ** Name:         GetEmailAccount
    **
    ** Purpose:      
    **
    ** Return:       true if installed else false
    *******************************************************************************/
    GetEmailAccount: function() {
      var retval = $ss.agentcore.dal.registry.GetRegValue(REG_TREE, $ss.agentcore.dal.config.GetRegRoot(), "username");
      if (retval == "") {
        retval = "sprt";
      }
      var isp = $ss.agentcore.diagnostics.smartissue.PersistInfo($ss.agentcore.constants.smartissue.GEN_CLASS, "isp");
      var domain = $ss.agentcore.dal.config.GetConfigValue("email", "domain_" + isp, "");

      retval = retval.toLowerCase();                  // fix for agent test problem 
      retval = retval + "@" + domain
      return (retval);
    },

    /*******************************************************************************
    ** Name:         MailSMTP
    **
    ** Purpose:      
    **
    ** Return:       true if installed else false
    *******************************************************************************/
    MailSMTP: function(mailServer, smtpPort, timeout) {
      var retval = false;
      try {
        retval = ss_net_TestConnect(null, mailServer, smtpPort, timeout);
        // check connection on specified port.. if it is invalid don't bother with rest of check
        if (!retval)
          return retval;

        var oSSMail = $ss.agentcore.utils.activex.CreateObject("SdcUser.SdcMailCtl");
        var toAddress = GetEmailAccount();
        var fromAddress = $ss.agentcore.dal.config.GetConfigValue("email", "fromaddress", "");
        var subject = $ss.agentcore.dal.config.GetConfigValue("email", "subject", "");
        var body = $ss.agentcore.dal.config.GetConfigValue("email", "body", "");
        // MailSMTP call will return true or false
        retval = oSSMail.MailSMTP(mailServer, toAddress, "", fromAddress, "", subject, body);
        delete oSSMail;
      } catch (ex) {
        _exception.HandleException(ex, '$ss.snapin.email.utility', 'MailSMTP', arguments);
      }
      return retval;
    },

    /*******************************************************************************
    ** Name:         CheckPop
    **
    ** Purpose:      Test connection to POP server
    **
    ** Parameter:    pop      -- address of POP server to test
    **               username -- string
    **               password -- string 
    **
    ** Return:       true if succeeded else false
    *******************************************************************************/
    CheckPop: function(pop, username, password, popPort, timeout) {
      var retval = false;
      try {

        retval = ss_net_TestConnect(null, pop, popPort, timeout);
        // check connection on specified port.. if it is invalid don't bother with rest of check
        if (!retval)
          return retval;
        var oSSMail = $ss.agentcore.utils.activex.CreateObject("SdcUser.SdcMailCtl");
        retval = (oSSMail.CheckPop(pop, username, password) != -1);
        delete oSSMail;
      } catch (ex) {
        _exception.HandleException(ex, '$ss.snapin.email.utility', 'CheckPop', arguments);
      }
      return retval;
    },

    /*******************************************************************************
    ** Name:         NormalizeString
    **
    ** Purpose:      Expand string with zeroes to the specified length
    **
    ** Parameter:    sStr     -- String to Normalize
    **               nLength  -- length to normalize it to
    **
    ** Return:       normalized string
    *******************************************************************************/
    NormalizeString: function(sStr, nLength) {
      var sRet = "";
      try {
        if (sStr && sStr.length > 0) {
          sRet = sStr;
          var nLoop = nLength - sStr.length;
          for (var i = 0; i < nLoop; i++)
            sRet = "0" + sRet;
        }
      } catch (ex) {
        _exception.HandleException(ex, '$ss.snapin.email.utility', 'NormalizeString', arguments);
      }
      return sRet;
    },

    //*******************************************************************************************
    // Name:         EmailValidation
    // Purpose:      Check validity of any email server based on config_setting from config xml
    // Parameter:    email address string, config xml string to look up
    // Return:       bool
    //*******************************************************************************************
    EmailValidation: function(emailaddress, config_setting) {
      try {
        var arrDomains = $ss.agentcore.dal.config.GetValues("email", config_setting);
        for (var i = 0; i < arrDomains.length; i++) {
          if (arrDomains[i] != "") {
            var emailpattern = new RegExp(arrDomains[i], "i");
            if (emailpattern.test(emailaddress)) {
              return true;
            }
          }
        }
      } catch (ex) { }
      return false;
    },

    //*******************************************************************************************
    // Name:         IsEmailInDomainList
    // Purpose:      see EmailValidation
    //*******************************************************************************************
    IsEmailInDomainList: function(emailaddress) {
      try {
        emailaddress = emailaddress.split('@')[1];
      } catch (ex) {
        return false; //email address has no @
      }
      return this.EmailValidation(emailaddress, "email_domain_regex");
    },


    //*******************************************************************************************
    // Name:         IsServerInPOP3List
    // Purpose:      see EmailValidation
    //*******************************************************************************************
    IsServerInPOP3List: function(server) {
      return this.EmailValidation(server, "pop3_server_regex");
    },

    //*******************************************************************************************
    // Name:         IsServerInSMTPList
    // Purpose:      see EmailValidation
    //*******************************************************************************************
    IsServerInSMTPList: function(server) {
      return this.EmailValidation(server, "smtp_server_regex");
    },

    //*******************************************************************************************
    // Name:         IsServerInExchangeList
    // Purpose:      see EmailValidation
    //*******************************************************************************************
    IsServerInExchangeList: function(server) {
      return this.EmailValidation(server, "exchange_server_regex");
    },

    IsWindowsMailInstalled: function() {
      var gWindowsMail = null;
      try {
        gWindowsMail = new ssCWindowsMail();
      }
      catch (ex) {
        _exception.HandleException(ex, '$ss.snapin.email.utility', 'IsWindowsMailInstalled', arguments);
      }

      if (gWindowsMail) {
        // Get OS Details
        if ($ss.agentcore.utils.GetOS().toUpperCase() === "WIN7WS") {
          // if win7 -- blindly return false
          //check for winmailgadget.exe in windowsmail folder
          //if(!$ss.agentcore.dal.file.FileExists($ss.agentcore.dal.config.ExpandAllMacros("%ProgramFiles%\\Windows Mail\\WindowsMailGadget.exe")))
          return false;
        }
        // else -continue
        return (gWindowsMail.GetVersion() != 0);
      }

      return false;
    },

    TrimString: function(strIn) {
      if (!strIn) {
        return false;
      }
      var strTrimmed;

      strTrimmed = strIn.replace(/\s+$/g, ""); // strip trailing spaces
      strTrimmed = strTrimmed.replace(/^\s+/g, ""); // strip leading spaces

      return strTrimmed;
    },

    AddEmailAccount: function(oAddOns, selMailClient) {
      
      var sType = selMailClient;

      var oEmail = new ssCEmail(false);

      oEmail.Init(oAddOns);
      var oEmailClient = this.GetEmailClient(sType, oEmail);
      if (oEmailClient != null) {
        if (sType == EM_WINDOWS_MAIL) {
          var mailFolder = oEmailClient.GetMailFolder();
          if (!$ss.agentcore.dal.file.FolderExists(mailFolder)) {
            $ss.agentcore.dal.file.CreateDir(mailFolder);
          }
        }
        return oEmailClient.CreateAccount();
      }
      return -1;
    },


    /*******************************************************************************
    ** Name:         IsWinLiveMailInstalled
    **
    ** Purpose:      Returns true if Windows Live mail is installed
    **
    ** Return:       true if installed else false
    ********************************************************************************/
    IsWinLiveMailInstalled: function() {
      // Version not giving properly even though windows live mail installed
      // so checking with exe name exists or not

      try {
        var installPath = $ss.agentcore.dal.registry.GetRegValue("HKLM", "SOFTWARE\\Microsoft\\Windows Live Mail", "InstallRoot");
        if (installPath != "" && installPath != null) {
          var exepath = installPath + "wlmail.exe";
          if ($ss.agentcore.dal.file.FileExists(exepath))
            return true
          else
            return false;
        }
      }
      catch (e) {
        return false;
      }

      return false;
    },

    /*******************************************************************************
    ** Name:         GetWinLiveMailConfig
    **
    ** Purpose:      Returns Windows Live mail related client_config values
    **
    ** Parameter:    sName:    Config Name
    **               sDefault: Default return value
    **
    ** Return:       Config value for
    ********************************************************************************/
    GetWinLiveMailConfig: function(sName, sDefault) {
      try {
        var sValue = $ss.agentcore.dal.config.GetConfigValue("WindowsLiveMail", sName, sDefault);
      }
      catch (e) {
      }
      return sDefault;
    }

  });

  var _logger = $ss.agentcore.log.GetDefaultLogger("$ss.snapin.email.ssCWindowsMail");
  var _exception = $ss.agentcore.exceptions;
})();