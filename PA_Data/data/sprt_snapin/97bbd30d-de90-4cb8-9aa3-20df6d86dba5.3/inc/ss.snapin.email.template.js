////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//  cl_em_template.js                                                         //
//  -----------------                                                         //
//                                                                            //
//  Copyright (C) 2008, SupportSoft Inc., All Rights Reserved                 //
//                                                                            //
//  This file defines the email template class                                //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//  Function : cl_em_GetTemplates                                             //
//                                                                            //
//  Access   : Public                                                         //
//                                                                            //
//  Arguments: None                                                           //
//                                                                            //
//  Returned : an array of ssCEmailTemplate objects                           //
//                                                                            //
//  Get the list of email templates and create an object for each one         //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

$ss.snapin = $ss.snapin ||
{}; 

$ss.snapin.email = $ss.snapin.email ||
{};

$ss.snapin.email.template = $ss.snapin.email.template ||
{}; 

( function(){
    $.extend($ss.snapin.email.template,{
    
      GetTemplates:function(){
          var arrTemplateNames,arrTemplates,L1;

          // Get the template names

          arrTemplateNames = $ss.agentcore.dal.config.GetValues("email","account_templates","");

          // Create a template for each name

          arrTemplates = new Array ();

          for (L1 = 0; L1 < arrTemplateNames.length; L1++)
              arrTemplates[L1] = new ssCEmailTemplate (arrTemplateNames[L1]);

          // Return the templates

          return (arrTemplates);
      }
    
    });

})();


////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//  Function : ssCEmailTemplate                                               //
//                                                                            //
//  Access   : Public                                                         //
//                                                                            //
//  Arguments: [IN] strName - the name of the template in the config file     //
//                                                                            //
//  ssCEmailTemplate constructor                                              //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

function ssCEmailTemplate (strName)
{
    var arrProperties,strPropertyName,L1;

    // Set defaults

    this.smtpport = "25";
    this.smtpauth = "false";
    this.smtpssl  = "false";
    this.pop3port = "110";
    this.pop3auth = "false";
    this.pop3ssl  = "false";

    // Get the template settings

    arrProperties = $ss.agentcore.dal.config.GetSectionKeyNames(strName);
    if( arrProperties ){
      for (L1 = 0; L1 < arrProperties.length; L1++) {
          strPropertyName = arrProperties[L1].toLowerCase ();
          this[strPropertyName] = $ss.agentcore.dal.config.GetConfigValue(strName,arrProperties[L1]);
      }
    }

    // Throw an exception if certain settings are not defined

    if (typeof (this.emailsuffix)   == "undefined" &&
        typeof (this.emailsuffixre) == "undefined")
       throw ("ssCEmailTemplate: Missing mandatory property");

    if (typeof (this.smtpserver) == "undefined" ||
        typeof (this.pop3server) == "undefined")
       throw ("ssCEmailTemplate: Missing mandatory property");
}


////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//  Function : ssCEmailTemplate::GetProperty                                  //
//                                                                            //
//  Access   : Public                                                         //
//                                                                            //
//  Arguments: [IN] strName - the property name                               //
//                                                                            //
//  Returned : the property value or an empty string if the property is       //
//             undefined                                                      //
//                                                                            //
//  Get a property value                                                      //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

ssCEmailTemplate.prototype.GetProperty = function (strName)
{

    // All properties are held in lower case

    strName = strName.toLowerCase ();

    // Try to get the property

    if (typeof (this[strName]) == "undefined")
       return ("");
    else
       return (this[strName]);
}

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//  Function : ssCEmailTemplate::GetRegExp                                    //
//                                                                            //
//  Access   : Private                                                        //
//                                                                            //
//  Arguments: [IN] strName - the property name                               //
//                                                                            //
//  Returned : a regular expression which can be used in a string match       //
//                                                                            //
//  Get a regular expression from a template property                         //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

ssCEmailTemplate.prototype.GetRegExp = function (strName)
{
    var strValue;

    // All properties are held in lower case

    strName = strName.toLowerCase ();

    // See if there is a regular expression property already defined

    if (typeof (this[strName + "re"]) != "undefined")
       strValue = this[strName + "re"];
    else if (typeof (this[strName]) != "undefined")
       strValue = "^" + this[strName].replace (/\./g,"[.]") + "$";
    else
       throw ("ssCEmailTemplate: Property not found " + strName);

    // Return the regular expression

    return (new RegExp (strValue,"i"));
}