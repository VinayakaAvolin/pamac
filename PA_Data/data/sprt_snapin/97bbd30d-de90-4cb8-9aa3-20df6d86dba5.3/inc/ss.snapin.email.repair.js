////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//  cl_em_repair.js                                                           //
//  ---------------                                                           //
//                                                                            //
//  Copyright (C) 2008, SupportSoft Inc., All Rights Reserved                 //
//                                                                            //
//  This file contains functions for the email troubleshooter to repair       //
//  accounts                                                                  //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

$ss.snapin = $ss.snapin ||
{}; 

$ss.snapin.email = $ss.snapin.email ||
{};

$ss.snapin.email.repair = $ss.snapin.email.repair ||
{}; 

(function() {
  $.extend($ss.snapin.email.repair, {


    ////////////////////////////////////////////////////////////////////////////////
    //                                                                            //
    //  Function : ShowRepairAccountScreen                                  //
    //                                                                            //
    //  Access   : Public                                                         //
    //                                                                            //
    //  Arguments: [IN] objAccount - account                                      //
    //                                                                            //
    //  Show the repair account screen                                            //
    //                                                                            //
    ////////////////////////////////////////////////////////////////////////////////

    GetDataForRepairAccountScreen: function(objAccount, arrAutoProps, arrManualProps) {
      if ((!objAccount) || (!arrAutoProps) || (!arrManualProps)) {
        return false;
      }

      try {
        var objEmailClient, strRegKey, strEmail, strValue;

        // Get the account object

        //g_nRepairAccountIndex = nIndex; Todo remove the global variable Dinesh


        // Determine which settings can be automatically repaired

        //arrAutoProps = new Array ();
        strRegKey = "mailinformation-" + objAccount.Pop3User.toLowerCase();
        //strEmail  = $ss.agentcore.dal.config.GetConfigFromHKLM(strRegKey,"user_email");

        var sDomain = objAccount.m_objTemplate.GetProperty("Domain");

        if (objAccount.Email === objAccount.Name + "@" + sDomain) {
          strEmail = objAccount.Email;
        }
        else {
          strEmail = objAccount.Name + "@" + sDomain;
          try {
            //check if already present email is valid 
            var arrEmail = objAccount.Email.split("@");
            if (arrEmail && (2 == arrEmail.length)) {
              if (arrEmail[1] === sDomain) {
                strEmail = objAccount.Email;
              }
            }
          }
          catch (ex) {

          }
        }

        //strEmail  = objAccount.Name + "@" + objAccount.m_objTemplate.GetProperty ("Domain");
        this.AddAutoRepairSetting(objAccount, "Email", arrAutoProps, strEmail);

        this.AddAutoRepairSetting(objAccount, "Pop3Server", arrAutoProps);
        this.AddAutoRepairSetting(objAccount, "Pop3Port", arrAutoProps);
        this.AddAutoRepairSetting(objAccount, "Pop3SSL", arrAutoProps);
        this.AddAutoRepairSetting(objAccount, "Pop3Auth", arrAutoProps);
        this.AddAutoRepairSetting(objAccount, "SmtpServer", arrAutoProps);
        this.AddAutoRepairSetting(objAccount, "SmtpPort", arrAutoProps);
        this.AddAutoRepairSetting(objAccount, "SmtpSSL", arrAutoProps);
        this.AddAutoRepairSetting(objAccount, "SmtpAuth", arrAutoProps);

        // Determine which manual input fields we should show

        //arrManualProps = new Array ();

        // Determine which manual input fields we should show

        this.AddManualEmailRepairSetting(objAccount, "Email", arrManualProps);

        // If POP3 authentication failed, then we show the credentials fields

        if (objAccount.m_objResult.Pop3AuthTest == false) {

          this.AddManualEmailRepairSetting(objAccount, "Pop3User", arrManualProps);
          this.AddManualRepairSetting("Password", "", arrManualProps);

          objEmailClient = $ss.snapin.email.utility.GetEmailClient(objAccount.MailClient,
                                                                     $ss.snapin.email.utility.GetEmailObject(objAccount));

          objEmailClient.sAccountId = objAccount.AccountId;
          objEmailClient.sProfile = objAccount.Profile;

          if (objEmailClient.IsPasswordSaved() == true)
            this.AddManualRepairSetting("Password_Value", "********", arrManualProps);
        }

        // Create the repair screen

        //cl_em_ui_CreateRepairScreen (nIndex,objAccount.Email,arrAutoProps,arrManualProps); Todo handle UI later Dinesh
      }
      catch (ex) {
        return false;
      }

      return true;
    },

    ////////////////////////////////////////////////////////////////////////////////
    //                                                                            //
    //  Function : RepairAccount                                            //
    //                                                                            //
    //  Access   : Public                                                         //
    //                                                                            //
    //  Arguments: [IN] objAccount - account                                    //
    //                                                                            //
    //  Repair an e-mail account                                                  //
    //                                                                            //
    ////////////////////////////////////////////////////////////////////////////////

    RepairAccount: function(objAccount, pop3User, password) {
      try {
        var strRegKey, strEmailPrefix, strUserPrefix, strPassword, strEmail;

        // Repair the settings from the template
        objAccount.Pop3Server = objAccount.m_objTemplate.GetProperty("Pop3Server");
        objAccount.Pop3Port = objAccount.m_objTemplate.GetProperty("Pop3Port");
        objAccount.Pop3SSL = objAccount.m_objTemplate.GetProperty("Pop3SSL");
        objAccount.Pop3Auth = objAccount.m_objTemplate.GetProperty("Pop3Auth");
        objAccount.SmtpServer = objAccount.m_objTemplate.GetProperty("SmtpServer");
        objAccount.SmtpPort = objAccount.m_objTemplate.GetProperty("SmtpPort");
        objAccount.SmtpSSL = objAccount.m_objTemplate.GetProperty("SmtpSSL");
        objAccount.SmtpAuth = objAccount.m_objTemplate.GetProperty("SmtpAuth");

        var sDomain = objAccount.m_objTemplate.GetProperty("Domain");

        if (objAccount.Email === objAccount.Name + "@" + sDomain) {
          strEmail = objAccount.Email;
        }
        else {
          strEmail = objAccount.Name + "@" + sDomain;
          try {
            //check if already present email is valid 
            var arrEmail = objAccount.Email.split("@");
            if (arrEmail && (2 == arrEmail.length)) {
              if (arrEmail[1] === sDomain) {
                strEmail = objAccount.Email;
              }
            }
          }
          catch (ex) {

          }
        }

        objAccount.Email = strEmail;

        //objAccount.Email      = objAccount.Name + "@" + objAccount.m_objTemplate.GetProperty ("Domain");

        // For Windows Mail we have a password issue if the password remains in binary.
        // So flag it as binary now to allow correct generation of the password tag

        objAccount.BinaryPassword = true;

        if (pop3User) {
          objAccount.Pop3User = pop3User;
        }

        if (password) {
          if (password != "********") {
            objAccount.Password = password;
            objAccount.BinaryPassword = false;
          }
        }

        // Update the account
        return $ss.snapin.email.utility.UpdateAccount(objAccount);
      }
      catch (ex) {
        return false;
      }

      return true;
    },

    ////////////////////////////////////////////////////////////////////////////////
    //                                                                            //
    //  Function : AddAutoRepairSetting                                 //
    //                                                                            //
    //  Access   : Private                                                        //
    //                                                                            //
    //  Arguments: [IN]  objAccount - the account object                          //
    //             [IN]  strSetting - the setting                                 //
    //             [OUT] arrProps   - the property array                          //
    //             [IN]  strValue   - optional value which overrides the template //
    //                                                                            //
    //  Add a repair setting if the setting is incorrect in the account           //
    //                                                                            //
    ////////////////////////////////////////////////////////////////////////////////

    AddAutoRepairSetting: function(objAccount, strSetting, arrProps, strValue) {
      try {
        var objProp;

        if (objAccount.m_objResult[strSetting] == false) {

          objProp = new Object();
          objProp.m_strSetting = strSetting;
          objProp.m_strIncorrect = objAccount[strSetting];

          if (typeof (strValue) == "undefined")
            objProp.m_strCorrect = objAccount.m_objTemplate.GetProperty(strSetting);
          else
            objProp.m_strCorrect = strValue;

          arrProps[arrProps.length] = objProp;
        }
      }
      catch (ex) {
      }
    },

    ////////////////////////////////////////////////////////////////////////////////
    //                                                                            //
    //  Function : AddManualRepairSetting                               //
    //                                                                            //
    //  Access   : Private                                                        //
    //                                                                            //
    //  Arguments: [IN]  strSetting - the setting                                 //
    //             [IN]  strValue   - the value                                   //
    //             [OUT] arrProps   - the property array                          //
    //                                                                            //
    //  Add a repair setting if the setting is incorrect in the account           //
    //                                                                            //
    ////////////////////////////////////////////////////////////////////////////////

    AddManualRepairSetting: function(strSetting, strValue, arrProps) {
      try {
        var objProp;

        objProp = new Object();
        objProp.m_strSetting = strSetting;
        objProp.m_strValue = strValue;

        arrProps[arrProps.length] = objProp;
      }
      catch (ex) {
      }
    },

    ////////////////////////////////////////////////////////////////////////////////
    //                                                                            //
    //  Function : AddManualEmailRepairSetting                          //
    //                                                                            //
    //  Access   : Private                                                        //
    //                                                                            //
    //  Arguments: [IN]  strElement - the HTML element identifier                 //
    //             [IN]  strSetting - the setting                                 //
    //             [OUT] arrProps   - the property array                          //
    //                                                                            //
    //  Add a repair setting if the setting is incorrect in the account           //
    //                                                                            //
    ////////////////////////////////////////////////////////////////////////////////

    AddManualEmailRepairSetting: function(objAccount, strSetting, arrProps) {
      try {
        var strValue;

        if (typeof (objAccount.m_objResult[strSetting]) == "undefined") {

          // Show the element

          this.AddManualRepairSetting(strSetting, "", arrProps);

          // Set the prefix and suffix

          strValue = (objAccount[strSetting].split("@"))[0];
          if (typeof (strValue) == "undefined") strValue = "";
          this.AddManualRepairSetting(strSetting + "Prefix", strValue, arrProps);

          // Set the suffix if one is defined

          strValue = (objAccount[strSetting].split("@"))[1];
          if (typeof (strValue) == "undefined") strValue = "";
          this.AddManualRepairSetting(strSetting + "Suffix", strValue, arrProps);
        }
      }
      catch (ex) {
      }
    }

  });

})();







