SnapinEmailController = SnapinBaseController.extend('snapin_email', {
  emailModel: null,
  toLocation: "",
  imagePath: "",
  layoutImagePath: "",
  accounts: null,
  emailValidateData: null,
  sleepInterval: $ss.agentcore.dal.config.GetConfigValue("snapin_email", "sleep_interval", "3000"),
  manualEntry: {
    email: "",
    pop3User: "",
    password: "********",
    index: 0
  },
  troubeshootData: ""
}, {
  "#snap_email_landing_AddAccount click": function(params){
    this.HandleAddEmailAccountClick(params);
  },
  
  "#snap_email_landing_AccountInfo click": function(params){
    this.HandleShowAccountInfoClick(params);
  },
  
  "#snap_email_landing_FixAccount click": function(params){
    this.HandleTroubleShooterClick(params);
  },
  
  "#snap_email_AccountInfo_back click": function(params){
    this.RenderPage(this.Class.toLocation, "\\views\\home.html");
  },
  
  "#snp_email_troubleshoot_auto_back click": function(params){
    this.HandleTrobleshootBackClick(params);
  },
  
  "#snap_email_AddAccount click": function(params){
    this.HandleAddAccountButtonClick(params);
    params.event.kill();
  },
  
  "#snap_email_AddAccount_Cancel click": function(params){
    this.RenderPage(this.Class.toLocation, "\\views\\home.html");
  },
  
  "#snap_email_add_another_account click": function(params){
    this.HandleAddEmailAccountClick(params);
    params.event.kill();
  },
  
  "#snap_email_email_success_back click": function(params){
    this.RenderPage(this.Class.toLocation, "\\views\\home.html");
    params.event.kill();
  },
  
  ".snapemailtroubleshootmainsettingsright click": function(params){
    try {
      var data = params.element.id.split("_");
      this.HandleExpandCollapse(data[1], "Settings");
    } 
    catch (ex) {
    }
  },
  
  ".snapemailtroubleshootmainconnectivityright click": function(params){
    try {
      var data = params.element.id.split("_");
      this.HandleExpandCollapse(data[1], "Connectivity");
    } 
    catch (ex) {
    }
  },
  
  ".snapemailtroubleshootmainauthright click": function(params){
    try {
      var data = params.element.id.split("_");
      this.HandleExpandCollapse(data[1], "Authentication");
    } 
    catch (ex) {
    }
  },
  
  ".expandcollapse click": function(params){
    this.HandleExpandCollapseAll(params);
  },
  
  ".snpemailrepair click": function(params){
    this.HandleRepairClick(params);
  },
  
  "#snp_email_troubleshoot_auto click": function(params){
    this.HandleAutoNextClick(params);
  },
  
  "#snp_email_modify_user click": function(params){
    this.HandleModifyUserClick(params);
  },
  
  "#snp_email_username_submit click": function(params){
    this.HandleUserNameSaveClick(params);
    params.event.kill();
  },
  
  "#snpemailloginnamechange submit": function(params){
    this.HandleUserNameSaveClick(params);
    params.event.kill();
  },
  
  "#snp_email_auth_finish click": function(params){
    this.HandleAuthenticationFinishClick(params);
  },
  
  "#snp_email_modify_password click": function(params){
    this.HandleModifyPasswordClick(params);
  },
  
  "#snp_email_troubleshoot_password_submit click": function(params){
    this.HandlePasswordSaveClick(params);
    params.event.kill();
  },
  
  "#snpemailusernamechange submit": function(params){
    this.HandlePasswordSaveClick(params);
    params.event.kill();
  },
  
  "#snp_troubleshoot_main_back click": function(params){
    this.RenderPage(this.Class.toLocation, "\\views\\home.html");
    params.event.kill();
  },
  
  "#snp_email_username_cancel click": function(params){
    this.HandleModifyScreenCancelClick(params);
    params.event.kill();
  },
  
  "#snp_email_troubleshoot_password_cancel click": function(params){
    this.HandleModifyScreenCancelClick(params);
    params.event.kill();
  },
  
  "#snp_email_auth_back click": function(params){
    this.HandleTrobleshootBackClick(params);
  },
  
  ".snpemailtroubleshootconnectivitybutton click": function(params){
    this.HandleTrobleshootBackClick(params);
  },
  
  "#email_topbar_main click": function(params){
    this.RenderPage(this.Class.toLocation, "\\views\\home.html");
  },
  
  ///////////////////////////////// 
  
  
  
  
  ".ToggleDisplayResults click": function(params){
    this.HandleToggleDisplayResultsClick(params);
  },
  
  ".RepairButton click": function(params){
    this.HandleRepairClick(params);
  },
  
  "#Next click": function(params){
    this.HandleNextClick(params);
  },
  
  "#Back click": function(params){
    document.getElementById("Next").disable = true;
    
    try {
      var data = {
        'accounts': accounts
      };
      this.RenderPage(this.Class.toLocation, "\\views\\troubleshoot_main_email.html", data);
    } 
    catch (e) {
    }
  },
  
  "#snap_email_landing_FAQ click": function(params){
    this.HandleFAQClick(params);
  },
  
  "#cancel click": function(params){
    this.RenderPage(this.Class.toLocation, "\\views\\home.html");
  },
  
  "#troubeshoot_back_button click": function(params){
    this.RenderPage(this.Class.toLocation, "\\views\\home.html");
  },
  
  
  ///////////////////////////////////////////////
  
  HandleModifyScreenCancelClick: function(params){
    try {
      var data = {
        'email': this.Class.manualEntry.email,
        'pop3User': this.Class.manualEntry.pop3User
      };
      
      this.RenderPage(this.Class.toLocation, "\\views\\troubleshoot_repair_auth.html", data);
    } 
    catch (ex) {
    }
  },
  
  HandlePasswordSaveClick: function(params){
    try {
      //validate the form
      var formData = $("#snpemailusernamechange").formToArray();
      
      var isValid = true;
      //get the array of the values
      if (this.Class.emailValidateData) {
        var validateStatus = $("#snpemailusernamechange").validate(this.Class.emailValidateData);
        
        if (validateStatus) {
          validateStatus.settings.highlight = false;
          validateStatus.settings.errorClass = "myerror";
          
          for (var x = 0; x < formData.length; x++) {
            if (!validateStatus.element("#" + formData[x].name)) {
              isValid = false;
              $("#snpemailusernamechange .error").show();
            }
          }
        }
      }
      
      validateStatus = null;
      if (!isValid) {
        return false;
      }
      
      this.Class.manualEntry.password = formData[1].value;
      var data = {
        'email': this.Class.manualEntry.email,
        'pop3User': this.Class.manualEntry.pop3User
      };
      this.RenderPage(this.Class.toLocation, "\\views\\troubleshoot_repair_auth.html", data);
    } 
    catch (ex) {
    }
    
    return true;
  },
  
  HandleModifyPasswordClick: function(params){
    //initialize the data so that validation can be filled by the view
    this.Class.emailValidateData = null;
    try {
      var data = {
        'email': this.Class.manualEntry.email,
        'pop3User': this.Class.manualEntry.pop3User
      };
      this.RenderPage(this.Class.toLocation, "\\views\\modify_password.html", data);
    } 
    catch (ex) {
    }
  },
  
  HandleTrobleshootBackClick: function(params){
    try {
      this.RenderPage(this.Class.toLocation, "\\views\\troubleshoot_main_email.html", null);
      $("#snapin_email_troubleshoot_main").html(this.Class.troubeshootData);
    } 
    catch (ex) {
    }
  },
  
  HandleAuthenticationFinishClick: function(params){
    try {
      params.element["val"] = this.Class.manualEntry.index;
      if ($ss.snapin.email.repair.RepairAccount(this.Class.accounts[params.element.val], this.Class.manualEntry.pop3User, this.Class.manualEntry.password)) {
        this.RenderPage(this.Class.toLocation, "\\views\\troubleshoot_main_email.html", null);
        $("#snapin_email_troubleshoot_main").html(this.Class.troubeshootData);
        $("#snpemailtroubleshoottable_" + this.Class.manualEntry.index)[0].outerHTML = "";
        
        params["LoadAccount"] = false;
        this.HandleTroubleShooterClick(params);
      }
    } 
    catch (ex) {
    }
  },
  
  HandleUserNameSaveClick: function(params){
    try {
      //validate the form
      var formData = $("#snpemailloginnamechange").formToArray();
      if (!formData) {
        return false;
      }
      
      var isValid = true;
      //get the array of the values
      if (this.Class.emailValidateData) {
      
        var validateStatus = $("#snpemailloginnamechange").validate(this.Class.emailValidateData);
        
        if (validateStatus) {
          validateStatus.settings.highlight = false;
          validateStatus.settings.errorClass = "myerror"
          
          for (var x = 0; x < formData.length; x++) {
            if (!validateStatus.element("#" + formData[x].name)) {
              isValid = false;
              $("#snpemailloginnamechange .error").show();
            }
          }
        }
      }
      
      validateStatus = null;
      if (!isValid) {
        return false;
      }
      
      //this.Class.manualEntry.pop3User = $("#snp_email_username").val();
      this.Class.manualEntry.pop3User = formData[0].value;
      var data = {
        'email': this.Class.manualEntry.email,
        'pop3User': this.Class.manualEntry.pop3User
      };
      this.RenderPage(this.Class.toLocation, "\\views\\troubleshoot_repair_auth.html", data);
    } 
    catch (ex) {
      return false;
    }
    
    return true;
  },
  
  HandleModifyUserClick: function(params){
    this.Class.emailValidateData = null;
    try {
      var data = {
        'email': this.Class.manualEntry.email,
        'pop3User': this.Class.manualEntry.pop3User
      };
      this.RenderPage(this.Class.toLocation, "\\views\\modify_username.html", data);
    } 
    catch (ex) {
    }
  },
  
  HandleAutoNextClick: function(params){
    try {
      if ($ss.snapin.email.repair.RepairAccount(this.Class.accounts[params.element.val], null, null)) {
      
        this.RenderPage(this.Class.toLocation, "\\views\\troubleshoot_main_email.html", null);
        $("#snapin_email_troubleshoot_main").html(this.Class.troubeshootData);
        $("#snpemailtroubleshoottable_" + params.element.val)[0].outerHTML = "";
        $("#snp_troubleshoot_main_back").hide();
        
        params["LoadAccount"] = false;
        this.HandleTroubleShooterClick(params);
      }
    } 
    catch (ex) {
    }
  },
  
  
  HandleRepairClick: function(params){
    if (!params) {
      return false;
    }
    
    try {
      var arrParams = (params.element.id).split("_");
      var arrAutoProps = [];
      var arrManualProps = [];
      var index = arrParams[1];
      $ss.snapin.email.repair.GetDataForRepairAccountScreen(this.Class.accounts[index], arrAutoProps, arrManualProps);
      
      //show the error window 
      if (0 === this.Class.accounts[index].bSettings) {
        var data = {
          'account': this.Class.accounts[index],
          'AutoProps': arrAutoProps,
          'index': index
        };
        
        this.RenderPage(this.Class.toLocation, "\\views\\troubleshoot_repair.html", data);
      }
      else 
        if (false === this.Class.accounts[index].bConnectivity) {
          var data = {
            'email': this.Class.accounts[index].Email,
            'mailclient' : this.Class.accounts[index].MailClient
          };
          
          this.RenderPage(this.Class.toLocation, "\\views\\troubleshoot_connectivity.html", data);
        }
        else 
          if (false === this.Class.accounts[index].bAuthenticated) {
            this.Class.manualEntry.email = this.Class.accounts[index].Email;
            this.Class.manualEntry.pop3User = this.Class.accounts[index].Pop3User;
            this.Class.manualEntry.index = index;
            
            var data = {
              'email': this.Class.accounts[index].Email,
              'pop3User': this.Class.accounts[index].Pop3User,
              'mailclient' : this.Class.accounts[index].MailClient
            };
            
            this.RenderPage(this.Class.toLocation, "\\views\\troubleshoot_repair_auth.html", data);
          }
      
      
    } 
    catch (ex) {
      return false;
    }
    return true;
  },
  
  HandleExpandCollapseAll: function(params){
    try {
      var data = params.element.id.split("_");
      var id = "#" + params.element.id;
      if ("Expand" === $(id)[0].val) {
        $(id)[0].val = "Collapse";
        $(id).html($ss.agentcore.utils.LocalXLate("snapin_email","CollapseAll"));
      }
      else {
        $(id)[0].val = "Expand";
        $(id).html($ss.agentcore.utils.LocalXLate("snapin_email","ExpandAll"));
      }
      
      this.HandleExpandCollapse(data[1], "Settings", $(id)[0].val);
      this.HandleExpandCollapse(data[1], "Connectivity", $(id)[0].val);
      this.HandleExpandCollapse(data[1], "Authentication", $(id)[0].val);
    } 
    catch (ex) {
    }
  },
  
  HandleExpandCollapse: function(iter, status, forceAction){
    var linkId = null;
    var detailsId = null;
    switch (status) {
      case "Settings":
        linkId = "#snapemailtroubleshootmainsettingsright_" + iter;
        detailsId = "#snapemailtroubleshootmainsettings_" + iter;
        break;
      case "Connectivity":
        linkId = "#snapemailtroubleshootmainconnectivityright_" + iter;
        detailsId = "#snapemailtroubleshootmainconnectivity_" + iter;
        break;
      case "Authentication":
        linkId = "#snapemailtroubleshootmainauthright_" + iter;
        detailsId = "#snapemailtroubleshootmainauth_" + iter;
        break;
    }
    
    //in case user clicks on expand all or collapse all then irrespective of the current state, the 
    //controls have to force themselves to expand or collapse
    if (forceAction) {
      if ("Collapse" === forceAction) {
        $(linkId)[0].val = "Expand";
      }
      else {
        $(linkId)[0].val = "Collapse";
      }
    }
    
    //var id = "#snapemailtroubleshootmainsettingsright_" + iter; 
    if ("Expand" === $(linkId)[0].val) {
      $(linkId)[0].val = "Collapse";
      $(linkId)[0].children[0].alt = $ss.agentcore.utils.LocalXLate("snapin_email", "Collapse");
      $(linkId)[0].children[0].src = this.Class.imagePath + "\\btn_collapse.gif";      
      $(detailsId + " .detailes").show();
    }
    else {
      $(linkId)[0].val = "Expand";
      $(linkId)[0].children[0].alt = $ss.agentcore.utils.LocalXLate("snapin_email", "Expand");
      $(linkId)[0].children[0].src = this.Class.imagePath + "\\btn_expand.gif";      
      $(detailsId + " .detailes").hide();
    }
    
    return true;
  },
  
  HandleTroubleShooterClick: function(params){ 
    var that = this;
    var opt = {
      fnPtr: function () {
        return that.HandleTroubleShooterClickEx(params);
      },
      timeout: 100,
      sleepTime: 300
    }

    return $ss.agentcore.utils.UpdateUI(opt);
  },

  HandleTroubleShooterClickEx: function (params) {
    try {
      var templates = null;
      var data = {};
      var loadAccounts = true;
      var processIndex = 0;
      
      //get all the email accounts
      try {
        if (("undefined" != typeof(params.LoadAccount)) && (false == params.LoadAccount)) {
          loadAccounts = false;
          processIndex = params.element.val;
        }
      } 
      catch (ex) {
        return false;
      }
      
      
      if (loadAccounts) {
        this.RenderPage(this.Class.toLocation, "\\views\\troubleshoot_main_email.html", data);
        
        //hide the back button
        $("#snp_troubleshoot_main_back").hide();
        this.Class.accounts = $ss.snapin.email.diagnose.GetAccounts(templates);
      }
      
      this.UpdateBreadCrumb("", "BR:Troubleshoot Email");
      
      if ((!this.Class.accounts) || (0 == this.Class.accounts.length)) {
        //in case there is no accounts display the status to the user						
        //var tempHTML = '<div style="color:#d3edfa">' + $ss.agentcore.utils.LocalXLate("snapin_email","NO_ACC_TROUBLE") + '</div>';
        //$("#snp_email_troubleshoot_main").html(tempHTML);
				//debugger;
				$("#snp_email_troubleshoot_main").addClass("noaccount");
        $("#snp_email_troubleshoot_main").html($ss.agentcore.utils.LocalXLate("snapin_email","NO_ACC_TROUBLE"));
				
        //Update the back button to display it        
        $("#snp_troubleshoot_main_back").show();
      }
      else {
        var accountsInfoArray = [];
        data["accounts"] = accountsInfoArray;
        
        $("#snp_troubleshoot_main_back").hide();
        
        try {
          for (var i = 0; i < this.Class.accounts.length; i++) {
            //for (var i = 0; i < 1; i++) {
            if (!loadAccounts) {
              if (i != processIndex) {
              
                continue;
              }
            }
            this.Class.accounts[i].m_objResult = {};
            var renderAccountsInfo = {};
            accountsInfoArray.push(renderAccountsInfo);
            
            renderAccountsInfo.Result = {};
            
            //Prepair the UI for the first run
            if (!this.PrepairDataForTheFirstRunOfTroubleShoot(renderAccountsInfo.Result, this.Class.accounts[i], i, data)) {
              return false;
            }
            
            //check settings
            if (!this.DiagnoseSettings(renderAccountsInfo.Result, this.Class.accounts[i], i, data)) {
              return false;
            }
            
            //check connectivity
            if (!this.DiagnoseConnectivity(renderAccountsInfo.Result, this.Class.accounts[i], i, data)) {
              return false;
            }
            
            //check authenitication
            if (!this.DiagnoseAuthentication(renderAccountsInfo.Result, this.Class.accounts[i], i, data)) {
              return false;
            }
          }
          
          for (i = 0; i < this.Class.accounts.length; i++) {
            //check whether we need to display the repair button or not
            if ((!this.Class.accounts[i].bSettings) ||
            (!this.Class.accounts[i].bConnectivity) ||
            (!this.Class.accounts[i].bAuthenticated)) {
              this.ShowRepairButton(i);
            }
          }
        } 
        catch (ex) {
        }
        
        //Update the done button to display it        
        $("#snp_troubleshoot_main_back").show();
        
        this.Class.troubeshootData = $("#snapin_email_troubleshoot_main").html();
      }
    }
    catch (e) {
      $("#snp_troubleshoot_main_back").show();
      return false;
    }
    
    return true;
  },
  
  ShowRepairButton: function(counter){
    var id = "#snpemailrepair_" + counter;
    
    $(id)[0].style.display = "";
    
  },
  
  //This function will diagnose the settings and render the page
  DiagnoseSettings: function(result, account, counter, data){
    
    account.bSettings = $ss.snapin.email.diagnose.DiagnoseSettings(account);
    
    var image = "\\ico_error.gif";
    if (account.bSettings) {
      image = "\\ico_correct_big.gif";
    }
    
    result["Settings"] = {
      "LeftImage": this.Class.imagePath + image,
      "RightImage": this.Class.imagePath + "\\btn_expand.gif",
      "DisplayStyle": "display: none;",
      "SettingsInfo": this.PrepairSettingsInfo(account)
    };
    
    
    
    var key = "#snapemailtroubleshootmainsettings_" + counter;
    $(key).html(this.GetSettingHTML(result, account, counter, data, false));
    //this.RenderPage(this.Class.toLocation, "\\views\\troubleshoot_main_email.html", data);  
    
    $ss.agentcore.utils.system.Sleep(this.Class.sleepInterval);
    return true;
  },
  
  //this function will diagnose the connectivity and render the page
  DiagnoseConnectivity: function(result, account, counter, data){
  
    if (1 === $ss.snapin.email.diagnose.DiagnoseConnectivity(account)) {
      account.bConnectivity = true;
    }
    else {
      account.bConnectivity = false;
    }
    
    
    var image = "\\ico_error.gif";
    if (account.bConnectivity) {
      image = "\\ico_correct_big.gif";
    }
    
    result["Connectivity"] = {
      "LeftImage": this.Class.imagePath + image,
      "RightImage": this.Class.imagePath + "\\btn_expand.gif",
      "DisplayStyle": "display: none;",
      "ConnectivityInfo": this.PrepairConnectivityInfo(account)
    };
    
    //this.RenderPage(this.Class.toLocation, "\\views\\troubleshoot_main_email.html", data); 
    var key = "#snapemailtroubleshootmainconnectivity_" + counter;
    $(key).html(this.GetConnectivityHTML(result, account, counter, data, false));
    
    $ss.agentcore.utils.system.Sleep(this.Class.sleepInterval);
    return true;
  },
  
  //this function will diagnose the authentication and render the page
  DiagnoseAuthentication: function(result, account, counter, data){
    
    if (1 === $ss.snapin.email.diagnose.DiagnoseAuthentication(account)) {
      account.bAuthenticated = true;
    }
    else {
      account.bAuthenticated = false;
    }
    
    var image = "\\ico_error.gif";
    if (account.bAuthenticated) {
      image = "\\ico_correct_big.gif";
    }
    
    result["Authentication"] = {
      "LeftImage": this.Class.imagePath + image,
      "RightImage": this.Class.imagePath + "\\btn_expand.gif",
      "DisplayStyle": "display: none;",
      "AuthenticationInfo": this.PrepairAuthenticationInfo(account)
    };
    
    //this.RenderPage(this.Class.toLocation, "\\views\\troubleshoot_main_email.html", data);
    var key = "#snapemailtroubleshootmainauth_" + counter;
    $(key).html(this.GetAuthenticationHTML(result, account, counter, data, false));
    
    $ss.agentcore.utils.system.Sleep(this.Class.sleepInterval);
    return true;
  },
  
  PrepairDataForTheFirstRunOfTroubleShoot: function(result, account, counter, data){
    //email id
    var htmlData = null;
    result["Email"] = account.Email;
    
    result["ExpandCollapse"] = {
      "Val": "Expand",
      "Text": "Expand All"
    };
    
    
    //Settings info    
    result["Settings"] = {
      "LeftImage": this.Class.imagePath + "\\loader.gif",
      "RightImage": this.Class.imagePath + "\\btn_expand.gif",
      "DisplayStyle": "display: none;",
      "SettingsInfo": this.PrepairSettingsInfo()
    };
    
    //connectivity info
    result["Connectivity"] = {
      "LeftImage": this.Class.imagePath + "\\loader.gif",
      "RightImage": this.Class.imagePath + "\\btn_expand.gif",
      "DisplayStyle": "display: none;",
      "ConnectivityInfo": this.PrepairConnectivityInfo()
    };
    
    //authentication info
    result["Authentication"] = {
      "LeftImage": this.Class.imagePath + "\\loader.gif",
      "RightImage": this.Class.imagePath + "\\btn_expand.gif",
      "DisplayStyle": "display: none;",
      "AuthenticationInfo": this.PrepairAuthenticationInfo()
    };
    
    htmlData = '<table cellpadding="0" cellspacing="0" border="0" width="96%" id="snpemailtroubleshoottable_' + counter + '">';
    htmlData += this.GetTableHeaderHTML(result, account, counter, data);
    htmlData += this.GetSettingHTML(result, account, counter, data, true);
    htmlData += this.GetConnectivityHTML(result, account, counter, data, true);
    htmlData += this.GetAuthenticationHTML(result, account, counter, data, true);
    htmlData += this.GetRepairButtonHTML(result, account, counter, data);
    htmlData += '</table>';
    
    var presentData = $("#snp_email_troubleshoot_main").html();
    
    
    $("#snp_email_troubleshoot_main").html(htmlData + "<br>" + presentData);
    $ss.agentcore.utils.system.Sleep(this.Class.sleepInterval);
    
    return true;
  },
  
  //this function wil prepair the table header
  GetTableHeaderHTML: function(result, account, counter, data){
    var htmlData = null;
    
    htmlData = '<tr id="snapemailtroubleshootmainhead_' + counter + '">';
    htmlData += '<th>';
    htmlData += '<div class="emailid">'
    htmlData += result["Email"] + ' (' + account.MailClient + ')';
    htmlData += '</div>';
    htmlData += '<a href="#" class="expandcollapse" id="expandcollapse_' + counter + '" val="' + result.ExpandCollapse.Val + '">' + result.ExpandCollapse.Text + '</a>';
    htmlData += '</th>';
    htmlData += '</tr>';
    
    return htmlData;
  },
  
  //this function will prepair the html for settings
  GetHTML: function(result, account, counter, data, bFirstTime, caller){
    var htmlData = "";
    
    var header = "";
    var functionCaller = "";
    var oResult = null;
    var oInfo = null;
    
    switch (caller) {
      case "Settings":
        header = $ss.agentcore.utils.LocalXLate("snapin_email", "TS:Settings");
        functionCaller = "settings";
        oResult = result.Settings;
        oInfo = result.Settings.SettingsInfo;
        break;
      case "Connectivity":
        header = $ss.agentcore.utils.LocalXLate("snapin_email", "TRC:Connectivity");
        functionCaller = "connectivity";
        oResult = result.Connectivity;
        oInfo = result.Connectivity.ConnectivityInfo;
        break;
      case "Authentication":
        header = $ss.agentcore.utils.LocalXLate("snapin_email", "TSR:Authentication");
        functionCaller = "auth";
        oResult = result.Authentication;
        oInfo = result.Authentication.AuthenticationInfo;
        break;
    }
    
    
    if (bFirstTime) {
      htmlData = '<tr id="snapemailtroubleshootmain' + functionCaller + '_' + counter + '">';
    }
    
    htmlData += '<td class="row3">';
    //head
    htmlData += '<div class="headproblem">';
    htmlData += '<div class="headname">';
    htmlData += '<img src="' + oResult.LeftImage + '"' + 'width="16" height="16" border="0" align="absmiddle" />&nbsp;&nbsp;' + header;
    //htmlData += '<img src="' + oResult.LeftImage + '"' + 'border="0" align="absmiddle" />&nbsp;&nbsp;' + header;
    htmlData += '</div>';
    htmlData += '<a href="#" val="Expand" class="collapse snapemailtroubleshootmain' + functionCaller + 'right" id="snapemailtroubleshootmain' + functionCaller + 'right_' + counter + '">';
    htmlData += '<img src="' + oResult.RightImage + '" width="14" height="14" alt="' + $ss.agentcore.utils.LocalXLate("snapin_email", "Expand") + '" border="0" />';
    htmlData += '</a>';
    htmlData += '</div>';
    
    //details
    htmlData += '<div class="detailes" style="' + oResult.DisplayStyle + '">';
    for (var i = 0; i < oInfo.length; i++) {
      htmlData += '<div class="' + oInfo[i].Class + '">';
      htmlData += oInfo[i].Data;
      htmlData += '</div>';
    }
    htmlData += '</div>';
    
    htmlData += '</td>';
    
    if (bFirstTime) {
      htmlData += '</tr>';
    }
    
    
    return htmlData;
  },
  
  //this function will prepair the html for settings
  GetSettingHTML: function(result, account, counter, data, bFirstTime){
    return this.GetHTML(result, account, counter, data, bFirstTime, "Settings");
  },
  
  //this function will prepair the html for connectivity
  GetConnectivityHTML: function(result, account, counter, data, bFirstTime){
    return this.GetHTML(result, account, counter, data, bFirstTime, "Connectivity");
  },
  
  //this function will prepair the html for authenication
  GetAuthenticationHTML: function(result, account, counter, data, bFirstTime){
    return this.GetHTML(result, account, counter, data, bFirstTime, "Authentication");
  },
  
  GetRepairButtonHTML: function(result, account, counter, data){
    var htmlData = null;
    htmlData = '<tr>';
    htmlData += '<td class="row4">';
    htmlData += '<a href="#" style="display: none" class="snpemailrepair" val="' + counter + '"' + ' id="snpemailrepair_' + counter + '"' + '><img src="' + this.Class.layoutImagePath + '\\btn_repair.gif"' + ' width="77" height="20" alt="' + $ss.agentcore.utils.LocalXLate("snapin_email", "Repair") + '" border="0"/></a>';
    htmlData += '</td>'
    htmlData += '</tr>';
    
    return htmlData;
  },
  
  
  
  PrepairSettingsInfo: function(account){
    try {
      var settingsInfo = [];
      var className = null;
      var data = null;
      
      if (account) {
        for (var info in account.Settings) {
          if (account.m_objResult[info]) {
            className = "noproblem";
          }
          else {
            className = "problem";
          }
          
          data = account.Settings[info];
          settingsInfo.push({
            "Class": className,
            "Data": data
          });
        }
      }
      else {
        settingsInfo.push({
          "Class": "noproblem",
          "Data": "emailaddress"
        });
        settingsInfo.push({
          "Class": "noproblem",
          "Data": "SmtpServer"
        });
        settingsInfo.push({
          "Class": "noproblem",
          "Data": "SmtpPort"
        });
        settingsInfo.push({
          "Class": "noproblem",
          "Data": "SmtpSSL"
        });
        settingsInfo.push({
          "Class": "noproblem",
          "Data": "SmtpAuth"
        });
        settingsInfo.push({
          "Class": "noproblem",
          "Data": "Pop3Server"
        });
        settingsInfo.push({
          "Class": "noproblem",
          "Data": "Pop3Port"
        });
        settingsInfo.push({
          "Class": "noproblem",
          "Data": "Pop3SSL"
        });
        settingsInfo.push({
          "Class": "noproblem",
          "Data": "Pop3Auth"
        });
      }
    } 
    catch (ex) {
      settingsInfo = null;
    }
    
    return settingsInfo;
  },
  
  PrepairConnectivityInfo: function(account){
    try {
      var connectivityInfo = [];
      var className = null;
      var data = null;
      
      if (account) {
        for (var info in account.Connectivity) {
          if (account.m_objResult[info + "Connectivity"]) {
            className = "noproblem";
          }
          else {
            className = "problem";
          }
          
          data = account.Connectivity[info];
          connectivityInfo.push({
            "Class": className,
            "Data": data
          });
        }
      }
      else {
        connectivityInfo.push({
          "Class": "noproblem",
          "Data": "SmtpServer"
        });
        connectivityInfo.push({
          "Class": "noproblem",
          "Data": "Pop3Server"
        });
      }
    } 
    catch (ex) {
      connectivityInfo = null;
    }
    
    return connectivityInfo;
  },
  
  PrepairAuthenticationInfo: function(account){
    try {
      var authenticationInfo = [];
      var className = null;
      var data = null;
      
      if (account) {
        for (var info in account.Authentication) {
          if (account.m_objResult[info + "Test"]) {
            className = "noproblem";
          }
          else {
            className = "problem";
          }
          
          data = account.Authentication[info];
          authenticationInfo.push({
            "Class": className,
            "Data": data
          });
        }
      }
      else {
        authenticationInfo.push({
          "Class": "noproblem",
          "Data": "Pop3Auth"
        });
      }
    } 
    catch (ex) {
      authenticationInfo = null;
    }
    
    return authenticationInfo;
  },
  
  
  SetButtonStatusForTroubleShoot: function(state){
    $("#snap_email_landing_FAQ").disabled = state;
    $("#snap_email_landing_AccountInfo").disabled = state;
    $("#snap_email_landing_AddAccount").disabled = state;
  },
  
  HandleAddAccountButtonClick: function(params){
	
    try {
      var oA = this.GetEmailObject();
      
      if (!this.Class.emailModel.UpdateEmailFields(oA)) {       
        return false;
      }
      
      if (EM_RET_OK != $ss.snapin.email.utility.AddEmailAccount(oA, oA.EmailClient)) {
        $(".error").html($ss.agentcore.utils.LocalXLate("snapin_email","INTERNAL_ERROR"));
        
        return false;
      }
      
      this.RenderPage(this.Class.toLocation, "\\views\\add_email_account_success.html");
    } 
    catch (Ex) {
    }
  },
  
  GetEmailObject: function(){
    try {
      var displaynamePattern = null;
      var emailaddressPattern = null;
      var usernamePattern = null;
      
      //get the elements in the ui
      var formData = $("#snp_email_add_form").formToArray();
      if (!formData) {
        return false;
      }
      
      var oRegex = this.Class.emailModel.GetRegexForElements(formData[3]["value"]);
      if (!oRegex) {
        return false;
      }
      
      displaynamePattern = eval(oRegex.DisplayName);
      emailaddressPattern = eval(oRegex.Email);
      usernamePattern = eval(oRegex.UserName);
      
      var isValid = true;
      //get the array of the values
      if (this.Class.emailValidateData) {
        //display name
        $.validator.addMethod("valdisp", function(value, element){
          if (!displaynamePattern.test(value)) {
            return false;
          }
          return true;
        }, "Please enter a valid display name");
        
        //email
        $.validator.addMethod("valemail", function(value, element){
          if (!emailaddressPattern.test(value)) {
            return false;
          }
          return true;
        }, "Please enter a valid email address");
        
        //username
        $.validator.addMethod("valusername", function(value, element){
          if (!usernamePattern.test(value)) {
            return false;
          }
          return true;
        }, "Please enter a valid user name");
        
        
        var validateStatus = $("#snp_email_add_form").validate(this.Class.emailValidateData);
        
        if (validateStatus) {
          validateStatus.settings.highlight = false;
          validateStatus.settings.errorClass = "myerror"
          
          for (var x = 0; x < formData.length; x++) {
            if (!validateStatus.element("#" + formData[x].name)) {
              isValid = false;
              //$("#snp_email_add_form .error").css({display: "block"});
              $("#snp_email_add_form .error").show();
              $("#snp_email_add_form .snp_addemail_form_area").scrollTo(50);
            }
          }
        }
      }
      
      validateStatus = null;
      if (!isValid) {
        return false;
      }
      
      var oA = {
        "EmailClient": formData[0]["value"],
        "sDisplayName": formData[1]["value"],
        "sAccountName": formData[2]["value"],
        "sEmailAccount": formData[2]["value"],
        "TemplateName": formData[3]["value"],
        "sUserName": formData[4]["value"],
        "sPassword": formData[5]["value"],
        "sIsDefault": formData[6]["value"]
      };
      return oA;
    } 
    catch (ex) {
      return null;
    }
  },
  
  HandleAddEmailAccountClick: function(params){

    try {  
      //load the data from the config file into model
      if (!this.Class.emailModel.GetDataForEmailWindow()) {
        return false;
      }
      
      //initialize the data so that validation can be filled by the view
      this.Class.emailValidateData = null;
      
      //get the data to be rendered
      var data = {
        'client': this.Class.emailModel.GetClientList(),
        'domain': this.Class.emailModel.GetDomainList()
      };
      if(data['client'].length == 0)
        this.RenderPage(this.Class.toLocation, "\\views\\add_email_no_default_client.html", null);
      else
      {
        this.RenderPage(this.Class.toLocation, "\\views\\add_email_account.html", data);
        $("#emailclient").focus();
      }
      this.UpdateBreadCrumb("", "Add Email");
    } 
    catch (ex) {
    }
    
    return true;
  },
  
  UpdateBreadCrumb: function(url, text){
    if (NavigationController.UpdateBreadCrumbHTMLElement) {
      var loc_text = $ss.agentcore.utils.LocalXLate("snapin_email", text)
      var breadCrumbHTML = "";
      if(!text) {
        breadCrumbHTML = NavigationController.UpdateBreadCrumbHTMLElement();
      } else {
        breadCrumbHTML = NavigationController.UpdateBreadCrumbHTMLElement({
        url: url,
        text: loc_text
      });
      }
      
      if (breadCrumbHTML) {
        $("#breadcrumb").html(breadCrumbHTML);
      }
    }
  },
  
  HandleShowAccountInfoClick: function(params){
    try {
      var infoColl = this.Class.emailModel.GetAccountInfoColl();
      if (!infoColl) {
        return false;
      }
      
      //get the data that needs to be rendered.
      var data = {
        'account_info_coll': infoColl
      };
      
      this.RenderPage(this.Class.toLocation, "\\views\\display_email_accounts.html", data);
      this.UpdateBreadCrumb("", "View Email settings");
    } 
    catch (ex) {
    }
  },
  
  index: function(params){
    try {
      if (!params) {
        return false;
      }
      var page = "\\views\\home.html";
      
      //create the model for the email
      if (!this.Class.emailModel) {
        this.Class.emailModel = new $ss.snapin.email.model.Email();
        if (!this.Class.emailModel) {
          return false;
        }
      }
      
      //create the parameters that are required to render the page
      this.Class.toLocation = params.toLocation;
      this.Class.imagePath = $ss.getSnapinAbsolutePath("snapin_email") + $ss.agentcore.dal.config.ExpandAllMacros("\\skins\\%SKINNAME%") + "\\images";
      this.Class.layoutImagePath = $ss.getLayoutAbsolutePath() + $ss.agentcore.dal.config.ExpandAllMacros("\\skins\\%SKINNAME%") + "\\buttons\\" + $ss.agentcore.dal.config.ExpandAllMacros("%LANGCODE%");
      
      //This page can be passed as an parameter, so process it
      if ((params.requestArgs) && (params.requestArgs.type)) {
        switch (params.requestArgs.type) {
          case "troubleshoot":
				var that = this;
				that.params = params;
				setTimeout( function() {
								that.HandleTroubleShooterClick(that.params);
							},100);
				return;
			case "create":
            this.HandleAddEmailAccountClick(params);
				return;
			case "display":
            this.HandleShowAccountInfoClick(params);
				return;
			default:
            page = "\\views\\home.html";
            break;
        }
      }
      
      this.RenderPage(this.Class.toLocation, page);
    } 
    catch (ex) {
    }
  },
  
  RenderPage: function(toLocation, page, data){
    try {
      this.renderSnapinView("snapin_email", toLocation, page, data);
      $ss.agentcore.utils.ui.FixPng();
    } 
    catch (ex) {
    }
  }
});

