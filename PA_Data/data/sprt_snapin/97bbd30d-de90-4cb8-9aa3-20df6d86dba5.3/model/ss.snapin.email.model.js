/**
 * @author Dinesh Venugopalan
 */
$ss.snapin = $ss.snapin ||
{};
$ss.snapin.email = $ss.snapin.email ||
{};
$ss.snapin.email.model = $ss.snapin.email.model ||
{};

$ss.snapin.email.model.EmailFieldInfo = Class.extend({

  init: function(){
    this._template_name = "";
    this._smtp_server = "";
    this._smtp_port = "";
    this._smtp_ssl = "";
    this._smtp_authentication = "";
    this._pop_server = "";
    this._pop3_port = "";
    this._pop3_ssl = "";
    this._pop3_authentication = "";
    this._smtp_auth_method = "";
    this._email_client = "";
    this._domain = "";
    this._displayNameRegEx = "";
    this._userNameRegEx = "";
    this._emailRegEx = "";
    
    this._logger = $ss.agentcore.log.GetDefaultLogger("$ss.snapin.email.model.EmailFieldInfo");
    this._exception = $ss.agentcore.exceptions;
  },
  
  GetTemplateName: function(){
    return this._template_name;
  },
  
  GetSmtp_Server: function(){
    return this._smtp_server;
  },
  
  GetSmtp_Port: function(){
    return this._smtp_port;
  },
  
  GetSmtp_SSL: function(){
    return this._smtp_ssl;
  },
  
  GetSmtp_Authentication: function(){
    return this._smtp_authentication;
  },
  
  GetPop3_Server: function(){
    return this._pop_server;
  },
  
  GetPop3_Port: function(){
    return this._pop3_port;
  },
  
  GetPop3_SSL: function(){
    return this._pop3_ssl;
  },
  
  GetPop3_Authentication: function(){
    return this._pop3_authentication;
  },
  
  GetSmtp_Authentication_Method: function(){
    this._smtp_auth_method = "";
  },
  
  GetDomain: function(){
    return this._domain;
  },
  
  GetDisplayNameRegEx: function(){
    return this._displayNameRegEx;
  },
  
  GetUserNameRegEx: function(){
    return this._userNameRegEx;
  },
  
  GetEmailRegEx: function(){
    return this._emailRegEx;
  },
  
  LoadInfo: function(template){
    if (!template) {
      return false;
    }
    
    try {
      this._template_name = template;
      this._smtp_server = $ss.agentcore.dal.config.GetValues(template, "SmtpServer", "Test")[0];
      this._smtp_port = $ss.agentcore.dal.config.GetValues(template, "SmtpPort", "Test")[0];
      this._smtp_ssl = $ss.agentcore.dal.config.GetValues(template, "SmtpSSL", "Test")[0];
      this._smtp_authentication = $ss.agentcore.dal.config.GetValues(template, "SmtpAuth", "Test")[0];
      this._smtp_auth_method = $ss.agentcore.dal.config.GetValues(template, "SmtpAuthMethod", "Test")[0];
      this._pop_server = $ss.agentcore.dal.config.GetValues(template, "Pop3Server", "Test")[0];
      this._pop3_port = $ss.agentcore.dal.config.GetValues(template, "Pop3Port", "Test")[0];
      this._pop3_ssl = $ss.agentcore.dal.config.GetValues(template, "Pop3SSL", "Test")[0];
      this._pop3_authentication = $ss.agentcore.dal.config.GetValues(template, "Pop3Auth", "Test")[0];
      this._domain = $ss.agentcore.dal.config.GetValues(template, "Domain", "Test")[0];
      this._displayNameRegEx = $ss.agentcore.dal.config.GetValues(template, "DisplayNameRE", "Test")[0];
      this._userNameRegEx = $ss.agentcore.dal.config.GetValues(template, "UserNameRE", "Test")[0];
      this._emailRegEx = $ss.agentcore.dal.config.GetValues(template, "EmailRE", "Test")[0];

      this._smtp_port = this._smtp_port||(this._smtp_ssl === "true"? 465:25);
      this._pop3_port = this._pop3_port||(this._pop3_ssl === "true"? 995:110);
      
    } 
    catch (ex) {
      this._exception.HandleException(ex, '$$ss.snapin.email.model', 'LoadAccountTemplates', arguments);
      templateNodes = null;
    }
    return true;
  }
});




$ss.snapin.email.model.Email = MVC.Model.extend({
  init: function(){
    try {
      this._emailFieldColl = [];
      this._emailClient = [];
      this._defaultEmailClient = "";
      
      this._logger = $ss.agentcore.log.GetDefaultLogger("$ss.snapin.email.model.Email");
      this._exception = $ss.agentcore.exceptions;
    } 
    catch (e) {
      throw {
        name: 'TypeError',
        message: _const.PR_CONSTRUCTOR_FAILED
      }
    }
  },
  
  GetDataForEmailWindow: function(){
    if ((this._emailClient) && (0 < this._emailClient.length)) {
      return true;
    }       
    
    this._defaultEmailClient = $ss.snapin.email.utility.GetDefaultEmailClient();
    
    if (!this.GetEmailClientList()) {
      return false;
    }
    
    //load the data from the config file
    if (this.LoadDatafromConfigFile()) {
      return false;
    }
    
    return true;
  },
  
  GetEmailClientList: function(){
    var isdefault = null;
    
    if ($ss.snapin.email.utility.IsOutlookInstalled()) {
      if (EM_MS_OUTLOOK == this._defaultEmailClient) {
        isdefault = EM_YES;
      }
      else {
        isdefault = EM_NO;
      }
      
      var emailClient = {};
      emailClient["Name"] = EM_MS_OUTLOOK;
      emailClient["isDefault"] = isdefault;
      emailClient["Display"] = EM_MS_OUTLOOK;
      this._emailClient.push(emailClient);
      //this._emailClient.push(EM_MS_OUTLOOK + EM_CLIENT_DELIMETER + isdefault);
    }
    
    if ($ss.snapin.email.utility.IsOutlookExpressInstalled()) {
      if (EM_OUTLOOK_EXPRESS == this._defaultEmailClient) {
        isdefault = EM_YES;
      }
      else {
        isdefault = EM_NO;
      }
      
      var emailClient = {};
      emailClient["Name"] = EM_OUTLOOK_EXPRESS;
      emailClient["isDefault"] = isdefault;
      emailClient["Display"] = EM_OUTLOOK_EXPRESS;
      this._emailClient.push(emailClient);
      //this._emailClient.push(EM_OUTLOOK_EXPRESS + EM_CLIENT_DELIMETER + isdefault);
    }
    
    if ($ss.snapin.email.utility.IsWindowsMailInstalled()) {    		
      if (EM_WINDOWS_MAIL == this._defaultEmailClient) {
        isdefault = EM_YES;
      }
      else {
        isdefault = EM_NO;
      }
      
      var emailClient = {};
      emailClient["Name"] = EM_WINDOWS_MAIL;
      emailClient["IsDefault"] = isdefault;
      emailClient["Display"] = EM_WINDOWS_MAIL;
      this._emailClient.push(emailClient);
      //this._emailClient.push(EM_WINDOWS_MAIL + EM_CLIENT_DELIMETER + isdefault);
    }
    
    if($ss.snapin.email.utility.IsWinLiveMailInstalled())
    {
      if (EM_WINDOWS_LIVE_MAIL == this._defaultEmailClient) {
        isdefault = EM_YES;
      }
      else {
        isdefault = EM_NO;
      }      
      var emailClient = {};
      emailClient["Name"] = EM_WINDOWS_LIVE_MAIL;
      emailClient["IsDefault"] = isdefault;
      emailClient["Display"] = EM_WINDOWS_LIVE_MAIL;
      this._emailClient.push(emailClient);
      
    }
    
    return true;
  },
  
  LoadDatafromConfigFile: function(){
    var accountTemplates = this.LoadAccountTemplates()
    if (!accountTemplates) {
      return false;
    }
    
    if (!this.LoadInfoForAccountTemplates(accountTemplates)) {
      return false;
    }
  },
  
  LoadAccountTemplates: function(){
    var templateNodes = null;
    
    try {
      templateNodes = $ss.agentcore.dal.config.GetValues("email", "account_templates", "Test")
    } 
    catch (ex) {
      this._exception.HandleException(ex, '$$ss.snapin.email.model', 'LoadAccountTemplates', arguments);
      templateNodes = null;
    }
    
    return templateNodes;
  },
  
  LoadInfoForAccountTemplates: function(templateNodes){
    if (!templateNodes) {
      return false;
    }
    
    //iterate and for each template query for the node
    for (var iter = 0; iter < templateNodes.length; iter++) {
      if (!this.LoadDataForThisTemplate(templateNodes[iter])) {
        return false;
      }
    }
    
    return true;
  },
  
  LoadDataForThisTemplate: function(template){
    if (!template) {
      return false;
    }
    
    var emailFieldInfo = new $ss.snapin.email.model.EmailFieldInfo();
    if (!emailFieldInfo) {
      return false;
    }
    
    if (emailFieldInfo.LoadInfo(template)) {
      this._emailFieldColl.push(emailFieldInfo);
    }
    
    return true;
  },
  
  GetClientList: function(){
    return this._emailClient
  },
  
  
  GetDomainList: function(){
    var domainList = null;
    var emailFieldInfo;
    
    
    try {
      domainList = [];
      for (var iter = 0; iter < this._emailFieldColl.length; iter++) {
        emailFieldInfo = this._emailFieldColl[iter];
        
        var domain = {};
        domain["Template"] = emailFieldInfo.GetTemplateName();
        domain["DomainName"] = emailFieldInfo.GetDomain();
        domainList.push(domain);
        
        //domainList.push(emailFieldInfo.GetTemplateName() + EM_DOMAIN_DELIMETER + emailFieldInfo.GetDomain());
      }
    } 
    catch (ex) {
      this._exception.HandleException(ex, '$$ss.snapin.email.model', 'GetDomainList', arguments);
      domainList = null;
    }
    
    return domainList;
  },
  
	/*
	 * Update the email objects with the data present in the model
	 */
  UpdateEmailFields: function(emailFields){
    if (!emailFields) {
      return false;
    }
    
		//fill the information to the object based on the template
    if (!this.FillInfoForTemplate(emailFields)) {
      return false;
    }
    
		//add domain name
    emailFields.sEmailAddress = emailFields.sEmailAccount + "@" + emailFields.Domain;
    //emailFields.sUserName = emailFields.sUserName + "@" + emailFields.Domain;
    //emailFields.sUserName = emailFields.sUserName;
    
    return true;
  },
  
	/*
	 * Fill all the information for this template
	 */
  FillInfoForTemplate: function(emailFields){
    if (!emailFields) {
      return false;
    }
    
		try {
			//iterate the collection, match the template name and fill the information
			for (var iter = 0; iter < this._emailFieldColl.length; iter++) {
				if (emailFields.TemplateName == this._emailFieldColl[iter].GetTemplateName()) {
					emailFields.Domain = this._emailFieldColl[iter].GetDomain();
					
					emailFields.sSMTPServer = this._emailFieldColl[iter].GetSmtp_Server();
					emailFields.sSMTPPort = this._emailFieldColl[iter].GetSmtp_Port();
					emailFields.sSMTPSSL = this._emailFieldColl[iter].GetSmtp_SSL();
					emailFields.sSMTPAuth = this._emailFieldColl[iter].GetSmtp_Authentication();
					
					emailFields.sPOPServer = this._emailFieldColl[iter].GetPop3_Server();
					emailFields.sPOPPort = this._emailFieldColl[iter].GetPop3_Port();
					emailFields.sPOPSSL = this._emailFieldColl[iter].GetPop3_SSL();
					emailFields.sPOPAuth = this._emailFieldColl[iter].GetPop3_Authentication();
					
					return true;
				}
			}
		}
		catch(ex){
			return false;
		}
    
    return false;
  },
  
	/*
	 * This function will return the information required for rendering display email
	 */
  GetAccountInfoColl: function(){
		
		//get all the accounst valid for this system
    var accounts = $ss.snapin.email.utility.GetAllEmailAccounts(true);
    if (!accounts) {
      return null;
    }
    
		//load the required data
    for (var i = 0; i < accounts.length; i++) {
      if (ssCOutlook.ACCT_TYPE_EXCHANGE == accounts[i].Type) {
        accounts[i]["UserName"] = (accounts[i]["ExchangeUser"] == "undefined") ? "" : accounts[i]["ExchangeUser"];
        accounts[i]["Incoming_Server"] = (accounts[i]["ExchangeServer"] == "undefined") ? "" : accounts[i]["ExchangeServer"];
        accounts[i]["Outgoing_Server"] = (accounts[i]["ExchangeServer"] == "undefined") ? "" : accounts[i]["ExchangeServer"];
      }
      else {
        accounts[i]["UserName"] = (accounts[i]["Pop3User"] == "undefined") ? "" : accounts[i]["Pop3User"];
        accounts[i]["Incoming_Server"] = (accounts[i]["Pop3Server"] == "undefined") ? "" : accounts[i]["Pop3Server"];
        accounts[i]["Outgoing_Server"] = (accounts[i]["SmtpServer"] == "undefined") ? "" : accounts[i]["SmtpServer"];
      }
    }
    
    return accounts;
  },
	
	/*
	 * This function will return the regex for the current template
	 */
	GetRegexForElements: function(templateName){
		if( !templateName ){ return null }	
		try{
			//iterate the collection, match the template name and fill the information
	    for (var iter = 0; iter < this._emailFieldColl.length; iter++) {      
	      
	      if (templateName == this._emailFieldColl[iter].GetTemplateName()) {
	        var regExForElemnts = {
						"DisplayName" : this._emailFieldColl[iter].GetDisplayNameRegEx(),
						"UserName" 		: this._emailFieldColl[iter].GetUserNameRegEx(),
						"Email" 			: this._emailFieldColl[iter].GetEmailRegEx()
					};
	        
	        return regExForElemnts;
	      }
	    }
		}
		catch(ex){
			return null;
		}
		
		
		return null;
	}
  
  
});

