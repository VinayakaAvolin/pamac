﻿//<dependsOn>  
//  <script type="text/javascript" src="..\JSUnit\app\jsUnitCore.js"></script>
//  <script type="text/javascript" src="..\jquery.js"></script>
//  <script type="text/javascript" src="..\ss.agentcore.ns.js"></script>
//  <script type="text/javascript" src="..\ss.agentcore.constants.js"></script>
//  <script type="text/javascript" src="..\ss.agentcore.utils.string.js"></script>
//  <script type="text/javascript" src="..\log4js-mod.js"></script>
//  <script type="text/javascript" src="..\ss.agentcore.log.js"></script>
//  <script type="text/javascript" src="..\ss.agentcore.exceptions.js"></script>
//  <script type="text/javascript" src="..\ss.agentcore.utils.activex.js"></script>
//  <script type="text/javascript" src="..\ss.agentcore.dal.ini.js"></script>
//</dependsOn>


/** @namespace Holds all ini related functionality*/
$ss.agentcore.vacode = $ss.agentcore.vacode || {};

//------------------------
//  functions to generate Voice Assist code
//-----------------------
/* 
  Configuration for generating the SmartIssueCode
  Length of the SmartIssueCode = 12
  
  Position 1 -  Major Version number
  Position 2 -  Minor Version number
  Position 3 -  OS (5 - XP, 4 - 2000, etc.)
  Position 4 -  CPU Speed
  Position 5 -  memory  
  Position 6 -  number of processes running
  Position 7 -  modem code
  Position 8 -  Number of NIC Adapters Present (0 to 8)
  Position 9 -  Default Mail Client (1 - Outlook, 2 - OE, etc.)
  Position 10 - e-mail diagnostic
  Position 11 - network checks (tcp/ip; cable, loopback)
  Position 12 - DHCP check
  Postion  13 - RFFU
  Positon  14 - DNS and web
  Position 15 - Default browser
  Position 16 - Parity Checking Digit (Used to detect any errors in transmitting the SmartIssueCode)
*/



(function () {
  $.extend($ss.agentcore.vacode,
  {

    //----------------------------------------------------------------------
    //--  getSICode()
    //--  Main function to generate VoiceAssist code.
    //--  Returns:  VoiceAssist Code as string
    //----------------------------------------------------------------------
        GetVACode: function (xmlFile, issueID) {
      var aVoiceAssist = [];
            if (xmlFile == null) {
        xmlFile = _config.GetConfigValue("voiceassist", "xml");
      }
      var vaCodeXML = _config.ParseMacros(xmlFile, false);  
      var vaCodeDOM = _xml.LoadXML(vaCodeXML);
      
            if (vaCodeDOM == null) {
                return ("-1");
      }
      
            try {
        _GetNetworkObject().Refresh();
      }
            catch (oErr) {
        //g_Logger.Debug("getSICode()", "Couldn't successfully instanitate a new Network object.");
        return;  
      }
     
      var major = vaCodeDOM.selectSingleNode("//version/major");
            if (major == null) {
        //g_Logger.Debug("getSICode()", "Major version doesn't exist in VA code xml definition.");
                return ("-1");
      }
      
      var minor = vaCodeDOM.selectSingleNode("//version/minor");
            if (minor == null) {
        //g_Logger.Debug("getSICode()", "Minor version doesn't exist in VA code xml definition.");
                return ("-1");
      }
      
      aVoiceAssist.push(major.text);         //  push major code
      aVoiceAssist.push(minor.text);         //  push minor code
       
      var bits = vaCodeDOM.selectNodes("//bits/bit");
            if (bits.length == 0) {
        //g_Logger.Debug("getSICode()", "VA code xml definition is improperly formatted - can't find bit entries.");
                return ("-1");
      }
     
       //--- Generate each VoiceAssist code in turn  ------------------------------ 
            for (var i = 0; i < bits.length; i++) {
                var bit = vaCodeDOM.selectSingleNode('//bits/bit[@number="' + (i + 1) + '"]');
                if (bit == null) {
          //g_Logger.Debug("getSICode()", "Bit #: " + i + " doesn't exist in VA code xml definition.");
                    return ("-1");
        }
                if (typeof bit.text == "undefined") {
          //g_Logger.Debug("getSICode()", "Bit #: " + i + " doesn't exist in VA code xml definition.");
                    return ("-1");
        }
        _issueID = issueID
        aVoiceAssist.push(_utils.GetHandler("$ss.agentcore.vacode." + bit.text)());
      }     
      
      aVoiceAssist.push(_AddParityToVACode(aVoiceAssist));
          
      return _FormatVACode(aVoiceAssist);
    },
    
    
    //--------------------------------------------------------------------
    //-  ss_va_DefBrowser() -  Return default browser code
    //--------------------------------------------------------------------
    GetDefaultBrowserCode:function()
    {
      //GS Customization: To get the Default browser version
      var defBrowser = this.GetDefaultBrowser();//_SI.GetIssueData(_issueID,'BrowserInfo','BrowserInfo','DefaultBrowser');
            var defBrowserVer;
      var code = 0; 
      
            // Value 4 & 5 are currently unused, use those values to enable support for new Browsers/Versions
            // Add corresponding entries in the golden database in sprt_registry and sprt_si_thresholds table for dataset_vacommon.dat
            // Removed code w.r.t IE 5.5 and 6.0
      switch(defBrowser.toLowerCase()) 
      {
       case "iexplore":
                    defBrowserVer = _SI.GetIssueData(_issueID, 'BrowserInfo', 'BrowserInfo', 'IEVersion');
                    if (defBrowserVer.indexOf(".") == 2)
                        defBrowserVer = defBrowserVer.substring(0, 4);
		else
                        defBrowserVer = defBrowserVer.substring(0, 3);
                    switch (defBrowserVer) {
                        case "7.0": code = 7; break;
                        case "8.0": code = 8; break;
                        case "9.0": code = 3; break;
                        case "10.0": code = 2; break; //Code changes for IE10 Compatibility
                        case "11.0":
                        case "11.1": code = 1; break; // Changes for Windows10 Compatability
                        default: code = 0; break;
        }
        break;
       case "firefox":    //HKEY_LOCAL_MACHINE\SOFTWARE\Classes\http\shell\open\command
       case "mozilla firefox":
         code = 6;
         break;

       //GS Customization: To display chrome if set as default browser
       case "chrome":
        code = 5;
        break;
       case "netscape":
       case "netscp":
       case "naviga~1":
       case "navigator":
        code = 4;
        break;
       default:         // code is already ok
         break;
      }
      return code;  
    },
    
    //--------------------------------------------------------------------
    //-  ss_va_Null() -  Generate a null code for an unused VA
    //--------------------------------------------------------------------
        GetNullCode: function () {
            return (0);
    },

    // TD : Dependent on smapi
    //--------------------------------------------------------------------
    //-  ss_va_Modem() -   Modem code translated -- to be supplied
    //--------------------------------------------------------------------
        GetModemCode: function () {
      var code = 0;
      
            try {
        // TD : sdcDSLModem is defined in smapi
        var modem = $ss.agentcore.smapi.methods.GetInstance();
        code = modem.GetInstanceValue("vacode", 0);
      }
            catch (ex) {
        code = 0;
      }
      
      return code;
    },

    //GS Customization start: Function to get the Default browser version
    GetDefaultBrowser: function(){
    
      var strCommand = _registry.GetRegValue ("HKCU","Software\\Microsoft\\Windows\\Shell\\Associations\\UrlAssociations\\http\\UserChoice","Progid");
      var sReturn = "";
      if (strCommand != "") {
         // Browser on Vista and above

         var strClassPath = "Software\\Classes\\" + strCommand + "\\shell\\open\\command";
         strCommand = _registry.GetRegValue ("HKCU",strClassPath,"");
         if (strCommand == "") {
            strCommand = _registry.GetRegValue ("HKLM",strClassPath,"");
            if (strCommand == "")
               strCommand = _registry.GetRegValue ("HKCR","http\\shell\\open\\command","");
         }
      } 
      else {// Browser on XP and below
         strCommand = _registry.GetRegValue ("HKCR","http\\shell\\open\\command","");
      }
      //get the browser name from the command obtained
      if(strCommand != "") {
        strCommand = strCommand.toLowerCase();
        var nBegin = strCommand.lastIndexOf("\\") + 1;
        var nEnd = strCommand.lastIndexOf(".exe");
        sReturn = strCommand.slice(nBegin, nEnd);        
      } 
      return sReturn.toLowerCase();
    },
    //GS Customization end

    //--------------------------------------------------------------------
    //-  ss_va_Processes() -   Number of processes (in ranges)
    //--------------------------------------------------------------------
        GetProcessesCode: function () {
      var nGroup = 0;
      try {
      var sections = _SI.GetXMLSections(_issueID, "SPRT_Applications");
      var secArray = sections.split(",");
                var numProc = parseInt(secArray.length / 10);
                nGroup = (numProc > 6) ? 6 : numProc;
      }   
            catch (oE) { }
            return (nGroup);
    },
    
    //--------------------------------------------------------------------
    //-  ss_va_CPUSpeed() -  CPU Speed (in ranges)
    //--------------------------------------------------------------------
        GetCPUSpeedCode: function () {
      try {
        var macID = _SI.GetMacId(_issueID);
                var CpuSpeed = _SI.GetIssueData(_issueID, 'PCH_Sysinfo', macID, 'ClockSpeed');
                var SpeedGroup = [[0, 399], [400, 599], [600, 799], [800, 999], [1000, 1999], [2000, 2999], [3000, 3999], [4000, 32767]];
                for (var ndx = 0; ndx < SpeedGroup.length; ndx++) {
          var Group = SpeedGroup[ndx];
          if (CpuSpeed >= Group[0] && CpuSpeed <= Group[1]) {
                        return (ndx);
          }
        }
            } catch (oE) { }
            return (0);          // to be sure we have some value
    },

    //--------------------------------------------------------------------
    //-  ss_va_Memory() -  Memory amount (in ranges)
    //--------------------------------------------------------------------
        GetMemoryCode: function () {
      try {
                var Memory = _SI.GetIssueData(_issueID, 'Win32_LogicalMemoryConfig', 'Win32_LogicalMemoryConfig', 'TotalPhysicalMemory');
        //GS Customization: Memory value range is  modified
        var MemoryGroup = [[0,256],[256,512],[512,1024],[1024,1536],[1536,2048],[2048,3072],[3072,4096],[4096,32767]];
                for (var ndx = 0; ndx < MemoryGroup.length; ndx++) {
          var Group = MemoryGroup[ndx];
          if (Memory >= Group[0] && Memory <= Group[1]) {
                        return (ndx);
          }
        }
            } catch (oE) { }
            return (0);          // to be sure we have some value
    },

    
    //--------------------------------------------------------------------
    //-  ss_va_EMailClient(() -  Default e-mail client code
    //--------------------------------------------------------------------
        GetEmailClientCode: function () {
      var defMailClient = "";
            try {
        defMailClient = _utils.GetDefaultEmailClient();
      }
            catch (ex) {
                defMailClient = _SI.GetIssueData(_issueID, 'Software', 'Software', 'DefaultEmailClient');
      }

      if (!defMailClient) {         // no default e-mail client set
                return (0);
      }
      //---  decode the default client
      var code = 0;
            switch (defMailClient) {
                case "Microsoft Outlook":
          code = 1; break;
                case "Outlook Express":
          code = 2; break;
                case "Hotmail":
          code = 3; break;
                case "Yahoo! Mail":
          code = 4; break;
                case "Netscape Mail":
          code = 5; break;
                case "Windows Mail":
          code = 6; break;
        case "Windows Live Mail":
          code = 7; break;
        default:
          code = 0; break;
      }
      return code;
    },

    //--------------------------------------------------------------------
    //-  ss_va_NICCount() -        Count of NIC cards (max 8)
    //--------------------------------------------------------------------
        GetNICCode: function () {
      var adapterCount = 0; 
            for (card in _GetNetworkObject().cardsProps) {
        adapterCount++;
      } 
      // determine whether user has at least 1 non-virtual adapter plugged in
            var adapterCode = (adapterCount > 8) ? 8 : adapterCount;
      
      return adapterCode;
    },

    //--------------------------------------------------------------------
    //-  ss_va_DHCP() -         DHCP enabled and IP assigned on 1 adapter
    //-                        Coding  is strange, see doc
    //--------------------------------------------------------------------
    GetDHCPAdapterCode:function(netobj) 
    {
      //GS Customization to call FlushDNS function
      _utils.FlushDNS();
      if (!netobj)
        netobj = _utils.CreateActiveXObject("SPRT.SdcNetCheck");
        
      var adapterCount = 0; 
      
      // get present and enabled adapter count
            for (card in _GetNetworkObject().cardsProps) {
                if (_netCheck.IsAdapterEnabled(netobj, card)) adapterCount++;
      }
      
            if (adapterCount == 0) {    // handle case of disabled adapter
          return 0;      
      }
      
      var dhcpCode = _GetNetworkObject().CheckForDHCPAdapters() ? 2 : 0;
            var ipCode = _GetNetworkObject().CheckForValidIP() ? 1 : 0;
      var code = dhcpCode + ipCode; 
      
      //--   Translate code to voiceassist code and save
            var aTrans = [0, 3, 2, 1];
      return aTrans[code];  
    },

    //--------------------------------------------------------
    //-  ss_va_EMail() -          POP and SMTP server access(?)
    //-                        Coding  is strange, see doc
    //--------------------------------------------------------
        GetPOPSMTPCode: function (netobj) {
      if (!netobj)
        netobj = _utils.CreateActiveXObject("SPRT.SdcNetCheck");
        
      var ispNum = _SI.ReadPersistInfo(_SIConstants.GEN_CLASS, "isp");
      
            if (ispNum == "") {
        ispNum = "isp1";
      }
      
            var popServer = _config.GetConfigValue("email", "pop_" + ispNum, "");
            var smtpServer = _config.GetConfigValue("email", "smtp_" + ispNum, "");
            var popPort = _config.GetConfigValue("email", "popport_" + ispNum, "110");
            var smtpPort = _config.GetConfigValue("email", "smtpport_" + ispNum, "25");
            var emailTimeout = _config.GetConfigValue("email", "emailtimeout_" + ispNum, "5000");
      
      var popCode = _TestServerList(netobj, popServer, parseInt(popPort), parseInt(emailTimeout)) ? 2 : 0;  
      var smtpCode = _TestServerList(netobj, smtpServer, parseInt(smtpPort), parseInt(emailTimeout)) ? 1 : 0;
      var code = popCode + smtpCode; 
            var aTrans = [0, 2, 3, 1];
            return aTrans[code];
    },

    //--------------------------------------------------------
    //-  ss_va_NetChecks() -          TCP/IP Cable and Loopback
    //-                            coding is strange, see doc
    //--------------------------------------------------------
        GetTCPIPAndLoopBackCode: function (netobj) {
      if (!netobj)
        netobj = _utils.CreateActiveXObject("SPRT.SdcNetCheck");
        
      var adapterCount = 0; 
      
      // get present and enabled adapter count
            for (card in _GetNetworkObject().cardsProps) {
                if (_netCheck.IsAdapterEnabled(netobj, card)) adapterCount++;
      }

      //--     Get Test Results of interest
            var cable = _GetNetworkObject().IsCableConnectedToAdapter();
            var tcp = _GetNetworkObject().CheckForBoundAdapters();
      var loopback = netobj.PingTest("127.0.0.1", 32, 2000, 56);
      
            if (adapterCount == 0) {    // handle case of disabled adapter, only check loopback since others failed
                return (loopback ? "7" : "0");
      }
      
      //--     Create bit code of TCP|Cable|Loopback ---
            var tcpCode = tcp ? 4 : 0;
      var cableCode = cable ? 2 : 0;
            var loopCode = loopback ? 1 : 0;
      var code = tcpCode + cableCode + loopCode;
      
      //--     Translate [real] code to the brain dead voiceassist code ---
            var aTrans = [0, 7, 4, 6, 5, 3, 2, 1];
            return (aTrans[code]);
    },

    //--------------------------------------------------------
    //-  ss_va_DNSWeb() -          DNS resolve and Web ping
    //-                         not sure why these are together?
    //--------------------------------------------------------
        GetDNSWebCode: function (netobj) {
      if (!netobj)
        netobj = _utils.CreateActiveXObject("SPRT.SdcNetCheck");
        
       var sHost = _config.ExpandAllMacros("%SERVERBASEURL%");
        //_DNSLookup will return "" instead of IP if we pass sHost value in this format "http://qaserver.supportsoft.com/" 
        //so removing http(s) and passing only qualified domain name.
      var dHost = sHost.replace(/^https?\:\/\//i, "").split("/")[0];
      var dnsResolve = _netCheck.ResolveHostname(netobj, dHost, 2000);
      var web = netobj.PingTest(dHost, 32, 2000, 56);
       
      //--   Create true bit code for dns and web
      var dnsCode = (dnsResolve != "" && dnsResolve != -1) ? 2 : 0;
      var webCode = (web) ? 1 : 0;
      var code = dnsCode + webCode;
      
      //--   Translate code to voiceassist code and save
            var aTrans = [0, 3, 2, 1];
            return (aTrans[code]);
    },

    //------------------------------------------------------
    //-  ss_va_GetOS() -              Operating System code
    //------------------------------------------------------
        GetOSCode: function () {
            // Add entries in the golden database (dataset_vacommon.dat) in sprt_registry and sprt_si_thresholds table.
      
            var sOS = _utils.GetOS();
            var is64BitOs = _utils.Is64BitOS();
            switch (sOS) {
              case "WINXP":
                          return 4; 
              case "WIN7WS":
                          return (is64BitOs) ? 2 : 1;
              case "WIN8": //Changes for Windows 8 Compatibility
                          return (is64BitOs) ? 5 : 9;
              case "WIN8_1": //Changes for Win8.1 & IE11 Compatibility
                          return (is64BitOs) ? 3 : 8;
              case "WIN10" : // Changes for Windows10 Compatability
                          return (is64BitOs) ? 6 : 7;
              default:
                          return 0;
      }
    },

    // Returns info on the network stack
    //  First  - TCP/IP bound
    //  Second - DHCP Enabled
    //  Third   - DHCP Dynamic

        GetNetworkStackCode: function () {
            var tcp = _GetNetworkObject().CheckForBoundAdapters() ? 1 : 0;
            var dhcp = _GetNetworkObject().CheckForDHCPAdapters() ? 2 : 0;
      var ipCode = _GetNetworkObject().CheckForValidIP() ? 4 : 0;
      
            return (tcp + dhcp + ipCode);
    },
    
    // Returns info on the selected adapter
    // First - Adapter Type (0 - ethernet, 1 - usb)
    // Second  - Adapter Enabled (0/1)
        GetAdapterCode: function () {
      var conntype = _SI.ReadPersistInfo(MODEM_CLASS, CONN_TYPE);
      var adapterType = 0;
      
      var netobj = _utils.CreateActiveXObject("SPRT.SdcNetCheck");
        
            if (conntype == "ethernet") {
        adapterType = 1;
      }
            else {
        adapterType = 0;
      }
      
      var adapterEnabled = 0; 
      
      // get present and enabled adapter count
            for (card in _GetNetworkObject().cardsProps) {
                if (_netCheck.IsAdapterEnabled(netobj, card)) adapterEnabled++;
      }
      
            if (adapterEnabled >= 1) {
        adapterEnabled = 2;
      }
      
            return (adapterType + adapterEnabled);
    },

    // Returns 0/1 whether drivers have been installed
        GetUSBCode: function () {
      var installedDrivers = _SI.ReadPersistInfo(MODEM_CLASS, MODEM_INSTALLUSBDRIVERS);
     
      // if usb drivers were installed, SI_ADAPTER_INSTALLDRIVERS will contain the success message.
      
      // TD : Check the translated string  
      //if (installedDrivers == ss_loc_GlobalXLate("ss_glb_success", "success"))
            if (installedDrivers == "success") {
        return 1;
      }
            else {
        return 0;
      }
    },

    // Returns info regarding Modem Connectivity
    // first  - Detect Modem (0/1)
    // second - IP Release/Renew (0/1)
        GetModemConnectivityCode: function () {
      var detect = (_databag.GetValue('ss_gc_db_ModemDetected') == "true") ? 1 : 0;
      
      // if the modem is previously detected, we don't hit the detect modem page..
      // therefore databag value is null
            if (_databag.GetValue('ss_gc_db_ModemDetected') == null) {
        detect = 1;
      }

            try {
        var modemCode = _SI.ReadPersistInfo(MODEM_CLASS, MODEM_CODE);
        var sModemCode = modemCode.toLowerCase();

                if (sModemCode.substr(0, 10) == "null_modem") {
          detect = 0;
        }
      }
            catch (oE) { }

      var releaseRenew = (_databag.GetValue('ss_gc_db_releaserenew') == "true") ? 2 : 0;
      
            return (detect + releaseRenew);
    },

    // First set of Modem Troubleshooting data
    // first -  Whether cables were checked (0/1)
    // second - Whether additional router was checked (0/1)
    // third -  Whether modem was powercycled (0/1)
        GetModemTroubleshootingBasicCode: function () {
      var checkcables = (_databag.GetValue('ss_gc_db_checkcables') == "true") ? 1 : 0;
            var powercycle = (_databag.GetValue('ss_gc_db_powercycle') == "true") ? 2 : 0;
      var checkrouter = (_databag.GetValue('ss_gc_db_checkrouter') == "true") ? 4 : 0;
      
      return (checkcables + powercycle + checkrouter);
    },

    // Second set of Modem Troubleshooting data
    // first -  Whether firewall was checked (0/1)
    // second - Whether Modem Password was changed (0/1)
    // third -  Whether filters were checked (0/1)
        GetModemTroubleshootingAdvancedCode: function () {
      var checkfirewall = (_databag.GetValue('ss_gc_db_firewall') == "true") ? 1 : 0;  
      var pwchanged = (_databag.GetValue('ss_gc_db_modem_password_changed') == "true") ? 2 : 0;
      var checkfilters = (_databag.GetValue('ss_gc_db_checkfilters') == "true") ? 4 : 0;
      
            return (checkfirewall + pwchanged + checkfilters);
    },

    // Check DSL/Cable Synch
    // first -  Whether we have DSL synch (0/1)
    // second - Whether phone line/cable was checked (0/1)
    // third -  Whether activation date has been reached (0/1)
        GetDSLCableSyncCode: function () {
      var dslSync = (_databag.GetValue('ss_gc_db_SyncOk') == "true") ? 1 : 0;
      
      // if the modem has sync, we may not have hit the check modem sync page..
      // therefore databag value is null.. but also check the SI first
            if (_databag.GetValue('ss_gc_db_SyncOk') == null) {
        // TD : cross check the translated string
        //if(_SI.ReadPersistInfo(MODEM_CLASS, MODEM_STATUS_SYNC) == ss_loc_GlobalXLate("ss_glb_failure", "failure"))
                if (_SI.ReadPersistInfo(MODEM_CLASS, MODEM_STATUS_SYNC) == "failure") {
          dslSync = 1;
        }
      }

      // check if we were able to detect the modem.. if we weren't, synch must be false.
      var nodetect = (_databag.GetValue('ss_gc_db_ModemDetected') == "false") ? 1 : 0;
            if (nodetect) {
        dslSync = 0;
      }

      var phonecable = (_databag.GetValue('ss_gc_db_phonecable') == "true") ? 2 : 0;
            var activationdate = (_databag.GetValue('ss_gc_db_activationdate') == "true") ? 0 : 4;
      
      return (dslSync + phonecable + activationdate);
    },

    // Returns info about Network Synch
    // first - Whether HTTP request succeeded
    // second - Whether Proxy is disabled
    // third - Whether IE is offline
        GetNetworkSyncCode: function () {
      var netobj = _utils.CreateActiveXObject("SPRT.SdcNetCheck");
      var wgServer = _config.GetConfigValue("smartissue", "si_server");
      var timeOut = _config.GetConfigValue("global", "wg_detect_timeout", 5000);

      wgServer = wgServer.replace(/(http:\/\/)|(https:\/\/)/ig, "");
      var ar = wgServer.split(":");
      var host = ar[0];
      var port = (ar.length > 1) ? parseInt(ar[1]) : 80;

      var httpReq = _netCheck.TestConnecttion(null, host, port, timeOut) ? 1 : 0;
      var proxyEnabled = _inet.IsProxyEnabled() ? 2 : 0;
      var IEOffline = _inet.IsIEOffline() ? 4 : 0;
      
      return (httpReq + proxyEnabled + IEOffline);
    },

    // fills the SI with network information
        GetAdapterNICCode: function () {
            var netobj = _utils.CreateActiveXObject("SPRT.SdcNetCheck");
            var adapters = _netCheck.GetPresentAdapters(netobj, true, true, true, false);
            var adapterProps = _netCheck.GetAdaptersProperties(netobj, adapters, "ServiceName");
      var retNicCode = "others";

            for (var i in adapterProps) {
        var cardProps = adapterProps[i];

                for (var i2 in cardProps) {
          var serviceName = cardProps['ServiceName'];                  

          var nicCode = _nics.GetNICCodeBySvcName(cardProps['ServiceName']);
                    if (nicCode != "others") {
            return nicCode;
          }
        }
      }
      
      return retNicCode;
    },

    // returns VA code for make/model of Adapter
        GetAdapterModelCode: function () {
      var nicCode = this.GetAdapterNICCode();  
          
      var nicVaCode = "5";
      
      var nicDOM = _nics.GetNICDOM();

      var value = "";
           
            if (nicDOM) {
        var oNode = null;
                var expr = "//adapter[@code=\"" + nicCode + "\"]/vacode";
        oNode = nicDOM.selectSingleNode(expr);
          
                if (oNode) {
                    if (oNode.cdata != null && typeof (oNode.cdata) != "undefined")
            value = oNode.cdata.toString();
                    else if (oNode.text != null && typeof (oNode.text) != "undefined")
            value = oNode.text.toString();
        }        
      }
      
            if (value != "") {
        nicVaCode = value;
      }
      
            return (parseInt(nicVaCode));
    },

    // returns type of wireless adapter:
    // 0 - other
    // 1 - usb
    // 2 - laptop (PCMCIA)
    // 3 - PCI
        GetWirelessAdapterTypeCode: function () {
      var nicCode = this.GetAdapterNICCode();        
      
      var nicDOM = _nics.GetNICDOM();

      var value = "";
      var conntype = "";
             
            if (nicDOM) {
        var oNode = null;
                var expr = "//adapter[@code=\"" + nicCode + "\"]/connection_type";
        oNode = nicDOM.selectSingleNode(expr);
          
                if (oNode) {
                    if (oNode.cdata != null && typeof (oNode.cdata) != "undefined")
            value = oNode.cdata.toString();
                    else if (oNode.text != null && typeof (oNode.text) != "undefined")
            value = oNode.text.toString();
        }        
      }
      
            if (value != "") {
        conntype = value;
      }
      
      var adapterType = 0;
          
            switch (conntype) {
                case "usb":
          adapterType = 1;
          break;
                case "laptop":
          adapterType = 2;
          break;
                case "pci":
          adapterType = 3;
          break;
      }

      return (adapterType);
    },

    // returns info about the wireless adapter:
    // first - Wireless NIC Detected (0/1)
    // second - Number of wireless NICs detected (0 - One, 1 - Multiple)
    // third - Wireless driver installed (0/1)
        GetWirelessAdapterDetailsCode: function () {
      var numWireless = ss_wrl_GetPresentWirelessAdaptersCount(null, true);
      var wirelessDetected = 1;
      var installedDrivers = _SI.ReadPersistInfo(ADAPTER_CLASS, ADAPTER_INSTALLDRIVERS);
      var wirelessDriverInstalled = 0;
      
      // if drivers were installed, SI_ADAPTER_INSTALLDRIVERS will contain the success message.  
      //if (installedDrivers == ss_loc_GlobalXLate("ss_glb_success", "success"))
            if (installedDrivers == "success") {
        wirelessDriverInstalled = 4;
      }

      // assume that if we detected a wireless card, the drivers have been installed.
            if (numWireless >= 1) {
       wirelessDriverInstalled = 4;
      }
        
            if (numWireless > 1) {
        numWireless = 1;
      }
            else {
        numWireless = 0;
      }
     
      numWireless = (numWireless == 1) ? 2 : 0;
      
      return (numWireless + wirelessDetected + wirelessDriverInstalled);
    },

    // returns info about Wireless security settings
    // second - Security Type (0 - None, 1 - WEP, 2 - WPA)
        GetWirelessSecurityCode: function () {
      var secMap = $ss.snapin.smapi.methods.GetInstance().GetWirelessSecurityMap();
      var securitySelection = _databag.GetValue("ss_wrl_db_SecurityLevelSelection");
      var secLevel = secMap[securitySelection];
      var authType = "";
      var encType = "";

            if (secLevel) {
        authType = ss_wrl_utl_ConvertAuthentication(secLevel.oSecurity.ulSecMode, secLevel.oSecurity.ulAuthentication);
        encType = ss_wrl_utl_ConvertEncryption(secLevel.oSecurity.ulEncryption);
      }
      
            if (authType == WZCSVC_AUTHTYPE_OPEN || authType == "") {
        return 0;
      }

            if (encType == WZCSVC_ENCTYPE_WEP) {
        return 1;
      }
            else {
        return 2;
      }
    },

    // returns 0/1
        IsWirelessSSIDBroadcastEnabled: function () {
            try {
       // can't rely on param being set - Subagent/Repman may not have 
       // gone through modem setup.. check modem directly if possible
       // default to null modem case with broadcast enabled

       var modem = $ss.snapin.smapi.methods.GetInstance();
                if (modem.GetLastError() != SMAPI_SUCCESS) {
         return 1;
       }
       var typeOfModem = modem.GetInstanceValue("class", "_null_modem");

                if (typeOfModem == "_null_modem") {
         // default to true for Null modems
        // return 1;
       }
        
        var modemInfo = modem.GetSSIDInfo(WLS_SSID_BROADCAST);
                if (modem.GetLastError() != SMAPI_SUCCESS) {
          return 1;
        }
                else {
                    if (modemInfo.NewSSIDBroadcastEnabled) {
          return 1;
         }
                    else {
           return 0;
         }
        } 
      }
            catch (e) {
       return 1;
      }

      return 1;
    },

    //--------------------------------------------------------------------
    //-  ss_va_FWSoftware() -  Firewall software
    //--------------------------------------------------------------------
        GetFWSoftwareCode: function () {
      var antivr = false;
      var Norman = false;
      var filter = false;
      var virus_signatures = "navapsvc.exe;navapw32.exe;avconsol.exe;defwatch.exe;avsynmgr.exe;webscanx.exe;mcshield.exe;mcvsescn.exe;mcagent.exe";   
      var antivirus = virus_signatures.split(";");
            var sSections = _SI.GetXMLSections(_issueID, "SDC_Applications");

      aSections = sSections.split(",");
            for (var i = 0; i < aSections.length; i++) {
                for (var j = 0; j < antivirus.length; j++) {
                    if (aSections[i].toLowerCase().indexOf(antivirus[j]) != -1)
            antivr |= true;
                    if (aSections[i].toLowerCase().indexOf("zlh.exe") != -1)
            Norman = true;
                    if (aSections[i].toLowerCase().indexOf("tdc net filter.exe") != -1)
            filter = true;        
        }
      }
            var code = (Norman) ? 4 : 0;
            code += (filter) ? 2 : 0;
            code += (antivr) ? 1 : 0;
      //--   Translate code to voiceassist code and save
            var aTrans = [0, 4, 3, 7, 2, 6, 5, 1];
            return (aTrans[code]);
    }
    
  });
  var _objContainer = $ss.agentcore.utils.activex.GetObjInstance();
  var _logger = $ss.agentcore.log.GetDefaultLogger("$ss.agentcore.vacode");  
  var _exception = $ss.agentcore.exceptions;
  var _utils = $ss.agentcore.utils;
  var _config = $ss.agentcore.dal.config;
  var _nics = $ss.agentcore.network.nics;
  var _network = $ss.agentcore.network;
  var _inet = $ss.agentcore.network.inet;
  var _registry = $ss.agentcore.dal.registry;
  var _SI = $ss.agentcore.diagnostics.smartissue;
  var _SIConstants = $ss.agentcore.constants.smartissue;
  var _xml = $ss.agentcore.dal.xml;
  var _databag = $ss.agentcore.dal.databag;
  var _objNet = null;
  var _netCheck = $ss.agentcore.network.netcheck;
  var _issueID = null;
  
    function _GetNetworkObject() {
        _objNet = _objNet || new _network.network();
    return _objNet;
  }
  
  //--------------------------------------------------------------------
  //-  ss_va_Formatted() -  add parity digit to code
  //--------------------------------------------------------------------
  function _FormatVACode(aVoiceAssist) {
    var sOut = new String();
        for (var ndx = 0; ndx < aVoiceAssist.length; ndx++) {
      sOut += aVoiceAssist[ndx];
            if ((ndx + 1) % 4 == 0)
        sOut += '-';
    }
        return (sOut.substr(sOut.length - 1, 1) == '-') ? sOut.substr(0, sOut.length - 1) : sOut;
  }
  
  //--------------------------------------------------------------------
  //-  ss_va_GenerateParity() -  add parity digit to code
  //--------------------------------------------------------------------
  function _AddParityToVACode(aVoiceAssist) {
    var sum = 0;
        for (var ndx = 2; ndx < aVoiceAssist.length; ndx++)    // note: start at 2
            sum += aVoiceAssist[ndx] * (ndx + 1);
        return (sum % 10);
  }
  
    function _TestServerList(netobj, serverToTest, port, timeout) {
        if (_netCheck.TestConnection(netobj, serverToTest, port, timeout)) {
      return true;
    }
        
    return false;
  }
  
  // returns 0/1 based on all Minreqs passing
    function _HasMinRequirements() {
    var allPassed = _SI.ReadPersistInfo(_SIConstants.MINREQ_CLASS, "sma_mrq_allpassed");
    
        if (allPassed == "true") {
      return 1; 
    }
        else {
      return 0;
    }
  }

  
  
})();



// Not being used    
////------------------------------------------------------------------------------------
////-  ss_va_AreProcessesRunning(processList) - takes a semicolon delimited list of
////    process names and returns 0 or 1 if they ALL are found. 

////    Can be useful to pass in multiple process names if a particular application relies
////    on multiple services/processes to run correctly.
////                       
////------------------------------------------------------------------------------------
//function ss_va_AreProcessesRunning(processList)
//{
//  var processes = processList.split(";");
//  var sSections = ss_si_GetXMLSections("SDC_Applications");

//  aSections = sSections.split(",");

//   // iterate through list of passed in processes
//  for(var i=0;i<processes.length;i++)
//  {
//    var bWasFound = false;

//    // iterate through list of currently running processes
//    for(var j=0;j<aSections.length;j++)
//    {

//      if(aSections[j].toLowerCase().indexOf(processes[i].toLowerCase()) != -1)
//      {
//        bWasFound = true;
//        break;
//      }
//    }

//    // if any process is not found, return 0 
//    if (!bWasFound)
//    {
//      return(0);
//    }
//  }

//  // otherwise, all were found.. return 1.
//  return(1);
//}