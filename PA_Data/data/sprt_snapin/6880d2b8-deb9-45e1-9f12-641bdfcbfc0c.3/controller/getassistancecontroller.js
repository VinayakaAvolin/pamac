SnapinGetassistanceController = SnapinBaseController.extend('snapin_getassistance', {
  exception: $ss.agentcore.exceptions
}, {
  index: function(params){
    try {
      var websiteUrl = NavigationController.GetServerUrl() + "/sdcxuser/asp/login.asp";
      var skinName = $ss.agentcore.dal.config.ParseMacros("%SKINNAME%");
      // getting the prefered language and passing the same value from PA to xuser portal
      var sPrefLangCode = $ss.agentcore.utils.GetLanguage();
      var sLangCode = $ss.agentcore.dal.config.GetConfigValue("languagemapping", sPrefLangCode, "en-US");
      var sRequestParam = $ss.agentcore.dal.config.GetConfigValue("UserPrefLangRequestParam", "USER_PREF_LANG_REQUEST_PARAM", "UserPrefLangID");
      //Refer to connect variable
      this.renderSnapinView("snapin_getassistance", params.toLocation, "\\views\\getassistance.html", {
          "ootbURL": websiteUrl + "?"+ sRequestParam + "=" + sLangCode,
        "skinName": skinName
      });
    } 
    catch (ex) {
      this.Class.exception.HandleException(ex, 'snapin_getassistancecontroller', 'index', params);
    }
  },
  
  GetVACodeXml: function(){
    var absPath = $ss.getSnapinAbsolutePath("snapin_getassistance");
    var pathVAXml = absPath + "\\xml\\vacode.xml";
    
    var isWireless = false;
    
    var objSI = $ss.agentcore.diagnostics.smartissue;
    var objConstants = $ss.agentcore.constants;
    var objDatabag = $ss.agentcore.dal.databag;
    
    var wrlEntryPoint = objDatabag.GetValue("ss_wrl_db_EntryPoint");
    
    if (wrlEntryPoint != null) {
      if (wrlEntryPoint.indexOf("wireless_") == 0) {
        isWireless = true;
      };
          }
    
    if (objSI.ReadPersistInfo(objConstants.MODEM_CLASS, objConstants.CONN_TYPE) == "wireless") {
      isWireless = true;
    }
    
    if (isWireless) {
      pathVAXml = absPath + "\\xml\\vacode_gc_wrl.xml";
    }
    else {
      if (objDatabag.GetValue("ss_gc_db_StartingPoint") != null) {
        pathVAXml = absPath + "\\xml\\vacode_gc.xml";
      }
    }
    return pathVAXml;
  },
  
  GetSupportCode: function(){
    var objVACode = $ss.agentcore.vacode;
    var objSI = $ss.agentcore.diagnostics.smartissue;
    var sVaCode = "";
    
    var issPath = $ss.getSnapinAbsolutePath("snapin_getassistance") + "\\xml\\default.xml"; //template for SI
    var issueId = objSI.CreateIssue("Contact us", true, issPath, false, false, null);
    
    var vaPath = this.GetVACodeXml();
    sVaCode = objVACode.GetVACode(vaPath, issueId);
    objSI.DeleteIssue(issueId);
    return sVaCode;   
  },
  
  "#copyvacode click": function(params){
    var bResult = window.clipboardData.setData("Text", $(this.params.element).attr("vacode"));
    if (bResult && this.params.element.display) {
      $("#code_display").append("<p/>" + $ss.agentcore.utils.LocalXLate("snapin_getassistance", "snp_contact_supportcode_copied"));
      this.params.element.display = false;
    }
  },
  
  "#voice_assist click": function(params){
    try {
      $("#code_display").hide();
      $("#code_status").show();
      $ss.agentcore.utils.Sleep(500);
      var that = this;
      var opt = {
        fnPtr: function(){
          return that.GetSupportCode();
        },
        timeout: 30,
        sleepTime: 1000
      }
      var vaCode = $ss.agentcore.utils.UpdateUI(opt);
      $ss.agentcore.utils.Sleep(500);
      $("#code_status").hide();
      var str = $ss.agentcore.utils.LocalXLate("snapin_getassistance", "snp_contact_lbl_SiCodeHead_SpanCodeDesc");
      var snapinPath = $ss.getSnapinAbsolutePath("snapin_getassistance")
      var language = $ss.agentcore.dal.config.ParseMacros("%LANGCODE%");
      var skinName = $ss.agentcore.dal.config.ParseMacros("%SKINNAME%");
      var copytext = $ss.agentcore.utils.LocalXLate("snapin_getassistance", "snp_contact_lbl_copy_code");
      $("#code_display").html(str + "<br />" + vaCode + "<br><a hidefocus=\"true\" href=\"#\" id =\"copyvacode\" class=\"btn btn-primary-ss btn-xs\" display=true vacode=\"" + vaCode + "\">" + copytext + "</a>") ;
      $("#code_display").show();
    } 
    catch (ex) {
      this.Class.exception.HandleException(ex, 'snapin_alertcontroller', 'voice_assistclick', params);
    }
  },
  
  ".contactusurl click": function(params){
    var url = $(this.params.element).attr("displayurl");
    var objHttp = $ss.agentcore.dal.http;
    var objConfig = $ss.agentcore.dal.config;
    var iHTTPTimeOut = parseInt(objConfig.GetConfigValue("snapin_getassistance", "HTTPTimeout", 5000));
      var browserToUse = objConfig.GetConfigValue("snapin_getassistance", "BrowserToUse", "");      
      var retVal = 0;
      var alertText = "";
      if (browserToUse == "ie") {
        retVal = $ss.agentcore.utils.RunIEBrowser(url);
        alertText = $ss.agentcore.utils.LocalXLate("snapin_getassistance", "snp_contact_ie_disabled");
      }
      else {
        retVal = $ss.agentcore.utils.RunDefaultBrowser(url);
        alertText = $ss.agentcore.utils.LocalXLate("snapin_getassistance", "snp_contact_browser_disabled");
      }
      if (retVal == -1) {
        alert(alertText);
      }
    }
});
