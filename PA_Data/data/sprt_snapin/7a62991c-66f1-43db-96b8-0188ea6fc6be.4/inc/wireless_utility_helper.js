﻿$ss.snapin = $ss.snapin || {};

$ss.snapin.wireless = $ss.snapin.wireless || {};
  
$ss.snapin.wireless.utils = $ss.snapin.wireless.utils || {};

(function(){
  $.extend($ss.snapin.wireless.utils,
  {        
    /*******************************************************************************
    ** Name:         ConvertAuthentication
    **
    ** Purpose:      Convert SMAPI's auth values to WZCSVC constants  
    **
    ** Parameter:    ulSecMode - security mode value obtained from SMAPI
    **               ulAuthentication - authentication value obtained from SMAPI
    **
    ** Return:       "Open", "WPAPSK", "WPA2PSK"
    *******************************************************************************/
    ConvertAuthentication : function(ulSecMode,ulAuthentication)
    {      
      if (_wrlcommon.maskequal(ulAuthentication, _const.WLS_AUTH_OPEN)) return _sswrlconstants.WZCSVC_AUTHTYPE_OPEN;    
      
      if (_wrlcommon.maskequal(ulAuthentication,_const.WLS_AUTH_PSK))
      {
        if (_wrlcommon.maskequal(ulSecMode, _const.WLS_SECMODE_WPA2)) return _sswrlconstants.WZCSVC_AUTHTYPE_WPA2_PSK;
        if (_wrlcommon.maskequal(ulSecMode, _const.WLS_SECMODE_WPA))  return _sswrlconstants.WZCSVC_AUTHTYPE_WPA_PSK;
      } 
      return "";  
    },
    
    /*******************************************************************************
    ** Name:         ConvertEncryption
    **
    ** Purpose:      Convert SMAPI's encryption values to WZCSVC constants
    **
    ** Parameter:    ulEncryption - encryption value obtained from SMAPI
    **
    ** Return:       "None", "WEP", "TKIP", "AES"
    *******************************************************************************/
     ConvertEncryption : function(ulEncryption)
     {
        if (_wrlcommon.maskequal(ulEncryption, _const.WLS_ENCRYPT_NONE)) return _sswrlconstants.WZCSVC_ENCTYPE_NONE;
        if (_wrlcommon.maskequal(ulEncryption, _const.WLS_ENCRYPT_WEP))  return _sswrlconstants.WZCSVC_ENCTYPE_WEP;
        if (_wrlcommon.maskequal(ulEncryption, _const.WLS_ENCRYPT_TKIP)) return _sswrlconstants.WZCSVC_ENCTYPE_TKIP;
        if (_wrlcommon.maskequal(ulEncryption, _const.WLS_ENCRYPT_AES))  return _sswrlconstants.WZCSVC_ENCTYPE_AES;
        return "";
      },
    
     ApplyWiFiSettings : function(objSettings)
     {
      var sSvcName = objSettings.serviceName;      
      var oCfg = _sswrlutility.GetWiFiConfig(sSvcName);
      var retCode = oCfg.CreateProfile(objSettings, true);
      _logger.debug("ApplyWiFiSSettings"," retCode: " + retCode);      
      return (retCode == 0);
    },
    
    IsSecuritySupportedByNIC : function(sSvcName, secLevel)
    {
       var supportedAuthTypes = g_supportedAuthTypes[sSvcName];
       var supportedEncTypes  = g_supportedEncTypes[sSvcName];  
        if (!supportedAuthTypes)
        {
          supportedAuthTypes = _utils.MakeHashFromArrayValues(_nics.GetSupportedConfigBySvcName(sSvcName, "authentication"));
          g_supportedAuthTypes[sSvcName] = supportedAuthTypes;
        }    
        if (!supportedEncTypes)
        {
          supportedEncTypes = _utils.MakeHashFromArrayValues(_nics.GetSupportedConfigBySvcName(sSvcName, "encryption"));
          g_supportedEncTypes[sSvcName] = supportedEncTypes;
        }     
        var authType = _wrlu.ConvertAuthentication(secLevel.oSecurity.ulSecMode, secLevel.oSecurity.ulAuthentication);
        var encType = _wrlu.ConvertEncryption(secLevel.oSecurity.ulEncryption);
        return (supportedAuthTypes[authType] == true) && (supportedEncTypes[encType] == true);
    },
    
    sortAscending : function(a,b)
    {
      if(a.nOrdinal < b.nOrdinal) return -1;
      if(a.nOrdinal > b.nOrdinal) return 1;
      return 0;
    }    
  }); 
  
  
var _utils      = $ss.agentcore.utils;
var _system     = $ss.agentcore.utils.system;
var _config     = $ss.agentcore.dal.config;
var _databag    = $ss.agentcore.dal.databag;
var _registry   = $ss.agentcore.dal.registry;
var _network    = $ss.agentcore.network.network;
var _netcheck   = $ss.agentcore.network.netcheck;
var _inet       = $ss.agentcore.network.inet;
var _const      = $ss.agentcore.constants;
var _nics       = $ss.snapin.wireless.nics;
var _logger     = $ss.agentcore.log.GetDefaultLogger("$ss.snapin.wireless.utils"); 
var _sswrlconstants   = $ss.snapin.network.wireless.constants;
var _sswrlutility     = $ss.snapin.network.wireless.utility;
var _sdcDSLModem      = $ss.agentcore.smapi.methods;
var _smapiUtils       = $ss.agentcore.smapi.utils ;
var _wrlcommon        = $ss.snapin.wireless;
var _wrlu             = $ss.snapin.wireless.utils;

  
    
   /*******************************************************************************
    **    Constants
    *******************************************************************************/
    var g_supportedAuthTypes = {};
    var g_supportedEncTypes  = {};
    
    

})();