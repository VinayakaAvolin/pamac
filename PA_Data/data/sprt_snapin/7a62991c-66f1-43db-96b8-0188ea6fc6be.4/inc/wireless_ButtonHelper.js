﻿
$ss.snapin = $ss.snapin ||{}; 
$ss.snapin.wireless = $ss.snapin.wireless || {};
$ss.snapin.wireless.buttonhelper = $ss.snapin.wireless.buttonhelper || {};

(function(){
    
    $.extend($ss.snapin.wireless.buttonhelper, 
    {
      updatelayoutbuttons: function(Status) 
      {
          switch (Status) 
          {
            case "ENABLE_NEXT":
              this.EnableButton("#nextbutton"); 
              break;      
            case "ENABLE_BACK":
              this.EnableButton("#prevbutton"); 
              break;
            case "DISABLE_NEXT":
              this.DisableButton("#nextbutton"); 
              break; 
             case "HIDE_NEXT":
              this.HideButton("#nextbutton"); 
              break;      
            case "DISABLE_BACK":
              this.DisableButton("#prevbutton"); 
              break;   
             case "HIDE_BACK":
              this.HideButton("#prevbutton"); 
              break;   
            case "SHOW_RETRY":
              this.ShowOrHideButton("#retrybutton");
              break;
            case "ENABLE_RETRY":
              this.EnableButton("#retrybutton");
              break;
            case "HIDE_RETRY":
              this.HideButton("#retrybutton");
              break;                                
            case "HIDE_PRINT":
              this.HideButton("#printbutton");
              break;
            case "ENABLE_PRINT":
               this.EnableButton("#printbutton");
              break;
            case "ENABLE_DONE":
              this.EnableButton("#donebutton");
              break;  
            case "HIDE_DONE":
            this.HideButton("#donebutton");
            break;  
                                            
            default:
              break;
          }
        },
        ShowOrHideButton: function(sBtnId) 
        {
          if(sBtnId.toLowerCase().indexOf("show") != -1)
          {
		        $(sBtnId).css("display","inline");
		        $(sBtnId).css("cursor","hand");
		      }
          else
          {
            $(sBtnId).css("display","none");
            $(sBtnId).css("cursor","default");
          }
        },
        EnableButton: function(sBtnId) 
        {
          var sImgsrc = "";
          var sEvt = "";
          switch(sBtnId)
          {
            case "#nextbutton":
              sImgsrc  = "btn_next.gif";
              sEvt = "this.ProcessNext";
              break;
            case "#prevbutton":
              sImgsrc  = "btn_back.gif";
              sEvt = "this.ProcessPrevious";
              break;
            case "#retrybutton":
              sImgsrc  = "btn_retry.gif";              
              break;
             case "#printbutton":
              sImgsrc  = "btn_print.gif";              
              break;
             case "#donebutton":
              sImgsrc  = "btn_done.gif";              
              break;
            default:
              break;
          }
          $(sBtnId).css("display","inline");	
           //Remove the class "btnDisabled" to control the click event of the button. 
          $(sBtnId).removeClass("btndisabled");
          //hand
          $(sBtnId).css("cursor","hand");	
          var sNewImg = $ss.GetLayoutAbsolutePath() + "\\skins\\"+_config.ParseMacros("%SKINNAME%")+"\\buttons\\"+_config.ParseMacros("%LANGCODE%")+"\\"+ sImgsrc;
	        $(sBtnId).attr("src",sNewImg);	
	       	  	  
	      },
        DisableButton: function(sBtnId) 
        {
          var sImgsrc = "";
          var sEvt = "";
          switch(sBtnId)
          {
            case "#nextbutton":
              sImgsrc  = "btn_next_disabled.gif";
              break;
            case "#prevbutton":
              sImgsrc  = "btn_back_disabled.gif";
              break;
            /*case "#retrybutton":
              sImgsrc  = "pop_up_retry_btn_off.png";
              break;*/
            default:
              break;
          }
	        //Remove the class "btnDisabled" to controll the click event of the button. 
            $(sBtnId).addClass("btndisabled");
          $(sBtnId).css("cursor","default");	
	        var sNewImg = $ss.GetLayoutAbsolutePath() + "\\skins\\"+_config.ParseMacros("%SKINNAME%")+"\\buttons\\"+_config.ParseMacros("%LANGCODE%")+"\\"+ sImgsrc;
	        $(sBtnId).attr("src",sNewImg);	
	        $(sBtnId).unbind("click"); 
	      },
	      
	      HideButton : function (sBtnId)
	      {	        
	        $(sBtnId).css("display","none");  
	      }
	      
     });
    var _system = $ss.agentcore.utils.system;
    var _ui = $ss.agentcore.utils.ui;
    var _config = $ss.agentcore.dal.config;        
    var _databag = $ss.agentcore.dal.databag;    
    var _netCheck = $ss.agentcore.network.netcheck;
    var _inet = $ss.agentcore.network.inet;
    var _events = $ss.agentcore.events;
    var _logger = $ss.agentcore.log.GetDefaultLogger("$ss.snapin.wireless.renderHelper");  
})();
