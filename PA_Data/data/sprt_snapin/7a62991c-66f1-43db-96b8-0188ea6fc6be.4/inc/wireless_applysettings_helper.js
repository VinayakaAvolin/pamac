$ss.snapin= $ss.snapin || {};

$ss.snapin.wireless= $ss.snapin.wireless || {};

$ss.snapin.wireless.applysettings= $ss.snapin.wireless.applysettings || {};

(function(){
    
    $.extend($ss.snapin.wireless.applysettings, {
    /********************************************************************************/
    //All Public functions
    /********************************************************************************/
    
    /*******************************************************************************
    ** Name:         TestConnection
    **
    ** Purpose:      Test the connectivity for a passed encryption scheme
    **
    ** Parameter:   
    **
    ** Return:       
    *******************************************************************************/
   
    TestConnection:function(secLevelId)
    {
        var bSuccess = false;
        var svcName = _databag.GetValue("ss_wrl_db_NICServiceName");
        var ssid = _databag.GetValue("ss_wrl_db_SSID");        
        try
        {
                                             
              // for every 5 times, check if we are in a corner case where we failed to associate
              // because wzcsvc decides to pick another SSID, if so then delete currently connected ssid 
              // profile and test again                
              if ((++testcnt % 5 )== 0)
              {
                var cur_ssid =_wrlUtility.GetCurrentSSID(null, svcName);
                if (cur_ssid && cur_ssid != ssid)
                {
                  var oCfg =_wrlUtility.GetWiFiConfig(svcName);
                  oCfg.DeleteProfile(cur_ssid, cur_ssid);
                  
                }

                // for every 5 times, refresh ip if on windows vista to kick start the connection
                if (_system.IsWinVST(true))
                {
                    _utils.RenewIpConfig();
                }
              }

              bSuccess =_wrlUtility.TestAssociation(null, svcName, ssid);
              _logger.debug("TestConnection", "bSuccess: " + bSuccess);
                        
      }
      catch (err)
      {
        _logger.error('$ss.snapin.wireless.applysettings.TestConnection',err.message);
      }
      
      return bSuccess;   
    }, 
    /*******************************************************************************
    ** Name:         ApplySettings
    **
    ** Purpose:      Applies the creds required to connect using wireless in the machine
    **
    ** Parameter:   
    **
    ** Return:       
    *******************************************************************************/
    ApplySettings:function(secLevelId)
    {
        try
        {  
          var bResult  = true;          
          g_bDoNIC=_databag.GetValue("ss_wrl_db_ConfigNIC");
          if (bResult && g_bDoNIC)
          {   
            $("#ss_wrl_aws_connection").css("display","none");
            $("#ss_wrl_aws_adapter").css("display","block");                
            sTime = new Date().getTime();             
            bResult =  _appset.TimerforApplySetings(secLevelId,"ss_wrl_aws_adapter");
            if (bResult)
            {  
              //since the first 2 are true,we are setting the third one as busy here
              _databag.SetValue("ss_wrl_db_vflink3_animation", true);
              $ss.snapin.wireless.viewhelper.AutoUpdateVisualFlowState(); 
              
              $("#ss_wrl_aws_adapter").css("display","none");
              $("#ss_wrl_aws_connection").css("display","block");     
              sTime = new Date().getTime();     
              bResult = _appset.TimerforTestingConnection(secLevelId,"ss_wrl_aws_connection");                  
              if (!bResult)
              {
                _databag.SetValue("ss_wrl_db_ApplyResults", "connfail");               
               
              }
                     
            }
            else
             {
               _databag.SetValue("ss_wrl_db_ApplyResults", "nicfail");
             }
            
            
          }
          if (bResult) 
          {
            _databag.SetValue("ss_wrl_db_RetryMode", false);
            _databag.SetValue("ss_wrl_db_ApplyResults", "success");
          }
          $ss.snapin.wireless.viewhelper.AutoUpdateVisualFlowState();
          return bResult;
        }
        catch(err)
        {
          _logger.error('$ss.snapin.wireless.applysettings.ApplySettings',err.message);
        }
    
    },
   
    /*******************************************************************************
    ** Name:         ApplyNICSettings
    **
    ** Purpose:      
    **
    ** Parameter:   
    **
    ** Return:       
    *******************************************************************************/
    ApplyNICSettings:function(securityLevel)
    { 
          var bSuccess = false;              
          try
          {

                var db_password      = _databag.GetValue("ss_wrl_db_Password");
                var encKey           = "";
              
                var wifiProps            = {};
                wifiProps["serviceName"] = _databag.GetValue("ss_wrl_db_NICServiceName");
                wifiProps["ssid"]        = _databag.GetValue("ss_wrl_db_SSID");
              
                if (securityLevel != null && securityLevel != "")
                {
                    var modem = _sdcDSLModem.GetInstanceByCode("null_modem_dsl");      
                    var g_secMap = modem.GetWirelessSecurityMap();
                    var oSecurityInfo = g_secMap[securityLevel].oSecurity;
                    
                    // in standalone mode, encKey is the password itself,
                    // otherwise, generate encKey
                    g_bStandalone=_databag.GetValue("ss_wrl_db_Standalone");
                    if (g_bStandalone)
                    {
                      encKey = db_password;
                    }
                                  
                    // set databag for display purposes in the completion page
                    _databag.SetValue("ss_wrl_db_EncKey", encKey);
                   
                    wifiProps["authType"]    = _wrlu.ConvertAuthentication(oSecurityInfo.ulSecMode, oSecurityInfo.ulAuthentication);
                    wifiProps["encType"]     = _wrlu.ConvertEncryption(oSecurityInfo.ulEncryption);
                    wifiProps["encKey"]      = encKey;
               }
               else
               {
                    wifiProps["authType"]    = _sswrlconstants.WZCSVC_AUTHTYPE_OPEN;
                    wifiProps["encType"]     = _sswrlconstants.WZCSVC_ENCTYPE_NONE;
                    wifiProps["encKey"]      = "";
               }
             
               bSuccess  = _wrlu.ApplyWiFiSettings(wifiProps);            
          }
          catch (err)
          {          
             _logger.error('$ss.snapin.wireless.applysettings.ApplyNICSettings',err.message);
          }
                  
            
          _logger.debug("ApplyNICSettings", "bSuccess: " + bSuccess);
          
          return bSuccess;
    },
    
    TimerforApplySetings : function(secLevelId,sDivPrefix)
    {
      var timeOut = Number(_config.GetConfigValue("wireless", "apply_nic_timeout", "120")); 
      var duration = timeOut;
      var timer = timeOut;
      timeOut = timeOut * 1000;
      var time;
      var sTime = new Date();
      
      var self =this;
       var opt = 
          {
            fnPtr    : function(){return _appset.ApplyNICSettings(secLevelId); },
            timeout  : timeOut,
            sleepTime: 3000,
            delayExec: 2000,
            maxReTry: 20,
            callback : this.ShowWaitImage(self,sDivPrefix,"ss_wrl_aws_adapterstatus",timer,sTime,duration) 
          } ;
          var result = _uiutils.UpdateUI(opt);   
          return result; 
    },
    ShowWaitImage: function(self,sDivPrefix,localeString,timer,sTime,duration) {

      return function() {
        //var cTime = new Date().getTime();        
        //var timeElapsed = parseInt((cTime - sTime)/1000);
        //var remainTime  = timer - timeElapsed;
         var remTime = $ss.agentcore.utils.ui.GetRemainingTime(sTime,duration);
         if($('#' + sDivPrefix).length > 0 )
         {
            $('#' + sDivPrefix ).css("display", "block");
            //time = timer--;
			      //if(time<0) time=0; 
            //time = time.toString();
            var uitext = _utils.LocalXLate("snapin_wireless", localeString , [remTime]);
            $('#' + sDivPrefix).html(uitext);
         }
        }
      
      
    },
    
    TimerforTestingConnection : function(secLevelId,sDivPrefix)
    {
      var timeOut = Number(_config.GetConfigValue("wireless", "association_timeout", "60"));
      var duration = timeOut;
      var timer = timeOut;
      timeOut = timeOut * 1000;
      var time;
      var sTime = new Date();
      
      var self = this;
       var opt = 
          {
            fnPtr    : function(){return _appset.TestConnection(secLevelId); },
            timeout  : timeOut,
            sleepTime: 5000,
            maxReTry: 20,
            callback : this.ShowWaitImage(self,sDivPrefix,"ss_wrl_aws_connectionstatus",timer,sTime,duration)

          } ;
          var result = _uiutils.UpdateUI(opt);   
          return result; 
    }
    

    });//end of extend
    
    /********************************************************************************/
    //Private Area 
    /********************************************************************************/
    //agentcore constants
    var _config = $ss.agentcore.dal.config;        
    var _databag = $ss.agentcore.dal.databag;    
    var _wrlUtility=$ss.snapin.network.wireless.utility;
    var _sswrlconstants = $ss.snapin.network.wireless.constants;
    var _events = $ss.agentcore.events;
    var _utils= $ss.agentcore.utils;
    var _system=$ss.agentcore.utils.system;
    var _exception = $ss.agentcore.exceptions;
    var _uiutils = $ss.agentcore.utils.ui;
	var _logger  = $ss.agentcore.log.GetDefaultLogger("$ss.snapin.wireless.applysettingshelper"); 
    
    //Other namespace constants
    var _wrlu=$ss.snapin.wireless.utils;
    var _appset=$ss.snapin.wireless.applysettings; 
    var _sdcDSLModem = $ss.agentcore.smapi.methods ;  
    var _smapiUtils = $ss.agentcore.smapi.utils;
    
    //user defined constants
    var g_bInTimer = false;
    var g_bDoNIC   = null;
    var g_bStandalone = null;
    var testcnt=0;    
    var sTime; 
     
})();