
/** @namespace Holds all ini related functionality*/

$ss.snapin = $ss.snapin || {};

$ss.snapin.wireless = $ss.snapin.wireless || {};

$ss.snapin.wireless.nics = $ss.snapin.wireless.nics || {};

(function()
{
  $.extend($ss.snapin.wireless.nics,
  {
  
    /**
    *  @name  GetSetCode
    *  @memberOf $ss.snapin.wireless.nics
    *  @function
    *  @description  Gets the code from global variable or registry
    *  @returns adapter Code
    *  @example
    *
    *
    */
    GetSetCode:function()
    {
      try
      {
          _adapterCode  = _SI.ReadPersistInfo(_SIConstants.ADAPTER_CLASS, _SIConstants.ADAPTER_CODE);
      }
      catch(ex)
      {
        
      }
      return _adapterCode;
    },        

    /**
    *  @name GetNodeValue
    *  @memberOf $ss.snapin.wireless.nics
    *  @function
    *  @description  Gets the node value. Reads the ss_nics.xml
    *  @param nodeName name of the node
    *  @param defVal default value
    *  @param parentNode parent node
    *  @returns null
    *  @example
    *
    *
    */
    GetNodeValue:function(nodeName, defVal, parentNode)
    {
      var value  = (typeof(defVal) == 'undefined') ? "" : defVal;

      try
      {
        // if parent node is specified, then use it, otherwise do then whole nine yards
        if (parentNode)
        {
          var childNode = parentNode.selectSingleNode(nodeName);
          if (childNode) 
          {
            if (childNode.cdata != null && typeof(childNode.cdata) != "undefined")
              value = childNode.cdata.toString();
            else if (childNode.text != null && typeof(childNode.text) != "undefined")
              value = childNode.text.toString();
          } 
        }
        else
        {
          if(this.GetSetCode() == "") return value;

          // now check if we have nics xml loaded      
          if(!_GetNICDOM()) return value;

          var oNode = null;
          var expr  = "//adapter[@code=\""+ _adapterCode + "\"]/" + nodeName;
                  oNode = _adapterDOM.selectSingleNode(expr);
          
          if (oNode) 
          {
            if (oNode.cdata != null && typeof(oNode.cdata) != "undefined")
              value = oNode.cdata.toString();
            else if (oNode.text != null && typeof(oNode.text) != "undefined")
              value = oNode.text.toString();
          }    
        }
      }
      catch(ex)
      {
      }
      
      return value;
    },

    /**  
    *  @name InstallWirelessDriver
    *  @memberOf $ss.snapin.wireless.nics
    *  @function
    *  @description Installs a Wireless Adapter Driver 
    *  @param nTimeout timeout value in seconds
    *  @returns true/false
    *  @example
    *
    *
    */
    InstallWirelessDriver:function(nTimeout)
    {
      try
      {   
        var oInstallInfo = _GetAdapterDriverInstallInfo();
        var driverPath = oInstallInfo.sDriverPath;
        
        if (!driverPath)
        {
          return false;
        }

        driverPath = driverPath.replace(/\\/g, "/");   // normalize slashes
        
        var bIsRemoteTarget = driverPath.startsWith("http:") || driverPath.startsWith("https:");
        var bIsCompressed   = driverPath.endsWith(".zip");    
        var bResult = true;

        // if remote target, then download the file locally first
        if (bIsRemoteTarget)
        {      
          var localTarget = driverPath.replace(/.*\//, _config.ExpandSysMacro("%temp%"));      
          bResult = _http.GetFile(localTarget, driverPath, nTimeout * 1000);
          if (bResult)
          {
            driverPath = localTarget;
          }
        }

        if (bResult)
        {
          var localDriverPath = "";

          // if zip, then extract to temp      
          if (bIsCompressed)            // .ZIP format
          {
            localDriverPath = _config.ExpandSysMacro("%temp%") + this.GetSetCode();
            bResult = _file.Decompress(driverPath, localDriverPath, 3,null);
          }
          else                          // if not zip, then just remove the base filename from the path
          {
            localDriverPath = driverPath.substring(0, driverPath.lastIndexOf("/"));
          }
          
          if (bResult)
          {
            // if PNP is set to true, then set PNP path
            if (oInstallInfo.bPnP)
            {
              var nicType = this.GetNodeValue("connection_type");
              
              // if usb type, let's first remove the existing device so when it is replugged it will find our drivers        
              if (nicType == "usb")
              {          
                var usbname = this.GetNodeValue("usbname");
                _usb.RemoveDevice(usbname);
              }

              if (oInstallInfo.bUseDPInst)
              {
                // strip trailing slashes
                localDriverPath = localDriverPath.replace(/\\$/g, "");
                
                var exepath = "\"" + _config.ParseMacros("%ROOTPATH%%COMMON%\\software\\dpinst\\dpinst.exe") + "\"";
                exepath += " /s";                                      // silent mode
                exepath += " /sa";                                     // suppress Add/Remove Program
                exepath += " /path " + "\"" + localDriverPath + "\"";  // specify driver path

                _utils.RunCommand(exepath, 2);
              }
              else
              {
                _utils.AddPnPPath(localDriverPath);
              }
            }
            
            // if PostCmd is specified, then run it
            if (oInstallInfo.sPostCmd)
            {        
              var cmdPath = localDriverPath + "\\" + oInstallInfo.sPostCmd;
              _utils.RunCommand(cmdPath, 2);  // run in hidden mode
            }
          }
        }
        return bResult;
      }
      catch(e)
      {
        return false;
      }
    },

    /**
    *  @name NeedReboot
    *  @memberOf $ss.snapin.wireless.nics
    *  @function
    *  @description Return <reboot> attribute for that adapter 
    *  @returns reboot attribute value
    *  @example
    *
    *
    */
    NeedReboot:function ()
    {
      var rebootattr;
      var expr = "reboot";
      var rebootattr = this.GetNodeValue(expr, "");
      return rebootattr;
    },

    /**
    *  @name GetNICCodeBySvcName
    *  @memberOf $ss.snapin.wireless.nics
    *  @function
    *  @description Returns the niccode of the adapter
    *  @param sSvcName name of the service 
    *  @returns reboot attribute value
    *  @example
    *
    *
    */
    GetNICCodeBySvcName:function (sSvcName)
    {
      var nicCode = "others";
      if (_GetNICDOM())
      {
        var cards = _netCheck.GetEthernetCards(null);
        var deviceName = cards[sSvcName];

        var expr  = "//adapter";
        var oNode = _adapterDOM.selectNodes(expr);    
        for (var i = 0; i < oNode.length; i++)
        {
          var devicePattern = this.GetNodeValue("devicename", "", oNode[i]);
          if (devicePattern)
          {
            var regexp = new RegExp(devicePattern, "gi");
            if (deviceName && deviceName.match(regexp))
            {
              nicCode = oNode[i].getAttribute("code");
              break;
            }
          }
        }
      }
      return nicCode;
    },

    
    /**
    *  @name GetSupportedConfigBySvcName
    *  @memberOf $ss.snapin.wireless.nics
    *  @function
    *  @description  
    *  @param sSvcName
    *  @param section
    *  @returns 
    *  @example
    *
    *
    */
    GetSupportedConfigBySvcName:function(sSvcName, section)
    {
      var nicCode = this.GetNICCodeBySvcName(sSvcName);
      return _GetSupportedConfigByCode(nicCode, section);
    }

  });

  var _adapterCode;
  var _adapterDOM;

  var _SI = $ss.agentcore.diagnostics.smartissue;
  var _http = $ss.agentcore.dal.http; 
  var _config = $ss.agentcore.dal.config;
  var _file = $ss.agentcore.dal.file;
  var _utils = $ss.agentcore.utils;  
  var _xml = $ss.agentcore.dal.xml;
  var _usb = $ss.agentcore.devices.usb;
  var _netCheck = $ss.agentcore.network.netcheck;
  var _SIConstants = $ss.agentcore.constants.smartissue;
  var _nics = $ss.snapin.wireless.nics;


 FILEVERSION 9,3,146,0

  function _GetNICDOM()
  {
    if(!_adapterDOM)
    {
      //get modem file name
      var cfgNICXML =$ss.getSnapinAbsolutePath("snapin_wireless")+"\\"+ _config.GetConfigValue("wireless", "nicsxml");
      cfgNICXML=_config.ExpandAllMacros(cfgNICXML);
      _adapterDOM = _xml.LoadXML(cfgNICXML);

      if(!_adapterDOM)
      {
        return null;
      }
    }
    return _adapterDOM;
  }

  function _CompareFileVersions(strFileVersion1, strFileVersion2)
  {
    // split delimit parts of version
    var arrString1 = strFileVersion1.split(FILEVERSION_DELIMITER);
    var arrString2 = strFileVersion2.split(FILEVERSION_DELIMITER);

    // determine number of version parts
    var nMax1 = arrString1.length;
    var nMax2 = arrString2.length;

    // determine which string has fewest parts
    var nSmaller = (nMax2 < nMax1) ? nMax2 : nMax1;

    // compare integer values of parts of the versions
    for (var i = 0; i < nSmaller; i++)
    {
      if(Number(arrString1[i]) > Number(arrString2[i]))
      {
        return 1;
      }
      else if (Number(arrString1[i]) < Number(arrString2[i]))
      {
        return 2;
      }
    }

    // strings are so far equal
    if (nMax1 == nMax2)
    {
      return 3;
    }
    else if (nMax1 > nMax2)
    {
      return 1;
    }
    else
    {
      return 2;
    }

    return -1;  // should never come here
  }

  /***************************************************************************************
  ** Name:         GetAdapterDriverInstallInfo
  **
  ** Purpose:      Returns the driver path and install method for adapters as mentioned in the ss_nics.xml
  **
  ** Parameter:    
  **
  ** Return:       null
  ***************************************************************************************/
  function _GetAdapterDriverInstallInfo()
  {
    var oInstallInfo = {};
    oInstallInfo.sDriverPath = "";
    oInstallInfo.bPnP = true;
    oInstallInfo.sPostCmd = "";  
    
    if(_GetNICDOM())
    {
      var oNode = null;
      var expr  = "//adapter[@code=\""+ _adapterCode + "\"]/drivers/os[@system=\""+ _utils.GetOS().toLowerCase() + "\"]";
      oNode = _adapterDOM.selectSingleNode(expr);

      if (!oNode)
      {
        // try default value to use
        expr  = "//adapter[@code=\""+ _adapterCode + "\"]/drivers/os[@system=\"default\"]";
        oNode = _adapterDOM.selectSingleNode(expr);    
      }
      
      if (oNode)
      {
        oInstallInfo.sDriverPath = _GetDriverPath(oNode);
        oInstallInfo.bPnP = _utils.RecastString(_nics.GetNodeValue("pnp", "false", oNode));
        oInstallInfo.bUseDPInst = _utils.RecastString(_nics.GetNodeValue("use_dpinst", "false", oNode));      
        oInstallInfo.sPostCmd = _nics.GetNodeValue("postcmd", "", oNode);      
      }
    }
    
    return oInstallInfo; 
  }

  //***********************************************************************
  // Name:         GetDriverPath
  // Purpose:      Retrieves path to NIC drivers (local if file exists, 
  //               otherwise assumes remote
  // Parameter:    driver/os node to the adapter
  // Return:       fully expanded driver path
  //***********************************************************************
  function _GetDriverPath(oNode)
  {
    var relativePath = _nics.GetNodeValue("path", "", oNode);  
    var localPath = "%ROOTPATH%" + relativePath;
    var serverPath = "%SERVER%" + "/" + relativePath;
    
    localPath = _config.ParseMacros(localPath, false);
    serverPath = _config.ParseMacros(serverPath, false);
    
    return _file.FileExists(localPath) ? localPath : serverPath;       
  }

  //***********************************************************************
  // Name:         GetImagePath
  // Purpose:      Return <imagepath> Image path for the adapter
  // Parameter:    Adapter type
  // Return:       Image Path
  //***********************************************************************
  function GetImagePath(adapterType, type)
  {
    var expr = "images/image[@type=\""+ type + "\"]";
    var imagepath = this.GetNodeValue(expr, "");
    return imagepath;
  }

  function _GetSupportedConfigByCode(nicCode, section)
  {
    // now check if we have modem xml loaded
    var retArray = [];

    try
    {
      var os = _utils.GetOS().toLowerCase();
      var wzcsvcDllPath = _config.ExpandSysMacro("%winsys%wzcsvc.dll");
      var wzcsvcVer = _file.GetFileSystemObject().GetFileVersion(wzcsvcDllPath);

      if (!_GetNICDOM()) return retArray;

      var oCommonNode = null;
      var oAggregateNode = null;
      var expr  = "//adapter[@code=\'"+ nicCode + "\']/supported_config/config[@os='" + "common" + "']/" + section;
      oCommonNode = _adapterDOM.selectSingleNode(expr);

      if (oCommonNode != null)
      {
        oAggregateNode = oCommonNode;
        var sysExpr = "//adapter[@code=\'"+ nicCode + "\']/supported_config/config[@os='" + os + "']/" + section;
        var sysNodes = _adapterDOM.selectNodes(sysExpr);
        for (var i = 0; i < sysNodes.length; i++)
        {
          var oNode = sysNodes[i];
          var valVer = oNode.parentNode.getAttribute("wzcsvc");
          var nCompare = -1;

          if (valVer)
          {
            nCompare = _CompareFileVersions(wzcsvcVer, valVer)
          }

          // augment if
          // 1. version is not specified in xml,
          // 2. or version in the machine is greater or equal to the version specified in the xml
          if (valVer == null || nCompare == 1 || nCompare == 3)
          {
            _xml.AugmentDoc(oAggregateNode, oNode, "", "/" + section + "/");
          }
        }
      }

      if(oAggregateNode && oAggregateNode.hasChildNodes())
      {
        var aChildren = oAggregateNode.childNodes;
        for(var i=0; i<aChildren.length; i++)
        {
          if(aChildren[i].nodeType in {1: "Element", 3: "Text"})
          {
            if (aChildren[i].cdata != null && typeof(aChildren[i].cdata) != "undefined")
            {
              sValue = aChildren[i].cdata.toString();
              retArray.push(sValue);
            }
            else if (aChildren[i].text != null && typeof(aChildren[i].text) != "undefined")
            {
              sValue = aChildren[i].text.toString();
              retArray.push(sValue);
            }
          }
        }
      }

      return retArray;
    }
    catch(ex)
    {
    }
  }

})()


