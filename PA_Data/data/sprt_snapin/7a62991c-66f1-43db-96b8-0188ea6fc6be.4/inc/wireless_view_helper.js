﻿$ss.snapin = $ss.snapin || {};

$ss.snapin.wireless = $ss.snapin.wireless || {};
  
$ss.snapin.wireless.viewhelper=$ss.snapin.wireless.viewhelper || {}; 

(function()
{
  $.extend($ss.snapin.wireless.viewhelper,
  {    
  
       /*******************************************************************************
          ** Name:    GetSecurityLevels
          **
          ** Purpose: Returns the security list
          **
          ** Parameter:   
          **               
          ** Return:       
       *******************************************************************************/     
        GetSecurityLevels:function(secMap)
        {      
          try
          {
            var optionStr = "";
            var sortedSecurityList =_utils.MakeSortedArrayFromHashValues(secMap,_wrlu.sortAscending);
            return sortedSecurityList; 
          }
          catch (err)
          {
            _logger.error("$ss.snapin.wireless.viewhelper.GetSecurityLevels",err.message);
             
          }      
        },
        DisableElemRecursive:function(bFlag, oElem, arrExcludeTags)
        {
          try
          {
            if (oElem)
            {
               var elem;
               for (var i=0,j = oElem.length; i<j; i++)
               {
                 elem = oElem(i);

                // if an exclusion array is passed in,
                // then only disable those elements with tag names that are not in the exclusion array
                if (typeof(arrExcludeTags) != "undefined" && arrExcludeTags.constructor.toString() == ([]).constructor.toString()) 
                 {
                   if (!_wrlviewhelper.IsTagNameInArray(elem.tagName, arrExcludeTags)) 
                   {
                    elem.disabled = bFlag;
                   }
                 }
                else 
                {
                  elem.disabled = bFlag;
                }
                if (elem.children.length) 
                {
                  _wrlviewhelper.DisableElemRecursive(bFlag, elem);
                }
               }
            }//end of if
          } 
          catch(err) 
          {
            _logger.error("$ss.snapin.wireless.viewhelper.DisableElemRecursive",err.message);
          }
       }, 
        
       IsTagNameInArray:function (sTagName, arrExcludeTags)
       {
         try
         {
           for (var i=0; i < arrExcludeTags.length; i++)
           {
             if(sTagName.toLowerCase() == arrExcludeTags[i].toLowerCase())
               return true;
           }
         }
         catch (err)
         {
            _logger.error("$ss.snapin.wireless.viewhelper.IsTagNameInArray",err.message);
         }
         return false;
       },
       
       /***********************************************************************/
                 //Visual flow 
       /***********************************************************************/
       
       
       	InitializeVisualFlow: function() 
       	{
					_databag.SetValue("ss_wrl_db_vflink1_animation", false);
	        _databag.SetValue("ss_wrl_db_vflink2_animation", false);
	        _databag.SetValue("ss_wrl_db_vflink3_animation", false);
	        _databag.SetValue("ss_wrl_db_NICServiceName", null);
          _databag.SetValue("ss_wrl_db_ApplyResults", "");
	      },
	      AutoUpdateVisualFlowState: function()
	      {
							
					//Comp to Adapter
					var compToAdapter = _databag.GetValue("ss_wrl_db_NICServiceName", "");
					if(compToAdapter!=null 
					|| _databag.GetValue("ss_wrl_db_vflink2_animation", false)==true 
					|| _databag.GetValue("ss_wrl_db_vflink3_animation", false)==true){
						this.VFLinkComputerToAdapterPass();		
						//Link 1 need not animate at all once passed the modem detect
						_databag.SetValue("ss_wrl_db_vflink1_animation", null);
					}
					else if (compToAdapter==null)
					{
	          var tmpanimation=_databag.GetValue("ss_wrl_db_vflink1_animation", null);				
					  if(tmpanimation==null ||tmpanimation==false)
					  {
						  this.VFLinkComputerToAdapterFail();						
						  return;
						}
					}
										
					//Comp to Adapter Link Animation check					
					var compToAdapterLink = _databag.GetValue("ss_wrl_db_vflink1_animation", false);
					//alert("compToAdapterLink: "+compToAdapterLink)
					if(compToAdapterLink==true){
						this.VFLinkComputerToAdapterAnimate();
						//Link 1 animation stopped, till set again
						_databag.SetValue("ss_wrl_db_vflink1_animation", null);
					}else if (compToAdapterLink==false){
						this.VFLinkComputerToAdapterNoResult();
					}					
															
					//Adapter to Router					
					var adapterToRouter = _databag.GetValue("ss_wrl_db_ApplyResults", "false");
					if(adapterToRouter=="success" 
					|| _databag.GetValue("ss_wrl_db_vflink3_animation", false)==true){
						this.VFLinkAdapterToRouterPass();		
						//Link 2 need not animate at all once passed the WG Check
						_databag.SetValue("ss_wrl_db_vflink2_animation", null);
					}else if (adapterToRouter=="nicfail") 
           {
						this.VFLinkAdapterToRouterFail();
					 }
											
					//Adapter to Router Link Animation check
					var adapterToRouterLink = _databag.GetValue("ss_wrl_db_vflink2_animation", false);
					//alert("adapterToRouterLink: "+adapterToRouterLink)
					if(adapterToRouterLink==true){
						this.VFLinkAdapterToRouterAnimate();
						//Link 2 animation stopped, till set again
						_databag.SetValue("ss_wrl_db_vflink2_animation", null);
					}else if(adapterToRouterLink==false){
						this.VFLinkAdapterToRouterNoResult();
					}
					
					//Router to Internet					
					var routerToInternet = _databag.GetValue("ss_wrl_db_ApplyResults", "false");
					if(routerToInternet=="success"){
						this.VFLinkRouterToInternetPass();		
						//Link 3 need not animate at all once passed the Internet Check
						_databag.SetValue("ss_wrl_db_vflink3_animation", null);
					}else if (routerToInternet=="connfail" ||routerToInternet=="nicfail" ){
						this.VFLinkRouterToInternetFail();						
	      }
											
					//Router to Internet Link Animation check
					var routerToInternetLink = _databag.GetValue("ss_wrl_db_vflink3_animation", false);
					//alert("routerToInternetLink: "+routerToInternetLink)
					if(routerToInternetLink){
						this.VFLinkRouterToInternetAnimate();
						//Link 3 animation stopped, till set again
						_databag.SetValue("ss_wrl_db_vflink3_animation", null);
					}else if(routerToInternetLink==false){
						this.VFLinkRouterToInternetNoResult();
					}
	      },
	      AnimateVFComputer: function(){
					
	      },
	      VFLinkComputerToAdapterNoResult: function(){
					//alert("called VFLinkComputerToModemNoResult");
					if(!$("#wrl_vflow_link1").hasClass("cls_wrl_vflowlink1_nr")){
						$("#wrl_vflow_link1").removeClass();
						$("#wrl_vflow_link1").addClass("cls_wrl_vflowlink1_nr");					
					}					
	      },
	      VFLinkComputerToAdapterAnimate: function(){
					//alert("called VFLinkComputerToModemAnimate");
					if(!$("#wrl_vflow_link1").hasClass("cls_wrl_vflowlink1_anim")){
						$("#wrl_vflow_link1").removeClass();
						$("#wrl_vflow_link1").addClass("cls_wrl_vflowlink1_anim");					
					}					
	      },	      
	      VFLinkComputerToAdapterPass: function(){
					//alert("called VFLinkComputerToModemPass");
					if(!$("#wrl_vflow_link1").hasClass("cls_wrl_vflowlink1_pass")){
						$("#wrl_vflow_link1").removeClass();
						$("#wrl_vflow_link1").addClass("cls_wrl_vflowlink1_pass");
					}
	      },
	      VFLinkComputerToAdapterFail: function(){
					//alert("called VFLinkComputerToModemFail");
					if(!$("#wrl_vflow_link1").hasClass("cls_wrl_vflowlink1_fail")){
						$("#wrl_vflow_link1").removeClass();
						$("#wrl_vflow_link1").addClass("cls_wrl_vflowlink1_fail");
					}
	      },
	      VFLinkAdapterToRouterNoResult: function(){
					//alert("called VFLinkModemToWalledGardenNoResult");
					if(!$("#wrl_vflow_link2").hasClass("cls_wrl_vflowlink2_nr")){
						$("#wrl_vflow_link2").removeClass();
						$("#wrl_vflow_link2").addClass("cls_wrl_vflowlink2_nr");
					}
	      },
	      VFLinkAdapterToRouterAnimate: function(){
					//alert("called VFLinkModemToWalledGardenAnimate");
					if(!$("#wrl_vflow_link2").hasClass("cls_wrl_vflowlink2_anim")){
						$("#wrl_vflow_link2").removeClass();
						$("#wrl_vflow_link2").addClass("cls_wrl_vflowlink2_anim");
					}
	      },
	      VFLinkAdapterToRouterPass: function(){
					//alert("called VFLinkModemToWalledGardenPass");
					if(!$("#wrl_vflow_link2").hasClass("cls_wrl_vflowlink2_pass")){
						$("#wrl_vflow_link2").removeClass();
						$("#wrl_vflow_link2").addClass("cls_wrl_vflowlink2_pass");
					}
	      },
	      VFLinkAdapterToRouterFail: function(){
					//alert("called VFLinkModemToWalledGardenFail");
					if(!$("#wrl_vflow_link2").hasClass("cls_wrl_vflowlink2_fail")){
						$("#wrl_vflow_link2").removeClass();
						$("#wrl_vflow_link2").addClass("cls_wrl_vflowlink2_fail");
					}
	      },
	      VFLinkRouterToInternetNoResult: function(){
					//alert("called VFLinkWalledGardenToInternetNoResult");
					if(!$("#wrl_vflow_link3").hasClass("cls_wrl_vflowlink3_nr")){
						$("#wrl_vflow_link3").removeClass();
						$("#wrl_vflow_link3").addClass("cls_wrl_vflowlink3_nr");
					}
	      },
	      VFLinkRouterToInternetAnimate: function(){
					//alert("called VFLinkWalledGardenToInternetAnimate");
					if(!$("#wrl_vflow_link3").hasClass("cls_wrl_vflowlink3_anim")){
						$("#wrl_vflow_link3").removeClass();
						$("#wrl_vflow_link3").addClass("cls_wrl_vflowlink3_anim");
					}
	      },
	      VFLinkRouterToInternetPass: function(){
					//alert("called VFLinkWalledGardenToInternetPass");
					if(!$("#wrl_vflow_link3").hasClass("cls_wrl_vflowlink3_pass")){
						$("#wrl_vflow_link3").removeClass();
						$("#wrl_vflow_link3").addClass("cls_wrl_vflowlink3_pass");					
					}
	      },
	      VFLinkRouterToInternetFail: function(){
					//alert("called VFLinkWalledGardenToInternetFail");
					if(!$("#wrl_vflow_link3").hasClass("cls_wrl_vflowlink3_fail")){
						$("#wrl_vflow_link3").removeClass();
						$("#wrl_vflow_link3").addClass("cls_wrl_vflowlink3_fail");
					}
	      },
       
	      ResetParamstoHandleBackFlows : function(){
            _databag.SetValue("ss_wrl_db_ApplyResults", ""); 
            _databag.SetValue("ss_wrl_db_vflink2_animation", true);
            _databag.SetValue("ss_wrl_db_vflink3_animation", false);         
            _wrlviewhelper.AutoUpdateVisualFlowState();				  
				  },
				  
	    ResetParamstoHandleNicBackFlows : function()
				  {
				    _databag.SetValue("ss_wrl_db_NICServiceName", null);
            _databag.SetValue("ss_wrl_db_ApplyResults", "");
            _databag.SetValue("ss_wrl_db_vflink1_animation", true);
            _databag.SetValue("ss_wrl_db_vflink2_animation", false);
            _databag.SetValue("ss_wrl_db_vflink3_animation", false); 
            _wrlviewhelper.AutoUpdateVisualFlowState();
				  },
       
       
       //********** no wireless nic page *****************//
       nwns_ss_wrl_no_wireless_nic :
       {
         AfterRender: function ()
         {
           _wrlbtn.updatelayoutbuttons("HIDE_NEXT");          
           _wrlbtn.updatelayoutbuttons("ENABLE_DONE"); 
         }
       },
       
       /******************************************************************/
      /*********************ss_wrl_manualselection.htm*************************/
      doad_ss_wrl_manualselection :
      {
        BeforeRender: function()
        {     
         _wrlbtn.updatelayoutbuttons("DISABLE_NEXT");        
          
          if(_databag.GetValue("ss_wrl_db_Standalone"))
          {          
            modem = _sdcDSLModem.GetInstanceByCode("null_modem_dsl");          
          }
          wrlsecmap = modem.GetWirelessSecurityMap();
          var DataEncLevels=_wrlviewhelper.GetSecurityLevels(wrlsecmap);
          return {DE_levels:DataEncLevels};
        },
        NavNext: function()
        {
          var ntwrkProblemText="";
          if($("#ss_wrl_mansel_wifi_ssid").val().length && _sswrlutility.ValidateSSID($("#ss_wrl_mansel_wifi_ssid").val()))
          {
            var manualSSID = $("#ss_wrl_mansel_wifi_ssid").val();
            //To set it to null when network name is given
            $("#ss_wrl_mansel_keyproblem").css("color",_errMsgColor);
            $("#ss_wrl_mansel_keyproblem").html(ntwrkProblemText);
            if (typeof(validateFunc) == 'function')
            {
              
              var passphrase = $("#ss_wrl_mansel_Passphrase").val();
              if (!validateFunc(passphrase))
              {
                $("#ss_wrl_mansel_problem").css("color",_errMsgColor);
                return false;
              }
              else
              {
                _databag.SetValue("ss_wrl_db_SSID",manualSSID);
                _databag.SetValue("ss_wrl_db_SecurityLevelSelection", manualSelLevel);
                _databag.SetValue("ss_wrl_db_Password", passphrase);
                return true;
              }
            }
            else
            {
              //May be used for some messages.Not required for now  
            }
          }
          else
          {
            if(!_sswrlutility.ValidateSSID($("#ss_wrl_mansel_wifi_ssid").val()))
              ntwrkProblemText  = _utils.LocalXLate("snapin_wireless","ss_wrl_snw_InvalidSSID");
            /*else
              ntwrkProblemText  = _utils.LocalXLate("snapin_wireless","ss_wrl_mansel_mandatorykey");   */
            $("#ss_wrl_mansel_keyproblem").css("color",_errMsgColor);
            $("#ss_wrl_mansel_keyproblem").html(ntwrkProblemText);
            return false;
          }
          return false;
        },
         OnSelect:function(secDisplayId)
        { 
          try
          {
            var secLevel = wrlsecmap[secDisplayId];
            var problemText="";
            if (secLevel)
            {       
              var authType =_wrlu.ConvertAuthentication(secLevel.oSecurity.ulSecMode, secLevel.oSecurity.ulAuthentication);
              var encType =_wrlu.ConvertEncryption(secLevel.oSecurity.ulEncryption);
              manualSelLevel = secDisplayId;
            
              // enable/disable passphrase div depending on security type
              if (authType == _wrlConst.WZCSVC_AUTHTYPE_WPA2_PSK || authType == _wrlConst.WZCSVC_AUTHTYPE_WPA_PSK)
              {   
                //_wrlviewhelper.DisableElemRecursive(false, $("#ss_wrl_mansel_passphrase_div").attr("children"));
                $("#ss_wrl_mansel_passphrase_div").css("display","block");
                validateFunc = _sswrlutility.ValidateWPAPSKKey;
                problemText  = _utils.LocalXLate("snapin_wireless","ss_wrl_sms_errBadWPAKEY");
              }
              else if (authType == _wrlConst.WZCSVC_AUTHTYPE_OPEN && encType == _wrlConst.WZCSVC_ENCTYPE_WEP)
              {
                //_wrlviewhelper.DisableElemRecursive(false, $("#ss_wrl_mansel_passphrase_div").attr("children"));
                $("#ss_wrl_mansel_passphrase_div").css("display","block");
                validateFunc = _sswrlutility.ValidateWEPKey;
                problemText  =_utils.LocalXLate("snapin_wireless","ss_wrl_sms_errBadWEPKEY");
              }
              else
              {
                //_wrlviewhelper.DisableElemRecursive(true, $("#ss_wrl_mansel_passphrase_div").attr("children"));
                $("#ss_wrl_mansel_passphrase_div").css("display","none");
                validateFunc = function () { return true; };
                problemText  = "";
              }
              $("#ss_wrl_mansel_problem").html(problemText);
              _wrlbtn.updatelayoutbuttons("ENABLE_NEXT");
            }
            else
            {
              _wrlbtn.updatelayoutbuttons("DISABLE_NEXT");    
            } 
          }
            catch(err)
            {
              _logger.error("$ss.snapin.wireless.viewhelper.doad_ss_wrl_manualselection.OnSelect",err.message);
            }       
          }
      },
      
       /******************************************************************/
      /*********************ss_wrl_selectmode.htm*************************/
      doad_ss_wrl_selectmode : 
      {
        BeforeRender : function()
        {
          // Visual flow value
          _databag.SetValue("ss_wrl_db_vflink2_animation", true);
          _databag.SetValue("ss_wrl_db_vflink3_animation", null);
			    $ss.snapin.wireless.viewhelper.AutoUpdateVisualFlowState(); 
        },
         NavNext: function()
        {   
           try
           {
             var selmode = $("#dvmainSelectMode #ss_wrl_selmode_automatic").attr("checked");
             if(selmode==true)
             {
               _databag.SetValue("ss_wrl_db_SSIDmode", $("#dvmainSelectMode #ss_wrl_selmode_automatic").val());
             }
             else
             {
               _databag.SetValue("ss_wrl_db_SSIDmode", $("#dvmainSelectMode #ss_wrl_selmode_manual").val());
             }
           }
           catch(err)
           {
             _logger.error("$ss.snapin.wireless.viewhelper.doad_ss_wrl_selectmode.NavNext",err.message);
             return false;
           }
        },
                      
        NavBack : function()
        {
          _wrlviewhelper.ResetParamstoHandleNicBackFlows();  
        }
      },
      
      /******************************************************************/
      /*********************ss_wrl_selectnic.htm********************/
      dwn_ss_wrl_selectnic : 
      {
        BeforeRender: function()
        {
          var adapterList=_wrlviewhelper.dwn_ss_wrl_selectnic.GetWirelessAdaptors();
          return {adaptor_array:adapterList};
        },
        AfterRender: function ()
        {
		     
		      _wrlbtn.updatelayoutbuttons("DISABLE_BACK");
          if (_databag.GetValue("ss_gc_db_SelectedAdapter") != null)
          {
            $("#ss_wrl_selectnic_adapter").val(_databag.GetValue("ss_gc_db_SelectedAdapter"));
          }
        },
        NavNext: function()
        {
         try
         {
          _databag.SetValue("ss_wrl_db_NICServiceName", $("#ss_wrl_selectnic_adapter").attr("value"));
          _databag.SetValue("ss_gc_db_SelectedAdapter", $("#ss_wrl_selectnic_adapter").attr("value"));
          _wrlviewhelper.AutoUpdateVisualFlowState(); 
         }
         catch(err)
         {
           _logger.error("$ss.snapin.wireless.viewhelper.dwn_ss_wrl_selectnic.NavNext",err.message);
           return false; 
         } 
        },
       
         /*******************************************************************************
          ** Name:         GetWirelessAdaptors
          **
          ** Purpose:     returns all wireless adaptors available 
          **
          ** Parameter:   
          **               
          ** Return:       
        *******************************************************************************/
        GetWirelessAdaptors:function()
        {
          try
          { 
            var adapterList = _sswrlutility.GetPresentWirelessAdapters(null, true);
            return adapterList;
          }
          catch(err)
          {
             _logger.error("$ss.snapin.wireless.viewhelper.dwn_ss_wrl_selectnic.GetWirelessAdaptors",err.message);
          } 
        }
      } ,
      
      /******************************************************************/
      /*********************SS_WRL_SURVEYNETWORKS.HTM********************/
      doad_ss_wrl_surveynetworks :
      {
        BeforeRender:function ()
        { 
           WifiList= _wrlviewhelper.doad_ss_wrl_surveynetworks.GetAllNetworks();
           return {wifi_list:WifiList};
        },
        AfterRender: function ()
        { 
          //_wrlbtn.updatelayoutbuttons("DISABLE_NEXT");         
        },
        NavNext: function()
        {
         var selPrivacy=""; 
           var ssid=null;
           if (_wrlviewhelper.doad_ss_wrl_surveynetworks.validateSSID())
           {
              ssid=_wrlviewhelper.doad_ss_wrl_surveynetworks.GetSelectedSSID()
              _databag.SetValue("ss_wrl_db_SSID",ssid);
              for(var i in WifiList)
                  {
                    var wfl = WifiList[i]; 
                    if(wfl["SSID"]==ssid)
                    {
                      selPrivacy=wfl["Privacy"];
                      _wrlviewhelper.doad_ss_wrl_surveynetworks.SetProtected(selPrivacy);
                      break;
                    }
                  }
                          
              _databag.SetValue("ss_wrl_db_NetworkProtected", bIsNetworkProtected);
              if(!bIsNetworkProtected)
                {
                   _databag.SetValue("ss_wrl_db_EncKey", "");
                   _databag.SetValue("ss_wrl_db_Password", "");   
                   _databag.SetValue("ss_wrl_db_SecurityLevelSelection", "off");
                }
              return true;
           }
         return false;
         
        },
        
        /*OnSelect:function()
        {
           _wrlbtn.updatelayoutbuttons("ENABLE_NEXT");   
        },*/ 
        /*******************************************************************************
          ** Name:    GetAllNetworks
          **
          ** Purpose:  Returns all available wireless networks
          **
          ** Parameter:   
          **               
          ** Return:       
        *******************************************************************************/ 
      GetAllNetworks:function()
      {
        try
        {
          var svcName = _databag.GetValue("ss_wrl_db_NICServiceName");
          var oCfg = _sswrlutility.GetWiFiConfig(svcName);
          var APs = oCfg.GetAvailableNetworks(true);
          return APs; 
        }
        catch (err)
        {
          _logger.error("$ss.snapin.wireless.viewhelper.doad_ss_wrl_surveynetworks.GetAllNetworks",err.message);
        }
      },
      /*******************************************************************************
          ** Name:    validateSSID
          **
          ** Purpose: Performs validations on SSID
          **
          ** Parameter:   
          **               
          ** Return:       
       *******************************************************************************/     
      validateSSID:function()
      {
        try
        {
          var ssid =_wrlviewhelper.doad_ss_wrl_surveynetworks.GetSelectedSSID();
          if (!ssid.length)
          {
            $("#pageStatusMessage").css("color",_errMsgColor);
            $("#pageStatusMessage").html($ss.agentcore.utils.LocalXLate("snapin_wireless","ss_wrl_snw_NoSSID"));
            return false;
          }
          if (!_sswrlutility.ValidateSSID(ssid))
          {
            $("#pageStatusMessage").css("color",_errMsgColor);
            $("#pageStatusMessage").html($ss.agentcore.utils.LocalXLate("snapin_wireless","ss_wrl_snw_InvalidSSID"));
            return false;
          }
        }
        catch(err)
        {
          _logger.error('$ss.snapin.wireless.viewhelper.doad_ss_wrl_surveynetworks.validateSSID',err.message);
        } 
        return true;
      },
      /*******************************************************************************
          ** Name:    GetSelectedSSID
          **
          ** Purpose: 
          **
          ** Parameter:   
          **               
          ** Return:       
       *******************************************************************************/     
      GetSelectedSSID:function()
      {
        try
        {
           var ssid="";
           var obj=$("#ss_wrl_snw_frmCreateWifi").attr("ssid");
            var list = [];
             if (obj)
             {
               if (typeof(obj.length) != "undefined")  // have multiple elements
               {
                 list = obj;
               }
               else    // single element
               {
                 list.push(obj);
               }
             }
            
             for (var i = 0; i < list.length; i++)
             {
              if (list[i].checked)
              {
                ssid = list[i].value;
                break;
              }
             }
                  
        }
        catch(err)
        {
          _logger.error('$ss.snapin.wireless.viewhelper.doad_ss_wrl_surveynetworks.GetSelectedSSID',err.message);
        }
        return ssid;
      },
      
      SetProtected:function(bProtected)
      {
        bIsNetworkProtected = bProtected ? true : false;
      }
       
    },
    
     /******************************************************************/
     /**********************ss_wrl_setupsecurity.htm********************/
      doad_ss_wrl_setupsecurity :
      {
        BeforeRender: function()
        {
          _wrlbtn.updatelayoutbuttons("DISABLE_NEXT");
          var nicServiceName = _databag.GetValue("ss_wrl_db_NICServiceName");
          if (_databag.GetValue("ss_wrl_db_Standalone"))
          {          
            modem = _sdcDSLModem.GetInstanceByCode("null_modem_dsl");          
          }
           
           // write security selections        
          secMap = modem.GetWirelessSecurityMap();
          var secLevels=_wrlviewhelper.GetSecurityLevels(secMap);
          return {security_levels:secLevels};
        },
        NavNext: function()
        {    
          if (typeof(validateFunc) == 'function')
          {
            var passphrase = $("#ss_wrl_sms_Passphrase").val();
            if (!validateFunc(passphrase))
            {
              $("#ss_wrl_sms_problem").css("color",_errMsgColor);
              return false;
            }
            else
            {
              _databag.SetValue("ss_wrl_db_SecurityLevelSelection", selectedLevel);
              _databag.SetValue("ss_wrl_db_Password", passphrase);
              return true;
            }
          }
          return false;
        },
        
        OnSelect:function(secDisplayId)
        { 
          try
          {
            var secLevel = secMap[secDisplayId];
            var problemText="";
            if (secLevel)
            {       
              var authType =_wrlu.ConvertAuthentication(secLevel.oSecurity.ulSecMode, secLevel.oSecurity.ulAuthentication);
              var encType =_wrlu.ConvertEncryption(secLevel.oSecurity.ulEncryption);
              selectedLevel = secDisplayId;
            
              // enable/disable passphrase div depending on security type
              if (authType == _wrlConst.WZCSVC_AUTHTYPE_WPA2_PSK || authType == _wrlConst.WZCSVC_AUTHTYPE_WPA_PSK)
              {   
                //_wrlviewhelper.DisableElemRecursive(false, $("#passphrase_div").attr("children"));
                $("#passphrase_div").css("display","block");
                validateFunc = _sswrlutility.ValidateWPAPSKKey;
                problemText  = _utils.LocalXLate("snapin_wireless","ss_wrl_sms_errBadWPAKEY");
              }
              else if (authType == _wrlConst.WZCSVC_AUTHTYPE_OPEN && encType == _wrlConst.WZCSVC_ENCTYPE_WEP)
              {
                //_wrlviewhelper.DisableElemRecursive(false, $("#passphrase_div").attr("children"));
                $("#passphrase_div").css("display","block");
                validateFunc = _sswrlutility.ValidateWEPKey;
                problemText  =_utils.LocalXLate("snapin_wireless","ss_wrl_sms_errBadWEPKEY");
              }
              else
              {
                //_wrlviewhelper.DisableElemRecursive(true, $("#passphrase_div").attr("children"));
                $("#passphrase_div").css("display","none");
                validateFunc = function () { return true; };
                problemText  = "";
              }
              $("#ss_wrl_sms_problem").html(problemText);
               _wrlbtn.updatelayoutbuttons("ENABLE_NEXT");  
            }
            else
            {
               _wrlbtn.updatelayoutbuttons("DISABLE_NEXT");
            } 
          }
            catch(err)
            {
              _logger.error('$ss.snapin.wireless.viewhelper.doad_ss_wrl_setupsecurity.OnSelect',err.message);
            }       
          },
          
         IsSelected: function(secLevel)
         {
           var prevSelection = _databag.GetValue("ss_wrl_db_SecurityLevelSelection");
           var prevPassphrase =_databag.GetValue("ss_wrl_db_Password"); 
           var currentSetting = _databag.GetValue("ss_wrl_db_Current_SecurityLevel");
           var currentPassphrase =_databag.GetValue("ss_wrl_db_Current_EncKey");

           if (prevSelection != null && secLevel == prevSelection)
           {
              $("#ss_wrl_sms_Passphrase").html(prevPassphrase);
              return true;
           }        
           else if (prevSelection == null && currentSetting == secLevel)
           {
              $("#ss_wrl_sms_Passphrase").html(currentPassphrase);
              return true;
           }
            return false;
        }
                           
      },
      
      
      saf_ss_wrl_restartadapter : 
      {
             
        AfterRender: function ()
        {
          var nicDisplayName = "";
          var svcName = _databag.GetValue("ss_wrl_db_NICServiceName");
          var displayName = _netcheck.GetAdapterDisplayName(null, svcName);
          if (displayName != null && displayName != "") nicDisplayName += displayName + " "; 
            $("#ss_wrl_rsa_Prompt").html(_utils.LocalXLate("snapin_wireless","ss_wrl_rsa_PromptContent",nicDisplayName));               
        },

        NavNext: function()
        {               
           // Visual flow changes
          _wrlviewhelper.ResetParamstoHandleBackFlows();                     
          var svcName = _databag.GetValue("ss_wrl_db_NICServiceName");
          $("#ss_wrl_rsa_Prompt").css("display","none");
          $("#ss_wrl_rsa_PromptInProgress").css("display","block");
          
          _utils.Sleep(0); // let UI refresh itself to draw page
          _netcheck.DisableAdapter(null, svcName );
          _utils.Sleep(2000);
          _netcheck.EnableAdapter(null, svcName );
          _databag.SetValue("ss_wrl_db_HaveTriedRestart", true);
          return true;          
        },
        NavBack : function()
        {
          _wrlviewhelper.ResetParamstoHandleBackFlows();
        }        
      },
      
      ass_ss_wrl_complete : 
      {
        AfterRender: function ()
        {
          _wrlbtn.updatelayoutbuttons("HIDE_BACK");
          _wrlbtn.updatelayoutbuttons("HIDE_NEXT");
          _wrlbtn.updatelayoutbuttons("ENABLE_PRINT"); 
          _wrlbtn.updatelayoutbuttons("ENABLE_DONE");         
          var secMap = _sdcDSLModem.GetInstance().GetWirelessSecurityMap();
          var securitySelection = _databag.GetValue("ss_wrl_db_SecurityLevelSelection");
          var secLevel = secMap[securitySelection];
          var authType = "";
          var encType = "";

          if (secLevel)
          {       
            authType = _wrlu.ConvertAuthentication(secLevel.oSecurity.ulSecMode, secLevel.oSecurity.ulAuthentication);
            encType = _wrlu.ConvertEncryption(secLevel.oSecurity.ulEncryption);
          }
                    
          $("#ssid").html(_databag.GetValue("ss_wrl_db_SSID"))
          $("#security").html(_databag.GetValue("security"))
          $("#authType").html(authType);
          $("#encType").html((encType == _wrlConst.WZCSVC_ENCTYPE_NONE) ? _utils.LocalXLate("snapin_wireless","ss_wrl_cpl_EncryptionNone") : encType);
          $("#password").html(_databag.GetValue("ss_wrl_db_Password"))
          $("#key").html(_databag.GetValue("ss_wrl_db_EncKey"))
                 

          // hide security level if it's empty
          if (!_databag.GetValue("ss_wrl_db_SecurityLevelSelection"))
          {
            $("#div_security").css("display","none");             
          }
          
          // hide password if we are in standalone mode 
          if (_databag.GetValue("ss_wrl_db_Standalone"))
          {
            $("#div_password").css("display","none");            
          }
          
          // hide password and encryption key if databag not set
          if (!_databag.GetValue("ss_wrl_db_EncKey")) 
          {            
            $("#div_password").css("display","none");  
            $("#div_key").css("display","none");              
          }                        
          },
          
          Finish : function()
          {
           // Visual flow changes
           _wrlviewhelper.ResetParamstoHandleBackFlows();
          },
          Print : function()
          {
            var currentData = $("#ss_wrl_cpl_Main").html();                       
            $ss.snapin.wireless.PrintWindow(currentData,$("#snapin_wireless #wrl_print_frame")[0].contentWindow);
          }            
      },
      
            
        cas_ss_wrl_applysettings : 
        {
      
        BeforeRender: function ()
        {
         _wrlbtn.updatelayoutbuttons("HIDE_RETRY");
         _wrlbtn.updatelayoutbuttons("DISABLE_NEXT");
         _wrlbtn.updatelayoutbuttons("DISABLE_BACK");
         g_bDoModem = _databag.GetValue("ss_wrl_db_ConfigModem");
         g_bDoNIC   = _databag.GetValue("ss_wrl_db_ConfigNIC");
         g_bStandalone = _databag.GetValue("ss_wrl_db_Standalone");
        
        // if running in standalone mode, use null_modem's instance, otherwise use what is set in registry
        if (g_bStandalone)
        {          
          modem = _sdcDSLModem.GetInstanceByCode("null_modem_dsl");          
        }
        else
        {
          modem = _sdcDSLModem.GetInstance();
        }      

        g_secMap = modem.GetWirelessSecurityMap();
        
        if (!g_bStandalone && g_bDoNIC)
        {
          // This is not SubAgent Flow
        }       
        
        var retobj = {modemStatusDisplay:g_bDoModem,adpStatusDisplay:g_bDoNIC};
        return retobj ;                              
        },
             
        AfterRender: function ()
        {           
        var secLevelId = _databag.GetValue("ss_wrl_db_SecurityLevelSelection"); 
        if (!_wrlappset.ApplySettings(secLevelId))
        {
          if (_sdcDSLModem.GetLastError() != _smapiUtils.SMAPI_NO_AUTH) 
          {

            // these errors are only valid if we are only setting NIC but not modem (i.e. 2ndpc flow)
            if (!g_bDoModem && g_bDoNIC)
            {              
              // Defect #10316: show the more info block only if we are not in standalone mode
              if (!g_bSecuritySupported && !g_bStandalone)
              {
                // This is not SubAgent Flow
                //ss_utl_Id("ss_wrl_aws_pb4").style.display = "block";
              }
            }
            $("#ss_wrl_aws_statustext").css("display","none");
            $("#ss_wrl_aws_connection").css("display","none");
            $("#ss_wrl_aws_adapter").css("display","none");   
            $("#ss_wrl_aws_problem").css("display","block"); 
            _wrlbtn.updatelayoutbuttons("ENABLE_RETRY");          
          }
         else 
          {
            // bad admin creds
            $("#ss_wrl_aws_statustext").css("display","none");
            $("#ss_wrl_aws_Content").css("display","none");  
            $("#ss_wrl_aws_admincreds").css("display","block");            
          }          
          
           _wrlbtn.updatelayoutbuttons("ENABLE_NEXT");
           _wrlbtn.updatelayoutbuttons("ENABLE_BACK");
          
        }
        else
        {
           _wrlbtn.updatelayoutbuttons("HIDE_RETRY"); 
           _wrlbtn.updatelayoutbuttons("ENABLE_NEXT");
           $("#ss_wrl_aws_connection").css("display","none");
           $("#ss_wrl_aws_adapter").css("display","none");
           $("#ss_wrl_aws_finished").css("display","block");          
           return {move:"forward"};         
        }       
        },

        NavNext: function()
        {
         try
         {
          _wrlbtn.updatelayoutbuttons("HIDE_RETRY");  
          return true;         
         }
         catch(err)
         {
           return false;
         }
        },
        Retest : function ()
        {          
          _wrlviewhelper.ResetParamstoHandleBackFlows();          
        },
        NavBack : function()
        {
         // Visual flow changes
         _wrlviewhelper.ResetParamstoHandleBackFlows();
        }     
      },
      
      doad_ss_wrl_displayconnected :
      {      
        BeforeRender: function ()
        {
          var nicID = _databag.GetValue("ss_wrl_db_NICServiceName");
          var currentSSID = _sswrlutility.GetCurrentSSID(null,nicID) ;
          // Visual flow chnages
          _databag.SetValue("ss_wrl_db_ApplyResults", "success");
           _wrlviewhelper.AutoUpdateVisualFlowState();    
          return {curSSID:currentSSID};
        },
        
        NavBack : function()
        {
         // Visual flow changes          
          _wrlviewhelper.ResetParamstoHandleNicBackFlows();          
        },
        
        NavNext : function()
        {
         _wrlviewhelper.ResetParamstoHandleBackFlows();
        }    
      }
      
      
      
  }); //end of extend
  
  //Private Area
  
  //agentcore constants
    var _sswrlutility       = $ss.snapin.network.wireless.utility;
    var _databag            = $ss.agentcore.dal.databag;
    var _netcheck           = $ss.agentcore.network.netcheck;
    var _utils              = $ss.agentcore.utils;
    var _wrlConst           = $ss.snapin.network.wireless.constants;
    var _exception          = $ss.agentcore.exceptions;
    var _wrlbtn             = $ss.snapin.wireless.buttonhelper;
    var _logger             = $ss.agentcore.log.GetDefaultLogger("$ss.snapin.wireless.utils");
    var _config             = $ss.agentcore.dal.config;
    
    var _errMsgColor        = _config.GetConfigValue("snapin_wireless", "error_msg_color", "yellow");
    
   //other variables
    var _wrlviewhelper       =$ss.snapin.wireless.viewhelper; 
    var _wrlu                =$ss.snapin.wireless.utils;
    var _wrlappset           =$ss.snapin.wireless.applysettings; 
    var _smapiUtils          =$ss.agentcore.smapi.utils ;
    var _sdcDSLModem         =$ss.agentcore.smapi.methods ;
    var bIsNetworkProtected  = false;
    var modem                = null;  
    var secMap               = null;
    var g_bDoNIC             = null;
    var g_bDoModem           = null;
    var g_bSecuritySupported = false;
    var g_bStandalone        = null;
    var WifiList             = null;
    var wrlsecmap            = null;
    var manualSelLevel       = null;
    var validateFunc;
        
    
      
}
)();


