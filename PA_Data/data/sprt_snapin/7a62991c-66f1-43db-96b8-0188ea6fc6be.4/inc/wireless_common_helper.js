﻿$ss.snapin= $ss.snapin || {};
$ss.snapin.wireless= $ss.snapin.wireless || {};
$ss.snapin.wireless= $ss.snapin.wireless || {};

(function(){
    
    $.extend($ss.snapin.wireless, {
    /********************************************************************************/
    //All Public functions
    /********************************************************************************/
    maskequal : function(val,flag)
    {
      return ((val & flag) == flag);
    },
    
   PrintWindow: function(sCurrentTabHTML, oTempPrintFrame){
      oTempPrintFrame.document.open();
      oTempPrintFrame.document.write("<html><body>" + sCurrentTabHTML + "</body></html>");
      oTempPrintFrame.document.close();
      oTempPrintFrame.focus();
      oTempPrintFrame.print();
    },
   ClearDataBagValues:function()
   {
     try
     {
       _databag.RemoveValue("ss_wrl_db_NICServiceName");
       _databag.RemoveValue("ss_wrl_db_ApplyResults");
       _databag.RemoveValue("ss_wrl_db_RetryMode");
       _databag.RemoveValue("ss_wrl_db_EncKey");
       _databag.RemoveValue("ss_wrl_db_ConfigNIC");
       _databag.RemoveValue("ss_wrl_db_ConfigModem");
       _databag.RemoveValue("ss_wrl_db_NetworkProtected");
       _databag.RemoveValue("ss_gc_db_SelectedAdapter");
       _databag.RemoveValue("ss_wrl_db_SSID");
       _databag.RemoveValue("ss_wrl_db_SecurityLevelSelection");
       _databag.RemoveValue("ss_wrl_db_Password");
       _databag.RemoveValue("ss_wrl_db_SSIDmode");
       _databag.RemoveValue("ss_wrl_db_HaveTriedRestart");
       _databag.RemoveValue("ss_wrl_db_Standalone");
       _databag.RemoveValue("ss_wrl_db_IsWinXPOrAbove");
       _databag.RemoveValue("ss_wrl_db_NumberOfNICs");
       _databag.RemoveValue("ss_wrl_db_Current_SecurityLevel");
       _databag.RemoveValue("ss_wrl_db_Current_EncKey");
       _databag.RemoveValue("security");
       _databag.RemoveValue("ss_wrl_db_PromptUserNIC");
       _databag.RemoveValue("ss_wrl_db_vflink1_animation");
       _databag.RemoveValue("ss_wrl_db_vflink2_animation");
       _databag.RemoveValue("ss_wrl_db_vflink3_animation");
     }
     catch(err)
     {
       _logger.error("ClearDataBagValues",err.message); 
     }
    }
    
    });//end of extend
    
    /********************************************************************************/
    //Private Area 
    /********************************************************************************/
    var _databag = $ss.agentcore.dal.databag;    
    var _logger = $ss.agentcore.log.GetDefaultLogger("$ss.snapin.wireless.commonhelper");  
})();
