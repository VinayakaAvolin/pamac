﻿$ss.snapin = $ss.snapin || {};

$ss.snapin.wireless = $ss.snapin.wireless || {};
  
$ss.snapin.wireless.decisions = $ss.snapin.wireless.decisions || {};

(function(){
  $.extend($ss.snapin.wireless.decisions,
  {    
  
  IsWinXPOrAbove  : function()
  {
   var bIsWinXPOrAbove = _databag.GetValue("ss_wrl_db_IsWinXPOrAbove");
   var retval = (bIsWinXPOrAbove != null) ? bIsWinXPOrAbove : _system.IsWinXP(true); // are we on xp or above?
   return retval;
  },
  PromptUserNIC  : function()
  {
    var bConfigNICPrompt = _databag.GetValue("ss_wrl_db_PromptUserNIC");
    var bConfigModem = _databag.GetValue("ss_wrl_db_ConfigModem");
      
    // suppress config prompt if user has pending modem changes and is connecting to the modem wirelessly
    // this is to prevent possible connectivity loss after modifying modem settings but not nic's
    if (bConfigNICPrompt)
    {
      // ******************
      // This is not SubAgent flow because we are explicitly setting "ss_wrl_db_PromptUserNIC" to "false" before the flow starts
    }
    // if we are NOT prompting, then use whatever ss_wrl_db_ConfigModem is set to. If it doesn't exist,
    // then assumes user wants to config NIC
    if (!bConfigNICPrompt)
    {    
      if (bConfigModem != null)
      {
        // ******************
        // This is not subagent flow because we are explicitly setting "ss_wrl_db_ConfigModem" to "false" before the flow starts
        _databag.SetValue("ss_wrl_db_ConfigNIC", bConfigModem);
      }
      else
      {
        _databag.SetValue("ss_wrl_db_ConfigNIC", true);
      }
    }      
    return bConfigNICPrompt;
  },
  GetNumberOfWirelessNICs  : function()
  {
    var bDebugNumberOfNICs = _databag.GetValue("ss_wrl_db_NumberOfNICs");
    var bNumberOfNICS = (bDebugNumberOfNICs != null) ? bDebugNumberOfNICs : _sswrlutility.GetPresentWirelessAdaptersCount(null, true);
    
    if (bNumberOfNICS > 1)
    {
      //Set Visual-Flow animation parameters
			_databag.SetValue("ss_wrl_db_vflink1_animation", true);
			$ss.snapin.wireless.viewhelper.AutoUpdateVisualFlowState(); 
      return "multi";
    }
    else if (bNumberOfNICS == 1)
    {
      var wNics = _sswrlutility.GetPresentWirelessAdapters(null, true);
      for (wNic in wNics)
      {
        _databag.SetValue("ss_wrl_db_NICServiceName", wNic);
        _databag.SetValue("ss_gc_db_SelectedAdapter", wNic);
      }
      $ss.snapin.wireless.viewhelper.AutoUpdateVisualFlowState();
      return "one";
    }
    else
    {
      $ss.snapin.wireless.viewhelper.AutoUpdateVisualFlowState();
      return "none";
    }
  },
  GetSettingsFrom  : function()
  {
    // *************
    // IN SubAgent flows we wont get data from "Modem" , "Factory defaults" or "Flash drive" so directly retuning "advanced inputs"
    // advanced mode : where user has to select ssid and enter the raw WEP key or PSK
    // without going through the password-to-enckey conversion
    return "from_advanced_inputs";
  },
  CheckApplyResult  : function()
  {
    var bStandalone = _databag.GetValue("ss_wrl_db_Standalone");
    var sApplyResults = _databag.GetValue("ss_wrl_db_ApplyResults");
    // handle result differently if we are in standalone mode and applysettings failed
    if (bStandalone && sApplyResults != "success")
    {
      sApplyResults = "standalone_fail";
    }
    return sApplyResults;
  },
  IsAdapterConnected : function()
  {
    var nicID = _databag.GetValue("ss_wrl_db_NICServiceName");
    var result = _netcheck.IsAdapterConnected(null,nicID);
    if(result)
    {
      result = _wrlD.CurrentNicHasValidIP(nicID);
    }    
    return result;  
  },
  
  CurrentNicHasValidIP : function(sSvcName)
  {
    var gw = "";
    var cardArr = {};
    cardArr[sSvcName] = true;
    var cardsProps = _netcheck.GetAdaptersProperties(null,
                                                  cardArr,
                                                  "DefaultGateway");
    gw = cardsProps[sSvcName]["DefaultGateway"];        
    return (gw != "" && gw != "0.0.0.0");
  }  
 }); 
  
  
var _utils      = $ss.agentcore.utils;
var _system     = $ss.agentcore.utils.system;
var _config     = $ss.agentcore.dal.config;
var _databag    = $ss.agentcore.dal.databag;
var _registry   = $ss.agentcore.dal.registry;
var _network    = $ss.agentcore.network.network;
var _netcheck   = $ss.agentcore.network.netcheck;
var _inet       = $ss.agentcore.network.inet;
var _const      = $ss.agentcore.constants;
var _SI         = $ss.agentcore.diagnostics.smartissue;
var _logger     = $ss.agentcore.log.GetDefaultLogger("$ss.snapin.wireless.utils"); 
var _sswrlconstants   = $ss.snapin.network.wireless.constants;
var _sswrlutility     = $ss.snapin.network.wireless.utility;
var _sdcDSLModem      = $ss.agentcore.smapi.methods ;  
var _smapiUtils       = $ss.agentcore.smapi.utils ;
var _wrlcommon        = $ss.snapin.wireless;
var _wrlD             = $ss.snapin.wireless.decisions;
var _nics       = $ss.snapin.wireless.nics;
  
   


})();