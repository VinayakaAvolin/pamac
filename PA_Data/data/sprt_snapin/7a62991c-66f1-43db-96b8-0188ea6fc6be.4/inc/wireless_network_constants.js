/*******************************************************************************
**
**  File Name: ss_wireless.js
**
**  Summary: SupportSoft Client Wireless Functions
**
**  Description: This file contains general purpose functions for
**               manipulating local wireless configuration
**
**  Dependencies: ss_constants.js
**                ss_container.js
**                ss_netcheck.js
**                ss_log.js
**
**  Copyright SupportSoft Inc. 2005, All rights reserved.
*******************************************************************************/
$ss.snapin = $ss.snapin ||
{}; 
$ss.snapin.network = $ss.snapin.network||
{};
$ss.snapin.network.wireless = $ss.snapin.network.wireless ||
{};
$ss.snapin.network.wireless.constants = $ss.snapin.network.wireless.constants ||
{};


(function(){
  $.extend($ss.snapin.network.wireless.constants,{
    // media state defines
    NdisMediaStateConnected : 0,
    NdisMediaStateDisconnected : 1,

    // authentication mode defines
    Ndis802_11AuthModeOpen : 0,
    Ndis802_11AuthModeShared : 1,
    Ndis802_11AuthModeAutoSwitch : 2,
    Ndis802_11AuthModeWPA : 3,
    Ndis802_11AuthModeWPAPSK : 4,
    Ndis802_11AuthModeWPANone : 5,
    Ndis802_11AuthModeWPA2 : 6,
    Ndis802_11AuthModeWPA2PSK : 7,

    // encryption status defines
    Ndis802_11Encryption1Enabled : 0,       // WEP
    Ndis802_11EncryptionDisabled : 1,       // Disabled
    Ndis802_11Encryption1KeyAbsent : 2,     // Disabled
    Ndis802_11EncryptionNotSupported : 3,   // Not Supported
    Ndis802_11Encryption2Enabled : 4,       // TKIP
    Ndis802_11Encryption2KeyAbsent : 5,     // TKIP
    Ndis802_11Encryption3Enabled : 6,       // AES
    Ndis802_11Encryption3KeyAbsent : 7,     // AES

    // infrastructure mode defines
    Ndis802_11IBSS : 0,
    Ndis802_11Infrastructure : 1,
    Ndis802_11AutoUnknown : 2,

    DOT11_AUTH_ALGO_80211_OPEN : 1,
    DOT11_AUTH_ALGO_80211_SHARED_KEY : 2,
    DOT11_AUTH_ALGO_WPA : 3,
    DOT11_AUTH_ALGO_WPA_PSK : 4,
    DOT11_AUTH_ALGO_WPA_NONE : 5,
    DOT11_AUTH_ALGO_RSNA : 6,
    DOT11_AUTH_ALGO_RSNA_PSK : 7,

    DOT11_CIPHER_ALGO_NONE : 0x00,
    DOT11_CIPHER_ALGO_WEP40 : 0x01,
    DOT11_CIPHER_ALGO_TKIP : 0x02,
    DOT11_CIPHER_ALGO_CCMP : 0x04,
    DOT11_CIPHER_ALGO_WEP104 : 0x05,
    DOT11_CIPHER_ALGO_WPA_USE_GROUP : 0x100,
    DOT11_CIPHER_ALGO_RSN_USE_GROUP : 0x100,
    DOT11_CIPHER_ALGO_WEP : 0x101,

    DOT11_BSS_TYPE_INFRASTRUCTURE : 1,
    DOT11_BSS_TYPE_INDEPENDENT : 2,
    DOT11_BSS_TYPE_ANY : 3,
	
	  WZCSVC_AUTHTYPE_OPEN      : "Open",
    WZCSVC_AUTHTYPE_SHARED    : "Shared",
    WZCSVC_AUTHTYPE_WPA       : "WPA",
    WZCSVC_AUTHTYPE_WPA_PSK   : "WPAPSK",
    WZCSVC_AUTHTYPE_WPA_NONE  : "WPA-NONE",
    WZCSVC_AUTHTYPE_WPA2      : "WPA2",
    WZCSVC_AUTHTYPE_WPA2_PSK  : "WPA2PSK",

    WZCSVC_ENCTYPE_NONE       : "None",
    WZCSVC_ENCTYPE_WEP        : "WEP",
    WZCSVC_ENCTYPE_TKIP       : "TKIP",
    WZCSVC_ENCTYPE_AES        : "AES",

    g_WZCRegKey   : "SOFTWARE\\Microsoft\\WZCSVC\\Parameters\\Interfaces\\",
    g_EAPOLRegKey : "SOFTWARE\\Microsoft\\EAPOL\\Parameters\\Interfaces\\",
    g_WZCSVCNAME  : "wzcsvc",
    g_NDISUIOSVC  : "ndisuio",
    g_WLANSVCNAME : "wlansvc",
    
    DOT11_TO_NDIS_AUTHMODE_MAP : {
      DOT11_AUTH_ALGO_80211_OPEN       : this.Ndis802_11AuthModeOpen,
      DOT11_AUTH_ALGO_80211_SHARED_KEY : this.Ndis802_11AuthModeShared,
      DOT11_AUTH_ALGO_WPA              : this.Ndis802_11AuthModeWPA,
      DOT11_AUTH_ALGO_WPA_PSK          : this.Ndis802_11AuthModeWPAPSK,
      DOT11_AUTH_ALGO_WPA_NONE         : this.Ndis802_11AuthModeWPANone,
      DOT11_AUTH_ALGO_RSNA             : this.Ndis802_11AuthModeWPA2,
      DOT11_AUTH_ALGO_RSNA_PSK         : this.Ndis802_11AuthModeWPA2PSK
    },
    
    DOT11_TO_NDIS_ENCTYPE_MAP : {
      DOT11_CIPHER_ALGO_NONE            : this.Ndis802_11EncryptionDisabled,
      DOT11_CIPHER_ALGO_WEP40           : this.Ndis802_11Encryption1Enabled,
      DOT11_CIPHER_ALGO_TKIP            : this.Ndis802_11Encryption2Enabled,
      DOT11_CIPHER_ALGO_CCMP            : this.Ndis802_11Encryption3Enabled,
      DOT11_CIPHER_ALGO_WEP104          : this.Ndis802_11Encryption1Enabled,
      DOT11_CIPHER_ALGO_WPA_USE_GROUP   : -1,  // unknown mapping
      DOT11_CIPHER_ALGO_WEP             : this.Ndis802_11Encryption1Enabled
    },

    DOT11_TO_NDIS_BSS_TYPE : {
      DOT11_BSS_TYPE_INFRASTRUCTURE : this.DOT11_BSS_TYPE_INFRASTRUCTURE,
      DOT11_BSS_TYPE_INDEPENDENT : this.Ndis802_11IBSS,
      DOT11_BSS_TYPE_ANY : this.Ndis802_11AutoUnknown
    }
    

  });

})();

