/*******************************************************************************
**
**  File Name: ss_wireless.js
**
**  Summary: SupportSoft Client Wireless Functions
**
**  Description: This file contains general purpose functions for
**               manipulating local wireless configuration
**
**  Dependencies: ss_constants.js
**                ss_container.js
**                ss_netcheck.js
**                ss_log.js
**
**  Copyright SupportSoft Inc. 2005, All rights reserved.
*******************************************************************************/
$ss.snapin = $ss.snapin ||
{}; 
$ss.snapin.network = $ss.snapin.network||
{};
$ss.snapin.network.wireless = $ss.snapin.network.wireless ||
{};
$ss.snapin.network.wireless.utility = $ss.snapin.network.wireless.utility ||
{};

(function(){
  $.extend($ss.snapin.network.wireless.utility,{
  
    /*******************************************************************************
    ** Name:         GetPresentWirelessAdaptersCount
    **
    ** Purpose:      Retrieve count of active wireless adapters
    **
    ** Parameter:    oNet - (Optional) instance of sdcnetcheck ctl.  If none specified
    **                      function creates an instance and deletes it.
    **               bFilterDisabled - filter out disabled adapters
    **
    ** Return:       integer - # of wireless adapters currently present on the machine
    **
    ** Supported OS: Win2K and above
    *******************************************************************************/
    GetPresentWirelessAdaptersCount: function(oNet, bFilterDisabled)
    {
      var cnt = 0;

      try
      {
        wifiList = this.GetPresentWirelessAdapters(oNet, bFilterDisabled);
        for (var i in wifiList) cnt++;
      }
      catch(err)
      {       
        _exception.HandleException(err,'$ss.snapin.network.wireless','GetPresentWirelessAdaptersCount',arguments);
      }

      return cnt;
    },
    
    /*******************************************************************************
    ** Name:         GetPresentWirelessAdapters
    **
    ** Purpose:      Retrieve active wireless adapters
    **
    ** Parameter:    oNet - (Optional) instance of sdcnetcheck ctl.  If none specified
    **                        function creates an instance and deletes it.
    **               bFilterDisabled - filter out disabled adapters
    **
    ** Return:       hash - (in the format of hash[svcName] = svcDesc)
    **
    ** Supported OS: Win2K and above
    *********************************************************************************/
    GetPresentWirelessAdapters:function(oNet, bFilterDisabled)
    {
      var presentAdapters = {};
      if(!_Is2KAndAbove()) return presentAdapters;

      try
      {
        if (oNet == null) oNet = $ss.agentcore.utils.activex.CreateObject("SPRT.SdcNetCheck");
        presentAdapters = $ss.agentcore.network.netcheck.GetPresentAdapters(oNet, true, true, bFilterDisabled);

        for (var i in presentAdapters)
        {
          var svcName = i;
          var svcDesc = presentAdapters[i];
          if (!$ss.agentcore.network.netcheck.IsAdapterWireless(oNet, svcName))
          {
            delete presentAdapters[i]; // not wireless, remove from hash
          }
        }
      }
      catch(err)
      {        
        _exception.HandleException(err,'$ss.snapin.network.wireless','GetPresentWirelessAdapters',arguments);
      }

      return presentAdapters;
    },
    
    GetWlanInterfaceInfo:function(oNet, sSvcName)
    {
      var oWlanInfo = null;
      try
      {
        if($ss.agentcore.utils.system.IsWinVST(true))
        {
          if (oNet == null) oNet = $ss.agentcore.utils.activex.CreateObject("SPRT.SdcNetCheck");    
          oWlanInfo = oNet.GetWlanInterfaceInfo(sSvcName);
        }
      }
      catch (err)
      {
        _exception.HandleException(err,'$ss.snapin.network.wireless','GetWlanInterfaceInfo',arguments);
      }
      return oWlanInfo;
    },

    GetCurrentSSID:function(oNet, sSvcName)
    {
      var curSsid = "";
      try
      {
        if (oNet == null) oNet = $ss.agentcore.utils.activex.CreateObject("SPRT.SdcNetCheck");
        var oWlanInfo = this.GetWlanInterfaceInfo(oNet, sSvcName);

        var curSsid = $ss.agentcore.network.netcheck.GetOIDValue(oNet,sSvcName,$ss.agentcore.constants.OID_802_11_SSID);
        if (curSsid == "" && $ss.agentcore.utils.system.IsWinVST(true) && oWlanInfo) // fallback
        {
          curSsid = oWlanInfo.Ssid;
        }    
      }
      catch(err)
      {
        _exception.HandleException(err,'$ss.snapin.network.wireless','GetCurrentSSID',arguments);
      }
      return curSsid;
    },

    /*******************************************************************************
    ** Name:         QueryWiFiProperties
    **
    ** Purpose:      Returns wireless properties of a specific wireless adapter
    **
    ** Parameter:    oNet - (Optional) instance of sdcnetcheck ctl.  If none specified
    **                        function creates an instance and deletes it.
    **               sSvcName - Service Name of adapter (basically id of adapter)
    **               sPropertiesStr - comma delimited list of properties to get
    **
    ** Return:       hash
    **
    ** Supported Properties:
    **                        "ConnectionName"
    **                        "Status" (0 - "Connected", 1 - "Not Connected")
    **                        "SSID"
    **                        "BSSID" (xx:xx:xx:xx:xx:xx)
    **                        "AuthMode" (0 - Open, 1 - Shared, 2 - AutoSwitch, 3 - WPA, 4 - WPA-PSK, 5 - WPA-None)
    **                        "EncType" (Disabled/WEP/TKIP/AES)
    **                        "Channel"
    **                        "RSSI"  (Received Signal Strength Indication - measured in dBm, typically between -10 and -200)
    **                        "LinkSpeed"
    **                        "SupportedRates"
    **                        "InfrastructureMode" (0 - adhoc, 1 - infrastructure, 2 - autoswitch)
    ********************************************************************************/
    QueryWiFiProperties:function(oNet, sSvcName, sPropertiesStr)
    {
      var wifiProps = {};
      try
      {
        var propNames = sPropertiesStr.split(",");
        if (oNet == null) oNet = $ss.agentcore.utils.activex.CreateObject("SPRT.SdcNetCheck");

        var oWlanInfo = this.GetWlanInterfaceInfo(oNet, sSvcName);

        // go through properties array
        for (var i = 0; i < propNames.length; i++)
        {
          var propName = propNames[i];
          switch(propName)
          {
            case "ConnectionName":
              wifiProps[propName] = $ss.agentcore.network.netcheck.GetAdapterConnectionName(sSvcName);
              break;
            case "Status":
              wifiProps[propName] = $ss.agentcore.network.netcheck.IsAdapterConnected(oNet,sSvcName) ?
                                    $ss.snapin.network.wireless.constants.NdisMediaStateConnected : $ss.snapin.network.wireless.constants.NdisMediaStateDisconnected;
              break;
            case "SSID":
              wifiProps[propName] = $ss.agentcore.network.netcheck.GetOIDValue(oNet,sSvcName, OID_802_11_SSID);
              if (wifiProps[propName] == "" && $ss.agentcore.utils.system.IsWinVST(true) && oWlanInfo) // fallback
              {
                wifiProps[propName] = oWlanInfo.Ssid;
              }
              break;
            case "BSSID":
              var tmp = $ss.agentcore.network.netcheck.GetOIDValue(oNet,sSvcName, OID_802_11_BSSID);
              if (tmp == "" && $ss.agentcore.utils.system.IsWinVST(true) && oWlanInfo)                 // fallback
              {
                tmp = oWlanInfo.Bssid;
              }
              wifiProps[propName] = _BssidDecToHex(tmp);
              break;
            case "AuthMode":
              wifiProps[propName] = $ss.agentcore.network.netcheck.GetOIDValue(oNet,sSvcName, OID_802_11_AUTHENTICATION_MODE);
              if (wifiProps[propName] == "" && $ss.agentcore.utils.system.IsWinVST(true) && oWlanInfo) // fallback
              {
                wifiProps[propName] = $ss.snapin.network.wireless.constants.DOT11_TO_NDIS_AUTHMODE_MAP[oWlanInfo.AuthMode];
              }
              break;
            case "EncType":
              wifiProps[propName] = $ss.agentcore.network.netcheck.GetOIDValue(oNet,sSvcName, OID_802_11_ENCRYPTION_STATUS);
              if (wifiProps[propName] == "" && $ss.agentcore.utils.system.IsWinVST(true) && oWlanInfo) // fallback
              {
                wifiProps[propName] = $ss.snapin.network.wireless.constants.DOT11_TO_NDIS_ENCTYPE_MAP[oWlanInfo.EncType];
              }
              break;
            case "Channel":
              var conf = $ss.agentcore.network.netcheck.GetOIDValue(oNet,sSvcName, OID_802_11_CONFIGURATION);
              wifiProps[propName] = _GetChannelFromConfigString(conf);
              if (wifiProps[propName] == "" && $ss.agentcore.utils.system.IsWinVST(true) && oWlanInfo) // fallback
              {
                wifiProps[propName] = oWlanInfo.Channel;
              }          
              break;
            case "RSSI":
              var iSignalStrength = 0;
              for(var k = 0; k < 5; k++)
              {
                iSignalStrength += parseInt($ss.agentcore.network.netcheck.GetOIDValue(oNet,sSvcName, OID_802_11_RSSI));
              }
              iSignalStrength = iSignalStrength / 5;
              wifiProps[propName] = iSignalStrength.toString();
             if (wifiProps[propName] == "" && $ss.agentcore.utils.system.IsWinVST(true) && oWlanInfo) // fallback
              {
                wifiProps[propName] = oWlanInfo.Rssi;
              }          
              break;
            case "LinkSpeed":
              var speed = $ss.agentcore.network.netcheck.GetOIDValue(oNet,sSvcName, OID_GEN_LINK_SPEED);
              if(parseInt(speed) > 0)
              {
                wifiProps[propName] = speed/100/100;  // MBit/sec
              }
              break;
            case "SupportedRates":
              var sRates = $ss.agentcore.network.netcheck.GetOIDValue(oNet,sSvcName, OID_802_11_SUPPORTED_RATES);
              wifiProps[propName] = _ConvertSupportedRates(sRates);
              break;
            case "InfrastructureMode":
              wifiProps[propName] = $ss.agentcore.network.netcheck.GetOIDValue(oNet,sSvcName, OID_802_11_INFRASTRUCTURE_MODE);
              if (wifiProps[propName] == "" && $ss.agentcore.utils.system.IsWinVST(true) && oWlanInfo) // fallback
              {
                wifiProps[propName] = $ss.snapin.network.wireless.constants.DOT11_TO_NDIS_BSS_TYPE[oWlanInfo.InfrastructureMode];
              }        
              break;
            default:
              wifiProps[propName] = "";
              break;
          }
        }
      }
      catch(err)
      {
        _exception.HandleException(err,'$ss.snapin.network.wireless','QueryWiFiProperties',arguments);
      }
      return wifiProps;
    },

    /*******************************************************************************
    ** Name:         TestAssociation
    **
    ** Purpose:      Test if adapter is currently connected to the specified SSID
    **               and have a valid IP
    **
    ** Parameter:    oNet - (Optional) instance of sdcnetcheck ctl.  If none specified
    **                      function creates an instance and deletes it.
    **               sSvcName - Service Name of adapter (basically id of adapter)
    **               sSsid - network profile in question
    **
    ** Return:       true/false
    ********************************************************************************/
    TestAssociation:function(oNet, sSvcName, sSsid)
    {
      function _HaveValidIP(sSvcName)
      {
        var gw = "";
        var cardArr = {};
        cardArr[sSvcName] = true;
        var cardsProps = $ss.agentcore.network.netcheck.GetAdaptersProperties(oNet,
                                                      cardArr,
                                                      "DefaultGateway");
        gw = cardsProps[sSvcName]["DefaultGateway"];
        _logger.info("_HaveValidIP", "sSvcName: " + sSvcName + "gw: " + gw);
        return (gw != "" && gw != "0.0.0.0");
      }
      
      function _haveValidConnection()
      {
        var sBrowser = $ss.agentcore.network.inet.GetDefaultBrowser(); 
        $ss.agentcore.utils.Sleep(3000);
        var bTest = $ss.agentcore.network.inet.BasicConnectionTest(false, true, sBrowser); 
        return bTest;
      }

      var data = this.GetCurrentSSID(oNet, sSvcName);
      _logger.info("TestAssociation", "data: " + data + " sSsid: " + sSsid);
      if(data == sSsid)
      {
        var bHaveValidIP =_HaveValidIP(sSvcName);
        $ss.agentcore.utils.Sleep(3000);
        _logger.info("TestAssociation", "bHaveValidIP: " + bHaveValidIP);       
        if(bHaveValidIP)
         {
           var bHaveValidConnection = _haveValidConnection();
           return bHaveValidConnection;
         }
         else
         {
           return false;
         }
      }
      return false;
    },

    /*******************************************************************************
    ** Name:         IsConnectedToModemWirelessly
    **
    ** Purpose:      Test if any of the wireless adapter is currently connected to the modem
    **
    ** Parameter:    sModemIP - ip of the modem
    **
    ** Return:       true/false
    ********************************************************************************/
    IsConnectedToModemWirelessly:function(sModemIP)
    {
      var wirelessNICs = this.GetPresentWirelessAdapters(null, true);  // query all present and enabled wireless nics
      var cardsProps = $ss.agentcore.network.netcheck.GetAdaptersProperties(null, wirelessNICs, "DefaultGateway");

      if (sModemIP)
      {
        // for each of the wireless nic, compare its Gateway and see if it's set to the modem's IP.
        // If so, then we know it's connected to the modem wirelessly
        for (var sSvcName in cardsProps)
        {
          var gw = cardsProps[sSvcName]["DefaultGateway"];
          if (gw == sModemIP)
          {
            return true;
          }
        }
      }
      return false;
    },

    /*******************************************************************************
    ** Name:         ValidateSSID
    **
    ** Purpose:      Validate SSID to make sure it's of the correct format
    **
    ** Parameter:    sKey - ssid
    **
    ** Return:       true/false
    ********************************************************************************/
    ValidateSSID:function(sKey)
    {
      try
      {
        // This can be entered as 1 to 32 ascii characters
        if(sKey.length >=1 && sKey.length <= 32)
        {
          return true;
        }
      }
      catch(err)
      {
        _exception.HandleException(err,'$ss.snapin.network.wireless','ValidateSSID',arguments);
      }
      return false;
    },

    /*******************************************************************************
    ** Name:         ValidateWEPKey
    **
    ** Purpose:      Validate WEP key
    **
    ** Parameter:    sKey - WEP key string
    **               nEncLevel - encryption level 64 or 128 (optional)
    **
    ** Return:       true/false
    ********************************************************************************/
    ValidateWEPKey:function(sKey, nEncLevel)
    {
      // network password can be entered as 5 or 13 ascii characters of 10 or 26 hexadecimal characters
      return _Validate_wep_hex(sKey, nEncLevel) || _Validate_wep_ascii(sKey);
    },

    /*******************************************************************************
    ** Name:         ValidateWPAPSKKey
    **
    ** Purpose:      Validate WPA-PSK key
    **
    ** Parameter:    sKey - WPA-PSK key string

    ** Return:       true/false
    ********************************************************************************/
    ValidateWPAPSKKey:function(sKey)
    {
      try
      {
        // This can be entered as 8 to 63 ascii characters or 64 hexadecimal characters.
        if(sKey.length == 64)
        {
          var hexMatch = sKey.match(/[a-f0-9]+/i);
          return (hexMatch && hexMatch == sKey);      
        }
        else if(sKey.length >=8 && sKey.length <= 63)
        {
          var ascii = sKey.match(/[\x01-\xff]+/i);
          return (ascii && ascii == sKey);
        }
      }
      catch(err)
      {
        _exception.HandleException(err,'$ss.snapin.network.wireless','ValidateWPAPSKKey',arguments);
      }
      return false;
    },

    /*******************************************************************************
    ** Name:         GeneratePreSharedKey
    **
    ** Purpose:      Generate a PSK from a passphrase string
    **
    ** Parameter:    sPassphrase - key string
    **
    ** Return:       encryption key generated by the passphrase
    ********************************************************************************/
    GeneratePreSharedKey:function(sPassphrase)
    {
      var sKey = "";

      try
      {
        sKey = $ss.agentcore.utils.GenerateMD5(sPassphrase);
      }
      catch(err)
      {
        _exception.HandleException(err,'$ss.snapin.network.wireless','GeneratePreSharedKey',arguments);
      }
      return sKey;
    },

    /*******************************************************************************
    ** Name:         GenerateWEPKey
    **
    ** Purpose:      Generate a WEP key from a passphrase string
    **
    ** Parameter:    sPassphrase - key string
    **               bIs128bit   - 64 or 128 bit
    **
    ** Return:       encryption key generated by the passphrase
    ********************************************************************************/
    GenerateWEPKey:function(sPassphrase, bIs128bit)
    {
      var sKey = "";

      try
      {
        // if enc level is not passed in, then should always use 128 bit
        if (typeof(bIs128bit) == "undefined") bIs128bit = true;

        if (bIs128bit)
        {
          sKey = _Wep_keygen128(sPassphrase);
        }
        else
        {
          sKey = _Wep_keygen64(sPassphrase)[0];
        }
      }
      catch(err)
      {
        _exception.HandleException(err,'$ss.snapin.network.wireless','GenerateWEPKey',arguments);
      }
      return sKey;
    },

    /*******************************************************************************
    ** Name:         GetSignalStrength
    **
    ** Purpose:      Convert signal strength to match Windows XP categories
    **
    ** Parameter:    nSignal - RSSI (Received Signal Strength Indication,
    **                               measured in dBm, typically between -10 and -200)
    **
    ** Return:       string
    ********************************************************************************/
    GetSignalStrength:function(nSignal)
    {
      if(nSignal < -90)
      {
        return "No Signal";
      }
      else if (nSignal < -81)
      {
        return "Very Low";
      }
      else if (nSignal < -71)
      {
        return "Low";
      }
      else if (nSignal < -67)
      {
        return "Good";
      }
      else if (nSignal < -57)
      {
        return "Very Good";
      }
      else
      {
        return "Excellent";
      }
    },
    
    /*******************************************************************************
    ** Factory methods
    ********************************************************************************/
    GetWiFiConfig:function(sSvcName) // by OS
    {
      var ob = null;
      var isSvcRunning=$ss.snapin.network.wireless.utility.IsSVCRunning($ss.snapin.network.wireless.constants.g_WLANSVCNAME);
      // ACM is available for Windows Vista or Windows SP2 with WLANAPI add-on installed
      if ($ss.agentcore.utils.system.IsWinVST(true) || ($ss.agentcore.utils.system.IsWinXP(true) && isSvcRunning))
      {
        ob = new ssACMProfileCfg(sSvcName);
      }
      else if ($ss.agentcore.utils.system.IsWinXP(true))
      {
        ob = new ssWZCProfileCfg(sSvcName);
      }
      return ob;
    },

    GetWiFiConfigByType: function(method, sSvcName)  // by method
    {
      var ob = null;
      switch (method)
      {
      case "acm":
        ob = new ssACMProfileCfg(sSvcName);
        break;
      case "wzc":
        ob = new ssWZCProfileCfg(sSvcName);
        break;
      }
      return ob;
    },
    
     /*******************************************************************************
    ** Name:         _IsSVCRunning
    **
    ** Purpose:      Determines if an NT service is running or not
    **
    ** Parameter:    sNTSvcName - NT service name (either "wzcsvc" or "wlansvc")
    **
    ** Return:       true/false
    ********************************************************************************/
    IsSVCRunning:function(sNTSvcName)
    {
      var status = $ss.agentcore.dal.service.GetWin32ServiceStatus(sNTSvcName);

      if(status.CurrentState == "undefined") return false;
      return (status.CurrentState == $ss.agentcore.constants.BCONT_SERVICE_RUNNING);
    },
    
    /*******************************************************************************
    ** Name:         _ModifySvcStartMode
    **
    ** Purpose:      modify WZCSVC service start mode
    **
    ** Parameter:    nNewMode - new startup mode  (2 - automatic, 3 - manual, 4 - disabled)
    **               sNTSvcName - NT service name (either "wzcsvc" or "wlansvc")
    **
    ** Return:       true/false
    ********************************************************************************/
    ModifySvcStartMode:function(nNewMode, sNTSvcName)
    {
      var ret = false;

      try
      {
        ret = $ss.agentcore.dal.service.ChangeWin32ServiceStartMode(sNTSvcName, nNewMode);
      }
      catch(err)
      {
        _exception.HandleException(err,'$ss.snapin.network.wireless','_ModifySvcStartMode',arguments);
      }

      return ret;
    },
    
    /*******************************************************************************
    ** Name:         _StartSvc
    **
    ** Purpose:      start WZCSVC service
    **
    ** Parameter:    bStart - TRUE = Start, FALSE = Stop
    **               sNTSvcName - NT service name (either "wzcsvc" or "wlansvc")
    **
    ** Return:       true/false
    ********************************************************************************/
    StartSvc:function(bStart, sNTSvcName)
    {
      var ret = false;

      try
      {
        ret = $ss.agentcore.dal.service.StartWin32Service(sNTSvcName, bStart);
        if(ret && bStart) $ss.agentcore.utils.system.Sleep(3000);
      }
      catch(err)
      {
        _exception.HandleException(err,'$ss.snapin.network.wireless','_StartSvc',arguments);
      }

      return ret;
    },
    
    /*******************************************************************************
    ** Name:         _WZC_EnableSvcForAdapter
    **
    ** Purpose:      Enable WZC on a particular interface
    **
    ** Parameter:    svcName - Service Name of adapter (basically id of adapter)
    **
    ** Return:       true/false
    ********************************************************************************/
    WZC_EnableSvcForAdapter:function(sSvcName)
    {
      var ret = true;
      var bEnable = _WZC_IsSvcEnabledForAdapter(sSvcName);
      if(!bEnable)
      {
        ret = this.StartSvc(false, $ss.snapin.network.wireless.constants.g_WZCSVCNAME);
        if(ret)
        {
          var  regKey = $ss.snapin.network.wireless.constants.g_WZCRegKey + sSvcName;
          var  controlFlags = $ss.agentcore.dal.registry.GetRegValue("HKLM", regKey, "ControlFlags");
          controlFlags = parseInt(controlFlags, 16);  // hex?
          controlFlags |= 0x8002;   // from reverse engineering,
                                    // 0x8000 means "Use Windows to configure my wireless network settings"
                                    // 0x0002 means "Any available network (access point preferred)"
          $ss.agentcore.dal.registry.SetRegValueByType("HKLM", regKey, "ControlFlags", 4, controlFlags);
        }
      }
      return ret && $ss.snapin.network.wireless.utility.StartSvc(true, $ss.snapin.network.wireless.constants.g_WZCSVCNAME);
    },
    
    /*******************************************************************************
    ** Name:         _WZC_GetWirelessProfiles
    **
    ** Purpose:      Get a list of preferred networks set up for this adapter
    **
    ** Parameter:    sSvcName - Service Name of adapter (basically id of adapter)
    **
    ** Return:       hash - {"0000" : "hq@air, "0001" : "hq@home"}
    ********************************************************************************/
    WZC_GetWirelessProfiles:function(sSvcName)
    {
      var profiles = {};

      try
      {
        var num = 0;
        var str = "";

        do
        {
          var strNum = _NumberInHex(num++, 4);
          str = _GetProfileStr(sSvcName, strNum);
          if(str != "")
          {
            var ssid = _GetSSIDFromProfile(str);
            profiles[strNum] = ssid;
          }
        } while (str != "");
      }
      catch(err)
      {
        _exception.HandleException(err,'$ss.snapin.network.wireless','_WZC_GetWirelessProfiles',arguments);
      }

      return profiles;
    },
    
    RemoveEAPOLKey:function(svcName, ssid)
    {
      var regKey = $ss.snapin.network.wireless.constants.g_EAPOLRegKey + svcName;
      var index = this.GetEAPOLIndex(svcName, ssid);
      return $ss.agentcore.dal.registry.DeleteRegVal("HKLM", regKey, index);
    },
    
    /*******************************************************************************
    ** Name:         _GetEAPOLIndex
    **
    ** Purpose:      Get the index representing the network in the registry
    **               SOFTWARE\Microsoft\EAPOL\Parameters\Interfaces\(svcName)
    **
    ** Parameter:    sSvcName - Service Name of adapter (basically id of adapter)
    **               sSsid - network profile in question
    **
    ** Return:       Empty string if not exists, returns the index if exists
    ********************************************************************************/
    GetEAPOLIndex:function(sSvcName, sSsid)
    {
      var num = "";
      try
      {
        var  regKey = $ss.snapin.network.wireless.constants.g_EAPOLRegKey + sSvcName;
        var  profValue = $ss.agentcore.dal.registry.EnumRegValue("HKLM", regKey, ":", "|");
        var  pairs;

        if(profValue) pairs = profValue.split("|");

        for (var i = 0; i < pairs.length; i++)
        {
          var dict = pairs[i].split(":");
          if(dict)
          {
            if(sSsid == _GetSSIDFromProfile(dict[1]))
            {
              num = dict[0];
              break;
            }
          }
        }
      }
      catch(err)
      {
        _exception.HandleException(err,'$ss.snapin.network.wireless','_GetEAPOLIndex',arguments);
      }
      return num;
    },
    
    RemoveWZCProfile:function(svcName, ssid)
    {
      var ret = true;

      var index = this.GetWZCProfileIndex(svcName, ssid);
      var totalCnt = _GetWirelessProfilesCount(svcName);
      var startCnt = parseInt(index, 16);
      var endCnt   = totalCnt - 1;

      // shift up the profiles starting from the one being deleted
      for(var i = startCnt; i < endCnt; i++)
      {
        var nextProfile = _GetProfileStr(svcName, _NumberInHex(i+1, 4));
        ret = ret && _SetProfileStr(svcName, _NumberInHex(i, 4), nextProfile);
      }

      // delete the last one after shifting
      ret = ret && _DeleteProfileStr(svcName, _NumberInHex(endCnt, 4));

      return ret;
    },
    
    /*******************************************************************************
    ** Name:         _GetWZCProfileIndex
    **
    ** Purpose:      Get the index (Static#XXXX) representing the network in the registry
    **               SOFTWARE\Microsoft\WZCSVC\Parameters\Interfaces\{svcName}
    **
    ** Parameter:    sSvcName - Service Name of adapter (basically id of adapter)
    **               sSsid - network profile in question
    **
    ** Return:       Empty string if not exists, returns the index (eg, "0000") if exists
    ********************************************************************************/
    GetWZCProfileIndex:function(sSvcName, sSsid)
    {
      var profiles = $ss.snapin.network.wireless.utility.WZC_GetWirelessProfiles(sSvcName);
      for (var i in profiles)
      {
        if (sSsid == profiles[i]) return i;
      }
      return "";
    },
    
    /*******************************************************************************
    ** Name:         _GetBssidListInfo
    ** Purpose:      Gets a list visible and available wireless networks
    ** Parameter:    oNet - (Optional) instance of sdcnetcheck ctl.  If none specified
    **                        function creates an instance and deletes it.
    **               sSvcName - Service Name of adapter (basically id of adapter)
    ** Return:       hash with each element being an object
    **
    ** Supported Properties:
    **                     "SSID"
    **                     "BSSID" (xx:xx:xx:xx:xx:xx)
    **                     "Channel"
    **                     "RSSI"
    **                     "SupportedRates"
    **                     "InfrastructureMode" (0 - adhoc, 1 - infrastructure, 2 - autoswitch)
    **                     "Privacy" (0 - Disabled, 1 - WEP/WPA1/WPA2)
    ********************************************************************************/
    GetBssidListInfo:function(oNet, sSvcName, bRefresh)
    {
      var access_pts = {};

      try
      {
        if(!_Is2KAndAbove()) return connName;
        if (oNet == null) oNet = $ss.agentcore.utils.activex.CreateObject("SPRT.SdcNetCheck");
        if (sSvcName)
        {
          if(typeof(bRefresh) != "undefined" && bRefresh)
          {
            // commented out for now
            // ndisSetOidValue requires binding to NDISUIO, but if WZCSVC is already binded to it
            // it requires us stopping the WZCSVC and that stopping and restarting takes a long time
            // netobj.ndisSetOidValue(OID_LIST["OID_802_11_BSSID_LIST_SCAN"], svcName, "");
          }

          var arrVB = new VBArray(oNet.GetBssidListInfo(sSvcName));
          var arr = arrVB.toArray();
          for (var i1 = 0; i1 < arr.length; ++i1)
          {
            var access_point = new Object();
            access_point["SSID"] = arr[i1].Ssid;
            access_point["BSSID"] = _BssidDecToHex(arr[i1].MacAddress);
            access_point["SupportedRates"] = _ConvertSupportedRates(arr[i1].SupportedRates);
            access_point["Privacy"] = arr[i1].Privacy;
            access_point["RSSI"] = arr[i1].Rssi;
            access_point["InfraStructureMode"] = arr[i1].InfraStructureMode;
            access_point["Channel"] = _GetChannelFromConfigString(arr[i1].Configuration);
            
            if (access_point["Channel"] == 0 && typeof(arr[i1].Frequency) != "undefined" &&  arr[i1].Frequency != 0xFFFFFFFF)
            {
              access_point["Channel"] = _GetChannelNumber(arr[i1].Frequency);
            }

            //access_pts[access_point["BSSID"]] = access_point;
            access_pts[access_point["SSID"]] = access_point;
          }
        }
      }
      catch(err)
      {
        _exception.HandleException(err,'$ss.snapin.network.wireless','_GetBssidListInfo',arguments);
      }
      return access_pts;
    }
  });
  
  var _logger = $ss.agentcore.log.GetDefaultLogger("$ss.snapin.network.wireless");   
  var _exception = $ss.agentcore.exceptions;
  var _constants = $ss.snapin.network.wireless.constants;
  
  /*******************************************************************************
  ** Private Methods Below
  ********************************************************************************/
  function _ConvertSupportedRates(sRateStr)
  {
    function convert(rate)
    {
      return (rate & 0x7F) * 0.5;
    }
    var rates = sRateStr.split(":");
    var rateInMbitPerSec = [];
    for(var i = 0; i < rates.length; i++)
    {
      var rate = convert(rates[i]);
      if(rate != 0) rateInMbitPerSec[rateInMbitPerSec.length] = rate;
    }
    return rateInMbitPerSec.join(",");
  }

  /*******************************************************************************
  ** Name:         _GetChannelNumber
  ** Purpose:      calculate 802.11b channel number for given frequency
  ** Parameter:    nFreq_kHz - frequency in kHz
  ** Return:       return 1-14 based on the given ulFrequency_kHz
  **               return 0 for invalid frequency range
  **
  **               2412 MHz = ch-1
  **               2417 MHz = ch-2
  **               2422 MHz = ch-3
  **               2427 MHz = ch-4
  **               2432 MHz = ch-5
  **               2437 MHz = ch-6
  **               2442 MHz = ch-7
  **               2447 MHz = ch-8
  **               2452 MHz = ch-9
  **               2457 MHz = ch-10
  **               2462 MHz = ch-11
  **               2467 MHz = ch-12
  **               2472 MHz = ch-13
  **               2484 MHz = ch-14
  *******************************************************************************/
  function _GetChannelNumber(nFreq_kHz)
  {
    var ulFrequency_MHz = nFreq_kHz/1000;
    if((2412<=ulFrequency_MHz) && (ulFrequency_MHz<2484))
      return ((ulFrequency_MHz-2412)/5)+1;
    else if(ulFrequency_MHz==2484)
      return 14;
    return 0;   // invalid channel number
  }

  function _BssidDecToHex(bssid)
  {
    function dec2hex(n)
    {
      var hex = "0123456789ABCDEF";
      var mask = 0xf;
      var retstr = "";
      while(n != 0)
      {
        retstr = hex.charAt(n&mask) + retstr;
        n>>>=4;
      }
      if(retstr.length==0) retstr = "0";
      if(retstr.length==1) retstr = "0" + retstr; // pad 0 in front
      return retstr;
    }

    if(bssid.length == 0) return "";
    var BssidInHex = [];
    var BssidInDec = bssid.split(":");
    for (var i = 0; i < BssidInDec.length; i++)
    {
      BssidInHex[i] = dec2hex(BssidInDec[i]);
    }
    return BssidInHex.join(":");
  }

  function _GetChannelFromConfigString(conf)
  {
    var freq = 0;
    var confParts = conf.split(";");
    for(var j = 0; j < confParts.length; j++)
    {
      var attr = confParts[j].split("|");
      if(attr[0] == "DSConfig")
      {
        freq = attr[1];
      }
    }
    return _GetChannelNumber(freq);
  }

  function _Validate_wep_hex(key, encLevel)
  {
    try
    {
      if(typeof(encLevel) == "undefined")
      {
        // auto-determine key len
        switch(key.length)
        {
          case 10: encLevel = 64; break;
          case 26: encLevel = 128; break;
        }
      }

      if (encLevel != 64 && encLevel != 128 && encLevel != 256)
      {
        return false;
      }
      if (encLevel == 64 && key.length != 10)
      {
        return false;
      }
      if (encLevel == 128  && key.length != 26)
      {
        return false;
      }

      var hexstr = key.match(/[a-f0-9]+/i);  // test if it's hex
      return (hexstr && hexstr == key);
    }
    catch (err)
    {
      _exception.HandleException(err,'$ss.snapin.network.wireless','_Validate_wep_hex',arguments);
    }
    return false;
  }

// algorithm based on the WEP defacto standard described in http://www.lava.net/~newsham/wlan/
  function _Wep_keygen64(sPassphrase)
  {
    var seed = 0;
    var keys = new Array(4);

    // seed is generated by xor'ing in the keystring bytes into the 4 bytes of the seed
    var shift = 0;
    for(var i = 0; i < sPassphrase.length; i++)
    {
      shift = i & 0x3;
      seed ^= (sPassphrase.charCodeAt(i) << (shift * 8));
    }

    // generate four subkeys from a seed using the defacto standard
    for(var i = 0; i < 4; i++)
    {
      keys[i] = "";
      for(var j = 0; j < 5; j++)
      {
        var key = 0;
        seed *= 0x343fd;
        seed += 0x269ec3;
        seed &= 0xffffffff;
        key = (seed >> 16) & 0xff;
        key = (key.toString(16)).toUpperCase();
        if(key.length == 1) key = '0' + key;
        keys[i] += key;
      }
    }
    return keys;
  }

  function _Wep_keygen128(sPassphrase)
  {
    var buffer = '';

    // repeat passphrase until buf is full
    for (var i=0; i < 64; i++)
    {
      buffer += sPassphrase.charAt(i % sPassphrase.length);
    }

    // extract first 26 chars resulting in 104 bits of keying
    buffer = $ss.agentcore.utils.GenerateMD5(buffer).toUpperCase();
    return buffer.substr(0, 26);
  }

  function _Validate_wep_ascii(key)
  {
    try
    {
      // key can be entered as 5 or 13 ascii characters
      if(key.length == 5 || key.length == 13)
      {
        var ascii = key.match(/[\x01-\xff]+/i);
        return (ascii && ascii == key);
      }
    }
    catch (err)
    {
      _exception.HandleException(err,'$ss.snapin.network.wireless','_Validate_wep_ascii',arguments);
    }
    return false;
  }  

  function _Is2KAndAbove()
  {
    return ($ss.agentcore.utils.system.GetOSGroup() == "NT") && ($ss.agentcore.utils.system.GetOS() != "WINNT");
  }

  function _Maskequal(val, flag)
  {
    return ((val & flag) == flag);
  }

  /*******************************************************************************
  ** WZC manipulation routines  
  ********************************************************************************/

  /*******************************************************************************
  ** Name:         _WZC_IsSvcEnabledForAdapter
  **
  ** Purpose:      Determins if WZCSVC is enabled for the specified adapter
  **
  ** Parameter:    sSvcName - Service Name of adapter (basically id of adapter)
  **
  ** Return:       true/false
  ********************************************************************************/
  function _WZC_IsSvcEnabledForAdapter(sSvcName)
  {
    var  regKey = $ss.snapin.network.wireless.constants.g_WZCRegKey + sSvcName;
    var  controlFlags = $ss.agentcore.dal.registry.GetRegValue("HKLM", regKey, "ControlFlags");
    controlFlags = parseInt(controlFlags, 16);  // hex?

    // make sure the correct bits are toggled ON
    return _Maskequal(controlFlags , 0x8002);  // 0x8000 means "Use Windows to configure my wireless network settings"
                                                         // 0x0002 means "Any available network (access point preferred)"
  }

  function _GetSSIDFromProfile(str)
  {
    var  i = 40;  // start at byte 21st
    var  numTimes = 0;
    var  ssid = "";

    do
    {
      // process a byte at a time
      var byteStr = str.charAt(i) + str.charAt(i+1);  // string representation of current byte
      var byteVal = parseInt(byteStr, 16);            // numerical value of current byte

      if(byteVal == 0) break;

      ssid += String.fromCharCode(byteVal);
      i += 2;
      numTimes++;
    } while (numTimes < 32);

    return ssid;
  }    

  function _GetWirelessProfilesCount(svcName)
  {
    var cnt = 0;
    var profiles = $ss.snapin.network.wireless.utility.WZC_GetWirelessProfiles(svcName);
    for (var i in profiles) cnt++;
    return cnt;
  }

  function _GetProfileStr(svcName, strProfileNum)
  {
    // check registry if profile exists
    var  regKey = $ss.snapin.network.wireless.constants.g_WZCRegKey + svcName;
    var  profValue = $ss.agentcore.dal.registry.GetRegValue("HKLM", regKey, "Static#" + strProfileNum);
    return profValue;
  }

  function _SetProfileStr(svcName, strProfileNum, data)
  {
    var  regKey = $ss.snapin.network.wireless.constants.g_WZCRegKey + svcName;
    return $ss.agentcore.dal.registry.SetRegValueByType("HKLM", regKey, "Static#" + strProfileNum, 3, data);
  }

  function _DeleteProfileStr(svcName, strProfileNum)
  {
    var  regKey = $ss.snapin.network.wireless.constants.g_WZCRegKey + svcName;
    return $ss.agentcore.dal.registry.DeleteRegVal("HKLM", regKey, "Static#" + strProfileNum);
  }

  function _NumberInHex(num, digits)
  {
    var strNum = num.toString(16);
    var len = digits - strNum.length;

    for (var i = 0; i < len; i++)
    {
      strNum = "0" + strNum;
    }

    return strNum;
  }


})();




/*******************************************************************************
**
** Main interface for manipulating wireless profiles
**
********************************************************************************/
function ssWiFiProfileCfgBase(sSvcName)
{
  this.sSvcName = sSvcName;
  this._logger = $ss.agentcore.log.GetDefaultLogger("$ss.snapin.network.wireless");   
  this._exception = $ss.agentcore.exceptions;
  this._constants = $ss.snapin.network.wireless.constants;
}

// base class prototypes
ssWiFiProfileCfgBase.prototype.CreateProfile = function(oWifiProps, bDeleteBeforeCreate) { this._logger.info("CreateProfile", "Not Implemented in " + this.toString()); }
ssWiFiProfileCfgBase.prototype.DeleteProfile = function(sProfile, sSsid) { this._logger.info("DeleteProfile", "Not Implemented" + this.toString()); }
ssWiFiProfileCfgBase.prototype.GetProfiles = function() { this._logger.info("GetProfiles", "Not Implemented" + this.toString()); }
ssWiFiProfileCfgBase.prototype.Connect = function(sProfile, sSsid) { this._logger.info("Connect", "Not Implemented" + this.toString()); }
ssWiFiProfileCfgBase.prototype.Disconnect = function() { this._logger.info("Disconnect", "Not Implemented" + this.toString()); }
ssWiFiProfileCfgBase.prototype.Scan = function() { this._logger.info("Scan", "Not Implemented" + this.toString()); }
ssWiFiProfileCfgBase.prototype.GetAvailableNetworks = function(bRefresh) { this._logger.info("GetAvailableNetworks", "Not Implemented" + this.toString()); }
ssWiFiProfileCfgBase.prototype.toString = function() { return "{ssWiFiProfileCfgBase}"; }

/*******************************************************************************
** Intermediate base class implementation
********************************************************************************/
function ssWiFiProfileCfg(sSvcName)
{
  this._logger = $ss.agentcore.log.GetDefaultLogger("$ss.snapin.network.wireless");   
  this._exception = $ss.agentcore.exceptions;
  this._constants = $ss.snapin.network.wireless.constants;
  ssWiFiProfileCfgBase.call(this, sSvcName);  // ssWiFiProfileCfg inherits from ssWiFiProfileCfgBase
}

ssWiFiProfileCfg.prototype = new ssWiFiProfileCfgBase();

ssWiFiProfileCfg.prototype.CreateProfile = function(oWifiProps, bDeleteBeforeCreate)
{
  var ret = -1;

  try
  {
    this.sWiFiCfgPath = this.GetWiFiCfgPath();

    if($ss.agentcore.dal.file.FileExists(this.sWiFiCfgPath))
    {
      var cmd = "\"" + this.sWiFiCfgPath + "\"";
      for(var prop in oWifiProps)
      {
        if(oWifiProps[prop] && oWifiProps[prop].length)
        {
          cmd += " /" + prop + " " + "\"" + oWifiProps[prop] + "\"";
        }
      }

      // show debugging info if cfg file says so
      var bDebugWifiCfg = $ss.agentcore.dal.config.GetConfigValue("wireless", "debug_wificfg", "false");
      if (bDebugWifiCfg == "true")
      {
        cmd += " /debugshow";
        cmd += " /verbose";
      }

      cmd += " /op create";

      if(typeof(bDeleteBeforeCreate) != "undefined" && bDeleteBeforeCreate)
      {
        this.DeleteProfile(oWifiProps["ssid"], oWifiProps["ssid"]);
      }

      this._logger.info('ssWifProfileCfg::CreateProfile', "cmd: " + cmd);
      return $ss.agentcore.utils.system.RunCommand(cmd, $ss.agentcore.constants.BCONT_RUNCMD_NORMAL );
    }
  }
  catch(err)
  {
    this._exception.HandleException(err,'$ss.snapin.network.wireless','ssWiFiProfileCfg.prototype.CreateProfile',arguments);
  }

  return ret;
}

ssWiFiProfileCfg.prototype.GetAvailableNetworks = function (bRefresh)
{
  var sSvcName = this.sSvcName;

  // make sure wireless service is started
  this.EnableSvcIfDisabled();

  if (bRefresh)
  {
    this.Scan();
  }
  return $ss.snapin.network.wireless.utility.GetBssidListInfo(null, sSvcName, bRefresh);
}

ssWiFiProfileCfg.prototype.GetWiFiCfgPath = function ()
{ 
  var wificfgPath = $ss.agentcore.dal.config.GetProviderBinPath() + "wificfg.exe";
  this._logger.info("ssWiFiProfileCfg::GetWiFiCfgPath", "wificfgPath: " + wificfgPath);

  // search sequence
  // 1) provider bin
  // 2) non-http rootpath
  // 3) non-http product rootpath
  if(!$ss.agentcore.dal.file.FileExists(wificfgPath))
  {
    var rootPath = $ss.agentcore.dal.config.ParseMacros("%ROOTPATH%");
    if (!rootPath.toLowerCase().startsWith("http"))
    {
      wificfgPath  = rootPath + "bin\\wificfg.exe";
      this._logger.info("ssWiFiProfileCfg::GetWiFiCfgPath", "wificfgPath (rootpath): " + wificfgPath);
      if(!$ss.agentcore.dal.file.FileExists(wificfgPath))
      {
        var productRootPath = $ss.agentcore.dal.config.ParseMacros("%ROOTPATH%%PRODUCTPATH%");
        wificfgPath = productRootPath + "\\bin\\wificfg.exe";
        this._logger.info("ssWiFiProfileCfg::GetWiFiCfgPath", "wificfgPath (productRootpath): " + wificfgPath);
      }
    }

    // last attempt
    if(!$ss.agentcore.dal.file.FileExists(wificfgPath))
    {
      this._logger.info("ssWiFiProfileCfg::GetWiFiCfgPath", "wificfgPath does not exist: " + wificfgPath);
      rootPath = $ss.agentcore.dal.ini.GetPath("EXE_PATH");
      rootPath = rootPath.substring(0, rootPath.lastIndexOf("\\"));
      wificfgPath = rootPath + "\\wificfg.exe";
      this._logger.info("ssWiFiProfileCfg::GetWiFiCfgPath", "wificfgPath (exe_path): " + wificfgPath);
    }
  }
  return wificfgPath;
}

ssWiFiProfileCfg.prototype.toString = function()
{
  return "{ssWiFiProfileCfg}";
}

/*******************************************************************************
** Concrete implementation for Wireless Zero Config (Windows XP and 2003)
********************************************************************************/
function ssWZCProfileCfg(sSvcName)
{
  this._logger = $ss.agentcore.log.GetDefaultLogger("$ss.snapin.network.wireless");   
  this._exception = $ss.agentcore.exceptions;
  this._constants = $ss.snapin.network.wireless.constants;
  ssWiFiProfileCfg.call(this, sSvcName);
}

ssWZCProfileCfg.prototype = new ssWiFiProfileCfg();  // ssWZCProfileCfg inherits from ssWiFiProfileCfg

/*******************************************************************************
** Name:         ssWZCProfileCfg::CreateProfile
**
** Purpose:      Create wireless profile through wificfg.exe
**
** Parameter:    oWifiProps - object/hash that contains the command line arguments
**               bDeleteBeforeCreate - delete profile before creating
**
** Return:       integer - process return code
********************************************************************************/
ssWZCProfileCfg.prototype.CreateProfile = function(oWifiProps, bDeleteBeforeCreate)
{
  var sSvcName = this.sSvcName;

  // hack to workaround winxp sp0 where an adapter restart is necessary for the connection
  // to show up in "Network connections"
  if (!$ss.agentcore.network.netcheck.GetAdapterConnectionName(sSvcName))
  {
    $ss.agentcore.network.netcheck.DisableAdapter(null, sSvcName);
    $ss.agentcore.utils.system.Sleep(2000);
    $ss.agentcore.network.netcheck.EnableAdapter(null, sSvcName);
    _logger.info("ssWZCProfileCfg::CreateProfile", "Restarted adapter");
  }
  
  this.EnableSvcIfDisabled();   // enable wzcsvc if disabled

  var ret = ssWiFiProfileCfg.prototype.CreateProfile.call(this, oWifiProps, bDeleteBeforeCreate);

  // 7 means timed-out waiting for dialog to dismiss (non-fatal)
  if (ret == 7) ret = 0;

  return ret;
}

/*******************************************************************************
** Name:         ssWZCProfileCfg::DeleteProfile
**
** Purpose:      Delete a particular preferred network from wireless zero config
**
** Parameter:    sProfile - network profile in question
**               sSsid - ssid in question (typically the same as sProfile unless
**               on Vista)
**
** Return:       true/false
**
** Note:         This method might break in future OSes
********************************************************************************/
ssWZCProfileCfg.prototype.DeleteProfile = function(sProfile, sSsid)
{
  var sSvcName = this.sSvcName;

  try
  {
    var ret = true;

    // consider success if no profile with such SSID exist
    if($ss.snapin.network.wireless.utility.GetWZCProfileIndex(sSvcName, sSsid) == "") return ret;

    if($ss.snapin.network.wireless.utility.StartSvc(false, $ss.snapin.network.wireless.constants.g_WZCSVCNAME))  // first stop the service
    {
      // delete WZC Profile
      ret = $ss.snapin.network.wireless.utility.RemoveWZCProfile(sSvcName, sSsid);

      // delete EAPOL settings if exists
      if($ss.snapin.network.wireless.utility.GetEAPOLIndex(sSvcName, sSsid) != "")
      {
        ret = $ss.snapin.network.wireless.utility.RemoveEAPOLKey(sSvcName, sSsid);
      }

      // restart service
      return $ss.snapin.network.wireless.utility.StartSvc(true, $ss.snapin.network.wireless.constants.g_WZCSVCNAME) && ret;  // start service
    }
  }
  catch(err)
  {
    this._exception.HandleException(err,'$ss.snapin.network.wireless','ssWZCProfileCfg.prototype.DeleteProfile',arguments);
  }
  return false;
}

/*******************************************************************************
** Name:         ssWZCProfileCfg::GetProfiles
**
** Purpose:      Get a list of preferred networks set up for this adapter
**
** Parameter:
**
** Return:       hash - {"0000" : "hq@air, "0001" : "hq@home"}
********************************************************************************/
ssWZCProfileCfg.prototype.GetProfiles = function()
{
  var sSvcName = this.sSvcName;
  return $ss.snapin.network.wireless.utility.WZC_GetWirelessProfiles(sSvcName);
}

ssWZCProfileCfg.prototype.EnableSvcIfDisabled = function()
{
  var sSvcName = this.sSvcName;
  try
  {
    // for WZC, we need to make sure Wireless Zero Config is started and set to "Automatic"
    // and the adapter is actually configured to use WZCSVC to manage wireless networks
    //if (!$ss.snapin.network.wireless.utility.IsSVCRunning($ss.snapin.network.wireless.constants.g_WZCSVCNAME) || !_WZC_IsSvcEnabledForAdapter(sSvcName))
    if (!$ss.snapin.network.wireless.utility.IsSVCRunning($ss.snapin.network.wireless.constants.g_WZCSVCNAME))
    {
      // change start type to "auto" before starting service
      $ss.snapin.network.wireless.utility.ModifySvcStartMode($ss.agentcore.constants.BCONT_SERVICE_AUTO_START, $ss.snapin.network.wireless.constants.g_WZCSVCNAME);      
      $ss.snapin.network.wireless.utility.WZC_EnableSvcForAdapter(sSvcName);
    }
  }
  catch(err)
  {
    this._exception.HandleException(err,'$ss.snapin.network.wireless','ssWZCProfileCfg.prototype.EnableSvcIfDisabled',arguments);
    return false;
  }
  return true;
}

ssWZCProfileCfg.prototype.toString = function()
{
  return "{ssWZCProfileCfg}";
}

/*******************************************************************************
** Concrete implementation for Auto Config Module (Windows Vista)
********************************************************************************/
function ssACMProfileCfg(sSvcName)
{
  this._logger = $ss.agentcore.log.GetDefaultLogger("$ss.snapin.network.wireless");   
  this._exception = $ss.agentcore.exceptions;
  this._constants = $ss.snapin.network.wireless.constants;
  ssWiFiProfileCfg.call(this, sSvcName);  // ssACMProfileCfg inherits from ssWiFiProfileCfg
}

ssACMProfileCfg.prototype = new ssWiFiProfileCfg();

ssACMProfileCfg.prototype.CreateProfile = function(oWifiProps, bDeleteBeforeCreate)
{
  var sSvcName = this.sSvcName;

  // Make sure wlansvc is started
  this.EnableSvcIfDisabled();

  // invoke base class implementation
  var ret = ssWiFiProfileCfg.prototype.CreateProfile.call(this, oWifiProps, bDeleteBeforeCreate);

  // post-process
  if (ret == 0)
  {
    // TTPRO# 9367
    // kickstart the adapter with a disable/enable sequence after profile creation,
    // otherwise it doesn't work (at least for linksys usb)
    if ($ss.agentcore.network.netcheck.DisableAdapter(null, sSvcName))
    {
      $ss.agentcore.utils.system.Sleep(2000);
      $ss.agentcore.network.netcheck.EnableAdapter(null, sSvcName);
    }
    else
    {
      $ss.agentcore.utils.system.RenewIpConfig();
    }
  }

  return ret;
}

ssACMProfileCfg.prototype.DeleteProfile = function(sProfile, sSsid)
{
  this.sWiFiCfgPath = "\"" + this.GetWiFiCfgPath() + "\"";
  var cmd = this.sWiFiCfgPath + " /op delete" + " /servicename " + this.sSvcName + " /ssid " + "\"" + sSsid + "\"";
  $ss.agentcore.utils.system.RunCommand(cmd, $ss.agentcore.constants.BCONT_RUNCMD_NORMAL );
}

ssACMProfileCfg.prototype.EnableSvcIfDisabled = function()
{
  try
  {
    // for ACM, we need to make sure WLANSVC service is started and set to "Automatic"
    if (!$ss.snapin.network.wireless.utility.IsSVCRunning($ss.snapin.network.wireless.constants.g_WLANSVCNAME))
    {
      // change start type to "auto" before starting service      
      $ss.snapin.network.wireless.utility.ModifySvcStartMode($ss.agentcore.constants.BCONT_SERVICE_AUTO_START, $ss.snapin.network.wireless.constants.g_WLANSVCNAME);      
      $ss.snapin.network.wireless.utility.StartSvc(true, $ss.snapin.network.wireless.constants.g_WLANSVCNAME);
    }
  }
  catch(err)
  {
    this._logger.info('ssACMProfileCfg::EnableSvcIfDisabled', err.message);
    return false;
  }
  return true;
}

ssACMProfileCfg.prototype.toString = function()
{
  return "{ssACMProfileCfg}";
}



