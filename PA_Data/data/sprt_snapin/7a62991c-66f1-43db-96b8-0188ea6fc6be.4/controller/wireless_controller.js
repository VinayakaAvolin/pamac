SnapinWirelessController = SteplistBaseController.extend('snapin_wireless',{
 //Public Access Area
 InitParam: 
  {
    aNavigationItems: ["#snapin_wireless #nextbutton","#snapin_wireless #prevbutton" ,"#snapin_wireless #retrybutton", "#snapin_wireless #printbutton", "#snapin_wireless #donebutton"],
    aContentPlaceHolderDIV: "snapin_wireless_content",
    //use the new dll to route this
    sBackToPageURL: $ss.agentcore.dal.config.GetConfigValue("preconfigured_snapin_url","subagent_getconnect_home"),//if one wants to go back to some page 
    sForwardToPageURL: $ss.agentcore.dal.config.GetConfigValue("preconfigured_snapin_url","subagent_getconnect_home"),//snapin completion as well as success url
    sErrorPageURL: $ss.agentcore.dal.config.GetConfigValue("preconfigured_snapin_url","contactus")  //snapin complete failure

  }	
},
{
  //Private Access Area
 index:function(params)
   {
    //clearing Databag values for the animation flow 
    params.stepListSession = params.stepListSession || {};
    params.element = params.element || {};
    var sessionId = params.stepListSession.sessionId || params.element.stepSessionId ;
    var bFirstRun = params.stepListSession.mode==="start";
    params.requestArgs = params.requestArgs || {};
    sesionId = sessionId || params.requestArgs["sl_session"]|| bFirstRun;
    if(!sessionId) 
    {
      $ss.snapin.wireless.ClearDataBagValues();
    }
    $ss.agentcore.log.GetDefaultLogger("$ss.snapin.wireless.controller").info("wireless_controller-index"); 
    this.renderSnapinView("snapin_wireless",params.toLocation,"\\views\\sa_layout.htm");
     //initialize the snapin...
    this._super(params,"snapin_wireless");
    //initialize the step .. however params will have some value 
    var flowInfo = this.Class.InitiateStep(params);
    if(flowInfo) 
    {
      this.ProcessRender(params,flowInfo);
    }
   },

  ProcessNext: function(params)
  {
    var flowInfo = this._super(this,params);
    if(flowInfo) 
    {
      this.ProcessRender(params,flowInfo);
    }
  },
  
  ProcessRetry: function(params)
  {
    var flowInfo = this._super(this,params);
    if(flowInfo) 
    {
      this.ProcessRender(params,flowInfo);
    }
  },

  ProcessPrevious: function(params)
  {
    var flowInfo = this._super(this,params);
    if(flowInfo) 
    {
      this.ProcessRender(params,flowInfo);
    }    
  },
  
  //stepId : "",
  ProcessRender: function(params,flowInfo) 
  {
    var renderHelper = $ss.snapin.wireless.viewhelper || {};
    var stepId = flowInfo.stepid;
    var retData = {};
    var opt = {};
    //start pre render/before load function 
    if (stepId) 
    {
      try 
      {
        if(renderHelper[stepId] && renderHelper[stepId].BeforeRender)
        {
          retData = renderHelper[stepId].BeforeRender(params,this);  
        }
        opt.oLocal = retData;
      } 
      catch(err) 
      {
        //failed to execute the renderhelper function
         $ss.agentcore.log.GetDefaultLogger("$ss.snapin.wireless.controller").error("ProcessRender",err.message); 
          
      }
    }
    //make sure it is being prcoessed...
    this.ProcessFlow(this,params,flowInfo,opt);
    
    //Loading the right frame "Know More" section
    var sName;
    var sArr = [];
    if(flowInfo.type == "url")
    {
      sName = flowInfo.file;
      sArr = sName.split("%/");
      sName = sArr[1];
   
      var fpath=$ss.getSnapinAbsolutePath("snapin_wireless")+"\\views\\helper\\"+$ss.agentcore.dal.config.ParseMacros("%LANGCODE%")+"\\"+sName;
    
      var defaultFileName=$ss.agentcore.dal.config.GetConfigValue("wireless","DefaultFileName");
      var defaultFilePath=$ss.getSnapinAbsolutePath("snapin_wireless")+"\\views\\helper\\"+$ss.agentcore.dal.config.ParseMacros("%LANGCODE%")+"\\"+defaultFileName;
      var isDefaultContent=$ss.agentcore.dal.config.GetConfigValue("wireless","ShowKnowMore_DefaultContent") ;
      
      if($ss.agentcore.dal.file.FileExists(fpath)) 
      {
        this.renderSnapinView("snapin_wireless","ConnectivityFlowHelper","\\views\\helper\\%LANGCODE%\\"+sName);
      }  
      else if(isDefaultContent=="true")
      {
        if($ss.agentcore.dal.file.FileExists(defaultFilePath))
          this.renderSnapinView("snapin_wireless","ConnectivityFlowHelper","\\views\\helper\\%LANGCODE%\\"+defaultFileName);
          
      }    
      else
      {
        $("#ConnectivityFlowHelper").html("");
      }
        
    }
    //End Loading the right frame "Know More" section
    
    // start the post processing
    if (stepId) 
    {
      try 
      {
        var afterStatus = {};
        if(renderHelper[stepId] && renderHelper[stepId].AfterRender) 
        {
          afterStatus = renderHelper[stepId].AfterRender(params,this);  
        }
        if(afterStatus!=undefined)
        {
          switch (afterStatus.move) 
          {
           case "forward":
            return this.ProcessNext(params);
            break;
           case "back":
            return this.ProcessPrevious(params);
            break;
           default:
            break;
          }
        }
                
      } 
      catch(err) 
      {
          //failed to execute the renderhelper after function
          $ss.agentcore.log.GetDefaultLogger("$ss.snapin.wireless.controller").error("ProcessRender",err.message); 
           
      }  
    }  //end of post process
  },
  
  "#donebutton click" : function(params)
  {   
    var bContinue = true;    
    var stepId = params.element.stepId;
    var navHandler = $ss.snapin.wireless.viewhelper || {};
    try 
    {
      if (stepId) 
      {
        if (navHandler[stepId] && navHandler[stepId].Finish) 
        {
          bContinue = navHandler[stepId].Finish(params, this);
        }
      }
    
    } 
    catch(err) 
    {
       $ss.agentcore.log.GetDefaultLogger("$ss.snapin.wireless.controller").error("Done Button Click",err.message);       
    }
    
    if(bContinue == undefined  || bContinue==true)
      this.ProcessNext(params);    
  },
   
  "#nextbutton click" : function(params)
  { 
   if (!$("#nextbutton").hasClass("btndisabled")) //Control whether to proceed with the onClick event on the navigation images (buttons)
   {
    var bContinue = true;
    $("#retrybutton").css("display", "none");
    $ss.snapin.wireless.buttonhelper.updatelayoutbuttons("ENABLE_BACK");
    $ss.snapin.wireless.buttonhelper.updatelayoutbuttons("HIDE_PRINT");    
    var stepId = params.element.stepId;
    var navHandler = $ss.snapin.wireless.viewhelper || {};
    try 
    {
      if (stepId) 
      {
        if (navHandler[stepId] && navHandler[stepId].NavNext) 
        {
          bContinue = navHandler[stepId].NavNext(params, this);
        }
      }
    
    } 
    catch(err) 
    {
       $ss.agentcore.log.GetDefaultLogger("$ss.snapin.wireless.controller").error("Next Button Click",err.message);       
    }
    
    if(bContinue == undefined  || bContinue==true)
      this.ProcessNext(params);
   }
   else return false;
    
  },
  "#retrybutton click": function(params)
  {
    if (!$("#retrybutton").hasClass("btndisabled"))  //Control whether to proceed with the onClick event on the navigation images (buttons)
    {
      var stepId = params.element.stepId;
      var renderHelper = $ss.snapin.wireless.viewhelper || {};
      
      try 
      {
        if (stepId) 
        {
          if (renderHelper[stepId] && renderHelper[stepId].Retest) 
          {
            renderHelper[stepId].Retest(params, this);
          }
        }
      } 
      catch(err) 
      {
        $ss.agentcore.log.GetDefaultLogger("$ss.snapin.wireless.controller").error("Retry Button Click",err.message);      
      }
      this.ProcessRetry(params);
    }
    else return false;
   
  },
  
  "#printbutton click": function(params)
  {
     if (!$("#printbutton").hasClass("btndisabled"))  //Control whether to proceed with the onClick event on the navigation images (buttons)
    {
      var stepId = params.element.stepId;
      var renderHelper = $ss.snapin.wireless.viewhelper || {};
      
      try 
      {
        if (stepId) 
        {
          if (renderHelper[stepId] && renderHelper[stepId].Print) 
          {
            renderHelper[stepId].Print(params, this);
          }
        }
      } 
      catch(err) 
      {
        $ss.agentcore.log.GetDefaultLogger("$ss.snapin.wireless.controller").error("Print Button Click",err.message);      
      }      
    }
    else return false;
   
  },
  "#prevbutton click": function(params)
  {
    if (!$("#prevbutton").hasClass("btndisabled")) 
   { //Control whether to proceed with the onClick event on the navigation images (buttons)
    try
     {
       var bContinue = true;
       $("#retrybutton").css("display", "none");
       $ss.snapin.wireless.buttonhelper.updatelayoutbuttons("HIDE_DONE");
       $ss.snapin.wireless.buttonhelper.updatelayoutbuttons("ENABLE_NEXT");
       $ss.snapin.wireless.buttonhelper.updatelayoutbuttons("HIDE_PRINT");    
       var stepId = params.element.stepId;
       var navHandler = $ss.snapin.wireless.viewhelper || {};
        
        if (stepId) 
        {
          if (navHandler[stepId] && navHandler[stepId].NavBack) 
          {
            bContinue = navHandler[stepId].NavBack(params, this);
          }
        }     
       this.ProcessPrevious(params);
     }
     catch(err)
     {
       $ss.agentcore.log.GetDefaultLogger("$ss.snapin.wireless.controller").error("Previous Button Click",err.message);
      
     }		
	 }
	 else return false;     
  },
 //ss_wrl_setupsecurity.htm events
  "#dvMainSetupSecurity .clsRadioButton click":function(params)
  {
    try
    {
      $ss.snapin.wireless.viewhelper.doad_ss_wrl_setupsecurity.OnSelect(params.element.value);
    }
    catch(err)
    {
      $ss.agentcore.log.GetDefaultLogger("$ss.snapin.wireless.controller").error("ss_wrl_setupsecurity OnSelect event",err.message);
      
    } 
  },
  
  //ss_wrl_manualselection.htm events
   "#dvmainManualSelection .clsRadioButton click":function(params)
  { 
    try
    {
      $ss.snapin.wireless.viewhelper.doad_ss_wrl_manualselection.OnSelect(params.element.value);
    }
    catch(err)
    {
      $ss.agentcore.log.GetDefaultLogger("$ss.snapin.wireless.controller").error("ss_wrl_manualselection OnSelect event",err.message);
      
    }  
  }
  
  
}

);//end of extend



