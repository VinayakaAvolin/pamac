SnapinProtectrestoreController = SnapinBaseController.extend('snapin_protectrestore',
{
  _protect: null,
  _restore: null,
  _undo: null,
  _toLocation: null,
  _snapinPath: null,
  _langCode: null,
  _controller: null,
  //Survey Feature Starts
  _content: null
  //Survey Feature End
},
{
  index:function(params)
  {
    fromSnapin = false;
    this.Class._toLocation = params.toLocation;
    sProtectId = params.SelectedId || {};
    if(params.SelectedId){
      fromSnapin = true;
    }
    this.Class._protect = null;
    this.Class._restore = null;
    this.Class._undo = null;
    this.Class._snapinPath = null;
    this.Class._langCode = null;
    this.Class._controller = null;
    //Survey Feature Starts
    _content = $ss.agentcore.dal.content;
    //Survey Feature End

    this.SetSnapinPath();
    this.SetLangCode();      
    this.Class._model = new $ss.snapin.protectrestore.model();
    this.Class._protectController = new ProtectController(this.Class._snapinPath,this.Class._langCode);
    
    this.Class._undoController = new UndoController(this.Class._snapinPath,this.Class._langCode);
    
    this.Class._controller = this.Class._protectController;
      if (bMac) {
        var that = this;
        that.params = params;
        fromFunction = 0; //from index function
        _LoadRequiredDataBeforeRender(that);
      }else {
    this.SelectLink();
    this.applyNewStyle("#lnkProtect");      
    if (params.element.id == "lnkRestore") {
      fromSnapin = true;
      this.Class.dispatch("snapin_protectrestore", ".sinavlinks click", params);
    }  
    sProtectId = {};
    fromSnapin = false; //Reinitialize once after used.so that the value will not be refereed on reload.

      }
  },
  
  ".viewall click":function(params){
    var action = "";
    var $id = $("#" + params.element.id);
    var idaction = $id.attr("id");
    if($("#" + idaction).attr("disabled") == "disabled") {
      return false;
    }
    if(idaction == "viewallbtn_Protect") action = "lnkProtect";
    else  action = "lnkRestore";
    params.element.id = action;
    this.Class.dispatch("snapin_protectrestore", ".sinavlinks click", params);
  },

  indexActionWidget:function(params){
    this.Class._toLocation = params.toLocation;
    sProtectId = params.SelectedId || {};
    this.Class._protect = null;
    this.Class._snapinPath = null;
    this.Class._langCode = null;
    this.Class._controller = null;
    
    this.SetSnapinPath();
    this.SetLangCode();      
    this.Class._model = new $ss.snapin.protectrestore.model();
    this.Class._protectController = new ProtectController(this.Class._snapinPath,this.Class._langCode);     
    
    this.Class._controller = this.Class._protectController;
      if (bMac) {
        fromFunction = 1;
        var that = this;
        that.params = params;
        _LoadRequiredDataBeforeRender(this);
      }else {
    this.Class._controller.BeforeRender();
    var protections = this.GetItems();
    var acontenttoProtect =[];
    for(var i=0; i < protections.length; i++){
      if(protections[i].lastProtected === _utils.LocalXLate("snapin_protectrestore", "neverSaved")){
        acontenttoProtect.push(protections[i]);
      }
    }
    params.guidListToProtect = acontenttoProtect;
      }
  },

  RenderPage:function(controller,snapinName,page)
  {
    var sSnapinPath = $ss.getSnapinAbsolutePath(snapinName);
    try {
      return controller.render({
        partial: sSnapinPath + page,
        absolute_url: sSnapinPath + page,
        cache: false
      });
    } 
    catch (ex) {
      return false;
    }
  },
  
  SelectLink:function()
  {
    this.Class._controller.BeforeRender();
    this.renderSnapinView("snapin_protectrestore", this.Class._toLocation, "\\views\\protectsettings_index.html", this.Class._controller.GetRenderData());
    this.Class._controller.AfterRender();
  },
  
  CheckisArray:function(sProtectId){
    if(sProtectId){
      sProtectId = $.isArray(sProtectId[0].id) ? sProtectId[0].id : sProtectId[0].id.split(",");
    }
    return sProtectId;
  },
  
  GetItems:function()
  {
    return this.Class._controller.GetItems();
  },
  
  "#btnAction click":function()
  {
    if (bMac) {
      this.Class._controller.ExecuteMac();
    }else {
    this.Class._controller.Execute();
    this.SelectLink();
    }
    
  },
  
  GetRestoreController:function()
  {
    this.Class._restore = this.Class._restore ||  
                                    new RestoreController(this.Class._snapinPath,this.Class._langCode);
                                    
    return this.Class._restore;
  },
  
  GetProtectController:function()
  {
    this.Class._protect = this.Class._protect|| 
                                    new ProtectController(this.Class._snapinPath,this.Class._langCode);
                                    
    return this.Class._protect;
  },
  
  GetUndoController:function()
  {
    this.Class._undo = this.Class._undo || 
                                    new UndoController(this.Class._snapinPath,this.Class._langCode);
                                    
    return this.Class._undo;
  },
  
  ".sinavlinks click": function(params) {
      this.applyDefaultStyle();
      var elemID = "#" + params.element.id;
      switch (elemID){
        case "#lnkProtect" :
          if(this.Class._controller != this.GetProtectController())
          this.Class._controller = this.GetProtectController();
          break;
        case "#lnkRestore" :
          if(this.Class._controller != this.GetRestoreController())
          this.Class._controller = this.GetRestoreController();
          break;
        case "#lnkUndo" :
          if(this.Class._controller != this.GetUndoController())
          this.Class._controller = this.GetUndoController();
          break;
      }
    if (bMac) {
      var that = this;
      that.elemID = elemID;
      fromFunction = 2;
      _LoadRequiredDataBeforeRender(that);
    }else {
      this.SelectLink();
      this.applyNewStyle(elemID);
      if(!fromSnapin){
        $("#viewallbtn_Protect").attr("disabled","disabled");
        $("#viewallbtn_Restore").attr("disabled","disabled");
      }
    }
  },

  applyDefaultStyle: function(){
    $('.tab-menu-filters').toggleClass('tab-menu-filters-selected',false);
    $('.tab-menu-filters').toggleClass('tab-menu-filters',true);
  },

  applyNewStyle: function(elemID){
    $(elemID +' .tab-menu-filters').toggleClass('tab-menu-filters-selected',true);
  },
  
  "#chkProtectAll click":function(params)
  {
    this.Class._protectController.CheckAll(params.element.checked);
  },
  
  ".clsUndo click":function()
  {
    this.Class._controller.Select();
  },
  
  ".clsProtections click":function()
  {
    this.Class._controller.Select();
  },
  
  ".clsRestores click":function()
  {
    this.Class._controller.Select();
  },
  
  SetSnapinPath:function()
  {
    this.Class._snapinPath = $ss.getSnapinAbsolutePath("snapin_protectrestore");
  },
  
  SetLangCode:function()
  {
    this.Class._langCode = $ss.agentcore.dal.config.ExpandAllMacros("%LANGCODE%");
  },
  LoadRequiredDataBeforeRenderCompleted : function() {
    switch(fromFunction) {
      case 0: {
        this.SelectLink();        
        this.applyNewStyle("#lnkProtect");      
          if (this.params.element.id == "lnkRestore") {
            fromSnapin = true;
            this.Class.dispatch("snapin_protectrestore", ".sinavlinks click", this.params);
          }  
          sProtectId = {};
          fromSnapin = false; //Reinitialize once after used.so that the value will not be refereed on reload. 
      }
      break;

      case 1: {
          this.Class._controller.BeforeRender();
           var protections = this.GetItems();
      //protections[0].lastProtected = "Thursday, December 15 2016 3:45:06 PM";
      //protections[2].lastProtected = "Friday, December 15 2016 3:45:06 PM";
      var acontenttoProtect =[];
      for(var i=0; i < protections.length; i++){
        if(protections[i].lastProtected == "Never Protected")
        {
          //acontenttoProtect.push({"id": protections[i]});
          acontenttoProtect.push(protections[i]);
        }
      }
      //params.guidListToProtect = ([{ "sprt_protect": acontenttoProtect}]);
      this.params.guidListToProtect = acontenttoProtect;
      this.Class.dispatch("snapin_pendingactionwidget", "RenderSnapin", this.params);
      }
      break;

      case 2: {
          this.SelectLink();
          this.applyNewStyle(this.elemID);
          if(!fromSnapin){
            $("#viewallbtn_Protect").attr("disabled","disabled");
            $("#viewallbtn_Restore").attr("disabled","disabled");
          }
      }
      break;
    }
  } 
});

//Survey Feature Starts
//ProtectController = Class.extend(
ProtectController = SnapinBaseController.extend('snapin_protectrestore',
    //Survey Feature End
{
    _data: {},
    _snapinPath: null,
    _skinName: null,
    _langCode: null,
    //Survey Feature Starts
    _content: null
    //Survey Feature End
},
{
  init:function(snapinPath,langCode)
  {
    this.Class._skinName = $ss.agentcore.dal.config.ExpandAllMacros("%SKINNAME%");
    this._modelProtection = new $ss.snapin.protectrestore.model.Protection();
    this.Class._data.sectionTitle = _utils.LocalXLate("snapin_protectrestore", "titleProtect");
    this.Class._data.bttnTxt = _utils.LocalXLate("snapin_protectrestore", "btnProtect");
    this.Class._data.bttnStatus = 'disabled';
    this.Class._snapinPath = snapinPath;
    this.Class._langCode = langCode;
    this.Class._data.page = "\\views\\protectsettings.html";
    //Survey Feature Starts
    this.Class._content = $ss.agentcore.dal.content;
    //Survey Feature End
  },
  
   LoadRequiredDataBeforeRender: function(scope) {
    this._modelProtection.LoadRequiredDataBeforeRender(scope);
  },

  BeforeRender:function()
  {
    this._modelProtection.BeforeRender();
  },
  
  AfterRender:function()
  {
    $ss.agentcore.utils.ui.FixPng();
    this._modelProtection.AfterRender();
  },
  
  Select:function()
  {
    if(this.IsAnyChecked()) $('#btnAction')[0].disabled = false;
    else $('#btnAction')[0].disabled = true;
  },
 
  GetItems:function()
  {
    return this._modelProtection.GetProtections();
  },
  
  ExecuteMac:function() {
    var that = this;
    var moveY = 0;
    var retVal;
    $("#divContent").scrollTo(moveY +'px', { axis:'y' } );
    $(".clsProtections").each(
      function(){
        $("#divContent").scrollTo('+=' + moveY +'px', { axis:'y' } );
        if(this.checked)
        {
          $("#img_" + this.id).attr("src", $ss.GetLayoutSkinPath() + "/icons/loader.gif");
          $("#img_" + this.id).attr("width","16");
          $("#img_" + this.id).attr("height","16");
          
          var idProtect = this.id;
          $ss.agentcore.utils.StartShellBusy();
          that._modelProtection.ProtectMac(idProtect,that);
        }
        moveY += 10;
        // return retVal;
      }
    );
  },

  ProtectionCompleted: function(selectedID) {
    var that = this;
    var moveY = 0;
    var retVal;
     var selectedGuid = that._modelProtection.GetGuidFromIndex(selectedID);
    var lastProtected = that._modelProtection.GetLastProtected(selectedGuid);
               
    $("#divContent").scrollTo(moveY +'px', { axis:'y' } );
    $(".clsProtections").each(
      function(){
        $("#divContent").scrollTo('+=' + moveY +'px', { axis:'y' } );
        if(this.checked)
        {  
          var idProtect = this.id;
          if (idProtect == selectedID) {
            if (lastProtected !== "")
            $("#cellLastProtected_" + this.id).html(lastProtected);
          
            $("#img_" + this.id).attr("src","");
            $("#img_" + this.id).attr("width","0");
            $("#img_" + this.id).attr("height","0");
            this.checked = false;
            that.ExecuteMac();
          }  
        }
        moveY += 10;
      }
    );
    setTimeout(function () {
        that.SelectLink();
        }, 50);
              $ss.agentcore.utils.EndShellBusy();
  },

  Execute:function()
  {
    var that = this;
    var moveY = 0;
    var retVal;
    $("#divContent").scrollTo(moveY +'px', { axis:'y' } );
    $(".clsProtections").each(
      function(){
        $("#divContent").scrollTo('+=' + moveY +'px', { axis:'y' } );
        if(this.checked)
        {
          $("#img_" + this.id).attr("src", $ss.GetLayoutSkinPath() + "/icons/loader.gif");
          $("#img_" + this.id).attr("width","16");
          $("#img_" + this.id).attr("height","16");
          
          var idProtect = this.id;
          retVal = null;
          var opt = 
          {
            fnPtr : function()
                    {
                      retVal = that._modelProtection.Protect(idProtect);
                    },
            timeout : 300,
            sleepTime : 300            
          }
          $ss.agentcore.utils.UpdateUI(opt);

          //Survey Feature Starts
          //var lastProtected = that._modelProtection.GetLastProtected(that._modelProtection.GetGuidFromIndex(idProtect));
          var selectedGuid = that._modelProtection.GetGuidFromIndex(idProtect);
          var lastProtected = that._modelProtection.GetLastProtected(selectedGuid);
          //Survey Feature End

          if (lastProtected !== "")
            $("#cellLastProtected_" + this.id).html(lastProtected);
          
          $("#img_" + this.id).attr("src","");
          $("#img_" + this.id).attr("width","0");
          $("#img_" + this.id).attr("height","0");
        }
        moveY += 10;
        return retVal;
      }
    );
  },
  
  CheckAll:function(selected)
  {
    if(selected)
    {
      $(".clsProtections").prop("checked","checked");
      $('#btnAction')[0].disabled = false;
    }
    else
    {
      $(".clsProtections").removeAttr("checked");
      $('#btnAction')[0].disabled = true;
    }
  },
  
  IsAnyChecked:function()
  {
      var retVal = false;
      $(".clsProtections").each(
      function(){
        if(this.checked)
        {
          if(this.id !== "chkProtectAll")
            retVal = true;
        }
      }
    );

    return retVal;
  },
  
  GetRenderData:function()
  {
    var dataObj =  this.Class._data;
    dataObj.bttnStatus = (sProtectId.length > 0) ? "" : 'disabled';
    return dataObj;
  }

});

//Survey Feature Starts
//RestoreController = Class.extend(
RestoreController = SnapinBaseController.extend('snapin_protectrestore',
    //Survey Feature End
{
    _data: {},
    _snapinPath: null,
    _skinName: null,
    _langCode: null,
    //Survey Feature Starts
    _content: null
    //Survey Feature End
},
{
  init:function(snapinPath,langCode)
  {
    this._modelRestore = new $ss.snapin.protectrestore.model.Restore();
    this.Class._skinName = $ss.agentcore.dal.config.ExpandAllMacros("%SKINNAME%");
    this._restoreSelection = null;
    this.Class._data.sectionTitle = _utils.LocalXLate("snapin_protectrestore", "titleRestore");
    this.Class._data.bttnTxt = _utils.LocalXLate("snapin_protectrestore", "btnRestore");
    this.Class._data.bttnStatus = 'disabled';
    this.Class._snapinPath = snapinPath;
    this.Class._langCode = langCode;
    this.Class._data.page = "\\views\\protectsettings_repair.html";
    //Survey Feature Starts
    this.Class._content = $ss.agentcore.dal.content;
    //Get Registry entry for to find out whether survey is enabled at product level
    //"Enable_Survey" should be an entry in registry at Software\SupportSoft\ProviderList\<provider>\users\<user>\ss_config\global\
    var sRegRoot = "HKCU";
    var provider = $ss.agentcore.dal.config.GetProviderID();
    var user = $ss.agentcore.dal.config.ExpandSysMacro("%USER%");
    var prodName = $ss.agentcore.dal.config.ParseMacros("%PRODUCT%");
    var regPath = "Software\\SupportSoft\\ProviderList\\" + provider + "\\" + prodName + "\\users\\" + user + "\\ss_config\\global\\";
    this.Class._enableSurvey = $ss.agentcore.dal.registry.GetRegValue("HKCU", regPath, "Enable_Survey");
    //Reading from clientui_config.xml file if value doesnot exist in registry
    if(this.Class._enableSurvey.length === 0){
      this.Class._enableSurvey = $ss.agentcore.dal.config.GetConfigValue("global", "show_survey", "false");
    }
    //Survey Feature End
  },
  
  LoadRequiredDataBeforeRender: function(callback) {
    this._modelRestore.LoadRequiredDataBeforeRender(callback);
  },

  BeforeRender:function()
  {
    this._modelRestore.BeforeRender();
  },
  
  AfterRender:function()
  {
    this.Select();
    $ss.agentcore.utils.ui.FixPng();
  },
 
  Select:function()
  {
    var val = $("input[name='rbRestores']:checked").val();
    if(val) $('#btnAction')[0].disabled=false;
    if(val)
    {
      this._restoreSelection = val;
    }
    else
    {
      this._restoreSelection = null;
    }
  },
 
  ExecuteMac:function() {
    var selected = this._restoreSelection;
    var item = {};
    $ss.agentcore.utils.StartShellBusy();

    //Survey Feature Starts
    var restoreResult;

    $("#img_" + selected).attr("src",$ss.GetLayoutSkinPath() + "/icons/loader.gif");
      $("#img_" + selected).attr("width","16");
      $("#img_" + selected).attr("height","16");
    if (this.GetRestoreDetails(item, selected))  {
      this._modelRestore.RestoreMac(this, item.guid, item.version, item.dateProtected);
    }
  },

  RestoreCompleted: function(restoreResult) {
    var selected = this._restoreSelection;
    $("#img_" + selected).attr("src","");
    $("#img_" + selected).attr("width","0");
    $("#img_" + selected).attr("height","0");
    this._restoreSelection = null;
    $ss.agentcore.utils.EndShellBusy();
    this.AfterRender();
    if (restoreResult.iItemsHealed != 0) {
      this.CallSurveyPage(restoreResult);
      if (this.Class._enableSurvey && this.Class._enableSurvey.toLowerCase() == "true") {
          var surveyRating = $ss.agentcore.dal.databag.GetValue("dictRating");
          var surveySolved = $ss.agentcore.dal.databag.GetValue("dictSolved");
          var dictSurveySubmitButtonClicked = $ss.agentcore.dal.databag.GetValue("dictSurveySubmitButtonClicked");

          var objDict = JSON.parse(surveyRating);
          var objDictSolved = JSON.parse(surveySolved);
          var objDictSurveySubmitButtonClicked = JSON.parse(dictSurveySubmitButtonClicked);

          restoreResult.rating = objDict[restoreResult.sContGuid];
          restoreResult.solved = objDictSolved[restoreResult.sContGuid];

          if (objDictSurveySubmitButtonClicked[restoreResult.sContGuid] == true) {
              this.LogHealDone(restoreResult);
          }

          $ss.agentcore.dal.databag.RemoveValue("dictRating");
          $ss.agentcore.dal.databag.RemoveValue("dictSolved");
          $ss.agentcore.dal.databag.RemoveValue("dictSurveySubmitButtonClicked");
      }
    }

  },

  Execute:function()
  {
    var selected = this._restoreSelection;
  
    var item = {};
    //Survey Feature Starts
    var restoreResult;
    try {
      //Survey Feature End
      if (this.GetRestoreDetails(item, selected)) {
        var that = this;
        var opt =
        {
          fnPtr: function () {
            //Survey Feature Starts
            //that._modelRestore.Restore(item.guid, item.version);
            restoreResult = that._modelRestore.Restore(item.guid, item.version);
            //Survey Feature End                        
          },
          timeout : 300,
          sleepTime : 300            
        }
        $("#img_" + selected).attr("src",$ss.GetLayoutSkinPath() + "/icons/loader.gif");
        $("#img_" + selected).attr("width","16");
        $("#img_" + selected).attr("height","16");
          
        $ss.agentcore.utils.UpdateUI (opt);
      
        $("#img_" + selected).attr("src","");
        $("#img_" + selected).attr("width","0");
        $("#img_" + selected).attr("height","0");
      
        this._restoreSelection = null;

      //Survey Feature Starts
      }
    }
    catch (ex) { }
    finally {
      //check if survey is enabled only then show survey snapin

      if (restoreResult.iItemsHealed != 0) {
        this.CallSurveyPage(restoreResult);
        if(this.Class._enableSurvey && this.Class._enableSurvey.toLowerCase() == "true") {
          var surveyRating = $ss.agentcore.dal.databag.GetValue("dictRating");
          var surveySolved = $ss.agentcore.dal.databag.GetValue("dictSolved");
          var dictSurveySubmitButtonClicked = $ss.agentcore.dal.databag.GetValue("dictSurveySubmitButtonClicked");

          var objDict = JSON.parse(surveyRating);
          var objDictSolved = JSON.parse(surveySolved);
          var objDictSurveySubmitButtonClicked = JSON.parse(dictSurveySubmitButtonClicked);

          restoreResult.rating = objDict[restoreResult.sContGuid];
          restoreResult.solved = objDictSolved[restoreResult.sContGuid];

          if (objDictSurveySubmitButtonClicked[restoreResult.sContGuid] == true) {
            this.LogHealDone(restoreResult);
          }

          $ss.agentcore.dal.databag.RemoveValue("dictRating");
          $ss.agentcore.dal.databag.RemoveValue("dictSolved");
          $ss.agentcore.dal.databag.RemoveValue("dictSurveySubmitButtonClicked");
        }
      }
            //Survey Feature End
    }
  },
 
  GetItems:function()
  {
    return this._modelRestore.GetRestores();
  },
  
  GetRestoreDetails:function(item,index)
  {
    var objSel = document.getElementById("selVersion" + index);
    if(!objSel) return false;
    var selectedIndex = objSel.selectedIndex;
    
    if(selectedIndex != -1)
    {
      item.guid = $("#selVersion"+ index+"option"+selectedIndex).attr("guid");   
      item.version = $("#selVersion"+ index+"option"+selectedIndex).attr("version");
      if (bMac) {
        item.dateProtected = objSel.value;
      }
      return true;
    }
    
    return false;
  },
  
  GetRenderData:function()
  {
    return this.Class._data;
  },

    //Survey Feature Starts
  CallSurveyPage: function (restoreResult) {
    if(restoreResult) {
      var params = {};
      var surveyData = {};
      var arrSurveyData = [];
      var protections = this.Class._content.GetContentByCid("sprt_protection", restoreResult.sContGuid, true);

      //Prepare survey data
      surveyData.guidContent = restoreResult.sContGuid;
      surveyData.version = restoreResult.iVersion;
      surveyData.verb = "restore_end";
      surveyData.viewed = "";
      surveyData.comment = "";
      surveyData.data = restoreResult.snapinName;
      surveyData.title = restoreResult.sTitle;
      surveyData.result = restoreResult.iItemsHealed;

      if (restoreResult.iItemsHealed >= 0) {
        surveyData.result = 0;
      }
      else {
        surveyData.result = 1;
      }

      arrSurveyData.push(surveyData);

      params.surveyData = arrSurveyData;
      try {
        if (this.Class._enableSurvey && this.Class._enableSurvey.toLowerCase() == "true") {
          this.Class.dispatch("snapin_survey", "index", params);
        }
      }
      catch (ex) {
      }
      if (!bMac) {
      	params.surveyData = null;
      	params = null;
  	  }
    }
  },
  
  LogStatus: function (surveyData) {
    try {
      var oLogEvnt = $ss.agentcore.reporting.Reports.CreateLogEntry(surveyData.guidContent, surveyData.version, surveyData.verb, surveyData.viewed, surveyData.solved, surveyData.comment, surveyData.data, surveyData.rating, surveyData.result);
      $ss.agentcore.reporting.Reports.Log(oLogEvnt, "snapin_protectrestore");
    }
    catch (ex) {
    }
  },

  _GetProbeObject: function () {
    var _oProbe = null;

    if (!_oProbe) {
      var sID = "Prefix_Protecting";
      var sPrefix = $ss.agentcore.utils.LocalXLate("snapin_protectrestore", sID);
      if (sPrefix === sID) sPrefix = "Protecting ";
            try {
      _oProbe = new $ss.agentcore.dna.protectrestore.Probe(sPrefix);
    }
            catch(ex) {
              var _logger = $ss.agentcore.log.GetDefaultLogger("$ss.agentcore.dal.file");  
            }
        }

    return _oProbe;
  },

  LogHealDone: function (options) {
    var iResult = 0;
    var _constants = $ss.agentcore.constants;
    var surveyData = {};
    try {

      // map iItemsHealed to completion codes for reporting
      if (options.iItemsHealed === 0)
        iResult = _constants.RESULT_SUCCESS_NOCHANGE; // success, but no actions taken
      if (options.iItemsHealed === -1)
        iResult = _constants.RESULT_FAILED; // failure code
      if (options.iItemsHealed > 0)
        iResult = _constants.RESULT_SUCCESS; // success

      //Prepare survey data
      surveyData.guidContent = options.sContGuid;
      surveyData.version = options.iVersion;
      surveyData.verb = "heal_end";
      surveyData.viewed = "view";
      surveyData.comment = "restore_logged";
      surveyData.data = "sprt_protection";
      surveyData.title = options.sTitle;
      surveyData.rating = options.rating;
      surveyData.result = iResult;
      surveyData.solved = options.solved;

      var oLogEntry = this.LogStatus(surveyData);
      return true;
    }
    catch (ex) {
    }
  }
    //Survey Feature End
});


//Survey Feature Starts
//UndoController = Class.extend(
UndoController = SnapinBaseController.extend('snapin_protectrestore',
    //Survey Feature End
{
    _data: {},
    _snapinPath: null,
    _skinName: null,
    _langCode: null,
    //Survey Feature Starts
    _content: null
    //Survey Feature End
},
{
  init:function(snapinPath,langCode)
  {
    this._undoSelection = null;
    this.Class._skinName = $ss.agentcore.dal.config.ExpandAllMacros("%SKINNAME%");
    this._modelUndo = new $ss.snapin.protectrestore.model.Undo();
    this.Class._data.sectionTitle = _utils.LocalXLate("snapin_protectrestore", "titleUndo");
    this.Class._data.bttnTxt = _utils.LocalXLate("snapin_protectrestore", "btnUndo");
    this.Class._data.bttnStatus = 'disabled';
    this.Class._snapinPath = snapinPath;
    this.Class._langCode = langCode;
    this.Class._data.page = "\\views\\protectsettings_undo.html";
    //Survey Feature Starts
    this.Class._content = $ss.agentcore.dal.content;
    //Get Registry entry for to find out whether survey is enabled at product level
    //"Enable_Survey" should be an entry in registry at Software\SupportSoft\ProviderList\<provider>\users\<user>\ss_config\global\
    var sRegRoot = "HKCU";
    var provider = $ss.agentcore.dal.config.GetProviderID();
    var user = $ss.agentcore.dal.config.ExpandSysMacro("%USER%");
    var prodName = $ss.agentcore.dal.config.ParseMacros("%PRODUCT%");
    var regPath = "Software\\SupportSoft\\ProviderList\\" + provider + "\\" + prodName + "\\users\\" + user + "\\ss_config\\global\\";
    this.Class._enableSurvey = $ss.agentcore.dal.registry.GetRegValue("HKCU", regPath, "Enable_Survey");    
    if(this.Class._enableSurvey.length === 0){
      //Reading from clientui_config.xml file if value doesnot exist in registry
      this.Class._enableSurvey = $ss.agentcore.dal.config.GetConfigValue("global", "show_survey", "false");
    }
    //Survey Feature End
  },
  
  LoadRequiredDataBeforeRender: function(scope) {
    this._modelUndo.LoadRequiredDataBeforeRender(scope);
  },

  UndoRestoredCompleted : function(undoResult) {
  var selected = $("#" + this._undoSelection).attr("key");
  var id = $("#" + this._undoSelection).attr("id");
  $("#img_" + id).attr("src","");
  $("#img_" + id).attr("width","0");
  $("#img_" + id).attr("height","0");    
      
  this._undoSelection = null;
  $ss.agentcore.utils.EndShellBusy();

    var storeName = $("input[name='rbUndo']:checked").attr("displayName");
    if (undoResult.iSuccess) {
      this.CallSurveyPage(storeName, undoResult);
      if (this.Class._enableSurvey && this.Class._enableSurvey.toLowerCase() == "true") {
        var surveyRating = $ss.agentcore.dal.databag.GetValue("dictRating");
        var surveySolved = $ss.agentcore.dal.databag.GetValue("dictSolved");
        var dictSurveySubmitButtonClicked = $ss.agentcore.dal.databag.GetValue("dictSurveySubmitButtonClicked");

        var objDict = null;
        var objDictSolved = null;
        var objDictSurveySubmitButtonClicked = null;

        if (surveyRating) {
            objDict = JSON.parse(surveyRating);
        }

        if (surveySolved) {
            objDictSolved = JSON.parse(surveySolved);
        }

        if (dictSurveySubmitButtonClicked) {
            objDictSurveySubmitButtonClicked = JSON.parse(dictSurveySubmitButtonClicked);
        }

        if (objDict != null) {
         undoResult.rating = objDict[undoResult.sContGuid];
        }
        if (objDictSolved != null) 
          undoResult.solved = objDictSolved[undoResult.sContGuid];
          if (objDictSurveySubmitButtonClicked != null) {
          if (objDictSurveySubmitButtonClicked[undoResult.sContGuid] == true) {
              this.LogUndoDone(undoResult);
          }
        }

        $ss.agentcore.dal.databag.RemoveValue("dictRating");
        $ss.agentcore.dal.databag.RemoveValue("dictSolved");
        $ss.agentcore.dal.databag.RemoveValue("dictSurveySubmitButtonClicked");
      }
    }
    if (_protectRestoreController != null) {
          fromFunction = 2;
          _LoadRequiredDataBeforeRender(_protectRestoreController);
    }
  },

  BeforeRender:function()
  {
    this._modelUndo.BeforeRender();
  },
  
  AfterRender:function()
  {
    this.Select();
    $ss.agentcore.utils.ui.FixPng();
  },
  
  Select:function()
  {
    var val = $("input[name='rbUndo']:checked").val();
    var id = $("input[name='rbUndo']:checked").attr("id");
    if(val) $('#btnAction')[0].disabled=false;
    if(val)
    {
      this._undoSelection = id;
    }
    else
    {
      this._undoSelection = null;
    }
  },

  ExecuteMac() {

    $ss.agentcore.utils.StartShellBusy();
    try {
      var selected = $("#" + this._undoSelection).attr("key");
      var id = $("#" + this._undoSelection).attr("id");
      //Survey Feature Starts
      var itemList = this._modelUndo.GetUndoList();

      //Survey Feature End
      var that = this;

      $("#img_" + id).attr("src", $ss.GetLayoutSkinPath() + "/icons/loader.gif");
      $("#img_" + id).attr("width","16");
      $("#img_" + id).attr("height","16");
      this._modelUndo.UndoMac(this, selected);
      
      
     


 }catch(ex) {}
  },

    Execute: function () {
      //Survey Feature Starts
      var undoResult;
      try {
        //Survey Feature End
        var selected = $("#" + this._undoSelection).attr("key");
        var id = $("#" + this._undoSelection).attr("id");
        //Survey Feature Starts
        var itemList = this._modelUndo.GetUndoList();
        //Survey Feature End
        var that = this;
        var opt =
          {
            fnPtr: function () {
            //Survey Feature Starts
            undoResult = that._modelUndo.Undo(selected);
            //Survey Feature End
            },
            timeout : 300,
            sleepTime : 300            
          }
        $("#img_" + id).attr("src", $ss.GetLayoutSkinPath() + "/icons/loader.gif");
        $("#img_" + id).attr("width","16");
        $("#img_" + id).attr("height","16");
          
        $ss.agentcore.utils.UpdateUI (opt);
      
        $("#img_" + id).attr("src","");
        $("#img_" + id).attr("width","0");
        $("#img_" + id).attr("height","0");    
      
        this._undoSelection = null;
            //Survey Feature Starts
      }
      catch (ex) { }
      finally {
        var storeName = $("input[name='rbUndo']:checked").attr("displayName");
        //check if survey is enabled only then show survey snapin

        if (undoResult.iSuccess) {
          this.CallSurveyPage(storeName, undoResult);
          if (this.Class._enableSurvey && this.Class._enableSurvey.toLowerCase() == "true") {
            var surveyRating = $ss.agentcore.dal.databag.GetValue("dictRating");
            var surveySolved = $ss.agentcore.dal.databag.GetValue("dictSolved");
            var dictSurveySubmitButtonClicked = $ss.agentcore.dal.databag.GetValue("dictSurveySubmitButtonClicked");

            var objDict = null;
            var objDictSolved = null;
            var objDictSurveySubmitButtonClicked = null;

            if (surveyRating) {
              objDict = JSON.parse(surveyRating);
            }

            if (surveySolved) {
              objDictSolved = JSON.parse(surveySolved);
            }

            if (dictSurveySubmitButtonClicked) {
              objDictSurveySubmitButtonClicked = JSON.parse(dictSurveySubmitButtonClicked);
            }

            undoResult.rating = objDict[undoResult.sContGuid];
            undoResult.solved = objDictSolved[undoResult.sContGuid];
            if (objDictSurveySubmitButtonClicked[undoResult.sContGuid] == true) {
              this.LogUndoDone(undoResult);
            }

            $ss.agentcore.dal.databag.RemoveValue("dictRating");
            $ss.agentcore.dal.databag.RemoveValue("dictSolved");
            $ss.agentcore.dal.databag.RemoveValue("dictSurveySubmitButtonClicked");
          }
        }
      }
      //Survey Feature End
  },
  
  GetItems:function()
  {
    return this._modelUndo.GetUndoList();
  },
  
  GetRenderData:function()
  {
    return this.Class._data;
  },

  CallSurveyPage: function (storeName, undoResult) {
    var params = {};
    var surveyData = {};
    var arrSurveyData = [];
    var protections = this.Class._content.GetContentByTitle("sprt_protection", storeName);
    if (protections.length > 0) {
      //Ideally there should not be more than one protection with the same name.
      //in worst multiple protections with the same name present select the first one
      var contentData = protections[0];
      //Prepair survey data
      surveyData.guidContent = contentData.cid;
      surveyData.version = contentData.version;
      surveyData.verb = "undo_end";
      surveyData.viewed = "";
      surveyData.comment = "";
      surveyData.data = contentData.snapinName;
      if (undoResult.iSuccess) {
        surveyData.result = 0;
      }
      else {
        surveyData.result = 1;
      }
      surveyData.title = contentData.title;

      arrSurveyData.push(surveyData);

      params.surveyData = arrSurveyData;
      try {
        if (this.Class._enableSurvey && this.Class._enableSurvey.toLowerCase() == "true") {
          this.Class.dispatch("snapin_survey", "index", params);
        }
      }
      catch (ex) { }
    }
    if(!bMac) {
    	params.surveyData = null;
    	params = null;
	}
  },

  LogStatus: function (surveyData) {
    try {
      var oLogEvnt = $ss.agentcore.reporting.Reports.CreateLogEntry(surveyData.guidContent, surveyData.version, surveyData.verb, surveyData.viewed, surveyData.solved, surveyData.comment, surveyData.data, surveyData.rating, surveyData.result);
      $ss.agentcore.reporting.Reports.Log(oLogEvnt, "snapin_protectrestore");
    }
    catch (ex) { }
  },
  
  _GetProbeObject: function () {
    var _oProbe = null;

    if (!_oProbe) {
      var sID = "Prefix_Protecting";
      var sPrefix = $ss.agentcore.utils.LocalXLate("snapin_protectrestore", sID);
      if (sPrefix === sID) sPrefix = "Protecting ";
      _oProbe = new $ss.agentcore.dna.protectrestore.Probe(sPrefix);
    }

    return _oProbe;
  },

  LogUndoDone: function (options) {
    var iResult = 0;
    var _constants = $ss.agentcore.constants;
    var surveyData = {};

    // map iSuccess to completion codes for reporting
    if (options.iSuccess === 0)
      iResult = _constants.RESULT_FAILED; // failure code
    else //if (options.iSuccess === 1 || options.iSuccess == true)
      iResult = _constants.RESULT_SUCCESS; // success

    //Prepare survey data
    surveyData.guidContent = options.sContGuid;
    surveyData.version = options.iVersion;
    surveyData.verb = "undo_end";
    surveyData.viewed = "view";
    surveyData.comment = "undo_logged";
    surveyData.data = "sprt_protection";
    surveyData.title = options.sTitle;
    surveyData.rating = options.rating;
    surveyData.result = iResult;
    surveyData.solved = options.solved;

    try {
      var oLogEntry = this.LogStatus(surveyData);
    }

    catch (ex) { }
    return false;
  }
  //Survey Feature End

});

var sProtectId = {};
var fromSnapin;
var _utils = $ss.agentcore.utils;
var maxStrLength = $ss.agentcore.dal.config.GetConfigValue("snapin_protectrestore","maxStringLenToDisp");

var _jsBridge = window.JSBridge;
var bMac = $ss.agentcore.utils.IsRunningInMacMachine();
var _protectRestoreController = null;
var fromFunction = 0;   //0 = index, 1 = indexActionwidget, 2 = onClick

function  _LoadRequiredDataBeforeRender(scope) {
  _protectRestoreController = scope;
  _protectRestoreController.Class._controller.LoadRequiredDataBeforeRender(scope);
}

function _LoadRequiredDataBeforeRenderCompleted() {
  _protectRestoreController.LoadRequiredDataBeforeRenderCompleted();
}