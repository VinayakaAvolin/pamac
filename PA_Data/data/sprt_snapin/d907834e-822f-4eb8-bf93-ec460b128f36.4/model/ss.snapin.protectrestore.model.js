$ss.snapin = $ss.snapin || {}; 
$ss.snapin.protectrestore = $ss.snapin.protectrestore || {};

(function()
{

  $ss.snapin.protectrestore.model = Class.extend(
  {
    
  },
  {
    init: function()
    {
    },
    
    SortProtectionsByName:function (a,b) {
      var a2 = a.name.toString().toLowerCase().replace(/\W/g,'');
      var b2 = b.name.toString().toLowerCase().replace(/\W/g,'');
      return (a2<b2)?-1:(a2>b2)?1:0;
    }
    
  });
   
  $ss.snapin.protectrestore.model.Protection = $ss.snapin.protectrestore.model.extend(
  {
    _protections : [],
    _controller: null,
    _modelProtection: this

  },
  {
    GetProtections:function()
    {
      return this.Class._protections;
    },
    
    GetGuidFromIndex:function(index)
    {
      try
      {
        var item = this.Class._protections[index];
        return item.guid;
      }
      catch(ex)
      {
      }
      
      return "";
    },
    
  
LoadVersionListCompleted : function(result) {
  var oProbe = _GetProbeObject();
  var parsedJSONObject = JSON.parse(result);
  var objects = parsedJSONObject.Data;
  for(var i = 0 ; i < objects.length ; i++) {
      var object = objects[i];
      var guid = object.guid;
      oProbe.guids[guid].LoadVersionListCompleted(object.Data);
  }
  _protectRestoreController.LoadRequiredDataBeforeRenderCompleted();
},
    
ReloadVersionListCompleted(result) {
  var oProbe = _GetProbeObject();
  var parsedJSONObject = JSON.parse(result);
  var objects = parsedJSONObject.Data;
  for(var i = 0 ; i < objects.length ; i++) {
    var object = objects[i];
    var guid = object.guid;
    oProbe.guids[guid].LoadVersionListCompleted(object.Data);
  }
  this.Class._controller.ProtectionCompleted(indexOfProtectedItem);
},

    ProtectionCompleted: function(result) {
      _modelProtection = this;
    var parsedJSONObject = JSON.parse(result);
    var guid = parsedJSONObject.Data.guid;
    var index = 0;
    var version = 0;
    for(var i = 0 ; i < this.Class._protections.length ; i++) {
      var item = this.Class._protections[i];
      if (item.guid == guid) {
        index = i;
        version = item.version;
        break;
      }
    }
    indexOfProtectedItem = index;
     var objects = [];
      var oProbe       = _GetProbeObject();
      for (var i=0; i<oProbe.list.length; i++) {
          objects.push({"guid":oProbe.list[i].guid, "version": oProbe.list[i].version});
      }
      oProbe.LoadProtectedVersionList(objects, '_ReloadVersionListCompleted');
    // var lastProtectedDate = parsedJSONObject.Data.lastProtectedDate;
    // this.Class._controller.ProtectionCompleted(index, lastProtectedDate);
    },

    ProtectMac:function(index, controler) {
    this.Class._controller = controler;
    try
      {
        var item = this.Class._protections[index];
        var displayName = unescape(item.displayName);
        var oProbe = _GetProbeObject();

        // special case the protections for "pre-test tests"
        var _bDoProtection=true;
        switch (item.guid)
        {
          case _config.GetConfigValue("protections","browser_settings_protection_guid") :
          case _config.GetConfigValue("protections", "network_settings_protection_guid") :
            var bConnStatus = $ss.agentcore.network.inet.GetConnectionStatus();
            if(!bConnStatus){
              _bDoProtection=false;
              var url = _config.GetConfigValue("preconfigured_snapin_url","subagent_getconnect","/sa/connect/getconnect")
              if (NavigationController.IsUrlValid(url)) {
                if (confirm(_utils.LocalXLate("snapin_protectrestore", "needConnection", [displayName]))) {
                  var params = {};
                  params.requesturl = url;
                  MVC.Controller.dispatch("navigation", "index", params);
                  params = null;
                  return false;
                }
              } else {
                alert(_utils.LocalXLate("snapin_protectrestore", "needConnectionTxt", [displayName]));
              }
            }
          break;
        }
        if(_bDoProtection) {
          var opt = {
          sContGuid :  oProbe.guids[item.guid].guid,
          iVersion : oProbe.guids[item.guid].contentVersion,
          sTitle : oProbe.guids[item.guid].name,
          snapinName : "snapin_protectrestore"
        }
        $ss.agentcore.reporting.Reports.LogProtectionStart(opt);
        _ProtectMac(this, item);
      }
    }
      catch(ex)
      {

      }
    },

    Protect:function(index)
    {
      try
      {
          var item = this.Class._protections[index];
          var displayName = unescape(item.displayName);
          var oProbe = _GetProbeObject();
          
          // special case the protections for "pre-test tests"
          var _bDoProtection=true;
          switch (item.guid)
          {
            case _config.GetConfigValue("protections","browser_settings_protection_guid") :
            case _config.GetConfigValue("protections", "network_settings_protection_guid") :
              var bConnStatus = $ss.agentcore.network.inet.GetConnectionStatus();
              if(!bConnStatus){
                _bDoProtection=false;
                var url = _config.GetConfigValue("preconfigured_snapin_url","subagent_getconnect","/sa/connect/getconnect")
                if (NavigationController.IsUrlValid(url)) {
                  if (confirm(_utils.LocalXLate("snapin_protectrestore", "needConnection", [displayName]))) {
                    var params = {};
                    params.requesturl = url;
                    MVC.Controller.dispatch("navigation", "index", params);
                    params = null;
                    return false;
                  }
                } else {
                  alert(_utils.LocalXLate("snapin_protectrestore", "needConnectionTxt", [displayName]));
                }
              }
            break;
          }
          if (_bDoProtection) {
            var opt = {
              sContGuid :  oProbe.guids[item.guid].guid,
              iVersion : oProbe.guids[item.guid].contentVersion,
              sTitle : oProbe.guids[item.guid].name,
              snapinName : "snapin_protectrestore"
            }
            $ss.agentcore.reporting.Reports.LogProtectionStart(opt);
            var success = oProbe.guids[item.guid].Protect();
            if(success) {
              var arr = [];
              arr.push(item);
              oProbe.guids[item.guid].ReloadVersionList();
              this.UpdateItems([item]);
            }

            opt.iSuccess = success ? 1 : 0;
            $ss.agentcore.reporting.Reports.LogProtectionDone(opt);
            return success;
          }
        }
        catch(ex)
        {
        }
        return false;
    },

    CheckHealFromReboot:function() 
    {
      var sHealReboot = _databag.GetValue("db_heal");
      if(sHealReboot) {
        var aHealParms = sHealReboot.split("|");
        _databag.RemoveValue("db_heal");
        
        var opt = {
          sContGuid :  aHealParms[2],
          iVersion : aHealParms[3],
          iItemsHealed : aHealParms[1],
          sTitle : aHealParms[4],
          snapinName : "snapin_protectrestore",
          showSurvey: false
        }
        $ss.agentcore.reporting.Reports.LogHealDone(opt);
        
      }      
    },
    
    AfterRender:function()
    {
      this.CheckHealFromReboot();
    },

    UpdateItems:function(arrUpdateItems)
    {
      for(var n=0;n<arrUpdateItems.length;n++)
      {
        arrUpdateItems[n].lastProtected = this.GetLastProtected(arrUpdateItems[n].guid);
      }
    },
  
    GetLastProtected:function(guid)
    {
      var lastProtected = "";
      if(!guid)
        return lastProtected;
      
      try
      {
        var oProbe = _GetProbeObject();
        var item = oProbe.guids[guid];
        var bAppFound     = true;
        //  DNA run options
        var TG_RUN_NOTFOUND = 64;
        
        for (var i2=0; i2<item.versions.length; i2++)
        {
          nextComparisonDate = (new Date (item.versions[i2].localdate))

          if (item.versions[i2].options & TG_RUN_NOTFOUND)
            bAppFound = false;

          if (! lastProtected || lastProtected < nextComparisonDate) {
            lastProtected = nextComparisonDate;
          }
        }
        if (! item.versions.length) 
        {
          lastProtected = _utils.LocalXLate("snapin_protectrestore", "neverSaved");
        }
        else if (!bAppFound) {
          lastProtected = _utils.LocalXLate("snapin_protectrestore", "appNotFoundAsOf", [lastProtected.toLocaleString()]);
        }
        else {
          lastProtected = lastProtected.toLocaleString();
        }
      }
      catch(ex)
      {
      }
      
      return lastProtected;
    },

    LoadRequiredDataBeforeRender(scope) {
      _modelProtection = this;
      _protectRestoreController = scope
      var objects = [];
      var oProbe       = _GetProbeObject();
      for (var i=0; i<oProbe.list.length; i++) {
          objects.push({"guid":oProbe.list[i].guid, "version": oProbe.list[i].version});
      }
      oProbe.LoadProtectedVersionList(objects, '_LoadVersionListCompleted');
    },

    BeforeRender:function() 
    {
      try
      {
        var type = "sprt_protection";
        if(!bMac) {
        _CreateProbeObject();
        }
        var oProbe       = _GetProbeObject();
        delete this.Class._protections;
        this.Class._protections = [];
        
        var lastProtected = "";
        var sProtectList  = "";
        var rowName       = "";

        //  Protection healing options
        var TG_HEAL_BY_USER   = 1073741824;
        var TG_HEAL_AT_LAUNCH = 2147483648;

        

        oProbe.list.sort(this.SortProtectionsByName);

        for (var i=0; i<oProbe.list.length; i++)
        {
          var oContentObj = $ss.agentcore.dal.content.GetContentByCid([type], oProbe.list[i].guid);
          //Two properties will be exposed here - title and description
          //Sample:
          //oContentObj.title => To access title
          //oContentObj.description => To access description
         
          var displayName = oContentObj.title;

          var lastProtected = this.GetLastProtected(oProbe.list[i].guid);
          
          var item = {};
          item.displayName = displayName;
          item.lastProtected = lastProtected;
          item.guid = oProbe.list[i].guid;
          this.Class._protections.push(item);
          
        }
      }
      catch(ex)
      {
      }
    }
  });
  
  $ss.snapin.protectrestore.model.Restore = $ss.snapin.protectrestore.model.extend(
  {
    _restores : []
  },
  { 
    PreRestore:function(guid)
    {     
      switch (guid) 
      {
        case _config.GetConfigValue("protections", "wzcsvc_settings_protection_guid") : 
          _service.StartWin32Service(_utils.IsWinVST(true) ? "wlansvc" : "wzcsvc",false);   // stop service
          break;
        case _config.GetConfigValue("protections", "firefox_bookmarks_protection_guid") :
        case _config.GetConfigValue("protections", "firefox_settings_protection_guid") :
          alert(_utils.LocalXLate("snapin_protectrestore", "firefoxRunning"));
          break;
        default:
          break;
      }
    },

    PostRestore:function(guid)
    {
      switch (guid) 
      { 
        case _config.GetConfigValue("protections", "wzcsvc_settings_protection_guid") :
          _service.StartWin32Service(_utils.IsWinVST(true) ? "wlansvc" : "wzcsvc",true);   // start service
          _utils.ReleaseIpConfig();
          _utils.RenewIpConfig();
          break;
        default:
          break;
      }    
    },
    
  RestoreMac:function(controler, guid,version, dateProtected) {
    try {
          this.Class._controller = controler;
      var oProbe = _GetProbeObject();
      var displayName = oProbe.guids[guid].name;

      var finishstr = _utils.LocalXLate("snapin_protectrestore", "restoreSuccess",[displayName ]);
      var rebootstr = _utils.LocalXLate("snapin_protectrestore", "restoreReboot");
      var errorstr  = _utils.LocalXLate("snapin_protectrestore", "restoreError");
              
      var opt = {
                  sContGuid: oProbe.guids[guid].guid,
                  iVersion: oProbe.guids[guid].contentVersion,
                  sTitle: oProbe.guids[guid].name,
                  snapinName: "snapin_protectrestore",
                  //Survey Feature Starts
                  //showSurvey: true
                  showSurvey: false
                  //Survey Feature End
      }
      $ss.agentcore.reporting.Reports.LogHealStart(opt);
        this.PreRestore(oProbe.guids[guid].guid);
        _RestoreMac(this, guid, version, dateProtected);

    }
    catch(ex) {

    }

    },

  RestoreCompleted : function(result) {
    var parsedJSONObject = JSON.parse(result);
    var data = parsedJSONObject.Data;
    var guid = data.guid;
    var oProbe = _GetProbeObject();
    this.PostRestore(oProbe.guids[data.guid].guid);
    var opt = {
                sContGuid: oProbe.guids[guid].guid,
                iVersion: oProbe.guids[guid].contentVersion,
                sTitle: oProbe.guids[guid].name,
                snapinName: "snapin_protectrestore",
                //Survey Feature Starts
                //showSurvey: true
                showSurvey: false
                //Survey Feature End

              }
    var sGuid = oProbe.guids[guid].guid;
    var displayName = oProbe.guids[guid].name;

    var finishstr = _utils.LocalXLate("snapin_protectrestore", "restoreSuccess",[displayName ]);
     
    if ((sGuid === _config.GetConfigValue("protections", "email_settings_protection_guid_oe")) ||
        (sGuid === _config.GetConfigValue("protections", "outlook_settings_protection_guid")) ||
        (sGuid === _config.GetConfigValue("protections", "email_settings_protection_guid_wlm"))) {
      finishstr = _utils.LocalXLate("snapin_protectrestore", "restorReeopenWM", [oProbe.guids[guid].name]);
      alert(finishstr);
    }else {
      alert(finishstr);
    }
    
    opt.iItemsHealed = 1;
    $ss.agentcore.reporting.Reports.LogHealDone(opt);
    this.Class._controller.RestoreCompleted(opt);
  },

    Restore:function(guid,version) 
    {
      try
      {
        var oProbe = _GetProbeObject();
        var displayName = oProbe.guids[guid].name;

        var finishstr = _utils.LocalXLate("snapin_protectrestore", "restoreSuccess",[displayName ]);
        var rebootstr = _utils.LocalXLate("snapin_protectrestore", "restoreReboot");
        var errorstr  = _utils.LocalXLate("snapin_protectrestore", "restoreError");
              
        var opt = {
                    sContGuid: oProbe.guids[guid].guid,
                    iVersion: oProbe.guids[guid].contentVersion,
                    sTitle: oProbe.guids[guid].name,
                    snapinName: "snapin_protectrestore",
                    //Survey Feature Starts
                    //showSurvey: true
                    showSurvey: false
                    //Survey Feature End

        }
        $ss.agentcore.reporting.Reports.LogHealStart(opt);
        this.PreRestore(oProbe.guids[guid].guid);

        var healed = oProbe.guids[guid].Heal(version);
        var healedCount = oProbe.guids[guid].healedItems.length;
        
        this.PostRestore(oProbe.guids[guid].guid);
        //
        if (healed == "-1" && healedCount <= 0) {
          alert(errorstr);
        }
        else
        {
          if(healedCount > 0)
          {
            // Update undo log
            if (oProbe.objTxHeal.NeedReboot || oProbe.guids[guid].guid == _config.GetConfigValue("protections","network_settings_protection_guid"))
            {
              if(confirm(_utils.LocalXLate("snapin_protectrestore", "confirmRestartPC")))
              {
                var sDBString = healed + "|" +
                                healedCount + "|" +
                                oProbe.guids[guid].guid + "|" +
                                oProbe.guids[guid].contentVersion + "|" +
                                oProbe.guids[guid].name + "|" +
                                "sprt_protection";
                _databag.SetValue("db_heal", sDBString);
                var protectUrl = _config.GetConfigValue("preconfigured_snapin_url","subagent_protections","/sa/protect")                        
                $ss.agentcore.utils.RestartWithSave(protectUrl,true);
                return;
              }
            }
            else {
              var sGuid = oProbe.guids[guid].guid;
              if ((sGuid === _config.GetConfigValue("protections", "email_settings_protection_guid_oe")) ||
                  (sGuid === _config.GetConfigValue("protections", "outlook_settings_protection_guid")) ||
                  (sGuid === _config.GetConfigValue("protections", "email_settings_protection_guid_wlm"))) {
                finishstr = _utils.LocalXLate("snapin_protectrestore", "restorReeopenWM", [displayName]);
                alert(finishstr);
                //Killing is not happening consistently in Win7
                /*var sCmdPath = '"' + _config.GetProviderBinPath() + 'sdckillw.exe" ';
                sCmdPath = sCmdPath + " /name winmail.exe";
                _utils.RunCommand(sCmdPath, _constants.BCONT_RUNCMD_HIDDEN);  // run in hidden mode*/
              }
              else
                alert(finishstr);
            }
          }
          else
          {
            alert(_utils.LocalXLate("snapin_protectrestore", "restoreNoChange"));
          }
        }

        opt.iItemsHealed = healedCount;
        $ss.agentcore.reporting.Reports.LogHealDone(opt);
                //Survey Feature Starts
                return opt;
                //Survey Feature End
      }
      catch(ex)
      {
      }
    },

     LoadRequiredDataBeforeRender(scope) {
      _modelRestore = this;
      _protectRestoreController = scope
      var objects = [];
      var oProbe       = _GetProbeObject();
      for (var i=0; i<oProbe.list.length; i++) {
          objects.push({"guid":oProbe.list[i].guid, "version": oProbe.list[i].version});
      }
      oProbe.LoadProtectedVersionList(objects, '_LoadVersionListCompletedForRestore');
    },

LoadVersionListCompleted : function(result) {
  var oProbe = _GetProbeObject();
  var parsedJSONObject = JSON.parse(result);
  var objects = parsedJSONObject.Data;
  for(var i = 0 ; i < objects.length ; i++) {
      var object = objects[i];
      var guid = object.guid;
      oProbe.guids[guid].LoadVersionListCompleted(object.Data);
  }
  _protectRestoreController.LoadRequiredDataBeforeRenderCompleted();
},

    BeforeRender:function() 
    {
      try
      {
        var type = "sprt_protection";
        var nextDate      = "";
        var version       = "";
        var vNum          = _config.GetConfigValue('protections','protect_max_number_of_protections');
        
        //  DNA run options
        var TG_RUN_NOTFOUND = 64;
        
        //  Protection healing options
        var TG_HEAL_BY_USER   = 1073741824;
        var TG_HEAL_AT_LAUNCH = 2147483648;
        
        if (!bMac) {
        _CreateProbeObject();
      }
        var oProbe       = _GetProbeObject();
        oProbe.list.sort(this.SortProtectionsByName);

        delete this.Class._restores;
        this.Class._restores = [];

        for (var i=0; i<oProbe.list.length; i++) 
        {
          oProbe.list[i].versions.reverse(); // inverse date order 
          iVersions = oProbe.list[i].versions.length;
          var oContentObj = $ss.agentcore.dal.content.GetContentByCid([type], oProbe.list[i].guid);
          var displayName = oContentObj.title;
          
          var item = {};
          item.guid = oProbe.list[i].guid;
          if (oProbe.list[i].versions.length) 
          {
            item.displayName = displayName;
            var bDnaExists = false;
            for (var i2=0; i2<iVersions; i2++) { 
              if (!(oProbe.list[i].versions[i2].options & TG_RUN_NOTFOUND)) {
                bDnaExists = true;
              }
            }
            
            if (bDnaExists) {
              item.listVersions = this.GetVersions(oProbe.list[i]);
            } 
            else 
            {
              item.neverSaved = _utils.LocalXLate("snapin_protectrestore", "appNotFound");
            }
          } 
          else  
          {        
            item.displayName = displayName;
            item.neverSaved = _utils.LocalXLate("snapin_protectrestore", "neverSaved");
          }
          item.checked = 0;
          this.Class._restores.push(item);
        } 
      }
      catch(ex)
      {
      }
    },
      
    GetVersions:function(item)
    {
      var listVersions = [];
      try
      {
        var TG_RUN_NOTFOUND = 64;
        var oProbe = _GetProbeObject();
        
        for (var i2=0; i2<item.versions.length; i2++) 
        {
          if (!(item.versions[i2].options & TG_RUN_NOTFOUND)) {
            // get short date format of date+time
            nextDate = item.versions[i2].date;//oProbe.objSystem.GetFormatedDateTime(item.versions[i2].date, 1);
            var version = {};
            version.guid = item.guid;
            version.ver = item.versions[i2].version;
            version.date = nextDate;
            listVersions.push(version);
          }
        }
      }
      catch(ex)
      {
      }
      return listVersions;
    },
      
    GetRestores:function()
    {
      return this.Class._restores;
    }
    
  });
  
  $ss.snapin.protectrestore.model.Undo = $ss.snapin.protectrestore.model.extend(
  {
    _listUndo : {}
  },
  {
    GetUndoList:function()
    {
      return this.Class._listUndo;
    },
    
    PreUndo:function(guid)
    {
      guid = guid.toString();
     
      switch (guid) 
      {
        case _config.GetConfigValue("protections","wzcsvc_settings_protection_guid") :
          _service.StartWin32Service("wzcsvc",false);   // stop service
          break;
        case _config.GetConfigValue("protections", "firefox_bookmarks_protection_guid") :
          alert(_utils.LocalXLate("snapin_protectrestore", "firefoxRunning"));
          break;
        default:
          break;
      }
    },
    
    PostUndo:function(guid)
    {
      guid = guid.toString();
      switch (guid) 
      {
        case ("protections","wzcsvc_settings_protection_guid") :
          _service.StartWin32Service("wzcsvc",false);   // start service
          break;
        default:
          break;
      }    
    },

    UndoMac: function(controler, key) {
      this.Class._controller = controler;
      var oProbe = _GetProbeObject();
      var displayName = oProbe.undoKeys[key].name;
      var version = oProbe.GetProtection(oProbe.undoKeys[key].name).contentVersion;
      var opt = {
        sContGuid :  oProbe.GetProtection(oProbe.undoKeys[key].name).guid,
        iVersion : version,
        sTitle : oProbe.GetProtection(oProbe.undoKeys[key].name).name,
        snapinName : "snapin_protectrestore"
      }
      $ss.agentcore.reporting.Reports.LogUndoStart(opt);
      this.PreUndo(oProbe.GetProtection(oProbe.undoKeys[key].name).guid);

       // firefox bookmarks require a force in order to copy over existing bookmark file
        if ( (oProbe.GetProtection(oProbe.undoKeys[key].name).guid == _config.GetConfigValue("protections", "firefox_bookmarks_protection_guid")) ||
             (oProbe.GetProtection(oProbe.undoKeys[key].name).guid == _config.GetConfigValue("protections", "firefox_settings_protection_guid")) )
        {
          oProbe.undoKeys[key].force = true;
        }
      _UndoMac(this, key, version);
    },

    UndoMacCompleted : function(result) {
      var parsedJSONObject = JSON.parse(result);
      var key = parsedJSONObject.Data.guid;
      var oProbe = _GetProbeObject();
      this.PostUndo(oProbe.GetProtection(oProbe.undoKeys[key].name).guid);
 var displayName = oProbe.undoKeys[key].name;
      var finishstr = _utils.LocalXLate("snapin_protectrestore", "undoSuccess",[displayName]);
 
      /*if (oProbe.objTxUndo.NeedReboot || oProbe.GetProtection(oProbe.undoKeys[key].name).guid == _config.GetConfigValue("protections","network_settings_protection_guid"))
        {
          if (confirm(_utils.LocalXLate("snapin_protectrestore", "confirmRestartPC")))
          {
            var protectUrl = _config.GetConfigValue("preconfigured_snapin_url","subagent_protections","/sa/protect")                        
            $ss.agentcore.utils.RestartWithSave(protectUrl,false);
          }
        }
        else {*/
          var sGuid = oProbe.GetProtection(oProbe.undoKeys[key].name).guid;
          if ((sGuid === _config.GetConfigValue("protections", "email_settings_protection_guid_oe")) ||
              (sGuid === _config.GetConfigValue("protections", "outlook_settings_protection_guid")) ||
              (sGuid === _config.GetConfigValue("protections", "email_settings_protection_guid_wlm"))) {
            alert(_utils.LocalXLate("snapin_protectrestore", "ReopenWMUndo"));
          }
          else
          {
            alert(finishstr);
          }
         
        // alert(finishstr);
        var opt = {
          sContGuid :  oProbe.GetProtection(oProbe.undoKeys[key].name).guid,
          iVersion : oProbe.GetProtection(oProbe.undoKeys[key].name).contentVersion,
          sTitle : oProbe.GetProtection(oProbe.undoKeys[key].name).name,
          snapinName : "snapin_protectrestore"
        }
        opt.iSuccess = true;
        $ss.agentcore.reporting.Reports.LogUndoDone(opt);
        this.Class._controller.UndoRestoredCompleted(opt);
    },

    Undo:function(key) 
    {
      try
      { 
        var oProbe = _GetProbeObject();
        var displayName = oProbe.undoKeys[key].name;

        var finishstr = _utils.LocalXLate("snapin_protectrestore", "undoSuccess",[displayName]);
        var rebootstr = _utils.LocalXLate("snapin_protectrestore", "restoreReboot");

        var opt = {
          sContGuid :  oProbe.GetProtection(oProbe.undoKeys[key].name).guid,
          iVersion : oProbe.GetProtection(oProbe.undoKeys[key].name).contentVersion,
          sTitle : oProbe.GetProtection(oProbe.undoKeys[key].name).name,
          snapinName : "snapin_protectrestore"
        }
        $ss.agentcore.reporting.Reports.LogUndoStart(opt);
        var protectedItem = oProbe.GetProtection(oProbe.undoKeys[key].name);		
    		if(!protectedItem) {
          protectedItem = oProbe.GetProtectionByGuid(oProbe.undoKeys[key].appGuid);
        }
        
        if(!protectedItem) throw  {message:'Unable to find protection by name or appGuid'};
        var sGuid = protectedItem.guid;
          
        this.PreUndo(sGuid);
        
        // firefox bookmarks require a force in order to copy over existing bookmark file
        if ( (sGuid == _config.GetConfigValue("protections", "firefox_bookmarks_protection_guid")) ||
             (sGuid == _config.GetConfigValue("protections", "firefox_settings_protection_guid")) )
        {
          oProbe.undoKeys[key].force = true;
        }
		var iSuccess = oProbe.undoKeys[key].Undo();
        
		this.PostUndo(sGuid);
        if (oProbe.objTxUndo.NeedReboot || sGuid == _config.GetConfigValue("protections","network_settings_protection_guid"))
        {
          if (confirm(_utils.LocalXLate("snapin_protectrestore", "confirmRestartPC")))
          {
            var protectUrl = _config.GetConfigValue("preconfigured_snapin_url","subagent_protections","/sa/protect")                        
            $ss.agentcore.utils.RestartWithSave(protectUrl,false);
          }
        }
        else {
          if ((sGuid === _config.GetConfigValue("protections", "email_settings_protection_guid_oe")) ||
              (sGuid === _config.GetConfigValue("protections", "outlook_settings_protection_guid")) ||
              (sGuid === _config.GetConfigValue("protections", "email_settings_protection_guid_wlm"))) {
            alert(_utils.LocalXLate("snapin_protectrestore", "ReopenWMUndo"));
          }
          else
          {
            alert(finishstr);
          }
        }

        opt.iSuccess = iSuccess;
		$ss.agentcore.reporting.Reports.LogUndoDone(opt);
		//Survey Feature Starts
		return opt;
		//Survey Feature End
      }
      catch(ex)
      {
      }
	  return {iSuccess:false};
    },

    LoadRequiredDataBeforeRender(scope) {
      _modelUndo = this;
      _protectRestoreController = scope
      var objects = [];
      _CreateProbeObject();
      var oProbe       = _GetProbeObject();
      for (var i=0; i<oProbe.list.length; i++) {
          objects.push({"guid":oProbe.list[i].guid, "version": oProbe.list[i].version});
      }
      oProbe.LoadUndoVersionList(objects, '_LoadUndoVersionListCompleted');
    },
    
    LoadUndoVersionListCompleted : function(result) {
      var oProbe = _GetProbeObject();
      var parsedJSONObject = JSON.parse(result);
      var objects = parsedJSONObject.Data;
      oProbe.LoadUndoVersionListCompleted(objects);
      _protectRestoreController.LoadRequiredDataBeforeRenderCompleted();
    },

    BeforeRender:function()
    {
      try
      {
        var type = "sprt_protection";
        var rowName       = "";
        var nextDate      = "";        
        var sName         = "";
        var bExists       = false;
        
        var myFinalArray = []; 
        if(!bMac) {
        _CreateProbeObject();
        }
        var oProbe       = _GetProbeObject();
        delete this.Class._listUndo;
        this.Class._listUndo = {};
      
        oProbe.undoList.reverse();
        oProbe.undoList.sort(this.SortProtectionsByName);
        
        for (var i=0; i<oProbe.undoList.length; i++)
        {
          var myName = oProbe.undoList[i].name;
          // newer binaries insert guid into undo log.  Use appguid from undo log if it exists in the undo log.  
          // Otherwise, use old method of looking it up by protection display name
          var myGuid = (oProbe.undoList[i].appGuid.length > 0) ? oProbe.undoList[i].appGuid : oProbe.GetProtection(myName).guid;

          // HT: skip undo entries where we don't have a corresponding protection
          if (myGuid != "" && !oProbe.HaveProtectionByGuid(myGuid)) continue;

          // KL: disabling undo for WZCSVCSettingProtection...if settings have changed after you last restored, undo will corrupt the current settings
          if(myGuid != _config.GetConfigValue("protections","wzcsvc_settings_protection_guid"))
          {
            if (! myFinalArray[myGuid]) 
            { 
              myFinalArray[myGuid] = []; 
            }
            myFinalArray[myGuid].push([oProbe.undoList[i].undoDate,oProbe.undoList[i].key,myName]);
          }
        }
        for (var pGuid in myFinalArray)
        {
          if(pGuid == "include") continue;
          var oContentObj = $ss.agentcore.dal.content.GetContentByCid([type], pGuid);
          var displayName = oContentObj.title;
          
          var item = {};
          
          item.displayName = displayName;
  
          date = new Date(myFinalArray[pGuid][0][0]).toLocaleString();
          item.undoDate = date;

          key = myFinalArray[pGuid][0][1];
          item.key = key;
          item.checked = 0;
          this.Class._listUndo[pGuid] = item;
        }
      }
      catch(ex)
      {
      }        
    },
    
    UndoAll:function()
    {
      for(var i=0;i<this.Class._undoList.length;i++)
      {
        if(this.Class._undoList[i].checked)
        {
          this.Undo(this.Class._undoList[i].key);
        }
      }
    }
    
    
  });
  
  var _config = $ss.agentcore.dal.config;
  var _databag = $ss.agentcore.dal.databag;
  var _service = $ss.agentcore.dal.service;
  var _utils = $ss.agentcore.utils;
  var _inet = $ss.agentcore.network.inet;
  var _constants = $ss.agentcore.constants;
  
  var _oProbe = null;
  
function _GetProbeObject() {
  if (!_oProbe) {
    var sID = "Prefix_Protecting";
    var sPrefix = _utils.LocalXLate("snapin_protectrestore", sID);
    if (sPrefix === sID) sPrefix = "Protecting ";

    try {
    _oProbe = new $ss.agentcore.dna.protectrestore.Probe(sPrefix);
    }catch(ex) {
      var _logger = $ss.agentcore.log.GetDefaultLogger("$ss.agentcore.dal.xml");  
    }
    }
    return _oProbe;
  }
  
  function _CreateProbeObject()
  {
    _oProbe = null;
    _GetProbeObject();      
  }
  
  function _ProtectMac(scope, item) {
    var oProbe = _GetProbeObject();
    _modelProtection = scope;
    oProbe.guids[item.guid].Protect('_ProtectionCompleted');

  }

  function _ReloadVersionList(scope, guid) {
        _modelProtection = scope;
    var oProbe = _GetProbeObject();
    oProbe.guids[guid].ReloadVersionList('_ReloadVersionListCompleted');
  }
  

  function _RestoreMac(scope, guid, version, dateProtected) {
    var oProbe = _GetProbeObject();
    _modelRestore = scope;
    oProbe.guids[guid].Heal(version, dateProtected, '_RestoreCompleted');
  }

  function _UndoMac(scope, guid, version) {
    var oProbe = _GetProbeObject();
    _modelUndo = scope;
    oProbe.undoKeys[guid].Undo(guid, version, '_UndoRestoreCompleted');
  }

  
})();
  var _modelProtection = null;
  var _modelRestore = null;
  var _modelUndo = null;
  var _protectRestoreController = null;
  var bMac = true;
  var indexOfProtectedItem = 0;
function _ProtectionCompleted(result) {
    _modelProtection.ProtectionCompleted(result);
  }

  function _ReloadVersionListCompleted(result) {
    _modelProtection.ReloadVersionListCompleted(result);
  }

  function _LoadVersionListCompleted(result) {
    _modelProtection.LoadVersionListCompleted(result);
  }

  function _LoadVersionListCompletedForRestore(result) {
    _modelRestore.LoadVersionListCompleted(result);
  }

  function _RestoreCompleted(result) {
    _modelRestore.RestoreCompleted(result);
  }

  function _LoadUndoVersionListCompleted(result) {
    _modelUndo.LoadUndoVersionListCompleted(result);
  }

  function _UndoRestoreCompleted(result) {
    _modelUndo.UndoMacCompleted(result);
  }