// always define this names spaces.
$ss.snapin = $ss.snapin ||
{};
$ss.snapin.langselect = $ss.snapin.langselect ||
{};
$ss.snapin.langselect.navhandler = $ss.snapin.langselect.navhandler ||
{};
$ss.snapin.langselect.renderhelper = $ss.snapin.langselect.renderhelper ||
{};
$ss.snapin.langselect.dec = $ss.snapin.langselect.dec ||
{};

//decision functions
(function(){
  $.extend($ss.snapin.langselect.dec, {
    LangCodeToLangLong: function(Lang){
      var translate_code = "cl_ls_lang_code_" + Lang;
      return $ss.agentcore.utils.LocalXLate('snapin_langselect',translate_code);
    },
    GetLangDescription: function(Lang){
      var translate_code = "cl_ls_lang_code_" + Lang+"_dsc";
      return $ss.agentcore.utils.LocalXLate('snapin_langselect',translate_code);
    }
  })
})();

//start of render helper function
(function() {
  $.extend($ss.snapin.langselect.renderhelper, {
    sta_langselect: {
      before: function(params, obj) {
        var $prevbutton = $(this, "input[type='previous']");
        $(prevbutton).removeAttr("disabled");
      },
      after: function(params, obj) {
        $("input.nextbutton:last").focus();
        var sLangHTML;

        //var g_LangVal = $ss.agentcore.dal.config.GetConfigValue("language", "default", "en");
        var g_LangVal = $ss.agentcore.utils.GetLanguage();
        var g_LangArr = $ss.agentcore.dal.config.GetValues("language", "supported", "en");

        var sLangDetails = "<p>";
        for (var u = 0; u < g_LangArr.length; u++) {
          sLangDetails += '<p>' + $ss.snapin.langselect.dec.GetLangDescription(g_LangArr[u]) + '</p>';
        }
        $('#LangDetails').html(sLangDetails);

        var sLangHTML = '<select id="ss_ls_LangSel" class="ss_ls_select" style="width: 150px;">';
        for (var i = 0; i < g_LangArr.length; i++) {
          if (g_LangArr[i] == $ss.agentcore.dal.config.GetConfigLanguage()) {
            sLangHTML += '<option id="ss_ls_opt_' + g_LangArr[i] + '" value="' + g_LangArr[i] + '" selected>' + $ss.snapin.langselect.dec.LangCodeToLangLong(g_LangArr[i]) + '</option>';
          } else {
            sLangHTML += '<option id="ss_ls_opt_' + g_LangArr[i] + '" value="' + g_LangArr[i] + '">' + $ss.snapin.langselect.dec.LangCodeToLangLong(g_LangArr[i]) + '</option>';
          }
        }
        sLangHTML += '</select>';
        $('#ls_langselect_text').html(sLangHTML);
      }
    }

  })
})();