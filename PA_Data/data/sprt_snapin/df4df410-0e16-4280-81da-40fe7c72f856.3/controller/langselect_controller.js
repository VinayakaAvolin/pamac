// always define this names spaces.
$ss.snapin = $ss.snapin ||
{};
$ss.snapin.langselect = $ss.snapin.langselect ||
{};
$ss.snapin.langselect.navhandler = $ss.snapin.langselect.navhandler ||
{};
$ss.snapin.langselect.renderhelper = $ss.snapin.langselect.renderhelper ||
{};
$ss.snapin.langselect.dec = $ss.snapin.langselect.dec ||
{};
//end of the name spaces ... 

SnapinLangselectController = SteplistBaseController.extend("snapin_langselect", {
  InitParam: {
    aNavigationItems: ["#snapin_langselect #nextbutton","#snapin_langselect #prevbutton" ],
    aContentPlaceHolderDIV: "snapin_langselect_content",
    sDefaultContentPage: "views/ls_main.htm"
  }
}, {
  index: function(params){
    this.renderSnapinView("snapin_langselect", params.toLocation, "\\views\\ls_layout.htm");
    this._super(params,"snapin_langselect");
    var flowInfo = this.Class.InitiateStep(params);
    if(flowInfo) {
      this.ProcessRender(params,flowInfo);
    }
  },
  ProcessNext: function(params){
    var flowInfo = this._super(this,params);
    if(flowInfo) {
      this.ProcessRender(params,flowInfo);
    }
  },

  ProcessPrevious: function(params){
    var flowInfo = this._super(this,params);
    if(flowInfo) {
      this.ProcessRender(params,flowInfo);
    }
    
  },
 
  ProcessRender: function(params,flowInfo) {
       var renderHelper = $ss.snapin.langselect.renderhelper || {};
       var stepId = flowInfo.stepid;
       var retData = {};
       var opt = {};
       if (stepId) {
          try {
            if(renderHelper[stepId] && renderHelper[stepId].before) {
              retData = renderHelper[stepId].before(params,this);  
            }
            opt.oLocal = retData;
          } catch(ex) {
            //failed to execute the renderhelper function
          }
      
        }
      this.ProcessFlow(this,params,flowInfo,opt);
      // start the post processing
      if (stepId) {
          try {
            var afterStatus = {};
            if(renderHelper[stepId] && renderHelper[stepId].after) {
              afterStatus = renderHelper[stepId].after(params,this);  
            }
            switch (afterStatus.move) {
              case "forward":
                return this.ProcessNext(params);
                break;
              case "back":
                return this.ProcessPrevious(params);
                break;
              default:
                break;
            }
          } catch(ex) {
              //failed to execute the renderhelper after function
          }
        }  
  },
  "#nextbutton click": function(params){
    //save the data
    var strVal = $("#ss_ls_LangSel").val();
    if (strVal) {
      var lastLang = $ss.agentcore.utils.GetLanguage();
      $ss.agentcore.dal.config.SetUserValue("global", "language", strVal);      
      if (strVal && lastLang !== strVal) {
        //reload everything .... 
        window.external.ResetUrl();
        return;
        //Write to registry for Reporting
        //ToDo:  Enable smartissue persist
        //$ss.agentcore.diagnostics.smartissue.PersistInfo(SI_GEN_CLASS, SI_INSTALL_LANG, strVal);
        //Write new language preference in the registry for localization

      }
    }
    this.ProcessNext(params);
  },
  "#prevbutton click": function(params){
    this.ProcessPrevious(params);
  }

});
