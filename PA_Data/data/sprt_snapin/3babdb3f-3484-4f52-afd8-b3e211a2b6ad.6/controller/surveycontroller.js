SnapinSurveyController = SnapinBaseController.extend('snapin_survey', {
}, {
    index: function (params) {
        var surveyFlag = $ss.agentcore.dal.config.GetConfigValue("global", "show_survey", "true");
        if ("true" !== surveyFlag) {
			return;
		}

        var surveyData = params.surveyData || null;
        if (!surveyData) {
			return;
		}

        //Survey Feature Starts
        var html = this.renderSnapinView("snapin_survey", params.toLocation, "\\views\\survey.html");
        //Survey Feature End

        var viewerhtml = $ss.GetSnapinAbsolutePath("snapin_survey") + "\\views\\survey.html";
        //Survey Feature Starts
        var dlgHeight = $ss.agentcore.dal.config.GetValues("snapin_survey", "snp_survey_dialogHeight", "330px")[0];
        var dlgWidth = $ss.agentcore.dal.config.GetValues("snapin_survey", "snp_survey_dialogWidth", "360px")[0];

        //Survey Feature End

    var dialogParams = 'resizable:no' + ';' +
    'status:no' +
    ';' +
    'help:no' +
    ';' +
    'dialogHeight:' + dlgHeight +
    ';' +
    'dialogWidth:' + dlgWidth +
        ';' +
    'scroll:no' +
    ';';

		data.shell = window;

		data.surveyData = surveyData;
        if (bMac)
        {
            var window_title = $ss.agentcore.utils.LocalXLate("snapin_survey", "SURVEY_TITLE");
            var regPath = "Software/SupportSoft/ProviderList/global";
            $ss.agentcore.dal.registry.SetRegValue("HKCU", regPath, "ModalDialogWidth", dlgWidth);
            $ss.agentcore.dal.registry.SetRegValue("HKCU", regPath, "ModalDialogHeight", dlgHeight);
            $ss.agentcore.dal.registry.SetRegValue("HKCU", regPath, "WindowTitle", window_title);
            window.open(viewerhtml);
        }else {
        window.showModalDialog(viewerhtml, data, dialogParams);
    }
    }
});
var data = {};
var bMac = $ss.agentcore.utils.IsRunningInMacMachine();
var jsBridge = window.JSBridge;

