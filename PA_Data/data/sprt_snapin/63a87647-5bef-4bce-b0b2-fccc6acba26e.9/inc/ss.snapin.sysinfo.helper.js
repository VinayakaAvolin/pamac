/** @namespace Provides System Information functionality */

$ss.snapin = $ss.snapin || {};
$ss.snapin.sysinfo = $ss.snapin.sysinfo || {};
$ss.snapin.sysinfo.helper = $ss.snapin.sysinfo.helper ||{};

(function(){

  // Private Variables
  var _allSysInfo;
  var _oCategories;
  var _sloadedTemplates;
  var _sSelectedCat;
  var _oInitialParams;
  var _sXslFile;
  var bMac = $ss.agentcore.utils.IsRunningInMacMachine();
  var _sIssuePath;
  var _issueID;
  var _jsBridge = window.JSBridge;
  $.extend($ss.snapin.sysinfo.helper, {
    /**
     *  @name Initialize
     *  @memberOf $ss.snapin.sysinfo.helper
     *  @function
     *  @description Initialize the variables
     *  @param none
     *  @returns none
     */
    Initialize: function(sParams){
      _allSysInfo = null;
      _oCategories = new Array();
      _sloadedTemplates = new Array();
      
      if (!_sSelectedCat) _sSelectedCat = sParams["snpsysinfo_selectCat"];
      var sXslFilename;
      var isModemFlowRequired = (($ss.agentcore.dal.config.GetConfigValue("modems", "modem_flow_required", "true")).toLowerCase() == "true");
      if(isModemFlowRequired)
      {
        if(bMac)
          {
            sXslFilename = "default_mac.xsl";
          }
        else
        {
        sXslFilename = "default.xsl";
      }
      }
      else
      {
        sXslFilename = "default_NoModem.xsl";
      } 
      if (sParams["snpsysinfo_xslfile"]) { sXslFilename = sParams["snpsysinfo_xslfile"] + ".xsl"; }
      
      _sXslFile = $ss.getSnapinAbsolutePath("snapin_system_information") + 
                        "\\views\\" + $ss.agentcore.utils.GetLanguage() + "\\" + sXslFilename;
      this.LoadCategories();
    },
    /**
     *  @name LoadCategories
     *  @memberOf $ss.snapin.sysinfo.helper
     *  @function
     *  @description Loads applicable categories from xsl file and 
     *               populates them to global variable called oCategories.
     *  @param none
     *  @returns object categories
     */
    LoadCategories: function(){
      var $xml = $ss.agentcore.dal.xml;

      var sSmartIssueXsl = $xml.LoadXML(_sXslFile, false, null);
      var xpathNode = "/xsl:stylesheet/xsl:template/div/div";
      var ret = $xml.GetNodes(xpathNode, "", sSmartIssueXsl);
      for (var i = 0; i < ret.length; i++) {
        var cat = {
          id: $xml.GetAttribute(ret[i], "id"),
          name: $xml.GetAttribute(ret[i], "name"),
          filename: $xml.GetAttribute(ret[i], "filename"),
          hidden: $xml.GetAttribute(ret[i], "hidden"),
          selected: $xml.GetAttribute(ret[i], "selected"),
          index: i
        };
        
        // Select first non-hidden as default
        if (_sSelectedCat == undefined && cat.hidden != "true") {
          _sSelectedCat = cat.id;
        // if there is one with selected tag equals true, then this is it
        } else if (cat.selected == "true" && _oCategories[_sSelectedCat].selected != "true") {
          _sSelectedCat = cat.id;
        }
        
        _oCategories[cat.id] = cat;
      }
      return _oCategories; //added to return the categories.
    },
    /**
     *  @name GetSelectedCat
     *  @memberOf $ss.snapin.sysinfo.helper
     *  @function
     *  @description returns the selected category
     *  @param none
     *  @returns string selected category
     */
    GetSelectedCat: function(){
      return _sSelectedCat;
    },
    
    /**
     *  @name SetSelectedCat
     *  @memberOf $ss.snapin.sysinfo.helper
     *  @function
     *  @description set the selected category
     *  @param sCategoryID ID name of the category
     *  @returns none
     */
    SetSelectedCat: function(sCategoryID){
      _sSelectedCat = sCategoryID;
    },
    
    /**
     *  @name GetCategoryList
     *  @memberOf $ss.snapin.sysinfo.helper
     *  @function
     *  @description Gets the list of category objects in an array
     *  @param none
     *  @returns the list of category objects in an array
     */    
    GetCategoryList: function() {
      return _oCategories;
    },

    /**
     *  @name GetCategoryName
     *  @memberOf $ss.snapin.sysinfo.helper
     *  @function
     *  @description Returns the category display name for a given category ID
     *  @param sCategoryID ID name of the category
     *  @returns Display name of category
     */    
    GetCategoryName: function(sCategoryID) {
      return _oCategories[sCategoryID].name;
    },
    
    /**
     *  @name LoadDefaultSysInfo
     *  @memberOf $ss.snapin.sysinfo.helper
     *  @function
     *  @description Loads the default template (default.xml) and transforms against the xsl file.  
     *               Store output in global variable called allSysInfo.
     *  @param none
     *  @returns none
     */
    LoadDefaultSysInfo: function(scope){
      _sysInfoController = scope;
      this.showWaitImage();
      if(bMac)
      {
        this.createIssueAndXslTransform("default_mac.xml", function(result) {
          _allSysInfo = result;
        }); //added this to scope it
      }
      else
      {
      _allSysInfo = this.createIssueAndXslTransform("default.xml"); //added this to scope it
      return _allSysInfo; //added to return _allSystInfo
      }
    },
    
    
    /**
     *  @name CheckAndLoadAdditionalTemplates
     *  @memberOf $ss.snapin.sysinfo.helper
     *  @function
     *  @description When a category is selected, check whether there is a template associated with it.  If so
     *               load the template, transform against the xsl file, and 
     *               replace its content in allSysInfo global variable
     *  @param catID The alphanumeric Category ID as defined in the xsl
     *  @returns none
     */
    CheckAndLoadAdditionalTemplates: function(catID){
      var objFile = $ss.agentcore.dal.file;
      var $xml = $ss.agentcore.dal.xml;
      
      // Check if file exists for this category
      var absPath = $ss.getSnapinAbsolutePath("snapin_system_information");
      if (objFile.FileExists(absPath + "\\smartissue\\" + catID + ".xml")) {
        template = catID + ".xml";
      }
      else {
        return;
      }
      
      // Check that it is not already loaded
      if (!_sloadedTemplates[template]) {
        this.showWaitImage();
        
        var xpathNode = "/div/div[@id='" + catID + "']";
        
        // New XML Output: Grab only the Node needed
        var sourceNode = $xml.GetNode(xpathNode, "", this.createIssueAndXslTransform(template));
        
        // Grab Old Node to be replaced
        var destNode = $xml.GetNode(xpathNode, "", _allSysInfo);
        
        // Replace
        _allSysInfo.documentElement.replaceChild(sourceNode, destNode);
        
        _sloadedTemplates[template] = template;
      }
      
    },
    
    /**
     *  @name getSysInfoByCat
     *  @memberOf $ss.snapin.sysinfo.helper
     *  @function
     *  @description Returns the html output associated with the provided catID
     *  @param catID The alphanumeric Category ID as defined in the xsl
     *  @returns string containing the html
     */
    getSysInfoByCat: function(catID){
      if (bMac) {
        var elem = _allSysInfo.getElementById(catID);
        var serializer = new XMLSerializer();
        var contents = serializer.serializeToString(elem);
        return contents;
      } else {
      var $xml = $ss.agentcore.dal.xml;
      var xpathNode = "/div/div[@id='" + catID + "']";
      var ret = $xml.GetNode(xpathNode, "", _allSysInfo);
      return ret.xml;
      }
    },
    
    /**
     *  @name PrintWindow
     *  @memberOf $ss.snapin.sysinfo.helper
     *  @function
     *  @description Brings up Windows Printing popup.  Prints passed in string
     *  @param sStyle Style to apply for the item to Print
     *         sCurrentTabHTML Text to print
     *         oTempPrintFrame A Frame object to which to print everything
     *  @returns none
     */
    PrintWindow: function(sStyle, sCurrentTabHTML, oTempPrintFrame){
      oTempPrintFrame.document.open();
      oTempPrintFrame.document.write("<html><body>" + sStyle + sCurrentTabHTML + "</body></html>");
      oTempPrintFrame.document.close();
      oTempPrintFrame.focus();
      oTempPrintFrame.print();
    },
    
    /**
     *  @name SaveToDesktop
     *  @memberOf $ss.snapin.sysinfo.helper
     *  @function
     *  @description Saves passed in content along with style to an html file.  
     *               File is saved to desktop with the filename provided. 
     *               Popup is displayed whether save is successful
     *  @param sStyle Style to include in saved html
     *  @param sCurrentTabHTML Content to be saved
     *  @param sFilename Name of file to save as
     *  @returns none
     */
    SaveToDesktop: function(sStyle, sCurrentTabHTML){
      var objConfig = $ss.agentcore.dal.config;
      var objFile = $ss.agentcore.dal.file;
      var catfilenames = _oCategories[_sSelectedCat];
      sFilename = (catfilenames.filename === null ? catfilenames.name : catfilenames.filename);

      sCurrentTabHTML = sCurrentTabHTML.replace(/\<script /gi, "<!--script ").replace(/\/script>/gi, "\/script-->");
      
      var dtSavedDate = new Date().toLocaleString();
      sCurrentTabHTML = sStyle + 
          $ss.agentcore.utils.LocalXLate("snapin_system_information","snp_sysinfo_save_header",[dtSavedDate]) +
          sCurrentTabHTML;
      
      if (bMac) {
          var sDesktopPath = objConfig.ExpandSysMacro("%SF_DESKTOP%") + "/" + sFilename + ".html";
      } else {
      var sDesktopPath = objConfig.ExpandSysMacro("%SF_DESKTOP%") + sFilename + ".html";
      }
      
      if (objFile.WriteNewFile(sDesktopPath, sCurrentTabHTML)) {
        return($ss.agentcore.utils.LocalXLate("snapin_system_information","snp_sysinfo_save_success",[sFilename]));
      }
      else {
        return($ss.agentcore.utils.LocalXLate("snapin_system_information","snp_sysinfo_save_fail"));
      }
    },
    
    
    /**
     *  @name showWaitImage
     *  @memberOf $ss.snapin.sysinfo.helper 
     *  @function
     *  @description Displays a "Wait" animation in scrollarea
     *  @param none
     *  @returns none
     */
    showWaitImage: function(){
      $("#scrollarea")[0].innerHTML = "<center><br><br>" + 
          $ss.agentcore.utils.LocalXLate("snapin_system_information","snp_sysinfo_gather_info") +
          "<br><br><img src='" + $ss.GetLayoutSkinPath() + "\\icons\\loader.gif'></center>";      
      $ss.agentcore.utils.Sleep(10);
    },
    
    /**
     *  @name createIssueAndXslTransform
     *  @memberOf $ss.snapin.sysinfo.helper
     *  @function
     *  @description Create issue xml for the provided template.  
     *               Transforms it against sysinfo.xsl and returns the output.
     *  @param template Filename of template.  Must reside in smartissue folder
     *  @returns String containing the transformed output
     */
    createIssueAndXslTransform: function(template, callback){
    
      var $xml = $ss.agentcore.dal.xml;
      var objSI = $ss.agentcore.diagnostics.smartissue;
      var objConfig = $ss.agentcore.dal.config;
      var absPath = $ss.getSnapinAbsolutePath("snapin_system_information");
      
      // Create Issue
      var issueID;
      if (bMac) {
          var message = {'JSClassNameKey':'SystemUtility','JSOperationNameKey':'GenGuid', 'JSArgumentsKey':[false], 'JSISSyncMethodKey' : '1'}
          var result = _jsBridge.execute(message);
          var parsedJSONObject = JSON.parse(result);
          _issueID = parsedJSONObject.Data;
          var inputPath = $ss.getSnapinAbsolutePath("snapin_system_information") + "/smartissue/" + template;
          var outputPath = $ss.getAttribute("startingpoint").replace("/data", "/state/issues") + _issueID + ".xml";

          _sIssuePath = outputPath;
          NativeCall(this, inputPath, outputPath, callback);
      } else {
         issueID = objSI.CreateIssue("SystemInfo", true, absPath + "\\smartissue\\" + template, false, false, null);
      
      if (template == "default.xml") {
          objSI.SetIssueData(issueID, 'ProductInfo', 'ProductInfo', 'ProductName', 'string',
                //GS Customization to get the brandname from the config
                //objConfig.GetConfigValue("global", "product", "Dynamic Agent"));
                objConfig.GetConfigValue("global", "brandname", $ss.agentcore.dal.config.ParseMacros("%BRANDNAME%")));
          return XslTransform()
      }
      }
    },
      

    XslTransform: function() {
      // Load XSL and SI output XML
      var $xml = $ss.agentcore.dal.xml;
      var objSI = $ss.agentcore.diagnostics.smartissue;

      var sSmartIssueXml = $xml.LoadXML(_sIssuePath, false, null);
      var sSmartIssueXsl = $xml.LoadXML(_sXslFile, false, null);
      
      var  xsltProcessor = new XSLTProcessor();
      xsltProcessor.importStylesheet(sSmartIssueXsl.responseXML);
      var resultDocument = xsltProcessor.transformToFragment(sSmartIssueXml.responseXML, document)
      // Transform and return XML DOM object
      //var returnValue = $xml.LoadXMLFromString(resultDocument/*sSmartIssueXml.transformNode(sSmartIssueXsl)*/, true, null);
      // Clean up
      objSI.DeleteIssue(_issueID);
      return resultDocument;
    },
    
    /**
     *  @name getStyleForMedia
     *  @memberOf $ss.snapin.sysinfo.helper
     *  @function
     *  @description Returns a string containing style to be used for media output (i.e. Print and File).
     *  @param none
     *  @returns String containing styles
     */
    getStyleForMedia: function(){
    
      var objConfig = $ss.agentcore.dal.config;
      var str = $ss.getSnapinAbsolutePath("snapin_system_information") + "/" + 
                    objConfig.GetConfigValue("snapin_system_information","media_styles","");

      var ret = $ss.agentcore.dal.file.ReadFile(str);
      return("<style>" + ret + "</style>");
		       
    }    
    
  });
})();

 var _scope;
 var _sysInfoController;
 var _resultCallback;

 function NativeCall(scope, inputPath, outputPath, sysInfoResultCallback) {
    _scope = scope;
    _resultCallback = sysInfoResultCallback;
    message = {'JSClassNameKey':'EndpointInfo','JSOperationNameKey':'CollectEndpointInfo', 'JSArgumentsKey':[inputPath, outputPath], 'JSCallbackMethodNameKey':"NativeCallback", 'JSISSyncMethodKey' : '0'}
    _jsBridge.execute(message);
    console.log("called");

  }

function NativeCallback() {
      var result = _scope.XslTransform();
      _resultCallback(result);
      _sysInfoController.Load();
  }
