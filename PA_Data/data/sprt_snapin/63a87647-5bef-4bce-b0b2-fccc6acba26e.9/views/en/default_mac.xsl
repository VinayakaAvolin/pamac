<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<div>
			<div id="systemsummary" name="System Summary" class="alternatelisttable">
				<table border="0" margin="0" width="100%">
					<tr>
						<td class="table-heading">My Computer and Incident Summary</td>
						<td class="table-heading"></td>
					</tr>
					<tr>
						<td class="even-row">Default Browser</td>
						<td class="even-row">
							<xsl:value-of select="//INSTANCE[@CLASSNAME='DefaultBrowser']/PROPERTY[@NAME='DefaultBrowserApp']/VALUE"></xsl:value-of>
						</td>
					</tr>
					<tr>
						<td class="even-row">Default Email Client</td>
						<td class="even-row">
							<xsl:value-of select="//INSTANCE[@CLASSNAME='DefaultEmailClient']/PROPERTY[@NAME='DefaultEmailClient']/VALUE"></xsl:value-of>
						</td>
					</tr>
				</table>
			</div>

			<div id="osinfo" name="Mac OS X">
				<table border="0" margin="0" width="100%">

					<tr>
						<td class="table-heading">My Operating System</td>
						<td class="table-heading"></td>
					</tr>

					<tr>
						<td class="even-row">OS Name</td>
						<td class="even-row">
							<xsl:value-of select="//INSTANCE[@CLASSNAME='OSSummary']/PROPERTY[@NAME='CodeName']/VALUE" ></xsl:value-of>
						</td>
					</tr>

					<tr>
						<td class="even-row">OS Version</td>
						<td class="even-row">
							<xsl:value-of select="//INSTANCE[@CLASSNAME='OSSummary']/PROPERTY[@NAME='version']/VALUE"></xsl:value-of>
						</td>
					</tr>

					<tr>
						<td class="even-row">Locale</td>
						<td class="even-row">
							<xsl:value-of select="//INSTANCE[@CLASSNAME='SystemInfo']/PROPERTY[@NAME='SystemLanguage']/VALUE"></xsl:value-of>
						</td>
					</tr>

				</table>	
			</div>
			
			<div id="myhardware" name="My Hardware" class="alternatelisttable">
				<table border="0" margin="0" width="100%">


					<tr>
						<td class="even-row">UUID</td>
						<td class="even-row">
							<xsl:value-of select="//INSTANCE[@CLASSNAME='HardwareInfo']/PROPERTY[@NAME='platform_UUID']/VALUE"></xsl:value-of>
						</td>
					</tr>

					<tr>
						<td class="even-row">Total Physical Memory</td>
						<td class="even-row">
							<xsl:value-of select="//INSTANCE[@CLASSNAME='HardwareInfo']/PROPERTY[@NAME='physical_memory']/VALUE"></xsl:value-of>
						</td>
					</tr>

					<tr>
						<td class="even-row">Machine Name</td>
						<td class="even-row">
							<xsl:value-of select="//INSTANCE[@CLASSNAME='HardwareInfo']/PROPERTY[@NAME='machine_name']/VALUE"></xsl:value-of>
						</td>
					</tr>

				</table>
			</div>
			 
			<div id="network" name="Network Information" class="alternatelisttable">
				<table border="0" margin="0" width="100%">
					<tr>
						<td class="table-heading">My Network</td>
						<td class="table-heading"></td>
					</tr>
					<tr>
						<td class="odd-row" width="40%">Computer Name</td>
						<td class="odd-row">
							<xsl:value-of select="//INSTANCE[@CLASSNAME='SystemInfo']/PROPERTY[@NAME='ComputerName']/VALUE" />
						</td>
					</tr>

					<tr>
						<td class="odd-row" width="40%">User Name</td>
						<td class="odd-row">
							<xsl:value-of select="//INSTANCE[@CLASSNAME='UserInfo']/PROPERTY[@NAME='UserName']/VALUE" />
						</td>
					</tr>
					<tr>
						<td class="odd-row" width="40%">TCP/IP Address</td>
						<td class="odd-row">
							<xsl:value-of select="//INSTANCE[@CLASSNAME='NetworkInfo']/PROPERTY[@NAME='ip_address']/VALUE" />
						</td>
					</tr>
				</table>
			</div>
			
			<div id="environmentvariables" name="Environment Variables">
				<table border="0" margin="0" width="100%" class="alternatelisttable">
					<tr>
      					<td class="table-heading">Environment Variable</td>
      					<td class="table-heading">Value</td>
     				</tr>
     				    <xsl:for-each select="//INSTANCE[@CLASSNAME='EnvironmentVariables']">
							<tr>
                            <td>
                                
                                <xsl:value-of select="./PROPERTY[@NAME='EnvKey']/VALUE" />
                            </td>
                            <td>
                                
                                <xsl:value-of select="./PROPERTY[@NAME='EnvVal']/VALUE" />
                            </td>
                        </tr>
     					</xsl:for-each>

     			</table>
			</div>
			
			<div id="printerlist" name="Printer List">
			    <table border="0" margin="0" width="100%" class="alternatelisttable">
				<tr>
					<td class="table-heading">Name</td>
			      	<td class="table-heading">Driver</td>
			    </tr>
     
     			<xsl:if test="count(//INSTANCE[@CLASSNAME='Printers']) &lt; 1">
       			<tr><td colspan="3"><br/><center>No Printer Found</center></td></tr>
     			</xsl:if>
     			<xsl:for-each select="//INSTANCE[@CLASSNAME='Printers']">
                        <tr>
                            <td>
                                
                                <xsl:value-of select="./PROPERTY[@NAME='_name']/VALUE" />
                            </td>
                            <td>
                                
                                <xsl:value-of select="./PROPERTY[@NAME='driver']/VALUE" />
                            </td>
                        </tr>
                    </xsl:for-each>
				</table>
			</div>
			
			<div id="runningprograms" name="Running Programs">
                <table border="0" margin="0" width="100%" class="alternatelisttable">
                    <tr>
                        <td class="table-heading">Product Name</td>
                        <td class="table-heading">PID</td>
                        <td class="table-heading">Program</td>
                    </tr>
                    <xsl:for-each select="//INSTANCE[@CLASSNAME='RunningPrograms']">
                        <tr>
                            <td>
                                
                                <xsl:value-of select="./PROPERTY[@NAME='ProcessName']/VALUE" />
                            </td>
                            <td>
                                <xsl:value-of select="./PROPERTY[@NAME='ProcessId']/VALUE" />
                                
                            </td>
                            <td>
                                <xsl:value-of select="./PROPERTY[@NAME='Path']/VALUE" />

                            </td>
                            
                        </tr>
                    </xsl:for-each>
                </table>
			</div>
		</div>

	</xsl:template>

</xsl:stylesheet> 
