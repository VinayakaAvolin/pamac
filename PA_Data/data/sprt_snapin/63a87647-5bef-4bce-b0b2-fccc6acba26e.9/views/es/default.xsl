<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
<div>
  <div id="systemsummary" name="Resumen del sistema" filename="System Summary" class="alternatelisttable">
    <table border="0" margin="0" width="100%">
     <tr>
      <td class="table-heading">Mi ordenador y Resumen de incidentes</td>
      <td class="table-heading"></td>
     </tr>
     <tr>
      <td class="odd-row">Id. de SmartIssue</td>
      <td class="odd-row">{<xsl:value-of select="//UPLOADDATA[1]/@USERNAME"/>}</td>
     </tr>
     <tr>
      <td class="even-row">Id. de la m�quina</td>
      <td class="even-row"><xsl:value-of select="//INSTANCE[@CLASSNAME='SPRT_Connectivity']/PROPERTY[@NAME='MacID']/VALUE"/></td>
     </tr>
     <tr>
      <td class="odd-row">Nombre de DNS</td>
      <td class="odd-row"><xsl:value-of select="//INSTANCE[@CLASSNAME='SPRT_Connectivity']/PROPERTY[@NAME='HostName']/VALUE"/></td>
     </tr>
     <tr>
      <td class="even-row">Dominio o grupo de trabajo</td>
      <td class="even-row"><xsl:value-of select="//INSTANCE[@CLASSNAME='WMIComputerSystem']/PROPERTY[@NAME='Domain']/VALUE"/></td>
     </tr>
     <tr>
      <td class="odd-row">Sistema operativo</td>
      <td class="odd-row"><xsl:variable name="displayOS">
            <xsl:value-of select="//INSTANCE[@CLASSNAME='WMIOperatingSystem']/PROPERTY[@NAME='Caption']/VALUE"/>
            <xsl:variable name="CSDVersion" select="//INSTANCE[@CLASSNAME='WMIOperatingSystem']/PROPERTY[@NAME='CSDVersion']/VALUE"/>
            <xsl:if test="normalize-space($CSDVersion) != ''">
              - <xsl:value-of select="$CSDVersion"/>
            </xsl:if>
          </xsl:variable>
          <xsl:value-of select="$displayOS"/>
     </td>
     </tr>
     <tr>
      <td class="even-row">Versi�n del sistema operativo</td>
      <td class="even-row"><xsl:value-of select="//INSTANCE[@CLASSNAME='WMIOperatingSystem']/PROPERTY[@NAME='Version']/VALUE"/></td>
     </tr>
     <tr>
      <td class="odd-row">Configuraci�n regional</td>
      <td class="odd-row">
        <xsl:variable name="localeName">
          <xsl:call-template name="getLocaleName">
            <xsl:with-param name="localeID"><xsl:value-of select="//INSTANCE[@CLASSNAME='OperatingSystemInfo']/PROPERTY[@NAME='Locale']/VALUE"/></xsl:with-param>
          </xsl:call-template>
        </xsl:variable>
        <xsl:value-of select="$localeName"/>
      </td>
     </tr>
     <tr>
      <td class="even-row">Memoria f�sica total</td>
      <td class="even-row"><xsl:variable name="totalMem" select="//INSTANCE[@CLASSNAME='Win32_LogicalMemoryConfig']/PROPERTY[@NAME='TotalPhysicalMemory']/VALUE"/>
          <xsl:value-of select="$totalMem"/> MB
      </td>
     </tr>
     <tr>
      <td class="odd-row">Memoria libre</td>
      <td class="odd-row"><xsl:variable name="percentLoad" select="//INSTANCE[@CLASSNAME='SPRT_AdditionalSysInfo']/PROPERTY[@NAME='MemoryLoad']/VALUE * 0.01"/>
          <xsl:value-of select="round($totalMem - ($percentLoad * $totalMem))"/> MB
      </td>
     </tr>
     <tr>
      <td class="even-row">Explorador por defecto</td>
      <td class="even-row">
        <xsl:variable name="browser_command" select="//INSTANCE[@CLASSNAME='DefaultBrowser']/PROPERTY[@NAME='BrowserOpenCommand']/VALUE"/>
        <xsl:choose>
          <xsl:when test="contains($browser_command, 'chrome')">Google Chrome</xsl:when>
          <xsl:when test="contains($browser_command, 'firefox')">Firefox</xsl:when>
          <xsl:when test="contains($browser_command, 'safari')">Safari</xsl:when>
          <xsl:when test="contains($browser_command, 'opera')">Opera</xsl:when>
          <xsl:when test="contains($browser_command, 'appxq0fevzme2pys62n3e0fbqa7peapykr8v')">Microsoft Edge</xsl:when>
          <xsl:when test="contains($browser_command, 'Unknown')">Desconocido</xsl:when>
          <xsl:otherwise>Internet Explorer</xsl:otherwise>
        </xsl:choose>
      </td>
     </tr>
     <tr>
      <td class="odd-row">Versi�n por defecto del explorador</td>
      <td class="odd-row"><xsl:value-of select="//INSTANCE[@CLASSNAME='DefaultBrowser']/PROPERTY[@NAME='DefaultBrowserVersion']/VALUE"/></td>
     </tr>     
     <tr>
      <td class="even-row">Cliente de correo electr�nico por defecto</td>
      <td class="even-row">
        <xsl:variable name="Def_HKCU" select="//INSTANCE[@CLASSNAME='Software']/PROPERTY[@NAME='DefaultEmailClientUser']/VALUE" />
        <xsl:choose>
          <xsl:when test="not($Def_HKCU)">
            <xsl:call-template name="check-value">
              <xsl:with-param name="value">
                <xsl:value-of select="//INSTANCE[@CLASSNAME='Software']/PROPERTY[@NAME='DefaultEmailClient']/VALUE"/>
              </xsl:with-param>
            </xsl:call-template>
          </xsl:when>
          <xsl:otherwise>
            <xsl:call-template name="check-value">
              <xsl:with-param name="value"><xsl:value-of select="//INSTANCE[@CLASSNAME='Software']/PROPERTY[@NAME='DefaultEmailClientUser']/VALUE"/></xsl:with-param>
            </xsl:call-template>
          </xsl:otherwise>
        </xsl:choose>
      </td>
     </tr> 
     <tr>
      <td class="odd-row">Direcciones TCP/IP</td>
      <td class="odd-row">         
        <xsl:variable name="tcpAddress" select="//INSTANCE[@CLASSNAME='SPRT_Connectivity']/PROPERTY[@NAME='TCPIP_Address']/VALUE"/>
        <xsl:choose>
          <xsl:when test ="normalize-space($tcpAddress) != '127.0.0.1'">
         <xsl:call-template name="output-tokens">
              <xsl:with-param name="list">
                <xsl:value-of select="normalize-space($tcpAddress)" />
              </xsl:with-param>
           <xsl:with-param name="delimiter">;</xsl:with-param>
         </xsl:call-template>
          </xsl:when>
          <xsl:otherwise>
            -
          </xsl:otherwise>
        </xsl:choose>
      </td>
     </tr> 
     <tr>
      <td class="even-row">Id. MAC</td>
      <td class="even-row">
        <xsl:call-template name="output-tokens">
          <xsl:with-param name="list"><xsl:value-of select="//INSTANCE[@CLASSNAME='EthernetInfo']/PROPERTY[@NAME='MACIDList']/VALUE"/></xsl:with-param>
          <xsl:with-param name="delimiter">;</xsl:with-param>
        </xsl:call-template>
      </td>
     </tr>
     <tr>
      <td class="odd-row">Nombre del producto</td>
      <td class="odd-row"><xsl:value-of select="//INSTANCE[@CLASSNAME='ProductInfo']/PROPERTY[@NAME='ProductName']/VALUE"/></td>
     </tr>
     <tr>
      <td class="even-row"><xsl:value-of select="//INSTANCE[@CLASSNAME='ProductInfo']/PROPERTY[@NAME='ProductName']/VALUE"/> Versi�n</td>
      <td class="even-row"><xsl:value-of select="//INSTANCE[@CLASSNAME='ClientVersion']/PROPERTY[@NAME='ClientBuildVersion']/VALUE"/></td>
     </tr>
    </table>

  </div>
  
  
  
  <div id="windows" name="Windows" filename="Windows">
    <table border="0" margin="0" width="100%" class="alternatelisttable">
     <tr>
      <td class="table-heading">Mi sistema operativo</td>
      <td class="table-heading"></td>
     </tr>
     <tr>
      <td class="odd-row">Sistema operativo</td>
      <td class="odd-row"><xsl:value-of select="$displayOS"/></td>
     </tr>
     <tr>
      <td class="even-row">Tipo de sistema operativo</td>
      <td class="even-row"><xsl:value-of select="//INSTANCE[@CLASSNAME='OperatingSystemInfo']/PROPERTY[@NAME='OSType']/VALUE"/></td>
     </tr>
     <tr>
      <td class="odd-row">Paquete de servicios</td>
      <td class="odd-row">
        <xsl:call-template name="check-value">
          <xsl:with-param name="value"><xsl:value-of select="//INSTANCE[@CLASSNAME='OperatingSystemInfo']/PROPERTY[@NAME='SvcPack']/VALUE"/></xsl:with-param>
        </xsl:call-template>
      </td>
     </tr>
     <tr>
      <td class="even-row">Versi�n del sistema operativo</td>
      <td class="even-row"><xsl:value-of select="//INSTANCE[@CLASSNAME='WMIOperatingSystem']/PROPERTY[@NAME='Version']/VALUE"/></td>
     </tr>
     <tr>
      <td class="odd-row">Informaci�n del archivo de intercambio</td>
      <td class="odd-row">
        <xsl:variable name="virtualMemFilename" select="normalize-space(//INSTANCE[@CLASSNAME='VirtualMemorySetting']/PROPERTY[@NAME='Filename']/VALUE)"/>
        <xsl:choose>
          <xsl:when test="$virtualMemFilename = ''">Paginaci�n deshabilitada</xsl:when>
          <xsl:otherwise><xsl:value-of select="$virtualMemFilename"/> 
                         <xsl:text disable-output-escaping='yes'> </xsl:text>
                         <xsl:value-of select="//INSTANCE[@CLASSNAME='SPRT_AdditionalSysInfo']/PROPERTY[@NAME='PageFileAvailable']/VALUE"/> MB Free                         
          </xsl:otherwise>
	</xsl:choose>
      </td>
     </tr>
     <tr>
      <td class="even-row">Directorio de Windows</td>
      <td class="even-row"><xsl:value-of select="//INSTANCE[@CLASSNAME='PCH_Sysinfo']/PROPERTY[@NAME='WindowsDirectory']/VALUE"/></td>
     </tr>     
     <tr>
      <td class="odd-row">Configuraci�n regional</td>
      <td class="odd-row"><xsl:value-of select="$localeName"/></td>
     </tr>
   </table>
  </div>
  
  
  <div id="myhardware" name="Mi Hardware" filename="My Hardware" class="alternatelisttable">
    <table border="0" margin="0" width="100%">
     <tr>
      <td class="table-heading">Mi CPU</td>
      <td class="table-heading"></td>
     </tr>
     <tr>
      <td class="odd-row" width="30%">N�mero de procesadores</td>
      <td class="odd-row"><xsl:value-of select="//INSTANCE[@CLASSNAME='SPRT_AdditionalSysInfo']/PROPERTY[@NAME='NumberOfProcessors']/VALUE"/></td>
     </tr>
     <tr>
      <td class="even-row">Arquitectura del procesador</td>
      <td class="even-row"><xsl:value-of select="//INSTANCE[@CLASSNAME='SPRT_AdditionalSysInfo']/PROPERTY[@NAME='ProcessorArchitecture']/VALUE"/></td>
     </tr>
     <tr>
      <td class="odd-row">Tipo de procesador</td>
      <td class="odd-row"><xsl:value-of select="//INSTANCE[@CLASSNAME='SPRT_AdditionalSysInfo']/PROPERTY[@NAME='ProcessorType']/VALUE"/></td>
     </tr>
     <tr>
      <td class="even-row">Nivel del procesador</td>
      <td class="even-row"><xsl:value-of select="//INSTANCE[@CLASSNAME='SPRT_AdditionalSysInfo']/PROPERTY[@NAME='ProcessorLevel']/VALUE"/></td>
     </tr>
     <tr>
      <td class="odd-row">Versi�n del procesador</td>
      <td class="odd-row"><xsl:value-of select="//INSTANCE[@CLASSNAME='WMIProcessor']/PROPERTY[@NAME='Version']/VALUE"/></td>
     </tr>
     <tr>
      <td class="even-row">Nombre completo del procesador</td>
      <td class="even-row"><xsl:value-of select="//INSTANCE[@CLASSNAME='WMIProcessor']/PROPERTY[@NAME='Name']/VALUE"/></td>
     </tr>
    </table>

    <table border="0" margin="0" width="100%" class="alternatelisttable">
     <tr>
      <td class="table-heading">Mi memoria</td>
      <td class="table-heading"></td>
     </tr>     
     <tr>
      <td class="odd-row" width="30%">Memoria f�sica total</td>
      <td class="odd-row"><xsl:value-of select="$totalMem"/> MB</td>
     </tr>
     <tr>
      <td class="even-row">Memoria libre</td>
      <td class="even-row"><xsl:value-of select="round($totalMem - ($percentLoad * $totalMem))"/> MB</td>
     </tr>
     <tr>
      <td class="odd-row">Tama�o m�ximo de archivo de p�ginas</td>
      <td class="odd-row">
        <xsl:variable name="virtualMemMaxSize" select="normalize-space(//INSTANCE[@CLASSNAME='VirtualMemorySetting']/PROPERTY[@NAME='MaxSize']/VALUE)"/>
        <xsl:choose>
          <xsl:when test="$virtualMemMaxSize = '0'">Sistema administrado</xsl:when>
          <xsl:when test="$virtualMemMaxSize = ''">Paginaci�n deshabilitada</xsl:when>
          <xsl:otherwise><xsl:value-of select="$virtualMemMaxSize"/> MB</xsl:otherwise>
	</xsl:choose>
      </td>
     </tr>
     <tr>
      <td class="even-row">Tama�o m�ximo del archivo de p�gina</td>
      <td class="even-row">
        <xsl:variable name="virtualMemInitSize" select="normalize-space(//INSTANCE[@CLASSNAME='VirtualMemorySetting']/PROPERTY[@NAME='InitSize']/VALUE)"/>
        <xsl:choose>
          <xsl:when test="$virtualMemInitSize = 0">Sistema administrado</xsl:when>
          <xsl:when test="$virtualMemInitSize = ''">Paginaci�n deshabilitada</xsl:when>
          <xsl:otherwise><xsl:value-of select="$virtualMemInitSize"/> MB</xsl:otherwise>
	</xsl:choose>      
      </td>
     </tr>
     <tr>
      <td class="odd-row">Memoria virtual disponible</td>
      <td class="odd-row"><xsl:value-of select="round(//INSTANCE[@CLASSNAME='WMIOperatingSystem']/PROPERTY[@NAME='FreeVirtualMemory']/VALUE div 1024)"/> MB</td>
     </tr>
     <tr>
      <td class="even-row">Memoria virtual total</td>
      <td class="even-row"><xsl:value-of select="round(//INSTANCE[@CLASSNAME='WMIOperatingSystem']/PROPERTY[@NAME='TotalVirtualMemorySize']/VALUE div 1024)"/> MB</td>
     </tr> 
     <tr>
      <td class="odd-row">Directorio de Windows</td>
      <td class="odd-row"><xsl:value-of select="//INSTANCE[@CLASSNAME='PCH_Sysinfo']/PROPERTY[@NAME='WindowsDirectory']/VALUE"/></td>
     </tr> 
    </table>   

    <table border="0" margin="0" width="100%" class="alternatelisttable">
     <tr>
      <td class="table-heading">Mis unidades de disco</td>
      <td class="table-heading"></td>
      <td class="table-heading"></td>
     </tr>    
     <tr>
      <td class="table-heading">Unidad</td>
      <td class="table-heading">Tama�o de unidad</td>
      <td class="table-heading">Porcentaje libre</td>
     </tr>
      <xsl:for-each select="//INSTANCE[@CLASSNAME='LogicalDiskInfo']">
        <tr>
        
        <td>
          <xsl:if test="position() mod 2 = 1"><xsl:attribute name="class">odd-row</xsl:attribute></xsl:if>
          <xsl:if test="position() mod 2 = 0"><xsl:attribute name="class">even-row</xsl:attribute></xsl:if>
          <xsl:value-of select="PROPERTY[@NAME='DriveName']/VALUE"/>
        </td>
        <xsl:variable name="driveSize" select="PROPERTY[@NAME='TotalCapacity']/VALUE"/>
        <td>
          <xsl:if test="position() mod 2 = 1"><xsl:attribute name="class">odd-row</xsl:attribute></xsl:if>
          <xsl:if test="position() mod 2 = 0"><xsl:attribute name="class">even-row</xsl:attribute></xsl:if>
          <xsl:choose>
            <xsl:when test="$driveSize &gt; 1048576">
              <xsl:value-of select="round($driveSize div 1048576)"/> GB
            </xsl:when>
            <xsl:when test="$driveSize &gt; 1024">
              <xsl:value-of select="round($driveSize div 1024)"/> MB
            </xsl:when>        
            <xsl:otherwise>
              <xsl:value-of select="round($driveSize)"/> KB
            </xsl:otherwise>
          </xsl:choose>
        </td>
        <td>
          <xsl:if test="position() mod 2 = 1"><xsl:attribute name="class">odd-row</xsl:attribute></xsl:if>
          <xsl:if test="position() mod 2 = 0"><xsl:attribute name="class">even-row</xsl:attribute></xsl:if>
          <xsl:value-of select="round(PROPERTY[@NAME='TotalFreeSpace']/VALUE div $driveSize * 100)"/>%
        </td>
        </tr>
      </xsl:for-each>     
     </table>


  </div>


      <div id="network" name="Informaci�n de red" filename="Network Information" class="alternatelisttable">
        <table border="0" margin="0" width="100%">
          <tr>
            <td class="table-heading">Mi red</td>
            <td class="table-heading"></td>
          </tr>
          <tr>
            <td class="odd-row" width="40%">Nombre de equipo</td>
            <td class="odd-row">
              <xsl:value-of select="//INSTANCE[@CLASSNAME='SPRT_Connectivity']/PROPERTY[@NAME='NetBIOSName']/VALUE" />
            </td>
          </tr>
          <tr>
            <td class="even-row">Nombre de DNS</td>
            <td class="even-row">
              <xsl:value-of select="//INSTANCE[@CLASSNAME='SPRT_Connectivity']/PROPERTY[@NAME='HostName']/VALUE" />
            </td>
          </tr>
          <tr>
            <td class="odd-row">Nombre de dominio</td>
            <td class="odd-row">
              <xsl:variable name="domainRole" select="//INSTANCE[@CLASSNAME='WMIComputerSystem']/PROPERTY[@NAME='DomainRole']/VALUE"/>
              <xsl:choose>
                <xsl:when test="$domainRole != 0">
                    <xsl:value-of select="//INSTANCE[@CLASSNAME='WMIComputerSystem']/PROPERTY[@NAME='Domain']/VALUE" />
                </xsl:when>
                <xsl:otherwise>-</xsl:otherwise>
              </xsl:choose>
            </td>
          </tr>
          <tr>
            <td class="even-row">Grupo de trabajo</td>
            <td class="even-row">
              <xsl:choose>
                <xsl:when test="$domainRole = 0">
                    <xsl:value-of select="//INSTANCE[@CLASSNAME='WMIComputerSystem']/PROPERTY[@NAME='Domain']/VALUE" />
                </xsl:when>
                <xsl:otherwise>-</xsl:otherwise>
              </xsl:choose>             
            </td>
          </tr>
          <tr>
            <td class="odd-row">Nombre de usuario</td>
            <td class="odd-row">
              <xsl:value-of select="//INSTANCE[@CLASSNAME='SPRT_Connectivity']/PROPERTY[@NAME='UserName']/VALUE" />
            </td>
          </tr>
          <tr>
            <td class="even-row">Servidores DNS</td>
            <td class="even-row">
              <xsl:call-template name="output-tokens">
                <xsl:with-param name="list"><xsl:value-of select="//INSTANCE[@CLASSNAME='EthernetInfo']/PROPERTY[@NAME='DnsUniqueServerList']/VALUE" /></xsl:with-param>
                <xsl:with-param name="delimiter">;</xsl:with-param>
              </xsl:call-template>            
            </td>
          </tr>
          <tr>
            <td class="odd-row">Direcciones TCP/IP</td>
            <td class="odd-row">
                <xsl:choose>
                  <xsl:when test ="normalize-space($tcpAddress) != '127.0.0.1'">
                    <xsl:call-template name="output-tokens">
                    <xsl:with-param name="list"><xsl:value-of select="normalize-space($tcpAddress)" /></xsl:with-param>
                      <xsl:with-param name="delimiter">;</xsl:with-param>
                    </xsl:call-template>
                  </xsl:when>
                  <xsl:otherwise>
                    -
                  </xsl:otherwise>
                </xsl:choose>
            </td>
          </tr>

          <tr>
            <td class="table-heading">Configuraci�n proxy</td>
            <td class="table-heading"></td>
          </tr>
          <tr>
            <td class="odd-row" width="40%">Proxy habilitado</td>
            <td class="odd-row">
              <xsl:variable name="ProxyEnabled" select="//INSTANCE[@CLASSNAME='ProxyInfo']/PROPERTY[@NAME='ProxyEnabled']/VALUE" />
              <xsl:choose>
	        <xsl:when test="$ProxyEnabled = 1">
	          verdadero
	        </xsl:when>
	        <xsl:otherwise>
	          False
	        </xsl:otherwise>
	      </xsl:choose>
            </td>
          </tr>
          <tr>
            <td class="even-row">Direcci�n proxy</td>
            <td class="even-row">
              <xsl:call-template name="check-value">
                <xsl:with-param name="value"><xsl:value-of select="//INSTANCE[@CLASSNAME='ProxyInfo']/PROPERTY[@NAME='ProxyAddress']/VALUE" /></xsl:with-param>
              </xsl:call-template>
            </td>
          </tr>
          <tr>
            <td class="odd-row">Direcci�n de configuraci�n autom�tica del proxy</td>
            <td class="odd-row">
              <xsl:call-template name="check-value">
                <xsl:with-param name="value"><xsl:value-of select="//INSTANCE[@CLASSNAME='ProxyInfo']/PROPERTY[@NAME='ProxyAutoConfigAddress']/VALUE" /></xsl:with-param>
              </xsl:call-template>
            </td>
          </tr>
          <tr>
            <td class="even-row">Proxy de derivaci�n para direcciones locales</td>
            <td class="even-row">
              <xsl:call-template name="check-value">
                <xsl:with-param name="value"><xsl:value-of select="//INSTANCE[@CLASSNAME='ProxyInfo']/PROPERTY[@NAME='ProxyBypassLocal']/VALUE" /></xsl:with-param>
              </xsl:call-template>
            </td>
          </tr>
          <tr>
            <td class="odd-row">Direcciones locales que derivar</td>
            <td class="odd-row">
              <xsl:call-template name="output-tokens">
                <xsl:with-param name="list"><xsl:value-of select="//INSTANCE[@CLASSNAME='ProxyInfo']/PROPERTY[@NAME='ProxyBypassLocalAddresses']/VALUE" /></xsl:with-param>
                <xsl:with-param name="delimiter">;</xsl:with-param>
              </xsl:call-template> 
            </td>
          </tr>
        </table>      
      
        <table border="0" margin="0" width="100%" class="alternatelisttable">
          <tr>
            <td class="table-heading">Adaptador de red</td>
            <td class="table-heading"></td>
          </tr>
          <xsl:for-each select="//INSTANCE[@CLASSNAME='EthernetInfo']/PROPERTY[starts-with(@NAME,'Counter')]">
            <xsl:variable name="counter" select="./VALUE" />
            
            <tr>
              <td colspan="2">
                <strong><xsl:value-of select="../PROPERTY[@NAME=concat('Description',$counter)]" /></strong>
              </td>
            </tr>
            <tr>
              <td class="odd-row" width="40%">Tipo</td>
              <td class="odd-row">
                <xsl:call-template name="check-value">
                  <xsl:with-param name="value"><xsl:value-of select="../PROPERTY[@NAME=concat('Type',$counter)]" /></xsl:with-param>
                </xsl:call-template>
              </td>
            </tr>
            <tr>
              <td class="even-row">Estado de conexi�n</td>
              <td class="even-row">
                <xsl:choose>
	          <xsl:when test="normalize-space(../PROPERTY[@NAME=concat('ConnectStatus',$counter)])='Connected'">
	            Connected
	          </xsl:when>
	          <xsl:otherwise>
	            Disconnected
	          </xsl:otherwise>
	        </xsl:choose>
              </td>
            </tr>            
            <tr>
              <td class="odd-row">DHCP habilitado</td>
              <td class="odd-row">
                <xsl:call-template name="check-value">
                  <xsl:with-param name="value"><xsl:value-of select="../PROPERTY[@NAME=concat('DhcpEnabled',$counter)]" /></xsl:with-param>
                </xsl:call-template>
              </td>
            </tr>            
            <tr>
              <td class="even-row">Direcciones IP</td>
              <td class="even-row">
                <xsl:call-template name="check-value">
                  <xsl:with-param name="value"><xsl:value-of select="../PROPERTY[@NAME=concat('IPAddress',$counter)]" /></xsl:with-param>
                </xsl:call-template>
              </td>
            </tr>
            <tr>
              <td class="odd-row">Autoconfiguraci�n habilitada</td>
              <td class="odd-row">
                <xsl:call-template name="check-value">
                  <xsl:with-param name="value"><xsl:value-of select="../PROPERTY[@NAME=concat('AutoConfigEnabled',$counter)]" /></xsl:with-param>
                </xsl:call-template>
              </td>
            </tr>
            <tr>
              <td class="even-row">M�scaras de subred</td>
              <td class="even-row">
                <xsl:call-template name="check-value">
                  <xsl:with-param name="value"><xsl:value-of select="../PROPERTY[@NAME=concat('SubnetMask',$counter)]" /></xsl:with-param>
                </xsl:call-template>
              </td>
            </tr>
            <tr>
              <td class="odd-row">Puerta de enlace por defecto</td>
              <td class="odd-row">
                <xsl:call-template name="check-value">
                  <xsl:with-param name="value"><xsl:value-of select="../PROPERTY[@NAME=concat('DefaultGateway',$counter)]" /></xsl:with-param>
                </xsl:call-template>
              </td>
            </tr>
            <tr>
              <td class="even-row">Servidores DNS</td>
              <td class="even-row">
                <xsl:call-template name="output-tokens">
                  <xsl:with-param name="list"><xsl:value-of select="../PROPERTY[@NAME=concat('DnsServerList',$counter)]" /></xsl:with-param>
                  <xsl:with-param name="delimiter">;</xsl:with-param>
                </xsl:call-template>
              </td>
            </tr>
            <tr>
              <td class="odd-row">WINS habilitados</td>
              <td class="odd-row">
                <xsl:call-template name="check-value">
                  <xsl:with-param name="value"><xsl:value-of select="../PROPERTY[@NAME=concat('HaveWins',$counter)]" /></xsl:with-param>
                </xsl:call-template>
              </td>
            </tr>
            <tr>
              <td class="even-row">Servidores WINS</td>
              <td class="even-row">
                <xsl:call-template name="check-value">
                  <xsl:with-param name="value"><xsl:value-of select="../PROPERTY[@NAME=concat('PrimaryWinsServer',$counter)]" /></xsl:with-param>
                </xsl:call-template>
                
                <xsl:if test="normalize-space(../PROPERTY[@NAME=concat('SecondaryWinsServer',$counter)]) != ''">
                  <br/><xsl:value-of select="../PROPERTY[@NAME=concat('SecondaryWinsServer',$counter)]" />
                </xsl:if>
                
              </td>
            </tr>
            <tr>
              <td class="odd-row">Direcci�n de difusi�n</td>
              <td class="odd-row">
                <xsl:call-template name="check-value">
                  <xsl:with-param name="value"><xsl:value-of select="../PROPERTY[@NAME=concat('BroadcastAddress',$counter)]" /></xsl:with-param>
                </xsl:call-template>
              </td>
            </tr>
            <tr>
              <td class="even-row">Servidor DHCP</td>
              <td class="even-row">
                <xsl:call-template name="check-value">
                  <xsl:with-param name="value"><xsl:value-of select="../PROPERTY[@NAME=concat('DhcpServer',$counter)]"/></xsl:with-param>
                </xsl:call-template>
              </td>
            </tr>
            <tr>
              <td class="odd-row">Subred</td>
              <td class="odd-row">
                <xsl:call-template name="check-value">
                  <xsl:with-param name="value"><xsl:value-of select="../PROPERTY[@NAME=concat('Subnet',$counter)]" /></xsl:with-param>
                </xsl:call-template>
              </td>
            </tr>
            <tr>
              <td class="even-row">Direcci�n MAC</td>
              <td class="even-row">
                <xsl:call-template name="check-value">
                  <xsl:with-param name="value"><xsl:value-of select="../PROPERTY[@NAME=concat('PhysicalAddress',$counter)]" /></xsl:with-param>
                </xsl:call-template>
              </td>
            </tr>
            <tr>
              <td class="odd-row">Nombre del servicio</td>
              <td class="odd-row">
                <xsl:call-template name="check-value">
                  <xsl:with-param name="value"><xsl:value-of select="../PROPERTY[@NAME=concat('ServiceName',$counter)]" /></xsl:with-param>
                </xsl:call-template>
              </td>
            </tr>
          </xsl:for-each>
        </table>
        
        
        <table border="0" margin="0" width="100%" class="alternatelisttable">
          <tr>
            <td class="table-heading">Informaci�n inal�mbrica</td>
            <td class="table-heading"></td>
          </tr>
          <tr>
            <td class="odd-row" width="40%">Nombre de conexi�n inal�mbrica</td>
            <td class="odd-row">
              <xsl:call-template name="check-value">
                <xsl:with-param name="value"><xsl:value-of select="//INSTANCE[@CLASSNAME='EthernetInfo']/PROPERTY[@NAME='WirelessConnectionName']/VALUE" /></xsl:with-param>
              </xsl:call-template>
            </td>
          </tr>
          <tr>
            <td class="even-row">Estado de conexi�n</td>
            <td class="even-row">
              <xsl:choose>
	        <xsl:when test="normalize-space(//INSTANCE[@CLASSNAME='EthernetInfo']/PROPERTY[@NAME='WirelessConnectionStatus']/VALUE)='Connected'">
	          Connected
	        </xsl:when>
	        <xsl:otherwise>
	          Disconnected
	        </xsl:otherwise>
	      </xsl:choose>            
            </td>
          </tr>
          <tr>
            <td class="odd-row">SSID</td>
            <td class="odd-row">
              <xsl:call-template name="check-value">
                <xsl:with-param name="value"><xsl:value-of select="//INSTANCE[@CLASSNAME='EthernetInfo']/PROPERTY[@NAME='WirelessSSID']/VALUE" /></xsl:with-param>
              </xsl:call-template>
            </td>
          </tr>
          <tr>
            <td class="even-row">BSSID</td>
            <td class="even-row">
              <xsl:call-template name="check-value">              
                <xsl:with-param name="value"><xsl:value-of select="//INSTANCE[@CLASSNAME='EthernetInfo']/PROPERTY[@NAME='WirelessBSSID']/VALUE" /></xsl:with-param>
              </xsl:call-template>              
            </td>
          </tr>
          <tr>
            <td class="odd-row">Modo de autenticaci�n</td>
            <td class="odd-row">
              <xsl:variable name="WirelessAuthMode" select="normalize-space(//INSTANCE[@CLASSNAME='EthernetInfo']/PROPERTY[@NAME='WirelessAuthMode']/VALUE)" />
              <xsl:choose>
                <xsl:when test="$WirelessAuthMode = ''">-</xsl:when>
	        <xsl:when test="$WirelessAuthMode = 0">Abrir</xsl:when>
	        <xsl:when test="$WirelessAuthMode = 1">Compartido</xsl:when>
	        <xsl:when test="$WirelessAuthMode = 2">Cambio autom�tico</xsl:when>
	        <xsl:when test="$WirelessAuthMode = 3">WPA</xsl:when>
	        <xsl:when test="$WirelessAuthMode = 4">WPA-PSK</xsl:when>
	        <xsl:when test="$WirelessAuthMode = 5">WPANone</xsl:when>
	        <xsl:when test="$WirelessAuthMode = 6">WPA2</xsl:when>
	        <xsl:when test="$WirelessAuthMode = 7">WPA2-PSK</xsl:when>
	        <xsl:otherwise>Desconocido o no aplicable</xsl:otherwise>
	      </xsl:choose>              
              
            </td>
          </tr>
          <tr>
            <td class="even-row">Tipo de cifrado</td>
            <td class="even-row">
              <xsl:variable name="WirelessEncType" select="normalize-space(//INSTANCE[@CLASSNAME='EthernetInfo']/PROPERTY[@NAME='WirelessEncryptType']/VALUE)" />
              <xsl:choose>
                <xsl:when test="$WirelessEncType = ''">-</xsl:when>
	        <xsl:when test="$WirelessEncType = 0">WEP</xsl:when>
	        <xsl:when test="$WirelessEncType = 1">Deshabilitado</xsl:when>
	        <xsl:when test="$WirelessEncType = 2">Deshabilitado</xsl:when>
	        <xsl:when test="$WirelessEncType = 3">No admitido</xsl:when>
	        <xsl:when test="$WirelessEncType = 4">TKIP</xsl:when>
	        <xsl:when test="$WirelessEncType = 5">TKIP</xsl:when>
	        <xsl:when test="$WirelessEncType = 6">AES</xsl:when>
	        <xsl:when test="$WirelessEncType = 7">AES</xsl:when>
	        <xsl:otherwise>Desconocido o no aplicable</xsl:otherwise>
	      </xsl:choose>              
            </td>
          </tr>
          <tr>
            <td class="odd-row">Canal</td>
            <td class="odd-row">
              <xsl:call-template name="check-value">
                <xsl:with-param name="value"><xsl:value-of select="//INSTANCE[@CLASSNAME='EthernetInfo']/PROPERTY[@NAME='WirelessChannel']/VALUE" /></xsl:with-param>
              </xsl:call-template>
            </td>
          </tr>
          <tr>
            <td class="even-row">Fuerza de la se�al</td>
            <td class="even-row">
              <xsl:variable name="WirelessSignalStrength" select="normalize-space(//INSTANCE[@CLASSNAME='EthernetInfo']/PROPERTY[@NAME='WirelessSignalStrength']/VALUE)" />
              <xsl:choose>
                <xsl:when test="$WirelessSignalStrength = ''">-</xsl:when>
	        <xsl:when test="$WirelessSignalStrength &lt; -90">Sin se�al</xsl:when>
	        <xsl:when test="$WirelessSignalStrength &lt; -81">Muy baja</xsl:when>
	        <xsl:when test="$WirelessSignalStrength &lt; -71">Baja</xsl:when>
	        <xsl:when test="$WirelessSignalStrength &lt; -67">Buena</xsl:when>
	        <xsl:when test="$WirelessSignalStrength &lt; -57">Muy buena</xsl:when>
	        <xsl:when test="$WirelessSignalStrength &lt; 0">Excelente</xsl:when>
	        <xsl:otherwise>Desconocido o no aplicable</xsl:otherwise>
	      </xsl:choose>              
            </td>
          </tr>
          <tr>
            <td class="odd-row">Velocidad de v�nculo</td>
            <td class="odd-row">
              <xsl:call-template name="check-value">
                <xsl:with-param name="value"><xsl:value-of select="//INSTANCE[@CLASSNAME='EthernetInfo']/PROPERTY[@NAME='WirelessLinkSpeed']/VALUE" /></xsl:with-param>
                <xsl:with-param name="postfix"> Mbit/s</xsl:with-param>
              </xsl:call-template>
            </td>
          </tr>
          <tr>
            <td class="even-row">Tasas admitidas</td>
            <td class="even-row">
              <xsl:call-template name="check-value">
                <xsl:with-param name="value"><xsl:value-of select="//INSTANCE[@CLASSNAME='EthernetInfo']/PROPERTY[@NAME='WirelessSupportedRates']/VALUE" /></xsl:with-param>
                <xsl:with-param name="postfix"> Mbit/s</xsl:with-param>
              </xsl:call-template>
            </td>
          </tr>
          <tr>
            <td class="odd-row">Modo de infraestructura</td>
            <td class="odd-row">
              <xsl:variable name="WirelessInfrastructure" select="normalize-space(//INSTANCE[@CLASSNAME='EthernetInfo']/PROPERTY[@NAME='WirelessInfrastructure']/VALUE)" />
              <xsl:choose>
                <xsl:when test="$WirelessInfrastructure = ''">-</xsl:when>
	        <xsl:when test="$WirelessInfrastructure = 0">Ad-hoc</xsl:when>
	        <xsl:when test="$WirelessInfrastructure = 1">Infraestructura</xsl:when>
	        <xsl:when test="$WirelessInfrastructure = 2">Auto</xsl:when>
	        <xsl:otherwise>Desconocido o no aplicable</xsl:otherwise>
	      </xsl:choose>              
            </td>
          </tr>          
        </table>        

        <table border="0" margin="0" width="100%" class="alternatelisttable">
          <tr>
            <td class="table-heading">Informaci�n del m�dem</td>
            <td class="table-heading"></td>
          </tr>
          <tr>
            <td class="odd-row" width="40%">Nombre</td>
            <td class="odd-row">
              <xsl:call-template name="check-value">
                <xsl:with-param name="value"><xsl:value-of select="//INSTANCE[@CLASSNAME='ModemInfo']/PROPERTY[@NAME='modem_name']/VALUE" /></xsl:with-param>
              </xsl:call-template>
            </td>
          </tr>
          <tr>
            <td class="even-row">C�digo</td>
            <td class="even-row">
              <xsl:call-template name="check-value">
                <xsl:with-param name="value">
                  <xsl:if test="not(starts-with(//INSTANCE[@CLASSNAME='ModemInfo']/PROPERTY[@NAME='modem_code']/VALUE,'null_modem'))" >
                  <xsl:value-of select="//INSTANCE[@CLASSNAME='ModemInfo']/PROPERTY[@NAME='modem_code']/VALUE" />
                  </xsl:if>
                </xsl:with-param>
              </xsl:call-template>
            </td>
          </tr>
          <tr>
            <td class="odd-row">Tipo de m�dem</td>
            <td class="odd-row">
              <xsl:call-template name="check-value">
                <xsl:with-param name="value"><xsl:value-of select="//INSTANCE[@CLASSNAME='ModemInfo']/PROPERTY[@NAME='modem_type']/VALUE" /></xsl:with-param>
              </xsl:call-template>
            </td>
          </tr>
          <tr>
            <td class="even-row">Tipo de conexi�n</td>
            <td class="even-row">
              <xsl:call-template name="check-value">
                <xsl:with-param name="value"><xsl:value-of select="//INSTANCE[@CLASSNAME='ModemInfo']/PROPERTY[@NAME='modem_conn_type']/VALUE" /></xsl:with-param>
              </xsl:call-template>
            </td>
          </tr>
        </table>
        
      </div>      
      
      
  <div id="environmentvariables" name="Variables de entorno" filename="Environment Variables">
    <xsl:variable name="vLowercaseChars_CONST" select="'abcdefghijklmnopqrstuvwxyz'"/> 
    <xsl:variable name="vUppercaseChars_CONST" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'"/> 
    <table border="0" margin="0" width="100%" class="alternatelisttable">
    <tr>
      <td class="table-heading">Variable de entorno</td>
      <td class="table-heading">Valor</td>
     </tr>
     <xsl:for-each select="//INSTANCE[@CLASSNAME='SPRT_Environments']">
       <tr>
         <td>
           <xsl:if test="position() mod 2 = 1"><xsl:attribute name="class">odd-row</xsl:attribute></xsl:if>
           <xsl:if test="position() mod 2 = 0"><xsl:attribute name="class">even-row</xsl:attribute></xsl:if>
           <xsl:value-of select="translate(./PROPERTY[@NAME='EnvVar']/VALUE, $vLowercaseChars_CONST , $vUppercaseChars_CONST)"/> 
         </td>
         <td>
           <xsl:if test="position() mod 2 = 1"><xsl:attribute name="class">odd-row</xsl:attribute></xsl:if>
           <xsl:if test="position() mod 2 = 0"><xsl:attribute name="class">even-row</xsl:attribute></xsl:if>
           <xsl:value-of select="./PROPERTY[@NAME='Value']/VALUE" />
         </td>
       </tr>
     </xsl:for-each>
   </table>
  </div>
      

  <div id="printerlist" name="Lista de impresora" filename="Printer List">
    <table border="0" margin="0" width="100%" class="alternatelisttable">
    <tr>
      <td class="table-heading">Impresora</td>
      <td class="table-heading">Nombre de servidor</td>
      <td class="table-heading">Tipo</td>
     </tr>
     
     <xsl:if test="count(//INSTANCE[@CLASSNAME='SPRT_Printers']) &lt; 1">
       <tr><td colspan="3"><br/><center>No se ha encontrado ninguna impresora</center></td></tr>
     </xsl:if>
     
     <xsl:for-each select="//INSTANCE[@CLASSNAME='SPRT_Printers']">
       <tr>
         <td>
           <xsl:if test="position() mod 2 = 1"><xsl:attribute name="class">odd-row</xsl:attribute></xsl:if>
           <xsl:if test="position() mod 2 = 0"><xsl:attribute name="class">even-row</xsl:attribute></xsl:if>
           <xsl:value-of select="./PROPERTY[@NAME='Printer']/VALUE" />
         </td>
         <td>
            <xsl:if test="position() mod 2 = 1"><xsl:attribute name="class">odd-row</xsl:attribute></xsl:if>
            <xsl:if test="position() mod 2 = 0"><xsl:attribute name="class">even-row</xsl:attribute></xsl:if>
            <xsl:call-template name="check-value">
              <xsl:with-param name="value"><xsl:value-of select="./PROPERTY[@NAME='Server']/VALUE"/></xsl:with-param>
              <xsl:with-param name="center-on-blank">verdadero</xsl:with-param>
            </xsl:call-template>
         </td>
         <td>
           <xsl:if test="position() mod 2 = 1"><xsl:attribute name="class">odd-row</xsl:attribute></xsl:if>
           <xsl:if test="position() mod 2 = 0"><xsl:attribute name="class">even-row</xsl:attribute></xsl:if>
           <xsl:value-of select="./PROPERTY[@NAME='Type']/VALUE" />
         </td>
       </tr>
     </xsl:for-each>
   </table>
  </div>
      
      
  <div id="runningprograms" name="Programas en ejecuci�n" filename="Running Programs">
    <table border="0" margin="0" width="100%" class="alternatelisttable">
    <tr>
      <td class="table-heading">Programa</td>
      <td class="table-heading">IDP</td>
      <td class="table-heading">Nombre del producto</td>
      <td class="table-heading">Versi�n</td>
     </tr>
     <xsl:for-each select="//INSTANCE[@CLASSNAME='SPRT_Applications']">
       <tr>
         <td>
	  <xsl:variable name="ProgName">
            <xsl:call-template name="substring-before-last">
              <xsl:with-param name="input"><xsl:value-of select="./PROPERTY[@NAME='Application']/VALUE"/>
              </xsl:with-param>
              <xsl:with-param name="substr">(</xsl:with-param>
            </xsl:call-template>
          </xsl:variable> 
           <xsl:if test="position() mod 2 = 1"><xsl:attribute name="class">odd-row</xsl:attribute></xsl:if>
           <xsl:if test="position() mod 2 = 0"><xsl:attribute name="class">even-row</xsl:attribute></xsl:if>
	   <xsl:choose>
	     <xsl:when test="starts-with($ProgName,'\??\')"><xsl:value-of select="substring-after($ProgName,'\??\')" /></xsl:when>
	     <xsl:otherwise><xsl:value-of select="$ProgName" /></xsl:otherwise>
	   </xsl:choose>
         </td>
         <td>
           <xsl:if test="position() mod 2 = 1"><xsl:attribute name="class">odd-row</xsl:attribute></xsl:if>
           <xsl:if test="position() mod 2 = 0"><xsl:attribute name="class">even-row</xsl:attribute></xsl:if>
           <xsl:value-of select="./PROPERTY[@NAME='PID']/VALUE" />
         </td>
         <td>
           <xsl:if test="position() mod 2 = 1"><xsl:attribute name="class">odd-row</xsl:attribute></xsl:if>
           <xsl:if test="position() mod 2 = 0"><xsl:attribute name="class">even-row</xsl:attribute></xsl:if>
           <xsl:call-template name="check-value">
             <xsl:with-param name="value"><xsl:value-of select="./PROPERTY[@NAME='ProductName']/VALUE" /></xsl:with-param>
             <xsl:with-param name="center-on-blank">verdadero</xsl:with-param>
           </xsl:call-template>
         </td>
         <td>
           <xsl:if test="position() mod 2 = 1"><xsl:attribute name="class">odd-row</xsl:attribute></xsl:if>
           <xsl:if test="position() mod 2 = 0"><xsl:attribute name="class">even-row</xsl:attribute></xsl:if>
           <xsl:call-template name="check-value">
             <xsl:with-param name="value"><xsl:value-of select="./PROPERTY[@NAME='ProductVersion']/VALUE" /></xsl:with-param>
             <xsl:with-param name="center-on-blank">verdadero</xsl:with-param>
           </xsl:call-template>
         </td>
       </tr>
     </xsl:for-each>
   </table>
  </div>
  
  
</div>
</xsl:template>


<xsl:template name="check-value">
  <xsl:param name="value" />
  <xsl:param name="center-on-blank" />
  <xsl:param name="postfix" />
  <xsl:choose>
    <xsl:when test="normalize-space($value)='' and $center-on-blank='true'">
      <center>-</center>
    </xsl:when>
    <xsl:when test="normalize-space($value)=''">
      -
    </xsl:when>    
    <xsl:otherwise>
      <xsl:value-of select="$value" /> <xsl:value-of select="$postfix" />
    </xsl:otherwise>
  </xsl:choose> 
</xsl:template>    


<xsl:template name="output-tokens">
  <xsl:param name="list" />
  <xsl:param name="delimiter" />
  <xsl:variable name="newlist">
    <xsl:choose>
      <xsl:when test="contains($list, $delimiter)"><xsl:value-of select="normalize-space($list)" /></xsl:when>
      <xsl:otherwise><xsl:value-of select="concat(normalize-space($list), $delimiter)"/></xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  <xsl:variable name="first" select="substring-before($newlist, $delimiter)" />
  <xsl:variable name="remaining" select="substring-after($newlist, $delimiter)" />
  <xsl:choose>
    <xsl:when test="normalize-space($first)=''">
      -
    </xsl:when>
    <xsl:otherwise>
      <xsl:value-of select="$first" /><br/>
    </xsl:otherwise>
  </xsl:choose>
  <xsl:if test="$remaining">
    <xsl:call-template name="output-tokens">
      <xsl:with-param name="list" select="$remaining" />
      <xsl:with-param name="delimiter"><xsl:value-of select="$delimiter"/></xsl:with-param>
    </xsl:call-template>
  </xsl:if>
</xsl:template>

<xsl:template name="substring-before-last">
  <xsl:param name="input" />
  <xsl:param name="substr" />
  <xsl:if test="$substr and contains($input, $substr)">
    <xsl:variable name="temp" select="substring-after($input, $substr)" />
    <xsl:value-of select="substring-before($input, $substr)" />
    <xsl:if test="contains($temp, $substr)">
      <xsl:value-of select="$substr" />
      <xsl:call-template name="substring-before-last">
        <xsl:with-param name="input" select="$temp" />
        <xsl:with-param name="substr" select="$substr" />
      </xsl:call-template>
    </xsl:if>
  </xsl:if>
</xsl:template>


<xsl:template name="getLocaleName">
  <xsl:param name="localeID" />
  <xsl:choose>
    <xsl:when test="$localeID='00000036'">Afrik�ans</xsl:when>
    <xsl:when test="$localeID='00000436'">Afric�ans (Sur�frica)</xsl:when>
    <xsl:when test="$localeID='0000001C'">Alban�s</xsl:when>
    <xsl:when test="$localeID='0000041C'">Alban�s (Albania)</xsl:when>
    <xsl:when test="$localeID='00000484'">Alsaciano (Francia)</xsl:when>
    <xsl:when test="$localeID='0000045E'">Am�rico (Etiop�a)</xsl:when>
    <xsl:when test="$localeID='00000001'">�rabe</xsl:when>
    <xsl:when test="$localeID='00001401'">�rabe (Argelia)</xsl:when>
    <xsl:when test="$localeID='00003C01'">�rabe (Bahr�in)</xsl:when>
    <xsl:when test="$localeID='00000C01'">�rabe (Egipto)</xsl:when>
    <xsl:when test="$localeID='00000801'">�rabe (Iraq)</xsl:when>
    <xsl:when test="$localeID='00002C01'">�rabe (Jordania)</xsl:when>
    <xsl:when test="$localeID='00003401'">�rabe (Kuwait)</xsl:when>
    <xsl:when test="$localeID='00003001'">�rabe (L�bano)</xsl:when>
    <xsl:when test="$localeID='00001001'">�rabe (Libia)</xsl:when>
    <xsl:when test="$localeID='00001801'">�rabe (Marruecos)</xsl:when>
    <xsl:when test="$localeID='00002001'">�rabe (Om�n)</xsl:when>
    <xsl:when test="$localeID='00004001'">�rabe (Qatar)</xsl:when>
    <xsl:when test="$localeID='00000401'">�rabe (Arabia Saud�)</xsl:when>
    <xsl:when test="$localeID='00002801'">�rabe (Siria)</xsl:when>
    <xsl:when test="$localeID='00001C01'">�rabe (T�nez)</xsl:when>
    <xsl:when test="$localeID='00003801'">�rabe (Emiratos �rabes Unidos)</xsl:when>
    <xsl:when test="$localeID='00002401'">�rabe (Yemen)</xsl:when>
    <xsl:when test="$localeID='0000002B'">Armenio</xsl:when>
    <xsl:when test="$localeID='0000042B'">Armenio (Armenia)</xsl:when>
    <xsl:when test="$localeID='0000044D'">Asam�s (India)</xsl:when>
    <xsl:when test="$localeID='0000002C'">Azer�</xsl:when>
    <xsl:when test="$localeID='0000082C'">Azer� (Cir�lico, Azerbaiy�n)</xsl:when>
    <xsl:when test="$localeID='0000042C'">Azer� (Lat�n, Azerbaiy�n)</xsl:when>
    <xsl:when test="$localeID='0000046D'">Bashkir (Rusia)</xsl:when>
    <xsl:when test="$localeID='0000002D'">Vasco</xsl:when>
    <xsl:when test="$localeID='0000042D'">Vasco (Vasco)</xsl:when>
    <xsl:when test="$localeID='00000023'">Bielorruso</xsl:when>
    <xsl:when test="$localeID='00000423'">Bielorruso (Belar�s)</xsl:when>
    <xsl:when test="$localeID='00000845'">Bengal� (Bangladesh)</xsl:when>
    <xsl:when test="$localeID='00000445'">Bengal� (India)</xsl:when>
    <xsl:when test="$localeID='0000201A'">Bosnio (Cir�lico, Bosnia-Herzegovina)</xsl:when>
    <xsl:when test="$localeID='0000141A'">Bosnio (Lat�n, Bosnia-Herzegovina)</xsl:when>
    <xsl:when test="$localeID='0000047E'">Bret�n (Francia)</xsl:when>
    <xsl:when test="$localeID='00000002'">B�lgaro</xsl:when>
    <xsl:when test="$localeID='00000402'">B�lgaro (Bulgaria)</xsl:when>
    <xsl:when test="$localeID='00000003'">Catal�n</xsl:when>
    <xsl:when test="$localeID='00000403'">Catal�n (catal�n)</xsl:when>
    <xsl:when test="$localeID='00000C04'">Chino (Hong Kong S.A.R.)</xsl:when>
    <xsl:when test="$localeID='00001404'">Chino (Macao S.A.R.)</xsl:when>
    <xsl:when test="$localeID='00000804'">Chino (Rep�blica Popular China)</xsl:when>
    <xsl:when test="$localeID='00000004'">Chino (Simplificado)</xsl:when>
    <xsl:when test="$localeID='00001004'">Chino (Singapur)</xsl:when>
    <xsl:when test="$localeID='00000404'">Chino (Taiw�n)</xsl:when>
    <xsl:when test="$localeID='00007C04'">Chino (Tradicional)</xsl:when>
    <xsl:when test="$localeID='00000483'">Corso (Francia)</xsl:when>
    <xsl:when test="$localeID='0000001A'">Croata</xsl:when>
    <xsl:when test="$localeID='0000041A'">Croata (Croacia)</xsl:when>
    <xsl:when test="$localeID='0000101A'">Bosnio (Lat�n, Bosnia-Herzegovina)</xsl:when>
    <xsl:when test="$localeID='00000005'">Checo</xsl:when>
    <xsl:when test="$localeID='00000405'">Checo (Rep�blica Checa)</xsl:when>
    <xsl:when test="$localeID='00000006'">Dan�s</xsl:when>
    <xsl:when test="$localeID='00000406'">Dan�s (Dinamarca)</xsl:when>
    <xsl:when test="$localeID='0000048C'">Dari (Afganist�n)</xsl:when>
    <xsl:when test="$localeID='00000065'">Divehi</xsl:when>
    <xsl:when test="$localeID='00000465'">Divehi (Maldivas)</xsl:when>
    <xsl:when test="$localeID='00000013'">Holand�s</xsl:when>
    <xsl:when test="$localeID='00000813'">Neerland�s (B�lgica)</xsl:when>
    <xsl:when test="$localeID='00000413'">Neerland�s (Pa�ses Bajos)</xsl:when>
    <xsl:when test="$localeID='00000009'">Ingl�s</xsl:when>
    <xsl:when test="$localeID='00000C09'">Ingl�s (Australia)</xsl:when>
    <xsl:when test="$localeID='00002809'">Ingl�s (Belice)</xsl:when>
    <xsl:when test="$localeID='00001009'">Ingl�s (Canad�)</xsl:when>
    <xsl:when test="$localeID='00002409'">Ingl�s (Caribe)</xsl:when>
    <xsl:when test="$localeID='00004009'">Ingl�s (India)</xsl:when>
    <xsl:when test="$localeID='00001809'">Ingl�s (Irlanda)</xsl:when>
    <xsl:when test="$localeID='00002009'">Ingl�s (Jamaica)</xsl:when>
    <xsl:when test="$localeID='00004409'">Ingl�s (Malasia)</xsl:when>
    <xsl:when test="$localeID='00001409'">Ingl�s (Nueva Zelanda)</xsl:when>
    <xsl:when test="$localeID='00003409'">Ingl�s (Rep�blica de Filipinas)</xsl:when>
    <xsl:when test="$localeID='00004809'">Ingl�s (Singapur)</xsl:when>
    <xsl:when test="$localeID='00001C09'">Ingl�s (Sud�frica)</xsl:when>
    <xsl:when test="$localeID='00002C09'">Ingl�s (Trinidad y Tobago)</xsl:when>
    <xsl:when test="$localeID='00000809'">Ingl�s (Reino Unido)</xsl:when>
    <xsl:when test="$localeID='00000409'">Ingl�s (Estados Unidos)</xsl:when>
    <xsl:when test="$localeID='00003009'">Ingl�s (Zimbabue)</xsl:when>
    <xsl:when test="$localeID='00000025'">Estonio</xsl:when>
    <xsl:when test="$localeID='00000425'">Estonio (Estonia)</xsl:when>
    <xsl:when test="$localeID='00000038'">Fero�s</xsl:when>
    <xsl:when test="$localeID='00000438'">Fero�s (Islas Faroe)</xsl:when>
    <xsl:when test="$localeID='00000464'">Tagal (Filipinas)</xsl:when>
    <xsl:when test="$localeID='0000000B'">Finland�s</xsl:when>
    <xsl:when test="$localeID='0000040B'">Finnish (Finlandia)</xsl:when>
    <xsl:when test="$localeID='0000000C'">Franc�s</xsl:when>
    <xsl:when test="$localeID='0000080C'">Franc�s (B�lgica)</xsl:when>
    <xsl:when test="$localeID='00000C0C'">Franc�s (Canad�)</xsl:when>
    <xsl:when test="$localeID='0000040C'">Franc�s (Francia)</xsl:when>
    <xsl:when test="$localeID='0000140C'">Franc�s (Luxemburgo)</xsl:when>
    <xsl:when test="$localeID='0000180C'">Franc�s (Principado de M�naco)</xsl:when>
    <xsl:when test="$localeID='0000100C'">Franc�s (Suiza)</xsl:when>
    <xsl:when test="$localeID='00000462'">Frisio (Pa�ses Bajos)</xsl:when>
    <xsl:when test="$localeID='00000056'">Gallego</xsl:when>
    <xsl:when test="$localeID='00000456'">Gallego (Galicia)</xsl:when>
    <xsl:when test="$localeID='00000037'">Georgiano</xsl:when>
    <xsl:when test="$localeID='00000437'">Georgiano (Georgia)</xsl:when>
    <xsl:when test="$localeID='00000007'">Alem�n</xsl:when>
    <xsl:when test="$localeID='00000C07'">Alem�n (Austria)</xsl:when>
    <xsl:when test="$localeID='00000407'">Alem�n (Alemania)</xsl:when>
    <xsl:when test="$localeID='00001407'">Alem�n (Liechtenstein)</xsl:when>
    <xsl:when test="$localeID='00001007'">Alem�n (Luxemburgo)</xsl:when>
    <xsl:when test="$localeID='00000807'">Alem�n (Suiza)</xsl:when>
    <xsl:when test="$localeID='00000008'">Greek</xsl:when>
    <xsl:when test="$localeID='00000408'">Griego (Grecia)</xsl:when>
    <xsl:when test="$localeID='0000046F'">Groenland�s (Groenlandia)</xsl:when>
    <xsl:when test="$localeID='00000047'">Gujarati</xsl:when>
    <xsl:when test="$localeID='00000447'">Gujarati (India)</xsl:when>
    <xsl:when test="$localeID='00000468'">Hausa (Lat�n, Nigeria)</xsl:when>
    <xsl:when test="$localeID='0000000D'">Hebreo</xsl:when>
    <xsl:when test="$localeID='0000040D'">Hebreo (Israel)</xsl:when>
    <xsl:when test="$localeID='00000039'">Hindi</xsl:when>
    <xsl:when test="$localeID='00000439'">Hindi (India)</xsl:when>
    <xsl:when test="$localeID='0000000E'">H�ngaro</xsl:when>
    <xsl:when test="$localeID='0000040E'">H�ngaro (Hungr�a)</xsl:when>
    <xsl:when test="$localeID='0000000F'">Island�s</xsl:when>
    <xsl:when test="$localeID='0000040F'">Island�s (Islandia)</xsl:when>
    <xsl:when test="$localeID='00000470'">Igbo (Nigeria)</xsl:when>
    <xsl:when test="$localeID='00000021'">Indonesio</xsl:when>
    <xsl:when test="$localeID='00000421'">Indonesio (Indonesia)</xsl:when>
    <xsl:when test="$localeID='0000085D'">Inuktitut (Lat�n, Canad�)</xsl:when>
    <xsl:when test="$localeID='0000045D'">Inuktitut (sil�bico, Canad�)</xsl:when>
    <xsl:when test="$localeID='0000083C'">Irland�s (Irlanda)</xsl:when>
    <xsl:when test="$localeID='00000434'">isiXhosa (Sud�frica)</xsl:when>
    <xsl:when test="$localeID='00000435'">isiZulu (Sud�frica)</xsl:when>
    <xsl:when test="$localeID='00000010'">Italiano</xsl:when>
    <xsl:when test="$localeID='00000410'">Italiano (Italia)</xsl:when>
    <xsl:when test="$localeID='00000810'">Italiano (Suiza)</xsl:when>
    <xsl:when test="$localeID='00000011'">Japon�s</xsl:when>
    <xsl:when test="$localeID='00000411'">Japon�s (Jap�n)</xsl:when>
    <xsl:when test="$localeID='0000004B'">Kannada</xsl:when>
    <xsl:when test="$localeID='0000044B'">Kannada (India)</xsl:when>
    <xsl:when test="$localeID='0000003F'">Kazajo</xsl:when>
    <xsl:when test="$localeID='0000043F'">Kazajo (Kazajist�n)</xsl:when>
    <xsl:when test="$localeID='00000453'">Khmer (Camboya)</xsl:when>
    <xsl:when test="$localeID='00000486'">K'iche (Guatemala)</xsl:when>
    <xsl:when test="$localeID='00000487'">Kinyarwanda (Rwanda)</xsl:when>
    <xsl:when test="$localeID='00000041'">Kiswahili</xsl:when>
    <xsl:when test="$localeID='00000441'">Kiswahili (Kenya)</xsl:when>
    <xsl:when test="$localeID='00000057'">Konkani</xsl:when>
    <xsl:when test="$localeID='00000457'">Konkani (India)</xsl:when>
    <xsl:when test="$localeID='00000012'">Coreano</xsl:when>
    <xsl:when test="$localeID='00000412'">Coreano (Corea)</xsl:when>
    <xsl:when test="$localeID='00000040'">Kirgu�s</xsl:when>
    <xsl:when test="$localeID='00000440'">Kirguist�n (Kirguist�n)</xsl:when>
    <xsl:when test="$localeID='00000454'">Lao (Lao P.D.R.)</xsl:when>
    <xsl:when test="$localeID='00000026'">Let�n</xsl:when>
    <xsl:when test="$localeID='00000426'">Let�n (Letonia)</xsl:when>
    <xsl:when test="$localeID='00000027'">Lituano</xsl:when>
    <xsl:when test="$localeID='00000427'">Lituano (Lituania)</xsl:when>
    <xsl:when test="$localeID='0000082E'">Bajo sorbio (Alemania)</xsl:when>
    <xsl:when test="$localeID='0000046E'">Luxemburgu�s (Luxemburgo)</xsl:when>
    <xsl:when test="$localeID='0000002F'">Macedonio</xsl:when>
    <xsl:when test="$localeID='0000042F'">Macedonio (Antigua Rep�blica Yugoslava de Macedonia)</xsl:when>
    <xsl:when test="$localeID='0000003E'">Malayo</xsl:when>
    <xsl:when test="$localeID='0000083E'">Malayo (Brunei Darussalam)</xsl:when>
    <xsl:when test="$localeID='0000043E'">Malayo (Malasia)</xsl:when>
    <xsl:when test="$localeID='0000044C'">Malayalam (India)</xsl:when>
    <xsl:when test="$localeID='0000043A'">Malt�s (Malta)</xsl:when>
    <xsl:when test="$localeID='00000481'">Maor� (Nueva Zelanda)</xsl:when>
    <xsl:when test="$localeID='0000047A'">Mapudungun (Chile)</xsl:when>
    <xsl:when test="$localeID='0000004E'">Marat�</xsl:when>
    <xsl:when test="$localeID='0000044E'">Marat� (India)</xsl:when>
    <xsl:when test="$localeID='0000047C'">Mohawk (Mohawk)</xsl:when>
    <xsl:when test="$localeID='00000050'">Mongol</xsl:when>
    <xsl:when test="$localeID='00000450'">Mongol (cir�lico)</xsl:when>
    <xsl:when test="$localeID='00000850'">Mongol (mongol tradicional, RPC)</xsl:when>
    <xsl:when test="$localeID='00000461'">Nepal� (Nepal)</xsl:when>
    <xsl:when test="$localeID='00000014'">Noruego</xsl:when>
    <xsl:when test="$localeID='00000414'">Noruego, Bokm�l (Noruega)</xsl:when>
    <xsl:when test="$localeID='00000814'">Noruego, Nynorsk (Noruega)</xsl:when>
    <xsl:when test="$localeID='00000482'">Occitano (Francia)</xsl:when>
    <xsl:when test="$localeID='00000448'">Oriya (India)</xsl:when>
    <xsl:when test="$localeID='00000463'">Pashto (Afganist�n)</xsl:when>
    <xsl:when test="$localeID='00000029'">Persa</xsl:when>
    <xsl:when test="$localeID='00000429'">Persa</xsl:when>
    <xsl:when test="$localeID='00000015'">Polaco</xsl:when>
    <xsl:when test="$localeID='00000415'">Polaco (Polonia)</xsl:when>
    <xsl:when test="$localeID='00000016'">Portugu�s</xsl:when>
    <xsl:when test="$localeID='00000416'">Portugu�s (Brasil)</xsl:when>
    <xsl:when test="$localeID='00000816'">Portugu�s (Portugal)</xsl:when>
    <xsl:when test="$localeID='00000046'">Punjab�</xsl:when>
    <xsl:when test="$localeID='00000446'">Punjab� (India)</xsl:when>
    <xsl:when test="$localeID='0000046B'">Quechua (Bolivia)</xsl:when>
    <xsl:when test="$localeID='0000086B'">Quechua (Ecuador)</xsl:when>
    <xsl:when test="$localeID='00000C6B'">Quechua (Peru)</xsl:when>
    <xsl:when test="$localeID='00000018'">Rumano</xsl:when>
    <xsl:when test="$localeID='00000418'">Rumano (Rumania)</xsl:when>
    <xsl:when test="$localeID='00000417'">Romanche (Suiza)</xsl:when>
    <xsl:when test="$localeID='00000019'">Ruso</xsl:when>
    <xsl:when test="$localeID='00000419'">Ruso (Rusia)</xsl:when>
    <xsl:when test="$localeID='0000243B'">Sami, Inari (Finlandia)</xsl:when>
    <xsl:when test="$localeID='0000103B'">Sami, Lule (Noruega)</xsl:when>
    <xsl:when test="$localeID='0000143B'">Sami, Lule (Suecia)</xsl:when>
    <xsl:when test="$localeID='00000C3B'">Sami, Norte�o (Finlandia)</xsl:when>
    <xsl:when test="$localeID='0000043B'">Sami, Norte�o (Noruega)</xsl:when>
    <xsl:when test="$localeID='0000083B'">Sami, Norte�o (Suecia)</xsl:when>
    <xsl:when test="$localeID='0000203B'">Sami, Skolt (Finlandia)</xsl:when>
    <xsl:when test="$localeID='0000183B'">Sami, Sure�o (Noruega)</xsl:when>
    <xsl:when test="$localeID='00001C3B'">Sami, Sure�o (Suecia)</xsl:when>
    <xsl:when test="$localeID='0000004F'">S�nscrito</xsl:when>
    <xsl:when test="$localeID='0000044F'">S�nscrito (India)</xsl:when>
    <xsl:when test="$localeID='00007C1A'">Serbio</xsl:when>
    <xsl:when test="$localeID='00001C1A'">Serbio (Cir�lico, Bosnia-Herzegovina)</xsl:when>
    <xsl:when test="$localeID='00000C1A'">Serbio (Cir�lico, Serbia)</xsl:when>
    <xsl:when test="$localeID='0000181A'">Serbio (Lat�n, Bosnia-Herzegovina)</xsl:when>
    <xsl:when test="$localeID='0000081A'">Serbio (Lat�n, Serbia)</xsl:when>
    <xsl:when test="$localeID='0000046C'">Sesotho sa Leboa (Sud�frica)</xsl:when>
    <xsl:when test="$localeID='00000432'">Setswana (Sud�frica)</xsl:when>
    <xsl:when test="$localeID='0000045B'">Sinhala (Sri Lanka)</xsl:when>
    <xsl:when test="$localeID='0000001B'">Eslovaco</xsl:when>
    <xsl:when test="$localeID='0000041B'">Eslovaco (Eslovaquia)</xsl:when>
    <xsl:when test="$localeID='00000024'">Esloveno</xsl:when>
    <xsl:when test="$localeID='00000424'">Esloveno (Eslovenia)</xsl:when>
    <xsl:when test="$localeID='0000000A'">Espa�ol</xsl:when>
    <xsl:when test="$localeID='00002C0A'">Espa�ol (Argentina)</xsl:when>
    <xsl:when test="$localeID='0000400A'">Espa�ol (Bolivia)</xsl:when>
    <xsl:when test="$localeID='0000340A'">Espa�ol (Chile)</xsl:when>
    <xsl:when test="$localeID='0000240A'">Espa�ol (Colombia)</xsl:when>
    <xsl:when test="$localeID='0000140A'">Espa�ol (Costa Rica)</xsl:when>
    <xsl:when test="$localeID='00001C0A'">Espa�ol (Rep�blica Dominicana)</xsl:when>
    <xsl:when test="$localeID='0000300A'">Espa�ol (Ecuador)</xsl:when>
    <xsl:when test="$localeID='0000440A'">Espa�ol (El Salvador)</xsl:when>
    <xsl:when test="$localeID='0000100A'">Espa�ol (Guatemala)</xsl:when>
    <xsl:when test="$localeID='0000480A'">Espa�ol (Honduras)</xsl:when>
    <xsl:when test="$localeID='0000080A'">Espa�ol (Mexico)</xsl:when>
    <xsl:when test="$localeID='00004C0A'">Espa�ol (Nicaragua)</xsl:when>
    <xsl:when test="$localeID='0000180A'">Espa�ol (Panam�)</xsl:when>
    <xsl:when test="$localeID='00003C0A'">Espa�ol (Paraguay)</xsl:when>
    <xsl:when test="$localeID='0000280A'">Espa�ol (Per�)</xsl:when>
    <xsl:when test="$localeID='0000500A'">Espa�ol (Puerto Rico)</xsl:when>
    <xsl:when test="$localeID='00000C0A'">Espa�ol (Espa�a)</xsl:when>
    <xsl:when test="$localeID='0000540A'">Espa�ol (Estados Unidos)</xsl:when>
    <xsl:when test="$localeID='0000380A'">Espa�ol (Uruguay)</xsl:when>
    <xsl:when test="$localeID='0000200A'">Espa�ol (Venezuela)</xsl:when>
    <xsl:when test="$localeID='0000001D'">Sueco</xsl:when>
    <xsl:when test="$localeID='0000081D'">Sueco (Finlandia)</xsl:when>
    <xsl:when test="$localeID='0000041D'">Sueco (Siria)</xsl:when>
    <xsl:when test="$localeID='0000005A'">Sirio</xsl:when>
    <xsl:when test="$localeID='0000045A'">Sirio (Siria)</xsl:when>
    <xsl:when test="$localeID='00000428'">Tayiko (Cir�lico, Tayikist�n)</xsl:when>
    <xsl:when test="$localeID='0000085F'">Tamazight (Lat�n, Argelia)</xsl:when>
    <xsl:when test="$localeID='00000049'">Tamil</xsl:when>
    <xsl:when test="$localeID='00000449'">Tamil (India)</xsl:when>
    <xsl:when test="$localeID='00000044'">T�rtaro</xsl:when>
    <xsl:when test="$localeID='00000444'">T�rtaro (Rusia)</xsl:when>
    <xsl:when test="$localeID='0000004A'">T�lugu</xsl:when>
    <xsl:when test="$localeID='0000044A'">T�lugu (India)</xsl:when>
    <xsl:when test="$localeID='0000001E'">Tailand�s</xsl:when>
    <xsl:when test="$localeID='0000041E'">Tailand�s (Tailandia)</xsl:when>
    <xsl:when test="$localeID='00000451'">Tibetano (RPC)</xsl:when>
    <xsl:when test="$localeID='0000001F'">Turco</xsl:when>
    <xsl:when test="$localeID='0000041F'">Turco (Turqu�a)</xsl:when>
    <xsl:when test="$localeID='00000442'">Turcomano (Turkmenist�n)</xsl:when>
    <xsl:when test="$localeID='00000480'">Uighur (RPC)</xsl:when>
    <xsl:when test="$localeID='00000022'">Ucraniano</xsl:when>
    <xsl:when test="$localeID='00000422'">Ucraniano (Ucrania)</xsl:when>
    <xsl:when test="$localeID='0000042E'">Alto sorbio (Alemania)</xsl:when>
    <xsl:when test="$localeID='00000020'">Urdu</xsl:when>
    <xsl:when test="$localeID='00000420'">Urdu (Rep�blica Isl�mica de Pakist�n)</xsl:when>
    <xsl:when test="$localeID='00000043'">Uzbeko</xsl:when>
    <xsl:when test="$localeID='00000843'">Uzbeko (Cir�lico, Uzbekist�n)</xsl:when>
    <xsl:when test="$localeID='00000443'">Uzbeko (Lat�n, Uzbekist�n)</xsl:when>
    <xsl:when test="$localeID='0000002A'">Vietnamita</xsl:when>
    <xsl:when test="$localeID='0000042A'">Vietnamita (Vietnam)</xsl:when>
    <xsl:when test="$localeID='00000452'">Gal�s (Reino Unido)</xsl:when>
    <xsl:when test="$localeID='00000488'">Wolof (Senegal)</xsl:when>
    <xsl:when test="$localeID='00000485'">Yakuto (Rusia)</xsl:when>
    <xsl:when test="$localeID='00000478'">Yi (RPC)</xsl:when>
    <xsl:when test="$localeID='0000046A'">Yoruba (Nigeria)</xsl:when>
  </xsl:choose>
</xsl:template>

</xsl:stylesheet> 
