<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
<div>
  <div id="systemsummary" name="Systemzusammenfassung" filename="System Summary" class="alternatelisttable">
    <table border="0" margin="0" width="100%">
     <tr>
      <td class="table-heading">Mein Computer- und Ereignis-Zusammenfassung</td>
      <td class="table-heading"></td>
     </tr>
     <tr>
      <td class="odd-row">SmartIssue-ID</td>
      <td class="odd-row">{<xsl:value-of select="//UPLOADDATA[1]/@USERNAME"/>}</td>
     </tr>
     <tr>
      <td class="even-row">Maschinen-ID</td>
      <td class="even-row"><xsl:value-of select="//INSTANCE[@CLASSNAME='SPRT_Connectivity']/PROPERTY[@NAME='MacID']/VALUE"/></td>
     </tr>
     <tr>
      <td class="odd-row">DNS-Name</td>
      <td class="odd-row"><xsl:value-of select="//INSTANCE[@CLASSNAME='SPRT_Connectivity']/PROPERTY[@NAME='HostName']/VALUE"/></td>
     </tr>
     <tr>
      <td class="even-row">Dom�ne oder Arbeitsgruppe</td>
      <td class="even-row"><xsl:value-of select="//INSTANCE[@CLASSNAME='WMIComputerSystem']/PROPERTY[@NAME='Domain']/VALUE"/></td>
     </tr>
     <tr>
      <td class="odd-row">Betriebssystem</td>
      <td class="odd-row"><xsl:variable name="displayOS">
            <xsl:value-of select="//INSTANCE[@CLASSNAME='WMIOperatingSystem']/PROPERTY[@NAME='Caption']/VALUE"/>
            <xsl:variable name="CSDVersion" select="//INSTANCE[@CLASSNAME='WMIOperatingSystem']/PROPERTY[@NAME='CSDVersion']/VALUE"/>
            <xsl:if test="normalize-space($CSDVersion) != ''">
              - <xsl:value-of select="$CSDVersion"/>
            </xsl:if>
          </xsl:variable>
          <xsl:value-of select="$displayOS"/>
     </td>
     </tr>
     <tr>
      <td class="even-row">Betriebssystemversion</td>
      <td class="even-row"><xsl:value-of select="//INSTANCE[@CLASSNAME='WMIOperatingSystem']/PROPERTY[@NAME='Version']/VALUE"/></td>
     </tr>
     <tr>
      <td class="odd-row">Gebietsschema</td>
      <td class="odd-row">
        <xsl:variable name="localeName">
          <xsl:call-template name="getLocaleName">
            <xsl:with-param name="localeID"><xsl:value-of select="//INSTANCE[@CLASSNAME='OperatingSystemInfo']/PROPERTY[@NAME='Locale']/VALUE"/></xsl:with-param>
          </xsl:call-template>
        </xsl:variable>
        <xsl:value-of select="$localeName"/>
      </td>
     </tr>
     <tr>
      <td class="even-row">Gesamter physikalischer Speicher</td>
      <td class="even-row"><xsl:variable name="totalMem" select="//INSTANCE[@CLASSNAME='Win32_LogicalMemoryConfig']/PROPERTY[@NAME='TotalPhysicalMemory']/VALUE"/>
          <xsl:value-of select="$totalMem"/> MB
      </td>
     </tr>
     <tr>
      <td class="odd-row">Freier Speicher</td>
      <td class="odd-row"><xsl:variable name="percentLoad" select="//INSTANCE[@CLASSNAME='SPRT_AdditionalSysInfo']/PROPERTY[@NAME='MemoryLoad']/VALUE * 0.01"/>
          <xsl:value-of select="round($totalMem - ($percentLoad * $totalMem))"/> MB
      </td>
     </tr>
     <tr>
      <td class="even-row">Standard-Browser</td>
      <td class="even-row">
        <xsl:variable name="browser_command" select="//INSTANCE[@CLASSNAME='DefaultBrowser']/PROPERTY[@NAME='BrowserOpenCommand']/VALUE"/>
        <xsl:choose>
          <xsl:when test="contains($browser_command, 'chrome')">Google Chrome</xsl:when>
          <xsl:when test="contains($browser_command, 'firefox')">Firefox</xsl:when>
          <xsl:when test="contains($browser_command, 'safari')">Safari</xsl:when>
          <xsl:when test="contains($browser_command, 'opera')">Opera</xsl:when>
          <xsl:when test="contains($browser_command, 'appxq0fevzme2pys62n3e0fbqa7peapykr8v')">Microsoft Edge</xsl:when>
          <xsl:when test="contains($browser_command, 'Unknown')">Unbekannt</xsl:when>
          <xsl:otherwise>Internet Explorer</xsl:otherwise>
        </xsl:choose>
      </td>
     </tr>
     <tr>
      <td class="odd-row">Standard-Browser-Version</td>
      <td class="odd-row"><xsl:value-of select="//INSTANCE[@CLASSNAME='DefaultBrowser']/PROPERTY[@NAME='DefaultBrowserVersion']/VALUE"/></td>
     </tr>
     <tr>
      <td class="even-row">Standard E-Mail-Client</td>
      <td class="even-row">
        <xsl:variable name="Def_HKCU" select="//INSTANCE[@CLASSNAME='Software']/PROPERTY[@NAME='DefaultEmailClientUser']/VALUE" />
        <xsl:choose>
          <xsl:when test="not($Def_HKCU)">
            <xsl:call-template name="check-value">
              <xsl:with-param name="value">
                <xsl:value-of select="//INSTANCE[@CLASSNAME='Software']/PROPERTY[@NAME='DefaultEmailClient']/VALUE"/>
              </xsl:with-param>
            </xsl:call-template>
          </xsl:when>
          <xsl:otherwise>
            <xsl:call-template name="check-value">
              <xsl:with-param name="value"><xsl:value-of select="//INSTANCE[@CLASSNAME='Software']/PROPERTY[@NAME='DefaultEmailClientUser']/VALUE"/></xsl:with-param>
            </xsl:call-template>
          </xsl:otherwise>
        </xsl:choose>
      </td>
     </tr>
     <tr>
      <td class="odd-row">TCP/IP-Adressen</td>
      <td class="odd-row">
        <xsl:variable name="tcpAddress" select="//INSTANCE[@CLASSNAME='SPRT_Connectivity']/PROPERTY[@NAME='TCPIP_Address']/VALUE"/>
        <xsl:choose>
          <xsl:when test ="normalize-space($tcpAddress) != '127.0.0.1'">
         <xsl:call-template name="output-tokens">
              <xsl:with-param name="list">
                <xsl:value-of select="normalize-space($tcpAddress)" />
              </xsl:with-param>
           <xsl:with-param name="delimiter">;</xsl:with-param>
         </xsl:call-template>
          </xsl:when>
          <xsl:otherwise>
            -
          </xsl:otherwise>
        </xsl:choose>
      </td>
     </tr>
     <tr>
      <td class="even-row">MAC IDs</td>
      <td class="even-row">
        <xsl:call-template name="output-tokens">
          <xsl:with-param name="list"><xsl:value-of select="//INSTANCE[@CLASSNAME='EthernetInfo']/PROPERTY[@NAME='MACIDList']/VALUE"/></xsl:with-param>
          <xsl:with-param name="delimiter">;</xsl:with-param>
        </xsl:call-template>
      </td>
     </tr>
     <tr>
      <td class="odd-row">Produktname</td>
      <td class="odd-row"><xsl:value-of select="//INSTANCE[@CLASSNAME='ProductInfo']/PROPERTY[@NAME='ProductName']/VALUE"/></td>
     </tr>
     <tr>
      <td class="even-row"><xsl:value-of select="//INSTANCE[@CLASSNAME='ProductInfo']/PROPERTY[@NAME='ProductName']/VALUE"/> Version</td>
      <td class="even-row"><xsl:value-of select="//INSTANCE[@CLASSNAME='ClientVersion']/PROPERTY[@NAME='ClientBuildVersion']/VALUE"/></td>
     </tr>
    </table>

  </div>



  <div id="windows" name="Windows" filename="Windows">
    <table border="0" margin="0" width="100%" class="alternatelisttable">
     <tr>
      <td class="table-heading">Mein Betriebssystem</td>
      <td class="table-heading"></td>
     </tr>
     <tr>
      <td class="odd-row">Betriebssystem</td>
      <td class="odd-row"><xsl:value-of select="$displayOS"/></td>
     </tr>
     <tr>
      <td class="even-row">Betriebssystem Typ</td>
      <td class="even-row"><xsl:value-of select="//INSTANCE[@CLASSNAME='OperatingSystemInfo']/PROPERTY[@NAME='OSType']/VALUE"/></td>
     </tr>
     <tr>
      <td class="odd-row">Service-Pack</td>
      <td class="odd-row">
        <xsl:call-template name="check-value">
          <xsl:with-param name="value"><xsl:value-of select="//INSTANCE[@CLASSNAME='OperatingSystemInfo']/PROPERTY[@NAME='SvcPack']/VALUE"/></xsl:with-param>
        </xsl:call-template>
      </td>
     </tr>
     <tr>
      <td class="even-row">Betriebssystemversion</td>
      <td class="even-row"><xsl:value-of select="//INSTANCE[@CLASSNAME='WMIOperatingSystem']/PROPERTY[@NAME='Version']/VALUE"/></td>
     </tr>
     <tr>
      <td class="odd-row">Informationen der Auslagerungsdatei</td>
      <td class="odd-row">
        <xsl:variable name="virtualMemFilename" select="normalize-space(//INSTANCE[@CLASSNAME='VirtualMemorySetting']/PROPERTY[@NAME='Filename']/VALUE)"/>
        <xsl:choose>
          <xsl:when test="$virtualMemFilename = ''">Paginierung deaktiviert</xsl:when>
          <xsl:otherwise><xsl:value-of select="$virtualMemFilename"/>
                         <xsl:text disable-output-escaping='yes'> </xsl:text>
                         <xsl:value-of select="//INSTANCE[@CLASSNAME='SPRT_AdditionalSysInfo']/PROPERTY[@NAME='PageFileAvailable']/VALUE"/> MB Free
          </xsl:otherwise>
  </xsl:choose>
      </td>
     </tr>
     <tr>
      <td class="even-row">Windows-Verzeichnis</td>
      <td class="even-row"><xsl:value-of select="//INSTANCE[@CLASSNAME='PCH_Sysinfo']/PROPERTY[@NAME='WindowsDirectory']/VALUE"/></td>
     </tr>
     <tr>
      <td class="odd-row">Gebietsschema</td>
      <td class="odd-row"><xsl:value-of select="$localeName"/></td>
     </tr>
   </table>
  </div>


  <div id="myhardware" name="Meine Hardware" filename="My Hardware" class="alternatelisttable">
    <table border="0" margin="0" width="100%">
     <tr>
      <td class="table-heading">Meine CPU</td>
      <td class="table-heading"></td>
     </tr>
     <tr>
      <td class="odd-row" width="30%">Anzahl Prozessoren</td>
      <td class="odd-row"><xsl:value-of select="//INSTANCE[@CLASSNAME='SPRT_AdditionalSysInfo']/PROPERTY[@NAME='NumberOfProcessors']/VALUE"/></td>
     </tr>
     <tr>
      <td class="even-row">Prozessorarchitektur</td>
      <td class="even-row"><xsl:value-of select="//INSTANCE[@CLASSNAME='SPRT_AdditionalSysInfo']/PROPERTY[@NAME='ProcessorArchitecture']/VALUE"/></td>
     </tr>
     <tr>
      <td class="odd-row">Prozessortyp</td>
      <td class="odd-row"><xsl:value-of select="//INSTANCE[@CLASSNAME='SPRT_AdditionalSysInfo']/PROPERTY[@NAME='ProcessorType']/VALUE"/></td>
     </tr>
     <tr>
      <td class="even-row">Prozessor-Level</td>
      <td class="even-row"><xsl:value-of select="//INSTANCE[@CLASSNAME='SPRT_AdditionalSysInfo']/PROPERTY[@NAME='ProcessorLevel']/VALUE"/></td>
     </tr>
     <tr>
      <td class="odd-row">Prozessor-Version</td>
      <td class="odd-row"><xsl:value-of select="//INSTANCE[@CLASSNAME='WMIProcessor']/PROPERTY[@NAME='Version']/VALUE"/></td>
     </tr>
     <tr>
      <td class="even-row">Vollst�ndiger Prozessorname</td>
      <td class="even-row"><xsl:value-of select="//INSTANCE[@CLASSNAME='WMIProcessor']/PROPERTY[@NAME='Name']/VALUE"/></td>
     </tr>
    </table>

    <table border="0" margin="0" width="100%" class="alternatelisttable">
     <tr>
      <td class="table-heading">Mein Speicher</td>
      <td class="table-heading"></td>
     </tr>
     <tr>
      <td class="odd-row" width="30%">Gesamter physikalischer Speicher</td>
      <td class="odd-row"><xsl:value-of select="$totalMem"/> MB</td>
     </tr>
     <tr>
      <td class="even-row">Freier Speicher</td>
      <td class="even-row"><xsl:value-of select="round($totalMem - ($percentLoad * $totalMem))"/> MB</td>
     </tr>
     <tr>
      <td class="odd-row">Max. Seitendateigr��e</td>
      <td class="odd-row">
        <xsl:variable name="virtualMemMaxSize" select="normalize-space(//INSTANCE[@CLASSNAME='VirtualMemorySetting']/PROPERTY[@NAME='MaxSize']/VALUE)"/>
        <xsl:choose>
          <xsl:when test="$virtualMemMaxSize = '0'">System verwaltet</xsl:when>
          <xsl:when test="$virtualMemMaxSize = ''">Paginierung deaktiviert</xsl:when>
          <xsl:otherwise><xsl:value-of select="$virtualMemMaxSize"/> MB</xsl:otherwise>
  </xsl:choose>
      </td>
     </tr>
     <tr>
      <td class="even-row">Erste Seitendateigr��e</td>
      <td class="even-row">
        <xsl:variable name="virtualMemInitSize" select="normalize-space(//INSTANCE[@CLASSNAME='VirtualMemorySetting']/PROPERTY[@NAME='InitSize']/VALUE)"/>
        <xsl:choose>
          <xsl:when test="$virtualMemInitSize = 0">System verwaltet</xsl:when>
          <xsl:when test="$virtualMemInitSize = ''">Paginierung deaktiviert</xsl:when>
          <xsl:otherwise><xsl:value-of select="$virtualMemInitSize"/> MB</xsl:otherwise>
  </xsl:choose>
      </td>
     </tr>
     <tr>
      <td class="odd-row">Verf�gbarer virtueller Speicher</td>
      <td class="odd-row"><xsl:value-of select="round(//INSTANCE[@CLASSNAME='WMIOperatingSystem']/PROPERTY[@NAME='FreeVirtualMemory']/VALUE div 1024)"/> MB</td>
     </tr>
     <tr>
      <td class="even-row">Gesamter virtueller Speicher</td>
      <td class="even-row"><xsl:value-of select="round(//INSTANCE[@CLASSNAME='WMIOperatingSystem']/PROPERTY[@NAME='TotalVirtualMemorySize']/VALUE div 1024)"/> MB</td>
     </tr>
     <tr>
      <td class="odd-row">Windows-Verzeichnis</td>
      <td class="odd-row"><xsl:value-of select="//INSTANCE[@CLASSNAME='PCH_Sysinfo']/PROPERTY[@NAME='WindowsDirectory']/VALUE"/></td>
     </tr>
    </table>

    <table border="0" margin="0" width="100%" class="alternatelisttable">
     <tr>
      <td class="table-heading">Meine Festplatten</td>
      <td class="table-heading"></td>
      <td class="table-heading"></td>
     </tr>
     <tr>
      <td class="table-heading">Festplatte</td>
      <td class="table-heading">Festplattengr��e</td>
      <td class="table-heading">Prozent Frei</td>
     </tr>
      <xsl:for-each select="//INSTANCE[@CLASSNAME='LogicalDiskInfo']">
        <tr>

        <td>
          <xsl:if test="position() mod 2 = 1"><xsl:attribute name="class">odd-row</xsl:attribute></xsl:if>
          <xsl:if test="position() mod 2 = 0"><xsl:attribute name="class">even-row</xsl:attribute></xsl:if>
          <xsl:value-of select="PROPERTY[@NAME='DriveName']/VALUE"/>
        </td>
        <xsl:variable name="driveSize" select="PROPERTY[@NAME='TotalCapacity']/VALUE"/>
        <td>
          <xsl:if test="position() mod 2 = 1"><xsl:attribute name="class">odd-row</xsl:attribute></xsl:if>
          <xsl:if test="position() mod 2 = 0"><xsl:attribute name="class">even-row</xsl:attribute></xsl:if>
          <xsl:choose>
            <xsl:when test="$driveSize &gt; 1048576">
              <xsl:value-of select="round($driveSize div 1048576)"/> GB
            </xsl:when>
            <xsl:when test="$driveSize &gt; 1024">
              <xsl:value-of select="round($driveSize div 1024)"/> MB
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="round($driveSize)"/> KB
            </xsl:otherwise>
          </xsl:choose>
        </td>
        <td>
          <xsl:if test="position() mod 2 = 1"><xsl:attribute name="class">odd-row</xsl:attribute></xsl:if>
          <xsl:if test="position() mod 2 = 0"><xsl:attribute name="class">even-row</xsl:attribute></xsl:if>
          <xsl:value-of select="round(PROPERTY[@NAME='TotalFreeSpace']/VALUE div $driveSize * 100)"/>%
        </td>
        </tr>
      </xsl:for-each>
     </table>


  </div>


      <div id="network" name="Netzwerkinformationen" filename="Network Information" class="alternatelisttable">
        <table border="0" margin="0" width="100%">
          <tr>
            <td class="table-heading">Mein Netzwerk</td>
            <td class="table-heading"></td>
          </tr>
          <tr>
            <td class="odd-row" width="40%">Computername</td>
            <td class="odd-row">
              <xsl:value-of select="//INSTANCE[@CLASSNAME='SPRT_Connectivity']/PROPERTY[@NAME='NetBIOSName']/VALUE" />
            </td>
          </tr>
          <tr>
            <td class="even-row">DNS Name</td>
            <td class="even-row">
              <xsl:value-of select="//INSTANCE[@CLASSNAME='SPRT_Connectivity']/PROPERTY[@NAME='HostName']/VALUE" />
            </td>
          </tr>
          <tr>
            <td class="odd-row">Dom�ne-Name</td>
            <td class="odd-row">
              <xsl:variable name="domainRole" select="//INSTANCE[@CLASSNAME='WMIComputerSystem']/PROPERTY[@NAME='DomainRole']/VALUE"/>
              <xsl:choose>
                <xsl:when test="$domainRole != 0">
                    <xsl:value-of select="//INSTANCE[@CLASSNAME='WMIComputerSystem']/PROPERTY[@NAME='Domain']/VALUE" />
                </xsl:when>
                <xsl:otherwise>-</xsl:otherwise>
              </xsl:choose>
            </td>
          </tr>
          <tr>
            <td class="even-row">Arbeitsgruppe</td>
            <td class="even-row">
              <xsl:choose>
                <xsl:when test="$domainRole = 0">
                    <xsl:value-of select="//INSTANCE[@CLASSNAME='WMIComputerSystem']/PROPERTY[@NAME='Domain']/VALUE" />
                </xsl:when>
                <xsl:otherwise>-</xsl:otherwise>
              </xsl:choose>
            </td>
          </tr>
          <tr>
            <td class="odd-row">Benutzername</td>
            <td class="odd-row">
              <xsl:value-of select="//INSTANCE[@CLASSNAME='SPRT_Connectivity']/PROPERTY[@NAME='UserName']/VALUE" />
            </td>
          </tr>
          <tr>
            <td class="even-row">DNS-Server</td>
            <td class="even-row">
              <xsl:call-template name="output-tokens">
                <xsl:with-param name="list"><xsl:value-of select="//INSTANCE[@CLASSNAME='EthernetInfo']/PROPERTY[@NAME='DnsUniqueServerList']/VALUE" /></xsl:with-param>
                <xsl:with-param name="delimiter">;</xsl:with-param>
              </xsl:call-template>
            </td>
          </tr>
          <tr>
            <td class="odd-row">TCP/IP-Adressen</td>
            <td class="odd-row">
                <xsl:choose>
                  <xsl:when test ="normalize-space($tcpAddress) != '127.0.0.1'">
                    <xsl:call-template name="output-tokens">
                    <xsl:with-param name="list"><xsl:value-of select="normalize-space($tcpAddress)" /></xsl:with-param>
                      <xsl:with-param name="delimiter">;</xsl:with-param>
                    </xsl:call-template>
                  </xsl:when>
                  <xsl:otherwise>
                    -
                  </xsl:otherwise>
                </xsl:choose>
            </td>
          </tr>

          <tr>
            <td class="table-heading">Proxy-Konfiguration</td>
            <td class="table-heading"></td>
          </tr>
          <tr>
            <td class="odd-row" width="40%">Proxy aktiviert</td>
            <td class="odd-row">
              <xsl:variable name="ProxyEnabled" select="//INSTANCE[@CLASSNAME='ProxyInfo']/PROPERTY[@NAME='ProxyEnabled']/VALUE" />
              <xsl:choose>
          <xsl:when test="$ProxyEnabled = 1">
            True
          </xsl:when>
          <xsl:otherwise>
            False
          </xsl:otherwise>
        </xsl:choose>
            </td>
          </tr>
          <tr>
            <td class="even-row">Proxy-Adresse</td>
            <td class="even-row">
              <xsl:call-template name="check-value">
                <xsl:with-param name="value"><xsl:value-of select="//INSTANCE[@CLASSNAME='ProxyInfo']/PROPERTY[@NAME='ProxyAddress']/VALUE" /></xsl:with-param>
              </xsl:call-template>
            </td>
          </tr>
          <tr>
            <td class="odd-row">Proxy automatische Konfigurationsadresse</td>
            <td class="odd-row">
              <xsl:call-template name="check-value">
                <xsl:with-param name="value"><xsl:value-of select="//INSTANCE[@CLASSNAME='ProxyInfo']/PROPERTY[@NAME='ProxyAutoConfigAddress']/VALUE" /></xsl:with-param>
              </xsl:call-template>
            </td>
          </tr>
          <tr>
            <td class="even-row">Proxy f�r die lokale Adresse umgehen</td>
            <td class="even-row">
              <xsl:call-template name="check-value">
                <xsl:with-param name="value"><xsl:value-of select="//INSTANCE[@CLASSNAME='ProxyInfo']/PROPERTY[@NAME='ProxyBypassLocal']/VALUE" /></xsl:with-param>
              </xsl:call-template>
            </td>
          </tr>
          <tr>
            <td class="odd-row">Lokale Adressen f�r die Umgehung</td>
            <td class="odd-row">
              <xsl:call-template name="output-tokens">
                <xsl:with-param name="list"><xsl:value-of select="//INSTANCE[@CLASSNAME='ProxyInfo']/PROPERTY[@NAME='ProxyBypassLocalAddresses']/VALUE" /></xsl:with-param>
                <xsl:with-param name="delimiter">;</xsl:with-param>
              </xsl:call-template>
            </td>
          </tr>
        </table>

        <table border="0" margin="0" width="100%" class="alternatelisttable">
          <tr>
            <td class="table-heading">Netzwerkadapter</td>
            <td class="table-heading"></td>
          </tr>
          <xsl:for-each select="//INSTANCE[@CLASSNAME='EthernetInfo']/PROPERTY[starts-with(@NAME,'Counter')]">
            <xsl:variable name="counter" select="./VALUE" />

            <tr>
              <td colspan="2">
                <strong><xsl:value-of select="../PROPERTY[@NAME=concat('Description',$counter)]" /></strong>
              </td>
            </tr>
            <tr>
              <td class="odd-row" width="40%">Typ</td>
              <td class="odd-row">
                <xsl:call-template name="check-value">
                  <xsl:with-param name="value"><xsl:value-of select="../PROPERTY[@NAME=concat('Type',$counter)]" /></xsl:with-param>
                </xsl:call-template>
              </td>
            </tr>
            <tr>
              <td class="even-row">Verbindungsstatus</td>
              <td class="even-row">
                <xsl:choose>
            <xsl:when test="normalize-space(../PROPERTY[@NAME=concat('ConnectStatus',$counter)])='Connected'">
	            Verbunden
            </xsl:when>
            <xsl:otherwise>
	            Getrennt
            </xsl:otherwise>
          </xsl:choose>
              </td>
            </tr>
            <tr>
              <td class="odd-row">DHCP aktiviert</td>
              <td class="odd-row">
                <xsl:call-template name="check-value">
                  <xsl:with-param name="value"><xsl:value-of select="../PROPERTY[@NAME=concat('DhcpEnabled',$counter)]" /></xsl:with-param>
                </xsl:call-template>
              </td>
            </tr>
            <tr>
              <td class="even-row">IP-Adressen</td>
              <td class="even-row">
                <xsl:call-template name="check-value">
                  <xsl:with-param name="value"><xsl:value-of select="../PROPERTY[@NAME=concat('IPAddress',$counter)]" /></xsl:with-param>
                </xsl:call-template>
              </td>
            </tr>
            <tr>
              <td class="odd-row">Autom. Konfiguration aktiviert</td>
              <td class="odd-row">
                <xsl:call-template name="check-value">
                  <xsl:with-param name="value"><xsl:value-of select="../PROPERTY[@NAME=concat('AutoConfigEnabled',$counter)]" /></xsl:with-param>
                </xsl:call-template>
              </td>
            </tr>
            <tr>
              <td class="even-row">Subnetz-Masken</td>
              <td class="even-row">
                <xsl:call-template name="check-value">
                  <xsl:with-param name="value"><xsl:value-of select="../PROPERTY[@NAME=concat('SubnetMask',$counter)]" /></xsl:with-param>
                </xsl:call-template>
              </td>
            </tr>
            <tr>
              <td class="odd-row">Standard-Gateway</td>
              <td class="odd-row">
                <xsl:call-template name="check-value">
                  <xsl:with-param name="value"><xsl:value-of select="../PROPERTY[@NAME=concat('DefaultGateway',$counter)]" /></xsl:with-param>
                </xsl:call-template>
              </td>
            </tr>
            <tr>
              <td class="even-row">DNS-Server</td>
              <td class="even-row">
                <xsl:call-template name="output-tokens">
                  <xsl:with-param name="list"><xsl:value-of select="../PROPERTY[@NAME=concat('DnsServerList',$counter)]" /></xsl:with-param>
                  <xsl:with-param name="delimiter">;</xsl:with-param>
                </xsl:call-template>
              </td>
            </tr>
            <tr>
              <td class="odd-row">WINS aktiviert</td>
              <td class="odd-row">
                <xsl:call-template name="check-value">
                  <xsl:with-param name="value"><xsl:value-of select="../PROPERTY[@NAME=concat('HaveWins',$counter)]" /></xsl:with-param>
                </xsl:call-template>
              </td>
            </tr>
            <tr>
              <td class="even-row">WINS-Server</td>
              <td class="even-row">
                <xsl:call-template name="check-value">
                  <xsl:with-param name="value"><xsl:value-of select="../PROPERTY[@NAME=concat('PrimaryWinsServer',$counter)]" /></xsl:with-param>
                </xsl:call-template>

                <xsl:if test="normalize-space(../PROPERTY[@NAME=concat('SecondaryWinsServer',$counter)]) != ''">
                  <br/><xsl:value-of select="../PROPERTY[@NAME=concat('SecondaryWinsServer',$counter)]" />
                </xsl:if>

              </td>
            </tr>
            <tr>
              <td class="odd-row">Broadcast-Adresse</td>
              <td class="odd-row">
                <xsl:call-template name="check-value">
                  <xsl:with-param name="value"><xsl:value-of select="../PROPERTY[@NAME=concat('BroadcastAddress',$counter)]" /></xsl:with-param>
                </xsl:call-template>
              </td>
            </tr>
            <tr>
              <td class="even-row">DHCP-Server</td>
              <td class="even-row">
                <xsl:call-template name="check-value">
                  <xsl:with-param name="value"><xsl:value-of select="../PROPERTY[@NAME=concat('DhcpServer',$counter)]"/></xsl:with-param>
                </xsl:call-template>
              </td>
            </tr>
            <tr>
              <td class="odd-row">Subnetz</td>
              <td class="odd-row">
                <xsl:call-template name="check-value">
                  <xsl:with-param name="value"><xsl:value-of select="../PROPERTY[@NAME=concat('Subnet',$counter)]" /></xsl:with-param>
                </xsl:call-template>
              </td>
            </tr>
            <tr>
              <td class="even-row">MAC-Adresse</td>
              <td class="even-row">
                <xsl:call-template name="check-value">
                  <xsl:with-param name="value"><xsl:value-of select="../PROPERTY[@NAME=concat('PhysicalAddress',$counter)]" /></xsl:with-param>
                </xsl:call-template>
              </td>
            </tr>
            <tr>
              <td class="odd-row">Dienstname</td>
              <td class="odd-row">
                <xsl:call-template name="check-value">
                  <xsl:with-param name="value"><xsl:value-of select="../PROPERTY[@NAME=concat('ServiceName',$counter)]" /></xsl:with-param>
                </xsl:call-template>
              </td>
            </tr>
          </xsl:for-each>
        </table>


        <table border="0" margin="0" width="100%" class="alternatelisttable">
          <tr>
            <td class="table-heading">Drahtlos-Informationen</td>
            <td class="table-heading"></td>
          </tr>
          <tr>
            <td class="odd-row" width="40%">Drahtlos Verbindungsname</td>
            <td class="odd-row">
              <xsl:call-template name="check-value">
                <xsl:with-param name="value"><xsl:value-of select="//INSTANCE[@CLASSNAME='EthernetInfo']/PROPERTY[@NAME='WirelessConnectionName']/VALUE" /></xsl:with-param>
              </xsl:call-template>
            </td>
          </tr>
          <tr>
            <td class="even-row">Verbindungsstatus</td>
            <td class="even-row">
              <xsl:choose>
          <xsl:when test="normalize-space(//INSTANCE[@CLASSNAME='EthernetInfo']/PROPERTY[@NAME='WirelessConnectionStatus']/VALUE)='Connected'">
	          Verbunden
          </xsl:when>
          <xsl:otherwise>
	          Getrennt
          </xsl:otherwise>
        </xsl:choose>
            </td>
          </tr>
          <tr>
            <td class="odd-row">SSID</td>
            <td class="odd-row">
              <xsl:call-template name="check-value">
                <xsl:with-param name="value"><xsl:value-of select="//INSTANCE[@CLASSNAME='EthernetInfo']/PROPERTY[@NAME='WirelessSSID']/VALUE" /></xsl:with-param>
              </xsl:call-template>
            </td>
          </tr>
          <tr>
            <td class="even-row">BSSID</td>
            <td class="even-row">
              <xsl:call-template name="check-value">
                <xsl:with-param name="value"><xsl:value-of select="//INSTANCE[@CLASSNAME='EthernetInfo']/PROPERTY[@NAME='WirelessBSSID']/VALUE" /></xsl:with-param>
              </xsl:call-template>
            </td>
          </tr>
          <tr>
            <td class="odd-row">Authentifizierungsmodus</td>
            <td class="odd-row">
              <xsl:variable name="WirelessAuthMode" select="normalize-space(//INSTANCE[@CLASSNAME='EthernetInfo']/PROPERTY[@NAME='WirelessAuthMode']/VALUE)" />
              <xsl:choose>
                <xsl:when test="$WirelessAuthMode = ''">-</xsl:when>
	        <xsl:when test="$WirelessAuthMode = 0">Offen</xsl:when>
	        <xsl:when test="$WirelessAuthMode = 1">Geteilt</xsl:when>
	        <xsl:when test="$WirelessAuthMode = 2">Autom. Umschaltung</xsl:when>
          <xsl:when test="$WirelessAuthMode = 3">WPA</xsl:when>
          <xsl:when test="$WirelessAuthMode = 4">WPA-PSK</xsl:when>
          <xsl:when test="$WirelessAuthMode = 5">WPANone</xsl:when>
          <xsl:when test="$WirelessAuthMode = 6">WPA2</xsl:when>
          <xsl:when test="$WirelessAuthMode = 7">WPA2-PSK</xsl:when>
	        <xsl:otherwise>Unbekannt oder nicht zutreffend</xsl:otherwise>
        </xsl:choose>

            </td>
          </tr>
          <tr>
            <td class="even-row">Verschl�sselungstyp</td>
            <td class="even-row">
              <xsl:variable name="WirelessEncType" select="normalize-space(//INSTANCE[@CLASSNAME='EthernetInfo']/PROPERTY[@NAME='WirelessEncryptType']/VALUE)" />
              <xsl:choose>
                <xsl:when test="$WirelessEncType = ''">-</xsl:when>
          <xsl:when test="$WirelessEncType = 0">WEP</xsl:when>
	        <xsl:when test="$WirelessEncType = 1">Deaktiviert</xsl:when>
	        <xsl:when test="$WirelessEncType = 2">Deaktiviert</xsl:when>
	        <xsl:when test="$WirelessEncType = 3">Nicht unterst�tzt.</xsl:when>
          <xsl:when test="$WirelessEncType = 4">TKIP</xsl:when>
          <xsl:when test="$WirelessEncType = 5">TKIP</xsl:when>
          <xsl:when test="$WirelessEncType = 6">AES</xsl:when>
          <xsl:when test="$WirelessEncType = 7">AES</xsl:when>
	        <xsl:otherwise>Unbekannt oder nicht zutreffend</xsl:otherwise>
        </xsl:choose>
            </td>
          </tr>
          <tr>
            <td class="odd-row">Kanal</td>
            <td class="odd-row">
              <xsl:call-template name="check-value">
                <xsl:with-param name="value"><xsl:value-of select="//INSTANCE[@CLASSNAME='EthernetInfo']/PROPERTY[@NAME='WirelessChannel']/VALUE" /></xsl:with-param>
              </xsl:call-template>
            </td>
          </tr>
          <tr>
            <td class="even-row">Signalst�rke</td>
            <td class="even-row">
              <xsl:variable name="WirelessSignalStrength" select="normalize-space(//INSTANCE[@CLASSNAME='EthernetInfo']/PROPERTY[@NAME='WirelessSignalStrength']/VALUE)" />
              <xsl:choose>
                <xsl:when test="$WirelessSignalStrength = ''">-</xsl:when>
	        <xsl:when test="$WirelessSignalStrength &lt; -90">Kein Signal</xsl:when>
	        <xsl:when test="$WirelessSignalStrength &lt; -81">Sehr schlecht</xsl:when>
	        <xsl:when test="$WirelessSignalStrength &lt; -71">Schlecht</xsl:when>
	        <xsl:when test="$WirelessSignalStrength &lt; -67">Gut</xsl:when>
	        <xsl:when test="$WirelessSignalStrength &lt; -57">Sehr gut</xsl:when>
	        <xsl:when test="$WirelessSignalStrength &lt; 0">Exzellent</xsl:when>
	        <xsl:otherwise>Unbekannt oder nicht zutreffend</xsl:otherwise>
        </xsl:choose>
            </td>
          </tr>
          <tr>
            <td class="odd-row">Link-Geschwindigkeit</td>
            <td class="odd-row">
              <xsl:call-template name="check-value">
                <xsl:with-param name="value"><xsl:value-of select="//INSTANCE[@CLASSNAME='EthernetInfo']/PROPERTY[@NAME='WirelessLinkSpeed']/VALUE" /></xsl:with-param>
                <xsl:with-param name="postfix"> Mbit/s</xsl:with-param>
              </xsl:call-template>
            </td>
          </tr>
          <tr>
            <td class="even-row">Unterst�tzte Raten</td>
            <td class="even-row">
              <xsl:call-template name="check-value">
                <xsl:with-param name="value"><xsl:value-of select="//INSTANCE[@CLASSNAME='EthernetInfo']/PROPERTY[@NAME='WirelessSupportedRates']/VALUE" /></xsl:with-param>
                <xsl:with-param name="postfix"> Mbit/s</xsl:with-param>
              </xsl:call-template>
            </td>
          </tr>
          <tr>
            <td class="odd-row">Infrastrukturmodus</td>
            <td class="odd-row">
              <xsl:variable name="WirelessInfrastructure" select="normalize-space(//INSTANCE[@CLASSNAME='EthernetInfo']/PROPERTY[@NAME='WirelessInfrastructure']/VALUE)" />
              <xsl:choose>
                <xsl:when test="$WirelessInfrastructure = ''">-</xsl:when>
                <xsl:when test="$WirelessInfrastructure = 0">Ad-hoc</xsl:when>
	        <xsl:when test="$WirelessInfrastructure = 1">Infrastruktur</xsl:when>
                <xsl:when test="$WirelessInfrastructure = 2">Auto</xsl:when>
	        <xsl:otherwise>Unbekannt oder nicht zutreffend</xsl:otherwise>
              </xsl:choose>
            </td>
          </tr>
        </table>
      </div>


  <div id="environmentvariables" name="Umgebungsvariablen" filename="Environment Variables">
    <xsl:variable name="vLowercaseChars_CONST" select="'abcdefghijklmnopqrstuvwxyz'"/>
    <xsl:variable name="vUppercaseChars_CONST" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'"/>
    <table border="0" margin="0" width="100%" class="alternatelisttable">
    <tr>
      <td class="table-heading">Umgebungsvariable</td>
      <td class="table-heading">Wert</td>
     </tr>
     <xsl:for-each select="//INSTANCE[@CLASSNAME='SPRT_Environments']">
       <tr>
         <td>
           <xsl:if test="position() mod 2 = 1"><xsl:attribute name="class">odd-row</xsl:attribute></xsl:if>
           <xsl:if test="position() mod 2 = 0"><xsl:attribute name="class">even-row</xsl:attribute></xsl:if>
           <xsl:value-of select="translate(./PROPERTY[@NAME='EnvVar']/VALUE, $vLowercaseChars_CONST , $vUppercaseChars_CONST)"/>
         </td>
         <td>
           <xsl:if test="position() mod 2 = 1"><xsl:attribute name="class">odd-row</xsl:attribute></xsl:if>
           <xsl:if test="position() mod 2 = 0"><xsl:attribute name="class">even-row</xsl:attribute></xsl:if>
           <xsl:value-of select="./PROPERTY[@NAME='Value']/VALUE" />
         </td>
       </tr>
     </xsl:for-each>
   </table>
  </div>


  <div id="printerlist" name="Druckerliste" filename="Printer List">
    <table border="0" margin="0" width="100%" class="alternatelisttable">
    <tr>
      <td class="table-heading">Drucker</td>
      <td class="table-heading">Servername</td>
      <td class="table-heading">Typ</td>
     </tr>

     <xsl:if test="count(//INSTANCE[@CLASSNAME='SPRT_Printers']) &lt; 1">
       <tr><td colspan="3"><br/><center>Kein Drucker vorhanden</center></td></tr>
     </xsl:if>

     <xsl:for-each select="//INSTANCE[@CLASSNAME='SPRT_Printers']">
       <tr>
         <td>
           <xsl:if test="position() mod 2 = 1"><xsl:attribute name="class">odd-row</xsl:attribute></xsl:if>
           <xsl:if test="position() mod 2 = 0"><xsl:attribute name="class">even-row</xsl:attribute></xsl:if>
           <xsl:value-of select="./PROPERTY[@NAME='Printer']/VALUE" />
         </td>
         <td>
            <xsl:if test="position() mod 2 = 1"><xsl:attribute name="class">odd-row</xsl:attribute></xsl:if>
            <xsl:if test="position() mod 2 = 0"><xsl:attribute name="class">even-row</xsl:attribute></xsl:if>
            <xsl:call-template name="check-value">
              <xsl:with-param name="value"><xsl:value-of select="./PROPERTY[@NAME='Server']/VALUE"/></xsl:with-param>
              <xsl:with-param name="center-on-blank">true</xsl:with-param>
            </xsl:call-template>
         </td>
         <td>
           <xsl:if test="position() mod 2 = 1"><xsl:attribute name="class">odd-row</xsl:attribute></xsl:if>
           <xsl:if test="position() mod 2 = 0"><xsl:attribute name="class">even-row</xsl:attribute></xsl:if>
           <xsl:value-of select="./PROPERTY[@NAME='Type']/VALUE" />
         </td>
       </tr>
     </xsl:for-each>
   </table>
  </div>


  <div id="runningprograms" name="Laufende Programme" filename="Running Programs">
    <table border="0" margin="0" width="100%" class="alternatelisttable">
    <tr>
      <td class="table-heading">Programm</td>
      <td class="table-heading">PID</td>
      <td class="table-heading">Produktname</td>
      <td class="table-heading">Version</td>
     </tr>
     <xsl:for-each select="//INSTANCE[@CLASSNAME='SPRT_Applications']">
       <tr>
         <td>
    <xsl:variable name="ProgName">
            <xsl:call-template name="substring-before-last">
              <xsl:with-param name="input"><xsl:value-of select="./PROPERTY[@NAME='Application']/VALUE"/>
              </xsl:with-param>
              <xsl:with-param name="substr">(</xsl:with-param>
            </xsl:call-template>
          </xsl:variable>
           <xsl:if test="position() mod 2 = 1"><xsl:attribute name="class">odd-row</xsl:attribute></xsl:if>
           <xsl:if test="position() mod 2 = 0"><xsl:attribute name="class">even-row</xsl:attribute></xsl:if>
     <xsl:choose>
       <xsl:when test="starts-with($ProgName,'\??\')"><xsl:value-of select="substring-after($ProgName,'\??\')" /></xsl:when>
       <xsl:otherwise><xsl:value-of select="$ProgName" /></xsl:otherwise>
     </xsl:choose>
         </td>
         <td>
           <xsl:if test="position() mod 2 = 1"><xsl:attribute name="class">odd-row</xsl:attribute></xsl:if>
           <xsl:if test="position() mod 2 = 0"><xsl:attribute name="class">even-row</xsl:attribute></xsl:if>
           <xsl:value-of select="./PROPERTY[@NAME='PID']/VALUE" />
         </td>
         <td>
           <xsl:if test="position() mod 2 = 1"><xsl:attribute name="class">odd-row</xsl:attribute></xsl:if>
           <xsl:if test="position() mod 2 = 0"><xsl:attribute name="class">even-row</xsl:attribute></xsl:if>
           <xsl:call-template name="check-value">
             <xsl:with-param name="value"><xsl:value-of select="./PROPERTY[@NAME='ProductName']/VALUE" /></xsl:with-param>
             <xsl:with-param name="center-on-blank">true</xsl:with-param>
           </xsl:call-template>
         </td>
         <td>
           <xsl:if test="position() mod 2 = 1"><xsl:attribute name="class">odd-row</xsl:attribute></xsl:if>
           <xsl:if test="position() mod 2 = 0"><xsl:attribute name="class">even-row</xsl:attribute></xsl:if>
           <xsl:call-template name="check-value">
             <xsl:with-param name="value"><xsl:value-of select="./PROPERTY[@NAME='ProductVersion']/VALUE" /></xsl:with-param>
             <xsl:with-param name="center-on-blank">true</xsl:with-param>
           </xsl:call-template>
         </td>
       </tr>
     </xsl:for-each>
   </table>
  </div>


</div>
</xsl:template>


<xsl:template name="check-value">
  <xsl:param name="value" />
  <xsl:param name="center-on-blank" />
  <xsl:param name="postfix" />
  <xsl:choose>
    <xsl:when test="normalize-space($value)='' and $center-on-blank='true'">
      <center>-</center>
    </xsl:when>
    <xsl:when test="normalize-space($value)=''">
      -
    </xsl:when>
    <xsl:otherwise>
      <xsl:value-of select="$value" /> <xsl:value-of select="$postfix" />
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>


<xsl:template name="output-tokens">
  <xsl:param name="list" />
  <xsl:param name="delimiter" />
  <xsl:variable name="newlist">
    <xsl:choose>
      <xsl:when test="contains($list, $delimiter)"><xsl:value-of select="normalize-space($list)" /></xsl:when>
      <xsl:otherwise><xsl:value-of select="concat(normalize-space($list), $delimiter)"/></xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  <xsl:variable name="first" select="substring-before($newlist, $delimiter)" />
  <xsl:variable name="remaining" select="substring-after($newlist, $delimiter)" />
  <xsl:choose>
    <xsl:when test="normalize-space($first)=''">
      -
    </xsl:when>
    <xsl:otherwise>
      <xsl:value-of select="$first" /><br/>
    </xsl:otherwise>
  </xsl:choose>
  <xsl:if test="$remaining">
    <xsl:call-template name="output-tokens">
      <xsl:with-param name="list" select="$remaining" />
      <xsl:with-param name="delimiter"><xsl:value-of select="$delimiter"/></xsl:with-param>
    </xsl:call-template>
  </xsl:if>
</xsl:template>

<xsl:template name="substring-before-last">
  <xsl:param name="input" />
  <xsl:param name="substr" />
  <xsl:if test="$substr and contains($input, $substr)">
    <xsl:variable name="temp" select="substring-after($input, $substr)" />
    <xsl:value-of select="substring-before($input, $substr)" />
    <xsl:if test="contains($temp, $substr)">
      <xsl:value-of select="$substr" />
      <xsl:call-template name="substring-before-last">
        <xsl:with-param name="input" select="$temp" />
        <xsl:with-param name="substr" select="$substr" />
      </xsl:call-template>
    </xsl:if>
  </xsl:if>
</xsl:template>


<xsl:template name="getLocaleName">
  <xsl:param name="localeID" />
  <xsl:choose>
    <xsl:when test="$localeID='00000036'">Afrikaans</xsl:when>
    <xsl:when test="$localeID='00000436'">Afrikaans (S�dafrika)</xsl:when>
    <xsl:when test="$localeID='0000001C'">Albanisch</xsl:when>
    <xsl:when test="$localeID='0000041C'">Albanisch (Albanien)</xsl:when>
    <xsl:when test="$localeID='00000484'">Els�ssisch (Frankreich)</xsl:when>
    <xsl:when test="$localeID='0000045E'">Amharisch (�thiopien)</xsl:when>
    <xsl:when test="$localeID='00000001'">Arabisch</xsl:when>
    <xsl:when test="$localeID='00001401'">Arabisch (Algerien)</xsl:when>
    <xsl:when test="$localeID='00003C01'">Arabisch (Bahrain)</xsl:when>
    <xsl:when test="$localeID='00000C01'">Arabisch (�gypten)</xsl:when>
    <xsl:when test="$localeID='00000801'">Arabisch (Irak)</xsl:when>
    <xsl:when test="$localeID='00002C01'">Arabisch (Jordanien)</xsl:when>
    <xsl:when test="$localeID='00003401'">Arabisch (Kuwait)</xsl:when>
    <xsl:when test="$localeID='00003001'">Arabisch (Libanon)</xsl:when>
    <xsl:when test="$localeID='00001001'">Arabisch (Libyen)</xsl:when>
    <xsl:when test="$localeID='00001801'">Arabisch (Marokko)</xsl:when>
    <xsl:when test="$localeID='00002001'">Arabisch (Ean)</xsl:when>
    <xsl:when test="$localeID='00004001'">Arabisch (Katar)</xsl:when>
    <xsl:when test="$localeID='00000401'">Arabisch (Saudi-Arabaien)</xsl:when>
    <xsl:when test="$localeID='00002801'">Arabisch (Syrien)</xsl:when>
    <xsl:when test="$localeID='00001C01'">Arabisch (Tunesien)</xsl:when>
    <xsl:when test="$localeID='00003801'">Arabisch (VAE)</xsl:when>
    <xsl:when test="$localeID='00002401'">Arabisch (Jemen)</xsl:when>
    <xsl:when test="$localeID='0000002B'">Armenisch</xsl:when>
    <xsl:when test="$localeID='0000042B'">Armenisch (Armenien)</xsl:when>
    <xsl:when test="$localeID='0000044D'">Assamesisch (Indien)</xsl:when>
    <xsl:when test="$localeID='0000002C'">Azeri</xsl:when>
    <xsl:when test="$localeID='0000082C'">Azeri (kyrillisch, Aserbaidschan)</xsl:when>
    <xsl:when test="$localeID='0000042C'">Azeri (Latein, Aserbaidschan)</xsl:when>
    <xsl:when test="$localeID='0000046D'">Bashkir (Russland)</xsl:when>
    <xsl:when test="$localeID='0000002D'">Baskisch</xsl:when>
    <xsl:when test="$localeID='0000042D'">Baskisch (Baskisch)</xsl:when>
    <xsl:when test="$localeID='00000023'">Wei�russisch </xsl:when>
    <xsl:when test="$localeID='00000423'">Wei�russisch (Wei�russland)</xsl:when>
    <xsl:when test="$localeID='00000845'">Bengali (Bangladesch)</xsl:when>
    <xsl:when test="$localeID='00000445'">Bengali  (Indien)</xsl:when>
    <xsl:when test="$localeID='0000201A'">Bosnisch (kyrillisch, Bosnien-Herzegowina)</xsl:when>
    <xsl:when test="$localeID='0000141A'">Bosnisch (Latein, Bosnien-Herzegowina)</xsl:when>
    <xsl:when test="$localeID='0000047E'">Bretonisch (Frankreich)</xsl:when>
    <xsl:when test="$localeID='00000002'">Bulgarisch</xsl:when>
    <xsl:when test="$localeID='00000402'">Bulgarisch (Bulgarien)</xsl:when>
    <xsl:when test="$localeID='00000003'">Catalan</xsl:when>
    <xsl:when test="$localeID='00000403'">Catalan (Catalan)</xsl:when>
    <xsl:when test="$localeID='00000C04'">Chinesisch (Hong Kong S.A.R.)</xsl:when>
    <xsl:when test="$localeID='00001404'">Chinesisch (Macao S.A.R.)</xsl:when>
    <xsl:when test="$localeID='00000804'">Chinesisch (Volksrepublik China)</xsl:when>
    <xsl:when test="$localeID='00000004'">Chinesisch (Vereinfacht)</xsl:when>
    <xsl:when test="$localeID='00001004'">Chinesisch (Singapur)</xsl:when>
    <xsl:when test="$localeID='00000404'">Chinesisch (Taiwan)</xsl:when>
    <xsl:when test="$localeID='00007C04'">Chinesisch (Traditionell)</xsl:when>
    <xsl:when test="$localeID='00000483'">Korsisch (Frankreich)</xsl:when>
    <xsl:when test="$localeID='0000001A'">Kroatisch</xsl:when>
    <xsl:when test="$localeID='0000041A'">Kroatisch (Kroatien)</xsl:when>
    <xsl:when test="$localeID='0000101A'">Kroatisch (Latein, Bosnien-Herzegowina)</xsl:when>
    <xsl:when test="$localeID='00000005'">Tschechisch</xsl:when>
    <xsl:when test="$localeID='00000405'">Tschechisch (Tschechische Republik)</xsl:when>
    <xsl:when test="$localeID='00000006'">D�nisch</xsl:when>
    <xsl:when test="$localeID='00000406'">D�nisch (D�nemark)</xsl:when>
    <xsl:when test="$localeID='0000048C'">Dari (Afghanistan)</xsl:when>
    <xsl:when test="$localeID='00000065'">Divehi</xsl:when>
    <xsl:when test="$localeID='00000465'">Divehi (Mallediven)</xsl:when>
    <xsl:when test="$localeID='00000013'">Fl�misch </xsl:when>
    <xsl:when test="$localeID='00000813'">Fl�misch (Belgien)</xsl:when>
    <xsl:when test="$localeID='00000413'">Fl�misch (Niederlande)</xsl:when>
    <xsl:when test="$localeID='00000009'">Englisch</xsl:when>
    <xsl:when test="$localeID='00000C09'">Englisch (Australien)</xsl:when>
    <xsl:when test="$localeID='00002809'">Englisch (Belize)</xsl:when>
    <xsl:when test="$localeID='00001009'">Englisch (Kanada)</xsl:when>
    <xsl:when test="$localeID='00002409'">Englisch (Karibik)</xsl:when>
    <xsl:when test="$localeID='00004009'">Englisch (Indien)</xsl:when>
    <xsl:when test="$localeID='00001809'">Englisch (Irland)</xsl:when>
    <xsl:when test="$localeID='00002009'">Englisch (Jamaika)</xsl:when>
    <xsl:when test="$localeID='00004409'">Englisch (Malaysia)</xsl:when>
    <xsl:when test="$localeID='00001409'">Englisch (Neuseeland)</xsl:when>
    <xsl:when test="$localeID='00003409'">Englisch (Republik Philippinen)</xsl:when>
    <xsl:when test="$localeID='00004809'">Englisch (Singapur)</xsl:when>
    <xsl:when test="$localeID='00001C09'">Englisch (S�dafrika)</xsl:when>
    <xsl:when test="$localeID='00002C09'">Englisch (Trinidad and Tobago)</xsl:when>
    <xsl:when test="$localeID='00000809'">Englisch (Vereinigtes K�nigreich)</xsl:when>
    <xsl:when test="$localeID='00000409'">Englisch (USA)</xsl:when>
    <xsl:when test="$localeID='00003009'">Englisch (Zimbabwe)</xsl:when>
    <xsl:when test="$localeID='00000025'">Estisch </xsl:when>
    <xsl:when test="$localeID='00000425'">Estisch (Estland)</xsl:when>
    <xsl:when test="$localeID='00000038'">F�r�isch</xsl:when>
    <xsl:when test="$localeID='00000438'">F�r�isch (F�r�er)</xsl:when>
    <xsl:when test="$localeID='00000464'">Filipino (Philippinen)</xsl:when>
    <xsl:when test="$localeID='0000000B'">Finnisch</xsl:when>
    <xsl:when test="$localeID='0000040B'">Finnisch (Finnland)</xsl:when>
    <xsl:when test="$localeID='0000000C'">Franz�sisch</xsl:when>
    <xsl:when test="$localeID='0000080C'">Franz�sisch (Belgien)</xsl:when>
    <xsl:when test="$localeID='00000C0C'">Franz�sisch (Kanada)</xsl:when>
    <xsl:when test="$localeID='0000040C'">Franz�sisch (Frankreich)</xsl:when>
    <xsl:when test="$localeID='0000140C'">Franz�sisch (Luxemburg)</xsl:when>
    <xsl:when test="$localeID='0000180C'">Franz�sisch (F�rstentum Monaco)</xsl:when>
    <xsl:when test="$localeID='0000100C'">Franz�sisch (Schweiz)</xsl:when>
    <xsl:when test="$localeID='00000462'">Friesisch (Niederlande)</xsl:when>
    <xsl:when test="$localeID='00000056'">Galizisch</xsl:when>
    <xsl:when test="$localeID='00000456'">Galizisch (Galizien)</xsl:when>
    <xsl:when test="$localeID='00000037'">Georgisch</xsl:when>
    <xsl:when test="$localeID='00000437'">Georgisch (Georgien)</xsl:when>
    <xsl:when test="$localeID='00000007'">Deutsch</xsl:when>
    <xsl:when test="$localeID='00000C07'">Deutsch (�sterreich)</xsl:when>
    <xsl:when test="$localeID='00000407'">Deutsch (Deutschland)</xsl:when>
    <xsl:when test="$localeID='00001407'">Deutsch (Liechtenstein)</xsl:when>
    <xsl:when test="$localeID='00001007'">Deutsch (Luxemburg)</xsl:when>
    <xsl:when test="$localeID='00000807'">Deutsch (Schweiz)</xsl:when>
    <xsl:when test="$localeID='00000008'">Griechisch</xsl:when>
    <xsl:when test="$localeID='00000408'">Griechisch (Griechenland)</xsl:when>
    <xsl:when test="$localeID='0000046F'">Gr�nl�ndisch (Gr�nland)</xsl:when>
    <xsl:when test="$localeID='00000047'">Gujarati</xsl:when>
    <xsl:when test="$localeID='00000447'">Gujarati (Indien)</xsl:when>
    <xsl:when test="$localeID='00000468'">Hausa (Latein, Nigeria)</xsl:when>
    <xsl:when test="$localeID='0000000D'">Hebr�isch</xsl:when>
    <xsl:when test="$localeID='0000040D'">Hebr�isch (Israel)</xsl:when>
    <xsl:when test="$localeID='00000039'">Hindi</xsl:when>
    <xsl:when test="$localeID='00000439'">Hindi (Indien)</xsl:when>
    <xsl:when test="$localeID='0000000E'">Ungarisch</xsl:when>
    <xsl:when test="$localeID='0000040E'">Ungarisch (Ungrand)</xsl:when>
    <xsl:when test="$localeID='0000000F'">Isl�ndisch</xsl:when>
    <xsl:when test="$localeID='0000040F'">Isl�ndisch (Island)</xsl:when>
    <xsl:when test="$localeID='00000470'">Igbo (Nigeria)</xsl:when>
    <xsl:when test="$localeID='00000021'">Indonesisch</xsl:when>
    <xsl:when test="$localeID='00000421'">Indonesisch (Indonesien)</xsl:when>
    <xsl:when test="$localeID='0000085D'">Inuktitut (Latein, Canada)</xsl:when>
    <xsl:when test="$localeID='0000045D'">Inuktitut (Silbenbildend)</xsl:when>
    <xsl:when test="$localeID='0000083C'">Irisch (Irland)</xsl:when>
    <xsl:when test="$localeID='00000434'">isiXhosa (S�dafrika)</xsl:when>
    <xsl:when test="$localeID='00000435'">isiZulu (S�dafrika)</xsl:when>
    <xsl:when test="$localeID='00000010'">Italienisch</xsl:when>
    <xsl:when test="$localeID='00000410'">Italienisch (Italien)</xsl:when>
    <xsl:when test="$localeID='00000810'">Italienisch (Schweiz)</xsl:when>
    <xsl:when test="$localeID='00000011'">Japanisch</xsl:when>
    <xsl:when test="$localeID='00000411'">Japanisch (Japan)</xsl:when>
    <xsl:when test="$localeID='0000004B'">Kannada</xsl:when>
    <xsl:when test="$localeID='0000044B'">Kannada (Indien)</xsl:when>
    <xsl:when test="$localeID='0000003F'">Kasachisch </xsl:when>
    <xsl:when test="$localeID='0000043F'">Kasachisch (Kasachstan)</xsl:when>
    <xsl:when test="$localeID='00000453'">Khmer (Kambodscha)</xsl:when>
    <xsl:when test="$localeID='00000486'">K'iche (Guatemala)</xsl:when>
    <xsl:when test="$localeID='00000487'">Kinyarwanda (Rwanda)</xsl:when>
    <xsl:when test="$localeID='00000041'">Kiswahili</xsl:when>
    <xsl:when test="$localeID='00000441'">Kiswahili (Kenia)</xsl:when>
    <xsl:when test="$localeID='00000057'">Konkani</xsl:when>
    <xsl:when test="$localeID='00000457'">Konkani (Indien)</xsl:when>
    <xsl:when test="$localeID='00000012'">Koreanisch</xsl:when>
    <xsl:when test="$localeID='00000412'">Koreanisch (Korea)</xsl:when>
    <xsl:when test="$localeID='00000040'">Kirgisisch</xsl:when>
    <xsl:when test="$localeID='00000440'">Kirgisisch (Kirgisistan)</xsl:when>
    <xsl:when test="$localeID='00000454'">Laotisch (Laos)</xsl:when>
    <xsl:when test="$localeID='00000026'">Lettisch</xsl:when>
    <xsl:when test="$localeID='00000426'">Lettisch (Lettland)</xsl:when>
    <xsl:when test="$localeID='00000027'">Litauisch</xsl:when>
    <xsl:when test="$localeID='00000427'">Litauisch (Litauen)</xsl:when>
    <xsl:when test="$localeID='0000082E'">Niedersorbisch (Deutschland)</xsl:when>
    <xsl:when test="$localeID='0000046E'">Luxemburgisch (Luxemburg)</xsl:when>
    <xsl:when test="$localeID='0000002F'">Mazedonisch</xsl:when>
    <xsl:when test="$localeID='0000042F'">Mazedonisch (ehemalige jugoslawische Republik Mazedonien)</xsl:when>
    <xsl:when test="$localeID='0000003E'">Malaiisch</xsl:when>
    <xsl:when test="$localeID='0000083E'">Malaiisch (Brunei Daressalam)</xsl:when>
    <xsl:when test="$localeID='0000043E'">Malaiisch (Malaysia)</xsl:when>
    <xsl:when test="$localeID='0000044C'">Malayalam (Indien)</xsl:when>
    <xsl:when test="$localeID='0000043A'">Maltesisch (Malta)</xsl:when>
    <xsl:when test="$localeID='00000481'">Maori (Neuseeland)</xsl:when>
    <xsl:when test="$localeID='0000047A'">Mapudungun (Chile)</xsl:when>
    <xsl:when test="$localeID='0000004E'">Marathi</xsl:when>
    <xsl:when test="$localeID='0000044E'">Marathi (Indien)</xsl:when>
    <xsl:when test="$localeID='0000047C'">Mohawk (Mohawk)</xsl:when>
    <xsl:when test="$localeID='00000050'">Mongolisch</xsl:when>
    <xsl:when test="$localeID='00000450'">Mongolisch (kyrillisch, Mongolei)</xsl:when>
    <xsl:when test="$localeID='00000850'">Mongolisch (Traditionelles mongolisch, Volksrepublik China)</xsl:when>
    <xsl:when test="$localeID='00000461'">Nepali (Nepal)</xsl:when>
    <xsl:when test="$localeID='00000014'">Norwegisch</xsl:when>
    <xsl:when test="$localeID='00000414'">Norwegisch, Bokm�l (Norwegen)</xsl:when>
    <xsl:when test="$localeID='00000814'">Norwegisch, Nynorsk (Norwegen)</xsl:when>
    <xsl:when test="$localeID='00000482'">Okzitanisch (Frankreich)</xsl:when>
    <xsl:when test="$localeID='00000448'">Oriya (Indien)</xsl:when>
    <xsl:when test="$localeID='00000463'">Pashto (Afghanistan)</xsl:when>
    <xsl:when test="$localeID='00000029'">Persisch</xsl:when>
    <xsl:when test="$localeID='00000429'">Persisch</xsl:when>
    <xsl:when test="$localeID='00000015'">Polnisch</xsl:when>
    <xsl:when test="$localeID='00000415'">Polnisch (Polen)</xsl:when>
    <xsl:when test="$localeID='00000016'">Portugiesisch</xsl:when>
    <xsl:when test="$localeID='00000416'">Portugiesisch (Brasilien)</xsl:when>
    <xsl:when test="$localeID='00000816'">Portugiesisch (Portugal)</xsl:when>
    <xsl:when test="$localeID='00000046'">Punjabi</xsl:when>
    <xsl:when test="$localeID='00000446'">Punjabi (Indien)</xsl:when>
    <xsl:when test="$localeID='0000046B'">Quechua (Bolivien)</xsl:when>
    <xsl:when test="$localeID='0000086B'">Quechua (Ecuador)</xsl:when>
    <xsl:when test="$localeID='00000C6B'">Quechua (Peru)</xsl:when>
    <xsl:when test="$localeID='00000018'">Rum�nisch</xsl:when>
    <xsl:when test="$localeID='00000418'">Rum�nisch (Rum�nien)</xsl:when>
    <xsl:when test="$localeID='00000417'">Romanisch (Schweiz)</xsl:when>
    <xsl:when test="$localeID='00000019'">Russisch</xsl:when>
    <xsl:when test="$localeID='00000419'">Russisch (Russland)</xsl:when>
    <xsl:when test="$localeID='0000243B'">Sami, Inari (Finnland)</xsl:when>
    <xsl:when test="$localeID='0000103B'">Sami, Lule (Norwegen)</xsl:when>
    <xsl:when test="$localeID='0000143B'">Sami, Lule (Schweden)</xsl:when>
    <xsl:when test="$localeID='00000C3B'">Sami, n�rdliches (Finnland)</xsl:when>
    <xsl:when test="$localeID='0000043B'">Sami, n�rdliches (Norwegen)</xsl:when>
    <xsl:when test="$localeID='0000083B'">Sami, n�rdliches (Schweden)</xsl:when>
    <xsl:when test="$localeID='0000203B'">Sami, Skolt (Finnland)</xsl:when>
    <xsl:when test="$localeID='0000183B'">Sami, Southern (Norwegen)</xsl:when>
    <xsl:when test="$localeID='00001C3B'">Sami, Southern (Schweden)</xsl:when>
    <xsl:when test="$localeID='0000004F'">Sanskrit</xsl:when>
    <xsl:when test="$localeID='0000044F'">Sanskrit (Indien)</xsl:when>
    <xsl:when test="$localeID='00007C1A'">Serbisch</xsl:when>
    <xsl:when test="$localeID='00001C1A'">Serbisch (Kyrillisch, Bosnien-Herzegowina)</xsl:when>
    <xsl:when test="$localeID='00000C1A'">Serbisch (Kyrillisch,Serbien)</xsl:when>
    <xsl:when test="$localeID='0000181A'">Serbisch (Latein, Bosnien-Herzegowina)</xsl:when>
    <xsl:when test="$localeID='0000081A'">Serbisch (Latein, Serbien)</xsl:when>
    <xsl:when test="$localeID='0000046C'">Sesotho sa Leboa (S�dafrika)</xsl:when>
    <xsl:when test="$localeID='00000432'">Setswana (S�dafrika)</xsl:when>
    <xsl:when test="$localeID='0000045B'">Sinhala (Sri Lanka)</xsl:when>
    <xsl:when test="$localeID='0000001B'">Slowakisch</xsl:when>
    <xsl:when test="$localeID='0000041B'">Slowakisch (Slowakei)</xsl:when>
    <xsl:when test="$localeID='00000024'">Slowenisch</xsl:when>
    <xsl:when test="$localeID='00000424'">Slowenisch (Slowenien)</xsl:when>
    <xsl:when test="$localeID='0000000A'">Spanisch</xsl:when>
    <xsl:when test="$localeID='00002C0A'">Spanisch (Argentinien)</xsl:when>
    <xsl:when test="$localeID='0000400A'">Spanisch (Bolivien)</xsl:when>
    <xsl:when test="$localeID='0000340A'">Spanisch (Chile)</xsl:when>
    <xsl:when test="$localeID='0000240A'">Spanisch (Kolumbien)</xsl:when>
    <xsl:when test="$localeID='0000140A'">Spanisch (Costa Rica)</xsl:when>
    <xsl:when test="$localeID='00001C0A'">Spanisch (Dominikanische Republik)</xsl:when>
    <xsl:when test="$localeID='0000300A'">Spanisch (Ecuador)</xsl:when>
    <xsl:when test="$localeID='0000440A'">Spanisch (El Salvador)</xsl:when>
    <xsl:when test="$localeID='0000100A'">Spanisch (Guatemala)</xsl:when>
    <xsl:when test="$localeID='0000480A'">Spanisch (Honduras)</xsl:when>
    <xsl:when test="$localeID='0000080A'">Spanisch (Mexico)</xsl:when>
    <xsl:when test="$localeID='00004C0A'">Spanisch (Nicaragua)</xsl:when>
    <xsl:when test="$localeID='0000180A'">Spanisch (Panama)</xsl:when>
    <xsl:when test="$localeID='00003C0A'">Spanisch (Paraguay)</xsl:when>
    <xsl:when test="$localeID='0000280A'">Spanisch (Peru)</xsl:when>
    <xsl:when test="$localeID='0000500A'">Spanisch (Puerto Rico)</xsl:when>
    <xsl:when test="$localeID='00000C0A'">Spanisch (Spanien)</xsl:when>
    <xsl:when test="$localeID='0000540A'">Spanisch (USA)</xsl:when>
    <xsl:when test="$localeID='0000380A'">Spanisch (Uruguay)</xsl:when>
    <xsl:when test="$localeID='0000200A'">Spanisch (Venezuela)</xsl:when>
    <xsl:when test="$localeID='0000001D'">Schwedisch</xsl:when>
    <xsl:when test="$localeID='0000081D'">Schwedisch(Finnland)</xsl:when>
    <xsl:when test="$localeID='0000041D'">Schwedisch(Schweden)</xsl:when>
    <xsl:when test="$localeID='0000005A'">Syrisch</xsl:when>
    <xsl:when test="$localeID='0000045A'">Syrisch (Syrien)</xsl:when>
    <xsl:when test="$localeID='00000428'">Tajik (kyrillisch, Tadschikistan)</xsl:when>
    <xsl:when test="$localeID='0000085F'">Tamazight (Latein, Algerien)</xsl:when>
    <xsl:when test="$localeID='00000049'">Tamil</xsl:when>
    <xsl:when test="$localeID='00000449'">Tamil (Indien)</xsl:when>
    <xsl:when test="$localeID='00000044'">Tatarisch</xsl:when>
    <xsl:when test="$localeID='00000444'">Tatarisch (Russland)</xsl:when>
    <xsl:when test="$localeID='0000004A'">Telugu</xsl:when>
    <xsl:when test="$localeID='0000044A'">Telugu (Indien)</xsl:when>
    <xsl:when test="$localeID='0000001E'">Thai</xsl:when>
    <xsl:when test="$localeID='0000041E'">Thai (Thailand)</xsl:when>
    <xsl:when test="$localeID='00000451'">Tibetanisch (Volksrepublik China)</xsl:when>
    <xsl:when test="$localeID='0000001F'">T�rkisch</xsl:when>
    <xsl:when test="$localeID='0000041F'">T�rkisch (T�rkei)</xsl:when>
    <xsl:when test="$localeID='00000442'">Turkmenisch (Turkmenistan)</xsl:when>
    <xsl:when test="$localeID='00000480'">Uighur (Volksrepublik China)</xsl:when>
    <xsl:when test="$localeID='00000022'">Ukrainisch</xsl:when>
    <xsl:when test="$localeID='00000422'">Ukrainisch (Ukraine)</xsl:when>
    <xsl:when test="$localeID='0000042E'">Hoch-Sorbisch (Deutschland)</xsl:when>
    <xsl:when test="$localeID='00000020'">Urdu</xsl:when>
    <xsl:when test="$localeID='00000420'">Urdu (Islamische Republik Pakistan)</xsl:when>
    <xsl:when test="$localeID='00000043'">Usbekisch</xsl:when>
    <xsl:when test="$localeID='00000843'">Usbekisch (kyrillisch, Usbekistan)</xsl:when>
    <xsl:when test="$localeID='00000443'">Usbekisch(Latein)</xsl:when>
    <xsl:when test="$localeID='0000002A'">Vietnamesisch</xsl:when>
    <xsl:when test="$localeID='0000042A'">Vietnamesisch (Vietnam)</xsl:when>
    <xsl:when test="$localeID='00000452'">Walisisch (Vereinigtes K�nigreich)</xsl:when>
    <xsl:when test="$localeID='00000488'">Wolof (Senegal)</xsl:when>
    <xsl:when test="$localeID='00000485'">Yakut (Russland)</xsl:when>
    <xsl:when test="$localeID='00000478'">Yi (Volksrepublik China)</xsl:when>
    <xsl:when test="$localeID='0000046A'">Yoruba (Nigeria)</xsl:when>
  </xsl:choose>
</xsl:template>

</xsl:stylesheet>
