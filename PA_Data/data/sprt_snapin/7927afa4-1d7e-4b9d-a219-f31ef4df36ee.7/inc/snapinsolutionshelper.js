$ss.snapin = $ss.snapin ||
{};
$ss.snapin.solutions = $ss.snapin.solutions ||
{};
$ss.snapin.solutions.helper = $ss.snapin.solutions.helper ||
{};

//Variables for Logging:
var g_rpt_pvt_rxLineBreaks = new RegExp("[\r|\n|\r\n]", "g");
var g_rpt_pvt_rxBadChars = new RegExp("[\\\|\"\']", "g");
var g_ss_rl_pvt_bCacheLogging = false;
var g_ss_rl_pvt_logCache = [];
var g_con_ScopeCache = null;
var g_con_bUsingBCONT = null;




(function(){
  $.extend($ss.snapin.solutions.helper, {
    GetContentLinkUrl: function(oContent, sPath, sTemplate, sBackToUrl){

      var sContentUrl = sPath + '?' +
      'snpsoln_template=' +
      sTemplate +
      '&snpsolution_cid=' +
      oContent.cid +
      '&snpsolution_version=' +
      oContent.version +
      '&snpsolution_ctype=' +
      oContent.ctype +
      '&snpsoln_doneurl=' +
      sBackToUrl;
      
      return sContentUrl;
    },
    
    GetContentPath: function(oContent){
      //var filePath = $ss.GetAttribute('startingpoint') + oContent.ctype + "\\" + oContent.cid + "." + oContent.version + "\\" + "View\\View.html";
      var filePath = _GetViewPage(oContent.ctype,oContent.cid,oContent.version);
      return filePath;
    },
    
    GetFAQLinkData: function(oContent, arrFAQExtTypes){
      var linkElements = {
        sTarget: "",
        bExternalView: false,
        sHref: "#"
      };
      
      for (var i = 0; i < arrFAQExtTypes.length; i++) {
        if (arrFAQExtTypes[i] === oContent.ctype) {
          linkElements.sTarget = 'target="_new"';
          linkElements.bExternalView = true;
          linkElements.sHref = this.GetContentPath(oContent);
          break;
        }
      }
      return linkElements;
    },
    
    GetFolderLinkUrl: function(oFolder, sPath, sTemplate, sBackToUrl){
      var sFolderLink = sPath + '?' +
      'snpsoln_template=' +
      sTemplate +
      '&snpsoln_fid=' +
      oFolder.fid +
      '&snpsoln_doneurl=' +
      sBackToUrl;
      
      return sFolderLink;
    },
    
    GetBackToURL: function(oFolder, sPath, sTemplate){
      var sLink = '&snpsoln_doneurl=' + sPath + '?' + 'snpsoln_template=' +
      sTemplate +
      '&snpsoln_fid=' +
      oFolder.fid;
      
      return sLink;
    },
    
    RenderSolutionView: function(sContentType, sContentGuid, sContentVersion){
      var viewerhtml = $ss.GetSnapinAbsolutePath("snapin_solutions") + "\\views\\SolutionView.html";     
      var dlgHeight = $ss.agentcore.dal.config.GetValues("snapin_solutions", "snp_soln_dialogHeight", "450px")[0];
      var dlgWidth = $ss.agentcore.dal.config.GetValues("snapin_solutions", "snp_soln_dialogWidth", "600px")[0];
      var dlgResizable = $ss.agentcore.dal.config.GetValues("snapin_solutions", "snp_soln_dialogResizable", "yes")[0];

      var dialogParams = 'resizable:'+ dlgResizable + ';' +
      'status:no' +
      ';' +
      'help:no' +
      ';' +
      'dialogHeight:' + dlgHeight +
      ';' +
      'dialogWidth:' + dlgWidth +
      ';';
      
      data = {};
      var src = _GetViewPage(sContentType,sContentGuid,sContentVersion);
       var message = {'JSClassNameKey':'File','JSOperationNameKey':'GetNativePath', 'JSArgumentsKey':[src], 'JSISSyncMethodKey' : '1'}
            var result = _jsBridge.execute(message);
            var parsedJSONObject = JSON.parse(result);
            
      data.src =  parsedJSONObject.Data;
      data.shell = window;
      data.title = $ss.agentcore.dal.content.GetContentProperty(sContentType, sContentGuid, sContentVersion, "scc_title");
      data.type = sContentType;
      data.guid = sContentGuid;
      data.version = sContentVersion;
      
      //display the modal box
      try {
        var message = {'JSClassNameKey':'File','JSOperationNameKey':'GetNativePath', 'JSArgumentsKey':[viewerhtml], 'JSISSyncMethodKey' : '1'}
             result = _jsBridge.execute(message);
             parsedJSONObject = JSON.parse(result);
            viewerhtml = parsedJSONObject.Data;
          //  window.dialogArguments = data;

            if (bMac) {
            window.open(viewerhtml);
            }else {
        window.showModalDialog(viewerhtml, data, dialogParams);
      } 
      } 
      catch (ex) {      
      g_Logger.info('excep = ' + ex);     
      }      
    },    
    
    showDialog: function(url, arg, opt) {
        url = url || ''; //URL of a dialog
        arg = arg || null; //arguments to a dialog
        opt = opt ;//|| 'dialogWidth:300px;dialogHeight:200px'; //options: dialogTop;dialogLeft;dialogWidth;dialogHeight or CSS styles
        // var caller = showModalDialog.caller.toString();
        var dialog = document.body.appendChild(document.createElement('DIALOG'));
        dialog.setAttribute('style', opt.replace(/dialog/gi, ''));
        dialog.innerHTML = '<a href="#" id="dialog-close" style="position: absolute; top: 0; right: 4px; font-size: 20px; color: #000; text-decoration: none; outline: none;">&times;</a><iframe id="dialog-body" src="' + url + '" style="border: 0; width: 100%; height: 100%;"></iframe>';
        document.getElementById('dialog-body').contentWindow.dialogArguments = arg;
        document.getElementById('dialog-close').addEventListener('click', function(e) {
            e.preventDefault();
            dialog.close();
        });
        dialog.showModal();
    },
   WriteToLog: function(srType, srFLTno, srCTX, content_guid, content_version, pretest_result, main_result, posttest_result, scan_time, tune_time, rescan_time) {
      var entryCTSS = {
          'content_guid': content_guid,
          'content_version': content_version,
          'pretest_result': pretest_result,
          'main_result': main_result,
          'posttest_result': posttest_result,
          'scan_time': scan_time,
          'tune_time': tune_time,
          'rescan_time': rescan_time
      }
      if (srType) {
          entryCTSS["sr_type"] = srType;
          entryCTSS["sr_fltno"] = srFLTno;
      }
      if (srCTX) {
          entryCTSS["sr_ctx"] = srCTX;
      }
      var sDebugMsg = " ";
      for (var fld in this.report) {
          sDebugMsg += fld + ":" + this.report[fld] + "; ";
          entryCTSS[fld] = this.report[fld];
      }

      if ($ss.snapin.solutions.helper.SR_systuneUsage(entryCTSS) === true)
          return sDebugMsg;
      else
          return "error in logging";
  },
    ConvertSolutionINXMLToHtml: function(event_in){
    
      var xmlDoc = $ss.agentcore.dal.xml.LoadXMLFromString(event_in)
      var retHtml = '<table "cellpadding="4" cellspacing="0" width="95%" border="0">';
      
      var rootDoc = xmlDoc.selectSingleNode("input");
      var rootElem = rootDoc.getElementsByTagName("param");
      var count, child, type, name, desc, def;
      
      for (count = 0; count < rootElem.length; count++) {
        child = rootElem.item(count);
        type = child.getAttribute("type");
        name = child.getAttribute("name");
        desc = child.getAttribute("prompt");
        if (desc === null) {
          desc = name;
        }
        defaultvalue = child.getAttribute("default");
        if (defaultvalue === null) {
          defaultvalue = "";
        }
        switch (type) {
          case "Text":
            retHtml = retHtml + _GetTextInput(desc, name, defaultvalue);
            break;
            
          case "Password":
            retHtml = retHtml + _GetPassword(desc, name);
            break;
            
          case "DropDown":
            var fieldElem = child.getElementsByTagName("field");
            var innerCount, innerChild;
            var fieldDisp = new Array();
            var fieldVal = new Array();
            
            for (innerCount = 0; innerCount < fieldElem.length; innerCount++) {
              innerChild = fieldElem.item(innerCount);
              fieldDisp[innerCount] = innerChild.getAttribute("display");
              fieldVal[innerCount] = innerChild.getAttribute("value");
            }
            
            retHtml = retHtml + _GetDropDown(desc, name, defaultvalue, fieldDisp, fieldVal);
            break;
            
          case "CheckBox":
            var fieldElem = child.getElementsByTagName("field");
            var innerCount, innerChild;
            
            retHtml = retHtml + _GetCheckInput(desc, name, defaultvalue);
            break;
            
          case "RadioButton":
            var fieldElem = child.getElementsByTagName("field");
            var innerCount, innerChild;
            var fieldDisp = new Array();
            var fieldVal = new Array();
            
            for (innerCount = 0; innerCount < fieldElem.length; innerCount++) {
              innerChild = fieldElem.item(innerCount);
              fieldDisp[innerCount] = innerChild.getAttribute("display");
              fieldVal[innerCount] = innerChild.getAttribute("value");
            }
            
            retHtml = retHtml + _GetRadioInput(desc, name, defaultvalue, fieldDisp, fieldVal);
            break;
        }
      }
      
      return retHtml + "</table>";
    },
    
    GetFAQDetails: function(faq_cid){
      var faq_types = $ss.agentcore.dal.config.GetValues("snapin_solutions", "supportedFAQCTypes", "");
      
      var content = $ss.agentcore.dal.content.GetContentByCid(faq_types,faq_cid,true);
      if( content ){
        var oDetails = {};
        oDetails.title = content.title;
        oDetails.src = _GetViewPage(content.ctype, faq_cid, content.version);
        return oDetails;
      }
      
      return null;
    },

    HideExecDisplay: function () {
      $("#snapin_solutions .supportActExec").hide();
    },

    DispExecSuccess: function (sToDisp) {
      try {
        var sSuccess = $ss.agentcore.utils.LocalXLate("snapin_solutions", "SA_EXE_SUCCESS");
        if (sToDisp) sSuccess = sToDisp;
        $("#snapin_solutions .supportActExecSucess").find("strong")[0].innerHTML = sSuccess;
        $("#snapin_solutions .supportActExecSucess").show();
      } catch (e) {}
    },

    DispExecFail: function (sToDisp) {
      try {
        var sFail = $ss.agentcore.utils.LocalXLate("snapin_solutions", "SA_EXE_FAIL");
        if (sToDisp) sFail = sToDisp;
        $("#snapin_solutions .supportActExecFail").find("strong")[0].innerHTML = sFail;
        $("#snapin_solutions .supportActExecFail").show();
      } catch (e) {}
    },

    DispNotApplicable: function (sToDisp) {
      var sNA = $ss.agentcore.utils.LocalXLate("snapin_solutions", "SA_NONEED");
      if (sToDisp) sNA = sToDisp;
      $("#snapin_solutions .supportActExecNotApplicable").find("strong")[0].innerHTML = sNA;
      $("#snapin_solutions .supportActExecNotApplicable").show();
    },

SR_systuneUsage: function(entryCTSSUsage)
{  

  var SRfields=['content_guid', 'content_version', 'pretest_result', 'main_result',  'posttest_result',   
              'float1', 'float2' ,  'float3' ,  'sr_fltno' , 'float5' , 'float6' ,
              'string1' , 'string2' , 'sr_ctx' ,  'sr_type' ,
              'scan_time' ,  'tune_time' ,  'rescan_time'
             ]; // order of fields is Important!
  var aMsg = [];

  for (i=0; i<SRfields.length ; i++)
  {
    entryCTSSUsage[SRfields[i]] = _self.SR_CleanLoggingField(entryCTSSUsage[SRfields[i]]);
    aMsg.push(entryCTSSUsage[SRfields[i]]);
  }
  var msg = aMsg.join("__");
  var ret = _self.SR_rl_LogMsg("SYSTUNE_USAGE", msg);
  return ret;
},

SR_CleanLoggingField: function(sData) {
  g_Logger.info("sstk_rpt_CleanLoggingField", sData, true);
  var sReturn = sData;
  try {
    if (typeof(sReturn) != "string") {
      sReturn = new String(sReturn).toString();
      sReturn= (sReturn === 'undefined' || sReturn === 'null')?"":sReturn;
    }
    sReturn = sReturn.replace(g_rpt_pvt_rxLineBreaks, "");
    sReturn = sReturn.replace(g_rpt_pvt_rxBadChars, "");
    sReturn = sReturn.replace(/__/g, "_");
  } 
  catch (e) {
    sReturn = "";
  } 
  g_Logger.info("sstk_rpt_CleanLoggingField","Returning" + sReturn, true);
  return sReturn;
},

SR_rl_LogMsg: function(type, msg) //Log type is ("CONTENT" )
{
  msg = type + '__' + msg;
  var ret = true;
  
  if(g_ss_rl_pvt_bCacheLogging)
  {
    // if /cachelog is specified on command line, then cache log entries in memory
    g_ss_rl_pvt_logCache[g_ss_rl_pvt_logCache.length] = msg;
  }
  else
  {
    // otherwise log the message right away
    ret = _self.SR_LogMsg(msg);
  }
  
  return ret;
},

SR_LogMsg: function(sMsg)
{
  try {
        if(_self.SR_IsUsingBCont()) {
            $ss.agentcore.utils.activex.GetObjInstance().LogMsg(sMsg);
            return true;
        }
  } catch (err) {
    g_Logger.error("SR_LogMsg()", err.message + " -- sMsg: " + sMsg);
    return false;
  }
},

SR_IsUsingBCont: function()
{
  try {
    var oScope = _self.SR_GetTopScope();
    if(g_con_bUsingBCONT == null) {
      if(oScope.g_con_bUsingBCONT == null) {
        oScope.g_con_bUsingBCONT = (oScope.external != null && oScope.external.ModuleVersion != null);
      } else {
        g_con_bUsingBCONT = oScope.g_con_bUsingBCONT; //make a local copy
      }
    }
  } catch (ex) {
    g_Logger.error("SR_IsUsingBCont()", ex.message);
  }
  return g_con_bUsingBCONT;
},

SR_GetTopScope: function()
{
  if (g_con_ScopeCache)
    return g_con_ScopeCache;
  
  if (typeof(shellwindow) != 'undefined')
    return (g_con_ScopeCache = shellwindow);
  
  if (top !== self)
    return (g_con_ScopeCache = top);
  
  return (g_con_ScopeCache = self);
},
    //To get the latest version of the content
    LocateLatestVersion: function (guid, sArgContentTypeToUse) {
      var rootFolder = _file.GetFolder($ss.agentcore.dal.config.GetContextValue("SdcContext:DirUserServer") + "data\\");
      var maxVer = -1;
      var actionFolder = null;
      
      if (g_snp_SnapinName == undefined) { var g_snp_SnapinName = ''; }  // This occurs if scripted action is used by shell
  
      var folder = _file.GetFolder(_file.BuildPath(rootFolder.Path, sArgContentTypeToUse));
      for (var oInnerEnum = new Enumerator(folder.SubFolders); !oInnerEnum.atEnd(); oInnerEnum.moveNext())
      {
        var contentFolder = oInnerEnum.item();
        if (contentFolder.Name.indexOf(guid) == 0) {
          var version = contentFolder.Name.substring(guid.length+1);
          if (parseInt(version) > maxVer) {
            maxVer = parseInt(version);
            actionFolder = contentFolder.Path;
          }
        }
        contentFolder = null;
      }    
      folder = null;
      rootFolder = null;
      contentFolders = null;
  
      return maxVer;
    },
    
    GetLocalizedFolderName : function(folderID,sDefaultFolderName){
      var sLocalizedFname = $ss.agentcore.utils.LocalXLate("snapin_solutions",folderID);
      if(sLocalizedFname !== folderID){
        return sLocalizedFname; 
      }
      return sDefaultFolderName;
    }
  });
  
  function _GetTextInput(display, name, defaultvalue){
    return "<tr><td width='20%'> " + display + ":</td><td width='80%'><input class=textbox type=text name=\"text.client." + name + "\" value=\"" + defaultvalue + "\"></td></tr>";
  }
  
  function _GetPassword(display, name){
    return "<tr><td width='20%'> " + display + ":</td><td width='80%'><input class=textbox type=password name=\"text.client." + name + "\" value=\"\"></td></tr>";
  }
  
  function _GetDropDown(display, name, defaultvalue, fieldDisp, fieldVal){
    var ret;
    ret = "<tr><td width='20%'>" + display + ":</td><td width='80%'><select  class=dropdown name=\"dropdown.client." + name + "\">"
    
    var count;
    for (count = 0; count < fieldDisp.length; count++) {
      if (defaultvalue === fieldVal[count]) {
        ret = ret + "<option value=" + fieldVal[count] + " SELECTED >" + fieldDisp[count];
      }
      else {
        ret = ret + "<option value=" + fieldVal[count] + ">" + fieldDisp[count];
      }
    }
    
    return ret + "</select></td></tr>";
  }
  
  function _GetCheckInput(display, name, defaultvalue){
    var ret;
    if (defaultvalue != null) {
      if (defaultvalue.toUpperCase() === "TRUE") {
        ret = "<tr><td width='80%'><input type=CHECKBOX name=\"checkbox.client." + name + "\" CHECKED>" + display
      }
      else {
        ret = "<tr><td width='80%'><input type=CHECKBOX name=\"checkbox.client." + name + "\">" + display
      }
    }
    else {
      ret = "<tr><td width='80%'><input type=CHECKBOX name=\"checkbox.client." + name + "\">" + display
    }
    return ret + "</td></tr>";
  }
  
  function _GetRadioInput(display, name, defaultvalue, fieldDisp, fieldVal){
    var ret;
    
    ret = "<tr><td width='20%'>" + display + ":</td><td width='80%'>";
    
    var count;
    for (count = 0; count < fieldDisp.length; count++) {
      if (fieldVal[count] === defaultvalue) {
        ret = ret + "<input type=radio name=\"radio.client." + name + "\" value=\"" + fieldVal[count] + "\" CHECKED>" + fieldDisp[count] + "<br>";
      }
      else {
        ret = ret + "<input type=radio name=\"radio.client." + name + "\" value=\"" + fieldVal[count] + "\">" + fieldDisp[count] + "<br>";
      }
    }
    
    return ret + "</td></tr>";
  }
  
  function _GetViewPage(sContentType, sContentGuid,sContentVersion ){
    var viewPage = $ss.GetAttribute('startingpoint') +
      sContentType +
      '\\' +
      sContentGuid +
      '.' +
      sContentVersion +
      '\\view\\view.html';
      
      //check whether the page exists or not
      if( $ss.agentcore.dal.file.FileExists(viewPage) ){
        return viewPage;
      }
      
      
//check for Answer.html
      viewPage = $ss.GetAttribute('startingpoint') +
      sContentType +
      '\\' +
      sContentGuid +
      '.' +
      sContentVersion +
      '\\Answer.html';
      
      if( $ss.agentcore.dal.file.FileExists(viewPage) ){
        return viewPage;
      }

      
      return null;
  }
  
  var _content = $ss.agentcore.dal.content;
  var _xml = $ss.agentcore.dal.xml;
  var _self = $ss.snapin.solutions.helper;
  var _file =$ss.agentcore.dal.file;
  var _jsBridge = window.JSBridge;
  g_Logger = $ss.agentcore.log.GetDefaultLogger("$ss.snapin.solutions.helper"); 
})();
  var data = {};
  var bMac = $ss.agentcore.utils.IsRunningInMacMachine();

