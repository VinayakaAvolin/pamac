$ss.snapin = $ss.snapin ||
{};
$ss.snapin.solutions = $ss.snapin.solutions ||
{};
$ss.snapin.solutions.helper = $ss.snapin.solutions.helper ||
{};

SnapinSolutionsController = SnapinBaseController.extend('snapin_solutions', {
  hasbeenInitalized: false,
  initialize: function () {
    if (!this.hasbeenInitalized) {
      $ss.agentcore.events.Subscribe("SNAPIN_DO_CLEAN", function () {
        return SnapinSolutionsController.AbortRunningSupporAction();
      });
      $ss.agentcore.events.Subscribe("BROADCAST_ON_BCONTCLOSE", function () {
        if (SnapinSolutionsController.supportActionIntialized) {
          //alert("here");
          SnapinSolutionsController.AbortRunningSupporAction();
          setTimeout("window.external.Close()", 200);

        }
      });
      this.hasbeenInitalized = true;
    }
  },
  snapinToLocation: null,
  arrFAQCTypes: [],
  arrFAQExtTypes: [],
  arrOneClickSolnCTypes: [],
  arrViewableContentTypes: [],
  arrExcludeOneClickGuids: [],
  arrExcludeOneClickCategories: [],
  arrValidFolders: [],
  arrInValidFolders: [],
  arrExcludeFolders: [],
  oFolderBreadCrumbs: {},
  helper: $ss.snapin.solutions.helper,
  logger: $ss.agentcore.log.GetDefaultLogger("solutions_controller"),
  content: $ss.agentcore.dal.content,
  config: $ss.agentcore.dal.config,
  scriptControl: null,
  SAWaitingForUserInput: false,
  surveyData: {},
  supportActionIntialized: false,
  SUCCESS_CODE: "0",
  FAIL_CODE: "1",
  ABORT_CODE: "999",
  UNDEFINED_CODE: "-1",
ASK_FIX_REQUIRED_CODE: "0",
  sSource: "",
  getParams: "",
    g_SA_FIX_COMPLETED_RESCAN_NOT_REQUIRED: "-2",
    g_SA_FAILED: "-1",
    g_SA_FIX_NOT_POSSIBLE: "2",
    g_UI_SHOW_FIX: "1",
    scan_time: "",
    fix_time: "",
    rescan_time: "",
    scan_result: "",
    fix_result: "",
    rescan_result: "",
    guid: "",
    version: "",
    SurveyRating: "",
    SurveyProblemSolvedYN: "",
    //Survey Feature Starts
    dictSurveySubmitButtonClicked: "",
    //Survey Feature End
    LibType: "",

  AbortRunningSupporAction: function () {
    if (this.supportActionIntialized) {
      try {
        if (this.scriptControl && this.scriptControl.srCtl) {
          this.scriptControl.srCtl.Abort("999");
          this.scriptControl.srCtl = null;
          this.scriptControl = null;
        }
        this.supportActionIntialized = false;
      } catch (ex) {

      }
    }
  },

  GetFAQTypesForExternalView: function () {
    if ((this.arrFAQExtTypes) && (0 < this.arrFAQExtTypes.length)) {
      return this.arrFAQExtTypes;
    }
    this.arrFAQExtTypes = this.config.GetValues("snapin_solutions", "externalviewFAQ", "");
    return this.arrFAQExtTypes;
  },

  GetFAQCTypes: function () {
    if (this.arrFAQCTypes && this.arrFAQCTypes.length > 0) {
      return this.arrFAQCTypes;
    }
    this.arrFAQCTypes = this.config.GetValues("snapin_solutions", "supportedFAQCTypes", "");
    return this.arrFAQCTypes;
  },

  GetOneClickSolnCTypes: function () {
    if (this.arrOneClickSolnCTypes && this.arrOneClickSolnCTypes.length > 0) {
      return this.arrOneClickSolnCTypes;
    }
    this.arrOneClickSolnCTypes = this.config.GetValues("snapin_solutions", "supportedOneClickSolnCTypes", "");
    return this.arrOneClickSolnCTypes;
  },

  GetAllViewableContentTypes: function () {
    if (this.arrViewableContentTypes && this.arrViewableContentTypes.length > 0) {
      return this.arrViewableContentTypes;
    }
    this.arrViewableContentTypes = this.arrViewableContentTypes.concat(this.GetFAQCTypes());
    this.arrViewableContentTypes = this.arrViewableContentTypes.concat(this.GetOneClickSolnCTypes());
    return this.arrViewableContentTypes;
  },

  GetExcludedOneClickGuids: function () {
    if (this.arrExcludeOneClickGuids && this.arrExcludeOneClickGuids.length > 0) {
      return this.arrExcludeOneClickGuids;
    }

    this.arrExcludeOneClickGuids = this.config.GetValues("snapin_solutions", "snp_soln_excludeSolution", "");

    return this.arrExcludeOneClickGuids;
  },

  GetExcludedOneClickCategories: function () {
    if (this.arrExcludeOneClickCategories && this.arrExcludeOneClickCategories.length > 0) {
      return this.arrExcludeOneClickCategories;
    }

    this.arrExcludeOneClickCategories = this.config.GetValues("snapin_solutions", "snp_soln_excludeSolutionCategories", "");

    return this.arrExcludeOneClickCategories;
  },

  GetExcludedFolders: function () {
    if (this.arrExcludeFolders && this.arrExcludeFolders.length > 0) {
      return this.arrExcludeFolders;
    }

    this.arrExcludeFolders = this.config.GetValues("snapin_solutions", "hidden_folder_guid", "");

    return this.arrExcludeFolders;
  },

  GetFolderBreadCrumb: function (oCurrentFolder, sRootFolder) {
    if (!oCurrentFolder)
      return "";

    if (!oCurrentFolder.fid)
      return "";

    if (this.oFolderBreadCrumbs && this.oFolderBreadCrumbs[oCurrentFolder.fid]) {
      return this.oFolderBreadCrumbs[oCurrentFolder.fid];
    }

    // build the folder bread crumb
    var arrFolderBreadCrumb = [];
    arrFolderBreadCrumb[arrFolderBreadCrumb.length] = oCurrentFolder;
    var oFolder = oCurrentFolder;
    while (oFolder.parent) {
      if (oFolder.fid === sRootFolder) break;
      oFolder = this.content.GetFolderDetails(oFolder.parent);
      arrFolderBreadCrumb[arrFolderBreadCrumb.length] = oFolder;
    }
    if (arrFolderBreadCrumb.length > 0) {
      this.oFolderBreadCrumbs[oCurrentFolder.fid] = arrFolderBreadCrumb;
    }

    return arrFolderBreadCrumb;
  }
}, {
  index: function (params) {
    this.Class.initialize();
    this.Class.AbortRunningSupporAction();
    try {
      this.Class.getParams = params.inInstanceParam;
      var oCmdLine = $ss.agentcore.utils.GetObjectFromCmdLine();
      
      //Survey Feature Starts            
      //Get from registry if Survey is enabled or not
      var provider = $ss.agentcore.dal.config.GetProviderID();
      var user = $ss.agentcore.dal.config.ExpandSysMacro("%USER%");
      var prodName = $ss.agentcore.dal.config.ParseMacros("%PRODUCT%");
      var regPath = "Software\\SupportSoft\\ProviderList\\" + provider + "\\" + prodName + "\\users\\" + user + "\\ss_config\\global\\";
      this.Class._enableSurvey = $ss.agentcore.dal.registry.GetRegValue("HKCU", regPath, "Enable_Survey");
      //Survey Feature Ends
      //Reading from clientui_config.xml file if value doesnot exist in registry
      if(this.Class._enableSurvey.length === 0){
        this.Class._enableSurvey = $ss.agentcore.dal.config.GetConfigValue("global", "show_survey", "false");
      }
      if (oCmdLine.autoexecute) {
        this.Class.sSource = oCmdLine.source;
        if(typeof(this.Class.sSource) == "undefined")
          this.Class.sSource = "";
        var sJobGuid = oCmdLine.sr_guid;
        if (!(sJobGuid == true || sJobGuid == undefined)) {
          this.scriptactionJob(sJobGuid);
        }
      }
      else {
        var reqArgs, instParams, sTemplate;
        reqArgs = params.requestArgs ||{};
        instParams = params.inInstanceParam ||{};
        sTemplate = reqArgs.snpsoln_template || instParams.snpsoln_template || null;

        this.Class.snapinToLocation = params.toLocation;

        switch (sTemplate) {
          case "AccordionViewTemplate.html":
            this.RenderAccordionView(params);
            break;
          case "SolutionListTemplate.html":
            this.RenderSolutionListView(params);
            break;
          case "SolutionView":
            this.RenderSolutionView(params);
            break;
          case "FolderViewTemplate.html":
          default:
          // assume default is folder view if not specified
            this.RenderFolderView(params);
            break;
        }
      }
    }
    catch (ex){ }
  },

  scriptactionJob: function (sJobGuid) {
    var sJb_Guids = sJobGuid.split(";");
    var sGuid, sVersion, sTitle, sType;
    sType = "sprt_actionlight";
    var that = this;
    $.each(sJb_Guids,function(i,v){
      sGuid = v;
      sVersion = $ss.snapin.solutions.helper.LocateLatestVersion(sGuid, sType);
      sTitle = $ss.agentcore.dal.content.GetContentProperty(sType, sGuid, sVersion, "scc_title");
      that.snp_script_action_run(sGuid, sTitle, sVersion);
    });
    $ss.agentcore.utils.CloseBcont();
    that = null;
  },

  RenderAccordionView: function (params) {
    var contentData = {};
    var reqArgs = params.requestArgs ||{};
    var instParams = params.inInstanceParam ||{};
    var sTemplate = reqArgs.snpsoln_template || instParams.snpsoln_template || null;
    var sCategories = reqArgs.snpsoln_categories || instParams.snpsoln_categories || null;
    var sBackToUrl = reqArgs.snpsoln_doneurl || instParams.snpsoln_doneurl || "/sa/faq";
    // get the required parameters to populate Accordion View
    contentData.sBackToUrl = sBackToUrl;
    if (sCategories) {
      contentData.arrCategories = sCategories.split(",");
    }
    else {
      contentData.arrCategories = [];
    }
    contentData.arrFAQCTypes = this.Class.GetFAQCTypes();
    contentData.arrOneClickSolnCTypes = this.Class.GetOneClickSolnCTypes();
    contentData.nFAQsToDisplay = this.Class.config.GetConfigValue("snapin_solutions", "nFAQsToDisplay", 0);
    contentData.nOneClickSolnsToDisplay = this.Class.config.GetConfigValue("snapin_solutions", "nOneClickSolnsToDisplay", 0);
    contentData.arrFAQContents = this.Class.content.GetContentsByAnyCategory(contentData.arrFAQCTypes, contentData.arrCategories, true);
    contentData.arrOneClickSolns = this.Class.content.GetContentsByAnyCategory(contentData.arrOneClickSolnCTypes, contentData.arrCategories, true);
    contentData.arrFAQExtTypes = this.Class.GetFAQTypesForExternalView();
    contentData.isMoreLinkSupported = this.Class.config.GetConfigValue("snapin_solutions", "displayMoreLinkInAccordianView", 0);
    //exclude the one click solutions if its guid matches with those present in config.xml
    contentData.arrOneClickSolns = this.ExcludeOnceClickSolnSpecfiedInConfig(contentData.arrOneClickSolns);
    //exclude faqs which dont have view page
    contentData.arrFAQContents = this.ExcludeNonViewableFAQs(contentData.arrFAQContents);
    this.renderSnapinView("snapin_solutions", params.toLocation, "\\views\\" + sTemplate, {
      contentData: contentData
    });
    // set the accordion view
    $("#snapin_solutions #snapin_solutions_list").accordion({
      active: 'div.selected',
      header: 'div.AccordionPanelTab'
    });

  },

  RenderSolutionListView: function (params) {

    var instParams = params.inInstanceParam ||{};
    var reqArgs = params.requestArgs ||{};
    var sContentTypes = reqArgs.snpsolution_ctype || instParams.snpsolution_ctype || null;
    var sTemplate = reqArgs.snpsoln_template || instParams.snpsoln_template || null;
    var sCategories = reqArgs.snpsoln_categories || instParams.snpsoln_categories || null;
    var sBackToUrl = reqArgs.snpsoln_doneurl || instParams.snpsoln_doneurl || "/sa/faq";

    var contentData = {};
    contentData.sBackToUrl = sBackToUrl;


    if (sCategories) {
      contentData.arrCategories = sCategories.split(",");
    }
    else {
      contentData.arrCategories = [];
    }

    if (sContentTypes) {
      contentData.arrContentTypes = sContentTypes.split(",");
    }
    else {
      contentData.arrContentTypes = [];
    }

    contentData.arrContents = this.Class.content.GetContentsByAllCategories(contentData.arrContentTypes, contentData.arrCategories, true);

    this.renderSnapinView("snapin_solutions", params.toLocation, "\\views\\" + sTemplate, {
      contentData: contentData
    });
  },

  RenderFolderView: function (params) {

    var contentData = {};
    var instParams = params.inInstanceParam ||
    {};
    var reqArgs = params.requestArgs ||
    {};

    var sTemplate = reqArgs.snpsoln_template || instParams.snpsoln_template || null;
    contentData.sFolderGuid = reqArgs.snpsoln_fid || instParams.snpsoln_fid || null;
    var sBackToUrl = reqArgs.snpsoln_doneurl || instParams.snpsoln_doneurl || null;
    var sRootFolder = instParams.snpsoln_fid || "sprt_root_folder";

    if (!sTemplate) {
      sTemplate = "FolderViewTemplate.html";
    }

    if (!sBackToUrl) {
      sBackToUrl = "/sa/soln";
    }

    contentData.sBackToUrl = sBackToUrl;
    contentData.oFolder = this.Class.content.GetFolderDetails(contentData.sFolderGuid);
    contentData.arrContents = this.Class.content.GetContentsByAnyFolder(this.Class.GetAllViewableContentTypes(), [contentData.sFolderGuid]);
    contentData.arrFolderBreadCrumb = this.Class.GetFolderBreadCrumb(contentData.oFolder, sRootFolder);
    contentData.arrFAQExtTypes = this.Class.GetFAQTypesForExternalView();

    contentData = this.ExcludeNonViewableContents(contentData);

    this.renderSnapinView("snapin_solutions", params.toLocation, "\\views\\" + sTemplate, {
      contentData: contentData
    });
  },

  RenderSolutionView: function (params) {
    var instParams = params.inInstanceParam ||{};
    var reqArgs = params.requestArgs ||{};
    var contentType = reqArgs.snpsolution_ctype || instParams.snpsolution_ctype || null;
    var contentVersion = reqArgs.snpsolution_version || instParams.snpsolution_version || null;
    var contentGUID = reqArgs.snpsolution_cid || instParams.snpsolution_cid || null;


    if (!contentGUID || !contentType || !contentVersion) {
      //do the error condtion display empty page with information no GUID available
      return;
    }
    switch (contentType) {
      case "sprt_actionlight":
        this.RenderSupportAction(params, contentType, contentGUID, contentVersion);
        break;
      default:
        $ss.snapin.solutions.helper.RenderSolutionView(contentType, contentGUID, contentVersion);
        break;
    }
  },

  RenderSupportAction: function (params, type, guid, version) {
    var contentDetail = {};
    var instParams = params.inInstanceParam ||{};
    var reqArgs = params.requestArgs ||{};

    var sTemplate = reqArgs.snpsoln_template || instParams.snpsoln_template || null;
    contentDetail.sFolderGuid = reqArgs.snpsoln_fid || instParams.snpsoln_fid || null;
    var sBackToUrl = reqArgs.snpsoln_doneurl || instParams.snpsoln_doneurl || null;
    var sRootFolder = instParams.snpsoln_fid || "sprt_root_folder";

    contentDetail.oFolder = this.Class.content.GetFolderDetails(contentDetail.sFolderGuid);
    contentDetail.arrContents = this.Class.content.GetContentsByAnyFolder(this.Class.GetAllViewableContentTypes(), [contentDetail.sFolderGuid]);
    contentDetail.arrFolderBreadCrumb = this.Class.GetFolderBreadCrumb(contentDetail.oFolder, sRootFolder);

    contentDetail.callerSnapinname = params.callerSnapin || "";
    contentDetail.sBackToUrl = reqArgs.snpsoln_doneurl || instParams.snpsoln_doneurl || "/sa/soln";
    contentDetail.guid = guid;
    contentDetail.version = version;
    var oContentObj = $ss.agentcore.dal.content.GetContentByCid([type], guid);
    contentDetail.title = oContentObj.title;
    contentDetail.description = oContentObj.description;
    
    //Passing Execution Type to UI
    var sExecType = $ss.agentcore.dal.content.GetContentFieldValue("sprt_actionlight", contentDetail.guid, contentDetail.version, "executiontype", "sccf_field_value_char");
    contentDetail.ExecType = sExecType.toUpperCase();

    //Prepair survey data
    this.Class.surveyData.guidContent = guid;
    this.Class.surveyData.version = version;
    this.Class.surveyData.verb = "view";
    this.Class.surveyData.viewed = "";
    this.Class.surveyData.comment = contentDetail.description;
    this.Class.surveyData.data = type;
    this.Class.surveyData.result = "";
    //Survey Feature Starts
    this.Class.surveyData.title = contentDetail.title;
    //Survey Feature End

    this.renderSnapinView("snapin_solutions", params.toLocation, "\\views\\" + "OneClickSolutionView.html", {
      contentDetail: contentDetail
    });
  },

  ExcludeNonViewableContents: function (contentData) {
    contentData.oFolder.subfolders = this.ExcludeFoldersSpecifiedInConfig(contentData.oFolder.subfolders);
    contentData.arrContents = this.ExcludeOnceClickSolnSpecfiedInConfig(contentData.arrContents);
    contentData.arrContents = this.ExcludeNonViewableFAQs(contentData.arrContents);
    //check whether these subfolders internally contains some data or not
    contentData.oFolder.subfolders = this.ExcludeInvalidSubFolders(contentData.oFolder.subfolders);
    return contentData;
  },

  ExcludeInvalidSubFolders: function (arrSubFolders) {
    var bFoundInValidSubFolder = false;
    var bFoundValidSubFolder = false;
    var arrValidSubFolders = [];

    if (0 === arrSubFolders.length) {
      return arrSubFolders;
    }

    //go through the collection of subfolders
    //if they fall in exclude category then remove it
    //if they fall in include category then add it
    for (var i = 0; i < arrSubFolders.length; i++) {
      bFoundInValidSubFolder = false;
      bFoundValidSubFolder = false;
      for (var j = 0; j < this.Class.arrInValidFolders.length; j++) {
        if (arrSubFolders[i].fid === this.Class.arrInValidFolders[j]) {
          bFoundInValidSubFolder = true;
          break;
        }
      }

      if (!bFoundInValidSubFolder) {
        for (var k = 0; k < this.Class.arrValidFolders.length; k++) {
          if (arrSubFolders[i].fid === this.Class.arrValidFolders[k]) {
            bFoundValidSubFolder = true;
            arrValidSubFolders.push(arrSubFolders[i]);
            break;
          }
        }

        if (!bFoundValidSubFolder) {
          //now we need to check whether this subfolders children/granchildren chain contains some data or not
          var bReturn = this.IsValidSubFolder(arrSubFolders[i].fid);
          if (bReturn) {
            this.Class.arrValidFolders.push(arrSubFolders[i].fid);
            arrValidSubFolders.push(arrSubFolders[i]);
          }
          else {
            this.Class.arrInValidFolders.push(arrSubFolders[i].fid);
          }
        }
      }
    }
    return arrValidSubFolders;
  },

  IsValidSubFolder: function (folder_fid) {
    //get all the contents for this subfolder
    var arrContents = this.Class.content.GetContentsByAnyFolder(this.Class.GetAllViewableContentTypes(), [folder_fid]);
    if (0 < arrContents.length) {
      return true;
    }

    var arrFolders = this.Class.content.GetFolderDetails(folder_fid);
    if (0 === arrFolders.subfolders.length) {
      return false;
    }

    for (var i = 0; i < arrFolders.subfolders.length; i++) {
      if (this.IsValidSubFolder(arrFolders.subfolders[i].fid)) {
        return true;
        break;
      }
    }

    return false;
  },

  ExcludeFoldersSpecifiedInConfig: function (arrFolderGuids) {
    var arrExcludeFolders = this.Class.GetExcludedFolders();
    var folderMatched = false;
    var validIndex = [];
    var validFolders = [];

    if ((0 === arrFolderGuids.length) || (0 === arrExcludeFolders.length)) {
      return arrFolderGuids;
    }

    for (var i = 0; i < arrFolderGuids.length; i++) {
      folderMatched = false;
      for (var j = 0; j < arrExcludeFolders.length; j++) {
        if (arrFolderGuids[i].fid === arrExcludeFolders[j]) {
          folderMatched = true;
          break;
        }
      }

      if (!folderMatched) {
        validIndex.push(i);
      }
    }

    for (var k = 0; k < validIndex.length; k++) {
      validFolders.push(arrFolderGuids[validIndex[k]]);
    }

    return validFolders;
  },

  ExcludeNonViewableFAQs: function (arrFAQs) {
    var includeGuids = [];
    for(var i = 0; i < arrFAQs.length; i++) {
      if(arrFAQs[i].ctype !== "sprt_actionlight") {
        if($ss.snapin.solutions.helper.GetContentPath(arrFAQs[i])) {
          includeGuids.push(arrFAQs[i]);
        }
      }
      else{
        includeGuids.push(arrFAQs[i]);
      }
    }
    return includeGuids;
  },

  ExcludeOnceClickSolnSpecfiedInConfig: function (arrOneClickSolns) {
    var arrExcludeCategories = this.Class.GetExcludedOneClickCategories();
    var arrExcludeGuids = this.Class.GetExcludedOneClickGuids();
    var categoriesMatched = false;
    var guidsMatched = false;
    var validIndex = [];
    var includeGuids = [];

    if (0 === arrOneClickSolns.length) {
      return arrOneClickSolns;
    }

    for (var i = 0; i < arrOneClickSolns.length; i++) {
      if (arrOneClickSolns[i].ctype === "sprt_actionlight") {
        categoriesMatched = false;
        for (var j = 0; j < arrOneClickSolns[i].category.length; j++) {
          if (categoriesMatched) {
            break;
          }
          for (var k = 0; k < arrExcludeCategories.length; k++) {
            if (arrOneClickSolns[i].category[j] === arrExcludeCategories[k]) {
              categoriesMatched = true;
              break;
            }
          }
        }

        if (!categoriesMatched) {
          guidsMatched = false;
          for (var l = 0; l < arrExcludeGuids.length; l++) {
            if (arrOneClickSolns[i].cid === arrExcludeGuids[l]) {
              guidsMatched = true;
              break;
            }
          }

          if (!guidsMatched) {
            validIndex.push(i);
          }
        }
      }
      else {
        validIndex.push(i);
      }
    }

    if (0 === validIndex.length) {
      return includeGuids;
    }

    for (var m = 0; m < validIndex.length; m++) {
      includeGuids.push(arrOneClickSolns[validIndex[m]]);
    }

    return includeGuids;

  },

    //Survey Feature Starts
  LogStatus: function () {
    try {
      if (this.Class.surveyData.solved == -1) { this.Class.surveyData.solved = ""; }
      var oLogEvnt = $ss.agentcore.reporting.Reports.CreateLogEntry(this.Class.surveyData.guidContent, this.Class.surveyData.version, this.Class.surveyData.verb, this.Class.surveyData.viewed, this.Class.surveyData.SurveyProblemSolvedYN, this.Class.surveyData.comment, this.Class.surveyData.data, this.Class.surveyData.SurveyRating, this.Class.surveyData.result);
      $ss.agentcore.reporting.Reports.Log(oLogEvnt, "snapin_solutions");
    }
    catch (ex) { }
  },
  //Survey Feature End

  resetsurveyDataProp: function(){
    this.Class.surveyData.SurveyProblemSolvedYN = "0";
    this.Class.surveyData.SurveyRating = "";
  },

  "#scriptActionRun click": function (params) {
    var sGuid = $(params.element).attr('guid');
    var sTitle = $(params.element).attr('title');
    var sVersion = $(params.element).attr('version');
    this.snp_script_action_run(sGuid, sTitle, sVersion);
  },

  "#scriptActionFix click": function (params) {
    var sGuid = $(params.element).attr('guid');
    var sTitle = $(params.element).attr('title');
    var sVersion = $(params.element).attr('version');
    this.snp_script_action_run(sGuid, sTitle, sVersion, "fix");
  },

  //Function to get the method based on execution type & bttn action
  getScriptControlMethod : function (action, execType){
    if (execType === "AUTO_FIX") return "EvaluateTestAndRun";
    if (execType === "SCAN_AND_FIX")  {
      return ((typeof(action)) !== "undefined" && action === "fix") ? "Evaluate" : "EvaluateTest";
    }
    return "EvaluateTestAndRun";
  },

  //Function to handle scan related activities
  Scan: function(cabFileName){
    try{
      var retValue;
      var startTime = new Date();
      retValue = this.Class.scriptControl.srCtl.EvaluateTest(cabFileName);            
      var endTime = new Date();
      this.Class.scan_time = endTime.valueOf() - startTime.valueOf();
      this.Class.scan_result = retValue;
      return retValue;
    }
    catch (ex) { }
  },
  
  //Function to handle fix related activities
  Fix: function(cabFileName){
    try{
      var retValue;
      var startTime = new Date();
      retValue = this.Class.scriptControl.srCtl.Evaluate(cabFileName);
      var endTime = new Date();
      this.Class.fix_time = endTime.valueOf() - startTime.valueOf();
      this.Class.fix_result = retValue;
      return retValue;
    }
    catch (ex) { }
  },
  //Function to handle re-scan related activities
  Rescan: function(cabFileName){
    try{
      var retValue;      
      var startTime = new Date();
      retValue = this.Class.scriptControl.srCtl.EvaluateTest(cabFileName);
      var endTime = new Date();
      this.Class.rescan_time = endTime.valueOf() - startTime.valueOf();
      this.Class.rescan_result = retValue;
      return retValue;
    }
    catch (ex) { }
  },

    ScanMac: function(guid, cabFileName) {
      this.Class.startTime = new Date();
      this.Class.scriptControl.EvaluateTest(guid, cabFileName);
      
    },

    FixMac: function(guid, cabFileName) {
      this.Class.startTime = new Date();
      this.Class.scriptControl.Evaluate(guid, cabFileName);
    },

    RescanMac: function(guid, cabFileName) {
      this.Class.startTime = new Date();
      this.Class.scriptControl.EvaluateTest(guid, cabFileName);
    },
    EvaluateTestCompleted: function(retValue) {
      retValue = "" + retValue;
      var endTime = new Date();
      this.Class.rescan_time = endTime.valueOf() - this.Class.startTime.valueOf();
      this.Class.rescan_result = retValue;
      switch(this.Class.methodToExecute) {
        case "EvaluateTest":
            this.Class.surveyData.comment = "Scan:<" + retValue + ">-->";
            if (retValue === this.Class.ASK_FIX_REQUIRED_CODE)  $ss.snapin.solutions.helper.DispExecSuccess();
            else  $ss.snapin.solutions.helper.DispExecFail();
            if ((this.Class.libType != "SupportRemediation" && retValue === "0") || (this.Class.libType == "SupportRemediation" && this.Class.execType == "SCAN_AND_FIX" && retValue > 0)) {
              $("#snapin_solutions #scriptActionFix").show(); //showing fix button
              unFixedSRs = this.Class.guid;
              if (this.Class.libType == "SupportRemediation") this.Class.exitLibType = this.Class.libType + "-scan"
            }
            else {
              if (this.Class.libType == "SupportRemediation") {
                // if no fix needed, log the same
                this.Class.exitLibType = this.Class.libType + "-scan"
                //this.WriteToLog(null, null, null);
              }
            }
            //log the values in the global variable to log if the the snapin is changed or bcont is closed
            SRLogger["content_guid"] = this.Class.guid;
            SRLogger["content_version"] = this.Class.version;
            SRLogger["pretest_result"] = retValue;
            SRLogger["scan_time"] = this.Class.scan_time;
            SRLogger["scan_status"] = "SA_SCAN";
            retValueOutFromScan = retValue;
            this.AfterScriptRun(retValueOutFromScan, retValue);
        break;

        case "Evaluate":
              this.Class.surveyData.comment += "Fix:<" + retValueOutFromScan + ">";
              retValueOutFromScan = "";
              this.AfterScriptRun(retValueOutFromScan, retValue)
        break;

        case "EvaluateTestAndRun":
        if (this.Class.rescan) {
          this.Class.rescan = false;
              this.Class.surveyData.comment = "Scan:<" + retValue + ">-->";
              this.Class.surveyData.comment += "Fix:<" + retValueOutFromScan + ">";
              retValueOutFromScan = "";
              this.AfterScriptRun(retValueOutFromScan, retValue);
        }
        else if (this.Class.libType == "SupportRemediation") {
              this.Class.exitLibType = this.Class.libType + "-scan"
              if (retValue > 0) {
               this.FixMac(this.Class.guid, this.Class.cabFileName);
                this.Class.exitLibType = this.Class.libType + "-fix"
              }else {
                this.Class.surveyData.comment = "Scan:<" + retValue + ">-->";
                this.Class.surveyData.comment = "Fix:<" + retValue + ">";
                retValueOutFromScan = "";
                this.AfterScriptRun(retValueOutFromScan, retValue);
              }
              //this.WriteToLog(null, null, null);
            }
            else {
              //Not a SR, for SA do the fix if the retVal == 0
              if (retValue == "0") {
                this.FixMac(this.Class.guid, this.cabFileName);
              }else {
                this.Class.surveyData.comment = "Scan:<" + retValue + ">-->";
                this.Class.surveyData.comment = "Fix:<" + retValue + ">";
                retValueOutFromScan = "";
                this.AfterScriptRun(retValueOutFromScan, retValue);
              }
            }
        break;
      }
    },

    EvaluateCompleted: function(retValue) {
      retValue = "" + retValue;
      var endTime = new Date();
      this.Class.rescan_time = endTime.valueOf() - this.Class.startTime.valueOf();
      this.Class.rescan_result = retValue;

      switch(this.Class.methodToExecute) {
        case "Evaluate":
        var callSurvay = true;
          if (this.Class.libType == "SupportRemediation") {
              this.Class.exitLibType = this.Class.libType + "-fix"
              if (!(retValue == this.Class.g_SA_FIX_COMPLETED_RESCAN_NOT_REQUIRED || retValue == this.Class.g_SA_FAILED || retValue == this.Class.g_SA_FIX_NOT_POSSIBLE)) {
                //rescan start
                callSurvay = false;
                this.Class.rescan = true;
                 this.RescanMac(this.Class.guid, this.Class.cabFileName);
                this.Class.exitLibType = this.Class.libType + "-rescan"
              }
              //this.WriteToLog(null, null, null);
            }
            if (callSurvay) {
              this.Class.surveyData.comment += "Fix:<" + retValueOutFromScan + ">";
              retValueOutFromScan = "";
              this.AfterScriptRun(retValueOutFromScan, retValue);
          }
        break;

        case "EvaluateTestAndRun" :
          this.Class.rescan = false;
          var callSurvay = true;
          if (this.Class.libType == "SupportRemediation") {
              this.Class.exitLibType = this.Class.libType + "-fix"
              if (!(retValue == this.Class.g_SA_FIX_COMPLETED_RESCAN_NOT_REQUIRED || retValue == this.Class.g_SA_FAILED || retValue == this.Class.g_SA_FIX_NOT_POSSIBLE)) {
                //rescan start
                callSurvay = false;
                this.Class.rescan = true;
                 this.RescanMac(this.Class.guid, this.Class.cabFileName);
                this.Class.exitLibType = this.Class.libType + "-rescan"
              }
              //this.WriteToLog(null, null, null);
            }
            if (callSurvay) {
              this.Class.surveyData.comment = "Scan:<" + retValue + ">-->";
              this.Class.surveyData.comment += "Fix:<" + retValueOutFromScan + ">";
              retValueOutFromScan = "";
              this.AfterScriptRun(retValueOutFromScan, retValue)
            }
        break;
      }
    },

    AfterScriptRun: function(retValueOutFromScan, retValue) {
      var UNDEFINED_RES = "";
      this.Class.scriptControl = null;
      this.Class.supportActionIntialized = false;
      try {
        if (retValue !== UNDEFINED_RES && retValue !== this.Class.ABORT_CODE) {
          $ss.snapin.solutions.helper.HideExecDisplay();
          var oExit = $ss.agentcore.exitcodes.GetExitCodeDetails(this.Class.exitLibType, retValue);
          $ss.agentcore.dal.databag.RemoveValue("snp_soln_Curr_SA_Running_Guid");
          $ss.agentcore.dal.databag.RemoveValue("snp_soln_Curr_SA_Running_Version");
          this.Class.surveyData.result = retValue;   // Same returned result incase of SA
          if (oExit !== null) this.Class.surveyData.result = oExit.exittype; // Mapped result incase of ASK
          if (this.Class.sSource == "sprt_job" || this.Class.sSource == "sprt_trigger") {
            this.Class.surveyData.data = "Job"
            this.LogStatus();
            islogged = true;
          }
          else if (this.Class.libType === "" || this.Class.execType === "") {
            if (retValue === this.Class.SUCCESS_CODE) {
              $ss.snapin.solutions.helper.DispExecSuccess();
              islogged = true;
              if (retValueOutFromScan === "" || retValueOutFromScan === undefined)
                this.CallSurveyPage();  // Survey page will log if survey enbaled or we are logging explicitly in this method
            }
            else {
              this.LogStatus();
              islogged = true;
              $ss.snapin.solutions.helper.DispExecFail();
              if (retValue == -1) {
                this.CallSurveyPage();
              }
            }
          } else {
            if (oExit !== null) {
              if (oExit.exittype === this.Class.SUCCESS_CODE || (this.Class.libType == "SupportRemediation" && this.Class.execType == "SCAN_AND_FIX" && retValueOutFromScan > 0)) {
                $ss.snapin.solutions.helper.DispExecSuccess(oExit.desc);
                islogged = true;
                if (retValueOutFromScan === "" || retValueOutFromScan === undefined)
                  this.CallSurveyPage();  // Survey page will log if survey enbaled or we are logging explicitly in this method

                } else {
                  this.LogStatus();
                  islogged = true;
                  $ss.snapin.solutions.helper.DispExecFail(oExit.desc);
                  if (retValue == -1) {
                    this.CallSurveyPage();
                  }
                }
              } else {
                this.Class.surveyData.result = this.Class.UNDEFINED_CODE;
                if (!(this.Class.libType == "SupportRemediation"))
                   this.LogStatus();
                islogged = true;
                $ss.snapin.solutions.helper.DispNotApplicable($ss.agentcore.utils.LocalXLate("snapin_solutions", "SA_UNDEFINED"));
              }
            }
          }
          if (!islogged) {
            this.Class.surveyData.result = "-1";   //Is this success or failure ???
            this.LogStatus();
          }
        } catch (e) { }
    },

  // Function name changed so that the same functionality is executed
  // when executing the solution either manually or through Job
  // Also includes Logging.
  snp_script_action_run: function (guid, title, version, action) {
    var UNDEFINED_RES = "";
    var sContentGuid = guid;
    this.Class.guid = guid;
    if (!sContentGuid) {
      return;
    }
    $("#snapin_solutions .supportActAuth").hide();
    $("#snapin_solutions #scriptActionRun").hide();
    $("#snapin_solutions .supportActAuthDoing").show();

    var bHideOutputSection = this.Class.config.GetConfigValue("snapin_solutions", "snp_soln_hideSupportActionOuptutMsgs", "true");
    (bHideOutputSection.toLowerCase() === "true") ? $("#snapin_solutions #SATextOutput").hide() : $("#snapin_solutions #SATextOutput").show();

    $ss.agentcore.utils.Sleep(200);
    this.Class.SAWaitingForUserInput = false;
    this.Class.scriptControl = null;
    this.Class.version = version;
    var sContentVersion = version;
    var stitle = title;
    var bElevated = false;
    var ElevationLevel = undefined;
    var self = this;
    var fCallBack = self.SupportActionCallBack
    if (bElevated)  ElevationLevel = "User";
    var providerID;
    //Reinitializing survey related properties
    this.resetsurveyDataProp();
    var _fso = $ss.agentcore.dal.file.GetFileSystemObject();
    var cabFileName = this.Class.config.GetContextValue("SdcContext:DirUserServer") + "data\\sprt_actionlight\\" + sContentGuid + "." + sContentVersion + "\\composite.cab";
    if(!_fso.FileExists(cabFileName) && this.Class.sSource == "sprt_job"){
      this.Class.surveyData.guidContent = sContentGuid;
      this.Class.surveyData.version = sContentVersion;
      this.Class.surveyData.verb = "view";
      this.Class.surveyData.viewed = "";
      this.Class.surveyData.comment = this.Class.sSource;
      this.Class.surveyData.data = "sprt_Actionlight";
      this.Class.surveyData.result = this.Class.FAIL_CODE;
      this.LogStatus();
      return;
    }
    var retValue = -1;
    var islogged = false;
    var sLibType = this.Class.content.GetContentFieldValue("sprt_actionlight", sContentGuid, sContentVersion, "librarytype", "sccf_field_value_char");
    var sExitLibType = sLibType;
        this.Class.exitLibType = sExitLibType;
    var sExecType = this.Class.content.GetContentFieldValue("sprt_actionlight", sContentGuid, sContentVersion, "executiontype", "sccf_field_value_char");
    this.Class.LibType = sLibType;
    sExecType = sExecType.toUpperCase();
    try {
      this.Class.scriptControl = new $ss.agentcore.supportaction(this, sContentGuid, providerID, cabFileName, bElevated, ElevationLevel, fCallBack, 1);
      $ss.agentcore.dal.databag.SetValue("snp_soln_Curr_SA_Running_Guid", sContentGuid);
      $ss.agentcore.dal.databag.SetValue("snp_soln_Curr_SA_Running_Version", sContentVersion);
      this.Class.supportActionIntialized = true;
      if (!bMac) {
      this.Class.scriptControl.srCtl.SetUserInputWait(true);
      }
      this.Class.surveyData.comment = "";

      var methodToExecute = this.getScriptControlMethod(action, sExecType);

      switch (methodToExecute) {
        case "EvaluateTest" :
          // Call test section first and then call main/run section if required in case of non ASK solution
          if (bMac) {
            this.Class.methodToExecute = methodToExecute;
            this.Class.libType = sLibType;
            this.Class.execType = sExecType;
            this.ScanMac(guid, cabFileName);
          }else {
          retValue = this.Scan(cabFileName);
          this.Class.surveyData.comment = "Scan:<" + retValue + ">-->";
          if (retValue === this.Class.ASK_FIX_REQUIRED_CODE)  $ss.snapin.solutions.helper.DispExecSuccess();
          else  $ss.snapin.solutions.helper.DispExecFail();
          if((sLibType != "SupportRemediation" && retValue === "0") || (sLibType == "SupportRemediation" && sExecType == "SCAN_AND_FIX" && retValue > 0)){
            $("#snapin_solutions #scriptActionFix").show(); //showing fix button
            unFixedSRs = sContentGuid;
            if (sLibType == "SupportRemediation") sExitLibType = sLibType + "-scan"
          }
          else{
            if (sLibType == "SupportRemediation") {
              //if no fix needed, log the same
              sExitLibType = sLibType + "-scan"
              //this.WriteToLog(null, null, null);
            }
          }
                    //log the values in the global variable to log if the the snapin is changed or bcont is closed
          SRLogger["content_guid"] = this.Class.guid;
          SRLogger["content_version"] = this.Class.version;
          SRLogger["pretest_result"] = retValue;
          SRLogger["scan_time"] = this.Class.scan_time;
          SRLogger["scan_status"] = "SA_SCAN";
          retValueOutFromScan = retValue;
        }
          break;
        case "Evaluate" :
          if (sLibType == "SupportRemediation" || (retValueOutFromScan === this.Class.ASK_FIX_REQUIRED_CODE || retValueOutFromScan === UNDEFINED_RES)) {
            $("#snapin_solutions #scriptActionFix").hide(); //hiding fix button
          if (bMac) {
            this.Class.methodToExecute = methodToExecute;
            this.Class.libType = sLibType;
            this.Class.execType = sExecType;
            this.FixMac(guid, cabFileName);
          }else {
            retValue = this.Fix(cabFileName);
            if(sLibType == "SupportRemediation"){
              sExitLibType = sLibType + "-fix"
              if (!(retValue == this.Class.g_SA_FIX_COMPLETED_RESCAN_NOT_REQUIRED || retValue == this.Class.g_SA_FAILED || retValue == this.Class.g_SA_FIX_NOT_POSSIBLE)) {
                //rescan start
                retValue = this.Rescan(cabFileName);
                sExitLibType = sLibType + "-rescan"
              }
              //this.WriteToLog(null, null, null);
            }
            this.Class.surveyData.comment += "Fix:<" + retValueOutFromScan + ">";
            retValueOutFromScan = "";
          }
        }
          break;
        case "EvaluateTestAndRun" :
        default :
        if (bMac) {
            this.Class.methodToExecute = methodToExecute;
            this.Class.libType = sLibType;
            this.Class.execType = sExecType;
            this.ScanMac(guid, cabFileName);
          }else {
          retValue = this.Scan(cabFileName);
          if (sLibType == "SupportRemediation") {
            sExitLibType = sLibType + "-scan"
            if (retValue > 0) {
              retValue = this.Fix(cabFileName);
              sExitLibType = sLibType + "-fix"
              if (!(retValue == this.Class.g_SA_FIX_COMPLETED_RESCAN_NOT_REQUIRED || retValue == this.Class.g_SA_FAILED || retValue == this.Class.g_SA_FIX_NOT_POSSIBLE)) {
                //rescan start
                retValue = this.Rescan(cabFileName);
                sExitLibType = sLibType + "-rescan"
              }
            }
          }
          else{
            //Not a SR, for SA do the fix if the retVal == 0
            if (retValue == "0") {
              retValue = this.Fix(cabFileName);
            }
          }
          this.Class.surveyData.comment = "Scan:<" + retValue + ">-->";
          this.Class.surveyData.comment = "Fix:<" + retValue + ">";
          retValueOutFromScan = "";
        }
          break;
      }
    }
    catch (ex) { }
    finally {
      if (!bMac) {
      this.Class.scriptControl = null;
      this.Class.supportActionIntialized = false;
    }
    }
    if (!bMac) {

    try {
      if (retValue !== UNDEFINED_RES && retValue !== this.Class.ABORT_CODE) {
        $ss.snapin.solutions.helper.HideExecDisplay();
        var oExit = $ss.agentcore.exitcodes.GetExitCodeDetails(sExitLibType, retValue);
        $ss.agentcore.dal.databag.RemoveValue("snp_soln_Curr_SA_Running_Guid");
        $ss.agentcore.dal.databag.RemoveValue("snp_soln_Curr_SA_Running_Version");
        this.Class.surveyData.result = retValue;   // Same returned result incase of SA
        if (oExit !== null) this.Class.surveyData.result = oExit.exittype; // Mapped result incase of ASK
        if (this.Class.sSource == "sprt_job" || this.Class.sSource == "sprt_trigger") {
          this.Class.surveyData.guidContent = sContentGuid;
          this.Class.surveyData.version = sContentVersion;
          this.Class.surveyData.verb = "view";
          this.Class.surveyData.viewed = "";
          this.Class.surveyData.SurveyProblemSolvedYN = ""
          this.Class.surveyData.comment = this.Class.sSource;
          this.Class.surveyData.data = "sprt_actionlight";
          this.Class.surveyData.SurveyRating = ""
          this.LogStatus();
          islogged = true;
        }
        else if (sLibType === "" || sExecType === "") {
          if (retValue === this.Class.SUCCESS_CODE) {
            $ss.snapin.solutions.helper.DispExecSuccess();
            islogged = true;
            if (retValueOutFromScan === "" || retValueOutFromScan === undefined)
              this.CallSurveyPage();  // Survey page will log if survey enbaled or we are logging explicitly in this method
          }
          else {
            this.LogStatus();
            islogged = true;
            $ss.snapin.solutions.helper.DispExecFail();
           }
        }
        else{
          if (oExit !== null) {
            if (oExit.exittype === this.Class.SUCCESS_CODE || (sLibType == "SupportRemediation" && sExecType == "SCAN_AND_FIX" && retValueOutFromScan > 0)) {
              $ss.snapin.solutions.helper.DispExecSuccess(oExit.desc);
              islogged = true;
              if (retValueOutFromScan === "" || retValueOutFromScan === undefined)
                this.CallSurveyPage();  // Survey page will log if survey enbaled or we are logging explicitly in this method
              }else {
                this.LogStatus();
                islogged = true;
                $ss.snapin.solutions.helper.DispExecFail(oExit.desc);
               }
            } 
            else {
              this.Class.surveyData.result = this.Class.UNDEFINED_CODE;
              if (!(sLibType == "SupportRemediation"))
              this.LogStatus();
              islogged = true;
              $ss.snapin.solutions.helper.DispNotApplicable($ss.agentcore.utils.LocalXLate("snapin_solutions", "SA_UNDEFINED"));
            }
          }
        }
        if (!islogged) {
          this.Class.surveyData.result = "-1";   //Is this success or failure ???
          this.LogStatus();
        }
    }
    catch (e) { }
  }
  },

	//Write to SYSTUNE log table, it does only for the support remediation
  WriteToLog: function (srType, srFLTno, srCTX) {
    var entryCTSS = {
      'content_guid': this.Class.guid,
      'content_version': this.Class.version,
      'pretest_result': this.Class.scan_result,
      'main_result': this.Class.fix_result,
      'posttest_result': this.Class.rescan_result,
      'scan_time': this.Class.scan_time,
      'tune_time': this.Class.fix_time,
      'rescan_time': this.Class.rescan_time,
      'float3': this.Class.SurveyRating,
      //Survey Feature Starts
      'boolean': this.Class.dictSurveySubmitButtonClicked,
      //Survey Feature End
      'string2': this.Class.SurveyProblemSolvedYN
    }
    if (srType) {
      entryCTSS["sr_type"] = srType;
      entryCTSS["sr_fltno"] = srFLTno;
    }
    if (srCTX) {
      entryCTSS["sr_ctx"] = srCTX;
    }
    var sDebugMsg = " ";
    for (var fld in this.report) {
      sDebugMsg += fld + ":" + this.report[fld] + "; ";
      entryCTSS[fld] = this.report[fld];
    }

    if ($ss.snapin.solutions.helper.SR_systuneUsage(entryCTSS) === true)
      return sDebugMsg;
    else
      return "error in logging";
  },

  SupportActionCallBack: function (context_in, event_in) {
    //do not use this here .. this here will refer to the callback caller context...

    switch (context_in) {
      case "InstallProgress":
        //Display The content Showing Install In Progress
        //Not Supported Here
        break;
      case "ScriptError":
        //Display Error Problem
        $("#SATextOuputContentArea").append("<span>ScriptScript Error!!!! " + event_in + "<br></span>");
        $ss.agentcore.utils.Sleep(200);
        break;
      case "STDOUT":
        //display the stdout content
        $("#SATextOuputContentArea").append("<span>" + event_in + "<br></span>");
        $ss.agentcore.utils.Sleep(200);
        break;
      case "STDIN":
        //prompt the user
        return prompt(event_in, "");
        break;
      case "STDINXML":

        $("#snapin_solutions .supportActExec").hide();
        $("#snapin_solutions .supportActExecWaitUserInput").show();
        var cHTML = $ss.snapin.solutions.helper.ConvertSolutionINXMLToHtml(event_in);
        $("#snapin_solutions #supportActionInput").show();
        $("#snapin_solutions #supportActionInputContent").append(cHTML);
        //this.Class.SAWaitingForUserInput = true; DO not use this here ..
        SnapinSolutionsController.SAWaitingForUserInput = true;
        //prompt the user for input XML TODO
        break;
      case "STDOUTXML":
        return "";
        break;
      case "ScriptAuthentication":
        //prompt for script authentication
        $("#snapin_solutions .supportActAuth").hide();
        if (event_in === "OK") {
          $("#snapin_solutions .supportActExec").hide();
          $("#snapin_solutions .supportActAuthSucess").show();
          $("#snapin_solutions .supportActExecDoing").show();
        }
        else {
          $("#snapin_solutions .supportActExec").hide();
          $("#snapin_solutions .supportActAuthFail").show();
          $("#snapin_solutions .supportActExecFail").show();
        }
        $ss.agentcore.utils.Sleep(200);
        break;
      case "TEST":
        var sContentGuid = $ss.agentcore.dal.databag.GetValue("snp_soln_Curr_SA_Running_Guid");
        var sContentVersion = $ss.agentcore.dal.databag.GetValue("snp_soln_Curr_SA_Running_Version");
        var sLibType = $ss.agentcore.dal.content.GetContentFieldValue("sprt_actionlight", sContentGuid, sContentVersion, "librarytype", "sccf_field_value_char");
        var oExit = $ss.agentcore.exitcodes.GetExitCodeDetails(sLibType, event_in);
        if (event_in != "0" && event_in != "999") {
          $ss.snapin.solutions.helper.HideExecDisplay();
          if (oExit === null)
            $ss.snapin.solutions.helper.DispNotApplicable();
          else
            $ss.snapin.solutions.helper.DispNotApplicable(oExit.desc);
        }
        break;
      case "WAITING_FOR_USER_INPUT": //Do Nothing, Event waiting for User Input (event_in = Dummy)
        $ss.agentcore.utils.Sleep(200);
        break;
      default:
        //handle the unknown stuff
        $("#SATextOuputContentArea").append("<span>Unknown Event!!!! " + context_in + " : " + event_in + "<br></span>");
        $ss.agentcore.utils.Sleep(200);
        break;
    }
    return "";

  },

  "#scriptActionContinue click": function (params) {
    this.SaveAndContinueSA();
  },

  SaveAndContinueSA: function () {
    if (this.SupportActionSave()) {
      try {
        $("#snapin_solutions #supportActionInput").hide();

        this.Class.scriptControl.srCtl.NotifyUserInputDone();
        $("#snapin_solutions .supportActExec").hide();
      }
      catch (ex) {
      }
    }
    else {
      //display error
    }
  },

  "#gcform submit": function (params) {
    this.SaveAndContinueSA();
    params.event.kill();
  },

  "#scriptActionQuit click": function (params) {
    try {
      this.Class.scriptControl.srCtl.Abort(999);
      this.Class.scriptControl = null;
      this.Class.supportActionIntialized = false;
      $("#snapin_solutions .supportActExec").hide();
      $("#snapin_solutions #supportActionInput").hide();
      $("#snapin_solutions #inputXMLButtons").hide();
      $("#snapin_solutions #SATextOutput").hide();

      $("#snapin_solutions .supportActExecAbort").show();
    }
    catch (ex) {
    }
  },

  SupportActionSave: function () {
    var bRetValue = true;
    var i = 0;

    try {
      for (i = 0; i < document.gcform.length; i++) {
        if (document.gcform.elements[i].name.indexOf("client.") != -1) {
          var name = document.gcform.elements[i].name.split(".");

          if (document.gcform.elements[i].name.indexOf("checkbox.") != -1) {
            if (document.gcform.elements[i].checked) {
              this.Class.scriptControl.SetParameter(name[2], "true");
            }
            else {
              this.Class.scriptControl.SetParameter(name[2], "false");
            }
          }
          else
            if (document.gcform.elements[i].name.indexOf("radio.") != -1) {
              if (document.gcform.elements[i].checked) {
                this.Class.scriptControl.SetParameter(name[2], document.gcform.elements[i].value);
              }
            }
            else {
              this.Class.scriptControl.SetParameter(name[2], document.gcform.elements[i].value);
            }
        }
      }
    }
    catch (ex) {
      bRetValue = false;
    }

    return bRetValue;
  },

  ".SolutionFaq click": function (params) {
    if ("true" === $(params.element).attr('externalview')) {
      return;
    }

    var cid = $(params.element).attr('guid');
    var version = $(params.element).attr('version');
    var ctype = $(params.element).attr('ctype');
    $ss.snapin.solutions.helper.RenderSolutionView(ctype, cid, version);
  },


  ".SolutionAction click": function (params) {
    var url = $(params.element).attr('url');
    params.requesturl = url;
    params.requestPath = $ss.agentcore.utils.GetRootFromURL(params.requesturl);
    params.requestQS = $ss.agentcore.utils.GetQSFromURL(params.requesturl);
    params.requestArgs = $ss.agentcore.utils.GetQSObjectFromURL(params.requesturl);

    //get the value from databag
    var backURL = $ss.agentcore.dal.databag.GetValue("snp_soln_oneclick_back") || null;
    if (backURL) {
      $ss.agentcore.dal.databag.SetValue("snp_soln_oneclick_back", null, "true");
      params.requestArgs.snpsoln_doneurl = backURL;
    }
    this.Class.dispatch("navigation", "index", params);
  },

  CallSurveyPage: function () {
    var params = {};
    var surveyData = {};
    var arrSurveyData = [];
        
    if (this.Class._enableSurvey && this.Class._enableSurvey.toLowerCase() == "true") {
      var surveyData = this.Class.surveyData;
      params.LibType = this.Class.LibType;

      //Prepair survey data
      surveyData.guidContent = this.Class.surveyData.guidContent;
      surveyData.version = this.Class.surveyData.version;
      surveyData.verb = this.Class.surveyData.verb;
      surveyData.viewed = this.Class.surveyData.viewed;
      surveyData.comment = this.Class.surveyData.comment;
      surveyData.title = this.Class.surveyData.title;
      surveyData.data = this.Class.surveyData.data;

      arrSurveyData.push(surveyData);
      params.surveyData = arrSurveyData;

      try {
        if (this.Class._enableSurvey && this.Class._enableSurvey.toLowerCase() == "true") {
          this.Class.dispatch("snapin_survey", "index", params);
        }
      }
      catch (ex) { }
            //    this.WriteToLog(null, null, null);
            //}
            //if library type is support remediation then perfrom SYSTUNE logging for
            //the rating and solved flag in the log file 
            //if (this.Class.LibType == "SupportRemediation") {
            //this.Class.SurveyRating = JSON.parse($ss.agentcore.dal.databag.GetValue("dictRating"));
            //this.Class.SurveyProblemSolvedYN = JSON.parse($ss.agentcore.dal.databag.GetValue("solved"));

      var surveyRating = $ss.agentcore.dal.databag.GetValue("dictRating");
      var surveySolved = $ss.agentcore.dal.databag.GetValue("dictSolved");
      var dictSurveySubmitButtonClicked = $ss.agentcore.dal.databag.GetValue("dictSurveySubmitButtonClicked");

      var objDict = null;
      var objDictSolved = null;
      var objDictSurveySubmitButtonClicked = null;

      if (surveyRating) {
        objDict = JSON.parse(surveyRating);
      }

      if (surveySolved) {
        objDictSolved = JSON.parse(surveySolved);
      }

      if (dictSurveySubmitButtonClicked) {
        objDictSurveySubmitButtonClicked = JSON.parse(dictSurveySubmitButtonClicked);
      }

      this.Class.surveyData.SurveyRating = objDict[this.Class.surveyData.guidContent];
      this.Class.surveyData.SurveyProblemSolvedYN = objDictSolved[this.Class.surveyData.guidContent];
      this.Class.surveyData.dictSurveySubmitButtonClicked = objDictSurveySubmitButtonClicked[this.Class.surveyData.guidContent];

      this.Class.SurveyRating = objDict[this.Class.surveyData.guidContent];
      this.Class.SurveyProblemSolvedYN = objDictSolved[this.Class.surveyData.guidContent];

      if (this.Class.LibType == "SupportRemediation") {
        if (objDictSurveySubmitButtonClicked[this.Class.surveyData.guidContent] == true) {
          this.WriteToLog(null, null, null);
        }
      }
      else if (objDictSurveySubmitButtonClicked[this.Class.surveyData.guidContent] == true) {
        this.LogStatus();
      }
      $ss.agentcore.dal.databag.RemoveValue("dictRating");
      $ss.agentcore.dal.databag.RemoveValue("dictSolved");
      $ss.agentcore.dal.databag.RemoveValue("dictSurveySubmitButtonClicked");      

      params.LibType = null;
      params.surveyData = null;
      params = null;
    }else{
      //log it even if survey is disabled
      this.LogStatus();
    }
  }
});

var retValueOutFromScan;
var SRLogger = {
  'content_guid': null,
  'content_version': null,
  'pretest_result': null,
  'main_result': null,
  'posttest_result': null,
  'scan_time': null,
  'tune_time': null,
  'rescan_time': null,
  'scan_status' : null
};

var bMac = $ss.agentcore.utils.IsRunningInMacMachine();
