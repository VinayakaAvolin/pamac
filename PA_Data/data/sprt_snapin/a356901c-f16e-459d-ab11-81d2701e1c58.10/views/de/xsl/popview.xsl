<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output  method="html"
              omit-xml-declaration="yes"/>
  <xsl:include href="common.xsl"/>

  <xsl:template match="/">
    <xsl:call-template name="displayOrder" />
    <xsl:call-template name="pagerCount" />
  </xsl:template>

  <xsl:template match="msg">
    <xsl:for-each select=".">

      <div>
        <xsl:attribute name="id">
          <xsl:text>id</xsl:text>
          <xsl:value-of select="@guid" />
        </xsl:attribute>
        <div class="clsMsgMarker">
          <xsl:attribute name="guid">
            <xsl:value-of select="@guid" />
          </xsl:attribute>
          <xsl:attribute name="version">
            <xsl:value-of select="@ver" />
          </xsl:attribute>
          <xsl:attribute name="tv">
            <xsl:value-of select="@tv" />
            <xsl:value-of select="@tv" />
          </xsl:attribute>
          <xsl:attribute name="content_type">
            <xsl:value-of select="@content_type" />
          </xsl:attribute>
          <div class="clsMsgDiv">
            <xsl:attribute name="id">
              <xsl:value-of select="@guid" />
            </xsl:attribute>

            <div id="triggersubject" class="panel-title">
              <table cellspacing="0" cellpadding="0" border="0" width="100%">
                <tr>
                  <td width="30">

                    <xsl:choose>
                      <xsl:when test="p[@name='severity']='1'">
                        <span class="fa fa-exclamation-triangle"/>
                      </xsl:when>
                      <xsl:otherwise>
						<span class="fa fa-info-circle"/>
                      </xsl:otherwise>
                    </xsl:choose>
                  </td>

                  <td>
                    <xsl:attribute name="id">
                      <xsl:text>title</xsl:text>
                      <xsl:value-of select="@guid" />
                    </xsl:attribute>
                    <xsl:if test="p[@name='shown_dt']=''">
                      <xsl:attribute name="style"></xsl:attribute>
                    </xsl:if>
                    <!--Concat the string (...) only for string greater than 50-->
                    <xsl:choose>
                      <xsl:when test="string-length(p[@name='title']) > 50">
                        <xsl:value-of select="concat(substring(p[@name='title'],1,50), '...')"   disable-output-escaping="no"/>
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:value-of select="p[@name='title']"   disable-output-escaping="no"/>
                      </xsl:otherwise>
                    </xsl:choose>
                  </td>
                </tr>
              </table>
            </div>
            <div id="triggermesssage">
              <form name="" id="" method="get" action="#">
                <div class="messagearea">
                  <!-- <p>A short text description of the message can be displayed here. The message can be truncated...</p>-->
                  <p>
                    <span id="messagedesc">
                <xsl:value-of select="p[@name='desc']"   disable-output-escaping="yes"/>
              </span>
            </p>
            <p>
              <xsl:if test="@d='1'">
                <span href="#" class="linkmore"  id="cl_msg_ViewDetailsLink">
                  <xsl:attribute name="popguid">
                    <xsl:value-of select="@guid" />
                  </xsl:attribute>
                  <xsl:attribute name="popver">
                    <xsl:value-of select="@ver" />
                  </xsl:attribute>
                  Mehr Details
                </span>
              </xsl:if>
            </p>
            <xsl:choose>
              <xsl:when test="@d='1'">
                <p style="display:none" class="MoreDetails_Busy_samessagesviewer busy_message" ><b>Die Anwendung führt eine andere Aufgabe aus. Öffnen Sie bitte das 'Nachrichten'-Snapin im Agent, für 'Mehr Details'.</b></p>
                <p style="display:none" class="ViewAll_Busy_samessagesviewer busy_message" ><b>Die Anwendung führt eine andere Aufgabe aus. Öffnen Sie bitte das 'Nachrichten'-Snapin im Agent, um 'alle Nachrichten anzusehen'.</b></p>
              </xsl:when>
              <xsl:otherwise>
                <p style="display:none" class="ViewAll_Busy_dm busy_message" ><b>Die Anwendung führt eine andere Aufgabe aus. Öffnen Sie bitte das 'Download Manager'-Snap-In im Agent, für die 'Download-Ansicht'.</b></p>
              </xsl:otherwise>
            </xsl:choose>
          </div>

          <div class="buttonarea">
            <xsl:variable name="caption">
              <xsl:choose>
                <xsl:when test="p[@name='payload_status']='S'">
                  <xsl:value-of select="substring(p[@name='completed_caption'],1,15)" />
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="substring(p[@name='incomplete_caption'],1,15)" />
                </xsl:otherwise>
              </xsl:choose>
            </xsl:variable>
            <a href="#" tabindex="3" id="cl_msg_Action">
              <span class="btn btn-primary-ss btn-xs button">
                <xsl:attribute name="popmsgguid">
                  <xsl:value-of select="@guid" />
                </xsl:attribute>
                <xsl:attribute name="popmsgver">
                  <xsl:value-of select="@ver" />
                </xsl:attribute>
                <xsl:attribute name="content_type">
                  <xsl:value-of select="@content_type" />
                </xsl:attribute>

                <xsl:choose>
                  <xsl:when test="p[@name='payload']='S'">
                    <xsl:attribute name="popmsgguid">
                      <xsl:value-of select="@guid" />
                    </xsl:attribute>
                    <xsl:attribute name="popmsgver">
                      <xsl:value-of select="@ver" />
                    </xsl:attribute>
                    <xsl:attribute name="popmsgtype">
                      <xsl:text>0</xsl:text>
                    </xsl:attribute>
                    <xsl:if test="p[@name='payload_status']='S'">
                      <xsl:attribute name="disabled">true</xsl:attribute>
                    </xsl:if>
                    <xsl:attribute name="elevation">
                      <xsl:value-of select="substring(p[@name='elevation'],1,15)" />
                    </xsl:attribute>
                  </xsl:when>
                  <xsl:when test="p[@name='payload']='U'">
                    <xsl:attribute name="popmsgguid">
                      <xsl:value-of select="@guid" />
                    </xsl:attribute>
                    <xsl:attribute name="popmsgver">
                      <xsl:value-of select="@ver" />
                    </xsl:attribute>
                    <xsl:attribute name="popmsgtype">
                      <xsl:text>3</xsl:text>
                    </xsl:attribute>
                  </xsl:when>
                  <xsl:when test="p[@name='payload']='V'">
                    <xsl:attribute name="popmsgguid">
                      <xsl:value-of select="@guid" />
                    </xsl:attribute>
                    <xsl:attribute name="popmsgver">
                      <xsl:value-of select="@ver" />
                    </xsl:attribute>
                    <xsl:attribute name="popmsgtype">
                      <xsl:text>1</xsl:text>
                    </xsl:attribute>
                  </xsl:when>
                  <xsl:when test="p[@name='payload']='VS'">
                    <xsl:attribute name="popmsgguid">
                      <xsl:value-of select="@guid" />
                    </xsl:attribute>
                    <xsl:attribute name="popmsgver">
                      <xsl:value-of select="@ver" />
                    </xsl:attribute>
                    <xsl:attribute name="popmsgtype">
                      <xsl:text>2</xsl:text>
                    </xsl:attribute>

                    <xsl:if test="p[@name='payload_status']='S'">
                      <xsl:attribute name="disabled">true</xsl:attribute>
                    </xsl:if>
                    <xsl:attribute name="elevation">
                      <xsl:value-of select="substring(p[@name='elevation'],1,15)" />
                    </xsl:attribute>
                  </xsl:when>
                </xsl:choose>

                <xsl:value-of select="substring($caption,1,15)" />

              </span>
            </a>
            <span>&#160;&#160;</span>
            <xsl:choose>
              <xsl:when test="@not_sprt_msg='1'">
              </xsl:when>
              <xsl:otherwise>
                <a href="#" tabindex="4">
                  <span class="btn btn-primary-ss btn-xs buttondel" id="btndelmsg">
                    <xsl:attribute name="popdelguid">
                      <xsl:value-of select="@guid" />
                    </xsl:attribute>
                    <xsl:text>Löschen</xsl:text>
                  </span>
                </a>
              </xsl:otherwise>
            </xsl:choose>
          </div>

          <div class="messagemore">
            <div class="coleft">
              <xsl:choose>
                <xsl:when test="@not_sprt_msg='1'">
                  <span class="moremmesages cl_msg_NewAlerts" style="display:none" id="cl_msg_NewAlerts">Neue Nachrichten!</span>
                  <span class="moremmesages cl_msg_ViewAlerts" tabindex="5" id="cl_msg_ViewAllLink"></span>
                </xsl:when>
                <xsl:otherwise>
                  <span class="moremmesages cl_msg_NewAlerts" id="cl_msg_NewAlerts" style="display:none;">Neue Nachrichten!</span>
                  <span class="moremmesages cl_msg_ViewAlerts" tabindex="5" id="cl_msg_ViewAllLink">Alle Nachrichten ansehen</span>
                </xsl:otherwise>
              </xsl:choose>

            </div>
            <div class="coright">
               <ul class="pages" id="idMsgCounter">
                <li tabindex="6" class="previous fa fa-chevron-left" id="idMsgPrev"></li>
                <li class="placeholder"></li>
                <span class="idMsgCounterPageId" >1</span>
                <span class="numbers"> von </span>
                <span class="idMsgCounterTotalPageCount" >1</span>
                <li class="placeholder"></li>
                <li tabindex="7" class="next fa fa-chevron-right" id="idMsgNext"> </li>
              </ul>
            </div>
          </div>
        </form>
      </div>

    </div>

  </div>


</div>
    </xsl:for-each>
  </xsl:template>
  <xsl:template name="pagerCount">
  </xsl:template>
</xsl:stylesheet>