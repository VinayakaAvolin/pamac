<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" omit-xml-declaration="yes"/>

  <!-- When the Sort type is "severity"
    1. Critical alerts\Real Time alerts 
        a. Unexecuted and Unviewed; a1. any content other than sprt_msg Availability Notification;
        b. Unexecuted and Viewed; c. Executed
    2. Default alerts
        2.1. Unexecuted and Unviewed 
            a. high; b. medium; c. low
        2.2. Unexecuted and Viewed 
            a. high; b. medium; c. low
        2.3. Executed
            a. high; b. medium; c. low
            
       When the Sort type is "unread"
    1. Unexecuted and Unviewed 
        a. Critical alerts\Real Time alerts ; b. high; c. medium
        d. low; a1. any content other than sprt_msg Availability Notification;
    2. Unexecuted and Viewed
        a. Critical alerts\Real Time alerts; b. high;
        c. medium; d. low
    3. Executed
        a. Critical alerts\Real Time alerts; b. high
        c. medium; d. low
  -->
  
      <xsl:template name="displayOrder">

        <xsl:choose>
          <xsl:when test="/sprt/msgs/metadata = 'severity' ">

            <!-- 1a. Real Time alerts -->
            <xsl:apply-templates select="/sprt/msgs/msg[@top='1']" />

            <!-- 1a. Unexecuted and UnViewed Critical alerts -->
            <xsl:apply-templates select=" /sprt/msgs/msg[p[@name='severity']= '1' and p[@name='shown_dt']='' and (@top!='1' or not(@top))]" />

            <!-- 1.a1. any content other than sprt_msg Availability Notification -->
            <xsl:apply-templates select="/sprt/msgs/msg[@not_sprt_msg='1']" />

            <!-- 1b. Unexecuted and Viewed Critical alerts -->    
            <xsl:apply-templates select="/sprt/msgs/msg[p[@name='severity']='1' and p[@name='shown_dt']!='' and p[@name='payload_status']='N' and (@top!='1' or not(@top))]" />
            
            <!-- 1c. Executed Critical alerts -->
            <xsl:apply-templates select="/sprt/msgs/msg[p[@name='severity']='1' and p[@name='shown_dt']!='' and p[@name='payload_status']!='N' and (@top!='1' or not(@top))]" />

            <!-- 2.1 Unexecuted and Unviewed -->
            <xsl:apply-templates select="/sprt/msgs/msg[p[@name='severity']!='1' and p[@name='shown_dt']='' and (@top!='1' or not(@top))]" >
              <xsl:sort select="p[@name='severity']" data-type="number"/>
              <xsl:sort select="p[@name='create_dt']"/>
            </xsl:apply-templates>     

            <!-- 2.2 Unexecuted and Viewed  -->
            <xsl:apply-templates select="/sprt/msgs/msg[p[@name='severity']!='1' and p[@name='shown_dt']!='' and p[@name='payload_status']='N' and (@top!='1' or not(@top))]" >
              <xsl:sort select="p[@name='severity']" data-type="number"/>      
              <xsl:sort select="p[@name='create_dt']"/>
            </xsl:apply-templates>

            <!-- 2.3 Executed -->
            <xsl:apply-templates select="/sprt/msgs/msg[p[@name='severity']!='1' and p[@name='shown_dt']!='' and p[@name='payload_status']!='N' and (@top!='1' or not(@top))]" >
              <xsl:sort select="p[@name='severity']" data-type="number"/>
              <xsl:sort select="p[@name='create_dt']"/>
            </xsl:apply-templates>
              
            
          </xsl:when>
          <xsl:otherwise>

            <!-- 1a. Real Time alerts -->
            <xsl:apply-templates select="/sprt/msgs/msg[@top='1']" />

            <!-- 1. Unexecuted and UnViewed alerts -->
            <xsl:apply-templates select="/sprt/msgs/msg[p[@name='severity']!= '-1' and p[@name='shown_dt']='' and (@top!='1' or not(@top))]" >
              <xsl:sort select="p[@name='severity']" data-type="number"/>
              <xsl:sort select="p[@name='create_dt']"/>
            </xsl:apply-templates>

            <!-- 1.1. any content other than sprt_msg Availability Notification -->
            <xsl:apply-templates select="/sprt/msgs/msg[@not_sprt_msg='1']" />

            <!-- 2. Unexecuted and Viewed  -->
            <xsl:apply-templates select="/sprt/msgs/msg[p[@name='severity']!='-1' and p[@name='shown_dt']!='' and p[@name='payload_status']='N' and (@top!='1' or not(@top))]" >
              <xsl:sort select="p[@name='severity']" data-type="number"/>
              <xsl:sort select="p[@name='create_dt']"/>
            </xsl:apply-templates>

            <!-- 3. Executed -->
            <xsl:apply-templates select="/sprt/msgs/msg[p[@name='severity']!='-1' and p[@name='shown_dt']!='' and p[@name='payload_status']!='N' and (@top!='1' or not(@top))]" >
              <xsl:sort select="p[@name='severity']" data-type="number"/>
              <xsl:sort select="p[@name='create_dt']"/>
            </xsl:apply-templates>

          </xsl:otherwise>
        </xsl:choose>
      </xsl:template>
</xsl:stylesheet>