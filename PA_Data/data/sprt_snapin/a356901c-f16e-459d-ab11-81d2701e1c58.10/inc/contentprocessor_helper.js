//Generic Class to read the specified content types and XML and will have basic functionalities.

$ss.snapin = $ss.snapin ||
{};
$ss.snapin.msg = $ss.snapin.msg ||
{};

$ss.snapin.msg.ContentModel = MVC.Class.extend({
  init: function(sContentType, sStatusXMLName){
    this.contentType = sContentType;
    this.statusXMLFileName = sStatusXMLName || (sContentType + ".xml");
    this.contentTypePath = $ss.agentcore.dal.config.GetContextValue("SdcContext:DirUserServer") + "\\data\\" + sContentType;
    this.statusXMLFile = this.contentTypePath + "\\" + this.statusXMLFileName;
    if (bMac) {
      this.contentTypePath = $ss.agentcore.dal.config.GetContextValue("SdcContext:DirUserServer") + "data/" + sContentType;
      this.statusXMLFile = this.contentTypePath + "/" + this.statusXMLFileName;
    }
    this.XPATH_MSG_STRING = "/sprt/msgs/msg";
    
    this.MACRO_MESSAGE_MACRO = "#JTD_MACRO_GUID#";
    this.MACRO_MESSAGE_STRNG = this.XPATH_MSG_STRING + "[@guid='#JTD_MACRO_GUID#']";
    
    this.MACRO_MESSAGE_VERSION = "#JTD_MACRO_VERSION#";
    this.MACRO_MESSAGE_STRNG_VERSION = this.XPATH_MSG_STRING + "[@guid='#JTD_MACRO_GUID#']" + " and " + "[@ver='#JTD_MACRO_VERSION#']";
    this.IsTreatNewVersionAsNewContent = $ss.agentcore.dal.config.GetConfigValue("sprtmsg_for_" + sContentType, "TreatNewVersionAsNewContent", "false").toLowerCase() === "true";
    if (bMac) {
    this.sesssionId = "";
    }else{
    this.sesssionId = window.external.SessionId;
    }
    this.hasSaved = false;
    this.newContent = this.GetNewContent();
    
    
  },
  GetCurrentContent: function(){
    return $ss.agentcore.dal.content.GetContentsByAnyType([this.contentType]);
  },
  GetNewContent: function(){
    var newContent = [];
    //Adding sLangcode to download.xml to should minibcont popup once per language.
    var sLangcode = "";
    try {
      var availableContent = this.GetCurrentContent();
      //No content available just return false
      if (!availableContent || availableContent.length === 0) 
        return newContent;
      //macros
      //message.xml xpaths
      var _xml = $ss.agentcore.dal.xml;
      var msgXML = this.GetMessagesXml();
      for (var i = 0; i < availableContent.length; i++) {
        var sContGuid = availableContent[i].cid;
        var sContVersion = availableContent[i].version;
        var sContTitle = availableContent[i].title;
        var xPathForXML = this.GetXPathForMsgGuid(sContGuid, sContVersion);
        if(availableContent[i].ctype === "sprt_download"){
          sLangcode = $ss.agentcore.utils.GetLanguage();
          xPathForXML = xPathForXML + "[@langcode='" + sLangcode + "']";         
        }
        var oNodes = _xml.GetNodes(xPathForXML, "", msgXML);
        if (oNodes && oNodes.length) {
          //do nothing
        }
        else {
          var newMsg = {};
          newMsg.contentType = this.contentType;
          if(newMsg.contentType === "sprt_download"){
            newMsg.langcode = sLangcode;
          }
          newMsg.guid = sContGuid;
          newMsg.version = sContVersion;
          newMsg.title = sContTitle;
          newContent.push(newMsg);
          msgXML = this.InsertNewMessageNode(msgXML, newMsg);
        }
      }
      
      if (newContent.length) {
        //persisit the XML status file
        if (bMac) {
          var serializer = new XMLSerializer();
          var contents = serializer.serializeToString(msgXML.responseXML);
          this.SaveXMLFile(contents, this.statusXMLFile)
        }else{
        this.SaveXMLFile(msgXML.xml, this.statusXMLFile)
      }
    } 
    } 
    catch (ex) {
      newContent = [];
    }
    return newContent;
    
  },
  
  HasNewContent: function(){
    var newContent = this.newContent;
    if (newContent && newContent.length) 
      return true;
    return false;
  },
  GetMessagesXml: function(){
    //store return
    var oXml = null;
    var _file = $ss.agentcore.dal.file;
    var _xml = $ss.agentcore.dal.xml;
    
    var XML_CONTRACT_BASE = "<?xml version=\"1.0\" encoding=\"utf-8\"?><sprt><msgs></msgs></sprt>";
    //get path to messages.xml
    var sMessagesXml = this.statusXMLFile;
    var bSave = false;
    // if messages.xml doesn't exist, create a skeleton one and save it
    try {
    
      if (!_file.FileExists(sMessagesXml)) {
        //load up some skeleton xml    
        oXml = _xml.LoadXMLFromString(XML_CONTRACT_BASE, false);
        bSave = true;
      }
      else {
        //try to load message.xml 
        //oXml = _xml.LoadXML(sMessagesXml,false);
        oXml = null;
        oXml = _xml.LoadXML(sMessagesXml, false);
      }
    } 
    catch (oErr) {
      //load up some skeleton xml    
      oXml = _xml.LoadXMLFromString(XML_CONTRACT_BASE, false);
      bSave = true;
    }
    finally {
      if (bSave) {
        if (bMac) {
          var serializer = new XMLSerializer();
          var contents = serializer.serializeToString(oXml.responseXML);
          this.SaveXMLFile(contents, sMessagesXml);

        }else{
        this.SaveXMLFile(oXml.xml, sMessagesXml);
      }
    }
    }
    
    return (oXml);
  },
  SaveXMLFile: function(sData, fileName){
    var _file = $ss.agentcore.dal.file;
    return _file.WriteNewFile(fileName, sData);
  },
  GetXPathForMsgGuid: function(psMsgGuid, psVersion){
    var xPath = "";
    if (this.IsTreatNewVersionAsNewContent) {
      xPath = this.MACRO_MESSAGE_STRNG_VERSION.replace(this.MACRO_MESSAGE_MACRO, psMsgGuid);
      xPath = this.MACRO_MESSAGE_STRNG_VERSION.replace(this.MACRO_MESSAGE_VERSION, psVersion);
    }
    else {
      xPath = this.MACRO_MESSAGE_STRNG.replace(this.MACRO_MESSAGE_MACRO, psMsgGuid);
    }
    //take: "/sprt/msgs/msg[@guid='"+MACRO_MESSAGE_MACRO+"']";
    //return back MACRO_MESSAGE_STRING where we replace the string 
    //MACRO_MESSAGE_MACRO with psMsgGuid
    
    return xPath;
  },
  InsertNewMessageNode: function(oXML, oMsg){
    try{
      var _xml = $ss.agentcore.dal.xml;
      var oMsgsNode = _xml.GetNode("/sprt/msgs", "", oXML);
      var oNewMsgNode = null;
      if (bMac) {
        oNewMsgNode = oXML.responseXML.createElement('msg');
      }else {
        oNewMsgNode = oXML.createElement('msg');
      }
      
      //set guid attr
      oNewMsgNode.setAttribute('guid', oMsg.guid);
      //set ver attr
      oNewMsgNode.setAttribute('ver', oMsg.version);
      oNewMsgNode.setAttribute('sid', this.sesssionId);
      oNewMsgNode.setAttribute('langcode',oMsg.langcode);
      oNewMsgNode.appendChild(this.CreateNewPropertyNode(oXML,"title",oMsg.title));
       
      oMsgsNode.appendChild(oNewMsgNode);
    
      $ss.snapin.msgprocessor.helper.cl_msg_updateDownloadReport(oMsg.guid, oMsg.version ,this.contentType );
    }catch(ex) {
      
    }
    
    return oXML;
  },
  
  InsertDisplayMessageNode: function(oXML, oMsg){
    $ss.snapin.msgprocessor.helper.AddContentTypeAsDisplayed(this.contentType);
    var _xml = $ss.agentcore.dal.xml;
    var _utils = $ss.agentcore.utils;
    var oMsgsNode = _xml.GetNode("/sprt/msgs", "", oXML);
    var oNewMsgNode = null;
      if (bMac) {
        oNewMsgNode = oXML.responseXML.createElement('msg');
      }else {
        oNewMsgNode = oXML.createElement('msg');
      }
    //set guid attr
    oNewMsgNode.setAttribute('sid', this.sesssionId);
    //set the session id
    oNewMsgNode.setAttribute('guid', oMsg.guid);
    //set ver attr
    oNewMsgNode.setAttribute('ver', oMsg.version);
    //set content type as attribute
    oNewMsgNode.setAttribute('content_type', this.contentType);
    //set this as non message type 
    oNewMsgNode.setAttribute('not_sprt_msg', 1);
    //oNewMsgNode.setAttribute('title', oMsg.title);
    //put the content type as an attribute
    oNewMsgNode.setAttribute( this.contentType ,1);
    var title= _utils.LocalXLate("snapin_messagespopup","cl_msg_"+this.contentType +"_title");
    var desc = _utils.LocalXLate("snapin_messagespopup","cl_msg_"+this.contentType+"_desc",[oMsg.title])
    var button = _utils.LocalXLate("snapin_messagespopup","cl_msg_"+this.contentType+"_incomplete_button")
    //append new msg mode to the msgs mode
    oNewMsgNode.appendChild(this.CreateNewPropertyNode(oXML,"desc",desc));
    oNewMsgNode.appendChild(this.CreateNewPropertyNode(oXML,"title",title));
    oNewMsgNode.appendChild(this.CreateNewPropertyNode(oXML,"incomplete_caption",button));
    
    
    //for backward compatibility with sprt msg add following attributes
    //set content type as attribute // this is to say it is not a registry triggered type of message .. 
    oNewMsgNode.setAttribute('t', 0);
    oNewMsgNode.setAttribute('exp', 0); //not an expired msg
    oNewMsgNode.setAttribute('act', 1); //actual message
    oNewMsgNode.appendChild(this.CreateNewPropertyNode(oXML,"shown_dt",""));
    oNewMsgNode.appendChild(this.CreateNewPropertyNode(oXML,"deleted_dt",""));

    
    oMsgsNode.appendChild(oNewMsgNode);
    return oXML;
  },
  
  CanDisplayContent: function(tmpMsg) {
    var sUnmanned = "no";
    var tmpXML = $ss.agentcore.dal.content.GetContentDetailsAsXML(tmpMsg.contentType, tmpMsg.guid, tmpMsg.version);
    try {
      var oUnmanned = null;
      if (bMac) {
            var _xml = $ss.agentcore.dal.xml;
          oUnmanned = _xml.SelectSingleNode(tmpXML.responseXML, "//sprt/content-set/content/field-set/field[@name='sdc_sd_silent:::1']/value[@name='sccf_field_value_char']");
      }else {
       oUnmanned = tmpXML.selectSingleNode("//sprt/content-set/content/field-set/field[@name='sdc_sd_silent:::1']/value[@name='sccf_field_value_char']");
      }
      if (oUnmanned && oUnmanned.text) sUnmanned = oUnmanned.text;
    } catch (ex) { }
    if (sUnmanned === "no") return true;
    return false;
  },
  
  GetDisplayXMLMessage: function() {
    var oXml = null;
    var _xml = $ss.agentcore.dal.xml;
    var XML_CONTRACT_BASE = "<?xml version=\"1.0\" encoding=\"utf-8\"?><sprt><msgs></msgs></sprt>";
    var msgXML = _xml.LoadXML(this.statusXMLFile) || null;
    var CURRENT_MESSAGE_XPATH = "/sprt/msgs/msg[@sid='"+this.sesssionId+"']";
    var oNodes = _xml.GetNodes(CURRENT_MESSAGE_XPATH,"",msgXML);
    if(oNodes && oNodes.length) {
      oXml = _xml.LoadXMLFromString(XML_CONTRACT_BASE, false);
      for (var i = 0; i < oNodes.length; i++) {
        var oNode = oNodes[i];
        var tmpMsg = {};
        tmpMsg.contentType = this.contentType;
        tmpMsg.guid = _xml.GetAttribute(oNode,"guid");
        tmpMsg.version = _xml.GetAttribute(oNode,"ver");
        var titleNode = null;
        if (bMac) {
          titleNode = _xml.SelectSingleNodeFromNode(oNode,'name','title');
        }else {
         titleNode = _xml.GetNode("p[@name='title']","",oNode);
      }
        var title = _xml.GetNodeText(titleNode);
        if (this.contentType === "sprt_download") {
          if (!this.CanDisplayContent(tmpMsg)) {
            titleNode = null;
            oNode = null;
            continue;
          }
        }
        oXml = this.InsertDisplayMessageNode(oXml, tmpMsg);
        titleNode = null;
        oNode = null;
      }
      
    }
    return oXml;
  },
  
  CreateNewPropertyNode: function(poXml, psName, psValue) {
      //store return, create node
      var oNode = null;

      if (bMac) {
        oNode = poXml.responseXML.createElement("p");
        var newAtt = poXml.responseXML.createAttribute("name");
        newAtt.nodeValue = psName;
        oNode.setAttributeNode(newAtt);
        oNode.textContent = psValue;

      }else{
      var oNode = poXml.createElement("p");

      //set name attr
      oNode.setAttribute("name",psName);
      var cData = poXml.createCDATASection(psValue)  
      //set value
      //oNode.text = psValue;
      oNode.appendChild(cData);

    }
     
      return(oNode);
    }
  
  
});
var bMac = $ss.agentcore.utils.IsRunningInMacMachine();
