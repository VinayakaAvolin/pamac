$ss.snapin = $ss.snapin || {};
$ss.snapin.msgprocessor=$ss.snapin.msgprocessor || {};
$ss.snapin.msgprocessor.helper = $ss.snapin.msgprocessor.helper || {};

(function()
{
    
  $.extend($ss.snapin.msgprocessor.helper, 
  {
/********************************************************************************/
//All Public functions
/********************************************************************************/
  
  // Keeps track of the last modified folder date in sprt_msg.  This indicates whether a refresh is necessary
  g_latestSynchFolderDate : null,
  
  cl_msg_PeekMessages:function(oCmdLine)
  {
    try
    {
    var oPendingMsgs = {"sMsgXML" : "",
                        "nPendingCriticalMessages" : 0,
                        "nPendingAnyMessages"      : 0};
    // sync contents of messages.xml to filesystem
    var current_msgs_xml = this.cl_msg_getMessagesXml();
    oPendingMsgs.sMsgXML = this.cl_msg_updateMessagesXml(current_msgs_xml, false);
        
    //Use this for get other content type messages ...
    //oPendingMsgs.sMsgXML = this.GetMessagesOtherContentType(oPendingMsgs.sMsgXML,oCmdLine);
    
    oPendingMsgs.nPendingCriticalMessages = _cl_msg_getCountFor(XPATH_PENDING_CRITICAL_STRING, oPendingMsgs.sMsgXML);  
    oPendingMsgs.nPendingAnyMessages = _cl_msg_getCountFor(XPATH_PENDING_ANY_STRING, oPendingMsgs.sMsgXML);      
    return oPendingMsgs;
    }
    catch(err)
    {
      _logger.error("$ss.snapin.msgprocessor.helper.cl_msg_PeekMessages()",err.message);
             
    }  
  },
  /************************
   * This is used to get other type of contents 
   * @param {Object} oXml
   */
  GetMessagesOtherContentType: function(oXml,oCmdLine){

    if ($ss.snapin.msgViewer.Helper.IsCurrentEventSycnEvent()) {
      var supportedContentTypes = _config.GetValues("notification_content_types", "supported");
      var contentDisplayXML = {};
      //only supported for sync event type
      if (supportedContentTypes && supportedContentTypes.length) {
        for (var ix = 0; ix < supportedContentTypes.length; ix++) {
          var tmpContentType = supportedContentTypes[ix];
          var contentModel = _contentPopModels[tmpContentType];
          if (!contentModel) {
            _contentPopModels[tmpContentType] = new $ss.snapin.msg.ContentModel(tmpContentType);
            contentModel = _contentPopModels[tmpContentType];
            
          }
          contentDisplayXML[tmpContentType] = contentModel.GetDisplayXMLMessage();
          //if content is there update the sync event ...
          if (contentDisplayXML[tmpContentType]) {
            _hasContentData[tmpContentType] = true;
            var node = null;
            if (bMac) {
              node = _xml.SelectSingleNode(oXml.responseXML, "//sprt/msgs");
            }
            else {
              node = oXml.selectSingleNode("//sprt/msgs");
            }
            if(!node) {
                //no orignal XML 
                oXml = _xml.LoadXMLFromString(XML_CONTRACT_BASE, false);
            }
            var node1 = null;
            if (bMac) {
              node1 = _xml.SelectSingleNode(oXml.responseXML,"//sprt/msgs");
            }
            else {
              node1 = oXml.selectSingleNode("//sprt/msgs");
            }

            var node2 = null;
            if (bMac) {
              node2 = _xml.SelectSingleNode(contentDisplayXML[tmpContentType].responseXML, "//sprt/msgs");
            }
            else {
              node2 = contentDisplayXML[tmpContentType].selectSingleNode("//sprt/msgs");
            }
            if (bMac) {
            _xml.AugmentDoc(node1, node2, "guid", "//sprt/msgs/");
            }else {
            _xml.AugmentDoc(node1, node2, "guid", "//");
          }
          }
        }
      }
    }
    return oXml;
     
  },
  
  /*******************************************************************************
    ** Name:        cl_msg_prequal()
    **		
    ** Purpose:     Test to see if we need to display ourselves 
    **		
    ** Parameter:   none
    **               
    ** Return:      EVT type indicating what type of event we need to show
    *******************************************************************************/
    cl_msg_prequal:function(oCmdLine, oPendingMsgs)
    {
      // initialization
      var nEventTypeToShow = EVT_NONE;
      
      if (typeof(oCmdLine) == "undefined")
      {
        oCmdLine = _utils_ui.GetObjectFromCmdLine();
      }
     
      // depending on the command line, we have different display criteria  
      if (oCmdLine["triggerevent"] && (typeof(oCmdLine["triggerevent"]) == typeof("")))
      {
        /*
          triggerevent [2.0 Messages TechSpecs @1.7.1.2]:
          
          /triggerevent <guid>
          This flag from Sprocket means that a trigger was fired.  A SupportMessage
          guid must be provided as a parameter to this call.  The Snapin reads the
          SupportMessage from sprt_msg folder based on the GUID provided.   It updates
          messages.xml to include this SupportMessage in the list of messages.  Then 
          it notifies the user via the notification method specified by the user, and 
          Google Gadget is available.  The triggered SupportMessage is to be displayed 
          first.
        */  
        var triggerGuid = oCmdLine["triggerevent"];    
        nEventTypeToShow = (!this.cl_msg_isIgnored(oPendingMsgs.sMsgXML, triggerGuid)) ? EVT_TRIGGEREVENT : EVT_NONE;
      }
      else if (oCmdLine["syncevent"])
      {
        /*
          syncevent [2.0 Messages TechSpecs @1.7.1.2]:
          
          /syncevent
          This flag from Sprocket means that a synch occurred.  With this flag, the Snapin
          updates the messages.xml catalog file based on the SupportMessages stored in
          sprt_msg directory.  If there are any unexecuted messages, it displays them in the
          notification method specified by the user, and Google Gadget, if available. 
        */    
        nEventTypeToShow = (oPendingMsgs.nPendingCriticalMessages) ? EVT_SYNCEVENT : EVT_NONE;
        if(nEventTypeToShow === EVT_NONE) {
          nEventTypeToShow =  this.PrequalOtherContent(EVT_SYNCEVENT,oPendingMsgs);
        }
        
        this.cl_msg_updateSysTray(false);
      }    
      else if (oCmdLine["timerevent"])
      {
        /*
          timerevent [2.0 Messages TechSpecs @1.7.1.2]:

          /timerevent
          This flag from Sprocket means that time is due to display messages on a schedule basis.
          This schedule is default to every 7 days, but user may configure to a different interval.
          The Snapin reads messages.xml and if there are any unexecuted messages, it displays them
          in the notification method specified by the user, and Google Gadget, if available.
        */
        nEventTypeToShow = (oPendingMsgs.nPendingAnyMessages) ? EVT_TIMEREVENT : EVT_NONE;    
      }
      else if (oCmdLine["startevent"])
      {
        /* Only update status of systray icon */
        this.cl_msg_updateSysTray(false);
      }

      return nEventTypeToShow;
    },
    PrequalOtherContent:function(eventType, oPendingMsgs) {
      var showEvent = EVT_NONE;
      var defPrefType="disabled";
      defPrefType = this.GetUserPrefNotificationOnContent(eventType,defPrefType);
      if(defPrefType!=="disabled") showEvent = EVT_SYNCEVENT;
      return showEvent;
    },
    GetUserPrefNotificationOnContent: function(eventType,oPrefType){
       var bPrefType = oPrefType;
       var user=_config.ExpandSysMacro("%USER%");
       var regRoot = _config.GetRegRoot();
       var bPrefEnable=_reg.GetRegValue(_constants.REG_HKCU, regRoot+"\\users\\"+user+"\\ss_config\\Message", "enabled");      
       if(bPrefEnable==="False") return bPrefType;
       
       if (eventType == EVT_SYNCEVENT) {
         for (var key in _hasContentData) {
           if (_hasContentData[key]) {
            var prefValue = _config.GetConfigValue("pop_notification", key + "_method", "popup");
             //if it is disabled only then over ride
            switch(prefValue) {
              case "popup":
                bPrefType = "popup"
                break;
              case "systray":
                if(bPrefType!=="popup") bPrefType = "systray";
                break;
              default:
                break;    
            }
          }
        }
      } 
      return bPrefType;
    },
    
  /*******************************************************************************
    ** Name:        cl_msg_updateSysTray
    **		
    ** Purpose:     Based on the latest FS and messages.xml, determines if what icon
    **              systray needs to show
    **		
    ** Parameter:   none
    **              
    ** Return:      none   
    *******************************************************************************/
    cl_msg_updateSysTray:function(bCheckPref)
    {    
      try 
      {
        // sync contents of messages.xml to filesystem, but do not write to FS
        var oPendingMsgs = this.cl_msg_PeekMessages();
        // get user's prefs.  
        var sPref = this.getNotificationMethod();
        if (!bCheckPref || sPref == "popup")
          this.cl_msg_notifyTray(oPendingMsgs, false);
        }
      catch(err) 
      {
         _logger.error("$ss.snapin.msgprocessor.helper.cl_msg_updateSysTray()",err.message);
      }
    },
    
    cl_msg_setTimeoutOnSystray:function()
    {
     setTimeout(function(){$ss.snapin.msgprocessor.helper.cl_msg_updateSysTray(false);},50);
    },
    
    /*******************************************************************************
    ** Name:        getNotificationMethod
    **		
    ** Purpose:     Returns the notification method.
    **		
    ** Parameter:   none
    **              
    ** Return:      none   
    *******************************************************************************/ 
    getNotificationMethod:function()
    {
      try
      {
        var user=_config.ExpandSysMacro("%USER%");
        var sPref="systray"; //default value
        var regRoot = _config.GetRegRoot();
        var bSystray=_reg.GetRegValue(_constants.REG_HKCU, regRoot+"\\users\\"+user+"\\ss_config\\Message", "systray");
        var bPrefEnable=_reg.GetRegValue(_constants.REG_HKCU, regRoot+"\\users\\"+user+"\\ss_config\\Message", "enabled");      
      
        var pathFirstRun = regRoot + "\\users\\" + user + "\\ss_config\\firstrun\\";
        var bForceFirstRun = (_config.GetConfigValue("firstrun", "force_first_run", "true") === "true");
        var firstRunCompleted = "true";
        if (bForceFirstRun) {
          firstRunCompleted = _reg.GetRegValue("HKCU", pathFirstRun, "FirstRunCompleted");
        }

      if(bPrefEnable=="True" && bSystray=="True")
    {
        if (!_isTrayIconPresent() && firstRunCompleted=="true")
        sPref = "popup";
      }
      else if(bPrefEnable=="True" && bSystray=="False")
         sPref="popup"; 
      else if(bPrefEnable=="False")// || bSystray=="False")
        sPref ="disabled"; 
     
      return sPref.toLowerCase().trim();
      }
      catch(err)
      {
        _logger.error("$ss.snapin.msgprocessor.helper.getNotificationMethod()",err.message);
      }
    },

     
  /*******************************************************************************
  ** Name:        cl_msg_getMessagesXml
  **		
  ** Purpose:     gets the xmldom with messages.xml - this function will create a
  **		skeleton messages.xml if file not found
  **		
  ** Parameter:   none
  **              
  ** Return:      xmldom
  *******************************************************************************/
  cl_msg_getMessagesXml:function()
  {
    //store return
    var oXml = null;
    //get path to messages.xml
    var sMessagesXml =this.cl_msg_getMessageXMLFilePath();
    // if messages.xml doesn't exist, create a skeleton one and save it
    if (!_file.FileExists(sMessagesXml))
    {
      //load up some skeleton xml    
      oXml = _xml.LoadXMLFromString(XML_CONTRACT_BASE, false);
      //save skeleton as new messages.xml

    if (bMac) {
      var serializer = new XMLSerializer();
      var contents = serializer.serializeToString(oXml.responseXML);
      this.cl_msg_saveToFile(contents, sMessagesXml);
    }
    else {
      this.cl_msg_saveToFile(oXml.xml, sMessagesXml);
    }

    }

    //on error we'll create
    try
    {
      //try to load message.xml 
      //oXml = _xml.LoadXML(sMessagesXml,false);
      oXml = null;
      oXml =_xml.LoadXML(sMessagesXml,false);
    }
    catch(oErr)
    {
      //load up some skeleton xml    
      oXml = _xml.LoadXMLFromString(XML_CONTRACT_BASE,false);
      var saveStatus = false;
      if (bMac) {
        var serializer = new XMLSerializer();
        var contents = serializer.serializeToString(oXml.responseXML);
        saveStatus = this.cl_msg_saveToFile(contents, sMessagesXml);
      }
      else {
        saveStatus = this.cl_msg_saveToFile(oXml.xml, sMessagesXml);
      }
      if (saveStatus)
      {
        //try loading again    
        oXml = _xml.LoadXML(sMessagesXml,false);    
      }    
    }
    
     oXml = this.GetMessagesOtherContentType(oXml);
    return(oXml);
  },
    
    /*******************************************************************************
    ** Name:        cl_msg_updateMessagesXml
    **		
    ** Purpose:     checks sprt_msg/subdirs and looks for <guid>.<version> dirs and builds 
    **		/updates message.xml with the date in these directories - message.xml
    **		is the "catalog" file that will be loaded later and will help us quickly
    **		list out the messages. 
    **		
    ** Parameter:   poXml - xml object, xmldom with message.xml
    **              bSave - boolean indicates if we should save consolidated content
    **                      to disk
    **
    ** Return:      consolidated XMLDocument object
    *******************************************************************************/
    cl_msg_updateMessagesXml:function(poXml, bSave)
    {
      // create session id - use to strip out any msg nodes not found on filesystem
      var sSessionId = _cl_msg_getSessionId();  
      
      //var objJson="";
      var messages = _content.GetContentsByAnyType(["sprt_msg"]);

      var sContGuid = "";
      var sContTitle = "";
      var sContVersion = '0';
      var sContLangId = "";  
      
      if (!g_oFso) g_oFso = _file.GetFileSystemObject();
      var oFolder = "";
      var sDateReceived;
      
      try {

        //add the metadata needed for xsl transformation
        if (bSave) _cl_msg_insertMetaDataNode(sSessionId, poXml);
               
        for(var i=0;i<messages.length;i++)
        {
          _utils.Sleep(10);
          //may be used in future
         // objJson = _content.GetContentDetailsAsJSON("sprt_msg", messages[i]["cid"], messages[i]["version"]);
          
          sContGuid = messages[i].cid;
          sContVersion = messages[i].version;
          sContTitle = messages[i].title;
          sContLangId = messages[i].language;
           var folderPath = this.cl_msg_getBuildPath(this.cl_msg_getMessagesPath(),sContGuid+"."+sContVersion);
          //is this message already in messages.xml?
          var oTmpNode = null;
          if (bMac) {
            oTmpNode = _cl_msg_getNode(poXml, this.cl_msg_getXPathForMsgGuid(sContGuid));
              sDateReceived = new Date(g_oFso.GetModifiedDate(folderPath));
          }
          else {
            oTmpNode = _cl_msg_getNode(poXml, this.cl_msg_getXPathForMsgGuid(sContGuid));
           // Grab the latest date.  This will be stored in the xml file for an insert.  It will also be used to tell whether refreshes are necessary
          oFolder = g_oFso.GetFolder(this.cl_msg_getBuildPath(this.cl_msg_getMessagesPath(),sContGuid+"."+sContVersion));
		      sDateReceived = new Date(oFolder.DateLastModified);
          }
          
    		  
		      if (!this.g_latestSynchFolderDate || sDateReceived > this.g_latestSynchFolderDate) 
		      {		
		        this.g_latestSynchFolderDate = sDateReceived;
		      }
        
		      //code merge
		      var tv = '0';
        
          //if oTmpNode is not false then we already know about msg so just update some properties
          if (oTmpNode)
          {
             tv = oTmpNode.getAttribute('tv');
             var isInMessageList=$ss.agentcore.dal.databag.GetValue("db_msg_MessageList");
             if (isInMessageList) 
             {
                if (tv != '1' && oTmpNode.getAttribute('t') != '1') 
                {
                  oTmpNode.setAttribute('tv', '1');
                  if (bSave) this.cl_msg_updateTitleViewReport(sContGuid, sContVersion);
                }
             }
            
            //update some properties and get these properties from sprt_msg\<guid>.<version>\<guid>.<version>.xml
            if (bMac) {
              _cl_msg_updateNewMsgNode(sSessionId, oTmpNode,folderPath+"\\"+sContGuid+"."+sContVersion+".xml", sContVersion);    
            }else {
            _cl_msg_updateNewMsgNode(sSessionId, oTmpNode,oFolder.path+"\\"+sContGuid+"."+sContVersion+".xml", sContVersion);
          }
            
          }
          else
          {    
            //we got a message we didn't know about so let's insert a new msg node and get the details from sprt_msg\<guid>.<version>\<guid>.<version>.xml
            if (bMac) {
              _cl_msg_insertNewMsgNode(sSessionId, poXml, folderPath+"\\"+sContGuid+"."+sContVersion+".xml", sContGuid, sContVersion, this.cl_msg_getTs(sDateReceived),bSave);
            }
            else{
              _cl_msg_insertNewMsgNode(sSessionId, poXml, oFolder.path+"\\"+sContGuid+"."+sContVersion+".xml", sContGuid, sContVersion, this.cl_msg_getTs(sDateReceived),bSave);              
            }
            if (bSave) this.cl_msg_updateDownloadReport(sContGuid, sContVersion);
            
          }
 
        } 
   
      } 
      catch (err) 
      { 
        return null; 
      }
        
      //added to support non content type xml ...
 var tempPoxmlObj = null;
 var otherContentNodes = null;
    if (bMac) {
      var serializer = new XMLSerializer();
      var contents = serializer.serializeToString(poXml.responseXML);
      var tempPoxmlObj = _xml.LoadXMLFromString(contents,false);
      otherContentNodes =  _xml.SelectNodes(tempPoxmlObj.responseXML, XPATH_MSG_STRING+"[@not_sprt_msg='1']");
    }else {
      var tempPoxml = poXml.xml;
      var tempPoxmlObj = _xml.LoadXMLFromString(tempPoxml,false);
      otherContentNodes =  tempPoxmlObj.selectNodes(XPATH_MSG_STRING+"[@not_sprt_msg='1']");
    }
     
      
      
      
      //each msg node in messages.xml that we touched we set the sid attr with session id
      //any msg nodes without this sid attr=sSessionId is no longer on FS so just remove them  
      
      // KL (7/15/07): Also keep the "ignored" nodes around in case a client falls back into a collection and re-download the same message,
      //               we still want to remember that this message was previously "ignored"
     if (bMac) {
        var nodes = _xml.SelectNodes(poXml.responseXML, XPATH_MSG_STRING + "[@sid!='"+sSessionId+"' and p[@name='deleted_dt']='']");
        if (nodes) {
          for(var i = 0;  i< nodes.length ; i++) {
            var aNode = nodes[i];
            aNode.parentNode.removeChild(aNode);
          }
         }
      }else {
      poXml.selectNodes(XPATH_MSG_STRING + "[@sid!='"+sSessionId+"' and p[@name='deleted_dt']='']").removeAll();
    }
      var oMsgsNodes = _cl_msg_getNode(poXml,"/sprt/msgs");
      if (otherContentNodes) {
        for (var iN = 0; iN < otherContentNodes.length; iN++) {
          oMsgsNodes.appendChild(otherContentNodes[iN]);
        }
      }
      
      
      if (bSave)
      {
        //save the the updated message.xml to file
        var saveStatus = false;
        if (bMac) {
          var serializer = new XMLSerializer();
          var writetofile = serializer.serializeToString(poXml.responseXML);
          saveStatus = this.cl_msg_saveToFile(writetofile, this.cl_msg_getMessageXMLFilePath());
        }
        else {
          saveStatus = this.cl_msg_saveToFile(poXml.xml, this.cl_msg_getMessageXMLFilePath());

        }
        if (!saveStatus)
        {
          try 
          {
            _logger.error("cl_msg_updateMessagesXml()", "Failed to save messages.xml");
          } 
          catch (ex) 
          {
          }
        }
      }
      
      return poXml;
    },

    /*******************************************************************************
    ** Name:        cl_msg_getXPathForMsgGuid
    **		
    ** Purpose:     returns a string that is an xpath to the msg node for the psMsgGuid
    **		
    ** Parameter:   psMsgGuid, string, guid of msg that you want the xpath built for
    **              
    ** Return:      xpath string for with psMsgGuid in query (for use to retrieve msg 
    ** 		nodes from messages.xml)
    **		"/sprt/msgs/msg[@guid='<psMsgGuid>']"
    *******************************************************************************/
    cl_msg_getXPathForMsgGuid:function(psMsgGuid)
    {
      //take: "/sprt/msgs/msg[@guid='"+MACRO_MESSAGE_MACRO+"']";
      //return back MACRO_MESSAGE_STRING where we replace the string 
      //MACRO_MESSAGE_MACRO with psMsgGuid
      return (MACRO_MESSAGE_STRNG.replace(MACRO_MESSAGE_MACRO, psMsgGuid));
    }, 
   
    /*******************************************************************************
    ** Name:        cl_msg_notifyTray()
    **		
    ** Purpose:     calls to set tray icon status - 
    **              message sent is determined by what's in tray_config.xml, currently:
    **              "default", "UrgentMessage","UrgentMessageBlink","NewMessage" and "NewMessageBlink"
    **		
    ** Parameter:   
    **               
    ** Return:      none
    **
    ** Note:	assumption is that you don't call this function unless you know
    ** 		the icon needs to be displayed...I'll sort of check what kind of message
    **		to send but you got make sure I need to send one else I'll fire
    **		off regardless unless a correct pnCEvent isn't passed
    **
    *******************************************************************************/
    cl_msg_notifyTray:function(oPendingMsgs, bBlink)
    {  
      //what kind of message are we sending out
      var sMessage = "";
     
      if (oPendingMsgs.nPendingCriticalMessages)
      {
        sMessage = MSG_TRAY_URGENTMESSAGE;
      }
      else if(oPendingMsgs.nPendingAnyMessages)
      {
        sMessage = MSG_TRAY_NEWMESSAGE;
      }
      else
      {
        sMessage = MSG_TRAY_DEFAULT;
        bBlink = false;
      }
      
      if (bBlink)
      {
        sMessage = sMessage + MSG_TRAY_BLINK;
      }
      
      _cl_msg_fireAgentEvent(sMessage);  
    },
    /*******************************************************************************
    ** Name:        cl_msg_isIgnored()
    **		
    ** Purpose:     determines if the content was ignored or not
    **		
    ** Parameter:   none
    **               
    ** Return:      true if it's previously ignored
    *******************************************************************************/
    cl_msg_isIgnored:function(oXml, guid)
    {
      var bIgnored = true;  
      var oNode = null;
      if (bMac) {
        oNode = _xml.SelectSingleNode(oXml.responseXML, this.cl_msg_getXPathForMsgGuid(guid));
      }else {
        oNode = oXml.selectSingleNode(this.cl_msg_getXPathForMsgGuid(guid));
      }
      
      if (oNode)
      {
        bIgnored = (oNode.selectSingleNode("p[@name='deleted_dt']").text != "");
      }
      
      return bIgnored;
    },
    /*******************************************************************************
    ** Name:        cl_msg_createNewPropertyCDataNode
    **		
    ** Purpose:     creates a new p (property) node and sets value as cdata
    **              in xml object
    **		
    ** Parameter:   poXml, xml object, xml object (needed to create element)
    **		psName, string, used to set name attribute
    **		psValue, string, value to set
    **              
    ** Return:      node created
    *******************************************************************************/
    cl_msg_createNewPropertyCDataNode:function(poXml,psName,psValue)
    {
      //store return, create node
      var oNode = null;
      if (bMac) {
        oNode = poXml.responseXML.createElement("p");
        var newAtt = poXml.responseXML.createAttribute("name");
        newAtt.nodeValue = psName;
        oNode.setAttributeNode(newAtt);
        oNode.textContent = psValue;
      }
      else {
       oNode = poXml.createElement("p");

      //create cdata section with psValue
      var oCdata = poXml.createCDATASection(psValue);

      //set name attr
      oNode.setAttribute("name",psName);

      //append cdata node
      oNode.appendChild(oCdata);
    }

      return(oNode);
    },
    
    /*******************************************************************************
    ** Name:        cl_msg_getMessageUrl
    **		
    ** Purpose:     gets message url from <guid>.<version>.xml file for a particular
    **		message
    **		
    ** Parameter:   psGuid, string, guid of message
    **		psVersion, string, version of message
    **              
    ** Return:      url for that particular message (string)
    *******************************************************************************/
    cl_msg_getMessageUrl:function(psGuid,psVersion)
    {
      //load the /path/<guid>.<version>.xml for psGuid.psVersion message and get the URL
      var oXml = _xml.LoadXML(this.cl_msg_getBuildPath(this.cl_msg_getBuildPath(this.cl_msg_getMessagesPath(),psGuid+"."+psVersion),psGuid+"."+psVersion+".xml"));
      return(_cl_msg_getNodeValue(oXml,XPATH_TELLMEURL_STRING));
    },
    /*******************************************************************************
    ** Name:        cl_msg_getMessageXMLFilePath
    **		
    ** Purpose:     builds path string to the message.xml
    **		
    ** Parameter:   none
    **              
    ** Return:      path string to messages.xml
    *******************************************************************************/
    cl_msg_getMessageXMLFilePath:function()
    {
      if (!g_sMessagesPath)
      {
        //set global path to messages.xml
        g_sMessagesPath = this.cl_msg_getBuildPath(this.cl_msg_getMessagesPath(),TO_MESSAGES_XML);
      }  
      
      //return back path string to messages.xml
      return g_sMessagesPath;
    },
    /*******************************************************************************
    ** Name:        cl_msg_saveToFile
    **		
    ** Purpose:     saves psData to psFilePath - wrapper calll to ss_con_WriteNewFile
    **		- this will overwrite psFilePath
    **		
    ** Parameter:   psData, string, data to write
    **		psFilePath, string, path to file
    **              
    ** Return:      none
    *******************************************************************************/
    cl_msg_saveToFile:function(psData,psFilePath)
    {
      var tempPoxmlObj = _xml.LoadXMLFromString(psData,false);
      
      if (bMac) {
        var nodes = _xml.SelectNodes(tempPoxmlObj.responseXML, XPATH_MSG_STRING + "[@not_sprt_msg='1']");
        if (nodes) {
          for(var i = 0;  i< nodes.length ; i++) {
            var aNode = nodes[i];
            aNode.parentNode.removeChild(aNode);
          }
        }
        var serializer = new XMLSerializer();
        var contents = serializer.serializeToString(tempPoxmlObj.responseXML);
        return _file.WriteNewFile(psFilePath, contents);
      }else {
        tempPoxmlObj.selectNodes(XPATH_MSG_STRING+"[@not_sprt_msg='1']").removeAll();
      return _file.WriteNewFile(psFilePath,tempPoxmlObj.xml);
      }
      
    },
        /*******************************************************************************
    ** Name:        cl_msg_show
    **		
    ** Purpose:     Shows the window, as well as notifying big bcont that it is opening
    **		
    ** Parameter:   none
    **              
    ** Return:      none   
    *******************************************************************************/ 
    cl_msg_show:function()
    {
      var sInstanceName=$ss.agentcore.dal.ini.GetIniValue("", "PROVIDERINFO", "instancename", "");
      // only notify if mini bcont is running
       if(sInstanceName=="minibcont")
       { 
         if (!bMac) {
         this.cl_msg_SendBcontMsg(BIG_BCONT,_constants.BCONT_MINI_RUNNING);
       }
       }
       
      //_AnimateShowWindow();
      
      // fix the problem for Double underlined links issue.
      var instance = _ini.GetIniValue("", "PROVIDERINFO", "instancename", "");
      if (instance == "minibcont")
      {
        _utils_ui.HideBcont();
        _utils_ui.FocusBcont();  
      }  
    } ,
    
    cl_msg_SendBcontMsg:function(bcontType, msgToSend,sPath)
    {
      var bcontInstance = "";
      
      if(!sPath) {
        sPath = _config.GetConfigValue("preconfigured_snapin_url", "supportmessage_list");
      }
      if (bcontType == BIG_BCONT)
      {
        bcontInstance = _config.GetConfigValue("minishell", "app_container_instance","bigbcont");
      }
      else
      { 
        bcontInstance = _config.GetConfigValue("minishell", "msg_container_instance","minibcont");
      }


      if((bcontInstance != "") && (bcontInstance != undefined))
      {
        if(_activex.CheckInstance(_config.GetBcontMutexPrefix() + bcontInstance))
        {
          var bcontExe = "";
          var bcontArgs = "";

          if (bcontType == BIG_BCONT)
          {
            bcontExe = _ini.GetPath("EXE_PATH");
            bcontArgs = "/ini \"" + _config.GetProviderRootPath() + "\agent\\bin\\bcont_nm.ini \"  /entry Minibcont /p " + _config.GetProviderID() + " /path " + "\""+ sPath +"\"";
          }
          else
          {
            bcontExe = _ini.GetPath("EXE_PATH");
            bcontArgs = "/ini "+_config.GetProviderRootPath()+"\agent\\bin\\minibcont.ini /entry Bigbcont";
          }

          if((bcontExe != "") && (bcontExe != undefined))
          {
            this.cl_msg_runCommand(bcontExe, bcontArgs + " " + msgToSend, 4);
          }
        };
      }
    },
    /*******************************************************************************
    ** Name:        cl_msg_runCommand
    **		
    ** Purpose:     wrapper call - calls ss_con_RunCommand
    **		
    ** Parameter:   psCmd, string, command to run
    **		pnMode, integer:  0: Normal, 1: Minimized, 2: Hidden, 4: Async
    **              
    ** Return:      From cl_msg_runCommand: "number   process exit code (0 if Async specified)"
    *******************************************************************************/
    cl_msg_runCommand:function (sCmd, sArgs, nMode)
    {
      if (sCmd.indexOf('"') != 0)
      {
        sCmd = '"' + sCmd + '"';
      }
      sCmd = sCmd + " " + sArgs;  
      
      return(_utils.RunCommand(sCmd, nMode));
    },
    
    cl_msg_runSupportAction:function(psProvider,psCabFile)
    {
        var oSR=new $ss.agentcore.supportaction("",psProvider,psCabFile);
        return oSR.Evaluate(); 
            
    },

     EvaluateCompleted: function(retValue) {
        bSuccess = IsExecutionSuccessful(retValue, payloadHelperObject.psGuid, payloadHelperObject.psVersion);
        payloadHelperObject.helper.cl_msg_ExecPayloadHelperCompleted(bSuccess);
    },
    /*******************************************************************************
    ** Name:        cl_msg_updateDeleteReport
    **		
    ** Purpose:     update a deleted report information for reporting
    **		
    ** Parameter:   guid of the message
    **              version of the message
    **              
    ** Return:      none   
    *******************************************************************************/ 
      cl_msg_updateDeleteReport:function(psGuid, psVersion) 
      {
        var oLogEntry = $ss.agentcore.reporting.Reports.CreateLogEntry(psGuid,psVersion,"msg_deleted", 1,     "",       "",        "",    "",_constants.RESULT_SUCCESS);
        return(this.cl_msg_ContentUsage(oLogEntry,false));
       },
     /*******************************************************************************
    ** Name:        cl_msg_ContentUsage
    **		
    ** Purpose:     Logs the log entry
    **		
    ** Parameter:   guid of the message
    **              version of the message
    **              
    ** Return:      none   
    *******************************************************************************/    
       cl_msg_ContentUsage:function(oLogEntry,bDoSurvey)
       {
          if(typeof(bDoSurvey) == "undefined") 
            bDoSurvey = true;
            
          if(!bDoSurvey || !(_config.GetConfigValue("global", "show_survey") == "true") )
            return  _cl_msg_pvt_ContUsage(  oLogEntry.guidContent,
                                            oLogEntry.version,
                                            oLogEntry.verb,
                                            oLogEntry.viewed,
                                            oLogEntry.solved,
                                            oLogEntry.comment,
                                            oLogEntry.data,
                                            oLogEntry.rating,
                                            oLogEntry.result );

    },
    
    /*******************************************************************************
    ** Name:        cl_msg_CloseMiniBcont
    **		
    ** Purpose:     Send minibcont a message to close
    **		
    ** Parameter:   none
    **              
    ** Return:      none   
    *******************************************************************************/ 
    cl_msg_CloseMiniBcont:function ()
    {
      var bcontInstance = _config.GetConfigValue("bigshell", "msg_container_instance","minibcont");
      var bcontExe = _ini.GetPath("EXE_PATH");//_config.GetConfigValue("shell", "msg_container_exe");
      var bcontArgs ="/ini \""+_config.GetProviderRootPath()+"\agent\\bin\\minibcont.ini\"";//_config.GetConfigValue("shell", "msg_container_args");	 
      try 
	    {
	      // Ideally it should check for existing mutex.  Should it fail, then just invoke command to close.
	      if (_activex.CheckInstance(_config.GetBcontMutexPrefix() + bcontInstance)) 
	      {
	        this.cl_msg_runCommand(bcontExe, bcontArgs + " " + _constants.BCONT_CLOSE, _constants.BCONT_RUNCMD_ASYNC);
	      }
	    } 
	    catch(e) 
	    {
	      this.cl_msg_runCommand(bcontExe, bcontArgs + " " + _constants.BCONT_CLOSE, _constants.BCONT_RUNCMD_ASYNC);
	    }
    },
    /*******************************************************************************
    ** Name:        updateXMLValue
    **		
    ** Purpose:     Generic utility file to update any value in the XML, i.e. showndate and payload status
    **		
    ** Parameter:   psGuid, string, guid of message we want to update 
    **              psField, string, field we want to update
    **              
    ** Return:      none
    **
    ** Notes:	using custom select nodes because poXml may need Xpath set instead
    **		of xslpattern
    *******************************************************************************/
    updateXMLValue:function (psGuid, psField, psValue)
    { 
      //get the message.xml as dom
      var oXml = this.cl_msg_getMessagesXml();

      //get msg node for psGuid
      var oNode = null;
      if (bMac) {
          oNode = _xml.SelectSingleNode(oXml.responseXML, this.cl_msg_getXPathForMsgGuid(psGuid));
      }else {
          oNode = oXml.selectSingleNode(this.cl_msg_getXPathForMsgGuid(psGuid));
      }
      
      var bSaveSuccess = false;

      //if we got back a node
      if (oNode)
      {
        //set the shown property with current timestamp
        if (bMac) {
          var node = _xml.SelectSingleNodeFromNode(oNode,'name',psField);
              if (node) {
                 node.textContent = psValue;
              }
            }else {
        oNode.selectSingleNode("p[@name='" + psField + "']").text = psValue;
            }

        //save message.xml to file
        if (bMac) {
          var serializer = new XMLSerializer();
          var contents = serializer.serializeToString(oXml.responseXML);
          bSaveSuccess = this.cl_msg_saveToFile(contents, this.cl_msg_getMessageXMLFilePath());
        }else{
        bSaveSuccess = this.cl_msg_saveToFile(oXml.xml, this.cl_msg_getMessageXMLFilePath());
      }
      }
      if(bSaveSuccess) 
        return oXml; 
      else
        return null;   
     
    },
    /*******************************************************************************
    ** Name:        updateXMLAttribute
    **		
    ** Purpose:     Generic utility function to update any attribute value in the XML
    **		
    ** Parameter:   psGuid, string, guid of message we want to update 
    **              psField, string, field we want to update
    **              
    ** Return:      none
    **
    ** Notes:	using custom select nodes because poXml may need Xpath set instead
    **		of xslpattern
    *******************************************************************************/
    updateXMLAttribute:function(psGuid, psField, psValue)
    {
      //get the message.xml as dom
      var oXml = this.cl_msg_getMessagesXml();

      //get msg node for psGuid
      var oNode = null;
      if (bMac) {
       oNode = _xml.SelectSingleNode(oXml.responseXML, this.cl_msg_getXPathForMsgGuid(psGuid));

      }else {
       oNode = oXml.selectSingleNode(this.cl_msg_getXPathForMsgGuid(psGuid));
      }
      
      var bSaveSuccess = false;

      //if we got back a node
      if (oNode)
      {
        oNode.setAttribute(psField,psValue);  
        //save message.xml to file
 
          if (bMac) {
          var serializer = new XMLSerializer();
          var writetofile = serializer.serializeToString(oXml.responseXML);
          bSaveSuccess = this.cl_msg_saveToFile(writetofile, this.cl_msg_getMessageXMLFilePath());
        }else {
        bSaveSuccess = this.cl_msg_saveToFile(oXml.xml, this.cl_msg_getMessageXMLFilePath());
      }
      }
      if(bSaveSuccess) 
       return oXml; 
      else 
       return null; 
           
    },
     /*******************************************************************************
    ** Name:        cl_msg_getBuildPath
    **		
    ** Purpose:     will return path string
    **		
    ** Parameter:   psParent, string, parent path
    **		psChild, string, child path to be appended to psParent
    **              
    ** Return:      path string
    *******************************************************************************/
    cl_msg_getBuildPath:function(psParent,psChild)
    {
      return(_file.BuildPath(psParent,psChild));
    },
    
      /*******************************************************************************
    ** Name:        cl_msg_getMessagesPath
    **		
    ** Purpose:     gets path to user's sprt_msg folder
    **		
    ** Parameter:   none
    **              
    ** Return:      path string to user' sprt_msg folder
    *******************************************************************************/
    cl_msg_getMessagesPath:function()
    {
      //call api - get appdata/profile for current user
      var sParent = _config.GetContextValue("SdcContext:DirUserServer")
      //return the built path to sprt_msg
      return(this.cl_msg_getBuildPath(sParent,TO_MESSAGES_PATH));
    },
    
      
    /*******************************************************************************
    ** Name:         AnimateHideWindow
    **
    ** Purpose:      Animates the container window while hiding (slide/roll, alpha blend, etc.)
    **
    ** Parameter:    none
    **
    ** Return:       none
    *******************************************************************************/
    AnimateHideWindow:function()
    {
      try
      {
        var animateTime  = _config.GetConfigValue("minishell", "animate_time", 500);
        var animFlags = _GetAnimateFlags(false);
         _activex.GetObjInstance().Animate(animateTime, animFlags | g_con_animateOpts["AW_HIDE"]);
      }
      catch (err)
      {
        _logger.error("$ss.snapin.msgprocessor.helper.AnimateHideWindow()", err.message);
      }
    },

    cl_msg_ExecPayloadHelperMac: function(helper,psGuid, psVersion, pCType, sContentType) {
      payloadHelperObject = new Object();
      payloadHelperObject.helper = helper;
      payloadHelperObject.psGuid = psGuid;
      payloadHelperObject.psVersion = psVersion;
      payloadHelperObject.pCType = pCType;

      var sAction = ""
      var sViewlet = "";
      var bSuccess = true;

      var supportedContentTypes = _config.GetValues("notification_content_types", "supported");
      if (supportedContentTypes && supportedContentTypes.length) {
        for (var ct = 0; ct < supportedContentTypes.length; ct++) {

          if (sContentType === supportedContentTypes[ct]) { //found other content types other than sprt_msg...
            var configUrl = _config.GetConfigValue("notification_" + sContentType, "on_click_url_name");
            var configPath = null;
            if (configUrl) {
              configPath = _config.GetConfigValue("preconfigured_snapin_url", configUrl)
              if (configPath) {
                $ss.snapin.msgViewer.Helper.LaunchBcont(configPath, "", "viewall");
              }
            }
            return helper.cl_msg_ExecPayloadHelperCompleted(false); //could not find the preconfigured url...
          }
        }
      } 
      switch(pCType)
      {
         //supportaction = 0
        case TYPE_SUPPORTACTIONONLY:
        //get path to the action cab by looking it up in the msgs xml
        sAction=this.cl_msg_getBuildPath(this.cl_msg_getBuildPath(this.cl_msg_getMessagesPath(),psGuid+"."+psVersion),this.cl_msg_getValueFrom(psGuid,psVersion,XPATH_SUPPORTACTION_STRING));
        this.cl_msg_runSupportAction(_config.GetProviderID(),sAction);
        break;

         //viewlet = 1
      case TYPE_VIEWLETONLY:
        
        //get path to the viewlet cab by looking it up in the msgs xml
        sViewlet=this.cl_msg_getBuildPath(this.cl_msg_getBuildPath(this.cl_msg_getMessagesPath(),psGuid+"."+psVersion),this.cl_msg_getValueFrom(psGuid,psVersion,XPATH_VIEWLET_STRING));
        //run as async - nothing to do with return on viewlet
        this.cl_msg_runCommand(sViewlet, "", 4);
        payloadHelperObject.helper.cl_msg_ExecPayloadHelperCompleted(bSuccess);
        break;

         //viewlet and supportaction = 2
        case TYPE_VIEWLETACTION:
        
        //get path to the action cab by looking it up in the msgs xml
        sAction=this.cl_msg_getBuildPath(this.cl_msg_getBuildPath(this.cl_msg_getMessagesPath(),psGuid+"."+psVersion),this.cl_msg_getValueFrom(psGuid,psVersion,XPATH_SUPPORTACTION_STRING));
        var oSR=new $ss.agentcore.supportaction(this, psGuid,_config.GetProviderID(),sAction);
        // Control may be null if user cancels UAC prompt, in which case, don't show the Viewlet
        if (oSR != null) 
        {
          //get path to the viewlet cab by looking it up in the msgs xml
          sViewlet=this.cl_msg_getBuildPath(this.cl_msg_getBuildPath(this.cl_msg_getMessagesPath(),psGuid+"."+psVersion),this.cl_msg_getValueFrom(psGuid,psVersion,XPATH_VIEWLET_STRING));
          //call run command on viewlet with async so that scriptrunner comes immediately after
          this.cl_msg_runCommand(sViewlet, "", 4);
        }
        //run cab for supportaction
         oSR.Evaluate(psGuid, sAction); 
        break;

         //open tell me url = 3
      case TYPE_TELLME:
        var bConnStatus = $ss.agentcore.network.inet.GetConnectionStatus();
        if(bConnStatus)
        {
          var retVal = _utils_ui.RunDefaultBrowser(_config.ParseMacros(this.cl_msg_getValueFrom(psGuid,psVersion,XPATH_TELLMEURL_STRING)), null);
          if ( retVal == -1 )
          {
            alert(_utils.LocalXLate("snapin_messagespopup", "cl_browser_disabled"));
            bSuccess = false;
          }
        }  
        else  
        {
          alert(_utils.LocalXLate("snapin_messagespopup", "cl_msg_offline"));
          _databag.SetValue("INTERNET_FAILED", true);
          bSuccess = false;
        }
        helper.cl_msg_ExecPayloadHelperCompleted(bSuccess);
        break;
      }  
    },

    cl_msg_ExecPayloadHelper: function(psGuid, psVersion, pCType, sContentType)    //added sContentType for other contentType..
    {
      var sAction = ""
      var sViewlet = "";
      var bSuccess = true;

      var supportedContentTypes = _config.GetValues("notification_content_types", "supported");
      if (supportedContentTypes && supportedContentTypes.length) {
        for (var ct = 0; ct < supportedContentTypes.length; ct++) {

          if (sContentType === supportedContentTypes[ct]) { //found other content types other than sprt_msg...
            var configUrl = _config.GetConfigValue("notification_" + sContentType, "on_click_url_name");
            var configPath = null;
            if (configUrl) {
              configPath = _config.GetConfigValue("preconfigured_snapin_url", configUrl)
              if (configPath) {
                $ss.snapin.msgViewer.Helper.LaunchBcont(configPath, "", "viewall");
              }
            }
            return false; //could not find the preconfigured url...
          }
        }
      }      
      
      
      switch(pCType)
      {    
        //supportaction = 0
      case TYPE_SUPPORTACTIONONLY:
        //get path to the action cab by looking it up in the msgs xml
        sAction=this.cl_msg_getBuildPath(this.cl_msg_getBuildPath(this.cl_msg_getMessagesPath(),psGuid+"."+psVersion),this.cl_msg_getValueFrom(psGuid,psVersion,XPATH_SUPPORTACTION_STRING));
        var retValue = this.cl_msg_runSupportAction(_config.GetProviderID(),sAction);
        bSuccess = IsExecutionSuccessful(retValue, psGuid, psVersion);
        break;	
        
        //viewlet = 1
      case TYPE_VIEWLETONLY:
        
        //get path to the viewlet cab by looking it up in the msgs xml
        sViewlet=this.cl_msg_getBuildPath(this.cl_msg_getBuildPath(this.cl_msg_getMessagesPath(),psGuid+"."+psVersion),this.cl_msg_getValueFrom(psGuid,psVersion,XPATH_VIEWLET_STRING));
        //run as async - nothing to do with return on viewlet
        this.cl_msg_runCommand(sViewlet, "", 4);
        break;
        
        //viewlet and supportaction = 2
      case TYPE_VIEWLETACTION:
        
        //get path to the action cab by looking it up in the msgs xml
        sAction=this.cl_msg_getBuildPath(this.cl_msg_getBuildPath(this.cl_msg_getMessagesPath(),psGuid+"."+psVersion),this.cl_msg_getValueFrom(psGuid,psVersion,XPATH_SUPPORTACTION_STRING));
        var oSR=new $ss.agentcore.supportaction("",_config.GetProviderID(),sAction);
        // Control may be null if user cancels UAC prompt, in which case, don't show the Viewlet
        if (oSR.srCtl != null) 
        {
          //get path to the viewlet cab by looking it up in the msgs xml
          sViewlet=this.cl_msg_getBuildPath(this.cl_msg_getBuildPath(this.cl_msg_getMessagesPath(),psGuid+"."+psVersion),this.cl_msg_getValueFrom(psGuid,psVersion,XPATH_VIEWLET_STRING));
          //call run command on viewlet with async so that scriptrunner comes immediately after
          this.cl_msg_runCommand(sViewlet, "", 4);
        }
        //run cab for supportaction
        var retValue = oSR.Evaluate(); 
        bSuccess = IsExecutionSuccessful(retValue, psGuid, psVersion);
        break;
        
        //open tell me url = 3
      case TYPE_TELLME:
				var bConnStatus = $ss.agentcore.network.inet.GetConnectionStatus();
        if(bConnStatus)
        {
					var retVal = _utils_ui.RunDefaultBrowser(_config.ParseMacros(this.cl_msg_getValueFrom(psGuid,psVersion,XPATH_TELLMEURL_STRING)), null);
					if ( retVal == -1 )
					{
						alert(_utils.LocalXLate("snapin_messagespopup", "cl_browser_disabled"));
						bSuccess = false;
					}
        }  
        else  
        {
          alert(_utils.LocalXLate("snapin_messagespopup", "cl_msg_offline"));
          _databag.SetValue("INTERNET_FAILED", true);
          bSuccess = false;
        }
        break;
      }
      return bSuccess; 
    } ,
    /*******************************************************************************
    ** Name:        cl_msg_getValueFrom
    **		
    ** Purpose:     gets a value from the <guid>.<version>\<guid>.<version>.xml file
    **		
    ** Parameter:   psGuid, string, guid of message that we need value from
    **		psVersion, string, version of message that we need value from
    **		psXPath, string, xPath to value needed
    **              
    ** Return:      value found, string or "" if err or not found
    *******************************************************************************/
    cl_msg_getValueFrom:function(psGuid,psVersion,psXPath)
    {
      //load xmldom from buildpath to this psGuid's file 
      var oXml = _xml.LoadXML(this.cl_msg_getBuildPath(this.cl_msg_getBuildPath(this.cl_msg_getMessagesPath(),psGuid+"."+psVersion),psGuid+"."+psVersion+".xml"));
      //return the value based on psXPath
      return(_cl_msg_getNodeValue(oXml,psXPath));
    },
    /*******************************************************************************
    ** Name:        updateShownTs
    **		
    ** Purpose:     called by Ui, when message is show we update that timestamp in
    **		message.xml
    **		
    ** Parameter:   psGuid, string, guid of message we want to update the shown timestamp for
    **              sStatus, string: 
    **                      'V': result of viewed, 
    **                      'S': successful run payload, 
    **                      'U': Unsuccessful run payload
    **
    ** Return:      none
    **
    ** Notes:	using custom select nodes because poXml may need Xpath set instead
    **		of xslpattern
    *******************************************************************************/
    updateShownTs: function(psGuid, sStatus,iVersion)
    {
      var xml = this.updateXMLValue(psGuid, "shown_dt", this.cl_msg_getTs());
     // update tmp data island to what we just updated
      return xml;
    },
    
    /*******************************************************************************
    ** Name:        cl_msg_getTs
    **		
    ** Purpose:     formats current datetime into string "YYYY-MM-DDTHH:MM:SS"
    **		
    ** Parameter:   none
    **              
    ** Return:      formatted datetime string
    *******************************************************************************/
    cl_msg_getTs:function()
    {
      //new date
      var vDate=new Date();
      //string out specifics so we can manip later doing it this way so we have
      //better control over format
      var sYear=new String(vDate.getFullYear());
      var sMonth=new String((vDate.getMonth()+1));
      var sDate=new String(vDate.getDate());
      var sHr=new String(vDate.getHours());
      var sMin=new String(vDate.getMinutes());
      var sSec=new String(vDate.getSeconds());
      
      //make sure all are two-digits
      sMonth=sMonth.length<2?"0"+sMonth:sMonth;
      sDate=sDate.length<2?"0"+sDate:sDate;
      sHr=sHr.length<2?"0"+sHr:sHr;
      sMin=sMin.length<2?"0"+sMin:sMin;
      sSec=sSec.length<2?"0"+sSec:sSec;
      return(sYear+"-"+sMonth+"-"+sDate+"T"+sHr+":"+sMin+":"+sSec);
    },
        
    /*******************************************************************************
    ** Name:        cl_msg_updateTitleViewReport
    **		
    ** Purpose:      Upload a title viewed report information for reporting
    **		
    ** Parameter:   guid of the message
    **              version of the message
    **              
    ** Return:      none   
    *******************************************************************************/
    //code merge 
    cl_msg_updateTitleViewReport:function(psGuid, psVersion,psContentType) 
    {

      var verb = "msg_title_viewed";
      if(psContentType && psContentType !== "sprt_msg") {
        verb = "msg_"+psContentType+"_viewed";
      }
      var oLogEntry = $ss.agentcore.reporting.Reports.CreateLogEntry(psGuid,psVersion,verb, 1,     "",       "",        "",    "",_constants.RESULT_SUCCESS);
      return(this.cl_msg_ContentUsage(oLogEntry,false));
    },
    
        /*******************************************************************************
    ** Name:        cl_msg_updateDownloadReport
    **		
    ** Purpose:     Upload a downloaded report information for reporting
    **		
    ** Parameter:   guid of the message
    **              version of the message
    **              
    ** Return:      none   
    *******************************************************************************/ 
    //code merge
     cl_msg_updateDownloadReport:function(psGuid, psVersion,psContentType) 
    {
       var verb = "msg_downloaded";
       if(psContentType && psContentType !== "sprt_msg") {
          verb = psContentType+"_downloaded";
       }
       var oLogEntry = $ss.agentcore.reporting.Reports.CreateLogEntry(psGuid,psVersion,verb, 1,     "",       "",        "",    "",_constants.RESULT_SUCCESS);
       return(this.cl_msg_ContentUsage(oLogEntry,false));
    },
    
    /*******************************************************************************
    ** Name:         isSyncEnabled
    **
    ** Purpose:      Returns whether synch is enabled.
    **
    ** Return:       true - Synch is enabled
    **               false- Synch is disabled
    *******************************************************************************/
    isSyncEnabled:function() 
    {
      return false;
      var providerID = _config.GetProviderID();
      var synchPath = REG_SPRT+"ProviderList\\" + providerID + "\\users\\_default";
      return(_reg.GetRegValue("HKLM", synchPath, providerID).indexOf(GS_SYNCHPATH_DUMMY) == -1);
    },

    AddContentTypeAsDisplayed: function(sContentType) {
      AddDisplayContentType(sContentType);
    },

    GetDisplayedContentType: function(sContentType) {
      return GetDisplayContentType();
    }
    
  
    });//end of extend
    
    /********************************************************************************/
    //Private Area 
    /********************************************************************************/
    var _logger = $ss.agentcore.log.GetDefaultLogger("$ss.snapin.msgprocessor.helper");
    var _utils_ui =$ss.agentcore.utils.ui;
    var _utils =$ss.agentcore.utils;
    var _config = $ss.agentcore.dal.config;
    var _content=$ss.agentcore.dal.content;
    var _file=$ss.agentcore.dal.file;
    var _xml = $ss.agentcore.dal.xml;
    var _reg = $ss.agentcore.dal.registry;
    var _activex=$ss.agentcore.utils.activex;
    var _ini=$ss.agentcore.dal.ini;
    var _events=$ss.agentcore.events;
    var _constants = $ss.agentcore.constants;
    var _messages = $ss.snapin.msgprocessor.helper;
    var _databag = $ss.agentcore.dal.databag;   
    var GS_SYNCHPATH_DUMMY = "noman";
    //stores path to messages.xml
    var g_sMessagesPath  = '';
  
    var bMac = $ss.agentcore.utils.IsRunningInMacMachine();
    var payloadHelperObject = null;

    //path to sprt_msg folder relative from user's profile dir
    var TO_MESSAGES_PATH               = 'data\\sprt_msg';
    //path to messages.xml relative from TO_MESSAGES_PATH
    var sLangcode = _utils.GetLanguage();
    var TO_MESSAGES_XML  = 'messages_'+sLangcode+'.xml';   
 

    var XML_CONTRACT_BASE = "<?xml version=\"1.0\" encoding=\"utf-8\"?><sprt><msgs></msgs></sprt>";
    
    //added to support other content type notification
    var _contentPopModels = {};
    var _hasContentData = {};
    
    //action types
    var TYPE_SUPPORTACTIONONLY         = 0;
    var TYPE_VIEWLETONLY               = 1;
    var TYPE_VIEWLETACTION             = 2;
    var TYPE_TELLME                    = 3;  
    
    //xpaths for the <guid>.<ver>.xml's
    var XPATH_CONTENTSET_STRING        = "/sprt/content-set/content";
    var XPATH_COMPLETEDCAPTION_STRING  = XPATH_CONTENTSET_STRING + "/field-set/field[@name='Completed Link Caption']/value";
    var XPATH_INCOMPLETECAPTION_STRING = XPATH_CONTENTSET_STRING + "/field-set/field[@name='Incomplete Link Caption']/value";
    var XPATH_PRIORITY_STRING          = XPATH_CONTENTSET_STRING + "/field-set/field[@name='Priority']/value";
    var XPATH_TITLE_STRING             = XPATH_CONTENTSET_STRING + "/field-set/field[@name='Title']/value";
    var XPATH_TELLME_STRING            = XPATH_CONTENTSET_STRING + "/field-set/field[@name='Tell Me']/value";
    var XPATH_VIEWLET_STRING           = XPATH_CONTENTSET_STRING + "/field-set/field[@name='Viewlet']/value[@name='sccfb_field_value_blob']";
    var XPATH_SUPPORTACTION_STRING     = XPATH_CONTENTSET_STRING + "/field-set/field[@name='SupportAction']/value[@name='sccfb_field_value_blob']";
    var XPATH_SA_REQUIRE_ADMIN_STRING  = XPATH_CONTENTSET_STRING + "/field-set/field[@name='SupportAction Admin Privileges']/value[@name='sccf_field_value_int']";
    var XPATH_MOREDETAILS_STRING       = XPATH_CONTENTSET_STRING + "/field-set/field[@name='Detail']/value[@name='sccfc_field_value_clob']";

    var XPATH_EXPIREDATE_STRING        = XPATH_CONTENTSET_STRING + "/property-set/property[@name='scc_expire_date']";
    var XPATH_ACTIVEDATE_STRING        = XPATH_CONTENTSET_STRING + "/property-set/property[@name='scc_active_date']";
    var XPATH_TELLMEURL_STRING         = XPATH_CONTENTSET_STRING + "/field-set/field[@name='Tell Me']/value[@name='sccf_field_value_char']";
    var XPATH_ISTRIGGER_STRING         = XPATH_CONTENTSET_STRING + "/field-set/field[@name='TriggerType']";  

    //message.xml xpaths
    var XPATH_MSG_STRING               = "/sprt/msgs/msg";
     
    // Pending emergency, un-deleted, un-viewed, non-trigger-type or trigger-type but already triggered, un-expired, activated messages
    var XPATH_PENDING_CRITICAL_STRING  = XPATH_MSG_STRING + "[p[@name='severity']='1' and p[@name='shown_dt']='' and p[@name='deleted_dt']='' and @t!='1' and @exp='0' and @act='1']";

    // Pending any un-deleted, un-viewed, non-trigger-type or trigger-type but already triggered, un-expired, activated messages
    var XPATH_PENDING_ANY_STRING       = XPATH_MSG_STRING + "[p[@name='shown_dt']='' and p[@name='deleted_dt']='' and @t!='1' and @exp='0' and @act='1']";

    //macros
    var MACRO_MESSAGE_MACRO            = "#JTD_MACRO_GUID#";
    var MACRO_MESSAGE_STRNG            = XPATH_MSG_STRING + "[@guid='"+MACRO_MESSAGE_MACRO+"']";

    //event types (or, why were we called)
    EVT_NONE                               = 0;
    EVT_TIMEREVENT                         = 1;
    EVT_SYNCEVENT                          = 2;
    EVT_TRIGGEREVENT                       = 3;

    //messages that we send to fire tray icon - determined by tray_config.xml
    MSG_TRAY_DEFAULT                       = "default";
    MSG_TRAY_URGENTMESSAGE                 = "UrgentMessage";
    MSG_TRAY_NEWMESSAGE                    = "NewMessage";
    MSG_TRAY_BLINK                         = "Blink";

    var BIG_BCONT = "bigbcont";
    var MINI_BCONT = "minibcont";

    // Global File System object
    var g_oFso = null;
    
    var REG_SPRT  = "Software\\SupportSoft\\";
    
    var g_con_animateOpts = 
    {
      AW_HOR_POSITIVE : 0x00000001,
      AW_HOR_NEGATIVE : 0x00000002,
      AW_VER_POSITIVE : 0x00000004,
      AW_VER_NEGATIVE : 0x00000008,
      AW_CENTER       : 0x00000010,
      AW_HIDE         : 0x00010000,
      AW_ACTIVATE     : 0x00020000,
      AW_SLIDE        : 0x00040000,
      AW_BLEND        : 0x00080000
    }

    var oMessagesDisplayed = {length: 0};
    function AddDisplayContentType (sContentType) {
      if(!oMessagesDisplayed[sContentType]) {
        oMessagesDisplayed[sContentType] = 1;
        oMessagesDisplayed.length++;
      }
    }

    function GetDisplayContentType() {
      return oMessagesDisplayed;
    }

    /****************************************************************************************
    ** Name:        _cl_msg_pvt_ContUsage
    **
    ** Purpose:     This API logs content usage from either the Browse Solution snapin or 
    **              through other API calls. 
    **              
    **              After writing to the log file this API calls a cousin helper ss_rl_pvt_RegContUsage
    **              to write the log into the registry. 
    **              
    ** Parameter:   sContGuid   String            GUID or ID of content 
    **              sVersion     String            Version, 0 if not version controlled.
    **              sVerb        String            [view|flow_end|flow_sucess|???]
    **              sView        Integer/String    Indicates if the content was viewed "", 0, or 1
    **              sSolved      Integer/String    Indicates if the content solved the problem "", 0, or 1
    **              sComment     String            extra information, such as path 
    **              sData        String            extra information, such as trigger context
    **              
    ** Return:      Boolean result of logging the message 
    *****************************************************************************************/
     
     function _cl_msg_pvt_ContUsage(sContGuid, sVersion, sVerb, sView, sSolved, sComment, sData, sRating, sResult)
     {
       try
       {
       sContGuid = sContGuid.replace("snapin_content_template", "");
       var principal = "Client"; 
       var rating = (typeof(rating) == "undefined") ? "" : sRating; 
       var oArgs = _utils_ui.GetObjectFromCmdLine();
       var sEntry = unescape(oArgs.entry);
       if (sEntry == null || sEntry == "undefined" || sEntry == "") 
         sEntry = "Others";
       var msg = "CONTENT__"+sContGuid + '__' + sVersion + '__' + sVerb + '__' + principal + '__' + sView + '__' + sSolved + '__' + sComment + '__' + sData + '__' + sRating + '__' + sEntry + '__' + sResult;
       var ret = true;
       if (bMac) {
            $ss.agentcore.utils.WriteUsageLog(msg);
       }     
       else {
            ret=$ss.agentcore.utils.activex.GetObjInstance().LogMsg(msg);
      }
       _cl_msg_pvt_RegContUsage(sContGuid, sVersion, sVerb, principal, sView, sSolved, sComment, sData, sRating, sResult);
       return ret;
     }
       catch(err)
       {
         _logger.error("$ss.snapin.msgprocessor.helper._cl_msg_pvt_ContUsage()", err.message);
       } 
     }
     
     function _cl_msg_pvt_RegContUsage(sContGuid, sVersion, sVerb, principal, sView, sSolved, sComment, sData, sRating, sResult)
     {
        try
        {
        var sUserName = _config.GetContextValue("SdcContext:UserName");
        var sTimeStamp = _utils.FormatDateString(new Date(), true, true);
        var oArgs = _utils_ui.GetObjectFromCmdLine();
        var sEntry = unescape(oArgs.entry);
        
        if (!sEntry || sEntry === "undefined") 
          sEntry = "Others";
        
        var subkey = "Software\\SupportSoft\\ProviderList\\" +
        _config.GetContextValue("SdcContext:ProviderId") +
        "\\users\\" +
        sUserName +
        "\\LogCache\\" +
        sTimeStamp;
        
        _reg.SetRegValueByType("HKCU", subkey, "sContGuid", 1, sContGuid);
        _reg.SetRegValueByType("HKCU", subkey, "version", 1, sVersion);
        _reg.SetRegValueByType("HKCU", subkey, "verb", 1, sVerb);
        _reg.SetRegValueByType("HKCU", subkey, "principal", 1, "Client");
        _reg.SetRegValueByType("HKCU", subkey, "solved", 1, sSolved);
        _reg.SetRegValueByType("HKCU", subkey, "comment", 1,sComment);
        _reg.SetRegValueByType("HKCU", subkey, "data", 1, sData);
        _reg.SetRegValueByType("HKCU", subkey, "rating", 1, sRating);
        _reg.SetRegValueByType("HKCU", subkey, "entry", 1, sEntry);
        _reg.SetRegValueByType("HKCU", subkey, "result", 1,sResult);
     }
        catch(err)
        {
           _logger.error("$ss.snapin.msgprocessor.helper._cl_msg_pvt_RegContUsage()", err.message);
        } 
     }
           
    /*******************************************************************************
    ** Name:        _cl_msg_getSessionId
    **		
    ** Purpose:     gets unique id for use within this session
    **		
    ** Parameter:   none
    **              
    ** Return:      guid string or timestamp if fails
    *******************************************************************************/
    function _cl_msg_getSessionId()
    { 
      var sId = "";
      try
      {
        try
        {      
          sId = _utils.GenGuid(false);
        }
        catch(oErr)
        {
          //if err then just get timestamp
          sId = _messages.cl_msg_getTs();
        }  
      }
      catch (ex)
      {
        try 
        {
          _logger.error("$ss.snapin.msgprocessor.helper._cl_msg_getSessionId", ex.message);
        } 
        catch (ex2) 
        {
        }
      }
        
      return(sId);
    }
   
    
    /*******************************************************************************
    ** Name:        _cl_msg_getNode
    **		
    ** Purpose:     returns node found in xpath query
    **		
    ** Parameter:   poXml, XML object, xml object to search 
    **		psXPath, string, xpath to node
    **              
    ** Return:      node object found in xpath, or false on err
    **
    ** Notes:	do not use api call to return node - some queries in this snapin
    **		require xpath to work (as opposed to xslpattern).
    *******************************************************************************/
    function _cl_msg_getNode(poXml,psXPath)
    {
      //store return
      var vReturn;
      try
      {
        //get node
        if (bMac) {
        vReturn = _xml.SelectSingleNode(poXml.responseXML, psXPath);
        }else{
        vReturn = poXml.selectSingleNode(psXPath);
      }
      }
      catch(oErr)
      {
        //on err, return false
        vReturn = false;
      }
      return(vReturn);
    }

    

    /*******************************************************************************
    ** Name:        _cl_msg_insertNewMsgNode
    **		
    ** Purpose:     when we find a msg not in message.xml we will insert a new one
    **		here and set all the properties
    **		
    ** Parameter:   poXml,xml object, xmldom of messages.xml
    **		psMsgXmlPath, string, path to this msg's xml
    **		psMsgGuid, string, guid of msg
    **		psMsgVersion, string, version of msg
    **    psDateReceived, string, date this msg was received or updated from the server
    **    bSave - boolean indicates if we should save consolidated content to disk
    **              
    ** Return:      none
    *******************************************************************************/
     //code merge - added bSave param
    function _cl_msg_insertNewMsgNode(sSessionId,poXml,psMsgXmlPath,psMsgGuid,psMsgVersion,psMsglanguage,psDateReceived,bSave)
    {
      try
      {
        //new xml object - using min version that will get us xpath
        var oThisMsgXml =  null;
                var loadResult = false;
        if (bMac) {
          oThisMsgXml = _xml.LoadXML(psMsgXmlPath,false);
          loadResult = oThisMsgXml != null;
        }else{
          oThisMsgXml = _utils.CreateActiveXObject("msxml2.domdocument");
        }

        //load up msgs node from poXml this is where we store new msg nodes
        var oMsgsNode = _cl_msg_getNode(poXml,"/sprt/msgs");
        
        //variable for new message node
        var oNewMsgNode;
        if (!bMac) {
        //we'll wait for load
        oThisMsgXml.async = false;

        //set selection language to xpath, not default xslpattern
        oThisMsgXml.setProperty("SelectionLanguage","XPath");

        //if we can load the msg's xml
          loadResult = oThisMsgXml.load(psMsgXmlPath);
        }

        //if we can load the msg's xml
        if (loadResult)
        {
          //create msg node for this msg
          if (bMac) {
          oNewMsgNode = poXml.responseXML.createElement('msg');
          }
          else {
            oNewMsgNode = poXml.createElement('msg');
          }

          //set guid attr
          oNewMsgNode.setAttribute('guid',psMsgGuid);

          //set ver attr
          oNewMsgNode.setAttribute('ver',psMsgVersion);
          
          //set langcode attr
          oNewMsgNode.setAttribute('langcode',psMsglanguage);

          //set session attr - all msgs without a session's id set will be removed when synching
          oNewMsgNode.setAttribute('sid',sSessionId);

          //set expired attr (using cl_msg_isExpired to compare date) - used to filter out expired
          oNewMsgNode.setAttribute('exp',_cl_msg_isExpired(_cl_msg_getNodeValue(oThisMsgXml,XPATH_EXPIREDATE_STRING)));

          //set activated attr (using cl_msg_isActivated to compare date) - used to filter out not activated
          oNewMsgNode.setAttribute('act',_cl_msg_isActivated(_cl_msg_getNodeValue(oThisMsgXml,XPATH_ACTIVEDATE_STRING)));

          //set trigger attr - used later to filter out/in triggers  
          var sTriggerType = _cl_msg_getNodeValue(oThisMsgXml,XPATH_ISTRIGGER_STRING);
          oNewMsgNode.setAttribute('t',(sTriggerType==''||sTriggerType.toLowerCase()=='none')?'0':'1');

          //set details attr - used by xsl to determine if we should display view details link
          oNewMsgNode.setAttribute('d',_cl_msg_getNodeValue(oThisMsgXml,XPATH_MOREDETAILS_STRING)==''?'0':'1');

          //code merge
          //set tv attr - used to indicate whether the message title is viewed
          //If viewing messagelist page and alert is non trigger then set as viewed
          var isInMessageList=$ss.agentcore.dal.databag.GetValue("db_msg_MessageList");
          if (isInMessageList && oNewMsgNode.getAttribute('t') != '1') 
          {
            oNewMsgNode.setAttribute('tv', '1');
            if (bSave) _messages.cl_msg_updateTitleViewReport(psMsgGuid, psMsgVersion);
          } 
          else 
          {
              oNewMsgNode.setAttribute('tv', '0');
          }


          //Add a created date and time
          oNewMsgNode.appendChild(_cl_msg_createNewPropertyNode(poXml,"create_dt",psDateReceived));

          //copy shown_dt value over to p node
          oNewMsgNode.appendChild(_cl_msg_createNewPropertyNode(poXml,"shown_dt",""));

          //code merge
          if(_cl_msg_getNodeValue(oThisMsgXml,XPATH_SUPPORTACTION_STRING) == "enableSync.cab" && _messages.isSyncEnabled()) 
          {
            oNewMsgNode.appendChild(_cl_msg_createNewPropertyNode(poXml,"deleted_dt",psDateReceived));
          } 
          else 
          {
          //copy deleted_dt value over to p node
          oNewMsgNode.appendChild(_cl_msg_createNewPropertyNode(poXml,"deleted_dt",""));
          }

          //set the payload value - S,U,V,VS (Do It,Tell Me,Show Me, Show Me/Do It)
          var bHasTellMeURL = (_cl_msg_getNodeValue(oThisMsgXml,XPATH_TELLME_STRING)==''?'':'U'); 
          var bHasViewlet   = (_cl_msg_getNodeValue(oThisMsgXml,XPATH_VIEWLET_STRING)==''?'':'V'); 
          var bHasSA        = (_cl_msg_getNodeValue(oThisMsgXml,XPATH_SUPPORTACTION_STRING)==''?'':'S');
          var sPayload      = bHasTellMeURL + bHasViewlet + bHasSA;
          
          //code merge
          oNewMsgNode.appendChild(_cl_msg_createNewPropertyNode(poXml,"sa_executable",_cl_msg_getNodeValue(oThisMsgXml,XPATH_SUPPORTACTION_STRING)));
          
          oNewMsgNode.appendChild(_cl_msg_createNewPropertyNode(poXml,"payload",sPayload));      

          var bSARequiresElevation = (_cl_msg_getNodeValue(oThisMsgXml,XPATH_SA_REQUIRE_ADMIN_STRING)==''?'0':'1');
          oNewMsgNode.appendChild(_cl_msg_createNewPropertyNode(poXml,"elevation",bSARequiresElevation));

          //copy payload_status value over to p node
          //if msg has a payload specified, then initialize it to "N"
          //otherwise, leave it blank so we can distinguish between "not having a payload at all" vs "haven't run the payload yet"
          var sPayloadStatus = (sPayload != "") ? "N" : "";
          oNewMsgNode.appendChild(_cl_msg_createNewPropertyNode(poXml,"payload_status",sPayloadStatus));

          //copy completed caption value over to p node (will be title over button in popup after use has view/ran sucessfully)
          oNewMsgNode.appendChild(_cl_msg_createNewPropertyNode(poXml,"completed_caption",_cl_msg_getNodeValue(oThisMsgXml,XPATH_COMPLETEDCAPTION_STRING)));

          //copy incomplete caption value over to p node (will be title over button in popup after user has view/ran *unsucessfully* or not at all)
          oNewMsgNode.appendChild(_cl_msg_createNewPropertyNode(poXml,"incomplete_caption",_cl_msg_getNodeValue(oThisMsgXml,XPATH_INCOMPLETECAPTION_STRING)));

          //copy severity value over to p node (1=Emergency,2=High,3=Medium,4=Low)
          var nSev = _cl_msg_pvt_SeverityToNumber(_cl_msg_getNodeValue(oThisMsgXml,XPATH_PRIORITY_STRING));
          oNewMsgNode.appendChild(_cl_msg_createNewPropertyNode(poXml,"severity",nSev));

          //copy title value over to p node
          var oContentObj = $ss.agentcore.dal.content.GetContentByCid(["sprt_msg"], psMsgGuid);
          oNewMsgNode.appendChild(_cl_msg_createNewPropertyNode(poXml,"title",oContentObj.title));

          //copy expiraton_dt value over to p node
          oNewMsgNode.appendChild(_cl_msg_createNewPropertyNode(poXml,"expiration_dt",_cl_msg_getNodeValue(oThisMsgXml,XPATH_EXPIREDATE_STRING)));

          //copy activation_dt value over to p node
          oNewMsgNode.appendChild(_cl_msg_createNewPropertyNode(poXml,"activation_dt",_cl_msg_getNodeValue(oThisMsgXml,XPATH_ACTIVEDATE_STRING)));

          //append new msg mode to the msgs mode
          oMsgsNode.appendChild(oNewMsgNode);
        }
      }
      catch (ex)
      {
        try 
        {
          _logger.error("$ss.snapin.msgprocessor.helper._cl_msg_insertNewMsgNode()", ex.message);
        } 
        catch (ex2) 
        {
        } 
      }
    }
    

  /*******************************************************************************
  ** Name:        _cl_msg_insertMetaDataNode
  **		
  ** Purpose:     when we find a msg not in message.xml we will insert a new one
  **		here and set all the properties
  **		
  ** Parameter:   poXml,xml object, xmldom of messages.xml
  **              
  ** Return:      none
  *******************************************************************************/
  function _cl_msg_insertMetaDataNode(sSessionId, poXml) {
    try {
      
      var sSortType = _config.GetConfigValue("sprtmsg", "display_sort_type", "severity");
      var oMetaNode = _cl_msg_getNode(poXml, "/sprt/msgs/metadata");
      
      //load up msgs node from poXml this is where we store new msg nodes
      var oMsgsNode = _cl_msg_getNode(poXml, "/sprt/msgs");

      //variable for new message node
      var oNewMsgNode;

      //if we can load the msg's xml
      //create metadata node which is used for xsl decisions
      if (bMac) {
        oNewMsgNode = poXml.responseXML.createElement('metadata');
      }else{
      oNewMsgNode = poXml.createElement('metadata');
      }



      //copy activation_dt value over to p node
      oNewMsgNode.appendChild(_cl_msg_createNewPropertyNode(poXml, "sort_type", sSortType));
      if (oMetaNode === null) {
        //append new msg mode to the msgs mode
        oMsgsNode.appendChild(oNewMsgNode);
      }
      else {
        oMsgsNode.replaceChild(oNewMsgNode, oMetaNode)
      }
    }
    catch (ex) {
      _logger.error("$ss.snapin.msgprocessor.helper._cl_msg_insertMetaDataNode()", ex.message);

    }
  }

    
    /*******************************************************************************
    ** Name:       	_cl_msg_isExpired
    **		
    ** Purpose:     determines if psDate is past current time (ie, expired) - is specific
    **		for messages timestamp format
    **		
    ** Parameter:   psDate, string, datetime string to check against (expecting: YYY-MM-DDTHH:MM:DD[.0000+00])
    **              
    ** Return:      string, "0" = not expired, "1" = expired,err
    **
    *******************************************************************************/
    function _cl_msg_isExpired(psDate)
    {  
      //store tmps
      var dNow,dOther;
      
      //store return, default "0"  (ie, psDate == "")
      var sReturn = "0";
      
      //if we got an actual value
      if(psDate)
      {
        try
        {      
          //get current date, and parse to get ms since 1/1/1970 12am
          dNow = Date.parse(new Date());
          
          //get back valid date object and parse to get ms since 1/1/1970 12am
          dOther = Date.parse(_cl_msg_getDateFrom(psDate));
          
          //if expired return "1"
          if(dNow > dOther)
          {
            sReturn = "1";
          }
        }
        catch(oErr)
        {      
          //if err return default
          sReturn = "1";
        }
      }
      return(sReturn);
    }

    /*******************************************************************************
    ** Name:        _cl_msg_getDateFrom
    **		
    ** Purpose:     converts datetime/string and attempts to format into agreed format
    **		
    ** Parameter:   psDateTime, string, datetime string to be formatted
    **              
    ** Return:      Date object, NAN if err
    *******************************************************************************/
    function _cl_msg_getDateFrom(psDateTime)
    {  
      //strip out the "T" and replace '-' with '/'
      var sTimeString = psDateTime.toString().replace(/T/g,' ').replace(/-/g,'/');
      
      //some of the datetime strings have ms at the end so lets check for '.' to get max right
      var nMi = psDateTime.indexOf('.');
      
      //store our return
      var oDate;
      
      //if we didn't find '.' above make max right the same as len
      if(nMi==(-1))
      {
        nMi = psDateTime.length;
      }
      
      oDate = new Date(psDateTime.slice(0,nMi));
      return(oDate);
    }
    
    /*******************************************************************************
    ** Name:        _cl_msg_getNodeValue
    **		
    ** Purpose:     gets value of node back
    **		
    ** Parameter:   poXml, xml object, xml object to search
    **		psXPath, string, xpath query
    **              
    ** Return:      returns value of node found, empty "" on err
    **
    ** Notes:	do not use api call to return node - some queries in this snapin
    **		require xpath to work (as opposed to xslpattern).
    *******************************************************************************/
    function _cl_msg_getNodeValue(poXml,psXPath)
    {
      //store return
      var sReturn = '';
      try
      {
        //get node and store text property
        var node = null; 
        if (bMac) {
          try{
            if (poXml) {
              node = _cl_msg_getNode(poXml,psXPath);
            }
        }
        catch(ex){}
          if (node) {
            sReturn = _xml.GetNodeText(node);
          }
        }
        else {
            //get node and store text property
        sReturn = _cl_msg_getNode(poXml,psXPath).text;
      }
      }
      catch(oErr)
      {
        //on err, return empty
        sReturn='';
      }
      return(sReturn);
    }
    
    /*******************************************************************************
    ** Name:        _cl_msg_getNode
    **		
    ** Purpose:     returns node found in xpath query
    **		
    ** Parameter:   poXml, XML object, xml object to search 
    **		psXPath, string, xpath to node
    **              
    ** Return:      node object found in xpath, or false on err
    **
    ** Notes:	do not use api call to return node - some queries in this snapin
    **		require xpath to work (as opposed to xslpattern).
    *******************************************************************************/
    function _cl_msg_getNode(poXml,psXPath)
    {
      //store return
      var vReturn;
      try
      {
        //get node
         if (bMac) {
          var xmlDoc = poXml;
          if (poXml.responseXML != undefined) {
            xmlDoc = poXml.responseXML;
          }
        vReturn = _xml.SelectSingleNode(xmlDoc, psXPath);
        }else{
        vReturn = poXml.selectSingleNode(psXPath);
      }
      }
      catch(oErr)
      {
        //on err, return false
        vReturn = false;
      }
      return(vReturn);
    }
    
    
    /*******************************************************************************
    ** Name:       	_cl_msg_isActivated
    **		
    ** Purpose:     determines if psDate is before the current time (ie, not active) - is specific
    **		for messages timestamp format
    **		
    ** Parameter:   psDate, string, datetime string to check against (expecting: YYY-MM-DDTHH:MM:DD[.0000+00])
    **              
    ** Return:      string, "1" = active, "0" = not active,err
    **
    *******************************************************************************/
    function _cl_msg_isActivated(psDate)
    {
      //store tmps
      var dNow,dOther;

      //store return, default "1" (ie, psDate == "")
      var sReturn = "1";

      //if we got an actual value
      if(psDate)
      {
        try
        {
          //get current date, and parse to get ms since 1/1/1970 12am
          dNow = Date.parse(new Date());

          //get back valid date object and parse to get ms since 1/1/1970 12am
          dOther = Date.parse(_cl_msg_getDateFrom(psDate));

          //if expired return "0"
          if(dNow < dOther)
          {
            sReturn = "0";
          }
        }
        catch(oErr)
        {
          //if err return default
          sReturn = "1";
        }
      }
      return(sReturn);
    }

    
    /*******************************************************************************
    ** Name:        _cl_msg_createNewPropertyNode
    **		
    ** Purpose:     creates a new p (property) node in xml object
    **		
    ** Parameter:   poXml, xml object, xml object (needed to create element)
    **		psName, string, used to set name attribute
    **		psValue, string, value to set
    **              
    ** Return:      node created
    *******************************************************************************/
    function _cl_msg_createNewPropertyNode(poXml,psName,psValue)
    {
      //store return, create node
      var oNode = null;
      if (bMac) {
        oNode = poXml.responseXML.createElement("p");
        var newAtt = poXml.responseXML.createAttribute("name");
        newAtt.nodeValue = psName;
        oNode.setAttributeNode(newAtt);
        oNode.textContent = psValue;

      }else{
        oNode = poXml.createElement("p");

      //set name attr
      oNode.setAttribute("name",psName);

      //set value
      oNode.text = psValue;
      }

      

      return(oNode);
    }

    // convert string to numeric (defaults to low priority (4))
    function _cl_msg_pvt_SeverityToNumber(sSev)
    {
      var nSev = 4;  
      switch (sSev.toLowerCase())
      {
        case "emergency": nSev = 1; break;
        case "high":      nSev = 2; break;
        case "medium":    nSev = 3; break;
        case "low":
        default:          nSev = 4; break;
      }
      return nSev;
    }
    
    /*******************************************************************************
    ** Name:        _cl_msg_getCountFor
    **		
    ** Purpose:     gets the number of nodes found with XPath
    **		
    ** Parameter:   psXPath, string, XPath to try select nodes on
    **		poXml,xml object, xml object to try select nodes on
    **              
    ** Return:      count integer of number of nodes found with xpath, 0=err
    **
    ** Notes:	using custom select nodes because poXml may need Xpath set instead
    **		of xslpattern
    *******************************************************************************/
    function _cl_msg_getCountFor(psXPath,poXml)
    {
      //store return, default 0
      var nReturn = 0;
      try
      {
        //try select nodes and get length
        if (bMac) {
          var nodes = _xml.SelectNodes(poXml.responseXML, psXPath);
          if (nodes) {
            nReturn = nodes.length;
          }
        }else {
        nReturn = poXml.selectNodes(psXPath).length;
      }
      }
      catch(oErr)
      {
        //if err, return
        nReturn = 0;
      }
      return(nReturn);
    }
    
    function _isTrayIconPresent() 
    {
      var trayIconSprocket = REG_SPRT + "ProviderList\\" + _config.GetProviderID() + "\\sprtcmd\\Sprockets\\TrayIcon";
      var trayIconEnabled = REG_SPRT + "ProviderList\\" + _config.GetProviderID() + "\\users\\" + _config.GetContextValue("SdcContext:UserName");
      return _reg.RegValueExists(_constants.REG_TREE, trayIconSprocket, "DllPath") && (_reg.GetRegValue(_constants.REG_HKCU,trayIconEnabled, "EnableIcon") == 1)
    }  
    
    /*******************************************************************************
    ** Name:        _cl_msg_fireAgentEvent()
    **		
    ** Purpose:     wrapper call to windows.external.FireAgentEvent() to set tray
    **		icon status.  message sent is determined by what's in
    **		tray_config.xml, currently: "default", "UrgentMessage" and "NewMessage"
    **		
    ** Parameter:   sEventMessage, string Constant, 
    **		  MSG_TRAY_DEFAULT         = "default";
    **		  MSG_TRAY_URGENTMESSAGE   = "UrgentMessage";
    **		  MSG_TRAY_NEWMESSAGE      = "NewMessage";
    **               
    ** Return:      none
    *******************************************************************************/
    function _cl_msg_fireAgentEvent(sEventMessage)
    {  
      //put it in correct format - SPROCKET_<MESSAGE>_<PROVIDER>_<USER>
      var sMessage = "SPROCKET_" + sEventMessage + "_" + _config.GetProviderID().toLowerCase() + "_" + _config.GetContextValue("SdcContext:UserName");
      
      //call windows.external.FireAgentEvent with psEventMessage
      _FireAgentEvent(sMessage);
    }

    /*******************************************************************************
    ** Name:         _FireAgentEvent
    **
    ** Purpose:      Wrapper for window.external.FireAgentEvent, which fires the  
    **               specified event to the sprockets
    **
    ** Parameter:    sEventMessage 
    **
    ** Return:       
    *******************************************************************************/
    function _FireAgentEvent(sEventMessage)
    {
      var retVal = null;
      try
      {
        if (!bMac) {


        var objContainer = _activex.GetObjInstance();
        //pending - this method not working
        retVal = objContainer.FireAgentEvent(sEventMessage);      
      }
      }
      catch (e) 
      {
        _logger.error("$ss.snapin.msgprocessor.helper._FireAgentEvent()", e.message);
      }
      return retVal; 
    }
    
    /*******************************************************************************
    ** Name:        _cl_msg_updateNewMsgNode
    **		
    ** Purpose:     if we found a new version on the under sprt_msg folder then 
    **              we update the msg node in message.xml with new properties
    **              - we only do this if the user hasn't deleted (ignored) in which
    **              case we only update the attributes.
    **		
    ** Parameter:   poMsgNode, xml object (node), this is the node of msg found in message.xml
    **		psMsgXmlPath, string, path to this msg's xml
    **		psMsgVersion, string, version of msg
    **              
    ** Return:      none
    *******************************************************************************/
    function _cl_msg_updateNewMsgNode(sSessionId,poMsgNode,psMsgXmlPath,psMsgVersion)
    {
      try
      {
        //load new xml object    
        var oThisMsgXml = _xml.LoadXML(psMsgXmlPath,false);  

        //if we got something valid then
        if (oThisMsgXml)
        {
          AddDisplayContentType("sprt_msg");
          //update version attr
          poMsgNode.setAttribute('ver',psMsgVersion);

          //update session id so that this node doesn't get stripped out
          poMsgNode.setAttribute('sid',sSessionId);

          //set expired attr - xsl display will only look for exp=0 - find out expired by calling cl_msg_isExpired with date
          poMsgNode.setAttribute('exp',_cl_msg_isExpired(_cl_msg_getNodeValue(oThisMsgXml,XPATH_EXPIREDATE_STRING)));

          //set activated attr - xsl display will only look for exp=0 - find out activated by calling cl_msg_isActivated with date
          poMsgNode.setAttribute('act',_cl_msg_isActivated(_cl_msg_getNodeValue(oThisMsgXml,XPATH_ACTIVEDATE_STRING)));

          //set details attr (xsl will look for this when determining if we should show view details link on popup)
          poMsgNode.setAttribute('d',_cl_msg_getNodeValue(oThisMsgXml,XPATH_MOREDETAILS_STRING)==''?'0':'1');

          //reset trigger attr - used later to filter out/in triggers      
          var sTriggerType = _cl_msg_getNodeValue(oThisMsgXml,XPATH_ISTRIGGER_STRING);
          poMsgNode.setAttribute('t',(sTriggerType==''||sTriggerType.toLowerCase()=='none')?'0':'1');
          

          //if user has chosen to ignore then do nothing (cause we will only have the delete_t p node)
          var result = false;
          if (bMac) {
                        result = _cl_msg_getNodeValue(poMsgNode,"//sprt/msgs/msg/p[@name='deleted_dt']")=="";
              var node = _xml.SelectSingleNodeFromNode(poMsgNode,'name','deleted_dt');
              if (node) {
                var text = node.getAttribute('deleted_dt');
                if (text != null) {
                result = text == "";
              }else
                result = false;
              }
          }
          else {
            result = _cl_msg_getNodeValue(poMsgNode,"p[@name='deleted_dt']")=="";
          }
          if (result)
          {
            //else update with new properties
             if (bMac) {
              var node = _xml.SelectSingleNodeFromNode(poMsgNode,'name','completed_caption');
              if (node) {
                var text = _cl_msg_getNodeValue(oThisMsgXml,XPATH_COMPLETEDCAPTION_STRING);
                  //node.setAttribute('completed_caption',text);
                  node.textContent = text;
              }

              node = _xml.SelectSingleNodeFromNode(poMsgNode,'name','incomplete_caption');
              if (node) {
                var text = _cl_msg_getNodeValue(oThisMsgXml,XPATH_INCOMPLETECAPTION_STRING);
                  // node.setAttribute('incomplete_caption',text);
                  node.textContent = text;
              }

              node = _xml.SelectSingleNodeFromNode(poMsgNode,'name','severity');
              if (node) {
                var text = _cl_msg_pvt_SeverityToNumber(_cl_msg_getNodeValue(oThisMsgXml,XPATH_PRIORITY_STRING));
                  // node.setAttribute('severity',text);
                  node.textContent = text;
              }

              node = _xml.SelectSingleNodeFromNode(poMsgNode,'name','title');
              if (node) {
                var text = _cl_msg_getNodeValue(oThisMsgXml,XPATH_TITLE_STRING);
                  // node.setAttribute('title',text);
                  node.textContent = text;
              }

              node = _xml.SelectSingleNodeFromNode(poMsgNode,'name','expiration_dt');
              if (node) {
                var text = _cl_msg_getNodeValue(oThisMsgXml,XPATH_EXPIREDATE_STRING);
                  // node.setAttribute('expiration_dt',text);
                  node.textContent = text;
              }

              node = _xml.SelectSingleNodeFromNode(poMsgNode,'name','activation_dt');
              if (node) {
                var text = _cl_msg_getNodeValue(oThisMsgXml,XPATH_ACTIVEDATE_STRING);
                  // node.setAttribute('activation_dt',text);
                  node.textContent = text;
              }

              node = _xml.SelectSingleNodeFromNode(poMsgNode,'name','elevation');
              if (node) {
                var text = _cl_msg_getNodeValue(oThisMsgXml,XPATH_SA_REQUIRE_ADMIN_STRING);
                  // node.setAttribute('elevation',text);
                  node.textContent = text;
              }
              //_cl_msg_getNode(poMsgNode,"/sprt/msgs/msg/p[@name='completed_caption']");
            }
            else {
            poMsgNode.selectSingleNode("p[@name='completed_caption']").text = _cl_msg_getNodeValue(oThisMsgXml,XPATH_COMPLETEDCAPTION_STRING);
            poMsgNode.selectSingleNode("p[@name='completed_caption']").text = _cl_msg_getNodeValue(oThisMsgXml,XPATH_COMPLETEDCAPTION_STRING);
            poMsgNode.selectSingleNode("p[@name='incomplete_caption']").text = _cl_msg_getNodeValue(oThisMsgXml,XPATH_INCOMPLETECAPTION_STRING);
            poMsgNode.selectSingleNode("p[@name='severity']").text = _cl_msg_pvt_SeverityToNumber(_cl_msg_getNodeValue(oThisMsgXml,XPATH_PRIORITY_STRING));
            
            var oContentGuid = _cl_msg_getNodeValue(oThisMsgXml,XPATH_CONTENT_GUID);
            var oContentObj = $ss.agentcore.dal.content.GetContentByCid(["sprt_msg"], oContentGuid);
            poMsgNode.selectSingleNode("p[@name='title']").text = oContentObj.title;

            poMsgNode.selectSingleNode("p[@name='expiration_dt']").text = _cl_msg_getNodeValue(oThisMsgXml,XPATH_EXPIREDATE_STRING);
            poMsgNode.selectSingleNode("p[@name='activation_dt']").text = _cl_msg_getNodeValue(oThisMsgXml,XPATH_ACTIVEDATE_STRING);
            poMsgNode.selectSingleNode("p[@name='elevation']").text = _cl_msg_getNodeValue(oThisMsgXml,XPATH_SA_REQUIRE_ADMIN_STRING)==''?'0':'1';
            }
            //code merge
            if (_cl_msg_getNodeValue(oThisMsgXml,XPATH_SUPPORTACTION_STRING) == "enableSync.cab" && _messages.isSyncEnabled()) 
            {
              if (bMac) {
                  var node = _xml.SelectSingleNodeFromNode(poMsgNode,'name','deleted_dt');
                  if (node) {
                    // node.setAttribute('deleted_dt',$ss.snapin.msgprocessor.helper.cl_msg_getTs());
                    node.textContent = $ss.snapin.msgprocessor.helper.cl_msg_getTs();
                  }
              }
              else {
              poMsgNode.selectSingleNode("p[@name='deleted_dt']").text = $ss.snapin.msgprocessor.helper.cl_msg_getTs();
            }
            }
            
          }
        }  
      }
      catch (ex)
      {
        try 
        {
          _logger.error("$ss.snapin.msgprocessor.helper._cl_msg_updateNewMsgNode()", ex.message);
        } 
        catch (ex2) 
        {
        }
      }  
    }
    
  
    /*******************************************************************************
    ** Name:         _AnimateShowWindow
    **
    ** Purpose:      Animates the container window while displaying (slide/roll, alpha blend, etc.)
    **
    ** Parameter:    none
    **
    ** Return:       none
    *******************************************************************************/
    function _AnimateShowWindow()
    {
      try
      {
        var animateTime  = _config.GetConfigValue("minishell", "animate_time", 500);
        _activex.GetObjInstance().Animate(animateTime,_GetAnimateFlags(true));
      }
      catch (err)
      {
        _logger.error("$ss.snapin.msgprocessor.helper._AnimateShowWindow()", err.message);
      }
    }

    
    function _GetAnimateFlags(bShow)
    {
      var animateFlags = "AW_BLEND";

      if(bShow)
      {
        animateFlags = _config.GetConfigValue("minishell", "animate_flags", "AW_BLEND");
      }
      else
      {
        animateFlags = _config.GetConfigValue("minishell", "animate_flags_hide", "AW_BLEND");
      }

      var bitmaskArr = animateFlags.split("|");
      var animOpts = 0;

      for (var bitmask in bitmaskArr)
      {
        animOpts = animOpts | g_con_animateOpts[bitmaskArr[bitmask]];
      }

      return animOpts;     
    }
        
    function IsExecutionSuccessful(sExitCode, sCid, sVersion) {
      var bSuccess = false;
      var sLibType = $ss.agentcore.dal.content.GetContentFieldValue("sprt_msg", sCid, sVersion, "librarytype", "sccf_field_value_char");
      var oExit = $ss.agentcore.exitcodes.GetExitCodeDetails(sLibType, sExitCode);
    
      if(oExit !== null) {
        if (oExit.exittype === "0") bSuccess = true;
      } else {
        if(sExitCode == 0) bSuccess = true;
      }
      return bSuccess;
    }

})();
