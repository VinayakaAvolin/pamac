
$ss.snapin = $ss.snapin || {};
$ss.snapin.msgViewer =$ss.snapin.msgViewer || {};
$ss.snapin.msgViewer.Helper = $ss.snapin.msgViewer.Helper || {};

(function()
{
    
    $.extend($ss.snapin.msgViewer.Helper, 
    {
        /********************************************************************************/
        //All Public functions
        /********************************************************************************/
      
   
        /*******************************************************************************
        ** Name:        cl_msg_Message()
        **		
        ** Purpose:     main function for handling messages 
        **		
        ** Parameter:   oCmdLine
        **               
        ** Return:      none
        *******************************************************************************/
        cl_msg_Message : function(oCmdLine)
        {

            if (typeof(oCmdLine) == "undefined" || oCmdLine == null)
            {
                oCmdLine=_utils_ui.GetObjectFromCmdLine();
            }
      
            if(oCmdLine["syncevent"]) {
                _isSyncEvent = true;
            }else {
                _isSyncEvent = false;
            }
            var oPendingMsgs = _msgprocessor.cl_msg_PeekMessages(oCmdLine);
            var nEventTypeToShow =_msgprocessor.cl_msg_prequal(oCmdLine, oPendingMsgs);
            //added here to support other content types notification...

       
      
            // if we determined that we need to display something, sync contents of messages.xml to filesystem 
            // and save it for real
            if (nEventTypeToShow != EVT_NONE)
            {
                var current_msgs_xml = _msgprocessor.cl_msg_getMessagesXml();
                var updated_msgs_xml = _msgprocessor.cl_msg_updateMessagesXml(current_msgs_xml, true);    
        
        
        
                // idTmp is XML Data island - after syncing contents we load it to island for state.
                if (bMac) {
                    try{
                        var serializer = new XMLSerializer();
                        var contents = serializer.serializeToString(updated_msgs_xml.responseXML);
                       document.getElementById('idTmp').nodeValue = contents;  
                    }catch(ex){
                    
                    }
                }else {
                document.getElementById('idTmp').load(updated_msgs_xml);  
            }
        
            } 

            this.g_notificationType = nEventTypeToShow;
      
            try    
            {
                switch (nEventTypeToShow)
                {    
                    case EVT_TRIGGEREVENT:
                        this.cl_msg_showMessages(oPendingMsgs, EVT_TRIGGEREVENT, oCmdLine["triggerevent"]);
                        break;
                    case EVT_SYNCEVENT:
                        this.cl_msg_showMessages(oPendingMsgs, EVT_SYNCEVENT);
                        break;
                    case EVT_TIMEREVENT:
                        this.cl_msg_showMessages(oPendingMsgs, EVT_TIMEREVENT);
                        break;
                    case EVT_NONE:      
                    default:
                        this.cl_msg_close();      
                }       
            }
            catch (oErr)
            {
                try 
                {
                    g_Logger.error("cl_msg_Message", "Error processing messages: " + oErr.message);
                } 
                catch (ex2) 
                {
                }
                throw (oErr);
            }  
   
        },
    
        HideBusyMessage: function(sPath) {
            if(!sPath) {
                $(".busy_message").css("display", "none");
            } else {
                $(".MoreDetails_Busy_" + sPath).css("display", "none");
                $(".ViewAll_Busy_" + sPath).css("display", "none");
            }
        },

        DisplayBusyMsg: function(caller, sPath) {
            this.HideBusyMessage(sPath);
            if(caller == "moredetails") {
                $(".MoreDetails_Busy_" + sPath).css("display", "block");				
            }
            else if(caller == "viewall") {						
                $(".ViewAll_Busy_" + sPath).css("display", "block");							
            }						
        },
			
        /*******************************************************************************
        ** Name:        cl_msg_showMessages()
        **		
        ** Purpose:     determines user preference for displaying messages and calls
        **		appropriate function
        **		
        ** Parameter:   pnCEvent, integer Constant, 
        **                TIMEREVENT   = 0;
        **                SYNCEVENT    = 1;
        **                TRIGGEREVENT = 2;
        **                USEREVENT    = 3;  //no arguments or msglist
        **              Used to pass into next function call so we know why it was
        **              called later
        **               
        ** Return:      none
        **
        *******************************************************************************/
        cl_msg_showMessages: function(oPendingMsgs, pnCEvent, guid)
        {
            //get user's prefs.  
            var sPref = _msgprocessor.getNotificationMethod();
      
            //Get the preference for all other content types - added to support download offers notification
            sPref = _msgprocessor.GetUserPrefNotificationOnContent(pnCEvent,sPref);
      
            // **** SPECIAL USER PREFERENCE OVERRIDE ****
            // if there are any pending critical messages OR if it's a trigger event, then we always
            // override the pref to be "popup", otherwise we honor user's preference
            if (oPendingMsgs.nPendingCriticalMessages || pnCEvent == EVT_TRIGGEREVENT)
            {
                sPref = "popup";
            }
      
            switch(sPref)
            {
                case "disabled":   // user has notification disabled
                    this.cl_msg_close();
                    break;

                case "systray":    // user wants notification through system tray
                    _msgprocessor.cl_msg_notifyTray(oPendingMsgs, true);
                    this.cl_msg_close();  // close after firing event, job's done
                    break;
                       
                default:
                    //user wants notification through minibcont (popup) or something else not yet defined
        
                    this.cl_msg_displayMsgs(pnCEvent, guid);
                    break;    
            }
      
            // Set a registry to store the last notified time
      
            try 
            {
                var msgReg = _const.REG_SPRT + "ProviderList\\" + _config.GetProviderID() + "\\users\\" + _config.GetContextValue("SdcContext:UserName") + "\\Message";
                _registry.SetRegValue(_const.REG_HKCU, msgReg, "msg_notified_datetime", _msgprocessor.cl_msg_getTs());
      
                // Update the status of the systray if notification was anything than systray (whose status is already update above). 
                if (sPref != "systray") 
                {
                    _msgprocessor.cl_msg_setTimeoutOnSystray();
                }
            } 
            catch (ex2) 
            {
            }
         
        },
        cl_msg_getTmpXmlObj: function()
        {
            _databag.SetValue("db_msg_MessageList", true);
            // sync contents of messages.xml to filesystem - cl_msg_getMessagesXml() returns messages.xml XMLDocument
            var current_msgs_xml = _msgprocessor.cl_msg_getMessagesXml();
            var updated_msgs_xml = _msgprocessor.cl_msg_updateMessagesXml(current_msgs_xml, true);
            _databag.SetValue("db_msg_MessageList", false);
            return(updated_msgs_xml);
        },
    
        /*******************************************************************************
        ** Name:        cl_msg_displayMsgs
        **		
        ** Purpose:     handles display of messages:  gets details/url of message,
        **		transforms to ui, and sets initial view (by setting current index)
        **		
        ** Parameter:   none
        **              
        ** Return:      none
        *******************************************************************************/
   
        cl_msg_displayMsgs: function(pnCEvent, guid)
        {  
            //load idTmp data island - this is where we have cached the latest data
            var oTmp = this.cl_msg_getTmpXmlObj();
      
            // KL (7/17/07): if there's an trigger event, then remove all other non-triggered nodes EXCEPT the
            //               one which got triggered  
            if (pnCEvent == EVT_TRIGGEREVENT && guid)
            {        
                if (bMac) {
                    var nodes = _Xml.SelectNodes(oTmp.responseXML, XPATH_MSG_STRING + "[@sid!='"+sSessionId+"' and p[@name='deleted_dt']='']");
                    if (nodes) {
                      for(var i = 0;  i< nodes.length ; i++) {
                        var aNode = nodes[i];
                        aNode.parentNode.removeChild(aNode);
                      }
                     }

                     nodes = _Xml.SelectNodes(oTmp.responseXML, XPATH_MSG_STRING + "[@guid != '" + guid + "']");
                    if (nodes) {
                      for(var i = 0;  i< nodes.length ; i++) {
                        var aNode = nodes[i];
                        aNode.parentNode.removeChild(aNode);
                      }
                     }

                      var oNode = _Xml.SelectSingleNode(oTmp.responseXML, _msgprocessor.cl_msg_getXPathForMsgGuid(guid));
                if (oNode)
                {
                    oNode.setAttribute("top", "1");
                }
                }
                else {
                oTmp.selectNodes(XPATH_MSG_STRING + "[@t='1' and @guid != '" + guid + "']").removeAll();

                //GS Customization Start: Remove all messages except triggered message.
                oTmp.selectNodes(XPATH_MSG_STRING + "[@guid != '" + guid + "']").removeAll();

                //GS End 
                // set the top attribute to 1 so it will show on top
                var oNode = oTmp.selectSingleNode(_msgprocessor.cl_msg_getXPathForMsgGuid(guid));
                if (oNode)
                {
                    oNode.setAttribute("top", "1");
                }
            }

                
                
            }
            else
            {
                var oNodes = null;
                if (bMac) {
                    var nodes = _Xml.SelectNodes(oTmp.responseXML, XPATH_NONTRIGGERED_STRING);
                    if (nodes) {
                      for(var i = 0;  i< nodes.length ; i++) {
                        var aNode = nodes[i];
                        aNode.parentNode.removeChild(aNode);
                      }
                     }

                     nodes = _Xml.SelectNodes(oTmp.responseXML, XPATH_OUTDATED_STRING);
                    if (nodes) {
                      for(var i = 0;  i< nodes.length ; i++) {
                        var aNode = nodes[i];
                        aNode.parentNode.removeChild(aNode);
                      }
                     }

                    oNodes = _Xml.SelectNodes(oTmp.responseXML, XPATH_MSG_STRING);

                }
                else {
                // for all other types, remove all non-triggered nodes as they don't belong here
                oTmp.selectNodes(XPATH_NONTRIGGERED_STRING).removeAll();
      
            // remove all outdated or non-active nodes
            oTmp.selectNodes(XPATH_OUTDATED_STRING).removeAll();
      
            //get the collection of msg nodes
                     oNodes = oTmp.selectNodes(XPATH_MSG_STRING);
                }
               
            }
      
           
      
           
      
            //store counter, and msg nodes length
            var nI,nMi = oNodes != null ? oNodes.length : 0;
      
            //for each msg in idTmp data island
            for (nI=0;nI<nMi;nI++)
            {  
                _system.Sleep(10);
                //also, get the msg's url (if there is one) and add to msg node in idTmp
                oNodes[nI].appendChild(_msgprocessor.cl_msg_createNewPropertyCDataNode(oTmp,"url",_msgprocessor.cl_msg_getMessageUrl(oNodes[nI].getAttribute('guid'),oNodes[nI].getAttribute('ver'))));
            }
      
            //**begin hiding of ui   
            //idMessages is where we innerHTML the UI (transformed xml)
            //first, turn of the display so that user only sees finished product - in case of slowness
            $('#idMessages').css("display", 'none');
      
    
            //transform idTmp data island with what ever idXsl is set to  - currently, popview.xsl 
            sDefaultHTM = $("#idMessages").html();
            if (bMac) {
                var xsltProcessor = new XSLTProcessor();
                var doc = document.getElementById('idXsl');
                var path = doc.getAttribute('src');
                var xsl = _Xml.LoadXML(path,false);
                xsl = this.xslResolveIncludes(xsl.responseXML);
                xsltProcessor.importStylesheet(xsl);
                var tr = xsltProcessor.transformToFragment(oTmp.responseXML, document);
                $('#idMessages').html(tr);
            }else {
            $('#idMessages').html(oTmp.transformNode(document.getElementById('idXsl')));
        }
            //now, perform localization on the new output (which is still hidden)
            $ss.agentcore.utils.ui.FixPng();    
            if (pnCEvent == EVT_TRIGGEREVENT && guid)
            {
                // after the xsl transform,
                // update messages.xml to indicate this message was "triggered"
                _msgprocessor.updateXMLAttribute(guid, "t", "2");
            }    

            //we need to update the pagecounter so count all "message" tags to see what the total count is
            //"message" tag is special and just used by us and has no display...it makes it easier to track
            //messages in ui for user    
            var oMsgs = $("#idMessages div.clsMsgMarker");
      
            //store index counter, and count of messages  
            var nX=0,nMx=oMsgs.length;
      
            var guidArray = new Array();  
      
            //for each message
            for(nX=0;nX<nMx;nX++)
            {  
                _system.Sleep(10);
                //set the attr idx of that "message" to index counter (later we use this to determine
                //which message user is currently on)
                oMsgs[nX].setAttribute('idx',nX+1);
        
                //since we don't know total message counts in the UI (because of the way the xsl is ran)
                //we have to tell each message element what the total count is now since we have that info.
                //we could use this information later but currently we don't
                oMsgs[nX].setAttribute('of',nMx);
        
                guidArray[nX] = oMsgs[nX].getAttribute('guid'); 
        
                //modify the #id of the content ...
                $("#idMessages " + "#id" + guidArray[nX] + " .idMsgCounterPageId").text(nX+1);
        
        
            }
            //modify the total count of number..
            $("#idMessages  .idMsgCounterTotalPageCount").text(nMx);

            var oMsgsDoForMe = $("#idMessages div.clsMsgMarker span.button:disabled");
            var len = oMsgsDoForMe.length;
            for(var k=0;k<len;k++)
            {
                oMsgsDoForMe[k].className = "btn btn-primary-ss btn-sm buttondisabled";
                //GS Customization Start: Hide Delete button for triggered message
                // Only one message will be available as we are removing all other messages
                if (pnCEvent == EVT_TRIGGEREVENT) {
                    try {
                        $("#btndelmsg")[0].style.display = "none";
                        $("#cl_msg_ViewDetailsLink")[0].style.display = "none";
                        $("#cl_msg_ViewAllLink")[0].style.display = "none";
                    }
                    catch (ex) {
                    }
                }
                    //GS Customization End     
                }     
      
                _databag.SetValue("ss_msg_db_guids", guidArray.toString());
      
                //if we have any messages in UI
                if (oMsgs.length)
                {    
                    //set the global index pointer to the 1st one
                    g_nCurrentMessage = 1;
        
                    //and cl_msg_toggleDisplayOf() which will turn off the display of all messages except
                    //for g_nCurrentMessage
                    this.cl_msg_toggleDisplayOf();
        
                    //**end hiding of ui
                    //all done, now turn on UI so user can see messages 
                    $('#idMessages').css("display", 'block');
        
                    //show bcont if hidden    
                    _msgprocessor.cl_msg_show();    
                }
                else
                {   
                    //no messages to show so let's just close
                    //ref:ss_container.js:ss_con_Close();
                    this.cl_msg_close();
                }
            },
      
      xslResolveIncludes: function( xsl )
    {
      var xslns = "http://www.w3.org/1999/XSL/Transform";
      var includes = xsl.getElementsByTagNameNS( xslns, "include" ); // NodeList

      for ( var i = includes.length -1; i >= 0; i-- )
      {
        var n = includes.item(i);
        var path = _file.BuildPath(xsl.documentURI + "/../",n.getAttribute('href'));
        var d = _Xml.LoadXML(path,false);

        this.replaceNode( n, d.responseXML.documentElement.childNodes );
      }
      return xsl;
    },
    
    replaceNode: function( orig, replacement )
    {
      var tmp;
      //if (typeof replacement == "
      if ( replacement instanceof NodeList)
      {
        var frag = document.createDocumentFragment();
        for ( var i = 0; i < replacement.length; i ++ )
          frag.appendChild( document.importNode( replacement.item(i), true ) );
        replacement = frag;
      }
      orig.parentNode.replaceChild( tmp=document.importNode( replacement, true ), orig );
      return tmp;
    },
            /*******************************************************************************
            ** Name:        cl_msg_ChangeButtonState
            **		
            ** Purpose:     for next arrow to be grayed when no more messages are there
            **		
            ** Parameter:   poThis, object, span element containing background image of arrow
            **              bFlag : 0 : disable
            **                      1 : enable
            **              
            ** Return:      none
            *******************************************************************************/
            cl_msg_ChangeButtonState: function(poThis, bFlag)
            {
                try {
                    if(!poThis) return;
                    var elemBtn = $("#" + poThis);
                    if(poThis.hasClass("previous")) { 
                        if (bFlag) {
                            poThis.removeClass("previousdisabled");
                            poThis.attr("disabled", false);
                        } else {
                            poThis.removeClass("previous");
                            poThis.addClass("previousdisabled");
                            poThis.attr("disabled", true);
                        }  
                    } else if (poThis.hasClass("next")) {
                        if (bFlag) {
                            poThis.removeClass("nextdisabled");
                            poThis.attr("disabled", false);
                        } else {
                            poThis.removeClass("next");
                            poThis.addClass("nextdisabled");
                            poThis.attr("disabled", true);
                        }
                    }  
                } catch (ex) {
                    try {
                        g_Logger.error("cl_msg_dropHover", "Unexpected Error " + ex.message);
                    } catch (ex2) {}
                }
            },

            /*******************************************************************************
            ** Name:        cl_msg_getMessageProperty
            **		
            ** Purpose:     Retrieves the message property value.
            **		
            ** Parameter:   Guid of the content, Version # of the content, name of property
            **              
           ** Return:      Content property value
            *******************************************************************************/
            cl_msg_getMessageProperty: function(psContentType,psGuid,psVersion,psProperty)
            {
                try {
                    if (psContentType == '')
                        return _content.GetContentProperty("sprt_msg", psGuid, psVersion, psProperty);
                    else
                        return _content.GetContentProperty(psContentType, psGuid, psVersion, psProperty);
                } catch(ex) {}
            },
            /*******************************************************************************
            ** Name:        cl_msg_toggleDisplayOf
            **		
            ** Purpose:     updates the ui to display current message only.
            **		
            ** Parameter:   none
            **              
            ** Return:      none
            *******************************************************************************/
            cl_msg_toggleDisplayOf: function()
            {  
                if (g_showAll) return;

                //get collection of messages elements - this tell us how many messages in current ui
                var oMsgs = $("#idMessages div.clsMsgMarker");
      
                //var counter and  msgs.length
                var nX=0,nMx=oMsgs.length;
      
                //store guid of message that we are currently on
                var sGuid = '';     
                var sDescription = '';                
  
  
                var nCurX = g_nCurrentMessage - 1;            // current message index
                if (nCurX < 0) nCurX = 0;                     // check for lower-bound

                sGuid = oMsgs[nCurX].getAttribute('guid');    // get the msg guid from the element  
                g_nCurrentMessageGUID = sGuid;                // set the global GUID to the currently select GUID      
                $("#" + sGuid).css("display", 'block');     // set display to block
                if(oMsgs[nCurX].getAttribute('content_type') == "sprt_download"){
                  var ocontentobj = $ss.agentcore.dal.content.GetContentByCid(["sprt_download"], sGuid);
                  sDescription = ocontentobj.description;
                }
                
                if(sDescription === "") {
                  sDescription = this.cl_msg_getMessageProperty(oMsgs[nCurX].getAttribute('content_type'),sGuid,oMsgs[nCurX].getAttribute('version'),'scc_description');
                }
     
                //GS Customizations start : Display description for trigger events 
                var deslength = 50;
                var extrades = "";
                if(sDescription.length > 49)
                  extrades = " ....";  
                if (this.g_notificationType != EVT_TRIGGEREVENT)
                {       
                  // GS : Display description for message events        
                  if (sDescription.length < 50) {
                    $('#' + sGuid + ' #cl_msg_ViewDetailsLink').css("display", "none")
                  }
                  else {
                    $('#' + sGuid + ' #cl_msg_ViewDetailsLink').css("display", "block")
                  }        
                }        
                $('#' + sGuid + ' #messagedesc').html(sDescription.substring(0, deslength) + extrades);  
                //GS Customizations end   
                //update the X of N counter on top of UI
                $('#' + sGuid + ' #idMsgCounter1').html(g_nCurrentMessage);
                $('#idMsgCounter2').html(nMx);      
      
                if (g_nCurrentMessage == nMx){
                    g_bAllViewed = true;
                    if($ss.agentcore.state.busy.IsItemBusy("snapin_messagespopup"))
                        $ss.agentcore.state.busy.RemoveFromBusyList("snapin_messagespopup");
                    this.cl_msg_ChangeButtonState($('#' + sGuid + " #idMsgNext"), false);
                } 
                else if (g_nCurrentMessage == (nMx-1)) {
                    this.cl_msg_ChangeButtonState($('#' + sGuid + " idMsgNext"), true);
                }

                //GS Customizations start - Commented the additional confirmation popup shown on close of minibcont(Popup message)
                /*if (g_nCurrentMessage < nMx) {
                  if(g_bExitInfoNeeded && !g_bAllViewed && !$ss.agentcore.state.busy.IsItemBusy("snapin_messagespopup"))
                    $ss.agentcore.state.busy.AddToBusyList("snapin_messagespopup", _utils.LocalXLate("snapin_messagespopup","snp_messages_popup_exit_info"));
                }*/
                //GS Customizations end   
      
                //if the current message index <  1 then reset index to msgs.length (so we loop through)
                if (g_nCurrentMessage == 1){
                    this.cl_msg_ChangeButtonState($("#idMsgPrev"), false);
                } else if (g_nCurrentMessage == 2) {
                    this.cl_msg_ChangeButtonState($("#idMsgPrev"), true);
                }

                var sVer = oMsgs[nCurX].getAttribute('version');
                var tv = oMsgs[nCurX].getAttribute('tv');
                var contentType = oMsgs[nCurX].getAttribute('content_type');

                if (tv != '1')
                {
                    oMsgs[nCurX].setAttribute('tv', '1'); 
                    _msgprocessor.updateXMLAttribute(sGuid, 'tv', '1'); 
                    _msgprocessor.cl_msg_updateTitleViewReport(sGuid, sVer,contentType);  
                }    
      
                for(var tmp=0;tmp < oMsgs.length ;tmp++){
                    if (tmp != nCurX){
                        sGuid = oMsgs[tmp].getAttribute('guid');
                        $("#" + sGuid).css("display", 'none');
                    }
                }
       
            },
      
            cl_msg_IgnoreIt: function(psGuid)
            {
                //load messages.xml
                var oXml = _msgprocessor.cl_msg_getMessagesXml();
      
                //load msg node for the one we want to ignore
                var oNode = null;
                if (bMac) {
                    oNode = _Xml.SelectSingleNode(oXml.responseXML,_msgprocessor.cl_msg_getXPathForMsgGuid(psGuid));
                }else {
                    oNode = oXml.selectSingleNode(_msgprocessor.cl_msg_getXPathForMsgGuid(psGuid));
                }      
                // Gets the version of this message
                var oVersion = oNode.getAttribute('ver');
      
                //let's check just in case
                if(oNode)
                {    

                    if (bMac) {
                        var node = _Xml.SelectSingleNodeFromNode(oNode, "name","deleted_dt");
                        if (node) {
                            node.textContent = _msgprocessor.cl_msg_getTs();
                        }
                        for (i = 0 ; i < oNode.childNodes.length ; i++) {
                            var node = oNode.childNodes[i];
                            if (node.attributes.length > 0) {
                                if (node.attributes[0].nodeValue != 'deleted_dt') {
                                    oNode.removeChild(node);
                                    i--;
                                }
                            }
                        }
                        var serializer = new XMLSerializer();
                        var writetofile = serializer.serializeToString(oXml.responseXML);
                        _msgprocessor.cl_msg_saveToFile(writetofile, _msgprocessor.cl_msg_getMessageXMLFilePath());

                }
                else {
                    //set delete_dt property to timestamp 
                    oNode.selectSingleNode("p[@name='deleted_dt']").text = _msgprocessor.cl_msg_getTs();
        
                    //remove all unecessary p nodes (everything except for the one that had deleted_dt)
                    oNode.selectNodes("p[@name!='deleted_dt']").removeAll();
        
                    //save the new message.xml
                    _msgprocessor.cl_msg_saveToFile(oXml.xml, _msgprocessor.cl_msg_getMessageXMLFilePath());
        
                }

                    
                    
        
                    // decrement current message if it is the last message
                    // var oMsgs = ss_ui_html_getElementsByClassName("div", "clsMsgMarker", ss_utl_Id("idMessages"));    
                    var oMsgs =$("#idMessages div.clsMsgMarker");    

                    if (g_nCurrentMessage == oMsgs.length) g_nCurrentMessage--;
                }

                //wipe out the node for this message  
                if (bMac) {
                    var node = document.getElementById('id'+psGuid);
                    node.parentNode.removeChild(node);
                } 
                else {
                document.getElementById('id'+psGuid).removeNode(true);
                }
      
                //Adding this event for reporting
                _msgprocessor.cl_msg_updateDeleteReport(psGuid,oVersion);
       
                this.cl_msg_resynchTmpMessages();  
            },
            cl_msg_ViewAllLink: function()
            {
                try {
                    var url = _config.GetConfigValue("preconfigured_snapin_url","supportmessage_list");                       
                    var sArgs ="";
                    _LaunchBcont(url,sArgs, "viewall");
                }catch(ex) {
                    g_Logger.error("MessagePopupSnapin:$ss.snapin.msgViewer.Helper.cl_msg_ViewAllLink()",ex.message);
                }
            },
            /*******************************************************************************
            ** Name:        cl_msg_close
            **            
            ** Purpose:     Closes the window conditionally based on whether it is a show all
            **            
            ** Parameter:   none
            **              
            ** Return:      none   
            *******************************************************************************/
            cl_msg_close: function()
            {
                try {
                    if (g_showAll) return;
      
                    // start fading away
                    _msgprocessor.AnimateHideWindow();
       
                    _utils_ui.HideBcont();
                    // close it for real
                    _utils_ui.CloseBcont(); 
                }catch(ex) {
                    g_Logger.error("MessagePopupSnapin:$ss.snapin.msgViewer.Helper.cl_msg_close()",ex.message);
                }

            },
            /*******************************************************************************
            ** Name:        cl_msg_checkKey
            **		
            ** Purpose:     For accessiblity purposes, trap enter key and send click event. 
            ** 		Used in tabstops
            **		
            ** Parameter:   element to send click event to
            **              
            ** Return:      none   
            *******************************************************************************/ 
            cl_msg_checkKey: function(el)
            {  
                try {
                    if (event.keyCode==13)
                        el.click();
                }
                catch(ex) {
                    g_Logger.error("MessagePopupSnapin:$ss.snapin.msgViewer.Helper.cl_msg_checkKey()",ex.message);
                }
            },
            /*******************************************************************************
            ** Name:        cl_msg_ViewDetailsLink
            **		
            ** Purpose:     handler for ui view details click for a given message.  Calls
            **		cl_msg_runBigBcont with psGuid
            **		
            ** Parameter:   psGuid, string, guid of message to provide to bcont
            **              
            ** Return:      none
            *******************************************************************************/
            cl_msg_ViewDetailsLink: function(psGuid)
            {
                try {
                    var sArgs = "/msgguid " + psGuid;
                    var url = _config.GetConfigValue("preconfigured_snapin_url","supportmessage_list")                        
                    _LaunchBcont(url, sArgs, "moredetails");
                }      
                catch(ex) {
                    g_Logger.error("MessagePopupSnapin:$ss.snapin.msgViewer.Helper.cl_msg_ViewDetailsLink()",ex.message);
                }
            },
            /*******************************************************************************
            ** Name:        cl_msg_dropClick
            **		
            ** Purpose:     for arrows on display, this returns arrows back to normal (ie,
            **		called by onmouseout event).  calls from ui - look in xsl (pagercount)
            **		
            ** Parameter:   poThis, object, span element containing background image of arrow
            **              
            ** Return:      none
            *******************************************************************************/
            cl_msg_dropClick: function(poThis)
            {
                // do nothing
            },
            /*******************************************************************************
            ** Name:        cl_msg_dropHover
            **		
            ** Purpose:     for arrows on display, this drops arrows down to hover slide (ie,
            **		called by onhover event).  calls from ui - look in xsl (pagercount)
            **		
            ** Parameter:   poThis, object, span element containing background image of arrow
            **              
            ** Return:      none    
            *******************************************************************************/
            cl_msg_dropHover: function(poThis)
            {
                try {
                    if(!poThis) return;
                    if(poThis.hasClass("clsMsgPrev")) {
                        poThis.addClass("clsMsgPrevOver");
                    } else if (poThis.hasClass("clsMsgNext")) {
                        poThis.addClass("clsMsgNextOver");
                    }  
                } catch (ex) {
                    try {
                        g_Logger.error("cl_msg_dropHover", "Unexpected Error " + ex.message);
                    } catch (ex2) {}
                }
            },
            /*******************************************************************************
            ** Name:        cl_msg_dropBack
            **		
            ** Purpose:     for arrows on display, this returns arrows back to normal (ie,
            **		called by onmouseout event).  calls from ui - look in xsl (pagercount)
            **		
            ** Parameter:   poThis, object, span element containing background image of arrow
            **              
            ** Return:      none
            *******************************************************************************/
            cl_msg_dropBack: function(poThis)
            {
                try {
                    if(!poThis) return;
                    poThis.removeClass("clsMsgPrevOver");
                    poThis.removeClass("clsMsgNextOver");
                } catch (ex) {
                    try {
                        g_Logger.error("cl_msg_dropBack", "Unexpected Error " + ex.message);
                    } catch (ex2) {}
                }
            },
            /*******************************************************************************
            ** Name:        cl_msg_prevMessage
            **		
            ** Purpose:     called by ui when arrows are clicked - sets up call to determine which
            **		message to display next (by turning on/off displays)
            **		
            ** Parameter:   none
            **              
            ** Return:      none   
            *******************************************************************************/
            cl_msg_prevMessage: function()
            {
                try {
                    //set global current message index by decreasing by one
                    if(g_nCurrentMessage==1) return false;
                    g_nCurrentMessage--;

                    $("#idMessages .idMsgCounterPageId").text(g_nCurrentMessage);
                    //now call cl_msg_toggleDisplayOf() to determine which msg to show next
                    this.cl_msg_toggleDisplayOf();
                }
                catch(ex) {
                    g_Logger.error("MessagePopupSnapin:$ss.snapin.msgViewer.Helper.cl_msg_prevMessage()",ex.message);
                }
            },
            /*******************************************************************************
            ** Name:        cl_msg_nextMessage
            **		
            ** Purpose:     called by ui when arrows are clicked - sets up call to determine which
            **		message to display next (by turning on/off displays)
            **		
            ** Parameter:   none
            **              
            ** Return:      none   
            *******************************************************************************/
            cl_msg_nextMessage: function()
            {
                try { 
                    if($("#idMsgNext").attr("disabled")) return;
                    //set global current message index by increasing by one
                    g_nCurrentMessage++;

                    //now call cl_msg_toggleDisplayOf() to determine which msg to show next
                    this.cl_msg_toggleDisplayOf();
                }      
                catch(ex) {
                    g_nCurrentMessage--; 
                    g_Logger.error("MessagePopupSnapin:$ss.snapin.msgViewer.Helper.cl_msg_nextMessage()",ex.message);
                }
                $("#idMessages .idMsgCounterPageId").text(g_nCurrentMessage);
            },
            /*******************************************************************************
            ** Name:        cl_msg_ExecPayload
            **		
            ** Purpose:     for Executing any payload type - run SA, Viewlet, both, or Tell me URL
            **		
            ** Parameter:   psGuid, string, guid of message that contains SA to run
            **		psVersion, string, version of message that contains SA to run
            **		pCType, integer/Constant, corresponds with message type:
            **		0 = TYPE_SUPPORTACTIONONLY - SA only
            **		1 = TYPE_VIEWLETONLY - viewlet only
            **		2 = TYPE_VIEWLETACTION - viewlet and SA
            **    3 = TYPE_TELLME - Tell me URl
            **              
            ** Return:      none
            *******************************************************************************/
            cl_msg_ExecPayload: function(psGuid,psVersion,pCType,buttonElement,sContentType)
            {
                // do nothing if buttonElement is disabled
                if (buttonElement.disabled) return;
      
                //store local
                var sAction,sViewlet;
                var bSuccess = true;
      
                // For backward compatibity -- only keep the checking for sprt_download only...
                if (sContentType === "sprt_msg" || sContentType === "") {
                    // If message folder is no longer available, i.e. pruned, then send event for a refresh
                    if (!_file.FolderExists(_msgprocessor.cl_msg_getBuildPath(_msgprocessor.cl_msg_getMessagesPath(), psGuid + "." + psVersion))) {
                        _events.Send(_events.Create(SS_EVT_BCONT_REFRESH));
                        return;
                    }
                }
      
                // gray out and disable link/button so user can't click on it twice
                // it will be re-enabled if payload did not get executed successfully  
                buttonElement.disabled = true;
                buttonElement.className = "btn btn-primary-ss btn-sm buttondisabled";
      
                var bNeedsElevation = (buttonElement.elevation == "1");
                payloadObject = new Object();
                payloadObject.psGuid = psGuid;
                payloadObject.psVersion = psVersion;
                payloadObject.pCType = pCType;
                payloadObject.buttonElement = buttonElement;
                payloadObject.sContentType = sContentType;
                
                if (bMac) {
                    _msgprocessor.cl_msg_ExecPayloadHelperMac(this, psGuid,psVersion,pCType,sContentType);
                }else {
                    bSuccess = _msgprocessor.cl_msg_ExecPayloadHelper(psGuid,psVersion,pCType,sContentType);
                    payloadObject.bSuccess = bSuccess;
                    this.cl_msg_Update_after_execution(payloadObject);
                }
               
            },

            cl_msg_ExecPayloadHelperCompleted: function(result) {
                payloadObject.bSuccess = result;
                this.cl_msg_Update_after_execution(payloadObject);
            },

            cl_msg_Update_after_execution: function(result) {
                 // when running supportactions, 0 means success
                var bSuccess = result.bSuccess;
                var psGuid = result.psGuid;
                var psVersion = result.psVersion;
                var pCType = result.pCType;
                var buttonElement = result.buttonElement;
                var sContentType = result.sContentType;

                var pXML;
                // When user performs an action, it should change to "viewed"
                if (bSuccess) {
                    this.cl_msg_updateExecutedReport(psGuid,'S',psVersion,pCType,sContentType);
        
                    // Try to update the title font weight
                    try {
                        var titleElement = $('#title' + psGuid);   
                        titleElement.css("fontWeight", "normal");
                    } catch (e) {}    
        
                } else {
                    this.cl_msg_updateExecutedReport(psGuid,'U',psVersion,pCType,sContentType);       
                }
                //if the content type is different do not do anything
                if (!(sContentType === "sprt_msg" || sContentType === "")) {
                    buttonElement.disabled = false;
                    buttonElement.className = "btn btn-primary-ss btn-sm button";
                    return bSuccess;
                }
                // Update XML file
                pXML = _msgprocessor.updateXMLValue(psGuid, 'payload_status', bSuccess ? 'S' : 'U');
                if(pXML)
                {
                    if (bMac) {
                        var serializer = new XMLSerializer();
                        var contents = serializer.serializeToString(pXML.responseXML);
                        document.getElementById('idTmp').nodeValue = contents;
                    }else{
                    document.getElementById('idTmp').load(pXML);
                } 
                } 
                // Re-enable button/link if payload did not get executed successfully
                // Exceptions are "Tell me" and "Viewlet Only" type, we always want to re-enable them
                if (!bSuccess || isNaN(pCType) || (pCType == TYPE_TELLME) || (pCType == TYPE_VIEWLETONLY) )
                {
                    buttonElement.disabled = false;
                    buttonElement.className = "btn btn-primary-ss btn-sm button";
                }
    
                // set button/link caption to "completed" if payload was executed successfully
                if (bSuccess)
                {
                    // Set the button value after it has been clicked
                    buttonElement.innerHTML = _msgprocessor.cl_msg_getValueFrom(psGuid,psVersion,XPATH_COMPLETEDCAPTION_STRING);
                } else {
                    buttonElement.innerHTML = _utils.LocalXLate("snapin_messagespopup", "cl_msg_TryAgain");
                }

                // Try to update the status field with the latest status
                try {
                    var statusElement = $('#status' + psGuid);
                    if (bSuccess) {
                        if (pCType == TYPE_TELLME || pCType == TYPE_VIEWLETONLY) {
                            statusElement[0].innerHTML = _utils.LocalXLate("snapin_messagespopup", "sa_msg_status_completed");
                            statusElement[0].className = "status";
                        } else {
                            statusElement[0].innerHTML = _utils.LocalXLate("snapin_messagespopup", "sa_msg_status_success");
                            statusElement[0].className = "status";
                        }
                    } else {
                        statusElement[0].innerHTML = _utils.LocalXLate("snapin_messagespopup", "sa_msg_status_fail");
                        statusElement[0].className = "statuserror";
                    }
                } catch (e) {}   
       
                //GS Customization Start: Closing bcont for triggered doitforme message. 
                try{
                    if (this.g_notificationType == EVT_TRIGGEREVENT)
                    {
                        $ss.agentcore.utils.CloseBcont();
                    }     
                }
                catch(ex){
                }  
                //GS Customization End      
                return bSuccess;
            },

            cl_msg_updateExecutedReport: function(psGuid, psStatus, iVersion, iActionType,sContentType)
            {
                // Capture only once.  If new version of alert is downloaded and detail is viewed, no need to count again.
                var logShownTs = true;

                //load idTmp data island - this is where we have cached the latest data 
                var oTmp = this.cl_msg_getTmpXmlObj();

                //get msg node for psGuid
                var oNode = null;
                if (bMac) {
                    oNode = _Xml.SelectSingleNode(oTmp.responseXML,_msgprocessor.cl_msg_getXPathForMsgGuid(psGuid));
                }
                else {
                    oNode = oTmp.selectSingleNode(_msgprocessor.cl_msg_getXPathForMsgGuid(psGuid));
                }

                if (bMac) {
                    if (_Xml.SelectSingleNodeFromNode(oNode, 'name', 'shown_dt')!= "") ogShownTs = false;
                }
                else {
                if (this.getNodeValueHelper(oNode,"p[@name='shown_dt']")!="") logShownTs = false;
                }
                var pXML=_msgprocessor.updateXMLValue(psGuid, "shown_dt", _msgprocessor.cl_msg_getTs());
      
                if(pXML) {
                    if (bMac) {
                        var serializer = new XMLSerializer();
                        var contents = serializer.serializeToString(pXML.responseXML);
                    document.getElementById('idTmp').nodeValue = contents;

                    }else {
                    document.getElementById('idTmp').load(pXML);
                    }
                }

         
                if ((psStatus  != undefined) && logShownTs)
                {
                    // actionstring not logged now.May be used in future
                    var actionstring = "";
  
                    switch(iActionType)
                    {
                        case 0: actionstring = "_do"; break;
                        case 1: 
                        case 2: actionstring = "_show"; break;
                        case 3: actionstring = "_tell"; break;
                    }
      
                    if (psStatus == 'S')
                        var oLogEntry = $ss.agentcore.reporting.Reports.CreateLogEntry(psGuid,iVersion,"msg_execute", 1,"","","","", $ss.agentcore.constants.RESULT_SUCCESS);
                    else if (psStatus == 'U')
                        var oLogEntry = $ss.agentcore.reporting.Reports.CreateLogEntry(psGuid,iVersion,"msg_execute", 1,"","","","", $ss.agentcore.constants.RESULT_FAILED);
         
                    _msgprocessor.cl_msg_ContentUsage(oLogEntry,false);
                }   
         
            }, 
            getNodeValueHelper: function (poXml,psXPath)
            {
                //store return
                var sReturn = '';
                try
                {
                    //get node and store text property
                    sReturn = this.getNodeHelper(poXml,psXPath).text;
                }
                catch(oErr)
                {
                    //on err, return empty
                    sReturn='';
                }
                return(sReturn);
            },
            getNodeHelper: function(poXml,psXPath)
            {
                //store return
                var vReturn;
                try
                {
                    //get node
                    if (bMac) {
                        vReturn = _Xml.SelectSingleNode(poXml, psXPath);
                    }
                    else{
                    vReturn = poXml.selectSingleNode(psXPath);
                }
                }
                catch(oErr)
                {
                    //on err, return false
                    vReturn = false;
                }
                return(vReturn);
            },
            /*******************************************************************************
            ** Name:        cl_msg_resynchTmpMessages
            **		
            ** Purpose:     counts the "message" tags and sets the idx/of of the each element
            **		which will be read by cl_msg_cl_msg_toggleDisplayOf() 
            **		
            ** Parameter:   none
            **              
            ** Return:      none
            *******************************************************************************/
            cl_msg_resynchTmpMessages: function()
            {  
                //get collection of "message" tags  
                var oMsgs = $("#idMessages div.clsMsgMarker");
      
                //get length and set nX=0
                var nX=0,nMx=oMsgs.length;
  
                //for each one
                for(nX=0;nX<nMx;nX++)
                {    
                    //set idx attr - later this will tell us what msg we are currently reading, etc..
                    oMsgs[nX].setAttribute('idx',nX+1);
        
                    //set of attr - total msg counts which we will need later for X of <N> logic
                    oMsgs[nX].setAttribute('of',nMx);
                }
                //added this code to properly decrement the total and present count of messages on the UI
                $("#idMessages .idMsgCounterPageId").textContent =(g_nCurrentMessage);
                $("#idMessages  .idMsgCounterTotalPageCount").textContent =(nMx);
      
                //if there are messages this go display the next one
                if(nMx)
                {    
                    //will handle display and count (X of <N>)    
                    this.cl_msg_toggleDisplayOf();
                }
                else
                {    
                    //no more msgs so let's just exit here.
                    this.cl_msg_close();
                }
            },
            IsCurrentEventSycnEvent: function() {
                return _isSyncEvent;
            },
            /*******************************************************************************
            ** Name:        cl_msg_Message_Multi()
            **		
            ** Purpose:     main function for handling messages when an existing instance is up
            **		
            ** Parameter:   oCmdLine
            **               
            ** Return:      none
            *******************************************************************************/
            cl_msg_Message_Multi: function(oCmdLine)
            {
                // if no cmd line is passed in, then bail
                if (!oCmdLine) return;
      
                // if the current displayed popup was generated from a triggerevent, 
                // and another trigger event occurs ignore it as it will cause a lot of unwanted behavior
                if (this.g_notificationType == EVT_TRIGGEREVENT && oCmdLine["timerevent"]) return;
      

      
                if (oCmdLine["syncevent"] || oCmdLine["timerevent"]) {
                    var found = false;
                    if (!g_oFso) g_oFso = _file.GetFileSystemObject();  
                    var oFolder = g_oFso.GetFolder(_msgprocessor.cl_msg_getMessagesPath());
                    for (var oEnum = new Enumerator(oFolder.SubFolders); !oEnum.atEnd(); oEnum.moveNext())
                    {
                        if ((new Date(oEnum.item().DateLastModified)) > _msgprocessor.g_latestSynchFolderDate) {
                            found = true;
                            break;
                        }
                    }
                    oFolder = null;
                    oEnum = null;
        
                    if (!found) return;
                }
      
                // pre-qualification test to see what type of event to show or not at all
                var oPendingMsgs = _msgprocessor.cl_msg_PeekMessages(oCmdLine);
                var nEventTypeToShow = _msgprocessor.cl_msg_prequal(oCmdLine, oPendingMsgs);
      
                var bRefreshExistingInstance = false;
  
                try
                {    
                    var oNodesToCheck = [];  // initialize to empty    
        
                    switch (nEventTypeToShow)
                    {
                        case EVT_SYNCEVENT:        
                            oNodesToCheck = oPendingMsgs.sMsgXML.selectNodes(XPATH_PENDING_CRITICAL_STRING);
                            break;
                        case EVT_TIMEREVENT:        
                            oNodesToCheck = oPendingMsgs.sMsgXML.selectNodes(XPATH_PENDING_ANY_STRING);                
                            break;    
                        case EVT_TRIGGEREVENT:
                            // refresh if the new trigger msg is not already showing in the existing instance
                            var triggerGuid = oCmdLine["triggerevent"];        
                            if ($("#" + triggerGuid).length == 0) {
                                // consider this message is triggered
                                _msgprocessor.updateXMLAttribute(triggerGuid, "t", "2");
                                bRefreshExistingInstance = true;
                            }
                            break;
                        case EVT_NONE:      
                        default:
                            break;
                    }

                    // this loop is for syncevent and timerevent
                    for (var i = 0; i < oNodesToCheck.length; i++)
                    {
                        var displayNode = $("#" + oNodesToCheck[i].getAttribute('guid'));
                        if (displayNode.length == 0)
                        {
                            bRefreshExistingInstance = true;   
                            break;
                        }
                    }
                }
                catch (ex)
                {
                    try {
                        g_Logger.error("cl_msg_Message_Multi", "Error processing multi-instance: " + ex.message);
                    } catch (ex2) {}
                }
  
                if (bRefreshExistingInstance)
                {
                    //this.cl_msg_ChangeButtonState($("#idMsgPrev"), false);
                    //this.cl_msg_ChangeButtonState($("#idMsgNext"), false);   
                    //var sMsgs = $("#idMessages"); 
        
                    //for(var k=0; k<sMsgs.length; k++)
                    //sMsgs[k].disabled = true;    
      
                    //$("#idMessages #cl_msg_ViewAllLink")[0].style.display = "none";
                    //$("#idMessages #cl_msg_NewAlerts")[0].style.display = "block";
                    //$("#idMessages #cl_msg_NewAlerts")[0].onclick =  function(){$ss.snapin.msgViewer.Helper.cl_msg_ViewAllLink();};
                    var oViewAll = $(".messagemore .cl_msg_ViewAlerts");
                    for(var i = 0; i < oViewAll.length; i++) { 
                        oViewAll[i].style.display = "none";
                    }
                    var oNew = $(".messagemore .cl_msg_NewAlerts");
                    for(var i = 0; i < oNew.length; i++) { 
                        oNew[i].style.display = "block";
                        oNew[i].onclick =  function(){$ss.snapin.msgViewer.Helper.cl_msg_ViewAllLink();};
                    }
                }
            },

            LaunchBcont: function(path, sargs, caller) {
                _LaunchBcont(path, sargs, caller);    
            },

            IsBigBcontRunning: function() {
                if (bMac) {
                    var message = {'JSClassNameKey':'UIUtility','JSOperationNameKey':'isAppRunning', 'JSArgumentsKey':['proactiveAssistUI'], 'JSISSyncMethodKey' : '1'}
                    var result = jsBridge.execute(message);
                    var parsedJSONObject = JSON.parse(result);
                    return parsedJSONObject.Data;
                }else {
                if (_utils.CheckInstance(_GetBCONTMutexPrefix() + "bigbcont"))
                    return true;
                return false;
            }
            }
        });//end of extend
    
    /********************************************************************************/
    //Private Area 
    /********************************************************************************/
    var _content = $ss.agentcore.dal.content;
    var _databag = $ss.agentcore.dal.databag;    
    var _utils =$ss.agentcore.utils;
    var _utils_ui =$ss.agentcore.utils.ui;
    var _file =$ss.agentcore.dal.file;
    var _Xml =$ss.agentcore.dal.xml;
    var _const = $ss.agentcore.constants;
    var _system = $ss.agentcore.utils.system;
    var _config = $ss.agentcore.dal.config;
    var _registry = $ss.agentcore.dal.registry;
    var _shell = $ss.agentcore.shell;
    var g_Logger = $ss.agentcore.log.GetDefaultLogger("$ss.snapin.msgViewer.Helper");  
    var _msgprocessor=$ss.snapin.msgprocessor.helper;    
    var _rlogging = $ss.agentcore.reporting.Reports ;
    var _events = $ss.agentcore.events ;
    var _helper = $ss.snapin.msgViewer.Helper;
  
    var _contentPopModels= {};
  
    var sDefaultHTM;
    var g_isRefreshNeeded = false;
    var g_isViewingAlertPage = false;
    var g_bDialogShowing = false;
    var g_notificationType;
    var msgArray;
    var current_msgs_xml;
    var SS_EVT_BCONT_REFRESH          = "SS_EVT_BCONT_REFRESH";
    var SS_EVT_BCONT_CLOSE_DIALOG     = "SS_EVT_BCONT_CLOSE_DIALOG";
    //index to current message to show in ui
    var g_nCurrentMessage              = 0;

    //current message guid
    var g_nCurrentMessageGUID;
  
    //used by big bcont to indicate showing all
    var g_showAll;
    var g_oFso = null;
    var g_bAllViewed = false;
    try {
        var g_bExitInfoNeeded = (_config.GetConfigValue("sprtmsg", "need_unviewed_exit_info", "True").toLowerCase() === "true")? true: false;
        var g_bConfirmExitInfoNeeded = (_config.GetConfigValue("sprtmsg", "need_confirm_exit_info",  "False").toLowerCase() === "false")? false: true; 
    } catch (e) {}
    var g_BlinkCookie = 0;
    var g_NumOfBlinks = 0;
    var g_MAXBLINKS = 3;
    //action types
    var TYPE_SUPPORTACTIONONLY         = 0;
    var TYPE_VIEWLETONLY               = 1;
    var TYPE_VIEWLETACTION             = 2;
    var TYPE_TELLME                    = 3; 
  
    var _isSyncEvent = false;
    var bMac = $ss.agentcore.utils.IsRunningInMacMachine();
    var jsBridge = window.JSBridge;
    var payloadObject = null;
  
    //xpaths for the <guid>.<ver>.xml's
    var XPATH_CONTENTSET_STRING        = "/sprt/content-set/content";
    var XPATH_COMPLETEDCAPTION_STRING  = XPATH_CONTENTSET_STRING + "/field-set/field[@name='Completed Link Caption']/value";
    var XPATH_INCOMPLETECAPTION_STRING = XPATH_CONTENTSET_STRING + "/field-set/field[@name='Incomplete Link Caption']/value";
    var XPATH_PRIORITY_STRING          = XPATH_CONTENTSET_STRING + "/field-set/field[@name='Priority']/value";
    var XPATH_TITLE_STRING             = XPATH_CONTENTSET_STRING + "/field-set/field[@name='Title']/value";
    var XPATH_TELLME_STRING            = XPATH_CONTENTSET_STRING + "/field-set/field[@name='Tell Me']/value";
    var XPATH_VIEWLET_STRING           = XPATH_CONTENTSET_STRING + "/field-set/field[@name='Viewlet']/value[@name='sccfb_field_value_blob']";
    var XPATH_SUPPORTACTION_STRING     = XPATH_CONTENTSET_STRING + "/field-set/field[@name='SupportAction']/value[@name='sccfb_field_value_blob']";
    var XPATH_SA_REQUIRE_ADMIN_STRING  = XPATH_CONTENTSET_STRING + "/field-set/field[@name='SupportAction Admin Privileges']/value[@name='sccf_field_value_int']";
    var XPATH_MOREDETAILS_STRING       = XPATH_CONTENTSET_STRING + "/field-set/field[@name='Detail']/value[@name='sccfc_field_value_clob']";

    var XPATH_EXPIREDATE_STRING        = XPATH_CONTENTSET_STRING + "/property-set/property[@name='scc_expire_date']";
    var XPATH_ACTIVEDATE_STRING        = XPATH_CONTENTSET_STRING + "/property-set/property[@name='scc_active_date']";
    var XPATH_LASTMODDATE_STRING       = XPATH_CONTENTSET_STRING + "/property-set/property[@name='scc_last_mod']";
    var XPATH_TELLMEURL_STRING         = XPATH_CONTENTSET_STRING + "/field-set/field[@name='Tell Me']/value[@name='sccf_field_value_char']";
    var XPATH_ISTRIGGER_STRING         = XPATH_CONTENTSET_STRING + "/field-set/field[starts-with(@name,'TriggerType')]"; 
    
    //event types (or, why were we called)
    EVT_NONE                               = 0;
    EVT_TIMEREVENT                         = 1;
    EVT_SYNCEVENT                          = 2;
    EVT_TRIGGEREVENT                       = 3;
    EVT_USEREVENT                          = 4;  //no arguments or msglist
    EVT_VIEWDETAILSEVENT                   = 5;  //user select to view details

    //path to a msg's detail.htm file (the message) relative from TO_MESSAGES_PATH\<guid>.<version>\dir
    var TO_MESSAGES_DETAILS            = 'Detail.htm';

    //message.xml xpaths
    var XPATH_MSG_STRING               = "/sprt/msgs/msg";

    // non-viewed trigger messages (triggered messages will be @t='2')
    var XPATH_NONTRIGGERED_STRING      = XPATH_MSG_STRING + "[@t='1']";

    // out-dated messages
    var XPATH_OUTDATED_STRING          = XPATH_MSG_STRING + "[p[@name='deleted_dt']!='' or @exp='1' or @act='0']";

    // Pending emergency, un-deleted, un-viewed, non-trigger-type or trigger-type but already triggered, un-expired, activated messages
    var XPATH_PENDING_CRITICAL_STRING  = XPATH_MSG_STRING + "[p[@name='severity']='1' and p[@name='shown_dt']='' and p[@name='deleted_dt']='' and @t!='1' and @exp='0' and @act='1']";

    // Pending any un-deleted, un-viewed, non-trigger-type or trigger-type but already triggered, un-expired, activated messages
    var XPATH_PENDING_ANY_STRING       = XPATH_MSG_STRING + "[p[@name='shown_dt']='' and p[@name='deleted_dt']='' and @t!='1' and @exp='0' and @act='1']";

    function _LaunchBcont(path, sargs, caller) {    
        var sPath = path.replace(/\//g,"");
        var bBcontRunning = _helper.IsBigBcontRunning();
        if (bBcontRunning) {
            _helper.DisplayBusyMsg(caller, sPath);
        } else {
            _helper.HideBusyMessage(sPath);
        }

        var bLaunch = true;
        if(g_bConfirmExitInfoNeeded && $ss.snapin.msgprocessor.helper.GetDisplayedContentType().length > 1 && !bBcontRunning && !g_bAllViewed)
            bLaunch = confirm(_utils.LocalXLate("snapin_messagespopup","snp_messages_popup_view_info"));
        if(bLaunch) {
            try {
                var sArgs = "";
                if(sargs != "undefined")
                    sArgs = sargs;
                path += "?caller=messages"
                if (bMac) {
                    _utils.RunCommand("proactiveAssistUI"  + " /ini "+
                                   _config.GetProviderRootPath() + "\\agent\\bin\\bcont_nm.ini \" /p " + _config.GetProviderID() + 
                                   " /path \"" +path + "\" " + sArgs, $ss.agentcore.constants.BCONT_RUNCMD_ASYNC); 
    
                }else {
                _utils.RunCommand("\"" + _utils.GetBCONTPath() + "\" /ini \""+
                                   _config.GetProviderRootPath() + "\\agent\\bin\\bcont_nm.ini \" /p " + _config.GetProviderID() + 
                                   " /path \"" +path + "\" " + sArgs, $ss.agentcore.constants.BCONT_RUNCMD_ASYNC); 
    
                } 
            }
            catch(Err) {}  
        }
    }
    function _GetBCONTMutexPrefix()
    {
        var mutexPrefix = "__SDC_BCONT_MUTEX__" + _config.GetProviderID();
        return mutexPrefix.toUpperCase();
    }
    function _rl_MsgShownDetail(sFlowId, iVersion,bDoSurvey )
    {
        var oLogEntry = _rlogging.CreateLogEntry(sFlowId, iVersion, "msg_detail", 1, "", "", "", "", "");
        return _msgprocessor.cl_msg_ContentUsage(oLogEntry, bDoSurvey);
    }
  

})();
