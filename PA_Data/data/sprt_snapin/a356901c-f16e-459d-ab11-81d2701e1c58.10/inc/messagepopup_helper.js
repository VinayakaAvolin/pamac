$ss.snapin= $ss.snapin || {};
$ss.snapin.msgPopup=$ss.snapin.msgPopup || {};
$ss.snapin.msgPopup.Helper=$ss.snapin.msgPopup.Helper || {};

(function()
{    
  $.extend($ss.snapin.msgPopup.Helper, 
  { 
    cl_msg_OnMultiInstance: function(oEvent)
    {
      try
      {
        var cmdline = oEvent.description;
        var oCmdLine = _CmdToObjEx_pvt(cmdline.split(" "));         

        if (cmdline.indexOf(BCONT_CLOSE) > -1)
        {
           g_showAll = false;
           _msgViewer.cl_msg_close(); 
        }

        // In Vista, if current popup is originated from a trigger, and another trigger is fired,
        // multiple instances of the same trigger occurs. The following prevents it from happening
        if (oCmdLine["triggerevent"]) {
          if (g_existingTriggeredInstances.indexOf(oCmdLine["triggerevent"]) > -1) return;
          g_existingTriggeredInstances = g_existingTriggeredInstances + oCmdLine["triggerevent"];

          // Apply mutex on trigger to prevent multiple alert to fire on same event
          if (!g_multiInstanceMutex) {
            g_multiInstanceMutex = true;
            _msgViewer.cl_msg_Message_Multi(oCmdLine);
            _system.Sleep(1000);  // Hold for 1 minute before releasing mutex
            g_multiInstanceMutex = false;
          }

        } else {
          _msgViewer.cl_msg_Message_Multi(oCmdLine);
        }

      }
      catch(ex)
      {
        try {
          g_Logger.error("cl_msg_OnMultiInstance", "Exception caught: " + ex.message);
        } catch (e) {}
      }
    },

    cl_msg_OnClose: function()
    {
      try 
      {
        var oPendingMsgs = _msgprocessor.cl_msg_PeekMessages();
        var regRoot = _config.GetRegRoot();
        var user=_config.ExpandSysMacro("%USER%");
        var bSystray=_registry.GetRegValue($ss.agentcore.constants.REG_HKCU, regRoot+"\\users\\"+user+"\\ss_config\\Message", "systray");
        if(bSystray=="True")
          _msgprocessor.cl_msg_notifyTray(oPendingMsgs,true);
        else
          _msgprocessor.cl_msg_notifyTray(oPendingMsgs,false);
		_utilsui.HideBcont();  
      } 
      catch(ex) 
      {
          g_Logger.error("MessagePopupSnapin:$ss.snapin.msgPopup.Helper.cl_msg_OnClose()",ex.message);
        }
    },
    BeforeRender: function()
	  { 
	    _events.Subscribe("BROADCAST_ON_EXTERNALREFRESH", this.cl_msg_OnMultiSnapinInstance);
   
	  },

    
    cl_msg_OnMultiSnapinInstance: function(oEvent)
    { 
	    try
	    {
	      var sInstanceName=$ss.agentcore.dal.ini.GetIniValue("", "PROVIDERINFO", "instancename", "");
	      // only notify if mini bcont is running
	      if(sInstanceName=="minibcont") 
	      {
	        var cmdline = oEvent.description;
          var oCmdLine = _CmdToObjEx_pvt(cmdline.split(" "));
    	    if (cmdline.indexOf(" /trigger ") > -1)
    	    {
    	       $("#triggerwindow").css("display","none");
             $("#divdialog").css("display","block");
             $ss.agentcore.dal.databag.SetValue("triggertype",oCmdLine.trigger);
		      }
  		  	
	      }
	    }
	    catch(err)
	    {
	      g_Logger.error("MessagePopupSnapin:$ss.snapin.msgPopup.Helper.cl_msg_OnMultiSnapinInstance()",err.message);
	    }	
    },
    

    AfterRender: function()
    {
      try
      {
        // Set localized title
        _utilsui.SetTitle(_utils.LocalXLate("snapin_messagespopup", "cl_msg_Header"));
         _events.Subscribe("BROADCAST_ON_EXTERNALREFRESH", this.cl_msg_OnMultiInstance); //Pending
         _events.Subscribe("BROADCAST_ON_EXTERNALCLOSE", this.cl_msg_OnClose); 
        _msgViewer.cl_msg_Message();
         $ss.agentcore.utils.ui.FixPng();
      }
      catch (ex)
      {
        // Any un-handled exception should always leads to bcont closure to prevent zombie instances
        try
        {
          g_Logger.error("AfterRender", "Catch-All exception: " + ex.message);
        }
        catch (e) { }
        finally
        {
          try {

	        _utilsui.HideBcont();
            _utilsui.CloseBcont();
          } catch (e) {}
        }
      }
    }
   
  });//end of extend
  //var _layout = $ss.snapin.getconnect.layoutHelper;
  var _msgprocessor=$ss.snapin.msgprocessor.helper;   
  var _msgViewer = $ss.snapin.msgViewer.Helper;
  var _databag = $ss.agentcore.dal.databag;    
  var _utils =$ss.agentcore.utils;
  var _utilsui =$ss.agentcore.utils.ui;
  var _file =$ss.agentcore.dal.file;
  var _Xml =$ss.agentcore.dal.xml;
  var _const = $ss.agentcore.constants;
  var _system = $ss.agentcore.utils.system;
  var _config = $ss.agentcore.dal.config;
  var _registry = $ss.agentcore.dal.registry;
  var _shell = $ss.agentcore.shell;
  var g_Logger = $ss.agentcore.log.GetDefaultLogger("$ss.snapin.msgViewer.Helper");  
  var _events = $ss.agentcore.events ; 
  var g_showAll;
  var g_existingTriggeredInstances = "";
  var g_multiInstanceMutex = false;
  var BCONT_CLOSE = "/close"; 

  function _CmdToObjEx_pvt(aParameters)
  {
    var oQStr = new Object();
    
    try {
    for(var iCnt=1;iCnt<aParameters.length;iCnt++) {
        var p1=aParameters[iCnt];
        if(p1.substr(0,1) == "/") {
          p1=p1.replace("\/","");
          oQStr[p1] = true;

          if(iCnt+1 < aParameters.length) // if we're on the last arg, don't look for another param
          {
            var x=aParameters[iCnt + 1];
            if(x.substr(0,1) != "/") {
              oQStr[p1]=aParameters[iCnt +1];
              iCnt++;
            }
         }
      }
      }
     } catch (ex) {
      g_Logger.error("CmdToObjEx_pvt()", ex.message);
    } 
    
    return oQStr;
  }
})();