$ss.snapin= $ss.snapin || {};
$ss.snapin.msgdecision=$ss.snapin.msgdecision || {};
$ss.snapin.msgdecision.helper=$ss.snapin.msgdecision.helper || {};

(function()
{
    
  $.extend($ss.snapin.msgdecision.helper, 
  { 
    
    //case 1:if .net dll and message.dll exists - don't do anything
    //case 2:if .net dll exists and message.dll doesn't exist - execute support action
    //case 3: if .net dll doesn't exists and message.dll doesn't exist - don't do anything
    cl_decideExecution:function()
    {
      //To set the default preference values if not present.
      var regRoot = _config.GetRegRoot();
      var user=_config.ExpandSysMacro("%USER%");
      //check for one key
      var bSystray=_registry.GetRegValue($ss.agentcore.constants.REG_HKCU, regRoot+"\\users\\"+user+"\\ss_config\\Message", "enabled");
      if(bSystray=="")
      {
         var DisableNtf=_config.GetConfigValue("sprtmsg","disable_notification","False");
         var EnableNtf=_config.GetConfigValue("sprtmsg","enable_notification","True");
         var PopupNtf=_config.GetConfigValue("sprtmsg","notification_popup","False");
         var SystrayNtf=_config.GetConfigValue("sprtmsg","notification_systray","True");
        _registry.SetRegValue($ss.agentcore.constants.REG_HKCU, regRoot+"\\users\\"+user+"\\ss_config\\Message", "disabled",DisableNtf);
        _registry.SetRegValue($ss.agentcore.constants.REG_HKCU, regRoot+"\\users\\"+user+"\\ss_config\\Message", "enabled",EnableNtf);
        _registry.SetRegValue($ss.agentcore.constants.REG_HKCU, regRoot+"\\users\\"+user+"\\ss_config\\Message", "popup",PopupNtf);
        _registry.SetRegValue($ss.agentcore.constants.REG_HKCU, regRoot+"\\users\\"+user+"\\ss_config\\Message", "systray",SystrayNtf);
      
      }
      
      var PathforFreq="Software\\SupportSoft\\ProviderList\\";
      var AlertFreq=_config.GetConfigValue("sprtmsg","alert_frequencyhrs","168");
      var alert_freq=_registry.GetRegValue($ss.agentcore.constants.REG_HKCU, PathforFreq+_config.GetProviderID()+"\\users\\"+user+"\\Message", "LowFreqHours");
      if(alert_freq=="")
        // _registry.SetRegValue($ss.agentcore.constants.REG_HKCU, PathforFreq+_config.GetProviderID()+"\\users\\"+user+"\\Message", "LowFreqHours",AlertFreq);
        _registry.SetRegValueByType($ss.agentcore.constants.REG_HKCU,PathforFreq+_config.GetProviderID()+"\\users\\"+user+"\\Message","LowFreqHours",$ss.agentcore.constants.REG_DWORD,AlertFreq);
      //.NET AND MSG DLL check
      if((this.cl_isDotNetInstalled()) && (!this.cl_isMessagedllpresent()))
      {
        this.cl_execute_ProcessAction();
        if(this.cl_isMessagedllpresent())//only if sprtmessage dll is correctly registered..rechecking
        {
          var sInstanceName=$ss.agentcore.dal.ini.GetIniValue("", "PROVIDERINFO", "instancename", "");
          this.startStopSprtCMD();
		      //setTimeout($ss.snapin.msgdecision.helper.cl_Settimeout_StartStopSprtCmd,7000);
          
            
        } 
      }
      
    },
    
    cl_Settimeout_StartStopSprtCmd:function()
    {
      $ss.agentcore.utils.StartSprtCmd(false);
      $ss.agentcore.utils.Sleep(10000);
      $ss.agentcore.utils.StartSprtCmd(true);
    },
	  startStopSprtCMD: function() {
      try{
      var bProcessRunning = this.isSprtCmdRunning();
      if (!bProcessRunning) {
        $ss.agentcore.utils.StartSprtCmd(true);
      }
      else {
      
        //firs stop the sprt command .. max time try is 15 times
        $ss.agentcore.utils.StartSprtCmd(false);
        
        for (var count = 1; count <= 12; count++) {
          var isSprtRunning = this.isSprtCmdRunning();
          if (isSprtRunning) {
            $ss.agentcore.utils.StartSprtCmd(false);
            $ss.agentcore.utils.Sleep(2000);
          }
          else {
            $ss.agentcore.utils.StartSprtCmd(true);
            break;
          }
        }
        
      }
      }catch(ex){
        
      }finally{
        //last ditch try once again and fail ...
        $ss.agentcore.utils.StartSprtCmd(true);
      }
    },

	  isSprtCmdRunning: function(){

      var bProcessRunning = false;
      var providerID = $ss.agentcore.dal.config.GetProviderID().toLowerCase();
      var eServiceList = new Enumerator(window.external.GetObject("winmgmts:{impersonationLevel=impersonate}!root/cimv2").ExecQuery("Select CommandLine From Win32_Process where caption='sprtcmd.exe'"));
      for (; !eServiceList.atEnd(); eServiceList.moveNext()) {
        var cmdLine = (new String(eServiceList.item().CommandLine)).toLowerCase();
        if (cmdLine.indexOf(providerID) > -1) {
          bProcessRunning = true;
          break;
        }
      }
      return bProcessRunning;
    },
 
    cl_isMessagedllpresent:function()
    {
      try
      {
      var sProviderID = _config.GetProviderID();
      var sRegRoot = "HKLM";
      var sRegKey = "Software\\SupportSoft\\ProviderList\\" + sProviderID + "\\sprtcmd\\Sprockets\\Message";
      var regval = _registry.GetRegValue("HKLM", sRegKey, "Version");
      if(regval.length>0)
        return true; 
      else
        return false;    
    } 
      catch(err) 
      {
        $ss.agentcore.log.GetDefaultLogger("$ss.snapin.msgdecision.helper").error("cl_isMessagedllpresent()",err.message);
      }
    } 
    ,
    //function to check for the .NET Version 1.1 and above
    cl_isDotNetInstalled:function()
    {
      var sUserAgent=window.navigator.userAgent;
      if (sUserAgent.search(/\.NET CLR ((1\.1)|2|3)/) >=0 ) 
        return true;
      else
        return false;  
          
    }, 
    
    cl_execute_ProcessAction:function()
    {
      try
      {
      var sProviderID = _config.GetProviderID();
      var sSnapinPath = $ss.GetSnapinAbsolutePath("snapin_messagespopup");
      //guid,provider,cab,bElevate,sElevationLevel,callback,authOption
      var oSR=new $ss.agentcore.supportaction("",sProviderID,sSnapinPath + "\\setup\\sa_reg_supportmsg_dll.cab");
      oSR.SetParameter("sProviderID",sProviderID);
      oSR.SetParameter("sDllPath",sSnapinPath + "\\setup\\");
      //oSR.SetParameter("sVersion",sVersion);
      return oSR.Evaluate(); 
      }
      catch(err)
      {
        $ss.agentcore.log.GetDefaultLogger("$ss.snapin.msgdecision.helper").error("cl_execute_ProcessAction()",err.message);
      } 
       
     
    }
    
 
  });//end of extend
    
    /********************************************************************************/
    //Private Area 
    /********************************************************************************/
    var _config = $ss.agentcore.dal.config;    
    var _registry =  $ss.agentcore.dal.registry;
    
       
       
})();

$ss.snapin.msgdecision.helper.cl_decideExecution();