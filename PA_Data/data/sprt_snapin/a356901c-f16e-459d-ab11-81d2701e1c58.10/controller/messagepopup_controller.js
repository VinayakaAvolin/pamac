SnapinMessagespopupController = SnapinBaseController.extend('snapin_messagespopup',
{
  //Private Access Area
  index: function (params) {
    var _config = $ss.agentcore.dal.config;
    var bForceFirstRun = (_config.GetConfigValue("firstrun", "force_first_run", "true") === "true");
    if (bForceFirstRun) {
      var firstRunCompleted = _config.GetUserValue("firstrun", "FirstRunCompleted");
      if (!firstRunCompleted) {
        $ss.agentcore.utils.CloseBcont();
        return;
      }
    }

    //Removing previous history of trigger event.
    $ss.agentcore.dal.databag.RemoveValue("triggertype");
    $ss.snapin.msgPopup.Helper.BeforeRender();
    this.renderSnapinView("snapin_messagespopup", params.toLocation, "\\views\\messagepopup_home.htm");
    $ss.agentcore.utils.ui.FixPng();
    $ss.snapin.msgPopup.Helper.AfterRender();

  },
  "#iddialogmsg #btnOK click": function (params) {
    var params = {};
    params.requesturl = "/trigger?type=" + $ss.agentcore.dal.databag.GetValue("triggertype");
    MVC.Controller.dispatch("navigation", "index", params);
  },
  "#iddialogmsg #btnCancel click": function (params) {
    $("#triggerwindow").css("display", "block");
    $("#divdialog").css("display", "none");
  },
  "#idMsgNext click": function (params) {
    $ss.snapin.msgViewer.Helper.cl_msg_nextMessage();
  },
  "#idMsgNext keypress": function (params) {
    $ss.snapin.msgViewer.Helper.cl_msg_checkKey(params.element);
  },
  "#idMsgPrev click": function (params) {
    $ss.snapin.msgViewer.Helper.cl_msg_prevMessage();
  },
  "#cl_msg_ViewAllLink click": function (params) {
    $ss.snapin.msgViewer.Helper.cl_msg_ViewAllLink();
  },
  "#cl_msg_ViewDetailsLink click": function (params) {
    $ss.snapin.msgViewer.Helper.cl_msg_ViewDetailsLink($(($(params)[0].element.outerHTML)).attr("popguid"), $(($(params)[0].element.outerHTML)).attr("popver"));
  },
  "#btndelmsg click": function (params) {
    $ss.snapin.msgViewer.Helper.cl_msg_IgnoreIt($(($(params)[0].element.outerHTML)).attr("popdelguid"));
  },
  "#cl_msg_Action .button click": function (params) {
    $ss.snapin.msgViewer.Helper.cl_msg_ExecPayload($(($(params)[0].element.outerHTML)).attr("popmsgguid"), $(($(params)[0].element.outerHTML)).attr("popmsgver"), parseInt($(($(params)[0].element.outerHTML)).attr("popmsgtype")), params.element, $(params.element.outerHTML).attr("content_type"));
  },
  ".clsMsgDiv click": function () {
    if (!$ss.snapin.msgViewer.Helper.IsBigBcontRunning())
      $ss.snapin.msgViewer.Helper.HideBusyMessage();
    return;
  }

});


