/**
* @author
*/
$ss.snapin = $ss.snapin || {};
$ss.snapin.manageticket = $ss.snapin.manageticket || {};
$ss.snapin.manageticket.model = $ss.snapin.manageticket.model || {};

/* * This class will hold the information existing Tickets */

(function () {
  $ss.snapin.manageticket.model = Class.extend({
    init: function () {
      try { }
      catch (ex) {
        this._exception.HandleException(ex, '$ss.snapin.manageticket.model', 'init', arguments);
      }
    },
    GetTicketDetails: function (xmlobj, RequestID) {
      if (xmlobj == "") {
        var resobj = {};
        resobj.error = "Message";
        resobj.disp = $ss.agentcore.utils.LocalXLate("snapin_manage_ticket", "servererror");
        return resobj;
      }
      else {
        var xmldoc = new ActiveXObject("Microsoft.XMLDOM");
        xmldoc.async = false;
        if (xmldoc.loadXML(xmlobj)) {
          return GetTicketDetails_Inner(xmldoc, RequestID)
        }
        else {
          var oTicket = {};
          oTicket.error = "Message";
          oTicket.disp = _utils.LocalXLate("snapin_manage_ticket", "errormsg");
          return oTicket;
        }
      }
    },
    GetTicketCount: function (xmlobj) {
      if (xmlobj == "") {
        var resobj = {};
        resobj.error = "Message";
        resobj.disp = $ss.agentcore.utils.LocalXLate("snapin_manage_ticket", "servererror");
        return resobj;
      }
      else {
        var xmldoc = new ActiveXObject("Microsoft.XMLDOM");
        xmldoc.async = false;
        if (xmldoc.loadXML(xmlobj)) {
          return GetTicketsFromXML(xmldoc)
        }
        else {
          var oTicket = {};
          oTicket.error = "Message";
          oTicket.disp = _utils.LocalXLate("snapin_manage_ticket", "errormsg");
          return oTicket;
        }
      }
    },

    AjaxCalls: function (tempdata, sRemedyUrl) {
      var sSessionId = $ss.agentcore.utils.GetSessionId();
      var sUser = _config.ExpandSysMacro("%USER%");
      var sWSrespose = "";
      var data = "integrationId=remedy&username=" + sUser;
      if (tempdata != "") data = data + tempdata;
      return $.ajax({
        type: "POST",
        url: sRemedyUrl,
        timeout: 10000,
        data: data,
        cache: false
      });
    },
  });
  var _config = $ss.agentcore.dal.config;
  var _content = $ss.agentcore.dal.content;
  var _xml = $ss.agentcore.dal.xml;
  var _utils = $ss.agentcore.utils;
  var _file = $ss.agentcore.dal.file;
  var _sOpenTicketsFilePath = $ss.getSnapinAbsolutePath("snapin_manage_ticket") + '\\test_open_tickets.json';
  var _XPATH_GETLISTVALUES = "getListValues";// "ns0:getListValues"; //"getRecordsResult";
  var _XPATH_CASEID = "Request_ID"; //"number";
  var _XPATH_STATUS = "Status"; //"state";
  var _XPATH_DESCRIPTION = "Summary"; //"description";
  var _XPATH_CATEGORY = "Category"; //"ns0:Categorization_Tier_1";
  var _XPATH_TYPE = "Type"; //"ns0:Categorization_Tier_2";
  var _XPATH_ITEM = "Item"; //"ns0:Categorization_Tier_3";
  var _XPATH_CREATEDTIME = "Create_Date"; //"opened_at";
  var _XPATH_MODIFIEDTIME = "Modified_Date"; //"sys_updated_on";
  var _XPATH_WORKLOG = "Status_History"; //"ns0:Work_Log";
  var _XPATH_Dtails = "QueryTicketDetailsResponse/Request_ID"; //"getRecordsResult/number"
  var _XPATH_ASSIGNEE = "Assigned_To"; //"assigned_to"
  var _XPATH_SHORT_DES = "Short_Description"; //"short_description";
  var _XPATH_ACTIVITYLOG = "Activity_Log" //"urgency"
  var _XPATH_TITLE = "Title";

  function GetTicketDetails_Inner(xmlobj, RequestID) {
    try {
      var oTicketDetail = [];
      var objTickets = xmlobj.getElementsByTagName(_XPATH_GETLISTVALUES);
      var numberOfTickets = objTickets.length;
      var j = 0;

      if (numberOfTickets > 0) {
        for (var i = 0; i < objTickets.length; i++) {
          if ((RequestID == objTickets[i].getElementsByTagName(_XPATH_CASEID)[0].text) || RequestID == undefined) {
            var oTickets = {};
            oTickets.Request_ID = objTickets[i].getElementsByTagName(_XPATH_CASEID)[0].text;
            oTickets.Status = objTickets[i].getElementsByTagName(_XPATH_STATUS)[0].text;
            oTickets.Assignee = objTickets[i].getElementsByTagName(_XPATH_ASSIGNEE)[0].text;
            oTickets.Short_Description = unescape(objTickets[i].getElementsByTagName(_XPATH_SHORT_DES)[0].text);
            oTickets.Create_Date = objTickets[i].getElementsByTagName(_XPATH_CREATEDTIME)[0].text;
            oTickets.Modified_Date = objTickets[i].getElementsByTagName(_XPATH_MODIFIEDTIME)[0].text;
            oTickets.ActivityLog = unescape(objTickets[i].getElementsByTagName(_XPATH_ACTIVITYLOG)[0].text);
            oTickets.Description = unescape(objTickets[i].getElementsByTagName(_XPATH_DESCRIPTION)[0].text);
            oTickets.Type = unescape(objTickets[i].getElementsByTagName(_XPATH_TYPE)[0].text);
            oTickets.Item = unescape(objTickets[i].getElementsByTagName(_XPATH_ITEM)[0].text);
            oTickets.Title = unescape(objTickets[i].getElementsByTagName(_XPATH_TITLE)[0].text);
            oTicketDetail[j] = oTickets; j++;
          }
        }
      }
      else {
        var oTicket = {};
        oTicket.error = "Message";
        oTicket.disp = _utils.LocalXLate("snapin_manage_ticket", "notickets");
        return oTicket;
      }
    } catch (ex) {
      var oTicket = {};
      oTicket.error = "Message";
      oTicket.disp = _utils.LocalXLate("snapin_manage_ticket", "errormsg");
      return oTicket;
    }
    return oTicketDetail;
  }
  function GetTicketCount_Inner(xmlobj) {
    try {
      var oTicketDetail = [];
      var objTickets = xmlobj.getElementsByTagName(_XPATH_GETLISTVALUES);
      var numberOfTickets = objTickets.length;
      var numberOfActiveTickets = 0;
      if (numberOfTickets > 0) {
        for (var i = 0; i < objTickets.length; i++) {
          var oTickets = {};
          oTickets.Request_ID = objTickets[i].getElementsByTagName(_XPATH_CASEID)[0].text;
          oTickets.Status = objTickets[i].getElementsByTagName(_XPATH_STATUS)[0].text;
          oTickets.Assignee = objTickets[i].getElementsByTagName(_XPATH_ASSIGNEE)[0].text;
          oTickets.Short_Description = unescape(objTickets[i].getElementsByTagName(_XPATH_SHORT_DES)[0].text);
          oTickets.Create_Date = objTickets[i].getElementsByTagName(_XPATH_CREATEDTIME)[0].text;
          oTickets.Modified_Date = objTickets[i].getElementsByTagName(_XPATH_MODIFIEDTIME)[0].text;
          oTickets.ActivityLog = unescape(objTickets[i].getElementsByTagName(_XPATH_ACTIVITYLOG)[0].text);
          oTickets.Description = unescape(objTickets[i].getElementsByTagName(_XPATH_DESCRIPTION)[0].text);
          oTickets.Type = unescape(objTickets[i].getElementsByTagName(_XPATH_TYPE)[0].text);
          oTickets.Item = unescape(objTickets[i].getElementsByTagName(_XPATH_ITEM)[0].text);
          oTickets.TotalTickets = numberOfTickets;
          oTickets.TotalActiveTickets = numberOfActiveTickets;
          oTickets.Title = unescape(objTickets[i].getElementsByTagName(_XPATH_TITLE)[0].text);
          oTicketDetail[numberOfActiveTickets] = oTickets;
          numberOfActiveTickets++;
        }
      }
      else {
        var oTicket = {};
        oTicket.error = "Message";
        oTicket.disp = _utils.LocalXLate("snapin_manage_ticket", "notickets");
        return oTicket;
      }
    } catch (ex) {
      var oTicket = {};
      oTicket.error = "Message";
      oTicket.disp = _utils.LocalXLate("snapin_manage_ticket", "errormsg");
      return oTicket;
    }
    return oTicketDetail;
  }
  function GetTicketsFromXML(xmlobj) {
    try {
      var oTicketDetail = [];
      var objTickets = xmlobj.getElementsByTagName(_XPATH_GETLISTVALUES);
      var numberOfTickets = objTickets.length;
      if (objTickets.length > 0) {
        for (var i = 0; i < objTickets.length; i++) {
          var oTickets = {};
          oTickets.Request_ID = objTickets[i].getElementsByTagName(_XPATH_CASEID)[0].text;
          oTickets.Title = unescape(objTickets[i].getElementsByTagName(_XPATH_TITLE)[0].text);
          oTickets.TotalTickets = numberOfTickets;
          oTicketDetail[i] = oTickets;
        }
      }
      else {
        var oTicket = {};
        oTicket.error = "Message";
        oTicket.disp = _utils.LocalXLate("snapin_manage_ticket", "notickets");
        return oTicket;
      }
    } catch (ex) {
      var oTicket = {};
      oTicket.error = "Message";
      oTicket.disp = _utils.LocalXLate("snapin_manage_ticket", "errormsg");
      return oTicket;
    }
    return oTicketDetail;
  }
})();