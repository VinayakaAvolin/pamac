(function () {
  SnapinManageTicketController = SnapinBaseController.extend('snapin_manage_ticket',
  {
    oModel: {},
    snapinToLoaction: null,
    initialized: false,
    init: function () {
      this._super();
      var _config = $ss.agentcore.dal.config;
      var _xml = $ss.agentcore.dal.xml;
      var _utils = $ss.agentcore.utils;
    },
    templateLocation: "",
  }, {
    indexWidget: function (pArgs) {
      if (pArgs.toLocation) this.Class.templateLocation = pArgs.toLocation;
      snapinToLoaction = pArgs.toLocation;
      var obj = {};
      obj.error = "Message";
      this.Class.oModel = new $ss.snapin.manageticket.model();
      obj.disp = _utils.LocalXLate("snapin_manage_ticket", "processingWidget");
      this.viewticket(obj, this.Class.templateLocation);
      /*  CHACHE mechanism begin */
      var reload = pArgs.snpmngtck_refresh || isStale();
      var oContent = {};
      var that = this;
      if (pArgs.snpmngtck_refresh) {
        _utils.Sleep(10);
        that.GetAllTickets(reload)
        .done(function (data) {
          try {
            var oTemp = JSON.parse(data);
            that.Class.data = that.Class.oModel.GetTicketCount(oTemp.responseXML);
            that.Class.initialized = true;
            var oOpenTickets = that.Class.data;
            if (that.Class.data.length != undefined)
              that.renderSnapinView("snapin_manage_ticket", that.Class.templateLocation, "\\views\\TicketWidget.html", { tickets: oOpenTickets });
            else {
              that.displayerrorpage(true);
            }
          }
          catch (ex) {
            that.displayerrorpage(true);
          }
        })
        .fail(function (reason) {
          that.displayerrorpage(true);
        });
      }
      else {
        that.GetAllTickets(reload)
        .done(function (data) {
          try {
            oContent.bShowLastUpdateTime = _config.GetConfigValue("snapin_manage_ticket", "show_timestamp").match(/(i?)true/) !== null ? true : false;
            if (oContent.bShowLastUpdateTime === true) oContent.time = formatDateTime(data.time); // conversion to local timezone occurs
            var oTemp = JSON.parse(data);
            that.Class.data = that.Class.oModel.GetTicketCount(oTemp.responseXML);
            that.Class.initialized = true;
            var oOpenTickets = that.Class.data;
            if (that.Class.data.length != undefined) {
              if (obj.disp == _utils.LocalXLate("snapin_manage_ticket", "processingWidget")) that.renderSnapinView("snapin_manage_ticket", that.Class.templateLocation, "\\views\\TicketWidget.html", { tickets: oOpenTickets });
              else that.renderSnapinView("snapin_manage_ticket", snapinToLoaction, "\\views\\ManageTickets.html", { tickets: oOpenTickets });
            }
            else {
              that.displayerrorpage(false);
            }
          }
          catch (ex) {
            that.displayerrorpage(false);
          }
        })
        .fail(function (reason) {
          that.displayerrorpage(false);
        });
      }
    },
    index: function (pArgs) {
      var that = this;
      snapinToLoaction = pArgs.toLocation;
      var obj = {};
      obj.error = "Message";
      this.Class.oModel = new $ss.snapin.manageticket.model();
      obj.disp = _utils.LocalXLate("snapin_manage_ticket", "processing");
      this.viewticket(obj, snapinToLoaction);
      _utils.Sleep(10);

      var oOpenTickets = this.Class.data;
      if (pArgs.requestQS.indexOf("create") > -1) {
        this.renderSnapinView("snapin_manage_ticket", snapinToLoaction, "\\views\\CreateTicket.html", { tickets: oOpenTickets }); return;
      }
      var oResponse = "";
      var Request_ID = $ss.agentcore.utils.GetQSObjectFromURL(pArgs.requesturl).RequestID
      if (Request_ID != undefined) {
        var respobj = {};
        var sRemedyUrl = _config.GetConfigValue("snapin_manage_ticket", "qualification_url", "").replace("SERVER_URL", NavigationController.GetServerUrl());
        var tempdata = "&Request_ID= " + Request_ID;
        this.callAjaxCallInModel(tempdata, sRemedyUrl)
        .done(function (resp) {
          if (resp != null) {
            if (resp.error) return;
            resp.time = $.now();
            var oTemp = JSON.parse(resp);
            oResponse = oTemp.responseXML;
            respobj = that.Class.oModel.GetTicketDetails(oResponse, Request_ID)
            that.renderSnapinView("snapin_manage_ticket", snapinToLoaction, "\\views\\ViewTicket.html", { tickets: respobj });
          }
          else {
            respobj = that.Class.oModel.GetTicketDetails(oResponse, Request_ID)
            that.renderSnapinView("snapin_manage_ticket", snapinToLoaction, "\\views\\ViewTicket.html", { tickets: respobj });
          }
        })
        .fail(function () {
          that.Class.data = that.Class.oModel.GetTicketDetails(oResponse, Request_ID);
          that.Class.initialized = true;
          that.viewticket(that.Class.data);
        })
      }
      else {
        this.SendAjaxCall("allopentickets", obj)
        .done(function (resp) {
          try {
            var oTemp = JSON.parse(resp);
            oResponse = oTemp.responseXML;
            that.Class.data = that.Class.oModel.GetTicketDetails(oResponse, $ss.agentcore.utils.GetQSObjectFromURL(pArgs.requesturl).RequestID);
            that.Class.initialized = true;
            if (that.Class.data.error) {
              that.viewticket(that.Class.data);
            } else {
              var oOpenTickets = that.Class.data;
              if (pArgs.requestQS.indexOf("RequestID") > -1) that.renderSnapinView("snapin_manage_ticket", snapinToLoaction, "\\views\\ViewTicket.html", { tickets: oOpenTickets });
              else that.renderSnapinView("snapin_manage_ticket", snapinToLoaction, "\\views\\ManageTickets.html", { tickets: oOpenTickets });
            }
          }
          catch (ex) { that.viewticket(that.Class.data); }
        })
          .fail(function (data) {
            that.Class.data = that.Class.oModel.GetTicketDetails(oResponse, Request_ID);
            that.viewticket(that.Class.data);
          })
      }
    },
    create: function (pArgs) {
      snapinToLoaction = pArgs.toLocation;
      var obj = {};
      obj.error = "Message";
      this.Class.oModel = new $ss.snapin.manageticket.model();
      obj.disp = _utils.LocalXLate("snapin_manage_ticket", "creating");

      var oResponse = "";
      if (pArgs.Title == "" || pArgs.Category == "---Select Category---" || pArgs.Type == "---Select Type---" || pArgs.Item == "---Select Item---") {
        oResponse = _utils.LocalXLate("snapin_manage_ticket", "sa_tck_mandatory");
        $('#createMsg')[0].innerHTML = "<span class='error-msg'>" + oResponse + "</span>";
      }
      else {
        var sRemedyUrl = _config.GetConfigValue("snapin_manage_ticket", "qualification_create_url", "").replace("SERVER_URL", NavigationController.GetServerUrl());
        var Title = encodeURIComponent(escape(pArgs.Title));
        var Short_Description = encodeURIComponent(escape(pArgs.Short_Description));
        var Category = encodeURIComponent(escape(pArgs.Category));
        var Type = encodeURIComponent(escape(pArgs.Type));
        var Item = encodeURIComponent(escape(pArgs.Item));
        var Activity_Log = encodeURIComponent(escape(pArgs.Activity_Log));
        Activity_Log = Activity_Log.replace(/%0A/g, "<br/>");

        var tempdata = "&channel_option=pa&Short_Description=" + Short_Description + "&Title=" + Title + "&Activity_Log=" + Activity_Log + "&Category=" + Category + "&Type=" + Type + "&Item=" + Item;
        var that = this;
        this.callAjaxCallInModel(tempdata, sRemedyUrl)
        .done(function (data) {
          if (data == null) return;
          try {
            that.GetAllTickets(true);
            var oTemp = JSON.parse(data);
            if (oTemp.ticketId != undefined) oResponse = "<span style=\"color:green\">" + _utils.LocalXLate("snapin_manage_ticket", "sa_tck_createsuccess") + ". " + _utils.LocalXLate("snapin_manage_ticket", "sa_tck_id") + ": " + oTemp.ticketId + "</span>";
            else oResponse = "<span style=\"color:red\">" + _utils.LocalXLate("snapin_manage_ticket", "sa_tck_servererror") + "</span>";
            $('#createMsg')[0].innerHTML = oResponse;
            $("#short_description,#short_title").val('');
            $('#Category, #Type, #Item').find('option:first').attr('selected', 'selected');
          }
          catch (ex) {
            oResponse = "<span style=\"color:red\">" + _utils.LocalXLate("snapin_manage_ticket", "sa_tck_notconnected") + "</span>";
            $('#createMsg')[0].innerHTML = oResponse;
          }
        })
        .fail(function (data) {
          oResponse = "<span style=\"color:red\">" + _utils.LocalXLate("snapin_manage_ticket", "sa_tck_notconnected") + "</span>";
          $('#createMsg')[0].innerHTML = oResponse;
        })

      }
    },
    update: function (pArgs) {
      snapinToLoaction = pArgs.toLocation;
      var obj = {};
      obj.error = "Message";
      this.Class.oModel = new $ss.snapin.manageticket.model();
      obj.disp = _utils.LocalXLate("snapin_manage_ticket", "updating");

      var oRequest = {};
      oRequest.ticketId = pArgs.ticketId;
      oRequest.Activity_Log = pArgs.Activity_Log;
      var oResponse = "";
      var that = this;
      if (pArgs.Activity_Log == "") {
        oResponse = _utils.LocalXLate("snapin_manage_ticket", "sa_tck_activitylogempty");
        $('#activityStatus_' + pArgs.ticketId)[0].innerHTML = "<span class='error-msg'>" + oResponse + "</span>";
      }
      else {
        var oResponse = "";
        var ActivityLog = encodeURIComponent(escape(oRequest.Activity_Log));
        ActivityLog = ActivityLog.replace(/%0A/g, "<br/>");
        var tempdata = "&ticketId=" + oRequest.ticketId + "&Activity_Log=" + ActivityLog;
        var sRemedyUrl = _config.GetConfigValue("snapin_manage_ticket", "qualification_update_url", "").replace("SERVER_URL", NavigationController.GetServerUrl());
        that.callAjaxCallInModel(tempdata, sRemedyUrl)
        .done(function (response) {
          if (response == null) return;
          var oTemp = JSON.parse(response);
          if (oTemp.status == 1) {
            oResponse = "<span style=\"color:green\">" + _utils.LocalXLate("snapin_manage_ticket", "sa_tck_updatesuccess") + "</span>";
            var respobj = {};
            var tempdata = "&Request_ID= " + oRequest.ticketId;
            var sRemedyUrl = _config.GetConfigValue("snapin_manage_ticket", "qualification_url", "").replace("SERVER_URL", NavigationController.GetServerUrl());
            that.callAjaxCallInModel(tempdata, sRemedyUrl)
            .done(function (response) {
              if (response.error) {
                return;
              }
              response.time = $.now();
              var oTemp = JSON.parse(response);
              oResponse = oTemp.responseXML;
              respobj = that.Class.oModel.GetTicketDetails(oResponse, oRequest.ticketId)
              that.renderSnapinView("snapin_manage_ticket", snapinToLoaction, "\\views\\ViewTicket.html", { tickets: respobj });
            })
            .fail(function (response) { return; })
          } else {
            oResponse = "<span style=\"color:red\">" + _utils.LocalXLate("snapin_manage_ticket", "sa_tck_servererror") + "</span>"
          }
        })
        .fail(function (response) {
          $('#activityStatus_' + pArgs.ticketId)[0].innerHTML = "<span class='error-msg'>" + _utils.LocalXLate("snapin_manage_ticket", "sa_tck_notconnected") + "</span>";
        })

      }
    },
    updatefromviewall: function (pArgs) {
      snapinToLoaction = pArgs.toLocation;
      var obj = {};
      obj.error = "Message";
      this.Class.oModel = new $ss.snapin.manageticket.model();
      obj.disp = _utils.LocalXLate("snapin_manage_ticket", "updating");

      var oRequest = {};
      oRequest.ticketId = pArgs.ticketId;
      oRequest.Activity_Log = pArgs.Activity_Log;
      var oResponse = "";
      var dataResponse = "";
      if (pArgs.Activity_Log == "") {
        oResponse = _utils.LocalXLate("snapin_manage_ticket", "sa_tck_activitylogempty");
        $('#activityStatus_' + pArgs.ticketId)[0].innerHTML = "<span class='error-msg'>" + oResponse + "</span>";
      }
      else {
        var that = this;
        var ActivityLog = encodeURIComponent(escape(oRequest.Activity_Log));
        ActivityLog = ActivityLog.replace(/%0A/g, "<br/>");
        var tempdata = "&ticketId=" + oRequest.ticketId + "&Activity_Log=" + ActivityLog;
        var sRemedyUrl = _config.GetConfigValue("snapin_manage_ticket", "qualification_update_url", "").replace("SERVER_URL", NavigationController.GetServerUrl());
        that.callAjaxCallInModel(tempdata, sRemedyUrl)
        .done(function (data) {
          if (data == null) return;
          var oTemp = JSON.parse(data);
          if (oTemp.status == 1) {
            oResponse = "<span style=\"color:green\">" + _utils.LocalXLate("snapin_manage_ticket", "sa_tck_updatesuccess") + "</span>";
            var sRemedyUrl = _config.GetConfigValue("snapin_manage_ticket", "qualification_getticketbyid_url", "").replace("SERVER_URL", NavigationController.GetServerUrl());
            var tempdata = "&Request_ID= " + oRequest.ticketId;
            that.Class.oModel.AjaxCalls(tempdata, sRemedyUrl)
            .then(function (data) {
              if (data.error) return;
              data.time = $.now();
              var oTemp = JSON.parse(data);
              dataResponse = oTemp.responseXML;
              respobj = that.Class.oModel.GetTicketDetails(dataResponse, oRequest.ticketId)
              $('#activityStatus_' + pArgs.ticketId)[0].innerHTML = ""; $("#activitylog_" + pArgs.ticketId)[0].innerHTML = ""; $("#lblactvititylog_" + pArgs.ticketId)[0].innerHTML = respobj[0].ActivityLog.replace(/\n/g, "<br/>").replace(/&/g, '&amp;');
              $("#lblmodifieddate_" + pArgs.ticketId)[0].innerHTML = respobj[0].Modified_Date;
              $('#activityStatus_' + pArgs.ticketId)[0].innerHTML = oResponse;
            })
            .fail(function (data) { return });
          } else {
            oResponse = "<span style=\"color:red\">" + _utils.LocalXLate("snapin_manage_ticket", "sa_tck_servererror") + "</span>"
          }
        })
        .fail(function (data) {
          oResponse = "<span style=\"color:red\">" + _utils.LocalXLate("snapin_manage_ticket", "sa_tck_servererror") + "</span>"
        })
      }
    },
    viewticket: function (data, snapinToLoaction) {
      if (data.disp == _utils.LocalXLate("snapin_manage_ticket", "processingWidget")) {
        this.renderSnapinView("snapin_manage_ticket", snapinToLoaction, "\\views\\ProgressWidget.html", { "tickets": data });
      } else {
        if (!data.error) {
          this.renderSnapinView("snapin_manage_ticket", snapinToLoaction, "\\views\\ViewTicket.html", { "tickets": data });
        } else {
          this.renderSnapinView("snapin_manage_ticket", snapinToLoaction, "\\views\\DispMessage.html", { "tickets": data });
        }
      }
    },
    displayerrorpage: function (bShowAlert) {
      if(bShowAlert){
        alert(_utils.LocalXLate("snapin_manage_ticket", "sa_tck_offline"));
      }
      this.Class.data = this.Class.oModel.GetTicketCount("");
      this.Class.initialized = true;
      var oOpenTickets = this.Class.data;
      this.renderSnapinView("snapin_manage_ticket", this.Class.templateLocation, "\\views\\TicketWidget.html", { tickets: oOpenTickets });
    },
    ".ticketsRefresh click": function (args) {
      args.snpmngtck_refresh = true;
      snapinToLoaction = "#ssl_content_area #2ndRow #2ndRow_sinst_0";
      this.renderSnapinView("snapin_manage_ticket", "#ssl_content_area#2ndRow#2ndRow_sinst_0", "\\views\\ProgressWidget.html");
      this.indexWidget(args);
    },
    ".createTicket click": function (args) {
      var params = {};
      params.toLocation = "#snapin_manage_ticket";
      params.Category = $("#Category option:selected").text();
      params.Type = $("#Type option:selected").text();
      params.Item = $("#Item option:selected").text();
      params.Title = $("#short_title").val();
      params.Short_Description = $("#short_description").val();
      params.Activity_Log = "";
      SnapinManageTicketController.dispatch('snapin_manage_ticket', 'create', params);
    },
    ".UpdateActivity click": function (args) {
      var params = {};
      params.toLocation = "#snapin_manage_ticket";
      params.ticketId = args.element.attributes["role"].value;
      params.Activity_Log = $("#activitylog_" + params.ticketId).text();
      if (args.element.attributes["pagename"].value == "viewticket")
        SnapinManageTicketController.dispatch('snapin_manage_ticket', 'update', params);
      else if (args.element.attributes["pagename"].value == "manageticket")
        SnapinManageTicketController.dispatch('snapin_manage_ticket', 'updatefromviewall', params);
    },
    ".displaymore click": function (e) {
      if ($(".display_" + e.element.id)[0].style.display === "none") {
        this.Class.oModel = new $ss.snapin.manageticket.model();
        var oResponse = "";
        $(".display_" + e.element.id)[0].style.display = "block";
        $(".more_" + e.element.id)[0].innerHTML = _utils.LocalXLate("snapin_manage_ticket", "snp_tickets_less_txt");
        var scrollht = 0;
        scrollht = ($("#" + e.element.id).position().top - 234 + $("#tickets .itemsscrollarea").scrollTop());
        if (scrollht < 0)
          scrollht = 0;
        $("#tickets .itemsscrollarea").scrollTo(scrollht);
      } else {
        $(".display_" + e.element.id)[0].style.display = "none";
        $(".more_" + e.element.id)[0].innerHTML = _utils.LocalXLate("snapin_manage_ticket", "snp_tickets_more_txt");
      }
    },

    SendAjaxCall: function (sFilter, obj) {
      var oResponse = "";
      var objReg = $ss.agentcore.dal.registry;
      var path = "Software\\SupportSoft\\ProviderList\\";
      var provider_id = $ss.agentcore.dal.config.GetProviderID();
      var userId = _config.GetContextValue("SdcContext:UserName")
      var regpath = objReg.GetRegValue("HKCU", path + provider_id + "\\DynamicAgent\\users\\" + userId + "\\ss_config\\global", "debug");
      var sRemedyUrl = _config.GetConfigValue("snapin_manage_ticket", "qualification_url", "").replace("SERVER_URL", NavigationController.GetServerUrl());
      /* TO DELETE AFTER REMEDY SERVER IS FIXED */

      //regpath = "true";

      /* END =--- TO DELETE AFTER REMEDY SERVER IS FIXED */

      if (regpath == "true") {
        try {
          var oDOM = $.parseJSON(_file.GetFileContents(_sOpenTicketsFilePath));
          oResponse = oDOM.responseXML;
        } catch (ex) { }
      }
      else {
        var $def = $.Deferred();
        this.Class.oModel.AjaxCalls("", sRemedyUrl)
        .then(function (response) {
          if (response.error) {
            $def.reject(response.error);
            return;
          }
          response.time = $.now();
          $def.resolve(response);
        })
        .fail(function (response) {
          $def.reject(response.error);
          return;
        })
      }
      return $def.promise();
    },

    GetAllTickets: function (reload) {
      var $def = $.Deferred();
      var fetchFromServer = reload === true || !_file.FileExists(_sOpenTicketsFilePath);
      if (fetchFromServer) {
        var sRemedyUrl = _config.GetConfigValue("snapin_manage_ticket", "qualification_url", "").replace("SERVER_URL", NavigationController.GetServerUrl());
        this.Class.oModel.AjaxCalls("", sRemedyUrl)
        .then(function (c) {
          if (c.error) {
            $def.reject(c.error);
            return;
          }
          c.time = $.now();
          $def.resolve(c);
          try {
            _file.WriteNewFile(_sOpenTicketsFilePath, c);
          } catch (e) {
            logger.error("Unable to save ticket data to machine");
          }
        })
        .fail(function (c) {
          $def.reject(c.error);
          return;
        })
      }
      else {
        try {
          var contentData = _file.GetFileContents(_sOpenTicketsFilePath);
          $def.resolve(contentData);
        } catch (e) {
          logger.error("Invalid local data");
          fetchFromServer = true;
        }
      }
      return $def.promise();
    },
    callAjaxCallInModel: function (tempdata, sRemedyUrl) {
      var $def = $.Deferred();
      this.Class.oModel.AjaxCalls(tempdata, sRemedyUrl)
      .then(function (response) {
        if (response.error) {
          $def.reject(response.error);
          return;
        }
        response.time = $.now();
        $def.resolve(response);
      })
      .fail(function (response) {
        $def.reject(response.error);
        return;
      })
      return $def.promise();
    }
  });

  function isStale() {
    try {
      if (_xmlTicket_load_freq == 0) return true;
      if (_file.FileExists(_sOpenTicketsFilePath)) {
        var contentData = $.parseJSON(_file.GetFileContents(_sOpenTicketsFilePath));
        var time_now = new Date().toUTCString();
        if (IsValidDateTime(contentData.time)) {
          var diff = (new Date(Date.parse(time_now)) - new Date(Date.parse(contentData.time))) / (1000 * 24 * 3600);
          return (diff >= _xmlTicket_load_freq);
        }
      }
    } catch (e) {
      logger.error('failed to detemine state from local file [' + _sOpenTicketsFilePath + '].Reason=' + e.message);
    }
    return true;
  }

  function IsValidDateTime(data) {
    if ((new Date(Date.parse(data)).toString().match(/(i?)Invalid Date/)) === null) return true;
    return false;
  }

  function formatDateTime(DateString) {
    var date = new Date(Date.parse(DateString));
    return date.toLocaleDateString() + " " + date.toLocaleTimeString();
  }


  /************ Private data *****************/
  var _sOpenTicketsFilePath = $ss.getSnapinAbsolutePath("snapin_manage_ticket") + '\\test_open_tickets.json';
  var _xmlTicket_load_freq = parseInt(_config.GetConfigValue("snapin_manage_ticket", "frequency_for_xmlTickets_load_days"));
  var logger = $ss.agentcore.log.GetDefaultLogger('snapin_manage_ticket');
  var _file = $ss.agentcore.dal.file;
})();