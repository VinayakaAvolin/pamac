﻿$ss.snapin = $ss.snapin || {};
$ss.snapin.manageticket = $ss.snapin.manageticket || {};
$ss.snapin.manageticket.helper = $ss.snapin.manageticket.helper || {};

(function()
  {
    $.extend($ss.snapin.manageticket.helper,
    {
      OpenChatPage : function(remedyid,category,type,item,prbtype){
        var userId = _config.GetContextValue("SdcContext:UserName")
        var sUrl=_config.GetConfigValue("snapin_manage_ticket", "paremedy_chat_url", "").replace("SERVER_URL", $ss.psCustom.helper.GetSyncURL());
        var querystring = "category=" + category + "&rtype=" + type + "&item=" + item + "&ticketid=" + remedyid + "&source=ET&associateID=" + userId + "&pdesc=" +prbtype;
        var chaturl = sUrl;
        var winobj = window.open(chaturl, remedyid, "toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=1,resizable=1,width=940,height=700,left=15,top=2");
        winobj.focus();
      }
    });
  }
)();