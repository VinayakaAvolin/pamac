//expected input parameters 
//a) contentType is must --> the type of content, must
//b) GUID - String/Array of the content GUIDs to be loaded; Optional
//c) folderId =  name of the parent Folder ID
//d) count = Number of Items to be loaded, optional, default is unlimited;
//e) isLang = boolean, optional, default false. If specified only user selected language content will be loaded
//f) template = Name of the Template to be used, optional. default singlecontentTemplate.htm is used
//g) category = Name of the categores, optional
//h) fileName = Name of the file to be picked up
//in case of any through any URL invocation the url must be of the following
// suppose the page is /sa
// url == /sa?snpextcon_uid=homeextsnp&snpextcon=true&content_type=mycontent_type&content_guid=12345-123-2321-123-21312-123
// In the page configuration of this snapin the unique name should be passed as input parameter
// <inputParam variableName="snapin_uname">homeextsnp</inputParam>

SnapinExtcontentController = SnapinBaseController.extend('snapin_extcontent', {
  index: function (params) {
    try {
      var toLocation = params.toLocation;
      var instanceParam = params.inInstanceParam;
      //as an input parameter provide unique name to this snapin
      //instance. It should be unique in a given page.             
      var instanceName = params.inInstanceParam.snapin_uname || null;

      var reQArgs = params.requestArgs || {};

      if (reQArgs && reQArgs.snpextcon_uid === instanceName) {
        //honor the request parameter
        var contentType = reQArgs.content_type || null;
        var aGUID = reQArgs.content_guid ? reQArgs.content_guid.split(";") : null;
        var aFolderId = reQArgs.content_fid ? reQArgs.content_fid.split(";") : null;
        var count = reQArgs.content_count || null;
        var isLang = reQArgs.content_islang ? true : false;
        var template = reQArgs.content_template || "singlecontentTemplate.html";
        var category = reQArgs.content_category ? reQArgs.content_category.split(";") : null;
        var fileName = reQArgs.content_filename || null;
        var resFileName = reQArgs.content_resfile || null;
        var sID = reQArgs.id || null;

      } else {
        //honor the input params..
        var contentType = instanceParam.content_type || null;
        var aGUID = instanceParam.content_guid ? instanceParam.content_guid.split(";") : null;
        var aFolderId = instanceParam.content_fid ? instanceParam.content_fid.split(";") : null;
        var count = instanceParam.content_count || null;
        var isLang = instanceParam.content_islang ? true : false;
        var template = instanceParam.content_template || "singlecontentTemplate.html";
        var category = instanceParam.content_category ? instanceParam.content_category.split(";") : null;
        var fileName = instanceParam.content_filename || null;
        var resFileName = instanceParam.content_resfile || null;
        var sID = instanceParam.id || null;

      }

      //if content type or fileName is not specified just return false
      if (!contentType)
        return false;
      if (!fileName)
        return false;
      var queryFilter = null;
      if (aGUID) {
        queryFilter = queryFilter || {};
        queryFilter.cid = aGUID;
      };
      if (aFolderId) {
        queryFilter = queryFilter || {};
        queryFilter.fid = aFolderId;
      };
      if (isLang) {
        queryFilter = queryFilter || {};
        queryFilter.language = $ss.agentcore.dal.config.GetConfigLanguage();
      };

      if (category) {
        queryFilter = queryFilter || {};
        queryFilter.category = {
          has: category
        };
      };
      if (!queryFilter)
        return false;
      var _content = $ss.agentcore.dal.content;
      var qObj = _content.GetContentsAdvQuery([contentType], queryFilter);
      if (sID !== null) {
        var tempqObj;
        for (var i = 0; i < qObj.length; i++) {
          if (_content.GetContentFieldValue(qObj[i].ctype, qObj[i].cid, qObj[i].version, "ID", "sccf_field_value_char").toLowerCase() === sID.toLowerCase()) {
            tempqObj = new Array(qObj[i]);
            break;
          }
        }
        qObj = tempqObj;
      }
      var locals = {
        content: qObj,
        fileName: fileName,
        resFileName: resFileName
      }
      this.renderSnapinView("snapin_extcontent", params.toLocation, "\\views\\" + template, locals);
    }
    catch (ex) {

    }
  }
});
