/**
 * @author Nihar Naigaonkar
 */
/***************************************************************************************
**
**  File Name: ras_wrapper.js
**
**  Summary: SMAPI derived class for RAS modem support
**
**  Description: This javascript contains SMAPI derivative to support RAS modem
**               implementations. It maps SMAPI functions to RAS specific calls.
**
**  Copyright SupportSoft Inc. 2007, All rights reserved.
***************************************************************************************/

$ss.snapin = $ss.snapin ||
{};
$ss.snapin.modem = $ss.snapin.modem ||
{};
$ss.snapin.modem.ras = $ss.snapin.modem.ras ||
{};
$ss.snapin.modem.ras.wrapper = $ss.snapin.modem.ras.wrapper ||
{};
$ss.snapin.modem.ras.wrapper = Class.extend('', {
},{// prototype methods

/***************************************************************************************
** Name:         init
**
** Purpose:      Class that wraps SMAPI functionality and provides RAS modem
**               implementation.
**
** Parameter:    none
**
** Return:       true if we have sync
***************************************************************************************/
init : function (oSMAPI)
{
  this.oSMAPI         = oSMAPI;
  this.mProperties    = oSMAPI.GetInstanceProperties();
  this.m_sUserName    = "";
  this.m_sUserPass    = "";

  try
  {
    this.oRas         = _utils.CreateActiveXObject("SPRT.SdcNetCheck", true);
  }
  catch(e){
    this.oRas = null;
    _logger.error("_ras", e.message);
  };
},

/***************************************************************************************
** Name:         _ras_ID
**
** Purpose:      Determine the unique identifier for the modem. This implementation is
**               helpful at protocol level so that SMAPI can use it to infer modem
**               specific implementations.
**
** Parameter:    none
**
** Return:       unique identifier of the modem matching modem xml file
***************************************************************************************/
ID : function ()
{
  var id = "";
  try {
    id = this.mProperties.id;
  } catch (ex) {
    _logger.error("_null_modem_ID", ex.message );
    this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
  }
  return id;
},

/*******************************************************************************
** Name:         _ras_Name
**
** Purpose:      Gets the modem name as defined in the modem.xml
**
** Parameter:    none
**
** Return:       str
*******************************************************************************/
Name : function ()
{
  try {
    return this.mProperties.name;
  } catch(ex) {
    _logger.error("_ras_Name", ex.message );
    this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
  }
},

/*******************************************************************************
** Name:         _ras_SerialNumber
**
** Purpose:      Get Serial Number for RAS compliant modems.
**
** Parameter:    none
**
** Return:       string having Serial Number else empty
*******************************************************************************/
SerialNumber : function ()
{
  // this not supported via RAS
  return _constants.SMAPI_EMPTY_STR;
},

/*******************************************************************************
** Name:         _ras_MACAddress
**
** Purpose:      Get MACAddress for RAS compliant modems.
**
** Parameter:    none
**
** Return:       string having MACAddress else empty
*******************************************************************************/
MACAddress : function ()
{
  // this not supported via RAS
  return _constants.SMAPI_EMPTY_STR;
},

/*******************************************************************************
** Name:         _ras_Firmware
**
** Purpose:      Get firmware version
**
** Parameter:    none
**
** Return:       string having firmware else empty
*******************************************************************************/
Firmware : function ()
{
  // this not supported via RAS
  return _constants.SMAPI_EMPTY_STR;
},

/***************************************************************************************
** Name:         _ras_Detect
**
** Purpose:      Verifies that a RAS compliant modem is connected to the PC. Detection
**               could fail if PC to modem connection is broken. Detects modem by looking
**               for a name match in the list of present net class devices.
**
** Parameter:    none
**
** Return:       true if detected.
***************************************************************************************/
Detect : function ()
{
  var nRet = 0;

  try
  {
    var modem = this.oSMAPI;
    var arValues = modem.GetInstanceArray("modem_device", _constants.SMAPI_EMPTY_STR);
    var sFoundDevice = _netCheck.FindIfDevicePresent(this.oRas, arValues);
    if (sFoundDevice)
    {
      _logger.debug("_ras_Detect", "Found modem: " + sFoundDevice);
      nRet = 1;
    }
    else
    {
      _logger.debug("_ras_Detect", "No modem found");
      nRet = 0;
    }
  }
  catch (ex)
  {
    _logger.error("_ras_Detect", ex.message );
    this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
  }

  return nRet;
},

/***************************************************************************************
** Name:         _ras_SetUserCreds
**
** Purpose:      Sets PPPoA creds on the modem.
**
** Parameter:    sName     :  User name
**               sPassword :  User passowrd
**
** Return:       check GetLastError to confirm success and then check response objeect
**               if GetLastError return failure
***************************************************************************************/
SetUserCreds : function (sName, sPassword)
{
  _logger.debug("_ras_SetUserCreds", "Entered function.");

  var oRet = new Object();
  oRet.errorCode = _constants.SMAPI_VAL_UNKNOWN;

  try
  {
    var connectionname = this._GetConnectionName();
    var obj = _rasimp.ConfigureUser(this.oRas, sName, sPassword, connectionname, this.oSMAPI);
    if (obj)
    {
      this.m_sUserName = sName;
      this.m_sUserPass = sPassword;

      // ras entry successfully created, so dial out now
      var timeout = this.mProperties.dialer_connection_wait;
      var oRet = _netCheck.RasDialEntry(this.oRas, connectionname, timeout);
      if (!oRet.bSuccess)
      {
        this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_FAIL);
        oRet.errorCode = oRet.nLastError;
      }
    }
  }
  catch (ex)
  {
    _logger.error("_ras_SetUserCreds", ex.message );
    this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
  }
},

/***************************************************************************************
** Name:         _ras_IsSync
**
** Purpose:      Determine if modem is in sync. This is a required procedure to
**               check modem to DSLAM connection.
**
** Parameter:    none
**
** Return:       true if we have sync
***************************************************************************************/
IsSync : function ()
{
  var nRet = 0;

  // RAS modem has sync if it does not return a ras error code 680 (no dial tone)
  // 3/3/07: on Vista, ST330 returns error code 692 (hardware failure) if phone line is unplugged
  try
  {
    var oRAS = _rasimp.GetRASObject(this.oRas);

    // if we have ANY active dialup connection, then assume modem has sync
    nRet = this.IsConnected();

    // not connected, so need to create a "test" connection to dial out and test
    if (!nRet)
    {
      var wg_username = _config.GetConfigValue("global","wg_username");
      var wg_password = _config.GetConfigValue("global","wg_password");
      var wg_connectionname = "Temp Connection";    // hard-code test connection

      // configure connection with WG creds, but do NOT call SetUserCreds directly since we are
      // using a test connection
      var obj = _rasimp.ConfigureUser(this.oRas, wg_username, wg_password, wg_connectionname, this.oSMAPI);
      if (obj)
      {
        // ras entry successfully created, so dial out now
        var timeout = this.mProperties.dialer_connection_wait;
        var oRet = _netCheck.RasDialEntry(this.oRas, wg_connectionname, timeout);
        nRet = (oRet.nLastError != 680 && oRet.nLastError != 692) ? 1 : 0;
      }

      // disconnect and delete the test connection regardless of success or failure
      _netCheck.RasDisconnect(oRAS, wg_connectionname);
      _netCheck.DelRasEntry(oRAS, "", wg_connectionname);
    }
  }
  catch (ex)
  {
    _logger.error("_ras_IsSync", ex.message );
    this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
  }

  return nRet;
},

/***************************************************************************************
** Name:         _ras_IsConnected
**
** Purpose:      Determine if modem ppp status is "connected".
**
** Parameter:    (none)
**
** Return:       true/false
***************************************************************************************/
IsConnected : function ()
{
  var nRet = 0;

  // returns true if we have an active connection to the modem
  try
  {
    var oRAS = _rasimp.GetRASObject(this.oRas);
    var arrActiveConn = _netCheck.GetActiveRasConnections(oRAS, _rasimp.GetModemDevice(oRAS, this.oSMAPI));
    nRet = (arrActiveConn.length > 0) ? 1 : 0;
    _logger.debug("_ras_IsConnected", "Result :" + nRet);
  }
  catch(e)
  {
    _logger.error("_ras_IsConnected", e.message );
    this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
    return 0;
  }

  return nRet;
},

/***************************************************************************************
** Name:         _ras_Reboot
**
** Purpose:      Reboots modem
**
** Parameter:    none
**
** Return:       GetLastError to confirm success
***************************************************************************************/
Reboot : function ()
{
 // not supported with RAS
},

/***************************************************************************************
** Name:         _ras_GetDSLLinkInfo
**
** Purpose:      Provides bunch of DSL link parameters in one shot. Modem implementations
**               can choose to fill all required output parameters or default to -1 if
**               a particular param is not implemented.
**
** Parameter:    none
**
** Return:       Object having the following properties
**               NewUpstreamCurrentRate
**               NewDownstreamCurrentRate
**               NewUpstreamNoiseMargin
**               NewDownstreamNoiseMargin
**               NewUpstreamPower
**               NewDownstreamPower
**               NewUpstreamAttenuation
**               NewDownstreamAttenuation
***************************************************************************************/
GetDSLLinkInfo : function ()
{
  try {
      var newObj = new Object();
      newObj.NewUpstreamCurrentRate   = -1;
      newObj.NewDownstreamCurrentRate = -1;
      newObj.NewUpstreamNoiseMargin   = -1;
      newObj.NewDownstreamNoiseMargin = -1;
      newObj.NewUpstreamPower         = -1;
      newObj.NewDownstreamPower       = -1;
      newObj.NewUpstreamAttenuation   = -1;
      newObj.NewDownstreamAttenuation = -1;
      return newObj;
  } catch(ex) {
    _logger.error("_ras_GetDSLLinkInfo", ex.message );
    this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
  }
  return null;
},

_GetConnectionName : function ()
{
  var cur_connName = "";
  try
  {
    var modem = this.oSMAPI;
    var expr = "rasentry_name/item[@default=\"true\"]";
    var defConn = modem.GetInstanceValue(expr, _constants.SMAPI_EMPTY_STR);

    cur_connName = _smapiutils.g_oSMAPIDependencies.GetValue("ss_gc_db_SelectedRASConnection");

    // use default connection name if there's none selected by user
    if (cur_connName == null || cur_connName == "" || typeof(cur_connName) == "undefined")
    {
      cur_connName = defConn;
    }

    _logger.debug("GetConnectionName", "Connection name to use is:" + cur_connName);

  } catch (ex) {
    _logger.error("_ras_pvt_GetConnectionName", ex.message );
    this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
  }
  return cur_connName;
}
});

  var _logger = $ss.agentcore.log.GetDefaultLogger("_ras");
  var _utils = $ss.agentcore.utils;
  var _netCheck = $ss.agentcore.network.netcheck;
  var _config = $ss.agentcore.dal.config;
  var _constants = $ss.agentcore.constants;
  var _smapiutils = $ss.agentcore.smapi.utils;
  var _rasimp = $ss.snapin.modem.ras.imp;