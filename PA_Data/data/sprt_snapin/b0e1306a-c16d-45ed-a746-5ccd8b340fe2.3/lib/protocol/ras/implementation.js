/***************************************************************************************
**
**  File Name: ras_imp.js
**
**  Summary: Common APIs for RAS dialup modem support
**
**  Description: This javascript contains general purpose functions for
**               RAS modem implementations
**           
**  Copyright SupportSoft Inc. 2007, All rights reserved.
***************************************************************************************/
$ss.snapin = $ss.snapin ||
{};
$ss.snapin.modem = $ss.snapin.modem ||
{};
$ss.snapin.modem.ras = $ss.snapin.modem.ras ||
{};

/** @namespace Holds all getconnect utilities related functionality*/
$ss.snapin.modem.ras.imp = $ss.snapin.modem.ras.imp || {};

(function()
{
 $.extend($ss.snapin.modem.ras.imp,
  {   
	/***************************************************************************************
	**                      RAS Helper Functions                                          **
	***************************************************************************************/
	GetRASObject : function (rasObj)
	{
	  var retObj = rasObj;  
	  if(rasObj == null)
	  {  
	    retObj = _utils.CreateActiveXObject('SPRT.SdcNetCheck', true);
	  }
	  return retObj;
	},
	 
	ConnectionCheck_WG : function (rasObj)
	{
	  var nRet = 0;
	  try
	  {
	    var wgServer = _config.ParseMacros("%SERVER%");
	    var sServer = wgServer.replace(/(http:\/\/)|(https:\/\/)/ig, "");
	    var ar = sServer.split(":");
	    var host = ar[0];
	    var port = (ar.length > 1) ? parseInt(ar[1]) : 80;
	    var timeout = _config.GetConfigValue("global", "wg_detect_timeout", 15000);
	
	    _logger.debug("ConnectionCheck_WG", "host: " + host + " port: " + port + " timeout: " + timeout);
	    nRet = _netCheck.TestConnection(rasObj, host, port, timeout) ? 1 : 0;
	    _logger.debug("ConnectionCheck_WG", "TestConnect returns: " + nRet);
	  }
	  catch (ex)
	  {
	   _logger.error("ConnectionCheck_WG", ex.message );
	  }  
	  return (nRet);
	},
	
	ConnectionCheck_Internet : function ()
	{  
	  var nRet = 0;
	  try 
	  {
	    var sites = _config.GetValues("modems", "internet_pingsite");
	    for (var i=0 ; i < sites.length; i++) {
	      _smapiutils.g_oSMAPIDependencies.Sleep(1500); 
	      if (_netCheck.Ping(sites[i])) return 1;
	      _smapiutils.g_oSMAPIDependencies.Sleep(1500); 
	    }
	  }
	  catch (ex) 
	  {
	    _logger.error("ConnectionCheck_Internet", ex.message );
	  }
	  return nRet;
	},
	
	GetModemDevice : function (rasObj, oSMAPI)
	{
	  var strOS = _utils.GetOS();
	  if(strOS == "WIN98" || strOS == "WINME") // return blank
	    return "";
	
	  // enumerate modem_device list specified in modem xml and return the one that's
	  // currently present. If none of them is present, return empty string
	  var arValues = oSMAPI.GetInstanceArray("modem_device", _constants.SMAPI_EMPTY_STR);
	  var sFoundDevice = _netCheck.FindIfDevicePresent(rasObj, arValues);
	  
	  _logger.debug("GetModemDevice", sFoundDevice);  
	  return sFoundDevice;
	},
	
	ConfigureUser: function (rasObj, username, password, connectionname, oSMAPI)
	{
	  try 
	  {
	    // 1. Disconnect the active connection
	    _logger.debug("ConfigureUser", "Disconnecting : " + connectionname);           
	    _netCheck.RasDisconnect(rasObj, connectionname);
	    
	    // 2. Delete the existing connection
	    _logger.debug("ConfigureUser", "Deleting : " + connectionname);
	    _netCheck.DelRasEntry(rasObj, "", connectionname);    
	    
	    // 3. Create new connection
	    _logger.debug("ConfigureUser", "Creating connection : [" + connectionname + "] username : [" + username + "] password : [" + password + "]");        
	    var result = _netCheck.CreatePPPoARasEntry(rasObj, connectionname, "0", "0", username, password, "nodialrules", this.GetModemDevice(rasObj, oSMAPI));
	    
	    _logger.debug("ConfigureUser", "Connection create: " + result); 
	    return result;
	  }
	  catch(e)
	  {
	    _logger.debug("ConfigureUser", "Connection create failed - exception:" + e.message);
	    return false;
	  }
	}
  });
  var _logger = $ss.agentcore.log.GetDefaultLogger("$ss.snapin.modem.ras.imp");
  var _utils = $ss.agentcore.utils;
  var _netCheck = $ss.agentcore.network.netcheck;
	var _config = $ss.agentcore.dal.config;
	var _constants = $ss.agentcore.constants;
	var _smapiutils = $ss.agentcore.smapi.utils;
})();