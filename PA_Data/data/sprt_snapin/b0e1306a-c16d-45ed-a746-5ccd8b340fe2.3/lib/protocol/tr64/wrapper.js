/********************************************************************************
**
**  File Name: tr64.wrapper.js
**
**  Summary: SMAPI derived class for TR64 protocol support
**
**  Description: This javascript contains SMAPI derivative to support TR64 protocol
**               implementation. It maps SMAPI functions to TR64 specific calls.
**           
**  Copyright SupportSoft Inc. 2005, All rights reserved.
********************************************************************************/

$ss.snapin = $ss.snapin ||
{};
$ss.snapin.modem = $ss.snapin.modem ||
{};
$ss.snapin.modem.tr64 = $ss.snapin.modem.tr64 ||
{};
$ss.snapin.modem.tr64.wrapper = $ss.snapin.modem.tr64.wrapper ||
{};
$ss.snapin.modem.tr64.wrapper = Class.extend('', {
},{// prototype methods

	/********************************************************************************
	** Name:         _tr64
	**
	** Purpose:      Class that wraps SMAPI functionality and provides TR64 protocol 
	**               implementation.
	**
	** Parameter:    none
	**
	** Return:       tr64 object instance
	********************************************************************************/
	init : function(oSMAPI)
	{
	  this.oSMAPI      = oSMAPI;
	  this.mProperties = oSMAPI.GetInstanceProperties();  
	  this.mObjTR64    = null;
	  this.mObjSOAP    = null;
	  this.mResponses  = new Object(); 
	  this.m_bInit     = false;   
	},
	
	/********************************************************************************
	** Name:         _tr64_ID
	**
	** Purpose:      Determine the unique identifier for the modem. This implementation is 
	**               helpful at protocol level so that SMAPI can use it to infer modem 
	**               specific implementations.
	**
	** Parameter:    none
	**
	** Return:       unique identifier of the modem matching modem xml file
	********************************************************************************/
	ID : function ()
	{
	  try {
	    var oRet = this.GetDeviceInfo();
	    return (this.oSMAPI.GetLastError() == _constants.SMAPI_SUCCESS) ? oRet.NewModelName : _constants.SMAPI_VAL_UNKNOWN;
	  } catch(ex) {
	    _logger.error("_tr64_ID", ex.message );
	    this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
	  }
	  return _constants.SMAPI_VAL_UNKNOWN;
	},
	
	/********************************************************************************
	** Name:         _tr64_Name
	**
	** Purpose:      Gets the modem name as defined in the modem.xml
	**
	** Parameter:    none
	**
	** Return:       str
	********************************************************************************/
	Name : function () 
	{
	  try {
	    return this.mProperties.name;
	  } catch(ex) {
	    _logger.error("_tr64_Name", ex.message );
	    this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
	  }
	  return _constants.SMAPI_VAL_UNKNOWN;
	},
	
	/*******************************************************************************
	** Name:         _tr64_SerialNumber
	**
	** Purpose:      Gets Serial Number of derived modem. This info is generally 
	**               present at back of the modem.
	**
	** Parameter:    none
	**
	** Return:       GetLastError to confirm success and then check response object
	**               [NewSerialNumber] property.
	*******************************************************************************/
	SerialNumber : function () 
	{
	  try {
	    var oRsp = this.GetDeviceInfo();
	    return (this.oSMAPI.GetLastError() == _constants.SMAPI_SUCCESS) ? oRsp.NewSerialNumber : _constants.SMAPI_VAL_UNKNOWN;
	  } catch(ex) {
	    _logger.error("_tr64_SerialNumber", ex.message );
	    this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
	  }
	  return _constants.SMAPI_VAL_UNKNOWN;
	},
	
	/*******************************************************************************
	** Name:         _tr64_MACAddress
	**
	** Purpose:      Gets MACAddress of derived modem. 
	**
	** Parameter:    none
	**
	** Return:       GetLastError to confirm success and then check response object
	**               [NewMACAddress] property.
	*******************************************************************************/
	MACAddress : function () 
	{
	  try {
	    var oRsp = this.ExecuteCommand( SVC_LAN_ETHERNET_IFCONFIG, ACT_LEI_GET_INFO );
	    return (this.oSMAPI.GetLastError() == _constants.SMAPI_SUCCESS) ? oRsp.NewMACAddress : _constants.SMAPI_VAL_UNKNOWN;
	  } catch(ex) {
	    _logger.error("_tr64_MACAddress", ex.message );
	    this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
	  }
	  return _constants.SMAPI_VAL_UNKNOWN;
	},
	
	/*******************************************************************************
	** Name:         _tr64_Firmware
	**
	** Purpose:      Gets firmware version of derived modem. 
	**
	** Parameter:    none
	**
	** Return:       GetLastError to confirm success and then check response object
	**               [NewFirmwareVersion] property.
	*******************************************************************************/
	Firmware : function () 
	{
	  try {
	    var oRet = this.GetDeviceInfo();
	    return (this.oSMAPI.GetLastError() == _constants.SMAPI_SUCCESS) ? oRet.NewFirmwareVersion : _constants.SMAPI_VAL_UNKNOWN;
	  } catch(ex) {
	    _logger.error("_tr64_Firmware", ex.message );
	    this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
	  }
	  return _constants.SMAPI_VAL_UNKNOWN;
	},
	
	/********************************************************************************
	** Name:         _tr64_Detect
	**
	** Purpose:      Verifies that a TR64 compliant modem is connected to the PC. Detection 
	**               could fail if PC to modem connection is broken or modem connected is not
	**               TR64 compliant
	**
	** Parameter:    none
	**
	** Return:       0/1 Status if detected.
	********************************************************************************/
	Detect : function ()
	{ 
	  // Don't want to trap SMAPI errors here as we force re-initialize
	  return (this.pvt_Init(true)) ? 1: 0;
	},
	
	/********************************************************************************
	** Name:         _tr64_GetDSLLinkConfig
	**
	** Purpose:      Gets all information related to DSL Link GetInfo as per TR64
	**               spec
	**
	** Parameter:    none
	**
	** Return:       Returns object having the following
	**               NewEnable
	**               NewLinkType
	**               NewLinkStatus
	**               NewAutoConfig
	**               NewModulationType
	**               NewDestinationAddress etc...
	********************************************************************************/
	GetDSLLinkConfig : function () 
	{
	  var oRsp = this.ExecuteCommand(SVC_WAN_DSLLINK_CONFIG, ACT_WDC_GET_INFO);
	  return (this.oSMAPI.GetLastError() == _constants.SMAPI_SUCCESS) ? oRsp : _constants.SMAPI_OBJ_UNKNOWN;
	},
	
	/********************************************************************************
	** Name:         _tr64_SetDSLLinkType
	**
	** Purpose:      Sets DSL Link Type to the one passed to function
	**
	** Parameter:    sLinkType
	**
	** Return:       GetLastError to confirm success 
	********************************************************************************/
	SetDSLLinkType : function (sLinkType) 
	{
	  this.ExecuteCommand(SVC_WAN_DSLLINK_CONFIG, ACT_WDC_SET_DSLLINK_TYPE, {"NewLinkType": sLinkType});
	},
	
	/********************************************************************************
	** Name:         _tr64_SetDestinationAddress
	**
	** Purpose:      Sets Destination Address to the one passed to function
	**
	** Parameter:    sAddr
	**
	** Return:       GetLastError to confirm success 
	********************************************************************************/
	SetDestinationAddress : function (sAddr) 
	{
	  this.ExecuteCommand(SVC_WAN_DSLLINK_CONFIG, ACT_WDC_SET_DEST_ADDR, {"NewDestinationAddress": sAddr});
	},
	
	/********************************************************************************
	** Name:         _tr64_SetDSLLinkEnable
	**
	** Purpose:      Sets DSL Link Enable to the one passed to function
	**
	** Parameter:    nEnable 1: Enable/ 0: Disable
	**
	** Return:       GetLastError to confirm success 
	********************************************************************************/
	SetDSLLinkEnable : function (nEnable) 
	{
	  this.ExecuteCommand(SVC_WAN_DSLLINK_CONFIG, ACT_WDC_SET_ENABLE, {"NewEnable": nEnable});
	},
	
	/********************************************************************************
	** Name:         _tr64_IsSync
	**
	** Purpose:      Determine if modem has acquired DSL sync. This is a required 
	**               procedure to check modem to DSLAM connection.
	**
	** Parameter:    none
	**
	** Return:       true if modem has DSL Sync
	********************************************************************************/
	IsSync : function () 
	{
	  var nRet = _constants.SMAPI_VAL_UNKNOWN;
	  try {
	    var oRsp = this.GetDSLLinkConfig();
	    if(this.oSMAPI.GetLastError() == _constants.SMAPI_SUCCESS) {
	      if(oRsp.NewLinkStatus == "Up" &&
	         oRsp.NewLinkType   == this.mProperties.dsl_link_type) {
	        nRet = 1;
	      } else {
	        nRet = 0;
	      }  
	    }      
	  } catch (ex) {
	    _logger.error("_tr64_IsSync", ex.message ); 
	    this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
	  }
	  return nRet;
	},
	
	/********************************************************************************
	** Name:         _tr64_IsConnected
	**
	** Purpose:      Checks to see if modem has WAN IP connectivity.
	**
	** Parameter:    (none)
	**
	** Return:       0/1 based on WAN connectivity status
	********************************************************************************/
	IsConnected : function () 
	{
	  var nRet = _constants.SMAPI_VAL_UNKNOWN;
	  try {
	    var oRsp = this.ExecuteCommand(SVC_WAN_PPP_CONNECTION, ACT_WPC_GET_STATUS_INFO);
	    if(this.oSMAPI.GetLastError() == _constants.SMAPI_SUCCESS) {
	      nRet = (oRsp.NewConnectionStatus == "Connected")? 1 : 0;
	    }
	  } catch (ex) {
	    _logger.error("_tr64_IsConnected", ex.message ); 
	    this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
	  }
	  return nRet;
	},
	
	/********************************************************************************
	** Name:         _tr64_GetPPPConnectionTypeInfo
	**
	** Purpose:      Checks for WAN PPP Connection type info
	**
	** Parameter:    (none)
	**
	** Return:       string
	********************************************************************************/
	GetPPPConnectionTypeInfo : function () 
	{
	  var sRet = _constants.SMAPI_VAL_UNKNOWN;
	  try {
	    var oRsp = this.ExecuteCommand(SVC_WAN_PPP_CONNECTION, ACT_WPC_GET_CONNTYPE_INFO);
	    if(this.oSMAPI.GetLastError() == _constants.SMAPI_SUCCESS) {
	      sRet = oRsp.NewConnectionType;
	    }
	  } catch (ex) {
	    _logger.error("_tr64_GetPPPConnectionTypeInfo", ex.message ); 
	    this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
	  }  
	  return sRet;
	},
	
	/********************************************************************************
	** Name:         _tr64_SetPPPConnectionType
	**
	** Purpose:      Sets WANPPPConnection service's connection type to the one 
	**               passed to function
	**
	** Parameter:    sConnType : e.g. IP_Routed
	**
	** Return:       GetLastError to confirm success 
	********************************************************************************/
	SetPPPConnectionType : function (sConnType) 
	{
	  this.ExecuteCommand(SVC_WAN_PPP_CONNECTION, ACT_WPC_SET_CONNTYPE, {"NewConnectionType": sConnType});
	},
	
	/********************************************************************************
	** Name:         _tr64_SetPPPUserName
	**
	** Purpose:      Sets WANPPPConnection service's user name to the one 
	**               passed to function
	**
	** Parameter:    sUserName : user name
	**
	** Return:       GetLastError to confirm success 
	********************************************************************************/
	SetPPPUserName : function (sUserName) 
	{
	  this.ExecuteCommand(SVC_WAN_PPP_CONNECTION, ACT_WPC_SET_USERNAME, {"NewUserName": sUserName});
	},
	
	/********************************************************************************
	** Name:         _tr64_SetPPPPassword
	**
	** Purpose:      Sets WANPPPConnection service's user password to the one 
	**               passed to function
	**
	** Parameter:    sPassword : user password 
	**
	** Return:       GetLastError to confirm success 
	********************************************************************************/
	SetPPPPassword : function (sPassword) 
	{
	  this.ExecuteCommand(SVC_WAN_PPP_CONNECTION, ACT_WPC_SET_PASSWORD, {"NewPassword": sPassword});
	},
	
	/********************************************************************************
	** Name:         _tr64_TerminatePPPConnection
	**
	** Purpose:      Terminate WANPPPConnection
	**
	** Parameter:    bForce : whether to force or request termination
	**
	** Return:       GetLastError to confirm success 
	********************************************************************************/
	TerminatePPPConnection : function (bForce) 
	{
	  if(typeof bForce != "undefined" && bForce) 
	    this.ExecuteCommand(SVC_WAN_PPP_CONNECTION, ACT_WPC_FRCE_TERMINATION);
	  else
	    this.ExecuteCommand(SVC_WAN_PPP_CONNECTION, ACT_WPC_RQST_TERMINATION);
	  
	  if(this.oSMAPI.GetLastError() == _constants.SMAPI_SUCCESS)
	    _smapiutils.g_oSMAPIDependencies.Sleep(6000);  
	},
	
	/********************************************************************************
	** Name:         _tr64_RequestPPPConnection
	**
	** Purpose:      Requests WANPPPConnection
	**
	** Parameter:    none
	**
	** Return:       GetLastError to confirm success 
	********************************************************************************/
	RequestPPPConnection : function () 
	{
	  this.ExecuteCommand(SVC_WAN_PPP_CONNECTION, ACT_WPC_RQST_CONNECTION);
	},
	
	/********************************************************************************
	** Name:         _tr64_SetUserCreds
	**
	** Purpose:      Sets the username and password on the modem and to the registry.
	**
	** Parameter:    sUserName : user name
	**               sPassword : user password
	**
	** Return:       GetLastError to confirm success 
	********************************************************************************/
	SetUserCreds : function (sUserName, sPassword ) 
	{
	  // Get WANPPConnection Type and if not IP_Routed then set it to IP_Routed
	  var sConnType = this.GetPPPConnectionTypeInfo();
	  if(this.oSMAPI.GetLastError() == _constants.SMAPI_SUCCESS && sConnType != "IP_Routed") {
	    this.SetPPPConnectionType("IP_Routed");
	  }  
	  if(this.oSMAPI.GetLastError() != _constants.SMAPI_SUCCESS) return;
	  
	  // Get WANPPPConnection Status and if connected then terminate connection as 
	  // we want to set new creds
	  if(this.IsConnected()) {
	    this.TerminatePPPConnection(true);
	  }  
	  if(this.oSMAPI.GetLastError() != _constants.SMAPI_SUCCESS) return;
	  
	  // Now Set WANPPP UserName and proceed only if things are OK till now
	  this.SetPPPUserName(sUserName);
	  if(this.oSMAPI.GetLastError() != _constants.SMAPI_SUCCESS) return;
	  
	  // Set WANPPP Password and proceed only if things are OK till now
	  this.SetPPPPassword(sPassword);
	  if(this.oSMAPI.GetLastError() != _constants.SMAPI_SUCCESS) return;
	  
	  // TTP 10981 fix as sometimes modem is not ready to accept a RequestPPPConnection 
	  // since already busy configuring the previous calls
	  _smapiutils.g_oSMAPIDependencies.Sleep(20000);
	  
	  // now Request PPP connection
	  this.RequestPPPConnection();
	  if(this.oSMAPI.GetLastError() != _constants.SMAPI_SUCCESS) return;
	},
	
	/********************************************************************************
	** Name:         _tr64_Reboot
	**
	** Purpose:      Reboots modem
	**
	** Parameter:    none
	**
	** Return:       GetLastError to confirm success 
	********************************************************************************/
	Reboot : function () 
	{
	  this.ExecuteCommand(SVC_DEVICE_CONFIG, ACT_DCF_REBOOT);
	},
	
	/*******************************************************************************
	** Name:         _tr64_GetDeviceInfo
	**
	** Purpose:      Provides bunch of CPE generic parameters in one shot. Many 
	**               implementaions have noticed such information available via 
	**               1 or 2 actual modem calls. Thus clubbing it together helps 
	**               reducing actual calls being made to modem. 
	**               Modem implementations can choose to fill all required output 
	**               parameters or default to -1 if a particular param is not 
	**               implemented.
	**
	** Parameter:    none
	**
	** Return:       GetLastError to confirm success and then check response object
	**               for the following properties
	**               NewManufacturerName
	**               NewModelName
	**               NewSerialNumber
	**               NewFirmwareVersion
	**               NewHardwareVersion
	*******************************************************************************/
	GetDeviceInfo : function () 
	{
	  try {
	    var oRsp = this.ExecuteCommand(SVC_DEVICE_INFO, ACT_DIN_GET_INFO);
	    if(this.oSMAPI.GetLastError() == _constants.SMAPI_SUCCESS) {
	      var oRet = {};
	      oRet.NewManufacturerName = oRsp.NewManufacturerName;
	      oRet.NewModelName        = oRsp.NewModelName;
	      oRet.NewSerialNumber     = oRsp.NewSerialNumber;
	      // Note the difference as TR64 says this is the current firmware version
	      oRet.NewFirmwareVersion  = oRsp.NewSoftwareVersion; 
	      oRet.NewHardwareVersion  = oRsp.NewHardwareVersion;
	      return oRet;
	    } 
	  } catch (ex) {
	    _logger.error("_tr64_GetDeviceInfo", ex.message ); 
	    this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
	  }
	  return _constants.SMAPI_OBJ_UNKNOWN;
	},
	
	/********************************************************************************
	** Name:         _tr64_GetDSLLinkInfo
	**
	** Purpose:      Provides bunch of DSL link parameters in one shot as 
	**               per the TR64 Spec. 
	**
	** Parameter:    none
	**
	** Return:       Object having the following properties
	**               NewUpstreamCurrentRate
	**               NewDownstreamCurrentRate
	**               NewUpstreamNoiseMargin
	**               NewDownstreamNoiseMargin
	**               NewUpstreamPower
	**               NewDownstreamPower
	**               NewUpstreamAttenuation
	**               NewDownstreamAttenuation 
	********************************************************************************/
	GetDSLLinkInfo : function () 
	{
	  try {
	    var oRsp = this.ExecuteCommand(SVC_WAN_DSL_IFCONFIG, ACT_WDI_GET_INFO);
	    if(this.oSMAPI.GetLastError() == _constants.SMAPI_SUCCESS) {
	      var oRet = new Object();
	      oRet.NewUpstreamCurrentRate   = oRsp.NewUpstreamCurrRate;
	      oRet.NewDownstreamCurrentRate = oRsp.NewDownstreamCurrRate;
	      oRet.NewUpstreamNoiseMargin   = oRsp.NewUpstreamNoiseMargin;
	      oRet.NewDownstreamNoiseMargin = oRsp.NewDownstreamNoiseMargin;
	      oRet.NewUpstreamPower         = oRsp.NewUpstreamPower;
	      oRet.NewDownstreamPower       = oRsp.NewDownstreamPower;
	      oRet.NewUpstreamAttenuation   = oRsp.NewUpstreamAttenuation;
	      oRet.NewDownstreamAttenuation = oRsp.NewDownstreamAttenuation;
	      return oRet;
	    }  
	  } catch (ex) {
	    _logger.error("_tr64_GetDSLLinkInfo", ex.message ); 
	    this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
	  }
	  return _constants.SMAPI_OBJ_UNKNOWN;
	},
	
	/*******************************************************************************
	** Name:         _tr64_GetWirelessInfo
	**
	** Purpose:      Retrieves all the wireless state variables other than the  
	**               statistics and security keys. 
	**
	** Parameter:    none
	**
	** Return:       GetLastError to confirm success and then check response object
	**               for the following properties
	**               NewEnable
	**               NewStatus
	**               NewMaxBitRate
	**               NewChannel
	**               NewMACAddressControlEnabled
	**               NewStandard
	**               NewMACAddress
	**               ---------------------------------------------------------------
	**               Optional for vendor specific implementations if not TR64 as 
	**               proprietary implementations can use other SMAPI calls as well
	**               to return below information
	**               NewSSID                           OR use GetWirelessSSID
	**               NewSecurityMode                   OR use GetWirelessSecurity
	**               NewBasicEncryptionModes           OR use GetWirelessSecurity
	**               NewBasicAuthenticationMode        OR use GetWirelessSecurity
	*******************************************************************************/
	GetWirelessInfo : function () 
	{
	  try {
	    var oRsp = this.ExecuteCommand(SVC_WLS_LAN_CONFIG, ACT_WLC_GET_INFO);
	    if(this.oSMAPI.GetLastError() == _constants.SMAPI_SUCCESS) {
	      var oRet = {};
	      for(var sProp in oRsp) {
	        if(sProp == "err" || sProp == "ActionName") {
	          ; //do nothing continue
	        } else if (sProp == "NewBSSID") {
	          oRet.NewMACAddress  = oRsp[sProp];
	        } else if (sProp == "NewBeaconType") {
	          oRet.NewSecurityMode  = oRsp[sProp]; 
	        } else {
	          oRet[sProp] = oRsp[sProp];
	        }     
	      }
	      return oRet;    
	    }  
	  } catch (ex) {
	    _logger.error("_tr64_GetWirelessInfo", ex.message ); 
	    this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
	  }
	  return _constants.SMAPI_OBJ_UNKNOWN;
	},
	
	/***************************************************************************************
	** Name:         _tr64_SetWirelessEnable
	**
	** Purpose:      Enables/Disables wireless interface. 1:on/0:off
	**
	** Parameter:    nEnable : 1:Enable/0:Disable
	**
	** Return:       GetLastError to confirm success
	***************************************************************************************/
	SetWirelessEnable : function (nEnable)
	{
	  this.ExecuteCommand(SVC_WLS_LAN_CONFIG, ACT_WLC_SET_ENABLE, {"NewEnable": nEnable});
	},  
	
	/***************************************************************************************
	** Name:         _tr64_SetWirelessRadio
	**
	** Purpose:      Enables/Disables wireless radio. Comes handy in cases where some 
	**               implementations do not have interface enablement.
	**               1:on/0:off
	**
	** Parameter:    nEnable : 1:Enable/0:Disable
	**
	** Return:       GetLastError to confirm success
	***************************************************************************************/
	SetWirelessRadio : function (nEnable)
	{
	  this.ExecuteCommand(SVC_WLS_LAN_CONFIG, ACT_WLC_SET_RADIOMODE, {"NewRadioEnabled": nEnable});
	},  
	
	/*******************************************************************************
	** Name:         _tr64_SetWirelessConfig  
	**
	** Purpose:      Applies wireless configuration to the modem
	**
	** Parameter:    object of type WLSSecurity
	** 
	** WLSConfig() {   // all default values mean no operation to perform for those 
	**                 // values leave settings in modem as is
	**   this.nEnable      = SMAPI_INT_DONT_CARE; // set wireless radio mode in 
	**                 // other words enable/disable wireless interface 1:on/0:off 
	**   this.sMaxBitRate  = SMAPI_STR_DONT_CARE; // Auto, OR the largest of 
	**                 // the OperationalTrxRates values in (Mbps)
	**   this.nChannel     = SMAPI_INT_DONT_CARE; // set wireless channel
	**   this.ulStandard   = SMAPI_HEX_DONT_CARE; // sets the IEEE 802.11 standard 
	**                 // the device is operating in. Not implemented in TR64
	**   this.nEnableMACL  = SMAPI_INT_DONT_CARE; 
	**                 // set MAC Address control filtering 1:on/0:off
	**   this.oWLSDataTransmission = SMAPI_OBJ_DONT_CARE; 
	**                 // new WLSDataTransmission(); 
	**                 // In general used for setting frame transmission rates. 
	**   this.oWLSSSID     = SMAPI_OBJ_DONT_CARE; // new WLSSSID(); set SSID Info
	**   this.oWLSSecurity = SMAPI_OBJ_DONT_CARE; // new WLSSecurity();   set wireless security                                          
	**   this.oUserDefined = SMAPI_OBJ_DONT_CARE; // for future expansion or use with custom implementations
	** }
	**
	** Return:       GetLastError to confirm success 
	*******************************************************************************/
	SetWirelessConfig : function (oWLSConfig) 
	{
	  // TBD: transaction logic to be built
	  try {
	    // Check if wireless has to be enabled/disabled
	    if(oWLSConfig.nEnable != _constants.SMAPI_INT_DONT_CARE) {
	      this.SetWirelessRadio(oWLSConfig.nEnable);
	      if(this.oSMAPI.GetLastError() != _constants.SMAPI_SUCCESS) return;
	    }  
	    
	    // Check if SSID settings are to be updated
	    if(oWLSConfig.oWLSSSID != _constants.SMAPI_OBJ_DONT_CARE) {
	      this.SetSSIDInfo(oWLSConfig.oWLSSSID);
	      if(this.oSMAPI.GetLastError() != _constants.SMAPI_SUCCESS) return;
	    }  
	    
	    // set wireless security
	    if(oWLSConfig.oWLSSecurity != _constants.SMAPI_OBJ_DONT_CARE) {
	      this.SetWirelessSecurity(oWLSConfig.oWLSSecurity);
	      if(this.oSMAPI.GetLastError() != _constants.SMAPI_SUCCESS) return;
	    }
	    
	    // Now use TR64 SetConfig for the remaining properties    
	    var oArgs = {};
	    var bInit = false;
	    if(oWLSConfig.nChannel != _constants.SMAPI_INT_DONT_CARE) {
	      oArgs["NewChannel"] = oWLSConfig.nChannel;
	      bInit = true;
	    }  
	    if(oWLSConfig.nEnableMACL != _constants.SMAPI_INT_DONT_CARE) {
	      oArgs["NewMACAddressControlEnabled"] = oWLSConfig.nEnableMACL;
	      bInit = true;
	    }  
	    if(oWLSConfig.sMaxBitRate != _constants.SMAPI_STR_DONT_CARE) {
	      oArgs["NewMaxBitRate"] = oWLSConfig.sMaxBitRate;
	      bInit = true;
	    }  
	    if(bInit) this.ExecuteCommand(SVC_WLS_LAN_CONFIG, ACT_WLC_SET_CONFIG, oArgs);
	    
	  } catch (ex) {
	    _logger.error("_tr64_SetWirelessConfig", ex.message ); 
	    this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
	  }
	},
	
	/*******************************************************************************
	** Name:         _tr64_GetWirelessSecurity
	**
	** Purpose:      Retrieves all the security variables requested by caller.
	**
	** Parameter:    ulFlag : Helps implementations in knowing what is required
	**               by caller at this particular time. This helps in reducing the
	**               calls made to modem or targetting them better.
	**               SMAPI_HEX_DONT_CARE : return only what is set in modem
	**               WLS_SECMODE_BASIC   : return properties set for mode Basic
	**               WLS_SECMODE_WPA     : return properties set for mode WPA
	**               WLS_SECMODE_WPA2    : return properties set for 11i.
	**
	** Return:       GetLastError to confirm success and the check response object
	**               for object of type WLSSecurity
	*******************************************************************************/
	GetWirelessSecurity : function (ulFlag) 
	{
	  if(!this.pvt_Init()) return null;
	  try {
	    var oWLSSecurity = new WLSSecurity;
	    
	    if(ulFlag == _constants.SMAPI_HEX_DONT_CARE) {     //return only what is set in modem
	      
	      oWLSSecurity.ulSecMode = this.GetWirelessBeaconType();
	      if(this.oSMAPI.GetLastError() != _constants.SMAPI_SUCCESS) return _constants.SMAPI_OBJ_DONT_CARE;
	      
	      // DB: TODO considering only single flag values. Need to research how TR64
	      // allows handling multiple security modes in one shot
	      switch(oWLSSecurity.ulSecMode) {
	        case WLS_SECMODE_NONE:              // no need to check further in no security
	          oWLSSecurity.ulEncryption     = WLS_ENCRYPT_NONE;
	          oWLSSecurity.ulAuthentication = WLS_AUTH_OPEN;  
	          break;
	        
	        case WLS_SECMODE_BASIC:             // call with basic security   
	          oWLSSecurity = this.GetWirelessSecurity(WLS_SECMODE_BASIC);
	          break;
	          
	        case WLS_SECMODE_WPA:               // Security mode WPA
	          oWLSSecurity = this.GetWirelessSecurity(WLS_SECMODE_WPA);
	          break;
	      
	        case WLS_SECMODE_WPA2:              // Security mode WPA2 or 11i
	          this.oSMAPI.SetLastError(_constants.SMAPI_FUNCTION_NOTIMPL);
	          return _constants.SMAPI_OBJ_DONT_CARE;
	      }  
	      return oWLSSecurity;
	    
	    } else if (_smapiutils.MaskIn(ulFlag, WLS_SECMODE_BASIC)) {
	      oWLSSecurity.ulSecMode = WLS_SECMODE_BASIC;
	      var oRet = this.GetWirelessBasicSecurity();
	      if (this.oSMAPI.GetLastError() == _constants.SMAPI_SUCCESS) {
	        oWLSSecurity.ulAuthentication = oRet.ulAuthentication;
	        oWLSSecurity.ulEncryption     = oRet.ulEncryption;
	        // In basic mode we may or may not have encryption turned 
	        // on hence check before calling security keys
	        if(oWLSSecurity.ulEncryption == (oWLSSecurity.ulEncryption & WLS_ENCRYPT_WEP)){ 
	          oWLSSecurity.oKeys = this.GetWirelessSecurityKeys();
	        }
	      }
	      return oWLSSecurity;
	      
	    } else if (_smapiutils.MaskIn(ulFlag, WLS_SECMODE_WPA)) {
	      oWLSSecurity.ulSecMode = WLS_SECMODE_WPA;
	      var oRet = this.GetWirelessWPASecurity();
	      if (this.oSMAPI.GetLastError() == _constants.SMAPI_SUCCESS) {
	        oWLSSecurity.ulAuthentication = oRet.ulAuthentication;
	        oWLSSecurity.ulEncryption     = oRet.ulEncryption;
	        // In WPA encryption is always on so call to get security keys
	        // for pre shared key
	        oWLSSecurity.oKeys = this.GetWirelessSecurityKeys();
	      }
	      return oWLSSecurity;
	      
	    } else if (_smapiutils.MaskIn(ulFlag, WLS_SECMODE_WPA2)) {
	      oWLSSecurity.ulSecMode = WLS_SECMODE_WPA2;
	      this.oSMAPI.SetLastError(_constants.SMAPI_FUNCTION_NOTIMPL);
	      return null;
	    }
	  } catch (ex) {
	    _logger.error("_tr64_GetWirelessSecurity", ex.message ); 
	    this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
	  }
	  return null;  
	},
	
	/*******************************************************************************
	** Name:         _tr64_SetWirelessSecurity 
	**
	** Purpose:      Applies Wireless security settings to the modem
	**
	** Parameter:    object of type WLSSecurity
	**
	** WLSSecurity() {  // all default values mean no operation to perform for those values
	**                  // leave settings in modem as is
	**  this.ulSecMode        = SMAPI_HEX_DONT_CARE;
	**  this.ulAuthentication = SMAPI_HEX_DONT_CARE; 
	**  this.ulEncryption     = SMAPI_HEX_DONT_CARE;
	**  this.oKeys            = new WLSSecurityKeys();
	**  this.oUserDefined     = SMAPI_OBJ_DONT_CARE; // for future expansion or use with 
	**                                               // custom implementations
	** }
	**    
	** Return:       GetLastError to confirm success 
	*******************************************************************************/
	SetWirelessSecurity : function (oWLSSecurity) 
	{
	  // TBD: transaction logic to be built
	  if(!this.pvt_Init()) return;
	  
	  try {
	    // Check if security mode is to be set
	    if(oWLSSecurity.ulSecMode != _constants.SMAPI_HEX_DONT_CARE) {
	      this.SetWirelessBeaconType(oWLSSecurity.ulSecMode);
	      if(this.oSMAPI.GetLastError() != _constants.SMAPI_SUCCESS) return;
	    }  
	    
	    // Check if authentication and encryption are to be set
	    if( oWLSSecurity.ulAuthentication  != _constants.SMAPI_HEX_DONT_CARE ||
	        oWLSSecurity.ulEncryption != _constants.SMAPI_HEX_DONT_CARE) {
	      switch(oWLSSecurity.ulSecMode) {
	        case WLS_SECMODE_NONE:
	          // Authentication open and no encryption is by default
	          // used with no security hence do nothing here
	          break;
	        case WLS_SECMODE_BASIC: 
	          if(oWLSSecurity.ulEncryption == (oWLSSecurity.ulEncryption & WLS_ENCRYPT_WEP)) {
	            // In WEPEncryption passing a WEPKeyIndex or a passphrase is necessary
	            if(oWLSSecurity.oKeys.sWEPKey0 == _constants.SMAPI_EMPTY_STR &&
	              oWLSSecurity.oKeys.sKeyPassphrase == _constants.SMAPI_EMPTY_STR) {
	              this.oSMAPI.SetLastError(_constants.SMAPI_INVALID_ARGUMENT);
	              return;
	            }
	          }    
	          this.SetWirelessBasicSecurity(oWLSSecurity.ulAuthentication, oWLSSecurity.ulEncryption);
	          break;
	        case WLS_SECMODE_WPA  :
	          if(oWLSSecurity.ulAuthentication == (oWLSSecurity.ulAuthentication & WLS_AUTH_PSK)) {
	            // In WEPEncryption passing a WEPKeyIndex or a passphrase is necessary
	            if(oWLSSecurity.oKeys.sPreSharedKey == _constants.SMAPI_EMPTY_STR) {
	              this.oSMAPI.SetLastError(_constants.SMAPI_INVALID_ARGUMENT);
	              return;
	            }
	          }
	          this.SetWirelessWPASecurity(oWLSSecurity.ulAuthentication, oWLSSecurity.ulEncryption);
	          break;
	        case WLS_SECMODE_WPA2 : 
	          this.oSMAPI.SetLastError(_constants.SMAPI_FUNCTION_NOTIMPL); // TBD 
	          break;
	      }
	      if(this.oSMAPI.GetLastError() != _constants.SMAPI_SUCCESS) return;
	    }  
	    
	    // Now set wireless security keys
	    this.SetWirelessSecurityKeys(oWLSSecurity.oKeys);
	    
	  } catch (ex) {
	    _logger.error("_tr64_SetWirelessSecurity", ex.message ); 
	    this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
	  }
	},
	
	/***************************************************************************************
	** Name:         _tr64_GetWirelessSecurityKeys
	**
	** Purpose:      
	**
	** Parameter:    none
	**
	** Return:       GetLastError to confirm success and then check the returned object
	***************************************************************************************/
	GetWirelessSecurityKeys : function ()
	{
	  var oRet = _constants.SMAPI_OBJ_UNKNOWN;
	  try {
	    oRet = this.ExecuteCommand(SVC_WLS_LAN_CONFIG, ACT_WLC_GET_SECKEYS);
	    if(this.oSMAPI.GetLastError() == _constants.SMAPI_SUCCESS) {
	      var oWLSSecurityKeys = new WLSSecurityKeys();  
	      oWLSSecurityKeys.sWEPKey0       =  oRet.NewWEPKey0;  
	      oWLSSecurityKeys.sWEPKey1       =  oRet.NewWEPKey1; 
	      oWLSSecurityKeys.sWEPKey2       =  oRet.NewWEPKey2; 
	      oWLSSecurityKeys.sWEPKey3       =  oRet.NewWEPKey3; 
	      oWLSSecurityKeys.sPreSharedKey  =  oRet.NewPreSharedKey; 
	      oWLSSecurityKeys.sKeyPassphrase =  oRet.NewKeyPassphrase; 
	      return oWLSSecurityKeys;
	    }  
	  } catch (ex) {
	    _logger.error("_tr64_GetWirelessSecurityKeys", ex.message ); 
	    this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
	  }
	  return oRet;
	},
	
	/***************************************************************************************
	** Name:         _tr64_SetWirelessSecurityKeys 
	**
	** Purpose:      Sets WEP keys or passphrase or Preshared Key used in WPA
	**
	** Parameter:    object of type WLSSecurityKeys
	**
	** Return:       GetLastError to confirm success and then check the returned object
	***************************************************************************************/
	SetWirelessSecurityKeys : function (oWLSSecurityKeys)
	{
	  // TBD: transaction logic to be built
	  try {
	    var oArgs = {
	      "NewWEPKey0":       oWLSSecurityKeys.sWEPKey0,
	      "NewWEPKey1":       oWLSSecurityKeys.sWEPKey1,
	      "NewWEPKey2":       oWLSSecurityKeys.sWEPKey2,
	      "NewWEPKey3":       oWLSSecurityKeys.sWEPKey3,
	      "NewPreSharedKey":  oWLSSecurityKeys.sPreSharedKey,
	      "NewKeyPassphrase": oWLSSecurityKeys.sKeyPassphrase
	    };
	    return this.ExecuteCommand(SVC_WLS_LAN_CONFIG, ACT_WLC_SET_SECKEYS, oArgs); 
	  } catch (ex) {
	    _logger.error("_tr64_SetWirelessSecurityKeys", ex.message ); 
	    this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
	  }
	  return _constants.SMAPI_OBJ_UNKNOWN;
	},  
	
	/*******************************************************************************
	** Name:         _tr64_GetSSIDInfo 
	**
	** Purpose:      Gets Network name(SSID) and SSID broadcast values from modem. 
	**               
	** Parameter:    ulFlag : Helps implementations in knowing what is required
	**               by caller at this particular time. This helps in reducing the
	**               calls made to modem or targetting them better.
	**
	**               WLS_SSID_NAME:       caller needs only NewSSID value
	**               WLS_SSID_BROADCAST:  caller needs only NewSSIDBroadcast value
	**               WLS_SSID_NAME|WLS_SSID_BROADCAST: caller needs both  
	**               SMAPI_HEX_DONT_CARE: special case again caller needs both
	**
	** Return:       GetLastError to confirm success and then check response object
	**               for the following properties
	**               NewSSID
	**               NewSSIDBroadcastEnabled
	*******************************************************************************/
	GetSSIDInfo : function (ulFlag) 
	{
	  try {
	    var oRsp = _constants.SMAPI_OBJ_UNKNOWN;
	    var oRet = {};
	    // Changing the meaning of don't care as nobody should call this function
	    // if they don't care to get even a single value
	    if(ulFlag == _constants.SMAPI_HEX_DONT_CARE) ulFlag = WLS_SSID_NAME|WLS_SSID_BROADCAST;
	      
	    // Get Network Name
	    if(sma_mut_MaskIn(ulFlag, WLS_SSID_NAME)) {
	      oRsp = this.ExecuteCommand(SVC_WLS_LAN_CONFIG, ACT_WLC_GET_SSID);
	      if(this.oSMAPI.GetLastError() == _constants.SMAPI_SUCCESS) {
	        oRet.NewSSID = oRsp.NewSSID;  
	      }     
	    } 
	    
	    //Get SSID Broadcast value
	    if(this.oSMAPI.GetLastError() == _constants.SMAPI_SUCCESS &&
	      _smapiutils.MaskIn(ulFlag, WLS_SSID_BROADCAST)) {
	      oRsp = this.ExecuteCommand(SVC_WLS_LAN_CONFIG, ACT_WLC_GET_SSID_BRDCAST);
	      if(this.oSMAPI.GetLastError() == _constants.SMAPI_SUCCESS) {
	        oRet.NewSSIDBroadcastEnabled = oRsp.NewBeaconAdvertisementEnabled;  
	      }
	    }  
	    
	    // Check if oRet has something worth returning
	    if(this.oSMAPI.GetLastError() == _constants.SMAPI_SUCCESS)
	      return oRet;
	  } catch (ex) {
	    _logger.error("_tr64_GetSSIDInfo", ex.message ); 
	    this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
	  }
	  return _constants.SMAPI_OBJ_UNKNOWN;
	},
	
	/*******************************************************************************
	** Name:         _tr64_SetSSIDInfo
	**
	** Purpose:      Sets Network name(SSID) and SSID broadcast values to modem.
	**
	** Parameter:    object of type oWLSSSID
	**
	** WLSSSID() {   // all default values mean no operation to perform for those values
	**               // leave settings in modem as is
	**   this.sSSID            = SMAPI_STR_DONT_CARE;   // Server Set ID
	**   this.nSSIDBroadcast   = SMAPI_INT_DONT_CARE;   // 1:On / 0: Off
	** }
	** 
	**Return:       GetLastError to confirm success 
	*******************************************************************************/
	SetSSIDInfo : function (oWLSSSID) 
	{
	  // TBD: transaction logic to be built
	  try {
	    // Check if SSID is to be set
	    if(oWLSSSID.sSSID != _constants.SMAPI_STR_DONT_CARE) {
	      this.ExecuteCommand(SVC_WLS_LAN_CONFIG, ACT_WLC_SET_SSID, {"NewSSID": oWLSSSID.sSSID});
	    }  
	  
	    // Check if SSID Broadcast is to be updated
	    if(this.oSMAPI.GetLastError() == _constants.SMAPI_SUCCESS &&
	       oWLSSSID.nSSIDBroadcast != _constants.SMAPI_INT_DONT_CARE) {
	       
	      var oArgs = {"NewBeaconAdvertisementEnabled": oWLSSSID.nSSIDBroadcast};
	      this.ExecuteCommand(SVC_WLS_LAN_CONFIG, ACT_WLC_SET_SSID_BRDCAST, oArgs);
	    }  
	  } catch(ex) {
	    _logger.error("_tr64_SetSSIDInfo", ex.message );
	    this.oSMAPI.SetLastError(_constants.SMAPI_EXCEPTION);
	  }
	},
	
	/***************************************************************************************
	** Name:         _tr64_GetWirelessBeaconType
	**
	** Purpose:      
	**
	** Parameter:    none
	**
	** Return:       GetLastError to confirm success and then check the returned hex value
	***************************************************************************************/
	GetWirelessBeaconType : function ()
	{
	  var ulRet = _constants.SMAPI_VAL_UNKNOWN;
	  try {
	    var oRsp = this.ExecuteCommand(SVC_WLS_LAN_CONFIG, ACT_WLC_GET_BEACON_TYPE);
	    if(this.oSMAPI.GetLastError() == _constants.SMAPI_SUCCESS) {
	      switch(oRsp.NewBeaconType) {
	        case "None":              ulRet = WLS_SECMODE_NONE ; 
	          break;
	        case "Basic":             ulRet = WLS_SECMODE_BASIC;
	          break;
	        case "WPA":               ulRet = WLS_SECMODE_WPA;
	          break;
	        case "11i":               ulRet = WLS_SECMODE_WPA2;
	          break;
	        case "BasicandWPA":       ulRet = WLS_SECMODE_BASIC|WLS_SECMODE_WPA;
	          break;     
	        case "Basicand11i":       ulRet = WLS_SECMODE_BASIC|WLS_SECMODE_11i;
	          break;     
	        case "WPAand11i":         ulRet = WLS_SECMODE_WPA|WLS_SECMODE_11i;
	          break;       
	        case "BasicandWPAand11i": ulRet = WLS_SECMODE_BASIC|WLS_SECMODE_WPA|WLS_SECMODE_11i;
	          break;           
	      }      
	    }  
	  } catch (ex) {
	    _logger.error("_tr64_GetWirelessBeaconType", ex.message ); 
	    this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
	  }
	  return ulRet;
	},  
	
	/***************************************************************************************
	** Name:         _tr64_SetWirelessBeaconType 
	**
	** Purpose:      Sets Wireless security mode or beacon type
	**
	** Parameter:    ulFlag : Having the beacon type to be set
	**
	** Return:       GetLastError to confirm success 
	***************************************************************************************/
	SetWirelessBeaconType : function (ulFlag)
	{
	  var oArgs = {"NewBeaconType": g_oTR64_SEC_MODES[ulFlag]};
	  this.ExecuteCommand(SVC_WLS_LAN_CONFIG, ACT_WLC_SET_BEACON_TYPE, oArgs);
	},  
	
	/***************************************************************************************
	** Name:         _tr64_GetWirelessBasicSecurity
	**
	** Purpose:      
	**
	** Parameter:    none
	**
	** Return:       GetLastError to confirm success and then check the returned object
	***************************************************************************************/
	GetWirelessBasicSecurity : function ()
	{
	  var oRet =  { 
	    ulAuthentication : _constants.SMAPI_HEX_DONT_CARE,
	    ulEncryption: _constants.SMAPI_HEX_DONT_CARE
	  };  
	  try {
	    var oRsp = this.ExecuteCommand(SVC_WLS_LAN_CONFIG, ACT_WLC_GET_BASIC_PROP); 
	    if(this.oSMAPI.GetLastError() == _constants.SMAPI_SUCCESS) {
	      switch(oRsp.NewBasicAuthenticationMode) {
	        case g_oTR64_AUTH_MODES[_constants.WLS_AUTH_OPEN]: oRet.ulAuthentication = WLS_AUTH_OPEN; 
	          break;
	        case g_oTR64_AUTH_MODES[_constants.WLS_AUTH_EAP] : oRet.ulAuthentication = WLS_AUTH_EAP; 
	          break;
	      }
	      switch(oRsp.NewBasicEncryptionModes) {
	        case g_oTR64_ENCRYPT_MODES[_constants.WLS_ENCRYPT_NONE]: oRet.ulEncryption = WLS_ENCRYPT_NONE; 
	          break;
	        case g_oTR64_ENCRYPT_MODES[_constants.WLS_ENCRYPT_WEP] : oRet.ulEncryption = WLS_ENCRYPT_WEP; 
	          break;
	      }      
	    }  
	  } catch (ex) {
	    _logger.error("_tr64_GetWirelessBasicSecurity", ex.message ); 
	    this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
	  }
	  return oRet;
	},  
	
	/***************************************************************************************
	** Name:         _tr64_SetWirelessBasicSecurity 
	**
	** Purpose:      Sets Wireless Basic security mode properties namely authentication and 
	**               encryption
	**
	** Parameter:    ulAuthentication :
	**               ulEncryption :
	**
	** Return:       GetLastError to confirm success 
	***************************************************************************************/
	SetWirelessBasicSecurity : function (ulAuthentication, ulEncryption)
	{
	  try {
	    var oArgs = {};
	    if(ulAuthentication != _constants.SMAPI_HEX_DONT_CARE)
	      oArgs["NewBasicAuthenticationMode"] = g_oTR64_AUTH_MODES[ulAuthentication];
	    if(ulEncryption != _constants.SMAPI_HEX_DONT_CARE)
	      oArgs["NewBasicEncryptionModes"] = g_oTR64_ENCRYPT_MODES[ulEncryption];
	    
	    this.ExecuteCommand(SVC_WLS_LAN_CONFIG, ACT_WLC_SET_BASIC_PROP, oArgs); 
	  } catch (ex) {
	    _logger.error("_tr64_SetWirelessBasicSecurity", ex.message ); 
	    this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
	  }
	},  
	
	/***************************************************************************************
	** Name:         _tr64_GetWirelessWPASecurity
	**
	** Purpose:      
	**
	** Parameter:    none
	**
	** Return:       GetLastError to confirm success and then check the returned object
	***************************************************************************************/
	GetWirelessWPASecurity : function ()
	{
	  var oRet =  { 
	    ulAuthentication : _constants.SMAPI_HEX_DONT_CARE,
	    ulEncryption: _constants.SMAPI_HEX_DONT_CARE
	  };  
	  try {
	    var oRsp = this.ExecuteCommand(SVC_WLS_LAN_CONFIG, ACT_WLC_GET_WPA_PROP); 
	    if(this.oSMAPI.GetLastError() == _constants.SMAPI_SUCCESS) {
	      for(var ulAuth in g_oTR64_AUTH_MODES) {
	        if(g_oTR64_AUTH_MODES[ulAuth] == oRsp.NewWPAAuthenticationMode) {
	          oRet.ulAuthentication = ulAuth;
	          break;
	        }
	      }
	      for(var ulEncrypt in g_oTR64_ENCRYPT_MODES) {
	        if(g_oTR64_ENCRYPT_MODES[ulEncrypt] == oRsp.NewWPAEncryptionModes) {
	          oRet.ulEncryption = ulEncrypt;
	          break;
	        }
	      }    
	    }  
	  } catch (ex) {
	    _logger.error("_tr64_GetWirelessWPASecurity", ex.message ); 
	    this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
	  }
	  return oRet;
	},  
	
	/***************************************************************************************
	** Name:         _tr64_SetWirelessWPASecurity 
	**
	** Purpose:      Sets Wireless WPA security mode properties namely authentication and 
	**               encryption
	**
	** Parameter:    ulAuthentication :
	**               ulEncryption :
	**
	** Return:       GetLastError to confirm success 
	***************************************************************************************/
	SetWirelessWPASecurity : function (ulAuthentication, ulEncryption)
	{
	  var oArgs = {};
	  // Check Authentication
	  if(ulAuthentication != _constants.SMAPI_HEX_DONT_CARE)
	    oArgs["NewWPAAuthenticationMode"] = g_oTR64_AUTH_MODES[ulAuthentication];
	  // Check Encryption  
	  if(ulEncryption != _constants.SMAPI_HEX_DONT_CARE)
	    oArgs["NewWPAEncryptionModes"] = g_oTR64_ENCRYPT_MODES[ulEncryption];
	    
	  this.ExecuteCommand(SVC_WLS_LAN_CONFIG, ACT_WLC_SET_WPA_PROP, oArgs);
	},  
	
	/***************************************************************************************
	** Name:         _tr64_ExecuteCommand
	**
	** Purpose:      Forms the SOAP request to be sent using the parameters passed. Returns
	**               the SOAP response parsed into an object after checking for errors.
	**
	** Parameter:    sServiceType : Service to use
	**               sActionName  : Action to be invoked
	**               oArgs        : Arguments to pass   
	**
	** Return:       GetLastError to confirm success and then look up the returned object
	***************************************************************************************/
	ExecuteCommand : function (sServiceType, sActionName, oArgs)
	{
	  if(!this.pvt_Init()) return null;
	    
	  try {
	    this.mObjSOAP.InitAction(sActionName, sServiceType, this.mObjTR64.deviceDOM); 
	    if(oArgs && typeof oArgs == "object") {
	      for(var sProp in oArgs) {
	        this.mObjSOAP.AddArgs(sProp, oArgs[sProp]);
	      }
	    }  
	    var status = this.mObjSOAP.SendRequest();
	    if(status) {
	      var rspObj = this.mObjSOAP.ParseResponse();
	      if(rspObj && rspObj.err != -1) {
	        return rspObj;      
	      } else {
	        // need to parse error and update last error
	        this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_FAIL);
	      }
	    } else {
	      // TBD handle status errror
	      this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_FAIL);
	    }  
	  } catch (ex) {
	    _logger.error("_tr64_ExecuteCommand", ex.message );
	    _logger.error("_tr64_ExecuteCommand", sServiceType + "#" + sActionName);
	    this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
	  }
	  return null;
	},  
	
	/***************************************************************************************
	** Name:         _tr64_pvt_Init
	**
	** Purpose:      Used to intialize internal TR64 objects like SSDP and SOAP requests.
	**               A valid TR64 device has to be connected for intialization to work
	**               propertly.
	**
	** Parameter:    bForce : If set we ignore the initialization and re-do it 
	**
	** Return:       true/false also uses SetLastError()
	***************************************************************************************/
	pvt_Init : function (bForce)
	{
	  if(typeof bForce != "undefined" && bForce)
	    this.m_bInit = false;
	  // no need to initialize if already done once
	  if(this.m_bInit) return true;
	  try {
	    _logger.debug("_tr64_pvt_Init", "Initialize TR64" );
	    var deviceLoc = "";
	    if (typeof this.mProperties.tr64_location_url != 'undefined') 
	      deviceLoc = this.mProperties.tr64_location_url;
	    var ssdpObj = new ssCSSDP(deviceLoc);
	    if(ssdpObj.DoDeviceDiscovery()) {
	      this.mObjTR64 = new ssCTR64(ssdpObj);
	      if(this.mObjTR64.LoadDeviceSpec()) {
	        this.mObjSOAP = new ssCTR64Communication(ssdpObj);
	        return (this.m_bInit = true);
	      }  
	    }
	    if(typeof bForce != "undefined" && bForce) 
	      ; //no op as caller will handle failure because it forced re-initialization
	    else 
	      this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_FAIL);  
	  } catch (ex) {
	    _logger.error("_tr64_pvt_Init", ex.message );
	    this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
	  }
	  return false;
	}
});
  var _logger = $ss.agentcore.log.GetDefaultLogger("_tr64");
  var _utils = $ss.agentcore.utils;
  var _netCheck = $ss.agentcore.network.netcheck;
  var _registry = $ss.agentcore.dal.registry;
  var _config = $ss.agentcore.dal.config;
  var _constants = $ss.agentcore.constants;
  var _smapiutils = $ss.agentcore.smapi.utils;
  var _smapiMethods = $ss.agentcore.smapi.methods;  
  //-------------------------------------------------------------------------------
  // Wireless security mode or beacon types
  //-------------------------------------------------------------------------------
  var g_oTR64_SEC_MODES = {};
  g_oTR64_SEC_MODES[_constants.WLS_SECMODE_NONE]                                   = "None";
  g_oTR64_SEC_MODES[_constants.WLS_SECMODE_BASIC]                                  = "Basic";
	g_oTR64_SEC_MODES[_constants.WLS_SECMODE_WPA]                                    = "WPA";
	g_oTR64_SEC_MODES[_constants.WLS_SECMODE_WPA2]                                   = "11i";
	g_oTR64_SEC_MODES[_constants.WLS_SECMODE_BASIC|_constants.WLS_SECMODE_WPA]                  = "BasicandWPA";
	g_oTR64_SEC_MODES[_constants.WLS_SECMODE_BASIC|_constants.WLS_SECMODE_WPA2]                 = "Basicand11i";
	g_oTR64_SEC_MODES[_constants.WLS_SECMODE_WPA|_constants.WLS_SECMODE_WPA2]                   = "WPAand11i";
	g_oTR64_SEC_MODES[_constants.WLS_SECMODE_BASIC|_constants.WLS_SECMODE_WPA|_constants.WLS_SECMODE_WPA2] = "BasicandWPAand11i";

  //-------------------------------------------------------------------------------
  // Wireless encryption modes
  //-------------------------------------------------------------------------------
  var g_oTR64_ENCRYPT_MODES = {};
	g_oTR64_ENCRYPT_MODES[_constants.WLS_ENCRYPT_NONE]                                 = "None";
	g_oTR64_ENCRYPT_MODES[_constants.WLS_ENCRYPT_WEP]                                  = "WEPEncryption";   
	g_oTR64_ENCRYPT_MODES[_constants.WLS_ENCRYPT_TKIP]                                 = "TKIPEncryption";
	g_oTR64_ENCRYPT_MODES[_constants.WLS_ENCRYPT_AES]                                  = "AESEncryption";   
	g_oTR64_ENCRYPT_MODES[_constants.WLS_ENCRYPT_WEP|_constants.WLS_ENCRYPT_TKIP]                 = "WEPandTKIPEncryption";   
	g_oTR64_ENCRYPT_MODES[_constants.WLS_ENCRYPT_WEP|_constants.WLS_ENCRYPT_AES]                  = "WEPandAESEncryption";
	g_oTR64_ENCRYPT_MODES[_constants.WLS_ENCRYPT_TKIP|_constants.WLS_ENCRYPT_AES]                 = "TKIPandAESEncryption";
	g_oTR64_ENCRYPT_MODES[_constants.WLS_ENCRYPT_WEP|_constants.WLS_ENCRYPT_TKIP|_constants.WLS_ENCRYPT_AES] = "WEPandTKIPandAESEncryption";   
  
  //-------------------------------------------------------------------------------
  // Wireless authentication modes
  //-------------------------------------------------------------------------------
  var g_oTR64_AUTH_MODES = {};
	g_oTR64_AUTH_MODES[_constants.WLS_AUTH_OPEN] = "None";
	g_oTR64_AUTH_MODES[_constants.WLS_AUTH_EAP]  = "EAPAuthentication";   
	g_oTR64_AUTH_MODES[_constants.WLS_AUTH_PSK]  = "PSKAuthentication";
  
  //-------------------------------------------------------------------------------
  // IEEE 802.11 standard
  //-------------------------------------------------------------------------------
  var g_oTR64_802_11_STD = {};
	g_oTR64_802_11_STD[_constants.WLS_STD_802_11A] = "a";
	g_oTR64_802_11_STD[_constants.WLS_STD_802_11B] = "b";
	g_oTR64_802_11_STD[_constants.WLS_STD_802_11G] = "g";


  //-------------------------------------------------------------------------------
  // TR64 services and Actions supported
  //-------------------------------------------------------------------------------
  var SVC_DEVICE_INFO           = "urn:dslforum-org:service:DeviceInfo:1";
  var ACT_DIN_GET_INFO          = "GetInfo";

  var SVC_LAN_ETHERNET_IFCONFIG = "urn:dslforum-org:service:LANEthernetInterfaceConfig:1";
  var ACT_LEI_GET_INFO          = "GetInfo";

  var SVC_WAN_DSLLINK_CONFIG    = "urn:dslforum-org:service:WANDSLLinkConfig:1";
  var ACT_WDC_GET_INFO          = "GetInfo";
  var ACT_WDC_SET_DSLLINK_TYPE  = "SetDSLLinkType";
  var ACT_WDC_SET_DEST_ADDR     = "SetDestinationAddress";
  var ACT_WDC_SET_ENABLE        = "SetEnable";

  var SVC_WAN_PPP_CONNECTION    = "urn:dslforum-org:service:WANPPPConnection:1";
  var ACT_WPC_GET_STATUS_INFO   = "GetStatusInfo";
  var ACT_WPC_GET_CONNTYPE_INFO = "GetConnectionTypeInfo";
  var ACT_WPC_SET_CONNTYPE      = "SetConnectionType";
  var ACT_WPC_SET_USERNAME      = "SetUserName";
  var ACT_WPC_SET_PASSWORD      = "SetPassword";
  var ACT_WPC_RQST_TERMINATION  = "RequestTermination";
  var ACT_WPC_FRCE_TERMINATION  = "ForceTermination";
  var ACT_WPC_RQST_CONNECTION   = "RequestConnection";

  var SVC_DEVICE_CONFIG         = "urn:dslforum-org:service:DeviceConfig:1";
  var ACT_DCF_REBOOT            = "Reboot";

  var SVC_WAN_DSL_IFCONFIG      = "urn:dslforum-org:service:WANDSLInterfaceConfig:1";
  var ACT_WDI_GET_INFO          = "GetInfo";

  var SVC_WLS_LAN_CONFIG        = "urn:dslforum-org:service:WLANConfiguration:1";
  var ACT_WLC_GET_INFO          = "GetInfo";
  var ACT_WLC_SET_ENABLE        = "SetEnable";
  var ACT_WLC_SET_RADIOMODE     = "SetRadioMode";
  var ACT_WLC_SET_CONFIG        = "SetConfig";
  var ACT_WLC_GET_SECKEYS       = "GetSecurityKeys";
  var ACT_WLC_SET_SECKEYS       = "SetSecurityKeys";
  var ACT_WLC_GET_SSID          = "GetSSID";
  var ACT_WLC_SET_SSID          = "SetSSID";
  var ACT_WLC_GET_SSID_BRDCAST  = "GetBeaconAdvertisement";
  var ACT_WLC_SET_SSID_BRDCAST  = "SetBeaconAdvertisement";
  var ACT_WLC_GET_BEACON_TYPE   = "GetBeaconType";
  var ACT_WLC_SET_BEACON_TYPE   = "SetBeaconType";
  var ACT_WLC_GET_BASIC_PROP    = "GetBasBeaconSecurityProperties";
  var ACT_WLC_SET_BASIC_PROP    = "SetBasBeaconSecurityProperties";
  var ACT_WLC_GET_WPA_PROP      = "GetWPABeaconSecurityProperties";
  var ACT_WLC_SET_WPA_PROP      = "SetWPABeaconSecurityProperties";
  