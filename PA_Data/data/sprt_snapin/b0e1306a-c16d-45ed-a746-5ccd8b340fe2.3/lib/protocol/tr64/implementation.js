/***************************************************************************************
 **
 **  File Name: tr64_imp.js
 **
 **  Summary: Supports TR64 implementation classes
 **
 **  Description: This javascript contains classes and methods that provide TR64
 **               protocol implementation and support for TR64 test harness.
 **
 **  Copyright SupportSoft Inc. 2005, All rights reserved.
 ***************************************************************************************/

$ss.snapin = $ss.snapin ||
{};
$ss.snapin.modem = $ss.snapin.modem ||
{};
$ss.snapin.modem.tr64 = $ss.snapin.modem.tr64 ||
{};
/** @namespace Holds all tr64 implementation related functionality*/
$ss.snapin.modem.tr64.imp = $ss.snapin.modem.tr64.imp ||
{};

(function(){
 $.extend($ss.snapin.modem.tr64.imp, {
    /***************************************************************************************
     ** Name:         ltrim
     **
     ** Purpose:
     ***************************************************************************************/
    ltrim: function(argvalue){
      while (1) {
        if (argvalue.substring(0, 1) != " ") 
          break;
        argvalue = argvalue.substring(1, argvalue.length);
      }
      return argvalue;
    },
    
    
    /***************************************************************************************
     ** Name:         rtrim
     **
     ** Purpose:
     ***************************************************************************************/
    rtrim: function(argvalue){
      while (1) {
        if (argvalue.substring(argvalue.length - 1, argvalue.length) != " ") 
          break;
        argvalue = argvalue.substring(0, argvalue.length - 1);
      }
      return argvalue;
    },
    
    /***************************************************************************************
     ** Name:         trim
     **
     ** Purpose:
     ***************************************************************************************/
    trim: function(argvalue){
      var tmpstr = ltrim(argvalue);
      return rtrim(tmpstr);
    },
  /*******************************************************************************
   ** Name:         pvt_HttpGetData
   **
   ** Purpose:      Http Get Data (Using password control)
   **               TBD: Porting to sprtExternal
   **
   ** Parameter:    sUrl     : Remote destination where sData will be posted to
   **               sUsername: Username for authentication
   **               sPassword: Password for authentication
   **
   ** Return:       sData    : Data returned if successful else empty string
   *******************************************************************************/
  _HttpGetData :function (sUrl, sUsername, sPassword){
    var sData = "";
    try {
      var oPassCtl = _utils.CreateActiveXObject("sdcuser.tgpassctl", true);
      oPassCtl.open(sUrl, sUsername, sPassword);
      oPassCtl.setRequestHeader("Authorization", "Basic ");
      var rVal = oPassCtl.Send("GET", sUrl, "");
      if (rVal && oPassCtl.status == 200) {
        _utils.Sleep(200);
        sData = oPassCtl.responseText;
      }
      else 
        if (oPassCtl.status == 401) {
          _logger.warning("_HttpGetData()", "Failed Authentication");
          // This can only happen in failed authentication case
          if (_smapiMethods) 
            _smapiMethods.SetLastError(_constants.SMAPI_NO_AUTH);
        }
      oPassCtl.close();
      oPassCtl = null;
    } 
    catch (ex) {
      _logger.error("_HttpGetData()", ex.message);
      if (_smapiMethods) 
        _smapiMethods.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
    }
    return sData;
  },
  
  /***************************************************************************************
   ** Name:         pvt_LoadDOM( sUrl )
   **
   ** Purpose:      Load the file into the DOM, check error status
   **
   ** Parameter:    The file to load
   **
   ** Return:       A DOM object if success, otherwise null
   ***************************************************************************************/
  _LoadDOM: function (sUrl){
    var oDOM = null;
    _logger.info("_LoadDOM()", "Url: " + sUrl);
    try {
      // Curent implementations of TR64 do not authentication turned ON
      // and so no need of passing any username and password
      var sData = this._HttpGetData(sUrl, "", "");
      if (sData != "") {
        oDOM = new ActiveXObject("Microsoft.XMLDOM");
        oDOM.loadXML(sData);
        if (oDOM.documentElement == null) {
          if (_smapiMethods) 
            _smapiMethods.SetLastError(_constants.SMAPI_DERIVED_FAIL);
        }
      }
      else {
        _logger.warning("_LoadDOM()", "DOM load failed for Url: " + sUrl);
      }
    } 
    catch (ex) {
      _logger.error("_LoadDOM()", ex.message + "Url: " + sUrl);
    }
    return oDOM;
  },
  
  /***************************************************************************************
   ** Name:         pvt_ShowBranch( divNo )
   **
   ** Purpose:
   ***************************************************************************************/
  _ShowBranch: function (num){
    var branchId = XML_BRANCH + num;
    var imageId = XML_FOLDER + num;
    var objBranch = document.getElementById(branchId);
    var objImage = document.getElementById(imageId);
    if (objBranch.className == "clsNavBranchShow") {
      objBranch.className = "clsNavBranchHide";
      objImage.className = "clsImageBranchHide";
    }
    else {
      objBranch.className = "clsNavBranchShow";
      objImage.className = "clsImageBranchShow";
    }
    event.cancelBubble = true;
  },
  
  /***************************************************************************************
   ** Name:         pvt_GetXMLHttpObj( )
   **
   ** Purpose:
   ***************************************************************************************/
  _GetXMLHttpObj: function (){
    try {
      if (document.xmlhttp) 
        return document.xmlhttp;
      var xmlHttp =  _utils.CreateActiveXObject("sdcuser.tgpassctl",true);
      return xmlHttp;
    } 
    catch (e) {
      _logger.error("_GetXMLHttpObj ", "Failed to get an instance of xmlhttp : " + e);
      return null;
    }
  },
  
  /***************************************************************************************
   ** Name:         pvt_GetServiceFromType
   **
   ** Purpose:
   ***************************************************************************************/
  _GetServiceFromType: function (svcType){
    var serviceName = svcType.replace("urn:dslforum-org:service:", "");
    serviceName = serviceName.replace("urn:schemas-upnp-org:service:", "");
    return serviceName.replace(":1", "");
  },
  
  /***************************************************************************************
   ** Name:         pvt_IsDevice
   **
   ** Purpose:
   ***************************************************************************************/
  _IsDevice: function (str){
    return (str.indexOf("device") != -1);
  },
  
  /***************************************************************************************
   ** Name:         pvt_IsService
   **
   ** Purpose:
   ***************************************************************************************/
  _IsService: function (str){
    return (str.indexOf("service") != -1);
  },
  
  /***************************************************************************************
   ** Name:         pvt_IsURL
   **
   ** Purpose:
   ***************************************************************************************/
  _IsURL: function (str){
    var retVal = false;
    if (str.indexOf("url") != -1) {
      var re = /^m|^u/i;
      retVal = (str.search(re) == -1);
    }
    return retVal;
  }

  });
  var _logger = $ss.agentcore.log.GetDefaultLogger("$ss.snapin.modem.tr64.imp");
  var _exception = $ss.agentcore.exceptions;
  var _constants = $ss.agentcore.constants;
  var _utils = $ss.agentcore.utils;
  var _UIutils = $ss.agentcore.utils.ui;
  var _smapiMethods = $ss.agentcore.smapi.methods;

})();
/***************************************************************************************
 ** Constants for XML
 ***************************************************************************************/
var XML_NODE = "xmlNode_";
var XML_FOLDER = "xmlFolder_";
var XML_BRANCH = "xmlBranch_";
var MAX_UITREE_LEVEL = 1;
var g_TreeLevel = 0;


/***************************************************************************************
 ** Name:         TR64 State variable Object
 **
 ** Purpose:      Encapsulate State variable implementation
 **
 ** Parameter:    name: Name of the state variable
 **               scpdDOM: Service Description Document that contains the state
 **                         variable definition
 ** Return:       State Variable Object if sucessfully initialized else null
 ***************************************************************************************/
function ssCStateVariable(name, scpdDOM){
  this.name = name;
  this.xmlNode = null;
  this._logger = $ss.agentcore.log.GetDefaultLogger("$ss.snapin.tr64.ssCStateVariable");   
  this._exception = $ss.agentcore.exceptions;
  if (scpdDOM != null) {
    var expr = "//stateVariable[name='" + name + "']";
    var svNode = scpdDOM.selectSingleNode(expr);
    if (svNode != null) {
      this.xmlNode = svNode;
    }
    else { // object not initialized properly hence quit
      return null;
    }
  }
  this.dataType = this.xmlNode.selectSingleNode("./dataType").text;
  this.optional = (this.xmlNode.selectSingleNode("./optional") == null) ? false : true;
  // TBD this.Validate     = __Validate;
}

//-------------------------------------------------------------------------------------
ssCStateVariable.prototype.GenerateUI = function ssCStateVariable__GenerateUI(objRow){
  try {
    /*
     <allowedValueRange>
     <minimum>1</minimum>
     <maximum>1540</maximum>
     </allowedValueRange>
     
     <allowedValueList>
     <allowedValue>Disabled</allowedValue>
     <allowedValue>Enabled</allowedValue>
     <allowedValue>Error</allowedValue>
     </allowedValueList>
     */
    var objCell = objRow.insertCell();
    objCell.className = "clsTableRow1";
    
    switch (this.dataType) {
      default:
        var objElement = document.createElement("INPUT");
        objElement.id = "sv_" + objRow.rowIndex + objCell.cellIndex; // get unique id
        // default is text objElement.type = "text";
        objElement.value = "";
        objCell.appendChild(objElement);
    }
    objCell = objRow.insertCell();
    objCell.className = "clsTableRow1";
    objCell.innerHTML = this.dataType; // get unique id
  } 
  catch (ex) {
    _logger.error("ssCStateVariable__GenerateUI", ex.message);
  }
}


/***************************************************************************************
 ** Name:         TR64 Argument Object
 **
 ** Purpose:      Encapsulate Argumet datatypes
 **
 ** Parameter:    name: Name of argument (optional)
 **               value: Value for that argument (optional)
 **
 ** Return:       State Variable Object if sucessfully initialized else null
 ***************************************************************************************/
function ssCArgument(name, value){
  this.name = (typeof name != "undefined") ? name : "";
  this.value = (typeof value != "undefined") ? value : "";
  this.direction = "IN";
  this.relatedStateVariable = null;
  this._logger = $ss.agentcore.log.GetDefaultLogger("$ss.snapin.tr64.ssCArgument");   
  this._exception = $ss.agentcore.exceptions;
}

//-------------------------------------------------------------------------------------
// Another way to initialize this object via argNode, description document
// e.g. argNode   
ssCArgument.prototype.Init = function ssCArgument__Init(argNode, scpdDOM){
  try {
    this.name = argNode.selectSingleNode("./name").text;
    this.direction = argNode.selectSingleNode("./direction").text;
    var sv = argNode.selectSingleNode("./relatedStateVariable").text;
    this.relatedStateVariable = new ssCStateVariable(sv, scpdDOM);
  } 
  catch (ex) {
    _logger.error("ssCArgument__Init", ex.message);
  }
}

//-------------------------------------------------------------------------------------
ssCArgument.prototype.GenerateUI = function ssCArgument__GenerateUI(objTable){
  try {
    var objRow = objTable.insertRow();
    var objCell = objRow.insertCell();
    // write the table cells
    objCell.innerHTML = this.name;
    objCell.className = "clsTableRow1";
    objCell.vAlign = "top";
    this.relatedStateVariable.GenerateUI(objRow);
  } 
  catch (ex) {
    _logger.error("ssCArgument__GenerateUI", ex.message);
  }
}

/***************************************************************************************
 ** Name:         TR64 Action Object
 **
 ** Purpose:      Encapsulate Aaction for a particular service
 **
 ** Parameter:    actionName: Name of the action
 **               serviceType: Service Type comple urn
 **
 ** Return:       Action Object if sucessfully initialized else null
 ***************************************************************************************/
function ssCAction(actionName, serviceType){
  try {
    if ((actionName == "") || (serviceType == "")) 
      return null;
    this.actionName = actionName;
    this.serviceType = serviceType;
    this.serviceName = $ss.snapin.modem.tr64.imp._GetServiceFromType(serviceType);
    this.argList = new Array();
    this.actionNode = null;
    this.optional = false;
	this._logger = $ss.agentcore.log.GetDefaultLogger("$ss.snapin.tr64.ssCAction");   
    this._exception = $ss.agentcore.exceptions;
  } 
  catch (ex) {
    _logger.error("ssCAction", ex.message);
    return null;
  }
}

//-------------------------------------------------------------------------------------
ssCAction.prototype.AddArgs = function ssCAction__AddArgs(name, value){
  try {
    var arg = new ssCArgument(name, value);
    this.argList.push(arg);
  } 
  catch (ex) {
    _logger.error("ssCAction__AddArgs", ex.message);
  }
}

//-------------------------------------------------------------------------------------
ssCAction.prototype.PopulateArgs = function ssCAction__PopulateArgs(scpdDOM){
  try {
    if ((scpdDOM == null) || (typeof scpdDOM == "undefined")) {
      var scpdPath = "../tr_64/scpd/" + this.serviceName + ".xml";
      scpdDOM = $ss.snapin.modem.tr64.imp._LoadDOM(scpdPath);
    }
    
    if (scpdDOM == null) 
      return;
    // cleanup argList
    this.argList.length = 0;
    var expr = "//actionList/action[name='" + this.actionName + "']";
    this.actionNode = scpdDOM.selectSingleNode(expr);
    // TBD: check if we want to only do in direction maybe out proves hadny for
    // responses.
    expr = "./argumentList/argument[direction='in']";
    argList = this.actionNode.selectNodes(expr);
    for (var i = 0, j = argList.length; i < j; i++) {
      var arg = new ssCArgument();
      arg.Init(argList[i], scpdDOM);
      this.argList.push(arg);
    }
    return this.argList;
  } 
  catch (ex) {
    _logger.error("ssCAction__PopulateArgs", ex.message);
  }
}

//-------------------------------------------------------------------------------------
ssCAction.prototype.GenerateUI = function ssCAction__GenerateUI(){
  try {
    var objTable = document.createElement("TABLE");
    objTable.className = "clsTable1";
    objTable.cellSpacing = 0;
    objTable.cellPadding = 0;
    var objRow = objTable.insertRow();
    
    // case when object is not properly initialized
    if (this.actionNode == null) {
      objTable.id = "noArg";
      objCell = objRow.insertCell();
      objCell.className = "clsSubTableHeader";
      objCell.innerHTML = "Need to calls PopulateArgs; Action is not initialized.";
      return objTable;
    }
    // case when no arguments of direction-in are required by the action
    if (this.argList.length == 0) {
      objTable.id = "noArg";
      objCell = objRow.insertCell();
      objCell.className = "clsSubTableHeader";
      objCell.innerHTML = "No arguments required for action:" + this.actionName;
      return objTable;
    }
    // others 
    objTable.id = "actionTable";
    var headings = new Array("Name", "Value", "Type");
    // write the table cells
    for (var i = 0; i < headings.length; i++) {
      objCell = objRow.insertCell();
      objCell.innerHTML = headings[i];
      objCell.className = "clsTableSubHeader";
    }
    for (var i = 0, j = this.argList.length; i < j; i++) {
      this.argList[i].GenerateUI(objTable);
    }
    return objTable;
  } 
  catch (ex) {
    _logger.error("ssCAction__GenerateUI", ex.message);
  }
}

/***************************************************************************************
 ** Name:         TR64 Simple Service Device Protocol Class
 **
 ** Purpose:      Encapsulate SSDP implementation for basically doing device Discovery
 **
 ** Parameter:    deviceLocation : if device location url is known specify it else
 **                                Discover device to find it
 **
 ** Return:       SSDP object
 ***************************************************************************************/
function ssCSSDP(deviceLocation){
  if ((typeof deviceLocation != "undefined") || (deviceLocation != "")) {
    var re = /\s/g;
    this.deviceLocation = deviceLocation.replace(re, "");
  }
  else {
    this.deviceLocation = null;
  }
  this.deviceHost = "";
  this.testLocation = null;
  this._logger = $ss.agentcore.log.GetDefaultLogger("$ss.snapin.tr64.ssCSSDP");   
  this._exception = $ss.agentcore.exceptions;
}

//-------------------------------------------------------------------------------------
ssCSSDP.prototype.pvt_GetDeviceLocation = function ssCSSDP__pvt_GetDeviceLocation(){
  try {
    var oNetCheck = new ActiveXObject("SPRT.SdcNetCheck.1");
    var locInfo = oNetCheck.GetTR064DeviceLocation();
    var elemArr = locInfo.split('\r\n');
    for (var i = 0; i < elemArr.length; i++) {
      var locindex = elemArr[i].indexOf('http://');
      if (locindex != -1) {
        this.deviceLocation = (elemArr[i].substring(locindex, elemArr[i].length));
        return;
      }
    }
  } 
  catch (ex) {
    _logger.error("ssCSSDP__pvt_GetDeviceLocation", ex.message);
  }
  
  return "";
}

//-------------------------------------------------------------------------------------
ssCSSDP.prototype.DoDeviceDiscovery = function ssCSSDP__DoDeviceDiscovery(){
  try {
    if (this.deviceLocation != null && this.deviceLocation != "") {
      if (this.deviceLocation.indexOf("http://") != -1) {
        // a url is used hence set ssdp host values
        this.deviceHost = this.deviceLocation.replace("http://", "");
        var re = /\/[0-9,a-z,\.]+/ig;
        this.deviceHost = this.deviceHost.replace(re, "");
      }
      else { //this can be only a local test location
        this.testLocation = this.deviceLocation;
        // hardcode other values
        this.deviceLocation = "http://192.168.1.1:51679/devicedesc.xml"
        this.deviceHost = "192.168.1.1:51679";
      }
    }
    else {
      try {
        this.pvt_GetDeviceLocation();
        if (this.deviceLocation.indexOf("http://") != -1) {
          // a url is used hence set ssdp host values
          this.deviceHost = this.deviceLocation.replace("http://", "");
          var re = /\/[0-9,a-z,\.]+/ig;
          this.deviceHost = trim(this.deviceHost.replace(re, ""));
        }
      } 
      
      catch (e) {
        alert(e.message);
      }
    }
    return true;
  } 
  catch (ex) {
    _logger.error("ssCSSDP__DoDeviceDiscovery", ex.message);
    return false;
  }
}

/***************************************************************************************
 ** Name:         ssCTransaction class
 **
 ** Purpose:      Encapsulate the TR64 Transaction locking / unlocking.
 **               The concept of having locking / unlocking can be achived by having the
 **               ConfigurationStarted & ConfigurationFinished  soap requests to the CPE
 **               device. This is required for complex async operations. Its upto caller
 **               to decide if the device needs to undergo a few cycles before completion.
 **               In such case, you should use the unique session ID to identify it.
 **
 ** Parameter:    objTR64Comm : ssCTR64Communication Instance
 **
 ** Return:       ssCTransaction object
 ***************************************************************************************/
function ssCTransaction(objTR64Comm){
  this.sessionId = "";
  // this.sessionId2  = ""; tried for Queuing
  this.objComm = objTR64Comm;
  this.serviceType = "urn:dslforum-org:service:DeviceConfig:1";
  this._logger = $ss.agentcore.log.GetDefaultLogger("$ss.snapin.tr64.ssCTransaction");   
  this._exception = $ss.agentcore.exceptions;
}

//-------------------------------------------------------------------------------------
ssCTransaction.prototype.Lock = function ssCTransaction_Lock(deviceDOM){
  try {
    if (this.sessionId != "") // we already have a transaction open
      return this.sessionId;
    // else start new transaction
    var actionName = "ConfigurationStarted";
    this.objComm.InitAction(actionName, this.serviceType, deviceDOM);
    // TBD: use ssdp or other control to get sessionID 
    this.objComm.AddArgs("NewSessionID", "2C0A2110-30BA-444e-8B83-566BC3F19C80");
    var status = this.objComm.SendRequest();
    if (status == true) {
      this.sessionId = "2C0A2110-30BA-444e-8B83-566BC3F19C80";
      // this.sessionId2 = "2C0A2110-30BA-FFFe-8B83-566BC3F19C80"; tried for Queuing
      _logger.debug("ssCTransaction_Lock", "Lock ACQUIRED: ID: " + "2C0A2110-30BA-444e-8B83-566BC3F19C80");
    }
  } 
  catch (ex) {
    _logger.error("ssCTransaction_Lock", ex.message);
  }
  return this.sessionId;
}

//-------------------------------------------------------------------------------------
ssCTransaction.prototype.Release = function ssCTransaction_Release(deviceDOM){
  try {
    if (this.sessionId == "") // we have nothing to release
      return this.sessionId;
    // else close old transaction
    var actionName = "ConfigurationFinished";
    this.objComm.InitAction(actionName, this.serviceType, deviceDOM);
    var status = this.objComm.SendRequest();
    if (status == true) {
      _logger.debug("ssCTransaction_Release", "Lock RELEASED: ID: " + "2C0A2110-30BA-444e-8B83-566BC3F19C80")
      this.sessionId = "";
      // this.sessionId2 = ""; tried for Queuing
    }
  } 
  catch (ex) {
    _logger.error("ssCTransaction_Release", ex.message);
  }
  return this.sessionId;
}


/***************************************************************************************
 ** Name:         ssCTR64Communication class
 **
 ** Purpose:      Encapsulate the TR64 SOAP implementation
 **
 ** Parameter:
 **
 ** Return:       nothing
 ***************************************************************************************/
function ssCTR64Communication(ssdpObj){
  try {
    this.httpObj = $ss.snapin.modem.tr64.imp._GetXMLHttpObj();
    this.ssdpObj = ssdpObj;
    this.trxObj = new ssCTransaction(this);
    this.actionObj = null;
    this.soapRequest = "";
    this.soapResponse = "";
    this.realm = "";
    this.nonce = "";
    this.opaque = "";
    this.username = "";
    this.uri = "";
    this.authNeeded = false;
    this.currControlUrl = "";
    this.allResponses = new Object();
	this._logger = $ss.agentcore.log.GetDefaultLogger("$ss.snapin.tr64.ssCTR64Communication");   
    this._exception = $ss.agentcore.exceptions;
  } 
  catch (ex) {
    _logger.error("ssCTR64Communication", ex.message);
    return null;
  }
}

//-------------------------------------------------------------------------------------
ssCTR64Communication.prototype.InitAction = function ssCTR64Communication__InitAction(actionName, serviceType, deviceDOM){
  try {
    this.actionObj = new ssCAction(actionName, serviceType);
    this.soapRequest = "";
    this.soapResponse = "";
    var expr = "//service[serviceType='" + serviceType + "']/controlURL";
    var oNode = deviceDOM.selectSingleNode(expr);
    if (oNode != null) {
      this.currControlUrl = oNode.text;
    }
  } 
  catch (ex) {
    _logger.error("ssCTR64Communication__InitAction", ex.message + " - ssCTR64Communication__ReInit()");
    return false;
  }
  return true;
}

//-------------------------------------------------------------------------------------
ssCTR64Communication.prototype.AddArgs = function ssCTR64Communication__AddArgs(name, value){
  try {
    this.actionObj.AddArgs(name, value);
  } 
  catch (ex) {
    _logger.error("ssCTR64Communication__AddArgs", ex.message);
  }
}

//-------------------------------------------------------------------------------------
ssCTR64Communication.prototype.LockTransaction = function ssCTR64Communication__LockTransaction(oDOM){
  // TBD: more error checking around this
  return (this.trxObj.Lock(oDOM) != "");
}

//-------------------------------------------------------------------------------------
ssCTR64Communication.prototype.ReleaseTransaction = function ssCTR64Communication__ReleaseTransaction(oDOM){
  // TBD: more error checking around this
  return (this.trxObj.Release(oDOM) == "");
}

//-------------------------------------------------------------------------------------
ssCTR64Communication.prototype.CreateRequest = function ssCTR64Communication__CreateRequest(){
  if (this.actionObj == null) 
    return "";
  try {
    var soapRequest = '<?xml version="1.0" encoding="UTF-8"?>' + "\n";
    // soap envelope
    soapRequest += '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"';
    soapRequest += '  s:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">';
    
    // soap header 
    if (this.trxObj.sessionId != "") {
      // we can try locking into transaction but better if caller does it as they know
      // when to unlock
      
      soapRequest += ' <s:Header>';
      soapRequest += ' <dslf:SessionID xmlns:dslf="http://dslforum-org" s:mustUnderstand="1">';
      soapRequest += this.trxObj.sessionId + '</dslf:SessionID></s:Header>';
      //soapRequest +=   this.trxObj.sessionId2 + '</dslf:SessionID></s:Header>'; tried for Queuing
    }
    
    // soap body
    soapRequest += '<s:Body><u:' + this.actionObj.actionName;
    soapRequest += ' xmlns:u="' + this.actionObj.serviceType + '">';
    // loop through the parameters required for this action
    for (var i = 0, j = this.actionObj.argList.length; i < j; i++) {
      var pName = this.actionObj.argList[i]["name"];
      var pValue = this.actionObj.argList[i]["value"];
      soapRequest += '<' + pName + '>' + pValue + '</' + pName + '>';
    }
    
    // complete the rest of the soap body
    soapRequest += '</u:' + this.actionObj.actionName + '>';
    soapRequest += '</s:Body>';
    
    // end soap envelope
    soapRequest += '</s:Envelope>';
    this.soapRequest = soapRequest;
  } 
  catch (ex) {
    _logger.error("ssCTR64Communication__CreateRequest", ex.message);
  }
  return (this.soapRequest);
}

//-------------------------------------------------------------------------------------
ssCTR64Communication.prototype.SendRequest = function ssCTR64Communication__SendRequest(){
  try {
    if ((typeof this.ssdpObj == "undefined") || (this.ssdpObj == null)) {
      return false;
    }
    if (this.soapRequest == "") { // create soap request and then send
      if (this.CreateRequest() == "") 
        return false;
    }
    
    // open for synchronous operation
    var sLog = "SENT: \n";
    sLog += "-----------------------------------\n";
    var str = "http://" + this.ssdpObj.deviceHost + this.currControlUrl;
    this.httpObj.open("http://" + this.ssdpObj.deviceHost + "/", "", "");
    
    sLog += "POST: " + str + "\n";
    this.httpObj.setRequestHeader("HOST", this.ssdpObj.deviceHost);
    sLog += "HOST: " + this.ssdpObj.deviceHost + "\n";
    this.httpObj.setRequestHeader("CONTENT-TYPE", 'text/xml; charset="utf-8"');
    sLog += "CONTENT-TYPE: " + 'text/xml; charset="utf-8"' + "\n";
    
    // encapsulate action in quotes
    var soapAction = '"' + this.actionObj.serviceType + '#' + this.actionObj.actionName + '"';
    this.httpObj.setRequestHeader("CONTENT-LENGTH", this.soapRequest.length);
    sLog += "CONTENT-LENGTH: " + this.soapRequest.length + "\n";
    this.httpObj.setRequestHeader("SOAPACTION", soapAction);
    sLog += "SOAPACTION: " + soapAction + "\n";
    if (this.authNeeded) {
      this.SetAuthRequestHeader();
    }
    _logger.debug("ssCTR64Communication__SendRequest", sLog + this.soapRequest);
    var responseText ;
    {
        this.httpObj.send("POST", str, this.soapRequest);
        responseText = this.httpObj.responseText;
    }
    sLog = "-----------------------------------\n";
    sLog += "HTTP status: " + this.httpObj.status + "\n";
    sLog += "-----------------------------------\n";
    sLog += "RECEIVED: \n";
    sLog += "-----------------------------------\n";
    
    var bRet = false;
    if (this.httpObj.status == 401) {
      this.soapResponse = this.httpObj.getAllResponseHeaders();
      _logger.debug("ssCTR64Communication__SendRequest", sLog + this.soapResponse);
      this.ParseAuthResponseHeader();
      if (!this.authNeeded) {
        this.authNeeded = true;
        bRet = this.SendRequest(); // resend
      }
      else {
        this.authNeeded = false;
      }
    }
    else {
     // this.soapResponse = this.httpObj.ResponseText;
      this.soapResponse = responseText;
      if (this.httpObj.status != 200) {
        sLog = "SOAPACTION: " + soapAction + "\n" + sLog;
        _logger.error("ssCTR64Communication__SendRequest", sLog + this.soapResponse);
      }
      else {
        _logger.debug("ssCTR64Communication__SendRequest", sLog + this.soapResponse);
        bRet = true;
      }
    }
    
    this.httpObj.close();
    return bRet;
    
  } 
  catch (ex) {
    sLog = "RECEIVED: \n";
    sLog += "-----------------------------------\n";
    _logger.error("ssCTR64Communication__SendRequest", sLog + ex.message);
    return false;
  }
}

//-------------------------------------------------------------------------------------
ssCTR64Communication.prototype.ParseAuthResponseHeader = function ssCTR64Communication__ParseAuthResponseHeader(){
  var index = this.soapResponse.indexOf('realm');
  var elemArr = this.soapResponse.substring(index).split(',');
  for (var i = 0; i < elemArr.length; i++) //Loop thru all the classes
  {
    var elemSubArr = elemArr[i].split('=');
    var item = (trim(elemSubArr[0])).toLowerCase();
    if (item == 'realm') {
      this.realm = elemSubArr[1];
    }
    
    else 
      if (item == 'nonce') {
        this.nonce = elemSubArr[1];
      }
      
      else 
        if (item == 'opaque') {
          this.opaque = elemSubArr[1];
        }
  }
}


//-------------------------------------------------------------------------------------
ssCTR64Communication.prototype.SetAuthRequestHeader = function ssCTR64Communication__SetAuthRequestHeader(){

  this.uri = '"/cpe/control/' + this.actionObj.serviceName + '1/' + this.actionObj.serviceType + '#' + this.actionObj.actionName + '"';
  var authStr = "";
  if (this.actionObj.serviceName == 'LANConfigSecurity') {
    this.username = '"dslf-reset"';
    authStr = 'Digest username=' + this.username + ', realm=' + this.realm + ', nonce=' + this.nonce + ', uri=' + this.uri + ', qop=auth, nc=00000001, cnonce="0a4f113b", response="9170d7f89c6d18b95ab36c0a4432a3dd", opaque=' + this.opaque;
  }
  else {
    this.username = '"dslf-config"';
    authStr = 'Digest username=' + this.username + ', realm=' + this.realm + ', nonce=' + this.nonce + ', uri=' + this.uri + ', qop=auth, nc=00000001, cnonce="0a4f113b", response="155092a5ceb5c45ef85e6962efb965fa", opaque=' + this.opaque;
  }
  //var authStr = 'Digest username=' + this.username + ', realm=' +  this.realm + ', nonce=' + this.nonce + ', uri=' + this.uri + ', opaque=' + this.opaque;
  
  this.httpObj.setRequestHeader("Authorization", authStr);
  _logger.debug("ssCTR64Communication__SetAuthRequestHeader", "Authorization: " + authStr);
}


//-------------------------------------------------------------------------------------
ssCTR64Communication.prototype.ReadResponse = function ssCTR64Communication__ReadResponse(){
  return this.soapResponse;
}

//-------------------------------------------------------------------------------------
ssCTR64Communication.prototype.ParseResponse = function ssCTR64Communication__ParseResponse(){
  if (this.soapResponse == "") 
    return null;
  try {
    var rspObj = new Object();
    rspObj.err = -1;
    
    var xmlDoc = document.createElement("xml");
    var xmlDI = document.body.appendChild(xmlDoc);
    xmlDI.async = false;
    xmlDI.loadXML(this.soapResponse);
    
    // TR64 response Expression as per SPEC
    var expr = "/s:Envelope/s:Body/u:" + this.actionObj.actionName + "Response/*";
    var params = xmlDoc.documentElement.selectNodes(expr);
    if (params == null || params.length == 0) {
      // Hacked for Thomson
      var expr = "/s:Envelope/s:Body/m:" + this.actionObj.actionName + "Response/*";
      params = xmlDoc.documentElement.selectNodes(expr);
    }
    // Set name space
    // rspObj.xmlns = params[0].getAttribute("xmlns:u");
    // set all params
    for (var i = 0; i < params.length; i++) {
      rspObj[params[i].nodeName] = params[i].text;
      /* optimization for later
       str  =  "this.allResponses." + this.actionObj.serviceName + ".";
       str  += params[i].nodeName+"=\""+params[i].text+"\";";
       eval(str);
       */
    }
    document.body.removeChild(xmlDoc);
    rspObj.err = 0;
    rspObj.ActionName = this.actionObj.actionName;
  } 
  catch (ex) {
    //Obviously the node doesnt exist or the xml is screwed up
    if (xmlDoc) 
      document.body.removeChild(xmlDoc);
  }
  this.soapResponse = ""; //clean up
  return rspObj;
}

/***************************************************************************************
 ** Name:         TR64 Class
 **
 ** Purpose:      Encapsulate the TR64 implementation
 **
 ** Parameter:    ssdpObj : Simple Service Discovery protocol object
 **
 ** Return:       nothing
 ***************************************************************************************/
function ssCTR64(ssdpObj){
  this.ssdpObj = ssdpObj;
  this.deviceDOM = null;
  this.serviceTree = "";
  this.serviceNodes = null;
  this.serviceNodeCnt = 0;
  this.scpdDOM = null;
  this.actionTree = "";
  this.actionNodes = null;
  this.currServiceType = null;
  this.currActionName = null;
  this._logger = $ss.agentcore.log.GetDefaultLogger("$ss.snapin.tr64.ssCTR64");   
  this._exception = $ss.agentcore.exceptions;
}

//-------------------------------------------------------------------------------------
ssCTR64.prototype.LoadDeviceSpec = function ssCTR64__LoadDeviceSpec(){
  try {
    var specLocation = "";
    // Check for location to load device spec from
    if (this.ssdpObj.testLocation != null) {
      specLocation = this.ssdpObj.testLocation;
    }
    else {
      specLocation = this.ssdpObj.deviceLocation;
    }
    var oDOM = _smapiutils.g_oSMAPIDependencies.LoadDOM(specLocation)
   // var oDOM = $ss.snapin.modem.tr64.imp._LoadDOM(specLocation);
    if (oDOM != null) {
      this.deviceDOM = oDOM;
      return true;
    }
    else {
      return false;
    }
  } 
  catch (ex) {
    _logger.error("ssCTR64__LoadDeviceSpec", ex.message);
    return false;
  }
}

//-------------------------------------------------------------------------------------
ssCTR64.prototype.GetActionsInService = function ssCTR64__GetActionsInService(svcType){
  this.currServiceType = svcType;
  var scpdPath = "";
  if (this.ssdpObj.testLocation == null) {
    // in real scenario we need real service descriptor documents
    var expr = "//service[serviceType='" + svcType + "']/SCPDURL";
    var oNode = this.deviceDOM.selectSingleNode(expr);
    if (oNode != null) {
      scpdPath = "http://" + this.ssdpObj.deviceHost + oNode.text;
    }
  }
  else {
    // test scenario use static files
    scpdPath = "../tr_64/scpd/" + $ss.snapin.modem.tr64.imp._GetServiceFromType(this.currServiceType) + ".xml";
  }
  var oDOM = $ss.snapin.modem.tr64.imp._LoadDOM(scpdPath);
  this.actionNodes = null;
  if (oDOM != null) {
    this.scpdDOM = oDOM;
    this.actionNodes = oDOM.selectNodes("//actionList/action");
  }
  return this.actionNodes;
}

//-------------------------------------------------------------------------------------
ssCTR64.prototype.GetActionsTree = function ssCTR64__GetActionsTree(){
  this.actionTree = "";
  if (this.actionNodes != null) {
    // others 
    this.actionTree = '<TABLE id="tb_ActionList" class=clsTable1 cellpadding=0 cellspacing=0 border=0>';
    this.actionTree += '<TR><TD class="clsTableSubHeader">Supported Actions</TD></TR>';
    // write the table cells
    for (var ndx = 0; ndx < this.actionNodes.length; ndx++) {
      action = this.actionNodes[ndx].selectSingleNode("./name");
      this.actionTree += this.pvt_ShowActionLink(action.text, ndx);
    }
    this.actionTree += '</TABLE>';
  }
  return this.actionTree;
}

//-------------------------------------------------------------------------------------
ssCTR64.prototype.GetServiceTree = function ssCTR64__GetServiceTree(){
  if (this.serviceTree == "") {
    this.serviceNodes = new Object();
    this.serviceNodeCnt = 0;
    this.serviceTree = this.pvt_TraverseDOM(this.deviceDOM.documentElement);
  }
  return this.serviceTree;
}

//-------------------------------------------------------------------------------------
ssCTR64.prototype.pvt_TraverseDOM = function ssCTR64__pvt_TraverseDOM(tree){
  if (tree.hasChildNodes()) {
    var nodes = tree.childNodes.length;
    strHTML = this.pvt_OpenDiv(tree, this.serviceNodeCnt++);
    for (var i = 0; i < tree.childNodes.length; i++) 
      strHTML += this.pvt_TraverseDOM(tree.childNodes(i));
    strHTML += this.pvt_CloseDiv(tree);
  }
  else {
    strHTML = this.pvt_ShowLink(tree);
  }
  return strHTML;
}

//-------------------------------------------------------------------------------------
ssCTR64.prototype.pvt_OpenDiv = function ssCTR64__pvt_OpenDiv(tree, divNo){
  clsBranch = (g_TreeLevel <= MAX_UITREE_LEVEL) ? "clsNavBranchShow" : "clsNavBranchHide";
  clsImage = (g_TreeLevel <= MAX_UITREE_LEVEL) ? "clsImageBranchShow" : "clsImageBranchHide";
  
  var name = tree.tagName.toLowerCase();
  if ((!$ss.snapin.modem.tr64.imp._IsDevice(name)) && (!$ss.snapin.modem.tr64.imp._IsURL(name)) && (!$ss.snapin.modem.tr64.imp._IsService(name))) 
    return "";
  g_TreeLevel++;
  // store each node for further use
  this.serviceNodes[XML_NODE + divNo] = tree;
  
  var strHTML = '<div class="clsNode" noWrap>';
  strHTML += '<span class="' + clsImage + '" onClick="$ss.snapin.modem.tr64.imp._ShowBranch(' + divNo + ');" id="' + XML_FOLDER + divNo + '" border="0"></span>';
  strHTML += this.pvt_ShowServiceLink(tree);
  strHTML += '<div class="' + clsBranch + '" id="' + XML_BRANCH + divNo + '">';
  return strHTML;
}

//-------------------------------------------------------------------------------------
ssCTR64.prototype.pvt_CloseDiv = function ssCTR64__pvt_CloseDiv(tree){
  var name = tree.tagName.toLowerCase();
  if ($ss.snapin.modem.tr64.imp._IsDevice(name) || $ss.snapin.modem.tr64.imp._IsURL(name) || $ss.snapin.modem.tr64.imp._IsService(name)) {
    g_TreeLevel--;
    return "<\/div><\/div>";
  }
  else {
    return "";
  }
}

//-------------------------------------------------------------------------------------
ssCTR64.prototype.pvt_ShowLink = function ssCTR64__pvt_ShowLink(tree){
  var name = tree.parentNode.tagName.toLowerCase();
  if ((!$ss.snapin.modem.tr64.imp._IsDevice(name)) && (!$ss.snapin.modem.tr64.imp._IsURL(name)) && (!$ss.snapin.modem.tr64.imp._IsService(name))) 
    return "";
  
  var strHTML = '<span>';
  if (name == "scpdurl") {
    var str = "http://" + this.ssdpObj.deviceHost + tree.text;
    strHTML = '<a href=# onClick="_UIutils.OpenWindow(\'' + str + '\')">';
  }
  strHTML += tree.text;
  if (name == "scpdurl") {
    strHTML += '</a>';
  }
  strHTML += '</span>';
  return strHTML;
}

//-------------------------------------------------------------------------------------
ssCTR64.prototype.pvt_ShowServiceLink = function ssCTR64__pvt_ShowServiceLink(tree){
  var strHTML = '<b>' + tree.tagName + ': <\/b>';
  if (tree.tagName == "service") {
    var strText = tree.selectSingleNode("./serviceType").text;
    var svcName = $ss.snapin.modem.tr64.imp._GetServiceFromType(strText);
    strHTML += ' <a href=# onclick="svcClicked(\'' + strText + '\')">' + svcName + '</a>';
  }
  else 
    if (tree.tagName == "device") {
      var strText = tree.selectSingleNode("./friendlyName").text;
      strHTML += strText;
    }
  return strHTML;
}

//-------------------------------------------------------------------------------------
ssCTR64.prototype.pvt_ShowActionLink = function ssCTR64__pvt_ShowActionLink(text, ndx){
  var strHTML = '<TR><TD class="clsTableRow1">';
  strHTML += '<a href=# onclick="actionClicked(\'' + text + '\', ' + ndx + ')">';
  strHTML += '<b>' + text + '<\/b><\/a>';
  strHTML += '<\/TD><\/TR>';
  
  return strHTML;
}



