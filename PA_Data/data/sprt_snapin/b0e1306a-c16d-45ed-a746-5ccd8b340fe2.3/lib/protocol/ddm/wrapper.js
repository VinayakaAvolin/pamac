/***************************************************************************************
**
**  File Name: ddm.wrapper.js
**
**  Summary: SMAPI derived class for DDM protocol support
**
**  Description: This javascript contains SMAPI derivative to support DDM protocol
**               implementation. It maps SMAPI functions to DDM specific calls.
**           
**  Copyright SupportSoft Inc. 2005, All rights reserved.
***************************************************************************************/
$ss.snapin = $ss.snapin ||
{};
$ss.snapin.modem = $ss.snapin.modem||
{};
$ss.snapin.modem.ddm = $ss.snapin.modem.ddm ||
{};
$ss.snapin.modem.ddm.wrapper = $ss.snapin.modem.ddm.wrapper ||
{};
 $ss.snapin.modem.ddm.wrapper = Class.extend('', {
},{// prototype methods
	/***************************************************************************************
	** Name:         _ddm
	**
	** Purpose:      Class that wraps SMAPI functionality and provides DDM protocol 
	**               implementation.
	**
	** Parameter:    none
	**
	** Return:       true if we have sync
	***************************************************************************************/
	init : function (oSMAPI)
	{
	  this.oSMAPI         = oSMAPI;
	  this.mProperties    = oSMAPI.GetInstanceProperties();  
	  this.m_sUserName    = "";
	  this.m_sUserPass    = ""; 
	},
	
	/***************************************************************************************
	** Name:         _ddm_ID
	**
	** Purpose:      Determine the unique identifier for the modem. This implementation is 
	**               helpful at protocol level so that SMAPI can use it to infer modem 
	**               specific implementations.
	**
	** Parameter:    none
	**
	** Return:       unique identifier of the modem matching modem xml file
	***************************************************************************************/
	ID : function ()
	{
	  var id = "";
	  try {
	    var obj = new Object();
	    _ddmMethods.SingleStatus(obj, "cpe_info", this.mProperties);
	    if((_smapiMethods.GetLastError() == _constants.SMAPI_SUCCESS) && (obj.error_no == 0)){
	      id = obj.cpe_model;
	    }  
	  } catch (ex) {
	    _logger.error("_ddm_ID", ex.message); 
	    this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
	  }
	  return id; 
	},
	
	/*******************************************************************************
	** Name:         _ddm_Name
	**
	** Purpose:      Gets the modem name as defined in the modem.xml
	**
	** Parameter:    none
	**
	** Return:       str
	*******************************************************************************/
	Name : function () 
	{
	  try {
	    return this.mProperties.name;
	  } catch(ex) {
	    _logger.error("_ddm_Name", ex.message );
	    this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
	  }
	},
	
	/*******************************************************************************
	** Name:         _ddm_SerialNumber
	**
	** Purpose:      Get Serial Number for ddm compliant modems.
	**
	** Parameter:    none
	**
	** Return:       string having Serial Number else empty
	*******************************************************************************/
	SerialNumber : function ()
	{
	  var sRet = _constants.SMAPI_EMPTY_STR;
	  try {
	    var xmlDOM = _ddmMethods.ExecuteCommand("cpe_info", this.mProperties);
	    var expr   = "//serial_number";
	    if(xmlDOM != null && _smapiMethods.GetLastError() == _constants.SMAPI_SUCCESS) {
	      var oNode = xmlDOM.selectSingleNode(expr);
	      sRet = oNode.text;
	    }  
	  } catch (ex) {
	    _logger.error("_ddm_SerialNumber", ex.message );
	    _smapiMethods.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
	  }
	  return sRet;
	},
	
	/*******************************************************************************
	** Name:         _ddm_MACAddress
	**
	** Purpose:      Get MACAddress for ddm compliant modems.
	**
	** Parameter:    none
	**
	** Return:       string having MACAddress else empty
	*******************************************************************************/
	MACAddress : function ()
	{
	  var sRet = _constants.SMAPI_EMPTY_STR;
	  try {
	    var xmlDOM = _ddmMethods.ExecuteCommand("cpe_configuration", this.mProperties);
	    var expr   = "//lan_client_details[lan_client_ip_lease=\"Leased, Active\"]/lan_client_mac_addr";
	    if(xmlDOM != null && _smapiMethods.GetLastError() == _constants.SMAPI_SUCCESS) {
	      var oNode = xmlDOM.selectSingleNode(expr);
	      sRet = oNode.text;
	    }  
	  } catch (ex) {
	    _logger.error("_ddm_MACAddress", ex.message );
	    _smapiMethods.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
	  }
	  return sRet;
	},
	
	/*******************************************************************************
	** Name:         _ddm_Firmware
	**
	** Purpose:      Get firmware version Thomson ST716G modem.
	**
	** Parameter:    none
	**
	** Return:       string having firmware else empty
	*******************************************************************************/
	Firmware : function ()
	{
	  var sRet = _constants.SMAPI_EMPTY_STR;
	  try {
	    var xmlDOM = _ddmMethods.ExecuteCommand("cpe_info", this.mProperties);
	    var expr   = "//software_ver";
	    if(xmlDOM != null && _smapiMethods.GetLastError() == _constants.SMAPI_SUCCESS) {
	      var oNode = xmlDOM.selectSingleNode(expr);
	      sRet = oNode.text;
	    }  
	  } catch (ex) {
	    _logger.error("_ddm_Firmware", ex.message );
	    _smapiMethods.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
	  }
	  return sRet;
	},
	
	/***************************************************************************************
	** Name:         _ddm_Detect
	**
	** Purpose:      Verifies that a DDM compliant modem is connected to the PC. Detection 
	**               could fail if PC to modem connection is broken or modem connected is not
	**               DDM compliant
	**
	** Parameter:    none
	**
	** Return:       true if detected.
	***************************************************************************************/
	Detect : function ()
	{ 
	  var nRet = 0;
	  try {
	    var obj = new Object();
	    _ddmMethods.SingleStatus(obj, "page_info", this.mProperties);
	    nRet = ((_smapiMethods.GetLastError() == _constants.SMAPI_SUCCESS) && (obj.error_no == 0)) ? 1 : 0;
	  } catch (ex) {
	    _logger.error("_ddm_Detect", ex.message );
	    _smapiMethods.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
	  }
	  return nRet;
	},
	
	/***************************************************************************************
	** Name:         _ddm_IsSync
	**
	** Purpose:      Determine if modem is in sync. This is a required procedure to 
	**               check modem to DSLAM connection.
	**
	** Parameter:    none
	**
	** Return:       true if we have sync
	***************************************************************************************/
	IsSync : function () 
	{
	  var nRet = 0;
	  try {
	    var obj = new Object();
	    _ddmMethods.SingleStatus(obj, "get_adsl_status", this.mProperties);
	    if((_smapiMethods.GetLastError() == _constants.SMAPI_SUCCESS) && (obj.error_no == 0)) {
	      nRet = (obj.sync_status == "Sync") ? 1 : 0;
	    }  
	  } catch (ex) {
	    _logger.error("_ddm_IsSync", ex.message );
	    _smapiMethods.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
	  }
	  return nRet;
	},
	
	/***************************************************************************************
	** Name:         _ddm_IsConnected
	**
	** Purpose:      Determine if modem ppp status is "connected".
	**
	** Parameter:    (none)
	**
	** Return:       true/false
	***************************************************************************************/
	IsConnected : function () 
	{
	  var nRet = 0;
	  try {
	    var obj = new Object();
	    _ddmMethods.SingleStatus(obj, "get_ppp_status", this.mProperties);
	    if((_smapiMethods.GetLastError() == _constants.SMAPI_SUCCESS) && (obj.error_no == 0)) {
	      nRet = (obj.pppoe_detect_status == "Connected") ? 1 : 0;
	    }    
	  } catch (ex) {
	    _logger.error("_ddm_IsConnected", ex.message );
	    _smapiMethods.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
	  }
	  return nRet;
	},
	
	/***************************************************************************************
	** Name:         _ddm_SetUserCreds
	** 
	** Purpose:      Sets PPPoE creds on the modem. DDM modems require reboot hence we do
	**               one here as per SMAPI spec.
	**
	** Parameter:    sName     :  User name
	**               sPassword :  User passowrd 
	**
	** Return:       none; check GetLastError
	***************************************************************************************/
	SetUserCreds : function (sName, sPassword)
	{
	  try {
	    var obj = _ddmMethods.SetPPPUserInfo(sName, sPassword, this.mProperties);
	    if((_smapiMethods.GetLastError() == _constants.SMAPI_SUCCESS) && (obj.error_no == 0)) {
	      this.m_sUserName = sName;
	      this.m_sUserPass = sPassword;
	      this.Reboot();
	    }   
	  } catch (ex) {
	    _logger.error("_ddm_SetUserCreds", ex.message );
	    _smapiMethods.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
	  }
	},
	
	/***************************************************************************************
	** Name:         _ddm_pvt_pppuser
	**
	** Purpose:      Determine if modem ppp status is "connected".
	**
	** Parameter:    (none)
	**
	** Return:       true/false
	***************************************************************************************/
	_pppuser : function () 
	{
	  var sRet = "";
	  try {
	    var obj = new Object();
	    _ddmMethods.SingleStatus(obj, "ppp_user_info", this.mProperties);
	    if((_smapiMethods.GetLastError() == _constants.SMAPI_SUCCESS) && (obj.error_no == 0)) {
	      sRet = obj.ppp_userid;
	    }    
	  } catch (ex) {
	    _logger.error("_ddm_pvt_pppuser", ex.message );
	    _smapiMethods.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
	  }
	  return sRet;
	},
	
	/***************************************************************************************
	** Name:         _ddm_Reboot
	**
	** Purpose:      Reboots modem
	**
	** Parameter:    none
	**
	** Return:       GetLastError to confirm success 
	***************************************************************************************/
	Reboot : function ()
	{
	  try {
	    //DDM modems need user name to drop/make connection so make sure we get one.
	    if(this.m_sUserName=="") {
	      this.m_sUserName=this._pppuser();
	    }
	    var obj = _ddmMethods.DropConn(this.m_sUserName, this.mProperties);
	    if((_smapiMethods.GetLastError() == _constants.SMAPI_SUCCESS) && (obj.error_no == 0)) {
	      _utils.Sleep(2000);  // wait 2 sec before reconnecting
	      obj = _ddmMethods.MakeConn(this.m_sUserName, this.mProperties);
	    }
	  } catch (ex) {
	    _logger.error("_ddm_Reboot", ex.message );
	    _smapiMethods.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
	  }
	},
	
	/***************************************************************************************
	** Name:         _ddm_GetDSLLinkInfo
	**
	** Purpose:      Provides bunch of DSL link parameters in one shot. Modem implementations 
	**               can choose to fill all required output parameters or default to -1 if
	**               a particular param is not implemented.
	**
	** Parameter:    none
	**
	** Return:       Object having the following properties
	**               NewUpstreamCurrentRate
	**               NewDownstreamCurrentRate
	**               NewUpstreamNoiseMargin
	**               NewDownstreamNoiseMargin
	**               NewUpstreamPower
	**               NewDownstreamPower
	**               NewUpstreamAttenuation
	**               NewDownstreamAttenuation 
	***************************************************************************************/
	GetDSLLinkInfo : function () 
	{
	  try {
	    var obj = new Object();
	    _ddmMethods.SingleStatus(obj, "get_adsl_status", this.mProperties);
	    if((_smapiMethods.GetLastError() == _constants.SMAPI_SUCCESS) && (obj.error_no == 0)){
	      var newObj = new Object();
	      newObj.NewUpstreamCurrentRate   = obj.upstream_rate;
	      newObj.NewDownstreamCurrentRate = obj.downstream_rate;
	      newObj.NewUpstreamNoiseMargin   = obj.upstream_margin;
	      newObj.NewDownstreamNoiseMargin = obj.downstream_margin;
	      newObj.NewUpstreamPower         = -1;
	      newObj.NewDownstreamPower       = -1;
	      newObj.NewUpstreamAttenuation   = -1;
	      newObj.NewDownstreamAttenuation = -1;
	      return newObj;
	    }   
	  } catch(ex) {
	    _logger.error("_ddm_GetDSLLinkInfo", ex.message );
	    _smapiMethods.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
	  }
	  return null;
	}
});

  var _logger = $ss.agentcore.log.GetDefaultLogger("_ddm");
  var _utils = $ss.agentcore.utils;
  var _netCheck = $ss.agentcore.network.netcheck;
  var _registry = $ss.agentcore.dal.registry;
  var _config = $ss.agentcore.dal.config;
  var _smapiMethods = $ss.agentcore.smapi.methods;
  var _constants = $ss.agentcore.constants;
  var _ddmMethods = $ss.snapin.modem.ddm.imp;
