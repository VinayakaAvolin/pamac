/***************************************************************************************
**
**  File Name: ddm.imp.js
**
**  Summary: Common APIs for DDM protocol support
**
**  Description: This javascript contains general purpose functions for
**               DDM protocol stack implementation
**           
**  Copyright SupportSoft Inc. 2005, All rights reserved.
***************************************************************************************/

/***************************************************************************************
<page_info>
<get_wan_ip_address_assignment_info>http://192.168.1.1/verizon/get_wan_ip_info.xml</get_wan_ip_address_assignment_info>
<get_admin_info>http://192.168.1.1/verizon/get_admin_info.xml</get_admin_info>
<set_admin_info>http://192.168.1.1/verizon/set_admin_info.xml</set_admin_info>
<cpe_info>http://192.168.1.1/verizon/cpe_info.xml</cpe_info>
<cpe_configuration>http://192.168.1.1/verizon/cpe_configuration.xml</cpe_configuration>
<set_cpe_ip_configuration>http://192.168.1.1/verizon/set_cpe_ip_info.xml</set_cpe_ip_configuration>
<ppp_user>http://192.168.1.1/verizon/ppp_user_info.xml</ppp_user>
<set_ppp_user_info>http://192.168.1.1/verizon/set_ppp_user_info.xml</set_ppp_user_info>
<drop_conn>http://192.168.1.1/verizon/drop_conn.xml</drop_conn>
<make_conn>http://192.168.1.1/verizon/make_conn.xml</make_conn>
<get_dsl_sync_status>http://192.168.1.1/verizon/get_adsl_status.xml</get_dsl_sync_status>
<get_pppoe_status>http://192.168.1.1/verizon/get_ppp_status.xml</get_pppoe_status>
<error_no>0</error_no>
<error_desc>No Error</error_desc>
</page_info>
***************************************************************************************/

$ss.snapin = $ss.snapin ||
{};
$ss.snapin.modem = $ss.snapin.modem||
{};
$ss.snapin.modem.ddm = $ss.snapin.modem.ddm ||
{};

/** @namespace Holds all ddm implementation related functionality*/
$ss.snapin.modem.ddm.imp = $ss.snapin.modem.ddm.imp || {};

(function()
{
 $.extend($ss.snapin.modem.ddm.imp,
  {   
	/***************************************************************************************
	**                      DDM Test Harness Functions                                    **
	***************************************************************************************/
	
	//-------------------------------------------------------------------------------------
	DisplayResult : function (xml)
	{
	  if (document.all.display != null) 
	    document.all.display.innerHTML = "<xmp>"+xml+"</xmp>";
	},
	
	//-------------------------------------------------------------------------------------
	AddResult : function (xml)
	{
	  if (document.all.display != null) 
	    document.all.display.innerHTML += "<xmp>"+xml+"</xmp>";
	},
	
	
	/***************************************************************************************
	**                      DDM Helper Functions                                          **
	***************************************************************************************/
	
	/***************************************************************************************
	** Name:         SingleStatus
	**
	** Purpose:      SingleStatus of a specific property
	**
	** Parameter:    obj, node, modemProps
	**
	** Return:       status
	***************************************************************************************/
	GetPage : function(p, modemProps)
	{
	  var sData = "";
	  try {
	    if(_oPWCtl == null)
	      _oPWCtl = _utils.CreateActiveXObject("sdcuser.tgpassctl", true);
	    _oPWCtl.open("http://"+modemProps.ip+"/", modemProps.modemuser, modemProps.modempwd);
	    _oPWCtl.setRequestHeader("Authorization", "Basic " + _utils.EncodeBase64(modemProps.modemuser + ":" + modemProps.modempwd));
	    rVal = _oPWCtl.send("GET", p, "");    //TBD check rVal
	    if(rVal && _oPWCtl.status == 200) {
	      _utils.Sleep(200); 
	      sData = _oPWCtl.responseText;    
	    } else if (_oPWCtl.status == 401) {
	      // This can only happen in failed authentication case
	      _smapiMethods.SetLastError(_constants.SMAPI_NO_AUTH);
	    } 
	    _oPWCtl.close();
	  } catch (ex) {
	    _logger.error("GetPage", ex.message + "Page: " + p);
	    _smapiMethods.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
	  }
	  return sData;
	},
	
	/***************************************************************************************
	** Name:         ProcessPage
	**
	** Purpose:      ProcessPage 
	**
	** Parameter:    p, obj, modemProps
	**
	** Return:       none
	***************************************************************************************/
	ProcessPage : function (p, obj, modemProps)
	{
	  var xml = _ddmMethods.GetPage(p, modemProps);
	  _ddmMethods.AddResult(xml);
	  if (xml=="") return;
	
	  tDOM = new ActiveXObject("Microsoft.XMLDOM");
	  tDOM.loadXML(xml);
	  _logger.debug("ProcessPage", xml);
	  if(tDOM.documentElement != null) {
	    var eNodeList = new Enumerator(tDOM.documentElement.childNodes);
	    for (; eNodeList.atEnd() == false; eNodeList.moveNext ())
	    {
	      str = "obj."+eNodeList.item().nodeName+"=\""+eNodeList.item().text+"\";";
	      eval(str);
	    }
	  } else {
	    _logger.error("ProcessPage", "Error in loading DOM.");
	    _smapiMethods.SetLastError(_constants.SMAPI_DERIVED_FAIL);
	  }    
	},
	
	/***************************************************************************************
	** Name:         SingleStatus
	**
	** Purpose:      SingleStatus of a specific property
	**
	** Parameter:    obj, node, modemProps
	**
	** Return:       status
	***************************************************************************************/
	SingleStatus : function (obj, node, modemProps)
	{
	  _ddmMethods.DisplayResult("");  
	  var p = "http://"+modemProps.ip+"/"+ modemProps.modemvirtual + "/"+node+".xml";    
	  _ddmMethods.ProcessPage(p, obj, modemProps);
	},
	
	/***************************************************************************************
	** Name:         ExecuteCommand
	**
	** Purpose:      SingleStatus version but changed to return xml DOM instead
	**
	** Parameter:    sPage, modemProps
	**
	** Return:       xml DOM
	***************************************************************************************/
	ExecuteCommand : function (sPage, modemProps)
	{
	  var xmlRaw = "";
	  var xmlDOM = null;
	  try {
	    _ddmMethods.DisplayResult("");  
	    var sUrl = "http://"+modemProps.ip+"/"+ modemProps.modemvirtual + "/"+sPage+".xml";    
	    xmlRaw = _ddmMethods.GetPage(sUrl, modemProps);
	    _ddmMethods.AddResult(xmlRaw);
	    if(xmlRaw != "") {
	      //load into DOM
	      xmlDOM = new ActiveXObject("Microsoft.XMLDOM");
	      xmlDOM.loadXML(xmlRaw);
	      if(xmlDOM.documentElement != null) {
	        // then its a well formed xml hence check for errors
	        var oNode = xmlDOM.selectSingleNode("//error_no");
	        if(oNode && oNode.text == "0") {
	          // success
	          return xmlDOM;
	        } else {
	          // error within xml
	          _logger.warning("ExecuteCommand", "Returned error_no: " + oNode.text );
	          _smapiMethods.SetLastError(_constants.SMAPI_DERIVED_FAIL);    
	        }  
	          
	      } else {
	        // invalid xml
	        _logger.error("ExecuteCommand", "Received erroneous xml");
	        _smapiMethods.SetLastError(_constants.SMAPI_DERIVED_FAIL);
	      }     
	    }
	  } catch (ex) {
	    _logger.error("ExecuteCommand", ex.message + "Page: " + sPage);
	    _smapiMethods.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
	  }
	  return xmlDOM;
	},
	
	/***************************************************************************************
	** Name:         PostDDM
	**
	** Purpose:      Post DDM data to server
	**
	** Parameter:    n, xml, modemProps
	**
	** Return:       object or null
	***************************************************************************************/
	PostDDM : function (n, xml, modemProps)
	{
	  _ddmMethods.DisplayResult(xml);
	  _logger.debug("PostDDM", xml);
	  var sPostUrl = "http://"+modemProps.ip+"/"+ modemProps.modemvirtual + "/"+n+".xml" 
	  _ddmMethods.AddResult(sPostUrl);
	  
	  var oRet = {error_no: -1};
	  var obj  = _http.HttpPostUsingPasswordControl(xml, sPostUrl, 0, modemProps.modemuser, modemProps.modempwd);
	  if (!obj.bSuccess) {
	    if (obj.responseText != "") DisplayResult(obj.responseText);
	  } else {
	    //function succeeded so lets check httpStatus
	    if(obj.httpStatus == 200) {
	      tDOM = new ActiveXObject("Microsoft.XMLDOM");
	      tDOM.loadXML(obj.responseText);
	      if(tDOM.documentElement != null) {
	        var eNodeList = new Enumerator(tDOM.documentElement.childNodes);
	        for (; eNodeList.atEnd() == false; eNodeList.moveNext()) {
	          oRet[eNodeList.item().nodeName] = eNodeList.item().text;
	        }
	      } else {
	        _logger.error("PostDDM", "Error in loading DOM.");
	        _smapiMethods.SetLastError(_constants.SMAPI_DERIVED_FAIL);
	      }   
	    } else if (obj.httpStatus == 401) {
	      // This can only happen in failed authentication case
	      _smapiMethods.SetLastError(_constants.SMAPI_NO_AUTH);
	    } else {
	      _smapiMethods.SetLastError(_constants.SMAPI_FAIL_MODEM_ACCESS);
	    }
	  }
	  return oRet;
	},
	
	/***************************************************************************************
	** Name:         SetPPPUserInfo
	**
	** Purpose:      Set PPPoE creds on the modem
	**
	** Parameter:    user, password, modemProps
	**
	** Return:       status as str
	***************************************************************************************/
	SetPPPUserInfo : function(user, password, modemProps)
	{
	  var xml = "<set_ppp_user_info><ppp_userid>"+user+"</ppp_userid><ppp_passwd>"+password+"</ppp_passwd></set_ppp_user_info>";
	  return _ddmMethods.PostDDM("set_ppp_user_info", xml, modemProps);
	},
	
	/***************************************************************************************
	** Name:         SetAdminInfo
	**
	** Purpose:      Set Admin Info 
	**
	** Parameter:    user, password, newpassword, modemProps
	**
	** Return:       object or null
	***************************************************************************************/
	SetAdminInfo : function (user, password, newpassword, modemProps)
	{
	  modemProps.modemuser = user;
	  modemProps.modempwd = password;
	
	  var xml = "<set_admin_info><adminid>"+user+"</adminid><old_adminpwd>"+password+"</old_adminpwd><new_adminpwd>"+newpassword+"</new_adminpwd></set_admin_info>";
	  obj = _ddmMethods.PostDDM("set_admin_info", xml, modemProps);
	  if (obj.error_no == 0) {
	    gAdminPass = newpassword;
	  }
	  return obj;
	},
	
	/***************************************************************************************
	** Name:         DropConn
	**
	** Purpose:      Drop Connection
	**
	** Parameter:    user, modemProps
	**
	** Return:       status as str
	***************************************************************************************/
	DropConn : function (user, modemProps)
	{
	  var xml = "<drop_conn><ppp_userid>"+user+"</ppp_userid></drop_conn>";
	  return _ddmMethods.PostDDM("drop_conn", xml, modemProps);
	},
	
	/***************************************************************************************
	** Name:         MakeConn
	**
	** Purpose:      Establish Connection
	**
	** Parameter:    user, modemProps
	**
	** Return:       status as str
	***************************************************************************************/
	MakeConn : function (user, modemProps)
	{
	  var xml = "<make_conn><ppp_userid>"+user+"</ppp_userid></make_conn>";
	  return _ddmMethods.PostDDM("make_conn", xml, modemProps);
	}
});	
	var _oPWCtl = null;            // Tracks password control usage throughout this file
  var _logger = $ss.agentcore.log.GetDefaultLogger("$ss.snapin.modem.ddm.imp");
  var _utils = $ss.agentcore.utils;
  var _netCheck = $ss.agentcore.network.netcheck;
	var _config = $ss.agentcore.dal.config;
	var _http = $ss.agentcore.dal.http;
	var _constants = $ss.agentcore.constants;
	var _smapiMethods = $ss.agentcore.smapi.methods;
	var _ddmMethods = $ss.snapin.modem.ddm.imp;
})();