/***************************************************************************************
**
**  File Name: smapi_wrapper.js
**
**  Summary: SMAPI derived class for Null Modem protocol support
**
**  Description: This javascript contains SMAPI derivative to support a successfull
**               path for SMAPI in cases when no modem support is built in.
**           
**  Copyright SupportSoft Inc. 2006, All rights reserved.
***************************************************************************************/

/***************************************************************************************
** Name:         _null_modem
**
** Purpose:      Class that wraps SMAPI functionality and provides NULL 
**               implementation.
**
** Parameter:    none
**
** Return:       true if we have sync
***************************************************************************************/

$ss.snapin = $ss.snapin ||
{};
$ss.snapin.modem = $ss.snapin.modem||
{};
$ss.snapin.modem.nullmodem = $ss.snapin.modem.nullmodem ||
{};
$ss.snapin.modem.nullmodem.wrapper = $ss.snapin.modem.nullmodem.wrapper ||
{};
$ss.snapin.modem.nullmodem.wrapper = Class.extend('', {
},{// prototype methods
  init: function (oSMAPI)
  {
    this.oSMAPI         = oSMAPI;
    this.mProperties    = oSMAPI.GetInstanceProperties();  
    this.m_sUserName    = "";
    this.m_sUserPass    = ""; 
  },

  /***************************************************************************************
  ** Name:         _null_modem_ID
  **
  ** Purpose:      Determine the unique identifier for the modem. This implementation is 
  **               helpful at protocol level so that SMAPI can use it to infer modem 
  **               specific implementations.
  **
  ** Parameter:    none
  **
  ** Return:       unique identifier of the modem matching modem xml file
  ***************************************************************************************/
  ID : function ()
  {
    var id = "";
    try {
      id = this.mProperties.id;  
    } catch (ex) {
      _logger.error("_null_modem_ID", ex.message ); 
      sdcDSLModem.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
    }
    return id; 
  },

  /*******************************************************************************
  ** Name:         _null_modem_Name
  **
  ** Purpose:      Gets the modem name as defined in the modem.xml
  **
  ** Parameter:    none
  **
  ** Return:       str
  *******************************************************************************/
  Name : function () 
  {
    try {
      return this.mProperties.name;
    } catch(ex) {
      _logger.error("_null_modem_Name", ex.message );
      this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
    }
  },

  /*******************************************************************************
  ** Name:         _null_modem_SerialNumber
  **
  ** Purpose:      Get Serial Number for ddm compliant modems.
  **
  ** Parameter:    none
  **
  ** Return:       string having Serial Number else empty
  *******************************************************************************/
  SerialNumber : function ()
  {
    return "NM011773";
  },

  /*******************************************************************************
  ** Name:         _null_modem_MACAddress
  **
  ** Purpose:      Get MACAddress for ddm compliant modems.
  **
  ** Parameter:    none
  **
  ** Return:       string having MACAddress else empty
  *******************************************************************************/
  MACAddress : function ()
  {
    return "00-00-00-00-00-00";
  },

  /*******************************************************************************
  ** Name:         _null_modem_Firmware
  **
  ** Purpose:      Get firmware version Thomson ST716G modem.
  **
  ** Parameter:    none
  **
  ** Return:       string having firmware else empty
  *******************************************************************************/
  Firmware : function ()
  {
    _smapiutils.g_oSMAPIDependencies.Sleep(1500); 
    return "01.17.73";
  },

  /***************************************************************************************
  ** Name:         _null_modem_Detect
  **
  ** Purpose:      Verifies that a no modem is connected to PC but you have internet 
  **               access.
  **
  ** Parameter:    none
  **
  ** Return:       true if detected.
  ***************************************************************************************/
  Detect : function ()
  { 
    var nRet = 0;
    try {
      var sites = _config.GetValues("modems", "internet_pingsite");
      for (var i=0 ; i < sites.length; i++) {
        _smapiutils.g_oSMAPIDependencies.Sleep(1500); 
        if (_netCheck.Ping(sites[i])) return 1;
        _smapiutils.g_oSMAPIDependencies.Sleep(1500); 
      }
    } catch (ex) {
      _logger.error("_null_modem_Detect", ex.message );
      sdcDSLModem.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
    }
    return nRet;
  },

  /***************************************************************************************
  ** Name:         _null_modem_IsSync
  **
  ** Purpose:      Determine if modem is in sync. This is a required procedure to 
  **               check modem to DSLAM connection.
  **
  ** Parameter:    none
  **
  ** Return:       true if we have sync
  ***************************************************************************************/
  IsSync : function () 
  {
    _smapiutils.g_oSMAPIDependencies.Sleep(2000); 
    return 1;
  },

  /***************************************************************************************
  ** Name:         _null_modem_IsConnected
  **
  ** Purpose:      Determine if modem ppp status is "connected".
  **
  ** Parameter:    (none)
  **
  ** Return:       true/false
  ***************************************************************************************/
  IsConnected : function () 
  {
    _smapiutils.g_oSMAPIDependencies.Sleep(2000);
    return 1;
  },

  /***************************************************************************************
  ** Name:         _null_modem_SetUserCreds
  ** 
  ** Purpose:      Sets PPPoE creds on the modem. 
  **
  ** Parameter:    sName     :  User name
  **               sPassword :  User passowrd 
  **
  ** Return:       none; check GetLastError
  ***************************************************************************************/
  SetUserCreds : function (sName, sPassword)
  {
    _smapiutils.g_oSMAPIDependencies.Sleep(2000); //do nothing
  },

  /***************************************************************************************
  ** Name:         _null_modem_Reboot
  **
  ** Purpose:      Reboots modem
  **
  ** Parameter:    none
  **
  ** Return:       GetLastError to confirm success 
  ***************************************************************************************/
  Reboot : function ()
  {
    _smapiutils.g_oSMAPIDependencies.Sleep(2000);
  },

  /***************************************************************************************
  ** Name:         _null_modem_GetDSLLinkInfo
  **
  ** Purpose:      Provides bunch of DSL link parameters in one shot. Modem implementations 
  **               can choose to fill all required output parameters or default to -1 if
  **               a particular param is not implemented.
  **
  ** Parameter:    none
  **
  ** Return:       Object having the following properties
  **               NewUpstreamCurrentRate
  **               NewDownstreamCurrentRate
  **               NewUpstreamNoiseMargin
  **               NewDownstreamNoiseMargin
  **               NewUpstreamPower
  **               NewDownstreamPower
  **               NewUpstreamAttenuation
  **               NewDownstreamAttenuation 
  ***************************************************************************************/
  GetDSLLinkInfo : function () 
  {
    try {
      var newObj = new Object();
      newObj.NewUpstreamCurrentRate   = -1;
      newObj.NewDownstreamCurrentRate = -1;
      newObj.NewUpstreamNoiseMargin   = "30.0";
      newObj.NewDownstreamNoiseMargin = "29.0";
      newObj.NewUpstreamPower         = "11.5";
      newObj.NewDownstreamPower       = "4.5";
      newObj.NewUpstreamAttenuation   = "3.5";
      newObj.NewDownstreamAttenuation = 0;
      return newObj;
    } catch(ex) {
      _logger.error("_null_modem_GetDSLLinkInfo", ex.message );
      sdcDSLModem.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
    }
    return null;
  }
});
  var _logger = $ss.agentcore.log.GetDefaultLogger("_null_modem");
  var _netCheck = $ss.agentcore.network.netcheck;
  var _constants = $ss.agentcore.constants;
  var _config = $ss.agentcore.dal.config;
  var _smapiutils = $ss.agentcore.smapi.utils;
  var sdcDSLModem = $ss.agentcore.smapi.methods;