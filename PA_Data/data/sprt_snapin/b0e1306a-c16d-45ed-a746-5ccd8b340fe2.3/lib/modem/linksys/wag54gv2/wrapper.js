/***************************************************************************************
**
**  File Name: smapi_wrapper.js
**
**  Summary: SMAPI derived class for Linksys WAG54GV2 modem
**
**  Description: This javascript contains SMAPI derivative to support Linksys WAG54GV2
**               implementation. It maps SMAPI functions to Linksys WAG54GV2 specific 
**               calls.
**           
**  Copyright SupportSoft Inc. 2005, All rights reserved.
***************************************************************************************/

  var _logger = $ss.agentcore.log.GetDefaultLogger("$ss.snapin.modem.WAG54GV2");
  var _utils = $ss.agentcore.utils;
  var _sysUtils = $ss.agentcore.utils.system
  var _netCheck = $ss.agentcore.network.netcheck;
  var _registry = $ss.agentcore.dal.registry;
  var _config = $ss.agentcore.dal.config;
  var _constants = $ss.agentcore.constants;
  var _smapiutils = $ss.agentcore.smapi.utils;
  var _tr64Wrapper = $ss.snapin.modem.tr64.wrapper;
/***************************************************************************************
** Name:         _wag54gv2
**
** Purpose:      Class that wraps SMAPI functionality and Linksys WAG54GV2 specific
**               implementation for it. The class uses TR64 protocol hence TR64 class and
**               its underlying functionality for generic implementation.
**
** Parameter:    oSMAPI     : An instance of SMAPI class is sent to derived implementation
**                            to avail base class functionality
**               arProtocol : (optional) Array of base protocol implementations could 
**                            be sent for direct use in derived class implementations
**                            Every protocol implementation has a property sNAME. Check
**                            that before directly using it.
**
** Return:       object
***************************************************************************************/
function _wag54gv2(oSMAPI, arProtocol)
{
  this.oSMAPI = oSMAPI;
  this.oTR64 = new _tr64Wrapper(oSMAPI);
  this.mProperties = this.oTR64.mProperties;
  // For batch update support
  this.sQueuedCmd = _constants.SMAPI_EMPTY_STR;
  this.oPWCtl     = null; 
}

/***************************************************************************************
** Name:         _wag54gv2_ID
**
** Purpose:      Determine the unique identifier for the modem. This implementation is 
**               helpful at protocol level so that SMAPI can use it to infer modem 
**               specific implementations.
**
** Parameter:    none
**
** Return:       unique identifier of the modem matching modem xml file
***************************************************************************************/
_wag54gv2.prototype.ID = function _wag54gv2_ID()
{
  return  this.oTR64.ID();
}

/***************************************************************************************
** Name:         _wag54gv2_Name
**
** Purpose:      Gets the modem name as defined in the modem.xml
**
** Parameter:    none
**
** Return:       str
***************************************************************************************/
_wag54gv2.prototype.Name = function _wag54gv2_Name() 
{
  return this.oTR64.Name();
}

/*******************************************************************************
** Name:         _wag54gv2_SerialNumber
**
** Purpose:      Gets Serial Number of derived modem. This info is generally 
**               present at back of the modem.
**
** Parameter:    none
**
** Return:       GetLastError to confirm success and then check response object
**               [NewSerialNumber] property.
*******************************************************************************/
_wag54gv2.prototype.SerialNumber = function _wag54gv2_SerialNumber() 
{
  return this.oTR64.SerialNumber();
}

/*******************************************************************************
** Name:         _wag54gv2_MACAddress
**
** Purpose:      Gets MACAddress of derived modem. 
**
** Parameter:    none
**
** Return:       GetLastError to confirm success and then check response object
**               [NewMACAddress] property.
*******************************************************************************/
_wag54gv2.prototype.MACAddress = function _wag54gv2_MACAddress() 
{
  return this.oTR64.MACAddress();
}

/*******************************************************************************
** Name:         _wag54gv2_Firmware
**
** Purpose:      Gets firmware version of derived modem. 
**
** Parameter:    none
**
** Return:       GetLastError to confirm success and then check response object
**               [NewFirmwareVersion] property.
*******************************************************************************/
_wag54gv2.prototype.Firmware = function _wag54gv2_Firmware() 
{
  return this.oTR64.Firmware();
}

/***************************************************************************************
** Name:         _wag54gv2_Detect
**
** Purpose:      Verifies that linksys wag54gv2 is connected to the PC. Detection 
**               could fail if PC to modem connection is broken or modem connected is not
**               with linksys wag54gv2 modem.
**
** Parameter:    none
**
** Return:       true if detected.
***************************************************************************************/
_wag54gv2.prototype.Detect = function _wag54gv2_Detect()
{ 
  var nRet = 0;
  try {
    // DOM loads in TR64 protocol way of detection on wireless connections 
    // are flaky with linksys firmware hence reverting to Pings.
    // nRet = this.oTR64.Detect();
    nRet = (_netCheck.Ping(this.mProperties.ip)) ? 1 : 0;
    if(nRet) {
      // detection with the protocol succeeded hence get the unique identifier
      var id = this.oTR64.ID(); 
      nRet = (id == this.mProperties.id) ? 1 : 0;
    }
  } catch (ex) {
    _logger.error("_wag54gv2_Detect", ex.message );
    this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
  }
  return nRet;
}

/***************************************************************************************
** Name:         _wag54gv2v2_IsSync
**
** Purpose:      Determine if modem is in sync.  This is a required 
**               procedure and should determine as best as possible if we have sync.
**
** Parameter:    (none)
**
** Return:       true if we have sync
***************************************************************************************/
_wag54gv2.prototype.IsSync = function _wag54gv2_IsSync() 
{
  var nSync = 0;
  try {
    // Get DSLLinkConfig and check its properties
    var oResp = this.oTR64.GetDSLLinkConfig();
    if(this.oSMAPI.GetLastError() == _constants.SMAPI_SUCCESS) {
      if(oResp.NewLinkStatus == "Up" &&
         oResp.NewLinkType   == this.mProperties.dsl_link_type &&
         oResp.NewDestinationAddress == this.mProperties.destination_address ) {
      
        nSync = 1;
      
      } else  {
      
        var bNeedReboot = false;
        // NewLinkType if not PPPoE or whatever is in this.mProperties.dsl_link_type
        // in ss_modems.xml then SetDSLLinkType
        if(oResp.NewLinkType != this.mProperties.dsl_link_type) {
          this.oTR64.SetDSLLinkType(this.mProperties.dsl_link_type);
          if (this.oSMAPI.GetLastError() != _constants.SMAPI_SUCCESS) return nSync;
          _smapiutils.g_oSMAPIDependencies.Sleep(1000);
          bNeedReboot = true;
        } 
        
        // NewDestinationAddress if not PVC:0/35 or whatever in this.mProperties.destination_address
        // in ss_modems.xml then SetDestinationAddress
        if(oResp.NewDestinationAddress != this.mProperties.destination_address) {
          this.oTR64.SetDestinationAddress(this.mProperties.destination_address);
          if (this.oSMAPI.GetLastError() != _constants.SMAPI_SUCCESS) return nSync;
          _smapiutils.g_oSMAPIDependencies.Sleep(1000);
          bNeedReboot = true;
        }
        
        // If reboot is happening then Enable DSL Link as well
        if(bNeedReboot) {
          this.oTR64.SetDSLLinkEnable(1);
          this.oSMAPI.SetLastError(_constants.SMAPI_DO_REBOOT);
        }
      }
    }
  } catch (ex) {
    _logger.error("_wag54gv2_IsSync", ex.message );
    this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
  }
  return nSync;
}

/***************************************************************************************
** Name:         _wag54gv2_IsConnected
**
** Purpose:      Checks to see if modem has WAN IP connectivity.
**
** Parameter:    (none)
**
** Return:       0/1 based on WAN connectivity status
***************************************************************************************/
_wag54gv2.prototype.IsConnected = function _wag54gv2_IsConnected() 
{
  return this.oTR64.IsConnected();
}

/***************************************************************************************
** Name:         _wag54gv2_SetUserCreds
**
** Purpose:      Set user credentials on modem
**
** Parameter:    sUserName : user name
**               sPassword : user password
**
** Return:       none; check GetLastError
***************************************************************************************/
_wag54gv2.prototype.SetUserCreds = function _wag54gv2_SetUserCreds(sUserName, sPassword) 
{
  // Get WANPPConnection Type and if not IP_Routed then set it to IP_Routed
  var sConnType = this.oTR64.GetPPPConnectionTypeInfo();
  if(this.oSMAPI.GetLastError() == _constants.SMAPI_SUCCESS) {
    if(sConnType != "IP_Routed") 
      this.oTR64.SetPPPConnectionType("IP_Routed");
  } else {
    // Some failures of this firmware are due to bad state and can be handled with Reboot
    if(this.oSMAPI.GetLastError() == _constants.SMAPI_DERIVED_FAIL) {
      this.oSMAPI.SetLastError(_constants.SMAPI_DO_REBOOT);
      return;
    }  
  }  
  if(this.oSMAPI.GetLastError() != _constants.SMAPI_SUCCESS) return;
  
  // Get WANPPConnection Status and if connected then terminate connection as 
  // we want to set new creds 
  if(this.oTR64.IsConnected()) {
    //TTPro 8445: We need to Request Termination rather than Force on Linksys.
    this.oTR64.TerminatePPPConnection();
  }  
  if(this.oSMAPI.GetLastError() != _constants.SMAPI_SUCCESS) return;
  
  // Now Set WANPPP UserName and proceed only if things are OK till now
  this.oTR64.SetPPPUserName(sUserName);
  if(this.oSMAPI.GetLastError() != _constants.SMAPI_SUCCESS) return;
  
  // Set WANPPP Password and proceed only if things are OK till now
  this.oTR64.SetPPPPassword(sPassword);
  if(this.oSMAPI.GetLastError() != _constants.SMAPI_SUCCESS) return;
  
  _smapiutils.g_oSMAPIDependencies.Sleep(2000);
  // now Request PPP connection
  this.oTR64.RequestPPPConnection();
  if(this.oSMAPI.GetLastError() != _constants.SMAPI_SUCCESS) return;
  
  
  // No IP association most of the times after setting creds on Linksys
  // hence do a release/renew
  if(this.oSMAPI.GetLastError() == _constants.SMAPI_SUCCESS) {
    _smapiutils.g_oSMAPIDependencies.Sleep(1000);
    _logger.debug("_wag54gv2_SetUserCreds", "Doing IP Release/Renew");
    if(_sysUtils.ReleaseIpConfig()) {
      _sysUtils.RenewIpConfig();
      _smapiutils.g_oSMAPIDependencies.Sleep(3000);
    }
  }    
}

/***************************************************************************************
** Name:         _wag54gv2_Reboot
**
** Purpose:      Checks to see if modem has WAN IP connectivity.
**
** Parameter:    (none)
**
** Return:       none; check GetLastError
***************************************************************************************/
_wag54gv2.prototype.Reboot = function _wag54gv2_Reboot() 
{
  this.oTR64.Reboot();
  if(this.oSMAPI.GetLastError() == _constants.SMAPI_SUCCESS) {
    _smapiutils.g_oSMAPIDependencies.Sleep(15000);
  }  
}

/*******************************************************************************
** Name:         _wag54gv2_GetDeviceInfo
**
** Purpose:      Provides bunch of CPE generic parameters in one shot. Many 
**               implementaions have noticed such information available via 
**               1 or 2 actual modem calls. Thus clubbing it together helps 
**               reducing actual calls being made to modem. 
**               Modem implementations can choose to fill all required output 
**               parameters or default to -1 if a particular param is not 
**               implemented.
**
** Parameter:    none
**
** Return:       GetLastError to confirm success and then check response object
**               for the following properties
**               NewManufacturerName
**               NewModelName
**               NewSerialNumber
**               NewFirmwareVersion
**               NewHardwareVersion
*******************************************************************************/
_wag54gv2.prototype.GetDeviceInfo = function _wag54gv2_GetDeviceInfo() 
{
  return this.oTR64.GetDeviceInfo();
}

/********************************************************************************
** Name:         _wag54gv2_GetDSLLinkInfo
**
** Purpose:      Provides bunch of DSL link parameters in one shot as 
**               per the TR64 Spec. 
**
** Parameter:    none
**
** Return:       Object having the following properties
**               NewUpstreamCurrentRate
**               NewDownstreamCurrentRate
**               NewUpstreamNoiseMargin
**               NewDownstreamNoiseMargin
**               NewUpstreamPower
**               NewDownstreamPower
**               NewUpstreamAttenuation
**               NewDownstreamAttenuation 
********************************************************************************/
_wag54gv2.prototype.GetDSLLinkInfo = function _wag54gv2_GetDSLLinkInfo() 
{
  return this.oTR64.GetDSLLinkInfo();
}

/*******************************************************************************
** Name:         _wag54gv2_GetWirelessInfo
**
** Purpose:      Retrieves all the wireless state variables other than the  
**               statistics and security keys. 
**
** Parameter:    none
**
** Return:       GetLastError to confirm success and then check response object
**               for the following properties
**               NewEnable
**               NewStatus
**               NewMaxBitRate
**               NewChannel
**               NewMACAddressControlEnabled
**               NewStandard
**               NewBSSID
**               ---------------------------------------------------------------
**               Optional for vendor specific implementations if not TR64 as 
**               proprietary implementations can use other SMAPI calls as well
**               to return below information
**               NewSSID                           OR use GetWirelessSSID
**               NewSecurityMode                   OR use GetWirelessSecurity
**               NewBasicEncryptionModes           OR use GetWirelessSecurity
**               NewBasicAuthenticationMode        OR use GetWirelessSecurity
*******************************************************************************/
_wag54gv2.prototype.GetWirelessInfo = function _wag54gv2_GetWirelessInfo() 
{
  return this.oTR64.GetWirelessInfo();
}

/*******************************************************************************
** Name:         _wag54gv2_SetWirelessConfig  
**
** Purpose:      Applies wireless configuration to the modem
**
** Parameter:    object of type WLSSecurity
** 
** WLSConfig() {   // all default values mean no operation to perform for those 
**                 // values leave settings in modem as is
**   this.nEnable      = SMAPI_INT_DONT_CARE; // set wireless radio mode in 
**                 // other words enable/disable wireless interface 1:on/0:off 
**   this.sMaxBitRate  = SMAPI_STR_DONT_CARE; // Auto, OR the largest of 
**                 // the OperationalTrxRates values in (Mbps)
**   this.nChannel     = SMAPI_INT_DONT_CARE; // set wireless channel
**   this.ulStandard   = SMAPI_HEX_DONT_CARE; // sets the IEEE 802.11 standard 
**                 // the device is operating in. Not implemented in TR64
**   this.nEnableMACL  = SMAPI_INT_DONT_CARE; 
**                 // set MAC Address control filtering 1:on/0:off
**   this.oWLSDataTransmission = SMAPI_OBJ_DONT_CARE; 
**                 // new WLSDataTransmission(); 
**                 // In general used for setting frame transmission rates. 
**   this.oWLSSSID     = SMAPI_OBJ_DONT_CARE; // new WLSSSID(); set SSID Info
**   this.oWLSSecurity = SMAPI_OBJ_DONT_CARE; // new WLSSecurity();   set wireless security                                          
**   this.oUserDefined = SMAPI_OBJ_DONT_CARE; // for future expansion or use with custom implementations
** }
**
** Return:       GetLastError to confirm success 
*******************************************************************************/
_wag54gv2.prototype.SetWirelessConfig = function _wag54gv2_SetWirelessConfig(oWLSConfig) 
{
  /*if(!this.oSMAPI.m_bInQueue) {
    
    // Linksys TR64 implementation does not allow just setting of 
    // MAC Address control filtering 1:on/0:off hence use screen scraping
    var sCmd = this.pvt_SetWirelessMACACLEnabled(oWLSConfig.nEnableMACL);
    if(sCmd != SMAPI_EMPTY_STR) {
      this.ExecuteCommand(sCmd);
      if(this.oSMAPI.GetLastError() != SMAPI_SUCCESS) return SMAPI_VAL_UNKNOWN;
      oWLSConfig.nEnableMACL = SMAPI_INT_DONT_CARE; //stop TR64 from handling this
    }  
    
    // Wrong implementation of SSID Broadcast in Linksys TR64 implementation
    // hence use screen scraping here too
    this.SetSSIDInfo(oWLSConfig.oWLSSSID);
    if(this.oSMAPI.GetLastError() != SMAPI_SUCCESS) return SMAPI_VAL_UNKNOWN;
    oWLSConfig.oWLSSSID = SMAPI_OBJ_DONT_CARE;
    
    // For remaining call TR64
    this.oTR64.SetWirelessConfig(oWLSConfig);
  } else {
    // Batch update is not yet implemented in TR64 way hence trying 
    // screen scraping for this call.
    this.pvt_SetWirelessConfigInBatch(oWLSConfig);
  }*/
  
  // TTPro: 8551
  // Linksys is NOT able to set WEPEncryption (128/64) bit via TR64 usage hence 
  // have to fall back to screen scraping ALWAYS
  this.pvt_SetWirelessConfigInBatch(oWLSConfig);
  
  // Non batch needs to be committed right away
  if(!this.oSMAPI.m_bInQueue && this.sQueuedCmd != _constants.SMAPI_EMPTY_STR) {
    this.CommitQueuedCommands();
  }  
}  

/*******************************************************************************
** Name:         _wag54gv2_GetWirelessSecurity
**
** Purpose:      Retrieves all the security variables requested by caller.
**
** Parameter:    ulFlag : Helps implementations in knowing what is required
**               by caller at this particular time. This helps in reducing the
**               calls made to modem or targetting them better.
**               SMAPI_HEX_DONT_CARE : return only what is set in modem
**               WLS_SECMODE_BASIC   : return properties set for mode Basic
**               WLS_SECMODE_WPA     : return properties set for mode WPA
**               WLS_SECMODE_WPA2    : return properties set for 11i.
**
** Return:       GetLastError to confirm success and the check response object
**               for object of type WLSSecurity
*******************************************************************************/
_wag54gv2.prototype.GetWirelessSecurity = function _wag54gv2_GetWirelessSecurity(ulFlag) 
{
  var oWLSSecurity = this.oTR64.GetWirelessSecurity(ulFlag);
  if(this.oSMAPI.GetLastError() != _constants.SMAPI_SUCCESS) return oWLSSecurity;
  // Cutomize output for half baked TR64 implementations of Linksys
  switch(oWLSSecurity.ulSecMode) {
    case _constants.WLS_SECMODE_BASIC:             // call with basic security   
      // Linksys in basic mode can return invalid TR64 params
      // If so then these are old firmwares that allow Open Authentication
      // with WEP Encryption only hence check and set that
      if((oWLSSecurity.ulAuthentication == _constants.SMAPI_HEX_DONT_CARE) &&
         (oWLSSecurity.ulEncryption == _constants.SMAPI_HEX_DONT_CARE) ) {
        oWLSSecurity.ulAuthentication |= _constants.WLS_AUTH_OPEN;
        oWLSSecurity.ulEncryption |= _constants.WLS_ENCRYPT_WEP;
      }  
      break;
      
    case _constants.WLS_SECMODE_NONE:              // no need to check further in no security
    case _constants.WLS_SECMODE_WPA:               // Security mode WPA
    case _constants.WLS_SECMODE_WPA2:              // Security mode WPA2 or 11i
      break;
  }
  return oWLSSecurity;
}

/*******************************************************************************
** Name:         _wag54gv2_SetWirelessSecurity
**
** Purpose:      Applies Wireless security settings to the modem
**
** Parameter:    object of type WLSSecurity
**
** WLSSecurity() {  // all default values mean no operation to perform for those values
**                  // leave settings in modem as is
**  this.ulSecMode        = SMAPI_HEX_DONT_CARE;
**  this.ulAuthentication = SMAPI_HEX_DONT_CARE; 
**  this.ulEncryption     = SMAPI_HEX_DONT_CARE;
**  this.oKeys            = new WLSSecurityKeys();
**  this.oUserDefined     = SMAPI_OBJ_DONT_CARE; // for future expansion or use with 
**                                               // custom implementations
** }
**    
** Return:       GetLastError to confirm success 
*******************************************************************************/
_wag54gv2.prototype.SetWirelessSecurity = function _wag54gv2_SetWirelessSecurity(oWLSSecurity) 
{
  /*
  if(!this.oSMAPI.m_bInQueue) {
    this.oTR64.SetWirelessSecurity(oWLSSecurity);
  } else {
    // Batch update is not yet implemented in TR64 way hence 
    // trying screen scraping for this call.
   
  }*/  
  
  // TTPro: 8551
  // Linksys is NOT able to set WEPEncryption (128/64) bit via TR64 usage hence 
  // have to fall back to screen scraping ALWAYS
  
  var sCmd = "submit_button=WL_WPATable&action=Apply&";
  switch(oWLSSecurity.ulSecMode) {
    case _constants.WLS_SECMODE_NONE:
      this.sQueuedCmd += sCmd + "security_mode=disabled&";
      break;
    
    case _constants.WLS_SECMODE_BASIC  :
      // Linksys allows only WEP Encryption with Open Authentication for Basic security mode
      if(_smapiutils.MaskIn(oWLSSecurity.ulAuthentication, _constants.WLS_AUTH_OPEN) &&
          _smapiutils.MaskIn(oWLSSecurity.ulEncryption, _constants.WLS_ENCRYPT_WEP) &&
          oWLSSecurity.oKeys.sWEPKey0 != _constants.SMAPI_EMPTY_STR ) {
        sCmd += "security_mode=wep&wl_key=1&wl_WEP_key=&wl_wep=restricted&";
        if(oWLSSecurity.oKeys.sWEPKey0.length == _constants.WLS_WEP128_KEYLENGTH) {
          sCmd += "wl_wep_bit=128&";
        } else if (oWLSSecurity.oKeys.sWEPKey0.length == _constants.WLS_WEP64_KEYLENGTH)  {
          sCmd += "wl_wep_bit=64&";
        } else {
          _logger.error("_wag54gv2_SetWirelessSecurity", "Invalid WEP Key length:" + oWLSSecurity.oKeys.sWEPKey0);
          this.oSMAPI.SetLastError(_constants.SMAPI_INVALID_ARGUMENT);
          return;
        }    
        //no passphrase in batch mode
        this.sQueuedCmd +=  sCmd + "generateButton=Null&wl_key1=" + oWLSSecurity.oKeys.sWEPKey0 + 
                            "&wl_key2=&wl_key3=&wl_key4=&";
      } else {
        _logger.error("_wag54gv2_SetWirelessSecurity", "Open Authentication with WEP Encryption is the only supported Basic mode");
        this.oSMAPI.SetLastError(_constants.SMAPI_OPERATION_NOTSUPPORTED);
      }    
      break;
      
    case _constants.WLS_SECMODE_WPA  :
      // Linksys in screen scraping mode only allows PSK Authentication
      // with TKIP Encryption for WPA security mode
      if(_smapiutils.MaskIn(oWLSSecurity.ulAuthentication, _constants.WLS_AUTH_PSK) &&
          _smapiutils.MaskIn(oWLSSecurity.ulEncryption, _constants.WLS_ENCRYPT_TKIP) ) {
        if(oWLSSecurity.oKeys.sPreSharedKey != _constants.SMAPI_STR_DONT_CARE) {
          this.sQueuedCmd += sCmd + "security_mode=psk&" +
            "wl_wpa_psk=" + oWLSSecurity.oKeys.sPreSharedKey +
            "&wl_wpa_gtk_rekey=3600&";
        } else {
          _logger.error("_wag54gv2_SetWirelessSecurity", "Preshared key cannot be empty in WPA mode");
          this.oSMAPI.SetLastError(_constants.SMAPI_INVALID_ARGUMENT);
        }    
      } else {
        _logger.error("_wag54gv2_SetWirelessSecurity", "PSK Authentication with TKIP Encryption is the only supported WPA mode");
        this.oSMAPI.SetLastError(_constants.SMAPI_OPERATION_NOTSUPPORTED);
      }    
      break;
      
    default :   
      this.oSMAPI.SetLastError(_constants.SMAPI_OPERATION_NOTSUPPORTED);
      break;
  }
  
  // Non batch needs to be committed right away
  if(!this.oSMAPI.m_bInQueue && this.sQueuedCmd != _constants.SMAPI_EMPTY_STR) {
    this.CommitQueuedCommands();
  }  
}

/*******************************************************************************
** Name:         _wag54gv2_GetSSIDInfo
**
** Purpose:      Sets Network name(SSID) and SSID broadcast values from modem. 
**               Implementations can choose to fill all required output parameters 
**               or default to -1 if a particular param is not implemented.
**
** Parameter:    ulFlag : Helps implementations in knowing what is required
**               by caller at this particular time. This helps in reducing the
**               calls made to modem or targetting them better.
**               
**               WLS_SSID_NAME:       caller needs only NewSSID value
**               WLS_SSID_BROADCAST:  caller needs only NewSSIDBroadcast value
**               WLS_SSID_NAME|WLS_SSID_BROADCAST: caller needs both  
**               SMAPI_HEX_DONT_CARE: special case again caller needs both
**
** Return:       GetLastError to confirm success and then check response object
**               for the following properties
**               NewSSID
**               NewSSIDBroadcastEnabled
*******************************************************************************/
_wag54gv2.prototype.GetSSIDInfo = function _wag54gv2_GetSSIDInfo(ulFlag) 
{
  // Wrong implementation of SSID Broadcast in Linksys TR64 implementation
  // hence TODO: screen scraping here too
  var oRet = this.oTR64.GetSSIDInfo(ulFlag);
  if(oRet && oRet.NewSSIDBroadcastEnabled) {
    oRet.NewSSIDBroadcastEnabled = _constants.SMAPI_VAL_UNKNOWN;
  }  
  return oRet;
}

/*******************************************************************************
** Name:         _wag54gv2_SetSSIDInfo
**
** Purpose:      Sets Network name(SSID) and SSID broadcast values to modem.
**
** Parameter:    object of type oWLSSSID
**
** WLSSSID() {   // all default values mean no operation to perform for those values
**               // leave settings in modem as is
**   this.sSSID            = SMAPI_STR_DONT_CARE;   // Server Set ID
**   this.nSSIDBroadcast   = SMAPI_INT_DONT_CARE;   // 1:On / 0: Off
** }
** 
**Return:       GetLastError to confirm success 
*******************************************************************************/
_wag54gv2.prototype.SetSSIDInfo = function _wag54gv2_SetSSIDInfo(oWLSSSID) 
{
  try {
    // Wrong implementation of SSID Broadcast in Linksys TR64 implementation
    // hence use screen scraping here too
    if(oWLSSSID != _constants.SMAPI_OBJ_DONT_CARE && 
       oWLSSSID.nSSIDBroadcast != _constants.SMAPI_INT_DONT_CARE) {
      var sCmd = "submit_button=Wireless_Basic&action=Apply&";
      if(oWLSSSID.nSSIDBroadcast) {
        sCmd += "wl_closed=0&";
      } else {
        sCmd += "wl_closed=1&";
      }
      this.ExecuteCommand(sCmd);
      if(this.oSMAPI.GetLastError() != _constants.SMAPI_SUCCESS) return;
      // clean broadcast value before calling TR64
      oWLSSSID.nSSIDBroadcast = _constants.SMAPI_INT_DONT_CARE;
    }
    this.oTR64.SetSSIDInfo(oWLSSSID);
  
  } catch (ex) {
    _logger.error("_wag54gv2_SetSSIDInfo", ex.message );
    this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
  }
}

/*******************************************************************************
** Name:         _wag54gv2_QueueCommands
**
** Purpose:      By handling this function just telling SMAPI and caller that
**               we can handle queueing of commands.
**               In Reality: Its just handled for SetWirelessConfig. No plans
**               of supporting Queueing for now till TR64 supports it.
** 
** Parameter:    none
**
** Return:       GetLastError to confirm success 
*******************************************************************************/
_wag54gv2.prototype.QueueCommands = function _wag54gv2_QueueCommands() 
{
  this.sQueuedCmd = _constants.SMAPI_EMPTY_STR;
  return true;
}

/*******************************************************************************
** Name:         _wag54gv2_CommitQueuedCommands
**
** Purpose:      By using this function caller is hinting to start queing of all
**               the SMAPI calls made hence forth till a commit commands is 
**               received. Implementations can hook up their support for this
**               accordingly and see how they can do a batch up[date on CPE's in 
**               their code.
**
** Parameter:    none
**
** Return:       GetLastError to confirm success 
*******************************************************************************/
_wag54gv2.prototype.CommitQueuedCommands = function _wag54gv2_CommitQueuedCommands() 
{
  if(this.sQueuedCmd != _constants.SMAPI_EMPTY_STR) {
    var oRet = this.ExecuteCommand(this.sQueuedCmd);
    if(this.oSMAPI.GetLastError() == _constants.SMAPI_SUCCESS)
      this.sQueuedCmd = _constants.SMAPI_EMPTY_STR;
    
    return oRet;
  } else {
    return _constants.SMAPI_VAL_UNKNOWN;
  }    
}

/*******************************************************************************
** Name:         _wag54gv2_ExecuteCommand
**
** Purpose:      Posts commands using using password control
**               
** Parameter:    sData    : Data to be posted
**               sUrl     : Remote destination where sData will be posted to
**               nTimeout : Timeout period in milliseconds
**               sUsername: Username for authentication
**               sPassword: Password for authentication
**
** Return:       obj with properties
**               bSuccess: true or false depending upon function succeeded or failed
**               httpStatus: http status of Post
**               responseText: response from post if any
*******************************************************************************/
_wag54gv2.prototype.ExecuteCommand = function _wag54gv2_ExecuteCommand(sCmd) 
{
  var oRet = {bSuccess: false};
  try {  
    var sUrl = "http://" + this.mProperties.ip + "/apply.cgi";   
    // TTPro: 8631 A "POST" on Linksys fails with http status 0 rather than 401
    // if user creds are wrong. So made an exclusive login method using "GET" to
    // capture bad cred failures.
    oRet.bSuccess = this.pvt_Login();
      
    if(oRet.bSuccess && this.oSMAPI.GetLastError() == _constants.SMAPI_SUCCESS) {
      this.oPWCtl.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
      var sLog = "-----------------------------------\n";
      sLog += "POST: " + sUrl + "\n";
      sLog += "-----------------------------------\n";
      sLog += "DATA: " + sCmd  + "\n";
      _logger.debug("_wag54gv2_ExecuteCommand", sLog );
      oRet.bSuccess     = this.oPWCtl.Send("POST", sUrl, sCmd);    
      g_oSMAPIDependencies.Sleep(200); 
      oRet.httpStatus   = this.oPWCtl.status;
      sLog = "-----------------------------------\n";
      sLog += "HTTP status: " + oRet.httpStatus + "\n";
      sLog += "-----------------------------------\n";
      _logger.debug("_wag54gv2_ExecuteCommand", sLog );
      oRet.responseText = ""; 
      // Check password control for hangs on response text
      // do not care for it right now as all commands using it are set commands
      // this.oPWCtl.responseText;    
      this.oPWCtl.close();
      sLog = "Response: " + oRet.responseText + "\n";
      sLog += "-----------------------------------\n";
      
      if(oRet.bSuccess && oRet.httpStatus == 200) {
        this.oSMAPI.SetLastError(_constants.SMAPI_SUCCESS);   
        _logger.debug("_wag54gv2_ExecuteCommand", sLog);
      } else if (oRet.httpStatus == 401) {
        // This can only happen in failed authentication case
        this.oSMAPI.SetLastError(_constants.SMAPI_NO_AUTH);
        _logger.warning("_wag54gv2_ExecuteCommand", sLog);
      } else {
        _logger.warning("_wag54gv2_ExecuteCommand", sLog);
        this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_FAIL);
      }
    }  
      
  } catch(ex) {
    var str =  "error: " + ex.message + "<br>";
    str     += "username: " + this.mProperties.modemuser + "<br>";
    str     += "password: " + this.mProperties.modempwd  + "<br>";
    str     += "url:      " + sUrl   
    this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION)
    _logger.error("_wag54gv2_ExecuteCommand", str);
  }    
  return oRet;
}

/*******************************************************************************
** Name:         _wag54gv2_pvt_Login
**
** Purpose:      Tries login in to modem. Used during batch mode. Batch mode
**               ia password authenticated, TR64 is not.
**               
**               TTPro: 8631 A "POST" on Linksys fails with http status 0 rather 
**               than 401 if user creds are wrong. So made an exclusive login
**               method using "GET" to capture bad cred failures.
**                   
** Parameter:    none
**
** Return:       GetLastError to confirm success 
*******************************************************************************/
_wag54gv2.prototype.pvt_Login = function _wag54gv2_pvt_Login() 
{
  _logger.info("_wag54gv2_pvt_Login");
  var sHost = "http://" + this.mProperties.ip + "/"; 
  if (this.oPWCtl == null) this.oPWCtl = _utils.CreateActiveXObject("sdcuser.tgpassctl", true);
  var bRet = false;
  try {
    bRet = this.oPWCtl.open(sHost, this.mProperties.modemuser, this.mProperties.modempwd);
    if(bRet) {
       _smapiutils.g_oSMAPIDependencies.Sleep(200); 
      var sAuth = _utils.EncodeBase64(this.mProperties.modemuser + ":" + this.mProperties.modempwd)
      this.oPWCtl.setRequestHeader("Authorization", "Basic " + sAuth);
      bRet = this.oPWCtl.Send("GET", sHost, "");
       _smapiutils.g_oSMAPIDependencies.Sleep(200); 
      if(bRet && this.oPWCtl.status == 200) {
        //everything good do nothing and let the caller proceed with actual call
      } else if (this.oPWCtl.status == 401) {
        // This can only happen in failed authentication case
        var sMsg = "Failed Login to Linksys host: " + sHost + " using username: " + 
                  this.mProperties.modemuser + " password: " + this.mProperties.modempwd;
        _logger.warning("_wag54gv2_pvt_Login", sMsg);
        this.oSMAPI.SetLastError(_constants.SMAPI_NO_AUTH);
        this.oPWCtl.close();
      } else {
        // catch all bad Gets
        var sMsg = "Failed Access to host: " + sHost + " httpStatus: " + 
                   this.oPWCtl.status;
        _logger.error("_wag54gv2_pvt_Login", sMsg);
        this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_FAIL);
        this.oPWCtl.close();
      }
    } else {
      _logger.error("_wag54gv2_pvt_Login", "Failed to open connection to: " + sHost);
      this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_FAIL);
    }  
  } catch (ex) {
    _logger.error("_wag54gv2_pvt_Login", ex.message);
    this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
  }
  return bRet;
}

/*******************************************************************************
** Name:         _wag54gv2_pvt_SetWirelessConfigInBatch
**
** Purpose:      Applies wireless configuration to the modem in batch mode
**               Linksys implementation of TR64 does NOT support batch hence
**               this version is in old screen scraping way.
**
** Parameter:    object of type WLSSecurity
**
** Return:       GetLastError to confirm success 
*******************************************************************************/
_wag54gv2.prototype.pvt_SetWirelessConfigInBatch = function _wag54gv2_pvt_SetWirelessConfigInBatch(oWLSConfig) 
{
  
  try {
    // Enable or Disable the wireless interface/radio mode
    if(oWLSConfig.nEnable != _constants.SMAPI_INT_DONT_CARE) {
      this.sQueuedCmd += "submit_button=Wireless_Basic&action=Apply&"
      if(oWLSConfig.nEnable) {
        this.sQueuedCmd += "wl_gmode=1&";   // Enabling in mixed mode as that is linksys default
      } else {
        this.sQueuedCmd += "wl_gmode=-1&";
        // if wireless interface is being disabled then no need to set anything further
        return;  
      }
    }  
    
    // set SSID Info
    if(oWLSConfig.oWLSSSID != _constants.SMAPI_OBJ_DONT_CARE) {
      // Need to set ssid info hence check if we did enable /diable wireless since
      // command prepration is same in both
      if(oWLSConfig.nEnable == _constants.SMAPI_INT_DONT_CARE) { // didn't do so add command now
        this.sQueuedCmd = "submit_button=Wireless_Basic&action=Apply&"
      }
        
      // Check if SSID is to be set
      if(oWLSConfig.oWLSSSID.sSSID != _constants.SMAPI_STR_DONT_CARE) {
        this.sQueuedCmd += "wl_ssid=" + oWLSConfig.oWLSSSID.sSSID + "&"; 
      }  
      
      // Check if SSID Broadcast is to be updated
      if(oWLSConfig.oWLSSSID.nSSIDBroadcast != _constants.SMAPI_INT_DONT_CARE) {
        if(oWLSConfig.oWLSSSID.nSSIDBroadcast) {
          this.sQueuedCmd += "wl_closed=0&";
        } else {
          this.sQueuedCmd += "wl_closed=1&";
        } 
      }    
    }
      
    // Set MAC Address control filtering 1:on/0:off
    this.sQueuedCmd += this.pvt_SetWirelessMACACLEnabled(oWLSConfig.nEnableMACL);
      
    // set wireless security
    if(oWLSConfig.oWLSSecurity != _constants.SMAPI_OBJ_DONT_CARE) {
      this.SetWirelessSecurity(oWLSConfig.oWLSSecurity);
    }  
    
    // Its all or none for Queued commands as we don't want half good info to go in
    if(this.oSMAPI.GetLastError() != "SMAPI_SUCCESS")
      this.sQueuedCmd = _constants.SMAPI_EMPTY_STR;
      
  } catch (ex) {
    _logger.error("_wag54gv2_pvt_SetWirelessConfigInBatch", ex.message );
    this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
  } 
}

/*******************************************************************************
** Name:         _wag54gv2_pvt_SetWirelessMACACLEnabled
**
** Purpose:      Set MACAddressControl filtering on or off
**               on: 1/ off: 0
**
** Parameter:    none
**
** Return:       returns 1/0 ; -1 incase of failure or non accessibility
*******************************************************************************/
_wag54gv2.prototype.pvt_SetWirelessMACACLEnabled = function _wag54gv2_pvt_SetWirelessMACACLEnabled(nEnableMACL) 
{
  var sCmd = _constants.SMAPI_EMPTY_STR;
  // Set MAC Address control filtering 1:on/0:off
  if(nEnableMACL != _constants.SMAPI_INT_DONT_CARE) {
    sCmd = "submit_button=Wireless_MAC&action=Apply&";
    if(nEnableMACL) {
      sCmd += "wl_macmode1=other&wl_macmode=deny&";
    } else {
      sCmd += "wl_macmode1=disabled&";
    }
  }    
  return sCmd;
}  
    