/*******************************************************************************
**
**  File Name: smapi_wrapper.js
**
**  Summary: SMAPI derived class for Thomson ST716G modem
**
**  Description: This javascript contains SMAPI derivative to support Thomson 
**               ST716G implementation. It maps SMAPI functions to Thomson 
**               ST716G specific calls.
**
**  Dependencies: tr64_imp.js
**                cli_imp.js
**                smapi.js
**           
**  Copyright SupportSoft Inc. 2005, All rights reserved.
*******************************************************************************/
var _logger = $ss.agentcore.log.GetDefaultLogger("$ss.snapin.modem.ST716G.wrapper");
var _utils = $ss.agentcore.utils;
var _sysUtils = $ss.agentcore.utils.system
var _netCheck = $ss.agentcore.network.netcheck;
var _registry = $ss.agentcore.dal.registry;
var _config = $ss.agentcore.dal.config;
var _constants = $ss.agentcore.constants;
var _smapiutils = $ss.agentcore.smapi.utils;
var _tr64Wrapper = $ss.snapin.modem.tr64.wrapper;
/*******************************************************************************
** Name:         _st716g_1
**
** Purpose:      Class that wraps SMAPI functionality and Thomson ST716G 
**               specific implementation for it. The class uses combination of 
**               CLI commands and TR64 protocol for its underlying functionality.
**
** Parameter:    oSMAPI     : An instance of SMAPI class is sent to derived 
**                            implementation to avail base class functionality
**               arProtocol : (optional) Array of base protocol implementations 
**                            could be sent for direct use in derived class 
**                            implementations Every protocol implementation has 
**                            a property sNAME. Checkthat before directly using 
**                            it.
** Return:       object
*******************************************************************************/
function _st716g_1(oSMAPI, arProtocol)
{
  this.oSMAPI = oSMAPI;
  this.oTR64  = new _tr64Wrapper(oSMAPI);
  this.oCLI   = new _st716g_cli(oSMAPI);
  
  this.mProperties         = this.oTR64.mProperties;
  this.m_bIsConfigBackedUp = false; // tracks if we backed up the config or not
}

/*******************************************************************************
** Name:         _st716g_1_ID
**
** Purpose:      Determine the unique identifier for the modem. This implemen-
**               tation is helpful at protocol level so that SMAPI can use it 
**               to infer modem specific implementations.
**
** Parameter:    none
**
** Return:       unique identifier of the modem matching modem xml file
*******************************************************************************/
_st716g_1.prototype.ID = function _st716g_1_ID()
{
  return (this.oCLI.EnableTR64()) ? this.oTR64.ID() : "";  
}

/*******************************************************************************
** Name:         _st716g_1_Name
**
** Purpose:      Gets the modem name as defined in the modem.xml
**
** Parameter:    none
**
** Return:       str
*******************************************************************************/
_st716g_1.prototype.Name = function _st716g_1_Name() 
{
  return this.oTR64.Name();
}

/*******************************************************************************
** Name:         _st716g_1_SerialNumber
**
** Purpose:      Get Serial Number for Thomson ST716G modem.
**
** Parameter:    none
**
** Return:       string having Serial Number else empty
*******************************************************************************/
_st716g_1.prototype.SerialNumber = function _st716g_1_SerialNumber()
{
  return this.oCLI.SerialNumber();
}

/*******************************************************************************
** Name:         _st716g_1_MACAddress
**
** Purpose:      Get MACAddress for Thomson ST716G modem.
**
** Parameter:    none
**
** Return:       string having MACAddress else empty
*******************************************************************************/
_st716g_1.prototype.MACAddress = function _st716g_1_MACAddress()
{
  return this.oCLI.MACAddress();
}

/*******************************************************************************
** Name:         _st716g_1_Firmware
**
** Purpose:      Get firmware version Thomson ST716G modem.
**
** Parameter:    none
**
** Return:       string having firmware else empty
*******************************************************************************/
_st716g_1.prototype.Firmware = function _st716g_1_Firmware()
{
  return this.oCLI.Firmware();
}

/*******************************************************************************
** Name:         _st716g_1_Detect
**
** Purpose:      Verifies that Thomson ST716G is connected to the PC. Detection 
**               could fail if PC to modem connection is broken or modem 
**               connected is not with Thomson ST716G modem.
**
** Parameter:    none
**
** Return:       true if detected.
*******************************************************************************/
_st716g_1.prototype.Detect = function _st716g_1_Detect()
{ 
  return this.oCLI.Detect();
}

/*******************************************************************************
** Name:         _st716g_1_IsSync
**
** Purpose:      Determine if modem is in sync.  This is a required procedure 
**               and should determine as best as possible if we have sync.
**
** Parameter:    (none)
**
** Return:       true if we have sync
*******************************************************************************/
_st716g_1.prototype.IsSync = function _st716g_1_IsSync() 
{
  return this.oCLI.IsSync();
}

/*******************************************************************************
** Name:         _st716g_1_IsConnected
**
** Purpose:      Checks to see if modem has WAN IP connectivity.
**
** Parameter:    (none)
**
** Return:       0/1 based on WAN connectivity status
*******************************************************************************/
_st716g_1.prototype.IsConnected = function _st716g_1_IsConnected() 
{
  return this.oCLI.IsConnected();
}

/*******************************************************************************
** Name:         _st716g_1_SetUserCreds
**
** Purpose:      Set user credentials on modem
**
** Parameter:    sName     :  User name
**               sPassword :  User passowrd 
**
** Return:       none; check GetLastError
*******************************************************************************/
_st716g_1.prototype.SetUserCreds = function _st716g_1_SetUserCreds(sName, sPassword) 
{
  this.oCLI.SetUserCreds(sName, sPassword);
}

/*******************************************************************************
** Name:         _st716g_1_Reboot
**
** Purpose:      Checks to see if modem has WAN IP connectivity.
**
** Parameter:    (none)
**
** Return:       none; check GetLastError
*******************************************************************************/
_st716g_1.prototype.Reboot = function _st716g_1_Reboot() 
{
  this.oCLI.Reboot();
}

/*******************************************************************************
** Name:         _st716g_1_GetDeviceInfo
**
** Purpose:      Get GetDeviceInfo for Thomson ST716G modem.
**
** Parameter:    none
**
** Return:       object having various device info parameters such as
**               NewManufacturerName
**               NewModelName
**               NewSerialNumber
**               NewFirmwareVersion
**               NewHardwareVersion
*******************************************************************************/
_st716g_1.prototype.GetDeviceInfo = function _st716g_1_GetDeviceInfo()
{
  return this.oCLI.GetDeviceInfo();
}

/*******************************************************************************
** Name:         _st716g_1_GetDSLLinkInfo
**
** Purpose:      Checks to see if modem has WAN IP connectivity.
**
** Parameter:    (none)
**
** Return:       obj if successful; else check GetLastError
*******************************************************************************/
_st716g_1.prototype.GetDSLLinkInfo = function _st716g_1_GetDSLLinkInfo() 
{
  //return (this.oCLI.EnableTR64()) ? this.oTR64.GetDSLLinkInfo() : null;
  return this.oCLI.GetDSLLinkInfo();
}

/*******************************************************************************
** Name:         _st716g_1_SetCWMPInfo
**
** Purpose:      Sets the CPE WAN Management Protocol parameters on the modem
**
** Parameter:    Object of type CWMPInfo  
** 
** CWMPInfo() //Support for CPE WAN Management Protocol
** {
**   this.sACSUrl        = SMAPI_STR_DONT_CARE; 
**            // indicates the Url of the management server (ACS)
**   this.ulACSAuthType  = SMAPI_HEX_DONT_CARE; 
**            // Authentication used by ACS when CPE communicates
**   this.sACSUsername   = SMAPI_STR_DONT_CARE; 
**            // set/get Username that ACS will use to Authenticate CPE
**   this.sACSPassword   = SMAPI_STR_DONT_CARE; 
**            // set/get Password that ACS will use to Authenticate CPE
**   this.ulCPEAuthType  = SMAPI_HEX_DONT_CARE; 
**            // Authentication used by CPE when ACS communicates
**   this.sCPEUsername   = SMAPI_STR_DONT_CARE; 
**            // set/get Username that CPE will use to Authenticate ACS
**   this.sCPEPassword   = SMAPI_STR_DONT_CARE; 
**            // set/get Password that CPE will use to Authenticate ACS
**   this.oUserDefined   = SMAPI_OBJ_DONT_CARE; 
**            // for future expansion or use with custom implementations
** }
**
** Return:       GetLastError to confirm success 
*******************************************************************************/
_st716g_1.prototype.SetCWMPInfo = function _st716g_1_SetCWMPInfo(oCWMPInfo) 
{
  this.oCLI.SetCWMPInfo(oCWMPInfo);
}

/*******************************************************************************
** Name:         _st716g_1_UpgradeFirmware
**
** Purpose:      Sets the ACS values such as URL, Shared Secret etc
**
** Parameter:    bDownload : true means the firmware has to be downloaded 
**                           from a site
**
**               sUrl      : ACS URL also called CPE WAN Management Server URL 
**
** Return:       GetLastError to confirm success 
*******************************************************************************/
_st716g_1.prototype.UpgradeFirmware = function _st716g_1_UpgradeFirmware(bDownload, sUrl) 
{
  this.oCLI.UpgradeFirmware(bDownload, sUrl);
}

/*******************************************************************************
** Name:         _st716g_1_GetWirelessInfo
**
** Purpose:      Retrieves all the state variables other than the wireless 
**               statistics and security keys. 
**
** Parameter:    none
**
** Return:       GetLastError to confirm success and then check response object
**               for the following properties
**               NewEnable
**               NewStatus
**               NewMaxBitRate
**               NewChannel
**               NewMACAddressControlEnabled
**               NewStandard
**               NewBSSID
**               ---------------------------------------------------------------
**               Optional for vendor specific implementations if not TR64 as 
**               proprietary implementations can use other SMAPI calls as well
**               to return below information
**               NewSSID                           OR use GetWirelessSSID
**               NewSecurityMode                   OR use GetWirelessSecurity
**               NewBasicEncryptionModes           OR use GetWirelessSecurity
**               NewBasicAuthenticationMode        OR use GetWirelessSecurity
*******************************************************************************/
_st716g_1.prototype.GetWirelessInfo = function _st716g_1_GetWirelessInfo() 
{
  return this.oCLI.GetWirelessInfo();
}

/*******************************************************************************
** Name:         _st716g_1_SetWirelessConfig  
**
** Purpose:      Applies wireless configuration to the modem
**
** Parameter:    object of type WLSSecurity
** 
** WLSConfig() {   // all default values mean no operation to perform for those 
**                 // values leave settings in modem as is
**   this.nEnable      = SMAPI_INT_DONT_CARE; // set wireless radio mode in 
**                 // other words enable/disable wireless interface 1:on/0:off 
**   this.sMaxBitRate  = SMAPI_STR_DONT_CARE; // Auto, OR the largest of 
**                 // the OperationalTrxRates values in (Mbps)
**   this.nChannel     = SMAPI_INT_DONT_CARE; // set wireless channel
**   this.ulStandard   = SMAPI_HEX_DONT_CARE; // sets the IEEE 802.11 standard 
**                 // the device is operating in. Not implemented in TR64
**   this.nEnableMACL  = SMAPI_INT_DONT_CARE; 
**                 // set MAC Address control filtering 1:on/0:off
**   this.oWLSDataTransmission = SMAPI_OBJ_DONT_CARE; 
**                 // new WLSDataTransmission(); 
**                 // In general used for setting frame transmission rates. 
**   this.oWLSSSID     = SMAPI_OBJ_DONT_CARE; // new WLSSSID(); set SSID Info
**   this.oWLSSecurity = SMAPI_OBJ_DONT_CARE; // new WLSSecurity();   set wireless security                                          
**   this.oUserDefined = SMAPI_OBJ_DONT_CARE; // for future expansion or use with custom implementations
** }
**
** Return:       GetLastError to confirm success 
*******************************************************************************/
_st716g_1.prototype.SetWirelessConfig = function _st716g_1_SetWirelessConfig(oWLSConfig) 
{
  this.oCLI.SetWirelessConfig(oWLSConfig);
}

/*******************************************************************************
** Name:         _st716g_1_GetWirelessSecurity
**
** Purpose:      Retrieves all the security variables requested by caller.
**
** Parameter:    ulFlag : Helps implementations in knowing what is required
**               by caller at this particular time. This helps in reducing the
**               calls made to modem or targetting them better.
**               SMAPI_HEX_DONT_CARE : return only what is set in modem
**               WLS_SECMODE_BASIC   : return properties set for mode Basic
**               WLS_SECMODE_WPA     : return properties set for mode WPA
**               WLS_SECMODE_WPA2    : return properties set for 11i.
**
** Return:       GetLastError to confirm success and the check response object
**               for object of type WLSSecurity
*******************************************************************************/
_st716g_1.prototype.GetWirelessSecurity = function _st716g_1_GetWirelessSecurity(ulFlag) 
{
  return this.oCLI.GetWirelessSecurity(ulFlag);
}

/*******************************************************************************
** Name:         _st716g_1_SetWirelessSecurity
**
** Purpose:      Applies Wireless security settings to the modem
**
** Parameter:    object of type WLSSecurity
**
** WLSSecurity() {  // all default values mean no operation to perform for those values
**                  // leave settings in modem as is
**  this.ulSecMode        = SMAPI_HEX_DONT_CARE;
**  this.ulAuthentication = SMAPI_HEX_DONT_CARE; 
**  this.ulEncryption     = SMAPI_HEX_DONT_CARE;
**  this.oKeys            = new WLSSecurityKeys();
**  this.oUserDefined     = SMAPI_OBJ_DONT_CARE; // for future expansion or use with 
**                                               // custom implementations
** }
**    
** Return:       GetLastError to confirm success 
*******************************************************************************/
_st716g_1.prototype.SetWirelessSecurity = function _st716g_1_SetWirelessSecurity(oWLSSecurity) 
{
  this.oCLI.SetWirelessSecurity(oWLSSecurity);
}

/*******************************************************************************
** Name:         _st716g_1_GetSSIDInfo 
**
** Purpose:      Gets Network name(SSID) and SSID broadcast values from modem. 
**               
** Parameter:    ulFlag : Helps implementations in knowing what is required
**               by caller at this particular time. This helps in reducing the
**               calls made to modem or targetting them better.
**
**               WLS_SSID_NAME:       caller needs only NewSSID value
**               WLS_SSID_BROADCAST:  caller needs only NewSSIDBroadcast value
**               WLS_SSID_NAME|WLS_SSID_BROADCAST: caller needs both  
**               SMAPI_HEX_DONT_CARE: special case again caller needs both
**
** Return:       GetLastError to confirm success and then check response object
**               for the following properties
**               NewSSID
**               NewSSIDBroadcastEnabled
*******************************************************************************/
_st716g_1.prototype.GetSSIDInfo = function _st716g_1_GetSSIDInfo(ulFlag) 
{
  return this.oCLI.GetSSIDInfo(ulFlag);
}

/*******************************************************************************
** Name:         _st716g_1_SetSSIDInfo
**
** Purpose:      Sets Network name(SSID) and SSID broadcast values to modem.
**
** Parameter:    object of type oWLSSSID
**
** WLSSSID() {   // all default values mean no operation to perform for those values
**               // leave settings in modem as is
**   this.sSSID            = SMAPI_STR_DONT_CARE;   // Server Set ID
**   this.nSSIDBroadcast   = SMAPI_INT_DONT_CARE;   // 1:On / 0: Off
** }
** 
**Return:       GetLastError to confirm success 
*******************************************************************************/
_st716g_1.prototype.SetSSIDInfo = function _st716g_1_SetSSIDInfo(oWLSSSID) 
{
  this.oCLI.SetSSIDInfo(oWLSSSID);
}

/*******************************************************************************
** Name:         _st716g_1_QueueCommands
**
** Purpose:      By using this function caller is hinting to start queuing of all
**               the SMAPI calls made hence forth till a commit commands is 
**               received. Implementations can hook up their support for this
**               accordingly and see how they can do a batch update on CPE's in 
**               their code. They should return a true /false to tell if
**               queuing was successful. 
**
** Parameter:    none
**
** Return:       GetLastError to confirm success 
*******************************************************************************/
_st716g_1.prototype.QueueCommands = function _st716g_1_QueueCommands() 
{
  return this.oCLI.QueueCommands();
}

/*******************************************************************************
** Name:         _st716g_1_CommitQueuedCommands
**
** Purpose:      By using this function caller is hinting to start queuing of all
**               the SMAPI calls made hence forth till a commit commands is 
**               received. Implementations can hook up their support for this
**               accordingly and see how they can do a batch up[date on CPE's in 
**               their code.
**
** Parameter:    none
**
** Return:       GetLastError to confirm success 
*******************************************************************************/
_st716g_1.prototype.CommitQueuedCommands = function _st716g_1_CommitQueuedCommands() 
{
  return this.oCLI.CommitQueuedCommands();
}
