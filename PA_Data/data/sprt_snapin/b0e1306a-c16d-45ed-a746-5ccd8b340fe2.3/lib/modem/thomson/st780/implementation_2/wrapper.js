/*******************************************************************************
**
**  File Name: smapi_wrapper.js
**
**  Summary: SMAPI derived class for Thomson st780 modem
**
**  Description: This javascript contains SMAPI derivative to support Thomson 
**               st780 implementation. It maps SMAPI functions to Thomson 
**               st780 specific calls.
**           
**  Dependencies: tr64_imp.js
**                cli_imp.js
**                smapi.js
**           
**  Copyright SupportSoft Inc. 2005, All rights reserved.
*******************************************************************************/
var _logger = $ss.agentcore.log.GetDefaultLogger("$ss.snapin.modem.th780");
var _constants = $ss.agentcore.constants;
var _smapiutils = $ss.agentcore.smapi.utils;

/*******************************************************************************
** Name:         _st780_2
**
** Purpose:      Class that wraps SMAPI functionality and Thomson st780 
**               specific implementation for it. The class uses combination of 
**               CLI commands and TR64 protocol for its underlying functionality.
**
** Parameter:    oSMAPI     : An instance of SMAPI class is sent to derived 
**                            implementation to avail base class functionality
**               arProtocol : (optional) Array of base protocol implementations 
**                            could be sent for direct use in derived class 
**                            implementations Every protocol implementation has 
**                            a property sNAME. Checkthat before directly using 
**                            it.
** Return:       object
*******************************************************************************/
function _st780_2(oSMAPI, arProtocol)
{
  this.oSMAPI = oSMAPI;
  this.oTR64  = new $ss.snapin.modem.tr64.wrapper(oSMAPI);
  this.oCLI   = new _st780_cli(oSMAPI);
    
  this.mProperties         = this.oTR64.mProperties;
  this.m_bIsConfigBackedUp = false; // tracks if we backed up the config or not
}

/*******************************************************************************
** Name:         _st780_2_ID
**
** Purpose:      Determine the unique identifier for the modem. This implemen-
**               tation is helpful at protocol level so that SMAPI can use it 
**               to infer modem specific implementations.
**
** Parameter:    none
**
** Return:       unique identifier of the modem matching modem xml file
*******************************************************************************/
_st780_2.prototype.ID = function _st780_2_ID()
{
  return (this.oCLI.EnableTR64()) ? this.oTR64.ID() : _constants.SMAPI_VAL_UNKNOWN;  
}

/*******************************************************************************
** Name:         _st780_2_Name
**
** Purpose:      Gets the modem name as defined in the modem.xml
**
** Parameter:    none
**
** Return:       str
*******************************************************************************/
_st780_2.prototype.Name = function _st780_2_Name() 
{
  return (this.oCLI.EnableTR64()) ? this.oTR64.Name() : _constants.SMAPI_VAL_UNKNOWN;
}

/*******************************************************************************
** Name:         _st780_2_SerialNumber
**
** Purpose:      Get Serial Number for Thomson st780 modem.
**
** Parameter:    none
**
** Return:       string having Serial Number else empty
*******************************************************************************/
_st780_2.prototype.SerialNumber = function _st780_2_SerialNumber()
{
  return (this.oCLI.EnableTR64()) ? this.oTR64.SerialNumber() : _constants.SMAPI_VAL_UNKNOWN;
}

/*******************************************************************************
** Name:         _st780_2_MACAddress
**
** Purpose:      Get MACAddress for Thomson st780 modem.
**
** Parameter:    none
**
** Return:       string having MACAddress else empty
*******************************************************************************/
_st780_2.prototype.MACAddress = function _st780_2_MACAddress()
{
  return (this.oCLI.EnableTR64()) ? this.oTR64.MACAddress() : _constants.SMAPI_VAL_UNKNOWN;
}

/*******************************************************************************
** Name:         _st780_2_Firmware
**
** Purpose:      Get firmware version Thomson st780 modem.
**
** Parameter:    none
**
** Return:       string having firmware else empty
*******************************************************************************/
_st780_2.prototype.Firmware = function _st780_2_Firmware()
{
  return (this.oCLI.EnableTR64()) ? this.oTR64.Firmware() : _constants.SMAPI_VAL_UNKNOWN;
}

/*******************************************************************************
** Name:         _st780_2_Detect
**
** Purpose:      Verifies that Thomson st780 is connected to the PC. Detection 
**               could fail if PC to modem connection is broken or modem 
**               connected is not with Thomson st780 modem.
**
** Parameter:    none
**
** Return:       true if detected.
*******************************************************************************/
_st780_2.prototype.Detect = function _st780_2_Detect()
{ 
  return this.oCLI.Detect();
}

/*******************************************************************************
** Name:         _st780_2_IsSync
**
** Purpose:      Determine if modem is in sync.  This is a required procedure 
**               and should determine as best as possible if we have sync.
**
** Parameter:    (none)
**
** Return:       true if we have sync
*******************************************************************************/
_st780_2.prototype.IsSync = function _st780_2_IsSync() 
{
  return (this.oCLI.EnableTR64()) ? this.oTR64.IsSync() : _constants.SMAPI_VAL_UNKNOWN;
}

/*******************************************************************************
** Name:         _st780_2_IsConnected
**
** Purpose:      Checks to see if modem has WAN IP connectivity.
**
** Parameter:    (none)
**
** Return:       0/1 based on WAN connectivity status
*******************************************************************************/
_st780_2.prototype.IsConnected = function _st780_2_IsConnected() 
{
  return (this.oCLI.EnableTR64()) ? this.oTR64.IsConnected() : _constants.SMAPI_VAL_UNKNOWN;
}

/*******************************************************************************
** Name:         _st780_2_SetUserCreds
**
** Purpose:      Set user credentials on modem
**
** Parameter:    sName     :  User name
**               sPassword :  User passowrd 
**
** Return:       none; check GetLastError
*******************************************************************************/
_st780_2.prototype.SetUserCreds = function _st780_2_SetUserCreds(sName, sPassword) 
{
  if(this.oCLI.EnableTR64())
    this.oTR64.SetUserCreds(sName, sPassword);
}

/*******************************************************************************
** Name:         _st780_2_Reboot
**
** Purpose:      Checks to see if modem has WAN IP connectivity.
**
** Parameter:    (none)
**
** Return:       none; check GetLastError
*******************************************************************************/
_st780_2.prototype.Reboot = function _st780_2_Reboot() 
{
  if(this.oCLI.EnableTR64())
    this.oTR64.Reboot();
}

/*******************************************************************************
** Name:         _st780_2_GetDeviceInfo
**
** Purpose:      Get GetDeviceInfo for Thomson st780 modem.
**
** Parameter:    none
**
** Return:       object having various device info parameters such as
**               NewManufacturerName
**               NewModelName
**               NewSerialNumber
**               NewFirmwareVersion
**               NewHardwareVersion
*******************************************************************************/
_st780_2.prototype.GetDeviceInfo = function _st780_2_GetDeviceInfo()
{
  return (this.oCLI.EnableTR64())? this.oTR64.GetDeviceInfo() : _constants.SMAPI_OBJ_UNKNOWN;
}

/*******************************************************************************
** Name:         _st780_2_GetDSLLinkInfo
**
** Purpose:      Checks to see if modem has WAN IP connectivity.
**
** Parameter:    (none)
**
** Return:       obj if successful; else check GetLastError
*******************************************************************************/
_st780_2.prototype.GetDSLLinkInfo = function _st780_2_GetDSLLinkInfo() 
{
  return (this.oCLI.EnableTR64()) ? this.oTR64.GetDSLLinkInfo() : _constants.SMAPI_OBJ_UNKNOWN;
}

/*******************************************************************************
** Name:         _st780_2_SetCWMPInfo
**
** Purpose:      Sets the CPE WAN Management Protocol parameters on the modem
**
** Parameter:    Object of type CWMPInfo  
** 
** CWMPInfo() //Support for CPE WAN Management Protocol
** {
**   this.sACSUrl        = SMAPI_STR_DONT_CARE; 
**            // indicates the Url of the management server (ACS)
**   this.ulACSAuthType  = SMAPI_HEX_DONT_CARE; 
**            // Authentication used by ACS when CPE communicates
**   this.sACSUsername   = SMAPI_STR_DONT_CARE; 
**            // set/get Username that ACS will use to Authenticate CPE
**   this.sACSPassword   = SMAPI_STR_DONT_CARE; 
**            // set/get Password that ACS will use to Authenticate CPE
**   this.ulCPEAuthType  = SMAPI_HEX_DONT_CARE; 
**            // Authentication used by CPE when ACS communicates
**   this.sCPEUsername   = SMAPI_STR_DONT_CARE; 
**            // set/get Username that CPE will use to Authenticate ACS
**   this.sCPEPassword   = SMAPI_STR_DONT_CARE; 
**            // set/get Password that CPE will use to Authenticate ACS
**   this.oUserDefined   = SMAPI_OBJ_DONT_CARE; 
**            // for future expansion or use with custom implementations
** }
**
** Return:       GetLastError to confirm success 
*******************************************************************************/
_st780_2.prototype.SetCWMPInfo = function _st780_2_SetCWMPInfo(oCWMPInfo) 
{
  this.oCLI.SetCWMPInfo(oCWMPInfo);
}

/*******************************************************************************
** Name:         _st780_2_UpgradeFirmware
**
** Purpose:      Sets the ACS values such as URL, Shared Secret etc
**
** Parameter:    bDownload : true means the firmware has to be downloaded 
**                           from a site
**
**               sUrl      : ACS URL also called CPE WAN Management Server URL 
**
** Return:       GetLastError to confirm success 
*******************************************************************************/
_st780_2.prototype.UpgradeFirmware = function _st780_2_UpgradeFirmware(bDownload, sUrl) 
{
  this.oCLI.UpgradeFirmware(bDownload, sUrl);
}

/*******************************************************************************
** Name:         _st780_2_GetWirelessInfo
**
** Purpose:      Retrieves all the state variables other than the wireless 
**               statistics and security keys. 
**
** Parameter:    none
**
** Return:       GetLastError to confirm success and then check response object
**               for the following properties
**               NewEnable
**               NewStatus
**               NewMaxBitRate
**               NewChannel
**               NewMACAddressControlEnabled
**               NewStandard
**               NewBSSID
**               ---------------------------------------------------------------
**               Optional for vendor specific implementations if not TR64 as 
**               proprietary implementations can use other SMAPI calls as well
**               to return below information
**               NewSSID                           OR use GetWirelessSSID
**               NewSecurityMode                   OR use GetWirelessSecurity
**               NewBasicEncryptionModes           OR use GetWirelessSecurity
**               NewBasicAuthenticationMode        OR use GetWirelessSecurity
*******************************************************************************/
_st780_2.prototype.GetWirelessInfo = function _st780_2_GetWirelessInfo() 
{
  return (this.oCLI.EnableTR64()) ? this.oTR64.GetWirelessInfo() : _constants.SMAPI_OBJ_UNKNOWN;
}

/*******************************************************************************
** Name:         _st780_2_SetWirelessConfig  
**
** Purpose:      Applies wireless configuration to the modem
**
** Parameter:    object of type WLSSecurity
** 
** WLSConfig() {   // all default values mean no operation to perform for those 
**                 // values leave settings in modem as is
**   this.nEnable      = SMAPI_INT_DONT_CARE; // set wireless radio mode in 
**                 // other words enable/disable wireless interface 1:on/0:off 
**   this.sMaxBitRate  = SMAPI_STR_DONT_CARE; // Auto, OR the largest of 
**                 // the OperationalTrxRates values in (Mbps)
**   this.nChannel     = SMAPI_INT_DONT_CARE; // set wireless channel
**   this.ulStandard   = SMAPI_HEX_DONT_CARE; // sets the IEEE 802.11 standard 
**                 // the device is operating in. Not implemented in TR64
**   this.nEnableMACL  = SMAPI_INT_DONT_CARE; 
**                 // set MAC Address control filtering 1:on/0:off
**   this.oWLSDataTransmission = SMAPI_OBJ_DONT_CARE; 
**                 // new WLSDataTransmission(); 
**                 // In general used for setting frame transmission rates. 
**   this.oWLSSSID     = SMAPI_OBJ_DONT_CARE; // new WLSSSID(); set SSID Info
**   this.oWLSSecurity = SMAPI_OBJ_DONT_CARE; // new WLSSecurity();   set wireless security                                          
**   this.oUserDefined = SMAPI_OBJ_DONT_CARE; // for future expansion or use with custom implementations
** }
**
** Return:       GetLastError to confirm success 
*******************************************************************************/
_st780_2.prototype.SetWirelessConfig = function _st780_2_SetWirelessConfig(oWLSConfig) 
{
  try {
    if(this.oSMAPI.m_bInQueue) {
      // No support for batch update in TR64 implementation of Thomson
      // hence fallback to CLI for this call.
      return this.oCLI.SetWirelessConfig(oWLSConfig);
    }  
    
    // Enable or Disable the wireless interface/radio mode
    // No support in TR64 implementation of Thomson hence go CLI
    if(oWLSConfig.nEnable != _constants.SMAPI_INT_DONT_CARE) {
      var sCmd = "wireless ifconfig state=";
      sCmd += (oWLSConfig.nEnable) ? "enabled" : "disabled";
      this.oCLI.ExecuteCommand(sCmd, true);
      if(this.oSMAPI.GetLastError() != _constants.SMAPI_SUCCESS) return;
    }
    
    // set MAC Address control filtering 1:on/0:off
    // No support in TR64 implementation of Thomson hence go CLI
    if(oWLSConfig.nEnableMACL != _constants.SMAPI_INT_DONT_CARE) {
      this.oCLI.SetWirelessMACACLEnabled(oWLSConfig.nEnableMACL);
      if(this.oSMAPI.GetLastError() != _constants.SMAPI_SUCCESS) return;
    }
    
    if(oWLSConfig.oWLSSSID != _constants.SMAPI_OBJ_DONT_CARE) {
      this.SetSSIDInfo(oWLSConfig.oWLSSSID);
    }
    
    if(this.oSMAPI.GetLastError() == _constants.SMAPI_SUCCESS &&
       oWLSConfig.oWLSSecurity != _constants.SMAPI_OBJ_DONT_CARE) {
      this.SetWirelessSecurity(oWLSConfig.oWLSSecurity);
    }
      
  } catch(ex) {
    _logger.error("_st780_2_SetWirelessConfig", ex.message );
    this.oSMAPI.SetLastError(_constants.SMAPI_EXCEPTION);
  }
}

/*******************************************************************************
** Name:         _st780_2_GetWirelessSecurity
**
** Purpose:      Retrieves all the security variables requested by caller.
**
** Parameter:    ulFlag : Helps implementations in knowing what is required
**               by caller at this particular time. This helps in reducing the
**               calls made to modem or targetting them better.
**               SMAPI_HEX_DONT_CARE : return only what is set in modem
**               WLS_SECMODE_BASIC   : return properties set for mode Basic
**               WLS_SECMODE_WPA     : return properties set for mode WPA
**               WLS_SECMODE_WPA2    : return properties set for 11i.
**
** Return:       GetLastError to confirm success and the check response object
**               for object of type WLSSecurity
*******************************************************************************/
_st780_2.prototype.GetWirelessSecurity = function _st780_2_GetWirelessSecurity(ulFlag) 
{
  try {
    var oWLSSecurity = new _smapiutils.WLSSecurity;
    
    if (ulFlag == _constants.SMAPI_HEX_DONT_CARE) {          //return only what is set in modem
      if(!this.oCLI.EnableTR64()) return null;     // need tr64 to be enabled for this
      oWLSSecurity.ulSecMode = this.oTR64.GetWirelessBeaconType();
      if( (this.oSMAPI.GetLastError() != _constants.SMAPI_SUCCESS) ||
          (oWLSSecurity.ulSecMode == (oWLSSecurity.ulSecMode & _constants.WLS_SECMODE_NONE)) ){ 
         return oWLSSecurity;                     // error or no security we don't care
      } 
      // For Security mode Basic
      if(oWLSSecurity.ulSecMode == (oWLSSecurity.ulSecMode & _constants.WLS_SECMODE_BASIC)) {
        oWLSSecurity = this.oTR64.GetWirelessSecurity(WLS_SECMODE_BASIC);
        return oWLSSecurity;
      }  
      // For Security mode WPA
      if(oWLSSecurity.ulSecMode == (oWLSSecurity.ulSecMode & _constants.WLS_SECMODE_WPA)) {
        oWLSSecurity = this.GetWirelessSecurity(_constants.WLS_SECMODE_WPA);
        return oWLSSecurity;
      }  
      // For Security mode WPA2 or 11i
      if(oWLSSecurity.ulSecMode == (oWLSSecurity.ulSecMode & _constants.WLS_SECMODE_WPA2)) {
        this.oSMAPI.SetLastError(_constants.SMAPI_OPERATION_NOTSUPPORTED);
        return null;
      }  
    
    } else if (ulFlag == (ulFlag & _constants.WLS_SECMODE_BASIC)) {     //TR64
      oWLSSecurity = this.oTR64.GetWirelessSecurity(_constants.WLS_SECMODE_BASIC);
      return oWLSSecurity;
      
    } else if (ulFlag == (ulFlag & _constants.WLS_SECMODE_WPA)) {      // Non TR64 as no thomson implementation for it
      oWLSSecurity.ulSecMode = _constants.WLS_SECMODE_WPA;
      var oRet = this.GetWirelessWPASecurity();
      if (this.oSMAPI.GetLastError() == _constants.SMAPI_SUCCESS) {
        oWLSSecurity.ulAuthentication = oRet.ulAuthentication;
        oWLSSecurity.ulEncryption     = oRet.ulEncryption;
        // In WPA encryption is always on so call to get security keys
        // for pre shared key
        oWLSSecurity.oKeys.sPreSharedKey = oRet.sPreSharedKey;
        // user defined property being added
        oWLSSecurity.oUserDefined = { nWPAEnabled: oRet.nEnabled};
      }
      return oWLSSecurity;
      
    } else if (ulFlag == (ulFlag & _constants.WLS_SECMODE_WPA2)) {
      oWLSSecurity.ulSecMode = _constants.WLS_SECMODE_WPA2;
      this.oSMAPI.SetLastError(_constants.SMAPI_OPERATION_NOTSUPPORTED);
      return null;
    }
  } catch (ex) {
    _logger.error("_st780_2_GetWirelessSecurity", ex.message ); 
    this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
  }
}

/*******************************************************************************
** Name:         _st780_2_SetWirelessSecurity
**
** Purpose:      Applies Wireless security settings to the modem
**
** Parameter:    object of type WLSSecurity
**
** WLSSecurity() {  // all default values mean no operation to perform for those values
**                  // leave settings in modem as is
**  this.ulSecMode        = SMAPI_HEX_DONT_CARE;
**  this.ulAuthentication = SMAPI_HEX_DONT_CARE; 
**  this.ulEncryption     = SMAPI_HEX_DONT_CARE;
**  this.oKeys            = new WLSSecurityKeys();
**  this.oUserDefined     = SMAPI_OBJ_DONT_CARE; // for future expansion or use with 
**                                               // custom implementations
** }
**    
** Return:       GetLastError to confirm success 
*******************************************************************************/
_st780_2.prototype.SetWirelessSecurity = function _st780_2_SetWirelessSecurity(oWLSSecurity) 
{
  try {
    if(this.oSMAPI.m_bInQueue) {
      // No support for batch update in TR64 implementation of Thomson
      // hence fallback to CLI for this call.
      return this.oCLI.SetWirelessSecurity(oWLSSecurity);
    }  
    
    // Note if the wireless setting is wpa-psk or wep already then the wireless setting MUST be 
    // changed before it can be re-configured. To avoid calls to first get settings and 
    // appropriately Set settings we are just going to start clean..
    this.oTR64.SetWirelessBeaconType(_constants.WLS_SECMODE_NONE);
    if (this.oSMAPI.GetLastError() != _constants.SMAPI_SUCCESS) return;
    
    // Check if authentication and encryption are to be set
    if( oWLSSecurity.ulAuthentication  != _constants.SMAPI_HEX_DONT_CARE ||
        oWLSSecurity.ulEncryption != _constants.SMAPI_HEX_DONT_CARE) {
      switch(oWLSSecurity.ulSecMode) {
        case _constants.WLS_SECMODE_BASIC: 
          if(oWLSSecurity.ulEncryption == (oWLSSecurity.ulEncryption & _constants.WLS_ENCRYPT_WEP)) {
            // Thomson specific tweak as it only allows encryption WEP with secmode Basic
            this.oTR64.SetWirelessBasicSecurity(oWLSSecurity.ulAuthentication, oWLSSecurity.ulEncryption);
          
            // set wireless security keys (Do Thomson specific tweak i.e copy passpharase to WEPKey0)
            oWLSSecurity.oKeys.sWEPKey0 = oWLSSecurity.oKeys.sKeyPassphrase; 
            oWLSSecurity.oKeys.sKeyPassphrase = _constants.SMAPI_STR_DONT_CARE;
            this.oTR64.SetWirelessSecurityKeys(oWLSSecurity.oKeys);  
          } else {
            this.oSMAPI.SetLastError(_constants.SMAPI_OPERATION_NOTSUPPORTED);
            _logger.error("_st780_2_SetWirelessSecurity", "WEP Encryption is the only option allowed with Basic Security mode in Thomson");
          }   
          break;
        case _constants.WLS_SECMODE_WPA  :
        case _constants.WLS_SECMODE_WPA2 :   
          this.SetWirelessWPASecurity(oWLSSecurity);
          break;
      }
    }
    if (this.oSMAPI.GetLastError() != _constants.SMAPI_SUCCESS) return;
    
    // Now enable the requested security
    if( oWLSSecurity.ulSecMode != _constants.SMAPI_HEX_DONT_CARE) {
      this.oTR64.SetWirelessBeaconType(oWLSSecurity.ulSecMode);
    }  
      
  } catch (ex) {
    _logger.error("_st780_2_SetWirelessSecurity", ex.message ); 
    this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
  }
}

/*******************************************************************************
** Name:         _st780_2_GetWirelessWPASecurity
**
** Purpose:      
**
** Parameter:    none
**
** Return:       GetLastError to confirm success and then check the returned object
*******************************************************************************/
_st780_2.prototype.GetWirelessWPASecurity = function _st780_2_GetWirelessWPASecurity()
{
  return this.oCLI.GetWirelessWPASecurity();
}  

/*******************************************************************************
** Name:         _st780_2_SetWirelessWPASecurity
**
** Purpose:      
**
** Parameter:    none
**
** Return:       GetLastError to confirm success and then check the returned object
*******************************************************************************/
_st780_2.prototype.SetWirelessWPASecurity = function _st780_2_SetWirelessWPASecurity(oWLSSecurity)
{
  this.oCLI.SetWirelessWPASecurity(oWLSSecurity);
}  

/*******************************************************************************
** Name:         _st780_2_GetSSIDInfo 
**
** Purpose:      Gets Network name(SSID) and SSID broadcast values from modem. 
**               
** Parameter:    ulFlag : Helps implementations in knowing what is required
**               by caller at this particular time. This helps in reducing the
**               calls made to modem or targetting them better.
**
**               WLS_SSID_NAME:       caller needs only NewSSID value
**               WLS_SSID_BROADCAST:  caller needs only NewSSIDBroadcast value
**               WLS_SSID_NAME|WLS_SSID_BROADCAST: caller needs both  
**               SMAPI_HEX_DONT_CARE: special case again caller needs both
**
** Return:       GetLastError to confirm success and then check response object
**               for the following properties
**               NewSSID
**               NewSSIDBroadcastEnabled
*******************************************************************************/
_st780_2.prototype.GetSSIDInfo = function _st780_2_GetSSIDInfo(ulFlag) 
{
  try {
    var newObj = {};
    // Changing the meaning of don't care as nobody should call this function
    // if they don't care to get even a single value
    if(ulFlag == _constants.SMAPI_HEX_DONT_CARE) ulFlag = _constants.WLS_SSID_NAME|_constants.WLS_SSID_BROADCAST;
      
    // Get Network Name
    if(sma_mut_MaskIn(ulFlag, _constants.WLS_SSID_NAME) && this.oCLI.EnableTR64()) {
      newObj = this.oTR64.GetSSIDInfo(_constants.WLS_SSID_NAME);
    } 
    if(this.oSMAPI.GetLastError() != _constants.SMAPI_SUCCESS) return null;
    
    // Check Flag for broadcast
    if(!_smapiutils.MaskIn(ulFlag, _constants.WLS_SSID_BROADCAST)) return newObj;
     
    // Get SSID Broadcast value
    var oRet = this.oCLI.GetSSIDInfo(_constants.WLS_SSID_BROADCAST);
    if(this.oSMAPI.GetLastError() == _constants.SMAPI_SUCCESS) {
      newObj.NewSSIDBroadcastEnabled = oRet.NewSSIDBroadcastEnabled;
    }
      
    return newObj;
  } catch (ex) {
    _logger.error("_st780_2_GetSSIDInfo", ex.message ); 
    this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
  }
  return null;
}

/*******************************************************************************
** Name:         _st780_2_SetSSIDInfo
**
** Purpose:      Sets Network name(SSID) and SSID broadcast values to modem.
**
** Parameter:    object of type oWLSSSID
**
** WLSSSID() {   // all default values mean no operation to perform for those values
**               // leave settings in modem as is
**   this.sSSID            = SMAPI_STR_DONT_CARE;   // Server Set ID
**   this.nSSIDBroadcast   = SMAPI_INT_DONT_CARE;   // 1:On / 0: Off
** }
** 
**Return:       GetLastError to confirm success 
*******************************************************************************/
_st780_2.prototype.SetSSIDInfo = function _st780_2_SetSSIDInfo(oWLSSSID) 
{
  try {
    if(this.oSMAPI.m_bInQueue) {
      // No support for batch update in TR64 implementation of Thomson
      // hence fallback to CLI for this call.
      return this.oCLI.SetSSIDInfo(oWLSSSID);
    }  
    
    // Check if SSID is to be set
    if(oWLSSSID.sSSID != _constants.SMAPI_STR_DONT_CARE && this.oCLI.EnableTR64()) {
      var oSSID = new _smapiutils.WLSSSID();
      oSSID.sSSID = oWLSSSID.sSSID;
      this.oTR64.SetSSIDInfo(oSSID);
    }  
  
    // Check if SSID Broadcast is to be updated
    if(oWLSSSID.nSSIDBroadcast != _constants.SMAPI_INT_DONT_CARE &&
       this.oSMAPI.GetLastError() == _constants.SMAPI_SUCCESS) {
      var oSSID = new _smapiutils.WLSSSID();
      oSSID.nSSIDBroadcast = oWLSSSID.nSSIDBroadcast;
      this.oCLI.SetSSIDInfo(oSSID);
    }
      
  } catch(ex) {
    _logger.error("_st780_2_SetSSIDInfo", ex.message );
    this.oSMAPI.SetLastError(_constants.SMAPI_EXCEPTION);
  }
}

/*******************************************************************************
** Name:         _st780_2_QueueCommands
**
** Purpose:      By using this function caller is hinting to start queuing of all
**               the SMAPI calls made hence forth till a commit commands is 
**               received. Implementations can hook up their support for this
**               accordingly and see how they can do a batch update on CPE's in 
**               their code. They should return a true /false to tell if
**               queuing was successful. 
**
** Parameter:    none
**
** Return:       GetLastError to confirm success 
*******************************************************************************/
_st780_2.prototype.QueueCommands = function _st780_2_QueueCommands() 
{
  return this.oCLI.QueueCommands();
}

/*******************************************************************************
** Name:         _st780_1_CommitQueuedCommands
**
** Purpose:      By using this function caller is hinting to start queuing of all
**               the SMAPI calls made hence forth till a commit commands is 
**               received. Implementations can hook up their support for this
**               accordingly and see how they can do a batch up[date on CPE's in 
**               their code.
**
** Parameter:    none
**
** Return:       GetLastError to confirm success 
*******************************************************************************/
_st780_2.prototype.CommitQueuedCommands = function _st780_2_CommitQueuedCommands() 
{
  return this.oCLI.CommitQueuedCommands();
}