/*******************************************************************************
**
**  File Name: cli_imp.js
**
**  Summary: SMAPI derived class for Thomson st780 modem
**
**  Description: This javascript contains SMAPI derivative to support Thomson 
**               st780 implementation. It maps SMAPI functions to Thomson 
**               st780 specific calls in pure CLI Implementation
**
**  Dependencies: ss_netcheck.js
**                smapi.js
**           
**  Copyright SupportSoft Inc. 2005, All rights reserved.
*******************************************************************************/

var _logger = $ss.agentcore.log.GetDefaultLogger("$ss.snapin.modem.th780");
var _utils = $ss.agentcore.utils;
var _sysUtils = $ss.agentcore.utils.system
var _netCheck = $ss.agentcore.network.netcheck;
var _registry = $ss.agentcore.dal.registry;
var _file = $ss.agentcore.dal.file;
var _http = $ss.agentcore.dal.http;
var _config = $ss.agentcore.dal.config;
var _constants = $ss.agentcore.constants;
var _smapiutils = $ss.agentcore.smapi.utils;

// Login/Logout values for Execute command
var CLI_LOGIN    = 0x00000001;
var CLI_LOGOUT   = 0x00000002;
var CLI_NOOP     = 0x00000004;

// Checking Response for the following values
var CLI_RSP_USERNAME       = 0x00000001;
var CLI_RSP_PASSWORD       = 0x00000002;
var CLI_RSP_INVALID_CREDS  = 0x00000004;
var CLI_RSP_ID             = 0x00000008;
var CLI_RSP_SESS_INUSE     = 0x00000016;
var CLI_RSP_NONE           = 0x00000032;

//-------------------------------------------------------------------------------
// CWMP Authentication Types
//-------------------------------------------------------------------------------
var _oCWMP_AUTH_TYPES = {};
_oCWMP_AUTH_TYPES[_constants.CWMP_AUTH_NONE]    = "none";
_oCWMP_AUTH_TYPES[_constants.CWMP_AUTH_BASIC]   = "basic";
_oCWMP_AUTH_TYPES[_constants.CWMP_AUTH_DIGEST]  = "digest";


/*******************************************************************************
** Name:         _st780_cli
**
** Purpose:      Class that wraps SMAPI functionality and Thomson st780 
**               specific implementation for it. The class uses combination of 
**               CLI commands and TR64 protocol for its underlying functionality.
**
** Parameter:    oSMAPI     : An instance of SMAPI class is sent to derived 
**                            implementation to avail base class functionality
**               arProtocol : (optional) Array of base protocol implementations 
**                            could be sent for direct use in derived class 
**                            implementations Every protocol implementation has 
**                            a property sNAME. Checkthat before directly using 
**                            it.
** Return:       object
*******************************************************************************/
function _st780_cli(oSMAPI, arProtocol)
{
  this.oSMAPI = oSMAPI;
  this.oPWCtl = _utils.CreateActiveXObject("sdcuser.tgpassctl",true);
  this.mProperties         = this.oSMAPI.GetInstanceProperties();
  this.m_bIsConfigBackedUp = false; // tracks if we backed up the config or not
  this.m_bIsSaved          = true;  // tracks if we saved settings over reboots
  this.m_bTR64Enabled      = false; // assume that devices are shipped with TR64 disabled

  // tracks if need to login/logout or do by default do both
  this.m_ulExecCommand     = CLI_LOGIN|CLI_LOGOUT; 
  // For Queued operations
  this.m_sScriptName       = "sprtbatch";
  this.m_nScriptIndex      = 0;
}

/*******************************************************************************
** Name:         _st780_cli_ID
**
** Purpose:      Determine the unique identifier for the modem. This implemen-
**               tation is helpful at protocol level so that SMAPI can use it 
**               to infer modem specific implementations.
**
** Parameter:    none
**
** Return:       unique identifier of the modem matching modem xml file
*******************************************************************************/
_st780_cli.prototype.ID = function _st780_cli_ID()
{
  try {
    return this.mProperties.id;
  } catch(ex) {
    _logger.error("_st780_cli_ID", ex.message );
    this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
  }
}

/*******************************************************************************
** Name:         _st780_cli_Name
**
** Purpose:      Gets the modem name as defined in the modem.xml
**
** Parameter:    none
**
** Return:       str
*******************************************************************************/
_st780_cli.prototype.Name = function _st780_cli_Name() 
{
  try {
    return this.mProperties.name;
  } catch(ex) {
    _logger.error("_st780_cli_Name", ex.message );
    this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
  }
}

/*******************************************************************************
** Name:         _st780_cli_SerialNumber
**
** Purpose:      Get Serial Number for Thomson st780 modem.
**
** Parameter:    none
**
** Return:       string having Serial Number else empty
*******************************************************************************/
_st780_cli.prototype.SerialNumber = function _st780_cli_SerialNumber()
{
  try {
    var sResp = this.ExecuteCommand("env list");
    if(this.oSMAPI.GetLastError() == _constants.SMAPI_SUCCESS) 
      return this.pvt_MatchTag1(sResp, /\s+_PROD_SERIAL_NBR=([\w\S]+)/);
  } catch(ex) {
    _logger.error("_st780_cli_SerialNumber", ex.message );
    this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
  } 
  return _constants.SMAPI_EMPTY_STR;
}

/*******************************************************************************
** Name:         _st780_cli_MACAddress
**
** Purpose:      Get MACAddress for Thomson st780 modem.
**
** Parameter:    none
**
** Return:       string having MACAddress else empty
*******************************************************************************/
_st780_cli.prototype.MACAddress = function _st780_cli_MACAddress()
{
  try {
    var sResp = this.ExecuteCommand("env list");
    if(this.oSMAPI.GetLastError() == _constants.SMAPI_SUCCESS) 
      return this.pvt_MatchTag1(sResp, /\s+_MACADDR=([\w-\S]+)/);
  } catch(ex) {
    _logger.error("_st780_cli_MACAddress", ex.message );
    this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
  } 
  return _constants.SMAPI_EMPTY_STR;
}

/*******************************************************************************
** Name:         _st780_cli_Firmware
**
** Purpose:      Get firmware version Thomson st780 modem.
**
** Parameter:    none
**
** Return:       string having firmware else empty
*******************************************************************************/
_st780_cli.prototype.Firmware = function _st780_cli_Firmware()
{
  try {
    var sResp = this.ExecuteCommand("env list");
    if(this.oSMAPI.GetLastError() == _constants.SMAPI_SUCCESS) 
      return this.pvt_MatchTag1(sResp, /\s+_BUILD=([\d+\.\S]+)/);
  } catch(ex) {
    _logger.error("_st780_cli_Firmware", ex.message );
    this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
  } 
  return _constants.SMAPI_EMPTY_STR;
}

/*******************************************************************************
** Name:         _st780_cli_Detect
**
** Purpose:      Verifies that Thomson st780 is connected to the PC. Detection 
**               could fail if PC to modem connection is broken or modem 
**               connected is not with Thomson st780 modem.
**
** Parameter:    none
**
** Return:       true if detected.
*******************************************************************************/
_st780_cli.prototype.Detect = function _st780_cli_Detect()
{ 
  var nRet = 0;
  try {
    nRet = (_netCheck.Ping(this.mProperties.ip)) ? 1 : 0;
  } catch (ex) {
    _logger.error("_st780_cli_Detect", ex.message );
    this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
  }
  return nRet;
}

/*******************************************************************************
** Name:         _st780_cli_IsSync
**
** Purpose:      Determine if modem is in sync.  This is a required procedure 
**               and should determine as best as possible if we have sync.
**
** Parameter:    (none)
**
** Return:       true if we have sync
*******************************************************************************/
_st780_cli.prototype.IsSync = function _st780_cli_IsSync() 
{
  var nRet = 0;
  var oReg  = new RegExp("[\\s]{2,}","g");      // despacer regexp
  var sResp = this.ExecuteCommand("adsl info");
  if(this.oSMAPI.GetLastError() == _constants.SMAPI_SUCCESS) {
    sResp = sResp.replace(oReg, " ");  
    nRet  = (sResp.toLowerCase().indexOf("modemstate : up") != -1) ? 1 : 0;
  } 
  return nRet;
}

/*******************************************************************************
** Name:         _st780_cli_IsConnected
**
** Purpose:      Checks to see if modem has WAN IP connectivity.
**
** Parameter:    (none)
**
** Return:       0/1 based on WAN connectivity status
*******************************************************************************/
_st780_cli.prototype.IsConnected = function _st780_cli_IsConnected() 
{
  var nRet = 0;
  var sResp = this.ExecuteCommand("ip iflist");
  if(this.oSMAPI.GetLastError() == _constants.SMAPI_SUCCESS) {
    nRet = (sResp.toLowerCase().indexOf("down") == -1) ? 1 : 0;
  } 
  return nRet;
}

/*******************************************************************************
** Name:         _st780_cli_SetUserCreds
**
** Purpose:      Set user credentials on modem
**
** Parameter:    sName     :  User name
**               sPassword :  User passowrd 
**
** Return:       none; check GetLastError
*******************************************************************************/
_st780_cli.prototype.SetUserCreds = function _st780_cli_SetUserCreds(sName, sPassword) 
{
  try {
    // login and set the command
    if(this.pvt_ModemLogin()) {
      this.m_ulExecCommand = CLI_NOOP; 
    } else {
      return;
    }
    
    try {
      var sProtocol  = "ppp";
      var sInterface = "Internet";
      // Need to detatch interface for safety if connected
      var sCmd  = sProtocol + " ifdetach intf=" + sInterface; 
      var sResp = this.ExecuteCommand(sCmd, true);
      if(this.oSMAPI.GetLastError() != _constants.SMAPI_SUCCESS) return;
      
      // Now configure username and password
      sCmd  = sProtocol + " ifconfig intf=" + sInterface; 
      sCmd += " user="+ sName + " password=" + sPassword;
      sCmd += " status=up retryinterval=25 restart=on";
      sResp = this.ExecuteCommand(sCmd, true);
      if(this.oSMAPI.GetLastError() != _constants.SMAPI_SUCCESS) return;
      
      // commit new changes before reboot
      this.pvt_PersistChanges();
      
    } catch (ex) {
      _logger.error("_st780_cli_SetUserCreds", ex.message ); 
      this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
    }
    
    // only if login passed we would be here hence need to logout (cmd is setback inside logout) 
    this.pvt_ModemLogout();
  
  } catch (ex) {
    _logger.error("_st780_cli_SetUserCreds", ex.message ); 
    this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
  }
}

/*******************************************************************************
** Name:         _st780_cli_Reboot
**
** Purpose:      Checks to see if modem has WAN IP connectivity.
**
** Parameter:    (none)
**
** Return:       none; check GetLastError
*******************************************************************************/
_st780_cli.prototype.Reboot = function _st780_cli_Reboot() 
{
  try {
    // reboot
    this.ExecuteCommand("system reboot");
  } catch (ex) {
    _logger.error("_st780_cli_Reboot", ex.message ); 
    this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
  }
}

/*******************************************************************************
** Name:         _st780_cli_GetDeviceInfo
**
** Purpose:      Get GetDeviceInfo for Thomson st780 modem.
**
** Parameter:    none
**
** Return:       object having various device info parameters such as
**               NewManufacturerName
**               NewModelName
**               NewSerialNumber
**               NewFirmwareVersion
**               NewHardwareVersion
*******************************************************************************/
_st780_cli.prototype.GetDeviceInfo = function _st780_cli_GetDeviceInfo()
{
  var sResp = this.ExecuteCommand("env list");
  try {
    if(this.oSMAPI.GetLastError() == _constants.SMAPI_SUCCESS) {
      var newObj = new Object();
      //NewManufacturerName
      newObj.NewManufacturerName = this.pvt_MatchTag1(sResp, /\s+_COMPANY_NAME=([\w\S]+)/) ;
      
      //NewModelName
      newObj.NewModelName = this.pvt_MatchTag1(sResp, /_PROD_NAME=([\w\S]+)/) ;
      if(newObj.NewModelName != -1) {
        var arResult = sResp.match(/_PROD_NUMBER=([\w\S]+)/);
        if (arResult != null && arResult.length > 1)
          newObj.NewModelName += " " + arResult[1].substr(0,3);
        else
          newObj.NewModelName = -1;
      }
      
      //NewSerialNumber
      newObj.NewSerialNumber    = this.pvt_MatchTag1(sResp, /\s+_PROD_SERIAL_NBR=([\w\S]+)/);
          
      //NewFirmwareVersion
      newObj.NewFirmwareVersion = this.pvt_MatchTag1(sResp, /\s+_BUILD=([\d+\.\S]+)/);
      
      //NewHardwareVersion
      newObj.NewHardwareVersion = this.pvt_MatchTag1(sResp, /_BOARD_NAME=([\w-\S]+)/);
      
      return newObj;
    }  
  } catch(ex) {
    _logger.error("_st780_cli_GetDeviceInfo", ex.message );
    this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
  }
  return null; 
}

/*******************************************************************************
** Name:         _st780_cli_GetDSLLinkInfo
**
** Purpose:      Checks to see if modem has WAN IP connectivity.
**
** Parameter:    (none)
**
** Return:       obj if successful; else check GetLastError
*******************************************************************************/
_st780_cli.prototype.GetDSLLinkInfo = function _st780_cli_GetDSLLinkInfo() 
{ 
  try {
    var sResp = this.ExecuteCommand("adsl info");
    if(this.oSMAPI.GetLastError() == _constants.SMAPI_SUCCESS) {
      var newObj = new Object();
      sResp = sResp.replace(/\ +/g, "@@");  
      //Noice margin
      var arResult = sResp.match(/Margin([\w\@\.\S]+)/);
      if(arResult != null && arResult.length > 1) {
        var arTmp = arResult[1].split("@@");
        newObj.NewDownstreamNoiseMargin = arTmp[3];
        newObj.NewUpstreamNoiseMargin   = arTmp[4];
      }
      //Attenuation
      arResult = sResp.match(/Attenuation([\w\@\.\S]+)/);
      if(arResult != null && arResult.length > 1) {
        var arTmp = arResult[1].split("@@");
        newObj.NewDownstreamAttenuation = arTmp[3];
        newObj.NewUpstreamAttenuation   = arTmp[4];
      }
      //Output Power
      arResult = sResp.match(/OutputPower([\w\@\.\S]+)/);
      if(arResult != null && arResult.length > 1) {
        var arTmp = arResult[1].split("@@");
        newObj.NewDownstreamPower = arTmp[3];
        newObj.NewUpstreamPower   = arTmp[4];
      } 
      
      newObj.NewUpstreamCurrentRate   = -1;
      newObj.NewDownstreamCurrentRate = -1;
      return newObj;
    }
  } catch(ex) {
    _logger.error("_st780_cli_GetDSLLinkInfo", ex.message );
    this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
  }
  return null;
}  

/*******************************************************************************
** Name:         _st780_cli_SetCWMPInfo
**
** Purpose:      Sets the CPE WAN Management Protocol parameters on the modem
**
** Parameter:    Object of type CWMPInfo  
** 
** CWMPInfo() //Support for CPE WAN Management Protocol
** {
**   this.sACSUrl        = _constants.SMAPI_STR_DONT_CARE; 
**            // indicates the Url of the management server (ACS)
**   this.ulACSAuthType  = _constants.SMAPI_HEX_DONT_CARE; 
**            // Authentication used by ACS when CPE communicates
**   this.sACSUsername   = _constants.SMAPI_STR_DONT_CARE; 
**            // set/get Username that ACS will use to Authenticate CPE
**   this.sACSPassword   = _constants.SMAPI_STR_DONT_CARE; 
**            // set/get Password that ACS will use to Authenticate CPE
**   this.ulCPEAuthType  = _constants.SMAPI_HEX_DONT_CARE; 
**            // Authentication used by CPE when ACS communicates
**   this.sCPEUsername   = _constants.SMAPI_STR_DONT_CARE; 
**            // set/get Username that CPE will use to Authenticate ACS
**   this.sCPEPassword   = _constants.SMAPI_STR_DONT_CARE; 
**            // set/get Password that CPE will use to Authenticate ACS
**   this.oUserDefined   = _constants.SMAPI_OBJ_DONT_CARE; 
**            // for future expansion or use with custom implementations
** }
**
** Return:       GetLastError to confirm success 
*******************************************************************************/
_st780_cli.prototype.SetCWMPInfo = function _st780_cli_SetCWMPInfo(oCWMPInfo) 
{
  try {
    // login and set the command
    if(this.pvt_ModemLogin()) {
      this.m_ulExecCommand = CLI_NOOP; 
    } else {
      return;
    }   
    
    try {
      var bCommit = false;
      var sCmd = "cwmp server config";
      
      // Set ACS params on CPE for WAN Management
      if(oCWMPInfo.sACSUrl != _constants.SMAPI_STR_DONT_CARE) {
        sCmd += " url=" + oCWMPInfo.sACSUrl;
      }
      
      if(oCWMPInfo.sACSUsername != _constants.SMAPI_STR_DONT_CARE) {
        sCmd += " username=" + oCWMPInfo.sACSUsername;
      }
      
      if(oCWMPInfo.sACSPassword != _constants.SMAPI_STR_DONT_CARE) {
        sCmd += " password=" + oCWMPInfo.sACSPassword;
      }
      
      var sCmdOrig = "cwmp server config";   
      if(sCmd.length > sCmdOrig.length) {
        this.ExecuteCommand(sCmd, true);
        if(this.oSMAPI.GetLastError() != _constants.SMAPI_SUCCESS) {
          this.pvt_ModemLogout();
          return;
        }  
        bCommit = true;
      }  
      
      //Set CPE params on CPE for WAN Management
      sCmd = "cwmp config";
      if(oCWMPInfo.ulCPEAuthType != _constants.SMAPI_HEX_DONT_CARE) {
        sCmd += " connectionReqAuth=" + _oCWMP_AUTH_TYPES[oCWMPInfo.ulCPEAuthType];
      }  
      
      if(oCWMPInfo.sCPEUsername != _constants.SMAPI_STR_DONT_CARE) {
        sCmd += " connectionReqUserName=" + oCWMPInfo.sCPEUsername;
      }
      
      if(oCWMPInfo.sCPEPassword != _constants.SMAPI_STR_DONT_CARE) {
        sCmd += " connectionReqPsswd=" + oCWMPInfo.sCPEPassword;
      }
        
      var sCmdOrig = "cwmp config";   
      if(sCmd.length > sCmdOrig.length) {
        sCmd += " state=enabled connectionRequest=enabled";
        this.ExecuteCommand(sCmd, true);
        if(this.oSMAPI.GetLastError() != _constants.SMAPI_SUCCESS) {
          this.pvt_ModemLogout();
          return;
        }  
        bCommit = true;
      }
      
      if(bCommit) {
      
        // DB: Load Object : Fix for Thomson modems to talk to Service Gateway
        sCmd = "mbus loadobjects";
        this.ExecuteCommand(sCmd, true);
        if(this.oSMAPI.GetLastError() != _constants.SMAPI_SUCCESS) {
          this.pvt_ModemLogout();
          return;
        }  
        _smapiutils.g_oSMAPIDependencies.Sleep(4000); // this operation is time consuming
        
        // commit new changes 
        this.pvt_PersistChanges();
      }  
      
    } catch (ex) {
      _logger.error("_st780_cli_SetCWMPInfo", ex.message ); 
      this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
    }
    
    // only if login passed we would be here hence need to logout (cmd is setback inside logout) 
    this.pvt_ModemLogout();
    
  } catch (ex) {
    _logger.error("_st780_cli_SetCWMPInfo", ex.message ); 
    this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
  }  
}

/*******************************************************************************
** Name:         _st780_cli_UpgradeFirmware
**
** Purpose:      Sets the ACS values such as URL, Shared Secret etc
**
** Parameter:    bDownload : true means the firmware has to be downloaded 
**                           from a site
**
**               sUrl      : ACS URL also called CPE WAN Management Server URL 
**
** Return:       GetLastError to confirm success 
*******************************************************************************/
_st780_cli.prototype.UpgradeFirmware = function _st780_cli_UpgradeFirmware(bDownload, sUrl) 
{
  var bRet = false;
  try {
    var sTempPath = _config.ExpandSysMacro("%TEMP%st780_firmware");
    var sFileName = sTempPath + "\\build_images\\" + this.mProperties.firmware_filename; 
    var sSrcDir   = this.mProperties.firmware_path.replace(/\//g, "\\");
    
    // first copy utilities needed for firmware upgrade from source directory 
    // to temp directory on machine
    _logger.debug("_st780_cli_UpgradeFirmware", "src: " + sSrcDir);
    _logger.debug("_st780_cli_UpgradeFirmware", "dest: " + sTempPath);
    _file.CopyDir(sSrcDir, sTempPath);
    
    // check if firmware is to be downloaded
    if(bDownload) {
      this.oSMAPI.SendMsg({type:_constants.SMAPI_MSG_FILE_DOWNLOAD});
      _logger.debug("_st780_cli_UpgradeFirmware", "Start File Download, Url: " + sUrl);
      
      // TODO: Get timeout. username, password from config for download if required
      bRet = _http.GetFile(sFileName, sUrl, 300000, "", "");
      if(!bRet) { // try once more
        bRet = _http.GetFile(sFileName, sUrl, 300000, "", "");
      }
    } else {
      bRet = true;
    }  
    
    if(bRet) {
      // Download was successfull hence read the stInstall.config and change values
      if(this.pvt_UpdateFirmwareConfig(sTempPath)) {
        // start upgrade
        var sRun = sTempPath + "\\stInstall.exe";
        this.oSMAPI.SendMsg({type:_constants.SMAPI_MSG_FIRMWARE_UPGRADE});
        _logger.debug("_st780_cli_UpgradeFirmware", "Start Run Silent Installer: " + sRun);
        _sysUtils.RunCommand(sRun, BCONT_RUNCMD_NORMAL);
        _logger.debug("_st780_cli_UpgradeFirmware", "Finish Silent Installer ");
      } else {
        bRet = false; 
      }  
    } 
    
    if(!bRet) {
      this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_FAIL);
    } else { 
      // clean up
      try {
        _file.DeleteDir(sTempPath, true);
      } catch(ex) {}
    }
  } catch (ex) {
    _logger.error("_st780_cli_UpgradeFirmware", ex.message ); 
    this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
  }
  return bRet;
}

/*******************************************************************************
** Name:         _st780_cli_GetWirelessInfo
**
** Purpose:      Retrieves all the state variables other than the wireless 
**               statistics and security keys. 
**
** Parameter:    none
**
** Return:       GetLastError to confirm success and then check response object
**               for the following properties
**               NewEnable
**               NewStatus
**               NewMaxBitRate
**               NewChannel
**               NewMACAddressControlEnabled
**               NewStandard
**               NewBSSID
**               ---------------------------------------------------------------
**               Optional for vendor specific implementations if not TR64 as 
**               proprietary implementations can use other SMAPI calls as well
**               to return below information
**               NewSSID                           OR use GetWirelessSSID
**               NewSecurityMode                   OR use GetWirelessSecurity
**               NewBasicEncryptionModes           OR use GetWirelessSecurity
**               NewBasicAuthenticationMode        OR use GetWirelessSecurity
*******************************************************************************/
_st780_cli.prototype.GetWirelessInfo = function _st780_cli_GetWirelessInfo() 
{
  try {
    var sCmd = "wireless ifconfig";
    var sResp = this.ExecuteCommand(sCmd);
    if(this.oSMAPI.GetLastError() == _constants.SMAPI_SUCCESS) {
      var newObj = new Object();
      
      // Enable and Status
      newObj.NewEnable = this.pvt_MatchTag1(sResp, / *State *: *([\w\S]+)/, "enabled");
      newObj.NewStatus = (newObj.NewEnable) ? "Up" : "Disconnected"
      
      // Channel
      newObj.NewChannel = this.pvt_MatchTag1(sResp, / *Channel *: *([\w\S]+)/);
      newObj.NewChannelSelection = this.pvt_MatchTag1(sResp, / *Channel *: *.*\[([\w\S]+)\]/);
      
      // Max Bit Rate (TBD: TR64 would have set it to Auto. Do we want to do that here?) 
      newObj.NewMaxBitRate = this.pvt_MatchTag1(sResp, / *Rate *: *([\w\S]+)/);
      
      // MACAddressControlEnabled
      newObj.NewMACAddressControlEnabled = this.GetWirelessMACACLEnabled();
      
      // Standard
      var std = this.pvt_MatchTag1(sResp, / *Interoperability *: *([\w\(\)\/\S]+)/);
      switch(std) {
        case "802.11b" : 
          newObj.NewStandard = _constants.WLS_STD_802_11B;
          break;
        case "802.11b(legacy)/g" : 
        case "802.11b/g" : 
          newObj.NewStandard = _constants.WLS_STD_802_11B|_constants.WLS_STD_802_11G;
          break;
        case "802.11g"   : 
          newObj.NewStandard = _constants.WLS_STD_802_11G;
          break;
        default: newObj.NewStandard = -1;
      }
      
      // SSID
      newObj.NewSSID = this.pvt_MatchTag1(sResp, / *Network name.*: *([\w\S]+)/);
      // SSID Broadcast
      newObj.NewSSIDBroadcastEnabled = this.pvt_MatchTag1(sResp, / *Public network.*: *([\w\S]+)/, "enabled");
      
      // NOT IMPLEMENTED LIST
      newObj.NewBSSID                   = -1;      
      newObj.NewSecurityMode            = -1;
      newObj.NewBasicEncryptionModes    = -1;
      newObj.NewBasicAuthenticationMode = -1;
      
      return newObj;
    }  
  } catch(ex) {
    _logger.error("_st780_cli_GetWirelessInfo", ex.message );
    this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
  }
  return null; 
}

/*******************************************************************************
** Name:         _st780_cli_GetWirelessSecurity
**
** Purpose:      Retrieves all the security variables requested by caller.
**
** Parameter:    ulFlag : Helps implementations in knowing what is required
**               by caller at this particular time. This helps in reducing the
**               calls made to modem or targetting them better.
**               _constants.SMAPI_HEX_DONT_CARE : return only what is set in modem
**               WLS_SECMODE_BASIC   : return properties set for mode Basic
**               WLS_SECMODE_WPA     : return properties set for mode WPA
**               WLS_SECMODE_WPA2    : return properties set for 11i.
**
** Return:       GetLastError to confirm success and the check response object
**               for object of type WLSSecurity
*******************************************************************************/
_st780_cli.prototype.GetWirelessSecurity = function _st780_cli_GetWirelessSecurity(ulFlag) 
{
  try {
    var oWLSSecurity = new _smapiutils.WLSSecurity;
    
    if (ulFlag == _constants.SMAPI_HEX_DONT_CARE) {          //return only what is set in modem
      
      var sCmd  = "wireless secmode config";
      var sResp = this.ExecuteCommand(sCmd);
      if(this.oSMAPI.GetLastError() == _constants.SMAPI_SUCCESS) {
        //Check response for Security level
        var nRet = this.pvt_MatchTag1(sResp, /Security level *(.+):/, "WEP");
        
        if(nRet == 1) {       // Security is set to WEP
          oWLSSecurity = this.GetWirelessBasicSecurity();
        
        } else if (nRet == 0) {   // Security is set to WPA-PSK
          oWLSSecurity = this.GetWirelessWPASecurity();
          
        } else {              // -1 case, Security is set to none
          // pvt_MatchTag1(sResp, /No security enabled/);
          oWLSSecurity.ulSecMode        = _constants.WLS_SECMODE_NONE;
          oWLSSecurity.ulEncryption     = _constants.WLS_ENCRYPT_NONE;
          oWLSSecurity.ulAuthentication = _constants.WLS_AUTH_OPEN; 
          
        }
        return oWLSSecurity;    
      }
    }
      
    if(ulFlag == (ulFlag & _constants.WLS_SECMODE_BASIC)) {         // For Security mode Basic
      oWLSSecurity = this.GetWirelessBasicSecurity();
    } else if (ulFlag == (ulFlag & _constants.WLS_SECMODE_WPA)) {   // For Security mode WPA
      oWLSSecurity = this.GetWirelessWPASecurity(WLS_SECMODE_WPA);
    } else if (ulFlag == (ulFlag & _constants.WLS_SECMODE_WPA2)) {  // For Security mode WPA2
      this.oSMAPI.SetLastError(_constants.SMAPI_OPERATION_NOTSUPPORTED);
      return null;
    }
    return oWLSSecurity;
  } catch (ex) {
    _logger.error("_st780_cli_GetWirelessSecurity", ex.message ); 
    this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
  }
}

/*******************************************************************************
** Name:         _st780_cli_SetWirelessConfig  
**
** Purpose:      Applies wireless configuration to the modem
**
** Parameter:    object of type WLSSecurity
** 
** WLSConfig() {   // all default values mean no operation to perform for those 
**                 // values leave settings in modem as is
**   this.nEnable      = _constants.SMAPI_INT_DONT_CARE; // set wireless radio mode in 
**                 // other words enable/disable wireless interface 1:on/0:off 
**   this.sMaxBitRate  = _constants.SMAPI_STR_DONT_CARE; // Auto, OR the largest of 
**                 // the OperationalTrxRates values in (Mbps)
**   this.nChannel     = _constants.SMAPI_INT_DONT_CARE; // set wireless channel
**   this.ulStandard   = _constants.SMAPI_HEX_DONT_CARE; // sets the IEEE 802.11 standard 
**                 // the device is operating in. Not implemented in TR64
**   this.nEnableMACL  = _constants.SMAPI_INT_DONT_CARE; 
**                 // set MAC Address control filtering 1:on/0:off
**   this.oWLSDataTransmission = _constants.SMAPI_OBJ_DONT_CARE; 
**                 // new WLSDataTransmission(); 
**                 // In general used for setting frame transmission rates. 
**   this.oWLSSSID     = _constants.SMAPI_OBJ_DONT_CARE; // new WLSSSID(); set SSID Info
**   this.oWLSSecurity = _constants.SMAPI_OBJ_DONT_CARE; // new WLSSecurity();   set wireless security                                          
**   this.oUserDefined = _constants.SMAPI_OBJ_DONT_CARE; // for future expansion or use with custom implementations
** }
**
** Return:       GetLastError to confirm success 
*******************************************************************************/
_st780_cli.prototype.SetWirelessConfig = function _st780_cli_SetWirelessConfig(oWLSConfig) 
{
  try {
    // login and set the command
    if(this.pvt_ModemLogin()) {
      this.m_ulExecCommand = CLI_NOOP; 
    } else {
      return;
    }   

    try {
      // Enable or Disable the wireless interface/radio mode
      if(oWLSConfig.nEnable != _constants.SMAPI_INT_DONT_CARE) {
        var sCmd = "wireless ifconfig state=";
        sCmd += (oWLSConfig.nEnable) ? "enabled" : "disabled";
        this.ExecuteCommand(sCmd, true);
        if(this.oSMAPI.GetLastError() != _constants.SMAPI_SUCCESS) {
          this.pvt_ModemLogout();
          return;
        }  
      }
      
      // set MAC Address control filtering 1:on/0:off
      if(oWLSConfig.nEnableMACL != _constants.SMAPI_INT_DONT_CARE) {
        this.SetWirelessMACACLEnabled(oWLSConfig.nEnableMACL);
        if(this.oSMAPI.GetLastError() != _constants.SMAPI_SUCCESS) {
          this.pvt_ModemLogout();
          return;
        }  
      }
      
      // set SSID Info
      if(oWLSConfig.oWLSSSID != _constants.SMAPI_OBJ_DONT_CARE) {
        this.SetSSIDInfo(oWLSConfig.oWLSSSID);
        if(this.oSMAPI.GetLastError() != _constants.SMAPI_SUCCESS) {
          this.pvt_ModemLogout();
          return;
        }  
      }
      
      // set wireless security
      if(oWLSConfig.oWLSSecurity != _constants.SMAPI_OBJ_DONT_CARE) {
        this.SetWirelessSecurity(oWLSConfig.oWLSSecurity);
        if(this.oSMAPI.GetLastError() != _constants.SMAPI_SUCCESS) {
          this.pvt_ModemLogout();
          return;
        }  
      }
      
      // commit new changes before reboot
      // this.pvt_PersistChanges();
    } catch(ex) {
      _logger.error("_st780_cli_SetWirelessConfig", ex.message );
      this.oSMAPI.SetLastError(_constants.SMAPI_EXCEPTION);
    }

    // only if login passed we would be here hence need to logout (cmd is setback inside logout) 
    this.pvt_ModemLogout();
     
  } catch(ex) {
    _logger.error("_st780_cli_SetWirelessConfig", ex.message );
    this.oSMAPI.SetLastError(_constants.SMAPI_EXCEPTION);
  }
}

/*******************************************************************************
** Name:         _st780_cli_SetWirelessSecurity
**
** Purpose:      Applies Wireless security settings to the modem
**
** Parameter:    object of type WLSSecurity
**
** WLSSecurity() {  // all default values mean no operation to perform for those values
**                  // leave settings in modem as is
**  this.ulSecMode        = _constants.SMAPI_HEX_DONT_CARE;
**  this.ulAuthentication = _constants.SMAPI_HEX_DONT_CARE; 
**  this.ulEncryption     = _constants.SMAPI_HEX_DONT_CARE;
**  this.oKeys            = new WLSSecurityKeys();
**  this.oUserDefined     = _constants.SMAPI_OBJ_DONT_CARE; // for future expansion or use with 
**                                               // custom implementations
** }
**    
** Return:       GetLastError to confirm success 
*******************************************************************************/
_st780_cli.prototype.SetWirelessSecurity = function _st780_cli_SetWirelessSecurity(oWLSSecurity) 
{
  try {
    // Note if the wireless setting is wpa-psk or wep already then the wireless setting MUST be 
    // changed before it can be re-configured. To avoid calls to first get settings and 
    // appropriately Set settings we are just going to start clean..
    this.SetWirelessBeaconType(_constants.WLS_SECMODE_NONE);
    if(this.oSMAPI.GetLastError() != _constants.SMAPI_SUCCESS) return;
    
    // Check if authentication and encryption are to be set
    if( oWLSSecurity.ulAuthentication  != _constants.SMAPI_HEX_DONT_CARE ||
        oWLSSecurity.ulEncryption != _constants.SMAPI_HEX_DONT_CARE) {
      switch(oWLSSecurity.ulSecMode) {
        case _constants.WLS_SECMODE_NONE:
          // Authentication open and no encryption is supported in Thomson
          // and that gets set by default when security is disabled so do
          // nothing here
          break;
        case _constants.WLS_SECMODE_BASIC: 
          if(oWLSSecurity.ulEncryption == (oWLSSecurity.ulEncryption & _constants.WLS_ENCRYPT_WEP)) {
            // Thomson only allows encryption WEP with Security Basic
            this.SetWirelessBasicSecurity(oWLSSecurity);
          } else {
            this.oSMAPI.SetLastError(_constants.SMAPI_OPERATION_NOTSUPPORTED);
            _logger.error("_st780_cli_SetWirelessSecurity", "WEP Encryption is the only option allowed with Basic Security mode in Thomson");
          }   
          break;
        case _constants.WLS_SECMODE_WPA  :
        case _constants.WLS_SECMODE_WPA2 :   
          this.SetWirelessWPASecurity(oWLSSecurity);
          break;
      }
    }
    if (this.oSMAPI.GetLastError() != _constants.SMAPI_SUCCESS) return;
    
    // Now enable the requested security mode
    if( oWLSSecurity.ulSecMode != _constants.SMAPI_HEX_DONT_CARE &&
        oWLSSecurity.ulSecMode != (oWLSSecurity.ulSecMode & _constants.WLS_SECMODE_NONE)) {
      this.SetWirelessBeaconType(oWLSSecurity.ulSecMode);
    }  
    
    // commit new changes before reboot
    this.pvt_PersistChanges();
        
  } catch (ex) {
    _logger.error("_st780_cli_SetWirelessSecurity", ex.message ); 
    this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
  }
}

/*******************************************************************************
** Name:         _st780_cli_SetWirelessBeaconType 
**
** Purpose:      Sets Wireless security mode or beacon type
**
** Parameter:    ulFlag : Having the security mode to be set
**
** Return:       GetLastError to confirm success and then check the returned object
*******************************************************************************/
_st780_cli.prototype.SetWirelessBeaconType  = function _st780_cli_SetWirelessBeaconType(ulFlag)
{
  try {
    // So disable it always before configuring
    var sCmd = "wireless secmode config mode=";
    switch(ulFlag) {
      case _constants.WLS_SECMODE_NONE:
        sCmd += "disable";
        break;
      case _constants.WLS_SECMODE_BASIC: 
        sCmd += "wep";
        break;
      case _constants.WLS_SECMODE_WPA  :
      case _constants.WLS_SECMODE_WPA2 :   
        sCmd += "wpa-psk";
        break;
      default:
        this.oSMAPI.SetLastError(_constants.SMAPI_INVALID_ARGUMENT);  
    }
      
    if(this.oSMAPI.GetLastError() != _constants.SMAPI_SUCCESS) return;  
    this.ExecuteCommand(sCmd, true);
  } catch (ex) {
    _logger.error("_st780_cli_SetWirelessBeaconType", ex.message ); 
    this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
  }  
}   
  
/*******************************************************************************
** Name:         _st780_cli_GetWirelessBasicSecurity
**
** Purpose:      
**
** Parameter:    none
**
** Return:       GetLastError to confirm success and then check the returned object
*******************************************************************************/
_st780_cli.prototype.GetWirelessBasicSecurity = function _st780_cli_GetWirelessBasicSecurity()
{
  try {
    var sCmd = "wireless secmode wep";
    var sResp = this.ExecuteCommand(sCmd);
    if(this.oSMAPI.GetLastError() != _constants.SMAPI_SUCCESS) return null;
     
    var oWLSSecurity = new _smapiutils.WLSSecurity(); 
    // hard coded thomson values as nothing else is supported when 
    // basic security is turned on
    oWLSSecurity.ulSecMode        = _constants.WLS_SECMODE_BASIC;
    oWLSSecurity.ulEncryption     = _constants.WLS_ENCRYPT_WEP;
    oWLSSecurity.ulAuthentication = _constants.WLS_AUTH_OPEN; 
 
    var wepKey = this.pvt_MatchTag1(sResp, / *encryption key *: *([\w\S]+)/);
    if(wepKey == -1) {
      _logger.error("_st780_cli_GetWirelessBasicSecurity", "No WEP encryption key found, this is not possible with Thomson");
      this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_FAIL);
    } else if (wepKey.length == 10 | wepKey.length == 26) {
      oWLSSecurity.oKeys.sWEPKey0 = wepKey;
    } else {
      oWLSSecurity.oKeys.sKeyPassphrase = wepKey;  
    }  
    
    // Get Enabled flag
    var nEnabled = this.pvt_MatchTag1(sResp, / *enabled *: *([\w\S]+)/, "yes");
    // user defined property being added
    oWLSSecurity.oUserDefined = { nWPAEnabled: nEnabled};
        
    return oWLSSecurity;  
  } catch (ex) {
    _logger.error("_st780_cli_GetWirelessBasicSecurity", ex.message ); 
    this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
  }
  return null;
}  

/*******************************************************************************
** Name:         _st780_cli_SetWirelessBasicSecurity
**
** Purpose:      
**
** Parameter:    none
**
** Return:       GetLastError to confirm success and then check the returned object
*******************************************************************************/
_st780_cli.prototype.SetWirelessBasicSecurity = function _st780_cli_SetWirelessBasicSecurity(oWLSSecurity)
{
  try {
    // For Thomson authentication doesn't matter. Based on what encryption is to be 
    // set we'll form the command
    var sCmd = "wireless secmode wep";
    var sPassphrase = _constants.SMAPI_EMPTY_STR;
    
    // Giving wep key priority over passphrase as there is only one way
    // to set an encryption key in Thomson
    if(oWLSSecurity.oKeys.sWEPKey0 != _constants.SMAPI_STR_DONT_CARE) {
      sPassphrase = " encryptionkey = \"" + oWLSSecurity.oKeys.sWEPKey0 + "\"";
    } else if (oWLSSecurity.oKeys.sKeyPassphrase != _constants.SMAPI_STR_DONT_CARE) {
      sPassphrase = " encryptionkey = \"" + oWLSSecurity.oKeys.sKeyPassphrase + "\"";
    } else {
      _logger.error("_st780_cli_SetWirelessBasicSecurity", "Not allowed to set WEP encryption with empty sKeyPassphrase and sWEPKey0");
      this.oSMAPI.SetLastError(_constants.SMAPI_INVALID_ARGUMENT)
    }  
    
    sCmd += sPassphrase;
    this.ExecuteCommand(sCmd, true);
    
  } catch (ex) {
    _logger.error("_st780_cli_SetWirelessBasicSecurity", ex.message ); 
    this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
  }
}  

/*******************************************************************************
** Name:         _st780_cli_GetWirelessWPASecurity
**
** Purpose:      
**
** Parameter:    none
**
** Return:       GetLastError to confirm success and then check the returned object
*******************************************************************************/
_st780_cli.prototype.GetWirelessWPASecurity = function _st780_cli_GetWirelessWPASecurity()
{
  try {
    var sCmd = "wireless secmode wpa-psk";
    var sResp = this.ExecuteCommand(sCmd);
    if(this.oSMAPI.GetLastError() != _constants.SMAPI_SUCCESS) return null;
     
    var oWLSSecurity = new _smapiutils.WLSSecurity();
    oWLSSecurity.ulSecMode  = _constants.WLS_SECMODE_WPA;
    
    // Get the preshared key 
    var pskKey = this.pvt_MatchTag1(sResp, / *preshared key *: *([\w\S]+)/);
    if(pskKey == -1) {
      _logger.error("_st780_cli_GetWirelessWPASecurity", "No Preshared key found, this is not possible with Thomson");
      this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_FAIL);
    } else {
      oWLSSecurity.oKeys.sPreSharedKey  = pskKey;
      oWLSSecurity.ulAuthentication = _constants.WLS_AUTH_PSK;
    } 
    
    // Get encryption
    oWLSSecurity.ulEncryption = this.pvt_MatchTag1(sResp, / *encryption *: *([\w\S]+)/);
    if(oWLSSecurity.ulEncryption == "TKIP") 
      oWLSSecurity.ulEncryption = _constants.WLS_ENCRYPT_TKIP;
    else if(oWLSSecurity.ulEncryption == "AES") 
      oWLSSecurity.ulEncryption = _constants.WLS_ENCRYPT_AES;
    else if(oWLSSecurity.ulEncryption == "TKIP+AES")
      oWLSSecurity.ulEncryption = _constants.WLS_ENCRYPT_TKIP|_constants.WLS_ENCRYPT_AES;
    else 
      oWLSSecurity.ulEncryption = _constants.SMAPI_HEX_DONT_CARE;
    
    // Get Enabled flag
    var nEnabled = this.pvt_MatchTag1(sResp, / *enabled *: *([\w\S]+)/, "yes");
    // user defined property being added
    oWLSSecurity.oUserDefined = { nWPAEnabled: nEnabled};
    
    return oWLSSecurity;  
  } catch (ex) {
    _logger.error("_st780_cli_GetWirelessWPASecurity", ex.message ); 
    this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
  }
  return null;
}  

/*******************************************************************************
** Name:         _st780_cli_SetWirelessWPASecurity
**
** Purpose:      
**
** Parameter:    none
**
** Return:       GetLastError to confirm success and then check the returned object
*******************************************************************************/
_st780_cli.prototype.SetWirelessWPASecurity = function _st780_cli_SetWirelessWPASecurity(oWLSSecurity)
{
  try {
    // For Thomson authentication doesn't matter. Based on what encryption is to be 
    // set we'll form the command
    var sCmd = "wireless secmode wpa-psk";
    var sPresharedKey = _constants.SMAPI_EMPTY_STR;
    
    if(oWLSSecurity.oKeys.sPreSharedKey != _constants.SMAPI_STR_DONT_CARE) {
      sPresharedKey = " presharedkey = \"" + oWLSSecurity.oKeys.sPreSharedKey + "\"";
    }
    
    if(oWLSSecurity.ulEncryption == _constants.WLS_ENCRYPT_TKIP) {
      sCmd += sPresharedKey;
    } else if ( oWLSSecurity.ulEncryption == _constants.WLS_ENCRYPT_AES) {
      sCmd += sPresharedKey + " version=WPA2";
    } else if ( oWLSSecurity.ulEncryption == (_constants.WLS_ENCRYPT_TKIP|_constants.WLS_ENCRYPT_AES)) {
      sCmd += sPresharedKey + " version=WPA+WPA2";
    } else {
      _logger.error("_st780_cli_SetWirelessWPASecurity", "Allowed encryptions are TKIP, AES and TKIP|AES");
      this.oSMAPI.SetLastError(_constants.SMAPI_OPERATION_NOTSUPPORTED);
    }  
    
    if(this.oSMAPI.GetLastError() != _constants.SMAPI_SUCCESS) return;
    this.ExecuteCommand(sCmd, true);
    
  } catch (ex) {
    _logger.error("_st780_cli_SetWirelessWPASecurity", ex.message ); 
    this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
  }
}  

/*******************************************************************************
** Name:         _st780_cli_GetSSIDInfo 
**
** Purpose:      Gets Network name(SSID) and SSID broadcast values from modem. 
**               
** Parameter:    ulFlag : Helps implementations in knowing what is required
**               by caller at this particular time. This helps in reducing the
**               calls made to modem or targetting them better.
**
**               WLS_SSID_NAME:       caller needs only NewSSID value
**               WLS_SSID_BROADCAST:  caller needs only NewSSIDBroadcast value
**               WLS_SSID_NAME|WLS_SSID_BROADCAST: caller needs both  
**               _constants.SMAPI_HEX_DONT_CARE: special case again caller needs both
**
** Return:       GetLastError to confirm success and then check response object
**               for the following properties
**               NewSSID
**               NewSSIDBroadcastEnabled
*******************************************************************************/
_st780_cli.prototype.GetSSIDInfo = function _st780_cli_GetSSIDInfo(ulFlag) 
{
  try {
    var sCmd = "wireless ifconfig";
    var sResp = this.ExecuteCommand(sCmd);
    if(this.oSMAPI.GetLastError() != _constants.SMAPI_SUCCESS) return null;
    
    var newObj = {};
    // SSID
    newObj.NewSSID = this.pvt_MatchTag1(sResp, / *Network name.*: *([\w\S]+)/);
    // SSID Broadcast
    newObj.NewSSIDBroadcastEnabled = this.pvt_MatchTag1(sResp, / *Public network.*: *([\w\S]+)/, "enabled");
    
    return newObj;
  } catch (ex) {
    _logger.error("_st780_cli_GetSSIDInfo", ex.message ); 
    this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
  }
  return null;
}

/*******************************************************************************
** Name:         _st780_cli_SetSSIDInfo
**
** Purpose:      Sets Network name(SSID) and SSID broadcast values to modem.
**
** Parameter:    object of type oWLSSSID
**
** WLSSSID() {   // all default values mean no operation to perform for those values
**               // leave settings in modem as is
**   this.sSSID            = _constants.SMAPI_STR_DONT_CARE;   // Server Set ID
**   this.nSSIDBroadcast   = _constants.SMAPI_INT_DONT_CARE;   // 1:On / 0: Off
** }
** 
**Return:       GetLastError to confirm success 
*******************************************************************************/
_st780_cli.prototype.SetSSIDInfo = function _st780_cli_SetSSIDInfo(oWLSSSID) 
{
  try {
    var sCmd = "wireless ifconfig";
    
    // Check if SSID is to be set
    if(oWLSSSID.sSSID != _constants.SMAPI_STR_DONT_CARE) {
      sCmd += " ssid=" + oWLSSSID.sSSID; 
    }  
    // Check if SSID Broadcast is to be updated
    if(oWLSSSID.nSSIDBroadcast != _constants.SMAPI_INT_DONT_CARE) {
      if(oWLSSSID.nSSIDBroadcast == 1) {
        sCmd += " any=enabled";
      } else if (oWLSSSID.nSSIDBroadcast == 0) {
        sCmd += " any=disabled";
      } else {
        this.oSMAPI.SetLastError(_constants.SMAPI_INVALID_ARGUMENT);
        return;
      } 
    }
    
    var sCmdOrig = "wireless ifconfig";   
    if(sCmd.length > sCmdOrig.length) {
      this.ExecuteCommand(sCmd, true);
      // commit new changes before reboot
      this.pvt_PersistChanges();
    }  
      
  } catch(ex) {
    _logger.error("_st780_cli_SetSSIDInfo", ex.message );
    this.oSMAPI.SetLastError(_constants.SMAPI_EXCEPTION);
  }
}

/*******************************************************************************
** Name:         _st780_cli_QueueCommands
**
** Purpose:      By using this function caller is hinting to start queing of all
**               the SMAPI calls made hence forth till a commit commands is 
**               received. Implementations can hook up their support for this
**               accordingly and see how they can do a batch update on CPE's in 
**               their code. They should return a true /false to tell if
**               queing was successful. 
**
** Parameter:    none
**
** Return:       GetLastError to confirm success 
*******************************************************************************/
_st780_cli.prototype.QueueCommands = function _st780_cli_QueueCommands() 
{
  var bRet = false;
  try {
    // clean up any existing scripts from the modem
    var sCmd = "script delete name=" + this.m_sScriptName;
    this.ExecuteCommand(sCmd);
    if(this.oSMAPI.GetLastError() != _constants.SMAPI_SUCCESS) return bRet;
    
    //set index to 0
    this.m_nScriptIndex = 0; 
    bRet = true;
  } catch(ex) {
    _logger.error("_st780_cli_QueueCommands", ex.message );
    this.SetLastError(_constants.SMAPI_EXCEPTION);
  }
  return bRet;
}

/*******************************************************************************
** Name:         _st780_cli_CommitQueuedCommands
**
** Purpose:      By using this function caller is hinting to start queing of all
**               the SMAPI calls made hence forth till a commit commands is 
**               received. Implementations can hook up their support for this
**               accordingly and see how they can do a batch up[date on CPE's in 
**               their code.
**
** Parameter:    none
**
** Return:       GetLastError to confirm success 
*******************************************************************************/
_st780_cli.prototype.CommitQueuedCommands = function _st780_cli_CommitQueuedCommands() 
{
  var bRet = true;
  try {
    // run existing script in the modem
    var sCmd = "script run name=" + this.m_sScriptName + " pars=a";
    this.ExecuteCommand(sCmd);
    
    // there could be scripts here that could cause connectivity drops hence
    // no need to check last error on these sets just simpley return
    // set index to 0
    this.m_nScriptIndex = 0; 
  } catch(ex) {
    _logger.error("_st780_cli_CommitQueuedCommands", ex.message );
    this.SetLastError(_constants.SMAPI_EXCEPTION);
  }
  return bRet;
}

/*******************************************************************************
** Name:         _st780_cli_ExecuteCommand
**
** Purpose:      One function for executing CLI commands. It does the following
**               -Logs in to modem using telnet session
**               -Send Cmd Request over socket
**               -Receives response for the command
**               -Logs out.
**
** Parameter:    sCmd       : command string to execute
**               bSet       : flag representing if command is of type set or get
**               ulFlag     : helps in minimising login/logout per function
**
** Return:       check last error for succes or failure
*******************************************************************************/
_st780_cli.prototype.ExecuteCommand = function _st780_cli_ExecuteCommand(sCmd, bSet, ulFlag) 
{
  var sResp      = ""; // tracks response text
  var sQueuedCmd = "script add name=" + this.m_sScriptName + 
                   " index=" + this.m_nScriptIndex +
                   " command=\"" + sCmd + "\"";
  
  try 
  {
    var bRet = true;
    if(_smapiutils.MaskIn(this.m_ulExecCommand, CLI_LOGIN)) {
      _logger.debug("_st780_cli_ExecuteCommand", "Login in for CMD: " + sCmd);
      bRet = this.pvt_ModemLogin();

    }
       
    if(bRet) {
      // successful login or no login required  
      if(bSet && this.oSMAPI.m_bInQueue) {
        _logger.debug("_st780_cli_ExecuteCommand", "CMD: " + sQueuedCmd);
        this.m_nScriptIndex++;
        this.oPWCtl.sendSocket(sQueuedCmd);
      } else {
        _logger.debug("_st780_cli_ExecuteCommand", "Executing CMD: " + sCmd);
        this.oPWCtl.sendSocket(sCmd);
      }
      
      _logger.debug("_st780_cli_ExecuteCommand", "Get response for CMD: " + sCmd);
      sResp = this.pvt_ModemGetResponse(); 
        
      if(_smapiutils.MaskIn(this.m_ulExecCommand, CLI_LOGOUT)) {
        _logger.debug("_st780_cli_ExecuteCommand", "Logout for CMD: " + sCmd);
        this.pvt_ModemLogout();
      }
        
    }  
  } catch (ex) {
    _logger.error("_st780_cli_ExecuteCommand", ex.message );
    this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
  }
  return sResp;
}


/*******************************************************************************
** Name:         _st780_cli_pvt_PersistChanges
**
** Purpose:      Persist changes over reboots. If this method is not called
**               changes are temporary and available only for the current session.
**
** Parameter:    none
**
** Return:       GetLastError to confirm success
*******************************************************************************/
_st780_cli.prototype.pvt_PersistChanges = function _st780_cli_pvt_PersistChanges() 
{
  try {
    var sCmd  = "saveall";
    var sResp = this.ExecuteCommand(sCmd, true);
    if(!this.oSMAPI.m_bInQueue &&
        this.oSMAPI.GetLastError() == _constants.SMAPI_SUCCESS) { 
        // commit takes a long time
        _smapiutils.g_oSMAPIDependencies.Sleep(10000);  
    }  
  } catch(ex) {
    _logger.error("_st780_cli_pvt_PersistChanges", ex.message );
    this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
  }
}

/*******************************************************************************
** Name:         _st780_cli_SetWirelessMACACLEnabled
**
** Purpose:      Set MACAddressControl filtering on or off
**               lock: 1/ unlock: 0
**
** Parameter:    none
**
** Return:       returns 1/0 ; -1 incase of failure or non accessibility
*******************************************************************************/
_st780_cli.prototype.SetWirelessMACACLEnabled = function _st780_cli_SetWirelessMACACLEnabled(nEnable) 
{
  try {
    var sCmd  = "wireless macacl config";
    if(nEnable) {
      sCmd += " control=lock";
    } else {
      sCmd += " control=unlock";
    }  
    var sResp = this.ExecuteCommand(sCmd, true);
  } catch(ex) {
    _logger.error("_st780_cli_SetWirelessMACACLEnabled", ex.message );
    this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
  }
}

/*******************************************************************************
** Name:         _st780_cli_GetWirelessMACACLEnabled
**
** Purpose:      Retrieves whether MACAddressControl filtering in on or not
**               lock: 1/ unlock: 0
**
** Parameter:    none
**
** Return:       returns 1/0 ; -1 incase of failure or non accessibility
*******************************************************************************/
_st780_cli.prototype.GetWirelessMACACLEnabled = function _st780_cli_GetWirelessMACACLEnabled() 
{
  var nRet = -1;
  try {
    var sCmd  = "wireless macacl config";
    var sResp = this.ExecuteCommand(sCmd);
    if(this.oSMAPI.GetLastError() == _constants.SMAPI_SUCCESS) {
      // NOTE: Register case and unlock currently treated as 0
      nRet = this.pvt_MatchTag1(sResp, / *Access Control *: *([\w\S]+)/, "lock"); 
    }  
  } catch(ex) {
    _logger.error("_st780_cli_GetWirelessMACACLEnabled", ex.message );
    this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
  }
  return nRet;
}

/*******************************************************************************
** Name:         _st780_cli_pvt_BackUpConfig
**
** Purpose:      Original config needs to backed up before we start setting 
**               any new values. Since set operations happen via different 
**               methods this way helps us to avoid multiple backups.
**
** Parameter:    (none)
**
** Return:       true/false else check GetLastError
*******************************************************************************/
_st780_cli.prototype.pvt_BackUpConfig = function _st780_cli_pvt_BackUpConfig() 
{
  if(!this.m_bIsConfigBackedUp) {
    
    // Check modem for original config 
    var sResp = this.ExecuteCommand("config list");
    if(this.oSMAPI.GetLastError() == _constants.SMAPI_SUCCESS) {
      this.m_bIsConfigBackedUp = (sResp.indexOf("user_org.ini") != -1);
    }  
    
    // If still not backed up
    if(!this.m_bIsConfigBackedUp) {
      // Backup the original config 
      this.ExecuteCommand("config save filename=user_org.ini");
      if(this.oSMAPI.GetLastError() == _constants.SMAPI_SUCCESS) {
        this.m_bIsConfigBackedUp = true;
        _smapiutils.g_oSMAPIDependencies.Sleep(5000);
      }
    }
  }    
  return this.m_bIsConfigBackedUp;
}    

/*******************************************************************************
** Name:         _st780_cli_pvt_UpdateFirmwareConfig
**
** Purpose:      Silent exe uses a config for firmware upgrade. We need to 
**               update this before starting the upgrade.
**
** Parameter:    (none)
**
** Return:       true/false else check GetLastError
*******************************************************************************/
_st780_cli.prototype.pvt_UpdateFirmwareConfig = function _st780_cli_pvt_UpdateFirmwareConfig(sTempPath) 
{
  var bRet = false;
  try {
    var oDOM  = _smapiutils.g_oSMAPIDependencies.LoadDOM(sTempPath + "\\tmpInstall.config");
    if(oDOM != null) {
      bRet = (
              pvt_SetValue("buildImagePath", "build_images") &&
              pvt_SetValue("buildImageFile", this.mProperties.firmware_filename) &&
              pvt_SetValue("serialFilter", "*") );
              
      if(bRet) { 
        var oFile = _file.CreateUTF8TextFile(sTempPath + "\\stInstall.config", true);
        if(oFile != null) {
          bRet = _file.WriteFile(oFile, oDOM.xml);
          _smapiutils.g_oSMAPIDependencies.Sleep(200);
          oFile.Close();
          oFile = null;
        } else {
          bRet = false;
        }    
      }  
    }  
    //----------------------------------------------------------------------------------
    //inline helper function
    function pvt_SetValue(sKey, sVal) {
      var sExpr = "//settings/setting[@key='" + sKey + "']";
      var oNode = oDOM.selectSingleNode(sExpr);
      if(oNode) { 
        oNode.setAttribute("value", sVal);
        return true;
      } else { 
        return false;
      }  
    }    
  } catch (ex) {
    _logger.error("_st780_cli_pvt_UpdateFirmwareConfig", ex.message ); 
    bRet = false;
  }    
  return bRet;
}

/*******************************************************************************
** Name:         _st780_cli_pvt_CheckResponse
**
** Purpose:      Provides one way to check various login responses from modem
**
** Parameter:    ulFlag: options that need to be checked can be ORed in the flag
**
** Return:       flag that matched the response
*******************************************************************************/
_st780_cli.prototype.pvt_CheckResponse = function _st780_cli_pvt_CheckResponse(ulFlag) 
{
  var sResp = this.pvt_ModemGetResponse();
   
  if(_smapiutils.MaskIn(ulFlag, CLI_RSP_USERNAME)) {
    if(sResp.toLowerCase().indexOf("username") != -1) {
      return CLI_RSP_USERNAME;
    }
  }
  
  if(_smapiutils.MaskIn(ulFlag, CLI_RSP_PASSWORD)) {
    if(sResp.toLowerCase().indexOf("password") != -1) {
      return CLI_RSP_PASSWORD;
    }
  }
  
  if(_smapiutils.MaskIn(ulFlag, CLI_RSP_INVALID_CREDS)) {
    if(sResp.toLowerCase().indexOf("invalid username/password") != -1) {
      return CLI_RSP_INVALID_CREDS;
    }
  }
  
  if(_smapiutils.MaskIn(ulFlag, CLI_RSP_ID)) {
    if(sResp.toLowerCase().indexOf(this.mProperties.id.toLowerCase()) != -1) {
      return CLI_RSP_ID;
    }
  }
  
  if(_smapiutils.MaskIn(ulFlag, CLI_RSP_SESS_INUSE)) {
    if(sResp.toLowerCase().indexOf("session") != -1) {
      return CLI_RSP_SESS_INUSE;
    }
  }
  
  if(_smapiutils.MaskIn(ulFlag, CLI_RSP_NONE)) {
    if(sResp == "") {
      return CLI_RSP_NONE;
    }
  }
    
  return _constants.SMAPI_HEX_DONT_CARE;    
}

/*******************************************************************************
** Name:         _st780_cli_pvt_ModemLogin
**
** Purpose:      Login to the modem
**
** Parameter:    none
**
** Return:       true if login OK, else false
*******************************************************************************/
_st780_cli.prototype.pvt_ModemLogin = function _st780_cli_pvt_ModemLogin() 
{
  var bRet = false;
  try {
    _logger.debug("_st780_cli_pvt_ModemLogin", "Opening socket - " + this.oSMAPI.GetLastError());
    var bSockOpen = this.oPWCtl.openSocket(this.mProperties.ip);
    if(bSockOpen) {
      //_smapiutils.g_oSMAPIDependencies.Sleep(500); 
      var ulVal = this.pvt_CheckResponse(CLI_RSP_SESS_INUSE) 
      
      if(ulVal == CLI_RSP_SESS_INUSE ) { // session in use
        this.oPWCtl.sendSocket("1 \n");  // kill 1
      }   
      _logger.debug("_st780_cli_pvt_ModemLogin", "Send User Name - " + this.oSMAPI.GetLastError());
      this.oPWCtl.sendSocket(this.mProperties.modemuser + "\n");
     // _smapiutils.g_oSMAPIDependencies.Sleep(200); 
      ulVal = this.pvt_CheckResponse(CLI_RSP_PASSWORD|CLI_RSP_ID); 
     // alert("send User " + ulVal);
      _logger.debug("_st780_cli_pvt_ModemLogin", "Response val - " + ulVal);
      switch(ulVal) 
      {
        
        case CLI_RSP_PASSWORD:
          _logger.debug("_st780_cli_pvt_ModemLogin", "Send Password - " + this.oSMAPI.GetLastError());
          this.oPWCtl.sendSocket(this.mProperties.modempwd);
         // _smapiutils.g_oSMAPIDependencies.Sleep(1000); 
          ulVal = this.pvt_CheckResponse(CLI_RSP_INVALID_CREDS|CLI_RSP_ID);  
     //    alert("send Password " + ulVal);
          _logger.debug("_st780_cli_pvt_ModemLogin", "Response val - " + ulVal);
          switch(ulVal) 
          {
            case CLI_RSP_INVALID_CREDS: 
              // login has failed this possible only due to bad admin creds
              this.oSMAPI.SetLastError(_constants.SMAPI_NO_AUTH);
              break;
              
            case CLI_RSP_ID: // login succeeded
              bRet = true;
              break;
              
            default: // unknown output
              this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_FAIL);  
              break;
          }  
          break;
        
        case CLI_RSP_ID: // login succeeded
          bRet = true;  
          break;
          
        default:  // unknown output
          this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_FAIL);
          break;
      }
      if(!bRet) { // we had opened socket so better close it to avoid multiple sessions
        _logger.debug("_st780_cli_pvt_ModemLogin", "Closing Socket val - " + ulVal);
        this.oPWCtl.close();
        _smapiutils.g_oSMAPIDependencies.Sleep(1500);  
      }
    } else {
      // we failed to telnet to modem
      _logger.error("_st780_cli_pvt_ModemLogin", "Failed to telnet to modem");
      this.oSMAPI.SetLastError(_constants.SMAPI_FAIL_MODEM_ACCESS);
    }  
         
  } catch(ex) {
    _logger.error("_st780_cli_pvt_ModemLogin", ex.message  + this.oPWCtl.status);
    alert("error");
    this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
    bRet = false;
  }
  return bRet;
}

/*******************************************************************************
** Name:         _st780_cli_pvt_ModemLogout
**
** Purpose:      Logout
**
** Parameter:    (none)
**
** Return:       (none), no error if not logged in
*******************************************************************************/
_st780_cli.prototype.pvt_ModemLogout = function _st780_cli_pvt_ModemLogout() 
{
  try {
    this.oPWCtl.sendSocket("exit");
    this.pvt_ModemGetResponse();
    this.oPWCtl.close();
    // reset command
    this.m_ulExecCommand = CLI_LOGIN|CLI_LOGOUT;
    _logger.debug("_st780_cli_pvt_ModemLogout", "LOGGED OUT - " + this.oSMAPI.GetLastError());
    // used to avoid concurrent telnet sessions
    _smapiutils.g_oSMAPIDependencies.Sleep(1500);  
  } catch(ex) {
    _logger.error("_st780_cli_pvt_ModemLogout", ex.message );
    this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
  }
}

/*******************************************************************************
** Name:         _st780_cli_pvt_ModemGetResponse
**
** Purpose:      Get the response from a sendsocket
**
** Parameter:    (none)
**
** Return:       response string
*******************************************************************************/
_st780_cli.prototype.pvt_ModemGetResponse = function _st780_cli_pvt_ModemGetResponse() 
{
  var sResp = "";
  try {
    _smapiutils.g_oSMAPIDependencies.Sleep(500);  
    sResp = this.oPWCtl.receiveSocket();
    _logger.debug("_st780_cli_pvt_ModemGetResponse", "CMD RESPONSE: " + sResp);
  } catch(ex) {
    _logger.error("_st780_cli_pvt_ModemGetResponse", ex.message );
    this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
  }
  return sResp;
}
  
/*******************************************************************************
** Name:         _st780_cli_EnableTR64
**
** Purpose:      Since some of the function of this class use CLI and others 
**               use TR64 we need to double check that the once using TR64 will 
**               work surely by enabling TR64 on device
**
** Parameter:    (none)
**
** Return:       true/false else check GetLastError
*******************************************************************************/
_st780_cli.prototype.EnableTR64 = function _st780_cli_EnableTR64() 
{
  var bTR64Enabled = this.m_bTR64Enabled;
  try 
  {
    if(!bTR64Enabled) {
      // check databag
      bTR64Enabled = (_smapiutils.g_oSMAPIDependencies.GetValue(_constants.SMAPI_DB_TR64ENABLED) == true);
    }
    
    if(!bTR64Enabled) { 
      
      // login and set the command
      if(this.pvt_ModemLogin()) {
        this.m_ulExecCommand = CLI_NOOP; 
      } else {
        return (this.m_bTR64Enabled = bTR64Enabled);
      }

      // Get from modem
      var sResp = this.ExecuteCommand("system config");
      if(this.oSMAPI.GetLastError() == _constants.SMAPI_SUCCESS) {
        bTR64Enabled = this.pvt_MatchTag1(sResp, /\s+TR-64 discovery *: *([\w\S]+)/, "enabled");
        bTR64Enabled = (bTR64Enabled == 1) ? true : false; //ignore -1 case
      
        if(!bTR64Enabled) {
          _logger.debug("_st780_cli_EnableTR64", "enable tr64 on modem");  
          sResp = this.ExecuteCommand("system config tr64=enabled");
          if(this.oSMAPI.GetLastError() == _constants.SMAPI_SUCCESS) {
            bTR64Enabled  = true;
            _smapiutils.g_oSMAPIDependencies.Sleep(4000);
            this.pvt_PersistChanges();
          }
        }
      }  
      
      // only if login passed we would be here hence need to logout (cmd is setback inside logout) 
      this.pvt_ModemLogout();
      _smapiutils.g_oSMAPIDependencies.SetValue(_constants.SMAPI_DB_TR64ENABLED, bTR64Enabled);
       
    } 
  } catch(ex) {
    _logger.error("_st780_cli_EnableTR64", ex.message );
    this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
  }
  this.m_bTR64Enabled = bTR64Enabled; 
  _logger.debug("_st780_cli_EnableTR64", "Value is: " + this.m_bTR64Enabled );  
  return this.m_bTR64Enabled;  
}

  
/*******************************************************************************
** Name:         _st780_cli_pvt_MatchTag1
**
** Purpose:      Get the response from a sendsocket
**
** Parameter:    (none)
**
** Return:       response string
*******************************************************************************/
_st780_cli.prototype.pvt_MatchTag1 = function _st780_cli_pvt_MatchTag1(sMatchInStr, sMatchExp, sMatchValueWith)
{
  var retVal = -1;
  try {
    var arResult = sMatchInStr.match(sMatchExp);
    if (arResult != null && arResult.length > 1) {
      if(typeof sMatchValueWith != "undefined") {
        retVal = (arResult[1] == sMatchValueWith)? 1: 0;
      } else {
        retVal = arResult[1];
      }
    }
  } catch(ex) {
    _logger.error("_st780_cli_pvt_MatchTag1", ex.message );
    this.oSMAPI.SetLastError(_constants.SMAPI_DERIVED_EXCEPTION);
  }
  return retVal;
}  
