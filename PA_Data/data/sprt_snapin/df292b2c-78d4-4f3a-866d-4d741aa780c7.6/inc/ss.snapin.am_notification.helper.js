$ss.snapin = $ss.snapin || {};
$ss.snapin.am_notification = $ss.snapin.am_notification || {};
$ss.snapin.am_notification.helper = $ss.snapin.am_notification.helper ||{};

(function () {
    $.extend($ss.snapin.am_notification.helper, {
      LaunchAgent: function(){
        var loggedInUser = $ss.agentcore.dal.config.ExpandSysMacro("%USER%");
        var providerID = $ss.agentcore.dal.config.GetProviderID().toLowerCase();
        var agentLaunchedUser;
        var count = 0;
        var eServiceList = new Enumerator(window.external.GetObject("winmgmts:{impersonationLevel=impersonate}!root/cimv2").ExecQuery("Select * From Win32_Process where name='bcont_nm.exe'"));
        for (; !eServiceList.atEnd(); eServiceList.moveNext()) {
          agentLaunchedUser = eServiceList.item().ExecMethod_("GetOwner");
          if (agentLaunchedUser.ReturnValue == 0) {
            if (agentLaunchedUser.User == loggedInUser) {
              var cmdline = eServiceList.item().CommandLine;
              if (cmdline.indexOf("minibcont") >= 0 && cmdline.indexOf("regnotification") >= 0
                  && cmdline.indexOf("minibcont_regnotification") >= 0) {
                count += 1;
                if (count > 1)  return false;
              }
            }
          }
        }
        return true;
      },

      GetModel: function(){
        var model = new $ss.snapin.notification.model.user();
        var $pDef = $.Deferred();
        try {
          UpdateUserInfo(model);
          GetUserInfo(model.sName, model.sDomain)
          .done(function (historyData) {
            ExtractHistoryStatus(model, historyData);
            $pDef.resolve(model);
          })
          .fail(function () {
            model.sErrMessage = _utils.LocalXLate("snapin_acm_notification", "reg_no_history") + " " + model.sName + ". " + _utils.LocalXLate("snapin_acm_notification", "reg_contact_help");
            $pDef.reject(model);
          });
          return $pDef;
        }
        catch (e) {}
      },  

      IsBigBcontRunning: function() {
        if (_utils.CheckInstance(_GetBCONTMutexPrefix() + "bigbcont"))
          return true;
        return false;
      }
 });
 
  function _GetBCONTMutexPrefix() {
    var mutexPrefix = "__SDC_BCONT_MUTEX__" + _config.GetProviderID();
    return mutexPrefix.toUpperCase();
  }

  function UpdateUserInfo(oUser) {
    try {
      var _si = $ss.agentcore.diagnostics.smartissue;
      var absPath = $ss.getSnapinAbsolutePath("snapin_acm_notification");
      // Create Issue
      var issueID = _si.CreateIssue("SystemInfo", true, absPath + "\\smartissue\\default.xml", false, false, null);
      var sIssuePath = $ss.getAttribute("startingpoint").replace("\\data", "\\state\\Issues\\");
      var sSmartIssueXml = _xml.LoadXML(sIssuePath + issueID + ".xml", false, null);
      var xpathNode = "//PROPERTY[@*]";
      var nodes = _xml.GetNodes(xpathNode, "", sSmartIssueXml);

      oUser.sDomain = _xml.GetNodeText(nodes[0], 'VALUE');
      oUser.sName = _xml.GetNodeText(nodes[2], 'VALUE').replace(oUser.sDomain + "\\", "");

      _si.DeleteIssue(issueID);
    }
    catch (e) { }
  }

  function GetUserInfo(user, domain) {
    var sUrl = NavigationController.GetServerUrl() + "/sdcxuser/accmgr/AMSnapin/GetUserInfo";
    return $.ajax({
      type: "POST",
      url: sUrl,
      data: { DomainName: domain, User: user, Snapin: true }
    });
  }

  function ExtractHistoryStatus(oUser, json) {
    //Store the User history info and current status
    oUser.sStatus = json.Status || "UNKNOWN";
    oUser.sFirstName = json.FirstName;
    oUser.sLastName = json.LastName;
    oUser.dDaysToExpire = (typeof (json.History) != 'undefined') ? json.History.DaysToExpire : "";
  }
 })();