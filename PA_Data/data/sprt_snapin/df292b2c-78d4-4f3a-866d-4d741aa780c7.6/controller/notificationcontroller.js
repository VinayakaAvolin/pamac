$ss.snapin = $ss.snapin || {};
$ss.snapin.am_notification = $ss.snapin.am_notification || {};
$ss.snapin.am_notification.helper = $ss.snapin.am_notification.helper || {};

(function () {
SnapinAcmNotificationController = SnapinBaseController.extend('snapin_acm_notification', {
    model: null,
    location: null
},
 {
  index: function (params) {
    try {
      $ss.agentcore.utils.system.Sleep(1000);
      if ($ss.snapin.am_notification.helper.LaunchAgent()) {
        var that = this;
        var params1 = $.extend({},params);
        that.GetUserModel(true)
        .done(function (model) {
            if (model.sFirstName == null && model.sLastName == null && model.sStatus == "UNKNOWN") {
                return;
            }
          if(/(INACTIVE|UNKNOWN)/i.test(model.sStatus)){
            that.renderSnapinView("snapin_acm_notification", params1.toLocation, "\\views\\register.html", { "ammodel": model });
            _utils.ShowBcont();
          }else if (/(ACTIVE)/i.test(model.sStatus)){
            var daysLeftForExpiration = model.dDaysToExpire;
            var expirationPeriod = _config.GetConfigValue("snapin_acm_notification", "no_of_days_of_expiration", "10");
            
            //already expired
            if (parseInt(daysLeftForExpiration) <= 0) {
              that.renderSnapinView("snapin_acm_notification", params1.toLocation, "\\views\\expired.html", { "ammodel": model });
              _utils.ShowBcont();
            }
            // days to expire password is less than the config value (expirationPeriod)
            else if ((parseInt(daysLeftForExpiration) <= parseInt(expirationPeriod)) && (parseInt(daysLeftForExpiration) > 0)) {
              that.renderSnapinView("snapin_acm_notification", params1.toLocation, "\\views\\reset.html", { "ammodel": model });
              _utils.ShowBcont();
            }
            else {
              that.Supress_Bcont();
            }
          }
        })
        .fail(function (model){
          //alert(model.sErrMessage);
          that.Supress_Bcont();
        })
      }
      else {
        this.Supress_Bcont();
      }
    }
    catch (ex) {
      this.Supress_Bcont();
    }
  },

  GetUserModel: function (forceReload) {
    var that = this;
    if (!that.Class.model || forceReload === true) {
      var $d = _helper.GetModel();
      $d.then(function (model) {
        that.Class.model = model;
      });
      return $d;
    } else {
      return $.Deferred().resolve(that.Class.model).promise()
    }
  },

  "#button click": function (params) {
    var requesturl = "/Accountmanager?id=account_manager_nav_summary";
    var bBcontRunning = _helper.IsBigBcontRunning();
    if(bBcontRunning) {
        alert("ProactiveAssist is busy performing other task. Please visit Account Manager snapin for more details.");
    }
    _utils.HideBcont();
    //Launch Account Manager snapin.
    _utils.RunCommand("\"" + _utils.GetBCONTPath() + "\" /ini \"" +
            _config.GetProviderRootPath() + "\\agent\\bin\\bcont_nm.ini \" /p " + _config.GetProviderID() +
            " /path \"" + requesturl + "\" " , _constant.BCONT_RUNCMD_ASYNC);    
    this.Supress_Bcont();	
  },

  Supress_Bcont: function () {
    _utils.ui.CloseBcont();
  }
});

  var _config = $ss.agentcore.dal.config;
  var _constant = $ss.agentcore.constants;
  var _utils = $ss.agentcore.utils;
  var _helper = $ss.snapin.am_notification.helper;

})();
