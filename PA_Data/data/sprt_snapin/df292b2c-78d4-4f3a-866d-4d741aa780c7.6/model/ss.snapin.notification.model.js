/**
* @author Mamatha
*/
$ss.snapin = $ss.snapin ||
{};
$ss.snapin.notification = $ss.snapin.notification ||
{};
$ss.snapin.notification.model = $ss.snapin.notification.model ||
{};

(function () {
    $ss.snapin.notification.model.user = MVC.Model.extend({
      init: function () {
        try {
          this.sName = "",
          this.sDomain = "",
          this.sStatus = "",
          this.sFirstName = "",
          this.sLastName = "",
          this.history = "",
          this.bError = false,
          this.sErrMessage = ""
        }
        catch (e) {
          throw {
            name: 'TypeError',
            message: _const.PR_CONSTRUCTOR_FAILED
          }
        }
      }
    });

    jQuery.support.cors = true;
}
)()