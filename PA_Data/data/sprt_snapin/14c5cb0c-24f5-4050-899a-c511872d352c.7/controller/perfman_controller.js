/** @namespace Provides System Information functionality */
$ss.snapin = $ss.snapin || {};
$ss.snapin.perfman = $ss.snapin.perfman || {};
$ss.snapin.perfman.helper = $ss.snapin.perfman.helper || {};
var sSearchSelCat = "";
var selOptimizeId = [];
(function () {
  SnapinPerfmanController = SnapinBaseController.extend('snapin_perfman', {

    // Class Variables
    oInitialParams: null,
    hasinitialized: false,
        //Survey Feature Starts
        _enableSurvey: null,
        _solnExecInitiator: "",
        //Survey Feature Ends

        Initialize: function () {
            $ss.agentcore.events.Subscribe("BROADCAST_ON_EXTERNALCLOSE", function (oEvent) { return SnapinPerfmanController.OnExternalClose(oEvent) });
      this.hasinitialized = true;
    },
        OnExternalClose: function (oEvent) {
      try {
        if (_helper.AreOptimizationsRunning()) {
              if (bMac) {
                  var macOptimizer = _helper.InitOptimizerForMac(this);
                  macOptimizer.Halt();
              }else {
                  _helper.InitOptimizer();
          optimizer.Halt();
              }
              _helper.SetOptimizationsRunningTo(false);
        }
      } catch (err) {
        //alert(err.message);
      }
    }

  }, {

    /**
    *  @function
    *  @description Entry point for this snapin.  Initializes variables and display view.
    *  @param params Optional MVC Params object
    *  @returns none
    */
        index: function (params, sCat) {
      sSearchSelCat = "";
      if (!this.Class.hasinitialized) {
        this.Class.Initialize();
      }
            //Get from registry if Survey is enabled or not
            var provider = $ss.agentcore.dal.config.GetProviderID();
            var user = $ss.agentcore.dal.config.ExpandSysMacro("%USER%");
            var prodName = $ss.agentcore.dal.config.ParseMacros("%PRODUCT%");
            var regPath = "Software\\SupportSoft\\ProviderList\\" + provider + "\\" + prodName + "\\users\\" + user + "\\ss_config\\global\\";
            this.Class._enableSurvey = $ss.agentcore.dal.registry.GetRegValue("HKCU", regPath, "Enable_Survey");

      this.Class.oInitialParams = params; //use this.Class to access any member variables/ methods
      _helper.Initialize(params.requestArgs);

      if (params.requestArgs.logging) {
        this.WriteUsageLog(params.requestArgs)
      }

      if (params.requestArgs.id) {
        sCat = params.requestArgs;
      }

            // GS Customization Start
            //If performance manager is initiated from main layout - load performance manager in the UI. If performance manager is initiated from mini layout - do not load performance manager in the UI
            if (params.inInstanceParam.from_layout == "mainlayout")
                // GS Customization end
      this.renderSnapinView("snapin_perfman", params.toLocation, "\\views\\perfman_index.html", {});
      $ss.agentcore.utils.ui.FixPng();

            if (params.SelectedId) selOptimizeId = params.SelectedId; //When loaded from Self Service Support page
  
      this.LoadSelectedCat(sCat);
      if (selOptimizeId.length > 0) selOptimizeId = []; //Scope is one time usage, hence reseting. This will let users navigate between links and load default selected contents.

            if (bMac) {
              _helper.InitOptimizerForMac(this);
            }else {
              _helper.InitOptimizer();
            }

      // Initialize modal window
      //$('#snapin_perfman_modal_shadow').css('background', 'transparent url("' + $ss.GetLayoutAbsolutePath() + "\\skins\\default\\core\\images\\bg.png" + '")');
      $('#snapin_perfman_modal').appendTo($('body'));
    },
    
     indexSummary: function (pArgs) {
            this.renderSnapinView("snapin_perfman", pArgs.toLocation, "\\views\\pmsummary.html", {});
        },
    /**
    *  @function
    *  @description Handles user click on a particular category from the left pane
    *  @param params MVC Params object, including the <a> element clicked
    *  @returns none
    */
        ".pmnavlinks click": function (params) {
      sSearchSelCat = "";
      this.LoadSelectedCat(params.element);
            $("#exOptButtonViewAll").attr("disabled", "disabled");
            $("#exAdvOptButtonViewAll").attr("disabled", "disabled");
    },

        applyDefaultStyle: function () {
            $('.tab-menu-filters').toggleClass('tab-menu-filters-selected', false);
            $('.tab-menu-filters').toggleClass('tab-menu-filters', true);
    },
    
        applyNewStyle: function (elemID) {
            $(elemID + ' .tab-menu-filters').toggleClass('tab-menu-filters-selected', true);
    },

        ".snapin_perfman_schedule_frequency click": function (params) {
      _helper.RenderScheduleFrequencyChange($(params.element).val());
    },
    
        WriteUsageLog: function (params) {
      try {
        if(!bMac)
          var _objContainer = $ss.agentcore.utils.activex.GetObjInstance();
        var fileLog = _file.BuildPath(_helper._path_sprt_optimize, params.logname);
        var xml = $ss.agentcore.dal.xml.LoadXML(fileLog, false, null);
                // GS Customization starts - Get the values
                var provider = $ss.agentcore.dal.config.GetProviderID();
                var user = $ss.agentcore.dal.config.ExpandSysMacro("%USER%");
                var prodName = $ss.agentcore.dal.config.ParseMacros("%PRODUCT%");
                var regPath = "Software\\SupportSoft\\ProviderList\\" + provider + "\\" + prodName + "\\users\\" + user + "\\ss_config\\global\\";
                var sSolnExecInitiator = $ss.agentcore.dal.registry.GetRegValue("HKCU", regPath, "SolnExecInitiator");
                // GS Customization End 
                $(xml).find('string').each(function (index) {
           var originalstr = $(this).text();
		   originalstr = _helper.AdaptUsageLogString(originalstr);
		   //_objContainer.LogMsg($(this).text());
                    // GS Customization starts   
                    //Adding different value for verb and comments in case of solutions executed through scheduler. -to identify if the optimizer is executed manually or through the scheduler
                    if (sSolnExecInitiator != "" && sSolnExecInitiator == "manual")
                        originalstr = originalstr.replace("__optimization__", "__optimizationManual__");
                    else
                        originalstr = originalstr.replace("__optimization__", "__optimizationSchedule__");

                    $ss.agentcore.dal.registry.DeleteRegVal("HKCU", regPath, "SolnExecInitiator");
                    // GS Customization End         
		      if (bMac) {
            $ss.agentcore.utils.WriteUsageLog(originalstr);
          }     
          else {
            _objContainer.LogMsg(originalstr);
          }
        });

      } catch (err) {
        //alert("Error : " + err.name + " => " + err.message);
        //$ss.snapin.perfman.helper.log(err.name + " => " + err.message);
      }
      $ss.agentcore.utils.ui.CloseBcont()
    },

        LoadSelectedCat: function (SelectElement) {
      _helper.SetOptimizationsRunningTo(false);

      var sSelectedCat = (SelectElement) ? SelectElement.id : "perfman_nav_summary";
      var sSelectedCat = sSearchSelCat ? sSearchSelCat : sSelectedCat;
      this.renderSnapinView("snapin_perfman", "#col_perfman_content", "\\views\\" + sSelectedCat.replace("_nav_", "_") + ".html", {});

      this.applyDefaultStyle();
            this.applyNewStyle("#" + sSelectedCat);

      $ss.agentcore.utils.ui.FixPng();

            if (SelectElement && SelectElement.idselect) {
                this.InitQuickFiltered();
                return;
            }
            if (SelectElement && SelectElement.isadvanced) {
                this.InitAdvancedFiltered();
                return;
            }

      switch (sSelectedCat) {
        case "perfman_nav_quick":
          this.InitQuick();
          break;
        case "perfman_nav_history":
          this.InitHistory();
          break;
        case "perfman_nav_advanced":
          this.InitAdvanced();
          break;
        case "perfman_nav_summary":
          _helper.InitSummary();
          break;
        case "perfman_nav_schedule":
          _helper.InitSchedule();
          break;
        default:
          break;
      }
    },

        ".PerfManSummaryGoToSchedule click": function (params) {
      this.LoadSelectedCat($("#perfman_nav_schedule")[0]);
    },

        "#PerfManSummaryStartQuickOptimization click": function (params) {
      this.LoadSelectedCat($("#perfman_nav_quick")[0]);
    },

        "#PerfManSchedulerSaveSchedule click": function (params) {
      _helper.SaveSchedule();
    },

        "#selectAllCheckBox click": function (params) {
      $('#optList :checkbox').prop('checked', $(params.element).prop('checked'));
    },

        InitQuick: function () {
            $("#snapin_perfman_quick #optList").empty();
            var strCategories = _config.GetConfigValue("snapin_perfman", "snapin_perfman_categories_quick", "");
            var arrCategories = strCategories.split(",");

            _helper.RenderOptimizationsTo(_content.GetContentsAdvQuery(["sprt_optimize"], { category: { has: arrCategories } }), '#snapin_perfman_quick #optList', {}, _config.GetUserValue("OptimizationQuick", "DefaultOptimizations"));
        },

        InitQuickFiltered: function () {
      var strCategories = _config.GetConfigValue("snapin_perfman", "snapin_perfman_categories_quick", "");
      var arrCategories = strCategories.split(",");

            _helper.RenderOptimizationsTo(_content.GetContentsAdvQuery(["sprt_optimize"], { cid: { has: selectedRowIds } }), '#snapin_perfman_quick #optList', {}, _config.GetUserValue("OptimizationQuick", "DefaultOptimizations"));
    },

        '.PerfManAdvancedLaunch click': function (params) {
      var path = _config.ExpandAllMacros($(params.element).attr('path'));
      var options = _constants[$(params.element).attr('pathOptions')];
      var _objContainer = $ss.agentcore.utils.activex.GetObjInstance();

      if (!options) {
        options = 0;
      }

    if (bMac) {
        $ss.agentcore.utils.WriteUsageLog("CONTENT__" + $(params.element).parents('.opt').attr('guid') + "__" + $(params.element).parents('.opt').attr('version') + "__optimization__Client__1____optimization________");
      }     
      else {
        var _objContainer = $ss.agentcore.utils.activex.GetObjInstance();
        _objContainer.LogMsg("CONTENT__" + $(params.element).parents('.opt').attr('guid') + "__" + $(params.element).parents('.opt').attr('version') + "__optimization__Client__1____optimization________");
      }
      $ss.agentcore.utils.RunCommand(path, options);
    },

        InitAdvanced: function () {
            $("#snapin_perfman_advanced #optList").empty();
            var strCategories = _config.GetConfigValue("snapin_perfman", "snapin_perfman_categories_advanced", "");
            var arrCategories = strCategories.split(",");

            _helper.RenderOptimizationsTo(_content.GetContentsAdvQuery(["sprt_optimize"], { category: { has: arrCategories } }), '#snapin_perfman_advanced #optList', {}, null);
        },

        InitAdvancedFiltered: function () {
      var strCategories = _config.GetConfigValue("snapin_perfman", "snapin_perfman_categories_advanced", "");
      var arrCategories = strCategories.split(",");

            _helper.RenderOptimizationsTo(_content.GetContentsAdvQuery(["sprt_optimize"], { cid: { has: selectedRowIds } }), '#snapin_perfman_advanced #optList', {}, null);
    },

        InitHistory: function () {
      _helper.GetHistoryList();
    },

        executeOptimizations: function () {
            //Survey Feature Starts
            var arrSurveyData = [];
            try {
                //Survey Feature End
                if (bMac) {
                    var macOptimizer = _helper.InitOptimizerForMac(this);
                    if (macOptimizer.GetStatus(_config.GetProviderID()) !== 0) {
                        //_helper.ShowModal(_helper.GetLocalizedString("snp_perfman_quick_already_progress"));
                        alert(_helper.GetLocalizedString("snp_perfman_quick_already_progress"));
                        return;
                    }

                    if (macOptimizer == null) {
                        //_helper.ShowModal(_helper.GetLocalizedString("snp_perfman_quick_pm_error"));
                        alert(_helper.GetLocalizedString("snp_perfman_quick_pm_error"));
                        return;
                    }
                }else {
      _helper.InitOptimizer();
      if (optimizer.GetStatus(_config.GetProviderID()) !== 0) {
        //_helper.ShowModal(_helper.GetLocalizedString("snp_perfman_quick_already_progress"));
        alert(_helper.GetLocalizedString("snp_perfman_quick_already_progress"));
        return;
      }

      if ($('#optimizer').length == 0) {
        //_helper.ShowModal(_helper.GetLocalizedString("snp_perfman_quick_pm_error"));
        alert(_helper.GetLocalizedString("snp_perfman_quick_pm_error"));
        return;
      }
                }

                

      var checkedOptimizations = $('#optList input:checkbox:checked');
      if (checkedOptimizations.length == 0) {
        //_helper.ShowModal(_helper.GetLocalizedString("snp_perfman_quick_select_optimization"));
        alert(_helper.GetLocalizedString("snp_perfman_quick_select_optimization"));
        return;
      }

      _helper.SetOptimizationsRunningTo(true);
      this.expandAll();

      $("#snapin_perfman_quick .colToggle").hide();
      $("#snapin_perfman_quick #exOptButton").hide();
      $("#snapin_perfman_quick #OptBtnStop").show();
      $("#snapin_perfman_quick #OptOptions").hide();
      $("#snapin_perfman_quick #selectAll").hide();
      $("#snapin_perfman_quick #optList .colCheckBox").hide();
      $("#snapin_perfman_quick #optList .colResultText").show();
      $("#snapin_perfman_quick #optList .colResultIcon").show();
      $("#snapin_perfman_quick #optList .category .colResultText").hide();
      $("#snapin_perfman_quick #optList .category .colResultIcon").hide();

      // Hide all the optimizations - used for only displaying optimizations that are checked
      $("#snapin_perfman_quick .opt").hide();

      var xToDoList = "";
                //Survey Feature Starts
                var Contents = [];
                //Survey Feature End

                checkedOptimizations.each(function (index) {
        if ($(this).attr('value').length > 2) // Check to see if it already exists
        {
          if (xToDoList.indexOf($(this).attr('value')) == -1) {
            xToDoList += '<Optimization><OptimizationGuid>' + $(this).attr('value') + '</OptimizationGuid></Optimization>';

            // Show only the items and categories that have checked optimizations
            $(this).parents('.optCategory').find('.category').show();
            $(this).parents('.opt').show();
          }
        }
      });

      _utils.Sleep(0);

      xToDoList = '<?xml version="1.0"?><OptimizationsList><Optimizations>' + xToDoList + '</Optimizations></OptimizationsList>'
      //$ss.snapin.perfman.helper.log('xToDoList - <xmp>' + xToDoList + '</xmp>');

      var file_todolist = _file.BuildPath(_helper._path_sprt_optimize, "optimizationstodolist.xml");
      _file.WriteNewFile(file_todolist, xToDoList, _constants.FILE_MODE.WRITE);
      var bCreateRestore = true;
      try {
        if ($('#OptRestore:checked').val()) {
          _utils.Sleep(0);
          bCreateRestore = _helper.CreateSystemRestore();
          _utils.Sleep(0);
        }
      } catch (err) {
        //To Do : Handle error log consistently
        $ss.snapin.perfman.helper.log(err.name + " => " + err.message);
      }
      if (bCreateRestore) {
                    //GS Customization starts - to identify if the optimizer is executed manually or through the scheduler					
                    var provider = $ss.agentcore.dal.config.GetProviderID();
                    var user = $ss.agentcore.dal.config.ExpandSysMacro("%USER%");
                    var prodName = $ss.agentcore.dal.config.ParseMacros("%PRODUCT%");
                    var regPath = "Software\\SupportSoft\\ProviderList\\" + provider + "\\" + prodName + "\\users\\" + user + "\\ss_config\\global\\";
                    $ss.agentcore.dal.registry.SetRegValue("HKCU", regPath, "SolnExecInitiator", "manual");
					this.Class._solnExecInitiator = "manual"
                    //GS Customization ends
        try {
          if (!bMac) {
            optimizer.EnableSystemTrayNotification(bshowNotification); //set before calling optimizer.start method
          }
        } catch(ex) {
          _helper.log("setting optimizer.EnableSystemTrayNotification: " + ex.name + " => " + ex.message);
        }
        try {
                        //Survey Feature Starts
                        var objOptimizer = {};
                        if (this.Class._solnExecInitiator != "" && this.Class._solnExecInitiator == "manual") {
                            //Prepair optimize start data
                            objOptimizer.guidContent = "performance_manager";
                            objOptimizer.version = 0;
                            objOptimizer.verb = "optimizer_start";
                            objOptimizer.viewed = 1;
                            objOptimizer.comment = "optimizer_start";
                            objOptimizer.rating = "";

                            this.LogStatus(objOptimizer);
                        }
                        if (bMac) {
                            var macOptimizer = _helper.InitOptimizerForMac(this);
                            macOptimizer.Start(_config.GetProviderID(), _snapinGuidVersion);
                        }
                        else {
                          optimizer.Start(_config.GetProviderID(), _snapinGuidVersion);
                        }
                        //Survey Feature End
        } catch (err) {
          $ss.snapin.perfman.helper.log(err.name + " => " + err.message);
          if (err.message == "2") {
            //_helper.ShowModal(_helper.GetLocalizedString("snp_perfman_quick_already_progress"));
            alert(_helper.GetLocalizedString("snp_perfman_quick_already_progress"));
            return;
          }
        }
                    if (!bMac) {
                      optimizer.Halt();
                      //Survey Feature Starts
                      this.prepareSurveyData(arrSurveyData);
                      //Survey Feature End
                    }
                        
                    
      } else {
        _helper.SetOptimizationsRunningTo(false);
        this.LoadSelectedCat($("#perfman_nav_quick")[0]);
      }
                //Survey Feature Starts
            }
            catch (ex)
            { }
            finally {
                if (!bMac) {
                this.CallSurveyPage(arrSurveyData);

                if (this.Class._solnExecInitiator != "" && this.Class._solnExecInitiator == "manual") {
                    var objOptimizer = {};
                    //Prepare optimize start data
                    objOptimizer.guidContent = "performance_manager";
                    objOptimizer.version = 0;
                    objOptimizer.verb = "optimizer_stop";
                    objOptimizer.viewed = 1;
                    objOptimizer.comment = "optimizer_stop";
                    objOptimizer.rating = "";

                    this.LogStatus(objOptimizer);
                }
            }
            }
            //Survey Feature End
      // $ss.snapin.perfman.helper.log('End');
    },

        OptimizationCompleted: function() {
            var macOptimizer = _helper.InitOptimizerForMac(this);
            macOptimizer.Halt();
            var arrSurveyData = [];

            //Survey Feature Starts
            this.prepareSurveyData(arrSurveyData);
            //Survey Feature End
            this.CallSurveyPage(arrSurveyData);

            if (this.Class._solnExecInitiator != "" && this.Class._solnExecInitiator == "manual") {
                var objOptimizer = {};
                //Prepare optimize start data
                objOptimizer.guidContent = "performance_manager";
                objOptimizer.version = 0;
                objOptimizer.verb = "optimizer_stop";
                objOptimizer.viewed = 1;
                objOptimizer.comment = "optimizer_stop";
                objOptimizer.rating = "";

                this.LogStatus(objOptimizer);
            }

        },

        "#exOptButton click": function (params) {
            $('#restorePt_defaultOpt').css("display", "none");
            $('#exOptButtonViewAll').css('display', 'none');
      this.executeOptimizations();
    },

        ".perfmanhistory click": function (params) {
      this.renderSnapinView("snapin_perfman", "#col_perfman_content", "\\views\\perfman_history_detail.html", {});
      _helper.RenderHistoryDetail($(params.element).attr('path'));
    },

        "#OptBtnSaveReport click": function (params) {
            var sSaveResult = _helper.SaveToDesktop(_helper.GetStyleForMedia(), "#snapin_perfman #containerDiv", _utils.LocalXLate("snapin_perfman", "snp_perfman_report_filename_prefix") + $(params.element).attr('datemodified'));

      //_helper.ShowModal(sSaveResult, "Saved Report");
      alert(sSaveResult, "Saved Report");
      params.event.kill();
    },

        "#OptHistoryDetailBack click": function (params) {
      this.LoadSelectedCat($("#perfman_nav_history")[0]);
      params.event.kill();
    },

        "#OptBtnStop click": function (params) {
      try {
                if (bMac) {
                var macOptimizer = _helper.InitOptimizerForMac(this);
                macOptimizer.Halt();
                }
                else {
        optimizer.Halt();
                }
      } catch (err) {
        _helper.log(err.name + " => " + err.message);
      }


      $("#snapin_perfman_quick .OptRunning")
            .removeClass("OptRunning")
            .addClass("OptAbort")
            .parent()
                .find('.colResultText')
                    .text('')
                    .end();

      $("#snapin_perfman_quick #OptBtnStop").hide();
      $("#snapin_perfman_quick #OptBtnQuickBack").show();
      $("#snapin_perfman_quick .colToggle").show();
      _helper.SetOptimizationsRunningTo(false);
    },

        "#OptBtnQuickBack click": function (params) {
      this.LoadSelectedCat($("#perfman_nav_quick")[0]);
      params.event.kill();
    },

        "#OptBtnScheduleSelectBack click": function (params) {
      $ss.agentcore.dal.databag.SetValue("pm_loadscheduleFromDatabag", "true");
      this.LoadSelectedCat($("#perfman_nav_schedule")[0]);
      params.event.kill();
    },

        "#lnkSaveDefaultOptimization click": function (params) {
      var arrGuids = [];
      var val;
            $('#optList input:checkbox:checked,#OptRestore:checked').each(function () {
        val = _helper.ParseFileNameForGuid($(this).attr('value'));

        if (val.length > 2 && $.inArray(val, arrGuids) < 0) {
          arrGuids.push(val);
        }
      });
      var bValid = (arrGuids.length > 0);
      if (arrGuids.length === 1) {
        if (arrGuids[0] === "OptRestore")
          bValid = false;
      }
      if (bValid) {
        var sDispModal = _helper.GetLocalizedString("snp_perfman_quick_saved_optimization");
        //helper.ShowString(_helper.GetLocalizedString("snp_perfman_quick_saving_optimization"), true);
        try {
          _config.SetUserValue("OptimizationQuick", "DefaultOptimizations", arrGuids.join(","));
        } catch (ex) {
          sDispModal = _helper.GetLocalizedString("snp_perfman_quick_save_error_optimization");
        }
        _utils.Sleep(500);
        //_helper.ShowString(sDispModal);
        alert(sDispModal);
      } else {
        //_helper.ShowString(_helper.GetLocalizedString("snp_perfman_select_save_error_message"));
        alert(_helper.GetLocalizedString("snp_perfman_select_save_error_message"));
      }
    },

        "#lnkSaveScheduledOptimizations click": function (params) {
      var arrGuids = [];
      var val;
      
            $('#optList input:checkbox:checked,#OptRestore:checked').each(function () {
        val = _helper.ParseFileNameForGuid($(this).attr('value'));

        if (val.length > 2 && $.inArray(val, arrGuids) < 0) {
          arrGuids.push(val);
        }
      });
      var bValid = (arrGuids.length > 0);
      if (bValid) {
        var sDispModal = _helper.GetLocalizedString("snp_perfman_schedule_select_saved_scheduled_optimizations");
        _helper.ShowString(_helper.GetLocalizedString("snp_perfman_schedule_select_saving_scheduled_optimizations"), true);
        try {
          _config.SetUserValue("OptimizationSchedule", "DefaultScheduleOptimizations", arrGuids.join(","));
          var prefix = '<Optimization><OptimizationGuid>';
          var suffix = '</OptimizationGuid></Optimization>';
          var xToDoList = '<?xml version="1.0"?><OptimizationsList><Optimizations>' + prefix + arrGuids.join(suffix + prefix) + suffix + '</Optimizations></OptimizationsList>';

          var file_todolist = _file.BuildPath(_helper._path_sprt_optimize, "optimizationschedulelist.xml");
          _file.WriteNewFile(file_todolist, xToDoList, _constants.FILE_MODE.WRITE);
        } catch (ex) {
          sDispModal = _helper.GetLocalizedString("snp_perfman_schedule_select_save_error_scheduled_optimizations");
        }
        _utils.Sleep(500);
        //_helper.ShowString(sDispModal);
        alert(sDispModal);
      } else {
        //_helper.ShowString(_helper.GetLocalizedString("snp_perfman_select_save_error_message"));
        alert(_helper.GetLocalizedString("snp_perfman_select_save_error_message"));
      }
      
      params.event.kill();
    },

        "#lnkSelectScheduledOptimizations click": function (params) {
      _helper.SaveScheduleInDatabag();
      this.renderSnapinView("snapin_perfman", "#col_perfman_content", "\\views\\perfman_schedule_select.html", {});

      var strCategories = _config.GetConfigValue("snapin_perfman", "snapin_perfman_categories_quick", "");
      var arrCategories = strCategories.split(",");

            _helper.RenderOptimizationsTo(_content.GetContentsAdvQuery(["sprt_optimize"], { category: { has: arrCategories } }), "#snapin_perfman_schedule_select #optList", {}, _config.GetUserValue("OptimizationSchedule", "DefaultScheduleOptimizations"));
      params.event.kill();
    },

        "#lnkWhatCreateRestorePoint click": function (params) {
      _helper.ShowModal(_utils.LocalXLate("snapin_perfman", "snapin_perfman_quick_what_is_create_restore_point_desc"), _utils.LocalXLate("snapin_perfman", "snapin_perfman_quick_what_is_create_restore_point_ques"));
    },

        "#lnkRestoreComputer click": function (params) {
      var o = _content.GetContentByCid(["sprt_optimize"], "a0642b2a-e668-445c-a704-618085f94d41", true);
      var configXml = _utils.LoadExternalResourceBundle(o.ctype, o.cid, o.version, "config.xml");
      var path = configXml['path_' + _utils.GetOS()] || configXml.path || "";
      var options = _constants[configXml.pathOptions];
      options = !options ? 0 : options;

      $ss.agentcore.utils.RunCommand(_config.ExpandAllMacros(path), options);
    },

        ".colToggle click": function (params) {
      var categoryHeight = $(params.element).parents(".optCategory").outerHeight(true);
      var headerHeight = $(params.element).parents(".category").outerHeight(true);

      if (categoryHeight == headerHeight) {
        $(params.element).parents(".optCategory")
                .css('overflow', 'visible')
                .css('height', 'auto');
      }
      else {
        $(params.element).parents(".optCategory")
                .height(headerHeight)
                .css('overflow', 'hidden');
      }

      $(params.element).toggleClass("iconDownArrow");
    },

        expandAll: function () {
      $(".optCategory")
            .css('overflow', 'visible')
            .css('height', 'auto')
            .find('.colToggle')
                .removeClass("iconDownArrow");
        },
        //Survey Feature Starts
        prepareSurveyData: function (arrSurveyData) {
            var STATUS_ABORTED = "2";
            var STATUS_SUCCESS = "0"; // 0 is success, but no actions taken and > 0 is Success
            //Find all files with optimization status from the directory
            var fileList = _file.GetFilesList(_helper._path_sprt_optimize, "optimizationsinfo-");
            var optimizerStatusFile = "";
            var fileContent = "";

            //assuming all files are sorted get the last element from array as the latest file
            if (fileList.length > 0) {
                optimizerStatusFile = fileList[fileList.length - 1];
                var filePath = _file.BuildPath(_helper._path_sprt_optimize, optimizerStatusFile);
                fileContent = _file.ReadFile(filePath);
            }

            var xmlDoc = null;
            if (!bMac) {
            //IE Parser
            var xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
            xmlDoc.async = false;
            xmlDoc.loadXML(fileContent);
            }else {
                xmlDoc = $ss.agentcore.dal.xml.LoadXMLFromString(fileContent, false);
                xmlDoc = xmlDoc.responseXML;
            }

            var optimizationList = xmlDoc.getElementsByTagName("Optimization");

            for (var i = 0; i < optimizationList.length; i++) {
                var content = _content.GetContentByCid(["sprt_optimize"], optimizationList[i].getElementsByTagName("OptimizationGuid")[0].text, true);
                if (content) {
                    var surveyData = {};

                    if (optimizationList[i].getElementsByTagName("OptimizationResult")[0] == null) {
                        var strPMstatusCode = "";
                    }
                    else {
                        var strPMstatusCode = optimizationList[i].getElementsByTagName("OptimizationResult")[0].text;
                    }

                    //if  (strPMstatusCode >= STATUS_SUCCESS) // 0 is success, but no actions taken and > 0 is Success
                    //{
                    //Prepair survey data
                    surveyData.guidContent = content.cid;
                    surveyData.version = content.version;
                    surveyData.verb = content.title;
                    surveyData.viewed = 1;
                    surveyData.comment = content.title;
                    surveyData.data = content.ctype;
                    surveyData.result = _helper.MapPMstatusCodeToSurveystatusCode(strPMstatusCode);
                    surveyData.title = content.title;
                    surveyData.pmstatusCode = strPMstatusCode;

                    arrSurveyData.push(surveyData);
                    //}
                }
            }
        },

        CallSurveyPage: function (arrSurveyData) {
            var params = {};
            params.surveyData = arrSurveyData;
            try {
                if (arrSurveyData.length > 0 && this.Class._enableSurvey && this.Class._enableSurvey.toLowerCase() == "true") {
                    this.Class.dispatch("snapin_survey", "index", params);
                }

                var surveyRating = $ss.agentcore.dal.databag.GetValue("dictRating");
                var surveysolved = $ss.agentcore.dal.databag.GetValue("dictSolved");
                var dictSurveySubmitButtonClicked = $ss.agentcore.dal.databag.GetValue("dictSurveySubmitButtonClicked");

                var objDict = null;
                var objDictSolved = null;
                var objDictSurveySubmitButtonClicked = null;

                if (surveyRating) {
                    objDict = JSON.parse(surveyRating);
                }

                if (surveysolved) {
                    objDictSolved = JSON.parse(surveysolved);
                }

                if (dictSurveySubmitButtonClicked) {
                    objDictSurveySubmitButtonClicked = JSON.parse(dictSurveySubmitButtonClicked);
                }

                for (var i = 0; i < arrSurveyData.length; i++) {
                    var objSurvey = arrSurveyData[i];
                    objSurvey.rating = objDict[objSurvey.guidContent];
                    objSurvey.solved = objDictSolved[objSurvey.guidContent];

                    if ((this.Class._solnExecInitiator != "" && this.Class._solnExecInitiator == "manual")
                    && (objDictSurveySubmitButtonClicked[objSurvey.guidContent] == true)) {

                        this.LogStatus(objSurvey);
                    }
                }

                $ss.agentcore.dal.databag.RemoveValue("dictRating");
                $ss.agentcore.dal.databag.RemoveValue("dictSolved");
                $ss.agentcore.dal.databag.RemoveValue("dictSurveySubmitButtonClicked");
                this.Class._solnExecInitiator = "";
            }
            catch (ex) {

            }

            params.surveyData = null;
            params = null;
        },
        LogStatus: function (surveyData) {
            try {
                if (surveyData.solved == -1) { surveyData.solved = ""; }
                var oLogEvnt = $ss.agentcore.reporting.Reports.CreateLogEntry(surveyData.guidContent, surveyData.version, surveyData.verb, surveyData.viewed, surveyData.solved, surveyData.comment, surveyData.data, surveyData.rating, surveyData.pmstatusCode);
                $ss.agentcore.reporting.Reports.Log(oLogEvnt, "snapin_protectrestore");

            }
            catch (ex) {
            }
        },
        //Survey Feature End
        '#exOptButtonViewAll click': function () {
            if ($("#exOptButtonViewAll").attr("disabled") == "disabled") {
                return false;
            }
            this.InitQuick();
            $("#exOptButtonViewAll").attr("disabled", "disabled");
        },

        '#exAdvOptButtonViewAll click': function () {
            if ($("#exAdvOptButtonViewAll").attr("disabled") == "disabled") {
                return false;
            }
            this.InitAdvanced();
            $("#exAdvOptButtonViewAll").attr("disabled", "disabled");
    }

  });

  var _helper = $ss.snapin.perfman.helper;
  var _content = $ss.agentcore.dal.content;
  var _config = $ss.agentcore.dal.config;
  var _utils = $ss.agentcore.utils;
  var _file = $ss.agentcore.dal.file;
  var _fso = _file.GetFileSystemObject();
  var _constants = $ss.agentcore.constants;

  var _snapinGuidVersion = $ss.getSnapinAbsolutePath("snapin_perfman").replace($ss.agentcore.dal.config.ExpandAllMacros("%CONTENTPATH%") + "\\sprt_snapin\\", "");
  var bshowNotification = (_config.GetConfigValue("snapin_perfman", "show_system_tray_notification", "true").toLowerCase() === "true");
    var bMac = $ss.agentcore.utils.IsRunningInMacMachine();
})();
