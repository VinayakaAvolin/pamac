$ss.snapin = $ss.snapin ||{};
$ss.snapin.preference = $ss.snapin.preference ||{};
$ss.snapin.preference.helper = $ss.snapin.preference.helper ||{};
$ss.snapin.preference.helper.perfman = $ss.snapin.preference.helper.perfman ||{};

$.extend($ss.snapin.preference.helper.perfman, 
{
    RenderPreferenceDetail: function (oObj, sSnapinName, sPagePath)
    { 
        var sSnapinPath = $ss.getSnapinAbsolutePath(sSnapinName);
        try {
            return oObj.render({
                partial: sSnapinPath + sPagePath,
                absolute_url: sSnapinPath + sPagePath,
                cache: false
            });
        } 
        catch (ex) {
            return false;
        }
    }
});

$ss.snapin.preference.perfman = $ss.snapin.preference.perfman ||{};
$.extend($ss.snapin.preference.perfman, 
{
    AfterRender: function()
    {
        $('#snapin_preference_reset').hide();
        $('#snapin_preference_save').hide();
        $ss.snapin.perfman.helper.InitSchedule();
        $ss.snapin.perfman.helper.InitOptimizer();
    }
});

var prefController = typeof(SnapinPreferenceController) !== "undefined" ? SnapinPreferenceController : SnapinBaseController;
SnapinPreferenceController = prefController.extend("snapin_preference",
{
    ".snapin_perfman_schedule_frequency click": function (params) {
        $ss.snapin.perfman.helper.RenderScheduleFrequencyChange($(params.element).val());
        //params.event.kill();
    },
    "#PerfManSchedulerSaveSchedule click": function (params) {
        $ss.snapin.perfman.helper.SaveSchedule();
        //params.event.kill();
    }
});