/** @namespace Provides System Information functionality */

$ss.snapin = $ss.snapin || {};
$ss.snapin.perfman = $ss.snapin.perfman || {};
$ss.snapin.perfman.helper = $ss.snapin.perfman.helper || {};

(function () {

  // Private Variables
  var _oInitialParams;
  var _path_sprt_optimize;
  var _contentOptimizationLogs = {};
  var _lastOptimization = {};
  
  var _OptimizationResponse = {
    "-4": "OptTargetBusy",
    "-2": "OptTestConditionNotSatisfied",
    "-1": "OptError",
    "0": "OptOptimal",
    "1": "OptNotApplied",
    "2": "OptRebootRequired",
    "3": "OptReloginRequired",
    "": "OptAbort"
  };
  var _OptimizationUnMapping = {
    "OptTargetBusy": "-4",
    "OptTestConditionNotSatisfied": "-2",
    "OptError": "-1",
    "OptOptimal": "0",
    "OptNotApplied": "1",
    "OptRebootRequired": "2",
    "OptReloginRequired": "3",
    "OptAbort": "" 
  };
  var _SAStatus = {
    iSASuccess: 0,
    iSAFailure: 1,
    iSAError: 2,
    iSAUnknown: 3
  };

    //Survey Feature Starts
    var _PMstatusToSurveystatusMapping = {
        "-4": "1", // OptTargetBusy is mapping to success
        "-2": "1", //"OptTestConditionNotSatisfied" to success
        "-1": "2", //  "OptError" to Error
        "0": "0",   // "OptOptimal" to Success
        "1": "0",  //"OptNotApplied" to Success
        "2": "0",  //"OptRebootRequired" to Success
        "3": "0",  //"OptReloginRequired" Success
        "": "4"  //"OptAbort" to Failure
    };
    //Survey Feature End

  var _OptimizationConfigs = {};
  var _OptimizationRunning = {
    start: function () {
      $ss.agentcore.state.busy.AddToBusyList("snapin_perfman", _helper.GetLocalizedString("snp_perfman_exit_info"));
    },
    stop: function () {
      $ss.agentcore.state.busy.RemoveFromBusyList("snapin_perfman");
    },
    get: function () {
      if (bMac) {
        var message = {'JSClassNameKey':'Optimization','JSOperationNameKey':'GetStatus', 'JSArgumentsKey':[], 'JSISSyncMethodKey' : '1'};
        var result = jsBridge.execute(message);
        var parsedJSONObject = JSON.parse(result);
        return parsedJSONObject.Data;
      }
      return $ss.agentcore.state.busy.IsItemBusy("snapin_perfman");
    }
  };

  $.extend($ss.snapin.perfman.helper, {
    /**
    *  @name Initialize
    *  @memberOf $ss.snapin.sysinfo.helper
    *  @function
    *  @description Initialize the variables
    *  @param none
    *  @returns none
    */
    Initialize: function (sParams) {
      this._path_sprt_optimize = $ss.agentcore.dal.config.GetContextValue('SdcContext:DirUserServer');
      this._path_sprt_optimize = _file.BuildPath(this._path_sprt_optimize, "data");
      this._path_sprt_optimize = _file.BuildPath(this._path_sprt_optimize, "sprt_optimize");

      // Get Summary page Last optimization date and display content
      // Create Content Object for History Table
      var path, dateModified;

      var max_history = new Date().setDate(new Date().getDate() - _config.GetConfigValue("snapin_perfman", "snapin_perfman_history_max_days", 30));

      $.each(_file.GetFilesList(_helper._path_sprt_optimize, "optimizationsinfo-"), function (key, filename) {
        path = _file.BuildPath(_helper._path_sprt_optimize, filename);
        if (bMac) {
          dateModified = new Date(_fso.GetModifiedDate(path));
        }else {
        dateModified = new Date(_fso.GetFile(path).DateLastModified);
        }

        if (dateModified < max_history) {
          _file.DeleteFile(path, true, null);
        }
        else {
          _contentOptimizationLogs[path] = dateModified;
          _lastOptimization = !_lastOptimization.datemodified || dateModified > _lastOptimization.datemodified ? { path: path, datemodified: dateModified } : _lastOptimization;
        }
      });
    },

    scriptControl: null,
    supportActionIntialized: false,
    supportActionResult: null,
    supportActionAuth: null,
    supportActionTest: null,

    InitOptimizerForMac: function(scope) {
      if (optimizer == null) {
        try {
        optimizer = new $ss.snapin.perfman.Optimizer(_helper, scope);
      }catch(ex) {
      }
      }
      return optimizer;
    },

    InitOptimizer: function () {
      // Initialize PerformanceManager .NET ActiveX Control and create callbacks
      // Only create object and callbacks if they do not exist
      if ($('#optimizer').length == 0) {
        $('<object />')
                .attr('classid', 'clsid:04030f21-f00c-4061-a5ae-e987b6bbea77')
                .attr('id', 'optimizer')
                .appendTo($('body'));

        $.globalEval('function optimizer::OptimizationStart(optimizationGuid) { $ss.snapin.perfman.helper.OptimizationStartCallback(optimizationGuid); }');
        $.globalEval('function optimizer::OptimizationStop(optimizationGuid, optimizationListXml) { $ss.snapin.perfman.helper.OptimizationStopCallback(optimizationGuid, optimizationListXml); }');
        $.globalEval('function optimizer::OptimizerStart() { $ss.snapin.perfman.helper.OptimizerStartCallback(); }');
        $.globalEval('function optimizer::OptimizerStop() { $ss.snapin.perfman.helper.OptimizerStopCallback(); }');
      }
    },

    InitSummary: function () {
      var LastOptPerformedDate = "";
      var frequency = _config.GetUserValue("OptimizationSchedule", "frequency");

      $('#PerfManSummaryScheduleOptimization').text(_utils.LocalXLate("snapin_perfman", "snp_perfman_summary_frequency_" + (frequency > 0 ? "on" : "off")));
      $('#PerfManSummaryScheduleType').text(_utils.LocalXLate("snapin_perfman", "snp_perfman_summary_frequency_" + (frequency != "undefined" ? frequency : "0")));

      if (_lastOptimization.datemodified) {
        LastOptPerformedDate = $.dateFormat(_lastOptimization.datemodified, "m/d/Y, h:i:s a");
        this.RenderOptimizationLogXml(_lastOptimization.path, "#snapin_perfman_summary #optList");
      }
      else {
        LastOptPerformedDate = _utils.LocalXLate("snapin_perfman", "snp_perfman_summary_not_run");

        $("#snapin_perfman_summary_options").hide();
        $("#snapin_perfman_summary h2").text(_utils.LocalXLate("snapin_perfman", "snp_perfman_summary_welcome"));
        $("#snapin_perfman_summary #optList")
                .html(_utils.LocalXLate("snapin_perfman", "snp_perfman_summary_getting_started"))
                .css('overflow-y', 'hidden');
      }
      $('#PerfManSummaryLastOptPerfomedDate').text(LastOptPerformedDate);
    },

    /**
    *  @name SaveToDesktop
    *  @memberOf $ss.snapin.sysinfo.helper
    *  @function
    *  @description Saves passed in content along with style to an html file.  
    *               File is saved to desktop with the filename provided. 
    *               Popup is displayed whether save is successful
    *  @param sStyle Style to include in saved html
    *  @param sCurrentTabHTML Content to be saved
    *  @param sFilename Name of file to save as
    *  @returns none
    */
    SaveToDesktop: function(sStyle, sCurrentTabHTML, sFilename) {
      // Replace short description with full description
      $(sCurrentTabHTML)
            .find('table').each(function(index) {
              var more = $(this).find('.opt_description_more').html();

              if (more) {
                $(this).find('.description').html(more).end();
              }
            }).end()
        .html();

      sCurrentTabHTML = $(sCurrentTabHTML).find('input').attr('style', '').attr('style', 'display:none;').end().html();
      sCurrentTabHTML = sCurrentTabHTML.replace(/\<script /gi, "<!--script ").replace(/\/script>/gi, "\/script-->");

      var dtSavedDate = new Date().toLocaleString();
      sCurrentTabHTML = sStyle +
          _utils.LocalXLate("snapin_perfman", "snp_perfman_save_header", [dtSavedDate]) +
          sCurrentTabHTML;

      var sDesktopPath = _config.ExpandSysMacro("%SF_DESKTOP%") + sFilename + ".html";
      if (bMac) {
        var sDesktopPath = _config.ExpandSysMacro("%SF_DESKTOP%") + "/" + sFilename + ".html";
      }
      if (_file.WriteNewFile(sDesktopPath, sCurrentTabHTML)) {
        return _utils.LocalXLate("snapin_perfman", "snp_perfman_save_success", [sFilename]);
      }
      else {
        return _utils.LocalXLate("snapin_perfman", "snp_perfman_save_fail");
      }
    },

    /**
    *  @name GetStyleForMedia
    *  @memberOf $ss.snapin.perfman.helper
    *  @function
    *  @description Returns a string containing style to be used for media output (i.e. Print and File).
    *  @param none
    *  @returns String containing styles
    */
    GetStyleForMedia: function () {
      var str = $ss.getSnapinAbsolutePath("snapin_perfman") + "/" + _config.GetConfigValue("snapin_perfman", "media_styles", "");
      return ("<style>" + _file.ReadFile(str) + "</style>");
    },
    GetOptId: function (guid) {
      return '.opt' + guid.replace('.', '_');
    },
    OptimizationStartCallback: function (optimizationGuid) {
      if (_helper.AreOptimizationsRunning() === false) return;

      $(this.GetOptId(optimizationGuid))
            .find('.resultIcon')
                .addClass('OptRunning')
                .end()
            .find('.colResultText')
                .addClass('OptRunning')
                .text('')
                .end();

      _utils.Sleep(0);
      //this.log('OptimizationStart Event Fired for optimizationGuid - ' + optimizationGuid);
    },
    AdaptSAExitType: function (result) {
      var _ExitTypeOptimizationResponse = {
        "1": "OptError",
        "0": "OptOptimal",
        "": "OptAbort"
      };
      if (_ExitTypeOptimizationResponse[result]) {
        var Resp = _ExitTypeOptimizationResponse[result];
        result = _OptimizationUnMapping[Resp];
      }
      return result;
    },

    AdaptErrorCode: function (result) {
    /* New object declared for ASK exit code mapping */
      var _ASKOptimizationResponse = {
        "-4": "OptTargetBusy",
        "-2": "OptTestConditionNotSatisfied",
        "-1": "OptError",
        "-3": "OptError",
        "0": "OptOptimal",
        "1": "OptError",
        "2": "OptRebootRequired",
        "3": "OptReloginRequired",
        "4": "OptError",
        "5": "OptOptimal",
        "6": "OptNotApplied",
        "": "OptAbort",
        "-5": "OptError",
        "-100": "OptError",
        "-110": "OptError",
        "-111": "OptError",
        "-120": "OptError",
        "-121": "OptError",
        "-10": "OptTestConditionNotSatisfied",
        "-11": "OptTestConditionNotSatisfied",
        "-12": "OptTestConditionNotSatisfied",
        "-13": "OptNotApplied",
        "-14": "OptAbort",
        "999": "OptAbort"
      };
      if (_ASKOptimizationResponse[result]) {
        var Resp = _ASKOptimizationResponse[result];
        return _OptimizationUnMapping[Resp];
      }
      return null;
    },

    AdaptUsageLogString: function (str) {
      if (str.length == 0)
        return str;
		  
      var arrStr = str.split("__");
      if (!arrStr[11]) return str;
      if (arrStr[11].length <= 0) return str;

      var libraryType = _helper.GetOptLibType(arrStr[1], arrStr[2]);
      var oExit = $ss.agentcore.exitcodes.GetExitCodeDetails(libraryType, arrStr[11]);

      arrStr[11] = _helper.GetSAMappedOptimizationResult(libraryType, oExit, arrStr[11]);
      return arrStr.join("__");
    },

    GetOptLibType: function (sCid, sVersion) {
      return _content.GetContentFieldValue("sprt_optimize", sCid, sVersion, "librarytype", "sccf_field_value_char");
    },

    GetSAMappedOptimizationResult: function (libraryType, oExit, result) {
      if (libraryType !== "") {
        if (oExit !== null) {
          if (libraryType === "ASK_SA") {
             result = _helper.AdaptErrorCode(result);
          } 
          if (!result || libraryType !== "ASK_SA") {
            //if exit is defined and result of ASK is not found and library type is not ASK_SA
            //then the result will be adapted based on the exit type
            result = _helper.AdaptSAExitType(oExit.exittype);
          }
        } else {
          return null; //if library is valid but exit is undefined then return null
        }
      }
      return result;
    },

    GetExitCodeDescription: function (libraryType, oExit) {
      if (libraryType !== "") {
        if (oExit !== null)
          return oExit.desc;
        else
          return _utils.LocalXLate("snapin_perfman", "snapin_perfman_quick_creating_undefined_output");
      }
      return null;
    },

    OptimizationStopCallback: function (optimizationGuid, optimizationListXml) {
      if (_helper.AreOptimizationsRunning() === false) return;

      var xml = _xml.LoadXMLFromString(optimizationListXml, false, null);
       if (bMac) {
        xml = xml.responseXML;
      }
      //var result = $(xml).find('OptimizationGuid:contains(' + optimizationGuid + ')').parent().find('OptimizationResult').text();
      var result = $(xml).find('OptimizationResult').text();
      var oList = $("#optList");
      var optId = $(this.GetOptId(optimizationGuid));
      var contentId = $(xml).find('OptimizationGuid').text();
      if (bMac) {
          var arrStr = contentId.split(".");
          if (arrStr.length > 0) {
            contentId = arrStr[0];
          }

      }
      var version = _OptimizationConfigs[contentId].version;
      
      var libraryType = _helper.GetOptLibType(contentId, version);
      var oExit = $ss.agentcore.exitcodes.GetExitCodeDetails(libraryType, result);

      var sText = _helper.GetExitCodeDescription(libraryType, oExit);
      result = _helper.GetSAMappedOptimizationResult(libraryType, oExit, result);
      var sClass = _OptimizationResponse[result];      
      var tooltiptext = "";
      if (libraryType === "") {
        tooltiptext = _utils.LocalXLate("snapin_perfman", "snp_perfman_optimization_response_tooltip_" + sClass);
      }
      if (!sText) var sText = _utils.LocalXLate("snapin_perfman", "snp_perfman_optimization_response_" + sClass);
      if (sClass == undefined) sText = _utils.LocalXLate("snapin_perfman", "snapin_perfman_quick_creating_undefined_output");
      
      optId.find('.resultIcon, .colResultText')
          .removeClass('OptRunning')
          .addClass(sClass)
          .end()
        .find('.colResultText')
          .text(sText)
          .end()
                .find('.colResultText').attr('title', (sClass ? tooltiptext : '')).end()
            //Survey Feature Starts
            .find('.solutionResult')
          .attr('value', result).end()
          .end();
            //Survey Feature End

    var center = (oList.height() - optId.height()) / 2;
    var scrollto = optId.position().top - oList.position().top + oList.scrollTop();

    oList.scrollTop((scrollto - center > 0) ? scrollto - center : scrollto);

    _utils.Sleep(0);

            //Survey Feature Starts
            return _OptimizationResponse;
            //Survey Feature End

      //this.log("OptimizationStop Event Fired for optimizationGuid - " + optimizationGuid); 
      //this.log("<xmp>" + xml.xml + "</xmp>");
      //this.log("<xmp>" + optimizer.GetOptimizationsInfo() + "</xmp>");
    },

    OptimizerStartCallback: function () {
      _utils.Sleep(0);
      //this.log("OptimizerStart Event Fired"); 
      //this.log("<xmp>" + optimizer.GetCurrentRunInfo() + "</xmp>");
    },

    OptimizerStopCallback: function () {
      if (_helper.AreOptimizationsRunning() === false) return;

      $("#snapin_perfman_quick #OptBtnStop").hide();
      $("#snapin_perfman_quick #OptBtnSaveReport").show();
      $("#snapin_perfman_quick #OptBtnQuickBack").show();
      $("#snapin_perfman_quick .colToggle").show();
      _helper.SetOptimizationsRunningTo(false);

      _utils.Sleep(0);

      // Update Summary and History tabs
      this.Initialize();
      $('#OptBtnSaveReport').attr('datemodified', $.dateFormat(_lastOptimization.datemodified, "Y_m_d_h_i_sa"));

      //this.log("OptimizerStop Event Fired"); 
      //this.log("<xmp>" + optimizer.GetLastRunInfo() + "</xmp>");
    },

    AreOptimizationsRunning: function () {
      return _OptimizationRunning.get();
    },

    SetOptimizationsRunningTo: function (bState) {
      if (bState)
        _OptimizationRunning.start();
      else
        _OptimizationRunning.stop();
    },

    log: function (t) {

      var d = new Date();
      //alert(t);

      $('<span />')
   		    .html(d + "<br>" + t + "<hr />")
		    .appendTo($('#optList'));

    },

    InitSchedule: function () {
      if (_databag.GetValue("pm_loadscheduleFromDatabag") == "true") {
        this.InitScheduleFromDatabag();
      }
      else {
        $(".snapin_perfman_schedule_frequency[value='" + _config.GetUserValue("OptimizationSchedule", "frequency") + "']").click();
        $("select[name='hour']").val(_config.GetUserValue("OptimizationSchedule", "hour"));
        $("select[name='minute']").val(_config.GetUserValue("OptimizationSchedule", "minute"));
        $("select[name='dayofmonth']").val(_config.GetUserValue("OptimizationSchedule", "dayofmonth"));
        $("select[name='dayofweek']").val(_config.GetUserValue("OptimizationSchedule", "dayofweek"));
        $("input[name='ScheduleOptRestore']").attr('checked', (_config.GetUserValue("OptimizationSchedule", "ScheduleOptRestore") == "true"));
      }
    },

    InitScheduleFromDatabag: function() {
      $(".snapin_perfman_schedule_frequency[value='" + _databag.GetValue("pm_schedule_frequency") + "']").click();
      $("select[name='hour']").val(_databag.GetValue("pm_hour"));
      $("select[name='minute']").val(_databag.GetValue("pm_minute"));
      $("select[name='dayofmonth']").val(_databag.GetValue("pm_dayofmonth"));
      $("select[name='dayofweek']").val(_databag.GetValue("pm_dayofweek"));
      $("input[name='ScheduleOptRestore']").attr('checked', (_databag.GetValue("pm_ScheduleOptRestore") == "true"));

      $ss.agentcore.dal.databag.SetValue("pm_loadscheduleFromDatabag", "false");
    },

    SaveScheduleInDatabag: function() {
      var frequency = $("input:radio[name='frequency']:checked").val();
      var hour = $("select[name='hour']").val();
      var minute = $("select[name='minute']").val();
      var dayofmonth = parseInt($("select[name='dayofmonth']").val());
      var dayofweek = parseInt($("select[name='dayofweek']").val());
      var ScheduleOptRestore = $("input[name='ScheduleOptRestore']").attr('checked');

      _databag.SetValue("pm_schedule_frequency", frequency);
      _databag.SetValue("pm_hour", hour);
      _databag.SetValue("pm_minute", minute);
      _databag.SetValue("pm_dayofmonth", dayofmonth);
      _databag.SetValue("pm_dayofweek", dayofweek);
      _databag.SetValue("pm_ScheduleOptRestore", ScheduleOptRestore);
    },

    RenderScheduleFrequencyChange: function (val) {
      $("#snapin_perfman_schedule #snapin_perfman_schedule_content_body div").hide();
      $("#snapin_perfman_schedule #snapin_perfman_schedule_" + val).show();

      if (val != 0) {
        $("#snapin_perfman_schedule #snapin_perfman_schedule_1").show();
      }

      if (val == 7 || val == 30) {
        $("#snapin_perfman_schedule #snapin_perfman_schedule_restore").show();
      }
    },

    SaveSchedule: function () {
      var hasOptimizer = false;
      if (bMac) {
        hasOptimizer = optimizer != null;
      }
      else {
        hasOptimizer = $('#optimizer').length > 0; 
      }

      if (hasOptimizer) {
        var sDispModal = _helper.GetLocalizedString("snp_perfman_schedule_saved");
        //_helper.ShowString(_helper.GetLocalizedString("snp_perfman_schedule_saving"), true);
        //alert(_helper.GetLocalizedString("snp_perfman_schedule_saving"), true);
        try {
          var frequency = $("input:radio[name='frequency']:checked").val();
          _config.SetUserValue("OptimizationSchedule", "frequency", frequency);

          if (frequency > 0) {
            var bValidated = ValidateSaveSchedule();
            if (!bValidated) return false;

            var hour = $("select[name='hour']").val();
            var minute = $("select[name='minute']").val();
            var dayofmonth = parseInt($("select[name='dayofmonth']").val());
            var dayofweek = parseInt($("select[name='dayofweek']").val());
            var ScheduleOptRestore = $("input[name='ScheduleOptRestore']").attr('checked');

            var d = new Date();
            var current_hour = d.getHours();
            var current_minute = d.getMinutes();

            d.setHours(hour, minute, 0, 0);

            var schedule_frequency, scheudle_day;

            if (frequency == 1) {
              if (((hour == current_hour) && (minute >= current_minute)) ||
                (hour > current_hour)) {
                d.setDate(d.getDate());
              }
              else {
                d.setDate(d.getDate() + 1);
              }
              schedule_frequency = 1;
              schedule_day = 0;
            }
            else if (frequency == 7) {
              var dow = d.getDay();
              d.setDate(d.getDate() + dayofweek - dow + (dow > dayofweek ? 7 : 0));
              schedule_frequency = 2;
              schedule_day = dayofweek;

            }
            else if (frequency == 30) {
              d.setMonth(d.getMonth() + (dayofmonth < d.getDate() ? 1 : 0), dayofmonth);
              schedule_frequency = 3;
              schedule_day = dayofmonth;
            }

            var schedule_datetime = $.dateFormat(d, "m/d/Y g:i:s");

            _config.SetUserValue("OptimizationSchedule", "schedule_datetime", schedule_datetime);
            _config.SetUserValue("OptimizationSchedule", "schedule_frequency", schedule_frequency);
            _config.SetUserValue("OptimizationSchedule", "schedule_day", schedule_day);
            _config.SetUserValue("OptimizationSchedule", "hour", hour);
            _config.SetUserValue("OptimizationSchedule", "minute", minute);
            _config.SetUserValue("OptimizationSchedule", "dayofmonth", dayofmonth);
            _config.SetUserValue("OptimizationSchedule", "dayofweek", dayofweek);
            _config.SetUserValue("OptimizationSchedule", "ScheduleOptRestore", ScheduleOptRestore);

            try {
              if (!bMac) {
                optimizer.EnableSystemTrayNotification(bshowNotification); //set before calling optimizer.start method
              }
            } catch (ex) {
              _helper.log("setting optimizer.EnableSystemTrayNotification: " + ex.name + " => " + ex.message);
            }

            optimizer.Schedule(_config.GetProviderID(), _snapinGuid, schedule_datetime, schedule_frequency, schedule_day);
            this.ClearScheduleFromDatabag();
          }
          else {
            optimizer.Disable(_config.GetProviderID());
          }

        } catch (err) {
          //alert(err.name + " => " + err.message);
          sDispModal = err.name + " => " + err.message;
        }
        _utils.Sleep(500);
        //_helper.ShowString(sDispModal);
        alert(sDispModal);
      }
    },

    ClearScheduleFromDatabag: function () {
      _databag.RemoveValue("pm_schedule_frequency");
      _databag.RemoveValue("pm_hour");
      _databag.RemoveValue("pm_minute");
      _databag.RemoveValue("pm_dayofmonth");
      _databag.RemoveValue("pm_dayofweek");
      _databag.RemoveValue("pm_ScheduleOptRestore");
    },

    GetHistoryList: function () {
      $.each(_contentOptimizationLogs, function (path, datemodified) {
        $('#perfman_history_table tbody')
                .prepend($('<tr />')
                    .append($('<td />').text($.dateFormat(datemodified, "m/d/Y, h:i:s a")))
                    .append($('<td />')
                        .append($('<a />')
                            .attr('path', path)
                            .addClass('perfmanhistory')
                            .text(_utils.LocalXLate("snapin_perfman", "snp_perfman_history_detail_desc"))))
                )
      });

      $('#perfman_history_table tbody tr:odd').addClass('odd-row');
      $('#perfman_history_table tbody tr:even').addClass('even-row');

      if ($('#perfman_history_table tbody tr').length == 0) {
        $('#perfman_history_table tbody').hide();
        $('#snapin_perfman_history .scrollarea').text(_utils.LocalXLate("snapin_perfman", "snp_perfman_history_no_optimizations"));
      }

      //        alert($ss.agentcore.utils.string.ConvertJSObjectToString(filelist));
      //        if(filelist.length > 0)
      //            return "<ul><li>" + filelist.join("</li><li>") + "</li></ul>";
      //        else   
      //            return "No History";
    },

    RenderHistoryDetail: function (path) {
      var dateModified = null;
       if (bMac) {
          dateModified = new Date(_fso.GetModifiedDate(path));
        }else {
          dateModified = new Date(_fso.GetFile(path).DateLastModified);
        }
      $('#snapin_perfman_history_detail .datecreated').append($('<span>' + dateModified.toLocaleString() + '</span>'));

      $('#OptBtnSaveReport').attr('datemodified', $.dateFormat(_contentOptimizationLogs[path], "Y_m_d_h_i_sa"));

      this.RenderOptimizationLogXml(path, "#snapin_perfman_history_detail #optList");
    },

    RenderOptimizationLogXml: function (path, selector) {
      var arrCids = [];
      var arrResult = {};
      var guid = "";

      var xml = _xml.LoadXML(path, false, null);
      if (bMac) {
        xml = xml.responseXML;
      }
      $(xml).find('OptimizationGuid').each(function (key, obj) {

        guid = _helper.ParseFileNameForGuid($(obj).text());

        arrCids.push(guid);
        arrResult[guid] = $(obj).parent().find('OptimizationResult').text();
      });

      this.RenderOptimizationsTo(_content.GetContentsAdvQuery(["sprt_optimize"], { cid: { has: arrCids } }), selector, arrResult, null);

      // Use this line to debug xml in log file
      //$('#snapin_perfman_history_detail #optList').append($('<xmp>' + xml.xml + '</xmp>'));
    },

    sortByTitle: function (a, b) {
      var x = a.category.toString().toLowerCase() + a.title.toLowerCase();
      var y = b.category.toString().toLowerCase() + b.title.toLowerCase();
      return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    },

    SetOptimizationDetails: function (sContentType, sContentID, sContentVer, sConfigPath) {
      _OptimizationConfigs[sContentID] = _utils.LoadExternalResourceBundle(sContentType, sContentID, sContentVer, sConfigPath);
      _OptimizationConfigs[sContentID].version = sContentVer;
    },

    GetOptimizationDetails: function (sContentId) {
      if (_OptimizationConfigs[sContentId]) {
        return _OptimizationConfigs[sContentId];
      }
      return false;
    },

    IsCachedConfigExisting: function (oContent) {
      var sCachedConfigAbsPath = _helper.GetAbsPathOfCahcedConfig(oContent);
      if (_file.FileExists(sCachedConfigAbsPath)) {
        return true
      }
      return false;
    },

    GetAbsPathOfCahcedConfig: function (oContent) {
      var sRelPathOfCachedConfig = _helper.GetRelativePathOfCahcedConfig(oContent);
      var sCachedConfigAbsPath = _helper._path_sprt_optimize + "\\" + oContent.cid + "." + oContent.version + sRelPathOfCachedConfig;
      return sCachedConfigAbsPath;
    },

    GetRelativePathOfCahcedConfig: function (oContent) {
      var sRelPathOfCachedConfig = "\\..\\configs\\" + oContent.cid + "." + oContent.version + ".xml";
      return sRelPathOfCachedConfig;
    },

    IsLatestAvailConfigLoaded: function (oContent) {
      var oOptDetails = _helper.GetOptimizationDetails(oContent.cid);
      if (!oOptDetails)
        return false;
      else if (parseInt(oContent.version) > parseInt(oOptDetails.version))
        return false;

      return true;
    },

    LoadConfig: function (oCont) {
      // Use the cached config file if it exists; this file remains even if the content file gets removed.
      // If the cached config does not exist, it will try to use the current piece of content
      // If neither exist, it will not display
      var sConfigRelativePath = "";
      if (_helper.IsCachedConfigExisting(oCont)) {
        sConfigRelativePath = _helper.GetRelativePathOfCahcedConfig(oCont)
      } else {
        sConfigRelativePath = "config.xml";
      }
      _helper.SetOptimizationDetails(oCont.ctype, oCont.cid, oCont.version, sConfigRelativePath);
    },

    RenderOptimizationsTo: function(content, selector, result, defaultChecked) {
      var configXml, cid, sTitle, sDescription, sDescriptionMore, sPath, sPathOptions;
      var sDefaultOptimizations = defaultChecked || "";

      var aTmpstore = [];
      if(selOptimizeId.length > 0){
        for(var i = 0;i<selOptimizeId.length;i++){
          aTmpstore.push(selOptimizeId[i].id);
        }
        sDefaultOptimizations = aTmpstore.toString(); //over write if coming from Search page
        aTmpstore = null;
      }
      try {
        //GS Customization Start - Commented next line to uncheck restore point - 07 March,2012
        //$('#OptRestore').attr('checked', (sDefaultOptimizations.indexOf("OptRestore") == -1 && sDefaultOptimizations != "undefined") ? false : true)
        //GS Customization End
        content.sort(_helper.sortByTitle);

        $.each(content, function (key, o) {
          if (!_helper.IsLatestAvailConfigLoaded(o)) {
            _helper.LoadConfig(o);
          }

          configXml = _helper.GetOptimizationDetails(o.cid);

          //          alert(_utils.string.ConvertJSObjectToString(configXml));

          cid = configXml.id || 'opt' + o.cid + '_' + o.version;
          sTitle = configXml.title || "";
          sDescription = configXml.description || "";
          sDescriptionMore = configXml.description_more || "";
          sPath = configXml['path_' + _utils.GetOS()] || configXml.path || "";
          sPathOptions = configXml.pathOptions || "";
          //Added to address issues in History Report - Begin
          var nExitCode = result[o.cid];

          var libraryType = _helper.GetOptLibType(o.cid, o.version);
          var oExit = $ss.agentcore.exitcodes.GetExitCodeDetails(libraryType, result[o.cid]);
          var sText = _helper.GetExitCodeDescription(libraryType, oExit);
          nExitCode = _helper.GetSAMappedOptimizationResult(libraryType, oExit, nExitCode);
          var sClass = _OptimizationResponse[nExitCode];
          var tooltiptext = "";
          if (libraryType === "") {
            tooltiptext = _utils.LocalXLate("snapin_perfman", "snp_perfman_optimization_response_tooltip_" + sClass);
          }
          if (!sText) sText = _utils.LocalXLate("snapin_perfman", "snp_perfman_optimization_response_" + sClass);
          //Added to address issues in History Report - End		
		
          // Initialize Array if undefined
          if (o.category.length == 0) {
            o.category = new Array("Uncategorized");
          }

          // Create category div and title bar if they do not exist
          $.each(o.category, function (ck, category) {
            if ($('#' + category).length == 0) {
              $('<div class="optCategory" />')
                        .attr('id', category)
                        .append($('#optTemplate')
                            .clone()
                            .attr('id', '')
                            .addClass('category')
                            .find(':checkbox')
                                .click(function () {
                                  $('#' + $(this).parents('.optCategory').attr('id'))
                                        .find(':checkbox')
                                            .prop('checked', $(this).prop('checked')).end();

                                  _helper.UpdateCheckboxSelection();
                                }).end()
                            .find('.title').text(_utils.LocalXLate("snapin_perfman", category)).end())
                        .appendTo($(selector));
            }

            // not sure exactly why an extra object is being returned in the array.
            if (key != "include") {
              $('#optTemplate')
                        .clone()
                        .attr('id', cid)
                        .attr('guid', o.cid)
			            .attr('version', o.version)
                        .addClass('opt' + o.cid + '_' + o.version)
                        .appendTo($(selector + ' #' + category));

              $('#' + cid)
			            .find(':checkbox')
				            .attr('value', o.cid + '.' + o.version)
                            .attr('guid', o.cid)
                            .click(function () {
                              _helper.UpdateCheckboxSelection();
                            })
                            .prop('checked', (sDefaultOptimizations.indexOf(o.cid) == -1) ? false : true)
				            .end()

			            .find('.title').html(sTitle).end()
                        .find('.description').html(sDescription).end()
                        .find('.colRightText')
                            .append(
                                $('<a data-toggle="modal" data-target="#myModal" />')
                                    .text(_utils.LocalXLate("snapin_perfman", "snp_perfman_optimization_response_more_details"))
                                    .append(
                                      $('<div style="display: none" class="opt_title" />').html(sTitle),
                                      $('<div style="display: none" class="opt_description_more" />').html(sDescriptionMore)
                                    )
                                    .click(function() {
                                      _helper.ShowModal(_helper.CreateModalInfoHTML($(this).find('.opt_description_more').html()), $(this).find('.opt_title').html());
                                    })
                            )
                            .end()
                        .find('.PerfManAdvancedLaunch')
                            .attr('path', sPath)
                            .attr('pathOptions', sPathOptions)
                            .end()
                        .find('.resultIcon, .colResultText').addClass(sClass ? sClass : '').end()
                        .find('.colResultText').text((sClass ? sText : '')).end()
                        .find('.colResultText').attr('title', (sClass ? tooltiptext : '')).end()
            }
          });

          if (key % 3 == 0) {
            _utils.Sleep(0);
          }
        });

        _helper.UpdateCheckboxSelection();
      } catch (ex) { }
    },

    ParseFileNameForGuid: function(str) {
      return str.split('.')[0];
    },

    CreateModalInfoHTML: function(desc) {
      return '<i class="fa fa-info-circle fa-lg success-msg"></i><span class="medium-text-Font padding-left">' + desc + '</span>';
    },

    ShowString: function(sString, bShowImage) {
      var sHtml = "";
      if (bShowImage)
        sHtml += "<div class='loading'>&nbsp;</div>";

      sHtml += "<span>" + sString + "</span>";
      _helper.ShowModal(sHtml, _helper.GetLocalizedString("snp_perfman_savedefault_opt_title"),'');
    },

    ShowModal: function(html, title) {
      $('.modal-header').html('<b>' + title + '</b>' + '<button type="button" class="close" data-dismiss="modal">&times;</button>');
      $('.modal-body').html(html);
      $ss.agentcore.utils.ui.FixPng();
    },

    UpdateCheckboxSelection: function() {
      var valSelectAll = true;
      $('.optCategory').each(function(index) {
        // Look at all the checkboxes in the same category (except the category checkbox) and set the category checkbox accordingly
        var categoryValue = true;

        $(this).find(":checkbox").each(function(index) {
          if ($(this).parents('.category').length == 0 && $(this).prop('checked') == false) {
            categoryValue = false;
          }
        });

        $(this).find(".category :checkbox").prop('checked', categoryValue);

        if (categoryValue == false) {
          valSelectAll = false;
        }
      });
      $('#selectAllCheckBox').prop('checked', valSelectAll);
    },

    GetLocalizedString: function (sResId, sSectionId) {
      if (sSectionId == false || sSectionId === "" || sSectionId == undefined)
        sSectionId = "snapin_perfman";
      return _utils.LocalXLate(sSectionId, sResId);
    },

    SupportActionCallBack: function (context_in, event_in) {
      var sSuccess = "0";
      switch (context_in) {
        case "ScriptAuthentication":
          if (event_in === "OK")
            $ss.snapin.perfman.helper.supportActionAuth = true;
          else
            $ss.snapin.perfman.helper.supportActionAuth = false;
          break;
        case "TEST":
          if (event_in === sSuccess)
            $ss.snapin.perfman.helper.supportActionTest = true;
          else
            $ss.snapin.perfman.helper.supportActionTest = false;
          break;
        case "STDOUT":
          if ($ss.snapin.perfman.helper.supportActionIntialized) {
            if (event_in === sSuccess)
              $ss.snapin.perfman.helper.supportActionResult = _SAStatus.iSASuccess;
            else
              $ss.snapin.perfman.helper.supportActionResult = _SAStatus.iSAError;
          }
          break;
        default: break;
      }
    },

    CreateSystemRestore: function () {
      var bContinue = false;
      var bElevated = false;
      var sContentGuid;
      var ElevationLevel = undefined;
      var fCallBack = this.SupportActionCallBack;
      var sSaName = _config.GetConfigValue("snapin_perfman", "create_restore_sa_filename", "CreateRestore.cab");
      var cabFileName = _helper.GetRootFolderPath() + "content\\SupportAction\\" + sSaName;
      var bFileExists = _file.FileExists(cabFileName);
      var retValue = -1;
      if (bFileExists) {
        try {
          _helper.ShowModal(_helper.GetLocalizedString("snapin_perfman_quick_creating_restore_point"),'');
          this.scriptControl = new $ss.agentcore.supportaction(sContentGuid, _config.GetProviderID(), cabFileName, bElevated, ElevationLevel, fCallBack, 1);
          this.supportActionIntialized = true;
          this.supportActionResult = _SAStatus.iSAUnknown;
          this.scriptControl.srCtl.SetUserInputWait(true);
          retValue = this.scriptControl.srCtl.EvaluateTestAndRun(cabFileName);
        }
        catch (ex) {
          this.supportActionResult = _SAStatus.iSAError;
        }
        finally {
          if (this.supportActionResult === _SAStatus.iSASuccess) {
            bContinue = true;
          }
          else {
            bContinue = false;
          }

          this.scriptControl = null;
          this.supportActionIntialized = false;
          this.supportActionResult = null;
        }
      }
      if (!bContinue) {
        bContinue = confirm(_helper.GetLocalizedString("snapin_perfman_quick_creating_restore_continue"));
      }
      return bContinue;
    },

    GetRootFolderPath: function () {
      return (_config.ExpandAllMacros("%PRODUCTPATH%"));
        },
        //Survey Feature Starts
        MapPMstatusCodeToSurveystatusCode: function (pmStatusCode) {
            try {
                return _PMstatusToSurveystatusMapping[pmStatusCode];
            }
            catch (ex) {
            }
            return pmStatusCode;
    }
        //Survey Feature End
  });

  //Private Data
  var _helper = $ss.snapin.perfman.helper;
  var _content = $ss.agentcore.dal.content;
  var _xml = $ss.agentcore.dal.xml;
  var _file = $ss.agentcore.dal.file;
  var _fso = _file.GetFileSystemObject();
  var _utils = $ss.agentcore.utils;
  var _config = $ss.agentcore.dal.config;
  var _constants = $ss.agentcore.constants;
  var _databag = $ss.agentcore.dal.databag;

  var _snapinGuidVersion = $ss.getSnapinAbsolutePath("snapin_perfman").replace($ss.agentcore.dal.config.ExpandAllMacros("%CONTENTPATH%") + "\\sprt_snapin\\", "");
  var _snapinGuid = _helper.ParseFileNameForGuid(_snapinGuidVersion);
  var optimizer = null;
  var bMac = $ss.agentcore.utils.IsRunningInMacMachine();
  //Private functions

  function ValidateSaveSchedule() {
    var bSaved = IsScheduleListPresent();
    if (!bSaved) {
      if (bMac) {
        alert(_helper.GetLocalizedString("snp_perfman_select_save_error_message"));
      }else {
      _helper.ShowString(_helper.GetLocalizedString("snp_perfman_select_save_error_message"));
      }
      return false;
    }
    return true;
  }

  function IsScheduleListPresent() {
    var sScheduleGuid = _config.GetUserValue("OptimizationSchedule", "DefaultScheduleOptimizations");
    if (sScheduleGuid === _constants.UNDEFINED || sScheduleGuid === "") return false;
    return true;
  }

})();

// Date formatting operations - similar to php date function - more switches should be added
// $.dateFormat(new Date(), "m-d-Y h:i a"); // "05-13-2010 11:33 am"
(function () {

  // Private Variables
  var switches = {};
  
  $.extend(jQuery, {
    dateFormat: function (oDate, sFormat) {
      switches.Y = oDate.getFullYear();                   // Y: 4-digit Year

      switches.n = oDate.getMonth() + 1;                  // n: Numeric month without leading zeros
      switches.m = this.addZero(switches.n);              // m: Numeric month with leading zeros

      switches.j = oDate.getDate();                       // j: Numeric day of the month without leading zeros
      switches.d = this.addZero(switches.j);              // d: Numeric day of the month with leading zeros

      switches.G = oDate.getHours();                      // G: 24-hour format without leading zeros
      switches.g = this.addZero(switches.G);              // g: 24-hour format with leading zeros
      switches.H = (switches.G == 0 ? 12 : (switches.G <= 12 ? switches.G : switches.G - 12));  
                                                          // H: 12-hour format without leading zeros
      switches.h = this.addZero(switches.H);              // h: 12-hour format with leading zeros

      switches.i = this.addZero(oDate.getMinutes());      // i: Minutes with leading zeros
      switches.s = this.addZero(oDate.getSeconds());      // s: Seconds with leading zeros

      switches.a = (switches.G < 12) ? "am" : "pm";       // a: Lowercase Ante meridian and Post meridian
      switches.A = (switches.G < 12) ? "AM" : "PM";       // A: Uppercase Ante meridian and Post meridian
              
      $.each(switches, function (key, val) {
        sFormat = sFormat.replace(key, val);
      });

      return sFormat;
    },

    addZero: function (n) {
      return (n < 10 ? "0" : "") + n;
    }
    
  });
})();