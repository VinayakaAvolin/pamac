$ss.snapin = $ss.snapin || {};
$ss.snapin.userinfo = $ss.snapin.userinfo || {};
$ss.snapin.userinfo.helper = $ss.snapin.userinfo.helper || {};

(function()
{
  $.extend($ss.snapin.userinfo.helper,
  {
    GetUserLogDetails : function(that){
      try{
        var loginProfileQuery = "SELECT FullName, PasswordExpires FROM Win32_NetworkLoginProfile WHERE FullName IS NOT NULL";
        var colItem = new Enumerator(window.external.GetObject("winmgmts:{impersonationLevel=impersonate}!root/cimv2").ExecQuery(loginProfileQuery));
        var objItem = colItem.item();
        if(typeof(objItem) != 'undefined'){
          that.Class.objLatestUserInfo["uFullName"] = objItem.FullName;
          var dtmWMIDate = objItem.PasswordExpires || "NA"; //"20160731000230.000000 000";
          if(dtmWMIDate && dtmWMIDate.indexOf("NA") <=-1) that.Class.objLatestUserInfo["uPswrdExpiry"] = this.GetPasswordExpiresInProperFormat(dtmWMIDate);
        }
      }catch(e){
        logger.error("Not able to fetch GetUserLogDetails");
      }
    },

    GetUserImg : function(that){
      that.Class.objLatestUserInfo["uImage"] = that.Class._registry.GetRegValue("HKCU", that.Class.userInfoRegPath, "uImage");
    },

    GetMachineTypeDetails : function(that){
      try{
        var loginProfileQuery = "SELECT ChassisTypes FROM Win32_SystemEnclosure";
        var colItem = new Enumerator(window.external.GetObject("winmgmts:{impersonationLevel=impersonate}!root/cimv2").ExecQuery(loginProfileQuery));
        var iChassisType = parseInt((colItem.item().ChassisTypes.toArray()).join(","));
        if(iChassisType)  that.Class.objLatestUserInfo["uMachineType"] = this.GetMachineNameFromChassisTypes(iChassisType);
      }catch(e){
        logger.error("Not able to fetch GetMachineTypeDetails");
      }
      
    },

    GetPasswordExpiresInProperFormat : function(expiryDate){
      expiryDate = expiryDate.slice(0, 8).replace(/(\d\d\d\d)(\d\d)(\d\d)/g, '$2/$3/$1');
      var d = new Date();
      var currDate = (d.getMonth() + 1) + '/' + d.getDate() + '/' + d.getFullYear();
      currDate = new Date(currDate);
      expiryDate = new Date(expiryDate);
      expiryDate = Math.ceil((expiryDate.getTime() - currDate.getTime()) / (1000 * 3600 * 24));
      return expiryDate;
    },

    GetMachineNameFromChassisTypes : function(val){
      var machineType="";
      switch(val){
        case 1:
          machineType = "Other";
          break;
        case 2:
          machineType = "Unknown";
          break;
        case 3:
          machineType = "Desktop";
          break;
        case 4:
          machineType = "Low Profile Desktop";
          break;
        case 5:
          machineType = "Pizza Box";
          break;
        case 6:
          machineType = "Mini Tower";
          break;
        case 7:
          machineType = "Tower";
          break;
        case 8:
          machineType = "Portable";
          break;
        case 9:
          machineType = "Laptop";
          break;
        case 10:
          machineType = "Notebook";
          break;
        case 11:
          machineType = "Hand Held";
          break;
        case 12:
          machineType = "Docking Station";
          break;
        case 13:
          machineType = "All in One";
          break;
        case 14:
          machineType = "Sub Notebook";
          break;
        case 15:
          machineType = "Space-Saving";
          break;
        case 16:
          machineType = "Lunch Box";
          break;
        case 17:
          machineType = "Main System Chassis";
          break;
        case 18:
          machineType = "Expansion Chassis";
          break;
        case 19:
          machineType = "SubChassis";
          break;
        case 20:
          machineType = "Bus Expansion Chassis";
          break;
        case 21:
          machineType = "Peripheral Chassis";
          break;
        case 22:
          machineType = "Storage Chassis";
          break;
        case 23:
          machineType = "Rack Mount Chassis";
          break;
        case 24:
          machineType = "Sealed-Case PC";
          break;
        default:
          machineType = "Unknown";
          break;
      }
      return machineType;
    },

    LoadDefaultSysInfo: function(that){
      _controllerObj = that;
    //this.showWaitImage();
    if (!bMac) {
      _allSysInfo = this.createIssueAndXslTransform("default.xml",that); //added this to scope it
      return _allSysInfo; //added to return _allSystInfo
    }
    else {
      this.createIssueAndXslTransformMac("default_mac.xml");
    }
    //return _allSystInfo;
       },

  /**
     *  @name createIssueAndXslTransform
     *  @memberOf $ss.snapin.sysinfo.helper
     *  @function
     *  @description Create issue xml for the provided template.
     *               Transforms it against sysinfo.xsl and returns the output.
     *  @param template Filename of template.  Must reside in smartissue folder
     *  @returns String containing the transformed output
     */
    createIssueAndXslTransform: function(template,that){
      var $xml = $ss.agentcore.dal.xml;
      var objSI = $ss.agentcore.diagnostics.smartissue;
      var absPath = $ss.getSnapinAbsolutePath("snapin_user_information");

      // Create Issue
      var issueID = objSI.CreateIssue("SystemInfo", true, absPath + "\\smartissue\\" + template, false, false, null);

      that.Class.objLatestUserInfo["uDNSName"] = _smartissue.GetIssueData(issueID, "SPRT_Connectivity", "ConnectionData", "DNSName");
      that.Class.objLatestUserInfo["uHostName"] = _smartissue.GetIssueData(issueID, "SPRT_Connectivity", "ConnectionData", "HostName");
      that.Class.objLatestUserInfo["uOS"] = _smartissue.GetIssueData(issueID, "SPRT_Connectivity", "ConnectionData", "OSName");
      that.Class.objLatestUserInfo["uIPAddress"] = _smartissue.GetIssueData(issueID, "SPRT_Connectivity", "ConnectionData", "TCPIP_Address");
      that.Class.objLatestUserInfo["uDomain"] = _smartissue.GetIssueData(issueID, "SPRT_Connectivity", "ConnectionData", "Domain");
      that.Class.objLatestUserInfo["uLoginID"] = _smartissue.GetIssueData(issueID, "SPRT_Connectivity", "ConnectionData", "UserName");
      that.Class.objLatestUserInfo["uMACId"] = _smartissue.GetIssueData(issueID, "EthernetInfo", "MACIDList", "MACIDList");
      that.Class.objLatestUserInfo["uTotalPhysicalMem"] = _smartissue.GetIssueData(issueID, "Win32_LogicalMemoryConfig", "Win32_LogicalMemoryConfig", "TotalPhysicalMemory");

      var sSmartIssueXsl = $xml.LoadXML(_sXslFile, false, null);
      // Transform and return XML DOM object
      var returnValue = $xml.LoadXMLFromString(sSmartIssueXml.transformNode(sSmartIssueXsl), false, null);
      // Clean up
    objSI.DeleteIssue(issueID);
      return returnValue;
    },

  createIssueAndXslTransformMac: function(template) {
      var absPath = $ss.getSnapinAbsolutePath("snapin_user_information");
      var objSI = $ss.agentcore.diagnostics.smartissue;
      objSI.CreateIssueMac(this, "SystemInfo", absPath + "/smartissue/" + template);
  },

  SmartIssueCreated: function(issueID) {
    var $xml = $ss.agentcore.dal.xml;
    var _smartissue = $ss.agentcore.diagnostics.smartissue;
    _controllerObj.Class.objLatestUserInfo["uDNSName"] = _smartissue.GetIssueDataMac(issueID, "NetworkInfo", "DNS Domain Name");
    // _controllerObj.Class.objLatestUserInfo["uHostName"] = _smartissue.GetIssueDataMac(issueID, "SPRT_Connectivity", "ConnectionData", "HostName");
    var osInfo = _smartissue.GetIssueDataMac(issueID, "OSSummary", "OSName");
    osInfo = osInfo + " ( " + _smartissue.GetIssueDataMac(issueID, "OSSummary", "CodeName") + " )";
    _controllerObj.Class.objLatestUserInfo["uOS"] = osInfo;
    _controllerObj.Class.objLatestUserInfo["uIPAddress"] = _smartissue.GetIssueDataMac(issueID, "NetworkInfo", "ip_address");
    // _controllerObj.Class.objLatestUserInfo["uDomain"] = _smartissue.GetIssueDataMac(issueID, "SPRT_Connectivity", "ConnectionData", "Domain");
    _controllerObj.Class.objLatestUserInfo["uLoginID"] = _smartissue.GetIssueDataMac(issueID, "UserInfo", "UserName");
    _controllerObj.Class.objLatestUserInfo["uFullName"] = _smartissue.GetIssueDataMac(issueID, "UserInfo", "FullUserName");
    _controllerObj.Class.objLatestUserInfo["uMACId"] = _smartissue.GetIssueDataMac(issueID, "HardwareInfo", "platform_UUID");
    _controllerObj.Class.objLatestUserInfo["uTotalPhysicalMem"] = _smartissue.GetIssueDataMac(issueID, "HardwareInfo", "physical_memory");
    _controllerObj.Class.objLatestUserInfo["uMachineType"] = _smartissue.GetIssueDataMac(issueID, "SystemInfo", "SystemType");

    // Clean up  
    _smartissue.DeleteIssue(issueID);
      // _smartissue.SubmitIssue(issueID, "");
    _controllerObj.RefreshUI();
  },

  xmlToJson: function(xml) {

    // Create the return object
    var obj = {};

    if (xml.nodeType == 1) { // element
      // do attributes
      if (xml.attributes.length > 0) {
      obj["@attributes"] = {};
        for (var j = 0; j < xml.attributes.length; j++) {
          var attribute = xml.attributes.item(j);
          obj["@attributes"][attribute.nodeName] = attribute.nodeValue;
        }
      }
    } else if (xml.nodeType == 3) { // text
      obj = xml.nodeValue;
    }

    // do children
    if (xml.hasChildNodes()) {
      for(var i = 0; i < xml.childNodes.length; i++) {
        var item = xml.childNodes.item(i);
        var nodeName = item.nodeName;
        if (typeof(obj[nodeName]) == "undefined") {
          obj[nodeName] = xmlToJson(item);
        } else {
          if (typeof(obj[nodeName].push) == "undefined") {
            var old = obj[nodeName];
            obj[nodeName] = [];
            obj[nodeName].push(old);
          }
          obj[nodeName].push(xmlToJson(item));
        }
      }
    }
    return obj;
    }
  });
}
)();