SnapinSystoolController = SnapinBaseController.extend('snapin_systool', {
  toLocation: "",
  displayResetModem: false,
  oModem: null,
	//displaySurvey:true,
	functionSucceeded:false
}, {
  /*
   * User clicked on restart system
   */
  "#snp_st_restart click": function(params){
    this.HandleSystemRestartClick(params);
  },
  
  /*
   * User clicked on OK button of restart system
   */
  "#snp_st_reboot_system_ok click": function(params){
    this.HandleSystemRestartOKClick(params);
  },
  
  /*
   * User clicked on cancel button of restart system
   */
  "#snp_st_reboot_system_cancel click": function(params){
    this.HandleSystemRestartCancelClick(params);
  },
  
  /*
   * User has clicked in release renew ip config
   */
  "#snap_ipconfig_renew_restart click": function(params){
    this.HandleIpConfigReleaseRenewClick(params);
  },
  
  /*
   * User has clicked on cancel of ipconfig
   */
  "#snp_st_ipconfig_cancel click": function(params){
    this.HandleIpconfigCancelClick(params);
  },
  
  /*
   * User has clicked on ok button of ipconfig
   */
  "#snp_st_ipconfig_ok click": function(params){
   
    this.HandleIpconfigOkClick(params);
  },
  
  /*
   * user has clicked on the done button of ipconfig
   */
  "#snp_st_ipconfig_done click": function(params){
    this.HandleIpconfigDoneClick(params);
  },
  
  /*
   * User has clicked on restart the modem
   */
  "#snap_modem_restart click": function(params){

    this.HandleRestartModemClick(params);
  },
  
  /*
   * User has clicked on the cancel button of reboot modem
   */
  "#snp_st_rebootmodem_cancel click": function(params){
    this.HandleCancelResetModemClick(params);
  },
  
  /*
   * User has clicked on the OK button of reset modem
   */
  "#snp_st_rebootmodem_ok click": function(params){
    this.HandleOkResetModemClick(params);
  },
  
  /*
   * User has clicked on detect modem
   */
  "#snp_st_detectmodem click": function(params){
    this.HandleDetectModem(params);
  },
	
	/*
	 * User has clicked on the back button after successfully rebooting the modem
	 */
	"#snp_st_rebootmodem_done click": function(params){
		this.HandleRebootModemDoneClick(params);
	},
  
  ///////////////////////////////////////////////////////////////////
  
  /*
   * This function will make a call to the modem snapin to detect the
   * modem used by the user.
   */
  HandleDetectModem: function(params){
 
    try {
      params.toLocation = this.Class.toLocation;
			params.stepListSession = {};			
			params.stepListSession.sBackToPageURL = $ss.agentcore.dal.config.GetConfigValue("snapin_systool", "BackToPageURL", "/sa/systool?refresh=yes");
			params.stepListSession.sForwardToPageURL = $ss.agentcore.dal.config.GetConfigValue("snapin_systool", "ForwardToPageURL", "/sa/systool?refresh=yes");;
			params.stepListSession.sErrorUrl = $ss.agentcore.dal.config.GetConfigValue("snapin_systool", "ErrorUrl", "/sa/systool?refresh=yes");;
			
      this.Class.dispatch("snapin_mdetect", "index", params);
			this.Class.displayResetModem = this.IsResetModemOptionAvialaible();
      $ss.agentcore.reporting.Reports.LogToolUsage("ModemDetect", "snapin_systool");     
      
    } 
    catch (ex) {
    }
  },
  
  /*
   * This function will find out whether the current modem is null or ras
   */
  /*
ModemTest: function(){
    //check whether  modem is present or not
    if ((!this.IsRasModem()) && (!this.IsNullModem())) {
      this.Class.displayResetModem = true;
    };
      },
*/
  
  /*
   * This function will get the modem name
   */
  GetModemName: function(){
    var message = "";
    var CurrentModemCode = $ss.agentcore.diagnostics.smartissue.ReadPersistInfo($ss.agentcore.constants.smartissue.MODEM_CLASS, $ss.agentcore.constants.smartissue.MODEM_CODE);
    ;
    if (CurrentModemCode === "" || CurrentModemCode === "null_modem_dsl") {
      message = $ss.agentcore.utils.LocalXLate("snapin_systool", "No Modem detected");
    }
    else {
      var modemName = $ss.agentcore.smapi.methods.GetInstanceByCode(CurrentModemCode).GetInstanceValue("name");
      message = $ss.agentcore.utils.LocalXLate("snapin_systool", "Modem Detected") + modemName;
    }
  },
  
  /*
   * This function will detect the presence of modem
   */
  DetectModem: function(){

    this.Class.oModem.Detect();
    if (($ss.agentcore.constants.SMAPI_SUCCESS === this.Class.oModem.GetLastError()) &&
    (this.Class.oModem.mObjResponse["NewStatus"] === 1)) {
      return true;
    }
    
    return false;
  },
  
  /*
   * This function will restart the modem
   */
  //
  DoModemReboot: function(){
    
    var bRet = false;
    var bDetected = false;
    try{
      if (!this.Class.oModem) {
        this.Class.oModem = $ss.agentcore.smapi.methods.GetInstance();
        if (!this.Class.oModem) {
          return bRet;
        }
			}
        
      if ($ss.agentcore.constants.SMAPI_SUCCESS === this.Class.oModem.GetLastError()) {          
        if (this.DetectModem()) {
          this.Class.oModem.Reboot();
          if ($ss.agentcore.constants.SMAPI_SUCCESS === this.Class.oModem.GetLastError()) {
            bRet = this.DetectModem();
          }
        }         
      }
     
    }
    catch(ex){
    }
    
    return bRet;
  },
  
  
  /*
   * This function wil handle the case when the user clicks on the ok button of modem reset
   */
  HandleOkResetModemClick: function(params){
    var bRet = false;
    $(".snp_st_rebootmodem_okcancel").hide();
    $("#reBootModemProgress").css("display", "block");

    //set the timer
    
    this.Class.functionSucceeded = false;   
    
    //var timeOut = Number(this.Class.oModem.GetInstanceValue("detect_timeout", "60"));
    //if ($ss.agentcore.constants.SMAPI_SUCCESS === this.Class.oModem.GetLastError()) {
    
      var that = this;
      var opt = {
        fnPtr: function(){
          return that.DoModemReboot();
        },
        timeout: 60,
        sleepTime: 300
      }
      var bRet = $ss.agentcore.utils.UpdateUI(opt);
    //}
    $("#reBootModemProgress").css("display", "none");
    if (!bRet) {
			//show the error
			$("#snp_st_reboot_err_status").show();
		}
		else {
			//show the success
			$("#snp_st_reboot_succ_status").show();
			this.Class.functionSucceeded = true;
		}	
		
		$(".snp_st_rebootmodem_okcancel").hide();
		$("#snp_st_rebootmodem_done").show();
 
  },
  
  /*
   * This function wil handle the case when the user clicks on the cancel button of modem reset
   */
  HandleCancelResetModemClick: function(params){
    this.RenderPage(this.Class.toLocation, "\\views\\home.html", null);
  },
	
	HandleRebootModemDoneClick: function(params){		
		if ( this.Class.functionSucceeded ) {
		  try {
			  this.CallSurveyPage("RestartModem", "0", "tool_use", "1", "RestartModem", "", "");
		  } 
		  catch (ex) {
		  }
		}
	
		this.RenderPage(this.Class.toLocation, "\\views\\home.html", null);
	},
  
  /*
   * This function will get the modem value
   */
  GetModemInstanceValue: function(valName, defValue){
    var modemValue = "";

    if (!this.Class.oModem) {
      this.Class.oModem = $ss.agentcore.smapi.methods.GetInstance();
      if ($ss.agentcore.constants.SMAPI_SUCCESS === this.Class.oModem.GetLastError()) {
        modemValue = this.Class.oModem.GetInstanceValue(valName, defValue);
      }
    }
    
    return modemValue;
  },
  
  /*
   * This function will check whether we have a RAS modem or not
   */
  IsRasModem: function(){
    var bRet = false;
    var modemClass = "";
    modemClass = this.GetModemInstanceValue($ss.agentcore.constants.SMAPI_XMLNODE_CLASS);
    
    bRet = (modemClass === "_ras");
    
    return bRet;
  },
  
  /*
   * This function will handle the case when the user clicks on modem restart
   */
  HandleRestartModemClick: function(params){
    this.RenderPage(this.Class.toLocation, "\\views\\rebootmodem.html", null);
  },
  
  /*
   * This function will check whether null modem is present
   */
  IsNullModem: function(){

    var sModemCode = $ss.agentcore.diagnostics.smartissue.ReadPersistInfo($ss.agentcore.constants.smartissue.MODEM_CLASS, $ss.agentcore.constants.smartissue.MODEM_CODE);
		if( "" === sModemCode){
			return false;
		}
    sModemCode = sModemCode.toLowerCase();
    if (sModemCode.substr(0, 10) == "null_modem") {
      return true;
    }
    else {
      return false;
    }
    
  },
  
  
  /*
   * This function will handle the case when the user clicks on DOne button of ipconfig screen
   */
  HandleIpconfigDoneClick: function(params){
		//call survey
		if ( this.Class.functionSucceeded ) {
			try {
				this.CallSurveyPage("ReleaseRenew", "0", "tool_use", "1", "ReleaseRenew", "", "");
			} 
			catch (ex) {
			}
		}
    this.RenderPage(this.Class.toLocation, "\\views\\home.html", null);
  },
  
  /*
   * This function will find out whether the user's system has DHCP or not
   */
  System_HasDHCP: function(oNics) {
    
    // if no adapter selected, skip this step
    if (oNics == null || oNics == "") {
      return false;
    }
    
    var bRet = false;
    for (var sAdapter in oNics) {
      if ($ss.agentcore.network.netcheck.IsDHCPEnabled(null, sAdapter)) {
        bRet = true;
        break;
      }
    }
    return bRet;
  },
  
  /*
  * This function will find out whether the adapter is connected to the network or not.
  */
  System_DHCPConnected: function(oNics) {

    // if no adapter selected, skip this step
    if (oNics == null || oNics == "") {
      return false;
    }
    var bRet = false;
    for (var sAdapter in oNics) {
      if ($ss.agentcore.network.netcheck.IsAdapterConnected(null, sAdapter)) {
        bRet = true;
        break;        
      }
    }
    return bRet;
  },  
  
  /*
   * This function will release renew the ipconfig
   */
  ReleaseAndRenewIpConfig: function(){
    var message = "";
		this.Class.functionSucceeded = false;
    try {
      //                                    oNet, filterVirtual, filterBridge, filterDisabled,  filterWireless
      var oNics = $ss.agentcore.network.netcheck.GetPresentAdapters(null, true, true, true, false);
      if (this.System_HasDHCP(oNics)) {
        if (this.System_DHCPConnected(oNics)) {             
        message = "RERE:ipconfig failure"
        if ($ss.agentcore.utils.system.ReleaseIpConfig()) {
          if ($ss.agentcore.utils.system.RenewIpConfig()) {
            message = "RERE:ipconfig success";
						this.Class.functionSucceeded = true;
          }
        }
      }
       else
       {
        message = "RERE:DhcpNotConnected";
       }
      }
      else {
        message = "RERE:NoDhcp";
      }
    } 
    catch (ex) {
      message = "ERROR"
    }
    
    return $ss.agentcore.utils.LocalXLate("snapin_systool", message);
  },
  
  /*
   * This function will handle the case when the user clicks on OK button on the confirmation
   * screen of ipconfig
   */
  HandleIpconfigOkClick: function(params){
    $("#ipRenewButtons").css("display", "none");
    $("#ipRenewProgress").css("display", "block");

    var that = this;
    var opt = {
      fnPtr: function(){
        return that.ReleaseAndRenewIpConfig();
      },
      timeout: 300,
      sleepTime: 300
    }
    var message = $ss.agentcore.utils.UpdateUI(opt);
    var data = {
      'message': message
    };
    $("#ipRenewProgress").css("display", "none");
    this.RenderPage(this.Class.toLocation, "\\views\\ipconfig_success.html", data);
  },
  
  /*
   * This function will handle the case when the user clicks on cancel button on the confirmation
   * screen of release renew ipconfig
   */
  HandleIpconfigCancelClick: function(params){
    this.RenderPage(this.Class.toLocation, "\\views\\home.html", null);
  },
  
  /*
   * This function will handle the case when user clicks on release renew Ipconfig
   */
  HandleIpConfigReleaseRenewClick: function(params){
    this.RenderPage(this.Class.toLocation, "\\views\\ipconfig.html", null);
  },
  
  /*
   * This function will handle the case when in the main ui the user clicks on the restart system
   */
  HandleSystemRestartClick: function(params){
 
    this.RenderPage(this.Class.toLocation, "\\views\\systemreboot.html", null);
  },
  
  /*
   * This function will handle the case when user click on the confirmation screen of system restart
   * Here it will handle ok click
   */
  HandleSystemRestartOKClick: function(params){
    //$ss.agentcore.dal.databag.SetValue("snp_systool_showsurvey", "1", true);
    $ss.agentcore.reporting.Reports.LogToolUsage("SystemRestart", "snapin_systool");
    $ss.agentcore.utils.ui.RestartWithSave("sa/systool", true);
    //set the resume mo
    //$ss.agentcore.utils.system.Reboot();
  },
  
  /*
   * This function will handle the case when user click on the confirmation screen of system restart
   * Here it will handle cancel click
   */
  HandleSystemRestartCancelClick: function(params){
    this.RenderPage(this.Class.toLocation, "\\views\\home.html", null);
  },
	
	/*
	 * This function will find out whether modem is present or not
	 */
	IsResetModemOptionAvialaible: function(){
		if (!this.IsNullModem()) {
      return true;
    }
		
		return false;
	},
  
  /*
   * Index main function will be called by the framework
   */
  index: function(params){ 
    try {
      if (!params) {
        return false;
      }
      
      //this.Class.displaySurvey = $ss.agentcore.dal.databag.GetValue("snp_systool_showsurvey");
      var page = "\\views\\home.html";
      this.Class.toLocation = params.toLocation;
          
		  //this.ModemTest();	
			
			this.Class.displayResetModem = this.IsResetModemOptionAvialaible();
      
      this.RenderPage(this.Class.toLocation, page, null);
    } 
    catch (ex) {
    }
  },
  
  /*
   * This function will render the html page
   * toLocation - Div is where the html needs to be loaded
   * page - html page
   * data - any additional data to be provided to this page
   */
  RenderPage: function(toLocation, page, data){
    try {
    
      data = data ||
      {}
      if ("\\views\\home.html" === page) {
        data.displayResetModem = this.Class.displayResetModem;
      }
      
      this.renderSnapinView("snapin_systool", toLocation, page, data);
      $ss.agentcore.utils.ui.FixPng();
    } 
    catch (ex) {
    }
  },
  
  /*
   * This function will create an object and call the survey page
   */
  CallSurveyPage: function(guidContent, version, verb, viewed, comment, data,result){
    var params = {};
    params.surveyData = {
      "guidContent": guidContent,
      "version": version,
      "verb": verb,
      "viewed": viewed,
      "comment": comment,
      "data": data,
      "result": result
    };
    try {
      this.Class.dispatch("snapin_survey", "index", params);
    } 
    catch (ex) {
    }
    params.surveyData = null;
    params = null;
  }
});

