"use strict";
if (typeof ($) !== "function") {// no jQuery!
  throw new Error("jquery.chat.plugin.custom: jQuery not found. Please ensure jQuery is referenced before the jquery.chat.plugin.custom.js file.");
}

if (!JSON) {// no JSON!
  throw new Error("jquery.chat.plugin.client.js: No JSON parser found. Please ensure json2.js is referenced before the jquery.chat.plugin.client.js file if you need to support clients without native JSON parsing support, e.g. IE<8.");
}
var chatInstanceHost = NavigationController.GetServerUrl();
var chatPluginInstance =null;
var isActiveOnChat = false;
var clickedEvent = "touchstart click";

var chatObj;
$.ajaxSetup({
  crossDomain : true
});
$.support.cors = true;

function formatTime(time_to_set) {
	  var time, hour, min, ap, sec;
	  function appendZero(val) { return (val > 10 || val == 10) ? val : "0" + val; }

	  ap = "AM";
	  hour = time_to_set.getHours();
	  min = appendZero(time_to_set.getMinutes());
	  sec = appendZero(time_to_set.getSeconds());
	  if (hour > 11) ap = "PM";
	  if (hour > 12) hour = hour - 12;
	  hour = appendZero(hour);
	  time = "[" + hour + ":" + min + ":" + sec + ap + "]";
	  return time;
}

function OnFocusHandler(ele) {
  if(ele.value === ele.defaultValue)
  ele.value="";
} 

function OnBlurHandler(ele) {
  if( jQuery.trim(ele.value) === "")
  ele.value=ele.defaultValue;
} 

var coreOpts = { urls: { hostURL: chatInstanceHost }, enableLogging: true },
preferences = { usesmileys: true, allowreconnect: true, showtranscript: false, usesurvey: false, usefeedback: true, autoEscalateIn: 30 , msgCount: 200, includeAudit:true},
  uiSettings = {
    startChatBtn: '#startChatBttn',
    messageContainer: "#msgDisplayNode",
    chatArea: "#msgDisplayNode",
    sendMessage: "#sendMsgBttn",
    newMessageArea: "#inputTextNode",
    closeChatBttn: "#closeChatBttn",
    templates: { newMessage1: $('#new-Message-template') },
    showEnterLeaveNotifcation: true,
    maxMessageLength: 3000
  },
customMessages = {
  accessDenied: 'You are not allowed to access this plugin',
  creatingticket: 'Creating ticket...',
  submittingChat: 'Submitting chat...',
  retrievingMessages: 'Retrieving your old messages..',
  retrievingMessagesFailed: 'Failed to retreive your old messages.',
  confirmMsgBeforeClosing: 'Do you really want to close the chat?',
  closingChat: 'Closing your session...',
  retrievingPreferences: 'Retrieving preferences...',
  reconnectNotSupported: 'Reconnect is not supported',
  invalidReconnectDetails: 'Details not available to reconnect',
  missingUserDetails: 'Invalid user name',
  notPartOfRoom: 'You are not part of the room',
  invalidRoom: 'Room seems to have been closed.',
  initializing: 'Initializing client...',
  reconnecting: 'Attempting to reconnect...',
  emergencyClose: 'Chat closed for urgent system maintenance',
  noContentsAvailable: 'No contents to display',
  viewTranscript: 'View Chat Transcript',
  downloadTranscript: 'Download Chat Transcript',
  submittingFeeback: 'Submitting your feedback...',
  feebackSubmitted: 'Thanks for submitting your feedback.',
  escalateToAnotherAnalyst: 'Analyst has left the room. Your problem is being escalated to another analyst',
  analystHasClosedChat: 'The analyst has left and your issue has been closed.',
  genericErrorMsg: 'An error occurred.'
};

//Customizable plugin code

  if(typeof $ui=='undefined') $ui = $.chat.ui;
  if(typeof $events=='undefined') $events= $.chat.events;

  var options = {};
  options = $.extend(true, {}, { core: coreOpts });
  options = $.extend(true, options, { preferences: preferences });
  options = $.extend(true, options, { ui: uiSettings });
  options = $.extend(true, options, { customMessages: customMessages });


  function _validateInput($inputElem, predicate) {
    if (!$inputElem || $inputElem.length == 0) return false;
    var val = jQuery.trim($inputElem);
    if (predicate) return predicate(val);
    return (val !== "");
  };

  function _isEmailAddress(string) {
    return /^%?\w+%?((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/.test(string);
  };
  function _checkLen(string, maxLen) {
    return (string && string.length <= maxLen)
  };

  function parseReturnData(data) {
    try { 
      if (typeof data === 'object') return data;
      return JSON.parse(data.replace(/^.*?\?*\(/, "").replace(/\)$/, "")); 
    } catch (e) { var t=e; }
    return {};
  };

  function autoScrollMessages($msgContainer) {
    var elements = $msgContainer.find('[id^=message-text]'),
       height = elements.outerHeight(),
       scrollAmt = elements.length * (height + 100);
    $msgContainer.animate({ scrollTop: scrollAmt }, 1);
  };

  function showswirl(title, duration) {
    $('#chat_container').find('.sprtBlockedUI').remove();
    $('#chat_container').append($("<div class='sprtBlockedUI' class='sprtBlockedUI'><\/div>"));
    var $notifElem = $('.sprtBlockedUI');
    $notifElem.append($("<img id='sprtswirling' class='sprtswirling'/>"));
    if (title) {
      $notifElem.append($("<div id='chatNotification' class='chatNotification'><p class='chatNotificationText'>" + title + "<\/p><\/div>"));
      //    var popMargTop = ($('#chatNotification').height() + 24) / 2, popMargLeft = ($('#chatNotification').width() + 24) / 2;
      //  $('#chatNotification').css({ 'margin-top': -popMargTop + ($('sprtswirlimg').css('margin-top')), 'margin-left': -$('#chatNotification').outerWidth() / 2 }).fadeIn(600);
    }
    var def = $notifElem.fadeIn('fast', function () { $(this).show(); });
    if (duration) def.delay(duration).fadeOut('slow', function () { $(this).remove(); });
  };

  function hideswirl() {
    $('#chat_container').find('.sprtBlockedUI').remove();
  };

  $('#msgDisplayNode').hide().empty();

  chatPluginInstance = $.chatPlugin($('.chat_plugin'), options);
  chatPluginInstance =  $.extend(chatPluginInstance, {
    showNotification: function (customMsg, duration) {
      showswirl(customMsg, duration);
    },
    hideNotification: hideswirl,
    showChatDiv: function () {
      this.hideProbDiv();
      if (this.$submit) this.$submit.attr('disabled', true);
      if (this.$close) this.$close.show()
      $(".collapsible").show();
      $("#chatDialogDiv,#sendBtnDiv ,#closeContainer, #msgDisplayNode").show();
      $("#surveyDiv, #ChatRichContentDisplay").hide();
      $("#sendMsgBttn, #inputTextNode").attr("disabled", false);
      // Now that the chat div is shown, display chat messages
      $(".hide_messages").remove();      
    },
    hideChatDiv:function(){
      $("#chatDialogDiv ,#sendBtnDiv ,#closeContainer ,#content_body, #ChatRichContentDisplay").hide();      
      $('#msgDisplayNode').empty();    
    },
    hideProbDiv:function(){
      $("#problemDialogDiv").hide();            
    },
    resetSurvey:function(){
      var $varSurvey = $("#surveyDiv");
      $varSurvey.hide();      
    },
    initializeChat: function(silent){
      var that= this;
      that.$container = $('.chat_plugin');
      if(silent){
        $.chat.ui.setup({});
        return;
      }
      this.setup();
      if(chatObj && typeof chatObj ==='object'){
          this.hideChatDiv();
          this.hideProbDiv();
          this.handleChatClosure(chatObj);
      }
      chatObj = null;       
      var core = that.core;
      if(this.canReconnect()){
        core.reconnect();
      }
    },
    showProbDiv: function (reload) {
      this.hideChatDiv();
      this.resetSurvey();
      reload = reload ||  false;
      if (this.$submit) this.$submit.removeAttr('disabled');
      if (this.$close) this.$close.hide()
      $("#problemDialogDiv").show();
      if(reload){
        // Reloading the text boxes with appr texts
        $('#probTextNode').val(_utils.LocalXLate("snapin_chat", "snp_chat_prob_des_txt"));
        $('#emailTxt').val(_utils.LocalXLate("snapin_chat", "snp_chat_email_add_txt"));
        $('#fnameTxt').val(_utils.LocalXLate("snapin_chat", "snp_chat_name_txt"));
        $('#inputTextNode').empty();
      }
    },
    reportError: function (errMsg, duration) {
      this.hideNotification();
      var $eDlg = $("#errorDisplayDiv");
      $eDlg.html(errMsg);
      var def = $eDlg.fadeIn('slow', function () { $(this).show(); });
      if (duration) def.delay(duration).fadeOut('slow', function () { $(this).hide(); $(this).text(''); });
    },
    showTranscriptLink: function (url) {
      if (!url) return;
      if ($.ss.utility.isMobile()) url += "&mode=download";
      
      var $transcriptDiv = $("#transcriptDiv"),
      vtranscript = (!$.ss.utility.isMobile()
                ? this.getCustomMessage('viewTranscript', 'View Chat Transcript')
                : this.getCustomMessage('downloadTranscript', 'Download Chat Transcript')),
      link = '<a id="chatTranscriptLink" name="chatTranscriptLink" href="{0}" target="_blank">{1}</a>'.format(url, vtranscript);
      $transcriptDiv.empty();
      $transcriptDiv.html(link).show();
      /*$transcriptDiv.on('click',function(){
        var html='<div id="myModal" class="modal fade" role="dialog"><div class="modal-dialog"><div class="modal-content">';
      html+='<div class="modal-body">';
      html+='<iframe src="' + url + '"></iframe>';
      html+='</div></div></div>';
      
      $('#transcriptDiv').append('<iframe src="' + url + '"></iframe>');
        window.showModalDialog(url);
      });*/
    },
    showSurvey: function (url) {
      if (!url) return;
      var $survey = $("#surveyDiv");
      $survey.empty();
      $survey.html('<iframe frameborder=no width="100%" height="100%" id="customSurvey" name="customSurvey" src="' + url + '" target="_blank"><\/iframe>').show();
    },
    showFeedback: function (ticket) {
      var self = this,
       objIssueResolved = $("input:radio[name=issueresolved]:checked"),
       $feedbackSection = $('#surveyDiv'),
       feedbackOpt = { room: ticket.room, issueresolved: objIssueResolved.val() },
       ratingPluginDefined = typeof $.fn.starRater !== 'undefined',
       $ratingsSection = $('#ratingsDiv');

      if ($feedbackSection.length == 0) return;
      if (!ratingPluginDefined) $ratingsSection.hide()
      else $ratingsSection.starRater();

      $feedbackSection.find("input:radio[name=issueresolved]").on(clickedEvent, function () {
        feedbackOpt.issueresolved = $(this).val();
      });
      $("#submitSurveyBttn").on(clickedEvent, function () {
        self.showNotification(self.getCustomMessage('submittingFeeback', 'Submitting feedback...'));
        if (ratingPluginDefined && $ratingsSection.length > 0) {
          feedbackOpt.rating = $ratingsSection.starRater('ratings');
        }
        self.core.submitFeedback(feedbackOpt)
              .done(function () {
                var msg = self.getCustomMessage('feebackSubmitted', '');
                //NB: uncomment the following line if you want to show notification about successfull submission of the rating.
                //if (msg) alert(msg);
              })
            .always(function () {
              self.hideNotification();
              // TODO : ideally transcript div should be handled while hiding/showing prev/next divs
              $("#transcriptDiv").empty();
              //setting default value of ratings radio after reading..
              $(objIssueResolved[0]).prop("checked",true);  // setting the default value to "yes"
              //$ratingsSection.starRater('ratings')
              self.showProbDiv(true);
            });
      });
      $feedbackSection.show();
    }
  });


  $(chatPluginInstance).bindFor('accessdenied', function (msg) {
    msg = this.getCustomMessage('accessDenied', msg);
    this.reportError(msg);
    this.showProbDiv();
  });
  
  $.extend(chatPluginInstance, {
    setup: function () {
      var self = this;
      $.chat.ui.setup(self.ui);
      this.hideNotification();
      this.$submit = self.$container.find(self.ui.startChatBtn);
      this.$submit
      .off()// unbinding any previous subscription
      .on(clickedEvent, function (e) {
        if (typeof self.submitChat === 'function') self.submitChat();
      });
      this.$close = self.$container.find(self.ui.closeChatBttn);
      this.$close
      .off() // unbinding any previous subscription
      .on(clickedEvent, function (e) {
        if (!confirm(self.getCustomMessage('confirmMsgBeforeClosing', _utils.LocalXLate("snapin_chat", "confirmMsgBeforeClosing")))) return false;
        e.preventDefault();
        self.core.closeChat();
      });
    },
    validateUserInfo: function (requestObj) {
      var errors = [];
      // Keeping the limit to 200 as sprt_nexus_xref table has the problem len for nvarchar 200
      if (!(_validateInput(requestObj.problem, function (input) { return _checkLen(input, 200); }))) errors.push(_utils.LocalXLate("snapin_chat", "validProblem")); 
      if (!(_validateInput(requestObj.emailAddress, _isEmailAddress))) errors.push(_utils.LocalXLate("snapin_chat", "validEmail"));
      if (!(_validateInput(requestObj.username, function (input) { return _checkLen(input, 40); }))) errors.push(_utils.LocalXLate("snapin_chat", "validName"));
      return errors;
    },
    getCustomMessage: function(key, defaultVal){
      var msg =  _utils.LocalXLate("snapin_chat", key);
      return (msg===key? defaultVal: msg);      
    },
    /*sample code moqing a ticketting call
    createTicket: function(ticketData){
      //return $.Deferred().resolve(ticketData).promise(); //return data as-is	  
	  //invoke call to ticketting system here and return a promise with the ticketting data
    },
	*/
    submitChat: function () {
      var self = this;
      var requestObj = {
          problem: jQuery.trim(($("#probTextNode").val() == $("#probTextNode").attr("placeholder")) ? "" : $("#probTextNode").val()),
          emailAddress: jQuery.trim(($("#emailTxt").val() == $("#emailTxt").attr("placeholder")) ? "" : $("#emailTxt").val()),
          username: jQuery.trim(($("#fnameTxt").val() == $("#fnameTxt").attr("placeholder")) ? "" : $("#fnameTxt").val()),
        includeSI: true
      }
      var errors = self.validateUserInfo(requestObj);
      if (errors.length > 0) {
        self.showProbDiv();
        self.reportError(errors.join('<br/>'), 2000);
        return;
      }
      if (self.$submit) self.$submit.attr('disabled', true);
      $.ss.utility.warn('TODO: move somewhere');
      $.chat.client.create(requestObj.username); //TODO: move somewhere
      self.createTicket(requestObj)
      .then(function (ticketInfo) {
		return self.core.startChat(ticketInfo);
	  })
	  .fail(function (xhr, reason, exp) {
		self.showProbDiv();
        var errorMsg = exp || reason;
        if(/(error)/i.test(reason)) {
          errorMsg = self.getCustomMessage('genericErrorMsg',"An error occurred.");
        }
        self.reportError(errorMsg, 5000);	  
	  });
    },
    handleChatClosure: function(ticket){
      if(!ticket) return;
      if (this.preferences.showtranscript && ticket.transcript) this.showTranscriptLink(ticket.transcript);
      //if survey has been configured for a queue then use only the survey & bypass the feedback
      if (this.preferences.usesurvey && ticket.survey) { this.showSurvey(ticket.survey);}
      else if (this.preferences.usefeedback) this.showFeedback(ticket);      
    }
  });

  $(chatPluginInstance).bindFor('notification', function (msgtype, msg) {
    //TODO: review what we want to notify
    var self = this;
    msg = this.getCustomMessage(msgtype, msg);
    if (/^escalateToAnotherAnalyst/i.test(msgtype)) {
      $ui.addMessageToUI(msg, 'system');
      return;
    }
    self.showNotification(msg, /^system/i.test(msgtype) ? 5000 : null);
  });

  $(chatPluginInstance).bindFor('reconnected', function (room) {
    isActiveOnChat = true;
    this.hideNotification();
    autoScrollMessages($('#msgDisplayNode'));
    this.showChatDiv();
  });

  $(chatPluginInstance).bindFor('reconnectfailed', function (reason, code) {
    //alert("reconnect failed");
    reason = this.getCustomMessage(code, reason);
    this.reportError(reason, 5000);
    this.showProbDiv();
  });

  $(chatPluginInstance).bindFor('ticketcreated', function () {
    isActiveOnChat = true;
    this.hideNotification();
    this.showChatDiv();
  });

  $(chatPluginInstance).bindFor('ticketcreationfailed', function (reason, code) {
    isActiveOnChat = false;
    reason = this.getCustomMessage(code, reason);
    this.reportError(reason, 5000);
    this.showProbDiv();
  });

  $(chatPluginInstance).bindFor('ticketclosed', function (ticket, reason, code) {
    isActiveOnChat = false;
    this.hideNotification();
    if (reason || code) {
      reason = this.getCustomMessage(code, reason);
      alert(reason);
    }
    var $elemWidget = $(".chatWidget");
    if (!(/true/i.test($elemWidget.attr('activeOnChat')))) {
      chatObj = ticket;
      $elemWidget.trigger('bringToFront');
      return;
    }
    this.hideProbDiv();
    this.hideChatDiv();
    this.handleChatClosure(ticket);
  });

//Content Handling

  if(typeof $plugin == 'undefined') $plugin = $.chatPlugin;
  if(typeof $ui == 'undefined') $ui = $.chat.ui;
  if(typeof $events == 'undefined') $events = $.chat.events;

  window.viewContent = function (contentIndex) {
    $.chat.ui.contents.goTo(contentIndex);
    if ($(".collapsible").collapsible('collapsed')) {
        $(".collapsible").collapsible("open");
      }
  }

  $.fn.slideFadeToggle = function (speed, easing, callback) {
    return this.animate({ opacity: 'toggle', height: 'toggle' }, speed, easing, callback);
  };

  $('.collapsible').collapsible({
    cookieName: '',
    speed: 'slow',
    animateOpen: function (elem, opts) { //replace the standard slideUp with custom function
      elem.next().slideFadeToggle(opts.speed);
    },
    animateClose: function (elem, opts) { //replace the standard slideDown with custom function
      elem.next().slideFadeToggle(opts.speed);
    },
    loadOpen: function (elem) { //replace the standard open state with custom function
      elem.next().show();
    },
    loadClose: function (elem, opts) { //replace the close state with custom function
      elem.next().hide();
    }
  }); 
  $('#content_body').on(clickedEvent, function (e) {
    var $richContentArea = $('#ChatRichContentDisplay'),
     $noContentsAvailable = $('#noContentsAvailable');

    if ($richContentArea.find('[id^=content-id]').length > 0) $noContentsAvailable.remove();
    if ($noContentsAvailable.length == 0) {
      var msg = customMessages.noContentsAvailable;
      if (!msg) return;
      $noContentsAvailable = $("<p id='noContentsAvailable' class='noContentsAvailable'><\/p>");
      $noContentsAvailable.html(msg);
      $richContentArea.append($noContentsAvailable);
    }
  })

  $events.on($events.ui.newContent, function (newcontents) {
    $('#noContentsAvailable').remove();
    var contents = JSON.parse(newcontents);
    if (!$.isArray(contents)) contents = $.makeArray(contents);
    var contentType = "", title = "", id, byWhom, roomId, time;
    $.each(contents, function (k, v) { id = v.Id; contentType = v.Type; title = v.Title; byWhom = v.From; roomId = v.RoomId; time = v.CreatedDate.toUTCDateTime(); return false; });
    if (contentType == "") return;
  
    var showIndex = Math.max($ui.contents.count(), contents.length) - contents.length, advTxt;
    advTxt = '{0}' + " "+ _utils.LocalXLate("snapin_chat", "snp_chat_content_pushed")
    if ((/(image|video|audio)/i.test(contentType))) {
      advTxt += $ui.contents.$container && $ui.contents.$container.length >0 
        ? '<a href="javascript:void(0)" onclick="viewContent({3}); return false; ">View</a>'
        : '{1} by {2}';
      advTxt = advTxt.format(contentType, title, byWhom, showIndex);      
    } else {
      advTxt = advTxt.format(contentType, title, byWhom);
     }        
    var newMsg = { Id: $.ss.utility.newGuid(), From: "advanced_tool_log", MessageType: "msg", Content: advTxt, Created: time.toLocalDateTime() };
    $ui.processMessage(newMsg, byWhom, roomId);
  });




