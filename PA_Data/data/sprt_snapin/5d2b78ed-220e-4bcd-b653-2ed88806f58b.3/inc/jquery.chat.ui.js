﻿(function ($, window, document, chat) {
  "use strict";

  if (typeof ($) !== "function") {// no jQuery!
    throw new Error("jquery.chat.ui: jQuery not found. Please ensure jQuery is referenced before the jquery.chat.ui.js file.");
  }

  if (!JSON) {// no JSON!
    throw new Error("jquery.chat.ui: No JSON parser found. Please ensure json2.js is referenced before the jquery.chat.ui.js file if you need to support clients without native JSON parsing support, e.g. IE<8.");
  }
  var isTouchDevice = (('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch),
		clickedEvent = "touchstart click dblclick"


  var $ui = null,
    $events = $.chat.events,
    api = $.chat.api,
    templateUrl = null,
    Keys = { Up: 38, Down: 40, Esc: 27, Enter: 13, Slash: 47, Space: 32, Tab: 9, Question: 191 },
    $window = $(window),
    $document = $(document),
    $sendMessage = null,
    $closeRoom = null,
    $newMessageArea = null,
    $chatArea = null,
    $messageContainer = null,
    $roomUsersContainer = null,
	  $presenceNotificationContainer = null,
    focus = true,
    msgTimeDisplayMode = null,
    networkStatus = null,
    isActive = false,
    isTyping = false,
    typingTimeout = 3000,
	typingDisplayTimeout = 3000,
    maxMsgLength = 3000,
    performUrlLinkify = false,
    notifyEnterLeaveEvent = false;

  var preferences = {}; //create a dummy one


  function isValidSelector(selectorObj) {
    return (selectorObj && selectorObj.length > 0);
  };


  function setRoomPreference(roomName, preferenceName, value) {
    //check if we already have any.if not  create it.
    var roomPreferences = preferences[roomName];
    if (!roomPreferences) {
      roomPreferences = {};
      preferences = roomPreferences;
    }
    //set the value
    preferences[preferenceName] = value;
  };

  function autoScrollMessages($msgContainer) {
    //The code needs to be fixed    
    if (!isValidSelector($msgContainer)) return;
		var element = $msgContainer.find('[id^=message-text]');
		var height = 0;
		$.each(element,function(k,v){
		    height = height + v.offsetHeight;
		    height = height + 100;
		});
		if(height == 0)
		    height = 100000;
		height = height + 100;
		//height = element.outerHeight(),
		//scrollAmt = element.length * (height + 100);
		//scrollAmt = scrollAmt + 2000;
		$msgContainer.animate({ scrollTop: height }, 10);
		//$($msgContainer.find(element[element.length - 1].id)).focus();
  };

  function setFocus() {
    if (!$.ss.utility.isMobile && amIConnected() && isValidSelector($newMessageArea)) {
      $newMessageArea.focus();
    }
  };

  function amIConnected() {
    return isActive;
  };

  function initiateSend() {
    if (!amIConnected() || !isValidSelector($newMessageArea)) {
      alert("You are not connected to network");
      return;
    }
    var msg = $.trim($newMessageArea.val());
    if (msg) {
      $(ui).triggerHandler($events.ui.sendMessage, [msg, null]);	//TODO: pass last param for private msg
    }
    focus = true;
    setFocus();
  };

  function handleAppError(error) {
    if ($chatArea) {
      var data = $chatArea.val();
      data += error + "\n";
      $chatArea.val(data);
    }
  };

  function _addUserToRoom(user, room, $roomUsers, $userListTemplate) {
    if (!isValidSelector($roomUsers) || !isValidSelector($userListTemplate)) return;
    if ($roomUsers.find('[id^=user-' + user.Id + '-room-' + room.Id + ']').length == 0) {
      $userListTemplate.tmpl(user)
					.appendTo($roomUsers);
    }
  };

  function _removeUserFromRoom(user, room, $roomUsers) {
    if (!isValidSelector($roomUsers)) return;
    $roomUsers.find('[id^=user-' + user.Id + '-room-' + room.Id + ']').remove();
  };

  var msgPreprocessors = [function (message) {
    message.Content = message.Content.replace(/(?:\r\n|\r|\n|&#10;|&#13;)/gm, '<br>');
    message.Content = message.Content.replace(/\\/gm, '\\\\');
    message.Created = message.Created.toUTCDateTime();
    message.isSystem = $.isSystem(message.From);
    message.isMe = $.isMe(message.From);
    if (performUrlLinkify) message.Content = $.ss.utility.linkifyUrls(message.Content)
  }];

  function _preProcessMessage(message) {
    if (typeof message !== 'object' || message['Content'] === 'undefined') return message;
    $.each(msgPreprocessors, function (i, processor) {
      try {
        if (typeof processor === 'function') processor(message);
      } catch (e) {
        $.ss.utility.log('Message pre-processor \'' + $.getFnName(processor) + '\' failed.');
      }
    });
  };

  function _messageExists($msgContainer, message, room) {
    if (!isValidSelector($msgContainer)) return false;
    return ($msgContainer.find('[id^=message-text-' + message.Id + ']').length > 0);
  };

  function _contentExists(contentObj, $containter) {
    var cid = contentObj.Id || null;
    if (cid) return ($containter.find('[id^=content-id-' + cid + ']').length > 0);
    return false;
  };

  function _isContentMessage(message) {
    if (typeof (message) === 'undefined') return false;
    return (/(video|image|url|iframe|package|surveywebpage|surveyinline)/i.test(message.MessageType));
  };

  function _appendMessage(message, $msgContainer, $msgTemplate) {
    if (!isValidSelector($msgContainer) || !isValidSelector($msgTemplate)) return;
    var $elem = $msgTemplate.tmpl(message);
    $elem.appendTo($msgContainer);

    if (msgTimeDisplayMode && typeof msgTimeDisplayMode !== 'undefined') {
      $elem.find('.msgtime').css({ display: msgTimeDisplayMode });
    }
    autoScrollMessages($msgContainer);
  };

  //NB: this function is meant only for SupportSoft rich contents
  function _getContentUrlFromId(cid) {
	//TODO: this is a temporary fix - chat engine server needn't be same as content server/webserver
    return (api.getHost() + '/sdccommon/lachat/asp/viewattachment.asp?redirect=true&instanceguid=' + cid);
  };

  //NB: this function is meant only for SupportSoft surveys
  function _getSurveyUrlFromId(sid, type, roomId) {
	//TODO: this is a temporary fix - chat engine server needn't be same as content server/webserver
    var sPrefLangCode = $ss.agentcore.utils.GetLanguage();
    var sLangCode = $ss.agentcore.dal.config.GetConfigValue("languagemapping", sPrefLangCode, "en-US");
    var sRequestParam = $ss.agentcore.dal.config.GetConfigValue("UserPrefLangRequestParam", "USER_PREF_LANG_REQUEST_PARAM", "UserPrefLangID");
    return (api.getHost() + '/sdccommon/asp/view_survey.asp?' + sid + '&format=JSON&dataType=' + type + '&source=chat&identifier=' + roomId + '&'+sRequestParam + '=' + sLangCode + '&isMobileChat=' + $.ss.utility.isMobile());
  };

  function _processContent($ui, contentModel, byUser, roomModel) {
    var content = $.extend({}, contentModel, {
      Id: contentModel.ContentID || contentModel.Id,
      RoomId: roomModel.Id,
      CreatedDate: contentModel.CreatedDate.toUTCDateTime(),
      From: contentModel.From || byUser.UserName
    }),
      $contentTemplate = $ui.templates.newContent,
      $msgTemplate = $ui.templates.newMessage,
      $contentNavigator = $ui.contents,
      $msgContainer = $messageContainer,
      contentType = contentModel.Type || null;

    if (/(image|video|audio)/i.test(contentType)) {

      if (/(video|audio)/i.test(contentType)
        && !content.MimeType) {
        if (typeof _getContentInfofromUrl === 'function') {
          var contentInfo = _getContentInfofromUrl(content.Source) || { MimeType: '', FileExtn: '' };
        content.MimeType = contentInfo.MimeType;
      }
      }
      var contents = []
      params = (content.Source).split("^^"),
			title = params.length >= 2 ? params[0] : '';

      if (params.length >= 2) {
        var cArray = params[1].split("||"),
        cLem = cArray.length;
        $.each(cArray, function (index, cid) {
          var ct = $.extend({}, content, { Id: cid + '-' + content.Id, Source: (_getContentUrlFromId(cid)), Title: title });
          contents.push(ct);
        });
      } else {
        contents.push(content);
      }
      if (contents.length > 0) {
        if (isValidSelector($contentTemplate)) {
          contents = $.grep(contents, function (c, index) {
            return !(_contentExists(c, $contentNavigator ? $contentNavigator.$container : $messageContainer));
          });
        $.each(contents, function (index, c) {       
          var $contentElem = $contentTemplate.tmpl(c);
          if (isValidSelector($contentElem)) {
              if (isValidSelector($contentNavigator.$container)) $contentNavigator.add($contentElem);
              else $contentElem.appendTo($messageContainer);
          }
        });
      }
        if (contents.length > 0) $.chat.events.fire($.chat.events.ui.newContent, JSON.stringify(contents));
      }
    } else if (/package/i.test(contentType)) {
      if (!isValidSelector($contentTemplate)) return;
      var params = (content.Source || content.Content || '').split("^^");
      if (params.length < 2) return;
      var title = params[0],
      pid = params[1],
			packageContent = $.extend({}, content, { Id: pid + '-' + content.Id, Source: (_getContentUrlFromId(pid)), Title: title });
      if (_contentExists(packageContent, $messageContainer)) return;
      $.chat.events.fire($.chat.events.ui.newContent, JSON.stringify([packageContent]));
      var $contentElem = $contentTemplate.tmpl(packageContent);
      if (isValidSelector($contentElem)) {
        $contentElem.appendTo($messageContainer);
        autoScrollMessages($msgContainer);
      }
    } else if (/(surveyinline|surveywebpage)/i.test(contentType)) {
      if (!isValidSelector($contentTemplate)) return;
      if (_contentExists(content, $messageContainer)) return;
      content.Source = _getSurveyUrlFromId(content.Source, content.Type, content.RoomId);
      $.chat.events.fire($.chat.events.ui.newContent, JSON.stringify([content]));
      var $contentElem = $contentTemplate.tmpl(content);
      if (isValidSelector($contentElem) && isValidSelector($messageContainer)) {
        $contentElem.appendTo($messageContainer);
        autoScrollMessages($msgContainer);
      }
    } else if (/(url|iframe)/i.test(contentType)) {
      if (_contentExists(content, $messageContainer)) return;
      $.chat.events.fire($.chat.events.ui.newContent, JSON.stringify([content]));
      if (!isValidSelector($contentTemplate)) return;
      var $contentElem = $contentTemplate.tmpl(content);
      if (isValidSelector($contentElem)) {
        $contentElem.appendTo($messageContainer);
        autoScrollMessages($msgContainer);
      }
    } else {
      $.ss.utility.log("Unsupported content Type. Data:[" + JSON.stringify(content) + "]");
    }
  };

  function loadTemplatesFromUrl(url) {
    if (!url) return;
    $.get(url)
        .success(function (data) {
          $(document.body).append(data);
          $(document.body).find("script[type='text/x-jquery-tmpl']").each(function (index) {
            var id = this.id,
              templateName = $(this).attr("name");
            //templates[templateName] = $('#' + id);
          });
          $.ss.utility.log("Successfully loaded template:" + url);
        })
        .fail(function (e) {
          $.ss.utility.log("Failed to load template:" + url);
        });

  };

  var ui = {
    contents: null,
    templates: {},
    setup: function (options) {
      $ui = $(this);
      networkStatus = null;
      var defaults = {
        maxMessageLength: maxMsgLength,
        chatArea: '#msgs',
        messageContainer: '#messages-container',
        newMessageArea: '#message',
        userListContainer: '#userList',
        sendMessage: '#SendMessage',
        close: '#close',
        presenceNotificationContainer: '#useractivity',
        templates: {
          newMessage: $('#new-Message-template'),
          newPrivateMessage: $('#new-pvt-Message-template'),
          newSystemMessage: $('#new-System-Message-template'),
          newContent: $('#new-Content-template'),
          userList: $('#room-users-List-template'),
          userPresence: $('#user-presence-template')
        },
        contents: {
          main: {
            container: '.content-container',
            nextBtn: '.traverse-right',
            prevBtn: '.traverse-left'
          },
          gallery: {
            container: '.gallery-view-container',
            showGalleryBtn: '.show-gallery',
            hideGalleryBtn: '.close-gallery',
            galleryPreview: '.gallery-view',
            galleryMini: '.gallery-view-mini'
          }
        },
        autoLinkifyUrls: performUrlLinkify,
        showEnterLeaveNotifcation: true
      };

      var settings = $.extend({}, defaults, options),
		  contentNavSettings = $.extend({}, defaults.contents, settings.contents);

      maxMsgLength = settings.maxMessageLength;
      performUrlLinkify = settings.autoLinkifyUrls;
      notifyEnterLeaveEvent = settings.showEnterLeaveNotifcation;
      $chatArea = $(settings.chatArea);
      $messageContainer = $(settings.messageContainer);
      $newMessageArea = $(settings.newMessageArea);
      $roomUsersContainer = $(settings.userListContainer);
      $sendMessage = $(settings.sendMessage);
      $closeRoom = $(settings.close);
      $presenceNotificationContainer = $(settings.presenceNotificationContainer);

      this.templates = $.extend({}, defaults.templates, settings.templates);

      if ($.isFunction($.contentNav)) {
        this.contents = $.contentNav(contentNavSettings.main.container, contentNavSettings);
      }

      $closeRoom.on(clickedEvent, function (ev) {
        var $client = $.chat.client;
        if (!$client.CurrentRoom) {
          $events.fire($events.app.error, "Error in closing room - Invalid Room");
          return;
        }
        api.closeRoom($client.CurrentRoom.Id, $client.UserName);
      });

      $sendMessage.on(clickedEvent, function (ev) {
        initiateSend();
      });

      $newMessageArea.keypress(function (ev) {
        var key = ev.keyCode || ev.which;
        switch (key) {
          case Keys.Up:
          case Keys.Down:
          case Keys.Esc:
          case Keys.Enter:
            break;
          default:
            var $client = $.chat.client;
            if (amIConnected() === true && isTyping === false) {
              isTyping = true;
              ui.markUserPresence({
                from: $client.UserName,
                event: "2",
                description: "typing.."
              });
              window.setTimeout(function () {
                isTyping = false;
              }, typingTimeout);
            }
            break;
        }
      });

      $newMessageArea.keydown(function (ev) {
        var key = ev.keyCode || ev.which;
        switch (key) {
          case Keys.Up:
          case Keys.Down:
            //do nothing
            ev.preventDefault();
            break;
          case Keys.Esc:
            $(this).val('');
            break;
          case Keys.Enter:
            initiateSend();
            ev.preventDefault();
            return false;
          case Keys.Space:
            break;
        }
      });

      $(document).bind('keydown', function (ev) {
        // ctrl + T event is sent to the page in Chrome when the user probably means to change browser tabs
        var keyCode = ev.keyCode || ev.which;
        if (ev.ctrlKey && keyCode == 84) {
          if ($messageContainer) {
            ev.preventDefault();
            $messageContainer.find('.msgtime').toggle();
            msgTimeDisplayMode = $messageContainer.find('.msgtime').first().css('display');
            autoScrollMessages($messageContainer);
            return false;
          }
        } else if (!ev.ctrlKey && $newMessageArea.val() === "") {
          if (amIConnected()) {
            $newMessageArea.focus();
          }
        }
      });

      if (templateUrl !== null) {
        loadTemplatesFromUrl(templateUrl);
      }
    },

    addRoom: function (room) { //adds a room to UI

    },

    removeRoom: function (room) { //removes a room from UI		

    },

    addUser: function (user, room) {  //adds user to a room
      //add the user to the room wiithin the UI		
      //+	  
      _addUserToRoom(user, room, $roomUsersContainer, this.templates.userList);
    },

    removeUser: function (user, room) { //remove the user from UI
      //remove the user from the room wiithin the UI
      //+
      _removeUserFromRoom(user, room, $roomUsersContainer);
    },

    processMessage: function (message, user, room) {
      if (typeof (user) !== 'object') user = new User(user || message.From);					//TODO: fix this, (User.Role)
			
      if (_isContentMessage(message)) {
        var contentModel = $.extend({}, new Content(message.Id, message.MessageType, message.Content),
                                          { RoomId: room, CreatedDate: message.Created, From: message.From });
        ui.addContent(contentModel, user, room);
        return;
      }
      _preProcessMessage(message);
      //if (_messageExists($messageContainer, message, room)) return;
      message.isMe = $.isMe(message.From || user);
      if ($.isSystem(message.From)) { ui.addSystemMessage(message); }
      else if (!room) { ui.addPrivateMessage(message, user); }
      else { ui.addNewMessage(message, user, room); }
    },

    //User Related
    addNewMessage: function (message, user, room) {
      _appendMessage(message, $messageContainer, this.templates.newMessage);
    },

    addSystemMessage: function (message) {
      var $msgTemplate = isValidSelector(this.templates.newSystemMessage) ? this.templates.newSystemMessage : this.templates.newMessage;
      _appendMessage(message, $messageContainer, $msgTemplate);
    },

    addPrivateMessage: function (message, from) {
      message.isMe = $.isMe(message.From || from);
      var $msgTemplate = isValidSelector(this.templates.newPrivateMessage) ? this.templates.newPrivateMessage : this.templates.newMessage;
      _appendMessage(message, $messageContainer, $msgTemplate);
    },

    replaceMessage: function (message, room) {
      //replace only the content
      $messageContainer.find('[id^=message-text-' + message.Id + ']').html(message.content);
    },

    addContent: function (contentObj, byUser, room) {
      _processContent(this, contentObj, byUser, room);
    },
    markUserPresence: function (presenceInfo) {	//make a give user with its state (activity related)
      var $client = $.chat.client;
      if (amIConnected() !== true ||
          !$client.CurrentRoom || typeof (api.sendUserPresence) !== 'function') {
        return;
      }
      var presence = $.extend({}, {
        //state: "0", //Available
        event: "5",	//Others
        from: $client.UserName
      }, presenceInfo);
      api.sendUserPresence($client.CurrentRoom.Id, presence);
    },
    registerMsgPreProcessor: function (msgPreProcessor) {
      if (typeof msgPreProcessor !== 'function') return;
      msgPreprocessors.push(msgPreProcessor);
    },
    //General UI
    showNetworkStatus: function (status) {
    }
  };

  //manage event subscriptions
  $events.on($events.app.connectionStateChange, function (status) {
    if (networkStatus === status) return;
    networkStatus = status;
    switch (status) {
      case "connected":
        isActive = true;
        break;
      case "disconnected":
        isActive = false;
        break;
    }
    ui.showNetworkStatus(status);
  });

  $events.on($events.app.error, handleAppError);
  $events.on($events.app.joinedRoom, ui);
  $events.on($events.app.leftRoom, ui);

  $events.on($events.app.newMessage, function (message, from, room) {
    ui.processMessage(message, from, room);
  });

  $events.on($events.app.newContent, function (contentObj, byUser, room) {
    ui.addContent(contentObj, byUser, room);
  });

  $events.on($events.ui.activity, ui);
  $events.on($events.ui.error, ui);

  $events.on($events.app.closedRoom, function (roomModel, userModel) {
    $(ui).triggerHandler($events.ui.closeRoom, [roomModel, userModel]);
  });

  $events.on($events.app.activity, function (userModel, activity) {
    $events.fire($events.ui.activity, [activity, userModel, $.chat.client.CurrentRoom]);
  });

  $(ui).on($events.app.joinedRoom, function (evt, userModel, roomModel) {

    if ($.isMe(userModel)) {
      ui.addRoom(roomModel);
    }
    ui.addUser(userModel, roomModel);

    if (!notifyEnterLeaveEvent) return;
    $(ui).triggerHandler($events.ui.activity, [{
      From: userModel.UserName,
      State: 0,	//Available
      Event: 1, //active
      Description: ""
    }, userModel, roomModel]);

  });

  $(ui).on($events.app.leftRoom, function (ev, userModel, roomModel) {
    if ($.isMe(userModel)) {
      ui.removeRoom(roomModel);
    }
    ui.removeUser(userModel, roomModel);

    if (!notifyEnterLeaveEvent) return;
    $(ui).triggerHandler($events.ui.activity, [{
      From: userModel.UserName,
      State: 1,	//Un-available
      Event: 3, //left
      Description: ""
    }, userModel, roomModel]);
  });

  $(ui).on($events.ui.closeRoom, function (ev, roomModel, userModel) {
    ui.removeRoom(roomModel);
  });

  $(ui).on($events.ui.sendMessage, function (ev, msgText, to) {
    if (typeof (msgText) !== 'string') {
      $events.fire($events.ui.error, _utils.LocalXLate("snapin_chat", "snp_chat_err_invalid_msg"));
      return;
    }
    if (msgText.length > maxMsgLength) {
      var msgLenExceededErr = _utils.LocalXLate("snapin_chat", "snp_chat_err_msg_len_exceed", [maxMsgLength]);
      msgLenExceededErr = msgLenExceededErr.format(maxMsgLength);
      $events.fire($events.ui.error, msgLenExceededErr);
      return;
    }
    if (typeof (api.sendMessage) !== 'function') return;
    var $client = $.chat.client;
    if (!$client.CurrentRoom) {	//if he is not part of room dont allow..
      $events.fire($events.ui.error, _utils.LocalXLate("snapin_chat", "snp_chat_err_invalid_room"));
      return;
    }
    api.sendMessage($client.UserName, $client.CurrentRoom.Id, msgText, "msg", to)
         .fail(function () { $events.fire($events.ui.error, _utils.LocalXLate("snapin_chat", "snp_chat_err_prev_msg")); });
    $newMessageArea.val('');//do we want to clear..even before we successfully sent the message?
  });


  $(ui).on($events.ui.activity, function (ev, activity, userModel, roomModel) {
    activity.Event = (activity.Event || activity.event || "").toString();
    var event = activity.Event
    switch (event) {
      case "2": //typing        
        if ($.isMe(userModel)) return;        //if user is not the current user dont do anything
        if (!isValidSelector($presenceNotificationContainer) || ! isValidSelector(this.templates.userPresence)) return;
        $presenceNotificationContainer.empty();
        this.templates.userPresence.tmpl($.extend({}, activity, { User: userModel, Room: roomModel })).appendTo($presenceNotificationContainer);        
        window.setTimeout(function () {
          $presenceNotificationContainer.empty();          
        }, typingDisplayTimeout);
        break;
      case "1": //active/chatting
      case "0": //away
      case "3": //left/away
      case "5": //aborted    
        if (!notifyEnterLeaveEvent) return;
        try {
          var msg = isValidSelector(this.templates.userPresence)
                    ? this.templates.userPresence.tmpl($.extend({}, activity, { User: userModel, Room: roomModel })).html()
                    : "{0} {1} the room {2}".format($.isMe(userModel) ? "You" : userModel.UserName, (event === "1") ? "have joined" : "has left", (roomModel != null) ? ":" + roomModel.Id : "");
          if (typeof (msg) === "string" && msg.length > 0) ui.addSystemMessage({ Id: $.ss.utility.newGuid(), From: "system", MessageType: "msg ", Content: msg, Created: new Date() });
        } catch (e) {
        }
        break;
      default:
        break;
    }
  });

  $(ui).on($events.ui.error, function (ev, msg) {
    //handling of UI errors
    alert(msg);
  });

  if (!chat) {
    chat = {};
  }
  $.chat.ui = ui;

})(window.jQuery, window, window.document, window.chat);
