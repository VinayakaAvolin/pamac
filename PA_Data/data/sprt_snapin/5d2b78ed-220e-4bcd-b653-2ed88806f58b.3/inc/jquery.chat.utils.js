﻿"use strict"
if(typeof getIEVersionNumber==='undefined'){
  var getIEVersionNumber = function () {
    var ie_version;
    try {
        var matches;
        if (window.navigator.appName === 'Microsoft Internet Explorer') {
            matches = /msie ([0-9]+\.[0-9]+)/i.exec(window.navigator.userAgent);
            if (matches && matches[1]) ie_version = window.parseFloat(matches[1]);
        } else {
            matches = /(MSIE |Trident\/.*rv[ :]|edge\/|iemobile\/)([0-9]+\.[0-9]+)/i.exec(window.navigator.userAgent);
            if (matches && matches[2]) ie_version = window.parseFloat(matches[2]);
        }
    } catch (e) { }
    return (ie_version);
};
}

function isValidDate(data) {
    if (Object.prototype.toString.call(data) !== '[object Date]') return false;
    return !isNaN(data.getTime());
};

Array.prototype.containsEx = function (obj) {
    var len = this.length;
    for (var i = 0; i < len; i++) {
        if (this[i].equals(obj)) {
            return true;
        }
    }
    return false;
};

Array.prototype.findEx = function (key, val) {
    var len = this.length;
    for (var i = 0; i < len; i++) {
        if (this[i][key] === val) {
            return this[i];
        }
    }
    return null;
};

Function.prototype.inherits = function (parent) {
    if (parent.constructor == Function) {	//use normal inheritance		
        this.prototype = new parent();
        this.prototype.constructor = this;
        this.prototype.parent = parent.prototype;
    } else {  //use virtual inheritance
        this.prototype = parent;
        this.prototype.constructor = this;
        this.prototype.parent = parent;
    }
    return this;
};

Date.prototype.toLocalDateTime = function () {
    var localNow = new Date();
    var fromDate = this;
    var timeZoneOffset = -localNow.getTimezoneOffset() * 60000;
    return new Date(fromDate.getTime() + timeZoneOffset);
};

Date.prototype.toUTCDateTime = function () {
    var fromDate = this;
    if (!isValidDate(fromDate)) return fromDate;
    var millis = fromDate.getTime() + (fromDate.getTimezoneOffset() * 60000)
    fromDate.setTime(millis - (fromDate.getTimezoneOffset() * 60000))
    return new Date(fromDate.getTime());
};

String.prototype.toUTCDateTime = function () {
    var isoDate = this;
    var utcDate = new Date(isoDate);
    if (isValidDate(utcDate)) {
        return new Date(utcDate.getTime() + (utcDate.getTimezoneOffset() * 60000));
    }
    var parts = isoDate.match(/\d+/g);
    return new Date(parts[0], parts[1] - 1, parts[2], parts[3], parts[4], parts[5]);
};

(function ($, chat) {
    "use strict";
    if (typeof ($) !== "function") {// no jQuery!
        throw new Error("jquery.chat.utils: jQuery not found. Please ensure jQuery is referenced before the jquery.chat.utils.js file.");
    }


    $.getProperty = function (obj, prop) {
        if (obj
            /*jshint laxbreak: true */
          && typeof obj === 'object'
          && obj.hasOwnProperty(prop)) {	// 'undefined'!==obj['prop'] would have been faster;-(
            /*jshint laxbreak: false */
            return obj[prop];
        }
        return null;

    };

    $.isFunc = function (obj) {
        return !!(obj && obj.constructor && obj.call && obj.apply);
    };

    $.getFnName = function (fn) {
        var isfuncType = $.isFunction(fn);
        if (!isfuncType) return null;
        var s = isfuncType && ((fn.name && ['', fn.name]) || fn.toString().match(/^function\s?([^\s(]*)/));
        return (s && s[1] || 'anonymous');
    };

    $.hasMembers = function (obj) {
        try {
            for (var key in obj) {
                if (obj.hasOwnProperty(key)) return true;
            }
        } catch (e) { }
        return false;
    };

    $.sequentialExecute = function (steps, continueOnFailure) {
        var handleStep, handleResult,
            chain = Array.prototype.slice.call(steps),
            def = new $.Deferred(),
            defs = [],
            results = [];

        if (continueOnFailure === undefined) {
            def.done(steps);
            def.resolve();
            return def;
        }
        handleStep = function () {
            if (!chain.length) {
                def.resolveWith(defs, [results]);
                return;
            }
            var step = chain.shift(),
                result = step();
            handleResult(
                $.when(result).always(function () {
                    defs.push(this);
                }).done(function () {
                    results.push({ resolved: Array.prototype.slice.call(arguments) });
                }).fail(function () {
                    results.push({ rejected: Array.prototype.slice.call(arguments) });
                })
            );
        };
        handleResult = continueOnFailure ?
                function (result) {
                    result.always(function () {
                        handleStep();
                    });
                } :
                function (result) {
                    result.done(handleStep)
                        .fail(function () {
                            $.ss.utility.error("Failed to execute step");
                            def.rejectWith(defs, [results]);
                        });
                };
        handleStep();
        return def.promise();
    };

    $.parallelExecute = function (steps, continueOnFailure) {
        var chain = Array.prototype.slice.call(steps);
        return $.when.apply(null, $.map(chain, function (step) {
            step();
        }));
    };

})(jQuery);


(function ($, chat) {
    "use strict";

    if (typeof ($) !== "function") {// no jQuery!
        throw new Error("jquery.chat.utils: jQuery not found. Please ensure jQuery is referenced before the jquery.chat.utils.js file.");
    }

    if (!$.chat) {
        $.chat = {};
    }

    var appevents = {
        connectionStateChange: 'chat.app.connstatechanged',
        createdRoom: 'chat.app.roomcreated',
        closedRoom: 'chat.app.roomclosed',
        ownedRoom: 'chat.app.ownroom',
        joinedRoom: 'chat.app.joinedroom',
        leftRoom: 'chat.app.leftroom',
        kickedOut: 'chat.app.kickedout',
        activity: 'chat.app.activity',
        error: 'chat.app.error',
        newMessage: 'chat.app.newmsg',
        newContent: 'chat.app.newcontent'
    },
    uievents = {
        activity: 'chat.ui.activity',
        closeRoom: 'chat.ui.closeRoom',
        sendMessage: 'chat.ui.sendMessage',
        newContent: 'chat.ui.newContentAvailable',
        error: 'chat.ui.error'
    },
    initialized = false;

    var $events = function () {
        return new $events.fn.init();
    };

    $events.fn = $events.prototype = {
        app: appevents,
        ui: uievents,
        init: function () {
            var self = this;
            if (!initialized) {
                this._ = {
                    callbackMap: {}
                };
                initialized = true;
            }
        },

        /// <summary>Wires up a callback to be invoked when a events are raised within the chat plugin.</summary>
        /// <param name="eventName" type="String">The name of the event to register the callback for.</param>
        /// <param name="callback" type="Function">The callback to be invoked.</param>     
        on: function (eventName, callback) {
            var self = this,
            callbackMap = self._.callbackMap;

            //If no event registered for this callback create an empty event callback map.      
            if (!callbackMap[eventName]) {
                callbackMap[eventName] = {};
            }
            // map the callback to our encompassed function
            if ($.isFunction(callback)) {
                callbackMap[eventName][callback] = function (e) {
                    callback.apply(self, Array.prototype.slice.call(arguments, 1));
                };
                $(self).bind(eventName, callbackMap[eventName][callback]);
                var callbackName = $.getFnName(callback) || "unknown";
                $.ss.utility.debug("Added subscription for event[" + eventName + "] with callback ['" + callbackName + "']");
            } else {
                callbackMap[eventName] = callback;
            }
        },

        /// <summary>Removes  a callback for plug-in events.</summary>
        /// <param name="eventName" type="String">The name of the event.</param>
        /// <param name="callback" type="Function">The callback registered for invocation.</param>
        off: function (eventName, callback) {
            var self = this,
                callbackMap = self._.callbackMap,
                  eventCallbacks;
            eventCallbacks = callbackMap[eventName];
            if (!eventCallbacks) {
                $.ss.utility.warn("No subscription for event[" + eventName + "] is available");
                return;
            }
            //we found the callbacks for this event
            if (callback) {
                var callbackName = $.getFnName(callback) || "object";
                if (!eventCallbacks[callback]) {
                    $.ss.utility.warn("Subscription for event[" + eventName + "] with callback['" + callbackName + "'] not available");
                    return;
                }
                //if callback is funct type unbind else ignore
                if ($.isFunction(callback)) {
                    $(self).unbind(eventName, eventCallbacks[callback]);	//unbind the event+callback combination
                }
                delete eventCallbacks[callback];  //cleanup
                $.ss.utility.debug("Removed subscription of ['" + callbackName + "'] for event[" + eventName + "]");
            } else {
                //unbind the entire event
                $(self).unbind(eventName);
                delete callbackMap[eventName];
                $.ss.utility.debug("Removed all subscriptions for event[" + eventName + "]");
            }
            if (!$.hasMembers(eventCallbacks)) {
                delete callbackMap[eventName];
            }
        },



        /// <summary>Fires a plug-in events.</summary>
        /// <param name="eventName" type="String">The name of the event.</param>
        /// <param name="data" type="object">data to be passed to the registered callback(s)</param>
        fire: function (eventName, data) {

            var self = this,
                  callbackMap = self._.callbackMap,
                  callback = callbackMap[eventName];
            if (callback) {
                $(self).triggerHandler(eventName, $.makeArray(data));
                $(callback).triggerHandler(eventName, $.makeArray(data));
            }
        }
    };


    $.chat.isContentMsg = function (messageObj) {
        return (/(video|image|url|iframe|package|surveywebpage|surveyinline)/i.test(messageObj.MessageType));
    };

    $events.fn.init.prototype = $events.fn;

    $.chat.events = $events.fn;

    $.chat.events.init();

})(jQuery);
