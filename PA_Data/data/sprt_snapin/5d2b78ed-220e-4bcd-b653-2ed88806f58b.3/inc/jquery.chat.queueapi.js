/*!
 * SupportSoft Chat Queue Api.
 *
 * Copyright (C) Aptean. All rights reserved.
 *
 * Dependencies:
 *    jquery.chat.api.js
 *    jquery.chat.utils.js
 *    Analyst side depends on queuepoll.js
 *
 
 */
(function ($, chat) {

  "use strict";

  if (typeof ($) !== "function") {// no jQuery!
    throw new Error("jquery.chat.utils: jQuery not found. Please ensure jQuery is referenced before the jquery.chat.utils.js file.");
  }

  if (!$.chat) {
    $.chat = {};
  }
  var $queueapi = function () {
    return new $queueapi.fn.init();
  };


  /* RAC [ private functions */

  /* RAC CheckQueueStatus
     This is fomerly QueueStatus() callback function from csr.asp
     that supports the existing queue polling engine.
  */
  function CheckQueueStatus(obj) {
    var objRoomInfo = {};

    if (obj.type == "loaderror") {
      $.ss.utility.log("QueueStatus Error. Reason:" + obj.reason);
      $.chat.events.fire($.chat.events.app.queueError, obj);
    } else if (obj.type == "status" && g_QInstance.getState() == Q_STATE_POLLING) {
      //do nothing as network check is updated on switch off
    } else {
      // TBD - perhaps show a system message, directly here
    }
    $.ss.utility.log("QueueStatus Object type [" + obj.type + "] QueueStatus Q State [" + g_QInstance.getState() + "]");

    if (obj.type == "msg") {
		$.chat.events.fire($.chat.events.app.newMessage,
		  {
			Id: $.ss.utility.newGuid(),
			From: "system",
			MessageType: "text",
			Content: obj.msg,
			Created: new Date()
		  }
		);
    } else if (obj.type == "status") {
      if (obj.user != null && obj.room != null && obj.user != "" && obj.room != "") {
        objRoomInfo.Ident = obj.ident;
        if (objRoomInfo.Ident.indexOf("SIP") != -1) {
          var re = /SIP:[^;]+;(.*)/g;
          objRoomInfo.Ident = objRoomInfo.Ident.replace(re, "$1");
        }
        objRoomInfo.remoteUser = obj.user;
        objRoomInfo.InRoom = obj.room;
        objRoomInfo.room = obj.room;
        objRoomInfo.CurrentQueue = obj.queue;
        objRoomInfo.EscalatedIn = (obj.esc_in == "1");
        objRoomInfo.problem = obj.problem;
        objRoomInfo.issGuid = getIssueGuidForRoom(obj.room);

        //taken from updateStats in chat.inc
        objRoomInfo.si = obj.si;
        objRoomInfo.cmd = obj.cmd;
        objRoomInfo.rtc = (obj.ident.indexOf("SIP") != -1);

        //value of rc is copied to ctls in formLAObject of queuepoll.js
        var sUserSideCtls = (obj.ctls != null && obj.ctls != "") ? obj.ctls : "";
        objRoomInfo.rc = (sUserSideCtls.indexOf("rc") != -1);
        objRoomInfo.ss = (sUserSideCtls.indexOf("ss") != -1);
        objRoomInfo.lnExe = (sUserSideCtls.indexOf("lnExe") != -1);
        objRoomInfo.ln = (sUserSideCtls.indexOf("ln") != -1);

        if (obj.nexus != null && obj.nexus != "") {
          var arr = obj.nexus.split("|");
          if (arr.length > 0) {
            objRoomInfo.Nexus = {};
            objRoomInfo.Nexus.ident = obj.ident;
            objRoomInfo.Nexus.nexus = arr[0];
            objRoomInfo.Nexus.name = arr[1];
            objRoomInfo.Nexus.ip = arr[2];
            objRoomInfo.Nexus.domain = arr[3];
          }
        }
        // RAC - the status is wrong by the time it gets here, do we still need to set it?
        // objRoomInfo.status = obj.status;
        pickQueuedItem(objRoomInfo);

      }
    }
  }

  function setQueueStatusForAnalyst(pobjData) {

    // RAC url presumes back-end for this is on the same domain
    // ... this is not necessarily a correct assumption
    return $.ajax({
      url: $.chat.api.getHost() + '/sdccommon/lachat/asp/setqueuestatus.asp',
      type: 'POST',
      data: JSON.stringify(pobjData),
      /*contentType: "application/json;charset=utf-8",
      beforeSend: function (request) {
        request.setRequestHeader('Accept', 'application/json; charset=utf-8');          
      },*/
      success: function (pobjStatus) {
        $.ss.utility.log("setQueueStatusForAnalyst() success: " + JSON.stringify(pobjData));
      },
      error: function (xhr, textStatus, errorThrown) {
        var errmsg = "setQueueStatusForAnalyst() FAIL: " + JSON.stringify(pobjData);

        $.ss.utility.log(errmsg);
        $.chat.events.fire($.chat.events.app.error, errmsg);
      }
    });

  }

  // RAC function to properly format return data from ASP calls
  function parseReturnData(data) {
    var objData = {};

    try {
      objData = JSON.parse(data.replace(/^.*?\?*\(/, "").replace(/\)$/, ""));
    } catch (e) {
      // noop
    }

    return objData
  }

  function getIssueGuidForRoom(pstrRoom) {

    var retVal = "";

    $.ajax({
      async: false,
      url: $.chat.api.getHost() + '/sdccommon/lachat/asp/getissueguidforroom.asp',
      type: 'GET',
      data: { r: pstrRoom },
      dataFilter: parseReturnData,
      success: function (data, status) {
        if (status == "success") {
          retVal = data.issguid;
        }
      },
      error: function (xhr, textStatus, errorThrown) {
        var errmsg = "FAIL in call to getissueguidforroom [" + pstrRoom + "]"

        $.ss.utility.log(errmsg);
      }
    });

    return retVal;
  }

  function pickQueuedItem(pickedQueueItem) {
    var objData = {
      op: "setWorking",
      params: {
        pAnalystid: $.chat.queueapi.username,
        pRoom: pickedQueueItem.room
      }
    };
    // RAC url presumes back-end for this is on the same domain
    // ... this is not necessarily a correct assumption
    setQueueStatusForAnalyst(objData)
      .fail(function (xhr, textStatus, errorThrown) {
        $.chat.events.fire($.chat.events.app.queueError, [{ type: "status", room: pickedQueueItem.room, user: $.chat.queueapi.username, reason: xhr.responseText }]);
      })
      .done(function (reponseString) {	  
        var responseObj = parseReturnData(reponseString);
        if (responseObj.status !== 'success') {
			    //something has gone horibly wrong-bail out
			    $.chat.events.fire($.chat.events.app.queueError, [{ type: "status", room: pickedQueueItem.room, user: $.chat.queueapi.username, reason: "API error" }]);
				  return;
        }
        $.ss.utility.log("pickedQueueItem [" + pickedQueueItem.room + "]");
        g_Room = pickedQueueItem.room;
        g_Queue = pickedQueueItem.CurrentQueue;
        g_QInstance.stopPoll(); //unconditionally(irrespective of current state-->g_QInstance.getState())  stop polling
        g_QInstance.setState(Q_STATE_WORKING);
        $.chat.events.fire($.chat.events.app.pickedQueueItem, [pickedQueueItem]);
      });
  }

  $queueapi.fn = $queueapi.prototype = {
      init: function () {

        /* RAC 12.2.2014 Queue Priority Configuration */
        $.chat.queueapi.queuepriority = {
          prefAnalyst: 10,
          autoEsc: 20,
          manualEsc: 30,
          reconnectChat: 40,
          normalChat: 40
        }
        // Set the flag so we know if we are analyst side or not.
        $.chat.queueapi.isAnalystSide = false;
        // This looks like a good place to initialize the global poll handler

        // portions of the initialization are analyst side only
        // ... since user-side includes the api
        try {
            if (typeof g_QInstance == 'undefined' 
              && typeof classQueuePoll == 'function') {

            $.chat.queueapi.isAnalystSide = true;
            // the register() call needs to initialize queues anyway; put in TBD to make the QInstance init() method happy
            var queues = [""];
            g_QInstance = new classQueuePoll(queues, Q_LIVEASSIST);
            g_QInstance.init(CheckQueueStatus); // register callback
            g_fnInpoll = function (bStarting) {
              if(bStarting) {
                availBttn.enabled = false;
                if (!availBttn.pressed) g_QInstance.stopPoll();
              } else {
                availBttn.enabled = true;
              }
            }
          }
          if($.chat.queueapi.isAnalystSide === true){
          $.chat.events.on($.chat.events.app.queueError, function (queueErrorInfo) {
            $.ss.utility.log("Failed to process queued item==>" + queueErrorInfo.room );

            var objData = {
               op: "setBackToWaiting",
               params: {
                  pAnalystid: $.chat.queueapi.username,
                  pRoom: queueErrorInfo.room
               }
            };
               
            setQueueStatusForAnalyst(objData)
               .fail(function (xhr, textStatus, errorThrown) {
                  $.ss.utility.log("setBackToWaiting failed also. ==>" + queueErrorInfo.room );
               })
               .done(function (reponseString) {
                  // What do we do here - anything?  
               });
          });

            $.chat.events.on($.chat.events.app.joinedRoom, function (userInfo,joinDetails) {              
              if(!$.isMe(userInfo)) return;
            // keeping the state as "working" when analyst joins the room or there's a refresh
            g_Room = joinDetails.Id;
            g_QInstance.setState(Q_STATE_WORKING);
          });


          $.chat.events.on($.chat.events.app.leftRoom, function (pWhoLeft, pobjRoomInfo) {
            // RAC TODO How should we treat analysts that are escalated/autoescalated out - as seen in the queue table?
            // Should closed_date be NULL and should there be a "left_date" ?
            // Showing it that way should ensure 1 row has a "closed_date" row populated and indicate that was the analyst
            // that actually closed it after any escalations/autoescalations
          });
        }
          
        }
        catch (e) {
          $.ss.utility.log("$.chat.queueapi.init() - analyst-side operations skipped");
          
        }

        // RAC Subcribe to the closed room event so we can call down and update the queue status - analyst and user side
        $.chat.queueapi.dClosedRoom = $.Deferred();
        $.chat.events.on($.chat.events.app.closedRoom, function (pobjRoomInfo, pBy) {
          
          // from queueapi
          
          if($.isMe(pBy.UserName)) {
            var objData = {
              op: "setClosed",
              params: {
                pRoom: pobjRoomInfo
              }
            };

            setQueueStatusForAnalyst(objData).then(function() {
               $.chat.queueapi.dClosedRoom.resolve();
            })
          }
        
          $.ss.utility.log("$.chat.events.app.closedRoom  completed from queueapi");
          
          
          
        });

      },

      registerAnalyst: function (username, queues) {

        $.chat.queueapi.username = username;

        // variable initialized for queue polling.  these are defined in queuepoll.js
        // RAC ?  g_fnInpoll = PollStarting;
        // RAC formerly in toggleAvailable().  Presumes g_QInstance is already setup in init()
        g_QInstance.arrQueue = queues;
        g_QInstance.setState(Q_STATE_AVAILABLE); //set logging from where ever required
        g_QInstance.startPoll();                 //don't want Q inits to handle logging     
      },

      unregisterAnalyst: function (username, queues) {

        // RAC presumes g_QInstance is setup already in init()
        g_QInstance.stopPoll();                 //don't want Q cleanups to handle logging

        g_QInstance.setState(Q_STATE_POLLING);  //set logging from where ever required
      },

      setAnalystActivityToBreak: function () {
        g_QInstance.setState(Q_STATE_BREAK); //set logging from where ever required
        g_QInstance.stopPoll();
      },
      setAnalystActivityToOffBreak: function () {
        g_QInstance.setState(Q_STATE_OFF_BREAK); //set logging from where ever required
      },

      setQueueForRoom: function (room, queue, preferred, prevRoom) {

        var objData = {
          op: "setQueueForRoom",
          params: {
            pRoom: room,
            pQueue: queue,
            Preferred: preferred
          }
        };
        
        if (typeof prevRoom !== 'undefined') {
           objData.params.prevRoom = prevRoom
        }

        // RAC url presumes back-end for this is on the same domain
        // ... this is not necessarily a correct assumption
        return setQueueStatusForAnalyst(objData);
      },

      escalateToAnotherAnalyst: function (pRoom, queue, comments, analystUserName, bIsAutoEscalated) {

        $.ss.utility.log("escalateToAnotherAnalyst() queue [" + queue + "], comments [" + comments + "], analystUserName [" + analystUserName + "]");
        var objData = {
          op: "escalateToAnotherAnalyst",
          params: {
            pRoom: pRoom,
            queue: queue,
            comments: comments,
            analystUserName: analystUserName,
            bIsAutoEscalated: bIsAutoEscalated
          }
        };

        return setQueueStatusForAnalyst(objData)
        .done(function(queueStatus) {
          // If preferred analyst then set this too
          // RAC - id is nPreferred because code in check.asp does this also
          if(analystUserName.length > 0) {
            var nPreferred = 2;

            objData = {
              "id": nPreferred,
              "room": pRoom,
              "analyst_name": analystUserName,
              "queue": queue,
              "entry_time": "",
              "created_date": "",
              "preferred": nPreferred
              };

            $.ajax({
              url: $.chat.api.getHost() + '/sdccommon/lachat/asp/setpreferredanalyst.asp',
              type: 'POST',
              data: JSON.stringify(objData),
                /*contentType: "application/json;charset=utf-8",
                beforeSend: function (request) {
                  request.setRequestHeader('Accept', 'application/json; charset=utf-8');             
                },*/
              success: function (roomInfo) {
                $.ss.utility.log("setpreferredanalyst success: " + JSON.stringify(objData));
              },
              error: function (xhr, textStatus, errorThrown) {
                var errmsg = "setpreferredanalyst FAIL: [" + xhr.statusText + "] " + JSON.stringify(objData);

                $.ss.utility.log(errmsg);
                $.chat.events.fire($.chat.events.app.error, errmsg);
              }
            });
          } //
        });
      },

      setDTLQueueForRoom: function (room, analystid) {

        var objData = {
          op: "setDTLQueueForRoom",
          params: {
            pRoom: room,
            panalystid: analystid
          }
        };

        // RAC url presumes back-end for this is on the same domain
        // ... this is not necessarily a correct assumption
        return setQueueStatusForAnalyst(objData);
      },

      setWorkingForSupervisecontrol: function (room, analystid) {

        var objData = {
          op: "setWorkingForSupervisecontrol",
          params: {
            pRoom: room,
            panalystid: analystid
          }
        };

        // RAC url presumes back-end for this is on the same domain
        // ... this is not necessarily a correct assumption
        return setQueueStatusForAnalyst(objData);

      },

      logAnalystEvent: function (mode) {
        sdcLogEvent(mode);
      },
      getIssueGuidForRoom: getIssueGuidForRoom,

      getCurrentQueueState: function (room) {
        var strUrl = "/sdccommon/lachat/asp/getqueuestatus.asp";
        var objData = { rm : room };
        var retVal = { };
        
        var objDfd = $.Deferred();
        
        $.ajax({
          url: $.chat.api.getHost() + strUrl,
          type: 'GET',
          data: objData,
          dataFilter: parseReturnData,
          success: function(data,status) {
            $.ss.utility.log("getCurrentQueueState() success");
            retVal = data;
            objDfd.resolve(retVal);
          },
          error: function (xhr, textStatus, errorThrown) {
            var errmsg = "getCurrentQueueState() FAIL: [" + xhr.statusText + "] " + JSON.stringify(objData);
            $.ss.utility.log(errmsg);
            $.chat.events.fire($.chat.events.app.error, errmsg);
          }
        });

        // return retVal;
        return objDfd;
      },

      getPreviousRoom: function (newRoom) {
        $.ss.utility.log("getPreviousRoom() called for newRoom [" + newRoom + "]");

        var retVal = "";

        var objData = { r : newRoom
        };

        var strUrl = "/sdccommon/lachat/asp/getpreviousroom.asp";

        $.ajax({
          async: false,
          url: $.chat.api.getHost() + strUrl,
          type: 'GET',
          data: objData,
          dataFilter: parseReturnData,
          success: function (prevRoomData, status) {
            retVal = prevRoomData.room;
          },
          error: function (xhr, textStatus, errorThrown) {
            var errmsg = "FAIL in call to " + strUrl + " [" + xhr.statusText + "] " + JSON.stringify(objData);

            $.ss.utility.log(errmsg);
            $.chat.events.fire($.chat.events.app.error, errmsg);
          }
        });

        return retVal;
      },

      getChatWaitingInQueue: function (room, queue) {
        var strUrl = "/sdccommon/lachat/asp/getchatwaitinginqueue.asp";

        var objData = { r : room, q: queue };

        var retVal = { };
        $.ajax({
          async: false,
          url: $.chat.api.getHost() + strUrl,
          type: 'GET',
          data: objData,
          dataFilter: parseReturnData,
          success: function (data, status) {
            $.ss.utility.log("getCurrentQueueState() success");
            retVal = data;
          },
          error: function (xhr, textStatus, errorThrown) {
            var errmsg = "getChatWaitingInQueue() FAIL: [" + xhr.statusText + "] " + JSON.stringify(objData);
            $.ss.utility.log(errmsg);
            $.chat.events.fire($.chat.events.app.error, errmsg);
          }
        });
        return retVal;
      }

    };


  $queueapi.fn.init.prototype = $queueapi.fn;

  $.chat.queueapi = $queueapi.fn;

  var queueEvents = {
      app: {
        pickedQueueItem: 'chat.app.pickedQueueItem',
        queueError: 'chat.app.queueError',
        agentAssigned: 'chat.app.agentAssigned'
      }
    };
  $.extend(true, $.chat.events, queueEvents);

  $.chat.queueapi.init();

}) (jQuery);