﻿(function ($, window, localization) {
  "use strict";

  var navigatorInfo = navigator.userAgent || navigator.vendor || window.opera;
  
  var isMobile = /android|avantgo|bada\/|blackberry|bb10|playbook|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od|ad)|iris|kindle|lge |maemo|midp|mmp|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino|\bSilk\/(.*\bMobile Safari\b)?/i.test(navigatorInfo) ||
                 /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|e\-|e\/|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(di|rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|xda(\-|2|g)|yas\-|your|zeto|zte\-/i.test(navigatorInfo.substr(0, 4)) ;

  function format(formatString) {
    if (!formatString) {
      return "";
    }
    var formatArgs = arguments;
    return formatString.replace(/{(?:[0-9]+)}/g, function (match) {
      return formatArgs[parseInt(match.substr(1, match.length - 2), 10) + 1];
    });
  };

  function getTranslatedText(languageKey) {
    var args = Array.prototype.slice.call(arguments, 0);
    args[0] = window.localization[languageKey];
    return format(null, args);
  };

  function linkifyHtmlUrls(html, target, hashTagUrl) {
    //needs jquery.linkify.js..so if missing return as-is.	
    if (typeof linkify === 'undefined') return html;
    target = typeof target === 'undefined' ? '_blank' : target;
    if (typeof hashTagUrl === 'function' || typeof hashTagUrl === 'string') {
      return linkify(html, typeof hashTagUrl === 'function' ? hashTagUrl : function (hashTag) { return hashTagUrl + '/' + hashTag; }, true, target);
    }
    return linkify(html, null, true, target);

  };

  function sprtlogger(option, rsrc) {
      function _lgr(lt, enabled) {
        return function (msg, src) {
          try {
            if (msg && enabled === true && window.console && typeof (window.console[lt]) === "function") {
              var lm = "[{0}] {1}: {2}".format(new Date().toTimeString(), (src || rsrc) || '', typeof (msg) === 'object' ? JSON.stringify(msg) : msg);
              window.console[lt](lm);
            }
          }catch (e) {}
        }
      }
      this.debug = _lgr('debug', /d/i.test(option));
      this.log = _lgr('info', /i/i.test(option));
      this.info = _lgr('info', /i/i.test(option));
      this.warn = _lgr('warn', /w/i.test(option));
      this.error = _lgr('error', /(e|f)/i.test(option));
  };
  
  var escapeMappings = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#39;'	//&apos for HTML 5
	,'\\':'&#92;'	
	,'<br>':'\n'	
  };

  var unescapeMappings = {};
  $.map(escapeMappings, function (val, key) { unescapeMappings[val.replace(/\\/g,'\\\\')] = key;});

  var _encoder = function (map) {
    var keys = [];
    $.map(map, function (val, key) { keys.push(key.replace(/\\/g,'\\\\')); });
	
    var source = '(?:' + keys.join('|') + ')';
    var testRegexp = RegExp(source);
    var replaceRegexp = RegExp(source, 'gm');
    return function (str) {
      if (str == null) return str;
      str = '' + str;
      return testRegexp.test(str) ? str.replace(replaceRegexp, function (match) { return map[match]; }) : str;
    };
  };

  var encoder = _encoder(escapeMappings);
  var decoder = _encoder(unescapeMappings);
  var logger =  new sprtlogger('diwef'); //default logger with all levels enabled
  var utility = {
    elipsify: function (value, length) {
      if (value.length > length) {
        return value.substr(0, length - 3) + '...';
      }
      return value;
    },
    isMobile: function () { return isMobile; },
    decodeHtml: function (html) {
      //TODO: replace with underscore.js
      html = $(document.createElement("div")).html(html).text();
      return decoder(html);
    },
    encodeHtml: function (html) {
      if (html == null) return '';
      //TODO: replace with underscore.js
      //NB: fastest would be document.createElement( 'div' ).appendChild( document.createTextNode( html ) ).parentNode.innerHTML;	  
      html = $(document.createElement("div")).text(html).html();
      return encoder(html);
    },
    newGuid: function () {
      var S4 = function () {
        return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
      };
      return (S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4());
    },
    random: function (min, max) {
      return Math.floor(Math.random() * (max - min + 1) + min);
    },
    linkifyUrls: linkifyHtmlUrls,
    format: format,
    getLocalizedString: getTranslatedText,
    log: logger.debug,
    info: logger.info,
    debug: logger.debug,
    warn: logger.warn,
    error: logger.error
  };

  if (!$.ss) {
    $.ss = {};
  }

  if (!window.localization) {
    window.localization = {};
  }
  $.ss.utility = utility;
  $.ss.logger = function(option,src){ return new sprtlogger(option,src)};
   
  //NB: funcs are defined unconditionally (w/o checks) - so it will override existing definations
  if (!Array.prototype.indexOf) {
      Array.prototype.indexOf = function (value , startIndex) {
    var len = this.length;
    for (var i = (startIndex || 0); i < len; i++) {
      if (this[i] === value) {
        return i;
      }
    }
    return -1;
  }
  };

  Array.prototype.contains = function (value) {
    return this.indexOf(value) > -1;    
  };

  String.prototype.format = function () {
    var args = arguments;
    return this.replace(/{(\d+)}/g, function (match, number) {
      return typeof args[number] != 'undefined'
        ? args[number]
        : match
      ;
    });
  };

  String.prototype.contains = function (str) {
    return !!~('' + this).indexOf(str);
  };

})(jQuery, window, window.localization);
