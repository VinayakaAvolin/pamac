(function ($) {
  "use strict";

  if (typeof ($) !== "function") {// no jQuery!
    throw new Error("jquery.chat.ui: jQuery not found. Please ensure jQuery is referenced before the jquery.chat.ui.js file.");
  }

  var isTouchDevice = (('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch),
		clickedEvent = "touchstart click dblclick"

  function isValidSelector($elem) {
    return ($elem && $elem.length > 0);
  };

  function toggleShowOrHide($this, bShow) {
    if (!isValidSelector($this)) return;
    if (bShow) {
      $this.removeClass('hide').addClass('show');
    } else {
      $this.removeClass('show').addClass('hide');
    }
    return $this;
  };
  function toggleLighBoxEffect() {
    try {
      var currBgCss = $('.white_content').css('display'),
			currFgCss = $('.black_overlay').css('display');
      $('.white_content').css({ display: /block/i.test(currFgCss) ? 'none' : 'block' });
      $('.black_overlay').css({ display: /block/i.test(currFgCss) ? 'none' : 'block' });
    } catch (e) { }
  };

  function fixContentDisplayUnderMiniGallery($miniGalleryItem) {
    if (!$miniGalleryItem) return;
    $miniGalleryItem.find('video').removeAttr('controls');
    $miniGalleryItem.find('object param[name="ShowControls"]').attr('value', 'false');
    $miniGalleryItem.find('embed').attr('ShowControls', '0');
  };

  var ContentNavigator = function (container, options) {
    //this.constructor.Container = 
    this.$container = $(container);
    this._contents = [];
    this._currentIndex = -1;
    this._galleryMode = false;
    this.settings = {
      main: {
        nextBtn: '.traverse-right',
        prevBtn: '.traverse-left'
      },
      gallery: {
        container: '.gallery-view-container',
        showGalleryBtn: '.show-gallery',
        hideGalleryBtn: '.close-gallery',
        galleryPreview: '.gallery-view',
        galleryMini: '.gallery-view-mini'
      }
    };
    $.extend(true, this.settings, options || {})

    this.$nextBtn = this.$container.find(this.settings.main.nextBtn);
    this.$prevBtn = this.$container.find(this.settings.main.prevBtn);
    this.$gallery = $(this.settings.gallery.container);

    //gallery related	 
    if (isValidSelector(this.$gallery)) {
      this.$showGalleryBtn = this.$gallery.find(this.settings.gallery.showGalleryBtn);
      if (!isValidSelector(this.$showGalleryBtn)) {
        this.$showGalleryBtn = $(this.settings.gallery.showGalleryBtn);
      }
      this.$hideGalleryBtn = this.$gallery.find(this.settings.gallery.hideGalleryBtn);
      this.$galleryMini = this.$gallery.find(this.settings.gallery.galleryMini);
    }
    this.count = function () {
      return this._contents.length;
    };
    this.setup();

  };

  ContentNavigator.prototype.setup = function () {

    var self = this;
    if (!isValidSelector(self.$container)) {
      $.ss.utility.log('container not setup');
    }

    if (isValidSelector(self.$nextBtn)) {
      self.$nextBtn.on(clickedEvent, function (e) {
        e.preventDefault();
        self.next();
      });
    }
    if (isValidSelector(self.$prevBtn)) {
      self.$prevBtn.on(clickedEvent, function (e) {
        e.preventDefault();
        self.previous();
      });
    }
    //gallery realted
    if (!isValidSelector(self.$gallery)) return;

    if (isValidSelector(self.$showGalleryBtn)) {
      self.$showGalleryBtn.hide();
      self.$showGalleryBtn.on(clickedEvent, function (e) {
        e.preventDefault();
        if (self.count() == 0) return;
        toggleLighBoxEffect();
        self.showGallery();
      });
    }
    if (isValidSelector(self.$hideGalleryBtn)) {
      self.$hideGalleryBtn.on(clickedEvent, function (e) {
        e.preventDefault();
        self.closeGallery();
        toggleLighBoxEffect();
      });
    }
    self.$container.data('content-navigator', self);
    return this;
  };

  ContentNavigator.prototype.add = function ($content) {
    if (!$content.length) {
      return;
    }
    var self = this,
		$clone = $content.clone();

    self._contents.push($clone);
    self.$container.append($clone);
    self._currentIndex = self.goTo(self._contents.length - 1);
    if(isValidSelector(self.$showGalleryBtn)) self.$showGalleryBtn.show();
    return self._contents.length;;
  };

  ContentNavigator.prototype.goTo = function (to) {
    var self = this,
			total = self._contents.length;
    if (to >= total || to == self._currentIndex) return self._currentIndex;;

    var $elem = self._contents[to];

    if (!$elem) return self._currentIndex;
    var contentId = $elem.attr('id');
    if (contentId === '') return self._currentIndex;

    self._currentIndex = to;
    if (self._galleryMode) {
      self.showGallery(to);
      return;
    }
    self.$prevBtn.toggleClass('hide', (to <= 0));
    self.$nextBtn.toggleClass('hide', (to >= (total - 1)));

    self.$container.find('.content').removeClass('show').addClass('hide');
    self.$container.find('[id^="' + contentId + '"]').removeClass('hide').addClass('show');
    return self._currentIndex;
  };

  ContentNavigator.prototype.previous = function () {
    var self = this,
		to = ((self._currentIndex - 1 > -1) ? self._currentIndex - 1 : 0);
    return self.goTo(to);
  };

  ContentNavigator.prototype.next = function () {
    var self = this,
      total = self._contents.length,
      to = ((self._currentIndex + 1 < total) ? self._currentIndex + 1 : (self._currentIndex));
    return self.goTo(to);
  };

  ContentNavigator.prototype.showGallery = function () {

    var self = this,
	    showIndex = arguments.length > 0 ? arguments[0] : self._currentIndex,
		$elem = self._contents[showIndex];
    if (!$elem || !$elem.length) return;

    toggleShowOrHide($elem, false);
    toggleShowOrHide(self.$gallery, true);

    toggleShowOrHide(this.$container, false);
    self._galleryMode = true;

    //remove old content
    self.$gallery.find('.content').remove();
    var $galleryElem = $elem.clone();
    toggleShowOrHide($galleryElem, true).addClass('gallery');
    self.$gallery.append($galleryElem);
    //add every content in the mini container
    self.$container.find('.content').each(function (index) {
      var id = this.id;
      if (typeof id === 'undefined') return;

      var $mini = $(this).clone();
      fixContentDisplayUnderMiniGallery($mini);

      toggleShowOrHide($mini, true)
            .removeClass('gallery')
            .addClass('mini');

      self.$galleryMini.append($mini);
      $mini.bind(clickedEvent + ' mouseenter', function () {
        self.goTo(index);
      });
    });

  };

  ContentNavigator.prototype.closeGallery = function () {
    var self = this,
		showIndex = arguments.length > 0 ? arguments[0] : self._currentIndex,
		$elem = self._contents[showIndex];
    toggleShowOrHide($elem, true);
    toggleShowOrHide(self.$gallery, false);
    toggleShowOrHide(self.$container, true);
    self._galleryMode = false;
  };

  var jfn = {
    contentNav: function (options) {
      $(this).each(function () {
        if ($(this).data('content-navigator')) return;
        var nav = new ContentNavigator(this, options);
        $(this).data('content-navigator', nav);
      });
      return this;
    }
  };

  $.fn.extend(jfn);
  $.extend({
    contentNav: function ($container, options) { return new ContentNavigator($container, options); }
  });
})(jQuery);

function _getContentInfofromUrl(url) {
  var contentInfo = {MimeType:'',FileExtn:''};
  if (url.match(/^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/)) {
    contentInfo.MimeType = 'application/x-shockwave-flash';
    return contentInfo;
  }
  var extn = '';
  var extns = url.match(/\.([0-9a-z]+)(?:[\?#]|$)/i);
  if (extns) extn = extns.length > 1 ? extns[1] : extns[0];
  contentInfo.FileExtn = extn;
  if (['mp4', 'webm', 'ogv', 'wmv', 'mpg'].indexOf(extn) >= 0) {
    var mimeType = 'video/' + extn;
    if (extn.search(/mp4/i) != -1) mimeType += '; codecs=&quot;avc1.42E01E, mp4a.40.2&quot;';
    if (extn.search(/webm/i) != -1) mimeType += '; codecs=&quot;vp8, vorbis&quot;';
    if (extn.search(/ogv/i) != -1) mimeType += '; codecs=&quot;theora, vorbis&quot;';
    if (extn.search(/wmv/i) != -1) mimeType = 'video/x-ms-wmv';
    contentInfo.MimeType = mimeType;
  }
  if (['ogg', 'oga', 'spx', 'wav', 'acc', 'm4a'].indexOf(extn) >= 0) {
    contentInfo.MimeType = 'audio/' + extn;
  }
  return contentInfo;
}

var ContentSupport = {};
var checks = {};

checks['video'] = function () {
  var elem = document.createElement('video'),
    supports = false;
  try {
    if (supports = !!elem.canPlayType) {
      supports = new Boolean(supports);
      supports.ogg = elem.canPlayType('video/ogg; codecs="theora"').replace(/^no$/, '');
      supports.ogv = elem.canPlayType('video/ogv; codecs="theora"').replace(/^no$/, '');
      supports.mp4 = elem.canPlayType('video/mp4; codecs="avc1.42E01E"').replace(/^no$/, '');
      supports.webm = elem.canPlayType('video/webm; codecs="vp8, vorbis"').replace(/^no$/, '');
      supports.mpg = elem.canPlayType('video/mpg;').replace(/^no$/, '');
    }
  } catch (e) { }
  return supports;
};

checks['audio'] = function () {
  var elem = document.createElement('audio'),
              bool = false;
  try {
    if (bool = !!elem.canPlayType) {
      bool = new Boolean(bool);
      bool.ogg = elem.canPlayType('audio/ogg; codecs="vorbis"').replace(/^no$/, '');
      bool.mp3 = elem.canPlayType('audio/mpeg;').replace(/^no$/, '');
      bool.wav = elem.canPlayType('audio/wav; codecs="1"').replace(/^no$/, '');
      bool.m4a = (elem.canPlayType('audio/x-m4a;') ||
                    elem.canPlayType('audio/aac;')).replace(/^no$/, '');
    }
  } catch (e) { }

  return bool;
};

for (var check in checks) {
  if (({}).hasOwnProperty.call(checks, check)) {
    ContentSupport[check.toLowerCase()] = checks[check]();
  }
}
