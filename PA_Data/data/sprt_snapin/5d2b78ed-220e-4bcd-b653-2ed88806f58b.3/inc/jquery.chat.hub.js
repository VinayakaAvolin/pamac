﻿(function ($) {
    "use strict";
    /*jslint plusplus: true */
    /*jshint laxbreak: true */
    /*global $, jQuery, setTimeout*/
    var hub = $.connection.ChatHub,
          $events = $.chat.events,
          isConnected = false,
          reconnectAttempts = 0,
          enableLogging = false;

    function log(message) {
        if (!enableLogging) return;
        try {
            $.ss.utility.log(message);
        } catch (e) { }
    };

    function _isContentMsg(messageObj) {
        return (/(video|image|url|iframe|package|surveywebpage|surveyinline)/i.test(messageObj.MessageType));
    };

    function initiateConnection(options) {
        return $.connection.hub.start(options)
             .done(function (result) {
                 log("SUCCESS: Chat hub Connection");
             })
             .fail(function (error) {
                 log("FAILED:Chat hub Connection. Error=" + error);
                 $events.fire($events.app.error, "Failed to connect");
             });
    };

    function notifyConnectionStateChange(status) {
        try {
            isConnected = ($.connection.hub.state === $.connection.connectionState.connected) ? true : false;
            var state = status || (isConnected ? "connected" : "disconnected");

            $events.fire($events.app.connectionStateChange, state);
        } catch (e) { }
    };

    function extractHubErrorDetails(hubErrorObj) {
        var hsSource = ({}).hasOwnProperty.call(hubErrorObj, 'source') && typeof hubErrorObj.source !== 'undefined';
        return ({
            code: hsSource ? hubErrorObj.source.status : '500',
            message: 'HUB ERROR' + ((hsSource ? hubErrorObj.source.statusText + '. ' : '') + hubErrorObj.message || 'Internal Error')
        });
    };

    function reportError(err) {
        try {
            var error = { code: 500, message: 'Internal Error' };
            if (typeof err === 'object') $.extend(error, err);
            else error.message = new String(err);
            $events.fire($events.app.error, error.message);		//currently sending only message part...
        } catch (e) { }
    };

    hub.isConnected = function () {
        var self = this;
        return (isConnected && self.connection.id !== 'undefined');
    };

    hub.setup = function (options) {
        var self = this,
            settings = {
                hostUrl: "",
                transport: "auto",
                logging: false,
                xdomain: true,
                version: "1.0.0.0",
                maxReconnectAttempts: -1,
                reconnectInterval: 3000
            };
		if (typeof getIEVersionNumber ==='function') settings.jsonp = getIEVersionNumber()<=9;
        if (!self.hasOwnProperty('connectionId')) {
            self["connectionId"] = null;
        }
        if ($.type(options) === "object") {
            $.extend(settings, options);
        }
		enableLogging = settings.logging;
        $.connection.hub.url = settings.hostUrl + "/sses/chat_engine";
        $.connection.hub.logging = settings.logging;
        $.connection.hub.qs = { "version": settings.version };
        //handle state changes
        $.connection.hub.stateChanged(function (changed) {
            if (changed.newState === $.connection.connectionState.connected) {
                if (self.hasOwnProperty('connectionId')) {
                    self.connectionId = $.connection.hub.id;
                }
                log("Connected with the Chat Hub....ID=" + $.connection.hub.id);
                notifyConnectionStateChange();
            } else if (changed.newState === $.connection.connectionState.disconnected) {
                log("Disconnected from the Chat Hub....");
                notifyConnectionStateChange();
            } else {
                log("Chat Hub connection status:" + changed.newState);
                notifyConnectionStateChange("disconnected");
            }
        });

        //handle disconnections
        $.connection.hub.disconnected(function () {
            if (settings.maxReconnectAttempts > -1 && ++reconnectAttempts > settings.maxReconnectAttempts) {
                log("Re-connect failed! Re-try attempts exceeded");
                return;
            }
            //try to re-connect after sometime
            setTimeout(function () {
                log("Retrying to connect back to Chat Hub");
                initiateConnection(settings);
            },
            settings.reconnectInterval);
        });

        $.connection.hub.connectionSlow(function () {
            notifyConnectionStateChange("slow");
        });

        //handle errors
        $.connection.hub.error(function (error) {
            log("Error occurred on the Chat Hub. CurrentState[" + $.connection.hub.state + "] Error=" + [JSON.stringify(error)]);
            var errObj = extractHubErrorDetails(error);
            reportError(errObj);
        });
        log("initializing hub..");
        return initiateConnection(settings);
    };

    hub.client.onError = function (err) {
        log("Error occurred on the Chat Hub client. Details:" + err);
        reportError(err);
    };

    hub.client.onNewSession = function (room, user) {
        try {
            var roomModel = $.extend(true, new Room(room.RoomID, room.Description), room),
                userModel = new User(user.UserName, user.Role, user.UserID);
            log("Chat Session [ " + roomModel.Id + " ] created by " + userModel.UserName);
            $events.fire($events.app.createdRoom, [roomModel, userModel]);
        } catch (e) {
            reportError(e);
        }
    };

    hub.client.onOwnSession = function (room, user) {
        try {
            var roomModel = $.extend(true, new Room(room.RoomID, room.Description), room),
              userModel = new User(user.UserName, user.Role, user.UserID);
            roomModel.addUser(userModel);
            if ($.isMe(userModel)) {
                $.chat.client.Id = user.UserID;
                $.chat.client.addRoom(roomModel);
                $.chat.client.setCurrentRoom(roomModel);
            }
            log("Chat session [ " + roomModel.Id + " ] Ownership changed to =>" + userModel.UserName);
            $events.fire($events.app.ownedRoom, [roomModel, userModel]);
        } catch (e) {
            reportError(e);
        }
    };

    hub.client.onSessionClosed = function (room, user, reason) {
        try {
            var roomModel = $.extend(true, new Room(room.RoomID, room.Description), room),
                  userModel = new User(user.UserName, user.Role, user.UserID);

            log("Chat Session [ " + roomModel.Id + " ] closed by " + userModel.UserName);
            $events.fire($events.app.closedRoom, [roomModel, userModel, reason]);
        } catch (e) {
            reportError(e);
        }
    };

    hub.client.onUserJoined = function (user, room) {
        try {
            var roomModel = $.extend(true,  new Room(room.RoomID, room.Description), room),
                  userModel = new User(user.UserName, user.Role, user.UserID);
            roomModel.addUser(userModel);
            if ($.isMe(userModel)) {
                $.chat.client.Id = user.UserID;
                $.chat.client.addRoom(roomModel);
                $.chat.client.setCurrentRoom(roomModel);
            }
            log("User: " + user.UserName + " has joined the Chat room: " + roomModel.Id);
            $events.fire($events.app.joinedRoom, [userModel, roomModel]);
        } catch (e) {
            reportError(e);
        }
    };

    hub.client.onUserLeft = function (user, room) {
        try {
            var roomModel = $.extend(true, new Room(room.RoomID, room.Description), room),
                  userModel = new User(user.UserName, user.Role, user.UserID);

            if ($.isMe(userModel)) {
                $.chat.client.removeRoom(roomModel);
            }
            log("User: " + userModel.UserName + " has left the Chat room: " + roomModel.Id);
            $events.fire($events.app.leftRoom, [userModel, roomModel]);
        } catch (e) {
            reportError(e);
        }
    };

    hub.client.onKickedOut = function (who, by, room) {
        try {
            var roomModel = $.extend(true, new Room(room.RoomID, room.Description), room),
              kickedUserModel = new User(who.UserName, who.Role, who.UserID),
              byUserModel = new User(by.UserName, by.Role, by.UserID);

            if ($.isMe(kickedUserModel)) {
                $.chat.client.removeRoom(roomModel);
            }
            log("User: " + kickedUserModel.UserName + " was kicked off the Chat room [" + roomModel.Id + "] by " + byUserModel.UserName);
            $events.fire($events.app.kickedOut, [kickedUserModel, byUserModel, roomModel]);
        } catch (e) {
            reportError(e);
        }
    };

    hub.client.onNewMessage = function (message, from, room) {
        try {
            var userModel = new User(from.UserName, from.Role, from.UserID),
                 roomModel = $.extend(true, new Room(room.RoomID, room.Description), room);

            $events.fire($events.app.newMessage, [message, userModel, roomModel]);
        } catch (e) {
            reportError(e);
        }
    };

    hub.client.onNewContent = function (content, by, room) {
        try {
            var userModel = new User(by.UserName, by.Role, by.UserID),
                      roomModel = $.extend(true, new Room(room.RoomID, room.Description), room),
                      contentModel = $.extend({}, new Content(content.ContentID, content.Type, content.Source, content.Content),
                                                                                      { RoomId: roomModel.Id, CreatedDate: content.Created, From: by.UserName, MimeType: content.Encoding || content.MimeType});
            $events.fire($events.app.newContent, [contentModel, userModel, roomModel]);
        } catch (e) {
            reportError(e);
        }
    };

    hub.client.onActivity = function (user, activity) {
        $events.fire($events.app.activity, [new User(user.UserName, user.Role, user.UserID), activity]);
    };

    if (!$.chat) {
        $.chat = {};
    }
    $.chat.hub = hub;

})(jQuery);

(function ($) {
    "use strict";
    $.chat.hub.version = $.signalR.version;
})(jQuery);

