/*!
 * SupportSoft Chat Api.
 *
 * Copyright (C) Aptean. All rights reserved.
 *
 */

(function ($, chat, ss) {
  "use strict";

  if (typeof ($) !== "function") {// no jQuery!
    throw new Error("jquery.chat.api: jQuery not found. Please ensure jQuery is referenced before the jquery.chat.api.js file.");
  }

  if (!JSON) {// no JSON!
    throw new Error("jquery.chat.api: No JSON parser found. Please ensure json2.js is referenced before the jquery.chat.api.js file if you need to support clients without native JSON parsing support, e.g. IE<8.");
  }

  var enableLogging = false,
		apiBaseURL = "/api/chat",
		chatHostVirtualDir = "/sdccommon",
		serverApiURL = window.location.protocol + "//" + window.location.host,
		$events = null;

  function getApiURL(resourcePath) {
    return serverApiURL + apiBaseURL + resourcePath || "";
  };


  function log(message) {
    if (!enableLogging) return;
    $.ss.utility.log('APILogger:' + message);
  };

  var api = function () {
    return new api.fn.init({ hostURL: serverApiURL });
  };

  api.fn = api.prototype = {
    init: function (options) {
      var self = this,
      settings = {
        enableLogging: false,
        hostURL: serverApiURL
      };

      if (typeof options != 'undefined' && $.type(options) === "object") {
        $.extend(settings, options);
      }

      enableLogging = settings.enableLogging;
      serverApiURL = settings.hostURL + chatHostVirtualDir;
      $events = $.chat.events;
      this._ = {
        hub: $.chat.hub,
        hostUrl: settings.hostURL
      };
      return self._.hub.setup({ hostUrl: settings.hostURL + chatHostVirtualDir, logging: enableLogging, version: $.chat.hub.version });
    },
    getApiURL: getApiURL,
    getHost: function () { return this._.hostUrl; },
    logout: function () {
      var def = $.Deferred();
      $.ajax({
        url: getApiURL('/logout'),
        type: 'POST',
        beforeSend: function (request) {
          request.setRequestHeader('Accept', 'application/json; charset=utf-8');
        },
        success: function (msg) { log(msg); }
      }).done(function () { def.resolveWith(null); });
      return def;
    },
    // privacyType==> 0=Open(A room open to all),1=Private(only selected memebers are allowed) ,
    //					2=Protected(user cannot enter w/o first providing credentials, invitation etc)            
    createRoom: function (roomname, userName, privacyType, trackingID, roomId) {
      if (typeof (roomname) !== 'string' || roomname.length == 0
      || typeof (userName) !== 'string' || userName.length == 0) {
        return $.Deferred().reject('Invalid Data').promise();
      }

      var self = this,
        chatHub = self._.hub,
        amIConnected = chatHub.isConnected();

      var session = {
        Name: roomname,
        Privacy: privacyType || 0,
        TrackingID: trackingID || null,
        Id: roomId || null,  //optional param
        UserInfo: {
          UserName: userName
        }
      };
      return $.ajax({
        url: getApiURL('/sessions'),
        type: 'POST',
        data: JSON.stringify(session),
        contentType: "application/json;charset=utf-8",
        beforeSend: function (request) {
          request.setRequestHeader('Accept', 'application/json; charset=utf-8');
          request.setRequestHeader('X-Chat-Connection-Id', chatHub.connectionId)
        },
        success: function (roomInfo) {
          if (!amIConnected) {
            var room = $.extend(false, new Room(roomInfo.Id, roomInfo.Name), roomInfo);
            $events.fire($events.app.createdRoom, [room]);
          }
        },
        error: function (xhr, textStatus, errorThrown) {
          var errmsg = "FAILED to create room[" + roomname + "].Reason[" + xhr.statusText + "]";
          $events.fire($events.app.error, errmsg);
        }
      });
    },

    joinRoom: function (roomID, userName) {
      if (typeof (roomID) !== 'string' || roomID.length == 0) {
        return $.Deferred().reject('Invalid Room').promise();
      }

      var self = this,
			chatHub = self._.hub,
			amIConnected = chatHub.isConnected(),
			userModel = $.extend(false, $.chat.client, { CallerID: chatHub.connectionId, UserName: userName || $.chat.client.UserName });

      if (amIConnected !== true) return $.Deferred().reject('Not connected to chat hub').promise();

      return $.ajax({
        url: getApiURL('/sessions/' + roomID + '/join'),
        processData: false,
        type: 'POST',
        data: JSON.stringify(userModel),
        contentType: "application/json;charset=utf-8",
        beforeSend: function (request) {
          request.setRequestHeader('Accept', 'application/json; charset=utf-8');
          request.setRequestHeader('X-Chat-Connection-Id', chatHub.connectionId);
        },
        error: function (xhr, textStatus, errorThrown) {
          var errmsg = "FAILED : While joining the room [" + roomID + "]-->Error[" + xhr.statusText + "]";
          $events.fire($events.app.error, [errmsg]);
        }
      });
    },

    leaveRoom: function (roomID, userName) {
      if (typeof (roomID) !== 'string' || roomID.length == 0) {
        return $.Deferred().reject('Invalid Room').promise();
      }

      var self = this,
      chatHub = self._.hub,
		  amIConnected = chatHub.isConnected(),
		  userModel = $.extend(false, $.chat.client, { CallerID: chatHub.connectionId, UserName: userName || $.chat.client.UserName });

      return $.ajax({
        url: getApiURL('/sessions/' + roomID + '/leave'),
        type: 'POST',
        data: JSON.stringify(userModel),
        contentType: "application/json;charset=utf-8",
        beforeSend: function (request) {
          request.setRequestHeader('Accept', 'application/json; charset=utf-8');
          request.setRequestHeader('X-Chat-Connection-Id', chatHub.connectionId);
        },
        success: function (roomInfo) {
          if (!amIConnected) {
            var room = $.extend(false, new Room(roomInfo.Id, roomInfo.Name), roomInfo);
            $events.fire($events.app.leftRoom, [userModel, room]);
          }
        },
        error: function (xhr, textStatus, errorThrown) {
          var errmsg = "FAILED : [" + userName + "] while leaving the room [" + roomID + "]-->Error[" + xhr.statusText + "]";
          $events.fire($events.app.error, [errmsg]);
        }
      });

    },

    closeRoom: function (roomID, userName) {
      if (typeof (roomID) !== 'string' || roomID.length == 0) {
        return $.Deferred().reject('Invalid Room').promise();
      }
      var self = this,
		  chatHub = self._.hub,
		  amIConnected = chatHub.isConnected(),
		  userModel = $.extend(false, $.chat.client, { CallerID: chatHub.connectionId, UserName: userName || $.chat.client.UserName });

      return $.ajax({
        url: getApiURL('/sessions/' + roomID + '/close'),
        type: 'POST',
        data: JSON.stringify(userModel),
        contentType: "application/json;charset=utf-8",
        beforeSend: function (request) {
          request.setRequestHeader('Accept', 'application/json; charset=utf-8');
          request.setRequestHeader('X-Chat-Connection-Id', chatHub.connectionId);
        },
        success: function (roomInfo) {
          if (!amIConnected) {
            var room = $.extend(false, new Room(roomInfo.Id, roomInfo.Name), roomInfo);
            $events.fire($events.app.closedRoom, [room, userModel]);
          }
        },
        error: function (xhr, textStatus, errorThrown) {
          var errmsg = "FAILED : [" + userName + "] while closing the room [" + roomID + "]-->Error[" + xhr.statusText + "]";
          $events.fire($events.app.error, [errmsg]);
        }
      });
    },

    getRoomInfo: function (roomID, bAsync) {
      var result = null,
		      asyncMode = (true == bAsync);
      if (typeof (roomID) !== 'string' || roomID.length == 0) {
        if (true != asyncMode) return result;
        return $.Deferred().reject('Invalid room').promise();
      }

      var def = $.ajax({
        url: getApiURL('/sessions/' + roomID),
        type: 'GET',
        async: asyncMode,
        contentType: "application/json;charset=utf-8",
        beforeSend: function (request) {
          request.setRequestHeader('Accept', 'application/json; charset=utf-8');
        }
      })
	    .then(function (roomInfo) {  return ((typeof (roomInfo.Id) != 'undefined') ? $.extend(true, new Room(roomInfo.Id, roomInfo.Name), roomInfo) : null); });
      if (asyncMode) return def;
      def.done(function (roomInfo) {  result = roomInfo; });
      return (result);
    },

    sendMessage: function (fromUser, room, msgText, msgType, toUser) {
      if (typeof (msgText) !== 'string' || msgText.length == 0
		    || typeof (room) !== 'string' || room.length == 0
		    || typeof (msgText) !== 'string' || msgText.length == 0) {
        return $.Deferred().reject('Invalid data').promise();
      }
      var self = this,
      chatHub = self._.hub,
      amIConnected = chatHub.isConnected(),
      apiUrl = getApiURL('/messaging/' + room) + (toUser ? '/' + toUser : '');

      var chatmessage = {
        Id: $.ss.utility.newGuid(),
        from: fromUser || null,
        messageType: msgType || "msg",
        content: msgText,
        to: toUser || null
      };
      return $.ajax({
        url: apiUrl,
        type: 'POST',
        data: JSON.stringify(chatmessage),
        contentType: "application/json;charset=utf-8",
        beforeSend: function (request) {
          request.setRequestHeader('Accept', 'application/json; charset=utf-8');
          request.setRequestHeader('X-Chat-Connection-Id', chatHub.connectionId);
        },
        success: function (message) {
          if (!amIConnected) {
            $events.fire($events.app.newMessage, [message]);
          }
        },
        error: function (xhr, textStatus, errorThrown) {
          var errmsg = "FAILED to send message to room[" + room + "].Error[" + xhr.statusText + "]";
          $events.fire($events.app.error, [errmsg]);
        }
      });
    },

    getMessages: function (roomID, filterOptions, bAsync) {
      if (typeof (roomID) !== 'string' || roomID.length == 0) {
        if (true != bAsync) return null;
        return $.Deferred().reject('Invalid room').promise();
      }

      var apiURL = getApiURL('/messaging/' + roomID),
        filter = { from: null, msgCount: 20, includeAudit: false, dateorder: 'asc', pageIndex: 0 },
        result = null,
        asyncMode = (true == bAsync);

      $.extend(filter, filterOptions);

      if (filter.from) apiURL += '/' + filter.from;
      apiURL += '?messagecount=' + filter.msgCount;
      apiURL += '&audit=' + filter.includeAudit;
      apiURL += '&dateorder=' + filter.dateorder;
      if (filter.pageIndex > 0) apiURL += '&page=' + filter.pageIndex;

      var def = $.ajax({
        url: apiURL,
        type: 'GET',
        async: asyncMode,
        contentType: "application/json;charset=utf-8",
        beforeSend: function (request) {
          request.setRequestHeader('Accept', 'application/json; charset=utf-8');
          request.setRequestHeader('Cache-Control', 'no-cache');
        }
      });
      if (asyncMode) return def;
      def.done(function (messages) {result = messages;});
      return (result);
    },

    getUsers: function (roomID, filterOptions, bAsync) {
      if (typeof (roomID) !== 'string' || roomID.length == 0) {
        if (true != bAsync) return null;
        return $.Deferred().reject('Invalid room').promise();
      }

      var apiURL = getApiURL('/sessions/' + roomID + '/users'),
        filter = { activeUsersOnly: true, includeStealthUsers: false },
        result = [],
        asyncMode = (true == bAsync);


      $.extend(filter, filterOptions);

      apiURL += '?activeonly=' + filter.activeUsersOnly;
      if (filter.includeStealthUsers) apiURL += '?includestealth=' + filter.includeStealthUsers;

      var def = $.ajax({
        url: apiURL,
        type: 'GET',
        async: asyncMode,
        contentType: "application/json;charset=utf-8",
        beforeSend: function (request) {
          request.setRequestHeader('Accept', 'application/json; charset=utf-8');
          request.setRequestHeader('Cache-Control', 'no-cache');
        }
      })
	    .then(function (users) { return $.map(users, function (user) { return new User(user.UserName, user.Role, user.UserID); }); });
      if (asyncMode) return def;
      def.done(function (users) { result = users; });
      return (result);
    },

    sendUserPresence: function (roomID, presenceInfo) {
      if (typeof (roomID) !== 'string' || roomID.length == 0) {
        return $.Deferred().reject('Invalid Room').promise();
      }

      var self = this,
      chatHub = self._.hub,
      amIConnected = chatHub.isConnected(),
      apiUrl = getApiURL('/presence/' + roomID);

      var presence = $.extend({}, {
        id: $.ss.utility.newGuid(),
        from: null,
        //state: "0", //Available
        event: "5",	//Others        
        description: ""
      }, presenceInfo);

      return $.ajax({
        url: apiUrl,
        type: 'POST',
        data: JSON.stringify(presence),
        contentType: "application/json;charset=utf-8",
        beforeSend: function (request) {
          request.setRequestHeader('Accept', 'application/json; charset=utf-8');
          request.setRequestHeader('X-Chat-Connection-Id', chatHub.connectionId);
        },
        error: function (xhr, textStatus, errorThrown) {
          var errmsg = "FAILED to send presence/user activity.Error[" + xhr.statusText + "]";
          $events.fire($events.app.error, [errmsg]);
        }
      });
    },

    postContent: function (roomID, url, from) {
      if (typeof (roomID) !== 'string' || roomID.length == 0) {
        return $.Deferred().reject('Invalid Room').promise();
      }

      var self = this,
      chatHub = self._.hub,
      amIConnected = chatHub.isConnected();

      var contentObj = {
        Id: $.ss.utility.newGuid(),
        From: from || null,
        Url: url
      };
      return $.ajax({
        url: getApiURL('/contents/' + roomID),
        type: 'POST',
        data: JSON.stringify(contentObj),
        contentType: "application/json;charset=utf-8",
        beforeSend: function (request) {
          request.setRequestHeader('Accept', 'application/json; charset=utf-8');
          request.setRequestHeader('X-Chat-Connection-Id', chatHub.connectionId);
        },
        error: function (xhr, textStatus, errorThrown) {
          var errmsg = "FAILED to post content to room[" + roomID + "].Error[" + xhr.statusText + "]";
          $events.fire($events.app.error, [errmsg]);
        }
      });
    }

  };

  api.fn.init.prototype = api.fn;
  if (!$.chat) {
    $.chat = {};
  }
  $.chat.api = api.fn;

})(jQuery);

(function ($, window, chat) {
  $.chat.api.version = "9.3.146.0";
})(window.jQuery, window, window.chat);
