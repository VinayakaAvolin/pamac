var navigatorInfo = navigator.userAgent || navigator.vendor || window.opera;
var isMobile = /android|avantgo|bada\/|blackberry|bb10|playbook|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od|ad)|iris|kindle|lge |maemo|midp|mmp|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino|\bSilk\/(.*\bMobile Safari\b)?/i.test(navigatorInfo) ||
                 /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|e\-|e\/|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(di|rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|xda(\-|2|g)|yas\-|your|zeto|zte\-/i.test(navigatorInfo.substr(0, 4)) ;  

function sil_getCPU()
{
  var cpu = "Unknown";
  if (navigator.userAgent.indexOf("WOW64") != -1 || navigator.userAgent.indexOf("Win64") != -1 ){
    cpu = "x64 - x64 Compatible (Intel, AMD, Cyrix etc.)";
  } else {
    if (typeof(navigator.cpuClass) != "undefined") {
      cpu = navigator.cpuClass;
      switch (cpu) {
        case "x64" : cpu += " - x64 Compatible (Intel, AMD, Cyrix etc.)"; break;
        case "x86" : cpu += " - x86 Compatible (Intel, AMD, Cyrix etc.)"; break;
        case "PPC" : 
        case "68K" : cpu += " - Motorola"; break;
        case "Alpha" : cpu += " - Digital"; break;
        case "Other" : cpu += " - Other, including Sun SPARC";
      }
    }
    //Using navigator.oscpu to get cpu details as navigator.cpuClass is not supported in firefox
    else if(typeof(navigator.oscpu) != "undefined"){
      var OSCPU = navigator.oscpu;
      if(OSCPU.indexOf("WOW64")!= -1)
        cpu = "x64- x64 Compatible (Intel, AMD, Cyrix etc.)";
      else if(OSCPU.indexOf("PPC")!= -1)
        cpu = "Mac OS X (PPC build)";
      else if(OSCPU.indexOf("Intel")!= -1)
        cpu = "Mac OS X (i386/x64 build)";
      else if(OSCPU.indexOf("Windows")!= -1 && OSCPU.indexOf("WOW")== -1)
        cpu = "x86- x86 Compatible (Intel, AMD, Cyrix etc.)";
    }
    else if(navigator.userAgent.indexOf("Windows")!= -1 && navigator.userAgent.indexOf("WOW")== -1 && navigator.userAgent.indexOf("Chrome") != -1)
       cpu = "x86- x86 Compatible (Intel, AMD, Cyrix etc.)";
  }
  return cpu;
}

function sil_getPlat()
{
  var plat = "Unknown";
  if ((navigator.userAgent.indexOf("Android"))>0) {
   var ua = navigator.appVersion;
   plat = (ua.slice(ua.indexOf("Android")+8)).split(';')[0].toString();
    if(plat.match('4.0')) plat += " - Icecream Sandwitch";
    else if (plat.match('4\.[4-9]') > 0) plat += " - KitKat";
    else if (plat.match('4\.[1-3]') > 0) plat += " - JellyBean";
    else if(plat.match('3\.[0-9]')>0) plat += " - HoneyComb";
    else if(plat.match('2.3')>0) plat += " - Gingerbread";
    else plat += " Android";
    
  }
  else if ((navigator.userAgent.match(/(iPhone|iPod|iPad)/)) != null) {
   var ua = navigator.appVersion;
   plat = ((ua.slice(ua.indexOf("OS")+2).split('like')[0]).toString().replace(/_/g,'.'));
  }
  else {
     if (navigator.userAgent.indexOf("WOW64") != -1 || navigator.userAgent.indexOf("Win64") != -1 ){
     plat = "Win64 - Windows 64-bit platform";
     } else {
        plat = navigator.platform;
        if (typeof(navigator.platform) != "undefined") {
          plat = navigator.platform;
          switch (plat) {
          case "Windows" : 
          case "Win64" : plat += " - Windows 64-bit platform"; break;
          case "Win32" : plat += " - Windows 32-bit platform"; break;
          case "Win16" : plat += " - Windows 16-bit platform"; break;
          case "WinCE" : plat += " - Windows CE platform"; break;
          case "MacPPC" : plat += " - Macintosh PowerPC-based platform"; break;
          case "MacIntel" : plat += " - Macintosh Intel-based platform"; break;
          case "Mac68k" : plat += " - Macintosh 68K-based platform"; break;
          case "Linux i686" : plat += " - Linux 32-bit platform"; break;
          case "Linux x86_64" : plat += " - Linux 64-bit platform"; break;
          case "HP-UX" : plat += " - HP UNIX-based platform"; break;
          case "SunOS" : plat += " - Solaris-based platform"; break;
          default : plat += " - Other";
         }
      }
    }
  }
  return plat;
}

function sil_getOS()
{
  var os = "Unknown";
  var userAgent = navigator.userAgent.toLowerCase();
  if (userAgent.match(/(iphone|ipod|ipad)/)) os = "IOS";
  else if (userAgent.indexOf('android') >= 0) os = "Android";
  else if (userAgent.indexOf('mac') >= 0) os = "Mac";
  else if (userAgent.indexOf('linux') >= 0) os = "Linux";
  else if (userAgent.indexOf('x11') >= 0) os = "Unix";
  else if (userAgent.indexOf('win') >= 0 || userAgent.indexOf("16bit") >= 0) {
    var ver = "Unknown";
    if (userAgent.indexOf("win95")!=-1 || userAgent.indexOf("windows 95")!=-1) ver = "Windows 95";
    else if (userAgent.indexOf("win 9x 4.90")!=-1) ver = "Windows ME";
    else if (userAgent.indexOf("win98")!=-1 || userAgent.indexOf("windows 98")!=-1) ver = "Windows 98";
    else if (userAgent.indexOf("windows nt 5.0")!=-1) ver = "Windows 2000";
    else if (userAgent.indexOf("windows nt 5.1")!=-1) ver = "Windows XP";
    else if (userAgent.indexOf("windows nt 5.2")!=-1) ver = "Windows 2003";
    else if (userAgent.indexOf("windows nt 6.0")!=-1) ver = "Windows Vista";
    else if (userAgent.indexOf("windows nt 6.1")!=-1) ver = "Windows 7";
    else if (userAgent.indexOf("windows nt 6.2") != -1) ver = "Windows 8";
    else if (userAgent.indexOf("windows nt 6.3") != -1) ver = "Windows 8.1";
    else if (userAgent.indexOf("windows nt 10.0") != -1) ver = "Windows 10";
    else if (userAgent.indexOf("winnt")!=-1 || userAgent.indexOf("windows nt")!=-1) ver = "Windows NT";
    os = "Windows" + " - " + ver; 
  }
  return os;
}

function sil_getBrowserName()
{
  var uAgent = navigator.userAgent.toLowerCase();
  var browser = "Unknown", version = 0, i = -1;
    
  if(isMobile) {
   if ((i = uAgent.indexOf('chrome'))>=0) browser = "Chrome";
   else if ((i = uAgent.indexOf('opera'))>=0) browser = "Opera";
   else if ((i = uAgent.indexOf('firefox'))>=0) browser = "Firefox";
   else if ((i = uAgent.indexOf('msie'))>=0) browser = "msie";
   else if (uAgent.indexOf('trident') >=0) browser = "msie";
   else if ((i = uAgent.indexOf('konqueror'))>=0) browser = "Konqueror";
   else if ((i = uAgent.indexOf('webtv'))>=0) browser = "WebTV";
   else if ((i = uAgent.indexOf('icab'))>=0) browser = "iCab"
   else if ((i = uAgent.indexOf('omniweb'))>=0) browser = "OmniWeb";
   else if (((i = uAgent.indexOf('safari'))>=0) && (uAgent.match(/(iphone|ipod|ipad)/))) browser = "Safari";
   else browser = "Built-in Browser";
  }
  else { 
   if ((i = uAgent.indexOf('chrome'))>=0) browser = "Chrome";
   else if ((i = uAgent.indexOf('konqueror'))>=0) browser = "Konqueror";
   else if ((i = uAgent.indexOf('safari'))>=0) browser = "Safari";
   else if ((i = uAgent.indexOf('opera'))>=0) browser = "Opera";
   else if ((i = uAgent.indexOf('webtv'))>=0) browser = "WebTV";
   else if ((i = uAgent.indexOf('icab'))>=0) browser = "iCab"
   else if ((i = uAgent.indexOf('msie'))>=0) browser = "msie";
   else if (uAgent.indexOf('trident') >=0) browser = "msie";
   else if ((i = uAgent.indexOf('omniweb'))>=0) browser = "OmniWeb";
   else if ((i = uAgent.indexOf('compatible'))<0) {
    browser = "Netscape Navigator";
    version = uAgent.charAt(8);
    if (typeof(navigator.product) != "undefined") {
      browser = "Mozilla";
      tmp = navigator.userAgent.match(/([Mozilla ]?Fire\w+)\/([\w|\+.]+)/);
      if (tmp) { 
        browser = tmp[1]; version = tmp[2];
      } else {
        tmp = navigator.userAgent.match(/rv:([\w|\+.]+)/);
        if (tmp) {
          tmp = tmp[0]; version = tmp.substr(3);
        }
      }
    }
   }
  }
  
  if(browser == "Chrome") {
    var subStr = uAgent.substring(i,uAgent.length);
    if (i && !version) 
      version = subStr.substring(subStr.indexOf("/")+1,subStr.indexOf(" "));
  }
  else if(browser == "Safari"){
    var subStr = uAgent.substring(uAgent.indexOf("version")+1, uAgent.indexOf('safari'));
    version = subStr.split("/")[1];
  }
  else if (browser == "msie"){
	version = (navigator.appName == 'Microsoft Internet Explorer') ? parseInt(navigator.appVersion.replace(/^.*MSIE /, '')) : parseInt(navigator.appVersion.replace(/.*rv:/, '')); 
	browser = "Internet Explorer";
  }
  else{
    if (i && !version) version = parseInt(uAgent.substr((i + browser.length + 1), 2));
  }
  if (version) browser += " " + version;
  return browser;
}