/* jquery.chat.core.plugin.js */
// Copyright (c) Aptean, Inc. All rights reserved. 
    "use strict";

    if (typeof ($) !== "function") {// no jQuery!
        throw new Error("jquery.chat.core.plugin.js: jQuery not found. Please ensure jQuery is referenced before the jquery.chat.core.plugin.js file.");
    }

    //TODO:this goes under infra/platform
    function localStore(name, duration) {
        //TODO: Fix it to use localStorage for HTML5 browsers
        var options = { path: '/', expires: duration || 30 };
        function canPersist() { return ($.cookie && name != ''); }
        function isStorageSet() {
            if (!canPersist()) return false;
            return ($.cookie(name) !== null && $.cookie(name).length > 0);
        }
        function persist(value, opt) {
            if (!canPersist()) return;
            $.cookie(name, (value) ? JSON.stringify(value) : value, (opt) ? opt : options);
        }
        function safeRetrieve() {
            try { return (JSON.parse($.cookie(name)) || {}); } catch (e) { }
            return {};
        }
        this.set = function (key, value) {
            if (!canPersist()) return;
            var data = safeRetrieve();
            data[key] = value;
            persist(data);
        },
        this.get = function (key) {
            if (!isStorageSet()) return null;
            var data = safeRetrieve();
            return (data.hasOwnProperty(key) ? data[key] : null);
        },
        this.remove = function (key) {
            if (!isStorageSet()) return;
            var data = safeRetrieve();
            if (!data.hasOwnProperty(key)) return;
            delete data[key];
            persist(data);
        }
        this.clear = function () { persist(null, { path: '/' }); }
    };
    if (!window.localStore) window.localStore = localStore;

    var jfn = {
        wait: function (time) { return $.Deferred(function (df) { setTimeout(df.resolve, time); }); }
    };
    $.fn.extend(jfn);
    $.extend(jfn);

  if(typeof $events=='undefined') $events = $.chat.events;
  if(typeof $api=='undefined') $api= $.chat.api;
  
  function parseReturnData(data) {
        if (typeof data === 'object') return data;
        try { return JSON.parse(data.replace(/^.*?\?*\(/, "").replace(/\)$/, "")); } catch (e) { }
        return {};
    };

    function chatcore(opts) {
        var self = this,
            def = {
                core: { urls: { hostURL: window.location.protocol + "//" + window.location.host } },
                preferences: { usesmileys: true, linkifyUrls:true }
            };
        
        this.settings = $.extend(true, def, opts);

        this._ = {
            origin: window.location.host ? (window.location.protocol + "//" + window.location.host):'',
            enableLogging: this.settings.core.enableLogging || false,
            host: this.settings.core.urls.hostURL,
            utilityUrl: this.settings.core.urls.hostURL.concat('/sdccommon/lachat/poll/chatpluginutility.asp'),
            transcriptURL: this.settings.core.urls.hostURL.concat('/sdcxuser/lachat/user/chat_view.asp?room={0}&problem={1}&from=at&user={2}'),
            surveyUrl: this.settings.core.urls.hostURL.concat('/sdccommon/asp/view_survey.asp?sId={0}&source=PAChat&format=JSON&identifier={1}&dataType=plugin'),
            feedbackUrl: this.settings.core.urls.hostURL.concat('/sdcxuser/lachat/user/survey.asp'),
            preferences: $.extend({}, this.settings.preferences),
            customMessages: $.extend({}, this.settings.customMessages),
            storage: new localStore('chat.user')
        };
        this.ticketInfo = { ticket: '', room: '', currentAgent: '', escalatedIn: 0, escalatedOut: 0, qStatus: '', qName: '', qId: '' };

        var logger = $.ss.logger(this._.enableLogging ? 'diwef' : '', self.constructor.name);

        function callbackInvoker(obj, cb) { return function () { cb.apply(obj, $.makeArray(arguments).splice(1, arguments.length)); } };
        function makeEventName(eventName) { return 'chat.pg.event.' + eventName; }
        function fireEvent(eventName) {
            eventName = makeEventName(eventName);
            $events.fire(eventName, $.makeArray(arguments).splice(1, arguments.length));
            //$(self).trigger(eventName,$.makeArray(arguments).splice(1,arguments.length));
        };
        function notify(msgt, msg) { fireEvent("notification", msgt, (msg || getCustomMessage(msgt, msg))); };
        $.each(this.settings.listeners, function (eventName, cb) {
            eventName = makeEventName(eventName);
            $events.on(eventName, cb);
            //$(self).on(eventName, callbackInvoker(null,cb));    
        });

        if (getPref('linkifyUrls')===true) {
            $.chat.ui.registerMsgPreProcessor(function (mObj) { try {mObj.Content = $.ss.utility.linkifyUrls(mObj.Content);} catch (e) {}});
        }

        if (getPref('usesmileys')===true) {
            $.chat.ui.registerMsgPreProcessor(function (mObj) {
                if (getPref('chat_MacroMsg1') && getPref('chat_MacroMsg2')) mObj.Content = escape(MacroMsg2(MacroMsg1(mObj.Content)));
            });
        }

        function MacroMsg1(msg) {
            var re;
            eval(getPref('chat_MacroMsg1') || '');
            return msg;
        }
        function MacroMsg2(msg) {
            var re;
            eval(getPref('chat_MacroMsg2') || '');
            return msg;
        }
        function setPrefs(prefs) { $.extend(self._.preferences, prefs); };
        function getPref(pref) { return (self._.preferences.hasOwnProperty(pref) ? self._.preferences[pref] : null); };
        function getCustomMessage(key, defaultVal) { return (self._.customMessages.hasOwnProperty(key) ? self._.customMessages[key] : defaultVal); }
        function loadAppPreferences() {
            notify('retrievingPreferences', 'Retrieving preferences..');
            return $.when(
                  $.ajax({ url: self._.utilityUrl, type: 'GET', data: { "action": "gatherDomains" }, dataFilter: parseReturnData }),
                    (getPref('usesmileys')) ? $.ajax({ url: self._.utilityUrl, type: 'GET', data: { "action": "getSmileys" }, dataFilter: parseReturnData }) : {}
                  )
                  .done(function (domainList, smileyResult) {
                      if (domainList[1] == "success" && domainList[0].domains) { setPrefs({ allowedDomains: domainList[0].domains.split("||") }); }
                      if (smileyResult[1] == "success") setPrefs({ chat_MacroMsg1: smileyResult[0].chat_MacroMsg1, chat_MacroMsg2: smileyResult[0].chat_MacroMsg2 });
                  })
                .fail(function (xhr, textStatus, errorThrown) {
                    //TODO: sud we firevent(...)?
                    logger.error(errorThrown + 'details:' + xhr.responseText);
                });
        };

        function initEngine() {
            notify('initializing', 'initializing client...');
            return $api.init({ enableLogging: self._.enableLogging, hostURL: self._.host })
                    .done(function () { fireEvent('initialized'); });
        };

        function isDomainAccessAllowed(origin) {
            if(!origin) return true;
            var allowedDomains = getPref('allowedDomains') || [];
            if (allowedDomains.length == 0) return false;
            var rgx = new RegExp('(' + [].join.call(allowedDomains, '|') + ')');
            rgx.ignoreCase = true;
            return rgx.test(origin);
        };

        function startSession(options) {
            self.ticketInfo = $.extend({}, { problem: options.problem, username: options.username, room: options.room, ticket: options.issGuid || options.ticket, qName: options.queue });
            if (!self.ticketInfo.problem || !self.ticketInfo.username) return $.Deferred().reject('Missing details', 'invalidDetails').promise();
            self._.storage.clear();
            $.chat.client.create(self.ticketInfo.username);
            notify('submittingChat', 'Submitting chat...');
            return createSession()
              .then(function (quequeStatus) {
                  notify('submittingChat', 'Submitting chat...');
                  return joinRoom()
                    .then(function (room) {
                        self._.storage.set('room', room.Id);
                        self._.storage.set('uname', self.ticketInfo.username);
                        fireEvent('ticketcreated', self.ticketInfo);
                    });
              })
              .fail(function (xhr, textStatus, errorThrown) {
                  var err = (errorThrown) ? errorThrown : xhr.responseText;
                  fireEvent('ticketcreationfailed', err, self.ticketInfo);
                  logger.error(err);
              });
        };

        function getRoomId() { 
        return (self.ticketInfo.room || self._.storage.get('room')); 
        }
        function getUserName() { return ($.chat.client.UserName || self._.storage.get('uname')); }

        function stopSession(reason, code) {
            var roomId = getRoomId();
            if (!roomId) return $.Deferred().reject('Invalid room', 'invalidRoom').promise();
            var ticket = $.extend({}, self.ticketInfo);
            notify('closingChat', 'Closing chat..');
            return closeRoom()
            .then(function () {
                var useSurvey = getPref('usesurvey') == true,
                  useTranscript = getPref('showtranscript') == true;
                if (!useSurvey && !useTranscript) return $.Deferred().resolve().promise();
                notify('retrievingPreferences', 'Retrieving preferences..');
                return $.when(useSurvey ? getSurveyUrl() : '',
                            useTranscript ? getTranscriptUrl() : ''
                           )
                      .then(function (surl, turl) {
                          $.extend(ticket, self.ticketInfo, { survey: surl, transcript: turl });
                      });
            }).always(function () {
                leaveRoom();
                fireEvent('ticketclosed', ticket, reason, code);
                self._.storage.clear();
            });
        };

        function getSurveyUrl() {
            return $.ajax({ url: self._.utilityUrl, type: 'GET', data: { "action": "getMappedSurvey", "queue": self.ticketInfo.qName }, dataFilter: parseReturnData })
                   .then(function (data) {
                       return ((data.hasOwnProperty('mappedSurvey') && data.mappedSurvey) ? self._.surveyUrl.format(window.encodeURIComponent(data.mappedSurvey), window.encodeURIComponent(getRoomId())) : '');
                   });
        };
        function getTranscriptUrl() {
            var room = getRoomId(), uname = getUserName();
            return ((room && uname) ? self._.transcriptURL.format(window.encodeURIComponent(room), window.encodeURIComponent(self.ticketInfo.problem), window.encodeURIComponent(uname)) : '');
        };

        function createSession() {
            var byWhom = getUserName();
            if (!byWhom) return $.Deferred().reject('Invalid User Name', 'missingUserDetails').promise();
            return $api.createRoom(self.ticketInfo.problem, byWhom, 0, self.ticketInfo.ticket, self.ticketInfo.room)
                      .then(function (createdRoom) {
                          if (!self.ticketInfo.room) self.ticketInfo.room = createdRoom.Id;
                          return $.chat.queueapi.setQueueForRoom(self.ticketInfo.room, self.ticketInfo.qName);
                      });

        };

        function joinRoom() {
            var roomId = getRoomId(),
              asAlias = getUserName();
            if (!roomId) return $.Deferred().reject('Invalid Room', 'invalidRoom').promise();
            if (!asAlias) return $.Deferred().reject('Invalid User Name', 'missingUserDetails').promise();
            return $api.joinRoom(roomId, asAlias)
              .then(function (room) {
                  return $.Deferred(function (d) {
                      var roomObj = $.extend(true, new Room(room.Id, room.Description), room);
                      d.resolve(roomObj);
                  }).promise();
              });
        };

        function closeRoom() {
            if (self.ticketInfo.closed === true) {	//should we check against server instead?
                logger.warn("room already closed");
                return $.Deferred().resolve().promise();
            }
            var roomId = getRoomId();
            if (!roomId) return $.Deferred().reject('Invalid Room', 'invalidRoom').promise();
            //REVIEW: do we need to chain-up $.chat.queueapi.setQueueStatusForAnalyst({op: "setClosed", params: { pRoom: getRoomId()}}),
            return $api.closeRoom(roomId)
              .done(function () {
                  $.extend(self.ticketInfo, { closed: true });
              });
        }

        function leaveRoom() {
            var roomId = getRoomId();
            if (!roomId) return $.Deferred().reject('Invalid Room', 'invalidRoom').promise();
            return $api.leaveRoom(roomId);
        };

        function getUsers(filterOpts, async) {
            var opt = { room: getRoomId(), activeUsersOnly: true };
            opt = $.extend(opt, filterOpts);
            return $api.getUsers(opt.room, opt, (async == true));
        };

        function isUserInRoom(uname, room, async) {
            try {
                var opts = { room: getRoomId(), activeUsersOnly: true };
                if (room) opts.room = room;
                uname = uname.toLowerCase();
                var results = getUsers(opts, async);
                //TODO: The foll ('grep' logic) needs to be fixed   assuming we do end up with more than one participants with the same name 
                function findMatch(users, username) {
                    return ($.grep(users, function (u, index) { return (u.UserName.toLowerCase() == username); }).length > 0);
                }
                if (async) return results.then(function (users) { return findMatch(users, uname); });
                else return findMatch(results, uname);
            } catch (e) { }
            if (async === true) return $.Deferred.reject(false).promise()
            return false;
        };

        function getMessages(room, options) {
            var roomId = (!room) ? getRoomId() : room;
            var opt = { msgCount: 200, dateorder: 'desc' };
            if(typeof options =='object') opt = $.extend(opt, {msgCount:options.msgCount, includeAudit: options.includeAudit});
            return $api.getMessages(roomId, opt, true);
        };

        //Queue handling

        function resetQueueObj() {
            function resetCache(qInfo) { if (qInfo.hasOwnProperty('qId')) $.extend(self.ticketInfo, qInfo); }
            return $.chat.queueapi.getCurrentQueueState(getRoomId(), true)
                    .then(function (qInfo) {
                        resetCache(qInfo);
                        return $.extend({}, self.ticketInfo);
                    }, function () { return null });
        }

        function getQStatus() { return (self.ticketInfo.qStatus); };
        function getQName() { return (self.ticketInfo.qName); };
        function getQId() { return (self.ticketInfo.qId); };

        function getServingAgent() { return (self.ticketInfo.currentAgent || ''); };
        function getServingAgentObj() {
            var agentName = getServingAgent().toLowerCase(),
                roomId = getRoomId();
            if (agentName.length == 0) return $.Deferred().resolve(null).promise();

            return $api.getUsers(roomId, null, true)
                  .then(function (currentUsers) {
                      var result = $.grep(currentUsers, function (user, index) { return (user.UserName.toLowerCase() == agentName && $.isAgent(user)); });
                      return (result.length > 0) ? result[0] : null;
                  },
                        function () { return null; });
        };
        function isQEscalated() { return (self.ticketInfo.escalatedIn == 1 && getQStatus() === 'waiting'); };
        function isBeingServedByAgent(agentName) {
            if (getQStatus() !== 'working' || isQEscalated()) resetQueueObj();
            return (getQStatus() === 'working' && (agentName ? getServingAgent().toLowerCase() === agentName.toLowerCase() : getServingAgent() !== ''));
        }

        var didAgentAbandonChat = false;
        function OnAgentJoinedEvent(agentObj, roomObj) {
            //TODO: Review for performance
            resetQueueObj()
              .done(function () {
                  if (!isBeingServedByAgent(agentObj.UserName)) return; // nah! not my serving agent..not interested bail out...
                  didAgentAbandonChat = false;
                  if (agentAbandonedChatTimer) window.clearTimeout(agentAbandonedChatTimer);
                  $events.fire($events.app.agentAssigned, [roomObj, agentObj]);
              });
        };

        var agentAbandonedChatTimer = null;
        function OnAgentLeaveEvent(agentObj, roomObj) {
            //TODO: Review for performance
            resetQueueObj().then(function () {
                var servingAgentLeft = isBeingServedByAgent(agentObj.UserName);
                if (!servingAgentLeft) return;
                didAgentAbandonChat = true;         // it's a possible abandonment case        

                logger.debug("possibly my agent has abandoned me");

                window.clearTimeout(agentAbandonedChatTimer);
                // For now give agent sometime to reconnect back
                agentAbandonedChatTimer = setTimeout(function () {
                    resetQueueObj().then(function () {                   //refresh/reload.          
                        getServingAgentObj().then(function (servingAgent) {
                            if (servingAgent != null || isQEscalated()) {//found my agent back or being escalated or waiting to be picked                      
                                logger.info("possibly found my agent back or already being escalated or waiting to be picked, don't escalate");
                                didAgentAbandonChat = false;
                                window.clearTimeout(agentAbandonedChatTimer);
                                return;
                            }
                            var strEscalateMsg = getCustomMessage('escalateToAnotherAnalyst', 'Analyst has left the room. Your problem is being escalated to another analyst');
                            $.chat.queueapi.escalateToAnotherAnalyst(roomObj.Id, getQName(), strEscalateMsg, "", false).done(function () {
                                notify('escalateToAnotherAnalyst', strEscalateMsg);
                            });
                        });
                    });

                }, ((getPref('autoEscalateIn') || 30) * 1000));
            });

        };

        var retry = -1;
        $events.on($events.app.connectionStateChange, function (state) {
            if (!getRoomId()) return;
            if (/^disconnected/i.test(state))++retry;
            else if (/^connected/i.test(state) && retry > 0) joinRoom().done(function () { retry = 0; logger.debug('You have joined back'); });
        });

        $events.on($events.app.joinedRoom, function (whoJoined, roominfo) {
            if ($.isAgent(whoJoined)) OnAgentJoinedEvent(whoJoined, roominfo);
            else if ($.isMe(whoJoined)) logger.debug('you have joined');
        });
        $events.on($events.app.leftRoom, function (whoLeft, roominfo) {
            if (whoLeft.IsAgent()) OnAgentLeaveEvent(whoLeft, roominfo);
            else if ($.isMe(whoLeft)) { }// placeholder
        });
        $events.on($events.app.closedRoom, function (roominfo, byWhom, reason) {
            $.extend(self.ticketInfo, { closed: true });
            if ($.isMe(byWhom)) return;
            if ($.isAgent(byWhom) || $.isSystem(byWhom)) {
                var code;
                if (/^Chat closed for urgent system maintenance/i.test(reason)) code = 'emergencyClose';
                else if ($.isAgent(byWhom)) code = 'analystHasClosedChat';
                stopSession(reason, code);
            }
        });
        $events.on($events.app.ownedRoom, function (roominfo, byWhom) {
            resetQueueObj().done(function () {
                $events.fire($events.app.agentAssigned, [roominfo, byWhom]);
            });
        });

        $events.on($events.app.agentAssigned, function (roomObj, byWhom) {
        });

        function isReconnectAllowed() { return (getPref('allowreconnect')) == true; }
        function tryReconnecting() {
            var deferred = $.Deferred();
            var roomId = getRoomId(),
                me = getUserName();
            if (!roomId || !me) return $.Deferred().reject('Cannot reconnect', 'invalidReconnectDetails').promise();

            notify('reconnecting', 'Attempting to reconnect to your old session...');
            return $api.getRoomInfo(roomId, true)
            .then(function (roomObj) {
                if (roomObj == null || !roomObj.Active) return $.Deferred().reject('Invalid room or room is closed', 'invalidRoom').promise();
                return getUsers({ room: roomId, activeUsersOnly: false }, true)
                      .then(function (users) {
                          //TODO: The foll ('grep' logic) needs to be fixed   assuming we do end up with more than one participants with the same name
                          var Iam = $.grep(users, function (user, index) { return (user.UserName.toLowerCase() == me.toLowerCase()); });
                          if (Iam.length == 0) return $.Deferred().reject('You were not part of the room', 'notPartOfRoom').promise();
                          Iam = Iam[0];        //TODO: fix as part of fixing the 'grep' logic above
                          $.chat.client.create(Iam.UserName, Iam.Role, Iam.Id);
                          return joinRoom()
                            .then(function (room) {
                                self.ticketInfo.room = room.Id;
                                self.ticketInfo.ticket = room.TrackingID;
                                self._.storage.set('room', room.Id);
                                $.chat.client.setCurrentRoom(room);
                                fireEvent('reconnected', room);
                            }).done(function () {
                                resetQueueObj(); //find if he was being served already                                                      
                            });
                      });
            })
            .fail(function (reason, code) {
                fireEvent('reconnectfailed', reason, code || 'notPartOfRoom');
                //TODO: review if we need to clear his details.
                if (code && /(notPartOfRoom|invalidRoom|accessDenied|missingUserDetails)/i.test(code)) {
                    logger.warn('re-setting localstore since we failed to connect to original room');
                    self._.storage.clear();
                }
            });
        };

        function submitFeedback(opt) {
            var feedbackOpt = {
                room: getRoomId(),
                reconnect: (getServingAgent().length > 0),   //NB: assumtption is that he is served atleast once by an agent
                cmd: 'submit', fmt: 'json', from: 'at', bMobileChat: $.ss.utility.isMobile(), issueresolved: '', rating: ''
            };

            $.extend(feedbackOpt, opt || {});
            feedbackOpt.helpful = feedbackOpt.rating;

            return $.ajax({ url: self._.feedbackUrl, type: 'GET', data: feedbackOpt, dataFilter: parseReturnData })
                .done(function (data) { $events.fire($events.app.surveySubmitSuccess, [feedbackOpt]); })
                .fail(function (xhr, ts, et) { logger.error(et); });
        };

        function onExit() { 
            $api.logout();
            if (!isReconnectAllowed()) self._.storage.clear();
        };

        if (window.addEventListener) {window.addEventListener('unload', function () { onExit() });}
        else if (window.attachEvent) {; window.attachEvent('onunload', function () { onExit() });}
        else { window.onunload = onExit;}//last resort (TODO: support older handlers attached to it)

        loadAppPreferences()
              .then(function () {
                  if (!isDomainAccessAllowed(self._.origin)) {
                      var msg = "Not Allowed to Access";
                      //NB: added delay just in case caller is not ready for receiving the event yet
                      $.wait(50)
                            .then(function () {
                                logger.error(msg);
                                self._.storage.clear();
                                fireEvent('accessdenied', msg, 'accessDenied');
                            });
                      return $.Deferred().reject(msg, 'accessDenied').promise();
                  }
              })
              .then(function () {
                  return initEngine();
              });
        return {
            startChat: startSession,
            closeChat: stopSession,
            canReconnect: function(){
              if (!isReconnectAllowed()) return false;
              return(getRoomId() && self.ticketInfo.closed !== true);
            },
            reconnect: function () {
                if (!isReconnectAllowed()) return $.Deferred().reject('Reconnect not supported', 'reconnectNotSupported').promise();               
                return tryReconnecting();
            },
            getMessages: getMessages,
            getRoomId: getRoomId,
            submitFeedback: submitFeedback
        };
    }
    if (!window.chat) window.chat = {};
    window.chat.core = chatcore
    
  if(typeof ui=='undefined') ui= $.chat.ui;
  
    function parseReturnData(data) {
        if (typeof data === 'object') return data;
        try { return JSON.parse(data.replace(/^.*?\?*\(/, "").replace(/\)$/, "")); } catch (e) { }
        return {};
    };

    function chatPlugin(container, options) {
        var self = this,
        listeners = {
            initialized: function () {
                self.setup();
                if (self.canReconnect()) self.core.reconnect();
            },
            accessdenied: function () { $(self).trigger('accessdenied', arguments); },
            ticketcreationfailed: function () { $(self).trigger('ticketcreationfailed', arguments); },
            ticketcreated: function () { $(self).trigger('ticketcreated', arguments); },
            ticketclosed: function () { $(self).trigger('ticketclosed', arguments); },
            reconnected: function () {
                var roomId = self.core.getRoomId(),
                fArgs = arguments;
                $(self).trigger('notification', ['retrievingMessages', 'Retreiving your old messages..']);
                self.core.getMessages(roomId, self.preferences)
                .done(function (messages) { if (messages && $.isArray(messages)) { $.each(messages, function (i, msg) {  $.chat.ui.processMessage(msg, msg.From, roomId); }); } })
                .fail(function () {  $(self).trigger('notification', ['retrievingMessagesFailed', 'Failed to retreive your old messages.']); })
                .always(function () {  $(self).trigger('reconnected', fArgs); });
            },
            reconnectfailed: function () { $(self).trigger('reconnectfailed', arguments); },
            notification: function () { $(self).trigger('notification', arguments); }
        };
        this.$container = typeof container === 'string' ? $(container) : container;
        var coreOpts = {
            core: options.core || {},
            listeners: listeners,
            preferences: options.preferences || {},
            customMessages: options.customMessages || {}
        },
            pluginOpts = {
                ui: options.ui || {},
                preferences: options.preferences || {},
                customMessages: options.customMessages || {}
            };
        $.extend(true, this, {}, pluginOpts);
        this.canReconnect=function() { return (self.core.canReconnect()); };

        this.show = function () { self.$container.show() };
        this.hide = function () { self.$container.hide() };
        this.setup = function () { $.chat.ui.setup(options.ui) }
        this.createTicket = function (options) {
            var ctx = $.extend({}, options);
            var sPrefLangCode = $ss.agentcore.utils.GetLanguage();
            var sLangCode = $ss.agentcore.dal.config.GetConfigValue("languagemapping", sPrefLangCode, "en-US");           
            var sLangString = $ss.agentcore.dal.config.GetConfigValue("language_list", "cl_ls_lang_code_" + _config.GetUserValue("global", "language"));
            if (!ctx.problem || !ctx.username || !ctx.emailAddress) {
                $(self).trigger('ticketcreationfailed', 'Missing details');
                return $.Deferred().reject('Missing details', 'invalidDetails').promise();
            }
            $(self).trigger('notification', ['creatingticket', 'Creating ticket...']);
            var data = {
                "format": "json", "Kernel::Kernel::sik_iss_type": "8AC68A4A-20A8-4ED9-A26B-0F58DE3A02D3", "channel_option": "lachat",
                "username": ctx.username, "emailAddress": ctx.emailAddress, "Kernel::Kernel::sik_short_desc": ctx.problem, "UserPrefLangId":sLangCode, "bMobileChat": $.ss.utility.isMobile(), "device": sil_getOS(),"99e6d1e1-28cb-456b-a17f-30890e7a8c0b::99e6d1e1-28cb-456b-a17f-30890e7a8c0b::b1b96c63-1eb6-4ee0-9f96-18ce5e619d5a": sLangCode, '99e6d1e1-28cb-456b-a17f-30890e7a8c0b::99e6d1e1-28cb-456b-a17f-30890e7a8c0b::1ffca9f3-d715-4e43-bd42-b7346d80072a' : sLangString
            },
            serviceUrl = (coreOpts.core.urls.hasOwnProperty('hostURL') ? coreOpts.core.urls.hostURL : window.location.protocol + "//" + window.location.host) + '/sdcxuser/rrn/issue_new_submit.asp?callback=?';

            if (ctx.includeSI) {
                var SIClassPropertyGuid = {
                    platform: "C4F8EA09-B9BB-4066-8F26-2A40CF9B6682::C4F8EA09-B9BB-4066-8F26-2A40CF9B6682::49966353-6FBF-4CFD-A2FA-9D84492C55A8",
                    browser: "C4F8EA09-B9BB-4066-8F26-2A40CF9B6682::C4F8EA09-B9BB-4066-8F26-2A40CF9B6682::4EB5429C-8F90-447D-9A27-06918C90D7DC",
                    CPU: "C4F8EA09-B9BB-4066-8F26-2A40CF9B6682::C4F8EA09-B9BB-4066-8F26-2A40CF9B6682::9CAF158D-A97C-4CD8-BACE-29AF22C1872E",
                    OS: "C4F8EA09-B9BB-4066-8F26-2A40CF9B6682::C4F8EA09-B9BB-4066-8F26-2A40CF9B6682::B79BB94D-AC76-4E22-A644-98154B228EA6"
                };

                var params = [SIClassPropertyGuid.platform + "=" + sil_getPlat(),
                              SIClassPropertyGuid.browser + "=" + sil_getBrowserName(),
                              SIClassPropertyGuid.CPU + "=" + sil_getCPU(),
                              SIClassPropertyGuid.OS + "=" + sil_getOS()]
                serviceUrl = serviceUrl.concat('&' + params.join('&'));
            }
			
			var $def =  jQuery.post(serviceUrl, data, "json");
			return $def.then(function (result) {
                      var tmp = parseReturnData(result);
                      if (!tmp.room || !tmp.issGuid)  return $.Deferred().rejectWith($def,[$def, $def.statusText, tmp.reason]).promise();					
                      return $.extend({}, tmp, ctx);
                  });			
        };
        this.closeTicket = function (options) {
            return $.Deferred().resolve(options).promise(); //SS OOTB : doesn't need to explicitly close ticket
        };

        this.getCustomMessage = function (key, defaultVal) {
            var msgs = self.customMessages;
            if (!msgs.hasOwnProperty(key)) return defaultVal;
            return msgs[key];
        };
        this.setup();
        this.core = new chatcore(coreOpts);
        return self;
    };

    $.fn.bindFor = function (event, callback, once) {
        var element = $(this);
        if (!once) {
            function invokeCallback() {
                callback.apply(this, [].slice.call(arguments, 1, arguments.length));
            }
            element.on(event, invokeCallback);
        } else {
            defer = element.data("cw_event_" + event);
            if (!defer) {
                defer = $.Deferred();
                function deferCallback() {
                    element.unbind(event, deferCallback);
                    defer.resolveWith(this, [].slice.call(arguments, 1, arguments.length));
                }
                element.bind(event, deferCallback)
                element.data("cw_event_" + event, defer);
            }
            return defer.done(callback).promise();
        }
    };
    var jfn = {
        chatPlugin: function (options) {
            $(this).each(function () {
                if ($(this).data('chat-plugin')) return;
                var plugin = new chatPlugin(this, options);
                $(this).data('chat-plugin', plugin);
            });
            return this;
        }
    };
    $.fn.extend(jfn);
    $.extend({
        chatPlugin: function ($container, options) { return new chatPlugin($container, options); }
    });

    if (typeof ui.addMessageToUI === 'undefined') {
        $.extend(ui, {
            addMessageToUI: function (msg, msgtype, msgfrom, id) {
                if (typeof ($.chat.ui.processMessage) !== 'function') return;
                var msg_from = (msgfrom) || /^advlog_(an|us|an_us|none)/i.test(msgtype) ? "advanced_tool_log" : "system";
                var msgObj = { Id: id || $.ss.utility.newGuid(), From: msg_from, MessageType: msgtype, Content: msg, Created: ((new Date())) };
                $.chat.ui.processMessage(msgObj, msg_from, $.chat.client.CurrentRoom);  //TODO; we sudnt pass $.chat.client.CurrentRoom but use plugin stuff
            }
        });
    }
