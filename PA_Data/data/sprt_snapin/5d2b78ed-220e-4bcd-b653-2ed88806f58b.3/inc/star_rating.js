//*********************************************************************
//**
//**  File Name: star_rating.js
//**
//**  Summary: 
//**
//**  Description: 
//**
//**  Copyright (C) 1999-2015 Aptean. All Rights Reserved.
//**
//*********************************************************************

(function ($, window, defaults) {
    "use strict"
    var RANGE = 5,
    FULL_WIDTH = 156,
     HEIGHT = 30,
     WIDTH = 30,
     GAP = 1,
     altText = "testing",
     RATEBAR_URL = "/sdccommon/images/ratebar";

    //fix the host(if passed)
    if (defaults.hostUrl) RATEBAR_URL = defaults.hostUrl + RATEBAR_URL;

    var RATEBAR00_GIF = RATEBAR_URL + "00.gif";
    var imgAttrs = " src=" + RATEBAR00_GIF + " alt=" + altText + " width=" + FULL_WIDTH + " height=" + HEIGHT + " border=0";

    var ratingVal = new Array();
    ratingVal[1] = _utils.LocalXLate("snapin_chat", "survey_rate_1");
    ratingVal[2] = _utils.LocalXLate("snapin_chat", "survey_rate_2");
    ratingVal[3] = _utils.LocalXLate("snapin_chat", "survey_rate_3");
    ratingVal[4] = _utils.LocalXLate("snapin_chat", "survey_rate_4");
    ratingVal[5] = _utils.LocalXLate("snapin_chat", "survey_rate_5");

    var jfn = {
        starRater: function (options) {
            var $this = $(this),
              rater = $this.data('ss-ratings');
            if (rater && typeof options === 'string') {	//trying to invoke method of plugin
                if (rater[options]) {
                    return rater[options].apply(this, Array.prototype.slice.call(arguments, 1));
                }
            }
                //else if (!rater && (typeof options === 'object' || !options)) {	//intializing plugin
            else {
                return $this.each(function () {
                    rater = new starRater(this, options);
                    $this.data('ss-ratings', rater);
                });
            }
            return $this;
        }
    };

    $.fn.extend(jfn);

    function starRater(container, options) {
        var self = this,
        $container = $(container);
        if (!$container || $container.length == 0) return;
        init($container);
        var $rating = $container.find('img[usemap]').eq(0);
        $rating.attr('src', RATEBAR00_GIF);
        $rating.data('rating', '');
        this.ratings = function (rating) {
            var $rating = $container.find('img[usemap]').eq(0);
            if (!rating) {
                rating = $rating.data('rating');
                return (rating > 0 ? ratingVal[rating] : '');
            }
            var selected = $container.find('area[title="' + rating + '"]').eq(0).attr('href');
            if (!selected || !ratingVal[selected]) return;
            $rating.attr('src', RATEBAR_URL + selected + '0.gif');
            $rating.data('rating', selected);
        }
        this.reset = function () {
            var $rating = $container.find('img[usemap]').eq(0);
            $rating.attr('src', RATEBAR00_GIF);
            $rating.data('rating', '');
        }

        //private methods	
        function init($container) {
            if ($container.find('img[usemap]').length > 0) return;
            if ($container.find('area').length > 0) return;
            var mapn = $container.selector.replace('#', '') + 'ratingMap';

            var $rating = $('<img ' + imgAttrs + ' name="ratingImg" usemap="#' + mapn + '"/>'),
            $map = $('<map name="' + mapn + '">');
            var i = 0;
            for (i = 1; i < RANGE + 1; i++) {
                var top = 0,
                bottom = HEIGHT - 1,
                left = (i - 1) * (WIDTH + GAP),
                right;

                right = (left + WIDTH + GAP < FULL_WIDTH) ? (left + WIDTH + GAP) : FULL_WIDTH;
                $map.append($('<area href="' + i + '" title="' + ratingVal[i] + '" alt="' + ratingVal[i] + '" shape="rect" coords="' + left + ',' + top + ',' + right + ',' + bottom + '">'));
            }
            $map.find('area').each(function () {
                var $area = $(this);
                registerListeners($area, $rating);
            });
            $rating.each(function () {
                if (typeof ($(this).attr('usemap')) == 'undefined') return;
                var $that = $(this);
                $that.load(function () {//reserved for future to do additional handling
                }).attr('src', $that.attr('src'));
            });
            $container.append($rating);
            $container.append($map);
        }

        function registerListeners($area, $rating) {
            $area.on('click', function (e) {
                e.preventDefault();
                var currRating = $rating.data('rating'),
                index = $(this).attr('href');
                if (currRating == index) {
                    self.reset();
                } else {
                    $rating.attr('src', RATEBAR_URL + index + '0.gif');
                    $rating.data('rating', index);
                }
            });

            $area.on('mouseover mouseout', function (e) {
                var rating = $rating.data('rating') || 0;
                if (rating > 0) return;
                var index = $(this).attr('href');
                if (e.type == 'mouseout') $rating.attr('src', RATEBAR00_GIF);
                else $rating.attr('src', RATEBAR_URL + index + '0.gif');
            });

        }
    }
})(window.jQuery, window,
{ hostUrl: NavigationController.GetServerUrl() }
);
