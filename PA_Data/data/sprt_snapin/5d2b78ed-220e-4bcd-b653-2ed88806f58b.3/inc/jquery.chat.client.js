function User(name, role, Id) {
  return new User.fn.create(name, role, Id);
};

User.fn = User.prototype = {  
  CurrentRoom: null,
  Rooms: [],
  Id : '',
  UserName :'',
  Role : 0,
  Presencestate : null,
  create: function (name, role, id) {  
    this.Id = id || '287B6105-A437-4426-8FDD-F5F63403108E';	//guest Id as recognized by SS
    this.UserName = name;
    this.Role = role || 0;
    this.Presencestate = null;
  },
  addRoom: function (room) {
	if(!this.isPartOfRoom(room)){
      this.Rooms.push(room);
    }    
  },
  removeRoom: function (room) {
    var len = this.Rooms.length,
    current = this.CurrentRoom;
    for (var i = 0; i < len; i++) {
      if (this.Rooms[i].equals(room)) {
        if (this.Rooms[i].equals(current)) this.CurrentRoom = null;        
        this.Rooms.splice(i, 1);
        return;
      }
    }
  },
  setCurrentRoom: function (room) {
    this.addRoom(room);
	  if(!this.CurrentRoom || !this.CurrentRoom.equals(room)) this.CurrentRoom = room;	
  },
  isPartOfRoom : function(r){
   var len = this.Rooms.length;
    while (len--) {
      if (this.Rooms[len].equals(r)) return true;      
    }
    return false;
  }
};

User.fn.create.prototype = User.fn;

User.prototype.equals = function (userObj) {
  return (userObj && userObj.hasOwnProperty('Id') && this.Id.toLowerCase() === userObj.Id.toLowerCase());
};

User.prototype.IsAgent = function () {
  return (this.Role === 1 || this.Role === 2 || this.Role ===3);	// 0=> guest, 1-3 are suppRep,Supervisor & admin resp		
};

User.prototype.IsSupervisor = function () { return (this.Role === 2);};

User.prototype.IsAdmin = function () { return (this.Role === 3);};

User.prototype.IsSystem = function () {
  return (this.UserName.toLowerCase() === 'system');
};

function Room(id, description, owner) {
  this.Id = id;
  this.Name = description || '';
  this.Owner = owner || null;
  this.Users = [];
  this.Active = false;   
  this.isUserInRoom = function(user){
    var len = this.Users.length;
    while (len--) {
      if (this.Users[len].equals(user)) return true;      
    }
    return false;
  }  
};

Room.prototype.addUser = function (user) {
  if (!this.isUserInRoom(user)) {
	if (!(user instanceof User))    user = new User(user.UserName, user.Role, user.Id);
    this.Users.push(user);
  }
};

Room.prototype.removeUser = function (user) {
  var len = this.Users.length;
  for (var i = 0; i < len; i++) {
    if (this.Users[i].equals(user)) {
      this.Users.splice(i, 1);
      return;
    }
  }
};

Room.prototype.equals = function (roomObj) {
  return (roomObj && roomObj.hasOwnProperty('Id') && this.Id.toLowerCase() === roomObj.Id.toLowerCase());
};

function Content(id, type, source, content, title, dated) {
  this.Id = id || '';
  this.Type = type || '';
  this.Source = source || '';    
  this.Content = content || '';  
  this.CreatedDate = dated || new Date();
  this.Title= title || '';
  this.From = null;
  this.RoomId = null;
  this.MimeType = null;
}

(function ($, chat) {
  "use strict";

  if (typeof ($) !== "function") {// no jQuery!
    throw new Error("jquery.chat.client: jQuery not found. Please ensure jQuery is referenced before the jquery.chat.client.js file.");
  }

  if (!$.chat) {
    $.chat = {};
  }

  $.chat.client = User.fn;

  $.isMe = function (userObj) {
    if (typeof userObj === 'undefined' || !userObj || !$.chat.client) return false;	
    if (typeof userObj === 'object') {
      var isMe = false;    
	  if (userObj instanceof User) isMe = (userObj.equals($.chat.client));
      if (!isMe && userObj.hasOwnProperty('UserName')) 
        isMe = ((userObj.UserName ? userObj.UserName.toLowerCase() : userObj.UserName) === ($.chat.client.UserName ? $.chat.client.UserName.toLowerCase() : $.chat.client.UserName));
      return isMe;
    }else if (typeof userObj === 'string'){
		  return  (userObj.toLowerCase() === $.chat.client.UserName.toLowerCase());
	  }
    return false;
  };

  $.isAgent = function (userObj) {
    if (typeof userObj === 'object') {
      if (userObj instanceof User) return (userObj.IsAgent());
      if (Object.prototype.hasOwnProperty.call(userObj, 'Role')) return (userObj.Role === 1 || userObj.Role === 2 || userObj.Role === 3);
    }
    return false;
  };

  $.isSystem = function (userObj) {
    if ( typeof userObj === 'undefined' || !userObj) return false;
    if (typeof userObj === 'object'
		&& Object.prototype.hasOwnProperty.call(userObj, 'UserName')) {
      return (userObj.UserName.toLowerCase() === 'system');
    } else if (typeof userObj === 'string') {
      return (userObj.toLowerCase() === 'system');
    }
    return false;
  };

})(jQuery);