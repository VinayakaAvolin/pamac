SnapinChatController = SnapinBaseController.extend('snapin_chat', {
    exception: $ss.agentcore.exceptions,
    notificationContainer: null,
    notify:  function(msg){
      $(this.notificationContainer).trigger(msg);
    },
    init: function () {
      var clsPtr = this;
      if(chatPluginInstance){
        var $events= $.chat.events;
        $events.on($events.ui.newContent, function (newcontents) {
          clsPtr.notify('notify_new_msg');
        });
        $events.on($events.app.newMessage,function(message, from, room){
          if($.isMe(from)) return;
          clsPtr.notify('notify_new_msg');
        });                 
        $(chatPluginInstance).bindFor('notification', function (msgtype, msg) {
          
        });
        $(chatPluginInstance).bindFor('reconnected', function (room) {
          clsPtr.notify("online");
        });
        $(chatPluginInstance).bindFor('reconnectfailed', function (reason, code) {
          clsPtr.notify("offline");
        });

        $(chatPluginInstance).bindFor('ticketcreated', function () {
          clsPtr.notify("online");
        });

        $(chatPluginInstance).bindFor('ticketclosed', function (ticket, reason, code) {
          clsPtr.notify("offline");
        });
        
      }
    }
    }, {
    showChatPage: function (params) {
      var that = this;
      if(!this.Class.notificationContainer){
        this.Class.notificationContainer = params.chatContainer;
      } 
      if(!chatPluginInstance){
        return;
      }     
      that.renderSnapinView("snapin_chat", params.toLocation, "\\views\\chat.html");    
        chatPluginInstance.initializeChat();
    },
    hideChatPage: function (params) {
      if(chatPluginInstance){
        chatPluginInstance.initializeChat(/true/i.test(params.activeOnChat));               
      }
    },
    
    reconnMe: function (params) {
    var that = this;
     if(!this.Class.notificationContainer){
        this.Class.notificationContainer = params.chatContainer;
      }
    if(chatPluginInstance){
        if(chatPluginInstance.canReconnect()){
          this.Class.notify("online");          
      }     
    }
    },
    chatWidget: function(params){
       this.renderSnapinView("snapin_chat", params.toLocation, "\\views\\chatWidget.html");
      }
    }
);

