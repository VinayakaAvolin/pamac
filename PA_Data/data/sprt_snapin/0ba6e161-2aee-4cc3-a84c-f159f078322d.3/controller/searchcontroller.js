$ss.snapins = $ss.snapins || {};
$ss.snapins.search = $ss.snapins.search|| {};
$ss.snapins.search.summary = $ss.snapins.search.summary || {};

var filterresult;
var hostUrl = "";
var Gridresult;

SnapinSearchController = SnapinBaseController.extend('snapin_search', {
    searchData: null,
    controller: null, 
    currentLang: "",
    searchString: null,
    init: function () {
      controller = this;
      this.Class.currentLang =$ss.agentcore.utils.GetLanguage();
    },

    initSearch: function (params) {
      
      var that = this;
      $('.back2search').hide();
  
      $("#search-btn").click(function(e){
        var p = params || {};
        delete p.backToSearch;
        delete p.SearchResult;        
        delete that.Class.searchString;
        
        var $searchTxtEl = $("#search-text").first();
        if($searchTxtEl.length!=1) return;
        var searchQry = $searchTxtEl.val().replace(/\s+/g, ' ').trim();
        if (searchQry === "") return;
        that.Class.searchString = searchQry;
        
        p.requesturl = "/search";
        p.action = ".navigate click";      
        that.index(p);
        e.preventDefault();

        $(".showClearIcon").click(function(){
            $("#search-text")[0].value = "";
            $("#search-text").focus();
            $("#idClearIcon").removeClass("hideClearIcon").addClass('showClearIcon');
            var p = params || {};
            delete p.backToSearch;
            delete p.SearchResult;        
            delete that.Class.searchString;
          $('.back2search').hide();
             
        });

      });
     
      $("#back-to-Search").off('click').on('click', function(){
        params.backToSearch = true;
        try {
          that.Class.dispatch("navigation", "index", params);
          $("#search-text").val(that.Class.searchString);
        }catch (ex) {
          that.Class.logger.error("Search Click: Error in Dispatching to Navigation Controller %1%", ex.message);
        }
      });

      var sAllSearchableData = $ss.helper.GetSearchableContents();
      controller.mapSysActionContents();
      hostUrl = that.GetHostUrl();
      if(params.searchTextArea && params.AutoComplete==true){
        $('#search-text').autocomplete({
          serviceUrl: function(query){
            return hostUrl + '/contents/search';
          },
          maxHeight1: 10,
          params:{
            page:1,
            pageSize: $ss.agentcore.dal.config.GetConfigValue("layout_layout1","page_size","5"),
            fields:"title, description",
            Language: this.Class.currentLang
          },
          preserveInput:false,
          noCache: true,
          appendTo: "#container",
          minChars: $ss.agentcore.dal.config.GetConfigValue("layout_layout1","minimum_characters","5"),
          groupBy: 'scc_content_type',
          maxItems: 3,
          orientation:'auto',
          transformResult: function(data,q){
            showfeatured = false;
            var filtersearchresult = [];
            data = typeof data === 'string' ? $.parseJSON(data) : data;
            var parseData = data.Docs || [];
            if (parseData.length > 0) {
              var mapsearchresult = $ss.helper.MapSearchableContent(parseData);
              filtersearchresult = $ss.helper.GetFilteredResult(mapsearchresult,sAllSearchableData);
            }
            var b;
            b = $.map(filtersearchresult, function (value) {
              var bAddToMap = true;
              var sContentTitle = value.title || value.scc_title;
              var sContentType = value.ctype || value.scc_content_type_guid || '';
              if (sContentType === "sprt_actionlight") {
                $.each(arrSysSupportAction, function (w, u) {
                  bAddToMap = (sContentTitle == u) ? false : true;
                  return bAddToMap; //skips $.each when value is false
                });
              }
              if(bAddToMap) {
                return { value: sContentTitle, data: sContentType }
              }
            });
            return {suggestions: b};
          },
          lookupFilter: function(suggestion, originalQuery, queryLowerCase) {
            var re = new RegExp('\\b' + $.Autocomplete.utils.escapeRegExChars(queryLowerCase), 'gi');
            return re.test(suggestion.value);
          },
          onSelect: function(suggestion) {
            $('#search-text').val(suggestion.value);
            $('#search-btn').focus().click();
          }
        });
      }
    },
    index: function (params) {
      var that = this;

      $ss.agentcore.utils.ui.FixPng();
      var sString = (this.Class.searchString != "") ? this.Class.searchString : "*:*";
      
      performSearch(sString, this.Class.currentLang)
      .then(function(data){
        if(hasValidResults(data)) handleSuccess(data.Docs);
        else reportError();
      }).fail(function(xhr, textStatus, errorThrown){
        _logger.error('Failed to get search result:' + xhr.statusText);
        reportError();
      });

      function performSearch (searchString, sLang){
        var searchRequestP = { query: searchString, language: sLang };
        return invokeSearchP(searchRequestP);
      }

      function invokeSearchP(searchRequest){
        $.support.cors = true;
        return $.ajax({
          async:false,
          url : hostUrl + '/contents/search?&page=1',
          dataType: 'json',
          contentType:'application/json',
          type:'POST',
          crossDomain :true,
          data:JSON.stringify(searchRequest)
        });
      }
      
      function hasValidResults(results){
        return (results && results.Docs && $.isArray(results.Docs) && results.Docs.length>0)
      }
      
      function handleSuccess(data){
        var results = data || [];
        params.SearchResult = $ss.helper.MapSearchableContent(results);
        that.displayResults(params);
        $('.back2search').show();
      }
    
      function reportError(){
        $('.back2search').hide();
        params.SearchResult = [];
        that.displayResults(params);
      }
    
    },
     
    
    GetHostUrl: function() {
      var regPath = "Software\\SupportSoft\\ProviderList\\" + $ss.agentcore.dal.config.GetProviderID() + "\\" + "SearchConfig";
      var user = $ss.agentcore.dal.config.ExpandSysMacro("%USER%");
      return $ss.agentcore.dal.registry.GetRegValue("HKLM", regPath, "HostUrl") + "//" + user;
    },

    displayResults: function(params){
      params.requesturl ="/selfservicesearch";
      params.requestPath ="/selfservicesearch";
      params.action =".navigate click";
      try{
        controller.Class.dispatch("navigation", "index", params);
      }catch (ex){
        this.Class.logger.error("Search controller: Error in Dispatching to Navigation Controller %1%", ex.message);
      }
    },
    
    mapSysActionContents :function(){
      arrSysSupportAction[0] = [_utils.GlobalXLate("0c219a28-ffa5-4256-a800-caee9604b494")];
      arrSysSupportAction[1] = [_utils.GlobalXLate("4d360154-2fbf-4ba9-9793-d5524f06b841")];
      arrSysSupportAction[2] = [_utils.GlobalXLate("2465e1b1-e256-4589-92da-d3ab98f17587")];
      arrSysSupportAction[3] = [_utils.GlobalXLate("a32614f1-7aa7-4171-b6a5-f150fbbfb589")];
      arrSysSupportAction[4] = [_utils.GlobalXLate("f7b6c494-c90a-442a-b988-efdfd143b99c")];
      arrSysSupportAction[5] = [_utils.GlobalXLate("ff8cb1c7-a918-44f7-bfdb-11c7eebae9e9")];
    }

});
var _logger = $ss.agentcore.log.GetDefaultLogger("search_controller");
var arrSysSupportAction = [];
