// Namespaces
$ss.snapin = $ss.snapin || {};
$ss.snapin.firstrun = $ss.snapin.firstrun || {};
$ss.snapin.firstrun.dec = $ss.snapin.firstrun.dec || {} 
$ss.snapin.firstrun.navhandler = $ss.snapin.firstrun.navhandler || {};
$ss.snapin.firstrun.renderhelper = $ss.snapin.firstrun.renderhelper || {};
$ss.snapin.firstrun.common = $ss.snapin.firstrun.common || {};

// Define snapin, buttons and content holders
SnapinFirstrunController = SteplistBaseController.extend("snapin_firstrun", {
  InitParam: {
    aNavigationItems: ["#snapin_firstrun_buttons #nextbutton","#snapin_firstrun #prevbutton" ],
    aContentPlaceHolderDIV: "snapin_firstrun_content",
    sBackToPageURL: "/home",
    sForwardToPageURL: "/home",
    sErrorPageURL:"/home"
  }
}, 

// Sub functions
{
  index: function(params){    

    if(params.stepListSession === undefined) {
      this.renderSnapinView("snapin_firstrun", params.toLocation, "\\views\\%LANGCODE%\\fr_layout_main.htm");
      $ss.agentcore.events.SendByName("DISABLE_NAVIGATION");
      $ss.agentcore.events.SendByName("DISABLE_HIDE_NW_CHECK");
    }
    //this.renderSnapinView("snapin_firstrun", params.toLocation, "\\views\\sa_layout.htm");
    params.toLocation = "#firstRunRightArea";
    this.renderSnapinView("snapin_firstrun", params.toLocation, "\\views\\sa_layout.htm");
    var toBeSelectedLeft = $ss.agentcore.dal.databag.GetValue("ss_fl_db_onresume_selected_section"); // this databag is used to determine which left one will be selected if it starts on reume because of get connect flow
	if(toBeSelectedLeft) { $ss.snapin.firstrun.common.ToggleLinkClass(toBeSelectedLeft, true); }
    this._super(params,"snapin_firstrun");
    var flowInfo = this.Class.InitiateStep(params);
    if(flowInfo) {
      this.ProcessRender(params,flowInfo);
    }
  },
  ProcessNext: function(params){
    var flowInfo = this._super(this,params);
    if(flowInfo) {
      this.ProcessRender(params,flowInfo);
  } else {
    $ss.snapin.firstrun.renderhelper.displayLeftNavigation.next();
    }
  },

  ProcessPrevious: function(params){
    var flowInfo = this._super(this,params);
    if(flowInfo) {
      this.ProcessRender(params,flowInfo);
  } else {
    $ss.snapin.firstrun.renderhelper.displayLeftNavigation.prev();
    }
    
  },
 
  ProcessRender: function(params,flowInfo) {
       var renderHelper = $ss.snapin.firstrun.renderhelper || {};
       var stepId = flowInfo.stepid;
       var retData = {};
       var opt = {};
       if (stepId) {
          try {
            if(renderHelper[stepId] && renderHelper[stepId].before) {
              retData = renderHelper[stepId].before(params,this);  
            }
            opt.oLocal = retData;
          } catch(ex) {
            //failed to execute the renderhelper function
          }
      
        }
      this.ProcessFlow(this,params,flowInfo,opt);
      // start the post processing
      if (stepId) {
          try {
            var afterStatus = {};
            if(renderHelper[stepId] && renderHelper[stepId].after) {
              afterStatus = renderHelper[stepId].after(params,this);  
            }
            switch (afterStatus.move) {
              case "forward":
                return this.ProcessNext(params);
                break;
              case "back":
                return this.ProcessPrevious(params);
                break;
              default:
                break;
            }
          } catch(ex) {
              //failed to execute the renderhelper after function
          }
        }  
  },
 
// Button Clicks and Registry saves
"#nextbutton click": function(params) {
  var stepId = params.element.stepId;
  var everythingFine = true;
  switch (stepId) {
    case "sta_intro":
      //in case of first step itself enable the back button.
      $ss.snapin.firstrun.common.EnableButton("previous");
      this.ProcessNext(params);
      break;
    case "sta_accountinfo":
      var bForceAccountVerification = ($ss.agentcore.dal.config.GetConfigValue("firstrun", "force_account_verification") == "true");
      if (bForceAccountVerification) {

        var aDataToSave = $("#ContactForm").formToArray();
        var validateStatus = $("#ContactForm").validate(this.Class.prefValidateData);
        validateStatus.settings.highlight = false;
        validateStatus.settings.errorClass = "myerror"
        for (var x = 0; x < aDataToSave.length; x++) {
          try {
            if (!validateStatus.element("#" + aDataToSave[x].name)) {
              everythingFine = false;
              $("#ContactForm .error").css({ display: "block" });
            }
          }
          catch (ex) {
          }
        }
      }
      if (everythingFine) {
        //Save data to registry
        $ss.snapin.firstrun.common.SaveFormData();
        this.ProcessNext(params);
      } else {
        $("#sa_fr_Content").scrollTo('+=340px', { axis: 'y' });
      }
      break;
    default:
      this.ProcessNext(params);
      break;
  }
},
  "#prevbutton click": function(params){
    this.ProcessPrevious(params);
  },
  "#snapin_firstrun_buttons #savebutton click": function(params){
    var objConfig = $ss.agentcore.dal.config;
    var objFile = $ss.agentcore.dal.file;
    var sFilename = "EULA";
    var innerHTML =  $("#snapin_firstrun #frscrollarea2")[0].innerHTML

    var sDesktopPath = objConfig.ExpandSysMacro("%SF_DESKTOP%") + sFilename + ".html";
    if (objFile.WriteNewFile(sDesktopPath, innerHTML)) {
      $("#closeclick").parents("p").remove();
      $("#popupfirstrun").hide('fast');      
      $("#popupfirstrun").show('fast');
      //var snapPath = $ss.getSnapinAbsolutePath("snapin_firstrun");
      var layoutSkinPath = $ss.GetLayoutSkinPath();
      var html = "<p class='close' align='center'><img src='" + layoutSkinPath + "/buttons/"+ $ss.GetShellLang()+"/btn_close.gif' id='closeclick' name='closeclick'  alt='" + $ss.agentcore.utils.LocalXLate("snapin_firstrun", "cl_fr_close_button") + "'  border='0'/></p>";
      $("#popupfirstrun").append(html);
    }
    else {
       alert($ss.agentcore.utils.LocalXLate("snapin_firstrun","cl_fr_eula_save_failed"));
    }  
  },
  "#snapin_firstrun_buttons #closebutton click": function(params){
    $ss.agentcore.utils.CloseBcont();
  },
  "#snapin_firstrun_buttons #printbutton click": function(params){
    var _helper = $ss.snapin.firstrun.common;
    _helper.PrintWindow($("#snapin_firstrun #frscrollarea2")[0].innerHTML, $("#snapin_firstrun #print_frame")[0].contentWindow);
    $ss.agentcore.utils.ui.BringBcontToFront();                            
  },
  "#popupfirstrun #closeclick click": function(params){
    $("#closeclick").parents("p").remove();
    $("#popupfirstrun").hide('fast');
  },
  //EULA Accept Button needs to clear any previous decline selections and then enable the Next button.
  //We also save the accepted EULA to the Registry for later use
  "#sta_eula #cl_fr_eula_accept click": function(params) {
    $("input[@name='HitYes']").attr("checked", true);
    $("input[@name='HitNo']").attr("checked", false);
    
    $ss.snapin.firstrun.common.EnableButton("next");
    $ss.agentcore.dal.config.SetUserValue("firstrun", "EulaAccepted", "true");
  },
  //EULA Decline Button needs to clear any previous accept selections and ensure the Next button is disabled.
  //We set EulaAccepted to False just incase it had already been selected
  "#sta_eula #cl_fr_eula_decline click": function(params) {
    $("input[@name='HitYes']").attr("checked", false);
    $("input[@name='HitNo']").attr("checked", true);
    
    $ss.snapin.firstrun.common.DisableButton("next");
    $ss.agentcore.dal.config.SetUserValue("firstrun", "EulaAccepted", "false");
  },
  "#cl_fr_gc_offlineoption #chkbxDiagnoseConnection_false click": function(params) {
    $ss.agentcore.dal.databag.SetValue("sa_fr_diagnose_connection","false");
    $ss.snapin.firstrun.common.EnableButton("next");
    $ss.snapin.firstrun.common.EnableButton("previous");
  },
  "#cl_fr_gc_offlineoption #chkbxDiagnoseConnection_true click": function(params) {
    $ss.agentcore.dal.databag.SetValue("sa_fr_diagnose_connection","true");
    $ss.snapin.firstrun.common.EnableButton("next");
    $ss.snapin.firstrun.common.EnableButton("previous");  
  }
});
