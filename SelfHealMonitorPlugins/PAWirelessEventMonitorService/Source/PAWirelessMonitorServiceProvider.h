//
//  PAWirelessMonitorServiceProvider.h
//  PAFirewallEventMonitorService
//
//  Copyright © 2018 Avolin. All rights reserved.
//
#import "PAMonitorServiceProvider.h"
#import "PAWirelessMonitorEventDelegate.h"

@interface PAWirelessMonitorServiceProvider : PAMonitorServiceProvider <PAWirelessMonitorEventDelegate>


@end
