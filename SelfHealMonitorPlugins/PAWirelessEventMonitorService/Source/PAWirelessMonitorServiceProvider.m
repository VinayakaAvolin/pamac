//
//  PAWirelessMonitorServiceProvider.m
//  PAFirewallEventMonitorService
//
//  Copyright © 2018 Avolin. All rights reserved.
//

#import "PAWirelessMonitorServiceProvider.h"
#import "PAWirelessMonitor.h"

@interface PAWirelessMonitorServiceProvider ()
{
    PAWirelessMonitor * _wirelessMonitor;
}
@property (nonatomic) PAMonitorServiceProviderResult replyBlock;

@end

@implementation PAWirelessMonitorServiceProvider

- (BOOL)canHandleCapability:(SelfHealEventClass)capability forEvent:(PASelfHealTriggerInfo*)eventInfo
{
    NetworkMonitorType type = [eventInfo networkMonitorType];
    return (type == Wireless);
}

            
- (void) startMonitoringEvent:(PASelfHealTriggerInfo*)eventInfo
{
    NSLog(@"Wireless monitoring started");
    //TODO: Identify the paths and data to be monitored.
    if (!_wirelessMonitor) {
        _wirelessMonitor = [[PAWirelessMonitor alloc] initWithTriggerInfo:eventInfo withDelegate:self];
    }
    [_wirelessMonitor startMonitoring];
    
}

- (void) startMonitoringEvent:(PASelfHealTriggerInfo*)eventTrigger withReply:(PAMonitorServiceProviderResult)reply
{
    NSLog(@"Wireless monitoring started");
    //TODO: Identify the paths and data to be monitored.
    if (!_wirelessMonitor) {
        _wirelessMonitor = [[PAWirelessMonitor alloc] initWithTriggerInfo:eventTrigger withDelegate:self];
    }
    self.replyBlock = reply;
    [_wirelessMonitor startMonitoring];
}

- (void) stopMonitoringEvent:(PASelfHealTriggerInfo*)eventInfo
{
    NSLog(@"Wireless monitoring ended");
    [_wirelessMonitor stopMonitoring];
    _wirelessMonitor = nil;
}

- (void) wirelessMonitor:(PAWirelessMonitorServiceProvider *)wirelessMonitor eventOccurred:(PASelfHealEvent *)event
{
    self.replyBlock(event);
}


@end

