//
//  PAWirelessMonitor.m
//  PAFirewallEventMonitorService
//
//  Copyright © 2018 Avolin. All rights reserved.
//
#import <PACoreServices/PACoreServices.h>
#import "PAWirelessMonitor.h"
#import "GCNetworkReachability.h"
static NSTimeInterval const latency = 0.1;

@interface PAWirelessMonitor ()

@property (nonatomic) id<PAWirelessMonitorEventDelegate> delegate;
@property (nonatomic) PASelfHealTriggerInfo * eventInfo;
@property (nonatomic) GCNetworkReachability * internalReachability;
@property (nonatomic) NSTimeInterval lastEventInterval;
@end

@implementation PAWirelessMonitor

-(instancetype) initWithTriggerInfo: (PASelfHealTriggerInfo *)triggerInfo withDelegate:(id<PAWirelessMonitorEventDelegate>) delegate
{
    if (self = [super init]) {
        self.delegate = delegate;
        self.eventInfo = triggerInfo;
    }
    return self;
}

-(void) startMonitoring
{
    [self subscribeToNetworkEventsAndMonitor];
}

-(void) stopMonitoring
{
    [self.internalReachability stopMonitoringNetworkReachability];
    self.delegate = nil;
    self.eventInfo = nil;
    self.internalReachability = nil;
}

-(void) subscribeToNetworkEventsAndMonitor {
    self.internalReachability = [GCNetworkReachability reachabilityWithHostName:@"www.google.com"];
    // Internet is reachable
    __weak PAWirelessMonitor* _weakSelf = self;
    __block BOOL onStart = YES;
    [self.internalReachability startMonitoringNetworkReachabilityWithHandler:^(GCNetworkReachabilityStatus status) {
        
        // this block is called on the main thread
        switch (status) {
            case GCNetworkReachabilityStatusNotReachable:
                if (_weakSelf.eventInfo.wirelessStatus == Disconnected && onStart == NO){
                    [_weakSelf postCallbackWithStatus:NO];
                }
                break;
            case GCNetworkReachabilityStatusWWAN:
                break;
            case GCNetworkReachabilityStatusWiFi:
                if (_weakSelf.eventInfo.wirelessStatus == Connected && onStart == NO){
                    [_weakSelf postCallbackWithStatus:YES];
                }
                break;
        }
        onStart = NO;
    }];
}

-(void) postCallbackWithStatus:(BOOL)isConnected
{
    NSTimeInterval current = [[NSDate date] timeIntervalSince1970];
    NSTimeInterval elapsedSinceLast = fabs(current - self.lastEventInterval);
    if(elapsedSinceLast > latency) {
        NSLog(@"Posting event = %d", isConnected);
        NSString *statusString = [NSString stringWithFormat:@"Wireless Status = %d", isConnected];
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:statusString];
        if (data) {
            data = [data base64EncodedDataWithOptions:0];
        }
        PASelfHealEvent *event = [[PASelfHealEvent alloc] initWithGuid:self.eventInfo.contentGuid triggerInfo:self.eventInfo andData:data];
        [[PALogger sharedLoggerForClient:PALogClientSelfHeal] debug:@"======>Wireless monitoring event triggered %@", self.eventInfo.contentGuid];
        [self.delegate wirelessMonitor:nil eventOccurred:event];
        self.lastEventInterval = current;
    }

}
@end
