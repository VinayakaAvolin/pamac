//
//  PAWirelessMonitorEventDelegate.h
//  ProactiveAssist
//
//  Copyright © 2018 Avolin. All rights reserved.
//

#ifndef PAWirelessMonitorEventDelegate_h
#define PAWirelessMonitorEventDelegate_h

@class PAWirelessMonitorServiceProvider;
@class PASelfHealEvent;

@protocol PAWirelessMonitorEventDelegate
@required

- (void) wirelessMonitor:(PAWirelessMonitorServiceProvider *)wirelessMonitor eventOccurred:(PASelfHealEvent *)event;

@end

#endif /* PAWirelessMonitorEventDelegate_h */
