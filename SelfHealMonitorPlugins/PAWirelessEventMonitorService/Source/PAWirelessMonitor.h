//
//  PAWirelessMonitor.h
//  PAFirewallEventMonitorService
//
//  Copyright © 2018 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PAWirelessMonitorEventDelegate.h"
#import "PASelfHealTriggerInfo.h"

@interface PAWirelessMonitor : NSObject


-(instancetype) initWithTriggerInfo: (PASelfHealTriggerInfo *)triggerInfo withDelegate:(id<PAWirelessMonitorEventDelegate>) delegate;
-(void) startMonitoring;

-(void) stopMonitoring;

@end
