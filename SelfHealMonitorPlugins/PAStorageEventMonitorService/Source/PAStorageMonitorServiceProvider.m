//
//  PAStorageMonitorServiceProvider.m
//  PAStorageEventMonitorService
//
//  Copyright © 2018 Avolin. All rights reserved.
//

#import "PAStorageMonitorServiceProvider.h"
#import "PAStorageMonitor.h"

@interface PAStorageMonitorServiceProvider ()
{
    PAStorageMonitor * _storageMonitor;
}
//typedef void (^PAMonitorServiceProviderResult)(PASelfHealEvent *event);
@property (nonatomic) PAMonitorServiceProviderResult replyBlock;

@end

@implementation PAStorageMonitorServiceProvider


- (BOOL)canHandleCapability:(SelfHealEventClass)capability forEvent:(PASelfHealTriggerInfo*)eventInfo
{
    return (capability == StorageMonitoring);
}


- (void) startMonitoringEvent:(PASelfHealTriggerInfo*)eventTrigger withReply:(PAMonitorServiceProviderResult)reply
{
    if (!_storageMonitor) {
        
        _storageMonitor= [[PAStorageMonitor alloc] initWithTriggerInfo:eventTrigger withDelegate:self];
    }
    self.replyBlock=reply;
    [_storageMonitor startMonitoring];
}
- (void) stopMonitoringEvent:(PASelfHealTriggerInfo*)eventInfo
{
    NSLog(@"Storage monitoring ended");
    [_storageMonitor stopMonitoring];
    _storageMonitor = nil;
}


- (void) storageMonitor:(PAStorageMonitorServiceProvider *)storageMonitor eventOccurred:(PASelfHealEvent *)event
{
    self.replyBlock(event);
}
@end
