//
//  PAStorageMonitorServiceProvider.h
//  PAStorageEventMonitorService
//
//  Copyright © 2018 Avolin. All rights reserved.
//

#import "PAMonitorServiceProvider.h"
#import "PAStorageMonitorEventDelegate.h"

@interface PAStorageMonitorServiceProvider : PAMonitorServiceProvider <PAStorageMonitorEventDelegate>

@end
