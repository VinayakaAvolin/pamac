//
//  PAStorageMonitor.h
//  PAStorageEventMonitorService
//
//  Copyright © 2018 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PAStorageMonitorEventDelegate.h"
#import "PASelfHealTriggerInfo.h"
#import "PASelfHealEvent.h"

@interface PAStorageMonitor : NSObject
-(instancetype) initWithTriggerInfo: (PASelfHealTriggerInfo *)triggerInfo withDelegate:(id<PAStorageMonitorEventDelegate>)delegate;
-(void) startMonitoring;
-(void) stopMonitoring;

@end
