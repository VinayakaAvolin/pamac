//
//  PAStorageMonitorEventDelegate.h
//  ProactiveAssist
//
//  Copyright © 2018 Avolin. All rights reserved.
//

#ifndef PAStorageMonitorEventDelegate_h
#define PAStorageMonitorEventDelegate_h
#import "PASelfHealEvent.h"
@class PAStorageMonitorServiceProvider;
@class PASelfHealTriggerInfo;
@class PASelfHealEvent;

@protocol PAStorageMonitorEventDelegate
@required

- (void) storageMonitor:(PAStorageMonitorServiceProvider *)storageMonitor eventOccurred:(PASelfHealEvent *)event;
@end

#endif /* PAStorageMonitorEventDelegate_h */
