//
//  PAStorageMonitor.m
//  PAStorageEventMonitorService
//
//  Copyright © 2018 Avolin. All rights reserved.
//

#import "PAStorageMonitor.h"
#import <CoreServices/CoreServices.h>
#import <AppKit/AppKit.h>
#import <PACoreServices/PACoreServices.h>


@interface PAStorageMonitor ()
{
    FSEventStreamRef fileStream;
}

@property (nonatomic) id<PAStorageMonitorEventDelegate> delegate;
@property (nonatomic) PASelfHealTriggerInfo  * triggerInfo;
@property (nonatomic) NSTimer * timer;

@property (strong) NSMutableArray * mountedVolumes;
@property (nonatomic, assign) float previousStorageLevelPercent;
@end


@implementation PAStorageMonitor
-(instancetype) initWithTriggerInfo: (PASelfHealTriggerInfo *)triggerInfo withDelegate:(id<PAStorageMonitorEventDelegate>)delegate
{
        if (self = [super init]) {
            self.delegate = delegate;
            self.triggerInfo = triggerInfo;
            self.previousStorageLevelPercent = 100.0;
        }
        return self;
}
//-(instancetype) initWithDelegate:(id<PAStorageMonitorEventDelegate>) delegate forStatus:(PASelfHealEvent *) event
//{
//    if (self = [super init]) {
//        self.delegate = delegate;
//        self.event = event;
//    }
//    return self;
//}

-(void) startMonitoring
{

    //Set System Volumes
//    NSArray *mountedVolumeUrls =  [[NSFileManager defaultManager ]  mountedVolumeURLsIncludingResourceValuesForKeys:nil options:NSVolumeEnumerationSkipHiddenVolumes];
//    NSInteger count = [mountedVolumeUrls count];
  // self.mountedVolumes = [[NSMutableArray alloc] initWithCapacity:[ mountedVolumeUrls count]];
//    for(int i =0; i<count; i++){
//        NSString *path = [mountedVolumeUrls[i] absoluteString];
//        if([path containsString:@"file://"]) {
//            path = [path stringByReplacingOccurrencesOfString:@"file://" withString:@""];
//        }
//        [self.mountedVolumes addObject:path];
//   }
    
    
   //iterate and collect path to be monitored as per xml
   self.mountedVolumes = [[NSMutableArray alloc] initWithCapacity:[self.triggerInfo.storageAttributes count]];;
    for( NSDictionary *content in self.triggerInfo.storageAttributes){
        NSString *path = [ content valueForKey:@"event_monitor_volume"];
        //[pathTobeMonitored addObject:path];
        [self.mountedVolumes addObject:path];
    }
   [self setupTimerAndPoll];
    
}

-(void) stopMonitoring
{
    [self.timer invalidate];
    self.timer = nil;
    self.delegate = nil;
}

- (BOOL)calculateSize:(NSDictionary*)volumeDict {
    // Get the path from the event array
    NSError *error=nil;
    BOOL isStorageLimitReached = FALSE;
    //NSArray *monitoring *@"event_monitor_volume";
    //@"event_monitor_level_percentage"
    NSString *path = [ volumeDict valueForKey:@"event_monitor_volume"];
    if(path)
    {
        NSDictionary * fileAttributes = [[NSFileManager defaultManager] attributesOfFileSystemForPath:path
                                                                                            error:&error];
        if (!error)
        {
            float totalsizeGb = [[fileAttributes objectForKey:NSFileSystemSize]floatValue] / 1000000000;
            float freesizeGb = [[fileAttributes objectForKey:NSFileSystemFreeSize]floatValue] / 1000000000;
            float percentage = (freesizeGb * 100 )/totalsizeGb;
            NSInteger percentageSet = [(NSNumber*)[volumeDict valueForKey:@"event_monitor_level_percentage"] intValue];
            if ( (percentage <= percentageSet) && fabs(self.previousStorageLevelPercent - percentage) > 1.0) {
                // [self.delegate storageMonitor:nil eventOccurred:nil];
                //NSLog(@"%d%d",percentage, percentageSet);
                isStorageLimitReached = TRUE;
            }
            self.previousStorageLevelPercent = percentage;
        }
    }
    return isStorageLimitReached;
}



- (void)setupTimerAndPoll {
    //typeof(self) __weak weakSelf = self;
    __weak PAStorageMonitor *weakSelf = self;
   self.timer = [NSTimer timerWithTimeInterval:self.triggerInfo.interval repeats:YES block:^(NSTimer * _Nonnull timer) {
        //iterate through monitoring path
        for( NSDictionary *content in weakSelf.triggerInfo.storageAttributes){
            NSString *path = [ content valueForKey:@"event_monitor_volume"];
            if([weakSelf.mountedVolumes containsObject:path])
            {
                BOOL isLimitReached = [weakSelf calculateSize:content];
                if(isLimitReached)
                {
                    NSString *statusString = [NSString stringWithFormat:@"Storage Status threshold below = %@", path];
                    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:statusString];
                    if (data) {
                        data = [data base64EncodedDataWithOptions:0];
                    }
                    PASelfHealEvent * eEvent = [[PASelfHealEvent alloc] initWithGuid:weakSelf.triggerInfo.contentGuid triggerInfo:weakSelf.triggerInfo andData:data];
                    [[PALogger sharedLoggerForClient:PALogClientSelfHeal] debug:@"======>Storage monitoring event triggered %@", weakSelf.triggerInfo.contentGuid];
                    [weakSelf.delegate storageMonitor:nil eventOccurred:eEvent];
                }
            }
        }
    }];
    [[NSRunLoop mainRunLoop] addTimer:self.timer forMode:NSRunLoopCommonModes];
}

@end
