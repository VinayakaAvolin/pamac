//
//  DiskMonitor.h
//  ProactiveAssist
//
//  Copyright © 2019 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PADisk.h"

NS_ASSUME_NONNULL_BEGIN

@protocol PADiskMonitorDelegate
@required

- (void) didMountDisk:(PADisk *)disk;
- (void) didUnmountDisk:(PADisk *)disk;

@end

@interface PADiskMonitor : NSObject

@property(nonatomic,weak) id<PADiskMonitorDelegate> delegate;

-(instancetype)initWithDelegate:(id<PADiskMonitorDelegate>)delegate;

- (void)startMonitoring;
- (void)stopMonitoring;


@end

NS_ASSUME_NONNULL_END
