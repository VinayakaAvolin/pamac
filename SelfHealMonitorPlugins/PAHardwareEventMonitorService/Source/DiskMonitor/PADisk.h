//
//  PADisk.h
//  ProactiveAssist
//
//  Copyright © 2019 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PADisk : NSObject

@property(nonatomic, readonly) NSString *volumeName;
@property(nonatomic, readonly) NSString *model;
@property(nonatomic, readonly) NSString *vendor;
@property(nonatomic, readonly) NSString *protocol;
@property(nonatomic, readonly) BOOL isRemovable;
@property(nonatomic, readonly) BOOL isUsbDevice;
@property(nonatomic, readonly) NSString *mountPath;

- (instancetype)initWithDiskInfo:(NSDictionary *)diskInfo;

@end

NS_ASSUME_NONNULL_END
