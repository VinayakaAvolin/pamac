//
//  PADisk.m
//  ProactiveAssist
//
//  Copyright © 2019 Avolin. All rights reserved.
//

#import "PADisk.h"

NSString *const kDeviceProtocolUsb = @"USB";
NSString *const kVolumesPath = @"/Volumes";

@implementation PADisk

- (instancetype)initWithDiskInfo:(NSDictionary *)diskInfo
{
    self = [super init];
    if (self) {
        if (diskInfo) {
            _volumeName = diskInfo[(NSString *)kDADiskDescriptionVolumeNameKey];
            _model = diskInfo[(NSString *)kDADiskDescriptionDeviceModelKey];
            _vendor = diskInfo[(NSString *)kDADiskDescriptionDeviceVendorKey];
            _protocol = diskInfo[(NSString *)kDADiskDescriptionDeviceProtocolKey];
            _isRemovable = [diskInfo[(NSString *)kDADiskDescriptionMediaRemovableKey] boolValue];
        } else {
            self = nil;
        }
        
    }
    return self;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"Disk voume name [%@], model [%@], vendor [%@], protocol [%@], isRemovable [%d] isUsb [%d] mountPath [%@]", _volumeName, _model, _vendor, _protocol, _isRemovable, [self isUsbDevice], [self mountPath]];
}

- (BOOL)isUsbDevice {
    // for Pen drive protocol is "USB"
    return (_protocol && [_protocol caseInsensitiveCompare:kDeviceProtocolUsb] == NSOrderedSame);
}

- (NSString *)mountPath {
    return (_volumeName ? [kVolumesPath stringByAppendingPathComponent:_volumeName] : nil);
}

@end
