//
//  DiskMonitor.m
//  ProactiveAssist
//
//  Copyright © 2019 Avolin. All rights reserved.
//

#import <DiskArbitration/DiskArbitration.h>
#import <PACoreServices/PACoreServices.h>
#import "PADiskMonitor.h"

@interface PADiskMonitor () {
    DASessionRef _session;
}

@end

@implementation PADiskMonitor

- (instancetype)initWithDelegate:(id<PADiskMonitorDelegate>)delegate
{
    self = [super init];
    if (self) {
        _delegate = delegate;
    }
    return self;
}
- (void)startMonitoring {
    [self registerDiskCallback];
}

- (void)stopMonitoring {
    [self unregisterDiskCallback];
}

#pragma mark - Private methods

- (void) registerDiskCallback {
    _session = DASessionCreate(kCFAllocatorDefault);
    if (_session != NULL) {
        DARegisterDiskAppearedCallback(_session, kDADiskDescriptionMatchVolumeMountable,diskAppearedCallback, (__bridge void * _Nullable)(self));
        DARegisterDiskDisappearedCallback(_session,kDADiskDescriptionMatchVolumeMountable, diskDisappearedCallback, (__bridge void * _Nullable)(self));
        DASessionScheduleWithRunLoop(_session, [[NSRunLoop mainRunLoop] getCFRunLoop], kCFRunLoopCommonModes);

    }
}
- (void) unregisterDiskCallback {
    if (_session != NULL) {
        DAUnregisterCallback(_session, diskAppearedCallback, (__bridge void * _Nullable)(self));
        DAUnregisterCallback(_session, diskDisappearedCallback, (__bridge void * _Nullable)(self));
        DASessionUnscheduleFromRunLoop(_session, [[NSRunLoop mainRunLoop] getCFRunLoop], kCFRunLoopCommonModes);

        CFRelease(_session);
        _session = NULL;
    }
}

void diskAppearedCallback(DADiskRef disk, void* context)
{
    PADiskMonitor *diskMonitor = (__bridge PADiskMonitor *)context;
    NSDictionary *diskInfo = CFBridgingRelease(DADiskCopyDescription(disk));
    PADisk *mountedDisk = [[PADisk alloc] initWithDiskInfo:diskInfo];
    NSLog(@"Mounted Disk :: [%@]",mountedDisk);
    [diskMonitor.delegate didMountDisk:mountedDisk];
}

void diskDisappearedCallback(DADiskRef disk, void* context)
{
    PADiskMonitor *diskMonitor = (__bridge PADiskMonitor *)context;

    NSDictionary *diskInfo = CFBridgingRelease(DADiskCopyDescription(disk));
    PADisk *ejectedDisk = [[PADisk alloc] initWithDiskInfo:diskInfo];
    NSLog(@"Ejected Disk :: [%@]",ejectedDisk);
    [diskMonitor.delegate didUnmountDisk:ejectedDisk];
}

@end
