//
//  PAHardwareMonitorServiceProvider.h
//  PAHardwareEventMonitorService
//
//  Copyright © 2018 Avolin. All rights reserved.
//

#import "PAMonitorServiceProvider.h"
#import "PAHardwareMonitorEventDelegate.h"


@interface PAHardwareMonitorServiceProvider : PAMonitorServiceProvider <PAHardwareMonitorEventDelegate>

@end
