//
//  PAHardwareMonitorServiceProvider.m
//  PAHardwareEventMonitorService
//
//  Copyright © 2018 Avolin. All rights reserved.
//

#import "PAHardwareMonitorServiceProvider.h"
#import "PAHardwareMonitor.h"
#import <PACoreServices/PACoreServices.h>

@interface PAHardwareMonitorServiceProvider ()
{
    PAHardwareMonitor * _hardwareMonitor;
}
@property (nonatomic) PAMonitorServiceProviderResult replyBlock;
@end


@implementation PAHardwareMonitorServiceProvider

- (BOOL)canHandleCapability:(SelfHealEventClass)capability forEvent:(PASelfHealTriggerInfo*)eventInfo
{
    return (capability == HardwareMonitoring);
}


- (void) startMonitoringEvent:(PASelfHealTriggerInfo*)eventInfo
{
    //TODO: Identify the paths and data to be monitored.
    if (!_hardwareMonitor ) {
        _hardwareMonitor = [[PAHardwareMonitor alloc] initWithTriggerInfo:eventInfo withDelegate:self];
    }
    [_hardwareMonitor startMonitoring];
    
}

- (void) startMonitoringEvent:(PASelfHealTriggerInfo*)eventTrigger withReply:(PAMonitorServiceProviderResult)reply
{
    //TODO: Identify the paths and data to be monitored.
    if (!_hardwareMonitor ) {
        _hardwareMonitor = [[PAHardwareMonitor alloc] initWithTriggerInfo:eventTrigger withDelegate:self];
    }
    self.replyBlock = reply;
    [_hardwareMonitor startMonitoring];
}

- (void) stopMonitoringEvent:(PASelfHealTriggerInfo*)event
{
    [_hardwareMonitor stopMonitoring];
    _hardwareMonitor = nil;
}

- (void) hardwareMonitor:(PAHardwareMonitorServiceProvider *)hardwareMonitor eventOccurred:(PASelfHealEvent *)event
{
    [[PALogger sharedLoggerForClient:PALogClientSelfHeal] debug:@"======>Hardware monitoring event triggered %@", event.eventGUID];

    self.replyBlock(event);
}


@end
