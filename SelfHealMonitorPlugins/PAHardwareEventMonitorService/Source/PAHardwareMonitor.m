//
//  PAHardwareMonitor.m
//  ProactiveAssist
//
//  Copyright © 2018 Avolin. All rights reserved.
//

#import <AppKit/AppKit.h>
#import <PACoreServices/PACoreServices.h>
#import "PAHardwareMonitor.h"
#import "PADiskMonitor.h"

#define NOTIFICATION_CENTER [[NSWorkspace sharedWorkspace] notificationCenter]

NSString *const kMountPathVolumes = @"/Volumes";

@interface PAHardwareMonitor () <PADiskMonitorDelegate>

@property (nonatomic) id <PAHardwareMonitorEventDelegate> delegate;
@property (nonatomic) PASelfHealTriggerInfo * eventInfo;
@property (nonatomic) PADiskMonitor *diskMonitor;

@end
@implementation PAHardwareMonitor

-(instancetype) initWithTriggerInfo: (PASelfHealTriggerInfo *)triggerInfo withDelegate:(id<PAHardwareMonitorEventDelegate>) delegate {
    if (self = [super init]) {
        self.delegate = delegate;
        self.eventInfo = triggerInfo;
        self.diskMonitor = [[PADiskMonitor alloc] initWithDelegate:self];
    }
    return  self;
}


-(void) startMonitoring
{
     /* Using Disk arbitration Framework*/
    [self.diskMonitor  startMonitoring];
    /*
    [NOTIFICATION_CENTER addObserver:self
                            selector: @selector(didVolumesUpdate:)
                                name:NSWorkspaceDidMountNotification
                              object: nil];
    
    [NOTIFICATION_CENTER addObserver:self
                            selector: @selector(didVolumesUpdate:)
                                name:NSWorkspaceDidUnmountNotification
                            object:nil];
     */
    
}

-(void) didVolumesUpdate: (NSNotification*) notification
{
    NSString* path = [notification.userInfo[NSWorkspaceVolumeURLKey] path];
    [self checkAndPostEvent:path];
}

-(void)checkAndPostEvent:(NSString *)eventVolumePath {
    if ([self.eventInfo.fileMonitorPaths containsObject:kMountPathVolumes] ||
         [self.eventInfo.fileMonitorPaths containsObject:eventVolumePath]) {
             NSData *data = [NSKeyedArchiver archivedDataWithRootObject:eventVolumePath];
             if (data) {
                 data = [data base64EncodedDataWithOptions:0];
             }
             PASelfHealEvent * eEvent = [[PASelfHealEvent alloc] initWithGuid:self.eventInfo.contentGuid triggerInfo:self.eventInfo andData:data];
             [self.delegate hardwareMonitor:nil eventOccurred:eEvent];
    }
}

-(void) stopMonitoring
{
    self.delegate = nil;
    self.eventInfo = nil;
    /* Using Disk arbitration Framework*/
    [self.diskMonitor stopMonitoring];
    self.diskMonitor = nil;
    /*
    [NOTIFICATION_CENTER removeObserver:NSWorkspaceDidMountNotification];
    [NOTIFICATION_CENTER removeObserver:NSWorkspaceDidUnmountNotification];
     */
}

#pragma mark - DiskMonitorDelegate methods

- (void)didMountDisk:(PADisk *)disk {
    [[PALogger sharedLoggerForClient:PALogClientSelfHeal] debug:@"======>Hardware monitoring did Mount Disk %@", disk];
    [self postRemovableDiskEvent:disk];
}

- (void)didUnmountDisk:(PADisk *)disk {
    [[PALogger sharedLoggerForClient:PALogClientSelfHeal] debug:@"======>Hardware monitoring did Unmount Disk %@", disk];
    [self postRemovableDiskEvent:disk];
}

#pragma mark -

- (void)postRemovableDiskEvent:(PADisk *)disk {
    if (disk.isRemovable && disk.isUsbDevice) {
        // if eventInfo.fileMonitorPaths contains "/Volumes" then need to pass "/Volumes" not the 'disk.mountPath' to get the archived event data in 'checkAndPostEvent' method
        if ([self.eventInfo.fileMonitorPaths containsObject:kMountPathVolumes]) {
            [self checkAndPostEvent:kMountPathVolumes];
        } else {
            [self checkAndPostEvent:disk.volumeName];
        }
    }
}

@end
