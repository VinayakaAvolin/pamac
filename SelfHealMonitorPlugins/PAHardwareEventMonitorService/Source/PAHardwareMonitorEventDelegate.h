//
//  PAHardwareMonitorEventDelegate.h
//  ProactiveAssist
//
//  Copyright © 2018 Avolin. All rights reserved.
//

#ifndef PAHardwareMonitorEventDelegate_h
#define PAHardwareMonitorEventDelegate_h

@class PAHardwareMonitorServiceProvider;
@class PASelfHealEvent;

@protocol PAHardwareMonitorEventDelegate
@required

- (void) hardwareMonitor:(PAHardwareMonitorServiceProvider *)hardwareMonitor eventOccurred:(PASelfHealEvent *)event;

@end


#endif /* PAHardwareMonitorEventDelegate_h */
