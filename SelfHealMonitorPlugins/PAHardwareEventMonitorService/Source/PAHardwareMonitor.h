//
//  PAHardwareMonitor.h
//  ProactiveAssist
//
//  Copyright © 2018 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PAHardwareMonitorEventDelegate.h"
#import "PASelfHealTriggerInfo.h"

@interface PAHardwareMonitor : NSObject

-(instancetype) initWithTriggerInfo: (PASelfHealTriggerInfo *)triggerInfo withDelegate:(id<PAHardwareMonitorEventDelegate>) delegate;

-(void) startMonitoring;

-(void) stopMonitoring;
@end

