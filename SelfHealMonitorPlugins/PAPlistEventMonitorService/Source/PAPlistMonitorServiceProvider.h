//
//  PAPlistMonitorServiceProvider.h
//  ProactiveAssist
//
//  Copyright © 2019 Avolin. All rights reserved.
//

#import "PAMonitorServiceProvider.h"
#import "PAPlistMonitorEventDelegate.h"

@interface PAPlistMonitorServiceProvider : PAMonitorServiceProvider <PAPlistMonitorEventDelegate>

@end
