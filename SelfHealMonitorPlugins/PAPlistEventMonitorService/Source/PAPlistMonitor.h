//
//  PAPlistMonitor.h
//  ProactiveAssistAgent
//
//  Copyright © 2019 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PAPlistMonitorEventDelegate.h"
#import "PASelfHealTriggerInfo.h"

@interface PAPlistMonitor : NSObject

-(instancetype) initWithTriggerInfo: (PASelfHealTriggerInfo *)triggerInfo withDelegate:(id<PAPlistMonitorEventDelegate>) delegate;
-(void) startMonitoring;

-(void) stopMonitoring;

@end
