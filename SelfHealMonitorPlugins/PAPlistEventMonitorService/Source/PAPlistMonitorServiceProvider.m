//
//  PAPlistMonitorServiceProvider.m
//  ProactiveAssistAgent
//
//  Copyright © 2019 Avolin. All rights reserved.
//
#import <PACoreServices/PACoreServices.h>
#import "PAPlistMonitorServiceProvider.h"
#import "PAPlistMonitor.h"
#import "PAEventBus.h"
#import "PAPlistWrapper.h"
#import "PAPlistUtility.h"
#import "PAPlistLockManager.h"
#import "PASelfHealTriggerInfo.h"


#define DATA_KEY @"Data"
#define PATH_KEY @"Path"

@interface PAPlistMonitorServiceProvider ()
{
    PAPlistMonitor * plistMonitor;
    PASelfHealTriggerInfo *_eventTrigger;
    PAFileSystemManager *_fsManager;
}
@property (nonatomic) PAMonitorServiceProviderResult replyBlock;
@property (nonatomic) NSString *keyPath;
@end


@implementation PAPlistMonitorServiceProvider

- (BOOL)canHandleCapability:(SelfHealEventClass)capability forEvent:(PASelfHealTriggerInfo*)eventTrigger
{
    return capability == PlistMonitoring;
}

- (void) startMonitoringEvent:(PASelfHealTriggerInfo*)eventTrigger
{
    [self startMonitoringEvent:eventTrigger withReply:nil];
}

- (void) startMonitoringEvent:(PASelfHealTriggerInfo*)eventTrigger withReply:(PAMonitorServiceProviderResult)reply
{
    NSLog(@"Plist monitoring started");
    //TODO: Identify the paths and data to be monitored.
    _eventTrigger = eventTrigger;
    self.keyPath = [[NSString alloc] initWithFormat:@"%@/%@",
                    @"software/supportsoft/providerlist/avolinproviderid/plistmonitor",_eventTrigger.contentGuid];
    _fsManager = [[PAFileSystemManager alloc] init];

    if(!plistMonitor)
    {
        [self setUserRegistryWithGuid:eventTrigger];
        plistMonitor = [[PAPlistMonitor alloc] initWithTriggerInfo:eventTrigger withDelegate:self];
    }
    self.replyBlock = reply;
    [plistMonitor startMonitoring];
}

- (void) stopMonitoringEvent:(PASelfHealTriggerInfo*)eventTrigger
{
    NSLog(@"Firewall monitoring ended");
    [plistMonitor stopMonitoring];
}

- (void) plistMonitor:(PAPlistMonitorServiceProvider *)plistMonitor eventOccurred:(PASelfHealEvent *)event
{
        //we are in right data, lets check the plist key chanages....
    NSDictionary *regData  = nil;
    NSDictionary *plistData  = nil;
    regData = [self dataForPath:self.keyPath];
    plistData = [self getPlistDictForEvent:_eventTrigger];
    if(![regData isEqualToDictionary:plistData]){
        NSDictionary *diffResult = [self computeDiffBetween:regData
                                                        and:plistData];
        [self patchRegistry:regData withPlist:plistData andDiff:diffResult];
        [self publishEvents:diffResult];
    }else{
        //[[PAEventBus sharedInstance] postEvent:eEvent];
        NSLog(@"Nothing in observed keypath changed...");
    }
}

-(void)publishEvents:(NSDictionary *) diffResult {
   EventMonitorType observedType = _eventTrigger.fileMonitorType;
    for(NSString *key in [diffResult allKeys]) {
        EventMonitorType currentEventOccured;
        // if both null then item does not exist
        if([diffResult[key][0] isEqual:[[NSNull null] description]] && [diffResult[key][1] isEqual:[[NSNull null] description]]) {
            currentEventOccured = 0; // none
            [[PALogger sharedLoggerForClient:PALogClientSelfHeal] debug:@"======>Plist monitoring: Item does not exist [%@]", key];
        } else if([diffResult[key][0] isEqual:[[NSNull null] description]]) {
            currentEventOccured = ItemCreated;
            [[PALogger sharedLoggerForClient:PALogClientSelfHeal] debug:@"======>Plist monitoring: Item created [%@]", key];
        } else if([diffResult[key][1] isEqual:[[NSNull null] description]]) {
            currentEventOccured = ItemDeleted;
            [[PALogger sharedLoggerForClient:PALogClientSelfHeal] debug:@"======>Plist monitoring: Item deleted [%@]", key];
        } else {
            currentEventOccured = ItemModified;
            [[PALogger sharedLoggerForClient:PALogClientSelfHeal] debug:@"======>Plist monitoring: Item modified [%@]", key];
        }
        if (observedType == currentEventOccured) {
            // There is matching event!
            NSData *eventData = [NSKeyedArchiver archivedDataWithRootObject:diffResult];
            eventData = [eventData base64EncodedDataWithOptions:0];
            PASelfHealEvent * event = [[PASelfHealEvent alloc] initWithGuid:_eventTrigger.contentGuid triggerInfo:_eventTrigger andData:eventData];
            [[PALogger sharedLoggerForClient:PALogClientSelfHeal] debug:@"======>Plist monitoring event triggered %@", _eventTrigger.contentGuid];
            self.replyBlock(event);
        }
    }
}
-(void)patchRegistry:(NSDictionary *)regData
                withPlist:(NSDictionary *)plistDict
                 andDiff:(NSDictionary*)diffResult {
    //First put everything currently in plist to registry
    NSMutableDictionary *result = [NSMutableDictionary dictionaryWithDictionary:plistDict];
    // Update with diff result
    for (NSString *key in [diffResult allKeys]) {
        result[key] = diffResult[key][1];
    }
    // Write to registry
    [self updateRegistryWith:result plistFilePath:[self registryPath]];
}

-(NSDictionary *)computeDiffBetween:(NSDictionary *)regData and:(NSDictionary*)plistData {
    NSMutableDictionary *result = [@{} mutableCopy];
    // notice that this will neglect keys in dict2 which are not in dict1
    for (NSString *key in [regData allKeys]) {
        id value1 = regData[key];
        id value2 = plistData[key];
        if (![value1 isEqual:value2]) {
            // since the values might be mismatched because value2 is nil
            value2 = (value2)? value2 : [[NSNull null] description];
            result[key] = @[value1, value2];
        }
    }
    
    // for keys in dict2 that we didn't check because they're not in dict1
    NSMutableSet *set1 = [NSMutableSet setWithArray:[regData allKeys]];
    NSMutableSet *set2 = [NSMutableSet setWithArray:[plistData allKeys]];
    [set2 minusSet:set1];
    for (NSString *key in set2) {
        result[key] = @[[[NSNull null] description], plistData[key]];
    }
    return [result copy];
}
- (void)updateRegistryWith:(NSDictionary *)newDomain plistFilePath:(NSString *)plistFilePath {
    for(id key in newDomain) {
        id value = [newDomain objectForKey:key];
        // do something with key and obj
        PARegistryWriter* writer = [[PARegistryWriter alloc] initWithPlistAtPath:plistFilePath];
        PAPlistLockManager *lckMgr = [PAPlistLockManager alloc];
        [lckMgr runWithWriteLock:plistFilePath codeBlock:^{
            
            [writer setRegValueForPath:self.keyPath withKey:key andData:value];
        }];
    }
}

- (void)setUserRegistryWithGuid:(PASelfHealTriggerInfo*)event {
    
    //find out keys
    NSArray *plistKeys = [ NSArray arrayWithArray:event.plistKeyPaths];
    NSString *moniterFilePath = event.fileMonitorPaths[0];
    
    NSMutableDictionary *newDomain = [[NSMutableDictionary alloc] init];
    [newDomain setObject:moniterFilePath forKey:@"fileMonitorPath"];
    id value = nil;
    NSString *valueFromArray = nil;
    NSString *trimedKey=nil;
    NSString *lastString=nil;
    PAPlistReader *reader =nil;
    if([[NSFileManager defaultManager] fileExistsAtPath:moniterFilePath]){
       //[ newDomain setValue:@"true" forKey:@"IsFileExist"];
        reader = [[PAPlistReader alloc]initWithPlistAtPath:moniterFilePath];
       for(int i=0;i<plistKeys.count;i++ ){;
           NSString *key = plistKeys[i];
           
          if([ key hasPrefix:@"/"]){
              key = [ key substringFromIndex:1];
          }
         NSInteger count = [[ key componentsSeparatedByString:@"/"] count];
          if(count>1){
              NSInteger tempIndex = (NSInteger)([key rangeOfString:@"/" options:NSBackwardsSearch].location);
              trimedKey = [key substringWithRange:NSMakeRange(0, tempIndex)];
              lastString = [key substringFromIndex:tempIndex];
              if( [lastString containsString:@"Item"]){
                  value = [reader valueForKeyPath:trimedKey withSeparator:@"/"];
                  if(value && [value isKindOfClass:[NSArray class]]) {
                      NSString *indexString = [key stringByReplacingOccurrencesOfString:@"Item" withString:@""];
                      NSInteger index = [indexString integerValue];
                      valueFromArray = index < [value count] ? value[index]: nil;
                      if(valueFromArray) {
                          [ newDomain setValue:valueFromArray forKey:plistKeys[i]];
                      }
                  }
              }
              else{
                  value = [reader valueForKeyPath:key withSeparator:@"/"];
                  if(value) {
                      [ newDomain setValue:value forKey:plistKeys[i]];
                  }
                  
              }
          }
           else{
               value = [reader valueForKeyPath:key withSeparator:@"/"];
               if(value) {
                   [ newDomain setValue:value forKey:plistKeys[i]];
               }
               
           }
       }
   }
   
    [self updateRegistryWith:newDomain plistFilePath:[self registryPath]];
    

}
-(NSDictionary*)dataForPath:(NSString*)path
{
    
    NSDictionary *data = nil;
    NSString* plistFilePath = [self registryPath];
    PARegistryReader *regReader = [[ PARegistryReader alloc] initWithPlistAtPath:plistFilePath];
    id fileContents = [regReader rootDictionary];
    if ([fileContents isKindOfClass:[NSArray class]])
    {
        NSArray* contents = (NSArray*)fileContents;
        for (NSDictionary* object in contents) {
            NSString* key = [object valueForKey:PATH_KEY];
            //key = [regReader nativePathForPath:key];
            if (key && [path isEqualToString:key]) {
                data = [object valueForKey:DATA_KEY];
                break;
            }
        }
    }
     return data;
}
- (NSDictionary*)getPlistDictForEvent:(PASelfHealTriggerInfo*)event
{
    //find out keys
    NSArray *plistKeys = [ NSArray arrayWithArray:event.plistKeyPaths];
    NSString *moniterFileName = event.fileMonitorPaths[0];
    
    NSMutableDictionary *newDomain = [[NSMutableDictionary alloc] init];
    [newDomain setObject:moniterFileName forKey:@"fileMonitorPath"];
    id value = nil;
    NSString *valueFromArray = nil;
    NSString *trimedKey=nil;
    NSString *lastString=nil;
    PAPlistReader *reader =nil;
    if([[NSFileManager defaultManager] fileExistsAtPath:moniterFileName]){
        //[ newDomain setValue:@"true" forKey:@"IsFileExist"];
        reader = [[PAPlistReader alloc] initWithPlistAtPath:moniterFileName];
        for(int i=0;i<plistKeys.count;i++ ){;
            NSString *key = plistKeys[i];
            
            if([ key hasPrefix:@"/"]){
                key = [ key substringFromIndex:1];
            }
            NSInteger count = [[ key componentsSeparatedByString:@"/"] count];
            if(count>1){
                NSInteger tempIndex = (NSInteger)([key rangeOfString:@"/" options:NSBackwardsSearch].location);
                trimedKey = [key substringWithRange:NSMakeRange(0, tempIndex)];
                lastString = [key substringFromIndex:tempIndex];
                if( [lastString containsString:@"Item"]){
                    value = [reader valueForKeyPath:trimedKey withSeparator:nil];
                    NSString *indexString = [key stringByReplacingOccurrencesOfString:@"Item" withString:@""];
                    NSInteger index = [indexString integerValue];
                    valueFromArray = value[index];
                    [ newDomain setValue:valueFromArray forKey:plistKeys[i]];
                }
                else{
                    value = [reader valueForKeyPath:key withSeparator:@"/"];
                    [ newDomain setValue:value forKey:plistKeys[i]];
                }
            }
            else{
                value = [reader valueForKeyPath:key withSeparator:@"/"];
                [ newDomain setValue:value forKey:plistKeys[i]];
            }
        }
    }
    
    return newDomain;
}
// Returns the registry path based on user mode. 
- (NSString *)registryPath {
    NSString *registryPath = [_fsManager hkcuRegistryPlistPath];
    switch (_eventTrigger.userMode) {
        case admin:
            registryPath = [_fsManager hklmRegistryPlistPath];
            break;
        default:
            break;
    }
    
    if(![_fsManager doesFileExist:registryPath]) {
        [@[] writeToFile:registryPath atomically:YES];
    }
    return registryPath;
}
@end

