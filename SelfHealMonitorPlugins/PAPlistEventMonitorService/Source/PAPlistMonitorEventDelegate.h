//
//  PAPlistMonitorEventDelegate.h
//  ProactiveAssistAgent
//
//  Copyright © 2019 Avolin. All rights reserved.
//

#ifndef PAPlistMonitorEventDelegate_h
#define PAPlistMonitorEventDelegate_h

@class PAPlistMonitorServiceProvider;
@class PASelfHealEvent;

@protocol PAPlistMonitorEventDelegate
@required

- (void) plistMonitor:(PAPlistMonitorServiceProvider *)plistMonitor eventOccurred:(PASelfHealEvent *)event;

@end

#endif /* PAFileMonitorEventDelegate_h */
