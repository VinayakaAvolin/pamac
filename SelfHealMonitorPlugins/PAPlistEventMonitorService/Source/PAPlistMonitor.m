//
//  PAPlistMonitor.m
//  ProactiveAssistAgent
//
//  Copyright © 2019 Avolin. All rights reserved.
//

#import "PAPlistMonitor.h"
#import <CoreServices/CoreServices.h>

NSString *const PAPlistMonitoringEventStreamCreationFailureException = @"PAEventsEventStreamCreationFailureException";

@interface PAPlistMonitor () {
    FSEventStreamRef _eventStream;
}

@property (nonatomic) id<PAPlistMonitorEventDelegate> delegate;
@property (nonatomic) PASelfHealTriggerInfo* triggerInfo;
@property (nonatomic) NSURL * fileURL;
typedef void (^PAPlistMonitoringEventBlock)(PAPlistMonitor *fileMonitor, PASelfHealTriggerInfo *triggerInfo);
@property (nonatomic) PAPlistMonitoringEventBlock eventBlock;

// The FSEvents callback function
static void PAEventsCallback(
                             ConstFSEventStreamRef streamRef,
                             void *callbackCtxInfo,
                             size_t numEvents,
                             void *eventPaths,
                             const FSEventStreamEventFlags eventFlags[],
                             const FSEventStreamEventId eventIds[]);

@end

@implementation PAPlistMonitor

-(instancetype) initWithTriggerInfo: (PASelfHealTriggerInfo *)triggerInfo withDelegate:(id<PAPlistMonitorEventDelegate>) delegate {
    if (self = [super init]) {
        self.delegate = delegate;
        self.triggerInfo = triggerInfo;
    }
    return self;
}

-(void) startMonitoring
{
    [self constructFileMonitoringEvent];
}

-(void) stopMonitoring
{
    if (_eventStream) {
        FSEventStreamStop(_eventStream);
        FSEventStreamInvalidate(_eventStream);
        FSEventStreamRelease(_eventStream);
        _eventStream = NULL;
    }
}

-(void) constructFileMonitoringEvent
{
    FSEventStreamContext callbackCtx;
    callbackCtx.version            = 0;
    callbackCtx.info            = (void *)CFBridgingRetain(self);
    callbackCtx.retain            = NULL;
    callbackCtx.release            = NULL;
    callbackCtx.copyDescription    = NULL;
    
    NSInteger totalFiles = [self.triggerInfo.fileMonitorPaths count];
    NSMutableArray *watchedPaths = [NSMutableArray arrayWithCapacity:totalFiles];
    for(int i=0;i<totalFiles; i++){
        NSString *filePath = self.triggerInfo.fileMonitorPaths[i];
        [watchedPaths addObject:filePath];
    }
   
    
    _eventStream = FSEventStreamCreate(kCFAllocatorDefault,
                                       &PAEventsCallback,
                                       &callbackCtx,
                                       (__bridge CFArrayRef)watchedPaths,
                                       kFSEventStreamEventIdSinceNow,
                                       0.01,
                                       ((kFSEventStreamCreateFlagUseCFTypes |
                                         kFSEventStreamCreateFlagFileEvents | kFSEventStreamCreateFlagNoDefer)));
//    FSEventStreamScheduleWithRunLoop(_eventStream,
//                                     [[NSRunLoop currentRunLoop] getCFRunLoop],
//                                     kCFRunLoopDefaultMode);
    FSEventStreamSetDispatchQueue(_eventStream, dispatch_get_main_queue());

    if (!FSEventStreamStart(_eventStream)) {
        [NSException raise:PAPlistMonitoringEventStreamCreationFailureException
                    format:@"Failed to create event stream."];
    }
}

static void PAEventsCallback(
                             ConstFSEventStreamRef streamRef,
                             void *callbackCtxInfo,
                             size_t numEvents,
                             void *eventPaths, // CFArrayRef
                             const FSEventStreamEventFlags eventFlags[],
                             const FSEventStreamEventId eventIds[])
{
    PAPlistMonitor *watcher            = (__bridge PAPlistMonitor *)callbackCtxInfo;
    [watcher _eventsAtPath:(__bridge NSArray *)eventPaths flags:eventFlags];
}


- (void)_eventsAtPath:(NSArray *)paths flags:(const FSEventStreamEventFlags [])flags
{
    NSLog(@"%@", paths);
    [self.delegate plistMonitor:nil eventOccurred:nil];
}

@end
