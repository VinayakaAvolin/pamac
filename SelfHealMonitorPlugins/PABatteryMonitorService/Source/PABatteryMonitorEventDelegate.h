//
//  PABatteryMonitorEventDelegate.h
//  ProactiveAssist
//
//  Copyright © 2018 Avolin. All rights reserved.
//

#ifndef PABatteryMonitorEventDelegate_h
#define PABatteryMonitorEventDelegate_h

@class PABatteryMonitorServiceProvider;
@class PASelfHealEvent;

@protocol PABatteryMonitorEventDelegate
@required

- (void)batteryMonitor:(PABatteryMonitorServiceProvider *)batteryMonitor eventOccurred:(PASelfHealEvent *)event;
@end

#endif /* PABatteryMonitorEventDelegate_h */
