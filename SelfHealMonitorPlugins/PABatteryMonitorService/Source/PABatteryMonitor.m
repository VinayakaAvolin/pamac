//
//  PABatteryMonitor.m
//  PAFileMonitorService
//
//  Copyright © 2018 Avolin. All rights reserved.
//

#import "PABatteryMonitor.h"
#include <IOKit/ps/IOPowerSources.h>
#import <PACoreServices/PACoreServices.h>

static CFRunLoopSourceRef batteryRunLoopSrcRef = NULL;


@interface PABatteryMonitor ()
{
    id<PABatteryMonitorEventDelegate> _delegate;
    NSMutableArray * batteryLevelsToMonitor;
    PASelfHealTriggerInfo *_triggerInfo;
}
@property (nonatomic, assign)NSUInteger previousLevel;
@end


@implementation PABatteryMonitor


static void powerSourceCallback(void * object)
{
    CFTypeRef    blob = IOPSCopyPowerSourcesInfo();
    CFArrayRef   batterySources = IOPSCopyPowerSourcesList(blob);
    unsigned    resourceCount = (unsigned) CFArrayGetCount(batterySources);
    unsigned int index;
    for (index = 0U; index < resourceCount; ++index)
    {
        CFTypeRef        powerSource;
        CFDictionaryRef description;
        powerSource = CFArrayGetValueAtIndex(batterySources, index);
        description = IOPSGetPowerSourceDescription(blob, powerSource);
        [(__bridge PABatteryMonitor *)object updateBatteryInfo:(__bridge NSDictionary *)description];
    }
    CFRelease(blob);
    CFRelease(batterySources);
}


//-(instancetype) initWithDelegate:(id<PABatteryMonitorEventDelegate>) delegate forBatteryLevelsTobeMonitored:(NSArray *) batteryLevel

-(instancetype) initWithTriggerInfo: (PASelfHealTriggerInfo *)triggerInfo withDelegate:(id<PABatteryMonitorEventDelegate>) delegate
{
    if (self = [super init]) {
        _delegate = delegate;
        _triggerInfo = triggerInfo;
        batteryLevelsToMonitor = [NSMutableArray arrayWithArray:[triggerInfo batteryLevelPercentages]];
    }
    return self;
}

-(void) startMonitoring
{
    [self subscribeToBatteryChange];
}

-(void) stopMonitoring
{
    CFRunLoopRemoveSource(CFRunLoopGetMain(), batteryRunLoopSrcRef, kCFRunLoopDefaultMode);
    CFRelease(batteryRunLoopSrcRef);
}

-(void) subscribeToBatteryChange {
    batteryRunLoopSrcRef = IOPSNotificationCreateRunLoopSource(powerSourceCallback,(__bridge void *)(self));
    if (batteryRunLoopSrcRef) {
        CFRunLoopAddSource([[NSRunLoop mainRunLoop] getCFRunLoop], batteryRunLoopSrcRef, kCFRunLoopCommonModes);
    }
}


-(void) updateBatteryInfo:(NSDictionary *) batteryInfo {
    // parse the battery info and trigger the event based on the battery percentage dips on certain levels (batteryLevelsToMonitor)
    if (batteryInfo && batteryInfo.allKeys.count) {
        NSNumber * batteryCapacity = [batteryInfo objectForKey:@"Current Capacity"];
        if ([batteryLevelsToMonitor containsObject:batteryCapacity] && self.previousLevel != batteryCapacity.unsignedIntegerValue) {
            
            NSString *statusString = [NSString stringWithFormat:@"Battery Level = %@", batteryCapacity];
            NSData *data = [NSKeyedArchiver archivedDataWithRootObject:statusString];
            if (data) {
                data = [data base64EncodedDataWithOptions:0];
            }
            PASelfHealEvent * event = [[PASelfHealEvent alloc] initWithGuid:_triggerInfo.contentGuid triggerInfo:_triggerInfo andData:data];
            [[PALogger sharedLoggerForClient:PALogClientSelfHeal] debug:@"======>Battery monitoring event triggered %@", _triggerInfo.contentGuid];
            [_delegate batteryMonitor:nil eventOccurred:event];
        }
        self.previousLevel = batteryCapacity.unsignedIntegerValue;
    }
}

@end
