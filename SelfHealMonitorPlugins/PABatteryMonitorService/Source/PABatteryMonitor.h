//
//  PABatteryMonitor.h
//  PAFileMonitorService
//
//  Copyright © 2018 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PABatteryMonitorEventDelegate.h"
#import "PASelfHealTriggerInfo.h"
@interface PABatteryMonitor : NSObject

//-(instancetype) initWithDelegate:(id<PABatteryMonitorEventDelegate>) delegate forBatteryLevelsTobeMonitored:(NSArray *) batteryLevel;
-(instancetype) initWithTriggerInfo: (PASelfHealTriggerInfo *)triggerInfo withDelegate:(id<PABatteryMonitorEventDelegate>) delegate;

-(void) startMonitoring;

-(void) stopMonitoring;
@end
