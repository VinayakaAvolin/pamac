//
//  PABatteryMonitorServiceProvide.h
//  PAFileMonitorService
//
//  Copyright © 2018 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PAMonitorServiceProvider.h"
#import "PABatteryMonitorEventDelegate.h"
@class PASelfHealTriggerInfo;
@interface PABatteryMonitorServiceProvider : PAMonitorServiceProvider <PABatteryMonitorEventDelegate>

@end

