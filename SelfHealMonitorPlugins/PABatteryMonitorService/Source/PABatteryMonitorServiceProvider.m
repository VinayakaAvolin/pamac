//
//  PABatteryMonitorServiceProvide.m
//  PAFileMonitorService
//
//  Copyright © 2018 Avolin. All rights reserved.
//

#import "PABatteryMonitorServiceProvider.h"
#import "PABatteryMonitor.h"

@interface PABatteryMonitorServiceProvider ()
{
    PABatteryMonitor * _batteryMonitor;
}

@property (nonatomic) PAMonitorServiceProviderResult replyBlock;

@end

@implementation PABatteryMonitorServiceProvider


- (BOOL)canHandleCapability:(SelfHealEventClass)capability forEvent:(PASelfHealTriggerInfo*)eventInfo
{
    return capability == BatteryLevelMonitoring;
}

- (void) startMonitoringEvent:(PASelfHealTriggerInfo*)eventInfo
{
    NSLog(@"Battery Event monitoring started");
    //TODO: Identify the paths and data to be monitored.
    
    if (!_batteryMonitor) {
        //_batteryMonitor = [[PABatteryMonitor alloc] initWithDelegate:self forBatteryLevelsTobeMonitored:@[]];
        _batteryMonitor = [[PABatteryMonitor alloc] initWithTriggerInfo:eventInfo withDelegate:self];
    }
    [_batteryMonitor startMonitoring];
    
}

- (void) startMonitoringEvent:(PASelfHealTriggerInfo*)eventTrigger withReply:(PAMonitorServiceProviderResult)reply
{
    NSLog(@"Battery Event monitoring started");
    //TODO: Identify the paths and data to be monitored.
    
    if (!_batteryMonitor) {
        //_batteryMonitor = [[PABatteryMonitor alloc] initWithDelegate:self forBatteryLevelsTobeMonitored:@[]];
        _batteryMonitor = [[PABatteryMonitor alloc] initWithTriggerInfo:eventTrigger withDelegate:self];
    }
    self.replyBlock = reply;
    [_batteryMonitor startMonitoring];
}

- (void) stopMonitoringEvent:(PASelfHealTriggerInfo*)eventInfo
{
    NSLog(@"Event monitoring ended");
    if (_batteryMonitor) {
        [_batteryMonitor stopMonitoring];
    }
    _batteryMonitor = nil;
}

- (void)batteryMonitor:(PABatteryMonitorServiceProvider *)batteryMonitor eventOccurred:(PASelfHealEvent *)event
{
    self.replyBlock(event);
}

@end
