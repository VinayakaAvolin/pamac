//
//  PAAppleScriptMonitor.m
//  PAAppleScriptMonitorService
//
//  Copyright © 2019 Avolin. All rights reserved.
//
#import <PACoreServices/PACoreServices.h>
#import "PAAppleScriptMonitor.h"

@interface PAAppleScriptMonitor ()

@property (nonatomic) id<PAAppleScriptMonitorEventDelegate> delegate;
@property (nonatomic) PASelfHealTriggerInfo * eventInfo;
@property (nonatomic) NSTimer * timer;
@property (nonatomic) NSString *installTimeChecksum;
@end

@implementation PAAppleScriptMonitor
-(instancetype) initWithTriggerInfo: (PASelfHealTriggerInfo *)triggerInfo withDelegate:(id<PAAppleScriptMonitorEventDelegate>) delegate {
    if (self = [super init]) {
        self.delegate = delegate;
        self.eventInfo = triggerInfo;
    }
    return self;
}

- (void)setupTimerAndPoll {
    __weak PAAppleScriptMonitor *weakSelf = self;
    
    self.timer = [NSTimer timerWithTimeInterval:self.eventInfo.interval repeats:YES block:^(NSTimer * _Nonnull timer) {
        if ([self checkScriptIntegrity]) {
            [PAScriptRunner executeScriptWithPath:self.eventInfo.appleScriptPath callBack:^(BOOL success, NSError *error, id scriptResult) {
                // Publish event only if script returns result
                if (scriptResult) {
                    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:scriptResult];
                    if (data) {
                        data = [data base64EncodedDataWithOptions:0];
                    }
                    
                    PASelfHealEvent * eEvent = [[PASelfHealEvent alloc] initWithGuid:weakSelf.eventInfo.contentGuid triggerInfo:self.eventInfo andData:data];
                    [[PALogger sharedLoggerForClient:PALogClientSelfHeal] debug:@"======>Applescript monitoring event triggered %@", weakSelf.eventInfo.contentGuid];
                    [weakSelf.delegate applescriptMonitor:nil eventOccurred:eEvent];
                }

            }];
        }
    }];
    [[NSRunLoop mainRunLoop] addTimer:self.timer forMode:NSRunLoopCommonModes];
}

-(void) startMonitoring {
    [self setupTimerAndPoll];
}

-(void) stopMonitoring {
    [self.timer invalidate];
    self.timer = nil;
    self.delegate = nil;
}

- (NSString *)checksumForFileAtPath:(NSString *)contentFolderPath {
    NSString *assistAgentPath = [PAConfigurationManager appConfigurations].assistAgentFolderPath;
    PAFileSystemManager *fsManager = [[PAFileSystemManager alloc]init];
    assistAgentPath = [fsManager stringByExpandingPath:assistAgentPath];
    NSBundle* bundle = [NSBundle bundleWithPath:assistAgentPath];
    NSString* dirScriptPath = [bundle pathForResource:@"genfilemd5" ofType:@"sh"];
    
    
    NSTask *task = [[NSTask alloc] init];
    task.launchPath = dirScriptPath;
    task.arguments = [NSArray arrayWithObjects:contentFolderPath, nil];
    
    NSPipe *pipe = [NSPipe pipe];
    task.standardOutput = pipe;
    
    NSFileHandle *fileHandle = [pipe fileHandleForReading];
    [task launch];
    [task waitUntilExit];
    
    NSString *newMd5 = [[NSString alloc] initWithData:[fileHandle availableData] encoding:NSUTF8StringEncoding];
    newMd5 = [newMd5 stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    return newMd5;
}

-(BOOL)checkScriptIntegrity {
    if (self.installTimeChecksum == nil) {
        self.installTimeChecksum = [self checksumForFileAtPath:self.eventInfo.appleScriptPath];
        return YES;
    }
    NSString *currentChecksum = [self checksumForFileAtPath:self.eventInfo.appleScriptPath];
    return [self.installTimeChecksum isEqualToString:currentChecksum];
}

@end
