//
//  PAAppleScriptMonitor.h
//  PAAppleScriptMonitorService
//
//  Copyright © 2019 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PAAppleScriptMonitorEventDelegate.h"
#import "PASelfHealTriggerInfo.h"


NS_ASSUME_NONNULL_BEGIN

@interface PAAppleScriptMonitor : NSObject

-(instancetype) initWithTriggerInfo: (PASelfHealTriggerInfo *)triggerInfo withDelegate:(id<PAAppleScriptMonitorEventDelegate>) delegate;

-(void) startMonitoring;

-(void) stopMonitoring;

@end

NS_ASSUME_NONNULL_END
