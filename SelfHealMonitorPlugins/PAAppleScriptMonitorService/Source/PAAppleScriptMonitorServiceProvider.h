//
//  PAAppleScriptMonitorServiceProvider.h
//  PAAppleScriptMonitorService
//
//  Copyright © 2019 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PAMonitorServiceProvider.h"
#import "PAAppleScriptMonitorEventDelegate.h"

NS_ASSUME_NONNULL_BEGIN

@interface PAAppleScriptMonitorServiceProvider : PAMonitorServiceProvider<PAAppleScriptMonitorEventDelegate>

@end

NS_ASSUME_NONNULL_END
