//
//  PAAppleScriptMonitorEventDelegate.h
//  ProactiveAssist
//
//  Copyright © 2019 Avolin. All rights reserved.
//

#ifndef PAAppleScriptMonitorEventDelegate_h
#define PAAppleScriptMonitorEventDelegate_h
@class PAAppleScriptMonitorServiceProvider;
@class PASelfHealEvent;

@protocol PAAppleScriptMonitorEventDelegate
@required

- (void) applescriptMonitor:(PAAppleScriptMonitorServiceProvider *)scriptMonitor eventOccurred:(PASelfHealEvent *)event;

@end

#endif /* PAAppleScriptMonitorEventDelegate_h */
