//
//  PAAppleScriptMonitorServiceProvider.m
//  PAAppleScriptMonitorService
//
//  Copyright © 2019 Avolin. All rights reserved.
//

#import "PAAppleScriptMonitorServiceProvider.h"
#import "PAAppleScriptMonitor.h"

@interface PAAppleScriptMonitorServiceProvider ()
{
    PAAppleScriptMonitor *_scriptMonitor;
}
@property (nonatomic) PAMonitorServiceProviderResult replyBlock;

@end

@implementation PAAppleScriptMonitorServiceProvider


- (BOOL)canHandleCapability:(SelfHealEventClass)capability forEvent:(PASelfHealTriggerInfo*)event
{
    return capability == AppleScriptMonitoring;
}

- (void) startMonitoringEvent:(PASelfHealTriggerInfo*)eventTrigger
{
    NSLog(@"AppleScript monitoring started");
    
    if(!_scriptMonitor)
    {
        _scriptMonitor = [[PAAppleScriptMonitor alloc] initWithTriggerInfo:eventTrigger withDelegate:self];
    }
    [_scriptMonitor startMonitoring];
    
}

- (void) startMonitoringEvent:(PASelfHealTriggerInfo*)eventTrigger withReply:(PAMonitorServiceProviderResult)reply {
    NSLog(@"AppleScript monitoring started");
    
    if(!_scriptMonitor)
    {
        _scriptMonitor = [[PAAppleScriptMonitor alloc] initWithTriggerInfo:eventTrigger withDelegate:self];
    }
    self.replyBlock = reply;
    [_scriptMonitor startMonitoring];
}

- (void) stopMonitoringEvent:(PASelfHealTriggerInfo*)event
{
    NSLog(@"AppleScript monitoring ended");
    [_scriptMonitor stopMonitoring];
}


- (void) applescriptMonitor:(PAAppleScriptMonitorServiceProvider *)scriptMonitor eventOccurred:(PASelfHealEvent *)event {
    self.replyBlock(event);
}
@end
