//
//  PAApplicationMonitor.h
//  ProactiveAssistAgent
//
//  Copyright © 2019 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PAApplicationMonitorEventDelegate.h"
#import "PASelfHealTriggerInfo.h"


static const NSString *const kPreferencesFolder = @"~/Library/Preferences";
//static const NSString *const kCrashLogFolder = @"~/Library/logs/DiagnosticReports";
static const NSString *const kCrashReporter = @"~/Library/Application Support/CrashReporter";
static const NSString *const kFirefoxCrashReports = @"~/Library/Application Support/Firefox/Crash Reports";
#define DATA_KEY @"Data"
#define PATH_KEY @"Path"


@interface PAApplicationMonitor : NSObject

-(instancetype) initWithTriggerInfo: (PASelfHealTriggerInfo *)triggerInfo withDelegate:(id<PAApplicationMonitorEventDelegate>) delegate;
-(void) startMonitoring;

-(void) stopMonitoring;

@end
