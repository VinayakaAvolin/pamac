//
//  PAPlistMonitorEventDelegate.h
//  ProactiveAssistAgent
//
//  Copyright © 2019 Avolin. All rights reserved.
//

#ifndef PAApplicationMonitorEventDelegate_h
#define PAApplicationMonitorEventDelegate_h
@class PAApplicationMonitorServiceProvider;
@class PASelfHealTriggerInfo;
@class PASelfHealEvent;

@protocol PAApplicationMonitorEventDelegate
@required

- (void) applicationMonitor:(PAApplicationMonitorServiceProvider *)applicationMonitor eventOccurred:(PASelfHealEvent *)event;
@end

#endif /* PAFileMonitorEventDelegate_h */
