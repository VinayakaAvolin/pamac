//
//  PAPlistMonitor.m
//  ProactiveAssistAgent
//
//  Copyright © 2019 Avolin. All rights reserved.
//

#import <PACoreServices/PACoreServices.h>
#import <CoreServices/CoreServices.h>
#import <AppKit/AppKit.h>
#import "PAApplicationMonitor.h"

NSString *const PAApplicationMonitoringEventStreamCreationFailureException = @"PAEventsEventStreamCreationFailureException";

@interface PAApplicationMonitor () {
    FSEventStreamRef _eventStream;
}

@property (nonatomic) id<PAApplicationMonitorEventDelegate> delegate;
@property (nonatomic) PASelfHealTriggerInfo* triggerInfo;
@property (nonatomic) NSTimer * timer;

typedef void (^PAApplicationMonitoringEventBlock)(PAApplicationMonitor *applicationMonitor, PASelfHealTriggerInfo *triggerInfo);

@property (nonatomic) PAApplicationMonitoringEventBlock eventBlock;

// The FSEvents callback function
static void PAEventsCallback(
                             ConstFSEventStreamRef streamRef,
                             void *callbackCtxInfo,
                             size_t numEvents,
                             void *eventPaths,
                             const FSEventStreamEventFlags eventFlags[],
                             const FSEventStreamEventId eventIds[]);

@end

@implementation PAApplicationMonitor

-(instancetype) initWithTriggerInfo: (PASelfHealTriggerInfo *)triggerInfo withDelegate:(id<PAApplicationMonitorEventDelegate>) delegate {
    if (self = [super init]) {
        self.delegate = delegate;
        self.triggerInfo = triggerInfo;
    }
    return self;
}

-(void) startMonitoring
{
    //[self constructFileMonitoringEvent];
     [self setupTimerAndPoll];
}

-(void) stopMonitoring
{
    [self.timer invalidate];
    self.timer = nil;
    self.delegate = nil;
}

-(void) constructFileMonitoringEvent
{
    FSEventStreamContext callbackCtx;
    callbackCtx.version            = 0;
    callbackCtx.info            = (void *)CFBridgingRetain(self);
    callbackCtx.retain            = NULL;
    callbackCtx.release            = NULL;
    callbackCtx.copyDescription    = NULL;
    
    NSInteger totalFiles = [self.triggerInfo.fileMonitorPaths count];
    NSMutableArray *watchedPaths = [NSMutableArray arrayWithCapacity:totalFiles];
    for(int i=0;i<totalFiles; i++){
        NSString *filePath = self.triggerInfo.fileMonitorPaths[i];
        [watchedPaths addObject:filePath];
    }
   
    
    _eventStream = FSEventStreamCreate(kCFAllocatorDefault,
                                       &PAEventsCallback,
                                       &callbackCtx,
                                       (__bridge CFArrayRef)watchedPaths,
                                       kFSEventStreamEventIdSinceNow,
                                       0,
                                       ((kFSEventStreamCreateFlagUseCFTypes |
                                         kFSEventStreamCreateFlagFileEvents))
                                       );
    FSEventStreamScheduleWithRunLoop(_eventStream,
                                     [[NSRunLoop currentRunLoop] getCFRunLoop],
                                     kCFRunLoopDefaultMode);
    if (!FSEventStreamStart(_eventStream)) {
        [NSException raise:PAApplicationMonitoringEventStreamCreationFailureException
                    format:@"Failed to create event stream."];
    }
}

static void PAEventsCallback(
                             ConstFSEventStreamRef streamRef,
                             void *callbackCtxInfo,
                             size_t numEvents,
                             void *eventPaths, // CFArrayRef
                             const FSEventStreamEventFlags eventFlags[],
                             const FSEventStreamEventId eventIds[])
{
    PAApplicationMonitor *watcher            = (__bridge PAApplicationMonitor *)callbackCtxInfo;
   
    [watcher _eventsAtPath:(__bridge NSArray *)eventPaths flags:eventFlags];
    
//    char **paths = eventPaths;
//
//    // printf("Callback called\n");
//    for (int i=0; i<numEvents; i++) {
//        int count;
//        /* flags are unsigned long, IDs are uint64_t */
//        printf("Change %llu in %s, flags %lu\n", eventIds[i], paths[i], eventFlags[i]);
//    }
    
    
//    //    NSArray *eventPathsArray    = (__bridge NSArray *)eventPaths;
//    //    NSArray *excludedURLs        = //[watcher excludedURLs];
//
//        for (NSUInteger i = 0; i < numEvents; ++i) {
//    //        FSEventStreamEventFlags flags = eventFlags[i];
//    //        FSEventStreamEventId identifier = eventIds[i];
//
//            // We do this hackery to ensure that the eventPath string doesn't
//            // contain any trailing slash.
//    //        NSURL *eventURL        = [NSURL fileURLWithPath:[[eventPathsArray objectAtIndex:i] stringByStandardizingPath]];
//
//
//
//            //if (!shouldIgnore)
//            {
//    //            PASelfHealEvent *event = [[PAFileMonitorEvent alloc] initWithIdentifier:identifier date:[NSDate date] URL:eventURL flags:flags];
//    
//                PAFileMonitoringEventBlock eventBlock = [watcher eventBlock];
//                eventBlock(watcher, [watcher event]);
//            }
//        }
//
//
}


- (void)_eventsAtPath:(NSArray *)paths flags:(const FSEventStreamEventFlags [])flags
{
 /*   NSLog(@"%@", paths);
    
    BOOL shouldPostEvent = FALSE;
    //assuming only one path will come at a time...
    NSString *path = paths[0];
   
    NSString *folderPath =[[[paths objectAtIndex:0] stringByExpandingTildeInPath] stringByDeletingLastPathComponent ];
    NSString *crashLogFolder = [kCrashReporter stringByExpandingTildeInPath];
    NSString *firefoxCrashFolder = [kFirefoxCrashReports stringByExpandingTildeInPath];
    NSString *prefFolder = [kPreferencesFolder stringByExpandingTildeInPath];
    if([crashLogFolder caseInsensitiveCompare:folderPath]==NSOrderedSame){
        self.triggerInfo.appMonitorType = appCrashFileCreated;
        
        NSArray* contents = (NSArray*)self.triggerInfo.applicationAttributes;
     
        for (NSDictionary* object in contents) {
            NSString *bundle_name = [object valueForKey:@"event_monitor_app_bundle_name"];
            if([path containsString:bundle_name]){
                shouldPostEvent = TRUE;
                break;
                
            }
        }
        if(shouldPostEvent){
            NSDictionary *crashFileDict = [ NSDictionary dictionaryWithContentsOfFile:path];
        if ([[crashFileDict allKeys] containsObject:@"Path"])
            [self.delegate applicationMonitor:nil eventOccurred:self.triggerInfo];
        }
        
    }
    else if([firefoxCrashFolder caseInsensitiveCompare:folderPath]==NSOrderedSame){
        self.triggerInfo.appMonitorType = appCrashFileCreated;
        if([path containsString:@"LastCrash"]){
            shouldPostEvent = TRUE;
           [self.delegate applicationMonitor:nil eventOccurred:self.triggerInfo];
        }
        
    }
    else if([prefFolder caseInsensitiveCompare:folderPath]==NSOrderedSame){
        self.triggerInfo.appMonitorType = appSettingChanged;
        NSArray *monitoringPaths = (NSArray *)self.triggerInfo.fileMonitorPaths;
        if([monitoringPaths containsObject:path]){
                [self.delegate applicationMonitor:nil eventOccurred:self.triggerInfo];
            }
    }
    
    

   // NSString *path = @"/Users/sudhaker/Desktop/com.apple.WebKit.WebContent_2019-01-02-132731_SudhakerB-MBP-2\ 2.21.54\ PM.crash";
    //NSString *nativePath = [path stringByConvertingIntoMacCompatibleFilePath];
   
    UInt32 value1 = kFSEventStreamEventFlagItemCreated;
    UInt32 value2 =  kFSEventStreamEventFlagItemModified;
    UInt32 value3 =  kFSEventStreamEventFlagItemRemoved;
    
    
//        && flags[0] & kFSEventStreamEventFlagItemModified
//        && !(flags[0] & kFSEventStreamEventFlagItemIsDir)
//        && !(flags[0] & kFSEventStreamEventFlagItemIsSymlink)
//        && !(flags[0] & kFSEventStreamEventFlagItemFinderInfoMod)) {
//
//        NSLog(@"%@", @"sudhaker");
//    }
//    NSError * error = nil;
//    //NSFileExtendedAttributes
//    NSDictionary * attrs = [[NSFileManager defaultManager] attributesOfItemAtPath:path error:&error];
//    if (attrs && !error){
//       
//        NSDictionary *fileExtendedAttributes = [attrs objectForKey:@"NSFileExtendedAttributes"];
//        NSData *bundle_id = [fileExtendedAttributes valueForKey:@"bundle_id"];
//        NSString* newStr = [[NSString alloc] initWithData:bundle_id encoding:NSUTF8StringEncoding];
//        NSLog(@"bundle_id: %@",bundle_id);
//    }
//    else{
//        
//    }
   
    

//     uint32 flag = flags[0];
//    if(flag & kFSEventStreamEventFlagItemCreated){
//        NSLog(@"%@", @"kFSEventStreamEventFlagItemCreated");
//    }
//    else if(flag & kFSEventStreamEventFlagItemRemoved)
//    {
//        NSLog(@"%@", @"kFSEventStreamEventFlagItemRemoved");
//    }
//    else if(flag & kFSEventStreamEventFlagItemModified){
//        NSLog(@"%@", @"kFSEventStreamEventFlagItemModified");
//    }
  
     
     const unsigned short TOTAL_EVENTS = 9;
     unsigned int anEvents[TOTAL_EVENTS];
     anEvents[0] = 125184;  download
     anEvents[1] = 105728;  another way to download
     anEvents[2] = 116992;  another way to download
     anEvents[3] = 128256;  same folder paste
     anEvents[4] = 108544;  new file created
     anEvents[5] = 110848;  another way new file created
     anEvents[6] = 67584;  file dragged in
     anEvents[7] = 127232;  file inside folder that was dragged in
     anEvents[8] = 128768;  file replace on paste
  
    
    //98304 = file modified
    //4295680 = just opened plist
    //    100352 or 4294656 = modified plist/
    */
}

- (void)setupTimerAndPoll {
    //typeof(self) __weak weakSelf = self;
    __weak PAApplicationMonitor *weakSelf = self;
    self.timer = [NSTimer timerWithTimeInterval:self.triggerInfo.interval
                                            repeats:YES block:^(NSTimer * _Nonnull timer) {
        weakSelf.triggerInfo.appMonitorType = appStatusMonitoring;
        ApplicationMonitorType appMonitorType = weakSelf.triggerInfo.appMonitorType;
        NSString *bundle_identifier = nil;
        if(appMonitorType == appStatusMonitoring)
        {
            NSMutableArray *runningApplications = [[NSMutableArray alloc] init];
            NSInteger apps = [weakSelf.triggerInfo.applicationAttributes count];
            for( int i = 0 ; i < apps; i++){
                NSDictionary* object = weakSelf.triggerInfo.applicationAttributes[i];
                bundle_identifier = [object valueForKey:@"event_monitor_app_bundle_identifier"];;
                if([weakSelf isAppWithBundleIdRunning:bundle_identifier]){
                    [runningApplications addObject:bundle_identifier];
                    
                    PASelfHealEvent * eEvent = [[PASelfHealEvent alloc] initWithGuid:weakSelf.triggerInfo.contentGuid triggerInfo:weakSelf.triggerInfo andData:nil];
                    [[PALogger sharedLoggerForClient:PALogClientSelfHeal] debug:@"======>Application monitoring event triggered %@", weakSelf.triggerInfo.contentGuid];
                    [weakSelf.delegate applicationMonitor:nil eventOccurred:eEvent];
                   
                }
            }
            
        }
    }];
    [[NSRunLoop mainRunLoop] addTimer:self.timer forMode:NSRunLoopCommonModes];
}

- (BOOL)isAppWithBundleIdRunning:(NSString *)bundleId {
   
   NSArray *matchingApps = [ NSRunningApplication runningApplicationsWithBundleIdentifier:bundleId];
    if (matchingApps.count) {
        return true;
    }
    
    return false;
    
}
@end



