//
//  PAPlistMonitorServiceProvider.m
//  ProactiveAssistAgent
//
//  Copyright © 2019 Avolin. All rights reserved.
//
#import <PACoreServices/PACoreServices.h>
#import "PAApplicationMonitorServiceProvider.h"
#import "PAApplicationMonitor.h"
#import "PAEventBus.h"
#import "PAPlistWrapper.h"
#import "PAPlistUtility.h"
#import "PAPlistLockManager.h"
#import "PASelfHealTriggerInfo.h"


@interface PAApplicationMonitorServiceProvider ()
{
    PAApplicationMonitor * applicationMonitor;
   
}
@property (nonatomic) PAMonitorServiceProviderResult replyBlock;

@end


@implementation PAApplicationMonitorServiceProvider

- (BOOL)canHandleCapability:(SelfHealEventClass)capability forEvent:(PASelfHealTriggerInfo*)eventTrigger
{
    return capability == AppiicationMonitoring;
}


- (void) startMonitoringEvent:(PASelfHealTriggerInfo*)eventTrigger withReply:(PAMonitorServiceProviderResult)reply
{
    [[PALogger sharedLoggerForClient:PALogClientSelfHeal] debug:@"Application monitoring started for [%@]",eventTrigger.contentGuid];
    NSArray* contents = (NSArray*)eventTrigger.applicationAttributes;
    NSMutableArray *folderPaths =[[ NSMutableArray alloc] init];
    for (NSDictionary* object in contents) {
         NSString *bundle_identifier = [object valueForKey:@"event_monitor_app_bundle_identifier"];
         //NSString *bundle_name = [object valueForKey:@"event_monitor_app_bundle_name"];
         NSString *preferencesFile = [ NSString stringWithFormat:@"%@/%@%@", [kPreferencesFolder stringByExpandingTildeInPath],bundle_identifier,@".plist"];
         [folderPaths addObject:preferencesFile];
        /*
        NSString *crashFileWithAppName = [ NSString stringWithFormat:@"%@/%@", [kCrashReporter stringByExpandingTildeInPath],
                                          @"Safari_CCA8DD59-154D-5580-A8C9-008D64D7A93F.plist"];
        NSString *crashFileWithBundleName = [ NSString stringWithFormat:@"%@/%@", [kCrashReporter stringByExpandingTildeInPath],bundle_identifier];
        [folderPaths addObject:crashFileWithAppName];
        [folderPaths addObject:crashFileWithBundleName];
        */
    }
    [folderPaths addObject:[kCrashReporter stringByExpandingTildeInPath]];
    [folderPaths addObject:[kFirefoxCrashReports stringByExpandingTildeInPath]];
    
    eventTrigger.fileMonitorPaths = (NSArray*)folderPaths;
    
    
    
    if(!applicationMonitor){
        applicationMonitor = [[PAApplicationMonitor alloc] initWithTriggerInfo:eventTrigger withDelegate:self];
    }
     self.replyBlock=reply;
    
    [applicationMonitor startMonitoring];
    
}

- (void) stopMonitoringEvent:(PASelfHealTriggerInfo*)eventTrigger
{
    NSLog(@"Firewall monitoring ended");
    [applicationMonitor stopMonitoring];
}

- (void) applicationMonitor:(PAApplicationMonitorServiceProvider *)applicationMonitor eventOccurred:(PASelfHealEvent *)event
{
   self.replyBlock(event);
}

@end

