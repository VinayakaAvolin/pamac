//
//  PAPlistMonitorServiceProvider.h
//  ProactiveAssist
//
//  Copyright © 2019 Avolin. All rights reserved.
//


#import "PAApplicationMonitorEventDelegate.h"
#import "PAMonitorServiceProvider.h"

@interface PAApplicationMonitorServiceProvider : PAMonitorServiceProvider <PAApplicationMonitorEventDelegate>



@end
