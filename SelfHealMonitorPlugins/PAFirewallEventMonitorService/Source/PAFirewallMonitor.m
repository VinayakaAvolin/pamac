//
//  PAFirewallMonitor.m
//  PAFirewallEventMonitorService
//
//  Copyright © 2018 Avolin. All rights reserved.
//
#import <PACoreServices/PACoreServices.h>
#import "PAFirewallMonitor.h"
#import "PAScriptRunner.h"

// /Library/Preferences/com.apple.alf.plist /globalstate
static NSString *const kFirewallPrefPlist = @"/Library/Preferences/com.apple.alf.plist";

@interface PAFirewallMonitor () {
    FSEventStreamRef _eventStream;
}


@property (nonatomic) id<PAFirewallMonitorEventDelegate> delegate;
@property (nonatomic) PASelfHealTriggerInfo * eventInfo;
@property (nonatomic) NSTimer * timer;
@property (nonatomic, assign) BOOL previousOnState;
@end

@implementation PAFirewallMonitor

-(instancetype) initWithTriggerInfo: (PASelfHealTriggerInfo *)triggerInfo withDelegate:(id<PAFirewallMonitorEventDelegate>) delegate
{
    if (self = [super init]) {
        self.delegate = delegate;
        self.eventInfo = triggerInfo;
    }
    return self;
}

- (void)setupTimerAndPoll {
    
    __weak PAFirewallMonitor * weakSelf = self;
    
    NSTimer *timer = [NSTimer timerWithTimeInterval:self.eventInfo.interval repeats:YES block:^(NSTimer * _Nonnull timer) {
        [PAScriptRunner executeScript:@"/usr/libexec/ApplicationFirewall/socketfilterfw --getglobalstate"
                               ofType:PAScriptTypeShell
                             callBack:^(BOOL success, NSError *error, id result) {
                                 PAFirewallMonitor * strongSelf = weakSelf;
                                 
                                 NSLog(@"result = %@", result); // Firewall is enabled. (State = 1)
                                 if (!error && [result isKindOfClass:[NSString class]]) {
                                     NSString * statusValue = (NSString *) result;
                                     if (statusValue && statusValue.length) {
                                         NSString * resultValue = [[statusValue componentsSeparatedByString:@"(State = "] lastObject];
                                         BOOL state = [[resultValue substringToIndex:resultValue.length-2] boolValue];
            
                                         if (state == NO && strongSelf.eventInfo.firewallStatus == Disabled) {
                                             NSString *statusString = [NSString stringWithFormat:@"Firewall Status = %d", state];
                                             [strongSelf publishFirewallEventWithStatus:statusString];
                                         } else if (strongSelf.eventInfo.firewallStatus == Enabled && state == YES) {
                                             NSString *statusString = [NSString stringWithFormat:@"Firewall Status = %d", state];
                                             [strongSelf publishFirewallEventWithStatus:statusString];
                                         }
                                     }
                                 }
                             }];
    }];

    [[NSRunLoop mainRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];

}

- (void)publishFirewallEventWithStatus:(NSString*)statusString {
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:statusString];
    if (data) {
        data = [data base64EncodedDataWithOptions:0];
    }
    PASelfHealEvent * event = [[PASelfHealEvent alloc] initWithGuid:self.eventInfo.contentGuid triggerInfo:self.eventInfo andData:data];
    [[PALogger sharedLoggerForClient:PALogClientSelfHeal] debug:@"======>Firewall monitoring event triggered %@", self.eventInfo.contentGuid];
    
    [self.delegate firewallMonitor:nil eventOccurred:event];
}

-(void) startMonitoring
{
   // [self setupTimerAndPoll];
    [self startFirewallMonitoring];
}

-(void) stopMonitoring
{
    //[self.timer invalidate];
    //self.timer = nil;
    self.delegate = nil;
    self.eventInfo = nil;
    if (_eventStream) {
        FSEventStreamStop(_eventStream);
        FSEventStreamInvalidate(_eventStream);
        FSEventStreamRelease(_eventStream);
        _eventStream = NULL;
    }
}

-(void) startFirewallMonitoring {
    FSEventStreamContext callbackCtx;
    callbackCtx.version            = 0;
    callbackCtx.info            = (void *)CFBridgingRetain(self);
    callbackCtx.retain            = NULL;
    callbackCtx.release            = NULL;
    callbackCtx.copyDescription    = NULL;
    NSArray *watchedPaths = @[kFirewallPrefPlist];
    _eventStream = FSEventStreamCreate(kCFAllocatorDefault,
                                       &PAEventsCallback,
                                       &callbackCtx,
                                       (__bridge CFArrayRef)watchedPaths,
                                       kFSEventStreamEventIdSinceNow,
                                       0.01,
                                       ((kFSEventStreamCreateFlagUseCFTypes |
                                         kFSEventStreamCreateFlagFileEvents | kFSEventStreamCreateFlagNoDefer)));
    FSEventStreamSetDispatchQueue(_eventStream, dispatch_get_main_queue());
    
    if (!FSEventStreamStart(_eventStream)) {
        [[PALogger sharedLoggerForClient:PALogClientSelfHeal] debug:@"======>Firewall monitoring alf.plist  file monitoring failed to start"];
    }
    PAPlistReader *plistReader = [[PAPlistReader alloc] initWithPlistAtPath: kFirewallPrefPlist];
    id state = [plistReader valueForKeyPath:@"globalstate" withSeparator:nil];
    if(state) {
        self.previousOnState = ((NSNumber *)state).boolValue;
    }
}

-(void)handleMonitoredChange {
    __weak PAFirewallMonitor * weakSelf = self;

    [PAScriptRunner executeScript:@"/usr/libexec/ApplicationFirewall/socketfilterfw --getglobalstate" ofType:PAScriptTypeShell
                         callBack:^(BOOL success, NSError *error, id result) {
                             NSLog(@"result = %@", result);
                             if (!error && [result isKindOfClass:[NSString class]]) {
                                 NSString * statusValue = (NSString *) result;
                                 if (statusValue && statusValue.length) {
                                     NSString * resultValue = [[statusValue componentsSeparatedByString:@"(State = "] lastObject];
                                     BOOL turnedOn = [[resultValue substringToIndex:resultValue.length-2] boolValue];
                                     if (turnedOn && weakSelf.eventInfo.firewallStatus == Enabled && weakSelf.previousOnState == NO) {
                                         [weakSelf publishFirewallEventWithStatus:[NSString stringWithFormat:@"Firewall Status = %d", turnedOn]];
                                     } else if(turnedOn == NO && weakSelf.eventInfo.firewallStatus == Disabled && weakSelf.previousOnState == YES) {
                                         [weakSelf publishFirewallEventWithStatus:[NSString stringWithFormat:@"Firewall Status = %d", turnedOn]];
                                     }
                                     weakSelf.previousOnState = turnedOn;
                                 }
                             }
                         }];
}

static void PAEventsCallback(
                             ConstFSEventStreamRef streamRef,
                             void *callbackCtxInfo,
                             size_t numEvents,
                             void *eventPaths, // CFArrayRef
                             const FSEventStreamEventFlags eventFlags[],
                             const FSEventStreamEventId eventIds[])
{
    PAFirewallMonitor *monitor = (__bridge PAFirewallMonitor *)callbackCtxInfo;
    NSArray * eventPathList = (__bridge NSArray *)eventPaths;
    if ( [eventPathList[0] isEqualToString:kFirewallPrefPlist]) {
        [monitor handleMonitoredChange];
    }
}


@end
