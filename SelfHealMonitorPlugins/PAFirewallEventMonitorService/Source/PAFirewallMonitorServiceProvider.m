//
//  PAFirewallMonitorServiceProvider.m
//  PAFirewallEventMonitorService
//
//  Copyright © 2018 Avolin. All rights reserved.
//

#import "PAFirewallMonitorServiceProvider.h"
#import "PAFirewallMonitor.h"


@interface PAFirewallMonitorServiceProvider ()
{
    PAFirewallMonitor * _firewallMonitor;
}
@property (nonatomic) PAMonitorServiceProviderResult replyBlock;

@end

@implementation PAFirewallMonitorServiceProvider


- (BOOL)canHandleCapability:(SelfHealEventClass)capability forEvent:(PASelfHealTriggerInfo*)eventInfo
{
    NetworkMonitorType type = [eventInfo networkMonitorType];
    return (type == Firewall);
}

- (void) startMonitoringEvent:(PASelfHealTriggerInfo*)eventTrigger
{
    NSLog(@"Firewall monitoring started");
    //TODO: Identify the paths and data to be monitored.
    if (!_firewallMonitor) {
    
        _firewallMonitor = [[PAFirewallMonitor alloc] initWithTriggerInfo:eventTrigger withDelegate:self];
    }
    [_firewallMonitor startMonitoring];
    
}

- (void) startMonitoringEvent:(PASelfHealTriggerInfo*)eventTrigger withReply:(PAMonitorServiceProviderResult)reply
{
    NSLog(@"Firewall monitoring started");
    //TODO: Identify the paths and data to be monitored.
    if (!_firewallMonitor) {
        
        _firewallMonitor = [[PAFirewallMonitor alloc] initWithTriggerInfo:eventTrigger withDelegate:self];
    }
    self.replyBlock = reply;
    [_firewallMonitor startMonitoring];
}

- (void) stopMonitoringEvent:(PASelfHealTriggerInfo*)event
{
    NSLog(@"Firewall monitoring ended");
    [_firewallMonitor stopMonitoring];
}

- (void) firewallMonitor:(PAFirewallMonitorServiceProvider *)batteryMonitor eventOccurred:(PASelfHealEvent *)event
{
    self.replyBlock(event);
}

@end
