//
//  PAFirewallMonitorEventDelegate.h
//  ProactiveAssist
//
//  Copyright © 2018 Avolin. All rights reserved.
//

#ifndef PAFirewallMonitorEventDelegate_h
#define PAFirewallMonitorEventDelegate_h

@class PAFirewallMonitorServiceProvider;
@class PASelfHealEvent;

@protocol PAFirewallMonitorEventDelegate
@required

- (void) firewallMonitor:(PAFirewallMonitorServiceProvider *)firewallMonitor eventOccurred:(PASelfHealEvent *)event;

@end

#endif /* PAFirewallMonitorEventDelegate_h */
