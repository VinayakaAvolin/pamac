//
//  PAFirewallMonitor.h
//  PAFirewallEventMonitorService
//
//  Copyright © 2018 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PAFirewallMonitorEventDelegate.h"
#import "PASelfHealTriggerInfo.h"

@interface PAFirewallMonitor : NSObject

-(instancetype) initWithTriggerInfo: (PASelfHealTriggerInfo *)triggerInfo withDelegate:(id<PAFirewallMonitorEventDelegate>) delegate;


-(void) startMonitoring;

-(void) stopMonitoring;

@end

