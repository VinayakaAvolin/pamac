//
//  PAFirewallMonitorServiceProvider.h
//  PAFirewallEventMonitorService
//
//  Copyright © 2018 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PAFirewallMonitorEventDelegate.h"
#import "PAMonitorServiceProvider.h"

@interface PAFirewallMonitorServiceProvider : PAMonitorServiceProvider<PAFirewallMonitorEventDelegate>

@end

