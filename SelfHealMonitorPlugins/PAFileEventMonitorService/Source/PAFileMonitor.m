//
//  PAFileMonitor.m
//  PAFileEventMonitorService
//
//  Copyright © 2018 Avolin. All rights reserved.
//

#import "PAFileMonitor.h"
#import <PACoreServices/PACoreServices.h>
#import <CoreServices/CoreServices.h>

NSString *const PAFileMonitoringEventStreamCreationFailureException = @"PAEventsEventStreamCreationFailureException";
static CFTimeInterval const latency = 0.01;
@interface PAFileMonitor () {
    FSEventStreamRef _eventStream;
}

@property (nonatomic) id<PAFileMonitorEventDelegate> delegate;
@property (nonatomic) PASelfHealTriggerInfo* triggerInfo;
//@property (nonatomic) NSURL * fileURL;
typedef void (^PAFileMonitoringEventBlock)(PAFileMonitor *fileMonitor, PASelfHealTriggerInfo *event);
@property (nonatomic) PAFileMonitoringEventBlock eventBlock;
@property (nonatomic) NSArray *watchPaths;
@property (nonatomic) NSArray *watchedFolders;
@property (nonatomic) NSTimeInterval lastEventInterval;

// The FSEvents callback function   
static void PAEventsCallback(
                             ConstFSEventStreamRef streamRef,
                             void *callbackCtxInfo,
                             size_t numEvents,
                             void *eventPaths,
                             const FSEventStreamEventFlags eventFlags[],
                             const FSEventStreamEventId eventIds[]);

@end

@implementation PAFileMonitor

-(instancetype) initWithTriggerInfo: (PASelfHealTriggerInfo *)triggerInfo withDelegate:(id<PAFileMonitorEventDelegate>) delegate ;
{
    if (self = [super init]) {
        self.delegate = delegate;
        self.triggerInfo = triggerInfo;
        //self.fileURL = url;
    }
    return self;
}

-(void) startMonitoring
{
    [self constructFileMonitoringEvent];
}

-(void) stopMonitoring
{
    if (_eventStream) {
        FSEventStreamStop(_eventStream);
        FSEventStreamInvalidate(_eventStream);
        FSEventStreamRelease(_eventStream);
        _eventStream = NULL;
    }
}

- (void)setupWatchFolders {
    NSMutableArray *watchDirs = [NSMutableArray new];
    for (NSString *watchPath in self.watchPaths) {
        BOOL isDir;
        NSString *watchedDir;
        [[NSFileManager defaultManager] fileExistsAtPath:watchPath isDirectory:&isDir];
        if (!isDir) {
            watchedDir = [watchPath stringByDeletingLastPathComponent];
        } else {
            watchedDir = watchPath;
        }
        [watchDirs addObject:watchedDir];
    }
    self.watchedFolders = [watchDirs copy];
}

-(void) constructFileMonitoringEvent
{
    FSEventStreamContext callbackCtx;
    callbackCtx.version            = 0;
    callbackCtx.info            = ( void *)CFBridgingRetain(self);
    callbackCtx.retain            = NULL;
    callbackCtx.release            = NULL;
    callbackCtx.copyDescription    = NULL;
    
    NSInteger totalFiles = [self.triggerInfo.fileMonitorPaths count];
    NSMutableArray *watchedPaths = [NSMutableArray arrayWithCapacity:totalFiles];
    for(int i=0;i<totalFiles; i++){
        NSString *filePath = [(NSString*)self.triggerInfo.fileMonitorPaths[i] stringByExpandingTildeInPath];
        [watchedPaths addObject:filePath];
    }
    self.watchPaths = [watchedPaths copy];
    if (self.triggerInfo.fileMonitorShouldExcludeSubDirs) {
        [self setupWatchFolders];
    }
    
    _eventStream = FSEventStreamCreate(kCFAllocatorDefault,
                                       &PAEventsCallback,
                                       &callbackCtx,
                                       (__bridge CFArrayRef)watchedPaths,
                                       kFSEventStreamEventIdSinceNow,
                                       latency,
                                       ((kFSEventStreamCreateFlagUseCFTypes |
                                         kFSEventStreamCreateFlagFileEvents | kFSEventStreamCreateFlagNoDefer)));

    FSEventStreamSetDispatchQueue(_eventStream, dispatch_get_main_queue());

    if (!FSEventStreamStart(_eventStream)) {
        [NSException raise:PAFileMonitoringEventStreamCreationFailureException
                    format:@"Failed to create event stream."];
    }
}

static void PAEventsCallback(
                             ConstFSEventStreamRef streamRef,
                             void *callbackCtxInfo,
                             size_t numEvents,
                             void *eventPaths, // CFArrayRef
                             const FSEventStreamEventFlags eventFlags[],
                             const FSEventStreamEventId eventIds[])
{
    PAFileMonitor *watcher            = (__bridge PAFileMonitor *)callbackCtxInfo;
    [watcher _eventsAtPath:(__bridge NSArray *)eventPaths flags:eventFlags and:numEvents];
}


- (void)_eventsAtPath:(NSArray *)paths flags:(const FSEventStreamEventFlags [])eventFlags and:(size_t)numEvents
{
    for (NSUInteger i = 0; i < numEvents; i++) {
        NSLog(@"%@", paths);
        NSString *status = nil;
        if (eventFlags[i] & (kFSEventStreamEventFlagItemModified | kFSEventStreamEventFlagItemRenamed)) {
            
            if (self.triggerInfo.fileMonitorType == ItemCreated) {
                NSDictionary* fileAttribs = [[NSFileManager defaultManager] attributesOfItemAtPath: paths[i] error:nil];
                NSDate *creationDate = fileAttribs[NSFileCreationDate];
                NSDate *now = [NSDate date];
                NSLog(@"Creation date = %f", [creationDate timeIntervalSince1970]);
                NSLog(@"Now = %f", [now timeIntervalSince1970]);
                if ([now timeIntervalSince1970] - [creationDate timeIntervalSince1970] < 1.0) {
                        status = @"File Status = Created";
                    
                }
            } else if ([[NSFileManager defaultManager] fileExistsAtPath:paths[i]]) {
                if (self.triggerInfo.fileMonitorType == ItemModified) {
                    NSDictionary* fileAttribs = [[NSFileManager defaultManager] attributesOfItemAtPath: paths[i] error:nil];
                    NSDate *modificationDate = fileAttribs[NSFileModificationDate];
                    NSDate *now = [NSDate date];
                    NSLog(@"Modification date = %f", [modificationDate timeIntervalSince1970]);
                    NSLog(@"Now = %f", [now timeIntervalSince1970]);
                    if (([now timeIntervalSince1970] - [modificationDate timeIntervalSince1970] < 1.0)) {
                        status = @"File Status = Modified";
                    }
                }
            } else {
                if(self.triggerInfo.fileMonitorType == ItemDeleted) {
                    status = @"File Status = Deleted";
                }
            }
        } else if(eventFlags[i] & kFSEventStreamEventFlagItemCreated) {
            if (self.triggerInfo.fileMonitorType == ItemCreated) {
                NSDictionary* fileAttribs = [[NSFileManager defaultManager] attributesOfItemAtPath: paths[i] error:nil];
                NSDate *creationDate = fileAttribs[NSFileCreationDate];
                NSDate *now = [NSDate date];
                if ([now timeIntervalSince1970] - [creationDate timeIntervalSince1970] < 1.0) {
                    status = @"File Status = Created";
                }
            }
        } else if(eventFlags[i] & kFSEventStreamEventFlagItemRemoved) {
            if(self.triggerInfo.fileMonitorType == ItemDeleted) {
                status = @"File Status = Deleted";
            }
        } 

        if (status) {
            BOOL shouldIgnore = YES;
            if(self.triggerInfo.fileMonitorShouldExcludeSubDirs) {
                NSURL *eventUrl = [NSURL fileURLWithPath: paths[i]];
                NSString *eventOccuredDir;

                if(![eventUrl hasDirectoryPath]) {
                    eventOccuredDir = [[eventUrl URLByDeletingLastPathComponent] path];
                } else {
                    eventOccuredDir = paths[i];
                }
                
                for (NSString *folder in self.watchedFolders) {
                    if ([folder isEqualToString:eventOccuredDir]) {
                        shouldIgnore = NO;
                        break;
                    }
                }
            } else {
                shouldIgnore = NO;
            }
            NSTimeInterval current = [[NSDate date] timeIntervalSince1970];
            NSTimeInterval elapsedSinceLast = fabs(current - self.lastEventInterval);
            NSTimeInterval intervalWindow = latency * 10;
            if (shouldIgnore == NO && elapsedSinceLast > intervalWindow) {
                // This event is of interest to us. Publish!
                status = [NSString stringWithFormat:@"%@ for file[%@]", status, paths[i]];
                NSData *data = [NSKeyedArchiver archivedDataWithRootObject:status];
                if (data) {
                    data = [data base64EncodedDataWithOptions:0];
                }
                PASelfHealEvent * event = [[PASelfHealEvent alloc] initWithGuid:_triggerInfo.contentGuid triggerInfo:_triggerInfo andData:data];
                [[PALogger sharedLoggerForClient:PALogClientSelfHeal] debug:@"======>File monitoring event triggered %@", _triggerInfo.contentGuid];
                [self.delegate fileMonitor:nil eventOccurred:event];
                NSLog(@"Time elapsed = %f", elapsedSinceLast);
                self.lastEventInterval = current;
            }

        }

    }

}

@end
