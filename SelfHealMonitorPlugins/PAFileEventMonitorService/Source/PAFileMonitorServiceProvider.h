//
//  PAFileMonitorServiceProvider.h
//  PAFileEventMonitorService
//
//  Copyright © 2018 Avolin. All rights reserved.
//
#import <Foundation/Foundation.h>
#import "PAFileMonitorEventDelegate.h"
#import "PAMonitorServiceProvider.h"
@interface PAFileMonitorServiceProvider : PAMonitorServiceProvider <PAFileMonitorEventDelegate>

@end

