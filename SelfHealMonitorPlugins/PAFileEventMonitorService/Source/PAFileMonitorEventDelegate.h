//
//  PAFileMonitorEventDelegate.h
//  ProactiveAssist
//
//  Copyright © 2018 Avolin. All rights reserved.
//

#ifndef PAFileMonitorEventDelegate_h
#define PAFileMonitorEventDelegate_h

@class PAFileMonitorServiceProvider;
@class PASelfHealEvent;

@protocol PAFileMonitorEventDelegate
@required

- (void) fileMonitor:(PAFileMonitorServiceProvider *)fileMonitor eventOccurred:(PASelfHealEvent *)event;

@end

#endif /* PAFileMonitorEventDelegate_h */
