//
//  PAFileMonitor.h
//  PAFileEventMonitorService
//
//  Copyright © 2018 Avolin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PAFileMonitorEventDelegate.h"
#import "PASelfHealTriggerInfo.h"

@interface PAFileMonitor : NSObject
/*
-(instancetype) initWithURL: (NSURL* )url withDelegate:(id<PAFileMonitorEventDelegate>) delegate forStatus:(PASelfHealTriggerInfo*) event;*/

-(instancetype) initWithTriggerInfo: (PASelfHealTriggerInfo *)triggerInfo withDelegate:(id<PAFileMonitorEventDelegate>) delegate;

-(void) startMonitoring;

-(void) stopMonitoring;

@end

