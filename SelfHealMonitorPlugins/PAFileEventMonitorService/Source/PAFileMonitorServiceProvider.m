//
//  PAFileMonitorServiceProvider.m
//  PAFileEventMonitorService
//
//  Copyright © 2018 Avolin. All rights reserved.
//
#import <PACoreServices/PACoreServices.h>
#import "PAFileMonitorServiceProvider.h"
#import "PAFileMonitor.h"
#import "PAEventBus.h"
#import "PASelfHealTriggerInfo.h"

@interface PAFileMonitorServiceProvider ()
{
    PAFileMonitor * _fileMonitor;
}
@property (nonatomic) PAMonitorServiceProviderResult replyBlock;

@end


@implementation PAFileMonitorServiceProvider

- (BOOL)canHandleCapability:(SelfHealEventClass)capability forEvent:(PASelfHealTriggerInfo*)eventInfo
{
    return capability == FileMonitoring;
}

- (void) startMonitoringEvent:(PASelfHealTriggerInfo*)eventInfo
{
    NSLog(@"File monitoring started");
    //TODO: Identify the paths and data to be monitored.
    
    if(!_fileMonitor)
    {
         _fileMonitor = [[PAFileMonitor alloc] initWithTriggerInfo:eventInfo withDelegate:self];
        
    }
    [_fileMonitor startMonitoring];
    
}

- (void) startMonitoringEvent:(PASelfHealTriggerInfo*)eventTrigger withReply:(PAMonitorServiceProviderResult)reply
{
    NSLog(@"File monitoring started");
    //TODO: Identify the paths and data to be monitored.
    
    if(!_fileMonitor)
    {
        _fileMonitor = [[PAFileMonitor alloc] initWithTriggerInfo:eventTrigger withDelegate:self];
        
    }
    self.replyBlock = reply;
    [_fileMonitor startMonitoring];
}
- (void) stopMonitoringEvent:(PASelfHealTriggerInfo*)eventInfo
{
    NSLog(@"File monitoring ended");
    [_fileMonitor stopMonitoring];
}

- (void) fileMonitor:(PAFileMonitorServiceProvider *)fileMonitor eventOccurred:(PASelfHealEvent *)event
{
    self.replyBlock(event);
}
@end
